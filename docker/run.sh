#!/bin/bash

############################################################
# run binary_c using docker on Linux/Unix
############################################################

############################################################
# we create a volume in binary_c_persistent : this
# is a persistent file space that is stored even when
# binary_c stops
############################################################
docker volume create binary_c_volume >/dev/null

############################################################
# Allow connections to our X display
############################################################
XAUTH=$(mktemp)
xauth nlist $DISPLAY |  sed -e 's/^..../ffff/' | xauth -f $XAUTH nmerge -
chmod 755 $XAUTH

############################################################
# run a bash login shell in the container : 
# this leaves us in an X-connected shell.
#
# Note the chown/chgrp to make the persistent directory
# writable by the user "binary_c"
############################################################
docker run \
       --name=binary_c \
       -it \
       --rm \
       -e DISPLAY=$DISPLAY \
       --ipc=host \
       --net=host \
       --ulimit stack=-1 \
       --mount source=binary_c_volume,target=/home/binary_c/binary_c_persistent \
       -v /tmp/.X11-unix \
       -v $XAUTH \
       -v $HOME/.Xauthority:/home/binary_c/.Xauthority \
       robizzard/binary_c:latest \
       /bin/bash -c "sudo chown binary_c binary_c_persistent ; sudo chgrp binary_c binary_c_persistent; cat /home/binary_c/progs/stars/binary_c/doc/README.docker; export BINARY_C=/home/binary_c/progs/stars/binary_c; bash --login"



# to clean the BINARY_C volume run:
# docker volume rm binary_c_volume

# clean up XAUTH
rm $XAUTH

