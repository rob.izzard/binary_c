#!/usr/bin/env perl
use strict;


############################################################
# shrink the docker file
#
# usage: merge.pl {file}
############################################################

my $docker = slurp($ARGV[0]//'Dockerfile');

# combine subsequent apt-gets
while($docker=~s/RUN\s+apt-get\s+-y\s+--no-install-recommends\s+install\s+(.*)\nRUN\s+apt-get\s+-y\s+--no-install-recommends install\s+(.*)\n/RUN apt-get -y --no-install-recommends install $1 $2\n/){};

# combine RUNs
while($docker=~s/RUN\s+(.*)\nRUN(\s+.*)/RUN $1 __newline__\&\&    $2/){};
$docker=~s/__newline__/\\\n/g;

print "$docker\n";

exit;

sub slurp
{
    # Usage: slurp(<filename>)
    # Returns the contents of file given by <filename> as a string, on failure dies with Carp::confess

    open(my $fh,'<'.$_[0])||confess("cannot open $_[0] in slurp");
    return (do { local( $/ ) ; <$fh> } );
}
