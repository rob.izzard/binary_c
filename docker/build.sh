#!/bin/bash

############################################################
# script to build a docker container from binary_c
#
# Run this script from binary_c's "docker" directory
############################################################

# initial working directory
WD=$PWD
echo "########################################"
echo
echo "binary_c docker build script"
echo
echo "If you get weird errors, you may have run out of disk space"
echo "Run 'docker system prune -a' to clean all of Docker's (tmp) data."
echo
echo "########################################"


# set cache date to 'date ...' for a unique (non-cached build)
#CACHE_DATE=$(date +%Y-%m-%d:%H:%M:%S) # default
# or today to use the cache
CACHE_DATE=today
echo "CACHE_DATE=$CACHE_DATE"

############################################################
# which Ubuntu version?
# currently tested on 22.04
############################################################
UBUNTU_VERSION=22.04

############################################################
# get binary_c version
############################################################
BINARY_C_STABLE_VERSION=$(grep define\ BINARY_C_STABLE_VERSION\ \" ../src/binary_c_version.h |gawk "{print \$3}"|sed s/\"//g)
echo "Building docker container for binary_c stable version $BINARY_C_STABLE_VERSION"

############################################################
# get binary_c-python version
############################################################
BINARY_C_PYTHON_VERSION=$(grep define\ BINARY_C_PYTHON_VERSION\ \" ../src/binary_c_version.h |gawk "{print \$3}"|sed s/\"//g)
echo "      ... and binary_c-python version $BINARY_C_PYTHON_VERSION"

############################################################
# merge docker lines to reduce image size
############################################################

#DOCKERFILE=Dockerfile
DOCKERFILE=Dockerfile.merged
merge.pl Dockerfile > $DOCKERFILE
echo "Merged Dockerfile"

############################################################
# build docker container (this takes a while...)
############################################################
echo "Call docker for build"
docker --log-level=debug build --build-arg=UBUNTU_VERSION=$UBUNTU_VERSION --build-arg BINARY_C_STABLE_VERSION=$BINARY_C_STABLE_VERSION --build-arg BINARY_C_PYTHON_VERSION=$BINARY_C_PYTHON_VERSION --build-arg CACHE_DATE=$CACHE_DATE -t binary_c -f $DOCKERFILE . |& tee build.log

############################################################
# tag docker container
############################################################
echo "Tagging container: robizzard/binary_c:master$BINARY_C_STABLE_VERSION"
docker tag binary_c robizzard/binary_c:master$BINARY_C_STABLE_VERSION

echo "Tagging container: robizzard/binary_c:latest"
docker tag binary_c robizzard/binary_c:latest

############################################################
# now you should do "docker push robizzard/binary_c:master<version>
############################################################
echo "Now you should"
echo
echo "Run the following command to update tagged version $BINARY_C_STABLE_VERSION"
echo
echo "docker push robizzard/binary_c:master$BINARY_C_STABLE_VERSION"
echo
echo "Run the following command to then update the latest tag"
echo
echo "docker push robizzard/binary_c"
echo
