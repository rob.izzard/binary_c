#!/usr/bin/env perl

use Term::ANSIColor;
use robqueue;
use threads;
use threads::shared;
use Thread::Queue;
use rob_misc;
use IPC::System::Simple qw/run capture EXIT_ANY/;
use strict;
use 5.16.0;

#
# Script that tests whether binary_c 2 and binary_c 1.20pre (SVN 3537)
# match.
#

# process id of this script
my $scriptpid = $$;

# number of threads
my $nthreads = ncpus();#-2;

# either stop on error, or label as a fail
my $stop_on_error=0;

# threshold score for failure
my $failthresh=0.1;

# number of perturbed systems to try
my $nperturb=50;

# strip floating point?
my $strip_fp=0;

# max number of systems to run (0 = never stop)
my $maxcount = 0;

# if the stopfile is created, stop
my $stopfile = 'stop';

# maximum memory use, per thread, in MB
my $maxmemuse = 200.0 * $nthreads;
my $m0 = getfree();

my $minmem = $m0 - $maxmemuse;

my $outblock;
share $outblock;
my $default_args = runcmd_capture('tbse echo');
chomp $default_args;

my @bin_cs = (
    $ENV{HOME}.'/binary_c.3537/src/binary_c',
    'binary_c'
    );
my $count=0;
my $runcount=0;

my $chidiff = 'timeout.pl -m 300000 chidiff.pl';

$|=1;
share $runcount;
my @faillist = load_faillist();
share @faillist;
my $failcount=0;
share $failcount;

my $dodiff=0;
my $dochisq=1;

if($ARGV[0])
{
    # we're given args to test : just run this one system
    $nperturb=0;
    run_system(0,'args');
}
else
{
    my $restart=1;
    while($restart)
    {
      
        print color('yellow'),"************************************************************\nRESTART QUEUE\n************************************************************\n",color('reset') if($restart>1);
        $restart=0;
        
        # run random systems
        my $q = robqueue->new(
            nthreads=>$nthreads,
            subr=>\&run_system,    
            prepend_thread_number_to_subroutine=>1, # send thread number 
            );

        # remove existing thread logs
        runcmd('rm /tmp/thread_log*');

        while(1)
        {
            $q->q();
            $q->qwait();
            $count++;
            if($count%10==0)
            {
                my $mem = rob_misc::mem_usage(1,$scriptpid,1);
                
                # allow 200MB each
                $restart = 2 if($mem > $maxmemuse);

                printf "Count = $count     %smem = %s%s ($minmem)     (restart = %d) \n",
                color('magenta'),$mem,color('reset'),$restart;

            }
            last if(($maxcount && $count > $maxcount) ||
                    $restart || -f $stopfile);
            
        }
        $q->end();
    }
}



exit;
############################################################

sub getfree
{
    my $mem =runcmd_capture2('free -tom');
    $mem = ($mem=~/Total\:\s+\S+\s+\S+\s+(\S+)/)[0];
    return $mem;
}

sub run_system
{ 
    my $args;
    state $logfp;
    my $n = shift; # thread number
    my $forcedump=0;
    my $cache = {}; 
    $runcount++;

    if(!defined $logfp)
    {
        open($logfp,'>',"/tmp/thread_log.$n")||die("cannot open thread log in /tmp/thread_log.$n"); 
    }

    my $args0;

    if($_[0] eq 'args')
    {
        $args = "@ARGV";
        $args0 = $args;
        $forcedump = 1;
    }
    else
    {
        if($#faillist==-1)
        {
            my $m1 = rand()*80+0.1;
            my $m2 = MAX(0.1,rand()*$m1);
            my $per = 10.0**(rand()*8.0);
            my $idum = int(rand()*5000);
            my $ecc = MAX(0.0,MIN(0.9999,rand()));
            my $Z = MAX(1e-4,MIN(0.03,1e-4+(0.03-1e-4)*rand()));
            my $jorb_loss = MAX(0,MIN(2,int(3*rand())));

            $args = sprintf "--M_1 %10.5e --M_2 %10.5e --orbital_period %10.5e --eccentricity %10.5e --metallicity %10.5e --idum % 6d --jorb_loss %d",
            $m1,$m2,$per,$ecc,$Z,$idum,$jorb_loss;
        }
        else
        {
            $args = shift @faillist;
        }
        $args0 = $args;
    }
    
    if($runcount)
    {
        printf "RUN % 8d (%6.2f%% failed): thread % 5d :      %s\n",
        $runcount,100.0*$failcount/$runcount,$n,$args;
    }
    print $logfp "Run $args\n";

    my %out;
    my %cmd;
    my $c=0;
    foreach my $executable (@bin_cs)
    {
        #$cmd{$executable} = "env BIN_C=$executable tbse $args";
        $cmd{$executable} = "$executable $default_args $args";

        print $logfp "Unlink /tmp/haverun.$n.$c /tmp/running.$n.$c\n";

        # remove both status files
        unlink "/tmp/haverun.$n.$c","/tmp/running.$n.$c";

        # save command in running file
        print $logfp "Open /tmp/running.$n.$c\n";
        open(my $fp, '>:raw', "/tmp/running.$n.$c") || die("cannot open /tmp/running.$n.$c");
        print $fp $cmd{$executable},"\n";
        close $fp;

        # run the command
        my $cmd= $dochisq ? "$cmd{$executable} --log_filename /tmp/test_log$n.$c  > /tmp/$n.$c.raw; cat /tmp/$n.$c.raw | filter TESTLOG " : 
            "$cmd{$executable} --log_filename /tmp/test_log$n.$c 2>\&1 ";
        
        print $logfp "Run $cmd\n";

        if($dodiff)
        {
            printf "Pre-chisq %s\n",getfree();
            my $x = runcmd_capture($cmd);
            printf "Post-chisq %s\n",getfree();

            # run finished : remove "running" file, write "haverun" file
            print $logfp "Unlink /tmp/running.$n.$c\n";
            unlink "/tmp/running.$n.$c";
            
            print $logfp "Write /tmp/haverun.$n.$c\n";
            open(my $fp, '>:raw', "/tmp/haverun.$n.$c") || die("cannot open /tmp/haverun.$n.$c");
            close $fp;
            
            print $logfp "Clean output (regexps)\m";
            $x=~s/\e\[\d+(?>(;\d+)*)m//g; # remove ANSI codes
            $x=~s/\e\[m//go;
            $x=~s/\x1b[[()=][;?0-9]*[0-9A-Za-z]?//go;
            $x=~s/\007//go;
            
            # remove line numbers : these change between code versions
            $x=~ s/\:\s+\d+\s+\://go;
            # remove pointers
            $x =~ s/0x[0-9a-f]+/0xPOINTER/og;

            # remove log_filenames
            $x =~ s/.*log_filename.*//og;

            $x=~s/Tick count.*//o;
            $x=~s/IDUM.*//o;

            if($strip_fp)
            {
                # strip floating point
                $x =~ s/(\d\.\d+)\de([+-]?\d+)/$1e$2/go;
                $x =~ s/(\d\.\d\d\d\d\d\d+)\d/$1/go;
                
                # reduce accuracy of floating point
                #$x =~ s/(\d\.\d\d\d\d\d\d)\d+e([+-]?\d+)/$1e$2/go;
                #$x =~ s/(\d\.\d\d\d\d\d\d)\d+/$1e$2/go;
            }

            print $logfp "Cache set\n";
            $cache->{$executable} = $x;
        }
        else
        {
            runcmd_capture($cmd." > /tmp/$n.$c.dat");
            # save number of lines also
            `wc -l /tmp/$n.$c.dat > /tmp/wc.$n.$c.dat`;
        }

        $c++;
    }

    my $er=0;

    if($forcedump && $dodiff)
    {
        print $logfp "Force dump /tmp/0.dat\n";
        dumpfile("/tmp/0.dat",$cache->{$bin_cs[0]});
    }

    for(my $i=1;$i<=$#bin_cs;$i++)
    {
        if($forcedump && $dodiff)
        {
            print $logfp "Force dump /tmp/$i.dat\n";
            dumpfile("/tmp/$i.dat",$cache->{$bin_cs[$i]});
        }

        if($dochisq)
        {
            $er=0;
            if(`$chidiff /tmp/$n.0.dat /tmp/$n.$i.dat | grep Score`=~/Score (\S+)/)
            {
                my $score = $1;
                my $fail = $score>$failthresh;
                printf "SCORE (%s %s) %s%g%s : $args\n","/tmp/$n.0.dat","/tmp/$n.$i.dat",($fail ? color('red') : color('green')),$score,color('reset');

                # save definite failures
                if($fail)
                {
                    $er=1;
                    if($nperturb)
                    {
                        # check for bifurcations
                        my $ii=0;
                        print "Check for bifurcation/timestepping issues\n";

                        {
                            state $lock : shared;
                            lock $lock;
                            open(my $b_fp,'>>',$ENV{HOME}.'/bifurcations');
                            print $b_fp $args0,"\n";
                            close $b_fp;
                        }

                        # check whether a shorter timestep helps
                        foreach my $dtmult (0.1,0.01)
                        {
                            # run all the executables, sending the data to /tmp
                            my $cc = 0;
                            my @files;
                            foreach my $executable (@bin_cs)
                            {
                                my $f = "/tmp/chi.$n.$cc";
                                my $cmd = "$cmd{$executable} $args --timestep_modulator $dtmult > $f.raw"; 
                                runcmd_capture($cmd);
                                $cmd = "cat $f.raw | filter TESTLOG > $f.dat"; 
                                runcmd_capture($cmd);
                                push(@files,$f.'.dat');
                                $cc++;
                            }
                            
                            # find the chisq for the new files
                            my $newscore = (`$chidiff @files | grep Score`=~/Score (\S+)/)[0];
                            print "NEWSCORE with dtmult = $dtmult, $ii : $newscore ($args)\n";
                            if($newscore<$failthresh)
                            {
                                # shorter timestep matched the systems
                                print "Timestep mult $dtmult fixed the system\n";
                                $er=0;
                                {
                                    state $lock : shared;
                                    lock $lock;
                                    open(my $newfails_fp,'>>',$ENV{HOME}.'/shorterdt');
                                    print $newfails_fp "(x $dtmult) ",$args0,"\n";
                                    close $newfails_fp;
                                }
                                last;
                            }
                        }

                        if($er)
                        {

                            # %age perturbations
                            my %perturbs = ('M_1',0.02,
                                            'M_2',0.02,
                                            'orbital_period',0.02);
                            my @perturbs = keys %perturbs;
                            my $nfail=0;
                            my $nsuccess=0;
                            while($ii < $nperturb)
                            {
                                # make perturbed arguments
                                my $args = $args0;
                                foreach my $var (@perturbs)
                                {
                                    my $was = ($args=~/--$var (\S+)/)[0];
                                    my $is = sprintf '%g',$was*(1.0 + $perturbs{$var}*(rand()-0.5));
                                    $args=~s/--$var \S+/--$var $is/; 
                                }

                                # run all the executables, sending the data to /tmp
                                my $cc = 0;
                                my @files;
                                foreach my $executable (@bin_cs)
                                {
                                    my $f = "/tmp/chi.$n.$cc.dat";
                                    runcmd_capture("$cmd{$executable} $args | filter TESTLOG > $f");
                                    push(@files,$f);
                                    $cc++;
                                }
                                
                                # find the chisq for the new files
                                my $newscore = (`$chidiff @files | grep Score`=~/Score (\S+)/)[0];
                                print "NEWSCORE $ii : $newscore ($args)\n";

                                # count : end bifurcation checks on success
                                $ii++;
                                if($newscore>$failthresh)
                                {
                                    $nfail++;
                                }
                                else
                                {
                                    $nsuccess++;
                                    print "Found bifurcation point within perturbation\n";
                                    last;
                                }
                            }
                            # report
                            my $ntot = $nsuccess+$nfail;
                            print "BIFURCATION : ran $ntot nearby systems of which $nfail failed and $nsuccess succeeded.\n"; 
                            if($nsuccess==0)
                            {
                                print "NONE succeeded : not a bifurcation point\n";
                                $er=1;
                            }
                            else
                            {
                                $er=0;
                            }
                        }
                        
                    }
                }
            }
            else
            {
                print "SCORE failed : $args\n";
            }
        }

        if($dodiff)
        {
            print $logfp "Diff $i vs 0\n";
            
            if($cache->{$bin_cs[$i]} ne $cache->{$bin_cs[0]} &&
               $cache->{$bin_cs[$i]} !~ /newline/ &&
               $cache->{$bin_cs[0]} !~ /newline/)
            {
                # rerun to check
                print "Diff : rerun...\n";
                if($n!=0 && runcmd_capture("test_new_derivs.pl $args")=~/System matched fine/)
                {
                    print "Rerun ok\n";
                }
                else
                {
                    print "ERROR : output for thread $n : $bin_cs[$i] differs from $bin_cs[0]\n";
                    print "Commands were\n$bin_cs[0] : $cmd{$bin_cs[0]}\n$bin_cs[$i] : $cmd{$bin_cs[$i]}\n";

                    dumpfile("/tmp/0.$n.dat",$cache->{$bin_cs[0]});
                    dumpfile("/tmp/$i.$n.dat",$cache->{$bin_cs[$i]});

                    print runcmd_capture("diff /tmp/0.$n.dat /tmp/$i.$n.dat");

                    print "\n-------\nThe above is the diff of /tmp/0.$n.dat /tmp/$i.$n.dat\n\nArgs were : $args\n\n";
                    
                    $er=1;
                }
            }
        }
    }
    
    if($er==1)
    {
        print $logfp "Error detected\n";
        {
            state $lock : shared;
            lock $lock;
            open(my $newfails_fp,'>>',$ENV{HOME}.'/newfails');
            print $newfails_fp $args0,"\n";
            close $newfails_fp;
        }
        $failcount++;

        if($stop_on_error)
        {
            print "Error detected : stopping\n";
            exit;
        }
    }
    elsif($_[0] eq 'args')
    {
        print $logfp "System matched fine\n";
        print "System matched fine\n";
    }

    print $logfp "Clear cache\n";
    undef $cache;

    print $logfp "Undefs\n";
    undef %cmd;
    undef %out;

    print $logfp "return from run_system\n\n";
    close $logfp;
}

sub runcmd_capture
{
    my $c = join(' ',@_);
    $c = substr($c,0,15).'...'.substr($c,-80,80);
    my $m0 = getfree();
#    printf "Pre-run      mem = %s      ($c)\n",$m0;
    my $x = runcmd_capture2(@_);
    my $m1 = getfree();
#    printf "Post-run     mem = %s, lost = %g MB     ($c)\n",$m1,($m0-$m1);
    return $x;
}

sub runcmd_capture2
{
    my $cmd = $_[0];
    
    # convert to old argument format
    if($cmd=~/3537/)
    {
        $cmd=~s/Bondi_Hoyle_accretion_factor/acc2/;
        $cmd=~s/rotationally_enhanced_exponent/rotationally_enhanced_fudge_power/;
        $cmd=~s/mass_of_pmz/size_of_pmz/;
        $cmd=~s/CRAP_parameter/bb/;
    }

#    print "RUN $cmd\n";
    return capture(EXIT_ANY,'/usr/bin/stdbuf -i 0 -o 0 -e 0 /usr/bin/timeout --foreground --preserve-status --kill-after 121 120 '.$cmd);
}

sub load_faillist
{
    return if(!-f $ENV{HOME}.'/faillist');
    open(my $fp,'<',$ENV{HOME}.'/faillist')||die;
    @faillist = grep { !/^#/ } <$fp>;
    chomp @faillist;
    close $fp;
}
