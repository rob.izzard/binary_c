#!/bin/bash

#
# The binary_c stellar population nucleosynthesis framework.
#
# Copyright Robert Izzard 2000-2024
#
# Contact rob.izzard@gmail.com
#
# Code repository:
# https://gitlab.com/binary_c
#
# Documentation is in doc/
# Code is in src/
#
# To build, run
#
# ---
#
# meson builddir
# cd builddir
# ninja binary_c_install
#
# ---
#
# Please see the files README, LICENCE and CHANGES,
# and the doc/ directory for documentation which is mirrored
# at https://binary_c.gitlab.io
#
######################
#
# Purpose:
#
# script to do profile-guided optimization (PGO) with Meson
#
######################

# make sure BINARY_C is set, or run from the binary_c
# root directory
: "${BINARY_C:=`pwd`}"

# set defaults
#SCRIPT_DIR="$(dirname "$(readlink -f "$0")")"
SCRIPT_DIR="$( cd "$(dirname "$( readlink -f ${BASH_SOURCE[0]} )")" >/dev/null 2>&1 && pwd)"
BUILDDIR="builddir"
N="$1"
: "${N:=1000}"
_NCORES=$(grep "cpu cores" /proc/cpuinfo |head -1|gawk "{print \$4}")
: "${NCORES:=$_NCORES}"
TMP="/tmp/"
export NCORES1=$((NCORES - 1))
export REPEAT_PER_CORE=$((N/NCORES))
SLEEP=0
: "${LTO:=true}"
: "${LTO_THREADS:=$NCORES}"
: "${LTO_MODE:=default}"
MESON="meson"
MESON_FLAGS="--buildtype=release -Db_lto=$LTO -Db_lto_threads=$LTO_THREADS -Db_lto_mode=$LTO_MODE"
NINJA="ninja"
DUMMY="${DUMMY:=0}" # if 1 just do a dummy run
STATUS_SLEEP=2
: "${WARMUP_SECS:=30}"
USE_EXISTING_SYSTEM_LISTS="${USE_EXISTING_SYSTEM_LISTS:=0}"  # use existing system lists?
if [ -f /sys/devices/system/cpu/intel_pstate/no_turbo ]; then
    NO_TURBO_IN=`cat /sys/devices/system/cpu/intel_pstate/no_turbo`
fi

# load functions
source $SCRIPT_DIR/pgo_functions.sh

# require builddir to be removed
if [ -d $BUILDDIR ] ; then
   echo "Please remove $BUILDDIR before we start"
   exit
fi

# enable turbo for non-testing
enable_turbo

# prevent tbse verbosity
export QUIET_TBSE=1

# where to redirect stderr, stdout on meson and ninja calls
MESON_STDERR=$TMP/meson.stderr
MESON_STDOUT=$TMP/meson.stdout
NINJA_STDERR=$TMP/ninja.stderr
NINJA_STDOUT=$TMP/ninja.stdout

# tbse command
TBSE="./tbse"
WARMUP="warmup_cpu $WARMUP_SECS"

# detect CPU speed
if [ -f "/proc/cpuinfo" ]; then
    export HAVE_CPUINFO=1
else
    export HAVE_CPUINFO=0
fi

# status message
echo "Using Meson to build binary_c with Profile Guided Optimization (PGO) on $N systems across $NCORES cores"
echo
echo
echo "CC                         : $CC"
echo "Ncores                     : $NCORES"
echo "N tests                    : $N"
echo "repeat/core                : $REPEAT_PER_CORE"
echo "Meson command              : $MESON"
echo "Meson flags                : $MESON_FLAGS"
echo "Meson flags in build 1     : $MESON_FLAGS1"
echo "Meson flags in build 2     : $MESON_FLAGS2"
echo "Link time optimization?    : $LTO"
echo "Use existing system lists? : $USE_EXISTING_SYSTEM_LISTS"
echo "Ninja command              : $NINJA"
echo "Build directory            : $BUILDDIR"
echo "Warmup sleep               : $SLEEP"
echo "Tmp dir                    : $TMP"
echo "tbse cmd                   : $TBSE"
echo "warmup secs                : $WARMUP_SECS"
echo "status sleep               : $STATUS_SLEEP"
echo "dummy run?                 : $DUMMY"
echo "have /proc/cpuinfo?        : $HAVE_CPUINFO"
echo "No turbo in                : $NO_TURBO_IN"
echo

# exit on error
set -e

# kill children on exit
trap '[ -n "$(jobs -pr)" ] && KillChildren $$' INT QUIT TERM EXIT

# might need many open files for PGO
ulimit -n 2048

############################################################
# first build: use builddir if already avaialble
BUILD=1
echo "Configure meson for PGO build $BUILD"
MESON_COMMAND="setup $BUILDDIR -Db_pgo=generate $MESON_FLAGS $MESON_FLAGS1"
echo $MESON_COMMAND
if ! [ $DUMMY -eq 1 ]; then
    if [ -d $BUILDDIR ] ; then
        $MESON --wipe $MESON_COMMAND \
               > $MESON_STDOUT 2> $MESON_STDERR
    else
        $MESON $MESON_COMMAND \
             > $MESON_STDOUT 2> $MESON_STDERR
    fi
fi

echo "Build ($BUILD) with ninja"
if ! [ $DUMMY -eq 1 ]; then
    $NINJA -C $BUILDDIR binary_c_install \
           > $NINJA_STDOUT 2> $NINJA_STDERR
fi

# show flags
./binary_c version | grep ^CC
./binary_c version | grep ^CFLAGS

############################################################
############################################################
# make lists of systems or use previously-generated lists
if [ $USE_EXISTING_SYSTEM_LISTS -eq 1 ]; then
    echo "Using existing system lists"
else
    echo "Make system lists"
    for i in $(seq 0 $NCORES1); do
        if ! [ $DUMMY -eq 1 ]; then
            #echo "taskset $i"
            taskset -c "$i" "$TBSE" "random_system_list" "True" "repeat" "$REPEAT_PER_CORE" > "$TMP/pgo_list_$i" &
            #echo "done taskset $i"
        fi
        sleep $SLEEP
    done
    wait
    echo "Done system list make"
fi

#echo "Finished loop"

############################################################
############################################################
# run and filter to files
disable_turbo
START=$(date +%s%N)

echo "Running binary_c (build 1)"
if ! [ $DUMMY -eq 1 ]; then
    echo
    run_stellar_list
fi
enable_turbo

END=$(date +%s%N)
PGO1_DURATION=$(echo "(0.000000001*($END - $START))"|bc)

# If using clang, you need to run something like
# llvm-profdata merge -output=default.profdata default_3597620414865729667_0.profraw
# from the builddir prior to setting up again
if [[ $("$CC" --version 2>/dev/null) == *"clang"* ]]; then
    echo "CC is clang, merging profdata."
    cd $BUILDDIR && llvm-profdata merge -output=default.profdata default*.profraw && cd ..
fi

############################################################
# rebuild
BUILD=2
if ! [ $DUMMY -eq 1 ]; then
    echo "Configure meson for PGO build $BUILD"
    MESON_COMMAND="setup $BUILDDIR --reconfigure -Db_pgo=use $MESON_FLAGS $MESON_FLAGS2"
    echo $MESON_COMMAND
    eval "$MESON $MESON_COMMAND > $MESON_STDOUT 2> $MESON_STDERR"
fi

if ! [ $DUMMY -eq 1 ]; then
    echo "Build ($BUILD) with ninja"
    $NINJA -C $BUILDDIR binary_c_install \
           > $NINJA_STDOUT 2> $NINJA_STDERR
fi

# show flags
./binary_c version |grep ^CC
./binary_c version |grep ^CFLAGS

# remove trap
trap - INT QUIT TERM EXIT

############################################################
# rerun

# kill children on exit
trap '[ -n "$(jobs -pr)" ] && KillChildren $$' INT QUIT TERM EXIT

disable_turbo
START=$(date +%s%N)
echo "Running binary_c (build 2)"
if ! [ $DUMMY -eq 1 ]; then
    run_stellar_list
fi

END=$(date +%s%N)
PGO2_DURATION=$(echo "(0.000000001*($END - $START))"|bc)
enable_turbo

# remove trap
trap - INT QUIT TERM EXIT
echo "Run 1 (without PGO) took : $PGO1_DURATION s"
echo "Run 2 (   with PGO) took : $PGO2_DURATION s"

# final speedup %
SPEEDUP=$(echo "(100.0*($PGO1_DURATION-$PGO2_DURATION)/$PGO1_DURATION)"|bc)
echo -n "Profile guided optimization speedup is "
echo -n "$SPEEDUP"
echo "%"
# reset CPU state
if [ -f /sys/devices/system/cpu/intel_pstate/no_turbo ]; then
    if [ $NO_TURBO_IN -eq 1 ]; then
        disable_turbo
    else
        enable_turbo
    fi
fi

echo "Finished meson/pgo.sh at "`date`
