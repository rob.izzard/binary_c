#!/bin/bash
#
# The binary_c stellar population nucleosynthesis framework.
#
# Copyright Robert Izzard 2000-2021
#
# Contact rob.izzard@gmail.com
#
# Code repository:
# https://gitlab.com/binary_c
#
# Documentation is in doc/
# Code is in src/
#
# To build, run
#
# ---
#
# meson builddir
# cd builddir
# ninja binary_c_install
#
# ---
#
# Please see the files README, LICENCE and CHANGES,
# and the doc/ directory for documentation which is mirrored
# at https://binary_c.gitlab.io
#
######################
#
# Purpose:
#
# return a string that determines if we're on a virtual machine
#
# This is ONLY for Linux
#
# echo "none" if not a VM, otherwise a string that
# describes the VM (see "systemd-detect-virt --list"
# for a list)
#
######################


VIRT=`systemd-detect-virt 2>/dev/null`
if [ -z $VIRT ]; then
    echo "none"
else
    echo $VIRT
fi
