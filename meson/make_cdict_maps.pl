#!/usr/bin/env perl
use strict;
use warnings;
use Data::Dumper;

#
# The binary_c stellar population nucleosynthesis framework.
#
# Copyright Robert Izzard 2000-2021
#
# Contact rob.izzard@gmail.com
#
# Code repository:
# https://gitlab.com/binary_c
#
# Documentation is in doc/
# Code is in src/
#
# To build, run
#
# ---
#
# meson builddir
# cd builddir
# ninja binary_c_install
#
# ---
#
# Please see the files README, LICENCE and CHANGES,
# and the doc/ directory for documentation which is mirrored
# at https://binary_c.gitlab.io/binary_c.html
#
######################
#
# Purpose:
#
# script to map binary_c variables to and from an cdict
# which can then be exported and imported as JSON
#
# Note:
# This is very experimental and likely to be buggy.
#
######################


my $vb = 0;
my $regex;
parse_args();
my $cc = $ENV{CC} // 'gcc';
my $headerfile = $ENV{BINARY_C_STRUCTURES_H} //
    'binary_c_structures.h';
my $cmd = "cd src; \"$cc\" -I. -D__CDICT_MAP__ -E \"$headerfile\" -o - 2>&1";
my $data = `$cmd`;
#print $data;exit;
$data=~s/libbinary_c_//g;
my $enums = {};
my @binary_c_map;

my %mapped_structs = (
    'stardata_t' => 'stardata->',
    'mint_t' => 'mint->',
    'star_t' => 'star->'
    );

get_binary_c_enums();
load_binary_c_structures();
make_map_cdict_to_binary_c();
make_map_binary_c_to_cdict();
exit;

sub make_map_binary_c_to_cdict
{
    my $code;
    foreach my $m (@binary_c_map)
    {
        next if(defined $regex &&
                $m->{structure} !~/$regex/);

        my $location =
            $mapped_structs{$m->{structure}} . $m->{varname};

        # build code
        $code .=
'
    {
';
        if(defined $m->{arraysize})
        {
            # array data
            $code .=
'
        '.$m->{type}.' data['.$m->{arraysize}.'];
        for(int i=0; i<'.$m->{arraysize}.'; i++)
        {
            data[i] = '.$location.'[i];
        }
';
        }
        else
        {
            # scalar data
            $code .=
'
        '.$m->{type}.' data = '.$location.';
';
        }

        $code .=
'
        Cdict_nest(hash,
                    "'.$m->{varname}.'",('.$m->{type}.(defined $m->{arraysize}?'[]':'').') data,
                    "'.$m->{structure}.'");
    }
';
    }
    safedumpfile('src/data/binary_c_to_cdict.h',
                 $code);
}

sub make_map_cdict_to_binary_c
{
    # make the function that maps from
    # cdict to binary_c
    my $code;

    foreach my $m (@binary_c_map)
    {
       next if(defined $regex &&
                $m->{structure} !~/$regex/);



       my $stripped_name = $m->{structure};
       $stripped_name=~s/_t$//;

        my $location =
            (
             $m->{structure} eq 'stardata_t'
             ? 'stardata'
             : $stripped_name
            )
            . '->' . $m->{varname};

        if(defined $m->{arraysize})
        {
            # array var
            $code .=
'
    {
        struct cdict_entry_t * entry = Cdict_nest_get_entry(hash,"'.$m->{structure}.'","'.$m->{varname}.'");
        if(entry != NULL)
        {
            '.$m->{type}.' * data = entry->value.value.'.$m->{type}.'_array_data;
            for(int i=0; i<'.$m->{arraysize}.'; i++)
            {
                '.$location.'[i] = data[i];
            }
        }
';
        }
        else
        {
            # scalar var
            $code .=
'
    {
        struct cdict_entry_t * entry = Cdict_nest_get_entry(hash,"'.$m->{structure}.'","'.$m->{varname}.'");
        if(entry != NULL)
        {
            '.$m->{type}.' data = entry->value.value.'.$m->{type}.'_data;
            '.$location.' = data;
        }
    }
';
        }
    }
    safedumpfile('src/data/cdict_to_binary_c.h',
                 $code);
}


sub get_binary_c_enums
{
    # get a hash of binary_c's enums
    while($data =~ /enum\s+(\{[^\}]+)/g)
    {
        my $chunk = $1;
        my $n = 0;
        foreach my $bit (split(/,/,$chunk))
        {
            $bit=~s/^\s+//;
            $bit=~s/\s+$//;
            if(defined $bit && $bit ne '')
            {
                if($bit =~/(\S+)\s*=\s*(\d+)/)
                {
                    $enums->{$1} = $2;
                }
                else
                {
                    $enums->{$bit} = $n++;
                }
            }
        }
    }
}

sub map_enums
{
    # turn enums to numbers
    foreach my $enum (keys %$enums)
    {
        $data =~ s/$enum\b/$enums->{$enum}/g;
    }
}

sub load_binary_c_structures
{
    get_binary_c_enums();
    map_enums();

    my $structure_name;
    while($data =~ /struct\s+(\S+)\s*\{([^\}]+)\}/g)
    {
        $structure_name = $1;
        if($mapped_structs{$structure_name})
        {
            my $contents = $2;
            print "Found struct $structure_name : ",$mapped_structs{$structure_name}//0,"\n"if($vb);

            while($contents=~/(double|int|Boolean)\s*([^;]+) __is_Cdict_var__;/g)
            {
                my $type = $1;
                my $name = $2;
                my $array = $name=~s/\[([^\]]+)\]// ? clear_var($1) : undef;

                no warnings;
                print "  map: type $type, name $name, array $array\n"if($vb);
                use warnings;

                push(@binary_c_map,
                     {
                         structure => $structure_name,
                         type => $type,
                         varname => $name,
                         arraysize => $array
                     });
            }
        }
    }
}

sub clear_var
{
    # remove variable type information
    my ($def) = @_;
    my $was = $def.' ';
    while($was ne $def)
    {
        $was = $def;
        while($def=~s/(unsigned|double|int|long|Boolean|short)//g){}
        $def=~s/\s+//;
        $def=~s/\(\)//g;
        if($def=~/^\(/ && $def=~/\)$/)
        {
            $def =~ s/^\(//;
            $def =~ s/\)$//;
        }
    }
    return $def;
}

sub safeslurp
{
    # Usage: safeslurp(<filename>)
    # Returns the contents of file given by <filename> and on failure returns undef

    open(my $fh,'<'.$_[0])||return undef;
    return (do { local( $/ ) ; <$fh> } );
}

sub safedumpfile
{
    # Usage: dumpfile($filename,$data)
    # Puts the string $data into the file given by $filename
    open(my $fp,'>'.$_[0])||return undef;
    binmode $fp;
    print {$fp} $_[1];
    close $fp;
}

sub parse_args
{
    foreach my $arg (@ARGV)
    {
        if($arg=~/-v/)
        {
            $vb = 1;
        }
        elsif($arg=~/--regex=(\S+)/)
        {
            $regex = $1;
            print STDERR "REGEX $1\n";
        }
    }
}
