#!/bin/bash

#
# The binary_c stellar population nucleosynthesis framework.
#
# Copyright Robert Izzard 2000-2021
#
# Contact rob.izzard@gmail.com
#
# Code repository:
# https://gitlab.com/binary_c
#
# Documentation is in doc/
# Code is in src/
#
# To build, run
#
# ---
#
# meson builddir
# cd builddir
# ninja binary_c_install
#
# ---
#
# Please see the files README, LICENCE and CHANGES,
# and the doc/ directory for documentation which is mirrored
# at https://binary_c.gitlab.io
#
######################
#
# Purpose:
#
# wrapper script in the meson scripts directory for binary_c's
# data_object_list.sh
#
######################
#

cd ${MESON_SOURCE_ROOT}



# list them
cd src || exit
GSL_INCLUDE=`gsl-config --cflags`
env INCDIRS="-I. -I/usr/include $GSL_INCLUDE -I/usr/include/libiberty " ../data_object_list.sh | sed -e s/^/src\\// |sed -e 's/[[:space:]]*$//' | sed s/\ /\ src\\//g
