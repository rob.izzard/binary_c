#!/bin/bash
cd ${MESON_SOURCE_ROOT}

#
# The binary_c stellar population nucleosynthesis framework.
#
# Copyright Robert Izzard 2000-2021
#
# Contact rob.izzard@gmail.com
#
# Code repository:
# https://gitlab.com/binary_c
#
# Documentation is in doc/
# Code is in src/
#
# To build, run
#
# ---
#
# meson builddir
# cd builddir
# ninja binary_c_install
#
# ---
#
# Please see the files README, LICENCE and CHANGES,
# and the doc/ directory for documentation which is mirrored
# at https://binary_c.gitlab.io
#
######################
#
# Purpose:
#
# wrapper for data_object_list.sh
#
######################



############################################################
# build them if they don't exist
/usr/bin/env perl ./meson/make_dataobjects.pl >./data_object_build.log || exit 1

############################################################
# change to src or exit
cd src || exit 1

############################################################
# list them
#../meson/data_object_list.sh | sed -e s/^/src\\// |sed -e 's/[[:space:]]*$//'| sed s/\ /\ src\\//g
../meson/data_object_list.sh
