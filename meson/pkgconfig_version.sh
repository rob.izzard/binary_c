#!/bin/bash

# use pkg-config to find version of something

pkg-config --debug "$1" 2>&1 |grep Version |gawk "{print \$2}"
