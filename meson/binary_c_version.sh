#!/bin/bash

#
# The binary_c stellar population nucleosynthesis framework.
#
# Copyright Robert Izzard 2000-2021
#
# Contact rob.izzard@gmail.com
#
# Code repository:
# https://gitlab.com/binary_c
#
# Documentation is in doc/
# Code is in src/
#
# To build, run
#
# ---
#
# meson builddir
# cd builddir
# ninja binary_c_install
#
# ---
#
# Please see the files README, LICENCE and CHANGES,
# and the doc/ directory for documentation which is mirrored
# at https://binary_c.gitlab.io
#
######################
#
# Purpose:
#
# script to return the binary_c version number
# based on which is in binary_c_version.h
# (i.e. does not require binary_c to be built)
#
######################
#


cd ${MESON_SOURCE_ROOT}
grep define\ BINARY_C_VERSION\ \" src/binary_c_version.h | awk "{print \$3}"|tr -d '"'
