#!/bin/bash
cd ${MESON_SOURCE_ROOT}
#
# The binary_c stellar population nucleosynthesis framework.
#
# Copyright Robert Izzard 2000-2021
#
# Contact rob.izzard@gmail.com
#
# Code repository:
# https://gitlab.com/binary_c
#
# Documentation is in doc/
# Code is in src/
#
# To build, run
#
# ---
#
# meson builddir
# cd builddir
# ninja binary_c_install
#
# ---
#
# Please see the files README, LICENCE and CHANGES,
# and the doc/ directory for documentation which is mirrored
# at https://binary_c.gitlab.io
#
######################
#
# Purpose:
#
# make generic data object(s) for binary_c
# http://gareus.org/wiki/embedding_resources_in_executables
#
######################


: "${CC:="gcc"}"
: "${OBJCOPY_ARCH:=$(objdump -f /bin/bash |grep architecture | gawk "{print \$2}" | sed s/,//)}"
: "${OBJCOPY_TARGET:=$(objdump -f /bin/bash  | grep format | gawk "{print \$4}")}"
: "${OBJCOPY_OPTS:="-I binary -B $OBJCOPY_ARCH -O $OBJCOPY_TARGET"}"
#: "${OBJCOPY_SUBOPTS:="--rename-section .data=.rodata,alloc,load,readonly,data,contents "}"

OBJCOPY_SUBOPTS=""
DBUILT="false"

COMMAND=$1
shift

SRCPREFIX=".."

for HFILE in "$@"; do

    TMPFILE=$(printf "%s" "$HFILE" | sed s/\.h$/.tmp/ | sed s/\\//_/g)
    OBJFILE=$(printf "%s/%s" "${MESON_BUILD_ROOT}" "$HFILE" | sed s/\.h$/.o/ | sed s/\\//_/g)

    echo "H $HFILE"
    ls -l $SRCPREFIX/$HFILE
    echo "T $TMPFILE"
    echo "O $OBJFILE"

    if [ "$COMMAND" = "build" ] ; then
        echo "Do build"
        if [ "$SRCPREFIX/$HFILE" -nt "$OBJFILE" ] ; then
            echo "Require objfile ($DBUILT)"
            if [ "$DBUILT" = true ]; then
                echo "Build double2bin"
                # first time, build double2bin
                $CC "$SRCPREFIX/double2bin.c" -o ./double2bin
            fi
            tr , ' ' < "$SRCPREFIX/$HFILE" | sed s/\\\\// | grep -v define |grep -v "\\*" | ./double2bin > "$TMPFILE"
            objcopy $OBJCOPY_OPTS $OBJCOPY_SUBOPTS "$TMPFILE" "$OBJFILE"
            rm "$TMPFILE"
            DBUILT=true
        fi
    fi

    echo -n "$OBJFILE "

done

echo
