#!/usr/bin/env perl
use strict;

#
# Script to extract and write tables for binary_c to interpolate
# based on BSE fitting formulae.
#
my $binary_c = './binary_c';
my $tabledir = './src/tables';
my $prefix = 'BSE_';
my $postfix = '.h';
my $test = 1;

check_binary_c();

make_table('TABLE_MS_LR',
           'MS_LR',
           3);

make_table('TABLE_TIMESCALES_H',
           'TIMESCALES_H',
           2);

exit;

############################################################

sub make_table
{
    my ($filter, $file, $nparams) = @_;

    # table macro
    my $macro = $prefix.$file;

    # construct filename
    $file = $tabledir.'/'.$prefix.$file.$postfix;
    $file=~s!/+!/!g;

    print `$binary_c | filter $filter | env CIFY=$macro cify.pl > $file`;
    print "Made $file\n";

    if($test)
    {
        print "Testing $file : ";
        my $test_result = `test_for_rectangular_table.pl $file $nparams`; 
    
        if($test_result=~/grid seems fine/)
        {
            print "it is fine\n";
        }
        else
        {
            print "error detected!\n$test_result";
            exit;
        }
    }

}


sub check_binary_c
{
    if(`$binary_c --version`=~/MAKE_BSE_TABLES is on/)
    {
        return;
    }
    else
    {
        die("You must build binary_c with MAKE_BSE_TABLES defined in code_options.h");
    }
}
