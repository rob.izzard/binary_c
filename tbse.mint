#!/bin/bash

#################################
# wrapper for tbse to test MINT #
#################################

# shared options
METALLICITY=0.02 # 1e-4 <= METALLICITY <= 0.03 
E2_PRESCRIPTION=E2_MINT
TIDES_CONVECTIVE_DAMPING=TIDES_CONVECTIVE_DAMPING_ZAHN1989
MAGNETIC_BRAKING_FACTOR=1.0
MINIMUM_TIMESTEP=1e-8
MAXIMUM_TIMESTEP=1e3
MAGNETIC_BRAKING_ALGORITHM=MAGNETIC_BRAKING_ALGORITHM_ANDRONOV_2003
MAGNETIC_BRAKING_FACTOR=1.0
MAGNETIC_BRAKING_GAMMA=3.0
COMENV_PRESCRIPTION=COMENV_BSE
STELLAR_TYPE1=MS
STELLAR_TYPE2=MS

# MINT options
MINT_METALLICITY=0.02 # set to -1 to ignore
MINT_KIPPENHAHN=0 # 0=off, 1=star1, 2=star2
MINT_KIPPENHAHN_STELLAR_TYPE=-2 # set stellar type for output, -1=always,-2=disable
MINT_KIPPENHAHN_COMPANION_STELLAR_TYPE=-2 # see previous
MINT_NSHELLS=0 # initial number of shells (can be 0)
MINT_MINIMUM_NSHELLS=0 # minimum number of shells
MINT_MAXIMUM_NSHELLS=1000 # maxmimum number of shells
MINT_MINIMUM_SHELL_MASS=1e-4
MINT_MAXIMUM_SHELL_MASS=1e-3
MINT_NUCLEAR_BURNING=0
MINT_REMESH=False
MINT_FILENAME_VB=False
MINT_MS_REJUVENATION=False
MINT_DISABLE_WARNINGS=False
MINT_DISABLE_GRID_LOAD_WARNINGS=False
MINT_DATA_CLEANUP=True
MINT_USE_ZAMS_PROFILES=False
MINT_FALLBACK_TO_TEST_DATA=True
MINT_USE_FALLBACK_COMENV=True # use BSE common envelope?

# colon-separated list of MINT data directories which
# are searched in order
MINT_DIR=$HOME/data/MINT/test_data:$HOME/data/MINT/data

# maximum stellar type allowed : 16 means run them all
# a negative number means this is ignored if MASSLESS_REMNANT (15)
MAX_STELLAR_TYPE1=3 # 3
MAX_STELLAR_TYPE2=3 #-3


# RUN tbse
tbse $TBSEARGS \
     multiplicity 1 \
     resolve_stellar_type_changes True \
    --max_evolution_time 15000 \
    --minimum_timestep $MINIMUM_TIMESTEP \
    --maximum_timestep $MAXIMUM_TIMESTEP \
    --stellar_structure_algorithm STELLAR_STRUCTURE_ALGORITHM_MINT \
    --metallicity $METALLICITY \
    --MINT_dir $MINT_DIR/ \
    --MINT_metallicity $MINT_METALLICITY \
    --MINT_Kippenhahn $MINT_KIPPENHAHN \
    --MINT_Kippenhahn_stellar_type $MINT_KIPPENHAHN_STELLAR_TYPE \
    --MINT_Kippenhahn_companion_stellar_type $MINT_KIPPENHAHN_COMPANION_STELLAR_TYPE \
    --MINT_minimum_shell_mass $MINT_MINIMUM_SHELL_MASS \
    --MINT_maximum_shell_mass $MINT_MAXIMUM_SHELL_MASS \
    --MINT_nuclear_burning $MINT_NUCLEAR_BURNING \
    --MINT_remesh $MINT_REMESH \
    --MINT_filename_vb $MINT_FILENAME_VB \
    --MINT_nshells $MINT_NSHELLS \
    --MINT_maximum_nshells $MINT_MAXIMUM_NSHELLS \
    --MINT_minimum_nshells $MINT_MINIMUM_NSHELLS \
    --MINT_MS_rejuvenation $MINT_MS_REJUVENATION \
    --MINT_disable_warnings $MINT_DISABLE_WARNINGS \
    --MINT_disable_grid_load_warnings $MINT_DISABLE_GRID_LOAD_WARNINGS \
    --MINT_data_cleanup $MINT_DATA_CLEANUP \
    --MINT_use_ZAMS_profiles $MINT_USE_ZAMS_PROFILES \
    --MINT_fallback_to_test_data $MINT_FALLBACK_TO_TEST_DATA \
    --MINT_use_fallback_comenv $MINT_USE_FALLBACK_COMENV \
    --magnetic_braking_algorithm $MAGNETIC_BRAKING_ALGORITHM \
    --magnetic_braking_factor $MAGNETIC_BRAKING_FACTOR \
    --magnetic_braking_gamma $MAGNETIC_BRAKING_GAMMA \
    --tides_convective_damping  $TIDES_CONVECTIVE_DAMPING \
    --E2_prescription $E2_PRESCRIPTION \
    --stellar_type_1 $STELLAR_TYPE1 \
    --stellar_type_2 $STELLAR_TYPE2 \
    --max_stellar_type_1 $MAX_STELLAR_TYPE1 \
    --max_stellar_type_2 $MAX_STELLAR_TYPE2 \
    --comenv_prescription $COMENV_PRESCRIPTION \
    --wind_mass_loss FALSE \
     \
    $@
