#include <stdio.h>
/*
 * Convert a file of floating point numbers (from stdin) into
 * binary form. Please separate the numbers with whitespace.
 */

/*
 * The binary_c stellar population nucleosynthesis framework.
 *
 * Copyright Robert Izzard 2000-2023
 *
 * Contact rob.izzard@gmail.com
 *
 * Code repository:
 * https://gitlab.com/binary_c
 *
 * Documentation is in doc/
 * Code is in src/
 *
 * To build, run
 *
 * ---
 *
 * meson builddir
 * cd builddir
 * ninja binary_c_install
 *
 * ---
 *
 * Please see the files README, LICENCE and CHANGES,
 * and the doc/ directory for documentation which is mirrored
 * at https://binary_c.gitlab.io/binary_c.html
 *
 **********************
 *
 * Subroutine: main
 *
 * Purpose: to convert an ASCII file of (double-precision) floating point
 *          numbers on stdin to binary format.
 *
 * Arguments: none.
 *
 * Returns: outputs the binary numbers to stdout, then returns 0.
 *
 **********************
 */

int main()
{
    double p;
    while(fscanf(stdin,"%lf",&p)!=EOF)
    {
        if(!fwrite(&p,sizeof(double),1,stdout)) return 1;
    }
    fflush(stdout);
    return 0;
}
