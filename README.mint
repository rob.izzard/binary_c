[01/04/2021 MINT rough instructions]


1) In binary_c_parameters.h uncomment

#define MINT

2) rebuild as usual

meson builddir
cd builddir
ninja binary_c_install_legacy
cd ..

3) git clone git@gitlab.surrey.ac.uk:gm0027/mesa-grids-for-binary_c.git

let's say that's in ~/git/mesa-grids-for-binary_c

4) edit tbse.mint

set MINT_DIR in there to wherever you put mesa-grids-for-binary_c

there's a MINT section - change the values as required
(beware: not all work 100% at present!)

e.g. if you want faster evolution, turn off the nuclear burning

5) run tbse.mint

This is set up with a test M1=1.25 + M2=1.0 that will have some RLOF then merge.
Note that this is slow at the beginning because of magnetic braking and tides:
these are quite stiff equations that require short timesteps.
