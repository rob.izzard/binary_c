#!/usr/bin/env perl
use strict;
use rob_misc;
use warnings;
use File::Copy;
use File::Path;

# make binary_c's HTML manual
my $tmpdir = '/tmp/binary_c_lyx';
my $args = "@ARGV";
my $lyxfile = "binary_c2.lyx";
my $lyxtmp = "$tmpdir/binary_c2.lyx";
my $htmlfile = "binary_c.html";
my $lyx = "lyx";
my $cmd = "cd $tmpdir ; $lyx -E xhtml $htmlfile $lyxtmp ";
my $watch = ($args =~ /watch/) ? 1 : 0;
my $install = ($args =~ /install/) ? 1 : 0;
my $svn_wwwdir = $ENV{SVN}.'/tex/www/doc/binary_c';
my $static_wwwdir = $ENV{BINARY_C}.'/public/doc/';
my $imagedir = 'images';
my $cssdir = 'css';
my $html;

while(1)
{
    ############################################################
    # changes to the lyx file
    ############################################################

    # clean tmpdir by removing and replacing it
    rmtree $tmpdir;
    maketmp();

    # get content for the parent lyx file and its children
    my $lyxcontent = {};

    sub parse_child
    {
        my $parent = shift;

        # get parent document
        $lyxcontent->{$parent} = slurp($parent.'.lyx');

        # get child documents
        my @lyxchildren = ($lyxcontent->{$parent} =~ m!filename \"([^\"]+)\.lyx\"!g);

        # parse child document too
        foreach my $child (@lyxchildren)
        {
            if(!defined $lyxcontent->{$child})
            {
                print "Get child content $child.lyx\n";
                parse_child($child);
            }
        }
    }
    parse_child('binary_c2');

    print "LYX CHILDREN ",join('  ',keys %$lyxcontent),"\n";

    # parse lyx children
    my $nimage = 1;
    foreach my $lyxchild (keys %$lyxcontent)
    {
        # point to tmp file
        $lyxcontent->{$lyxchild} =~s !filename \"\K!$tmpdir/!g;

        # make listings inline
        $lyxcontent->{$lyxchild} =~s !\\begin_inset listings\s+\K!inline true\n!gs;
        $lyxcontent->{$lyxchild} =~s !(inline true\n(?:lstparams.*\n)?)inline false!$1!g;

        my @images = ($lyxcontent->{$lyxchild}=~ /Graphics\s+filename (\S+)/sg);
        foreach my $image (@images)
        {
            my $suffix = ($image=~/(\.(?:pdf|ps|eps|png|gif|jpeg|jpg|tiff|pnm))$/)[0];

            if(!defined $suffix)
            {
                print "SUFFIX UNKNOWN from $image\n";
                exit;
            }
            my $newimage = "$tmpdir/images/$nimage$suffix\n";
            print "COPY $image to $newimage\n";
            `cp $image $newimage`;

            # rename in lyx
            $lyxcontent->{$lyxchild} =~ s!Graphics\s+filename \K$image!$newimage!;

            $nimage++;
        }

        # save to tmpdir
        print "SAVE $lyxchild.lyx\n";
        dumpfile($tmpdir.'/'.$lyxchild.'.lyx',
                 $lyxcontent->{$lyxchild});
    }

    ############################################################
    # make (X)HTML
    ############################################################

    # remove html file first
    unlink $htmlfile;

    # convert to xhtml
    print "$cmd\n";
    runcmd($cmd,'screen');

    # move here
    `cp $tmpdir/$htmlfile ./$htmlfile`;

    # check for success
    if(! -f $htmlfile)
    {
        print "error? $htmlfile not found\n";
    }

    # slurp in and edit
    $html = slurp($htmlfile);

    ############################################################
    # change HTML to suit
    ############################################################


    # remove layout-provided styles
    #$html =~s !\/\* LyX Provided Styles.*(<\/style>)!$1!s;

    my $variant = 'sans-serif-italic';

    no warnings;
    # replace math-generated binary_c macro with a normal word
    $html =~s !<math xmlns=\"http\:\/\/www.w3.org\/1998\/Math\/MathML\">\s*<mrow>\s*(?:<mstyle mathvariant=\'normal\'>)?\s*<mstyle mathvariant=\'italic\'>\s*<mrow><mi>([Bb])<\/mi><mi>i<\/mi><mi>n<\/mi><mi>a<\/mi><mi>r<\/mi><mi>y<\/mi>_<mi>c<\/mi>\s*<\/mrow>\s*(?:<\/mstyle>\s*)?<\/mstyle>\s*<\/mrow><\/math>!$1inary_c!gs;

    # same for libbinary_c
    $html =~s !<math xmlns=\"http\:\/\/www.w3.org\/1998\/Math\/MathML\">\s*<mrow>\s*(?:<mstyle mathvariant=\'normal\'>)?\s*<mstyle mathvariant=\'italic\'>\s*<mrow><mi>l<\/mi><mi>i<\/mi><mi>b<\/mi><mi>([Bb])<\/mi><mi>i<\/mi><mi>n<\/mi><mi>a<\/mi><mi>r<\/mi><mi>y<\/mi>_<mi>c<\/mi>\s*<\/mrow>\s*(?:<\/mstyle>\s*)?<\/mstyle>\s*<\/mrow><\/math>!lib$1inary_c!gs;
    use warnings;

    # and binary_grid
    $html =~s !<math xmlns=\"http\:\/\/www.w3.org\/1998\/Math\/MathML\">\s*<mrow>\s*<mstyle mathvariant='normal'>\s*<mstyle mathvariant='italic'>\s*<mrow><mi>b<\/mi><mi>i<\/mi><mi>n<\/mi><mi>a<\/mi><mi>r<\/mi><mi>y<\/mi>_<mi>g<\/mi><mi>r<\/mi><mi>i<\/mi><mi>d<\/mi>\s*<\/mrow>\s*<\/mstyle>\s*<\/mstyle>\s*<\/mrow><\/math>!binary_grid!gs;

    # fix title box : the ovalbox isn't centred
    $html =~s !(class=\'ovalbox\')!$1 style=\'text-align\: center\;\'!;

    # apply RTD sphinx themes (https://github.com/readthedocs/sphinx_rtd_theme)
    my @css = (
        'alabaster_binary_c.css',
        'pygments.css',
        'theme.css'
        );
    foreach my $cssfile (@css)
    {
        $html =~s !<\/head>!<link rel=\"stylesheet\" href="css/$cssfile\" type=\"text\/css\" \/>\n\n<\/head>!;
    }

    # convert TOC (table of contents) into side bar :
    # this converts div class 'toc' to 'sphinxsidebar', adds extra div of class 'sphinxsidebarwrapper' ad id 'toc'
    $html =~s !<div class=\'toc\'>!<div class=\"sphinxsidebar\" role=\"navigation\" aria-label=\"main navigation\">\n<div class=\"sphinxsidebarwrapper\"><div id=\'toc\'>!;
    $html =~s !(.*class=\'tocentry\'>.*)!$1<\/div><\/div>!;

    # fix up body
    $html =~s !(<body dir=\"auto\">)!$1\n<div class=\"document\">\n<div class=\"documentwrapper\">\n<div class=\"bodywrapper\">\n<div class=\"body\" role=\"main\">!;
    $html =~s !(<\/body>)!<\/div><\/div><\/div><\/div>$1!;

    # fix <a id...> targets
    $html =~s !<a\s+id=\"([^\"]+)\"\s+\/>!<div id=\"$1\"><\/div>!g;

    # fix empty target hyperlinks
    $html =~s !<[Aa] [Hh][Rr][Ee][Ff]=\"\">([^<]+)<\/[Aa]>!<a href=\"$1\">$1<\/a>!g;

    # shrink monospace to match main font size
    $html =~s !<span style=\'font-family\:monospace\;\'>!<span style=\'font-family\:monospace\;font-size\: 18px\;\'>!g;

    # convert MathML to sans serif, and scale down a little to
    # match the main font size
    no warnings;
    $html =~s !<math(\s+display=\"block\")?(\s+xmlns=\"http\:\/\/www\.w3\.org\/1998\/Math\/MathML\")>!<math$1$2><mstyle mathvariant=\'$variant\' mathsize=\'85\%\'>!g;
    $html =~s !<\/math>!<\/mstyle><\/math>!g;
    use warnings;

    # shift subscripts down a little
    $html =~s !<msub>!<msub subscriptshift=\"0.2ex\">!gs;
    $html =~s !<\/msub>!<\/msub>!sg;

    # convert URLs to hyperlinks
    $html =~s!<span class="flex_url">([^<]+)<\/span>!<a href=\"$1\">$1</a>!gs;

    #\binaryc \binarygrid \binarycpython \libbinaryc are dealt with badly in child documents
    $html =~s !binaryc!binary_c!g;
    $html =~s !libbinaryc!libbinary_c!g;
    $html =~s !binaryc_grid!binary_grid!g;


    # make binary_c etc. italic, except when preceded by /
    # which means it's likely inside a URL
    $html =~s !\/binary_c!___slashbinary_c!g;
    $html =~s !\bbinary_c\b!<i>binary_c</i>!g;
    $html =~s !___slashbinary_c!\/binary_c!g;
    $html =~s !<title>.*<\/title>!<title>binary_c documentation<\/title>!;


    no warnings;
    $html =~s !\blibbinary_c(.so)?\b!<i>libbinary_c$1</i>!g;
    use warnings;
    $html =~s ~\bbinary_grid(2?)(?![\:\;])\b~<i>binary_grid$1</i>~gx;

    # make Perl, C, GNU, FORTRAN italic
    $html =~s !\s\KFORTRAN\b!<i>FORTRAN</i>!gi;
    $html =~s !\s\KC\b!<i>C</i>!g;
    $html =~s !\s\KGNU\b!<i>GNU</i>!gi;
    #$html =~s !\sPerl\b!<i><a href=\"https://www.perl.com/\">Perl</a></i>!g;
    $html =~s !\s\KPython\b!<i><a href=\"https://www.python.org/\">Python</a></i>!g;

    # bash listings
    while($html =~s !<pre class ='listings bash'>(.*?)</pre>!__MATCH__!s)
    {
        my $match = $1;
        $match = '$ '.$match;
        $match =~s/\n/\n\$ /g;
        $match = "<pre class\\s*='listings xxxbashxxx'>" .$match. '</pre>';
        $html =~s/__MATCH__/$match/;
    }
    $html =~s !xxxbashxxx!bash!g;

    # remove labeling-item indent
    $html =~s !li\.labeling_item \{\s*text-indent\: \K-5em;!0em!g;

    # Appendix : HR and title
    $html =~s !(.*A(?:<\/span>) Grid options)!\n<HR><H1>Appendix<\/H1>\n$1!g;

    # make divs based on flex inserts be inline spans
    $html =~s !\\\s*<i>binary_c!binary_c!sg;
    $html =~s !\\\s*<i>libbinary_c!libbinary_c!sg;
    $html =~s !\\\s*<i>binary_grid!binary_grid!sg;

    # \b\\binary_c should be considered a mistake
    $html =~s !\\([Bb])inaryc!<i>${1}inary_c</i>!g;
    $html =~s !\\binarygrid!<I>binary_grid</I>!g;

    # convert to </span>\n, to </span>, to avoid spurious spaces
    $html =~s !<\/span>\n,!<\/span>,!gs;

    # clean up references
    if(1)
    {
        my ($references) = ($html =~m !<h2 class='bibtex'>References(.*)!gs);
        $html =~s/(<h2 class='bibtex'>References).*/$1/s;
        $references =~ s/<i>([A-Z])<\/i>/$1/g; # remove italics
        $references =~ s/\~([A-Z]\.)/ $1/g; # fix ~
        $references =~ s/--/-/g; # fix LaTeXx hyphens

        # make bibtex labels bold
        $html =~ s/(span.bibtexlabel\:before{ content: \")\[(\"; \})/$1$2/g;
        $html =~ s/(span.bibtexlabel\:after{ content: \")\] (\"; \})/$1 $2/g;
        $references =~ s/(<span class='bibtexlabel'>)(.*?)(<\/span>)/$1<b>$2<\/b>$3/g;
        $html .= $references;
    }

    # get images
    mkdir $imagedir;

    $nimage = 0;
    my @images;
    while ($html=~s/(src=[\'\"])([^\'\"]+?)\.(pdf|png|jpg|jpeg|gif)([\'\"])/__ IMAGEPLACER $nimage __/)
    {
        my $pre = $1;
        my $image = $2;
        my $suffix = $3;
        my $post = $4;
        my $file = $image.'.'.$suffix;

        print "PROCESS IMAGE $file\n";

        my $source =
            (-f -s $tmpdir.'/'.$file) ? ($tmpdir.'/'.$file) :
            (-f -s $file) ? $file :
            undef;
        if(!defined $source)
        {
            print "Image $nimage = $tmpdir/$image not found\n";
            exit;
        }
        else
        {
            # save or convert image
            my $newfile = $nimage.'.'.$suffix;
            my $target = $imagedir.'/'.$newfile;
            if($suffix eq 'pdf' || $suffix eq 'eps' || $suffix eq 'ps')
            {
                # convert pdf, eps, ps to png
                $target =~s/$suffix$/png/;
                $file=~s/^$tmpdir//; # strip leading tmpdir
                print "COVERTIMAGE $source $target\n";
                `convert -density 300 $source $target`;
            }
            else
            {
                # all other types render in the browser
                print "COPYIMAGE $source to $target\n";
                copy($source,$target);
            }
            $images[$nimage] = $pre."images/$newfile".$post;
        }
        $nimage++;
    }
    for(my $i=0; $i<=$#images; $i++)
    {
        if(defined $images[$i])
        {
            $html =~ s/__ IMAGEPLACER $i __/$images[$i]/;
        }
    }

    # add logo
    my $logofile = $ENV{SVN}.'/tex/www/images/binary_c/logo3a-800.png';
    copy($logofile, 'images/logo.png');
    $html =~s !<div class=\"sphinxsidebarwrapper\">\K!<img width=\"200px\" id=\"sidebarlogo\" src=\"images/logo.png\">!;

    # save
    dumpfile($htmlfile,$html);

    my @installed_pages;
    if($install)
    {
        # install in www directory
        push(@installed_pages,
             install($svn_wwwdir,'svn'));

        # install in www directory
        push(@installed_pages,
             install($static_wwwdir,'static'));
    }

    # Done
    print "\n\nDone! refresh the pages at:\n\n",
        join("\n",@installed_pages,'');

    if($watch)
    {
        # check for when size changes
        my $size = -s $lyxfile;
        my $modtime = -M $lyxfile;
        my $perlsize = -s $0;
        my $modperl = -M $0;
        while($size == -s $lyxfile &&
              $modtime == -M $lyxfile)
        {
            if($perlsize != -s $0 || $modperl != -M $0)
            {
                # relaunch this Perl script on change
                exec($^X, $0, @ARGV);
            }
            sleep 1;
        }
    }
    else
    {
        exit;
    }
}

sub install
{
    my ($wwwdir,$mode) = @_;
    if(-d $wwwdir)
    {
        print "Installing in $wwwdir\n";

        # save html
        my $wwwhtmlfile = $wwwdir.'/binary_c.html';
        dumpfile($wwwhtmlfile,
                 $html);
        if($mode eq 'svn')
        {
            `svn add $wwwhtmlfile`;
        }

        # copy images
        my $wwwimagedir = $wwwdir.'/images';
        mkdir $wwwimagedir;
        `cp $imagedir/* $wwwimagedir/`;
        if($mode eq 'svn')
        {
            `svn add $wwwimagedir`;
            `svn add $wwwimagedir/*`;
        }

        # copy css
        my $wwwcssdir = $wwwdir.'/css';
        mkdir $wwwcssdir;
        `cp $cssdir/*.css $wwwcssdir/`;
        if($mode eq 'svn')
        {
            `svn add $wwwcssdir`;
            `svn add $wwwcssdir/*`;
        }
        return $wwwdir;
    }
    else
    {
        print "Tried to install in $wwwdir which does not exist\n";
        exit;
    }
}

sub maketmp
{
    mkdir $tmpdir;
    mkdir $tmpdir.'/images';
    mkdir $tmpdir.'/css';
    copy('references.bib',$tmpdir.'/references.bib');
}
