-- from https://groups.google.com/g/pandoc-discuss/c/rO6TzyzbHPE
function Pandoc (doc)
doc.blocks = pandoc.utils.make_sections(true, nil, doc.blocks):walk {
Div = function (div)
if div.attributes.number then
-- first child should be a heading
local header = div.content[1]
header.content = {div.attributes.number, pandoc.Space()}
.. header.content
header.attributes.number = nil
return div.content
end
end
}
return doc
end