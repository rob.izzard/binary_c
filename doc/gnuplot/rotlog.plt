set terminal postscript colour enhanced "Times-Roman" 18
set pointsize 0.2
set output "rotlog.ps"
set lmargin 10


#set title "With Rotationally-enhanced Mass Loss"
#set title "Without Rotationally-enhanced Mass Loss"
#set xrange[11.6:11.8]

set xlabel "Time/Myr"
set ylabel "Mass/M_{sun}"
plot "rotlog.dat" u 2:($6+$16) w p lt 4 title "Total","rotlog.dat" u 2:6 w p title "Star 1", "rotlog.dat" u 2:16 w p lt 3 title "Star 2"

set ylabel "{/Italic J} "
plot "rotlog.dat" u 2:10 w p title "Star 1", "rotlog.dat" u 2:20 w p lt 3 title "Star 2"

set ylabel "{/Symbol w} / s^{-1}"
plot "rotlog.dat" u 2:11 w p title "Star 1", "rotlog.dat" u 2:21 w p lt 3 title "Star 2"

set ylabel "{/Italic v}_{rot}/km s^{-1}"
plot "rotlog.dat" u 2:12 w p title "Star 1", "rotlog.dat" u 2:22 w p lt 3 title "Star 2"

set ylabel "{/Symbol W}={/Italic v}_{rot}/{/Italic v}_{crit}" offset 2,0
plot "rotlog.dat" u 2:13 w p title "Star 1", "rotlog.dat" u 2:23 w p lt 3 title "Star 2", 1 lt -1 lw 0.5

set ylabel "Mass loss enhancement factor" 
plot "rotlog.dat" u 2:((1.0/(1.0-$13))**0.43) w p title "Star 1", "rotlog.dat" u 2:((1.0/(1.0-$23))**0.43) w p lt 3 title "Star 2"

set ylabel "(net) Mass loss rate"
plot "rotlog.dat" u 2:14 w p title "Star 1", "rotlog.dat" u 2:24 w p lt 3 title "Star 2"

set ylabel "Separation/Period"
plot "rotlog.dat" u 2:5 w p title "Separation", "rotlog.dat" u 2:4 axes x1y2 w p title "Period" lt 3

set ylabel "Surface Nitrogen"
plot "rotlog.dat" u 2:15 w lp ps 1 title "Star 1", "rotlog.dat" u 2:25 w l title "Star 2" lt 3