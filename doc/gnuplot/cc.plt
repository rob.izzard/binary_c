set terminal postscript colour solid "Times-Roman" 20 linewidth 3 enhanced
set output "cc.ps"

set xlabel "Time"

set logscale y
#set yrange[1:1000]
#set yrange[2:3]
plot "cc.dat" u 1:2 w l title "R","cc.dat" u 1:3 w l title "R_L" lt 3

unset logscale y

#set yrange[1.595:1.605]
plot "cc.dat" u 1:4 w l title "Total Mass","cc.dat" u 1:6 w l lt 3 title "Mc(He)", "cc.dat" u 1:9 w l lt 4 title "Mc(CO)"


set yrange[1.0:1.8]
plot "cc.dat" u 1:4 w l title "Total Mass","cc.dat" u 1:6 w l lt 3 title "Mc(He)", "cc.dat" u 1:9 w l lt 4 title "Mc(CO)"

set yrange[7000:9000]
plot "cc.dat" u 1:7 w l title "Luminosity"

set yrange[*:*]
set xlabel "L"
set ylabel "R"
plot "cc.dat" u 7:2 w l notitle