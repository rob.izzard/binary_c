set terminal postscript enhanced "Times-Roman" 18 solid colour
set output "mdot.ps"

set border lw 2

set xlabel "Time"

set pointsize 0.1
set logscale y
set yrange[1e-8:0.01]
set title "Star 1 mass loss rate"
plot "mdot.dat" u 1:2 w p ps 1 title "New Rate", "mdot.dat" u 1:3 w p lt 3 title "H02 Rate", "mdot.dat" u 1:4 w p ps 0.5 lt 5 title "Actual" , "mdot.dat" u 1:22 w p lt 6 title "Dynamical","mdot.dat" u 1:23 w p lt 7 title "Thermal"

unset logscale y

set yrange[*:*]
set title "Mass and Separation"
x=5
set logscale y2
set ytics nomirror
set y2tics

plot "mdot.dat" u 1:5 w p title "Star 1", "mdot.dat" u 1:6 w p lt 3 title "Star 2" , "mdot.dat" u 1:($5+$6) w p lt -1 title "1+2", "mdot.dat" u 1:13 w p lt 4 axes x1y2 title "Separation" 

set title "R/R_{L}"
x=7
set key right
set y2label ""
set ylabel ""
set yrange[0.9:1.1]
set yrange[0:2]
unset logscale y
unset logscale y2
plot 1 lt -1, "mdot.dat" u 1:x w p lt 1 title "Star 1" , "mdot.dat" u 1:x+1 w p lt 3 title "Star 2"  

set title "R/R_{L} zoom"
unset y2tics
set yrange[0.99:1.01]
plot "mdot.dat" u 1:x w p title "Star 1" axes x1y1 

set title "Equatorial rotational velocity {/Italic v_{r,eq}} (km/s)"
unset logscale xy
set yrange[*:*]
x=14
plot "mdot.dat" u 1:x w p title "Star 1", "mdot.dat" u 1:x+1 w p lt 3 title "Star 2" 

set title "Equatorial rotational velocity {/Italic v_{r,eq}} / Critical"
unset logscale xy
set yrange[0:1.2]
x=16
plot "mdot.dat" u 1:x w p lt 1 title "Star 1", "mdot.dat" u 1:x+1 w p lt 3 title "Star 2" 

set title "Radii"
unset logscale x
set logscale y
set yrange[*:*]
x=42
plot "mdot.dat" u 1:x w p lt 1 title "Star 1", "mdot.dat" u 1:x+1 w p lt 3 title "Star 2" 

set title "Luminosities"
unset logscale x
set logscale y
set yrange[*:*]
x=44
plot "mdot.dat" u 1:x w p lt 1 title "Star 1", "mdot.dat" u 1:x+1 w p lt 3 title "Star 2" 

set title "Hertzsprung-Russell"
unset logscale xy
set yrange[2.5:5]
set xrange[3.5:5] reverse
set xlabel ""
HRD "mdot.dat" u 9:10 w p lt 1 title "Star 1", "mdot.dat" u 11:12 w p lt 3 title "Star 2" 












exit;
     

set title "Rotationally enhanced wind loss factor"
unset logscale xy

set yrange[*:*]
x=32
plot "mdot.dat" u 1:x w p title "Star 1", "mdot.dat" u 1:x+1 lt 3 title "Star 2" 

set title "HR Diagram"
unset logscale xy
set xrange[*:*] reverse
set yrange[4.0:*]
set xlabel "log {/Italic T}_{eff}"
set ylabel "log {/Italic L}"
x=9

plot "mdot.dat" u 9:10 w p title "Star 1", "mdot.dat" u 11:12 w p lt 3 title "Star 2" 

set title "RLOF stats"
set key below
set xrange[*:*] noreverse
set xlabel "Time"
set yrange[-0.1:2.1]
set ylabel ""
plot "mdot.dat" u 1:26 w p ps 1 title "Used Rate"
set yrange[-0.1:1.1]
plot "mdot.dat" u 1:27 w p ps 1 title "Runaway RLOF?"
plot "mdot.dat" u 1:28 w p ps 1 title "Thermal Cap?"

set title "RLOF stats"
set key below
set xrange[*:*] noreverse
set xlabel "Time"
set yrange[-0.1:2.1]
set ylabel ""
plot "mdot.dat" u 1:26 w p ps 1 title "Used Rate"
set yrange[-0.1:1.1]
plot "mdot.dat" u 1:27 w p ps 1 title "Runaway RLOF?"
plot "mdot.dat" u 1:28 w p ps 1 title "Thermal Cap?"

