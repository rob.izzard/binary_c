#LyX 2.3 created this file. For more info see http://www.lyx.org/
\lyxformat 544
\begin_document
\begin_header
\save_transient_properties true
\origin unavailable
\textclass article
\begin_preamble
\usepackage[usenames,dvipsnames]{xcolor}
\usepackage{eurosym}
\usepackage{enumitem}
\usepackage{latexsym}
\usepackage{multicol}
\usepackage{setspace}
\usepackage{makecell}
\date{} 
%\usepackage{tikz}
%\usetikzlibrary{calc,shadings}

%\usepackage[usenames, dvipsnames]{color}
\definecolor{Sectioncolour2}{RGB}{210,180,180} 
\definecolor{Sectioncolour1}{RGB}{179,100,100} 
\definecolor{Sectioncolour}{RGB}{159,29,35}  

% background

%\usepackage{gradientframe}

%\usepackage[pages=all,color=Sectioncolour,angle=90,hshift=-5cm,scale=1.5,contents={Nucleosynthesis in AGB Stars}]{background} 
  
%\usepackage{framed}
%\colorlet{shadecolor}{gray!25}   % you may try 'blue' here
%\renewenvironment{leftbar}{%
% \def\FrameCommand{%
%{%
%\textcolor{Sectioncolour2}{\vrule width 2pt}%
%\textcolor{Sectioncolour1}{\vrule width 2pt}%
%\textcolor{Sectioncolour}{\vrule width 2pt}%
%\textcolor{Sectioncolour1}{\vrule width 2pt}%
%\textcolor{Sectioncolour2}{\vrule width 2pt}%
%}\hspace{10pt}}%
%  \MakeFramed {\advance\hsize-\width \FrameRestore}}%
%{\endMakeFramed}


%\rule{0.4pt}{\textheight}

% try this
%http://www.ctan.org/tex-archive/macros/latex/contrib/eso-pic/
%http://stackoverflow.com/questions/2986240/latex-how-to-make-a-fullpage-vertical-rule-on-every-page

%\usepackage{eso-pic}
%\usepackage{graphicx}
%\newcommand\BackgroundPic{} 

\newcommand\BackgroundPic{%
\put(0,0){%
\parbox[b][\paperheight]{\paperwidth}{%
\vfill
\centering
\includegraphics[width=\paperwidth,height=\paperheight,%
keepaspectratio]{images/falk2-low-lolores.eps}%
\vfill
}}}

% remove background image (for speed)
%\renewcommand\BackgroundPic{}

% redefine sections to use red colour
\usepackage{titlesec}
\titleformat{name=\section}% command to alter
{\color{Sectioncolour}\normalfont\itshape\bfseries\large\setstretch{0.5}}%format
{\thesection}%label
{1em}%sep
{}%before code
{}%after code

% redefine subsections to use red colour 
\titleformat{name=\subsection}% command to alter
{\color{Sectioncolour}\normalfont\itshape\bfseries\large\setstretch{0.5}}%format
{\thesubsection}%label
{1em}%sep
{}%before code
{}%after code


\def\aj{AJ}                   % Astronomical Journal
\def\araa{ARA\&A}             % Annual Review of Astron and Astrophys
\def\apj{ApJ}                 % Astrophysical Journal
\def\apjl{ApJ}                % Astrophysical Journal, Letters
\def\apjs{ApJS}               % Astrophysical Journal, Supplement
\def\apss{Ap\&SS}             % Astrophysics and Space Science
\def\aap{A\&A}                % Astronomy and Astrophysics
\def\aapr{A\&A~Rev.}          % Astronomy and Astrophysics Reviews
\def\aaps{A\&AS}              % Astronomy and Astrophysics, Supplement
\def\mnras{MNRAS}             % Monthly Notices of the RAS
\def\pra{Phys.~Rev.~A}        % Physical Review A: General Physics
\def\prl{Phys.~Rev.~Lett.}    % Physical Review Letters
\def\pasa{PASA}               % Publications of the ASP
\def\pasp{PASP}               % Publications of the ASP
\def\pasj{PASJ}               % Publications of the ASJ
\def\nat{Nature}              % Nature
\def\aplett{Astrophys.~Lett.} % Astrophysics Letters 
\def\physrep{Phys.~Rep.}   % Physics Reports%
\def\na{NewA}   % new astronomy%

\usepackage{natbibspacing} 
\setlength{\bibspacing}{0mm}

% header
\usepackage{fancyhdr} 
\pagestyle{fancy}
\renewcommand{\headrulewidth}{0pt}
\fancyhead[LE,RO]{\color{Sectioncolour}\normalfont\itshape\bfseries{}}           % page number in "outer" position of footer line
\fancyhead[RE,LO]{\color{Sectioncolour}\rightmark} % other info in "inner" position of footer line

\raggedbottom
\end_preamble
\use_default_options true
\maintain_unincluded_children false
\language british
\language_package default
\inputencoding auto
\fontencoding global
\font_roman "utopia" "default"
\font_sans "default" "default"
\font_typewriter "default" "default"
\font_math "auto" "auto"
\font_default_family rmdefault
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100 100
\font_tt_scale 100 100
\use_microtype false
\use_dash_ligatures true
\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize 12
\spacing single
\use_hyperref false
\papersize a4paper
\use_geometry true
\use_package amsmath 1
\use_package amssymb 1
\use_package cancel 1
\use_package esint 1
\use_package mathdots 1
\use_package mathtools 1
\use_package mhchem 1
\use_package stackrel 1
\use_package stmaryrd 1
\use_package undertilde 1
\cite_engine natbib
\cite_engine_type authoryear
\biblio_style plainnat
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\justification true
\use_refstyle 1
\use_minted 0
\index Index
\shortcut idx
\color #008000
\end_index
\leftmargin 2cm
\topmargin 3cm
\rightmargin 2cm
\bottommargin 3cm
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\paragraph_indentation default
\is_math_indent 0
\math_numbering_side default
\quotes_style english
\dynamic_quotes 0
\papercolumns 1
\papersides 2
\paperpagestyle fancy
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Title
RLOF on the ZAMS
\end_layout

\begin_layout Standard
I outline here the algorithm to detect the minimum mass ratio, 
\begin_inset Formula $q_{\mathrm{min}}$
\end_inset

, at which Roche-lobe overflow (RLOF) begins on the zero-age main sequence
 (ZAMS) given a fixed 
\begin_inset Formula $M_{1}$
\end_inset

 and orbital period, 
\begin_inset Formula $P$
\end_inset

.
 This algorithm can only check whether RLOF will happen on the ZAMS, not
 find the 
\begin_inset Formula $q_{\mathrm{min}}$
\end_inset

 at which it occurs.
 To find 
\begin_inset Formula $q_{\mathrm{min}}$
\end_inset

 you will need to employ a numerical algorithm of some sort.
 
\emph on
binary_c 
\emph default
does this using a very crude method, which could be improved, but bear in
 mind that very few systems exist with such short periods that RLOF happens
 on the ZAMS, so better is probably not required.
 
\end_layout

\begin_layout Standard
This algorithm is coded in 
\emph on
src/binary_star_functions/show_instant_RLOF.c
\emph default
.
\end_layout

\begin_layout Section*
Roche functions
\end_layout

\begin_layout Standard
Eggleton's (1983, Astrophysical Journal 268, 368) function, for 
\begin_inset Formula $0\leq q=M_{2}/M_{1}\leq1$
\end_inset

 (the opposite of his mass ratio definition which is 
\begin_inset Formula $M_{1}/M_{2}$
\end_inset

), is,
\begin_inset Formula 
\begin{eqnarray}
f\left(q=\frac{M_{2}}{M_{1}}\right) & = & \frac{0.49q^{-2/3}}{0.6q^{-2/3}+\ln\left(1+q^{-1/3}\right)}\,,
\end{eqnarray}

\end_inset

hence the Roche radius,
\begin_inset Formula 
\begin{eqnarray}
R_{L1} & = & af\left(q\right)\,,
\end{eqnarray}

\end_inset

and then star 1 overflows its Roche lobe when,
\begin_inset Formula 
\begin{eqnarray}
R_{1} & \geq & R_{L1}\,.
\end{eqnarray}

\end_inset

Then we also have, because 
\begin_inset Formula $M_{1}$
\end_inset

 and 
\begin_inset Formula $P$
\end_inset

 are constant,
\begin_inset Formula 
\begin{eqnarray}
a^{3} & = & P^{3}\frac{G\left(M_{1}+M_{2}\right)}{4\pi^{2}}\,,
\end{eqnarray}

\end_inset

where 
\begin_inset Formula $a$
\end_inset

 is the orbital semi-major axis (i.e.
\begin_inset space ~
\end_inset

separation when circular), hence,
\begin_inset Formula 
\begin{eqnarray}
a & = & \left(\frac{GM_{1}P^{2}}{4\pi^{2}}\right)^{1/3}\left(1+q\right)^{1/3}=a_{0}\left(1+q\right)^{1/3}\,,
\end{eqnarray}

\end_inset

where,
\begin_inset Formula 
\begin{eqnarray}
a_{0} & = & \left(\frac{GM_{1}P^{2}}{4\pi^{2}}\right)^{1/3}\,,
\end{eqnarray}

\end_inset

is constant.
 Then,
\begin_inset Formula 
\begin{eqnarray}
R_{L1} & = & a_{0}\left(1+q\right)^{1/3}\,f\left(q\right)\,,
\end{eqnarray}

\end_inset

and similarly 
\begin_inset Formula 
\begin{eqnarray}
R_{L2} & = & a_{0}\left(1+q\right)^{1/3}\,f\left(1/q\right)\,.
\end{eqnarray}

\end_inset

Note that to treat eccentric orbits, we set,
\begin_inset Formula 
\begin{eqnarray}
a_{0} & = & \left(1-e\right)\left(\frac{GM_{1}P^{2}}{4\pi^{2}}\right)^{1/3}\,.
\end{eqnarray}

\end_inset

.
\end_layout

\begin_layout Section*
Star 1
\end_layout

\begin_layout Standard
The radius of star 1, 
\begin_inset Formula $R_{1}$
\end_inset

, is constant.
 To avoid RLOF we require that 
\begin_inset Formula $R>R_{L1}$
\end_inset

 for all 
\begin_inset Formula $q$
\end_inset

.
 
\begin_inset Formula $R_{L1}$
\end_inset

 has a minimum at 
\begin_inset Formula $q=1$
\end_inset

,
\begin_inset Formula 
\begin{eqnarray}
R_{L1}\left(q=1\right) & = & a_{0}2^{1/3}f\left(1\right)\\
 & \approx & 0.81664a_{0}\,,
\end{eqnarray}

\end_inset

so if 
\begin_inset Formula $R_{1}>0.81664a_{0}$
\end_inset

 star 1 can never overflow its Roche lobe.
 If 
\begin_inset Formula $R_{1}<0.81664a_{0}$
\end_inset

 we have to find the root of,
\begin_inset Formula 
\begin{eqnarray}
R_{1} & = & a_{0}\left(1+q\right)^{1/3}f\left(q\right)\,,
\end{eqnarray}

\end_inset

by numerical means.
\end_layout

\begin_layout Section*
Star 2
\end_layout

\begin_layout Standard
The radius of star 2, 
\begin_inset Formula $R_{2}$
\end_inset

, is an unknown function of 
\begin_inset Formula $q=M_{2}/M_{1}$
\end_inset

, and to avoid RLOF we require that 
\begin_inset Formula $R_{2}>R_{L2}$
\end_inset

 for all 
\begin_inset Formula $q$
\end_inset

.
 Let us assume that 
\begin_inset Formula $R_{2}$
\end_inset

 monotonically increases with 
\begin_inset Formula $q$
\end_inset

, as stellar evolution tells us it should.
 
\begin_inset Formula $R_{L2}$
\end_inset

 has a maximum at 
\begin_inset Formula $q=1$
\end_inset

, 
\begin_inset Formula 
\begin{eqnarray}
R_{L2}\left(q=1\right) & = & a_{0}2^{1/3}f\left(1\right)\\
 & \approx & 0.47741a_{0}\,,
\end{eqnarray}

\end_inset

and a formal minimum at 
\begin_inset Formula $q=0$
\end_inset

,
\begin_inset Formula 
\begin{eqnarray}
R_{L2}\left(q=0\right) & = & a_{0}2^{1/3}f\left(1/0\right)=0\,.
\end{eqnarray}

\end_inset

However, 
\begin_inset Formula $q$
\end_inset

 cannot be 
\begin_inset Formula $0$
\end_inset

, it has a minimum
\begin_inset Foot
status collapsed

\begin_layout Plain Layout
The macro 
\family typewriter
MAXIMUM_BROWN_DWARF_MASS
\family default
 is defined in 
\emph on
binary_c
\emph default
 to represent the minimum stellar mass.
\end_layout

\end_inset

 
\begin_inset Formula $q_{\mathrm{*}}=M_{\mathrm{*min}}/M_{1}\approx0.07\mathrm{\,M_{\odot}}/M_{1}$
\end_inset

, so,
\begin_inset Formula 
\begin{eqnarray}
R_{L2}\left(q=q_{\mathrm{*}}\right) & = & a_{0}\left(1+q_{\mathrm{*}}\right)f\left(1/q_{\text{*}}\right)\,.
\end{eqnarray}

\end_inset

If
\begin_inset Formula 
\begin{eqnarray}
R_{2}\left(q=q_{*}\right) & > & R_{L2}\left(q=q_{*}\right)\,,
\end{eqnarray}

\end_inset

and
\begin_inset Formula 
\begin{eqnarray}
R_{2}\left(q=1\right) & > & R_{L2}\left(q=1\right)\,,
\end{eqnarray}

\end_inset

there is no RLOF.
 If 
\begin_inset Formula $R_{2}=R_{L2}$
\end_inset

 at some intermediate 
\begin_inset Formula $q$
\end_inset

 then 
\begin_inset Formula $R_{2}>R_{L2}\left(q=1\right)$
\end_inset

 also, hence our condition fails and RLOF is detected.
 As with star 1, we need to numerically calculate when 
\begin_inset Formula $R_{L2}=R_{2}$
\end_inset

 to find 
\begin_inset Formula $q_{\mathrm{min}}$
\end_inset

 if it exists.
\end_layout

\end_body
\end_document
