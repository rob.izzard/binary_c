#!/usr/bin/env perl
use strict;
use 5.16.0;
use rob_misc;
my $nthreads = ncpus();
my $bin = './src/perl/scripts2/gaiaHRD.pl';
#$bin = "find .";
while($nthreads > 0)
{
    my $cmd = "time $bin nthreads=$nthreads";
    my $r = `$cmd 2>\&1`;
    my @timings = ($r=~/(\S+)user (\S+)system (\S+)elapsed (\S+)CPU/);

    state $head = 0;
    if($head == 0)
    {
	$head = 1;
    }
    printf "%4d %10s %10s %10s %10s\n",$nthreads,@timings;
    
    $nthreads--;
}
