tbse : run one or more stellar systems with binary_c

Usage:

        tbse [option flags] [files] <(extra) binary_c args>


tbse is a script which runs one or more stellar systems.

The most common use is to just run tbse for the star defined in
the script by the various parameters, e.g. M1, M2, ORBITAL_PERIOD etc.

------------------------------------------------------------

If [files] are given to binary_c, each file is assumed to contain
a set of arguments. This is very useful for rerunning stars from
files which are saved by the binary_grid software.



The extra binary_c args are appended to the arguments set in the
tbse script. Thus, if you want to use the tbse script's arguments and,
say, set M1 to 10 solar masses, run

tbse --M_1 10.0

If you want to run 100 random systems, run

tbse --repeat 100 --random_systems 1

All other parameters will be as in the tbse script.

------------------------------------------------------------

option flags for running tbse 

echo
        Outputs the arguments that would be sent to binary_c,
        but does not run binary_c.

echolines
        As echo but with arguments on separate lines. This is
        useful if you're a human, or want to grep the output for
        a particular argument's value.

arglines <file>
         Runs binary_c using the arguments from <file> where
         each line of the file contains a single argument.

clip
        Runs binary_c, putting the output into the clipboard.

multicore <nthreads>
        Runs binary_c on multiple cores. Useful for running
        random systems until one crashes.
    

------------------------------------------------------------

options flags for building binary_c

pgo
        Rebuilds binary_c with profile guided optimization. 

------------------------------------------------------------

option flags for debugging


debug
        Run binary_c through gdb.
        Note that you have to build with debugging.
        
valgrind
        Run binary_c through valgrind's memcheck (its default).
        Note that you have to build with debugging.
        
valgrind_args
        Show the arguments that would be sent to valgrind but do not run binary_c.
        Note that you have to build with debugging.

multicore_valgrind <nthreads>
        Runs binary_c through valgrind on multiple cores. Useful for running
        random systems until one crashes.

massif
        Run binary_c through valgrind's massif tool.
        Note that you have to build with debugging.
        
callgrind
        Run binary_c through valgrind's callgrind tool.
        Note that you have to build with debugging.
        
cachegrind
        Run binary_c through valgrind's cachegrind tool.
        Note that you have to build with debugging.
        
ptrcheck
        Run binary_c through valgrind's ptrcheck tool.
        Note that you have to build with debugging.
        
drd
        Run binary_c through valgrind's drd tool.
        Note that you have to build with debugging.

gprof
gprof_with_stdout
gprof_lines
        Run binary_c through the gprof profiler.
        Note that you have to build with debugging.


------------------------------------------------------------

Return value

       tbse, when run normally, returns the return value of binary_c

------------------------------------------------------------

If you want help with a specific binary_c option, run

./binary_c <string>

where <string> is the option about which you hold some
curiosity.

------------------------------------------------------------
