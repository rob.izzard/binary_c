#!/usr/bin/env perl
use strict;
use Carp qw(confess);
#
# Filter script to clean up LaTeX before pandoc converts it to Markdown
#

# get working dir and temp dir
my $wd = shift @ARGV;
my $tmp = shift @ARGV;
my @file_locations = ($wd,$tmp);

my @commands;
my $in_listing = 0;
my $list_content = undef;
while(<>)
{
    chomp;

    # remove comments
    my $comment;
    if(s/([^\\]?)(\%.*)/$1/)
    {
        $comment = $2;
    }
    else
    {
        $comment = undef;
    }

    # input sub-files directly
    if(s/\\input\{([^}]+)\}/__FILE_GOES_HERE__/)
    {
        my $file = $1;
        my $found_file = search_tex_file($file);
        if(!defined $found_file)
        {
            print STDERR "Searched for LaTeX file $file but failed to find it in any known location (",join(',',@file_locations),")\n";
            exit;
        }
        my $content = slurp($found_file);
        s/__FILE_GOES_HERE__/$content/;
    }

    # replace macros
    my @newcommands;
    if(s/\\global\\long\\def\\([a-zA-Z]+)(.*)//)
    {
        my $macro = $1;
        my $replacement = $2;
#        $replacement =~ s/\\it\s+(\S+)/$1/g; # remove crude italics
        $replacement =~ s/\\text//g; # not required
        #$replacement =~ s/\\_/\\textunderscore{}/g; # what to do about subscripts?
        $replacement =~ s/\\protect//;

        push(@commands,[$macro,$replacement]);
        push(@newcommands,[$macro,$replacement]);
    }


    foreach my $cmd (@commands)
    {
        s/\$(?:\\protect)?\\$cmd->[0]\$/\\$cmd->[0]/g;
    }

    foreach my $cmd (@newcommands)
    {
        $_ .= "\n\\newcommand\\" . $cmd->[0] . '{' . $cmd->[1] . ' }';
    }

    if(s/(\\begin\{lstlisting\})(.*)/$1 /)
    {
        $in_listing = 1;
        $list_content = $2 . $comment;
        $comment = undef;
    }
    else
    {
        if(s/(.*)\\end\{lstlisting\}//)
        {
            $in_listing = 2;
            $list_content .= $1.$comment."\n";
            $comment = undef;
        }
        elsif($in_listing == 1)
        {
            $list_content .= $_.$comment."\n";
            $comment = undef;
            $_ = '';
        }
    }

    if($in_listing == 2)
    {
        $list_content =~ s/^\[[^\]]+\]//;
        $_ = $list_content . '\end{lstlisting}';
        $in_listing = 0;
    }

    s/\\protect//g;

    # remove inline lists
    if(0)
    {
        while(s/(\\lstinline)//)
        {
        }
    }

    # clean up labels
    s/\#(sub)*sec\:/\#/g;

    # Output
    print $_;
    if(defined $comment)
    {
        chomp $comment;
        print $comment;
    }
    print "\n";
}

sub slurp
{
    # Usage: slurp(<filename>)
    # Returns the contents of file given by <filename> as a string, on failure dies with Carp::confess

    open(my $fh,'<',$_[0])||confess("cannot open $_[0] in slurp");
    return (do { local( $/ ) ; <$fh> } );
}

sub search_tex_file
{
    # given a filename, search for this file
    my ($file) = @_;
    foreach my $location (@file_locations)
    {
        foreach my $extension ('.tex','')
        {
            if(-f $file.$extension)
            {
                my $found_file = $location.'/'.$file.$extension;
                return $found_file;
            }
        }
    }
    return undef;
}
