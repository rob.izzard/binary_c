#!/usr/bin/env perl
use strict;
use rob_misc;

print "USE apitest2.pl\n";exit;

my $od="/tmp/apitest/$$/";
mkdirhier($od);

while(1)
{
    # run the test program
    `./apitest.sh $$`;

    my @fp;
    open(APITEST,"</tmp/apitest/$$/apitest.out")||die;
    while(<APITEST>)
    {
	# remove ANSI colouring 
	$_=remove_ANSI($_);

	# clean up pointers (they will always differ!)
	s/0x[a-f0-9]+/pointer/g;
	
	if(s/^\# APITEST//)
	{
	    print $$.': '.$_;
	}
	elsif(s/^\(star (\d+)\) //)
	{
	    if(!defined($fp[$1]))
	    {
		#print "OPEN $1\n";
		open($fp[$1],">$od/$1.dat");
	    }
	    print {$fp[$1]} $_;
	}
    }
    close APITEST;

    my $error=0;
    for(my $i=0;$i<=$#fp;$i++)
    {
	close $fp[$i];

	if($i!=0)
	{
	    my $diff=`diff -q $od/0.dat $od/$i.dat`;
	    chomp $diff;
	    if($diff ne '')
	    {
		$diff=~s/and//;
		print "$$: DIFF $i : '$diff'\n";
		$error=1;
	    }	    
	}
    }
    if($error)
    {
	print "$$: Exit because of error (diff failed)\n";
	exit;
    }
}
