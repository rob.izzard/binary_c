!
! Demo code to run binary_c from FORTRAN
!
! Tested on Ubuntu 16.10 with gcc and gfortran 5.4.1
!
! See README and the Make_fortran script,
! and the src/api/fortran.c file which contains
! the interface functions that are built into the API
! shared library.
!
! See also
! http://www-h.eng.cam.ac.uk/help/tpl/languages/mixinglanguages.html
!

!
! This is a short test program to make ensemble data in binary_c
! and grab it in FORTRAN
!


program apitestf
  use iso_c_binding
  implicit none

  interface
     function binary_c_fortran_api_buffer_buffer(stardata) bind(c, name="binary_c_fortran_api_buffer_buffer_")
       use iso_c_binding
       implicit none
       type (c_ptr) :: stardata ! takes a stardata pointer
       type (c_ptr) :: binary_c_fortran_api_buffer_buffer ! returns a char* pointer
     end function binary_c_fortran_api_buffer_buffer
  end interface



  ! A local type to simplify memory management.
  TYPE string
     CHARACTER(LEN=:,KIND=C_CHAR), ALLOCATABLE :: item
  END TYPE string

  ! These are passed to binary_c to set up the stars
  ! and/or returned by binary_c
  double precision m1,m2,mc1,mc2,per,ecc,metallicity
  double precision maxt,max_m1,next_m1,dm1
  double precision ensemble_logdt,ensemble_startlogtime
  integer ensemble,ensemble_logtimes,ensemble_defer

  ! strings
  integer string_size
  parameter(string_size = 1024)
  character (len=string_size) :: argstring
  character (len=string_size) :: format_string

  type(c_ptr),pointer :: stardata_pointer,store_pointer,persistent_data_pointer
  type(c_ptr),target :: stardata,store,persistent_data,s

  type(c_ptr) :: c_buffer ! stores returned C buffer
  character (len=1,kind=c_char),dimension(:),pointer :: filchar=>null()

  !character,pointer::buffer
  character(:), allocatable :: buffer

  integer(kind=c_int) :: buffer_size

  stardata_pointer => stardata
  store_pointer => store
  persistent_data_pointer => persistent_data

  ! our pointers must be NULL when first sent to binary_c_fortran_api_new_system()
  stardata_pointer = C_NULL_PTR
  store_pointer = C_NULL_PTR
  persistent_data_pointer = C_NULL_PTR

  ! system properties (except M1)
  m2 = 0.5d0
  per = 1d6
  ecc = 0.d0
  metallicity = 0.02d0
  maxt = 15000d0

  ! ensemble options
  ensemble = 1 ! turn on ensemble
  ensemble_logdt = 0.1d0 ! d(logt) = 0.1 dex
  ensemble_logtimes = 1 ! use log time
  ensemble_startlogtime = 0.1d0 ! start logging at logt=0.1

  ! loop over M1, adding up the output in the deferred ensemble
  m1 = 1.0d0
  max_m1 = 6.0d0
  dm1 = 1.0d0

  do while (m1 <= max_m1)

     ! next mass
     next_m1 = m1 + dm1

     ! defer output if the next mass
     ! is not the final star
     if (next_m1 > max_m1) then
        ensemble_defer = 0
     else
        ensemble_defer = 1
     endif

     format_string = '("binary_c log_filename /dev/null M_1 ",E10.3," M_2 ",E10.3," metallicity ",E10.3," separation 0 orbital_period ",E10.3," eccentricity ",E10.3," max_evolution_time ",E10.3," ensemble ",I1," ensemble_logtimes ",I1," ensemble_logdt ",E10.3," ensemble_startlogtime ",E10.3," ensemble_filters_off True ensemble_defer ",I1," ensemble_filter_SCALARS True internal_buffering INTERNAL_BUFFERING_STORE ",A)'
     write(argstring,format_string) m1,m2,metallicity,per,ecc,maxt,&
          ensemble,ensemble_logtimes,ensemble_logdt,ensemble_startlogtime,&
          ensemble_defer,char(0)

     ! create new stardata structure
     call binary_c_fortran_api_new_system(stardata_pointer, &
          C_NULL_PTR,              &
          C_NULL_PTR,              &
          store_pointer,           &
          persistent_data_pointer, &
          argstring)

     ! evolve until maxt
     call binary_c_fortran_api_evolve_for_dt(maxt,stardata_pointer)

     ! set the mass
     m1 = next_m1
  end do

  ! get the binary_c output in a FORTRAN string
  call binary_c_fortran_api_buffer_size(stardata_pointer,buffer_size)
  buffer = stringc2f(buffer_size,binary_c_fortran_api_buffer_buffer(stardata_pointer))

  ! show results
  write(*,'(1A)')buffer

  ! free memory
  call binary_c_fortran_api_free_memory(stardata_pointer,1,1,1,1,1)

  ! done
  stop

contains

  ! function to convert C (char*) buffer to F string
   function stringc2f(n, cstr) result(fstr)
    use iso_c_binding
    implicit none
    integer, intent(in) :: n
    type(c_ptr), intent(in) :: cstr
    character(:), allocatable :: fstr
    character(n, kind=c_char), pointer :: fptr
    call c_f_pointer(cstr, fptr)
    fstr = fptr
  end function stringc2f

end program apitestf
