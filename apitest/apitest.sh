#!/bin/bash

LD_LIBRARY_PATH="$(../binary_c-config libdirs_list | tr ' ' ':')"
export LD_LIBRARY_PATH="$LD_LIBRARY_PATH"":../src"

# choose to
#
# 1 : just run
CMD="./binary_c-apitest 2>&1"
# 2 : run through gdb
#CMD="gdb ./binary_c-apitest 2>&1"
# 3 : run through valgrind
#CMD="$(../tbse valgrind_cmd) ./binary_c-apitest 2>&1"


echo $CMD
$CMD
