#!/usr/bin/env perl
use strict;
use rob_misc;
use Time::HiRes qw(gettimeofday tv_interval);
use 5.16.0;
use POSIX;

#
# Script to test the binary_c API by running many identical systems in parallel
# and compares the results from them to make sure they are truly identical.
#

$|=1;
my @blockn;
my $thissystem;
my $eps=2.0*DBL_EPSILON;
my $abs_eps=10.0*DBL_MIN;
my $neg_abs_eps=-$abs_eps;


#tests();# floating point tests

while(1)
{
    # run the test program
    open(APITEST,"-|","nice -n +19 ./apitest2.sh $$")||die("cannot open apitest2.sh for reading pipe");
    my @blocks;
    my $prevt;
    my $error=0;
    
    while(<APITEST>)
    {
	# remove ANSI colouring 
	$_=remove_ANSI($_);

	# clean up pointers (they will always differ!)
	s/0x[a-f0-9]+/pointer/g;

	if(/Evolve system \d+ from t=(\S+) to \S+/)
	{
	    $prevt=$1 if(!defined($prevt));
	    if($prevt != $1)
	    {
		$error=check_blocks($prevt,\@blocks); 
		$prevt=$1; # set prevt
	    }
	}
	elsif(s/^\# APITEST//)
	{
	    $thissystem=$_;
	    print "Testing system : ".$$.': '.$_;
	}
	elsif(s/^\(star (\d+)\) //)
	{
	    $blocks[$1].=$_;

	    $blockn[$1]++;
	    
	    if($1==0)
	    {
		output(sprintf "Time $prevt Block % 3d linecount % 7d            \x0d",$1,$blockn[$1]);
	    }
	    else
	    {
		output(sprintf "Time $prevt Block % 3d linecount % 7d (% 5.2f%%) \x0d",$1,$blockn[$1],
		       $blockn[$1]/MAX(1,$blockn[0])*100.0);
	    }
	}

	close APITEST if($error);
    }
    close APITEST;
    	
    # last blocks
    $error=check_blocks($prevt,\@blocks) if(!$error);
  
    print "\n";
    if($error)
    {
	print "$$: Exit because of error (diff failed)\n";
	print "System : $thissystem\n";
	exit;
    }
#    exit;
}

sub check_blocks
{
    my $prevt=$_[0];
    my $blocks=$_[1];
    my $error=0;
    my $indent="\e[60C";

    # do not compare t=0 because that contains all the setup
    # which is done ONCE, unless API_TESTING is defined, then all is ok
#    if($prevt>0.001)
    {
	my $n=$#{$blocks}+1;
	my $s=length($$blocks[0]);
	output("$indent Check $n blocks for t=$prevt (block size $s)      \x0d");

	for(my $i=1;$i<$n;$i++)
	{
	    #print "Check block $i = $$blocks[$i]\n";
	    if($$blocks[$i] ne $$blocks[0])
	    {

		print "$indent At prevt=$prevt : Block $i is not the same as block 0 : possible failure\n";
		my @block0=split(/\n/,$$blocks[0]);
		my @blocki=split(/\n/,$$blocks[$i]);
		
		my $m=1+MAX($#block0,$#blocki);
		print "$indent Line counts $#block0 $#blocki\n";

		for(my $j=0;$j<$m;$j++)
		{
		    if($block0[$j] ne $blocki[$j])
		    {
			# blocks non-equal : we have to be more careful
			# and check the floating point
			my @split0 = split(/\s+/,$block0[$j]);
			my @spliti = split(/\s+/,$blocki[$j]);
			
			my $p=1+MAX($#split0,$#spliti);
			for(my $k=0;$k<$p;$k++)
			{
			    #printf "CF '%s' to '%s' (eq=%d, fequal=%d)\n",$split0[$k],$spliti[$k],$split0[$k] eq $spliti[$k],fequal($split0[$k],$spliti[$k]);
			    
			    # if strings are equal, obviously all is ok
			    next if($split0[$k] eq $spliti[$k]);
			     
			    # then check floating point
			    next if fequal($split0[$k],$spliti[$k]);
			
 			    # otherwise fail
			    $error=1;
			    $k=$p+1; # force loop end
			}
			print "$indent Line $j\n0 : $block0[$j]\n$i : $blocki[$j]\n\n" if($error);
		    }
		}		
		last if($error);
	    }
	}
    }    
    @$blocks=(); # clear out blocks
    @blockn=();

    if(!$error)
    {
	output_last( "$indent Checked t=$prevt : OK!                                     \x0d");
    }

    return $error;
}

sub output
{
    state $last_out=[gettimeofday];
    my $t0=[gettimeofday];
    my $dt=tv_interval($last_out,$t0);
    if($dt > 0.2)
    {
	$last_out = $t0;
	print $_[0];
    }
}

sub output_last
{
    state $last_out=[gettimeofday];
    my $t0=[gettimeofday];
    my $dt=tv_interval($last_out,$t0);
    if($dt > 0.2)
    {
	$last_out = $t0;
	print $_[0];
    }
}

sub fequal
{
    # check if $_[0] and $_[1] are equal
    return (($_[0] == $_[1]) ||  # catches a=b (inc. 0=0)
	    ($_[0]-$_[1]<$abs_eps && $_[0]-$_[1]>$neg_abs_eps) || # catches 1e-300 vs 0
	    (abs(($_[0]-$_[1])/MAX($_[0],$_[1]))<$eps) # accurate to within eps
	);    
}

sub tests
{
    # floating point tests
    printf "DBL_EPSILON %g : eps=%g\n",DBL_EPSILON,$eps;
    print "Test fequal:\n";
    my @n= (1e200,1e10,1.0,0.999999,1e-10,1e-20,1e-50,1e-200,1e-300,DBL_MIN*2,DBL_MIN);
	
    my $format_space='% 15s';
    my $format_n='% 15g';
    my $format_test='% 15d';

    printf $format_space, '';
    foreach my $n2 (@n)
    {
	printf $format_n,$n2;
    }
    print "\n";

    foreach my $n1 (@n)
    {
	printf $format_n,$n1;
	
	foreach my $n2 (@n)
	{
	    printf $format_test, fequal($n1,$n2);
	}
	print "\n";
    }

    exit;

}
