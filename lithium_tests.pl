#!/usr/bin/env perl
use 5.16.0;
use strict;
use rob_misc;
use binary_grid2;
use binary_grid::C; # backend : C or Perl
use Sort::Key qw(nsort);
use Histogram;
use Maths_Double;

# tests of the lithium code

# which tests to run?
pertests() if(0);

my %tests=(
    single_stars=>0,
    binary_stars=>1
    );

my $titles=1; # 0 for talk
my $fortalk=0; # set to 1 for annotations for the talk
#print gphead();exit;

# make population with default physics
my $population = binary_grid2->new(
    'lithium_hbb_multiplier'  => 1.0,
    'lithium_GB_post_1DUP'    => 0.0,
    'lithium_GB_post_Heflash' => 0.0,
    'gb_reimers_eta'          => 0.5,
    'nova_retention_fraction' => 1.0,
    'vb'                      => 1,
    'metallicity'             => 0.02,
    'max_evolution_time'      => 13700.0,
    'nthreads'                => rob_misc::ncpus(),
    'yields_dt'               => 1,
    );
$population->parse_args();


# star-by-star plots
my $starsdir='/tmp/listars';
mkdirhier ($starsdir);

my %isotopes = $population->isotope_hash();
my %cols = map { state $c=1; $_ => $c++ } ('t','M','M0','R',
					   'kw','ntp','XLi','epsLi',
					   'tagb');
my $Alabel = $fortalk ? 'A(Li)=log(Li/H)+12' : '{/Symbol e}_{Li7}';

my @plots=(
    {
     'xlabel'=>'Radius/Rsun',
     'ylabel'=>'Number of thermal pulses',
     'xcol'=>$cols{R},
     'ycol'=>$cols{ntp},
    },
    {
     'xlabel'=>'Time on TPAGB/years',
     'ylabel'=>,$Alabel,
     'xcol'=>$cols{tagb},
     'ycol'=>$cols{epsLi},
     'xrange'=>'0:'.($fortalk ? '250000' : '1e6'),
     'yrange'=>'-1:*'
    },
    {
     'xlabel'=>'Time on TPAGB/years',
     'ylabel'=>'X_{Li7}',
     'xcol'=>$cols{tagb},
     'ycol'=>-$cols{XLi},
     'xrange'=>'0:5e5',
     'yrange'=>'1e-12:*'
    },

    {
	'xlabel'=>'Number of thermal pulses',
	'ylabel'=>,$Alabel,
	'xcol'=>$cols{ntp},
	'ycol'=>$cols{epsLi},
	'xrange'=>'0:50',
	'yrange'=>'-1:*'
    },
    
   {
	'xlabel'=>'Time/Myr',
	'ylabel'=>$Alabel,
	'xcol'=>-$cols{t},
	'ycol'=>$cols{epsLi},
	'xrange'=>'10:*',
	'yrange'=>'-1:*'
    },


    {
	'xlabel'=>'Radius/Rsun',
	#'ylabel'=>'{/Symbol e}_{Li7}',
	'ylabel'=>$Alabel,
	'xcol'=>$cols{R},
	'ycol'=>$cols{epsLi},
	'yrange'=>'-2:*',
	'xrange'=>'250:730'
    },

    );

# primary mass
my $mmin = 3.0;
my $mmax = 7.0;
my $nm   = 100;
my $dm   = ($mmax - $mmin) / ($nm - 1);

# override for talk
if($fortalk)
{
    $mmin=4.0;
    $mmax=6.0;
    $dm=1.0;
}

# make mass list
my @masses;
for(my $m1=$mmin; 
    $m1<=$mmax; 
    $m1+=$dm)
{
    push(@masses, (sprintf "%g", $m1));
}

my $surface = Histogram->new_shared();

if($tests{binary_stars})
{
    # secondary mass
    my $m2=0.5;

    # log period/days
    my $logpermin = log10(1000.0);
    my $logpermax = log10(10000.0);
    my $nper = 100;
    my $dlogper   = ($logpermax - $logpermin) / ($nper - 1);

    # make period list
    my @logperiods;
    for(my $logper = $logpermin; 
        $logper   <= $logpermax; 
        $logper   += $dlogper)
    {
	push(@logperiods,(sprintf "%g",$logper));
    }

    # set up surface plot
    $surface->format('gnuplot');
    $surface->pre_binned(1);
    $surface->binwidths($dm,$dlogper);
    
    # make a list of systems to be run and set
    # up the population to use this list
    my $starlist = [];
    $population->{_grid_options}->{flexigrid}->{'grid type'} = 'list';
    $population->{_grid_options}->{flexigrid}->{'list reference'} = $starlist;
    $population->{_grid_options}->{parse_bse_function_pointer} = \&parse_bse;
    
    # loop over period and m1 to set up the systems to be run
    foreach my $logper (@logperiods)
    {
	my @fixed_P_plotfiles;
	my $fixed_P_fp;
	
	# make period from log period
	my $per = 10.0**$logper;

	# loop over masses M1
	foreach my $m1 (@masses)
	{
	    $m1 = sprintf "%g",$m1;
	 
            push(@$starlist,
                {
                    'M_1' => $m1,
                        'M_2' => $m2,
                        'orbital_period' => $per,
                        'eccentricity' => 0.0,
                        'probability' => 1.0,
                }
                );
	}
    }

    #@$starlist = @$starlist[0..10]; # only do a few stars for testing...

    print "Run ",scalar @$starlist," stars\n";
    
    # run the stars
    $population->evolve();
        
    $surface->dump("/tmp/li.dat");
}

if($tests{single_stars})
{
    # TODO with binary_grid2
    my $od='/tmp/lithium-single'; 
    mkdir $od;
    my $plt="/tmp/lithium-single.plt";
    my $ps=$plt;
    $ps=~s/plt$/ps/;

    open(my $single_stars_fp,">$plt");
    say $single_stars_fp gphead($ps);

    my @datafiles;

    # now make plots as a function of mass
    foreach my $m (@masses)
    {
	my $of="$od/M$m.dat";
	my $r="tbse args --M_1 $m --orbital_period 1000000000  | filter LITHIUMF > $of";
	# TODO $q->q($r);
	push(@datafiles,$of);
    }

    # TODO evolve
    
    makeplot($single_stars_fp,\@datafiles,'M','M_{/CMSY10 \\014}');
    close $single_stars_fp;    
    `cd $od; gnuplot $plt`;
}

exit;


sub makeplot
{
    my $fp = shift;
    my $plotfiles=shift;
    my $v=shift;
    my $unit=shift;

    foreach my $plot (@plots)
    {
	foreach ('x','y')
	{
	    my $offset = $_ eq 'y' && $$plot{$_.'label'}=~/_/ ? 1 : 0;
	    print $fp "set ".$_."label \"".$$plot{$_.'label'}."\" offset $offset,0\n";
	}

	my ($xcol,$ycol) = ($$plot{xcol},$$plot{ycol});

	# select logged x/y columns
	foreach ('x','y')
	{
	    if(eval '$'.$_.'col'<0)
	    {
		eval '$'.$_.'col*=-1';
		say $fp "set logscale $_";
	    }
	    else
	    {
		say $fp "unset logscale $_";
	    }

	    say $fp "set ".$_."range[".(defined $$plot{$_.'range'} ? $$plot{$_.'range'} : '*:*').']';
	    
	}

	# plot command
	print $fp "plot ",join(',',
			       map{
				   "\"$_\" u $xcol:$ycol w lp ps 0.2 ".($titles?"title \"\{/=10 ".($_=~/$v(\d+(?:\.\d+)?)/)[0].' '.$unit."\}\"":'notitle')
			       }@$plotfiles),"\n";
    }
}

sub gphead
{
    my $ps=shift;
    my $s=
    "set terminal postscript solid linewidth 3 enhanced colour \"Helvetica\" 22 fontfile 'cmsy10.pfb' dashlength 4
set output \"$ps\"
set format xy '%g'
set key below samplen 1
";

# colours
    my @colours=grep {s/(\d\.\d+)\s*/sprintf'%02x',$1*255/ge && s/^\s*/#/}
    split(/\n/,
	  "0.00  0.00  1.00
	0.00  0.50  0.00 
	1.00  0.00  0.00 
	0.00  0.75  0.75
	0.75  0.00  0.75
	0.75  0.75  0.00 
	0.25  0.25  0.25
	0.75  0.25  0.25
	0.95  0.95  0.00 
	0.25  0.25  0.75
	0.75  0.75  0.75
	0.00  1.00  0.00 
	0.76  0.57  0.17
	0.54  0.63  0.22
	0.34  0.57  0.92
	1.00  0.10  0.60
	0.88  0.75  0.73
	0.10  0.49  0.47
	0.66  0.34  0.65
	0.99  0.41  0.23");

    my $c=1;
    map
    {
	$s .= "set style line $c linecolor rgbcolor \"$_\"\n";
	$c++;
    }@colours;

    $s.="set style increment user\n";

    return $s;
}

sub pertests
{
    my $m1=5.0;
    my $m2=0.5;

    my $agbper = binary_grid::agbperiod($m1,$m2);
    my $agbsep = binary_stars::calc_sep_from_period($m1,$m2,$agbper);
    my $agbr = binary_stars::ragb($m1);
    my $RL = binary_stars::roche_lobe($m1/$m2) * $agbsep;

    print "Period = $agbper\nSep = $agbsep vs R=$agbr, RL=$RL\n";

    # if R=RL=500 then...
    $RL = 500;
    $agbsep = $RL / binary_stars::roche_lobe($m1/$m2);
    $agbper = binary_stars::calc_period_from_sep($m1,$m2,$agbsep);

    print "R=RL=$RL implies a=$agbsep P=$agbper\n";

    exit;
}


sub parse_bse
{
    my ($population, $results) = @_;
    my $m1 = ($population->{_grid_options}->{args} =~ /M_1 (\S+)/)[0];
    my $per= ($population->{_grid_options}->{args} =~ /orbital_period (\S+)/)[0];
    my @x = ('H1','He4','Li7');
    my $logper = sprintf "%g",log10($per);
    my $yields=[];
    
    while(1)
    {
        my $la = $population->tbse_line();
        my $header = shift @$la;
        last if($header eq 'fin');
        
        if($header =~ /^DXYIELDbin/)
        {
            # differential yields : integrate them
            shift @$la; # remove the time
            $population->decompress_yields($la);
            simple_increment_array($yields, $la); 
        }
        elsif($header eq 'XYIELDbin__')
        {
            # single, final timestep yields
            shift @$la; # remove the time
            $population->decompress_yields($la);
            @$yields = @$la;
        }
    }

    printf "M=%10g per=%10g -> %s\n",$m1,$per,
        join(' ',grep {$_ = $_.'='.$isotopes{$_}.'='.$yields->[$isotopes{$_}]} @x);
    
    $surface->add_data($m1,$logper,$yields->[$isotopes{Li7}]);

    return;
}

