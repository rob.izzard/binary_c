#!/usr/bin/env perl

use strict;
use rob_misc;
my $d = $ARGV[0] // '/tmp/';
foreach my $f (`ls $d/c_log-*.dat`) 
{
    chomp $f;
    my $x = slurp($f);
    #print $x;
    my $args = ($x=~/(--M_1.*)/)[0];
    print "ARGS $args\n";
    `tbse $args > tbse.out`;
    <STDIN>;
}
