#!/bin/bash

# return a string with a list of data objects
# required for the current build of binary_c


tmpfile=$(mktemp)

if [ -z "$tmpfile" ]; then
    echo "Could not make tmpfile in data_object_list.sh"
    exit
fi

#echo "INCDIRS is $INCDIRS" > "$tmpfile"

# default to gcc
: "${CC:=gcc};"
: "${INCDIRS:=-I.}"

# remove tmpfile on script exit
#trap 'rm -f -- "$tmpfile"' INT TERM HUP EXIT

# make defined macro list in tmpfile
#echo "in dir $PWD" >> "$tmpfile"
#echo $CC $INCDIRS -dM -E main.c >> "$tmpfile"
$CC $INCDIRS -dM -E main.c >> "$tmpfile"

# function to determine dependency
function dependency {
    MACRO=$1
    OBJECT=$2

    CAT="cat $tmpfile"
    GREP='grep define[[:space:]]\+'$MACRO'\W'

    RESULT=$($CAT | $GREP)

    if [ ! -z "$RESULT" ]; then
        OBJECT_LIST+="$OBJECT "
    fi

}

OBJECT_LIST="stellar_structure/miller_bertolami_postagb_coeffs_R.o stellar_structure/miller_bertolami_postagb_coeffs_L.o stellar_structure/miller_bertolami_postagb.o "

dependency "OBJECT_SRC_STELLAR_COLOURS_KURUCZ" "stellar_colours/Kurucz.o"
dependency "OBJECT_SRC_STELLAR_COLOURS_ELDRIDGE2015_OSTAR" "stellar_colours/eldridge2015-ostar.o"
dependency "OBJECT_SRC_STELLAR_COLOURS_ELDRIDGE2015_BASEL" "stellar_colours/eldridge2015-basel.o"
dependency "OBJECT_SRC_NUCSYN_NUCSYN_EXTENDED_S_PROCESS" "nucsyn/nucsyn_extended_s_process.o"
dependency "OBJECT_NUCSYN_NUCSYN_NOVAE_JH98_CO" "nucsyn/nucsyn_novae_JH98_CO.o"
dependency "OBJECT_NUCSYN_NUCSYN_NOVAE_JH98_ONE" "nucsyn/nucsyn_novae_JH98_ONe.o"
dependency "OBJECT_COMMON_ENVELOPE_COMMON_ENVELOPE_POLYTROPE_3D" "common_envelope/common_envelope_polytrope_3d.o"
dependency "OBJECT_OPACITY_FERGUSON_OPAL_16062017" "opacity/ferguson_opal_16062017.o"
dependency "OBJECT_OPACITY_STARS" "opacity/STARS_Z0.02.dat.o"

echo "$OBJECT_LIST"
export OBJECT_LIST
