#!/bin/bash

# script to clean a directory for release

svn rm amanda_sn_plot.pl 
svn rm lithium_tests.pl
svn rm timing_results
svn rm imfticks
svn rm test_new_derivs.pl
svn rm tbse.*
svn rm disc.plt
svn rm binary_c.orig
svn rm amanda_sn_plot.pl
svn rm todo-comenv
svn rm TODO
svn rm grid-wrap.pl

rm amanda_sn_plot.pl binary_c-antiTZ  binary_c-Ba binary_c.orig binary_c-sn2014 binary_c-sn2014.old timing_results TODO tz.objects tbse.TZmerger tbse.TZcomenv test_new_derivs.pl run_tz.pl LOG lithium_tests.pl grid-wrap.pl imfticks do_newfails.pl tbse.COCO tbse.ONeWD test_holly.pl test_holly.plt todo-comenv notes discs.plt C.pl C2.pl disc.plt diffstacks.pl test_restrict.sh valC2.sh 

rm -rf faillist fails imfticks fionread
rm ./*~ src/*~ src/*/*~
clean-svn.pl

svn rm novae*pdf
svn rm novae*plt
svn rm cemps
