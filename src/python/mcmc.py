#!/usr/bin/env python3.8

#
# Required modules: if you don't have one, run
#
# pip3.8 install <module>
#
import compress_pickle
import emcee
import json
import math
import matplotlib
import matplotlib.pyplot as plt
from multiprocessing import Pool,cpu_count
import numpy as np
import os
from pathlib import Path
import pprint
import pygtc
import re
import subprocess
import sys
import types

############################################################
#
# Script to run binary_c many times in an MCMC
# test for the best parameter set for our disc
#
# Based loosely on the emcee example at
# https://emcee.readthedocs.io/en/develop/user/line/
#
############################################################
#
# Use the first command-line argument to specify a task
#
# 'generate' which makes new data using MCMC, plots and
#            saves the resulting data in a .pkl.gz file
#            which can be loaded.
#
# 'restart' loads the existing .pkl.gz file and then restarts
#           the MCMC to add new samples to the chains (note
#           they are restarted at random positions which is
#           not quite correct!).
#
# 'load' : loads pre-existing data from a .pkl.gz file
#          and outputs plots.
#
#
# The second (optional) argument is the pickled-object
# (.pkl.gz) filename. This is mcmc.pkl.gz by default.
#
############################################################

############################################################
# get task (see above) from the command line
task = sys.argv[1] or 'load';
if len(sys.argv)>=3:
    savefile = sys.argv[2]
else:
    savefile ='mcmc.pkl.gz' # default data filename
ncpus = cpu_count()
vb = False

# say hello to the terminal
print ('binary_c-disc MCMC : task = ',task,' ncpus=',ncpus,' savefile=',savefile)

############################################################
# global matplotlib plot setup:
# use TeX for output
plt.rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
plt.rc('text', usetex=True)
plt.rcParams['text.latex.preamble']=r"\usepackage{amsmath}"

############################################################
# Observational statistics:
#
# We give these in the form of ranges, e.g.
#
# 'M_disc' : [6e-3,0.02]
#
# where 6e-3 is the lower mass limit, 0.02 is the upper mass
# limit. We then convert this to a mu+-sigma form, where
# mu is the (6e-3 + 0.02)/2 and sigma is (0.02 - 6e-3)/2.
#
# You can, naturally, use whatever you like here.
#
# note: this should be global to prevent multiple pickling
#
# you can comment out each observation you do not wish to test
#
obs = {
    'M_disc' : [6e-3,0.02],
    'J_disc' : [4.8e51,6.1e52],
    'R_out'  : [6e15,2e16],
    'T_out'  : [36,50],
    'Hout_Rout' : [2.8e15/6e15, 4e15/2e16],
    'Mdot_visc' : [0.5 * 1e-7, 1.5 * 1e-7],
    'Rin_a' : [ 1.75 , 2.25 ]
}
obs_stats = { 'mean' : {} , 'sigma' : {}, 'sigma2': {} }
for o in obs:
    obs_stats['mean'][o] = (obs[o][0]+obs[o][1])*0.5
    obs_stats['sigma'][o] = abs(obs[o][1]-obs[o][0])*0.5
    obs_stats['sigma2'][o] = (obs_stats['sigma'][o])**2

# show the results
pprint.pprint(obs_stats)

############################################################
# parameters
#
# You can choose:
#
#
# nparams = 6 : this allows you to vary loggamma and logkappa
#               as well as the four below.
#
# nparams = 4 : this fixes loggamma and logkappa,
#               and allows you to vary log fM, log fJ,
#               log alpha and log torquef.
#
nparams = 4

if nparams == 6:
    # set LaTeX labels for each parameter
    labels = [r'$\log_{10}(\alpha_\mathrm{disc})$',
              r'$\log_{10}(f_\mathrm{tid})$',
              r'$\log_{10}(\kappa)$',
              r'$\log_{10}(\gamma)$',
              r'$\log_{10}(f_\mathrm{M})$',
              r'$\log_{10}(f_\mathrm{J})$']

    # set up initial parameters in log space
    initial_parameters = [math.log10(1e-3), # log alpha
                          math.log10(1e-3), # log torquef
                          math.log10(0.01), # log kappa
                          math.log10(1.4) , # log gamma
                          math.log10(0.01), # log fM
                          math.log10(0.1)]  # log fJ
else:
    labels = [r'$\log_{10}(\alpha_\mathrm{disc})$',
              r'$\log_{10}(f_\mathrm{tid})$',
              r'$\log_{10}(f_\mathrm{M})$',
              r'$\log_{10}(f_\mathrm{J})$']

    initial_parameters = [math.log10(1e-3), # log alpha -3.457 +2.442 -2.259
                          math.log10(1e-3), # log torquef -4.005 +1.347 -1.372
                          math.log10(0.01), # log fM +1.405 -0.402
                          math.log10(0.1)]  # log fJ +0.885 -0.356
    logkappa = math.log10(0.01)
    loggamma = math.log10(1.4)

############################################################
# initial random shift scale used to initialize the MCMC
# walkers
shift_scale = 1e-3

############################################################
# Function to run a star and return the results in a
# dictionary
def run_star(parameter_list):
    global logkappa,loggamma

    # set parameters
    if(nparams == 6):
        logalpha,logtorquef,logkappa,loggamma,logfM,logfJ = parameter_list
    else:
        logalpha,logtorquef,logfM,logfJ = parameter_list
        # in this case, logkappa and loggamma are global variables
        logkappa,loggamma = globals()['logkappa'],globals()['loggamma']

    if False:
        # Run a quick made-up test rather than a real system:
        # this is useful for debugging
        test_result = {
            'M_disc' : 10**logfM * 10.0,
            'J_disc' : 10**logfJ * 1e53,
            'R_out'  : 1e16 * 10**logkappa,
            'T_out'  : 100 * 10**logalpha,
            'Hout_Rout' : 0.1 * 10**loggamma
        }
        return test_result

    # binary_c executable location
    binary_c = os.environ['HOME'] + '/progs/stars/binary_c/binary_c'

    ############################################################
    # binary_c takes command-line arguments to set its many
    # parameters. Some are fixed, some vary because of the MCMC.
    #
    # read the fixed arguments from the default_args file
    # (you could specify them here)
    fixed_args = Path('../default_args').read_text()

    # make varying disc-specific args
    vary_args = "--cbdisc_alpha {cbdisc_alpha:g} --cbdisc_torquef {cbdisc_torquef:g} --cbdisc_kappa {cbdisc_kappa:g} --cbdisc_gamma {cbdisc_gamma:g} --comenv_disc_mass_fraction {comenv_disc_mass_fraction:g} --comenv_disc_angmom_fraction {comenv_disc_angmom_fraction:g}".format(cbdisc_alpha=10.0**logalpha, cbdisc_torquef=10.0**logtorquef, cbdisc_kappa=10.0**logkappa, cbdisc_gamma=10.0**loggamma, comenv_disc_mass_fraction=10.0**logfM, comenv_disc_angmom_fraction=10.0**logfJ)

    # construct complete arg string
    args = binary_c + ' ' + fixed_args + ' ' + vary_args
    if vb:
        print("Run star with varying args ",vary_args)

    ############################################################
    # run the star, grab the results in the binary_c_output
    # string.
    binary_c_output = str(subprocess.run(args,
                                         shell=True,
                                         capture_output=True,
                                         encoding='utf-8').stdout)

    # remove "Tick" lines from the output
    p = re.compile('Tick.*')
    binary_c_output = p.sub('',binary_c_output)

    ############################################################
    # binary_c_output now contains some JSON, extract it to
    # a Python dictionary
    json_dict = {}
    try:
        # use json.loads to load from the binary_c_output string
        json_dict = json.loads(binary_c_output)
    except:
        # complain on failure, but this can happen e.g.
        # if there is no disc
        print('no json')

    # convert JSON data, stored in strings, to floats
    if json_dict and 'IRAS' in json_dict:
        results = json_dict['IRAS']

        # convert to floats
        for x in results:
            results[x] = float(results[x])

        # include lifetime
        results['lifetime'] = json_dict['IRAS final']['lifetime']

        if vb:
            print(iras)
    else:
        # no results: return empty dictionary
        results = {}

    # return results
    return results

############################################################

def log_prior(parameters):
    ############################################################
    # a function that returns the MCMC prior
    #
    # In our case, these are flat:
    # the function returns 0 in the regions where
    # the parameter is valid, and -np.inf (-infinity)
    # in regions where it is invalid.

    ############################################################
    # parameters holds 6 or 4 variables: name them
    if(nparams == 6):
        logalpha,logtorquef,logkappa,loggamma,logfM,logfJ = parameters
    else:
        logalpha,logtorquef,logfM,logfJ = parameters
        # in this case logkappa and loggamma are their global values

    #return 0 # flat priors everywhere

    ############################################################
    # note: if you want to fix a parameter, set its prior to:
    #
    # 1.0-shift_scale < parameter/initial_parameter < 1.0+shift_scale
    #
    # or just don't include it in the list of parameters

    if (
            -7.0 < logalpha < 0.0 and # -7 to 0
            -5.0 < logtorquef < -1.0 and # -5 to -1
            -4.0 < logfM < -1.0 and # -4 to -1
            -4.0 < logfJ < 0.0 and # -4 to 0
            (
                (
                    nparams == 6 and
                    -4.0 < logkappa < 4.0 and # -4 to 4
                    1.0 < 10**loggamma < 2.0 # 1 < gamma < 2
                )
                or
                nparams == 4
            )
    ):
        lprior = 0.0
    else:
        lprior = -np.inf

    return lprior
############################################################

def log_likelihood(parameters,args):
    ############################################################
    # calculate the log likelihood of the model results
    #
    # model_results is a dictionary of results for the disc

    # the result is in log_like
    log_like = 0.0

    # calculate the model results for the parameters passed in
    model_results = run_star(parameters)

    # model_results can be empty: in which case the likelihood should be
    # very negative because the model failed
    if model_results:
        for o in obs:
            # difference between model and observation
            delta = model_results[o] - obs_stats['mean'][o]

            # this is the log of the Gaussian
            dlog_like = -0.5 * (delta**2/obs_stats['sigma2'][o])

            # these terms in the likelihood are constant, so
            # are usually ignored but perhaps you want to ignore them
            # + math.log(obs_stats['sigma2'][o]) + math.log(2*math.pi))
            if vb:
                print('obs ',o,' = ',obs_stats['mean'][o],' +- ',obs_stats['sigma'][o],' : model_results ',model_results[o],', delta = ',delta/obs_stats['sigma'][o],'sigma : dlog_likelihood=',dlog_like)

            # hence add the log likelihood from this observation
            log_like = log_like + dlog_like
    else:
        # -infinity is very negative
        log_like = -np.inf

    if vb:
        print('log likelihood ',log_like)
    return log_like

############################################################

def log_probability(parameters):
    ############################################################
    # log probability

    # first, calculate the prior probability
    lprior = log_prior(parameters)

    # if the prior is not infinitely negative, add the likelihood
    if not np.isfinite(lprior):
        lprob = -np.inf
    else:
        lprob = lprior + log_likelihood(parameters,obs_stats)

    return lprob

############################################################


if task == 'generate' or task == 'restart':

    ############################################################
    # number of walkers
    #
    # should be at least 2 * nparams but also
    # we should use all cpus if possible
    # (minus 2 to leave some for accountancy/failure)
    nwalkers = max(1,nparams*2,ncpus-2)

    ############################################################
    # max number of iterations of the MC
    #
    # there will be (maxiter+1)*nwalkers calls to run_star :
    # this may take a while! but you need maxiter to be large
    # enough to create triangle confusograms.
    maxiter = 2000

    ############################################################
    # set up random initial positions for the walkers:
    # you should adjust shift_scale to set how far away these
    # are from the initial guess
    initial_guess = initial_parameters + shift_scale * np.random.randn(nwalkers,nparams)
    nwalkers,ndim = initial_guess.shape

    print('nwalkers = ',nwalkers,' ndim = ',ndim)

    ############################################################
    # Now we either run the MCMC or load pre-computed
    # data from the appropriate file and start a new MCMC.
    if(task == 'restart'):
        ############################################################
        # use existing smples loaded from the pkl.gz file
        sampler = compress_pickle.load(savefile)
        print('loaded ',len(sampler.get_chain()),' prior iterations')
        with Pool(processes=ncpus) as pool:
            sampler.pool = pool
            sampler.run_mcmc(initial_guess, maxiter, progress=True, thin_by=1);
    else:
        ############################################################
        # run new MCMC
        with Pool(ncpus) as pool:
            sampler = emcee.EnsembleSampler(nwalkers, ndim, log_probability, pool=pool)
            sampler.run_mcmc(initial_guess, maxiter, progress=True, thin_by=1);

    ############################################################
    # save metadata in the sampler object (required for
    # later loading)
    sampler.local = types.SimpleNamespace();
    setattr(sampler.local, 'ndim', ndim)
    setattr(sampler.local, 'nwalkers', nwalkers)
    setattr(sampler.local, 'nparams', nparams)
    setattr(sampler.local, 'maxiter', maxiter)

    ############################################################
    # save the data for later processing
    compress_pickle.dump(sampler,savefile)

elif task == 'load':
    ############################################################
    # load existing pickled object
    sampler = compress_pickle.load(savefile)

    # load metadata
    ndim = sampler.local.ndim
    nwalkers = sampler.local.nwalkers
    nparams = sampler.local.nparams
    print ('loaded sampler : ndim=',ndim,' nwalkers=',nwalkers,' nparams=',nparams)

else:
    ############################################################
    # panic! unknown task
    print('Task is unknown : should be "generate" or "load"')
    exit()

############################################################
# we now have either generated or loaded the data
#
############################################################
# make samples plot?
if(0):
    fig, axes = plt.subplots(ndim, figsize=(10, 7), sharex=True)
    samples = sampler.get_chain()
    for i in range(ndim):
        ax = axes[i]
        ax.plot(samples[:, :, i], "k", alpha=0.3)
        ax.set_xlim(0, len(samples))
        ax.set_ylabel(labels[i])
        ax.yaxis.set_label_coords(-0.1, 0.5)
        axes[-1].set_xlabel("step number");
        plt.savefig('mcmc1.pdf')
        plt.close()

############################################################
# discard the first 100 steps, thin to every 15 models
flat_samples = sampler.get_chain(discard=100,
                                 thin=15,
                                 flat=True)

############################################################
# results: these are the 50th percentile (mean) +- 34%
# i.e. 1D 1-sigma
for i in range(ndim):
    mcmc = np.percentile(flat_samples[:, i], [16, 50, 84])
    q = np.diff(mcmc)

    # show the result
    txt = "{3} = {0:.3f} +{1:.3f} -{2:.3f}"
    txt = txt.format(mcmc[1], q[0], q[1], labels[i])
    print(txt)


############################################################
# make corner plot of parameters
print(flat_samples.shape)

if(0):
    # do it with the corner module
    figure = corner.corner(
        flat_samples,
        labels=labels,
        color='b'
    )

# save it
#plt.savefig('mcmc2.pdf')
#plt.show()

############################################################
# Recommended: use the gtc module to make
# Giant Triangle Confusograms.
#
# If your dataset is not large, you will need
# to reduce nBins (default == 30).
gtc = pygtc.plotGTC(
    flat_samples,
    paramNames=labels,
    figureSize='MNRAS_page', # or MNRAS_column
    customTickFont={
        'family':'Helvetica Neue LT Std',
        'size':5,
        'weight':'normal',
    },
    customLabelFont={
        'family':'Helvetica Neue LT Std',
        'size':8,
        'weight':'normal',
    },
    tickShifts=(0,0),
    mathTextFontSet='stixsans',
    nContourLevels=3,
    sigmaContourLevels=True, # use 2D sigmas
    nBins=30,
    smoothingKernel=0.0,
    plotDensity=False,
    filledPlots=True,
    panelSpacing='tight',
    labelRotation=(False,False),

)

# show on screen
#plt.show()

# save to pdf
plt.savefig('mcmc3.pdf')

############################################################
# done!
