#!/usr/bin/env python3

############################################################
#
# Script to process a binary_c log file and convert
# this to a dict of useful statistics, which is then output
# as JSON.
#
#
# Run like this from the binary_c directory:
#
# tbse <args> log_filename /dev/stdout | src/python/log_to_dict.py
#
############################################################

from binary_c_log_to_dict import binary_c_log_to_dict
import fileinput
import json

# make parser object
parser = binary_c_log_to_dict()

# open file
file = fileinput.input(encoding="utf-8")

# get stats from the file
stats = parser.parse(file)

# show results
print(json.dumps(stats,indent=4))
