#!/usr/bin/env python3

usage = """

Compute MINT datafile metadata, e.g. number of lines,
skip lengths, byte offsets.


Usage:

   MINT_metadata.py <filename> [--nparameters nparameters] [options]



The filename is the first command-line argument.

The optional second argument is the number of parameters
used by the table.


Options are:

--nparameters n
    Set the number of parameters of the dataset. To do this we
    assume you know that the first n columns are used as parameters,
    rather than data to be interpolated.

--replace
    Replace the file's metadata with a corrected version.

    Note: this requires that you can generate a temporary file
    of similar size to the original file in your temp directory
    (e.g. /tmp/ on Linux). We could rewrite to use fallocate()
    but this won't work on older Linux kernels and many filesystems.

--print
    Print the complete updated file contents to stdout.

--print_metdata
    Print the newly computed metadata to stdout.

--print_header
    Print the complete new header, including metadata, to stdout.

--check
    After we're finished, load the file at the point where data starts
    (the end of the header) and show the next few lines so you can check
    the output is correct.


Computes the following and outputs them in a form
that can be put into a MINT data table:

* METADATA_NUMBER_OF_LINES <nlines>
    The number of lines of data (NOT the number of
    lines in the file).
    Note: you can check this equals:

     grep -v ^\# <datafile> | wc -l

* METADATA_START_DATA_OFFSET_BYTES <nbytes>
    This is where the data starts.
    Note: you can check this equals:

      grep ^\# <datafile> | wc -m

* METADATA_LINE_OFFSET_DIFF_BYTES <list of bytes>
    This outputs the byte numbers corresponding to each line,
    although these are actually  the differences between the
    lengths of each successive pair of lines.
    Why this instead of just the offsets? The numbers are smaller
    so the resulting file is (much) shorter. Lines tend to be
    of similar lengths, so these numbers (except the second) are
    small (positive and negative) integers.


Also, if you specify the number of parameters you will
receive:

* METADATA_PARAMETER_VALUES <n> <list>
     A list of floats which are
     the number of values each parameter, n, takes.

* METADATA_PARAMETER_NVALUES <list>
     A list of the number of values
     each parameter can take.

* METADATA_PARAMETER_SKIP <list>
     A list of the number of lines it takes
     to change each parameter.



Note that the file is assumed to have:

1) A comment section, with its lines starting with # (hash).
2) A data section.


Example output:

# METADATA_START_DATA_OFFSET_BYTES 5356407
# METADATA_LINE_OFFSET_DIFF_BYTES 0 8461 -1 2 0 ...
# METADATA_NUMBER_OF_LINES 1068450
# METADATA_PARAMETER_VALUES 0 0.13 0.18 0.23 0.28 0.33 ...
# METADATA_PARAMETER_VALUES 1 -7.253 -7.205 -7.157 ...
# METADATA_PARAMETER_VALUES 2 0 0.02 0.04 0.06 0.08 0.1 ...
# METADATA_PARAMETER_NVALUES 50 419
# METADATA_PARAMETER_SKIP 21369 51 1



Example use: here we take the file MINT_Z2.00e-02_GB.dat, give it the number of
parameters in the table (so we can generate all possible metadata), regenerate the
table's header and replace the file with the new dataset.

./src/python/MINT_metadata.py $HOME/data/MINT/test_data/Z2.00e-02/GB/MINT_Z2.00e-02_GB.dat --nparameters 3  --replace

"""
import argparse
import atexit
from enum import Enum,auto
import os
import shutil
import sys
import tempfile
import time

# handle tempfiles on exit
fwrite = None
def _onexit():
    if fwrite!=None:
        print(f"Exit prematurely: closing and deleting tmpfile at {fwrite.name}")
        fwrite.close()
        os.unlink(fwrite.name)
atexit.register(_onexit)

# set up command-line arguments and parse them
parser = argparse.ArgumentParser(
    prog="MINT metadata",
    description="Compute MINT datafile metadata, e.g. number of lines, skip lengths, byte offsets.",
    usage=usage
)
parser.add_argument('filename', # first arg is always filename
                    nargs=1,
                    help="Filename to be analysed.")
parser.add_argument('--nparameters',
                    nargs=1,
                    help="Number of parameters used in interpolation.")
parser.add_argument('--print',
                    action=argparse.BooleanOptionalAction,
                    help="Print the resulting complete new dataset (including header) to stdout. (Default is to not.)")
parser.add_argument('--print_header',
                    action=argparse.BooleanOptionalAction,
                    help="Print the new header to stdout. (Default is to not.)")
parser.add_argument('--print_metadata',
                    action=argparse.BooleanOptionalAction,
                    help="Print the new metadata to stdout. (Default is to not.)")
parser.add_argument('--replace',
                    action=argparse.BooleanOptionalAction,
                    help="Replace the datafile file with a corrected version. Handy for postprocessing existing datasets. (Default is to not.)")
parser.add_argument('--check',
                    action=argparse.BooleanOptionalAction,
                    help="Show some data to confirm that the seek offset is correct. (Default is no checks.)")
parser.add_argument('--progress',
                    action=argparse.BooleanOptionalAction,
                    help="Show progress information. (Default is to not.)")
args = parser.parse_args()

# locations in the file
class Locations(Enum):
    COMMENT = auto()
    DATA = auto()



# get options from command line arguments
filename = args.filename[0]
if args.nparameters != None:
    nparameters = int(args.nparameters[0])
else:
    nparameters = None

# set up data structures
if nparameters != None:
    skip = [0]*nparameters
    prev = [None]*nparameters
    values = {}
    for i in range(0,nparameters):
        values[i] = {}
else:
    values = ()
    nparameters = None
    skip = None
    prev = None

# metadata lines, that we should replace, start
# with these strings
metadata_headers = (
    '# METADATA_NUMBER_OF_LINES',
    '# METADATA_START_DATA_OFFSET_BYTES',
    '# METADATA_LINE_OFFSET_DIFF_BYTES',
    '# METADATA_PARAMETER_VALUES',
    '# METADATA_PARAMETER_NVALUES',
    '# METADATA_PARAMETER_SKIP'
    )

# output string
header = ''
new_header = ''

############################################################
# first (perhaps only) parse
############################################################

# assume we start in a comment
location = Locations.COMMENT
datafilesize = os.path.getsize(filename)
diff = 0
data_line_count = 0
prev_data_line_len = 0
datasize = 0
totalsize = 0
next_progress = time.time()
with open(filename) as f:
    for line in f:
        line_len = len(line)
        totalsize += line_len
        comment = line.startswith('#')

        if line_len < 2:
            sys.exit("Found empty line in table: this will break everything!")

        if comment:
            if args.progress and time.time() > next_progress:
                next_progress = time.time() + 0.1
                print(f"Read comment line",end="\r")

            # check for a line we'll want to strip
            strip = False
            for metaline in metadata_headers:
                if line.startswith(metaline):
                    strip = True
                    break
            if not strip:
                header += line

        elif not comment and \
             location == Locations.COMMENT:
            # Changed from comment section to data section
            location = Locations.DATA
            new_header += "# METADATA_LINE_OFFSET_DIFF_BYTES";

        if location == Locations.DATA:
            datasize += line_len
            if args.progress and time.time() > next_progress:
                next_progress = time.time() + 0.1
                print(f"Read data line {data_line_count} {totalsize/datafilesize*100:8.2f} %",end="\r")

            new_header += f" {diff}"
            diff = line_len - prev_data_line_len

            if nparameters != None:
                line = line.strip()
                x = line.split(None,nparameters)[0:nparameters]
                for index,value in enumerate(x):
                    # note : key x[index] is a string, not a float
                    # to precisely preserve whatever's in the file
                    values[index][x[index]] = 1

                    if prev and \
                       prev[0]!=None and \
                       skip[index]==0 and \
                       x[index]!=prev[index]:
                        skip[index] = data_line_count
                prev = x

            data_line_count += 1
            prev_data_line_len = line_len

# zero skip should be the number of lines
if nparameters != None:
    for i in range(0,nparameters):
        if skip[i] == 0:
            skip[i] = data_line_count

new_header += f"\n# METADATA_NUMBER_OF_LINES {data_line_count}\n"
if nparameters != None:
    # output parameter information
    for n in values:
        new_header += f"# METADATA_PARAMETER_VALUES {n} {' '.join(values[n].keys())}\n"
    new_header += f"# METADATA_PARAMETER_NVALUES {' '.join([str(len(values[n].keys())) for n in values])}\n"
    if skip != None:
        new_header += f"# METADATA_PARAMETER_SKIP {' '.join([str(s) for s in skip])}\n"

# Finally, compute offset bytes. We have to converge on
# this because the offset bytes is part of the offset.
prev_offset = -1
offset = -2
max_iterations = 100
offset_statement = ''
iteration = 0
start_data_offset = None
while offset != prev_offset:
    prev_offset = offset
    start_data_offset = offset
    offset_statement = f"# METADATA_START_DATA_OFFSET_BYTES {start_data_offset}\n"
    offset = len(offset_statement) + len(new_header) + len(header)
    iteration += 1
    if iteration > max_iterations:
        sys.exit("Failed to converge on METADATA_START_OFFSET_BYTES after {max_iterations} iterations.")
new_header =  offset_statement + new_header

# reconstruct complete header in appropriate order
header_end = "# END HEADER"
header_array = header.split(header_end)
header = \
    header_array[0] + \
    new_header + \
    header_array[1].strip() + \
    header_end

############################################################
# actions
############################################################

if args.print_header:
    # print the complete new header
    print(header)

if args.print_metadata:
    # print only the new metadata
    print(new_header)

if args.print:
    # print everything to stdout:
    # this is the new header
    # and the data from the file
    # (we don't store the data in memory)
    with open(filename) as f:
        print(header)
        for line in f:
            if not line.startswith('#'):
                print(line,end='')

if args.replace:
    # replace the original file with a newly-created
    # temporary file
    fread = open(filename)

    # make temporary file
    fwrite = tempfile.NamedTemporaryFile(mode='w',delete=False)
    tmpfilename = fwrite.name

    # check we have space in our temporary location
    # (moving the file back should not be a problem)
    tmpdir = os.path.dirname(tmpfilename)
    tmpfree = shutil.disk_usage(tmpdir).free
    required = datasize + len(header)
    if args.progress:
        print(f"\nSpace available in {tmpdir} : {tmpfree*1e-6:8.2f} MB\nSpace required {required*1e-6:8.2f} MB")
    if tmpfree < required:
        sys.exit(f"temporary directory has insufficient space ({tmpfree*1e-6} MB): you probably need to make {(required-tmpfree)*1e-6} MB of extra space.")

    print(header, file=fwrite)
    if args.progress:
        print(f"Writing temporary file in {tmpfilename}")

    n=0
    for line in fread:
        if not line.startswith('#'):
            print(line, file=fwrite,end='')
            if args.progress and time.time() > next_progress:
                next_progress = time.time() + 0.1
                print(f"Write tmpfile data {100.0*n/data_line_count:8.2f} %",end="\r")
        n+=1
    fread.close()
    fwrite.close()
    fwrite = None
    if args.progress:
        print(f"Moving tmpfile \"{tmpfilename}\" to \"{filename}\"")
    shutil.move(tmpfilename,
                filename)

if args.check:
    # seek to appropriate places to check the offsets are working
    f = open(filename)

    # seek to end of the header
    f.seek(start_data_offset - (len(header_end) + 1));
    maxlines = 4
    print("Header final line and first data line:")
    print(f.readline(),end='')
    print(f.readline(),end='')

    # seek to data start
    f.seek(start_data_offset);
    print(f"From data start at {start_data_offset}:")
    n = 0
    nchars = 30
    for line in f:
        if len(line) > nchars:
            print (f"Line {n:10}: {line.strip()[:nchars]} ...")
        else:
            print (f"Line {n:10}: {line}",end='')
        n += 1
        if n>maxlines:
            break
    f.close()

if args.progress:
    print("MINT_metadata finished\n");
