#!/usr/bin/env python3

############################################################
#
# Script to launch binary_c and run various unit tests
#
# The the NUM_THREADS environment variable to override
# the number of multiprocessing threads. This defaults
# to the (logical) number of cores in the system if
# NUM_THREADS is not set.
#
############################################################
import sys
from binary_c_unit_tests import binary_c_unit_tests

print("You probably want to use do_unit_tests.py, not this script")
exit()

# generic lists of masses to be tested
#
# we provide a high resolution list for fast tests (e.g. durations),
# and low resolution for slower tests (e.g. ensembles)
masses_high_res = list(range(1,8,1)) + list(range(8,20,2)) + list(range(20,80,10))
masses_low_res = [1,2,3,4,5,6,10,20,40]

# typical set of metallicities to test
metallicities = [1e-4,1e-3,0.01,0.02,0.03]

# mass-metallicity combination dictionaries
MZ_low_res_dict = {
    'metallicity' :
    (
        metallicities,
        {
            'M_1' : masses_low_res,
        }
    )
}
MZ_high_res_dict = {
    'metallicity' :
    (
        metallicities,
        {
            'M_1' : masses_high_res,
        }
    )
}

# Define the tests and their parameters.
#
# Each sub-dict should contain:
#
# type : (string)
#   The test type, used in binary_c_unit_tests()
#
# binary_c_args: (dict)
#   This defines the physics setup.
#
# The binary_c_args dict is expanded by binary_c_ensemble_manager:
# please see its documentation to understand how
# to make this.
#
# vb: (boolean, optional)
#   Turns on or off verbose logging. False by default.
#
# skip: (boolean, optional)
#   If true, skips this test.
#
tests = {
    # stellar-type phase duration tests
    'durations' : {
        'type' : 'stellar type duration',
        'binary_c_args' : MZ_high_res_dict,
    },
    # chemical yield tests
    'ensembles' : {
        'type' : 'ensemble',
        'binary_c_args' : MZ_low_res_dict,
        'ensemble filters' : ('SCALARS','TEST','CHEMICAL_YIELDS'),
    }
}

print("Binary_c unit tests")

# make a testing object
tester = binary_c_unit_tests()

# epsilon for float comparisons (default 1e-6)
tester.math_epsilon = 1e-6

# set generate to True to generate expected data:
# usually you want to set this to False (the defualt)
#tester.generate = True

# run the tests and return what run_tests() returns
sys.exit(tester.run_tests(tests))
