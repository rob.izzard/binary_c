#!/bin/bash

# get CFLAGS from binary_c
CFLAGS=`../binary_c --version 2>\&1 |grep ^CFLAGS\ is\ \" | sed -e 's/CFLAGS is "//' -e 's/" $//'`

echo "CFLAGS are ",$CFLAGS

# hence the compiler
CPP="gcc -E $CFLAGS"

python2 ./ctypesgen.py --output-language=python --cpp="$CPP" --runtime-libdir=.. --compile-libdir=.. -L.. -I..  -lbinary_c  ../binary_c_structures.h -o binary_c_structures.py
