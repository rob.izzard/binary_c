import fileinput
import re
from strip_ansi import strip_ansi
import subprocess

class binary_c_log_to_dict:
    """
    A python3 class to convert binary_c's log output to a dictionary of statistics.
    """
    def __init__(self):
        """
        Initialize binary_c_log_to_dict object.
        """
        self.stats = {}
        return

    def list_strings_to_floats(self,list_in):
        """
        convert list_in to itself with strings as floats where possible

Input:
        list_in : a list to be converted
        Output:
        list in which strings are converted to floats (if possible)
        """
        list_out = []
        for x in list_in:
            try:
                y = float(x)
            except:
                y = x
                pass
            list_out.append(y)
        return list_out

    def process(self,line,prevline):
        """
 process a line of the log file

Input:
        line : the current line
        prevline : the previous line (or None if the first)

Output:
        The current line, or None on error
        """
        if line == None or prevline == None:
            return None

        # split this line
        this = self.clean_string(line).split()

        # skip header if the first word is in skips
        # or if there's no prevline
        skips = (
            'TIME',
            'SINGLE_STAR_LIFETIME',
            'Tick',
            'use',
            'At',
            '--multiplicity'
        )
        if this[0] in skips:
            return None
        if prevline is None:
            return line

        # skip lines without enough columns
        if len(this) < 10:
            return prevline

        # split prev line
        prev = None if prevline is None else prevline.split()

        # convert strings to floats if we can
        prev = self.list_strings_to_floats(prev)
        this = self.list_strings_to_floats(this)

        # commonly use the age
        age = this[0]

        # events to be added
        events = (
            'TYPE_CHNGE',
            'COMENV',
            'BEG_SYMB','END_SYMB',
            'BEG_RCHE','END_RCHE',
        )
        if this[10] in events:
            self.addstat(('event',this[10]),
                         age)

        # SN : special case
        if this[10] == 'SN' and this[11] == 'kick':
            self.addstat(('event','SN'+this[12]),
                         age)

        # changes
        if prev is not None and prev[0] != 'TIME':
            # stellar-type changes
            offset = 3
            for i in 0,1:
                if prev[i + offset] != this[i + offset]:
                    self.addstat(('stellar type change',i),
                                 age,
                                 prev[i + offset] + ' > ' + this[i + offset],
                                 )
        return line

    def clean_string(self,string):
        """
Take a string (usually containing a log file or part of a log file) and remove ANSI and other unwanted chars and convert others to ASCII equivalents.

Input:
        string
Output:
        cleaned string
        """
        removes = ('⇑','⇓','╔','╚','║')
        replaces = (('☉','sun'),
                    ('γ','MASSLESS_REMNANT'))
        s = strip_ansi(string).rstrip('\n')
        for x in removes:
            s = s.replace(x,'')
        for x in replaces:
            s = s.replace(x[0],x[1])
        return s

    def check_binary_c(self,binary_c):
        """
Check that binary_c is built appropriately for unit testing, i.e. without optimization and with the accurate flag.

Return True if binary_c is built appropriately, False otherwise.
        """
        result = subprocess.Popen([ binary_c, 'version' ],
                                  stdout = subprocess.PIPE,
                                  stderr = subprocess.PIPE)
        #print(f"version return code {result.returncode}, pid {result.pid}")
        out, err = result.communicate()
        version = str(out,'utf-8')
        cflags = re.findall('CFLAGS.*',version,re.MULTILINE)

        return \
            '__ACCURATE_BINARY_C__ is on' in version and \
            not '-O3' in cflags and \
            not '-O2' in cflags and \
            not '-O1' in cflags

    def addstat(self,keys,*content):
        """ add statistic nested at [keys] with *content to the stats dict """
        s = self.stats
        for key in keys:
            if not key in s:
                if key == keys[-1]:
                    # final key : list
                    s[key] = []
                else:
                    # non-final key : dict
                    s[key] = {}
            s = s[key]
        s.append([*content])

    def parse_string(self,input_string):
        """ parse a string containing data """
        prevline = None
        for line in input_string.splitlines():
            line = self.clean_string(line)
            prevline = self.process(line,prevline)
        return self.stats

    def parse(self,input_file):
        """ parse a given input file object (e.g. made with fileinput), input_file, return its stats dict """
        prevline = None
        for line in input_file:
            line = self.clean_string(line)
            prevline = self.process(line,prevline)
        return self.stats
