#!/usr/bin/env python3

############################################################
#
# Ensemble manager for binary_c using binary_c-python.
#
# (c) Robert Izzard (2022+)
#
############################################################

import binarycpython
from binary_c_ensemble_manager import binary_c_ensemble_manager
import os
import pathlib
import sys

def main():
    ############################################################
    #
    # Make ensemble manager object : this sets things up,
    # reads the command line, etc.
    manager = binary_c_ensemble_manager()

    ############################################################
    #
    # Setup: this parses the command line, sets up binary_c,
    # including cluster options, from the inlist.
    #
    # Note: This calls cluster_options() which cannot know about
    #       your cluster without you entering the details into
    #       the function, but you can also set the variables in
    #       your inlist.
    #
    ############################################################
    manager.setup(cluster_options=None)

    ############################################################
    #
    # Check given command and do something...
    #
    ############################################################
    opts = manager.runtime_vars.copy()

    if manager.command == 'status':
        ############################################################
        #
        # report population status
        #
        ############################################################
        manager.status(opts)

    elif manager.command == 'longstatus':
        ############################################################
        #
        # report population status with more information
        #
        ############################################################
        manager.long_status(opts)

    elif manager.command == 'launch' or \
         manager.command == 'forcelaunch':
        ############################################################
        #
        # launch populations
        #
        ############################################################
        if os.getenv('ENSEMBLE_MANAGER_DRY_RUN'):
            dry_run = True
        else:
            dry_run = False

        if not manager.inlist:
            print("You cannot launch jobs without an inlist as the next argument. Please construct one, and pass it in the next argument to this script.")
            sys.exit(1)

        for argdict in manager.expand_args(manager.variable_binary_c_args):
            opts = opts.copy()
            opts['variable_binary_c_args'] = argdict
            manager.launch(manager.command,
                           opts,
                           dry_run=dry_run)

    elif manager.command == 'update':
        # update population database
        _loop = True
        _done = {}
        while _loop:
            _loop, _did_something, _done = manager.update_database(opts,
                                                                   done=_done)
            pass

    elif manager.command == "stop":
        ############################################################
        #
        # stop jobs
        #
        ############################################################
        try:
            if len(manager.cmdline_args)>2:
                option = manager.cmdline_args
            else:
                option = manager.cmdline_args[0]
        except:
            option = None

        if option and option in manager.job_types or option == 'all':
            # stop all running, or all, jobs
            manager.stop_all_of(opts,option)
        else:
            # stop one job
            manager.stop_jobs(opts,option)

    elif manager.command == 'delete':
        ############################################################
        #
        # delete jobs from the database
        #
        ############################################################
        try:
            if len(manager.cmdline_args)>2:
                option = manager.cmdline_args
            else:
                option = manager.cmdline_args[0]
        except:
            option = None

        if option and option in manager.job_types or option == 'all':
            # delete all running, or all, jobs from the database
            manager.delete_all_of(opts,option)
        else:
            # delete one job from the database
            manager.delete_jobs(opts,option)

    elif manager.command == 'makeplots':
        ############################################################
        #
        # goes through the finished database entries
        # and tries to make their plots
        #
        ############################################################
        manager.make_plots(opts)

    elif manager.command == 'help':
        ############################################################
        #
        # show help text
        #
        ############################################################
        manager.help()

# if we're being run as a script, run the main function
if __name__ == '__main__':
    main()
