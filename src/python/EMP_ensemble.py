#!/usr/bin/env python3.8

############################################################
#
# binary_c ensemble generator
#
############################################################

print("Deprecated : Please use EMP_ensemble-obj.py")
exit()


import binarycpython

from binarycpython import Population
from binarycpython.utils.distribution_functions import gaussian,Izzard2012_period_distribution
from binarycpython.utils.dicts import multiply_float_values

import collections
import copy
import distutils
import json
import math
import matplotlib.pyplot as plt
import numpy as np
import os
import pickle
import psutil
import shutil
from str2bool import str2bool
import sys
import time
from distutils import util
############################################################
# Make population object
population = Population()
population.parse_cmdline()


############################################################
# stellar-grid big options
#
# dists:
#    choose which stellar distributions should be used
#
#    'Moe'   Moe and di Stefano (2017)
#    '2008'  KTG93 IMF, flat-q and Duquennoy and Mayor (binaries only)
#    '2008+' as 2008 with singles and binaries
#    '2018'  as in Izzard et al 2018, i.e. Kroupa 2001 for M1, flat-q for M2,
#            and the interpolated period distribution between Raghavan and Sana (2012+)
#            (binaries only)
#    '2018+' as 2018 with single and binaries
#
dists = population.custom_options['dists'] if 'dists' in population.custom_options else '2008'

# spacing of primary mass grid: either const_dt or logM1
M1spacing = population.custom_options['M1spacing'] if 'M1spacing' in population.custom_options else 'const_dt'

# resolution parameter, so you have ~ r**3 stars
# Note: the primary mass is distributed to have constant
#       spacing in log(time), so will have however many masses
#       as this requires, not r.
r = int(population.custom_options['r']) if 'r' in population.custom_options else 100

# outdir stub: use this to uniquely ID your output directory
stub = population.custom_options['stub'] if 'stub' in population.custom_options else ''

# sampling factor < 1.0 : used in const_dt mass spacing function
# This is like a Shannon-sampling factor. 0.25 is typically ok.
fsample = population.custom_options['fsample'] if 'fsample' in population.custom_options else 0.1 # 0.25

# output directory
outdir = os.path.abspath(os.environ['BINARY_C'] + '/ensemble-EMP-' + dists + stub)

# tmp_dir in /tmp/ : should be local (non-NFS) to be fast
tmpdir = '/tmp/izzard/' + dists

# normalize data to mass_into_stars? This is usually what we
# want to do for Galactic Chemical Evolution
normalize = str2bool(population.custom_options['normalize']) if 'normalize' in population.custom_options else True

# evolve binaries in 2008/2018 distributions?
binaries = str2bool(population.custom_options['binaries']) if 'binaries' in population.custom_options else True

# IMF slope above 1Msun : -2.3 is the default (Salpeter-like)
IMFslope = float(population.custom_options['IMFslope']) if 'IMFslope' in population.custom_options else -2.3

############################################################
# Stellar physics
#
# minimum stellar mass (0.07Msun)
mmin = population.minimum_stellar_mass()
# maximum stellar mass (80.0Msun)
mmax = 6.0
# simulated primary mass range
massrange=(mmin,mmax)

############################################################
# Set population options
#
population.set(
    # Grid configuration
    max_evolution_time=15000, # 15000 Myr
    combine_ensemble_with_thread_joining=True, # always combine results in final thread
    minimum_timestep=1e-6, # 1e-6 Myr == 1 yr
    log_runtime_systems=0, # do not log systems (can be slow)
    verbosity=1, # 1

    # Ensemble output options
    ensemble=1, # 1
    ensemble_defer=1, # 1
    ensemble_logtimes=0, # 1 : use log times
    ensemble_startlogtime=10e3, # 0.1 : start at 0.1 Myr
    ensemble_dt=1e3, # time resolution (used if logtimes==0)
    ensemble_logdt=0.1, # 0.1 dex time resolution (used if logtimes==1)
    ensemble_filters_off=1, # turn off all filters except...
    ensemble_filter_EMP=1, # require EMP ensemble

    # EMP physics
    metallicity=1e-4,
    CEMP_cfe_minimum=0.7,
    EMP_logg_maximum=4.0,
    EMP_minimum_age=10e3,
    minimum_envelope_mass_for_third_dredgeup=0.0,
    delta_mcmin=-0.1,
    lambda_min=0.8,
    WRLOF_method=0,
    Bondi_Hoyle_accretion_factor=1.5,
    CRAP_parameter=0,
    no_thermohaline_mixing=0,

    run_zero_probability_system=0, # skip zero-probability systems
    data_dir=outdir, # set the result directory to be local to this directory
    tmp_dir=tmpdir, # set tmp dir
)

population.parse_cmdline() # must reparse here in case we want to override the above

# make tmp_dir
os.makedirs(population.population_options['tmp_dir'],
            exist_ok=True)

# make data_dir
os.makedirs(population.custom_options['data_dir'],
            exist_ok=True)

############################################################
# Get version info of the binary_c build, check for NUCSYN
version_info = population.return_binary_c_version_info(parsed=True)

if version_info['macros']['NUCSYN']=='off':
    print("Please enable NUCSYN")
    quit()

############################################################
# set up stellar distributions e.g. M1, M2, orbital period
#


resolution = {"M_1": r, "q" : r, "per": r}
# Primary stars
# either 'const_dt' constant-time spacing, or space M1 logarithmically
if M1spacing == 'const_dt':
    M1name='M_1'
    M1samplerfunc="const_dt(self,maxdm=(({mmin},1,-0.1),(0.8,1.5,0.05),(1,30,-0.5),(30,{mmax},1)),dlogt={dlogt},mmin={mmin},mmax={mmax},nres={nres},logspacing={logspacing},tmin={tmin},tmax={tmax},fsample={fsample},factor=1.0,showtable=False,showlist=True,logmasses=False)".format(
        dlogt=population.bse_options['ensemble_logdt'],
        mmin=mmin,
        mmax=mmax,
        nres=1000, # number of stars in the lookup table (default 1000)
        logspacing=population.bse_options['ensemble_logtimes'],
        tmin=population.bse_options['ensemble_startlogtime'], # lookup table start time (e.g. 0.1Myr)
        tmax=15000,#population.bse_options['max_evolution_time'], # lookup table end time (usually max_evolution_time)
        fsample=fsample, # set above, <1
    )
    M1precode=""
    M1probdist="three_part_powerlaw(M_1, {mmin}, 0.5, 1.0, {mmax}, -1.3, -2.3, -2.7)".format(mmin=mmin,mmax=mmax)
else:
    M1name='lnM_1'
    M1samplerfunc="const(math.log({min}), math.log({max}), {res})".format(min=massrange[0],max=massrange[1],res=resolution["M_1"])
    M1precode="M_1=max({min},min({max},math.exp(lnM_1)));".format(min=massrange[0],max=massrange[1])
    M1probdist="M_1 * three_part_powerlaw(M_1, {mmin}, 0.5, 1.0, {mmax}, -1.3, -2.3, -2.7)".format(mmin=mmin,mmax=mmax)

if dists == 'Moe':
    # Moe and di Stefano (2017) distributions
    if binaries:
        rbin = r
    else:
        rbin = 0

    resolutions = {
        "M": [r, rbin, 0, 0], # NB 0 for M1 means automatic
        "logP": [rbin, 0, 0],
        "ecc": [0, 0, 0]
    }

    moe_options = {
        "normalize_multiplicities": "merge",
        "multiplicity": 2,
        "multiplicity_modulator": [1, 1, 0, 0],
        "resolutions": resolutions, # note: mass resolution is ignored if you use const-dt as the M1 samplerfunc
        'Mmin':mmin,
        "ranges": {
            "M": [
                massrange[0],
                massrange[1]
            ],
            'logP': [ 0.0, 8.0 ]
        },
        'samplerfuncs' : {
            'M' : [-
                M1samplerfunc,
                None,
                None,
                None
                ]
            },
        # IMF slope
        'M_1' : {
            'p3' : IMFslope
            }
    }

    population.Moe_di_Stefano_2017(
        options=moe_options
    )

elif dists.startswith('2008') or dists.startswith('2018'):
    # 2008 (2009 CEMP paper) or 2018 (thick disc paper) distributions

    if binaries and dists.endswith('+'):
        # add Multiplicity
        population.add_grid_variable(
            name="multiplicity",
            parameter_name="multiplicity",
            longname="multiplicity",
            valuerange=[1, 3],
            samplerfunc="range(1, 3)",
            precode='self.population_options["multiplicity"] = multiplicity; self.bse_options["multiplicity"] = multiplicity; ',
            gridtype="discrete",
            dphasevol=-1,
            condition='',
            probdist=1,
        )

        # Use Arenou (2010) function for multiplicity
        population.set(multiplicity_fraction_function='Arenou2010')
        precode2008 = "" # not required, but must exist
    else:
        # No multiplicity: all binaries
        population.set(multiplicity_fraction_function='None')
        if binaries:
            precode2008 = 'multiplicity=2; self.population_options["multiplicity"] = 2; self.bse_options["multiplicity"] = 2;'
        else:
            precode2008 = 'multiplicity=1; self.population_options["multiplicity"] = 1; self.bse_options["multiplicity"] = 1;'

    # M1 distribution
    population.add_grid_variable(
        name=M1name,
        parameter_name="M_1",
        longname="Primary mass",
        valuerange=massrange,
        # const (log) time spacing
            samplerfunc=M1samplerfunc,
        precode=M1precode + precode2008,
        postcode="",
        # KTG93 power law
        probdist=M1probdist,
        dphasevol="dm1",
        gridtype='centred',
    )


    if binaries:
        # Mass ratio : flat q
        population.add_grid_variable(
            name="q",
            longname="Mass ratio",
            valuerange=["0.1/M_1", 1],
            samplerfunc="const({}/M_1, 1.0, {})".format(mmin,resolution['q']),
            probdist="flatsections(q, [{{ 'min': {}/M_1, 'max': 1.0, 'height': 1.0 }}])".format(mmin),
            dphasevol="dq",
            precode="M_2 = q * M_1",
            parameter_name="M_2",
            gridtype='centred',
            condition='multiplicity >= 2',
            branchpoint=1,
        )


        if dists.startswith('2008'):
            # Orbital period : Duquennoy and Mayor 1991 distribution
            population.add_grid_variable(
                name="log10per", # in days
                longname="log10(Orbital_Period)",
                condition='(self.population_options["multiplicity"] >= 2)',
                valuerange=[-1.0, 10.0],
                samplerfunc="const({}, {}, {})".format(-1.0,10.0,resolution["per"]),
                precode="""orbital_period = 10.0 ** log10per
sep = calc_sep_from_period(M_1, M_2, orbital_period)""",
                probdist="gaussian(log10per, {mean}, {sigma}, {gmin}, {gmax})".format(mean=4.8,sigma=2.3,gmin=-1.0,gmax=10.0),
                parameter_name="orbital_period",
                dphasevol="dlog10per",
                gridtype='centred',
            )
        elif dists.startswith('2018'):
            # Orbital period : interpolate between Duquennoy
            # and Mayor 1991 and Sana+ 2012 distributions
            population.add_grid_variable(
                name="log10per", # in days
                longname="log10(Orbital_Period)",
                condition='(self.population_options["multiplicity"] >= 2)',
                valuerange=[-1.0, 10.0],
                samplerfunc="const({}, {}, {})".format(-1.0,10.0,resolution["per"]),
                precode="""orbital_period = 10.0 ** log10per
sep = calc_sep_from_period(M_1, M_2, orbital_period)""",
                probdist="Izzard2012_period_distribution(orbital_period,M_1)",
                parameter_name="orbital_period",
                dphasevol="dlog10per",
                gridtype='centred',
            )


# Export settings
population.export_all_info(use_datadir=True)

# Evolve grid
population.evolve()

if 'ensemble' not in population.grid_ensemble_results:
    print("Error: no ensemble data found")
    sys.exit()

# Evolved: find the mass that went into stars
if 'metadata' in population.grid_ensemble_results:
    mass_in_stars = population.grid_ensemble_results["metadata"]["total_probability_weighted_mass"]
else:
    mass_in_stars = None

if normalize:
    if not mass_in_stars:
        # fallback!
        print("Warning : mass_in_stars was not set, defaulting to 1.0")
    else:
        # divide all numbers by this
        print("Normalize to mass_in_stars = {} Msun".format(mass_in_stars))
        for k in ['Xyield','scalars','distributions','initial distributions']:
            if k in population.grid_ensemble_results['ensemble']:
                multiply_float_values(population.grid_ensemble_results['ensemble'][k],
                                      1.0/mass_in_stars)



# Make final output filename
# JSON output
outfile = os.path.join(population.custom_options['data_dir'],
                       'ensemble_output.json.bz2')
# msgpack output
#outfile = os.path.join(population.custom_options['data_dir'],
#                       'ensemble_output.msgpack')

# always convert to an absolute path
outfile = os.path.abspath(outfile)
print("outfile=",outfile)

# Write output
population.write_ensemble(outfile,
                          sort_keys=True,
                          indent=4)

# we are done!
print("ensemble has been written to ",outfile)
