#!/usr/bin/env python3

############################################################
#
# binary_c ensemble runner script
#
############################################################

import binarycpython
import collections
import copy
from datetime import datetime
import distutils
import json
import math
import numpy as np
import os
import pickle
import psutil
import shutil
import socket
from str2bool import str2bool
import sys
import time
from binarycpython import Population
from binarycpython.utils.dicts import multiply_float_values
from distutils import util
from binary_c_ensemble_manager import is_number,quoted_argstring
tstart = datetime.now()

############################################################
# Show args for later rerun
print("-"*60 + "\n" + quoted_argstring()  + "\n" + "-"*60)

############################################################
# Make population object
population = Population()
population.parse_cmdline()

############################################################
# stellar-grid big options
#
# dists:
#    choose which stellar distributions should be used
#
#    'Moe'   Moe and di Stefano (2017)
#    '2008'  KTG93 IMF, flat-q and Duquennoy and Mayor (binaries only)
#    '2008+' as 2008 with singles and binaries
#    '2018'  as in Izzard et al 2018, i.e. Kroupa 2001 for M1, flat-q for M2,
#            and the interpolated period distribution between Raghavan and Sana (2012+)
#            (binaries only)
#    '2018+' as 2018 with single and binaries
#

defaults = {
    # uuid is the hex ID of the job, only if run using run_populations.py
    'uuid' : None,
    # default to Moe's initial distributions
    'dists' : 'Moe',
    # spacing of primary mass grid: either const_dt or logM1
    'M1spacing' : 'logM1',
    # resolution parameter, so you have ~ r**3 stars
    # Note: the primary mass is distributed to have constant
    #       spacing in log(time), so will have however many masses
    #       as this requires, not r.
    'r' : 10, # global resolution
    'r1' : None,
    'r2' : None,
    'rP' : None,

    # no eccentricity distribution usually
    'recc' : 0,
    # outdir stub: use this to uniquely ID your output directory
    # if outdir isn't set manually
    'stub' : '',
    # sampling factor < 1.0 : used in const_dt mass spacing function
    # This is like a Shannon-sampling factor. 0.25 is typically ok.
    'fsample' : 0.25,
    # output directory for this ensemble job
    'outdir' : '/tmp/',
    # tmp_dir in /tmp/ : should be local (non-NFS) to be fast
    'tmp_dir' : '/tmp/',
    # nfs checks : do we want these?
    'nfschecks' : False,
    # normalize data to mass_into_stars? This is usually what we
    # want to do for Galactic Chemical Evolution
    'normalize' : "True",
    # evolve binaries in 2008/2018 distributions?
    'binaries' : "True",
    # maximum stellar mass (80.0Msun)
    'mmax' : 80.0,
    # minimum stellar mass (0.07Msun)
    'mmin' : population.minimum_stellar_mass()
    }

# override the defaults with custom_options, if given,
# otherwise exec the statements to set them
for k in defaults:
    if k in population.custom_options:
        x = population.custom_options[k]
    else:
        x = defaults[k]
    if not is_number(x) and x is not None:
        x = '"' + str(x) + '"'
    s = str(k) + " = " + str(x)
    print(f"EXEC: {s}")
    exec(s)

def filename(i):
    return f"/tmp/transients.{i}.dat"

def transient_parser (population,lines):
    # parse for transients output and write to a file
    if not 'transients file' in population.caches:
        population.caches['transients file'] = \
            open(filename(population.process_ID),'w')

    if lines:
        for line in lines.splitlines():
            if line.startswith('TRANSIENT'):
                print(line.lstrip('TRANSIENT '),
                      file=population.caches['transients file'])
        population.caches['transients file'].flush()

# force numeric types
fsample = float(fsample)
mmax = float(mmax)
mmin = float(mmin)

# force Boolean types
binaries = binaries if isinstance(binaries,bool) else str2bool(binaries)

# make sure outdir exists
os.makedirs(outdir,exist_ok=True)

# cache directory
cache_dir = os.path.abspath('/tmp/ri0005/binary_c_cache')
if nfschecks and population.NFSpath(cache_dir):
    print("ERROR: cache_dir at {} is mounted on NFS: this will break".format(cache_dir))
    population.exit(1)
# make the cache dir... hopefully this works!
os.makedirs(cache_dir,exist_ok=True)

# check tmp_dir is not NFS
if nfschecks and population.NFSpath(tmp_dir):
    print("ERROR: tmp_dir at {} is mounted on NFS: this will be slow and probably break".format(tmp_dir))
    population.exit(1)


############################################################
# Set population options
#

population.set(
    # Grid configuration
    max_evolution_time=15000, # 15000 Myr
    combine_ensemble_with_thread_joining=True, # always combine results in final thread
    minimum_timestep=1e-6, # 1e-6 Myr == 1 yr
    log_runtime_systems=0, # do not log systems (can be slow)
    verbosity=1, # 1
    num_cores=4,

    ############################################################
    # Ensemble output options
    log_transients_2024 = True,

    # COMENV_BSE : alpha prescription based on Hurley+ 2002
    # COMENV_NELEMANS_TOUT : gamma prescription of Nelemans + Tout 2005
    # COMENV_SCATTER : Scatter algorithm
    comenv_prescription='COMENV_BSE',

    # alpha, usually 0 < alpha < 1
    alpha_ce=1.0,

    # lambda prescription, either a number or algorithm
    #   if >0 (e.g. 0.5) then use the given value
    #   if LAMBDA_CE_DEWI_TAURIS = -1 then uses functional fit to Dewi and Tauris
    #   if LAMBDA_CE_WANG_2016 = -2 then use the Wang, Jia and Li (2016; RAA 2016 Vol16 No8, 126) formalism (experimental)
    #   if LAMBDA_CE_POLYTROPE = -3 EXPERIMENTAL! probably doesn't work
    #                               then uses polytropic structure with COMENV_SPLITMASS below
    #   if LAMBDA_CE_KLENCKI2020 = -4 uses Klencki+ 2020 (A&A 2020) fits if M>10, otherwise DEWI_TAURIS
    lambda_ce='LAMBDA_CE_KLENCKI_2020',

    # fraction of ionisation energy to use
    # (Works with Dewi+Tauris, Wang algorithms only)
    lambda_ionisation=0.1,

    # Scatter parameters
    # (work with Scatter algorithm only)
    comenv_Scatter_eta=1.0,
    comenv_Scatter_dim=3.0,

    # post-comenv spins
    #
    # Merged stars:
    # COMENV_MERGER_SPIN_METHOD_SPECIFIC = 0 = preserve specific angular momentum
    # COMENV_MERGER_SPIN_METHOD_CONSERVE_ANGMOM = 1 = preserve angular momentum
    # COMENV_MERGER_SPIN_METHOD_CONSERVE_OMEGA = 2 = preserve angular velocity
    # COMENV_MERGER_SPIN_METHOD_BREAKUP = 3 = leave the merged star at its breakup
    comenv_merger_spin_method='COMENV_MERGER_SPIN_METHOD_SPECIFIC',

    # Ejection spins:
    # COMENV_EJECTION_SPIN_METHOD_DO_NOTHING = leave spins as they were on comenv entry
    # COMENV_EJECTION_SPIN_METHOD_SYNCHRONIZE = synchronize to the orbit on comenv exit
    # COMENV_EJECTION_SPIN_METHOD_SYNCHRONIZE_IF_POSSIBLE = synchronize to the orbit up to breakup
    # COMENV_EJECTION_SPIN_METHOD_BREAKUP_DONOR = donor should have its breakup velocity
    # COMENV_EJECTION_SPIN_METHOD_BREAKUP_ACCRETOR = accretor should have its breakup velocity
    # COMENV_EJECTION_SPIN_METHOD_BREAKUP_BOTH = both stars should have their breakup velocity
    comenv_ejection_spin_method='COMENV_EJECTION_SPIN_METHOD_DO_NOTHING',

    # post-comenv eccentricity
    comenv_post_eccentricity=1e-5,

    # set envelopes on post-CE objects?
    post_ce_objects_have_envelopes=False,
    post_ce_adaptive_menv=False,

    # set parsing function
    parse_function=transient_parser,

    # setup for ensemble to get const_dt M1 spacing right
    ensemble_dt = 1000,
    ensemble_logdt = 0.1,
    ensemble_logtimes = False,
    ensemble_startlogtime = 0.1,

    ############################################################

    run_zero_probability_system=False, # skip zero-probability systems

    tmp_dir=tmp_dir, # set tmp dir
    cache_dir=cache_dir, # set cache directory
    status_dir=os.path.join(outdir, 'status'), # save status in the same place
    data_dir=outdir, # seems to be required
)

population.parse_cmdline() # must reparse here in case we want to override the above

# make directories
dirs = ['tmp_dir','cache_dir','status_dir']
for x in ['slurm','condor']:
    if x in population.population_options and population.population_options[x] == 1:
        dirs += [x + '_dir']

for d in dirs:
    try:
        os.makedirs(population.population_options[d],
                    exist_ok=True)
    except:
        print("Could not make",d,"at",population.population_options[d])
        exit()

############################################################
# Get version info of the binary_c build, check for NUCSYN
version_info = population.return_binary_c_version_info(parsed=True)

#if version_info['macros']['NUCSYN']=='off':
#    print("Please enable NUCSYN")
#    quit()

############################################################
# set up stellar distributions e.g. M1, M2, orbital period
#

if binaries:
    rbin = int(r) if r is not None else 0
    try:
        r1 = int(r1) if r1 is not None else rbin
    except:
        r1 = rbin
    try:
        r2 = int(r2) if r2 is not None else rbin
    except:
        r2 = rbin
    try:
        rP = int(rP) if rP is not None else rbin
    except:
        rP = rbin
    try:
        recc = int(recc)
    except:
        recc = 0
else:
    rbin = r2 = rP = recc = 0
    try:
        r = int(r)
    except:
        pass
    try:
        r1 = int(r1)
    except:
        pass


if r is None and r1 is not None:
    r = int(r1)
elif r is not None and r1 is None:
    r1 = int(r)

resolution = {
    'M_1': r1,
    'q' : r2,
    'per': rP,
    'ecc' : recc
}
print(resolution)

Moe_resolutions = {
    "M": [r1, r2, 0, 0], # NB 0 for M1 means automatic
    "logP": [rP, 0, 0],
    "ecc": [recc, 0, 0] # 0 means no eccentricity distribution
}

# Primary stars
#
# Initial mass function : KTG93
IMF_KTG93="self.three_part_powerlaw(M_1, {mmin}, 0.5, 1.0, {mmax}, -1.3, -2.3, {slope})".format(
    mmin=mmin,
    mmax=mmax,
    slope=-2.7,
)

# spacing function is either 'const_dt' constant-time spacing,
# or space M1 logarithmically
if M1spacing == 'const_dt':
    try:
        mass_spacings = const_dt_mass_spacings.format(
            mmin=mmin,
            mmax=mmax
        )
    except:
        # default mass spacing
        mass_spacings = "(({mmin},1,-0.1),(0.8,1.5,0.05),(1,30,-0.5),(30,{mmax},1))".format(
            mmin=mmin,
            mmax=mmax
        )

    M1samplerfunc="self.const_dt(maxdm={mass_spacings},dlogt={dlogt},mmin={mmin},mmax={mmax},nres={nres},logspacing={logspacing},tmin={tmin},tmax={tmax},max_evolution_time={max_evolution_time},fsample={fsample},factor={factor},showtable=False,showlist=True,logmasses=True,cachedir=\"{cachedir}\",vb=False)".format(
        mass_spacings=mass_spacings,
        mmin=mmin,
        mmax=mmax,
        nres=100, # number of stars in the lookup table (default 1000)
        dt=population.bse_options['ensemble_dt'],
        dlogt=population.bse_options['ensemble_logdt'],
        logspacing=population.bse_options['ensemble_logtimes'],
        tmin=population.bse_options['ensemble_startlogtime'], # lookup table start time (e.g. 0.1Myr)
        tmax=population.bse_options['max_evolution_time'], # lookup table end time (usually max_evolution_time)
        fsample=fsample, # set above, <1
        cachedir=population.population_options['cache_dir'],
        max_evolution_time=population.bse_options['max_evolution_time'],
        factor=1.0,
    )
else:
    # logarithmically-spaced, so our grid variable is lnM_1
    M1samplerfunc="self.const_linear(math.log({min}), math.log({max}), {res})".format(
        min=mmin,
        max=mmax,
        res=resolution["M_1"])



# always log-spaced M1
M1name='lnM_1'
M1precode="M_1=max({min},min({max},math.exp(lnM_1)));".format(min=mmin,max=mmax)
# always use the KTG93 IMF, and all the above use lnM1 so
# we need to multiply by M_1
M1probdist="M_1 * " + IMF_KTG93


if dists == 'Moe':
    # Moe and di Stefano (2017) distributions

    moe_options = {
        "normalize_multiplicities": "merge",
        "multiplicity": 2 if binaries else 1,
        "multiplicity_modulator": [1, 1 if binaries else 0, 0, 0],
        "resolutions": Moe_resolutions, # note: M1 resolution is ignored if you use const-dt as the M1 samplerfunc
        'Mmin':mmin,
        "ranges": {
            "M": [
                mmin,
                mmax
            ],
            'logP': [ 0.0, 8.0 ]
        },
        'samplerfuncs' : {
            'M' : [
                M1samplerfunc,
                None,
                None,
                None
                ]
            },
        '_update' : True
    }

    population.Moe_di_Stefano_2017(
        options=moe_options
    )


elif dists.startswith('2008') or dists.startswith('2018'):
    # 2008 (2009 CEMP paper) or 2018 (thick disc paper) distributions

    if binaries and dists.endswith('+'):
        # mixed population (not in 2008, but 2008+ adds this)
        # -> we need to add the multiplicity
        population.add_grid_variable(
            name="multiplicity",
            parameter_name="multiplicity",
            longname="multiplicity",
            valuerange=[1, 3],
            samplerfunc="range(1, 3)",
            precode='self.population_options["multiplicity"] = multiplicity; self.bse_options["multiplicity"] = multiplicity; ',
            gridtype="discrete",
            dphasevol=-1,
            condition='',
            probdist=1
        )
        precode2008 = "binfrac = self.Arenou2010_binary_fraction(M_1)"
    else:
        # No multiplicity
        if binaries:
            # all binaries
            precode2008 = 'binfrac = 1.0; multiplicity=2; self.population_options["multiplicity"] = 2; self.bse_options["multiplicity"] = 2;'
        else:
            # all single stars
            precode2008 = 'binfrac = 0.0; multiplicity=1; self.population_options["multiplicity"] = 1; self.bse_options["multiplicity"] = 1;'


    # M1 distribution
    population.add_grid_variable(
        name=M1name,
        parameter_name="M_1",
        longname="Primary mass",
        valuerange=massrange,
        samplerfunc=M1samplerfunc,
        precode = M1precode + precode2008,
        postcode="",
        # KTG93 power law * binary fraction
        probdist=M1probdist + ' * (binfrac if multiplicity==2 else (1.0 - binfrac))',
        dphasevol="dm1",
        gridtype='centred',
    )

    if binaries:
        # Mass ratio : flat q
        population.add_grid_variable(
            name="q",
            longname="Mass ratio",
            valuerange=["0.1/M_1", 1],
            samplerfunc="self.const_linear({}/M_1, 1.0, {})".format(mmin,resolution['q']),
            probdist="self.flatsections(q, [{{ 'min': {}/M_1, 'max': 1.0, 'height': 1.0 }}])".format(mmin),
            dphasevol="dq",
            precode="M_2 = q * M_1",
            parameter_name="M_2",
            gridtype='centred',
            condition='multiplicity >= 2',
            branchpoint=1,
        )


        if dists.startswith('2008'):
            # Orbital period : Duquennoy and Mayor 1991 distribution
            population.add_grid_variable(
                name="log10per", # in days
                longname="log10(Orbital_Period)",
                condition='(self.population_options["multiplicity"] >= 2)',
                valuerange=[-1.0, 10.0],
                samplerfunc="self.const_linear({}, {}, {})".format(-1.0,10.0,resolution["per"]),
                precode="""orbital_period = 10.0 ** log10per
sep = calc_sep_from_period(M_1, M_2, orbital_period)""",
                probdist="self.gaussian(log10per, {mean}, {sigma}, {gmin}, {gmax})".format(mean=4.8,sigma=2.3,gmin=-1.0,gmax=10.0),
                parameter_name="orbital_period",
                dphasevol="dlog10per",
                gridtype='centred',
            )
        elif dists.startswith('2018'):
            # Orbital period : interpolate between Duquennoy
            # and Mayor 1991 and Sana+ 2012 distributions
            population.add_grid_variable(
                name="log10per", # in days
                longname="log10(Orbital_Period)",
                condition='(self.population_options["multiplicity"] >= 2)',
                valuerange=[-1.0, 10.0],
                samplerfunc="self.const_linear({}, {}, {})".format(-1.0,10.0,resolution["per"]),
                precode="""orbital_period = 10.0 ** log10per
sep = calc_sep_from_period(M_1, M_2, orbital_period)""",
                probdist="self.Izzard2012_period_distribution(orbital_period,M_1)",
                parameter_name="orbital_period",
                dphasevol="dlog10per",
                gridtype='centred',
            )


# Export settings
if not population.HPC_job() or population.HPC_jobID_tuple()[1] == 0:
    population.export_all_info(use_datadir=True)

#print(f"Moe opts {moe_options}")

# Evolve grid
population.evolve()

if 'ensemble' not in population.grid_ensemble_results:
    print("Error? no ensemble data found")
    sys.exit()

# Evolved: find the mass that went into stars
if 'metadata' in population.grid_ensemble_results:
    try:
        mass_in_stars = population.grid_ensemble_results["metadata"]["total_probability_weighted_mass"]
    except:
        mass_in_stars = None
        pass
else:
    mass_in_stars = None



# Join output files
with open('/tmp/transients.dat','w') as outfile:

    # write some information about this simulation
    print(f"############################################################\n#\n# Transients 2024 project\n#\n############################################################\n#\n# Mass into stars {mass_in_stars}\n#\n############################################################\n# Parameters that are not as defaults:\n#",file=outfile)
    default_pop = Population()
    for parameter in population.bse_options:
        changed = parameter not in default_pop.bse_options or \
            population.bse_options[parameter] != default_pop.bse_options[parameter]
        if changed:
            print(f"# {parameter} = {population.bse_options[parameter]}",
                  file=outfile)

    print("#\n############################################################\n#",file=outfile)
    matches = ['git','Version','Build','BINARY_C_PYTHON_VERSION','BINARY_C_VERSION','__VERSION__','CFLAGS is \"','COMPILER','__GNUC__','__clang__']
    versioninfo = Population().return_binary_c_version_info(parsed=True)
    for key1 in versioninfo:
        if isinstance(versioninfo[key1],dict):
            for key2 in versioninfo[key1]:
                s2 = str(key2)
                if any(s.lower() in s2.lower() for s in matches):
                    print(f"# {key1}.{key2} {versioninfo[key1][key2]}",file=outfile)
        elif any(s.lower() in str(key1).lower() for s in matches):
            print(f"# {key1} {versioninfo[key1]}",file=outfile)

    print(f"#\n############################################################\n#\n# Simulation:\n# Run on {socket.gethostname()}\n# Started at {tstart}\n# Finished at {datetime.now()}\n# Resolutions: {resolution}\n#\n# dists : {dists}\n# mmin :  {mmin}\n# mmax : {mmax}\n# M1 spacing : {M1spacing}\n#\n############################################################\n",file=outfile)

    # join the files from each thread
    for i in range(0,population.population_options['num_cores']):
        with open(filename(i),'r') as infile:
            outfile.write(infile.read())

print("See /tmp/transients.dat")
