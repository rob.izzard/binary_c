#!/usr/bin/env python3

############################################################
# script to load, process and plot transients data
############################################################

import binarycpython
import copy
import json
import math
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
from matplotlib.backends.backend_pdf import PdfPages
import pandas as pd
import seaborn as sns
plt.ioff()
from binarycpython.utils.dicts import (
    AutoVivificationDict
)
import numpy
import sys

testing = False

if testing:
    import matplotlib.style as mplstyle
    mplstyle.use('fast')

# stellar type data
nst = 13 # number of stellar types (13) : we plot up to nst
stellar_types = range(nst+1)
stellar_type_strings = ('LMMS',
                        'MS',
                        'HG',
                        'GB',
                        'CHeB',
                        'EAGB',
                        'TPAGB',
                        'HeMS',
                        'HeHG',
                        'HeGB',
                        'HeWD',
                        'COWD',
                        'ONeWD',
                        'NS',
                        'BH',
                        'massless')
no_nst_above = 16 # 16 : set to lower to limit the stellar type to < this number

if testing:
    stellar_types1 = [0,1,2] # stellar_types
    stellar_types2 = [0,1,2] # stellar_types
else:
    stellar_types1 = stellar_types
    stellar_types2 = stellar_types
stellar_types1 = [0,1,2,3,4,5,6] # stellar_types
stellar_types2 = [0,1,2,3,4,5,6] # stellar_types

def gridtickfontsize():
    x = max(len(stellar_types1),len(stellar_types2))
    s = int((15.056)+(-2.1538)*x+(0.097436)*x*x)
    return s

def rightoffset():
    return max(0.0,min(0.1,0.002 * gridtickfontsize()))

# data setup
logz = True
logzmin = 1e-30
nan = float('nan') # empty data value

# set up output PDF files and formatting
pdfmargins = { 'x' : 0, 'y' : 0 }
pdffiles = {
    'grid' : {
        'figsize' : (11.7, 8.27), # A4
        'dpi' : 300, # resolution
        'file':'transients-grid.pdf',
        'renew_gridspec' : False,
        'gridspec_options' : {
            'top': 0.95,
            'bottom': 0.08,
            'right' : (0.9 - rightoffset()),
            'left' : 0.07,
            'hspace' : 0,
            'wspace' : 0,
            'ncols' : len(stellar_types1),
            'nrows' : len(stellar_types2),
        },
        'gridspec_subplots_options' : {
            'sharex' : 'col',
            'sharey' : 'row'
        },
        'label' : {
            'fontdict' : {
                'horizontalalignment' : 'left',
                'fontsize' : 6,
                'color' : 'red',
                'fontweight' : 'bold',
            },
            'location' : (0.2,0.65)
        },
        'tickfontsize' : gridtickfontsize(),
        'maxnticks' : 6,
        'cbfontsize' : 12,
        'datarange' : {
            'x' : None, # or tuple (xmin,xmax)
            'y' : None, # or tuple (ymin,ymax)
            },
    },
    'panels' : {
        'file':'transients-panels.pdf',
        'figsize' : (11.7, 8.27), # A4
        'dpi' : 300, # resolution
        'renew_gridspec' : True,
        'gridspec_options' : {
            'top': 0.95,
            'bottom': 0.15,
            'right' : 0.88,
            'left' : 0.12,
            'hspace' : 0,
            'wspace' : 0,
            'ncols' : 1,
            'nrows' : 1
        },
        'gridspec_subplots_options' : {
            'sharex' : 'col',
            'sharey' : 'row'
        },
        'label' : {
            'fontdict' : {
                'horizontalalignment' : 'left',
                'fontsize' : 16,
                'color' : 'red',
                'fontweight' : 'bold',
            },
            'location' : (0.2,0.65)
        },
        'tickfontsize' : 13,
        'maxnticks' : 6,
        'cbfontsize' : 12,
        'datarange' : {
            'x' : None, # or tuple (xmin,xmax)
            'y' : None, # or tuple (ymin,ymax)
            },
    }
}

## only plot...
#x = pdffiles['panels']
#pdffiles = {}
#pdffiles['panels'] = x

pdf = {}
for pdffile in pdffiles:
    pdf[pdffile] = {}
    pdf[pdffile]['pdfpages'] = PdfPages(pdffiles[pdffile]['file'])
    pdf[pdffile]['subplots'] = None

# plot setup
colourmap = 'cubehelix_r'
cbar_location = (0.91 - rightoffset(), 0.3, 0.03, 0.4)
cbar_label = 'Number of stars'
if logz:
    cbar_label = f'log10({cbar_label})'

# load transients ensemble data
transients = binarycpython.utils.ensemble.load_ensemble('ensemble_output.json.bz2')['ensemble']['transients']

# set x,y axis names (these should match the ensemble dict's keys)
xaxis = 'log timescale / d'
yaxis = 'log luminosity / Lsun'

# get available times
times = list(t for t in transients.keys() if t != 'all times')
print("Times:",', '.join(map(str,times)))

# find the parameter names and their values
params = {}
for t in times:
    for k in transients[t].keys():
        params[k]=1
print(f"Parameters {params.keys()}")
parameters = list(params.keys())
parameter_values = {}
for p in parameters:
    parameter_values[p] = {}
    for t in times:
        for value in transients[t][p]:
            if value != 'merged':
                parameter_values[p][value] = 1
    parameter_values[p] = list(parameter_values[p].keys())
    if testing:
        # limit for testing
        parameter_values[p] = [parameter_values[p][0]]

for p in parameters:
    print(f"{p} values: {parameter_values[p]}")

############################################################
# Functions
############################################################

def get_ensemble_data(parameter_name,
                      parameter_value,
                      stellar_type_primary,
                      stellar_type_secondary):

    if stellar_type_primary > no_nst_above or \
       stellar_type_secondary > no_nst_above:
        return None # for testing

    try:
        data = transients['all times'][parameter_name][parameter_value]['stellar type primary'][stellar_type_primary]['stellar type secondary'][stellar_type_secondary]
    except:
        data = None
    return data

def get_plotdata(data=None,
                 stellar_type1='all',
                 stellar_type2='all',
                 x=None,
                 y=None,
                 pad=True,
                 xypoints=None,
                 ticks=None):
    # return plot data for given parameter and stellar types
    # in nested dict: data[x][y] = z
    #
    # if pad==True, we make sure this dict has values
    # everywhere, with nan being set when there's no data
    if data:
        # convert to 2D dict
        plotdata = AutoVivificationDict()

        for x in data[xaxis].keys():
            for y in data[xaxis][x][yaxis]:
                z = data[xaxis][x][yaxis][y]
                # enter into plotdata, and clean up format
                plotdata[float(f"{x:g}")][float(f"{y:g}")] += z

        # log if required
        if logz:
            for x in plotdata:
                for y in plotdata[x]:
                    plotdata[x][y] = math.log10(plotdata[x][y])

        if pad:
            plotdata = pad_data(data=plotdata,
                                xypoints=xypoints,
                                xticks=ticks['x'][stellar_type1],
                                yticks=ticks['y'][stellar_type2])

        return plotdata
    else:
        return None


def get_datastats(data,
                  datastats = None):

    # given data[xaxis][yaxis] = z extract (xmin,xmax),(ymin,ymax),(zmin,zmax)

    if datastats is not None:
        xrange = datastats['x']
        yrange = datastats['y']
        zrange = datastats['z']
    else:
        xrange = [None,None]
        yrange = [None,None]
        zrange = [None,None]

    ranges = {
        'x':xrange,
        'y':yrange,
        'z':zrange
    }

    if data is None:
        return ranges

    for x in data.keys():
        if not math.isnan(x):
            if xrange[0] is None:
                xrange = [x,x]
            else:
                xrange[0] = min(xrange[0],x)
                xrange[1] = max(xrange[1],x)
            for y in data[x].keys():
                if not math.isnan(y):
                    if yrange[0] is None:
                        yrange = [y,y]
                    else:
                        yrange[0] = min(yrange[0],y)
                        yrange[1] = max(yrange[1],y)
                    z = data[x][y]
                    if not math.isnan(z):
                        if zrange[0] is None:
                            zrange = [z,z]
                        else:
                            zrange[0] = min(zrange[0],z)
                            zrange[1] = max(zrange[1],z)
    return {
        'x':xrange,
        'y':yrange,
        'z':zrange
    }

def pad_data(data=None,
             xypoints=None,
             xticks=[],
             yticks=[]):
    # return a padded version of data, making
    # sure the grid of xticks * yticks is filled
    # with nans where there is no data
    newdata = {}
    for x in xticks:
        newdata[x] = {}
        for y in yticks:
            if not y in data[x]:
                newdata[x][y] = nan
            else:
                newdata[x][y] = data[x][y]

    if xypoints:
        for x,y in xypoints:
            plotdata[float(f"{x:g}")][float(f"{y:g}")] += 0

    return newdata

def reduce_ticklist(pdffile=None,
                    axis=None,
                    stellar_type=None):

    # get sorted list of tick values
    l = sorted(list(ticks[axis][stellar_type].keys()))

    # reformat all numbers in the list
    l = [float(f"{x:g}") for x in l]

    ticks[axis][stellar_type] = l
    #show_ticklabels[axis][stellar_type] = l
    #return

    if len(l)>0:
        # reduce tick list length if required
        maxnticks = pdffiles[pdffile]['maxnticks']
        if maxnticks > 2 and len(l) > maxnticks:
            modulo = max(1,int(len(l)/(maxnticks-1)))
            l = l[1::modulo]

        # set the reduced list into a dict "labels"
        labels = { x : 1 for x in l }

        # replace list l with what's in "labels"
        # or empty strings
        l = []
        for x in ticks[axis][stellar_type]:
            if x in labels:
                l.append(x)
            else:
                l.append(None)

    # convert dict to list of ticks we want to show
    show_ticklabels[axis][stellar_type] = l


def safemin(x,y):
    # safe min : if one is None, return the other
    if x == None:
        if y == None:
            print("Min of None and None")
            sys.exit(1)
        else:
            return y
    else:
        if y == None:
            return x
        else:
            return min(x,y)

def safemax(x,y):
    # safe max : if one is None, return the other
    if x == None:
        if y == None:
            print("Max of None and None")
            sys.exit(1)
        else:
            return y
    else:
        if y == None:
            return x
        else:
            return max(x,y)

def globalstats(parameter_name,
                parameter_value):
    count = 0
    bigdatastats=None
    colrange = [None] * len(stellar_types)
    rowrange = [None] * len(stellar_types)
    cbrange = [None,None]
    rowpoints = {}
    colpoints = {}

    # column (x) and row (y) ranges
    for stellar_type in stellar_types:
        colrange[stellar_type] = [None,None]
        rowrange[stellar_type] = [None,None]
        colpoints[stellar_type] = {}
        rowpoints[stellar_type] = {}

    for stellar_type1 in stellar_types1:

        for stellar_type2 in stellar_types2:
            # 1 = primary, 2 = secondary
            ensemble_data = get_ensemble_data(parameter_name,
                                              parameter_value,
                                              stellar_type1,
                                              stellar_type2)
            #bigdatastats['ypoints'][stellar_type2] = []

            if ensemble_data:
                data2d = get_plotdata(ensemble_data,
                                      pad=False, # can't pad until we have set up ticks
                                      stellar_type1=stellar_type1,
                                      stellar_type2=stellar_type2)
                xypoints = {}
                if data2d:
                    # add to tick lists and x,y grid list
                    for x in data2d:
                        ticks['x'][stellar_type1][x] = 1
                        colpoints[stellar_type1][x] = 1
                        colrange[stellar_type1][0] = safemin(colrange[stellar_type1][0],x)
                        colrange[stellar_type1][1] = safemax(colrange[stellar_type1][1],x)

                        for y in data2d[x]:
                            colrange[stellar_type2][0] = safemin(colrange[stellar_type2][0],y)
                            colrange[stellar_type2][1] = safemax(colrange[stellar_type2][1],y)
                            ticks['y'][stellar_type2][y] = 1
                            rowpoints[stellar_type2][y] = 1
                            z = data2d[x][y]
                            cbrange[0] = safemin(cbrange[0],z)
                            cbrange[1] = safemax(cbrange[1],z)
                            xypoints[(x,y)]=1
                            #bigdatastats['ypoints'][stellar_type2].append(y)
                bigdatastats = get_datastats(data2d,
                                             datastats=bigdatastats)

            count+=1

    bigdatastats['colrange'] = colrange
    bigdatastats['rowrange'] = rowrange
    bigdatastats['cbrange'] = cbrange

    for _ in 'rowpoints','colpoints':
        if not _ in bigdatastats:
            bigdatastats[_] = {}

    for stellar_type in rowpoints:
        if stellar_type in rowpoints:
            bigdatastats['rowpoints'][stellar_type] = list(rowpoints[stellar_type].keys())
        else:
            bigdatastats['rowpoints'][stellar_type] = None
    for stellar_type in colpoints:
        if stellar_type in colpoints:
            bigdatastats['colpoints'][stellar_type] = list(colpoints[stellar_type].keys())
        else:
            bigdatastats['colpoints'][stellar_type] = None

    return bigdatastats,count

def range(parameter_name,
          parameter_value,
          stellar_types1,
          stellar_types2):
    # return x,y ranges of datasets with stellar types 1,2
    ranges = { 'x':[], 'y':[], 'z':[] }
    for stellar_type1 in stellar_types1:
        for stellar_type2 in stellar_types2:
            ensemble_data = get_ensemble_data(parameter_name,
                                              parameter_value,
                                              stellar_type1,
                                              stellar_type2)
            if ensemble_data:
                data2d = get_plotdata(ensemble_data,
                                      pad=False,
                                      stellar_type1=stellar_type1,
                                      stellar_type2=stellar_type2)

                if data2d:
                    for x in data2d:
                        ranges['x'][0] = min(ranges['x'][0],x)
                        ranges['x'][1] = max(ranges['x'][0],x)
                        for y in data2d[x]:
                            ranges['y'][0] = min(ranges['y'][0],y)
                            ranges['y'][1] = max(ranges['y'][0],y)
                            z = data2d[x][y]
                            ranges['z'][0] = min(ranges['z'][0],z)
                            ranges['z'][1] = max(ranges['z'][0],z)
    return ranges

def makeplot(pdffile=None,
             fig=None,
             ax=None,
             cbar_ax=None,
             make_cbar=False,
             parameter_name=None,
             parameter_value=None,
             data=None,
             xypoints=None,
             datastats=None,
             zmin=None,
             zmax=None,
             xticklabels='auto',
             yticklabels='auto',
             xrange=None,
             yrange=None,
             cbrange=None,
             x=None,
             y=None,
             stellar_type1='all',
             stellar_type2='all'):

    if not data:
        data = get_plotdata(x=xaxis,
                            y=yaxis,
                            pad=True,
                            xypoints=xypoints,
                            stellar_type1=stellar_type1,
                            stellar_type2=stellar_type2)

    if data:
        # get stats if not given
        if not datastats:
            datastats = get_datastats(data)

        # use solar units, log_10 etc. for axis labels
        if make_cbar:
            for axis in ('x','y'):
                label = eval(f"{axis}").replace('sun','$_{⊙}$').replace('log','log$_{10}$')
                exec(f"fig.sup{axis}label(label)")

        # make the plot
        dataframe = pd.DataFrame.from_dict(data)
        ax = sns.heatmap(ax = ax,
                         data = dataframe,
                         cmap = colourmap,
                         xticklabels=xticklabels,
                         yticklabels=yticklabels,
                         vmin=zmin,
                         vmax=zmax,
                         annot=False,
                         cbar_kws={
                             'label' : cbar_label,
                         },
                         cbar=make_cbar,
                         cbar_ax=cbar_ax,
                         square=False,
                         linewidths=0, # removes lines
                         rasterized=True, # removes lines
                         )

        _want_xticks = []
        _want_xticklabels = []
        for xtick,xticklabel in zip(ax.get_xticks(),xticklabels):
            if xticklabel != None:
                _want_xticks.append(xtick)
                _want_xticklabels.append(xticklabel)
        ax.set_xticks(_want_xticks)

        _want_yticks = []
        _want_yticklabels = []
        for ytick,yticklabel in zip(ax.get_yticks(),yticklabels):
            if yticklabel != None:
                _want_yticks.append(ytick)
                _want_yticklabels.append(yticklabel)
        ax.set_yticks(_want_yticks)

        # set ranges
        #print(f"SET AX LIM X {xrange}, Y {yrange}")
        #ax.set(xlim=xrange,
        #       ylim=yrange)

        # add stellar types label
        label = \
            stellar_type_strings[stellar_type1] + \
            "→\n" + \
            stellar_type_strings[stellar_type2]

        ax.text(*pdffiles[pdffile]['label']['location'],
                label,
                **pdffiles[pdffile]['label']['fontdict'],
                transform = ax.transAxes)

        # set up axes : flip y axis, show axis lines
        ax.invert_yaxis()
        for _, spine in ax.spines.items():
            spine.set_visible(True)

        return ax
    else:
        return None


def launch_plot(
        pdffile=None,
        fig=None,
        subplots=None,
        bigdatastats=None,
        parameter_name=None,
        parameter_value=None,
        stellar_type1=None,
        stellar_type2=None,
        make_cbar=False,
        ticks=None,
        show_ticklabels=None,
        cbar_ax=False,
        xrange=None,
        yrange=None,
        xypoints=None,
):
    # get axes on which we wish to plot
    #
    # if the subplots are an array, then choose the correct location
    # in it
    if isinstance(subplots,numpy.ndarray):
        ax = subplots[stellar_type2][stellar_type1]
    else:
        ax = subplots
    ax.tick_params(
        axis='x',
        which='both',
        direction='out',
        labelrotation=90,
        labelsize=pdffiles[pdffile]['tickfontsize']
    )
    ax.tick_params(
        axis='y',
        which='both',
        direction='out',
        labelrotation=0,
        labelsize=pdffiles[pdffile]['tickfontsize']
    )
    # get data
    ensemble_data = get_ensemble_data(parameter_name,
                                      parameter_value,
                                      stellar_type1,
                                      stellar_type2)

    if not ensemble_data:
        # no data found : enter dummy data for empty plot
        # with correct axis labels
        ensemble_data = {
            xaxis : {
                0 : {
                    yaxis : {
                        0 : nan
                    }
                }
            }
        }

    if ensemble_data:
        # we have data, format for seaborn
        data = get_plotdata(data=ensemble_data,
                            x=xaxis,
                            y=yaxis,
                            pad=True,
                            stellar_type1=stellar_type1,
                            stellar_type2=stellar_type2,
                            ticks=ticks)

        ax = makeplot(pdffile=pdffile,
                      fig=fig,
                      ax=ax,
                      cbar_ax=cbar_ax,
                      make_cbar=make_cbar,
                      parameter_name=parameter_name,
                      parameter_value=parameter_value,
                      data=data,
                      xypoints=xypoints,
                      zmin=bigdatastats['z'][0],
                      zmax=bigdatastats['z'][1],
                      xrange = xrange,
                      yrange = yrange,
                      x = xaxis,
                      y = yaxis,
                      xticklabels = show_ticklabels['x'][stellar_type1],
                      yticklabels = show_ticklabels['y'][stellar_type2],
                      stellar_type1=stellar_type1,
                      stellar_type2=stellar_type2)

        if ax:
            ax.margins(tight=True,
                       x=pdfmargins['x'],
                       y=pdfmargins['y'])

            # set tick font size and rotation
            ax.tick_params(
                axis='x',
                which='both',
                direction='out',
                labelrotation=90,
                labelsize=pdffiles[pdffile]['tickfontsize']
            )
            ax.tick_params(
                axis='y',
                which='both',
                direction='out',
                labelrotation=0,
                labelsize=pdffiles[pdffile]['tickfontsize']
            )

    return ax

def launch_plot_wrap(dict):
    return launch_plot(**dict)

def format_name(name):
    # https://gist.github.com/beniwohli/765262
    greek_alphabet = {
        u'\u0391': 'Alpha',
        u'\u0392': 'Beta',
        u'\u0393': 'Gamma',
        u'\u0394': 'Delta',
        u'\u0395': 'Epsilon',
        u'\u0396': 'Zeta',
        u'\u0397': 'Eta',
        u'\u0398': 'Theta',
        u'\u0399': 'Iota',
        u'\u039A': 'Kappa',
        u'\u039B': 'Lamda',
        u'\u039C': 'Mu',
        u'\u039D': 'Nu',
        u'\u039E': 'Xi',
        u'\u039F': 'Omicron',
        u'\u03A0': 'Pi',
        u'\u03A1': 'Rho',
        u'\u03A3': 'Sigma',
        u'\u03A4': 'Tau',
        u'\u03A5': 'Upsilon',
        u'\u03A6': 'Phi',
        u'\u03A7': 'Chi',
        u'\u03A8': 'Psi',
        u'\u03A9': 'Omega',
        u'\u03B1': 'alpha',
        u'\u03B2': 'beta',
        u'\u03B3': 'gamma',
        u'\u03B4': 'delta',
        u'\u03B5': 'epsilon',
        u'\u03B6': 'zeta',
        u'\u03B7': 'eta',
        u'\u03B8': 'theta',
        u'\u03B9': 'iota',
        u'\u03BA': 'kappa',
        u'\u03BB': 'lamda',
        u'\u03BC': 'mu',
        u'\u03BD': 'nu',
        u'\u03BE': 'xi',
        u'\u03BF': 'omicron',
        u'\u03C0': 'pi',
        u'\u03C1': 'rho',
        u'\u03C3': 'sigma',
        u'\u03C4': 'tau',
        u'\u03C5': 'upsilon',
        u'\u03C6': 'phi',
        u'\u03C7': 'chi',
        u'\u03C8': 'psi',
        u'\u03C9': 'omega',
    }
    greek_alphabet_inv = {v: k for k, v in greek_alphabet.items()}
    for ascii,unicode in greek_alphabet_inv.items():
        name = name.replace(ascii,unicode)
    return name

def clearticks():
    # set up empty tick lists
    ticks = {}
    for axis in ('x','y'):
        ticks[axis] = [None] * (nst+1)
        for stellar_type in stellar_types:
            ticks[axis][stellar_type] = {}
    return ticks

def make_xypoints(bigdatastats,
                  stellar_type1,
                  stellar_type2):
    # given stellar types to plot, 1=x=col, 2=y=rol, generate a list of
    # (x,y) tuples that are in the map
    xypoints = []
    for x in bigdatastats['colpoints'][stellar_type1]:
        for y in bigdatastats['rowpoints'][stellar_type2]:
            xypoints.append((x,y))
    return xypoints

############################################################
# Main loop that does the work
############################################################

if __name__ == '__main__':
    # loop over the parameters, make a plot for each
    for parameter_name in parameters:
        for parameter_value in parameter_values[parameter_name]:
            ticks = clearticks()
            show_ticklabels = copy.deepcopy(ticks)

            # get stats
            bigdatastats,count = globalstats(parameter_name,
                                             parameter_value)

        for parameter_value in parameter_values[parameter_name]:

            # set up figure
            fig = {}
            cbar_ax = {}
            for pdffile in pdffiles:

                # override tick label sizes
                plt.rcParams['xtick.labelsize'] = pdffiles[pdffile]['tickfontsize']
                plt.rcParams['ytick.labelsize'] = pdffiles[pdffile]['tickfontsize']

                fig[pdffile] = plt.figure(
                    figsize=pdffiles[pdffile]['figsize'],
                    dpi=pdffiles[pdffile]['dpi']
                )
                fig[pdffile].suptitle(f"{format_name(parameter_name)}={parameter_value:g}")

                if pdffiles[pdffile]['renew_gridspec'] == False:

                    # make grid of subplots here, once
                    pdf[pdffile]['subplots'] = \
                        fig[pdffile].\
                        add_gridspec(**pdffiles[pdffile]['gridspec_options']).\
                        subplots(**pdffiles[pdffile]['gridspec_subplots_options'])
                    for stellar_type1 in stellar_types1:
                        for stellar_type2 in stellar_types2:
                            ax = pdf[pdffile]['subplots'][stellar_type2][stellar_type1]
                            ax.tick_params(
                                axis='x',
                                which='both',
                                direction='out',
                                labelrotation=90,
                                labelsize=pdffiles[pdffile]['tickfontsize']
                            )
                            ax.tick_params(
                                axis='y',
                                which='both',
                                direction='out',
                                labelrotation=0,
                                labelsize=pdffiles[pdffile]['tickfontsize']
                            )

                    # also set colourbar font size
                    cbar_ax[pdffile] = fig[pdffile].add_axes(cbar_location)
                    cbar_ax[pdffile].tick_params(axis='y',
                                                 labelsize=pdffiles[pdffile]['cbfontsize'])

                    # make a blank plot in empty grid locations
                    for stellar_type1 in stellar_types1:
                        for stellar_type2 in stellar_types2:
                            ax = pdf[pdffile]['subplots'][stellar_type2][stellar_type1]
                            if stellar_type1 in stellar_types1 and stellar_type2 in stellar_types2:
                                ax.axis('on')
                            else:
                                ax.axis('off')


            # global data statistics so we can set plot ranges etc.
            ticks = clearticks()
            bigdatastats,count = globalstats(parameter_name,
                                             parameter_value)

            # limit number of ticks
            for axis in ('x','y'):
                for stellar_type in stellar_types:
                    reduce_ticklist(pdffile=pdffile,
                                    axis=axis,
                                    stellar_type=stellar_type)

            # enforce z range to start at zero if not using logz
            if bigdatastats and not logz:
                bigdatastats['z'][0] = 0.0

            # make plots, put them in the grid
            make_cbar = True
            made_count = 0
            arglist = []

            for stellar_type1 in stellar_types1:
                yrange = pdffiles[pdffile]['datarange']['y'] or bigdatastats['colrange'][stellar_type1]
                print(f"RANGE Y {yrange}")

                for stellar_type2 in stellar_types2:
                    xrange = pdffiles[pdffile]['datarange']['x'] or bigdatastats['rowrange'][stellar_type1]
                    print(f"RANGE X {xrange}")
                    if stellar_type1 in stellar_types1 and stellar_type2 in stellar_types2:
                        made_count += 1
                        print(f"Parameter {parameter_name} = {parameter_value} : Plot {made_count}/{count} stellar types {stellar_type1}, {stellar_type2}")

                        for pdffile in pdffiles:
                            # perhaps make new axes and new colourbar
                            if pdffiles[pdffile]['renew_gridspec'] == True:
                                fig[pdffile].clf()
                                fig[pdffile].suptitle(f"{format_name(parameter_name)}={parameter_value:g}")
                                pdf[pdffile]['subplots'] = fig[pdffile].add_gridspec(
                                    **pdffiles[pdffile]['gridspec_options']).subplots(
                                        **pdffiles[pdffile]['gridspec_subplots_options'])
                                cbar_ax[pdffile] = fig[pdffile].add_axes(cbar_location)
                                make_cbar = True
                                xypoints = None
                            else:
                                xypoints = make_xypoints(bigdatastats,stellar_type1,stellar_type2)

                            print(f"XYPOINTS {xypoints}")
                            # make the plot
                            plt.figure(fig[pdffile].number)
                            launch_plot_wrap({
                                'pdffile' : pdffile,
                                'fig' : fig[pdffile], # axis
                                'subplots' : pdf[pdffile]['subplots'],
                                'cbar_ax' : cbar_ax[pdffile],
                                'bigdatastats' : bigdatastats,
                                'parameter_name':parameter_name,
                                'parameter_value':parameter_value,
                                'stellar_type1':stellar_type1,
                                'stellar_type2':stellar_type2,
                                'make_cbar':make_cbar,
                                'ticks' : ticks,
                                'show_ticklabels' : show_ticklabels,
                                'xrange' : xrange,
                                'yrange' : yrange,
                                'xypoints' : xypoints
                            })

                            # save now, once per grid location
                            if pdffiles[pdffile]['renew_gridspec'] == True:
                                pdf[pdffile]['pdfpages'].savefig()
                            else:
                                # only make the colourbar once per plot
                                make_cbar = False

            for pdffile in pdffiles:
                if pdffiles[pdffile]['renew_gridspec'] == False:
                    # save to subgrid location
                    plt.figure(fig[pdffile].number)
                    pdf[pdffile]['pdfpages'].savefig()

                # close the plot
                plt.close()

    # close the pdf files
    print("See: ")
    for pdffile in pdffiles:

        print(f"{pdffiles[pdffile]['file']} containing figure {fig[pdffile]}")
        pdf[pdffile]['pdfpages'].close()
