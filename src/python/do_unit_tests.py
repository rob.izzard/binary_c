#!/usr/bin/env python3

############################################################
#
# Script to perform unit tests or generate their reference
# data
#
# Environment variables that affect this:
#
# BINARY_C : the location of the binary_c git directory
#
# BINARY_C_EXECDIR : the folder in which binary_c executable
#                    is located
#
# BIANRY_C_EXTRA_ARGS : extra args sent to binary_c
#
############################################################

from binary_c_log_to_dict import binary_c_log_to_dict
from binary_c_dictdiff import binary_c_dictdiff
from itertools import filterfalse
import glob
import os
import subprocess
import sys
import tempfile
ld = binary_c_log_to_dict()

# set up binary_c location
binary_c_dir = os.environ['BINARY_C']
exec_dir = os.environ['BINARY_C_EXECDIR'] or binary_c_dir
if binary_c_dir is None:
    print("Error: environment variable BINARY_C must be set")
    sys.exit()
try:
    extraargs = os.environ['BINARY_C_EXTRA_ARGS']
except:
    extraargs = ""

# run mode:
# 'generate' : make the data and save it
# 'generate_if_necessary' : make data if required
# 'compare' (default) : compare new data to that saved
if len(sys.argv) > 1:
    mode = sys.argv[1]
else:
    mode = 'compare'

force = False
if len(sys.argv) > 2:
    if sys.argv[2] == 'force':
        force = True

print(f"len {len(sys.argv)} mode {mode}, force {force}")
# define diff
diff_args = {
    'significant_digits' : None,
    'abs_tol' : None,
    'rel_tol' : 1e-6,
}

# assume the executable is 'binary_c'
binary_c = exec_dir + '/binary_c'

# check binary_c is built appropriately
if not force and not binary_c_log_to_dict.check_binary_c(None,binary_c):
    print("""
binary_c needs to be built without optimization and with -Daccurate=true, e.g. run meson like this

meson setup -Daccurate=true -Ddebug=true --buildtype=debug builddir
 """)
    sys.exit(2)

# path to unit tests directory
unit_tests_path = os.path.join(binary_c_dir,
                               'unit_tests')

# path to reference data directory
logs_dir = os.path.join(unit_tests_path,
                        'reference_data')

# arguments files path
args_dir = os.path.join(unit_tests_path,
                        'argument_lists')

print(f"args path {args_dir}")
print(f"reference logs path {logs_dir}\n")

# loop over files
nsucceeded = 0
nfailed = 0
with os.scandir(args_dir) as files:
    for filepath in files:
        # file's name (not including the path)
        file = str(filepath.name)
        if not file.endswith('~') and not file.startswith('#'):
            #print(f"Test {filepath}")
            #print(f"Test {filepath.path}")

            # reference paths
            ref_args_path = os.path.join(args_dir,file)
            ref_log_path = os.path.join(logs_dir,file)

            # open and read args, ignoring comment lines
            args = ''
            with open(filepath,'r') as f:
                for line in f:
                    if not line.startswith('#'):
                        args += line.replace('\n',' ')
            f.close()

            # open reference log, or leave it as None
            try:
                f = open(ref_log_path,'r')
                ref_log = f.read()
                f.close()
            except:
                ref_log = None

            # run binary_c and capture output
            result = subprocess.Popen([ binary_c, 'log_filename', '/dev/stdout' ] + args.split() + extraargs.split(),
                                      stdout = subprocess.PIPE,
                                      stderr = subprocess.PIPE)
            out, err = result.communicate()

            if result.returncode != 0:
                # failed to run and return code 0
                print(f"Unit test \"{file}\" failed (binary_c return code was {result.returncode} which is non-zero)")
                nfailed += 1

            else:

                #print(f"return code {result.returncode}, pid {result.pid}")

                new_log = str(out,'utf-8')

                # action!
                if mode == 'generate' or \
                   (ref_log == None and mode == 'generate_if_necessary'):
                    # generate data
                    print(f"Generate data to {ref_log_path}")
                    f = open(ref_log_path,'w')
                    f.write(new_log)
                    f.close()

                elif ref_log != None:
                    # compare ref to new data
                    # print(f"REF:\n{ld.clean_string(ref_log)}")
                    # print(f"NEW:\n{ld.clean_string(new_log)}")
                    ref_stats = ld.parse_string(ref_log)
                    new_stats = ld.parse_string(new_log)

                    diff = binary_c_dictdiff(
                        ref_stats,
                        new_stats,
                        significant_digits = diff_args['significant_digits'],
                        math_epsilon = diff_args['abs_tol'],
                        rel_tol = diff_args['rel_tol']
                    )
                    if diff:
                        # unit test diff failed
                        print(f"Unit test \"{file}\" failed")
                        print(ref_stats)
                        print(new_stats)
                        print(diff)
                        nfailed += 1
                    else:
                        print(f"Unit test \"{file}\" succeeded")
                        nsucceeded += 1
                else:
                    print(f"Unit test {file} failed\nref_log is {ref_log}\nMode is {mode}\n")
                    if ref_log == None and mode == 'compare':
                        print("No reference data is available. It is likely this is a new test that you have added. Please use the 'generate' or 'generate_if_necessary' mode to generate the reference data.")
                    sys.exit(2)

ntot = nsucceeded + nfailed
print(f"binary_c unit tests {nsucceeded}/{ntot} succeeded, {nfailed}/{ntot} failed")
if nfailed > 0:
    sys.exit(1)
else:
    sys.exit(0)
