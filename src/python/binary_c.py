#!/usr/bin/env python2

import ctypes
from ctypes import *
import binary_c_structures

# dependencies
ctypes.CDLL('libc.so.6',mode=ctypes.RTLD_GLOBAL)
ctypes.CDLL('libm.so.6',mode=ctypes.RTLD_GLOBAL)
ctypes.CDLL('libbsd.so',mode=ctypes.RTLD_GLOBAL)
ctypes.CDLL('libmemoize.so',mode=ctypes.RTLD_GLOBAL)
ctypes.CDLL('libgslcblas.so',mode=ctypes.RTLD_GLOBAL)
ctypes.CDLL('libgsl.so',mode=ctypes.RTLD_GLOBAL)
ctypes.CDLL('libbacktrace.so',mode=ctypes.RTLD_GLOBAL)
ctypes.CDLL('libbfd.so',mode=ctypes.RTLD_GLOBAL)
binary_c = ctypes.CDLL('../libbinary_c.so',mode=ctypes.RTLD_GLOBAL)


stardata = 0
system = binary_c.binary_c_new_system(stardata,)



#binary_c.binary_c_free_memory(
