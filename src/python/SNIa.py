#!/usr/bin/env python3

import os
os.environ['PYTHONPATH']='./src/python'
import binarycpython
from binary_c_ensemble_manager import binary_c_ensemble_manager
import deepdiff
import operator
from pprint import pprint
import subprocess
import sys
from typing import Iterable


# check cmd line args
if len(sys.argv) >1:
    task = sys.argv[-1]
    datadir = sys.argv[1] if len(sys.argv) > 2 else None
else:
    task = None
    datadir = None

# paths and commands
inlist = './src/inlists/ensemble_inlist_SNIa.py'
manager = './src/python/ensemble_manager.py'
MW='./src/python/MW.py'
nice = ['nice','-n','+19']
python = 'python3'
cmdlist = [nice,python,manager,inlist]

def flatten(items):
    """Yield items from any nested iterable; see Reference."""
    for x in items:
        if isinstance(x, Iterable) and not isinstance(x, (str, bytes)):
            for sub_x in flatten(x):
                yield sub_x
        else:
            yield x

# final argument is the command

print(f"SNeIa with binary_c : task is {task}")

if task is None:
    print("You must specify a task, e.g. 'launch' or 'rates'")
    sys.exit()

# read in the options from the inlist
localdict = {
    'data' : {}
    }
exec(open(inlist).read(),
     globals(),
     localdict)

if task == 'launch':
    for scenario in localdict['data']:
        print(f"Launch scenario {scenario}")

        cmd=flatten(['env',f"SNIA_JOB={scenario}",cmdlist,'launch'])
        print(f"Command : {cmd}")
        subprocess.run(cmd)

elif task == 'rates':
    print(f"Compute rates from data in {datadir}")

    if datadir is None:
        print("You must specify a data directory as the first argument.")
        sys.exit()

    # set up data directory
    os.environ['ENSEMBLE_MANAGER_DIR'] = datadir

    # initiate ensemble manager
    manager = binary_c_ensemble_manager(standalone=True)
    manager.setup(cluster_options=None)

    # open the database
    opts = manager.runtime_vars.copy()
    db = manager.connect_db(opts)
    manager.inlist = inlist

    for scenario in localdict['data']:
        # read the inlist without a job set to get the defaults
        os.environ['SNIA_JOB'] = ''
        manager.read_inlist()
        default_id_dict = manager.binary_c_ensemble_id_dict(opts=opts,force_strings=True,prefix=True)

        # read the inlist with a job set
        os.environ['SNIA_JOB'] = scenario
        manager.read_inlist()
        id_dict = manager.binary_c_ensemble_id_dict(opts=opts,force_strings=True,prefix=True)

        # list the keys that are different
        diff = deepdiff.DeepDiff(default_id_dict,id_dict)
        added = {}
        for x in ('dictionary_item_added','values_changed'):
            if x in diff:
                for k in diff.affected_root_keys:
                    added[k] = id_dict[k]

        # find a matching ensemble
        id = manager.search_db_dict(db,added,strip_uuids=True)
        if not id:
            print("\n\nDEFAULTS",default_id_dict)
            print("FAILED TO FIND")
            print("NEW",id_dict)
            print("DIFF",diff)
            sys.exit()

        # get uuid
        try:
            uuid = id[0][0]
        except:
            print(f"could not find uuid from id = {id}")
            sys.exit(1)

        # hence locate the ensemble file
        for extension in ('','.gz','.bz2'):
            ensemblefile = os.path.join(datadir,
                                        f'ensemble-{uuid}',
                                        f'ensemble_output.json{extension}')
            if os.path.isfile(ensemblefile):
                break

        print(f"\n{added} -> {uuid} -> {ensemblefile}")

        cmd=flatten(['env',python,MW,ensemblefile])
        result = subprocess.run(cmd,capture_output=True,text=True)

        for line in result.stdout.strip("\n").split("\n"):
            print("LINE",line)
            if line.startswith('SN') and 'Ia' in line:
                print(line)
