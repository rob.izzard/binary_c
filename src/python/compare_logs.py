#!/usr/bin/env python3

############################################################
#
# Script to process a binary_c log file and convert
# this to a dict of useful statistics, which is then output
# as JSON.
#
# The binary_c_dictdiff module uses deepdiff to do
# the work, with this adapted to use a rel_tol relative
# tolerance.
#
# The first argument to the script should be the reference
# log filename.
#
# Following arguments are filenames contain logs to be
# compared to the reference set.
#
############################################################

from binary_c_log_to_dict import binary_c_log_to_dict
from binary_c_dictdiff import binary_c_dictdiff
import fileinput
import json
import sys

# first argument is the reference file
ref_file = sys.argv[1]
other_files = sys.argv[2:]

def make_stats(file):
    return binary_c_log_to_dict().parse(
        fileinput.input(files=file,
                        encoding="utf-8")
    )

# make ref stats dict
ref_stats = make_stats(ref_file)

# make dicts for other files
other_stats = {}
for file in other_files:
    other_stats[file] = make_stats(file)

# compare ref_stats to the other_stats
for file in other_files:
    x = binary_c_dictdiff(
        ref_stats,
        other_stats[file],
        #significant_digits=None,
        #abs_tol=None
        rel_tol=1e-6,
    )
    if not x:
        sys.exit(0)
    else:
        print(f"Diff {ref_file} {file} : {x}")
        sys.exit(1)
