#!/usr/bin/env python3
import json
import argparse
from collections import deque
import gc
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
from matplotlib.backends.backend_pdf import PdfPages
from matplotlib.lines import Line2D
from matplotlib import rc
#matplotlib.rcParams['text.usetex'] = True
import multiprocess
import seaborn as sns
from str2bool import str2bool
import sys
import re
from binarycpython.utils.ensemble import load_ensemble,ensemble_setting
import pebble
try:
    from queue import Queue
except ImportError:
    from Queue import Queue

############################################################
#
# binary_c ensemble plotter
#
# test version for multiple files
#
############################################################

# function to return a unique, sorted list
def _uniq(_list):
    return sorted(list(set(_list)))

def arg2bool(x):
    # convert argument to boolean
    if x is None:
        return None
    elif isinstance(x,str):
        return bool(str2bool(x))
    else:
        return bool(str2bool(str(x)))

# set up and parse cmd-line args
description = "Binary_c ensemble plotter"
parser = argparse.ArgumentParser(description=description)
parser.add_argument('ensemble_files',type=str,default=None,nargs='*',help="The ensemble JSON file to use as a data source.")
parser.add_argument('--output_file',type=str,default='ensemble.pdf',nargs='?',help="Output (PDF) file name.")
parser.add_argument('--elements',type=str,default=None,nargs='*',help="Elements to plot, e.g. C, He, Y, Ba.")
parser.add_argument('--isotopes',type=str,default=None,nargs='*',help="Isotopes to plot, e.g. C12, He4, Cu65.")
parser.add_argument('--sources',type=str,default=None,nargs='*',help="Source types to plot, e.g. AGB, Wind, SNII.")
parser.add_argument('--scalars',type=str,default=None,nargs='*',help="Scalar output to plot, e.g. LMMS, MDOT. These are identical to binary_c's enesemble types.")
parser.add_argument('--style',type=str,default='white',nargs='?',help="Seaborn plot style to use.")
parser.add_argument('--font',type=str,default='DejaVu Sans',nargs='?',help="Set the font used in the plots.")
parser.add_argument('--backend',type=str,default='pdf',nargs='*',help="Plotting backend (see Matplotlib for details).")
parser.add_argument('--markersize',type=int,default=6,help="Size of marker points (defualt 6).")
parser.add_argument('--markevery',type=int,default=2,help="Put a point every markevery data items.")
parser.add_argument('--plot_orders',type=int,default=10,help="If set, ignore ylimits and plot the top plot_orders orders of magnitude of data. Ignored if zero, so if you want to set ymin and ymax, set this to zero. Default=10.")
parser.add_argument('--plot_yields',default=True,nargs='?',help="Turn on yield plotting (default True).")
parser.add_argument('--mpyields',default=False,nargs='?',help="Plot m_p yields rather than the mass ejected. These are the same with the initial mass into the isotope subtracted, so can be positive or negative.")
parser.add_argument('--plot_scalars',default=True,nargs='?',help="Turn on scalar plotting (default True).")
parser.add_argument('--log_yields',default=True,nargs='?',help="Logarithmically scale yield plots (default True).")
parser.add_argument('--symlog_yields',default=False,nargs='?',help="Symlog scale yield plots (default True).")
parser.add_argument('--symlog_linthresh',default=1e-5,nargs='?',help="Symlog linthresh parameter (default 1e-5).")
parser.add_argument('--log_scalars',default=False,nargs='?',help="Logarithmically scale scalar plots (default False).")
parser.add_argument('--symlog_scalars',default=False,nargs='?',help="Logarithmically scale scalar plots (default False).")
parser.add_argument('--unicode_minus',default=False,help="Use Unicode minus signs rather than hyphens (default False).")
parser.add_argument('--xmargin',type=float,default=None,help="Setting for the xmargin (default None).")
parser.add_argument('--ymargin',type=float,default=None,help="Setting for the ymargin (default None).")
parser.add_argument('--tight',default=True,help="Matplotlib tight parameter.")
parser.add_argument('--list_isotopes',default=False,help="If True, list isotopes and exit.")
parser.add_argument('--ignore_isotopes',default=['n'],nargs='+',help="Isotopes that are ignored by default. (usually just 'n')")
parser.add_argument('--sort_isotopes_by_mass',default=False,help="If True, plots are added in isotopic mass order.")
parser.add_argument('--yield_stats',default=True,help="If True, show yield stats (total, %% by source, etc.).")
parser.add_argument('--list_scalars',default=False,help="If True, list scalars and exit.")
parser.add_argument('--xmin',type=float,default=None,help="Set the xmin, i.e. the lower end of the x range (default None).")
parser.add_argument('--xmax',type=float,default=None,help="Set the xmax, i.e. the upper end of the x range (default None).")
parser.add_argument('--ymin',type=float,default=None,help="Set the ymin, i.e. the lower end of the y range : remember to set plot_orders to zero to make this work (default None).")
parser.add_argument('--ymax',type=float,default=None,help="Set the ymax, i.e. the upper end of the y range : remember to set plot_orders to zero to make this work (default None).")
parser.add_argument('--yrange_eps',type=float,default=1e-12,help="A small number which is compared to the plotted y range. If the y range is too small, set one y tick at the right place. This helps when you try to plot a constant value and matplotlib's locator functions don't work very well (default 1e-12).")
parser.add_argument('--ensemble_keys',default=['scalars','Xyield','Xinit'],nargs='+',help="Keys to be selected from the ensemble. If you use fewer keys, things run faster, however you require some keys for functionality.")
parser.add_argument('--nthreads',type=int,default=1,help="Number of threads to use.")

args = parser.parse_args()
# convert Boolean arguments to Python Booleans
args.plot_yields = arg2bool(args.plot_yields)
args.plot_scalars = arg2bool(args.plot_scalars)
args.unicode_minus = arg2bool(args.unicode_minus)
args.log_yields = arg2bool(args.log_yields)
args.log_scalars = arg2bool(args.log_scalars)
args.tight = arg2bool(args.tight)
args.list_scalars = arg2bool(args.list_scalars)
args.list_isotopes = arg2bool(args.list_isotopes)
args.mpyields = arg2bool(args.mpyields)
args.symlog_scalars = arg2bool(args.symlog_scalars)
args.symlog_yields = arg2bool(args.symlog_yields)
args.sort_isotopes_by_mass = arg2bool(args.sort_isotopes_by_mass)
args.yield_stats = arg2bool(args.yield_stats)
nfiles = len(args.ensemble_files)

# set backend and matplotlib options
matplotlib.use(args.backend) # pdf or pgf
matplotlib.rcParams['axes.unicode_minus'] = args.unicode_minus
msun = 'M$_⊙$'
found_error = False
logtimes = None

# set figure size
sns.set(rc={'figure.figsize':(15,7)})
# set background colour
sns.set_style(args.style)
# set axis labels to be formatted with mathtext
plt.rcParams['axes.formatter.use_mathtext'] = True
# set font
rc('font',**{'family':'sans-serif','sans-serif':[args.font]})

# open output file
pdf = PdfPages(args.output_file)

# get data from ensemble files
cache = {}
main_isotopes_list = []
main_sources_list = []

if len(args.ensemble_files)==0:
    print("{}\n\nYou must provide at least one ensemble file as the first argument to this script.".format(description))
    sys.exit(1)

for ensemble_file in args.ensemble_files:

    # get the ensemble filename
    print("Data from ensemble file {}".format(ensemble_file),flush=True)
    cache[ensemble_file] = {}
    cache[ensemble_file] = load_ensemble(ensemble_file,
                                         select_keys=args.ensemble_keys,
                                         convert_float_keys=True,
                                         allow_nan=True,
                                         timing=False)
    mycache = cache[ensemble_file]
    ensemble = mycache['ensemble']

    # strip unwanted keys
    ensemble_keys = list(ensemble.keys())
    for key in ensemble_keys:
        if not key in args.ensemble_keys:
            del ensemble['ensemble'][key]

    # find the yields and available times
    yields = ensemble['Xyield']['time']

    if yields:
        times = sorted(yields.keys())

        # build a list of available isotopes and sources
        mycache['isotopes_dict'] = {}
        mycache['isotopes_list'] = []
        mycache['sources_dict'] = {}

        isotopes_dict = mycache['isotopes_dict']
        isotopes_list = mycache['isotopes_list']
        sources_dict = mycache['sources_dict']
        for time in times:
            now = yields[time]['source']
            for source in now.keys():
                sources_dict[source] = 1
                for isotope in now[source]['isotope'].keys():
                    isotopes_dict[isotope] = 1
                isotopes_list = sorted(isotopes_dict.keys())

        # make useful lists
        main_isotopes_list += isotopes_list
        main_sources_list += sorted(sources_dict)

        # and a lookup dictionary for atomic mass
        mycache['isotope_mass_dict'] = {}
        for key in mycache['metadata']['settings']['binary_c_version_info']['isotopes'].keys():
            d = mycache['metadata']['settings']['binary_c_version_info']['isotopes'][key]
            mycache['isotope_mass_dict'][d['name']] = d['mass_amu']

        # find out if we're using log times
        mycache['logtimes'] = arg2bool(ensemble_setting(ensemble,'ensemble_logtimes'))
        if logtimes is not None:
            # cannot plot two files which are linear and log
            if logtimes is not mycache['logtimes']:
                print("Your files must either all have log times, or all linear.")
                os.exit()
        else:
            # first time
            logtimes = mycache['logtimes']


main_isotopes_list = _uniq(main_isotopes_list)
main_sources_list = _uniq(main_sources_list)

# show lists and then exit?
do_exit = False
if args.list_scalars:
    print("Scalars:",' '.join(ensemble['ensemble']['scalars'].keys()))
    do_exit = True
if args.list_isotopes:
    print("Isotopes:",' '.join(main_isotopes_list))
    do_exit = True
if do_exit: exit()

# set up plot markers
_main_marker_list = list(Line2D.filled_markers) + [m for m,func in Line2D.markers.items() if func != 'nothing' and m not in Line2D.filled_markers]
_marker_list = list(_main_marker_list)
markers={}
markers_style={}
for source in main_sources_list:
    marker_style = _marker_list.pop(0)
    if not _marker_list:
        _marker_list = list(_main_marker_list)
    if marker_style != 'nothing':
        markers[source] = marker_style
        markers_style = dict(marker=marker_style, color='black')

# x-axis label
logdt_string = '$\log _{10}$' if logtimes else ''
xlabel = '{log}{open} Time, $t$ / Myr {close}'.format(
    log=logdt_string,
    open='(' if logtimes else '',
    close=')' if logtimes else '',
)



if args.plot_yields:
    # override with cmd-line args if required
    if args.isotopes:
        isotopes_list = args.isotopes
    elif args.elements:
        # select by element
        isotopes_list = []

        for element in args.elements:
            r = re.compile("{}\\d".format(element))
            isotopes_list += [i for i in main_isotopes_list if r.match(i)]

    if args.sources:
        sources_list = args.sources
    else:
        sources_list = main_sources_list

    # perhaps order isotopes by atomic mass?
    if args.sort_isotopes_by_mass:
        isotopes_list.sort(key=lambda i: isotope_mass_dict.get(i))

    isotope_regex = re.compile(r'(\D+)(\d+)')

    # atomic print
    lock = multiprocess.Lock()
    def lprint(*args):
        with lock:
            print(*args,flush=True)

    def plot_isotope(isotope):
        # plot log ?
        if isotope.startswith('log'):
            log_this_isotope = 'log'
            isotope = isotope.replace("log","")
            lprint("Plot isotope vs time: log10(",isotope,")")
        elif isotope.startswith('symlog'):
            log_this_isotope = 'symlog'
            isotope = isotope.replace("symlog","")
            lprint("Plot isotope vs time: symlog(",isotope,")")
        else:
            log_this_isotope = False
            lprint("Plot isotope:",isotope)

        if not isotope in main_isotopes_list:
            print("*** Error: isotope ",isotope,"not found")
            found_error = True
            return None

        # make pretty isotope, formatted with the number superscripted
        match = isotope_regex.findall(isotope)
        if match:
            pretty_isotope = '$^{' + match[0][1] + '}\mathrm{' + match[0][0] + '}$'
        else:
            pretty_isotope = isotope # should not happen

        # get initial abundance, if available
        if 'Xinit' in ensemble:
            Xinit = ensemble['Xinit']['isotope'][isotope]
        else:
            Xinit = None
            if args.mpyields:
                print("Error: you want to plot mpyields, but have no Xinit data. Try running a newer ensemble.")

        # loop over ensembles, adding the data
        plotdata = {}
        my_markers = {}
        count = 0
        for ensemble_file in args.ensemble_files:
            count += 1
            yields = cache[ensemble_file]['ensemble']['Xyield']['time']
            minxval=float(times[0])
            maxxval=float(times[-1])
            minyval=1e100
            maxyval=0.0
            totyield = {}
            data = {}

            for source in main_sources_list:
                data[source] = {}
                totyield[source] = 0.0

                # get mass ejected from this source as all isotopes at all times
                mass_ejected = 0.0
                for time in times:
                    for i in main_isotopes_list:
                        try:
                            mass_ejected += yields[time]['source'][source]['isotope'][i]
                        except KeyError:
                            pass

                for time in times:
                    try:
                        y = yields[time]['source'][source]['isotope'][isotope]
                    except KeyError:
                        y = None
                    if y:
                        # add up the total yield : this assumes time bins are
                        # fixed in duration or log duration
                        # (true with the current ensemble)
                        totyield[source] += y
                        if args.mpyields:
                            # find total mass ejected at this time in this source
                            # from all isotopes
                            dm_all_isotopes = 0.0
                            for i in main_isotopes_list:
                                try:
                                    dm_all_isotopes += yields[time]['source'][source]['isotope'][i]
                                except KeyError:
                                    pass
                            if dm_all_isotopes < 0.0 or dm_all_isotopes / mass_ejected < 1e-10:
                                # mass ejected is tiny (<1e-10 or the total) or negative: neglect
                                data[source][time] = 0.0
                            else:
                                # hence m_p which has no units
                                data[source][time] = (y - dm_all_isotopes * Xinit) / dm_all_isotopes
                                if time > 3:
                                    print("mpyield of {} at t={} = ({} - {} * {})/{} -> {}".format(source,time,y,dm_all_isotopes,Xinit,dm_all_isotopes,data[source][time]))


                        else:
                            # simply mass ejected
                            data[source][time] = y

                        if y>0.0:
                            maxyval = max(maxyval,data[source][time])
                            minyval = min(minyval,data[source][time])

            # reformat source keys and markers
            total_yield = sum(totyield.values())
            firstsource = True

            for source in sources_list:
                new_key = source.replace('_',' ')
                header = "{count} {file} {newline}".format(
                    count=(str(count)+":") if nfiles > 1 else "",
                    file=ensemble_file if firstsource else "",
                    newline = "\n" if firstsource else "")

                if args.yield_stats:
                    new_key = "{header}{source} {total:.2g} {frac:5.2f}".format(
                        header=header,
                        source=new_key,
                        total=totyield[source],
                        frac=totyield[source]/(total_yield+1e-200)
                    )
                else:
                    new_key = "{header} {source}".format(
                        header=header,
                        source=new_key
                    )
                plotdata[new_key] = data[source]
                my_markers[new_key] = markers[source]
                firstsource = False

        # set yscale based on logging or lack of it
        if args.log_yields or log_this_isotope == 'log':
            yscale = 'log'
        elif args.symlog_yields or log_this_isotope == 'symlog':
            yscale = 'symlog'
        else:
            yscale = 'linear'

        # make the plot
        fig,ax1 = matplotlib.pyplot.subplots()
        print(f"MARKERS {my_markers}")
        ax = sns.lineplot(data = plotdata,
                          palette = "colorblind",
                          markers = my_markers,
                          dashes = True,
                          estimator = None,
                          markevery = args.markevery,
                          markersize = args.markersize,
                          markeredgewidth = 0.0,
                          markeredgecolor = None,
                          ax=ax1)
        plt.close(fig) # we don't need fig in plt

        # set axis labels and scale
        if args.mpyields:
            ylabel = 'd($m_p$)/d({log}$t$/Myr) of {isotope} ejected per {msun} into stars'.format(
                log=logdt_string,
                msun=msun,
                isotope=pretty_isotope
            )
        else:
            ylabel = 'd($M$/{msun})/d({log}$t$/Myr) of {isotope} ejected per {msun} into stars'.format(
                log=logdt_string,
                msun=msun,
                isotope=pretty_isotope
            )
        ax.set(xlabel=xlabel,
               yscale=yscale,
               ylabel=ylabel,
        )
        if yscale == 'symlog':
            ax.set_yscale('symlog',
                          linthresh=float(args.symlog_linthresh))

        # set axis ranges and scaling
        if args.plot_orders == 0.0:
            ymin = args.ymin
            ymax = args.ymax
        else:
            ymin = max(minyval,10**-args.plot_orders*maxyval)
            ymax = maxyval

        if ymin != ymax:
            ax.axis(xmin=args.xmin,
                    xmax=args.xmax,
                    ymin=ymin,
                    ymax=ymax)
        else:
            ax.axis(xmin=args.xmin,
                    xmax=args.xmax,
                    ymin=None,
                    ymax=None)

        # resize plot and put key on the right
        box = ax.get_position()
        ax.set_position([box.x0,box.y0,box.width*0.85,box.height])

        # set ticks
        ax.xaxis.set_major_locator(ticker.AutoLocator())
        if args.log_yields or log_this_isotope == 'log':
            ax.yaxis.set_major_locator(ticker.LogLocator())
        elif args.symlog_yields or log_this_isotope == 'symlog':
            ax.yaxis.set_major_locator(ticker.SymmetricalLogLocator(base=10,linthresh=float(args.symlog_linthresh)))
        else:
            ax.yaxis.set_major_locator(ticker.AutoLocator())
            ax.ticklabel_format(axis='both',style='scientific')
        ax.tick_params(which='minor',
                       direction='in',
                       reset=True)
        ax.tick_params(which='major',
                       direction='in',
                       reset=True)


        # check for lack of yticks
        ylim = ax.get_ylim()
        yticks = ax.get_yticks()

        line = ax.lines[0]
        if line and ylim[1] > args.yrange_eps:
            if(ylim[1] - ylim[0] < args.yrange_eps):
                first = line.get_ydata()[0]
                ax.set_yticks([first,first*1.01,first*0.99])
            elif (max(yticks) - min(yticks) < args.yrange_eps):
                # problem: yticks don't work sometimes
                # so set one manually
                mean = sum(yticks) / float(len(yticks))
                ax.set_yticks([mean,mean*1.01,mean*0.99])
        # make sure there's a tick at 0 for symlog
        if args.symlog_yields or log_this_isotope == 'symlog':
            ax.set_yticks(yticks + [0])

        # add legend
        l = ax.legend(bbox_to_anchor=(1.01,0.9),
                       loc=2,
                       markerscale=1.0,
                       ncol=1)

        ax.margins(tight=args.tight,
                    x=args.xmargin,
                    y=args.ymargin)
        return fig


    processes = []
    madeplots = {} # plot has been made
    pendingplots = {} # plot is waiting to be saved to PDF
    savedplots = {} # plot has been saved to PDF
    nthreads = max(1,args.nthreads)

    # worker : runs in the child process
    def worker(isotope):
        flushplots(pdf)
        fig = plot_isotope(isotope)
        return {
            'type':'figure',
            'isotope':isotope,
            'fig':fig
        }

    def flushplots(pdf):
        # flush isotopes in order
        for isotope in isotopes_list:
            if isotope not in savedplots and isotope in pendingplots:
                savedplots[isotope] = True
                pdf.savefig(pendingplots[isotope]['fig'])
                # memory cleanup
                del pendingplots[isotope]['fig']
                del pendingplots[isotope]
                gc.collect()

    pool = pebble.ProcessPool(max_workers=max(nthreads-1,1))
    future = pool.map(worker,isotopes_list)
    iterator = future.result()
    while True:
        q = pool._context.task_queue
        try:
            result = next(iterator)
            pendingplots[result['isotope']] = result
            flushplots(pdf)

        except StopIteration:
            break
        except Exception as error:
            print("exception:",error)
    flushplots(pdf)

if args.plot_scalars:
    # plot scalars

    # set up plot markers
    _main_marker_list = list(Line2D.filled_markers) + [m for m,func in Line2D.markers.items() if func != 'nothing' and m not in Line2D.filled_markers]
    _marker_list = list(main_marker_list)
    markers={}
    markers_style={}

    for ensemble_file in args.ensemble_files:
        marker_style = _marker_list.pop(0)
        if marker_style != 'nothing':
            markers[ensemble_file] = marker_style
            markers_style = dict(marker=marker_style, color='black')
        if not _marker_list:
            _marker_list = list(main_marker_list)


    if args.scalars:
        scalars = args.scalars
    else:
        scalars = []

        for ensemble_file in args.ensemble_files:
            scalars = cache[ensemble_file]['ensemble']['scalars'].keys()
        scalars = _uniq(scalars)


    count = 0
    for scalar in scalars:
        count += 1
        data = {}
        width = 0
        for ensemble_file in args.ensemble_files:
            width=max(width,len(ensemble_file))
        width += 1
        for ensemble_file in args.ensemble_files:
            scalar_data = cache[ensemble_file]['ensemble']['scalars']
            this_data = {}

            if nfiles > 1:
                header = "{file:{width}s}".format(file=ensemble_file,
                                                  width=width)
            else:
                header = ""

            if scalar.startswith("log"):
                log_this_scalar = 'log'
                scalar = scalar.replace("log","")
                print("{header}Plot scalar vs time: log10({scalar})".format(header=header,
                                                                            scalar=scalar))
            elif scalar.startswith("symlog"):
                log_this_scalar = 'symlog'
                scalar = scalar.replace("symlog","")
                print("{header}Plot scalar vs time: symlog10({scalar})".format(header=header,
                                                                               scalar=scalar))
            else:
                log_this_scalar = False
                print("{header}Plot scalar vs time: {scalar}".format(header=header,
                                                                     scalar=scalar))

            try:
                this_data = scalar_data[scalar]
            except:
                print("{file:{width}} *** Error: Scalar data {scalar} not found (skipping this)".format(file=ensemble_file,
                                                                                                        width=width,
                                                                                                        scalar=scalar))
                this_data = None

            if this_data:
                data[ensemble_file] = this_data

        ax = sns.lineplot(data = data,
                          palette = 'colorblind',
                          markers = markers,
                          dashes = True,
                          estimator = None,
                          markevery = args.markevery,
                          markersize = args.markersize,
                          markeredgewidth = 0.0,
                          markeredgecolor = None)
        ax.set(xlabel=xlabel,
               ylabel= str(scalar) + ' per {} into stars'.format(msun),
        )

        # remove legend
        if nfiles <=1:
            plt.legend([],[],frameon=False)

        # get min, max of the data
        minval = 1e100
        maxval = -1e100
        for key in data.keys():
            if data[key]:
                minval = min(minval,min(data[key].values()))
                maxval = max(maxval,max(data[key].values()))

        # if all data is zero or less, we cannot log or symlog the data
        # so issue a warning
        if maxval <= 0.0 and \
           (log_this_scalar or args.log_scalars or args.symlog_scalars):
            print("Warning: data which is all zero cannot be logged, skipping log plot of",scalar)
            continue

        # set axis ranges and scaling
        if args.plot_orders == 0.0:
            ymin = args.ymin
            ymax = args.ymax
        else:
            ymin = max(minval,10**-args.plot_orders*maxval)
            ymax = maxval

        if ymin != ymax:
            ax.axis(xmin=args.xmin,
                    xmax=args.xmax,
                    ymin=ymin,
                    ymax=ymax)
        else:
            ax.axis(xmin=args.xmin,
                    xmax=args.xmax,
                    ymin=None,
                    ymax=None)
        ax.get_xaxis().get_major_formatter().set_useOffset(False)
        ax.get_yaxis().get_major_formatter().set_useOffset(False)
        plt.ticklabel_format(useOffset=False)

        ax.xaxis.set_major_locator(ticker.AutoLocator())
        ax.yaxis.set_major_locator(ticker.AutoLocator())

        style='plain'
        try:
            plt.ticklabel_format(style=plain,useOffset=False,useMathText=True)
        except:
            pass
        try:
            plt.ticklabel_format(axis='y', style=plain,useOffset=False,useMathText=True)
        except:
            pass
        try:
            plt.ticklabel_format(axis='x', style=plain,useOffset=False,useMathText=True)
        except:
            pass
        plt.rcParams['axes.formatter.use_mathtext'] = True
        plt.rcParams['axes.formatter.useoffset'] = False

        # set ticks
        ax.xaxis.set_major_locator(ticker.AutoLocator())
        if args.log_scalars or \
           log_this_scalar == 'log':
            ax.set(yscale='log')
            ylocator = ticker.LogLocator()
        elif args.symlog_scalars or \
             log_this_scalar == 'symlog':
            ax.set(yscale='symlog')
            ylocator = ticker.SymmetricalLogLocator(base=10,linthresh=float(args.symlog_linthresh))
        else:
            ylocator = ticker.AutoLocator()
            ax.ticklabel_format(axis='both',style='scientific',useOffset=False)
            ax.ticklabel_format(axis='both',style='scientific',useOffset=False,useLocale=False)
        ax.yaxis.set_major_locator(ylocator)
        ax.tick_params(which='minor',
                       direction='in',
                       reset=True)
        ax.tick_params(which='major',
                       direction='in',
                       reset=True)

        # check for lack of yticks
        ylim = ax.get_ylim()
        yticks = plt.yticks()[0]
        line = ax.lines[0]
        if line and ylim[1] > args.yrange_eps:
            if(ylim[1] - ylim[0] < args.yrange_eps):
                first = ax.lines[0].get_ydata()[0]
                plt.yticks([first,first*1.01,first*0.99])
            elif (max(yticks) - min(yticks) < args.yrange_eps):
                # problem: yticks don't work sometimes
                # so set one manually
                mean = sum(yticks) / float(len(yticks))
                plt.yticks([mean,mean*1.01,mean*0.99])

        pdf.savefig()
        plt.close()
pdf.close()
print("\nFinished making plots : output is in",args.output_file)

if found_error:
    print("\nWarning: errors were found, please check the above for messages")
