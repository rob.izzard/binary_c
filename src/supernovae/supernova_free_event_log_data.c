#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "supernovae.h"
#include "sn.h"
/*
 * Free SN event log data
 */
void supernova_free_event_log_data(void ** data)
{
    if(data && *data)
    {
        struct binary_c_new_supernova_event_t * const edata =
            (struct binary_c_new_supernova_event_t *) *data;
        free_star(&edata->pre_explosion_star);
        free_star(&edata->post_explosion_star);
        free_stardata(&edata->pre_explosion_stardata);
        Safe_free(*data);
    }
}
