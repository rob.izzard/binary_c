#include "../binary_c.h"
No_empty_translation_unit_warning;

Boolean star_can_DDet(struct stardata_t * const stardata,
                      struct star_t * const star)
{
    /*
     * Return TRUE if this star can explode as an
     * sub-MCh double-detonation SNIa.
     *
     * Note: we just test the star itself, not whether
     * it is merging or accreting. Such tests must be
     * performed elsewhere.
     */


    /*
     * Check the stellar type
     */
#define _DDet_test_type(TYPE) (star->stellar_type == TYPE##WD)

    /*
     * Check the star has a helium layer on the surface
     */
#define _DDet_test_has_He_layer (Core_exists(star,CORE_He))

    /*
     * Test for sufficient mass, or don't if the
     * parameter is zero.
     */
#define _DDet_test_mass(TYPE)                                           \
    (                                                                   \
        Is_zero(stardata->preferences->minimum_mass_for_##TYPE##WD_DDet) \
        ||                                                              \
        star->mass > stardata->preferences->minimum_mass_for_##TYPE##WD_DDet \
        )

    /*
     * Test for sufficient age, or don't if the parameter
     * is zero.
     *
     * We ignore this test if the star just merged.
     */
#define _DDet_test_age(TYPE)                                            \
    (                                                                   \
        Is_zero(stardata->preferences->minimum_age_for_##TYPE##WD_DDet) \
        ||                                                              \
        star->merge_state > 0                                           \
        ||                                                              \
        star->age > stardata->preferences->minimum_age_for_##TYPE##WD_DDet \
        )                                                               \

    /*
     * Test that the mass of helium accreted is sufficent, which
     * or ignore if this parameter is zero.
     */
#define _DDet_test_He_massive_enough(TYPE)                              \
    (                                                                   \
        Is_zero(stardata->preferences->mass_accretion_for_##TYPE##WD_DDet) \
        ||                                                              \
        More_or_equal(                                                  \
            star->core_mass[CORE_He] - star->core_mass[CORE_##TYPE],    \
            stardata->preferences->mass_accretion_for_##TYPE##WD_DDet   \
            )                                                           \
        )
#define _DDet_Conditions(TYPE)                        \
    (                                                 \
        /* must be of appropriate type */             \
        _DDet_test_type(TYPE)                         \
        &&                                            \
                                                      \
        /* must have a He core */                     \
        _DDet_test_has_He_layer                       \
        &&                                            \
                                                      \
        /* check our WD is massive enough */          \
        _DDet_test_mass(TYPE)                         \
        &&                                            \
                                                      \
        /* check our WD is old enough */              \
        _DDet_test_age(TYPE)                          \
        &&                                            \
                                                      \
        /* and the helium layer is thick enough */    \
        _DDet_test_He_massive_enough(TYPE)            \
        )

    /*
     * We can have a DDet if supernovae are not denied,
     * and if the above conditions are satisfied.
     */
    const Boolean DDet =
        star->deny_SN == 0 &&
        (
            _DDet_Conditions(CO)
            ||
            _DDet_Conditions(ONe)
            );

    if(DEBUG)
    {
#define _TEST(TYPE,X)                           \

        char * cores = core_string(star,FALSE);
        const int core = star->stellar_type == COWD ? CORE_CO : CORE_ONe;
        Dprint("DDet check at t=%g star %d stellar type %s, cores %s; deny? %u %s; %s %s; %s %s; %s %g > %g %s; %s %g > %g %s; %s %g > %g %s : DDet ? %s && (%s || %s) = %s\n",
               stardata->model.time,
               star->starnum,
               Short_stellar_type_string_from_star(star),
               cores,
               star->deny_SN,
               Yesno(star->deny_SN),

               /* stellar type */
               "stellar_type",
               Yesno(star->stellar_type == COWD ? _DDet_test_type(CO) : _DDet_test_type(ONe)),

               /* has helium */
               "has_He_layer",
               Yesno(_DDet_test_has_He_layer),

               /* He envelope massive enough */
               "M_He",
               star->core_mass[CORE_He] - star->core_mass[core],
               star->stellar_type == COWD ? stardata->preferences->minimum_mass_for_COWD_DDet : stardata->preferences->minimum_mass_for_ONeWD_DDet,
               Yesno(star->stellar_type == COWD ? _DDet_test_He_massive_enough(CO) : _DDet_test_He_massive_enough(ONe)),

               /* total mass */
               "MWD",
               star->mass,
               star->stellar_type == COWD ? stardata->preferences->minimum_mass_for_COWD_DDet : stardata->preferences->minimum_mass_for_ONeWD_DDet,
               Yesno(star->stellar_type == COWD ? _DDet_test_mass(CO) : _DDet_test_mass(ONe)),

               /* age */
               "age",
               star->age,
               star->stellar_type == COWD ? stardata->preferences->minimum_age_for_COWD_DDet : stardata->preferences->minimum_age_for_ONeWD_DDet,
               Yesno(star->stellar_type == COWD ? _DDet_test_age(CO) : _DDet_test_age(ONe)),

               Truefalse(star->deny_SN == 0),
               Truefalse(_DDet_Conditions(CO)),
               Truefalse(_DDet_Conditions(ONe)),


               /* result */
               Yesno(DDet)
            );
        Safe_free(cores);
    }

    Dprint("DD ? %s\n",Yesno(DDet));

    return DDet;
}
