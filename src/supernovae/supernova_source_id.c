#include "../binary_c.h"
No_empty_translation_unit_warning;


/*
 * Map from SN_* type to Yield_source type
 *
 * We need to do this because there are fewer
 * yield source types than SN types: we don't
 * distinguish between all the SN types to add
 * up yields (there would be many more yield sources
 * if we did!).
 *
 * Note: If we return SOURCE_UNKNOWN then no yields
 * are ejected and this is likely to cause an error.
 */

Yield_source supernova_source_id(struct star_t * exploder,
                                 struct star_t * pre_explosion_star,
                                 struct star_t * companion)
{
    Yield_source src_id = SOURCE_UNKNOWN;
    switch (exploder->SN_type)
    {
    case SN_IBC:
    case SN_II:
    case SN_GRB_COLLAPSAR:
    case SN_PPI:
    case SN_PI:
    case SN_PHDIS:
        /*
         * SNIbc or SNII
         * NB SN_GRB_COLLAPSAR is really a IBC
         */
        src_id =
            (exploder->SN_type == SN_GRB_COLLAPSAR ||
             exploder->SN_type == SN_IBC) ?
            SOURCE_SNIbc : SOURCE_SNII;
        break;
    case SN_IA_He:
    case SN_IA_HeStar:
        if(pre_explosion_star->stellar_type == HeWD ||
           NAKED_HELIUM_STAR(pre_explosion_star->stellar_type))
        {
            src_id = SOURCE_SNIa_He;
        }
        break;
    case SN_IA_He_Coal:
        if(pre_explosion_star->stellar_type == HeWD &&
           companion->stellar_type == HeWD)
        {
            src_id = SOURCE_SNIa_He;
        }
        break;
    case SN_IA_SUBCHAND_He_Coal:
        src_id = SOURCE_SNIa_SUBCHAND_He;
        break;
    case SN_IA_SUBCHAND_CO_Coal:
        src_id = SOURCE_SNIa_SUBCHAND_CO;
        break;
    case SN_IA_COWD_DDet:
    case SN_IA_Hybrid_HeCOWD:
    case SN_IA_Hybrid_HeCOWD_subluminous:
        if(pre_explosion_star->stellar_type == COWD)
        {
            src_id = SOURCE_SNIa_COWD_DDet;
        }
        break;
    case SN_IA_ONeWD_DDet:
        if(pre_explosion_star->stellar_type == ONeWD)
        {
            src_id = SOURCE_SNIa_ONeWD_DDet;
        }
        break;
    case SN_IA_CHAND:
    case SN_IA_VIOLENT:
        if(pre_explosion_star->stellar_type == COWD ||
           pre_explosion_star->stellar_type == HeWD)
        {
            src_id = SOURCE_SNIa_CHAND;
        }
        break;
    case SN_IA_CHAND_Coal:
        src_id = SOURCE_SNIa_CHAND_Coal;
        break;
    case SN_IIa:
        src_id = SOURCE_SNIIa;
        break;
    case SN_ECAP:
        src_id = SOURCE_SNeCAP;
        break;
    case SN_AIC:
        src_id = SOURCE_SNAIC;
        break;
    default:
        /* everything else */
        src_id = SOURCE_UNKNOWN;
        break;
    }
    return src_id;
}
