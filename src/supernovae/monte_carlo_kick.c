#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "supernovae.h"
/*
 *
 * This subroutine possibly:
 *
 * 1) applies a kick to the star which explodes as a supernova.
 * 2) applies a kick to the companion star
 * 3) strips the companion star because of shock impact and ablation
 *
 * The kick speed and direction are chosen in a Monte carlo fashion, i.e.
 * based on random numbers.
 *
 * The star that undergoes the supernova is the "exploder", whether
 * the star actually explodes or not (it may just all collapse to a
 * black hole, releasing nothing to the interstellar medium).
 *
 * The other star is the "companion".
 */


/* Tests:
 * flags to turn on distribution test code:
 * these test either the kick speed or direction
 * by running the routine NTESTS times, outputting
 * the results (which you can bin and plot)
 */
//#define TEST_KICK_SPEED_DISTRIBUTION
//#define TEST_KICK_DIRECTIONS
#define NTESTS 500000

double monte_carlo_kick(struct star_t * const exploder,
                        struct star_t * const pre_explosion_star,
                        const double remnant_mass,
                        const double mass_before_explosion,
                        const double companion_mass,
                        double * const eccentricity,
                        double * const separation,
                        double * const jorb,
                        struct stardata_t * const stardata,
                        struct stardata_t * const pre_explosion_stardata)
{
    Dprint("orbit : e=%g a=%g jorb=%g\n",
           *eccentricity,
           *separation,
           *jorb
        );
    Dprint("Mexploder=%g Mcompanion=%g -> MremnaNt=%g\n",
           mass_before_explosion,
           companion_mass,
           remnant_mass);

    struct kick_system_t * kick_system = Malloc(sizeof(struct kick_system_t));
    long int random_count =
        stardata->common.random_buffer.count +
        (stardata->common.random_buffer.set==TRUE ? 0 : stardata->preferences->random_skip);

    int kick_distribution = stardata->preferences->sn_kick_distribution[exploder->SN_type];
    double kick_dispersion = stardata->preferences->sn_kick_dispersion[exploder->SN_type];
    double kick_companion = stardata->preferences->sn_kick_companion[exploder->SN_type];
    static char ** strings = (char**)supernova_type_strings;
    struct star_t * companion = &stardata->star[Other_star(exploder->starnum)];

    Dprint("kick: SN type %s (%d) at t=%g kick dist %d dispersion %g, kick companion %g (type %d)\nExploder mass was %g, remnant mass is %g (exploder->mass=%g), companion mass is %g (companion->mass=%g)\n",
           strings[exploder->SN_type],
           exploder->SN_type,
           stardata->model.time,
           kick_distribution,
           kick_dispersion,
           kick_companion,
           companion->stellar_type,
           mass_before_explosion,
           remnant_mass,
           exploder->mass,
           companion_mass,
           companion->mass
        );

    /*
     * Set up the binary system at the moment of the kick
     */
    setup_binary_at_kick(stardata,
                         *separation,
                         *eccentricity,
                         mass_before_explosion + companion_mass,
                         kick_system);

    /*
     * Choose kick velocity (speed and direction)
     */
    set_kick_velocity(stardata,
                      pre_explosion_star,
                      exploder,
                      kick_system,
                      kick_distribution,
                      kick_dispersion,
                      kick_companion);

    /*
     * Update the orbit
     */
    set_new_orbit(stardata,
                  pre_explosion_stardata,
                  pre_explosion_star,
                  exploder,
                  companion,
                  kick_system,
                  kick_distribution,
                  kick_dispersion,
                  kick_companion,
                  mass_before_explosion,
                  remnant_mass,
                  companion_mass,
                  jorb,
                  separation,
                  eccentricity);

    /* logging */
    char * corestring = core_string(pre_explosion_star,FALSE);
    Append_logstring(LOG_SN_AND_KICK,
                     "%s %s (star %d, SN type %d [pre %d], pre-explosion M=%g %s stellar_type=%d Eexp=%g foe vexp=%g km/s) -> kick %d(%g) vk=%g vr=%g omega=%g phi=%g -> vn=%g ; final sep %g ecc %g (random count %ld) ",
                     exploder->SN_type==SN_WDKICK ? "WD kick" : "SN",
                     strings[exploder->SN_type],
                     exploder->starnum,
                     exploder->SN_type,
                     pre_explosion_star->SN_type,
                     pre_explosion_star->mass,
                     corestring,
                     pre_explosion_star->stellar_type,
                     supernova_kinetic_energy(stardata,pre_explosion_star)/FOE,
                     supernova_ejecta_velocity(stardata,pre_explosion_star),
                     kick_distribution,
                     kick_dispersion,
                     kick_system->kick_speed,
                     kick_system->orbital_speed,
                     kick_system->omega,
                     kick_system->phi,
                     sqrt(Max(0.0,kick_system->new_orbital_speed_squared)),
                     *separation,
                     *eccentricity,
                     random_count
        );
    Safe_free(corestring);

#ifdef RUNAWAY_STARS
    Append_logstring(LOG_SN_AND_KICK,
                     "Runaway v=(%g,%g,%g) |v|=%g : companion v=(%g,%g,%g), |v|=%g ; ",
                     exploder->v_Rwx,
                     exploder->v_Rwy,
                     exploder->v_Rwz,
                     exploder->v_Rw,
                     companion->v_Rwx,
                     companion->v_Rwy,
                     companion->v_Rwz,
                     companion->v_Rw
        );
#endif

    Dprint("after kick: e=%g a=%g jorb=%g\n",
           *eccentricity,
           *separation,
           *jorb);

    Safe_free(kick_system);

    return (BINARY_C_NORMAL_EXIT);
}
