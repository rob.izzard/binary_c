#pragma once
#ifndef SN_H
#define SN_H
#include "sn.def"

/*
 * Header file for supernova-related data
 *
 * Includes integers defining SN types (SN_*)
 * and kick algorithms
 */

#undef X

#define X(CODE,STRING,CMDLINEARG) SN_##CODE,
enum { SN_TYPES_LIST };
#undef X
#define X(CODE,STRING,CMDLINEARG) STRING,
static const char * const supernova_type_strings[] Maybe_unused = { SN_TYPES_LIST };
#undef X
#define X(CODE,STRING,CMDLINEARG) CMDLINEARG,
static const char * const supernova_cmdline_strings[] Maybe_unused = { SN_TYPES_LIST };
#undef X

#define SN_String(TYPE) ((char*)supernova_type_strings[(TYPE)])

#define NUM_SN_STRINGS SN_NUM_TYPES

#define SN_STRING_LENGTH ((size_t)30)

#define X(CODE) CODE##_SN,
enum { SN_TEMPORAL_LIST };
#undef X

#define X(CODE) KICK_VELOCITY_##CODE,
enum { SN_KICK_VELOCITY_DISTRIBUTION_LIST };
#undef X

#define X(CODE) WD_KICK_##CODE,
enum { WD_KICK_WHEN_LIST };
#undef X

#define X(CODE) POST_SN_ORBIT_##CODE,
enum { POST_SN_ORBIT_LIST };
#undef X

#define X(CODE) SN_IMPULSE_##CODE,
enum { SN_IMPULSE_LIST };
#undef X

#define X(CODE) KICK_##CODE,
enum { KICK_DIRECTIONS_LIST };
#undef X

#ifdef PPISN
#define X(CODE,STRING,SN_TYPE,REMNANT_TYPE) PPISN_TYPE_##CODE,
enum { PPISN_TYPES_LIST };
#undef X
#endif // PPISN

#define X(CODE) CORE_COLLAPSE_ENERGY_##CODE,
enum { CORE_COLLAPSE_ENERGY_LIST };
#undef X

/*
 * Macros for detecting stellar type
 */
#define SN_TYPE_IS_IA(TYPE) (                       \
        (TYPE)==SN_IA_He ||                         \
        (TYPE)==SN_IA_COWD_DDet ||                  \
        (TYPE)==SN_IA_ONeWD_DDet ||                 \
        (TYPE)==SN_IA_CHAND ||                      \
        (TYPE)==SN_IA_CHAND_Coal ||                 \
        (TYPE)==SN_IA_Hybrid_HeCOWD ||              \
        (TYPE)==SN_IA_He_Coal ||                    \
        (TYPE)==SN_IA_SUBCHAND_CO_Coal ||           \
        (TYPE)==SN_IA_SUBCHAND_He_Coal ||           \
        (TYPE)==SN_IA_Hybrid_HeCOWD_subluminous ||  \
        (TYPE)==SN_IA_HeStar                        \
        )

#define SN_TYPE_IS_CORE_COLLAPSE(TYPE) (        \
        (TYPE)==SN_II ||                        \
        (TYPE)==SN_IBC                          \
        )

#define SN_TYPE_IS_AIC(TYPE) (                  \
        (TYPE)==SN_AIC ||                       \
        (TYPE)==SN_AIC_BH                       \
        )

#endif /* SN_H */
