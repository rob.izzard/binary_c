#include "../binary_c.h"
No_empty_translation_unit_warning;


/*
 * Event handling function for supernovae
 */

Event_handler_function supernova_event_handler(void * const eventp Maybe_unused,
                                               struct stardata_t * const stardata,
                                               void * data Maybe_unused)
{
    struct binary_c_event_t * const event = (struct binary_c_event_t *) eventp;
    struct binary_c_new_supernova_event_t * new_sn = (struct binary_c_new_supernova_event_t *) event->data;

    Dprint("SN event handler star %p starnum=%d SN_type=%d %d\n",
           (void*)new_sn->pre_explosion_star,
           new_sn->pre_explosion_star->starnum,
           new_sn->pre_explosion_star->SN_type,
           new_sn->post_explosion_star->SN_type);

    if(new_sn->pre_explosion_star->SN_type != SN_NONE)
    {
        stardata->model.supernova = TRUE;

#ifdef EVENT_BASED_LOGGING
        stardata->model.event_based_logging_SN_counter++;
#endif // EVENT_BASED_LOGGING

        /*
         * Call the supernova function to handle
         * kicks, mass change, yields, orbit changes,
         * event logging, etc.
         */
        struct star_t * const star Maybe_unused = supernova(stardata,
                                                            new_sn);
        Dprint("after supernova\n");
    }
    else
    {
        free_stardata(&new_sn->pre_explosion_stardata);
        free_star(&new_sn->post_explosion_star);
        Safe_free(event->data); // == new_sn
    }
    return NULL;
}
