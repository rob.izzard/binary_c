#pragma once
#ifndef SUPERNOVA_PROTOTYPES_H
#define SUPERNOVA_PROTOTYPES_H
struct star_t * supernova(struct stardata_t * const stardata,
                          struct binary_c_new_supernova_event_t * const new_sn);
void supernova_kick(struct stardata_t * const stardata,
                    struct stardata_t * const pre_explosion_stardata,
                    struct star_t * const star,
                    struct star_t * const pre_explosion_star);

double monte_carlo_kick(struct star_t * const star,
                        struct star_t * const pre_explosion_star,
                        const double m1n,
                        const double m1,
                        const double m2,
                        double * const eccentricity,
                        double * const separation,
                        double * const jorb,
                        struct stardata_t * const stardata,
                        struct stardata_t * const pre_explosion_stardata);


void set_kick_velocity(struct stardata_t * const stardata,
                       struct star_t * const pre_explosion_star,
                       struct star_t * const post_explosion_star,
                       struct kick_system_t * const kick_system,
                       const int kick_distribution,
                       const double kick_dispersion,
                       const double kick_companion Maybe_unused);

void setup_binary_at_kick(struct stardata_t * const stardata,
                          const double semi_major_axis,
                          const double eccentricity,
                          const double system_gravitational_mass,
                          struct kick_system_t * const kick_system);

void set_new_orbit(struct stardata_t * const stardata,
                   struct stardata_t * const pre_explosion_stardata,
                   struct star_t * const pre_explosion_star,
                   struct star_t * const exploder,
                   struct star_t * const companion,
                   struct kick_system_t * const kick_system,
                   const int kick_distribution,
                   const double kick_dispersion,
                   const double kick_companion,
                   const double m1 Maybe_unused,
                   const double m1n,
                   double m2 Maybe_unused,
                   double * const jorb,
                   double * const separation,
                   double * const eccentricity);

void tauris_takens_orbit(struct stardata_t * const stardata,
                         struct stardata_t * const pre_explosion_stardata,
                         struct star_t * const pre_explosion_star,
                         struct star_t * const exploder,
                         struct star_t * const companion,
                         struct kick_system_t * const kick_system,
                         const int kick_distribution Maybe_unused,
                         const double kick_dispersion Maybe_unused,
                         const double kick_companion Maybe_unused);


double Pure_function ejection_speed(const struct star_t * Restrict const star);

struct star_t * new_supernova(struct stardata_t * const stardata,
                              struct star_t * const star,
                              struct star_t * const companion,
                              struct star_t * const newstar);
double Pure_function supernova_kinetic_energy(struct stardata_t * Restrict const stardata,
        const struct star_t * Restrict const star);
double Pure_function supernova_luminous_energy(struct stardata_t * Restrict const stardata,
        const struct star_t * Restrict const star);
double Pure_function supernova_momentum(struct stardata_t * Restrict const stardata,
                                        const struct star_t * Restrict const star);
double Pure_function supernova_ejecta_velocity(struct stardata_t * Restrict const stardata,
        const struct star_t * Restrict const star);

Event_handler_function supernova_event_handler(void * const eventp Maybe_unused,
        struct stardata_t * const stardata,
        void * data Maybe_unused);
Event_handler_function supernova_erase_event_handler(void * const eventp Maybe_unused,
        struct stardata_t * const stardata,
        void * data Maybe_unused);

double ns_bh_mass(const double M,
                  const double Mc_CO,
                  struct stardata_t * Restrict const stardata,
                  struct star_t * Restrict const star,
                  double * const M_baryonic);

void supernova_yield(struct stardata_t *stardata,
                     struct star_t * exploder,
                     struct star_t * pre_explosion_star,
                     const double dm, /* mass ejected */
                     const Stellar_type pre_explosion_companion_stellar_type
                    );
Yield_source supernova_source_id(struct star_t * exploder,
                                 struct star_t * pre_explosion_star,
                                 struct star_t * companion);

Boolean supernova_should_split_evolution(struct stardata_t * const stardata);


#ifdef PPISN
double calc_helium_core_mass(struct star_t * RESTRICT star);
void pulsational_pair_instability(struct stardata_t * RESTRICT const stardata,
                                  struct star_t * RESTRICT star,
                                  const double m,
                                  const double mc_CO,
                                  double * RESTRICT const ppisn_mremnant,
                                  double * RESTRICT const ppisn_mbaryonic,
                                  int * RESTRICT const ppisn_type);
void ppisn_belczynski_2016(struct stardata_t * RESTRICT const stardata,
                           struct star_t * RESTRICT star,
                           const double m,
                           double * RESTRICT const ppisn_mremnant,
                           double * RESTRICT const ppisn_mbaryonic,
                           int * RESTRICT const ppisn_type);
void ppisn_spera_2017(struct stardata_t * RESTRICT const stardata,
                      struct star_t * RESTRICT star,
                      const double m,
                      double * RESTRICT const ppisn_mremnant,
                      double * RESTRICT const ppisn_mbaryonic,
                      int * RESTRICT const ppisn_type);
void ppisn_farmer_2019(struct stardata_t * RESTRICT const stardata,
                       struct star_t * RESTRICT star,
                       const double m,
                       const double mc_CO,
                       double * RESTRICT const ppisn_mremnant,
                       double * RESTRICT const ppisn_mbaryonic,
                       int * RESTRICT const ppisn_type);
void ppisn_stevenson_2019(struct stardata_t * RESTRICT const stardata,
                          struct star_t * RESTRICT star,
                          const double m,
                          double * RESTRICT const ppisn_mremnant,
                          double * RESTRICT const ppisn_mbaryonic,
                          int * RESTRICT const ppisn_type);
void ppisn_mapelli_2020(struct stardata_t * RESTRICT const stardata,
                        struct star_t * RESTRICT star,
                        const double m,
                        double * RESTRICT const ppisn_mremnant,
                        double * RESTRICT const ppisn_mbaryonic,
                        int * RESTRICT const ppisn_type);
void ppisn_hendriks_2023(struct stardata_t * RESTRICT const stardata,
                         struct star_t * RESTRICT star MAYBE_UNUSED,
                         const double m MAYBE_UNUSED,
                         const double mc_CO,
                         double * RESTRICT const ppisn_mremnant,
                         double * RESTRICT const ppisn_mbaryonic,
                         int * RESTRICT const ppisn_type);
#endif // PPISN

double Pure_function supernova_kinetic_energy_Marassi_2019(struct stardata_t * Restrict const stardata,
        const struct star_t * Restrict const star);
void supernova_load_Marassi_2019(struct stardata_t * Restrict const stardata);

void supernova_free_event_log_data(void ** data);
Boolean star_can_DDet(struct stardata_t * const stardata,
                      struct star_t * const star);

#endif // SUPERNOVA_PROTOTYPES_H
