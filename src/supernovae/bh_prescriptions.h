#pragma once
#ifndef BH_PRESCRIPTIONS_H
#define BH_PRESCRIPTIONS_H

/* Black Hole mass prescritions */
#include "bh_prescriptions.def"
#undef X
#define X(CODE) BH_##CODE ,
enum { BH_PRESCRIPTIONS_LIST };


#undef X

#ifdef PPISN
// PPISN prescriptions
#define X(CODE) PPISN_##CODE ,
enum { PPISN_PRESCRIPTIONS_LIST };


#undef X
#endif // PPISN

#endif // BH_PRESCRIPTIONS_H
