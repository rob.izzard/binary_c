#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "supernovae.h"
#include "sn.h"
/*
 * Do supernova :
 * remove mass, update the stellar type, kick the star
 */

struct star_t * supernova(struct stardata_t * const stardata,
                          struct binary_c_new_supernova_event_t * const new_sn)
{
    Dprint("supernova new_sn = %p\n",
           (void*)new_sn);

    if(new_sn == NULL)
    {
        Dprint("supernova called with new_supernova == NULL\n");
        return NULL;
    }

    /*
     * Which star in the current stardata is the star
     * that has triggered the SN?
     *
     * We try to match the star number, but that can fail
     * when there has been a merger or disruption, in which
     * case go for the other star.
     *
     * In almost all cases, new_sn->pre_explosion is the star
     * that is blowing up.
     */

    /*
     * Save the pre-explosion state of the system
     * (e.g. required to compute the new orbit).
     */
    new_sn->pre_explosion_stardata = New_stardata_from(stardata);

    /*
     * The star that will explode.
     *
     * Note: this is the actual star in stardata.
     */
    struct star_t * const star =
        new_sn->pre_explosion_star->stellar_type != MASSLESS_REMNANT ?
        new_sn->pre_explosion_star :
        Other_star_struct(new_sn->pre_explosion_star);

    if((star->stellar_type == NEUTRON_STAR && star->SN_type != SN_AIC_BH) ||
       star->stellar_type == BLACK_HOLE ||
       star->stellar_type == MASSLESS_REMNANT)
    {
        Exit_binary_c(BINARY_C_ALGORITHM_BRANCH_FAILURE,
                      "Exploding star is a NS, BH or massless (pre explosion %d). Stardata's stars have types %d %d. SN is type %d (%s). This is an error.",
                      new_sn->pre_explosion_star->stellar_type,
                      stardata->star[0].stellar_type,
                      stardata->star[1].stellar_type,
                      star->SN_type,
                      SN_String(star->SN_type));
    }

    /*
     * Save a copy of the pre-explosion star:
     * we need this e.g. for the yield computations
     */
    struct star_t * const pre_explosion_star_copy = New_star_from(star);

    Dprint("pre-explosion (type %d) structure: m0=%g mc=%g mc_1tp=%g stellar_type = %d\n",
           star->SN_type,
           star->phase_start_mass,
           Outermost_core_mass(star),
           star->phase_start_core_mass,
           star->stellar_type);

    /*
     * Shortcut to the companion
     */
    struct star_t * const companion Maybe_unused = Other_star_struct(star);

    /*
     * Compute mass to be ejected.
     * We save this in the star because it's used in the
     * SN energy, momentum and yield functions.
     *
     * Note: we should use the baryonic mass, as the mass
     * ejected goes into the yield.
     */
    Dprint("Post explosion masses gravitational %g baryonic %g\n",
           new_sn->post_explosion_star->mass,
           new_sn->post_explosion_star->baryonic_mass);
    pre_explosion_star_copy->SN_mass_ejected =
        star->SN_mass_ejected =
        Max(0.0,star->mass - new_sn->post_explosion_star->baryonic_mass);

    Dprint("Supernova in star %d stellar_type %d %d %d SN_type %s (%d) mgrav = %g mbary = %g (mass pre %g, post %g) eject %g\n",
           star->starnum,
           star->stellar_type,
           new_sn->pre_explosion_star->stellar_type,
           new_sn->post_explosion_star->stellar_type,

           SN_String(star->SN_type),
           star->SN_type,
           star->mass,
           star->baryonic_mass,
           new_sn->pre_explosion_star->mass,
           new_sn->post_explosion_star->mass,
           star->SN_mass_ejected
          );

    if(unlikely(star->SN_mass_ejected < -TINY))
    {
        /*
         * Mass ejected must be positive
         */
        Exit_binary_c(BINARY_C_MASS_OUT_OF_RANGE,
                      "supernova() called with negative mass_ejected\n");
    }

#ifdef LOG_SUPERNOVAE
    /*
     * Note that log_sn has been deprecated : you
     * should catch supernovae in log_every_timestep, not here
     */
    log_sn(stardata, star->starnum, star->SN_type, PRE_SN);
#endif

    /*
     * Update the stellar structure to become
     * a post-SN object (NS, BH or MASSLESS_REMNANT).
     * Its mass and stellar type certainly change.
     * We update its radius and luminosity below.
     */
    set_star_struct_from_stellar_structure(new_sn->post_explosion_star,
                                           star);
    set_no_core(star);
    const Core_type core_type = ID_core(star->stellar_type);
    if(core_type != CORE_NONE)
    {
        star->core_mass[core_type] = star->mass;
        star->phase_start_core_mass = star->core_mass[core_type];
    }
    star->core_radius = star->radius;
    star->stellar_timestep = star->stellar_type == MASSLESS_REMNANT ? 1e3 : 0.01;
    star->phase_start_mass = star->mass;
    star->age = 0.0;
    star->epoch = stardata->model.time - star->age;
    star->mass_at_RLOF_start = star->phase_start_mass;
    star->hybrid_HeCOWD = FALSE;

    /*
     * Update stellar structure
     */
    Dprint("call stellar structure Mgrav=%g Mbary=%g\n",
           star->mass,
           star->baryonic_mass);
    stellar_structure(stardata,
                      STELLAR_STRUCTURE_CALLER_supernova,
                      star,
                      NULL);
    Dprint("post-explosion structure: m(grav)=%g m(baryon)=%g m0=%g mc=%g mc_1tp=%g stellar_type = %d\n",
           star->mass,
           star->baryonic_mass,
           star->phase_start_mass,
           star->core_mass[core_type],
           star->mc_1tp,
           star->stellar_type);

#ifdef PPISN
    star->undergone_ppisn_type = new_sn->post_explosion_star->undergone_ppisn_type;
#endif // PPISN

    if(POST_SN_OBJECT(star->stellar_type))
    {

        /*
         * Maximum angular momentum a star can have: note that
         * the breakup_angular_momentum function uses the Kerr
         * solution for black holes
         */
        const double Jmax =
            breakup_angular_momentum(stardata,
                                     star);

        /*
         * Spin rate of the post-SN object.
         *
         * Assume it has the same specific angular momentum
         * as the pre-explosion star.
         */
        const double l =
            pre_explosion_star_copy->angular_momentum /
            pre_explosion_star_copy->mass;

        /*
         * Hence the new total angular momentum
         */
        star->angular_momentum = Min(Jmax, l * star->mass);

        if(star->stellar_type == BLACK_HOLE)
        {
            star->omega = bh_angular_velocity(star->mass,
                                              star->angular_momentum);
        }
        else
        {
            star->omega = star->angular_momentum /
                          moment_of_inertia(stardata,
                                            star,
                                            star->radius);
        }
    }

    /*
     * Kick the remnant
     */
    Dprint("Pre-kick SN type %d, exploder type %d %d, companion type %d (single? %s)\n",
           star->SN_type,
           new_sn->pre_explosion_star->stellar_type,
           new_sn->post_explosion_star->stellar_type,
           companion->stellar_type,
           Yesno(stardata->model.sgl));
    supernova_kick(stardata,
                   new_sn->pre_explosion_stardata,
                   star,
                   pre_explosion_star_copy);

    /*
     * Determine if the system is single or binary
     */
    stardata->model.sgl = System_is_single;
    Dprint("Is post-SN system single? %s\n",Yesno(stardata->model.sgl));

    /*
     * Nucleosynthesis
     */
#if defined NUCSYN
    Dprint("Yield SN type %d mass %g\n",
           star->SN_type,
           star->SN_mass_ejected);
    Append_logstring(LOG_SN_AND_KICK,
                     ", dm(exploder) = %g, dm(companion) = %g",
                     star->SN_mass_ejected,
                     companion->dm_companion_SN);
#endif // NUCSYN


    if(star->SN_mass_ejected > 0.0)
    {
        supernova_yield(
            stardata,
            star,
            pre_explosion_star_copy,
            star->SN_mass_ejected,
            companion->stellar_type
            );
    }

    /*
     * Logging
     */
    Dprint("Logging\n");
#ifdef LOG_SUPERNOVAE
    log_sn(stardata, star->starnum, star->SN_type, POST_SN);
#endif
#if(defined SHORT_SUPERNOVA_LOG ||             \
     defined SUPERNOVA_COMPANION_LOG)           \
    && defined FILE_LOG
    /*
     * Before we do anything, log the supernova
     * Note that this happens EVEN IF NUCSYN IS NOT DEFINED
     */
    log_supernova(star->SN_type,
                  stardata->model.log_fp,
                  star->starnum,
                  star->SN_mass_ejected,
                  Max(0.0, star->SN_mass_ejected - pre_explosion_star->menv),
                  stardata);
#endif

#if defined DISCS && defined DISC_LOG_POPSYN
    Dprint("Logging (popsyn)\n");
    for (int i = 0; i < stardata->common.ndiscs; i++)
    {
        struct disc_t * const disc = & stardata->common.discs[i];
        struct binary_system_t binary;
        disc_init_binary_structure(stardata, &binary, disc);
    }
#endif // DISCS



    /*
     * Check for broken system
     */
    if(System_is_single)
    {
        stardata->model.sgl = TRUE;
    }
    else
    {
        /* set the new period from the post-SN system mass */
        stardata->common.orbit.period =
            Is_zero(stardata->common.orbit.separation) ? 0.0 :
            ((stardata->common.orbit.separation / AU_IN_SOLAR_RADII)
             * sqrt(stardata->common.orbit.separation / (AU_IN_SOLAR_RADII * (stardata->star[0].mass + stardata->star[1].mass))));

        /* and the orbital angular frequency */
        stardata->common.orbit.angular_frequency =
            Is_zero(stardata->common.orbit.separation) ? 0.0 :
            (TWOPI / stardata->common.orbit.period);
    }

    /*
     * Convert pointer to a copy
     */
    struct binary_c_new_supernova_event_t * new_sn_copy =
        Malloc(sizeof(struct binary_c_new_supernova_event_t));
    memcpy(new_sn_copy,
           new_sn,
           sizeof(struct binary_c_new_supernova_event_t));
    new_sn_copy->pre_explosion_star = pre_explosion_star_copy;
    Add_event_log_data(star->starnum == 0 ? BINARY_C_EVENT_SUPERNOVA0 : BINARY_C_EVENT_SUPERNOVA1,
                       star->starnum,
                       new_sn_copy,
                       &supernova_free_event_log_data);
    Dprint("post - SN stellar types %d %d\n",
           stardata->star[0].stellar_type,
           stardata->star[1].stellar_type);
    return star;
}
