#include "../binary_c.h"
No_empty_translation_unit_warning;

double ns_bh_mass(const double M,
                  const double Mc_CO,
                  struct stardata_t * Restrict const stardata,
                  struct star_t * Restrict const star,
                  double * const M_baryonic)
{
    /*
     * Function to evaluate the NS/BH remnant mass given
     * total mass, M, and CO Core mass, Mc_CO.
     *
     * We return the gravitational mass because
     * this goes into orbital calculations.
     *
     * The baryonic mass is set in *M_baryonic.
     */
    double M_remnant;

    /*
     * If we are a TPAGB star, assume the core collapses to
     * a NS in an electron-capture supernova. This should
     * only happen in unusual circumstances or as an STPAGB star.
     */
    if(star->stellar_type == TPAGB)
    {
        *M_baryonic = star->core_mass[CORE_He];
        M_remnant = baryonic_to_gravitational_remnant_mass(stardata,
                                                                     star,
                                                                     *M_baryonic);
    }
    else if(stardata->preferences->BH_prescription == BH_HURLEY2002)
    {
        /*
         * Chris' NS/BH formula as in Hurley+ 2000, 2002
         */
        M_remnant = 1.170 + 0.090 * Mc_CO;
        *M_baryonic = M_remnant;
    }
    else if(stardata->preferences->BH_prescription == BH_BELCZYNSKI)
    {
        /*
         * Use NS/BH mass given by Belczynski et al. 2002, ApJ, 572, 407.
         */
        const double Mc_x =
            Mc_CO < 2.5 ? (0.1617670 * Mc_CO + 1.0670550) :
            (0.3141540 * Mc_CO + 0.6860880);
        *M_baryonic  =
            Mc_CO <= 5.0 ? Mc_x :
            Mc_CO < 7.60 ? (Mc_x + (Mc_CO - 5.0) * (M - Mc_x) / 2.60) :
            M;
        M_remnant = baryonic_to_gravitational_remnant_mass(stardata,
                                                                     star,
                                                                     *M_baryonic);
    }
    else if(stardata->preferences->BH_prescription == BH_SPERA2015)
    {
        /*
         * Black hole mass from Spera, Mapelli and Bressan 2015
         * (MNRAS 451, 4086, astro-ph 1505.05201) as a function
         * of the CO core mass and the metallicity.
         *
         * The following follows their Appendix C fitting
         * formulae.
         */
        double p;
        if(stardata->common.metallicity <= 5e-4)
        {
            /* Z < 5e-4 */
            p = -2.333 + 0.1559 * Mc_CO + 0.2700 * Mc_CO * Mc_CO; // Eq. C2
            if(Mc_CO <= 5.0)
            {
                M_remnant = Max(p, 1.27); // Eq. C1 MCO < 5
            }
            else if(Mc_CO < 10.0)
            {
                M_remnant = p; // Eq. C1 5 < MCO < 10
            }
            else
            {
                const double mZ = -6.476e2 * stardata->common.metallicity + 1.911; // Eq. C3
                const double qZ = 2.300e3 * stardata->common.metallicity + 11.67; // Eq. C3
                const double f = mZ * Mc_CO + qZ; // Eq. C2
                M_remnant = Min(p, f); // Eq. C1 MCO > 10
            }
        }
        else
        {
            /* Z > 5e-4 */
            double A1, A2, L, eta;
            if(stardata->common.metallicity >= 1e-3)
            {
                /* Z >= 1e-3 : Eq. C6 */
                A1 = 1.340 - 29.49 /
                     (1.0 + pow(stardata->common.metallicity / 1.110e-3, 2.361));
                A2 = 80.22 - 74.73 * pow(stardata->common.metallicity, 0.965) /
                     (2.720e-3 + pow(stardata->common.metallicity, 0.965));
                L = 5.683 + 3.533 /
                    (1.0 + pow(stardata->common.metallicity / 7.430e-3, 1.993));
                eta = 1.066 - 1.121 /
                      (1.0 + pow(stardata->common.metallicity / 2.558e-2, 0.609));
            }
            else
            {
                /* Z < 1e-3 : Eq. C7 */
                A1 = 1.105e5 * stardata->common.metallicity - 1.258e2;
                A2 = 91.56 - 1.957e4 * stardata->common.metallicity - 1.558e7 * Pow2(stardata->common.metallicity);
                L = 1.134e4 * stardata->common.metallicity - 2.143;
                eta = 3.090e-2 - 22.30 * stardata->common.metallicity + 7.363e4 * Pow2(stardata->common.metallicity);
            }

            /* Eq. C5 */
            const double h = A1 + (A2 - A1) / (1.0 + exp10( (L - Mc_CO) * eta));
            if(Mc_CO <= 5.0)
            {
                M_remnant = Max(h, 1.27); // Eq. C4 MCO < 5
            }
            else if(Mc_CO < 10.0)
            {
                M_remnant = h;  // Eq. C4 5 < MCO < 10
            }
            else
            {
                double mZ, qZ;

                if(stardata->common.metallicity >= 2e-3)
                {
                    /* Eq. C8, Z >= 2e-3 */
                    mZ = 1.217;
                    qZ = 1.061;
                }
                else if(stardata->common.metallicity >= 1e-3)
                {
                    /* Eq. C9, 1e-3 < Z < 2e-3 */
                    mZ = -43.82 * stardata->common.metallicity + 1.304;
                    qZ = -1.296e4 * stardata->common.metallicity + 26.98;
                }
                else
                {
                    /* Eq. C10, Z < 1e-3 */
                    mZ = -6.476e2 * stardata->common.metallicity + 1.911;
                    qZ = 2.300e3 * stardata->common.metallicity + 11.67;
                }

                const double f = mZ * Mc_CO + qZ; // Eq. C5
                M_remnant = Max(h, f); // Eq. C4 MCO > 10
            }
        }

        /* BH mass cannot exceed the total stellar mass */
        *M_baryonic = Min(M, M_remnant);
        M_remnant = baryonic_to_gravitational_remnant_mass(
            stardata,
            star,
            *M_baryonic);
    }
    else if(stardata->preferences->BH_prescription == BH_FRYER12_STARTRACK)
    {
        /*
         * Fryer et al. (ApJ 749,91 2012) startrack prescription
         */
        const double M_proto = Fryer2012_Eq10(Mc_CO);
        const double M_fallback = Fryer2012_Eq11(M, Mc_CO, M_proto);
        *M_baryonic = Fryer2012_Eq12(M_proto, M_fallback);
        M_remnant = baryonic_to_gravitational_remnant_mass(
                        stardata,
                        star,
                        *M_baryonic
                    );

        Dprint("Fryer startrack Mc_CO %g M %g -> M_proto %g, M_fallback %g, M_baryonic %g -> M_remn-ant(grav) = %g\n",
               Mc_CO,
               M,
               M_proto,
               M_fallback,
               *M_baryonic,
               M_remnant);

#ifdef EVENT_BASED_LOGGING
        star->fallback = Fryer2012_f_fb(stardata, M, Mc_CO);
        star->fallback_mass = M_fallback;
#endif // EVENT_BASED_LOGGING

    }
    else if(stardata->preferences->BH_prescription == BH_FRYER12_RAPID)
    {
        /*
         * Fryer et al. (ApJ 749,91 2012) rapid prescription
         */
        const double M_proto = Fryer2012_Eq15(Mc_CO);
        const double M_fallback = Fryer2012_Eq16(M, Mc_CO, M_proto);
        *M_baryonic = Fryer2012_Eq17(M_proto, M_fallback);

        M_remnant = baryonic_to_gravitational_remnant_mass(
                        stardata,
                        star,
                        *M_baryonic
                    );

        Dprint("Fryer rapid Mc_CO %g M %g -> M_proto %g, M_fallback %g, M_baryonic %g -> M_remn-ant(grav) = %g\n",
               Mc_CO,
               M,
               M_proto,
               M_fallback,
               *M_baryonic,
               M_remnant);

#ifdef EVENT_BASED_LOGGING
        star->fallback = Fryer2012_f_fb(stardata, M, Mc_CO);
        star->fallback_mass = M_fallback;
#endif // EVENT_BASED_LOGGING

    }
    else if(stardata->preferences->BH_prescription == BH_FRYER12_DELAYED)
    {
        /*
         * Fryer et al. (ApJ 749,91 2012) delayed prescription
         */
        const double M_proto = Fryer2012_Eq18(Mc_CO);
        const double M_fallback = Fryer2012_Eq19(M, Mc_CO, M_proto);
        *M_baryonic = Fryer2012_Eq20(M_proto, M_fallback);

        M_remnant = baryonic_to_gravitational_remnant_mass(
                        stardata,
                        star,
                        *M_baryonic);

        Dprint("Fryer delayed Mc_CO %g M %g -> M_proto %g, M_fallback %g, M_baryonic %g -> M_remn-ant(grav) = %g (binding-mass-energy %g)\n",
               Mc_CO,
               M,
               M_proto,
               M_fallback,
               *M_baryonic,
               M_remnant,
               *M_baryonic - M_remnant);

#ifdef EVENT_BASED_LOGGING // Add fallback info to the star
        star->fallback = Fryer2012_f_fb(stardata, M, Mc_CO);
        star->fallback_mass = M_fallback;
#endif // EVENT_BASED_LOGGING
    }
    else if(stardata->preferences->BH_prescription == BH_DARK)
    {
        /*
         * All mass swallowed
         */
        *M_baryonic = M;
        M_remnant = baryonic_to_gravitational_remnant_mass(
                        stardata,
                        star,
                        *M_baryonic);
    }
    else if(stardata->preferences->BH_prescription == BH_MARASSI2019)
    {
        /*
         * Marassi et al. (2019)
         * Remnant mass as a function of MHe.
         */
        supernova_load_Marassi_2019(stardata);
        const double params[] =
        {
            log10(stardata->common.metallicity / 0.0142),
            Hydrogen_burnt_core_mass(star)
        };
        double result[5];
        Interpolate(stardata->persistent_data->Marassi_2019_mapped,
                    params,
                    result,
                    FALSE);
        *M_baryonic =  result[1];
        M_remnant = baryonic_to_gravitational_remnant_mass(
                        stardata,
                        star,
                        *M_baryonic);
    }
    else
    {
        Exit_binary_c(BINARY_C_ALGORITHM_OUT_OF_RANGE,
                      "BH prescription %d is unknown",
                      stardata->preferences->BH_prescription);
    }

    /*
     * Make sure non-physical solutions cannot happen
     */
    Clamp(M_remnant,
          0.0,
          M);
    Clamp(*M_baryonic,
          0.0,
          M);


    Dprint("NS/BH mass from M = %g, MCO = %g, Z = %g -> M_remn-ant = %g\n", M, Mc_CO, stardata->common.metallicity, M_remnant);

    return M_remnant;
}
