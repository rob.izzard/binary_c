#include "../binary_c.h"
No_empty_translation_unit_warning;
/*
 * Load the Marassi et al. (2019) data
 *
 * We use their data that best fits the observed Ni56,
 * NOT the fixed explosion energy data.
 */

void supernova_load_Marassi_2019(struct stardata_t * Restrict const stardata)
{
    if(stardata->persistent_data->Marassi_2019_mapped == NULL)
    {
        Load_and_remap_table(
            stardata->persistent_data->Marassi_2019_mapped,
            "src/supernovae/supernova_Marassi_2019.dat",
            /*
             * Parameters:
             * 0 [Fe/H]
             * 6 MHe
             *
             * Data
             * 4 Eexp/foe
             * 5 Remnant mass/Msun
             * 7 Mejected/Msun
             * 8 MNi56/Msun
             * 9 Mdust/Msun
             */
            Wanted_cols(
                /* parameters */
                0,  6,
                /* data */
                4,  5,  7,  8,  9
                ),
            New_res( 0, 10 ),
            0,
            TRUE
            );
    }
}
