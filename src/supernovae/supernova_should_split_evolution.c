#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "supernovae.h"

Boolean supernova_should_split_evolution(struct stardata_t * const stardata)
{
    /*
     * This function returns TRUE if evolution splitting
     * should happen. This is based on a possibly complicated
     * set of logical decisions.
     *
     * Note: we must check both stars for supernovae,
     *       and we should use the previous_stardata to
     *       check the orbit (e.g. eccentricity, duplicity)
     *       because the stardata we have now has already
     *       had a kick if there was a suitable supernova.
     */

    /*
     * We don't need to split single stars: their
     * orbit cannot be sampled and their kick
     * speed is (probably) irrelevant.
     */
    if(System_was_binary)
    {
        Foreach_star(star)
        {
            if(star->SN_type != SN_NONE)
            {
                /*
                 * If the orbit is eccentric, we should split to
                 * sample different anomalies regardless of the
                 * kick speed.
                 */
                if(stardata->previous_stardata->common.orbit.eccentricity >
                   stardata->preferences->evolution_splitting_sn_eccentricity_threshold)
                {
                    return TRUE;
                }

                /*
                 * If there is a Maxwellian kick, with non-zero dispersion,
                 * splitting should happen because the kick speed is random.
                 */
                else if(
                    stardata->preferences->sn_kick_distribution[star->SN_type] == KICK_VELOCITY_MAXWELLIAN
                    &&
                    Is_not_zero(stardata->preferences->sn_kick_dispersion[star->SN_type])
                    )
                {
                    return TRUE;
                }

                /*
                 * If the kick is a custom function, we should split just
                 * in case.
                 */
                else if(stardata->preferences->sn_kick_distribution[star->SN_type] == KICK_VELOCITY_CUSTOM)
                {
                    return TRUE;
                }
            }
        }
    }

    /*
     * ... no criteria are satisfied, so no splitting is required.
     */
    return FALSE;
}
