#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "supernovae.h"

/* custom kick functions */
static double Maybe_unused maxwellian_kick_pdf(double v, double *params);
//static double maxwellian2_kick_pdf(double v,double *params);
//static double maxwellian3_kick_pdf(double v,double *params);
//static double kick_pdf(double v,double *params); // demo
static double Maybe_unused hartman_kick_pdf(double v, double *params);

/*
 * flags to turn on distribution test code:
 * these test either the kick speed or direction
 * by running the routine NTESTS times, outputting
 * the results (which you can bin and plot)
 */
//#define TEST_KICK_SPEED_DISTRIBUTION
//#define TEST_KICK_DIRECTIONS
#define NTESTS 500000
void set_kick_velocity(struct stardata_t * const stardata,
                       struct star_t * const pre_explosion_star,
                       struct star_t * const post_explosion_star,
                       struct kick_system_t * const kick_system,
                       const int kick_distribution,
                       const double kick_dispersion,
                       const double kick_companion Maybe_unused)
{
    double vk, vk2, phi, omega = 0.0, sphi;
    Random_seed * Restrict random_seed = & stardata->common.random_seed;
    if(kick_distribution == KICK_VELOCITY_MAXWELLIAN ||
       kick_distribution == KICK_VELOCITY_GIACOBBO_MAPELLI_2020)
    {
        /*
         * Generate Kick Velocity using Maxwellian Distribution (Phinney 1992).
         * Use Henon's method for pairwise components (Douglas Heggie 22/5/97).
         */
#if defined TEST_KICK_SPEED_DISTRIBUTION || defined TEST_KICK_DIRECTIONS
        for (int il = 0; il < NTESTS; il++)
        {
#endif
            double u1 = 0.0;
            if(Is_zero(kick_dispersion))
            {
                vk = vk2 = 0.0;
            }
            else
            {
                double v[5]; // v is the kick velocity vector
                unsigned int k;
                for (k = 1; k < 3; k++)
                {
                    u1 = (double)random_number(stardata, random_seed);
                    const double u2 = (double)random_number(stardata, random_seed);
                    Dprint("u1=%g, u2=%g\n", u1, u2);

                    /*
                     * Generate two velocities from polar coordinates S & OMEGA.
                     */
                    const double s = kick_dispersion * sqrt(-2.0 * log(1.0 - u1));
                    omega = TWOPI * u2;
                    v[2 * k - 1] = s * cos(omega);
                    v[2 * k] = s * sin(omega);
                    Dprint("v[%u]=%g, v[%u]=%g\n", 2 * k - 1, v[2 * k - 1], 2 * k, v[2 * k]);
                }
                vk2 = Pythag3_squared(v[1], v[2], v[3]);
                vk = sqrt(vk2);
            }

            if(kick_distribution == KICK_VELOCITY_GIACOBBO_MAPELLI_2020)
            {
                /*
                 * Giacobbo and Mapelli (2020, ApJ 891,141) scale the kick
                 * velocity to the mass ejected in Eq. 1.
                 *
                 * The mean_M_ej and mean_M_NS are defined in their paper.
                 */
                static const double mean_M_ej = 9.0;
                static const double mean_M_NS = 1.2;
                const double M_ej = pre_explosion_star->mass - post_explosion_star->mass;
                const double f = M_ej / mean_M_ej * mean_M_NS / post_explosion_star->mass;
                vk *= f;
            }

            /*
             * core_mass[CORE_CO] should be the CO core mass in Fryer
             * model, and kick is rescaled assuming spherical
             * fallback (Mathieu Renzo 2016)
             */
            if(stardata->preferences->BH_prescription == BH_FRYER12_DELAYED ||
               stardata->preferences->BH_prescription == BH_FRYER12_RAPID ||
               stardata->preferences->BH_prescription == BH_FRYER12_STARTRACK)
            {
                const double fb = Fryer2012_f_fb(stardata,
                                                 pre_explosion_star->mass,
                                                 pre_explosion_star->core_mass[CORE_CO]);
                vk *= 1.0 - fb;

                /*
                 * at this point we have a vk given by the asymmetries
                 * in the SN ejecta (and possibly neutrino emission)
                 * chosen in a monte-carlo way from a Maxwellian distribution,
                 * and re-scaled assuming some spherical fallback.
                 *
                 * The fallback fraction is calculated according to Fryer et al. 2012
                 *  algorithms (src/zfunc/ns_bh_mass.c)
                 */

#ifdef PPISN
                /*
                 * Check if the star would undergo a >=PPISN supernova.
                 * No kicks for those.
                 */
                double ppisn_mremnant = -1.0;
                double ppisn_mbaryonic = -1.0;
                int ppisn_type = PPISN_TYPE_NONE;

                pulsational_pair_instability(
                    stardata,
                    pre_explosion_star,
                    pre_explosion_star->mass,
                    pre_explosion_star->core_mass[CORE_CO],
                    &ppisn_mremnant,
                    &ppisn_mbaryonic,
                    &ppisn_type
                );
                Dprint("m: %g ppisn_type: %d kick velocity: %g\n", pre_explosion_star->mass, ppisn_type, vk);

                if(In_range(ppisn_type, PPISN_TYPE_PPI, PPISN_TYPE_PHDIS))
                {
                    vk = 0;
                    Dprint("Setting kick from %g to 0 because PPISN type is %d\n", vk, ppisn_type);
                }
#endif // PPISN

            }
            SNprint("SN vkick = %g\n", vk);

            /* angle */
            sphi = -1.0 + 2.0 * u1;
            phi = asin(sphi);

#ifdef TEST_KICK_SPEED_DISTRIBUTION
            /* testing */
            // gnuplot line:
            //pi=3.14159; s=190; plot "./vk.bin" u 1:($2/1e4), sqrt(2.0/pi) * x*x/(s*s*s) * exp(-x*x/(2.0*s*s))
            printf("VK %g 1.0\n", vk);
#endif
#ifdef TEST_KICK_DIRECTIONS
            /* 3D spherical vector */
            printf("DIRK %g %g %g\n", sin(omega)*cos(phi), sin(omega)*sin(phi), cos(omega));
#endif

#if(defined(TEST_KICK_SPEED_DISTRIBUTION)||defined(TEST_KICK_DIRECTIONS))
        }
        Exit_binary_c(BINARY_C_NORMAL_EXIT, "");
#endif
    }
    else if(kick_distribution == KICK_VELOCITY_CUSTOM)
    {

        /*
         * Custom kick function : your function should be defined at
         * the end of this source file and chosen here e.g.
         *
         * double (*kick_function) (double,double*) = &maxwellian_kick_pdf;
         * double params[1]={kick_dispersion}
         *
         * where the double* points to a list of parameters of arbitrary length
         */

        /************************************************************/

        /*
         * Standard Maxwellian single-parameter kick speed distribution,
         * as in the original BSE code (e.g. Hansen and Phinney 1997)
         */
        double (*kick_function) (double, double*) = &maxwellian_kick_pdf;
        double params[1] = {kick_dispersion};

        /* two maxwellians (4 parameters : 2 dispersions, 2 weights) */
        //double (*kick_function) (double,double*) = &maxwellian2_kick_pdf;
        //double params[4]={0.5,5.0,0.5,100.0};

        /* three maxwellians (6 parameters : 3 dispersions, 3 weights) */
        //double (*kick_function) (double,double*) = &maxwellian3_kick_pdf;
        //double params[6]={0.5,5.0,0.5,100.0,0.4,800};

        /*
         * Hartman (1997) distribution as used by Simon Portegies-Zwart (2000)
         */
        //double (*kick_function) (double,double*) = &hartman_kick_pdf;
        //double params[1]={sigma};

        /************************************************************/

        // resoluion in v-space : 1km/s is usually enough (binary_c_macros.h)
        const double dv = KICK_VELOCITY_RESOLUTION;
        double cdf; // cumulative probability density (used often)

#ifdef KICK_CDF_TABLE
        /* space for the CDF interpolation table containing ntable*2 elements */

        const int ntable = (int)((double)MAX_KICK_VELOCITY /
                                 (double)KICK_VELOCITY_RESOLUTION + 0.2);
#endif



        const double dvhalf = 0.5 * dv; // used lots, calculate once

        /*
         * First time only:
         * Force normalization of kick PDF just in case you
         * were too lazy to normalize the function yourself
         */
        if(Is_zero(stardata->common.monte_carlo_kick_normfac))
        {
            vk = 0.0;
            cdf = 0.0;
            while (vk < MAX_KICK_VELOCITY)
            {
                vk += dvhalf;
                cdf += dv * kick_function(vk, params);
                vk += dvhalf;
            }
            stardata->common.monte_carlo_kick_normfac = 1.0 / cdf;
        }
        const double dvn = dv * stardata->common.monte_carlo_kick_normfac; // used lots, save time by doing it once

#ifdef KICK_CDF_TABLE
        /*
         * Set up a table so we can simply look up the
         * cumulative distribution function (much faster (~20x!) than
         * calculating it every time but uses up RAM)
         */
        if(stardata->tmpstore->cdf_table == NULL)
        {
            /* allocate and make the table */

            /* integrate PDF to calculate CDF and fill in the table */
            cdf = 0.0;
            vk = 0.0;
            double *p = Malloc(sizeof(double) * 2 * ntable);
            double *q = p;
            while (vk < MAX_KICK_VELOCITY)
            {
                vk += dvhalf; // centred
                cdf += dvn * kick_function(vk, params);
#ifdef KICK_CDF_TABLE_USELOG
                *(q++) = log10(cdf);
#else
                *(q++) = cdf;
#endif
                *(q++) = vk;
                vk += dvhalf; // centred
            }

            NewDataTable_from_Pointer(p,
                                      stardata->tmpstore->cdf_table,
                                      1,
                                      1,
                                      ntable);
        }
#endif

#if defined TEST_KICK_SPEED_DISTRIBUTION || defined TEST_KICK_DIRECTIONS
        for (int il = 0; il < NTESTS; il++)
        {
#endif
            /*
             * Random number to sample the cumulative distribution function (CDF)
             */
            double r = random_number(stardata, random_seed);

            /*
             * calculate CDF up to value r but do not
             * allow vk to exceed MAX_KICK_VELOCITY
             * (just in case things go wrong!)
             */
#ifdef KICK_CDF_TABLE
            /* use lookup table to do the calculation (this is usually faster) */

            /*
             * choose whether to log the value or not,
             * it might be better to at the ends of the distributions
             * where the CDF changes little
             */

            double x[1];
#ifdef KICK_CDF_TABLE_USELOG
            x[0] = log10(r);
#else
            x[0] = r;
#endif
            Interpolate(stardata->tmpstore->cdf_table,
                        x, &vk,
                        FALSE);
#else
            /* manual integration : beware this is SLOW but good for debugging */
            vk = 0.0;
            cdf = 0.0;
            const double dvsmall = dv * 0.01;
            const double dvnsmall = dvn * 0.01;
            while (cdf < r)
            {
                cdf += dvnsmall * kick_function(vk, params);
                if(vk > MAX_KICK_VELOCITY) break; // break condition just in case!
                vk += dvsmall;
            }
            printf("manual integration vk=%g\n", vk);
#endif

#ifdef TEST_KICK_SPEED_DISTRIBUTION
            // gnuplot line:
            //pi=3.14159; s=190; plot "./vk.bin" u 1:($2/1e4), sqrt(2.0/pi) * x*x/(s*s*s) * exp(-x*x/(2.0*s*s))
            printf("VK %g 1.0\n", vk);
        }
        Exit_binary_c(BINARY_C_NORMAL_EXIT, "");
#endif

        //vk2=Pow2(vk);
        Dprint("Custom kick R=%g > vk=%g\n", r, vk);

        /*
         * Angle
         */
        {
            const double u1 = (double)random_number(stardata, random_seed);
            const double u2 = (double)random_number(stardata, random_seed);
            omega = TWOPI * u2;
            sphi = -1.0 + 2.0 * u1;
            phi = asin(sphi);
        }


    }
    else if(kick_distribution == KICK_VELOCITY_FIXED)
    {
        /*
         * Kick velocity is fixed but we require a direction
         */
        vk = kick_dispersion;
        vk2 = Pow2(vk);
        const double u1 = (double)random_number(stardata, random_seed);
        const double u2 = (double)random_number(stardata, random_seed);

#ifdef WD_KICKS
        /* choose WD kick direction */
        switch (stardata->preferences->wd_kick_direction)
        {
        case KICK_STRAIGHT_UP:
        {
            Dprint("KICK STRAIGHT UP\n");
            /* phi=pi/2 omega=undef */
            phi = PI * 0.5;
            sphi = sin(phi);
            omega = 0.0;
            break;
        }
        case KICK_FORWARD:
        {
            Dprint("KICK FORWARD\n");
            /* phi = 0, omega=pi */
            phi = 0.0;
            sphi = 0.0;
            omega = PI;
            break;
        }
        case KICK_BACKWARD:
        {
            Dprint("KICK BACKWARD\n");
            /* phi = 0, omega = 0 */
            phi = 0.0;
            sphi = 0.0;
            omega = 0.0;
            break;
        }
        case KICK_INWARD:
        {
            Dprint("KICK INWARD\n");
            /* phi = 0, omega = -pi/2 */
            phi = 0.0;
            sphi = 0.0;
            omega = -PI * 0.5;
            break;
        }
        case KICK_OUTWARD:
        {
            Dprint("KICK OUTWARD\n");
            /* phi = 0, omega = +pi/2 */
            phi = 0.0;
            sphi = 0.0;
            omega = +PI * 0.5;
            break;
        }
        default:
        case KICK_RANDOM:
        {
            Dprint("KICK RANDOM\n");
            omega = TWOPI * u2;
            /* phi more likely in orbital plane */
            sphi = -1.0 + 2.0 * u1;
            phi = asin(sphi);
        }
        }
#else
        Dprint("KICK RANDOM\n");
        omega = TWOPI * u2;
        /* phi more likely in orbital plane */
        sphi = -1.0 + 2.0 * u1;
        phi = asin(sphi);
#endif
    }
    else
    {
        sphi = 0.0;
        vk = 0.0;
        vk2 = 0.0;
        phi = 0.0;
        omega = 0.0;
        Exit_binary_c(BINARY_C_INVALID_KICK_VELOCITY_DISTRIBUTION,
                      "Invalid kick velocity distribution! Set to %d for SN type %d with dispersion \n",
                      kick_distribution,
                      pre_explosion_star->SN_type
                     );
    }
    kick_system->kick_speed = vk;
    kick_system->kick_speed_squared = Pow2(vk);
    kick_system->omega = omega;
    kick_system->sinomega = sin(omega);
    kick_system->cosomega = cos(omega);
    kick_system->phi = phi;
    kick_system->sinphi = sphi;
    kick_system->cosphi = cos(phi);

    Dprint("kick_speed=%g kick_speed_squared=%g relative_orbital_speed=%g relative_orbital_speed_squared=%g omega=%g phi=%g cphi=%g somega=%g comega=%g\n",
           kick_system->kick_speed,
           kick_system->kick_speed_squared,
           kick_system->orbital_speed,
           kick_system->orbital_speed_squared,
           kick_system->omega,
           kick_system->phi,
           kick_system->cosphi,
           kick_system->sinomega,
           kick_system->cosomega);

#ifdef LOG_SUPERNOVAE
    /* Save the velocities */
    pre_explosion_star->sn_v = kick_system->kick_speed;
    pre_explosion_star->sn_rel_v = kick_system->new_orbital_speed;
#endif

#ifdef EVENT_BASED_LOGGING
    /* Save the stuff in the stardata */
    stardata->star[pre_explosion_star->starnum].sn_kick_speed = kick_system->kick_speed;
    stardata->star[pre_explosion_star->starnum].sn_kick_omega = kick_system->omega;
    stardata->star[pre_explosion_star->starnum].sn_kick_phi = kick_system->phi;
#endif // EVENT_BASED_LOGGING
}


/* static double kick_pdf(double v, double *params) */
/* { */
/*   /\* */
/*    * skeleton kick probability density function: */
/*    * v is the kick velocity */
/*    * *params a pointer to an array containing the parameters */
/*    *\/ */

/*   // return your function here  */
/*   return( 0.0 ); */
/* } */


static double maxwellian_kick_pdf(double v, double *params)
{
    /*
     * Maxwellian speed distribution as a function of speed v,
     * dispersion s
     */
    const double s = params[0];
    double v2 = v * v;
    return (sqrt(2.0 / PI) * v2 / Pow3(s) * exp(-v2 / (2.0 * Pow2(s))));
}


/* static double maxwellian2_kick_pdf(double v,double *params) */
/* { */
/*     /\* */
/*      * Maxwellian speed distribution as a function of speed v, */
/*      * weights w0,w1 and dispersions s0,s1 */
/*      *\/  */
/*     double v2=v*v; */
/*     const double w0=params[0]; */
/*     const double s0=params[1]; */
/*     const double w1=params[2]; */
/*     const double s1=params[3]; */
/*     return (w0*v2/Pow3(s0) * exp(-v2/(2.0*s0*s0))+ */
/*          w1*v2/Pow3(s1) * exp(-v2/(2.0*s1*s1))); */
/* } */


/* static double maxwellian3_kick_pdf(double v,double *params) */
/* { */
/*     /\* */
/*      * Maxwellian speed distribution as a function of speed v, */
/*      * weights w0,w1,w2 and dispersions s0,s1,s2 */
/*      *\/  */
/*     double v2=v*v; */
/*     const double w0=params[0]; */
/*     const double s0=params[1]; */
/*     const double w1=params[2]; */
/*     const double s1=params[3]; */
/*     const double w2=params[4]; */
/*     const double s2=params[5]; */

/*     return (w0*v2/Pow3(s0) * exp(-v2/(2.0*s0*s0))+ */
/*          w1*v2/Pow3(s1) * exp(-v2/(2.0*s1*s1))+ */
/*          w2*v2/Pow3(s2) * exp(-v2/(2.0*s2*s2)) */

/*      ); */
/* } */


static double hartman_kick_pdf(double v, double *params)
{
    /*
     * Hartman speed distribution as a function of speed v,
     * dispersion s
     */
    const double s = params[0];
    double u = v / s;
    //printf("%f \n",u);
    double u2 = u * u;
    return ((4.0 / PI) * (1.0 / Pow2(1.0 + u2)));
}
