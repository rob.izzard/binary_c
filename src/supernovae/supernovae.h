#pragma once
#ifndef SUPERNOVAE_H
#define SUPERNOVAE_H

#define Kick_companion_with(A) (kick_system->bound_before_explosion == TRUE && \
                                companion->stellar_type!=MASSLESS_REMNANT && \
                                Fequal((kick_companion),-(A)))

#if defined DEBUG && DEBUG==1
#define SNprint(...) Dprint(__VA_ARGS__);fflush(NULL);
#else
#define SNprint(...)
#endif

//#undef SNprint
//#define SNprint(...) printf(__VA_ARGS__)

#endif // SUPERNOVAE_H
