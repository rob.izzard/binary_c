#include "../binary_c.h"
No_empty_translation_unit_warning;

/*
 * Function to be called to trigger a supernova.
 *
 * oldstar is the pre-supernova stellar structure,
 * e.g. to be called from within stellar_structure_*
 *
 * newstar is the stellar structure at the point just prior to the
 * supernova being triggered. If this is NULL, the stellar structure
 * in *oldstar is used instead (it is copied
 * using the same routine that is used in stellar structure
 * calculation).
 *
 * This routine allocates memory to store the appropriate structures,
 * and sets up the new structure ready to be modified by the
 * stellar structure routine.
 *
 * companion stores the companion stellar structure in cases where
 * this is required. It is ignored if it is NULL.
 *
 * returns the new star structure, or NULL if supernovae are not allowed.
 */

struct star_t * new_supernova(struct stardata_t * const stardata,
                              struct star_t * const oldstar,
                              struct star_t * const companion,
                              struct star_t * const newstar)
{
    struct star_t * retstar;

    {
        char * corestring = core_string(oldstar,FALSE);
        Dprint("NEW SUPERNOVA star %d st=%d M=%g %s t=%g dt=%g \n",
           oldstar->starnum,
               oldstar->stellar_type,
               oldstar->mass,
               corestring,
               stardata->model.time,
               stardata->model.dt);
        Safe_free(corestring);
    }

    /*
     * Use binary_c's event structure to trigger the supernova
     */
    struct binary_c_new_supernova_event_t * new_sn =
        Calloc(1,sizeof(struct binary_c_new_supernova_event_t));

    Dprint("NEW SN event data star %d (new_sn) = %p, oldstar = %p, companion %p, newstar %p (stardata %p %p)\n",
           newstar->starnum,
           (void*)new_sn,
           (void*)oldstar,
           (void*)companion,
           (void*)newstar,
           (void*)&stardata->star[0],
           (void*)&stardata->star[1]
        );

    /*
     * When we add a SN event, it should be unique because a star
     * can explode only once per timestep, and we have separate
     * event types for a supernova in star 0 or 1.
     */
    if(Add_new_event(stardata,
                     oldstar->starnum == 0 ? BINARY_C_EVENT_SUPERNOVA0 : BINARY_C_EVENT_SUPERNOVA1,
                     &supernova_event_handler,
                     &supernova_erase_event_handler,
                     new_sn,
                     UNIQUE_EVENT)
       != BINARY_C_EVENT_DENIED)
    {
        /*
         * Set up the event data
         *
         * The pre_explosion_star is a pointer to the
         * star, in stardata, that will explode.
         */
        new_sn->pre_explosion_star = &stardata->star[oldstar->starnum];
        Dprint("PRE_EXP %d\n",
               new_sn->pre_explosion_star->stellar_type);

        /*
         * The post_explosion_star is declared here but set up
         *  in your stellar structure routine.
         */
        new_sn->post_explosion_star = New_star_from(oldstar);

        /*
         * So return this.
         */
        retstar = new_sn->post_explosion_star;
    }
    else
    {
        Dprint("SN event denied\n");
        Safe_free(new_sn);
        retstar = NULL;
    }

    return retstar;
}
