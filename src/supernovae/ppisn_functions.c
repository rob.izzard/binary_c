#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef PPISN
double calc_helium_core_mass(struct star_t * RESTRICT star)
{
    /* Function to calculate the Core helium mass */
    if(star->stellar_type < HeMS)
    {
        return star->core_mass[CORE_He];
    }
    else if(NAKED_HELIUM_STAR(star->stellar_type))
    {
        return star->mass;
    }
    else
    {
        return 0;
    }
}

void ppisn_belczynski_2016(struct stardata_t * RESTRICT const stardata,
                           struct star_t * RESTRICT star,
                           const double m,
                           double * RESTRICT const ppisn_mremnant,
                           double * RESTRICT const ppisn_mbaryonic,
                           int * RESTRICT const ppisn_type)
{
    /*
     * PPISN prescription based on Belczynski et al. 2016 https://ui.adsabs.harvard.edu/abs/2016A%26A...594A..97B/abstract
     * - This prescription predicts a fixed black hole mass for a given helium core mass.
     */

    /* All mass above 45 solar mass helium core is lost, until full PISN occurs */

    /* Get helium 'core' mass */
    const double helium_core_mass = calc_helium_core_mass(star);
    Dprint("Using ppisn_belczynski_2016 with m: %g helium_core_mass: %g", m, helium_core_mass);

    if(helium_core_mass < 45.0)
    {
        *ppisn_mremnant = m;
        *ppisn_mbaryonic = m;
        *ppisn_type = PPISN_TYPE_CORE_COLLAPSE;
    }
    else if(In_range(helium_core_mass, 45.0, 65.0))
    {
        *ppisn_type = PPISN_TYPE_PPI;
        *ppisn_mbaryonic = 45.0;
        /* Baryonic to gravitational mass */
        *ppisn_mremnant  = baryonic_to_gravitational_remnant_mass(
            stardata,
            star,
            *ppisn_mbaryonic
            );
    }
    else if(In_range(helium_core_mass, 65.0, 135.0))
    {
        *ppisn_mremnant = 0.0;
        *ppisn_mbaryonic = 0.0;
        *ppisn_type = PPISN_TYPE_PI;
    }
    else if(helium_core_mass >= 135.0)
    {
        *ppisn_mremnant = m;
        *ppisn_mbaryonic = m;
        *ppisn_type = PPISN_TYPE_PHDIS;
    }
    else
    {
        *ppisn_mremnant = -1.0;
        *ppisn_mbaryonic = -1.0;
        *ppisn_type = PPISN_TYPE_ERROR;
    }
}

void ppisn_spera_2017(struct stardata_t * RESTRICT const stardata,
                      struct star_t * RESTRICT star,
                      const double m,
                      double * RESTRICT const ppisn_mremnant,
                      double * RESTRICT const ppisn_mbaryonic,
                      int * RESTRICT const ppisn_type)
{
    /* PPISN prescription from Spera et al 2017 https://ui.adsabs.harvard.edu/abs/2017MNRAS.470.4739S/abstract */
    double alpha_p;
    double helium_core_mass = calc_helium_core_mass(star);
    Dprint("Using ppisn_spera_2017 with m: %g helium_core_mass: %g", m, helium_core_mass);
    double F = helium_core_mass / star->mass;
    double K = 0.67000 * F + 0.10000;
    double S = 0.52260 * F - 0.52974;

    if(helium_core_mass <= 32.0)                                                                  // I
    {
        alpha_p = 1.0;
    }
    else if(helium_core_mass > 32.0 && helium_core_mass <= 37.0 && F < 0.9)                       // II
    {
        alpha_p = 0.2 * (K - 1.0) * helium_core_mass + 0.2 * (37.0 - 32.0 * K);
    }
    else if(helium_core_mass > 37.0 && helium_core_mass <= 60.0 && F < 0.9)                       // III
    {
        alpha_p = K;
    }
    else if(helium_core_mass > 32.0 && helium_core_mass <= 37.0 && F >= 0.9)                      // IV
    {
        alpha_p = S * (helium_core_mass - 32.0) + 1.0;
    }
    else if(helium_core_mass > 37.0 && helium_core_mass <= 56.0 && F >= 0.9 && S < -0.034168)     // V
    {
        alpha_p = 5.0 * S + 1.0;
    }
    else if(helium_core_mass > 37.0 && helium_core_mass <= 56.0 && F >= 0.9 && S >= -0.034168)    // VI
    {
        alpha_p = (-0.1381 * F + 0.1309) * (helium_core_mass - 56.0) + 0.82916;
    }
    else if(helium_core_mass > 56.0 && helium_core_mass <= 64.0 && F >= 0.9)                      // VII
    {
        alpha_p = -0.103645 * helium_core_mass + 6.63328;
    }
    else if(helium_core_mass > 60.0 && helium_core_mass <= 64.0 && F < 0.9)                       // VIII
    {
        alpha_p = -(K / 4.0) * helium_core_mass + 16.0 * K;
    }
    else if(helium_core_mass > 64.0 && helium_core_mass < 135.0)                                  // IX
    {
        alpha_p = 0.0;
    }
    else if(helium_core_mass >= 135)                                                              // X
    {
        alpha_p = 1.0;
    }
    else
    {
        //Set a value if None of the above conditions hold
        alpha_p = 0.0;
        Printf("Uncaught value for alpha_p, manually setting it to 0\n");
    }

    /* Determine remnant mass and type */
    if(helium_core_mass >= 135)
    {
        *ppisn_mremnant = m;
        *ppisn_mbaryonic = m;
        *ppisn_type = PPISN_TYPE_PHDIS;
    }
    /* Core collapse */
    else if(alpha_p >= 1)
    {
        *ppisn_mremnant = m; // Dummy value, it will be changed
        *ppisn_mbaryonic = m;
        *ppisn_type = PPISN_TYPE_CORE_COLLAPSE;
    }
    else if(alpha_p > 0 && alpha_p < 1)
    {
        *ppisn_type = PPISN_TYPE_PPI;
        *ppisn_mremnant = alpha_p * ns_bh_mass(star->mass,
                                               star->core_mass[CORE_CO],
                                               stardata,
                                               star,
                                               ppisn_mbaryonic);
    }
    else if(alpha_p <= 0)
    {
        *ppisn_mremnant = 0.0;
        *ppisn_mbaryonic = 0.0;
        *ppisn_type = PPISN_TYPE_PI;
    }
    else
    {
        *ppisn_mremnant = -1.0;
        *ppisn_mbaryonic = -1.0;
        *ppisn_type = PPISN_TYPE_ERROR;
    }
}

void ppisn_farmer_2019(struct stardata_t * RESTRICT const stardata,
                       struct star_t * RESTRICT star MAYBE_UNUSED,
                       const double m MAYBE_UNUSED,
                       const double mc_CO,
                       double * RESTRICT const ppisn_mremnant,
                       double * RESTRICT const ppisn_mbaryonic,
                       int * RESTRICT const ppisn_type)
{
    /*
     * PPISN Routine based on the paper of Farmer et al. 2019 https://ui.adsabs.harvard.edu/abs/2019ApJ...887...53F/abstract
     *
     * If the core mass is too low for a PPISN, we revert back to the ns_bh_mass function to determine the mass.
     *
     * Note: core collapse returns *ppism_remnant = 0.0 because the
     *       remnant mass is set elsewhere.
     */

    const double lower_bound_ppisn = 38.0;
    const double upper_bound_ppisn = 65.0;
    const double upper_bound_pisn = 114.0;
    const double min_mass_black_hole = 10.0;

    const double a1 = -0.096;
    const double a2 = 8.564;
    const double a3 = -2.07;
    const double a4 = -152.97;

    Dprint("Using ppisn_farmer_19 with m: %g mc_CO: %g\n", m, mc_CO);

    if(mc_CO < lower_bound_ppisn)
    {
        *ppisn_mremnant = Max(4.0 + mc_CO, m);
        *ppisn_type = PPISN_TYPE_CORE_COLLAPSE;
    }
    else if(In_range(mc_CO, lower_bound_ppisn, upper_bound_ppisn))
    {
        *ppisn_mbaryonic =
            a1 * Pow2(mc_CO) +
            a2 * mc_CO +
            a3 * log10(stardata->common.metallicity) +
            a4;
        *ppisn_type = PPISN_TYPE_PPI;

        /* We can manually remove (or add) an additional amount of mass */
        *ppisn_mbaryonic -= stardata->preferences->PPISN_additional_massloss;
        *ppisn_mbaryonic = Min(*ppisn_mremnant, m);

        /*
         * To be safe, make sure that when the upper boundary
         * leads to negative masses, set it as a PISN
         */
        if(*ppisn_mremnant <= min_mass_black_hole)
        {
            *ppisn_mremnant = 0.0;
            *ppisn_type = PPISN_TYPE_PI;
        }
        else
        {
            /* Baryonic to gravitational mass */
            *ppisn_mremnant  = baryonic_to_gravitational_remnant_mass(
                                   stardata,
                                   star,
                                   *ppisn_mbaryonic
                               );
        }
    }
    else if(In_range(mc_CO, upper_bound_ppisn, upper_bound_pisn))
    {
        *ppisn_mremnant = 0.0;
        *ppisn_mbaryonic = 0.0;
        *ppisn_type = PPISN_TYPE_PI;
    }
    else if(mc_CO >= upper_bound_pisn)
    {
        *ppisn_mremnant = m;
        *ppisn_mbaryonic = m;
        *ppisn_type = PPISN_TYPE_PHDIS;
    }
    else
    {
        *ppisn_mremnant = -1.0;
        *ppisn_mbaryonic = -1.0;
        *ppisn_type = PPISN_TYPE_ERROR;
    }

    Dprint("\tppisn_farmer_19: %s : ppisn: ppisn_mremn-ant %g ppisn_type %d\n",
           ppisn_type_strings[*ppisn_type],
           *ppisn_mremnant,
           *ppisn_type);
}

void ppisn_stevenson_2019(struct stardata_t * RESTRICT const stardata,
                          struct star_t * RESTRICT star,
                          const double m,
                          double * RESTRICT const ppisn_mremnant,
                          double * RESTRICT const ppisn_mbaryonic,
                          int * RESTRICT const ppisn_type)
{
    /* PPISN prescription from Stevenson et al. 2019 https://ui.adsabs.harvard.edu/abs/2019ApJ...882..121S/abstract based on the models of Marchant et al. 2018*/

    /* set coefficients */
    double coeff[8] =
    {
        7.39643451e3,
        -1.13694590e3,
        7.45060098e1,
        -2.69801221e0,
        5.83107626e-2,
        -7.52206933e-4,
        5.36316755e-6,
        -1.63057326e-8
    };

    /* calculate helium core mass */
    double helium_core_mass = calc_helium_core_mass(star);

    /* calculate ratio Mfinal/Mhe*/
    double ratio_mfinal_mhe = 0;

    int i;
    for (i = 0; i < 8; i++)
    {
        ratio_mfinal_mhe += coeff[i] * pow(helium_core_mass, i);
    }

    /* Photodissociation */
    if(helium_core_mass >= 135)
    {
        *ppisn_mremnant = m;
        *ppisn_mbaryonic = m;
         *ppisn_type = PPISN_TYPE_PHDIS;
    }
    /* if the ratio >= 1: No PPISN. revert to CC outside this function*/
    else if(ratio_mfinal_mhe >= 1)
    {
        *ppisn_mremnant = m; // Dummy value, it will be changed
        *ppisn_mbaryonic = m;
        *ppisn_type = PPISN_TYPE_CORE_COLLAPSE;
    }
    /* if the ratio 0 < ratio < 1: PPISN */
    else if((ratio_mfinal_mhe < 1) && (ratio_mfinal_mhe > 0))
    {
        *ppisn_type = PPISN_TYPE_PPI;
        *ppisn_mbaryonic = ratio_mfinal_mhe * helium_core_mass;

        /* Baryonic to gravitational mass */
        *ppisn_mremnant  = baryonic_to_gravitational_remnant_mass(
            stardata,
            star,
            *ppisn_mbaryonic
            );
    }
    /* if the ratio 0 < ratio < 1: PPISN */
    else if(ratio_mfinal_mhe <= 0)
    {
        *ppisn_mremnant = 0.0;
        *ppisn_mbaryonic = 0.0;
        *ppisn_type = PPISN_TYPE_PI;
    }
    else
    {
        *ppisn_mremnant = -1.0;
        *ppisn_mbaryonic = -1.0;
        *ppisn_type = PPISN_TYPE_ERROR;
    }
}

void ppisn_mapelli_2020(struct stardata_t * RESTRICT const stardata,
                        struct star_t * RESTRICT star,
                        const double m,
                        double * RESTRICT const ppisn_mremnant,
                        double * RESTRICT const ppisn_mbaryonic,
                        int * RESTRICT const ppisn_type)
{
    /*
     * PPISN prescription from Mapelli 2020 https://ui.adsabs.harvard.edu/abs/2020ApJ...888...76M/abstract
     */
    double alpha_p;
    const double helium_core_mass = calc_helium_core_mass(star);
    Dprint("Using ppisn_mapelli_2020 with m: %g helium_core_mass: %g", m, helium_core_mass);
    const double F = helium_core_mass / star->mass;
    const double K = 0.67000 * F + 0.10000;
    const double S = 0.52260 * F - 0.52974;

    if(helium_core_mass <= 32.0)                                                                  // I
    {
        alpha_p = 1.0;
    }
    else if(helium_core_mass > 32.0 && helium_core_mass <= 37.0 && F < 0.9)                       // II
    {
        alpha_p = 0.2 * (K - 1.0) * helium_core_mass + 0.2 * (37.0 - 32.0 * K);
    }
    else if(helium_core_mass > 37.0 && helium_core_mass <= 60.0 && F < 0.9)                       // III
    {
        alpha_p = K;
    }
    else if(helium_core_mass > 60 && helium_core_mass < 64 && F < 0.9)                            // IV
    {
        alpha_p = K * (16.0 - 0.25 * helium_core_mass);
    }
    else if(helium_core_mass > 32.0 && helium_core_mass <= 37.0 && F >= 0.9)                      // V
    {
        alpha_p = S * (helium_core_mass - 32.0) + 1.0;
    }
    else if(helium_core_mass > 37.0 && helium_core_mass <= 56.0 && F >= 0.9 && S < -0.034168)     // VI
    {
        alpha_p = 5.0 * S + 1.0;
    }
    else if(helium_core_mass > 37.0 && helium_core_mass <= 56.0 && F >= 0.9 && S >= -0.034168)    // VII
    {
        alpha_p = (-0.1381 * F + 0.1309) * (helium_core_mass - 56.0) + 0.82916;
    }
    else if(helium_core_mass > 56.0 && helium_core_mass < 64.0 && F >= 0.9)                      // VII
    {
        alpha_p = -0.103645 * helium_core_mass + 6.63328;
    }
    else if(helium_core_mass >= 64.0 && helium_core_mass < 135.0)                                  // IX
    {
        alpha_p = 0.0;
    }
    else if(helium_core_mass >= 135)                                                              // X
    {
        alpha_p = 1.0;
    }
    else
    {
        //Set a value if None of the above conditions hold
        alpha_p = 0.0;
        Printf("Uncaught value for alpha_p, manually setting it to 0\n");
    }

    /* Determine remnant mass and type */
    if(helium_core_mass >= 135.0)
    {
        *ppisn_mremnant = m;
        *ppisn_mbaryonic = m;
        *ppisn_type = PPISN_TYPE_PHDIS;
    }
    /* Core collapse */
    else if(alpha_p >= 1.0)
    {
        *ppisn_mremnant = m; // Dummy value, it will be changed
        *ppisn_mbaryonic = m;
        *ppisn_type = PPISN_TYPE_CORE_COLLAPSE;
    }
    else if(alpha_p > 0.0 && alpha_p < 1.0)
    {
        *ppisn_type = PPISN_TYPE_PPI;
        *ppisn_mremnant = alpha_p * ns_bh_mass(star->mass,
                                               star->core_mass[CORE_CO],
                                               stardata,
                                               star,
                                               ppisn_mbaryonic);
    }
    else if(alpha_p <= 0.0)
    {
        *ppisn_mremnant = 0.0;
        *ppisn_mbaryonic = 0.0;
        *ppisn_type = PPISN_TYPE_PI;
    }
    else
    {
        *ppisn_mremnant = -1.0;
        *ppisn_mbaryonic = -1.0;
        *ppisn_type = PPISN_TYPE_ERROR;
    }
}

void ppisn_hendriks_2023(struct stardata_t * RESTRICT const stardata,
                         struct star_t * RESTRICT star MAYBE_UNUSED,
                         const double m MAYBE_UNUSED,
                         const double mc_CO,
                         double * RESTRICT const ppisn_mremnant,
                         double * RESTRICT const ppisn_mbaryonic,
                         int * RESTRICT const ppisn_type)
{
    /*
     * PPISN remnant mass prescription described in Hendriks et al. 2023 (in
     * prep), which is a modified version of Renzo et al. 2022
     * (https://iopscience.iop.org/article/10.3847/2515-5172/ac503e). This
     * prescription is based on the detailed models from Farmer et al. 2019
     * https://ui.adsabs.harvard.edu/abs/2019ApJ...887...53F/abstract
     *
     * This prescription takes the top-down approach of calculating the mass
     * removed as a function of CO core mass, rather than the remnant mass as a
     * function of CO core mass
     *
     * Additionally, if there is a hydrogen envelope present, that mass will be
     * added to the above mentioned amount of removed mass.
     *
     * There are three extra options that can be used here:
     * - stardata->preferences->PPISN_additional_massloss: an additional amount
     *   of mass that is lost when a star undergoes PPISN. Currently this is
     *   taken as a constant over the whole range of stars that undergo PPISN.
     *   Positive number means more mass lost
     * - stardata->preferences->PPISN_core_mass_range_shift: Value by which we
     *   shift the range of CO core mass values that undergo PPISN. This is
     *   taken into account in the fit that calculates the mass removal.
     *   Negative number is shifting the range to lower masses, positive to
     *   higher masses.
     * - stardata->preferences->PPISN_massloss_multiplier: Factor by which we
     *   multiply the originally prescribed mass loss (disregarding the
     *   H-envelope loss). This multiplier does not affect the extra_massloss.
     */

    double hydrogen_envelope_massloss = 0;
    double additional_massloss = 0;
    double ppi_massloss = 0;
    double total_massloss = 0;

    const double lower_bound_ppisn = 38.0 + stardata->preferences->PPISN_core_mass_range_shift;
    const double upper_bound_ppisn = 114.0 + stardata->preferences->PPISN_core_mass_range_shift; // NOTE: hardcoding this upper bound gets in the way of the variations. Since we already check if the BH mass is lower than a value and then assign the PISN flag to it. The upper boundary for PISN should probably also be variable, but that part is a bit more uncertain. For this reason, i will set the PPISN upper boundary equal to the PISN upper boundary, and then let the code assign PISN when the mass is below the min bh mass value.
    const double upper_bound_pisn = 114.0;
    const double min_mass_black_hole = 10.0;

    Dprint("Using ppisn_new_fit_21 with m: %g mc_CO: %g lower_bound_ppisn: %g upper_bound_ppisn: %g PPISN_core_mass_range_shift: %g\n", m, mc_CO, lower_bound_ppisn, upper_bound_ppisn, stardata->preferences->PPISN_core_mass_range_shift);

    if(mc_CO < lower_bound_ppisn)
    {
        *ppisn_mremnant = Max(4.0 + mc_CO, m);
        *ppisn_mbaryonic = *ppisn_mremnant;
        *ppisn_type = PPISN_TYPE_CORE_COLLAPSE;
    }
    else if(In_range(mc_CO, lower_bound_ppisn, upper_bound_ppisn))
    {
        // Calculate the mass removed
        ppi_massloss = (5.72629809e-04 * log10(stardata->common.metallicity) + 5.43166453e-03) * Pow3((mc_CO - stardata->preferences->PPISN_core_mass_range_shift) - 3.47840387e+01) - 1.26709046e-03 * Pow2((mc_CO - stardata->preferences->PPISN_core_mass_range_shift) - 3.47840387e+01);

        // Use multiplier
        ppi_massloss = ppi_massloss * stardata->preferences->PPISN_massloss_multiplier;

        // Check if there is additional hydrogen envelope to be lost
        hydrogen_envelope_massloss = star->core_mass[CORE_He] > 0.0 ? (star->mass - star->core_mass[CORE_He]) : 0.0;

        // Handle the additional mass loss
        additional_massloss = stardata->preferences->PPISN_additional_massloss;

        // Combine the hydrogren envelope mass, the mass as described by the fit function and the custom mass to the r
        total_massloss = ppi_massloss + hydrogen_envelope_massloss + additional_massloss;

        //
        Dprint("Calculated the total mass removal by PPI (stellar type = %d/%s): ppi_massloss: %g (using multiplier: %g) hydrogen_envelope_massloss: %g additional_massloss: %g total: %g\n",
               star->stellar_type, Stellar_type_string(star->stellar_type), ppi_massloss, stardata->preferences->PPISN_massloss_multiplier, hydrogen_envelope_massloss, additional_massloss, total_massloss);

        // Calculate the remnant mass
        *ppisn_mbaryonic = m - total_massloss;
        *ppisn_type = PPISN_TYPE_PPI;

        // Make sure we're within the allowed bounds
        *ppisn_mbaryonic = Min(*ppisn_mremnant, m);
        *ppisn_mbaryonic = Max(*ppisn_mremnant, 0.0);

        /*
         * To be safe, make sure that when the upper boundary leads to masses
         * lower than the minimum mass allowed for PPISN (see TODO: insert
         * citation mathieu), set it as a PISN
         */
        if(*ppisn_mbaryonic <= min_mass_black_hole)
        {
            *ppisn_mremnant = 0.0;
            *ppisn_mbaryonic = 0.0;
            *ppisn_type = PPISN_TYPE_PI;
        }
        else
        {
            /* Baryonic to gravitational mass */
            *ppisn_mremnant  = baryonic_to_gravitational_remnant_mass(
                                   stardata,
                                   star,
                                   *ppisn_mbaryonic
                               );
        }
    }
    else if(In_range(mc_CO, upper_bound_ppisn, upper_bound_pisn))
    {
        *ppisn_mremnant = 0.0;
        *ppisn_mbaryonic = 0.0;
        *ppisn_type = PPISN_TYPE_PI;
    }
    else if(mc_CO >= upper_bound_pisn)
    {
        *ppisn_mremnant = m;
        *ppisn_mbaryonic = m;
        *ppisn_type = PPISN_TYPE_PHDIS;
    }
    else
    {
        *ppisn_mremnant = -1.0;
        *ppisn_mbaryonic = -1.0;
        *ppisn_type = PPISN_TYPE_ERROR;
    }

    Dprint("\tppisn_new_fit_21: %s : ppisn: ppisn_mremn-ant %g ppisn_type %d \n",
           ppisn_type_strings[*ppisn_type],
           *ppisn_mremnant,
           *ppisn_type);
}

void pulsational_pair_instability(struct stardata_t * RESTRICT const stardata,
                                  struct star_t * RESTRICT star,
                                  const double m,
                                  const double mc_CO,
                                  double * RESTRICT const ppisn_mremnant,
                                  double * RESTRICT const ppisn_mbaryonic,
                                  int * RESTRICT const ppisn_type)
{
    /*
     * Function that handles the chosen ppisn prescription and probes which type
     * (ppisn_type) of pair instability occurs (core collapse = 0, PPISN = 1,
     * PISN = 2, PHDIS = 3)
     *
     * Input:
     *   - stardata (stardata)
     *   - exploding star (star)
     *   - total mass of star (m)
     *   - CO core mass (mc_CO)
     *
     * Output (by setting reference):
     *   - remnant mass according to the chose routine (ppisn_mremnant)
     *   - type of instability (ppisn_type)
     */

    if(stardata->preferences->PPISN_prescription == PPISN_BELCZYNSKI16)
    {
        /*
         * Use Belczynski 2016 algorithm
         */
        ppisn_belczynski_2016(stardata, star, m, ppisn_mremnant, ppisn_mbaryonic, ppisn_type);
    }
    else if(stardata->preferences->PPISN_prescription == PPISN_SPERA17)
    {
        /*
         * Use Spera 2017 algorithm
         */
        ppisn_spera_2017(stardata, star, m, ppisn_mremnant, ppisn_mbaryonic, ppisn_type);
    }
    else if(stardata->preferences->PPISN_prescription == PPISN_FARMER19)
    {
        /*
         * Use Farmer 2019 algorithm
         */
        ppisn_farmer_2019(stardata, star, m, mc_CO, ppisn_mremnant, ppisn_mbaryonic, ppisn_type);
    }
    else if(stardata->preferences->PPISN_prescription == PPISN_STEVENSON19)
    {
        /*
         * Use Stevenson 2019 algorithm
         */
        ppisn_stevenson_2019(stardata, star, m, ppisn_mremnant, ppisn_mbaryonic, ppisn_type);
    }
    else if(stardata->preferences->PPISN_prescription == PPISN_MAPELLI20)
    {
        /*
         * Use Mapelli 2020 algorithm
         */
        ppisn_mapelli_2020(stardata, star, m, ppisn_mremnant, ppisn_mbaryonic, ppisn_type);
    }
    else if(stardata->preferences->PPISN_prescription == PPISN_HENDRIKS23)
    {
        /*
         * Use Hendriks 2023 / Renzo 2022 algorithm
         */
        ppisn_hendriks_2023(stardata, star, m, mc_CO, ppisn_mremnant, ppisn_mbaryonic, ppisn_type);
    }
    else
    {
        /*
         * Default to normal binary_c core collapse supernovae
         */
        *ppisn_mremnant = 0.0;
        *ppisn_mbaryonic = 0.0;
        *ppisn_type = PPISN_TYPE_CORE_COLLAPSE;
    }

    Dprint("pulsational_pair_instability: t %30.12e modelno %d stellartype %d m %g mc_CO %g ppisn_mremn-ant %g ppisn_mbaryonic %g ppisn_type %d\n",
           stardata->model.time,
           stardata->model.model_number,
           star->stellar_type,
           m,
           mc_CO,
           *ppisn_mremnant,
           *ppisn_mbaryonic,
           *ppisn_type);
}
#endif // PPISN
