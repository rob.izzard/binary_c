#include "../binary_c.h"
No_empty_translation_unit_warning;

/*
 * Check for explicit evolution stop:
 * return TRUE if we want to stop
 */

Boolean Pure_function check_for_evolution_stop(struct stardata_t * const stardata)
{
    Foreach_star(star)
    {
        /*
         * Check stellar type does not exceed the max stellar type
         * but skip if MASSLESS_REMNANT and the max_stellar_type is negative
         */
        if(stellar_type_exceeded(stardata,star) == TRUE)
        {
            return TRUE;
        }
    }
    return stardata->model.evolution_stop;
}
