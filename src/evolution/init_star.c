#include "../binary_c.h"
No_empty_translation_unit_warning;


/*
 * Initialise the star struct, except the star number
 * (which must be set elsewhere)
 */

void init_star(struct stardata_t * const stardata,
               struct star_t * const star)
{
    /*
     * Set most of the star to zero (this preserves
     * vital pointers)
     */
    zero_star(star);

    star->num_thermal_pulses = -1.0;


#ifdef NUCSYN
    star->temp_mult = 1.0;
    /*
     * Set variables which require non-zero defaults
     */

    star->sn_last_time = -1;
    star->he_t = -1.0; // zero age helium star birth time
    star->newhestar = TRUE;
    star->hezamsmetallicity = -1.0;
    star->effective_zams_mass = star->mass;
    star->start_HG_mass = -10;
#ifdef NUCSYN_FIRST_DREDGE_UP_PHASE_IN
    star->first_dup_phase_in_firsttime = TRUE;
#endif // NUCSYN_FIRST_DREDGE_UP_PHASE_IN
    star->max_mix_depth = 1e3;
    star->dont_change_stellar_type = FALSE;
#endif /* NUCSYN */
    star->stellar_type = MAIN_SEQUENCE;
    star->core_stellar_type = MASSLESS_REMNANT;
    star->nova = NOVA_STATE_NONE;

    Dprint("Done for this star\n");
    star->RLOFing = FALSE;
    star->white_dwarf_atmosphere_type = WHITE_DWARF_ATMOSPHERE_UNKNOWN;

#ifdef PPISN
    star->undergone_ppisn_type = PPISN_TYPE_NONE;
#endif //PPISN

#ifdef EVENT_BASED_LOGGING
    star->fallback = 0.0;
    star->fallback_mass = 0.0;
#endif // EVENT_BASED_LOGGING
    star->overspin = FALSE;
}
