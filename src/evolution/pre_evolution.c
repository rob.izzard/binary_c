#include "../binary_c.h"
No_empty_translation_unit_warning;


void pre_evolution(struct stardata_t * Restrict const stardata)
{
    /*
     * Perhaps load a stardata, if we do re-parse cmd line args
     * to override
     */
    Boolean force_log = FALSE;
    if(strlen(stardata->preferences->stardata_load_filename))
    {
        load_stardata(stardata,
                      stardata->preferences->stardata_load_filename,
                      FALSE);
        stardata->model.log_fp = NULL;
        force_log = TRUE;
    }

#ifdef FILE_LOG
    Dprint("opening log files\n");

#ifndef FILE_LOG_REWIND
    /* make sure the log files are closed first */
    close_log_files(&stardata->model.log_fp,
                    stardata);
#endif // not FILE_LOG_REWIND

    /* open log files */
    Dprint("call open_log_files (log_fp = %p)\n",
           (void*)stardata->model.log_fp);
    open_log_files(&stardata->model.log_fp,
                   stardata);

/* Set up the detailed log */
#ifdef DETAILED_LOG
    /* Blank line! */
    detailed_log(NULL);
#endif /* DETAILED_LOG */

#endif/*FILE_LOG*/
    Dprint("Log files open \n");

#ifdef LOG_SUPERNOVAE
    /* Set up the supernovae logging */
    log_sn(stardata,-1,0,0);
#endif /* LOG_SUPERNOVAE */

    if(force_log==TRUE)
    {
        logwrap(stardata,"Load","");
    }
}
