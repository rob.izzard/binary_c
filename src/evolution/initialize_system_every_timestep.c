#include "../binary_c.h"
No_empty_translation_unit_warning;



/*
 * This function contains stuff we need to initialize every timestep
 * for a generic system.
 */

static void initialize_system_every_timestep_star(struct star_t * Restrict const star);
static void initialize_system_every_timestep2(struct stardata_t * Restrict const stardata);

void initialize_system_every_timestep(struct stardata_t * Restrict const stardata)

{
    Dprint("Initialize detached system every timestep : masses %g %g\n",
           stardata->star[0].mass,
           stardata->star[1].mass
        );
    initialize_system_every_timestep2(stardata);
    Foreach_star(star)
    {
        initialize_system_every_timestep_star(star);
    }
    Dprint("init done\n");
}

void initialize_system_every_timestep2(struct stardata_t * Restrict const stardata)
{
    struct model_t * const model = &stardata->model;
    model->prec = FALSE;
    model->supernova = FALSE;
    model->com = FALSE;
    model->coalesce = FALSE;
    model->tphys0 = model->time;
    model->ndonor = 0;
    model->naccretor = 1;
    model->reject_same_timestep = REJECT_NONE;
    model->reject_shorten_timestep = REJECT_NONE;
    zero_stellar_mass_and_angmom_derivatives(stardata);

#ifdef DISCS
    disc_initialize_every_timestep(stardata);
#endif
    Foreach_star(star)
    {
        star->reject_same_timestep = REJECT_NONE;
        star->reject_shorten_timestep = REJECT_NONE;
#if defined NUCSYN && defined NUCSYN_ANGELOU_LITHIUM
        star->angelou_lithium_boost_this_timestep = FALSE;
#endif // NUCSYN && NUCSYN_LITHIUM_ANGELOU
#ifdef STELLAR_COLOURS
        if(stardata->tmpstore->stellar_magnitudes[star->starnum] != NULL)
        {
            Safe_free(stardata->tmpstore->stellar_magnitudes[star->starnum]);
        }
#endif // STELLAR_COLOURS
    }
#ifdef STELLAR_COLOURS
    Safe_free(stardata->tmpstore->unresolved_magnitudes);
#endif // STELLAR_COLOURS
#ifdef STELLAR_POPULATIONS_ENSEMBLE
    for(Yield_source s = 0;
        s < SOURCE_NUMBER;
        s++)
    {
        model->ensemble_yield[s] = 0.0;
    }
#endif//STELLAR_POPULATIONS_ENSEMBLE
    if(stardata->pre_events_stardata != NULL)
    {
        free_stardata(&stardata->pre_events_stardata);
    }
}


static void initialize_system_every_timestep_star(struct star_t * Restrict const star)
{
#ifdef ADAPTIVE_RLOF
    star->prev_dm=0.0;
#endif
    if(COMPACT_OBJECT(star->stellar_type))
    {
        star->stellar_timestep=0.01;
    }
#if defined SELMA_FIX_ACCRETED_MASS_IN_REJUVENATION ||  \
    defined SELMA_BETTER_TREATMENT_OF_MS_ACCRETORS
    star->last_mass=star->mass;
#endif
    star->overspin = FALSE;
}
