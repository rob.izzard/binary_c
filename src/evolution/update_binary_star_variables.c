#include "../binary_c.h"
No_empty_translation_unit_warning;


void update_binary_star_variables(struct stardata_t * Restrict const stardata,
                                  int * Restrict const retval)
{
    /*
     * Having calculated stellar evolution, do
     * binary-specific evolutionary tasks
     */
    if(retval && stardata->common.orbit.eccentricity > 1.0)
    {
        *retval = EVOLUTION_SYSTEM_IS_BROKEN_APART;
    }

    if(retval == NULL ||
       *retval != EVOLUTION_SYSTEM_IS_BROKEN_APART)
    {
        /*
         * Update mass ratios, and Roche and effective radii
         */
        determine_mass_ratios(stardata);
        determine_roche_lobe_radii(stardata,&stardata->common.orbit);
        set_effective_radii(stardata);
    }
}
