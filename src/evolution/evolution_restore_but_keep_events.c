#include "../binary_c.h"
No_empty_translation_unit_warning;

/*
 * Restore a system to the previous timestep, but keep its events
 */

void evolution_restore_but_keep_events(struct stardata_t * const stardata)
{
    evolution_restore_from_previous(stardata,
                                    TRUE);
}
