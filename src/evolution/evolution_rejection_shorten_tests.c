#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "../ensemble/ensemble_planetary_nebulae.h"

void evolution_rejection_shorten_tests(struct stardata_t * Restrict const stardata)
{
    /*
     * Test for whether evolution should be rejected and
     * restarted with a shorter timestep.
     *
     * If we cannot reject, simply do nothing
     */
    const Boolean can_and_shorten = can_reject_and_shorten_timestep(stardata);
    Dprint("can reject and shorten timestep? %s%s%s\n",
           Colouryesno(can_and_shorten));
    if(can_and_shorten == TRUE &&
       stardata != stardata->previous_stardata)
    {
        /*
         * Here you can reject a system if it changes too much.
         *
         * What you should *not* do is compare derivatives to determine
         * whether to reject. This won't work, because the derivatives
         * are a function of the previous structure, so will not change
         * when you reject and change the timestep.
         *
         * When you reject, set stardata->model.reject_shorten_timestep to one of the REJECT_*
         * macros, and immediately return (it is not required to test further
         * even if other tests may also reject the timestep).
         */

        const Boolean evolution_is_smooth = smooth_evolution(stardata);

#ifdef STELLAR_POPULATIONS_ENSEMBLE
        if(Use_ensemble(STELLAR_POPULATIONS_ENSEMBLE_FILTER_PNE) == TRUE)
        {
            /*
             * Zoom in to Menv = PN_envelope_mass for planetary nebulae.
             * We can only do this for wind-type PNe.
             */
            Star_number PN_star_number;
            const PN_type pn = ensemble_detect_new_PN(stardata,
                                                      &PN_star_number,
                                                      FALSE);

            if(PN_star_number != -1 &&
               pn != PN_NONE &&
               pn != PN_COMENV_EJECTION)
            {
                SETstar(PN_star_number);
                if(!PN_converged_on_envelope_mass(stardata,
                                                  star))
                {
                    /*
                     * Our envelope mass should be within 1% of
                     * stardata->preferences->PPN_envelope_mass
                     */
                    Dprint("PN reject star %d model %d\n",
                           PN_star_number,
                           stardata->model.model_number);
                    stardata->model.reject_shorten_timestep =  REJECT_PN;
                    star->reject_shorten_timestep = REJECT_PN;
                }
            }
        }
#endif // STELLAR_POPULATIONS_ENSEMBLE


        if(stardata->preferences->resolve_stellar_type_changes
           &&
           smooth_evolution_checker(stardata,
                                    FALSE,
                                    TRUE,
                                    TRUE))
        {
            Foreach_star(star)
            {
                /*
                 * Stellar type has changed : reject to
                 * zoom in on this change if we haven't had
                 * any events to cause this.
                 */
                struct star_t * const prev = &stardata->previous_stardata->star[star->starnum];
                if(star->stellar_type <= HeWD &&
                   prev->stellar_type <= HeWD &&
                   star->stellar_type !=
                   prev->stellar_type)
                {
                    stardata->model.reject_shorten_timestep = REJECT_STELLAR_TYPE_CHANGE;
        }
            }
        }


        /*
         * Test for RLOF in this timestep, but no RLOF in the previous timestep
         */
        const unsigned int RLOF_state = test_for_roche_lobe_overflow(stardata,
                                                                     TEST_RLOF_ONLY_THIS_TIMESTEP);
        const unsigned int prev_RLOF_state = test_for_roche_lobe_overflow(stardata->previous_stardata,
                                                                          TEST_RLOF_ONLY_THIS_TIMESTEP);
        Dprint("test for RLOF rejection prev = %u (require %u), now = %u (require not %u) -> %s\n",
               prev_RLOF_state,
               (unsigned int)EVOLUTION_ROCHE_OVERFLOW_NONE,
               RLOF_state,
               (unsigned int)EVOLUTION_ROCHE_OVERFLOW_NONE,
               Evolution_loop_string(RLOF_state)
            );
        if(prev_RLOF_state == EVOLUTION_ROCHE_OVERFLOW_NONE &&
           RLOF_state != EVOLUTION_ROCHE_OVERFLOW_NONE)
        {
            Dprint("RLOF reject : went from no RLOF to RLOF\n");
            stardata->model.reject_shorten_timestep = REJECT_RLOF;
            return;
        }

        /*
         * Mass change tests: don't let mass change too quickly
         */
        const double maxfm = 0.1;
        Dprint("mass checks\n");
        if(Is_not_zero(maxfm))
        {
#define Mchange(k) (Is_zero(stardata->previous_stardata->star[(k)].mass) ? 0.0 : \
                    fabs(1.0 - stardata->star[(k)].mass /               \
                         stardata->previous_stardata->star[(k)].mass))

            if(evolution_is_smooth == TRUE &&
               stardata != stardata->previous_stardata)
            {
                Dprint("evolution is smooth\n");
                Star_number k;
                Starloop(k)
                {
                    Dprint("Mchange %d = %g (%g vs %g)\n",
                           k,
                           Mchange(k),
                           stardata->previous_stardata->star[(k)].mass,
                           stardata->star[(k)].mass);

                    Dprint("star %d : %d %d %d\n",
                           k,
                           stardata->star[k].stellar_type != MASSLESS_REMNANT,
                           stardata->star[k].stellar_type == stardata->previous_stardata->star[k].stellar_type ,
                           Mchange(k) > maxfm);

                           if(stardata->star[k].stellar_type != MASSLESS_REMNANT &&
                       stardata->star[k].stellar_type == stardata->previous_stardata->star[k].stellar_type &&
                       Mchange(k) > maxfm)
                    {
                        stardata->star[k].reject_shorten_timestep = REJECT_ANGMOM;
                        Dprint("ZOOM REJECT star %d Yes by MCHANGE dt = %g (deltaM/M %g : M was %g, is %g) <<<\n",
                               k,
                               stardata->model.dt,
                               Mchange(k),
                               stardata->previous_stardata->star[k].mass,
                               stardata->star[k].mass
                            );
                        return;
                    }
                }
            }
            else
            {
                Dprint("evolution is not smooth\n");
            }
        }

        /*
         * Angular momentum change tests: don't let it
         * change by more than preferences->max_stellar_angmom_change.
         *
         * Ignore this test if max_stellar_angmom_change is zero.
         */
        if(Is_not_zero(stardata->preferences->max_stellar_angmom_change))
        {
#define Jchange(k) (Is_zero(stardata->previous_stardata->star[(k)].angular_momentum) ? 0.0 : \
                    fabs(1.0 - stardata->star[(k)].angular_momentum /   \
                         stardata->previous_stardata->star[(k)].angular_momentum))

            Dprint("check reject deltaJ %g %g [ %g %g ] (tidal factors %g %g) can reject? %s events? %s smooth? %s : %d,%d vs %d,%d : binary %d vs %d\n",
                   Jchange(0),
                   Jchange(1),
                   stardata->star[(1)].angular_momentum,
                   stardata->previous_stardata->star[(1)].angular_momentum,
                   Is_not_zero(stardata->common.orbit.angular_frequency) ? (stardata->star[0].omega / stardata->common.orbit.angular_frequency) : 0.0,
                   Is_not_zero(stardata->common.orbit.angular_frequency) ? (stardata->star[1].omega / stardata->common.orbit.angular_frequency) : 0.0,
                   Yesno(can_and_shorten),
                   Yesno(events_pending(stardata)),
                   Yesno(evolution_is_smooth),
                   stardata->star[0].stellar_type,
                   stardata->previous_stardata->star[0].stellar_type,
                   stardata->star[1].stellar_type,
                   stardata->previous_stardata->star[1].stellar_type,
                   _System_is_binary(stardata) ,
                   _System_is_binary(stardata->previous_stardata));

            /*
             * Do not allow stellar angular momentum to change too much
             * e.g. to avoid moving in/out of tidal lock
             */
            if(evolution_is_smooth == TRUE)
            {
                Star_number k;
                Starloop(k)
                {
                    if(stardata->star[k].stellar_type != MASSLESS_REMNANT &&
                       stardata->star[k].angular_momentum > MIN_ANGMOM_FOR_TIMESTEP_AND_REJECTION_TESTS &&
                       Jchange(k) > stardata->preferences->max_stellar_angmom_change)
                    {
                        stardata->star[k].reject_shorten_timestep = REJECT_ANGMOM;
                        Dprint("ZOOM REJECT star %d Yes by JCHANGE dt = %g (deltaJ/J %g : J was %g, is %g) <<<\n",
                               k,
                               stardata->model.dt,
                               Jchange(k),
                               stardata->previous_stardata->star[k].angular_momentum,
                               stardata->star[k].angular_momentum
                            );
                        return;
                    }
                }
            }
        }

#ifdef GAIAHRD

        /*
         * Perhaps reject timestep when things change too fast to make
         * the Gaia-like HRD.
         *
         * First check we can :
         * note we ignore stellar type
         * changes in this check.
         */
        if(0 && smooth_evolution_checker(stardata,FALSE,TRUE,TRUE))
        {
            /*
             * When constructing Gaia-like HRDs, do not let
             * Teff,L change too quickly
             */
            const double f = 1.99;
            const double max_dL = f * stardata->preferences->gaia_L_binwidth;
            const double max_dTeff = f * stardata->preferences->gaia_Teff_binwidth;

            /*
             * Get the next Gaia data
             */
            struct gaiaHRD_chunk_t this;
            gaia_log(stardata,
                     &this);

            /*
             * If we are going to log, check that we
             * haven't changed too much
             */
            if(this.do_log == TRUE &&
               Is_not_zero(stardata->common.gaia_Teff))
            {
                Boolean reject_HRD;

                /*
                 * Check L and Teff don't change too much
                 */
                if(fabs(this.L_binned - stardata->common.gaia_L) > max_dL)
                {
                    stardata->star[0].reject_shorten_timestep = REJECT_LUMINOSITY;
                    reject_HRD = TRUE;
                }
                else if(fabs(this.Teff_binned - stardata->common.gaia_Teff) > max_dTeff)
                {
                    stardata->star[0].reject_shorten_timestep = REJECT_TEFF;
                    reject_HRD = TRUE;
                }
                else
                {
                    reject_HRD = FALSE;
                }

                /*
                 * return if rejected
                 */
                if(reject_HRD == TRUE)
                {
                    return;
                }
            }
        }
#endif//GAIAHRD
    }
}
