#include "../binary_c.h"
No_empty_translation_unit_warning;


void evolution_rejection_same_tests(struct stardata_t * Restrict const stardata)
{
    /*
     * Test for whether evolution should be rejected and
     * restarted with the same timestep.
     *
     * If we cannot reject, simply do nothing
     */
    const Boolean can = can_reject_with_same_timestep(stardata);
    Dprint("can reject? %s\n",Yesno(can));

    if(can == TRUE)
    {
#ifdef ADAPTIVE_RLOF2
        Dprint("%d ev rej tests : adapting? %s\n",
               stardata->model.model_number,
               Truefalse(stardata->model.adapting_RLOF2 == TRUE));
        if(stardata->model.adapting_RLOF2 == TRUE)
        {
#ifdef NEW_ADAPTIVE_RLOF_CODE
            /*
             * If both stars have not converged we must
             * redo the timestep.
             */
            Boolean converged = FALSE;
            Star_number k;
            Starloop(k)
            {
                Dprint("Check star %d adaptive2_converged %s\n",
                       k,
                       Truefalse(stardata->star[k].adaptive2_converged));

                if(stardata->star[k].adaptive2_converged == TRUE)
                {
                    converged = TRUE;
                    break;
                }
            }

            Dprint("Hence system converged = %s\n",Truefalse(converged));
            if(converged == FALSE)
            {
                /*
                 * Update the RLOF rate guess
                 */
                k = stardata->model.adaptive_RLOF2_overflower;
                stardata->star[k].adaptive2_mdot = RLOF_adaptive_mass_transfer_rate2_update(stardata);
                printf("Updated RLOF %g\n",stardata->star[k].adaptive2_mdot);

                /*
                 * Store some variables in the mask so they are preserved
                 * on restore: these are the adaptive RLOF variables
                 * and the timestep, which shouldn't change
                 */
                if(stardata->model.restore_mask == NULL)
                {
                    stardata->model.restore_mask = New_stardata_mask;
                    stardata->model.restore_mask_contents = New_stardata_mask;
                }
                Stardata_mask(stardata,star[k].adaptive2_mdot);
                Stardata_mask(stardata,star[k].adaptive2_converged);
                Stardata_mask(stardata,star[k].prevMdot0);
                Stardata_mask(stardata,star[k].prevMdot1);
                Stardata_mask(stardata,star[k].prevR0);
                Stardata_mask(stardata,star[k].prevR1);
                Stardata_mask(stardata,star[k].prevRL0);
                Stardata_mask(stardata,star[k].prevRL1);
                Stardata_mask(stardata,star[k].adaptive2_Mdot_low);
                Stardata_mask(stardata,star[k].adaptive2_Mdot_high);
                Stardata_mask(stardata,star[k].adaptive2_Rexcess_low);
                Stardata_mask(stardata,star[k].adaptive2_Rexcess_high);
                Stardata_mask(stardata,star[k].adaptive2_converged);
                Stardata_mask(stardata,star[k].prev_radius_excess);
                Stardata_mask(stardata,star[k].prev_target_radius);
                Stardata_mask(stardata,model.adapting_RLOF2);
                Stardata_mask(stardata,model.adaptive_RLOF2_adapt_count);
                Stardata_mask(stardata,model.adaptive_RLOF2_reject_count);
                Stardata_mask(stardata,model.adaptive_RLOF2_drdm);
                Stardata_mask(stardata,model.adaptive_RLOF2_overflower);
                return;
            }
            else
            {
                /*
                 * Converged
                 */
                stardata->model.adaptive_RLOF2_adapt_count++;
            }
#endif
        }
#endif //ADAPTIVE_RLOF2

    }

}
