#include "../binary_c.h"
No_empty_translation_unit_warning;


/*
 * Apply derivatives
 *
 * return value: one of the SYSTEM_ACTION_* macros
 */

int apply_derivatives(struct stardata_t * const stardata,
                      const Boolean RLOF,
                      const Boolean can_reject,
                      const Boolean can_reject_and_shorten)
{
    /*
     * Apply derivatives
     */

#ifdef NEW_ADAPTIVE_RLOF_CODE
    show_derivatives(stardata);
#else
    if(stardata->preferences->derivative_logging == TRUE)
    {
        show_derivatives(stardata);
    }
#endif // NEW_ADAPTIVE_RLOF_CODE

    /*
     * Return value should be the first error
     */
#define Update_retval(X) if(retval==0) { retval = (X); }

    int retval = 0;
    if(stardata->preferences->tasks[BINARY_C_TASK_APPLY_DERIVATIVES] == TRUE)
    {
        Dprint("apply derivatives\n");
        for(unsigned int i=0; i<DERIVATIVES_GROUP_NUMBER; i++)
        {
            if(derivatives_order[i] == DERIVATIVES_GROUP_SYSTEM)
            {
                /* apply orbital derivatives */
                Dprint("Main loop: update and check ecc dtm=%g\n",
                       stardata->model.dtm);
                Nancheck(stardata->common.orbit.angular_momentum);
                const Boolean u =
                    apply_orbital_angular_momentum_and_eccentricity_derivatives(stardata,
                                                                                RLOF);
                if(u == FALSE)
                {
                    Dprint("apply derivatives failed\n");
                    Update_retval(SYSTEM_ACTION_UPDATE_MASSES_AND_ANGMOM_FAILED);
                }
                else
                {
                    Dprint("post-apply orb and ecc : J=%g NaN? %d\n",
                           stardata->common.orbit.angular_momentum,
                           isnan(stardata->common.orbit.angular_momentum));
                    Nancheck(stardata->common.orbit.angular_momentum);
                    Update_retval(SYSTEM_ACTION_CONTINUE_RLOF);
                }
            }

            if(derivatives_order[i] == DERIVATIVES_GROUP_STELLAR)
            {
                /*
                 * apply stellar mass and angular momentum derivatives
                 */
                if(apply_stellar_mass_and_angular_momentum_derivatives(stardata,RLOF) == FALSE)
                {
                    Dprint("apply stellar mass & angmom derivs failed\n");
                    Update_retval(SYSTEM_ACTION_UPDATE_MASSES_AND_ANGMOM_FAILED);
                }
                Dprint("post apply stellar\n");
            }

            if(derivatives_order[i] == DERIVATIVES_GROUP_OTHER)
            {
                /*
                 * apply other derivatives (which also may be zero)
                 */
                if(apply_other_stellar_derivatives(stardata,
                                                   RLOF,
                                                   can_reject,
                                                   can_reject_and_shorten) == FALSE)
                {
                    Update_retval(SYSTEM_ACTION_UPDATE_OTHER_STELLAR_FAILED);
                }
            }

            if(derivatives_order[i] == DERIVATIVES_GROUP_EXTRA)
            {
                Call_function_hook(extra_apply_derivatives);
            }
        }
    }
    else
    {
        retval = SYSTEM_ACTION_CONTINUE_RLOF;
    }

    if(stardata->common.orbit.couple_tides == TRUE)
    {
        /*
         * Stars should spin at the rate of the orbit
         * i.e. we assume tides are instantaneous
         */
        const double Jwas =
            stardata->common.orbit.angular_momentum +
            stardata->star[0].angular_momentum +
            stardata->star[1].angular_momentum;
        Foreach_star(star)
        {
            star->omega = stardata->common.orbit.angular_frequency;
            star->angular_momentum = star->omega * moment_of_inertia(stardata,star,star->effective_radius);
        }
        const double Jis =
            stardata->common.orbit.angular_momentum +
            stardata->star[0].angular_momentum +
            stardata->star[1].angular_momentum;

        /*
         * Conserve angular momentum
         */
        stardata->common.orbit.angular_momentum -= Jis - Jwas;

        /*
         * Recompute orbit
         */
        update_orbital_variables(stardata,
                                 &stardata->common.orbit,
                                 &stardata->star[0],
                                 &stardata->star[1]);

        /*
         * And decouple tides
         */
        stardata->common.orbit.couple_tides = FALSE;
    }

    Dprint("%snew dM/dt M1=%g M2=%g RLOF %g separation %30.20e, new Jorb %30.20e\n%s",
           stardata->store->colours[MAGENTA],
           stardata->star[0].mass,
           stardata->star[1].mass,
           stardata->star[1].derivative[DERIVATIVE_STELLAR_MASS_RLOF_LOSS],
           stardata->common.orbit.separation,
           stardata->common.orbit.angular_momentum,
           stardata->store->colours[COLOUR_RESET]);

    Dprint("apply derivatives return %d\n",retval);
    return retval;
}
