#include "../binary_c.h"
No_empty_translation_unit_warning;

Boolean stellar_type_exceeded(struct stardata_t * const stardata,
                              struct star_t * const star)
{
    /*
     * return TRUE if the stellar type exceeds the
     * max_stellar_type (set as an argument)
     */
    const Stellar_type max =
        stardata->preferences->max_stellar_type[star->starnum];
    if((star->stellar_type != MASSLESS_REMNANT ||
        max > 0)
       &&
       star->stellar_type > abs(max))
    {
        return TRUE;
    }
    else
    {
        return FALSE;
    }
}
