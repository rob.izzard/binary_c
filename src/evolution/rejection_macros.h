#pragma once
#ifndef REJECTION_MACROS_H
#define REJECTION_MACROS_H


/*
 * Macros specific to binary_c's timestep rejection algorithm.
 *
 * Definitions are in rejection_codes.def
 */
#include "rejection_codes.def"

/*
 * Construct rejection codes
 */
#undef X
#define X(CODE,TEXT) CODE,
enum { REJECTION_CODES };

#undef X
#define X(CODE,TEXT) #CODE,
static char * rejection_code_macros[] Maybe_unused = { REJECTION_CODES };

#undef X
#define X(CODE,TEXT) TEXT,
static char * rejection_code_strings[] Maybe_unused = { REJECTION_CODES };

#define Reject_string(N) (rejection_code_strings[(N)])


/*
 * Construct rejection results
 */
#undef X
#define X(CODE,TEXT) CODE,
enum { REJECTION_RESULTS };

#undef X
#define X(CODE,TEXT) #CODE,
static char * rejection_result_macros[] Maybe_unused = { REJECTION_RESULTS };

#undef X
#define X(CODE,TEXT) TEXT,
static char * rejection_result_strings[] Maybe_unused = { REJECTION_RESULTS };

#define Rejection_result_string(N) (rejection_result_strings[(N)])

#undef X

/*
 * Options for when we cannot shorten as set in
 * stardata->preferences->cannot_shorten_timestep_policy
 */
#define X(CODE) CANNOT_SHORTEN_##CODE,
enum { CANNOT_SHORTEN_OPTIONS_LIST };
#undef X

#endif // REJECTION_MACROS_H
