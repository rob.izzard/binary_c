#include "../binary_c.h"
No_empty_translation_unit_warning;


void zero_stellar_derivatives(struct stardata_t * Restrict const stardata)
{
    /*
     * Set all stellar derivatives to zero
     */
    Star_number k;
    Starloop(k)
    {
        Zero_stellar_derivatives(&stardata->star[k]);
    }
}
