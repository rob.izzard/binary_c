#include "../binary_c.h"
No_empty_translation_unit_warning;


/*
 * Initialize the model struct, but preserve the log_fp
 * file pointer and ensemble cdict
 */

void init_model(struct model_t * Restrict const model)
{
#ifdef FILE_LOG
    FILE *fp = model->log_fp;
#endif
    memset(model, 0, sizeof(*model));
#ifdef FILE_LOG
    model->log_fp = fp;
#endif
    model->merged = FALSE;
    model->probability = 1.0;
    model->comenv_count = 0;
#ifdef RLOF_ABC
    model->rlof_type = RLOF_NO_RLOF;
    model->do_rlof_log = FALSE;
    model->rlof_count = 0;
#endif
#ifdef BINARY_C_API
    model->id_number = -1; // default to -1
#endif
    int i;
    for (i = 0; i < LOG_NFLAGS; i++)
    {
        model->logflags[i] = FALSE;
        model->logstack[i] = LOG_NONE;
        model->logflagstrings[i][0] = '\0';
    }
    model->dt_zoomfac = 1.0;

#ifdef ZW2019_SNIA_LOG
    model->ZW2019_Hestar = -1;
    model->ZW2019_COWD = -1;
#endif // ZW2019_SNIA_LOG
#ifdef ADAPTIVE_RLOF2
    model->adaptive_RLOF2_reject_count = 0;
    model->adaptive_RLOF2_adapt_count = 0;
    model->adapting_RLOF2 = FALSE;
#endif // ADAPTIVE_RLOF2

#ifdef EVENT_BASED_LOGGING
    /* SN  stuff */
    model->event_based_logging_SN_counter = 0;

    /* RLOF stuff */
    model->event_based_logging_RLOF_prev_in_RLOF = FALSE; // use this to check for whether we're in RLOF phase
    model->event_based_logging_RLOF = FALSE;
    model->event_based_logging_RLOF_unstable = FALSE;
    model->event_based_logging_RLOF_starting = FALSE;
    model->event_based_logging_RLOF_ending = FALSE;
    model->event_based_logging_RLOF_disk_accretion = FALSE;
#endif // EVENT_BASED_LOGGING
}
