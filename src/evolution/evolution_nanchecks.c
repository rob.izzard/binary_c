#include "../binary_c.h"
No_empty_translation_unit_warning;


void evolution_nanchecks(struct stardata_t * Restrict const stardata Maybe_unused)
{
#if defined NUCSYN && defined NANCHECKS
    /* force nan! */
    //stardata->star[0].Xyield[100]=0.0/0.0;

    nancheck("XZAMS",stardata->preferences->zero_age.XZAMS,ISOTOPE_ARRAY_SIZE);
    nancheck("Xsolar",stardata->preferences->zero_age.Xsolar,ISOTOPE_ARRAY_SIZE);
    Foreach_star(star)
    {
        nancheck("evolution_nanchecks Xenv",star->Xenv,ISOTOPE_ARRAY_SIZE);
        nancheck("evolution_nanchecks Xacc",star->Xacc,ISOTOPE_ARRAY_SIZE);
        nancheck("evolution_nanchecks Xhbb",star->Xhbb,ISOTOPE_ARRAY_SIZE);
        nancheck("evolution_nanchecks Xyield",star->Xyield,ISOTOPE_ARRAY_SIZE);
    }
#endif // NUCSYN && NANCHECKS

}
