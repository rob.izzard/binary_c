#include "../binary_c.h"
No_empty_translation_unit_warning;

/*
 * Binary_c's main loop to evolve a binary-star system in time.
 *
 * Returns non-zero on error or failure (for any reason), otherwise 0.
 */
#include "evolve_system_binary_c.h"

int evolve_system_binary_c(struct stardata_t * Restrict const stardata)
{
    eprint("EVOLVE SYSTEM stardata=%p previous_stardata=%p store = %p (buffer = %p, size %zu) model_time = %30.20e, model_number = %d\n",
           (void*)stardata,
           (void*)stardata->previous_stardata,
           (void*)stardata->store,
           (void*)stardata->tmpstore->raw_buffer,
           stardata->tmpstore->raw_buffer_size,
           stardata->model.time,
           stardata->model.model_number
          );

    /*
     * Setup
     */
    reset_binary_c_timeout();
    Start_evolution_timer;

    /*
     * Call start_of_evolution to set things up,
     * this is required also for restarts
     */
    eprint("call start_of_evolution\n");
    start_of_evolution(stardata);

    /*
     * start with an evolving system, stop looping
     * when this is FALSE.
     * Also don't reject systems by default.
     */
    Boolean evolving = TRUE;
    Rejection reject = REJECTION_RESULT_NONE;

    /*
     * Set previous stardata(s)
     */
    eprint("Save to previous\n");
    evolution_save_to_previous(stardata);

    eprint("start evolution t=%g M1=%g (%g) M2=%g a=%g P=%g\n",
           stardata->model.time,
           stardata->star[0].mass,
           stardata->star[0].phase_start_mass,
           stardata->star[1].mass,
           stardata->common.orbit.separation,
           stardata->common.orbit.period
          );

    while(evolving == TRUE)
    {
        if(stardata->preferences->float_overflow_checks > 0)
        {
            floating_point_clear_flags(stardata);
        }

        stardata->model.model_number++;
        eprint("Evolution loop top : model %d\n",
               stardata->model.model_number);

        /*
         * Start of timestep initialization. If this returns
         * EVOLUTION_BREAK we shouldn't perform the timestep's
         * calculations, and we should stop evolving.
         */
        int evstatus = evolution_loop_top(stardata);

        /*
         * apply evolutionary-command events
         */
        apply_evolutionary_commands(stardata,FALSE,TRUE,FALSE);

        if(evstatus == EVOLUTION_BREAK)
        {
            eprint("Break evolution\n");
            evolving = FALSE;
        }
        else
        {
            /*
             * We can evolve for this timestep.
             *
             * Initialize things at the start of the step.
             */
            eprint("Initialize start of step\n");
            initialize_system_every_timestep(stardata);

            /*
             * Apply pre-time-evolution hook
             */
            Call_function_hook(pre_time_evolution);

            /*
             * Do time evolution.
             */
            eprint("call time_evolution() with dtm = %g\n", stardata->model.dtm);
            evstatus = time_evolution(stardata, GENERIC_SYSTEM_CALL);
            eprint("time_evolution returned %d \"%s\"\n",
                   evstatus,
                   Evolution_loop_string(abs(evstatus)));

            /*
             * Apply post-time-evolution hook
             */
            Call_function_hook(post_time_evolution);

            /*
             * Check if we can and should reject this timestep's evolution
             * because something went wrong in evolution().
             */
            reject = check_reject_flags(stardata);

            if(reject == REJECTION_RESULT_NONE ||
               reject == REJECTION_RESULT_WANT_TO_SHORTEN_TIMESTEP_BUT_CANNOT)
            {
                /*
                 * Evolution was successful or we should
                 * continue with the same timestep knowing
                 * we'd really prefer to shorten it
                 */
                if(reject == REJECTION_RESULT_NONE)
                {
                    eprint("evolution was successful\n");
                    evolution_success(stardata);
                }

                /*
                 * Check for Roche-lobe overflow
                 */
                eprint("test for RLOF\n");
                const int rlof_state = test_for_roche_lobe_overflow(stardata,
                                       TEST_RLOF_THIS_AND_PREVIOUS_TIMESTEP);

                eprint("rlof_state %d %s\n",
                       rlof_state,
                       Evolution_loop_string(rlof_state));

                if(rlof_state == EVOLUTION_ROCHE_OVERFLOW ||
                   rlof_state == EVOLUTION_ROCHE_OVERFLOW_AFTER_SPIRAL_IN)
                {
                    /*
                     * We are in Roche-lobe overflow
                     */
                    eprint("EVRLOF : -> RLOF (%g %g)\n",
                           stardata->star[0].radius / stardata->star[0].roche_radius,
                           stardata->star[1].radius / stardata->star[1].roche_radius
                          );
                    stardata->common.RLOF_do_overshoot = FALSE;

                    /*
                     * Test for a contact system.
                     *
                     * This is one that is either truly in contact, or
                     * whose spiral-in time is shorter than the timestep.
                     */
                    if(rlof_state == EVOLUTION_ROCHE_OVERFLOW_AFTER_SPIRAL_IN)
                    {
                        eprint("EVRLOF -> spiral in -> contact\n");
                        contact_system(stardata,
                                       FALSE,
                                       CONTACT_AFTER_SPIRAL_IN);
                    }
                    else if(test_if_primary_still_fills_roche_lobe(stardata) == EVOLUTION_CONTACT)
                    {
                        eprint("EVRLOF -> fills Roche lobe -> contact\n");
                        contact_system(stardata,
                                       FALSE,
                                       CONTACT_AFTER_RLOF);
                    }
                    else
                    {
                        /*
                         * Begin RLOF.
                         */
                        start_RLOF(stardata);
                    }
                }
                else
                {
                    stardata->model.in_RLOF = FALSE;
                }

                if(stardata->model.sgl == FALSE &&
                   Is_zero(stardata->common.orbit.separation) &&
                   NEITHER_STAR_MASSLESS)
                {
                    contact_system(stardata,
                                   FALSE,
                                   CONTACT_WITHOUT_RLOF);
                }

                /*
                 * Do updates that require a forward-Euler
                 * step only.
                 */
                evolution_forward_Euler(stardata);

                /*
                 * Update derivatives of both stars
                 */
                set_derived_derivatives(stardata, NULL, -1);

                /*
                 * Detect stellar type changes
                 */
#ifdef STELLAR_TYPE_CHANGE_EVENTS
                evolution_detect_stellar_type_change(stardata);
#endif // STELLAR_TYPE_CHANGE_EVENTS

                /*
                 * If evstatus is "STOP" we should stop evolving
                 * after we've finishsed up this timestep.
                 */
                if(abs(evstatus) == EVOLUTION_STOP)
                {
                    eprint("evstatus (set above) wants us to STOP");
                    evolving = FALSE;
                }
            }
        }

        if(stardata->model.timestep_rejected_without_shorten)
        {
            /*
             * previous timestep was rejected but we could not
             * shorten the timestep: thus don't reject this one
             * because it's already running with a short timestep.
             *
             * We will already have bailed out if
             * stardata->preferences->cannot_shorten_timestep_policy == CANNOT_SHORTEN_FAIL
             */
            reject = REJECTION_RESULT_NONE;
            stardata->model.timestep_rejected_without_shorten = FALSE;
        }
        else if(reject == REJECTION_RESULT_WANT_TO_SHORTEN_TIMESTEP_BUT_CANNOT)
        {
            if(stardata->preferences->cannot_shorten_timestep_policy == CANNOT_SHORTEN_RESTORE_AND_TRY_EVENTS &&
               events_pending(stardata))
            {

                /*
                 * We want to shorten the timestep, but cannot, instead
                 * we restore the previous stardata but keep the events
                 * queue in place to allow them to happen.
                 */
                evolution_restore_but_keep_events(stardata);
            }
        }


        if(reject == REJECTION_RESULT_SHORTEN_TIMESTEP ||
           reject == REJECTION_RESULT_SAME_TIMESTEP ||
           reject == REJECTION_RESULT_WANT_TO_SHORTEN_TIMESTEP_BUT_CANNOT)
        {
            /*
             * This step has been rejected and we can act on it.
             *
             * Note: if reject == REJECTION_RESULT_WANT_TO_SHORTEN_TIMESTEP_BUT_CANNOT
             */
            if(reject == EVOLUTION_REJECT_AND_RETURN_NOW)
            {
                /*
                 * We have hit a point where we've been told to
                 * simply return
                 */
                return EVOLUTION_REJECT_AND_RETURN_NOW;
            }
            else
            {
                /*
                 * Call evolution_rejected
                 * to do something about it, and unset the reject flag.
                 */
                eprint("CALL EV REJECTED AND SHORTEN TIMESTEP (why? %u %s)\n",
                       stardata->model.reject_shorten_timestep,
                       Reject_string(stardata->model.reject_shorten_timestep)
                      );
                erase_events(stardata);
                events_clean_log_stack(stardata);
                evolution_rejected(stardata);
                eprint("POST REJECT zoom %g\n",
                       stardata->model.dt_zoomfac);
                reject = FALSE;
            }
        }
        else
        {
            /*
             * Check if evolution needs to stop: if so, don't
             * catch events and stop evolving, but do log so
             * we know what happened.
             */
            const Boolean evstop = check_for_evolution_stop(stardata);
            if(evstop == TRUE)
            {
                evolving = FALSE;
                erase_events(stardata);
                sanity_checks(stardata);
                Evolution_logging;
                events_clean_log_stack(stardata);
            }
            else
            {
                /*
                 * catch events, e.g. supernovae, unstable RLOF,
                 * common-envelope evolution
                 */
                if(events_pending(stardata))
                {
                    eprint("\n\n\n\n>>>>>  EVENTS t=%30.20e <<<<<\n\n\n\n",
                           stardata->model.time);
                    catch_events(stardata);
                }

                /*
                 * Check for floating-point exceptions
                 */
                if(stardata->preferences->float_overflow_checks > 0)
                {
                    floating_point_exception_checks(stardata);
                }

                /*
                 * Save mass-loss history data
                 */
#ifdef SAVE_MASS_HISTORY
                save_mass_history(stardata);
#endif // SAVE_MASS_HISTORY

                /*
                 * Check for evolution split before
                 * we save_to_previous. If evolution_split
                 * does not return EVOLUTION_SPLIT_CONTINUE, it
                 * either splits the evolution here or restores
                 * the previously saved.
                 */
#ifdef EVOLUTION_SPLITTING
                if(unlikely(evolution_split(stardata, &evstatus) !=
                             EVOLUTION_SPLIT_CONTINUE))
                {
                    /*
                     * Keep evolving, but do nothing else: evolution_split
                     * does the administration of the splitting algorithm
                     */
                    evolving = TRUE;
                }
                else
#endif // EVOLUTION_SPLITTING
                {
                    /*
                     * Expire command stack
                     */
                    apply_evolutionary_commands(stardata,FALSE,FALSE,TRUE);

                    /*
                     * Sanity checks
                     */
                    sanity_checks(stardata);

                    /*
                     * Do logging
                     */
                    Evolution_logging;


                    /*
                     * Update timestep triggers
                     */
                    timestep_increment_fixed_timesteps(stardata);

                    /*
                     * Calculate the next timestep
                     */
                    set_next_timestep(stardata);

                    /*
                     * Clean events and their log stack
                     */
                    erase_events(stardata);
                    events_clean_log_stack(stardata);

                    /*
                     * Save the stardata to previous_stardata, unless evolving is FALSE,
                     * in which case we want to keep the previous in place
                     * just in case there is a stardata dump to follow
                     */
                    if(evolving)
                    {
                        eprint("LOOP this %d prev %d\n",
                               stardata->model.model_number,
                               stardata->previous_stardata->model.model_number);
                        evolution_save_to_previous(stardata);
                        eprint("have saved to previous\n");
                    }
                }
            }
        }
        eprint("end of evolving loop %d %g\n",
               stardata->star[0].stellar_type,
               stardata->star[0].age);
    }

    /*
     * The system has finished evolving. Clean up and return.
     */
    eprint("End of evolution\n");
    end_of_evolution(stardata);
    End_evolution_timer;
    evolution_cleanup(stardata, TRUE);
    return 0;
}
