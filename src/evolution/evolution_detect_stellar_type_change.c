#include "../binary_c.h"
No_empty_translation_unit_warning;



static Event_handler_function stellar_type_change_event_handler(
    void * const eventp Maybe_unused,
    struct stardata_t * const stardata,
    void * data Maybe_unused);

void evolution_detect_stellar_type_change(struct stardata_t * const stardata)
{
    /*
     * Detect stellar type changes and trigger them as
     * an event
     */
    Star_number k;
    Starloop(k)
    {
        if(stardata->star[k].stellar_type !=
           stardata->previous_stardata->star[k].stellar_type)
        {
            /*
             * Allocate data
             */
            void * data = Calloc(2,NUMBER_OF_STARS*sizeof(Stellar_type));
            if(data != NULL &&
               Add_new_event(stardata,
                             BINARY_C_EVENT_STELLAR_TYPE_CHANGE,
                             &stellar_type_change_event_handler,
                             NULL, // erase function
                             data,
                             NORMAL_EVENT)
               != BINARY_C_EVENT_DENIED)
            {
                Star_number kk;
                Stellar_type * i = (Stellar_type *) data;
                Starloop(kk)
                {
                    i[kk] = stardata->previous_stardata->star[kk].stellar_type;
                    i[kk+NUMBER_OF_STARS] = stardata->star[kk].stellar_type;
                }
            }
            else
            {
                /*
                 * Event allocation failed, free the memory
                 */
                Safe_free(data);
            }
            k = NUMBER_OF_STARS; /* exit loop */
        }
    }
}

/*
 * Event handling function for stellar type changes
 */

static Event_handler_function stellar_type_change_event_handler(
    void * const eventp Maybe_unused,
    struct stardata_t * const stardata Maybe_unused,
    void * data Maybe_unused)
{
    /*
     * This function is called when a stellar type changes.
     *
     * Note that the pointer *data is set above.
     */
    /*
    Stellar_type * i = (Stellar_type*) data;
    Star_number k;
    Printf("ST %.12g change from ",stardata->model.time);
    Starloop(k)
    {
        Printf("%d ",i[k]);
    }
    Printf("to ");
    Starloop(k)
    {
        Printf("%d ",i[k+NUMBER_OF_STARS]);
    }
    Printf("\n");
    */
    return NULL;
}
