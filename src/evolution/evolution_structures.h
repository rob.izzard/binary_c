#pragma once
#ifndef EVOLUTION_STRUCTURES_H
#define EVOLUTION_STRUCTURES_H

/* struct to store data that is compared for logging purposes */
struct diffstats_t
{
    double mass[NUMBER_OF_STARS],mc[NUMBER_OF_STARS];
    double radius[NUMBER_OF_STARS],roche_radius[NUMBER_OF_STARS];
    double age[NUMBER_OF_STARS],tms[NUMBER_OF_STARS];
    double omega[NUMBER_OF_STARS],omega_crit[NUMBER_OF_STARS];
    double luminosity[NUMBER_OF_STARS],accretion_luminosity[NUMBER_OF_STARS];
    double orbital_separation;
    double orbital_period;
    double orbital_eccentricity;
    double orbital_angular_frequency;
    double novarate[NUMBER_OF_STARS],v_eq_ratio[NUMBER_OF_STARS];
    Time time;
    Stellar_type stellar_type[NUMBER_OF_STARS];
    Stellar_type hybrid_HeCOWD[NUMBER_OF_STARS];
    Supernova_type SN_type[NUMBER_OF_STARS];
    Nova_type nova[NUMBER_OF_STARS];
    Boolean sgl;
    Boolean artificial_accretion;
    Boolean overspin[NUMBER_OF_STARS];
};


struct difflogitem_t {
    struct stardata_t * stardata;
    double m[NUMBER_OF_STARS];
    char * string;
    size_t n;
};

struct difflogstack_t {
    struct difflogitem_t ** items;
    size_t n;
    size_t alloc_size;
};



#endif // EVOLUTION_STRUCTURES_H
