#include "../binary_c.h"
No_empty_translation_unit_warning;


/*
 * Return TRUE if we require a convergence loop
 */
Boolean check_require_convergence_steps(struct stardata_t * const stardata)
{
#ifdef ADAPTIVE_RLOF2
    /*
     * If we're using the adaptive2 RLOF method, then we may need
     * to run a convergence loop
     */
    if(stardata->preferences->RLOF_method == RLOF_METHOD_ADAPTIVE2)
    {
        return TRUE;
    }
    else
#endif // ADAPTIVE_RLOF2
    {
        return FALSE;
    }
}
