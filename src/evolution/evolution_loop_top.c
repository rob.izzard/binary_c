#include "../binary_c.h"
No_empty_translation_unit_warning;



/*
 * Initialize at the start of the timestep
 *
 * Return EVOLUTION_BREAK to stop evolution immediately,
 * which should be unusual, otherwise return EVOLUTION_CONTINUE.
 */
int evolution_loop_top(struct stardata_t * Restrict const stardata)
{
#ifdef NO_IMMEDIATE_MERGERS
    if(THERE_IS_AN_IMMEDIATE_MERGER)
    {
        Dprint("Ending evolution() loop because there was an immediate merger\n");
        return EVOLUTION_BREAK;
    }
#endif

    Star_number k;
    Starloop(k)
    {
        stardata->star[k].SN_type = SN_NONE;
        stardata->star[k].reject_same_timestep = REJECT_NONE;
        stardata->star[k].reject_shorten_timestep = REJECT_NONE;
        stardata->star[k].merge_state = Max(stardata->star[k].merge_state-1,0);
    }
    stardata->model.supernova = FALSE;
    stardata->model.solver_step = 0;
    stardata->model.reject_shorten_timestep = REJECT_NONE;
    stardata->model.reject_same_timestep = REJECT_NONE;
    stardata->model.RLOF_recommended_timestep = 0.0;


#if defined SELMA_FIX_ACCRETED_MASS_IN_REJUVENATION || \
     defined SELMA_BETTER_TREATMENT_OF_MS_ACCRETORS
    stardata->star[0].last_mass  = stardata->star[0].mass;
    stardata->star[1].last_mass  = stardata->star[1].mass;
#endif // SELMA_*
#ifdef RLOF_ABC
    stardata->model.do_rlof_log=FALSE;
#endif //RLOF_ABC
    stardata->model.comenv_type = 0;

    stardata->common.had_event = FALSE;
#ifdef ADAPTIVE_RLOF2
    stardata->model.adapting_RLOF2 = FALSE;
#endif//ADAPTIVE_RLOF2
    return EVOLUTION_CONTINUE;
}
