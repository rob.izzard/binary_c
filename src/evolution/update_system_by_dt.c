#include "../binary_c.h"
No_empty_translation_unit_warning;

/*
 * Update masses, angular momenta and orbital variables to
 * values at time t+dt, then update the stellar structure
 * appropriately.
 *
 * NB This may change the stellar types as well, e.g.
 *    when hydrogen is accreted on top of a helium star or
 *    white dwarf.
 *
 * system_type can be:
 *
 * * DETACHED_SYSTEM_CALL (for system you know is detached)
 * * RLOF_SYSTEM_CALL or (for RLOFing system)
 * * GENERIC_SYSTEM_CALL (for non-RLOFing system, can be single or binary)
 */

int update_system_by_dt(struct stardata_t * Restrict const stardata,
                        const Evolution_system_type system_type,
                        const Boolean update_time)
{
    int stop = EVOLUTION_STATUS_OK;

    /*
     * Set mass and angular momentum derivatives to zero
     * before recalculating them
     */
    Dprint("zero derivs\n");
    zero_stellar_mass_and_angmom_derivatives(stardata);

    /*
     * Set minimum radius for accretion through a disc
     */
    Dprint("set rmin\n");
    RLOF_set_rmin(stardata);

    /*
     * Can we reject if derivative application fails?
     */
    const Boolean can_reject = can_reject_with_same_timestep(stardata);
    const Boolean can_reject_and_shorten = can_reject_and_shorten_timestep(stardata);


    if(system_type == GENERIC_SYSTEM_CALL)
    {
        /*
         * Generic system
         */
        Dprint("generic system");

        /*
         * test for RLOF, act depending on its stability
         */
        const Boolean RLOF =
            stardata->preferences->zero_age.multiplicity > 1 &&
            (test_for_roche_lobe_overflow(stardata,
                                          TEST_RLOF_THIS_AND_PREVIOUS_TIMESTEP) ==
             EVOLUTION_ROCHE_OVERFLOW ? TRUE : FALSE);

        Dprint("RLOF at t=%g? %s\n",
               stardata->model.time,
               Yesno(RLOF));

        const int stability = (RLOF == FALSE ?
                               RLOF_IMPOSSIBLE :
                               RLOF_stability_tests(stardata));

        Dprint("STABLE at t=%g? RLOF %d, stability %d \"%s\"\n",
               stardata->model.time,
               RLOF,
               stability,
               RLOF_stability_string(stability)
            );

        if(stability != RLOF_IMPOSSIBLE &&
           stability != RLOF_STABLE &&
           stability != RLOF_INSTABILITY_BLOCKED)
        {
            /*
             * Unstable mass transfer :
             *
             * the function RLOF_unstable_mass_transfer will take
             * care of changes to the stellar masses and updates
             * to structure by queueing an event which is then
             * handled at the end of the timestep.
             *
             * Note that this trigger is blocked on intermediate
             * steps.
             */
            Dprint("update masses UNSTABLE RLOF\n");
            RLOF_unstable_mass_transfer(stardata,
                                        stability,
                                        FALSE);
        }
        else
        {
            /*
             * Update the system with stable or no mass transfer
             */
            Dprint("update masses %s\n",RLOF==TRUE?"RLOF":(RLOF==FALSE?"detached":"unknown"));

            calculate_derivatives(stardata,
                                  RLOF);

            const int u = apply_derivatives(stardata,
                                            RLOF,
                                            can_reject,
                                            can_reject_and_shorten);
            Dprint("Caught %d from apply_derivatives : reject? %s\n",
                   u,
                   Yesno(u == EVOLUTION_LOOP_REJECT));
            if(u == SYSTEM_ACTION_UPDATE_MASSES_AND_ANGMOM_FAILED)
            {
                Dprint("Evolution Loop Reject\n");
                return EVOLUTION_LOOP_REJECT;
            }
        }

        /*
         * Test for non-RLOFing collision
         */
        Dprint("test for collision\n");
        if(stardata->preferences->zero_age.multiplicity > 1 &&
           RLOF==FALSE &&
           check_for_collision_at_periastron(stardata) == TRUE)
        {
            Dprint("CONTACT RLOF=%d %g %g \n",
                   RLOF,
                   Is_really_not_zero(stardata->star[0].roche_radius) ? (stardata->star[0].radius/stardata->star[0].roche_radius) : -1.0,
                   Is_really_not_zero(stardata->star[1].roche_radius) ? (stardata->star[1].radius/stardata->star[1].roche_radius) : -1.0
                );
            contact_system(stardata,
                           FALSE,
                           CONTACT_AFTER_COLLISION_AT_PERIASTRON);
        }
    }
    else if(system_type == RLOF_SYSTEM_CALL)
    {
        Dprint("RLOFing system mass changes");
        /*
         * RLOFing system mass changes
         *
         * Test for stability.
         */
        const int stability = RLOF_stability_tests(stardata);

        if(stability == RLOF_IMPOSSIBLE ||
           stability == RLOF_STABLE)
        {
            /*
             * stable mass transfer, or no RLOF at all
             */
            calculate_derivatives(stardata,
                                  TRUE);

            const int u = apply_derivatives(stardata,
                                            TRUE,
                                            can_reject,
                                            can_reject_and_shorten);

            if(u == SYSTEM_ACTION_UPDATE_MASSES_AND_ANGMOM_FAILED)
            {
                /* failure: return EVOLUTION_LOOP_REJECT */
                return EVOLUTION_LOOP_REJECT;
            }
            else if(u == SYSTEM_ACTION_END_RLOF)
            {
                Dprint("Caught SYSTEM_ACTION_END_RLOF\n");
                stop = EVOLUTION_LOOP;
            }
        }
        else
        {
            Dprint("Caught RLOF -> unstable mass transfer\n");
            /*
             * unstable mass transfer : we still must call the update function
             * so the derivatives are set to something *but* because the mass
             * transfer will be caught as an event we call with FALSE to
             * prevent the timestep being rejected for some other reason
             */
            calculate_derivatives(stardata,
                                  FALSE);

            const int u = apply_derivatives(stardata,
                                            FALSE,
                                            can_reject,
                                            can_reject_and_shorten);

            if(u == SYSTEM_ACTION_UPDATE_MASSES_AND_ANGMOM_FAILED)
            {
                return EVOLUTION_LOOP_REJECT;
            }

            stop = RLOF_unstable_mass_transfer(stardata,stability,FALSE) == 0
                ? EVOLUTION_LOOP
                : EVOLUTION_STATUS_OK;
            Dprint("unstable at %g : stop = %d %s : pending? %d, stability %d\n",
                   stardata->model.time,
                   stop,
                   Evolution_loop_string(stop),
                   events_pending(stardata),
                   stability
                );
        }
    }
    else
    {
        Dprint("Detached system mass changes");

        /*
         * Detached system mass changes
         */
        calculate_derivatives(stardata,
                              FALSE);

        const int u = apply_derivatives(stardata,
                                        FALSE,
                                        can_reject,
                                        can_reject_and_shorten);

        if(u == SYSTEM_ACTION_UPDATE_MASSES_AND_ANGMOM_FAILED)
        {
            stop = EVOLUTION_LOOP_REJECT;
        }
        else if(u == EVOLUTION_STATUS_OK)
        {
            Dprint("Called main_loop - was FALSE\n");
            stop = EVOLUTION_LOOP;
        }
        else
        {
            Dprint("main_loop returned TRUE\n");
        }
    }

    if(stop == EVOLUTION_LOOP_REJECT)
    {
        /*
         * loop is rejected: return
         */
        Dprint("return %d == EVOLUTION_LOOP_REJECT\n",stop);
        return EVOLUTION_LOOP_REJECT;
    }

    if(stop != EVOLUTION_LOOP_REJECT)
    {
        /*
         * Loop is not rejected, perform post-update accountancy
         */
        int status = 0;

        if(stop == 0)
        {
            /*
             * When should we rejuv, when should we not?
             * He-star fail suggests we should, but
             * other systems suggest we should not.
             */
            if(system_type == GENERIC_SYSTEM_CALL)
            {
                /*
                 * Code for a generic system
                 */
                save_detached_stellar_types(stardata);
                stardata->model.tphys0 = stardata->model.time;
                stardata->model.dtm0 = stardata->model.dtm;

#ifdef BSE
                if(stardata->model.in_RLOF)
                {
                    rejuvenate_MS_secondary_and_age_primary(stardata);
                }
#endif//BSE
            }
            else if(system_type==DETACHED_SYSTEM_CALL)
            {
                /*
                 * Detached system
                 */
                save_detached_stellar_types(stardata);
                if(stardata->model.intpol==0)
                {
                    stardata->model.tphys0 = stardata->model.time;
                    stardata->model.dtm0 = stardata->model.dtm;
                }
            }
            else
            {
                /*
                 * RLOFing system.
                 *
                 * Rejuvenate the secondary and
                 * age the primary if they are on
                 * the main sequence.
                 */
#ifdef BSE
                rejuvenate_MS_secondary_and_age_primary(stardata);
#endif
            }

            /*
             * Set the time to t+dt
             */
            if(update_time == TRUE)
            {
                update_the_time(stardata);

                Dprint("Advance the time: intpol=%d time was %30.22e (dt=%30.22e dtm=%30.22e) now %30.22e (tphys00 %30.22e, maxt=%g)\n",
                       stardata->model.intpol,
                       stardata->model.time-stardata->model.dtm,
                       stardata->model.dt,
                       stardata->model.dtm,
                       stardata->model.time,
                       stardata->model.tphys0,
                       stardata->model.max_evolution_time);
            }
            else
            {
                Dprint("Not not advance the time (update_time == FALSE)\n");
            }

            Nancheck(stardata->model.dt);
            Nancheck(stardata->model.dtm);

            /*
             * Do stellar evolution over the timestep
             */
            if(stardata->preferences->tasks[BINARY_C_TASK_CALCULATE_STELLAR_EVOLUTION])
            {
                status = stellar_evolution(stardata,system_type);
                if(stardata->preferences->extra_stellar_evolution_hook != NULL)
                {
                    stardata->preferences->extra_stellar_evolution_hook(stardata,
                                                                        system_type);
                }
            }
        }

        /*
         * Do binary stellar evolution over the timestep,
         * also checks for supernovae and updates the orbit
         * if required.
         */
        if(stardata->preferences->tasks[BINARY_C_TASK_CALCULATE_BINARY_EVOLUTION])
        {
            update_binary_star_variables(stardata,&status);

            Call_function_hook(extra_update_binary_star_variables);
        }

        int retval;
        if(stop!=0)
        {
            /*
             * Return without updating the time:
             * there has probably been a common envelope
             * or a merger or something like that.
             */
            retval = -stop;
            Dprint("retval forced to stop %d\n",retval);
        }
        else
        {
            /*
             * Update stellar spins (by conserving angular momenta)
             */
            calculate_spins(stardata);

            if(system_type==GENERIC_SYSTEM_CALL)
            {
                if(stardata->model.sgl==FALSE)
                {
                    /* in binaries, update the radius derivative */
                    adjust_radius_derivative(stardata);
                }
                else
                {
                    /* in single stars, make Roche lobes huge */
                    make_roche_lobe_radii_huge(stardata);
                }
            }
            else if(system_type==DETACHED_SYSTEM_CALL)
            {
                if(stardata->model.sgl==FALSE)
                {
                    /* in binaries, update the radius derivative */
                    adjust_radius_derivative(stardata);
                }
                else
                {
                    /* in single stars, make Roche lobes huge */
                    make_roche_lobe_radii_huge(stardata);
                }
                if(stardata->model.supernova==TRUE) stardata->model.dtm = 0.0;
            }

            /*
             * Update variables that save the phase start/end
             * of each star.
             */
            update_phase_variables(stardata);

            const Boolean exhausted = check_for_time_exhaustion(stardata,
                                                                stardata->model.intpol);
            const Boolean explicit_stop = check_for_evolution_stop(stardata);

            retval = (exhausted == TRUE || explicit_stop == TRUE)
                ? -EVOLUTION_STOP : status;
            Dprint("exhausted? (evolution) %d -> retval %d\n",
                   exhausted,
                   retval);
        }
        return retval;
    }


    Dprint("return from end %d\n",stop);
    return stop;
}
