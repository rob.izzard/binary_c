#include "../binary_c.h"
No_empty_translation_unit_warning;


/*
 * The binary_c stellar population nucleosynthesis framework.
 *
 * Contact:  rob.izzard@gmail.com
 *
 * https://binary_c.gitlab.io/
 * https://gitlab.com/binary_c/binary_c
 * https://groups.google.com/forum/#!forum/binary_c-nucsyn-announce
 * https://groups.google.com/forum/#!forum/binary_c-nucsyn-devel
 * https://twitter.com/binary_c_code
 * https://www.facebook.com/groups/149489915089142/
 *
 * Please see the files README, LICENCE and CHANGES,
 * and the doc/ directory for documentation.
 *
 *
 * Binary_c's main loop to evolve a binary system in time.
 */

int evolve_system(struct stardata_t * Restrict const stardata)
{
    /* first time setup */
    if(stardata->model.model_number == 0)
    {
        pre_evolution(stardata);
    }

    stardata->evolving = TRUE;

    /*
     * This stardata's stars could have nested stardata
     * hence we should recursively evolve over them first.
     */
    Foreach_star(star)
    {
        if(star->stardata != NULL)
        {
            evolve_system_binary_c(star->stardata);
        }
    }

    /*
     * Evolve the outermost stardata
     */
    const int retval = evolve_system_binary_c(stardata);

    post_evolution(stardata);

    return retval;
}
