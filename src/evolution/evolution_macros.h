#pragma once
#ifndef EVOLUTION_MACROS_H
#define EVOLUTION_MACROS_H

/** This file contains useful macros for the binary_c evolution loop **/

#include "evolution_status_codes.h"
#include "evolution_loop_codes.h"
#include "evolution_split_codes.h"
#include "evolution_system_call_codes.h"

// The maximum number of goes through the evolution loop
// NB under linux the timeout (via reset_binary_c_timeout) is a better
// way to ensure the program doesn't run forever
#define MAX_EVOLUTION_NUM_LOOPS 1e8

/* See test_for_roche_lobe_overflow for the use of MAX_INTPOL */
#define MAX_INTPOL 500
#define NEARLY_MAX_INTPOL 400

#define MAX_EVOLUTION_ITERATIONS 10000

/* some more useful macros */
#define Q12 (stardata->star[0].mass/stardata->star[1].mass)
#define Q21 (stardata->star[1].mass/stardata->star[0].mass)
#define MASS_PRODUCT (stardata->star[0].mass*stardata->star[1].mass)
#define ECC_SQUARED Pow2(stardata->common.orbit.eccentricity)
#define Separation_squared Pow2(stardata->common.orbit.separation)
#define JORB_SQUARED Pow2(stardata->common.orbit.angular_momentum)

#define THERE_IS_AN_IMMEDIATE_MERGER                    \
    (                                                   \
        Is_zero(stardata->model.time) &&                \
        (                                               \
            (Is_zero(stardata->star[0].mass)&&          \
             Is_not_zero(stardata->common.zero_age.mass[0]))   \
            ||                                          \
            (Is_zero(stardata->star[1].mass)&&          \
             Is_not_zero(stardata->common.zero_age.mass[1])    \
                )                                       \
            )                                           \
        )



#define Reset_num_thermal_pulses(A) if(stardata->star[(A)].num_thermal_pulses<-1.5) stardata->star[(A)].num_thermal_pulses=-1.0;


#define Evolution_logging                       \
    Dprint("calling difflog\n");                \
    evolution_difflog(stardata);                \
    Dprint("calling log every timestep\n");     \
    log_every_timestep(stardata);


#if (defined(TIMED) && defined(HAVE_FTIME))
#define Start_evolution_timer                   \
    struct timeb start_time,end_time;;          \
    ftime(&start_time);
#else
#define Start_evolution_timer
#endif

#if (defined TIMED && HAVE_FTIME)
#define End_evolution_timer                                             \
    ftime(&end_time);                                                   \
    printf("TIME took %lf seconds\n",                                   \
           ((double)end_time.time+(double)end_time.millitm/1000)-       \
           ((double)start_time.time+(double)start_time.millitm/1000));
#else
#define End_evolution_timer
#endif




#endif /* EVOLUTION_MACROS_H */
