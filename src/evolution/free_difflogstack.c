#include "../binary_c.h"
No_empty_translation_unit_warning;


void free_difflogstack(struct difflogstack_t * Restrict const logstack)
{
    /*
     * Free the local logstack and everything inside it.
     *
     * Note: we assume that clean_difflogstack has previously
     * been called on the stack, as it is in evolution_difflog.
     */
    if(logstack != NULL)
    {
        Safe_free(logstack->items);
        Unsafe_free(logstack);
    }
}
