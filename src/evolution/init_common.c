#include "../binary_c.h"
No_empty_translation_unit_warning;


void init_common(struct stardata_t * Restrict const stardata)
{
    struct common_t * common = &(stardata->common);

    /*
     * initialize the common struct
     */
#ifdef DISCS
    remove_discs(stardata,
                 DISC_CIRCUMBINARY,
                 stardata);
#endif

    /* the following are the actual settings */
    common->orbit.angular_momentum = 0.0;
    common->orbit.angular_frequency = 0.0;
    common->orbit.separation = 0.0;
    common->orbit.period = 0.0;
    common->orbit.eccentricity = 0.0;
    common->orbit.tcirc = 0.0;
    common->RLOF_speed_up_factor = 0.0;
    common->zero_age.eccentricity[0] = 0.0;
    common->zero_age.separation[0] = 0.0;
    common->diffstats_set = FALSE;
    common->done_single_star_lifetime = 0;
    common->file_log_prevlabel = NULL;
    memset(stardata->common.prevflags,
           0,
           NLOG_LABEL * sizeof(Boolean));
    memset(stardata->common.lockflags,
           0,
           sizeof(Boolean) * (LOCK_NFLAGS + 1)*NUMBER_OF_STARS);

#ifdef NUCSYN
    common->nucsyn_metallicity = DEFAULT_TO_METALLICITY;
#endif
    common->effective_metallicity = DEFAULT_TO_METALLICITY;

    //common->random_seed=random_seed();
    if(common->random_seed == 0)
    {
        common->random_seed = random_seed();
    }

#ifdef ADAPTIVE_RLOF
    common->mdot_RLOF_adaptive = 0.0;
    common->mdot_RLOF_BSE = 0.0;
#ifdef ADAPTIVE_RLOF_LOG
    common->used_rate =  RLOF_RATE_UNDEF;
    common->runaway_rlof = FALSE;
    common->thermal_cap = FALSE;
#endif
#endif
#ifdef VISCOUS_RLOF_TRIGGER_SMALLER_TIMESTEP
    common->viscous_RLOF_wants_smaller_timestep = FALSE;
#endif
    stardata->common.max_mass_for_second_dredgeup = DEFAULT_MAX_MASS_FOR_SECOND_DREDGEUP;

#ifdef EVENT_BASED_LOGGING
    common->event_based_logging_logstring_counter = 0;
#endif // EVENT_BASED_LOGGING


#ifdef EVENT_BASED_LOGGING
    common->event_based_logging_RLOF_counter = 0.0;

    /* New RLOF episode information */
    common->event_based_logging_RLOF_episode_number = 0;

    /* initial values RLOF episode */
    common->event_based_logging_RLOF_episode_initial_mass_accretor = 0;
    common->event_based_logging_RLOF_episode_initial_mass_donor = 0;

    common->event_based_logging_RLOF_episode_initial_radius_accretor = 0;
    common->event_based_logging_RLOF_episode_initial_radius_donor = 0;

    common->event_based_logging_RLOF_episode_initial_separation = 0;
    common->event_based_logging_RLOF_episode_initial_orbital_period = 0;

    common->event_based_logging_RLOF_episode_initial_stellar_type_accretor = 0;
    common->event_based_logging_RLOF_episode_initial_stellar_type_donor = 0;

    common->event_based_logging_RLOF_episode_initial_orbital_angular_momentum = 0;
    common->event_based_logging_RLOF_episode_initial_stability = 0;

    common->event_based_logging_RLOF_episode_initial_starnum_accretor = 0;
    common->event_based_logging_RLOF_episode_initial_starnum_donor = 0;

    common->event_based_logging_RLOF_episode_initial_time = 0;
    common->event_based_logging_RLOF_episode_initial_disk = 0;

    /* final values RLOF episode */
    common->event_based_logging_RLOF_episode_final_mass_accretor = 0;
    common->event_based_logging_RLOF_episode_final_mass_donor = 0;

    common->event_based_logging_RLOF_episode_final_radius_accretor = 0;
    common->event_based_logging_RLOF_episode_final_radius_donor = 0;

    common->event_based_logging_RLOF_episode_final_separation = 0;
    common->event_based_logging_RLOF_episode_final_orbital_period = 0;

    common->event_based_logging_RLOF_episode_final_stellar_type_accretor = 0;
    common->event_based_logging_RLOF_episode_final_stellar_type_donor = 0;

    common->event_based_logging_RLOF_episode_final_orbital_angular_momentum = 0;
    common->event_based_logging_RLOF_episode_final_stability = 0;

    common->event_based_logging_RLOF_episode_final_starnum_accretor = 0;
    common->event_based_logging_RLOF_episode_final_starnum_donor = 0;

    common->event_based_logging_RLOF_episode_final_time = 0;
    common->event_based_logging_RLOF_episode_final_disk = 0;

    /* Cumulative total values RLOF episode */
    common->event_based_logging_RLOF_episode_total_time_spent_masstransfer = 0;

    common->event_based_logging_RLOF_episode_total_mass_lost_from_common_envelope = 0;
    common->event_based_logging_RLOF_episode_total_mass_lost_from_accretor = 0;
    common->event_based_logging_RLOF_episode_total_mass_lost = 0;
    common->event_based_logging_RLOF_episode_total_mass_accreted = 0;
    common->event_based_logging_RLOF_episode_total_mass_transferred = 0;

    common->event_based_logging_RLOF_episode_total_mass_transferred_through_disk = 0;
    common->event_based_logging_RLOF_episode_total_time_spent_disk_masstransfer = 0;
#endif // EVENT_BASED_LOGGING
}
