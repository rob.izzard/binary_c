#ifdef __DO_WE_NEED_THIS

#pragma once
#ifndef EVOLUTION_ERROR_CODES_H
#define EVOLUTION_ERROR_CODES_H

#include "evolution_error_codes.def"

/*
 * Construct evolution error codes
 */
#undef X
#define X(CODE,TEXT) CODE,
enum { EVOLUTION_ERROR_CODES };

#undef X
#define X(CODE,TEXT) #CODE,
static char * evolution_error_code_macros[] Maybe_unused = { EVOLUTION_ERROR_CODES };

#undef X
#define X(CODE,TEXT) TEXT,
static char * evolution_error_code_strings[] Maybe_unused = { EVOLUTION_ERROR_CODES };

#define Evolution_error_string(N) Array_string(evolution_error_code_strings,(N))

#undef X
#endif // EVOLUTION_ERROR_CODES_H

#endif
