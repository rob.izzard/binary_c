#include "../binary_c.h"
No_empty_translation_unit_warning;


void start_RLOF(struct stardata_t * Restrict const stardata)
{
    /* Beginning of Roche Lobe Overflow */
    Dprint("Roche Lobe Overflow! Setting nuclear_timescale etc.\n");

    stardata->model.in_RLOF = TRUE;

    Dprint("RLOF off -> on (0 : r=%g roche_radius=%g, 1: r=%g roche_radius=%g)\n",
           stardata->star[0].radius,stardata->star[0].roche_radius,
           stardata->star[1].radius,stardata->star[1].roche_radius);

    /* Set the nuclear timescale in years and slow-down factor.*/
    set_nuclear_timescale_and_slowdown_factor(stardata);

    Dprint("Set nuclear timescale etc.\n");

    stardata->model.iter = 0;
    stardata->model.coalesce = FALSE;

    RLOF_stars;

    donor->effective_radius = Max(donor->roche_radius,donor->core_radius);
    accretor->effective_radius = accretor->radius;

#ifdef WD_KICKS
    if((stardata->star[0].stellar_type==TPAGB)&&
       (stardata->star[0].mass > 0.8*stardata->star[0].phase_start_mass)&&
       (stardata->preferences->wd_kick_when==WD_KICK_FIRST_RLOF))
    {
        stardata->star[0].kick_WD=TRUE;
    }
#endif//WD_KICKS
    //stardata->common.RLOF_do_overshoot = TRUE;
}
