#pragma once
#ifndef APPLY_EVOLUTIONARY_COMMANDS_H
#define APPLY_EVOLUTIONARY_COMMANDS_H

#define aprint(...) {                                           \
        printf("apply_evolutionary_commands model %d t %g: ",   \
               stardata->model.model_number,                    \
               stardata->model.time);                           \
        printf(__VA_ARGS__);                                    \
    }
#undef aprint
#define aprint(...) /* do nothing */


/*
 * Wrapper to get a double from the cdict, converting from
 * an int type if that was used accidentally.
 *
 * Assumes the key is the same as VAR, the variable name,
 * in string form.
 *
 * If you give a variable name made with underscores and this
 * is not found, we try to replace the underscores with spaces
 * and see if that key exists instead.
 *
 * Returns 0.0 if not found.
 */
#define Get_cdict_double(CDICT,VAR)                                     \
    __extension__                                                       \
    ({                                                                  \
        struct cdict_entry_t * __entry =                                \
            CDict_nest_get_entry((CDICT),                               \
                                 (char*)#VAR);                          \
        double __x = 0.0;                                               \
        if(__entry == NULL)                                             \
        {                                                               \
            char * __s = strdup(#VAR);                                  \
            for(size_t __i=0; __i<strlen(__s); __i++)                   \
            {                                                           \
                if(__s[__i]=='_')                                       \
                {                                                       \
                    __s[__i] = ' ';                                     \
                }                                                       \
            }                                                           \
            __entry = CDict_nest_get_entry((CDICT),                     \
                                           (char*)__s);                 \
            Safe_free(__s);                                             \
        }                                                               \
        if(__entry != NULL)                                             \
        {                                                               \
            if(__entry->key.type == CDICT_DATA_TYPE_INT)                \
            {                                                           \
                __x = (double) __entry->value.value.int_data;           \
            }                                                           \
            else if(__entry->key.type == CDICT_DATA_TYPE_LONG_INT)      \
            {                                                           \
                __x = (double) __entry->value.value.long_int_data;      \
            }                                                           \
            else if(__entry->key.type == CDICT_DATA_TYPE_LONG_LONG_INT) \
            {                                                           \
                __x = (double) __entry->value.value.long_long_int_data; \
            }                                                           \
            else                                                        \
            {                                                           \
                __x = (double) __entry->value.value.double_data;        \
            }                                                           \
        }                                                               \
        __x;                                                            \
    })

#define Expire(CDICT,...)                                   \
    {                                                       \
        char * _string = NULL;                              \
        if(asprintf(&_string,                               \
                    __VA_ARGS__))                           \
        {                                                   \
            char * _str = CDict_string((CDICT),_string);    \
            CDict_nest_set((CDICT),                         \
                           "__expired",                     \
                           (char*)_str);                    \
            free(_string);                                  \
        }                                                   \
        else                                                \
        {                                                   \
            CDict_nest_set((CDICT),"__expired",1);          \
        }                                                   \
    }
#endif // APPLY_EVOLUTIONARY_COMMANDS_H
