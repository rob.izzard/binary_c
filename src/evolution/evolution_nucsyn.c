#include "../binary_c.h"
No_empty_translation_unit_warning;



#ifdef NUCSYN

void evolution_nucsyn(struct stardata_t * Restrict const stardata)
{
#ifdef MINT
    if(stardata->preferences->stellar_structure_algorithm ==
       STELLAR_STRUCTURE_ALGORITHM_MINT)
    {
        MINT_binary(stardata);
#ifdef NUCSYN
        MINT_nucsyn(stardata);
#endif//NUCSYN
    }
    else
#endif // MINT
    {
        update_abundances_and_yields(stardata);
    }

    Dprint("post-nucsyn\n");

#ifdef NUCSYN_RADIOACTIVE_DECAY
    nucsyn_radioactive_decay(stardata);
#endif //NUCSYN_RADIOACTIVE_DECAY
}
#endif //NUCSYN
