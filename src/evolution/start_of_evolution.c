#include "../binary_c.h"
No_empty_translation_unit_warning;

/*
 * A function to collect together all the stuff that should happen
 * when we start evolving a system.
 *
 * Note that the system does not have to have time == zero,
 * it could start evolving half way through its lifetime.
 *
 * We only call initialize_parameters if model_number == 0
 * (i.e. on the first timestep).
 */

void start_of_evolution(struct stardata_t * Restrict stardata)
{

    Dprint("First run\n");

    /*
     * Set up variables and parameters.
     */
    stardata->common.system_start_tick = getticks();

    /*
     * Initialize the stellar structure algorithm(s)
     * data. Note that MINT must be initialized first.
     */
#ifdef MINT
    if(stardata->preferences->stellar_structure_algorithm ==
       STELLAR_STRUCTURE_ALGORITHM_MINT)
    {
        MINT_init(stardata);
    }
#endif//MINT

#ifdef BSE
    BSE_init(stardata);

    /* set metallicity parameters */
    Dprint("Setting up local variables, calling set_metallicity_parameters\n");
    set_metallicity_parameters(stardata);
#endif //BSE

    /*
     * Initialize stardata parameters
     * for first model.
     */
    if(stardata->model.model_number == 0)
    {
        Dprint("calling initialize_parameters because model number is zero\n");
        initialize_parameters(stardata);
    }

#ifdef NO_IMMEDIATE_MERGERS
    stardata->model.evolution_number = 0;
#endif

    setup_fixed_timesteps(stardata);

    /* maybe require thermal timescales before RLOF */
    set_kelvin_helmholtz_times(stardata);

    /*
     * Check if we want to show the period/separation
     * at which RLOF begins
     */
    if(stardata->preferences->show_minimum_separation_for_instant_RLOF==TRUE ||
       stardata->preferences->show_minimum_orbital_period_for_instant_RLOF==TRUE)
    {
        show_instant_RLOF(stardata);
    }

    Evolution_logging;
    Dprint("done\n");
}
