#include "../binary_c.h"
No_empty_translation_unit_warning;


/*
 * Wrapper to evolve the system in time, starting at time t,
 * and do:
 *
 * 1) stellar mass and angular momentum updates
 * 2) stellar evolution
 * 3) binary evolution
 *
 * leaving the stars at time t+dt.
 *
 * Note: events such as supernovae are handled after we converge the
 *       evolution.
 */

#include "time_evolution.h"

/************************************************************/

int time_evolution(struct stardata_t * Restrict const stardata,
                   Evolution_system_type system_type)
{
    int retval = 0;
    if(stardata->preferences->tasks[BINARY_C_TASK_TIME_EVOLUTION] == TRUE)
    {
        stardata->model.reject_shorten_timestep = REJECT_NONE;
        stardata->model.reject_same_timestep = REJECT_NONE;
        eprint("in time evolution solver loop\n");


        if(stardata->evolving == FALSE ||
           check_for_time_exhaustion(stardata,
                                     stardata->model.intpol) == TRUE ||
           check_for_evolution_stop(stardata))
        {
            eprint("evolution finished at start : return -STOP (evolving = %d, time exhausted? %g vs %g -> %s, explicit stop? %s)\n",
                   stardata->evolving,
                   stardata->model.time,
                   stardata->model.max_evolution_time,
                   Yesno(check_for_time_exhaustion(stardata,
                                                   stardata->model.intpol)),
                   Yesno(check_for_evolution_stop(stardata))
                );
            retval = -EVOLUTION_STOP;
        }
        else if(stardata->preferences->tasks[BINARY_C_TASK_APPLY_DERIVATIVES])
        {
            /*
             * Apply stellar mass and angular momentum changes,
             * e.g. wind loss, wind accretion, RLOF
             */

            /* save timesteps for later restore */
            const double dtm_in = stardata->model.dtm;
            const double dt_in = stardata->model.dt;
            stardata->model.true_dtm = dtm_in;

            /*
             * We're at the top of the evolution loop,
             * so cannot (yet) have rejected the timestep.
             */

            eprint("in evolution() model %d at t = %30.12e, dt = %g, dtm = %g : solver_step %d (intermediate? %d) masses %g %g\n",
                   stardata->model.model_number,
                   stardata->model.time,
                   stardata->model.dt,
                   stardata->model.dtm,
                   stardata->model.solver_step,
                   stardata->model.intermediate_step,
                   stardata->star[0].mass,
                   stardata->star[1].mass
                );
            retval = 0;

            if(stardata->preferences->solver == SOLVER_FORWARD_EULER)
            {
                /*
                 * Forward Euler has only one step
                 */
                stardata->model.solver_step = 0;
                stardata->model.intermediate_step = FALSE;
                retval = Take_time_evolution_step(UPDATE_TIME);
                eprint("Forward Euler retval %d\n",retval);
            }
            else if(stardata->preferences->solver == SOLVER_RK2)
            {
                /*
                 * First step from t to t + dt/2
                 */
                eprint("\n\nRK2 step 0 (first) : dtm = %g\n\n",stardata->model.dtm);
                const Boolean deny_was = stardata->model.deny_new_events;
                Set_event_denial(TRUE);
                Modulate_solver_timestep(0.5);
                stardata->model.solver_step = 0;
                stardata->model.intermediate_step = TRUE;
                retval = Take_time_evolution_step(UPDATE_TIME);

                /*
                 * RK2 second step and apply derivatives at time t + dt/2
                 * to obtain stars at t+dt
                 */
                eprint("\n\nRK2 step 1 (last) (reject %u %u)\n\n",
                       stardata->model.reject_shorten_timestep,
                       stardata->model.reject_same_timestep);
                stardata->model.solver_step = 1;
                stardata->model.intermediate_step = FALSE;
                Set_event_denial(deny_was);
                retval = Take_time_evolution_step(UPDATE_TIME);

                eprint("return %d\n",retval);

                Restore_solver_timestep;
            }
            else if(stardata->preferences->solver == SOLVER_RK4)
            {
                /*
                 * The way the equations are written, we always use half the
                 * timestep, not the full timestep
                 */
                Modulate_solver_timestep(0.5);
                const Boolean deny_was = stardata->model.deny_new_events;
                Set_event_denial(TRUE);
                eprint("*** RK4 start ***\n");
                /*
                 * Calculate k1' :
                 *    structure x = x(t)
                 *    derivatives f at t
                 *    apply for dt/2
                 *    x -> x + dt/2 * f = x + k1/2 = x + k1' = x1'
                 *    t -> t + dt/2
                 */
                stardata->model.solver_step = 0;
                stardata->model.intermediate_step = TRUE;
                eprint("RK4(0) call evolution\n");
                retval = Take_time_evolution_step(UPDATE_TIME);
                eprint("RK4(0) retval %d\n",retval);

                /*
                 * Calculate k2:
                 *    structure x = x(t) + k1' (set above)
                 *    derivatives at t + dt/2
                 *    apply for time 0
                 *    x1' = x + k1' -> x + k2/2 = x + k2' = x2'
                 *    t + dt/2 -> t + dt/2
                 */
                const double thalf = stardata->model.time;
                stardata->model.solver_step = 1;
                retval = Take_time_evolution_step(DO_NOT_UPDATE_TIME);
                stardata->model.time = thalf;
                eprint("RK4(1) retval %d\n",retval);

                /*
                 * Calculate k3:
                 *    structre x = x(t) + k2' (set above)
                 *    derivatives at t + dt/2 = thalf
                 *    apply for time dt/2
                 *    x2' = x + k2' -> x + k3 = x + 2*k3' = x3'
                 *    t + dt/2 -> t + dt
                 */
                stardata->model.solver_step = 2;
                retval = Take_time_evolution_step(UPDATE_TIME);
                eprint("RK4(2) retval %d\n",retval);

                /*
                 * Calculate k4:
                 *    structure x = x(t) + k3' (see above)
                 *    derivatives at t + dt
                 *    apply for time 0
                 *    x + k3 = x + 2*k3' -> x + k4 = x + 2*k3'
                 *    t + dt -> t + dt
                 */
                Set_event_denial(deny_was);
                stardata->model.solver_step = 3;
                stardata->model.intermediate_step = FALSE;
                retval = Take_time_evolution_step(DO_NOT_UPDATE_TIME);
                eprint("RK4(3) retval %d\n",retval);

                /*
                 * Restore the full timestep
                 */
                Restore_solver_timestep;
            }
            else if(stardata->preferences->solver == SOLVER_PREDICTOR_CORRECTOR)
            {
                /*
                 * PEC method : not really working yet!
                 * https://en.wikipedia.org/wiki/Predictor%E2%80%93corrector_method
                 */
                retval = Take_time_evolution_step(UPDATE_TIME);
                Modulate_solver_timestep(0.5);

                int i;
                const int maxk = 10;
                for(i=1;i<maxk;i++)
                {
                    stardata->model.solver_step = i;
                    eprint("PEC%d\n",i);
                    retval = Take_time_evolution_step(DO_NOT_UPDATE_TIME);
                }
                Restore_solver_timestep;
            }
            else
            {
                retval = 0;
                Exit_binary_c(BINARY_C_ALGORITHM_OUT_OF_RANGE,
                              "Unknown solver %d\n",stardata->preferences->solver);
            }

            /* go back to step 0 */
            stardata->model.solver_step = 0;

            eprint("DT RESTORE -> dtm = %g, dt = %g\n",
                   stardata->model.dtm,
                   stardata->model.dt
                );

            if(Is_zero(stardata->model.dt) &&
               Is_zero(stardata->model.time))
            {
                /* reset in case of repeat */
                stardata->common.test = 0.0;
            }

            /*
             * Linear integration test
             */
#ifdef LINEAR_INTEGRATION_TEST

            const double absdiff = Abs_diff(stardata->model.time*1e6,
                                            stardata->common.test);

            eprint("TEST check t = %30.12e test = %30.12e : diff %g\n",
                   stardata->model.time*1e6,
                   stardata->common.test,
                   absdiff
                );

            if(absdiff > 1e-3)
            {
                eprint("integration failed\n");
                fflush(NULL);
                _exit(0);
            }
#endif // LINEAR_INTEGRATION_TEST

            /*
             * Time-explicit algorithms
             */
            evolution_time_explicit(stardata);

            eprint("Devolution retval %d\n",retval);

            /*
             * rejection tests
             */
            evolution_rejection_shorten_tests(stardata);
        }
    }
    else
    {
        retval = 0;
    }
    return retval;
}
