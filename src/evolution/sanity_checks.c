#include "../binary_c.h"
No_empty_translation_unit_warning;

void sanity_checks(struct stardata_t * const stardata Maybe_unused)
{
    /*
     * Run a series of sanity checks to make sure
     * things like mass are conserved.
     */
#ifndef MINT
#ifdef NUCSYN
    if(1)
    {
        /*
         * Test that (baryonic) mass is conserved.
         */
        const double MZAMS =
            stardata->common.zero_age.mass[0] +
            stardata->common.zero_age.mass[1];
        const double Mnow =
            stardata->star[0].baryonic_mass +
            stardata->star[1].baryonic_mass;
        double Myield[NUMBER_OF_STARS] = {0.0};
        const double p = Is_zero(stardata->model.probability) ? 1.0 : stardata->model.probability;
        Foreach_star(star)
        {
            Myield[star->starnum] =
                sum_C_double_array(star->Xyield,
                                   ISOTOPE_ARRAY_SIZE);
            Myield[star->starnum] += star->other_yield;
            Myield[star->starnum] /= p;
        }
        const double diff = MZAMS - (Mnow + (Myield[0] + Myield[1])/p);
        const double tolerance = 1e-5;
        if(Is_not_zero(MZAMS) &&
           Is_not_zero(Mnow) &&
           Abs_diff(MZAMS,Mnow+Myield[0]+Myield[1])>tolerance)
        {
            Exit_binary_c(
                BINARY_C_MASS_NOT_CONSERVED,
                "Yield error at model %d time %g: MZAMS = %g, Mbary_now = %g + %g = %g, Myield 0 = %g, 1 = %g : should have MZAMS = %g = %g (diff %g) \n",
                stardata->model.model_number,
                stardata->model.time,
                MZAMS,
                stardata->star[0].baryonic_mass,
                stardata->star[1].baryonic_mass,
                Mnow,
                Myield[0],
                Myield[1],
                MZAMS,
                Mnow + Myield[0] + Myield[1],
                diff
                );
        }
    }

    /*
     * Prevent numerical issues
     */
    Foreach_evolving_star(star)
    {
        nucsyn_renormalize_to_max(star->Xenv);
        nucsyn_renormalize_to_max(star->Xacc);
    }
#endif // NUCSYN
#endif // MINT
}
