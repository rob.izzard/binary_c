#include "../binary_c.h"
No_empty_translation_unit_warning;
#include "evolution_macros.h"

int loop_check(struct stardata_t * Restrict const stardata,
               const int evstatus)
{
    /*
     * Evolution loop control based on the 'evstatus' value,
     * as returned by an evolve_* function (* = RLOF or detached)
     *
     * EVOLUTION_LOOP_NOW    : return EVOLUTION_CONTINUE
     *
     * EVOLUTION_LOOP        : return EVOLUTION_CONTINUE if loop_iteration is TRUE,
     *               otherwise EVOLUTION_BREAK
     *
     * EVOLUTION_LOOP_RLOF   : return EVOLUTION_CONTINUE_RLOF
     *
     * EVOLUTION_LOOP_REJECT : call evolution_rejected(), usually returns
     *               EVOLUTION_REJECT, but maybe EVOLUTION_CONTINUE_RLOF
     *
     * EVOLUTION_STOP        : return EVOLUTION_BREAK
     *
     * FALSE       : return EVOLUTION_NO_ACTION_REQUIRED
     *               (should not happen)
     */
    int ret = EVOLUTION_NO_ACTION_REQUIRED;
    Dprint("evstatus = %d\n",evstatus);
    if(evstatus)
    {
        if(evstatus == EVOLUTION_LOOP_REJECT)
        {
            Dprint("REJECT at t=%g dt=%g dtm=%g\n",
                   stardata->model.time,
                   stardata->model.dt,
                   stardata->model.dtm);
            ret = evolution_rejected(stardata);
        }
        else
        {
            evolution_success(stardata);

            if(evstatus == EVOLUTION_LOOP ||
               evstatus == EVOLUTION_LOOP_NOW)
            {
                ret = EVOLUTION_CONTINUE;
            }
            else if(evstatus == EVOLUTION_LOOP_RLOF)
            {
                ret = EVOLUTION_CONTINUE_RLOF;
            }
            else if(evstatus == EVOLUTION_STOP)
            {
                ret = EVOLUTION_BREAK;
            }
        }
    }
    else
    {
        ret = EVOLUTION_NO_ACTION_REQUIRED;
    }

    return ret;
}
