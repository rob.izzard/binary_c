#include "../binary_c.h"
No_empty_translation_unit_warning;



void evolution_save_to_previous(struct stardata_t * Restrict const stardata)
{
    /*
     * Save current stardata into the previous stardata, and
     * update the previous_stardatas stack if necessary
     */
    if(stardata->model.restore_mask)
    {
        Safe_free(stardata->model.restore_mask);
        Safe_free(stardata->model.restore_mask_contents);
    }

    /*
     * If we're using a solver that has created an intermediate
     * solution, do not save.
     *
     * e.g. the RK2 solver on its first step should not be saved
     *
     * If force is TRUE then we must save, regardless of the "intermediate"
     * state. This is done e.g. on the first timestep.
     */

    const Boolean save =
        Boolean_(stardata->model.intermediate_step == FALSE &&
                 stardata->previous_stardatas != NULL);

    Dprint("Save to prev? solver=%d solver_step=%d previous_stardatas=%p : %d\n",
           stardata->preferences->solver,
           stardata->model.solver_step,
           (void*)stardata->previous_stardatas,
           save
        );
    if(save==TRUE)
    {
        /*
         * Max number of previous stardatas
         */
        const unsigned int n = Solver_n_previous_stardatas(stardata->preferences->solver);

        /*
         * We have stardata->n_previous_stardata previous stardatas
         *
         * We want to save n previous stardatas.
         */
        if(stardata->n_previous_stardatas < n)
        {
            /*
             * Allocate memory for new previous_stardata
             */
            Dprint("New previous_stardata[%u] : want %u\n",
                   stardata->n_previous_stardatas,
                   n);

            /* increase array of pointers */
            stardata->previous_stardatas = Realloc(stardata->previous_stardatas,
                                                   sizeof(struct stardata_t *)*(1+stardata->n_previous_stardatas));
            memset(stardata->previous_stardatas + (size_t)stardata->n_previous_stardatas,
                   0,
                   sizeof(struct stardata_t*) * ((size_t)n - (size_t)stardata->n_previous_stardatas));

            unsigned int i;
            for(i=0;i<n;i++)
            {
                if(stardata->previous_stardatas[i] == NULL)
                {
                    stardata->previous_stardatas[i] = new_stardata(stardata->preferences);
                }
            }
            Dprint("previous_stardatas[%u] = %p\n",
                   stardata->n_previous_stardatas,
                   (void*)stardata->previous_stardatas);
            stardata->n_previous_stardatas++;
        }

        if(n == 1)
        {
#ifdef STARDATA_DIFFSTATS
            printf("Copy stardata : %5.2f %% different\n",
                   diff_struct_pc(stardata,
                                  stardata->previous_stardatas[0],
                                  sizeof(struct stardata_t)));
#endif
            /*
             * We should back up the presistent pointer
             */
            copy_stardata(
                stardata,
                stardata->previous_stardatas[0],
                COPY_STARDATA_PREVIOUS_NONE,
                COPY_STARDATA_PERSISTENT_FROM_POINTER
                );
        }
        else
        {
            /*
             * We require multiple stardata copies, n steps back in time
             */
            for(unsigned int i=n-1; i>0; i--)
            {
                Dprint("Copy stardata previous[%u] (%p) to previous[%u] (%p %p)\n",
                       i-1,
                       (void*)stardata->previous_stardatas[i-1],
                       i,
                       (void*)stardata->previous_stardatas[i],
                       (void*)stardata->previous_stardatas[i]->preferences
                    );

                /* use copy_stardata to do a pure copy */
                copy_stardata(
                    stardata->previous_stardatas[i-1],
                    stardata->previous_stardatas[i],
                    COPY_STARDATA_PREVIOUS_NONE,
                    COPY_STARDATA_PERSISTENT_FROM_POINTER
                    );
            }
            Dprint("Copy stardata (%p) to previous[%u] (%p)\n",
                   (void*)stardata,
                   (unsigned int)0,
                   (void*)stardata->previous_stardatas[0]
                );
            copy_stardata(
                stardata,
                stardata->previous_stardatas[0],
                COPY_STARDATA_PREVIOUS_NONE,
                COPY_STARDATA_PERSISTENT_FROM_POINTER
                );
        }
        stardata->previous_stardata = stardata->previous_stardatas[0];
    }


    Dprint("saved to previous at %p %p with prefs = %p\n",
           (void*)stardata->previous_stardata,
           (void*)stardata->previous_stardatas[0],
           (void*)stardata->previous_stardata->preferences);
#ifdef MINT
    Dprint("saved to previous : mints %p %p\n",
           (void*)stardata->star[0].mint,
           (void*)stardata->previous_stardata->star[0].mint);
#endif//MINT
}
