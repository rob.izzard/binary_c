#include "../binary_c.h"
No_empty_translation_unit_warning;


/*
 * DEPRECATED
 */

/*
 * Logging after a star has become single
 * or a binary has been detached.
 */
Boolean loop_evolution(struct stardata_t * Restrict const stardata)
{
    Dprint("In loop evolution\n");
    stardata->model.sgl = TRUE;

    Boolean keep_looping;

#ifdef STELLAR_POPULATIONS_ENSEMBLE
    /* loop for longer to not lose the final ensemble call */
    keep_looping = TRUE;
#else
    /* standard result : stop looping if the stars do nothing */
    keep_looping = Boolean_(EVOLVING(0) || EVOLVING(1));
#endif

    if(keep_looping==TRUE)
    {
        if(stardata->model.com==TRUE) stardata->model.com = FALSE;

        /* deal with remnants */
        Foreach_star(star)
        {
            if(star->stellar_type==MASSLESS_REMNANT)
            {
                Dprint("merge : Star 0 mass=%g phase_start_mass=%g\n",
                       stardata->star[0].mass,
                       stardata->star[0].phase_start_mass);

                /* set Roche lobe to be negative (i.e. unrealistic) */
                star->roche_radius = -1.0*star->radius;

                /* and timestep to be longer than the simulation */
                star->stellar_timestep = stardata->model.max_evolution_time;
            }
        }
        Dprint("Binary check : sep=%g ecc=%g kw=%d,%d : %d\n",
               stardata->common.orbit.separation,
               stardata->common.orbit.eccentricity,
               stardata->star[0].stellar_type,
               stardata->star[1].stellar_type,
               System_is_binary);

        /*
         * No longer a binary
         */
        if(More_or_equal(stardata->common.orbit.eccentricity,0.0) &&
           stardata->common.orbit.separation > TINY)
        {
            Dprint("system no longer binary, set sep=0, ecc=-1, dtm=0");
            stardata->common.orbit.eccentricity = -1.0;
            stardata->common.orbit.separation = 0.0;
            stardata->model.dtm = 0.0;
        }
        /* if a merger happened, set the appropriate flag */
        if(stardata->model.coalesce==TRUE)
        {
            Dprint("Detected merged system\n");
            stardata->model.merged = TRUE;
            stardata->model.coalesce   = FALSE;
        }

        Dprint("m1=%g m10=%g: return TRUE, sgl=%d\n",
               stardata->star[0].mass,
               stardata->star[0].phase_start_mass,
               stardata->model.sgl);
    }
    else
    {
        Dprint("return FALSE : sgl=%d\n",stardata->model.sgl);
    }

    return keep_looping;
}
