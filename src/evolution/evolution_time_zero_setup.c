#include "../binary_c.h"
No_empty_translation_unit_warning;


/*
 * Subroutine to set things up at time zero, which is
 * usually the first time a star is evolved (although, when
 * using the API, this may not be true : a star can then be
 * restarted).
 */

void evolution_time_zero_setup(struct stardata_t * Restrict const stardata)
{
    Dprint("time zero setup\n");
    struct common_t * const common =  &stardata->common;
    common->init_random_seed = common->random_seed;

    Dprint("set orbital parameters\n");
    if(Is_not_zero(stardata->star[0].mass)
       &&
       Is_not_zero(stardata->star[1].mass))
    {

        /*
         * if the period is zero, use the provided
         * separation, otherwise use the provided period
         * which is in days
         */
        if(Is_zero(common->orbit.period))
        {
            common->orbit.period = calculate_orbital_period(stardata);
        }

        if(Is_zero(common->zero_age.orbital_period[0]))
        {
            /* period in days */
            common->zero_age.orbital_period[0] = common->orbit.period;
        }

        /*
         * convert orbital period to years for further
         * calculation (years is the internal code unit)
         */
        common->orbit.period /= YEAR_LENGTH_IN_DAYS;

        /*
         * calculate the separation
         */
        common->orbit.separation =
            common->zero_age.separation[0] = calculate_orbital_separation(stardata);
    }
    common->zero_age.eccentricity[0] = common->orbit.eccentricity;
    set_metallicities(stardata);

#ifdef MINT
    if(stardata->preferences->stellar_structure_algorithm == STELLAR_STRUCTURE_ALGORITHM_MINT)
    {
        Foreach_star(star)
        {
            /*
             * Set up parameters for the MINT interpolation
             * scheme
             */
            const double offset = 0.7 - 0.6985;
            star->mint->XHc = 0.760 - 3.0 * stardata->common.nucsyn_metallicity
                - offset;
            //0.7381; // Asplund 2009 (solar)
            star->mint->XHec = 0.240 + 2.0 * stardata->common.nucsyn_metallicity
                + offset;
            //0.2485; // Asplund 2009 (solar)

        }
    }
#endif

#ifdef NUCSYN
    /*
     * Set initial abundances only if we aren't
     * doing an initial_abunds_only call (in which
     * case they are set elsewhere, and only once)
     */
    if(stardata->preferences->initial_abunds_only == FALSE)
    {
        Dprint("Set initial abundances (XZAMS, Xsolar, nucsynZ=%g)\n",
               stardata->common.nucsyn_metallicity);

        /*
         * copy initial abundances into the XZAMS array
         */
        Clear_isotope_array(stardata->preferences->zero_age.XZAMS);
        Clear_isotope_array(stardata->preferences->zero_age.Xsolar);

        /*
         * set the initial abundance mixture using the nucsyn_metallicity
         */
        nucsyn_initial_abundances(stardata,
                                  stardata->star[0].Xenv,
                                  common->nucsyn_metallicity,
                                  stardata->preferences->initial_abundance_mix,
                                  TRUE);
        Dprint("done init abundances");

        /* initial nuclear burning timesteps */
        Reaction_network i;
        for(i=0;i<NUCSYN_NETWORK_NUMBER;i++)
        {
            stardata->common.dtguess[i] = LONG_TIMESTEP;
        }

        /*
         * copy the initial abundances where they are required
         */
        Foreach_star(star)
        {
            if(star->starnum!=0) Copy_abundances(stardata->star[0].Xenv,star->Xenv);
            Copy_abundances(stardata->star[0].Xenv,star->Xinit);
            Copy_abundances(stardata->star[0].Xenv,star->Xacc);
        }
        Copy_abundances(stardata->star[0].Xenv,
                        stardata->preferences->zero_age.XZAMS);


        /*
         * Pure solar, Z=0.02 abundances
         */
        Dprint("Make solar abundances\n");
        nucsyn_initial_abundances(stardata,
                                  stardata->preferences->zero_age.Xsolar,
                                  0.02,
                                  stardata->preferences->initial_abundance_mix,
                                  FALSE);

        /*
         * overwrite the initial abundance array to be
         * the true initial abundances
         */
        Copy_abundances(stardata->star[0].Xenv,
                        stardata->preferences->the_initial_abundances);

#ifdef NANCHECKS
        nancheck("Xsolar in initialize_parameters",stardata->preferences->zero_age.Xsolar,ISOTOPE_ARRAY_SIZE);
        nancheck("XZAMS in initialize_parameters",stardata->preferences->zero_age.XZAMS,ISOTOPE_ARRAY_SIZE);
#endif //NANCHECKS
    }
#endif // NUCSYN

    /*
     * Compute ages of stars with core masses
     * specified on the command line or when their
     * stellar types require a core.
     */
#define _Compute_timescales(STAR)               \
    stellar_timescales(stardata,                \
                       STAR,                    \
                       STAR->bse,               \
                       STAR->phase_start_mass,  \
                       STAR->mass,              \
                       STAR->stellar_type);
    struct zero_age_system_t * const zero_age = &stardata->preferences->zero_age;
    Foreach_star(star)
    {
        Dprint("star %d type %d m %g m0 %g mc %g \n",
               star->starnum,
               star->stellar_type,
               star->mass,
               star->phase_start_mass,
               zero_age->core_mass[star->starnum]);
        star->deny_SN++;

        star->phase_start_mass = star->mass; /* best guess! */
        if(star->stellar_type > MAIN_SEQUENCE)
        {
            Boolean alloc[3];
            stellar_structure_BSE_alloc_arrays(star,NULL,alloc);
            _Compute_timescales(star);
            star->age = 0.0;

            if(star->stellar_type != HeMS &&
               Is_zero(zero_age->core_mass[star->starnum]))
            {
                /*
                 * We require a zero-age core mass for
                 * some types, so guess it
                 */
                double Mc = 0.0;
                if(star->stellar_type == GIANT_BRANCH ||
                   star->stellar_type == HERTZSPRUNG_GAP)
                {
                    Mc = mcbgb(star,
                               stardata);
                    if(star->stellar_type == HERTZSPRUNG_GAP)
                    {
                        star->age = star->bse->tm * (1.0 + 1e-6);
                    }
                }
                else if(star->stellar_type == CHeB)
                {
                    if(Less_or_equal(star->mass,
                                     stardata->common.metallicity_parameters[ZPAR_MASS_HE_FLASH]))
                    {
                        Mc = mcgbf(star->bse->luminosities[L_HE_IGNITION],
                                   star->bse->GB,
                                   star->bse->luminosities[L_LMX]);
                    }
                    else
                    {
                        Mc = mcheif(star->mass,
                                    stardata->common.metallicity_parameters[ZPAR_MASS_HE_FLASH],
                                    stardata->common.metallicity_parameters[10],
                                    stardata->common.giant_branch_parameters);
                    }
                }
                else if(star->stellar_type == EAGB)
                {
                    Mc = mcagbf(star->mass,
                                stardata->common.giant_branch_parameters);
                }
                else if(star->stellar_type == TPAGB)
                {
                    const double Mch = chandrasekhar_mass_wrapper(stardata,
                                                                  star->mass,
                                                            star->mass,
                                                            star->stellar_type);
                    Mc = guess_mc1tp(star->mass,
                                     star->mass,
                                     star,
                                     stardata);
                    if(Mc > Mch)
                    {
                        star->stellar_type = EAGB;
                        Mc = mcagbf(star->mass,
                                    stardata->common.giant_branch_parameters);
                    }
                }
                zero_age->core_mass[star->starnum] = Mc;
            }

            /*
             * Now get the age from the core mass
             */
            if(Is_not_zero(zero_age->core_mass[star->starnum]))
            {
                const Core_type core_id = ID_core(star->stellar_type);
                if(core_id != CORE_NONE)
                {
                    star->core_mass[core_id] = zero_age->core_mass[star->starnum];
                }
                if(Is_zero(star->age))
                {
                    double mc = Outermost_core_mass(star);
                    int kw = star->stellar_type;
                    giant_age(&mc,
                              star->mass,
                              &kw,
                              &star->phase_start_mass,
                              &star->age,
                              stardata,
                              star);
                }
                star->epoch = stardata->model.time - star->age;
            }


            if(star->stellar_type == TPAGB)
            {
                star->core_mass[CORE_CO] = star->core_mass[CORE_He] - dm_intershell(star->core_mass[CORE_He]);
            }
            else if(star->stellar_type == EAGB)
            {
                star->core_mass[CORE_CO] = mcgbtf(star->age,
                                                  star->bse->GB[GB_A_HE],
                                                  star->bse->GB,
                                                  star->bse->timescales[T_EAGB_TINF_1],
                                                  star->bse->timescales[T_EAGB_TINF_2],
                                                  star->bse->timescales[T_EAGB_T]);
            }
            char * corestring = core_string(star,TRUE);
            Dprint("Cores %d: type %d : %s : age %g\n",
                   star->starnum,
                   star->stellar_type,
                   corestring,
                   star->age);
            Safe_free(corestring);
        }
        star->deny_SN--;
    }
}
