#include "../binary_c.h"
No_empty_translation_unit_warning;


/** having missed the last bus home, the evolution has finally finished **/
/** write out some stuff to a few log files, then that's it! **/

void end_of_evolution(struct stardata_t * Restrict const stardata)
{
    stardata->model.com = FALSE;

    if(stardata->preferences->pause_at_evolution_end)
    {
#ifdef MEMORY_USE
        struct binary_c_memuse_t memuse;
        get_memory_use(stardata,&memuse);
#endif // MEMORY_USE
        Printf("paused to wait for stdin\n");
        fflush(stdin);
        fflush(stdout);
        getc(stdin);
    }

#ifdef FINAL_MASSES_LOG
    /* log post-AGB mass transfer only */
    if((stardata->star[0].stellar_type==COWD)&&
       (stardata->star[1].stellar_type<HeWD)&&
       (stardata->model.sgl==0))
    {
        Abundance *N = New_isotope_array;
        X_to_N(stardata->store->imnuc,
               10.0,N,stardata->star[1].Xenv,
               ISOTOPE_ARRAY_SIZE);
        printf("FINAL_MASSES %g %g %g logg=%g Teff=%g log(nF/nH)=%g log(C+N/H)=%g [C/Fe]=%g [N/Fe]=%g [F/Fe]=%g [C+N/Fe]=%g\n",
               stardata->star[0].mass,
               stardata->star[1].mass,
               stardata->star[1].Xenv[XC12],
               log10(GRAVITATIONAL_CONSTANT *
                     M_SUN*stardata->star[1].mass/
                     Pow2(R_SUN*stardata->star[1].radius)),
               Teff(1),
               log10(N[XF19]/N[XH1]),
               log10((N[XC12]+N[XC13]+N[XN14]+N[XN15])/N[XH1]),
               nucsyn_square_bracket(stardata->star[1].Xenv,
                                     stardata->common.Xsolar,
                                     XC12,XFe56),
               nucsyn_square_bracket(stardata->star[1].Xenv,
                                     stardata->common.Xsolar,
                                     XN14,XFe56),
               nucsyn_square_bracket(stardata->star[1].Xenv,
                                     stardata->common.Xsolar,
                                     XF19,XFe56),
               nucsyn_square_multibracket(stardata->star[1].Xenv,
                                          stardata->common.Xsolar,
                                          (Isotope[]){XC12,XC13,XN14,XN15},4,(Isotope[]){XFe56},1)
            );
        Safe_free(N);

    }
#endif


}
