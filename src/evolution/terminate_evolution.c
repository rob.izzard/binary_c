#include "../binary_c.h"
No_empty_translation_unit_warning;



void terminate_evolution(struct stardata_t * const stardata,
                         const char * const message)
{
    /*
     * End evolution by setting evolving to FALSE
     */
    stardata->evolving = FALSE;
    Set_logstring(LOG_TERMINATE,
                  "%s",
                  (char*)message);
}
