#include "binary_c.h"

#define _Rejected_dtm (stardata->model.dt_zoomfac * stardata->model.dtm)

#define _Timestep_large_enough                          \
    (_Rejected_dtm > stardata->preferences->minimum_timestep)


Boolean Pure_function_if_no_debug can_reject_and_shorten_timestep(struct stardata_t * Restrict const stardata)
{
    /*
     * Return TRUE if we're allowed to reject a timestep
     * and fall back on the old timestep, and then reduce the timestep
     *
     * can reject if we haven't hit the max number of rejections
     * and the new timestep will be larger than the minimum timestep
     *
     * can reject if we didn't have an event in the
     * previous timestep: events can change the system
     * in a non-continuous way, which means we can never
     * converge
     */
    const Boolean c = can_reject_with_same_timestep(stardata);
    const Boolean x = Boolean_(c && _Timestep_large_enough);

    Dprint("at %30.20g model %d can reject and shorten timestep? [new dtm large enough? %s (would be %g vs minimum %g)] : can reject same timestep? %s\n",
           stardata->model.time,
           stardata->model.model_number,
           Yesno(_Timestep_large_enough),
           _Rejected_dtm,
           stardata->preferences->minimum_timestep,
           Yesno(c));

    return x;
}
