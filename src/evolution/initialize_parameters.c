/*
 * This function initializes the binary star parameters on
 * the first timestep, i.e. when model=age=0.
 */
#include "../binary_c.h"
No_empty_translation_unit_warning;


void initialize_parameters(struct stardata_t * Restrict const stardata)
{
    struct model_t * const model = &stardata->model;
    struct common_t * const common = &stardata->common;
    struct star_t * const star1 = &stardata->star[0];
    struct star_t * const star2 = &stardata->star[1];
#ifdef PRE_MAIN_SEQUENCE
    double aperi;
#endif
    Star_number k = 0;
    /************************************************************/

    check_nans_are_signalled();
    model->time = stardata->preferences->start_time;
    model->dtm = model->dt = 0.0;

    Dprint("Initializing the parameters\n");
    Dprint("random_seed\n");

    /*
     * The random number seed can be specified as a preference,
     * or - if the seed is not yet set - it is set by the random_seed()
     * function
     */
    if(stardata->preferences->cmd_line_random_seed != 0)
    {
        common->random_seed = stardata->preferences->cmd_line_random_seed;
        Dprint("random_seed from cmd line %ld\n",
               (long int)common->random_seed);
    }
    else if(common->random_seed != 0)
    {
        common->random_seed = random_seed();
        Dprint("random_seed from random_seed() %ld\n",
               (long int)common->random_seed);
    }

#ifdef FILE_LOG
    if(model->log_fp != NULL)
    {
        fprintf(model->log_fp,
                "RANDOM_SEED=%ld RANDOM_COUNT=%ld\n",

                (stardata->preferences->cmd_line_random_seed != 0 ?
                 (long int)stardata->preferences->cmd_line_random_seed :
                 (long int)stardata->common.random_seed), // (33)

                stardata->common.random_buffer.count
            );
    }
#endif //FILE_LOG

#ifdef RANDOM_SEED_LOG
#ifdef RANDOM_SEED_LOG_STREAM
    if(RANDOM_SEED_LOG_STREAM != NULL) fprintf(RANDOM_SEED_LOG_STREAM, "random_seed=%ld\n", common->random_seed);
#endif//RANDOM_SEED_LOG_STREAM
#endif//RANDOM_SEED_LOG

    /*
     * If the time really is zero, we require some further setup.
     */
    if(Is_zero(model->time))
    {
        Dprint("call time zero setup\n");
        evolution_time_zero_setup(stardata);
        Dprint("done time zero setup\n");
    }
    Dprint("Memcopied XZAMS ok\n");

#ifdef PRE_MAIN_SEQUENCE
    aperi = stardata->common.orbit.separation * (1.0 - stardata->common.orbit.eccentricity);
#endif


    model->sgl = FALSE; /* true for single star evolution ? */

#ifdef FORCE_SINGLE_STAR_EVOLUTION_WHEN_SECONDARY_IS_A_PLANET
    if(Less_or_equal(star2->mass, 0.09))
    {
        /* Single star evoltion */
        model->sgl = TRUE;
        common->zero_age.eccentricity[0] = -1.0;
        common->zero_age.orbital_period[0] = 0.0;
        common->zero_age.eccentricity[0] = 0.0;
        common->separation = 1E7;
        common->orbit.angular_momentum = 0.0;
        common->orbit.angular_frequency = 0.0;
        star2->mass = 0.01;
        if(star1->omega < 0.0)star1->omega = 0.0;
        star2->omega = 0.0;
        star2->angular_momentum = 0.0;
        star2->mass = 0.01;
        Dprint("Star 2 is a planet: configure for single-star evolution\n");
    }
    else
#endif
    {
        if(stardata->preferences->zero_age.multiplicity == 1)
        {
            /* single star */
            common->orbit.angular_frequency = 0.0;
            common->orbit.angular_momentum = 0.0;
        }
        else
        {
            /* hopefully a binary */
            common->orbit.angular_frequency = Is_zero(common->orbit.period) ? 0.0 : (TWOPI / common->orbit.period);
            Dprint("calc Jorb from a = %g, P = %g, omega = %g\n",
                   common->orbit.separation,
                   common->orbit.period,
                   common->orbit.angular_frequency);
            common->orbit.angular_momentum = Is_zero(common->orbit.period) ? 0.0 : orbital_angular_momentum(stardata);
        }
        if(star1->omega < 0.0) star1->omega = common->orbit.angular_frequency;
        if(star2->omega < 0.0) star2->omega = common->orbit.angular_frequency;
    }
    Dprint("hence Jorb = %g\n", common->orbit.angular_momentum);

    const Boolean init_sgl = model->sgl;

    /**** per star setup ****/
    Number_of_stars_Starloop(k)
    {
        SETstar(k);
        Dprint("In first k loop, starnum=%d, stellar_type=%d (m=%g m0=%g other star m=%g m0=%g)\n",
               star->starnum,
               star->stellar_type,
               star->mass,
               star->phase_start_mass,
               stardata->star[Other_star(k)].mass,
               stardata->star[Other_star(k)].phase_start_mass);

        /*
         * If mass == 0 skip most of the per-star setup.
         *
         * This happens, e.g., when inital abundances
         * are required but nothing else.
         */
        if(Is_not_zero(star->mass))
        {
            //age = model->time - star->epoch;
            Dprint("Set initial age = %g from %g - epoch = %g\n",
                   star->age,
                   model->time,
                   star->epoch);

            Dprint("Calling stellar_structure...");
            if(init_sgl && k == 1)
            {
                star->phase_start_mass = 0.0;
                star->effective_zams_mass = 0.0;
            }
            else
            {
                /*
                 * Set the phase start mass if it hasn't yet been
                 * (it may have been e.g. if starting with a
                 *  star that's not a MS star).
                 */
                if(Is_zero(star->phase_start_mass))
                {
                    star->phase_start_mass = star->mass;
                }
                if(Is_zero(star->effective_zams_mass))
                {
                    star->effective_zams_mass = star->mass;
                }
            }
            Dprint("Set phase_start_mass %g\n",
                   star->phase_start_mass);
            star->max_MS_core_mass = 0.0;
            star->stellar_type_tstart = 0.0;

            star->deny_SN++;
            stellar_structure(stardata,
                              STELLAR_STRUCTURE_CALLER_initialize_parameters,
                              star,
                              NULL);
            star->deny_SN--;
            star->q = Q(k);
            determine_roche_lobe_radius(stardata,
                                        &stardata->common.orbit,
                                        star);
            Dprint("post-roche\n");
            star->rol0 = star->roche_radius;

            Dprint("setting lum to %12.12e\n", star->luminosity);

#ifdef PRE_MAIN_SEQUENCE
            /*
             * If the binary orbit is so close that the star is outside
             * its Roche lobe on the pre-main sequence, try to age the
             * star appropriately such that it fits inside its Roche lobe.
             *
             * Of course this is just a fudge...
             */

#define PMSprint if(1)printf

            Boolean pms_fit_failed = FALSE;

            if(ON_MAIN_SEQUENCE(star->stellar_type) &&
               stardata->preferences->pre_main_sequence == TRUE &&
               stardata->preferences->pre_main_sequence_fit_lobes == TRUE &&
               star->radius > star->roche_radius_at_periastron)
            {
                /* age the star until it fits just inside the main sequence */


                /* find the R/RZAMS factor now */
                const double f = preMS_radius_factor(star->mass, 0.0);

                /* hence RZAMS */
                const double RZAMS = star->radius / f;

                /*
                 * hence target factor = R/RZAMS = ROLperi/RZAMS
                 * but this must be >1
                 */
                const double targetf = Max(1.0, 0.999 * star->roche_radius_at_periastron / RZAMS);


                /*
                 * hence get the corresponding age
                 * NB on failure will put the star on the ZAMS
                 */
                star->age = 1e-6 * time_from_preMS_radius_factor(stardata,
                                                                 star->mass,
                                                                 targetf);

                PMSprint("star %d : R_PMS=%g > RLperi=%g : R_ZAMS = %g : ",
                         star->starnum, star->radius, star->roche_radius_at_periastron, RZAMS);
                PMSprint("target radius factor f = %g : ", targetf);
                PMSprint("new age %g\n", star->age);

                /* if we can, calculate new radius */
                if(!Fequal(f, 1.0))
                {
                    star->deny_SN++;
                    stellar_structure(stardata,
                                      STELLAR_STRUCTURE_CALLER_initialize_parameters,
                                      star,
                                      NULL);
                    star->deny_SN--;
                    star->epoch = -star->age;
                    Dprint("Set epoch = - age = %g, age = %g\n", star->epoch, star->age);
                }

                PMSprint("R=%g : RLperi=%g ",
                         star->radius,
                         star->roche_radius_at_periastron);

                if(star->radius > star->roche_radius_at_periastron)
                {
                    PMSprint("FAILED");
                    pms_fit_failed = TRUE;
                }
                else
                {
                    PMSprint("SUCCESS");
                }

                PMSprint(" (sep=%g perisep=%g) age=%g age/pms_lifetime=%g (pms lifetime %g)\n",
                         stardata->common.orbit.separation,
                         stardata->common.orbit.separation * (1.0 - stardata->common.orbit.eccentricity),
                         star->age,
                         star->age / (1e-6 * preMS_lifetime(star->mass)),
                         1e-6 * preMS_lifetime(star->mass)
                    );
            }

            if(pms_fit_failed == TRUE)
            {
                printf(">>> WARNING <<< : Pre-MS Roche lobe fitting failed, expect immediate RLOF from star %d\n", k);
            }
#endif // PRE_MAIN_SEQUENCE

            /*
             * If t=0 (first timestep) then set the spin,
             * otherwise we assume whatever is passed in.
             */
            if(Is_zero(model->time) &&
               star->omega < 1e-3)
            {
                calculate_rotation_variables(star, star->radius);

                if(Is_not_zero(stardata->preferences->zero_age.Prot[star->starnum]))
                {
                    /*
                     * v = omega * r = 2*pi*r / Prot in cgs (cm/s)
                     */
                    stardata->preferences->zero_age.vrot[star->starnum] =
                        2.0 * PI *
                        (R_SUN * star->radius) /
                        (DAY_LENGTH_IN_SECONDS * stardata->preferences->zero_age.Prot[star->starnum])

                        /* convert to km / s */
                        * 1e-5;


                    printf("ROT convert Prot = %g d, R = %g Rsun -> v = %g km/s\n",
                           stardata->preferences->zero_age.Prot[star->starnum],
                           star->radius,
                           stardata->preferences->zero_age.vrot[star->starnum]);

                }

                const double initial_vrot_kms = stardata->preferences->zero_age.vrot[star->starnum];
                if(Fequal(initial_vrot_kms, VROT_SYNC * 1.0))
                {
                    /*
                     * initial_vrot_kms = -2 :
                     * form at the orbital velocity, or 0 if single
                     */
                    star->omega =
                        stardata->common.zero_age.multiplicity > 1 ?
                        stardata->common.orbit.angular_frequency :
                        0.0;
                    Dprint("from omega orb");
                }
                else if(Fequal(initial_vrot_kms, VROT_NON_ROTATING * 1.0))
                {
                    /*
                     * initial_vrot_kms = -3 = VROT_NON_ROTATING :
                     * form with zero velocity
                     */
                    star->omega = 0.0;
                    Dprint("no rotation");
                }
                else
                {
                    if(Is_zero(initial_vrot_kms))
                    {
                        /*
                         * Given zero on the command line, or nothing,
                         * use the Hurley et al 2000/2002 formula
                         * Hurley, Phd
                         * Thesis (2000), Cambridge University,  p. 63
                         */
                        if(Is_not_zero(star->radius))
                        {
                            star->omega = OMEGA_FROM_VKM * Lang_initial_rotation(star->mass) / star->radius;
                        }
                        else
                        {
                            star->omega = 0.0;
                        }
                        Dprint("from Hurley");
                    }
                    else if(Fequal(initial_vrot_kms, VROT_BREAKUP * 1.0))
                    {
                        /*
                         * initial_vrot_kms = -1 = VROT_BREAKUP
                         * form at breakup velocity.
                         * NB This assumes the radius is deformed
                         * at the equator
                         * (see calculate_rotation_variables).
                         */
                        star->omega = star->omega_crit;
                        Dprint("from omega crit");
                    }
                    else
                    {
                        /*
                         * Use cmd-line specified vrot (km/s)
                         * and assume the star is not deformed.
                         */
                        if(Is_not_zero(star->radius))
                        {
                            star->omega = OMEGA_FROM_VKM * initial_vrot_kms / star->radius;
                        }
                        else
                        {
                            star->omega = 0.0;
                        }
                        Dprint("from cmd");
                    }
                }

                if(Is_not_zero(stardata->preferences->zero_age.fKerr[star->starnum]))
                {
                    /*
                     * Set angular momentum to be a fraction of the Kerr
                     * angular momentum. Made for black holes, but works for
                     * any other star also.
                     */
                    const double J =
                        stardata->preferences->zero_age.fKerr[star->starnum] *
                        Kerr_max_angular_momentum(star->mass);
                    star->angular_momentum = J;
                    star->omega = J / moment_of_inertia(stardata,
                                                        star,
                                                        star->radius);
                    Dprint("Set J = fKerr %g * JKerr %g = %g -> omega %g\n",
                           stardata->preferences->zero_age.fKerr[star->starnum],
                           Kerr_max_angular_momentum(star->mass),
                           J,
                           star->omega);
                }

                /*
                 * modulate by vrot_multiplier
                 */
                star->omega *= stardata->preferences->zero_age.vrot_multiplier[star->starnum];

                calculate_rotation_variables(star, star->radius);
            }

            /* and hence set the stellar angular momentum */
            Dprint("J from omega=%g moment_of_inertia_factor=%g rm=%g m=%g mc=%g rc=%g\n",
                   star->omega,
                   star->moment_of_inertia_factor,
                   star->radius,
                   star->mass,
                   Outermost_core_mass(star),
                   star->core_radius);

            star->angular_momentum = star->omega *
                moment_of_inertia(stardata,
                                  star,
                                  star->radius);

            Dprint("J = %g\n", star->angular_momentum);

            /* zero derivatives */
            star->derivative[DERIVATIVE_STELLAR_MASS_WIND_LOSS] = 0.0;
            star->derivative[DERIVATIVE_STELLAR_MASS_WIND_GAIN] = 0.0;
            star->derivative[DERIVATIVE_STELLAR_ANGMOM] = 0.0;
            star->stellar_timestep = 1e-6; /* large initial timestep */

            double dt;
            stellar_timestep(star->stellar_type,
                             star->age,
                             star->tm,
                             star->tn,
                             NULL,
                             &dt,
                             &stardata->model.time_remaining,
                             star,
                             stardata);
        }

        /*
         * Set phase start/end times to -1, i.e.
         * something unphysical.
         */
        for (Stellar_type kk = 0; kk < NUMBER_OF_STELLAR_TYPES; kk++)
        {
            star->phase_start[kk].time = -1.0;
            star->phase_end[kk].time = -1.0;
        }
    } /** end loop over stars **/

#if defined PRE_MAIN_SEQUENCE && defined FILE_LOG
    /*
     * Logging for the Pre-MS
     */
    if(Is_zero(model->dt) &&
       (ON_MAIN_SEQUENCE(stardata->star[0].stellar_type) ||
        ON_MAIN_SEQUENCE(stardata->star[1].stellar_type)) &&
       stardata->preferences->pre_main_sequence == TRUE)
    {
        Set_logstring(LOG_PREMAINSEQUENCE,
                      "PreMS aperi=%g t1=%g f1=%g R1=%g R1/RLperi1=%g R1/a(1-e)=%g t2=%g f2=%g R2=%g R2/RLperi2=%g R2/a(1-e)=%g R1+2/aperi=%g",
                      aperi,
                      stardata->star[0].age,
                      1e6 * stardata->star[0].age / preMS_lifetime(stardata->star[0].mass),
                      stardata->star[0].radius,
                      stardata->star[0].radius / stardata->star[0].roche_radius_at_periastron,
                      stardata->star[0].radius / aperi,
                      stardata->star[1].age,
                      1e6 * stardata->star[1].age / preMS_lifetime(stardata->star[1].mass),
                      stardata->star[1].radius,
                      stardata->star[1].radius / stardata->star[1].roche_radius_at_periastron,
                      stardata->star[1].radius / aperi,
                      (stardata->star[0].radius + stardata->star[1].radius) / aperi
            );
    }
#endif // PRE_MAIN_SEQUENCE && FILE_LOG


#ifdef NUCSYN
    Dprint("init abunds only ? %s\n", Yesno(stardata->preferences->initial_abunds_only == TRUE));
    if(stardata->preferences->initial_abunds_only == TRUE)
    {
        /* output initial abundances then exit */
        Printf("Initial abundances from mixture %d metallicity %g\n",
               (int)stardata->preferences->initial_abundance_mix,
               stardata->common.metallicity
            );
        nucsyn_initial_abundances(stardata,
                                  star1->Xenv,
                                  stardata->common.metallicity,
                                  stardata->preferences->initial_abundance_mix,
                                  TRUE);
        static const char element_strings[][4] = NUCSYN_SHORT_ELEMENT_STRINGS;
        Forward_isotope_loop(i)
        {
            Printf("%s%d=>%12.12e, # %d\n",
                   element_strings[stardata->store->atomic_number[i]],
                   stardata->store->nucleon_number[i],
                   star1->Xenv[i],
                   (int)i);
        }
        Exit_or_return_void(BINARY_C_SPECIAL_EXIT,
                            "exit init parameters: init abunds only");
    }
#endif //NUCSYN

    update_phase_variables(stardata);

    if(model->id_string[0] == '\0')
    {
        char * id_string;
        if(asprintf(&id_string,
                    "%d",
                    stardata->model.id_number) > 0)
        {
            strlcpy(stardata->model.id_string,
                    id_string,
                    STRING_LENGTH);
        }
        Safe_free(id_string);
    }

    set_uuid(stardata);
    memcpy(stardata->model.id_string,
           stardata->model.uuid,
           UUID_STR_LEN);

    /*
     * Set deprecated function pointers:
     * Note: this will be removed in 2.2.3
     */
#undef X
#define X(F) stardata->preferences->F##_function = stardata->preferences->function_hooks[BINARY_C_HOOK_##F];
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
    FUNCTION_HOOK_LIST;
#pragma GCC diagnostic pop
#undef X

    Dprint("Entering first timestep %g %g\n",
           stardata->star[0].phase_start_mass,
           stardata->star[1].phase_start_mass
        );
}
