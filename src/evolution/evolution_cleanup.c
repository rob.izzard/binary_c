#include "../binary_c.h"
No_empty_translation_unit_warning;



void evolution_cleanup(struct stardata_t * Restrict const stardata,
                       const Boolean close_logs)
{
    /*
     * post-evolution cleanup
     */

    Dprint("clean up evolution\n");

    if(close_logs == TRUE)
    {
        if(stardata->model.exit_after_end != 0)
        {
            /* exit code */
#ifdef FILE_LOG
            printf("FP1\n");
            close_log_files(&stardata->model.log_fp,
                            stardata);
            stardata->model.log_fp = NULL;
#endif
            Exit_binary_c(BINARY_C_WAS_TOLD_TO_EXIT,
                          "Exit requested through stardata->model.exit_after_end");
        }

        /***************************************************************/
        /** Close open files **/
#if !defined FILE_LOG_REWIND && defined FILE_LOG
        /* Log files must be closed before exiting evolution */
        close_log_files(&stardata->model.log_fp,
                        stardata);
        stardata->model.log_fp = NULL;
#endif
    }


    /* Final nucsyn logs */
#ifdef NUCSYN
    Dprint("Call calc_yields from evolution (XH1=%g)\n",
           stardata->star[0].Xenv[XH1]);
#endif // NUCSYN

    {
        SETstar(1);
        calc_yields(stardata,
                    star,
                    0.0,
#ifdef NUCSYN
                    NULL,
#endif
                    0.0,
#ifdef NUCSYN
                    NULL,
#endif
                    1,
                    YIELD_FINAL,
                    SOURCE_OTHER);

        /*
         * the following line is a fudge to get the binary_yield
         * output to work properly with the new evolution algorithm :
         * at some point this should be fixed!
         */
        stardata->model.dt = TINY * 2.0; // small but non-zero
#ifdef NUCSYN
        nucsyn_binary_yield(stardata, TRUE);
#endif // NUCSYN
    }
    Dprint("Leaving function evolution()\n");

//     /* Handle the printing of the events */
// #ifdef EVENT_BASED_LOGGING
//     event_based_logging_print_event_logstrings(stardata);
// #endif // EVENT_BASED_LOGGING

    /*
     * Clear the binary_c printf buffer here, if required
     * (this should have been done in log_every_timestep also)
     */
    if(stardata->preferences->internal_buffering ==
       INTERNAL_BUFFERING_PRINT)
    {
        Clear_printf_buffer;
    }

    stardata->evolving = FALSE;

    disable_binary_c_timeout();
}
