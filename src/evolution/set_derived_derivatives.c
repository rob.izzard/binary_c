#include "../binary_c.h"
No_empty_translation_unit_warning;



/*
 * Set derived derivatives
 *
 * Note: this is sometimes called prior to event handling,
 * in which case the derivatives may not be 100% correct.
 *
 * If kin (passed in) is -1, we loop over both stars,
 * otherwise we just evaluate for star k
 */
#undef Derivative
#define Derivative(X) ((new->X - old->X) * idt)
#define DerivativeM(MACRO) ((MACRO(new) - MACRO(old)) * idt)

void set_derived_derivatives(struct stardata_t * const stardata,
                             struct star_t * new,
                             Star_number kin)
{
    Star_number k;
    Boolean single = System_is_single;

    Number_of_stars_Starloop(k)
    {
        if(kin==-1 || k==kin)
        {
            if(new == NULL)
            {
                new = &stardata->star[k];
            }
            const struct star_t * const old = &stardata->previous_stardata->star[k];

            if(Is_not_zero(stardata->model.dt) &&
               new->stellar_type != MASSLESS_REMNANT)
            {
                /*
                 * Set derivatives of things that are NOT calculated
                 * as derivatives in any algorithm.
                 */
                const double idt = 1.0 / stardata->model.dt;
                new->derivative[DERIVATIVE_STELLAR_RADIUS] =
                    Derivative(radius);
                new->derivative[DERIVATIVE_STELLAR_ROCHE_RADIUS] =
                    single==TRUE ? 0.0 : Derivative(roche_radius);
                new->derivative[DERIVATIVE_STELLAR_LUMINOSITY] =
                    Derivative(luminosity);
                new->derivative[DERIVATIVE_STELLAR_TEMPERATURE] =
                    DerivativeM(Teff_from_star_struct);
                new->derivative[DERIVATIVE_STELLAR_CORE_MASS] =
                    DerivativeM(Outermost_core_mass);
            }
            else
            {
                new->derivative[DERIVATIVE_STELLAR_RADIUS] = 0.0;
                new->derivative[DERIVATIVE_STELLAR_ROCHE_RADIUS] = 0.0;
                new->derivative[DERIVATIVE_STELLAR_LUMINOSITY] = 0.0;
                new->derivative[DERIVATIVE_STELLAR_TEMPERATURE] = 0.0;
                new->derivative[DERIVATIVE_STELLAR_CORE_MASS] = 0.0;
            }
            Dprint("DERIVED %g %d : %d %d %d : Rwas %g Ris %g -> dR/dt %g\n",
                   stardata->model.time,
                   stardata->model.model_number,
                   k,
                   old->starnum,
                   new->starnum,
                   old->radius,
                   new->radius,
                   new->derivative[DERIVATIVE_STELLAR_RADIUS]);
        }
    }
}
