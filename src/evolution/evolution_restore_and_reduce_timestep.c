#include "../binary_c.h"
No_empty_translation_unit_warning;

void evolution_restore_and_reduce_timestep(struct stardata_t * Restrict const stardata,
                                           const Boolean preserve_events)
{
    /*
     * Restore stardata from previous_stardata,
     * reduce its timestep,
     * and reset its reject flag to FALSE.
     */
    const double dt_zoomfac = stardata->model.dt_zoomfac;

#ifdef LOG_REJECTIONS
    /*
     * Force logging of rejections *now*
     */
    const Reject_index reject_reason = Elvis3(stardata->model.reject_shorten_timestep,
                                              stardata->star[0].reject_shorten_timestep,
                                              stardata->star[1].reject_shorten_timestep);
    if(reject_reason &&
       stardata->preferences->rejects_in_log == TRUE)
    {
        char * reject_label = evolution_rejection_label(stardata,
                                                        reject_reason);
        logwrap(stardata,reject_label,"");
        Safe_free(reject_label);
    }
#endif // LOG_REJECTIONS

    /*
     * Save the mask so we can free it
     */
    struct stardata_t * mask = stardata->model.restore_mask;
    struct stardata_t * mask_contents = stardata->model.restore_mask_contents;
    if(mask != NULL)
    {
        Dprint("restore mask %p %p\n",
               (void*)mask,
               (void*)mask_contents);
    }

    evolution_restore_from_previous(stardata,
                                    preserve_events);

    if(mask != NULL)
    {
        Dprint("free restore mask %p %p\n",
               (void*)mask,
               (void*)mask_contents);
        Safe_free(mask);
        Safe_free(mask_contents);
    }
    stardata->model.restore_mask = NULL;
    stardata->model.restore_mask_contents = NULL;


    Dprint("ZOOM : Restored to (stardata %p, prefs = %p, prev %p, prev->prefs %p) at t=%30.16g with dtm = %g (reject = %u, dt_RLOF = %g, zoom = %g) [R0=%g RL0=%g -> RLOF %s] [R1=%g RL1=%g -> RLOF %s] \n",
           (void*)stardata,
           (void*)stardata->preferences,
           (void*)stardata->previous_stardata,
           (void*)stardata->previous_stardata->preferences,
           stardata->model.time,
           stardata->model.dtm,
           stardata->model.reject_shorten_timestep,
           stardata->model.RLOF_recommended_timestep,
           stardata->model.dt_zoomfac,
           stardata->star[0].radius,
           stardata->star[0].roche_radius,
           Yesno(stardata->star[0].radius > stardata->star[0].roche_radius),
           stardata->star[1].radius,
           stardata->star[1].roche_radius,
           Yesno(stardata->star[1].radius > stardata->star[1].roche_radius)
        );

    /*
     * Shorten the timestep.
     */

    /*
     * Restore the timestep zoom factor
     */
    stardata->model.dt_zoomfac = dt_zoomfac;

    /*
     * And reduce it to zoom in
     */
    modulate_zoomfac(stardata,
                     ZOOMFAC_DECREASE_TIMESTEP);

    stardata->model.dtm *= stardata->model.dt_zoomfac;

    /*
     * Never go smaller than the timestep minimum
     */
    stardata->model.dtm = Max(stardata->preferences->minimum_timestep,
                              stardata->model.dtm);

    Dprint("ZOOM new dtm = %g, dt_zoomfac = %g\n",
           stardata->model.dtm,
           stardata->model.dt_zoomfac);

    /*
     * Reset the reject flags
     */
    stardata->model.reject_shorten_timestep = REJECT_NONE;
    Star_number k;
    Starloop(k)
    {
        stardata->star[k].reject_shorten_timestep = REJECT_NONE;
    }

}
