#pragma once
#ifndef EVOLUTION_PARAMETERS_H
#define EVOLUTION_PARAMETERS_H

#include "evolution_parameters.def"

#undef X
#define X(CODE,NUM) SYSTEM_ACTION_##CODE = (NUM),
enum { SYSTEM_ACTIONS_LIST };
#undef X


#undef X
#define X(CODE) BINARY_C_TASK_##CODE ,
enum { BINARY_C_TASKS_LIST };
#undef X
#define X(CODE) #CODE,
static const char * const binary_c_tasks_strings[] = { BINARY_C_TASKS_LIST };
#undef X

#define X(CODE) SOLVER_##CODE ,
enum { SOLVERS_LIST };
#undef X


#endif // EVOLUTION_PARAMETERS_H
