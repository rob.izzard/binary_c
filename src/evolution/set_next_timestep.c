#include "../binary_c.h"
No_empty_translation_unit_warning;

void set_next_timestep(struct stardata_t * Restrict const stardata)
{
    /*
     * Set the timestep to be the minimum of the stellar
     * timesteps and the time remaining
     */
    const double dtmwas Maybe_unused = stardata->previous_stardata->model.dtm;
    const double time_remaining = stardata->model.max_evolution_time - stardata->model.time;
    Dprint("Set dtm = Min3(star0 = %g, star1 = %g, remaining = %g - %g = %g, zoomfac = %g) dtmwas = %g\n",
           stardata->star[0].stellar_timestep,
           stardata->star[1].stellar_timestep,
           stardata->model.max_evolution_time,
           stardata->model.time,
           stardata->model.max_evolution_time-stardata->model.time,
           stardata->model.dt_zoomfac,
           dtmwas
        );

    /*
     * Choose the minimum of all the stars and the
     * time remaining.
     */
    stardata->model.dtm = time_remaining;
    Dprint("first dtm = time_remaining = %g\n",stardata->model.dtm);

    Star_number k;
    Starloop(k)
    {
        stardata->model.dtm = Min(stardata->model.dtm,
                                  stardata->star[k].stellar_timestep);
        Dprint("apply star %d timestep = %g -> dtm = %g\n",
               k,
               stardata->star[k].stellar_timestep,
               stardata->model.dtm);
    }

    /*
     * Apply nonstellar timestep
     */
    double dtm = time_remaining;
    nonstellar_timestep(stardata,&dtm);
    stardata->model.dtm =
        Is_zero(stardata->model.dtm) ?
        dtm :
        Min(stardata->model.dtm,
            dtm);
    Dprint("nonstellar timestep %g\n",stardata->model.dtm);

    /*
     * Save the timestep on the first
     * RLOF iteration
     */
    if(stardata->model.iter==0) stardata->model.dtm0 = stardata->model.dtm;

    /*
     * Apply zoom factor
     */
    stardata->model.dtm *= stardata->model.dt_zoomfac;
    Dprint("post-zoomfac %g\n",stardata->model.dtm);

    /*
     * Do not let the timestep be > maximum_timestep_factor * previous timestep
     */
    if(Is_not_zero(dtmwas) &&
       Is_not_zero(stardata->preferences->maximum_timestep_factor))
    {
        stardata->model.dtm = Min(stardata->preferences->maximum_timestep_factor * dtmwas,
                                  stardata->model.dtm);
        Dprint("Limit by factor %g\n",stardata->model.dtm);
    }

    /*
     * Calculate timestep in years
     */
    stardata->model.dt = 1e6 * stardata->model.dtm;

    Dprint("at t=%20.12g next timestep is dtm=%g Myr (stellar_timestep %g,%g, dtr=%g) dt=%g (dt_zoomfac=%g)\n",
           stardata->model.time,
           stardata->model.dtm,
           Max(stardata->star[0].stellar_timestep,stardata->star[1].stellar_timestep),
           Min(stardata->star[0].stellar_timestep,stardata->star[1].stellar_timestep),
           stardata->model.max_evolution_time-stardata->model.time,
           stardata->model.dt,
           stardata->model.dt_zoomfac);

}
