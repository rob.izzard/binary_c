#include "../binary_c.h"
No_empty_translation_unit_warning;


Boolean smooth_evolution(struct stardata_t * const stardata)
{
    /*
     * Define "smooth" evolution to be when the stellar
     * types do not change, the system remains single or binary,
     * and there are no events
     */
    return Boolean_(stardata->previous_stardata != NULL &&
                    stardata->star[0].stellar_type == stardata->previous_stardata->star[0].stellar_type &&
                    stardata->star[1].stellar_type == stardata->previous_stardata->star[1].stellar_type &&
                    _System_is_binary(stardata) == _System_is_binary(stardata->previous_stardata) &&
                    events_pending(stardata) == FALSE);
}
