#include "../binary_c.h"
No_empty_translation_unit_warning;

/*
 * Return a string describing why we reject
 * the evolution.
 */

char * evolution_rejection_label(struct stardata_t * Restrict const stardata,
                                 Reject_index reject_reason)
{
    char * reject_label;
    const int x =
        asprintf(&reject_label,
                 "Reject %s->%s : restore to t=%30.20g (deltat=%g)",
                 (stardata->model.reject_shorten_timestep ? "model" :
                  stardata->star[0].reject_shorten_timestep ? "star0" :
                  stardata->star[1].reject_shorten_timestep ? "star1" :
                  "unknown"),
                 Reject_string(reject_reason),
                 stardata->previous_stardata->model.time,
                 stardata->previous_stardata->model.time - stardata->model.time
            );

    Dprint("REJECT %s\n",reject_label);
    if(x<0)
    {
        /* error */
        Exit_binary_c(BINARY_C_ALLOC_FAILED,
                      "Failed to asprintf a rejection label for the log.");
    }
    return reject_label;
}
