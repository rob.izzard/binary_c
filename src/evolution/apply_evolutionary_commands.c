#include "../binary_c.h"
No_empty_translation_unit_warning;
#include "apply_evolutionary_commands.h"

void apply_evolutionary_commands(struct stardata_t * Restrict const stardata,
                                 const Boolean apply_derivatives,
                                 const Boolean apply_events,
                                 const Boolean delete_expired)
{
    /*
     * Apply commands as set in JSON command file
     */

    struct cdict_t * command_cdict = stardata->preferences->commands;
    if(command_cdict != NULL &&
       (apply_derivatives || apply_events || delete_expired))
    {
        if(apply_events || apply_derivatives)
        {
            CDict_loop(command_cdict, event)
            {
                char * event_label Maybe_unused = event->key.key.string_data;
                aprint("Found event \"%s\"\n",
                       event_label);

                if(event->value.type == CDICT_DATA_TYPE_CDICT)
                {
                    CDict_loop(event->value.value.cdict_pointer_data, command)
                    {
                        struct cdict_t * const cdict = command->value.value.cdict_pointer_data;
                        const char * const subcommand = command->key.key.string_data;

                        if(Strings_equal(subcommand, "accrete period"))
                        {
                            /*
                             * Accretion over a period of time
                             *
                             * Required data:
                             * "stars" : an array of star numbers to which this is applied
                             * "start" : start time
                             * "end" : end time
                             * "rate" : accretion rate (Msun/year)
                             */
                            const double start = Get_cdict_double(cdict,start);
                            const double end = Get_cdict_double(cdict,end);
                            aprint("t = %g : accrete from t=%g to t=%g, in time range? %d %d \n",
                                   stardata->model.time,
                                   start,
                                   end,
                                   More_or_equal(stardata->model.time,start),
                                   stardata->model.time < end);

                            if(apply_derivatives == TRUE)
                            {
                                if(stardata->model.time >= end)
                                {
                                    Expire(cdict,
                                           "End %s \"%s\"",
                                           subcommand,
                                           event_label);
                                }
                                else if(More_or_equal(stardata->model.time,start))
                                {
                                    /*
                                     * Apply derivative
                                     */
                                    struct cdict_entry_t * const stars = CDict_nest_get_entry(cdict,
                                                                                              "stars");
                                    for(cdict_size_t i=0; i<stars->value.count; i++)
                                    {
                                        const int starnum = stars->value.value.int_array_data[i];
                                        const double rate = Get_cdict_double(cdict,rate);
                                        stardata->star[starnum].derivative[DERIVATIVE_STELLAR_MASS_COMMAND] = rate;
                                        aprint("Set star %d dM/dt = %g\n",starnum,rate);
                                    }

                                    /*
                                     * Perhaps log
                                     */
                                    if(CDict_nest_get_entry(cdict,"__started") == NULL &&
                                       Fequal(stardata->model.time,start))
                                    {
                                        CDict_nest_set(cdict,"__started",1);
                                        Append_logstring(LOG_COMMAND,
                                                         "Start %s \"%s\"",
                                                         subcommand,
                                                         event_label);
                                    }
                                }
                            }
                        }
                        else if(Strings_equal(subcommand, "accrete event"))
                        {
                            /*
                             * Event-like accretion, i.e. a delta function in time
                             *
                             * Required data
                             * stars : an array of star numbers to which this is applied
                             * at : time at which the accretion happens
                             * mass : mass to accrete (could be negative)
                             *
                             * Optional:
                             * mix centre : mass of region, from the centre, to be mixed
                             * mix centre frac : fraction of star mixed [overrides mix centre]
                             */
                            const double at = Get_cdict_double(cdict,at);
                            double mix_centre Maybe_unused = Get_cdict_double(cdict,mix_centre);
                            const double mix_centre_frac Maybe_unused = Get_cdict_double(cdict,mix_centre_frac);

                            aprint("Accrete event at %g, now? %d apply events? %d\n",
                                   at,
                                   Fequal(stardata->model.time,at),
                                   apply_events);

                            if(apply_events &&
                               Fequal(stardata->model.time,at))
                            {
                                struct cdict_entry_t * const stars = CDict_nest_get_entry(cdict,
                                                                                          "stars");
                                for(cdict_size_t i=0; i<stars->value.count; i++)
                                {
                                    const int starnum = stars->value.value.int_array_data[i];
                                    const double mass = Get_cdict_double(cdict,mass);
                                    struct star_t * const star = stardata->star + starnum;
                                    update_mass(stardata,star,mass);
#ifdef MINT
                                    //fprintf(stderr,"pre-mix %g\n",star->mint->XHc);
                                    if(Is_not_zero(mix_centre_frac))
                                    {
                                        mix_centre = mix_centre_frac * star->mass;
                                    }
                                    if(Is_not_zero(mix_centre))
                                    {
                                        MINT_mix_star(stardata,
                                                      star,
                                                      0.0,
                                                      mix_centre);
                                        star->mint->XHc = star->mint->shells[0].X[XH1];
                                        star->mint->XHec = star->mint->shells[0].X[XHe4];
                                    }
                                    //fprintf(stderr,"post-mix %g\n",star->mint->XHc);
#endif // MINT
                                }

                                Expire(cdict,
                                       "Event %s \"%s\"",
                                       subcommand,
                                       event_label);
                            }
                        }
                        else
                        {
                            Exit_binary_c(BINARY_C_UNKNOWN_COMMAND,
                                          "Unknown command \"%s\" in event \"%s\"\n",
                                          subcommand,
                                          event_label);
                        }
                    }
                }
            }
        }

        /*
         * Delete expired events and post to the log
         * that they have finished
         */
        if(delete_expired == TRUE)
        {
            CDict_loop(command_cdict, event)
            {
                char * const event_label Maybe_unused = event->key.key.string_data;
                CDict_loop(event->value.value.cdict_pointer_data, command)
                {
                    struct cdict_entry_t * const e =
                        CDict_nest_get_entry(command->value.value.cdict_pointer_data,
                                             "__expired");
                    if(e)
                    {
                        Append_logstring(LOG_COMMAND,
                                         "%s",
                                         e->value.value.string_data);
                        CDict_del(event->value.value.cdict_pointer_data,
                                  command);
                    }
                }

                int count = 0;
                CDict_loop(event->value.value.cdict_pointer_data, command)
                {
                    count++;
                }
                if(count == 0)
                {
                    CDict_del_and_contents(command_cdict,event,TRUE);
                }
            }
        }
    }
}
