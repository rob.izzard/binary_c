#include "../binary_c.h"
No_empty_translation_unit_warning;
#include "evolution_macros.h"

/*
 * Model has been rejected. Decide how to deal
 * with this situation.
 *
 * Usually, this means going back to the previous
 * timestep (stardata->previous_stardata) and
 * repeating with a smaller timestep.
 *
 * Some special cases are caught which would
 * otherwise cause lock-ups.
 */

int evolution_rejected(struct stardata_t * Restrict const stardata)
{
    int retval;
    Dprint("rejected at t=%20.12g with dt_zoomfac = %g (R1 %g, RL1 %g, M1 %g, st1 %d, R2 %g, RL2 %g, M2 %g st2 %d : prev R1 %g RL1 %g M1 %g st1 %d, RL2 %g R2 %g M2 %g st2 %d) : events pending? %d, SN %d %d\n",
           stardata->model.time,
           stardata->model.dt_zoomfac,
           stardata->star[0].radius,
           stardata->star[0].roche_radius,
           stardata->star[0].mass,
           stardata->star[0].stellar_type,
           stardata->star[1].radius,
           stardata->star[1].roche_radius,
           stardata->star[1].mass,
           stardata->star[1].stellar_type,
           stardata->previous_stardata->star[0].radius,
           stardata->previous_stardata->star[0].roche_radius,
           stardata->previous_stardata->star[0].mass,
           stardata->previous_stardata->star[0].stellar_type,
           stardata->previous_stardata->star[1].radius,
           stardata->previous_stardata->star[1].roche_radius,
           stardata->previous_stardata->star[1].mass,
           stardata->previous_stardata->star[1].stellar_type,
           events_pending(stardata),
           stardata->model.supernova,
           stardata->previous_stardata->model.supernova
        );

    /*
     * Free any pre-event data
     */
    if(stardata->pre_events_stardata != NULL)
    {
        free_stardata(&stardata->pre_events_stardata);
    }

    /*
     * Restore the previous stardata and reduce the timestep
     * if reject == REJECTION_RESULT_SHORTEN_TIMESTEP
     */
    evolution_restore_and_reduce_timestep(stardata,
                                          FALSE);

    /*
     * If the previous timestep contained a supernova,
     * it's possible that this caused RLOF, in which case
     * we'll *never* converge. Just let RLOF continue.
     */
    if(stardata->previous_stardata->model.supernova==TRUE)
    {
        retval =
            RLOF_overflowing(stardata,1.0)==TRUE ?
            EVOLUTION_CONTINUE_RLOF :
            EVOLUTION_CONTINUE;
    }

    /*
     * Otherwise restore to previous timestep's structure
     */
    else
    {
        retval = EVOLUTION_REJECT;
        evolution_restore_and_reduce_timestep(stardata,
                                             FALSE);

        if(DO_DEBUG)
        {
            const Boolean ov = RLOF_overflowing(stardata,1.0);
            Dprint("reject restored with retval %d (%s) to t=%20.12g with dt_zoomfac = %g (R1 %g, RL1 %g, R2 %g, RL2 %g) : prev overflow? %s : events pending? %d model.reject_shorten_timestep = %u, recommended next dt %g\n",
                   retval,
                   Evolution_status_string(retval),
                   stardata->model.time,
                   stardata->model.dt_zoomfac,
                   stardata->star[0].radius,
                   stardata->star[0].roche_radius,
                   stardata->star[1].radius,
                   stardata->star[1].roche_radius,
                   Yesno(ov),
                   events_pending(stardata),
                   stardata->model.reject_shorten_timestep,
                   stardata->model.RLOF_recommended_timestep
                );
        }
    }

    /*
     * Because the timestep has been rejected, erase the event stack
     * and any associated data.
     */
    erase_events(stardata);

    Dprint("reject return %d (%s) \n",
           retval,
           Evolution_status_string(retval)
        );
    return retval;
}
