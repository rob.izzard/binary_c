#pragma once
#ifndef EVOLUTION_STATUS_CODES_H
#define EVOLUTION_STATUS_CODES_H

#include "evolution_status_codes.def"

/*
 * Construct evolution status codes
 */
#undef X
#define X(CODE,TEXT) CODE,
enum { EVOLUTION_STATUS_CODES };

#undef X
#define X(CODE,TEXT) #CODE,
static char * evolution_status_code_macros[] Maybe_unused = { EVOLUTION_STATUS_CODES };

#undef X
#define X(CODE,TEXT) TEXT,
static char * evolution_status_code_strings[] Maybe_unused = { EVOLUTION_STATUS_CODES };

#define Evolution_status_string(N) Array_string(evolution_status_code_strings,(N))

#undef X
#endif // EVOLUTION_STATUS_CODES_H
