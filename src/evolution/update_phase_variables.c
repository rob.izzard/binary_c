#include "../binary_c.h"
No_empty_translation_unit_warning;

void update_phase_variables(struct stardata_t * Restrict const stardata)
{
    /*
     * Update the ages since the stellar type start,
     * and update phase start/end times.
     */
    Foreach_star(star)
    {
        const Stellar_type is = star->stellar_type;
        const Stellar_type was = stardata->previous_stardata->star[star->starnum].stellar_type;

        /*
         * Update phase_end variables with the current
         * variables: handy, e.g. in common-envelope evolution.
         */
        if(is != was)
        {
            star->stellar_type_tstart = stardata->model.time;



            if(star->phase_start[was].time < -0.5)
            {
                star->phase_end[was].time = stardata->model.time;
                star->phase_end[was].luminosity = star->luminosity;
                star->phase_end[was].mass = star->mass;
                star->phase_end[was].radius = star->radius;
                star->phase_end[was].drdm = star->drdm;
            }

            if(star->phase_start[is].time < -0.5)
            {
                star->phase_start[is].time = stardata->model.time;
                star->phase_start[is].luminosity = star->luminosity;
                star->phase_start[is].mass = star->mass;
                star->phase_start[is].radius = star->radius;
                star->phase_start[is].drdm = star->drdm;
            }

        }

        star->phase[is].maximum_luminosity =
            Max(star->luminosity,
                star->phase[is].maximum_luminosity);
        star->phase[is].maximum_mass =
            Max(star->mass,
                star->phase[is].maximum_mass);
        star->phase[is].maximum_radius =
            Max(star->radius,
                star->phase[is].maximum_radius);

        star->phase[is].time_weighted_mass +=
            stardata->model.dt * star->mass;
        star->phase[is].time_weighted_luminosity +=
            stardata->model.dt * star->luminosity;
        star->phase[is].time_weighted_radius +=
            stardata->model.dt * star->radius;
        star->phase[is].phase_time +=
            stardata->model.dt;

        /*
         * pre-empt phase end just in case there's, e.g., comenv
         */
        star->phase_end[is].time = stardata->model.time;
        star->phase_end[is].luminosity = star->luminosity;
        star->phase_end[is].mass = star->mass;
        star->phase_end[is].radius = star->radius;
        star->phase_end[is].drdm = star->drdm;
    }
}
