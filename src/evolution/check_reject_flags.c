#include "../binary_c.h"
No_empty_translation_unit_warning;

Rejection Pure_function check_reject_flags(struct stardata_t * Restrict const stardata)
{
    /*
     * Check global and stellar timestep reject flags.
     *
     * return value:
     *
     * REJECTION_RESULT_NONE : there is nothing to reject.
     * REJECTION_RESULT_SHORTEN_TIMESTEP : we can reject and shorten
     *                                     the timestep.
     * REJECTION_RESULT_SAME_TIMESTEP : we can reject with the same
     *                                  timestep.
     */
    stardata->common.last_reject_time = stardata->model.time;
    Rejection reject = REJECTION_RESULT_NONE;

    if(stardata->common.n_events > 0 &&
       stardata->preferences->stop_at_event == TRUE)
    {
        /*
         * We've detected an event and we're required to stop here
         */
        reject = EVOLUTION_REJECT_AND_RETURN_NOW;
        stardata->common.last_reject_time = stardata->model.time;
    }
    else
    {
        const Boolean can = can_reject_with_same_timestep(stardata);
        const Boolean can_and_shorten =
            can == FALSE ? FALSE : can_reject_and_shorten_timestep(stardata);
        char * reason = NULL;
        struct star_t * reject_star = NULL;

#define _Rejection_tests(X,Y)                                           \
        if(stardata->model.reject_##X##_timestep != REJECT_NONE)        \
        {                                                               \
            reject = Y;                                                 \
            stardata->common.last_reject_time = stardata->model.time;   \
            reason = Reject_string(stardata->model.reject_##X##_timestep); \
            Dprint("Reject (and %s timestep) model at t=%g : why? %s\n", \
                   Stringify_macro(X),                                  \
                   stardata->model.time,                                \
                   reason);                                             \
        }                                                               \
        else                                                            \
        {                                                               \
            Foreach_star(star)                                          \
            {                                                           \
                if(star->reject_##X##_timestep != REJECT_NONE)          \
                {                                                       \
                    reject = Y;                                         \
                    stardata->common.last_reject_time = stardata->model.time; \
                    reason = Reject_string(star->reject_##X##_timestep); \
                    reject_star = star;                                 \
                    Dprint("Reject (and %s timestep) star%d at t=%g : why? %s\n", \
                           Stringify_macro(X),                          \
                           star->starnum,                               \
                           stardata->model.time,                        \
                           reason);                                     \
                    break;                                              \
                }                                                       \
            }                                                           \
        }

        /*
         * First check if we need to reject and shorten the timestep.
         */
        _Rejection_tests(shorten,
                         REJECTION_RESULT_SHORTEN_TIMESTEP);
        Dprint("Do we want to reject with shortened timestep? %s\n",Yesno(reject));

        if(can == TRUE &&
           reject == REJECTION_RESULT_NONE)
        {

            /*
             * Now, if not already rejected, check if we need
             *  to reject and keep the timestep the same.
             */
            _Rejection_tests(same,
                             REJECTION_RESULT_SAME_TIMESTEP);
        }

        if(reject != REJECTION_RESULT_NONE)
        {
            stardata->model.n_rejected_models++;
        }

        if(reject != REJECTION_RESULT_NONE &&
           (can == FALSE ||
            (can_and_shorten == FALSE && reject == REJECTION_RESULT_SHORTEN_TIMESTEP)))
        {
            /*
             * We want to reject, but cannot, so perhaps log or fail.
             */

            if(reject == REJECTION_RESULT_SHORTEN_TIMESTEP
               &&
               (stardata->preferences->log_all_reject_timestep_failures == TRUE ||
                stardata->model.timestep_rejected_without_shorten == FALSE))
            {
                stardata->model.n_rejected_models_without_shorten++;
                reject = REJECTION_RESULT_WANT_TO_SHORTEN_TIMESTEP_BUT_CANNOT;
                char * log_string;
                if(asprintf(&log_string,
                            "Tried to reject and shorten timestep but could not. [%s%*d why? %s M=%g Menv=%g] (model %d at time %12.10e, currently dt = %g Myr, want dt = %g Myr < min dt = %g Myr) Will now %s.",
                            reject_star != NULL ? "star " : "system",
                            reject_star != NULL ? 1 : 0,
                            reject_star != NULL ? reject_star->starnum : 0,
                            reason,
                            stardata->star[0].mass,
                            Outermost_core_mass(&stardata->star[0]),
                            stardata->model.model_number,
                            stardata->model.time,
                            stardata->model.dt * 1e-6,
                            stardata->model.dt * 1e-6 * stardata->model.dt_zoomfac,
                            stardata->preferences->minimum_timestep,
                            stardata->preferences->cannot_shorten_timestep_policy == CANNOT_SHORTEN_FAIL ? "fail with an error" :
                            stardata->preferences->cannot_shorten_timestep_policy == CANNOT_SHORTEN_CONTINUE ? "continue in the hope things are ok" :
                            stardata->preferences->cannot_shorten_timestep_policy == CANNOT_SHORTEN_RESTORE_AND_TRY_EVENTS ? "revert to previous timestep but keep events" :
                            "do something unknown")<=0)
                {
                    Exit_binary_c(BINARY_C_ALLOC_FAILED,"Could not asprintf log string");
                }

                Append_logstring(LOG_REJECT,"%s",log_string);
                logwrap(stardata,log_string,"");
                Safe_free(log_string);
                stardata->model.timestep_rejected_without_shorten = TRUE;

                if(stardata->preferences->cannot_shorten_timestep_policy == CANNOT_SHORTEN_FAIL)
                {
                    /*
                     * We should fail immediately, before returning. This
                     * is to prevent further stellar evolution being attempted
                     * that may fail badly.
                     */
#undef X
#define X(CODE) "CANNOT_SHORTEN_"#CODE,
                    static const char * const cannot_shorten_policy_strings[] = { CANNOT_SHORTEN_OPTIONS_LIST };
#undef X
                    Exit_binary_c(BINARY_C_CANNOT_SHORTEN,
                                  "binary_c wanted to shorten the timestep because of a failure to apply a derivative, but the timestep is already %g Myr which <= minimum timestep %g Myr. Current cannot_shorten_timestep_policy is %d = %s\n",
                                  stardata->model.dt * 1e-6,
                                  stardata->preferences->minimum_timestep,
                                  stardata->preferences->cannot_shorten_timestep_policy,
                                  cannot_shorten_policy_strings[stardata->preferences->cannot_shorten_timestep_policy]
                        );
                }
            }
        }
    }

    Dprint("Reject? %s\n",Yesno(reject));
    return reject;
}
