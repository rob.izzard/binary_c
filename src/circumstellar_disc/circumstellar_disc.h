#pragma once
#ifndef CIRCUMSTELLAR_DISC_H
#define CIRCUMSTELLAR_DISC_H


/*
 * Circumstellar disc physics header file
 *
 * Currently, these are dedictated to Be-excretion disc
 * formation and detection, but other physics could easily be
 * incorporated.
 */
#include "circumstellar_disc/circumstellar_disc_macros.h"

/*
 * System types
 */
#define Be_XRAY_TYPES_LIST                      \
    X(I)                                        \
        X(II)

#undef X
#define X(N) Be_XRAY_TYPE_##N,
enum{
    Be_XRAY_TYPES_LIST
};

/*
 * Decretion disc radius algorithms
 */
#define DECRETION_DISC_RADIUS_ALGORITHMS_LIST   \
    X(ZHANG_RIMULO)                             \
        X(COE_KIRK_2015)

#undef X
#define X(ALGORITHM) DECRETION_DISC_RADIUS_##ALGORITHM,
enum{
    DECRETION_DISC_RADIUS_ALGORITHMS_LIST
};
#undef X

#endif // CIRCUMSTELLAR_DISC_H
