#include "../binary_c.h"
No_empty_translation_unit_warning;


double cs_LX_Be(struct stardata_t * const stardata,
                struct star_t * const accretor)
{
    /*
     * X-ray luminosity of a Be-XRB caused by disc
     * accretion, in solar units.
     *
     * Boyuan Liu's Eqs. 5 and 6
     */
    const double ee = Limit_range(stardata->common.orbit.eccentricity, 0.0, 1.0 - 1e-6);
    const double beta = 2.0; // Bondi-like accretion
    const double Sigma0 = cs_Sigma0(stardata, accretor);
    return
        1.25e36 /
        Pow2((1.0 - ee) * stardata->common.orbit.separation / 100.0) *
        (Sigma0 / 0.015) *
        pow(accretor->mass/1.4, beta)
        / L_SUN;
}
