#pragma once
#ifndef CIRCUMSTELLAR_DISC_PROTOTYPES_H
#define CIRCUMSTELLAR_DISC_PROTOTYPES_H


/*
 * Decretion discs
 */
double cs_decretion_disc_size(struct stardata_t * const stardata,
                              struct star_t * const donor);

double cs_Sigma0(struct stardata_t * const stardata,
                 struct star_t * const accretor);

double cs_truncation_fraction(struct stardata_t * const stardata Maybe_unused,
                              struct star_t * const accretor);


/*
 * Be star specific functions
 */
Boolean cs_Be_criterion(struct stardata_t * const stardata,
                        struct star_t * const donor,
                        struct star_t * const accretor);

double cs_LX_Be(struct stardata_t * const stardata,
                struct star_t * const accretor);

double cs_peak_Be_accretion_rate(struct stardata_t * const stardata,
                                 struct star_t * const accretor);

Boolean cs_Be_tidal_warp_criterion(struct stardata_t * const stardata,
                                   struct star_t * const donor,
                                   struct star_t * const accretor);

Boolean cs_Be_accretion_rate_criterion(struct stardata_t * const stardata,
                                       struct star_t * const accretor);

Boolean cs_XRB_duty_cycle_criterion(struct stardata_t * const stardata,
                                    struct star_t * const donor,
                                    struct star_t * const accretor);

int cs_Be_XRB_type(struct stardata_t * const stardata,
                   struct star_t * const donor,
                   struct star_t * const accretor);

double cs_tidal_warp_semi_major_axis(struct stardata_t * const stardata,
                                     struct star_t * const donor,
                                     struct star_t * const accretor);

#endif // CIRCUMSTELLAR_DISC_PROTOTYPES_H
