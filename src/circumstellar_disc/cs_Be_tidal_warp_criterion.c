#include "../binary_c.h"
No_empty_translation_unit_warning;

Boolean cs_Be_tidal_warp_criterion(struct stardata_t * const stardata,
                                   struct star_t * const donor,
                                   struct star_t * const accretor)
{
    /*
     * Boyuan Liu's tidal warp criterion (Eq. 8)
     * as computed in cs_tidal_warp_semi_major_axis(...).
     */
    const double acrit = cs_tidal_warp_semi_major_axis(stardata,donor,accretor);
    return stardata->common.orbit.separation * R_SUN < acrit;
}
