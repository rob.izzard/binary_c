#include "../binary_c.h"
No_empty_translation_unit_warning;

double cs_peak_Be_accretion_rate(struct stardata_t * const stardata,
                                 struct star_t * const accretor)
{
    /*
     * Estimate peak accretion rate onto accretor in Msun/year.
     *
     * Rimulo et al. (2018) MNRAS 476, 3555
     * Brown et al.  (2019) MNRAS 488, 387
     *
     */
    const double Sigma0 = cs_Sigma0(stardata,accretor);

    /*
     * Hence mdot acc,sim valid for a NS of mass 1.4Msun
     * around an 18Msun Be star with R=7Rsun for
     * 0 < e < 1.0 and
     * Porb = 2pi * sqrt(a^3 / G(Mstar + MX))
     */
    const double ee = Limit_range(stardata->common.orbit.eccentricity, 0.0, 1.0 - 1e-6);

    /*
     * Boyuan Liu's Eq. 2
     * https://www.overleaf.com/project/632c916bd7878c40e2dc9191
     */
    const double mdot_acc_sim =
        4.4e-10 /
        Pow2((1.0 - ee) * stardata->common.orbit.separation / 100.0) *
        Sigma0 / 0.015;

    const double beta = 2.0; // Bondi-like accretion

    /*
     * Boyuan Liu's Eq. 4
     * https://www.overleaf.com/project/632c916bd7878c40e2dc9191
     */
    const double mdot =
        0.25 * mdot_acc_sim * pow(accretor->mass/1.4, beta);

    return mdot;
}
