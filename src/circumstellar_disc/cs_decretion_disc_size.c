#include "../binary_c.h"
No_empty_translation_unit_warning;

double cs_decretion_disc_size(struct stardata_t * const stardata,
                              struct star_t * const donor Maybe_unused)
{
    /*
     * Estimate decretion disc size around donor star
     * in Rsun, according to Boyuan Liu's notes
     * https://www.overleaf.com/project/632c916bd7878c40e2dc9191
     */
    double Rout;
    if(stardata->preferences->decretion_disc_radius_algorithm == DECRETION_DISC_RADIUS_ZHANG_RIMULO)
    {
        /*
         * Zhang et al. (2004) ApJ 603, 663
         * Rimulo et al. (2018) MNRAS 476, 3555
         */
        const double ftrunc = cs_truncation_fraction(stardata,donor);
        Rout = stardata->common.orbit.separation * ftrunc;
    }
    else if(stardata->preferences->decretion_disc_radius_algorithm == DECRETION_DISC_RADIUS_COE_KIRK_2015)
    {
        /*
         * Alternatively, we can derive Rout from the empirical relation
         * a = (7e-12) Rout^2 + 0.4524 Rout + 4.3e10 (units : cm)
         * (with both a and Rout in metres) based on observations of Be-
         * XRBs in the SMC (Coe & Kirk 2015).
         *
         * Expand the above expression with Rout and a in units of Rsun
         *
         * 3.3876e10 * Rout^2 + 3.14716e10 Rout + (4.3e10 - 6.9566e10 * sep) = 0
         *
         * a = 3.3876e10
         * b = 3.14716e10
         * c = 4.3e10 - 6.9566e10 * sep
         *
         * solutions:
         *
         * Rout = [ -3.14716e10 \pm sqrt(9.9046e20 - 1.35504e11 * (4.3e10 - 6.9566e10 * sep) ]
         *         / 6.7752e10
         *
         * and only the positive solution is relevant.
         */

        Rout =
            ( -3.14716e10 + sqrt(9.9046e20 - 1.35504e11 * (4.3e10 - 6.9566e10 * stardata->common.orbit.separation) ) )
            / 6.7752e10 ;
    }
    else
    {
        Exit_binary_c(BINARY_C_ALGORITHM_BRANCH_FAILURE,
                      "Unknown decretion disc radius algorithm %d\n",
                      stardata->preferences->decretion_disc_radius_algorithm);
    }
    return Rout;
}
