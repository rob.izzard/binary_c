#include "../binary_c.h"
No_empty_translation_unit_warning;

/*
 * Return TRUE if the system is a Be-star system
 * FALSE otherwise according to Boyuan Liu's
 * notes.
 *
 * https://www.overleaf.com/project/632c916bd7878c40e2dc9191
 */

Boolean cs_Be_criterion(struct stardata_t * const stardata,
                        struct star_t * const donor,
                        struct star_t * const accretor)
{
    const Boolean vb = FALSE;
    /*
     * List of criteria using X macros with debugging information
     *
     * Each X macro has three arguments:
     * 1) the test : returns TRUE or FALSE
     * 2) format statement for debugging
     * 3) variables for the format statement (cannot be empty!)
     */
#define Be_CRITERIA                                                     \
    X(POST_SN_OBJECT(accretor->stellar_type),                           \
      "acc type %d",                                                    \
      accretor->stellar_type)                                           \
        X(ON_EITHER_MAIN_SEQUENCE(donor->stellar_type),                 \
          "donor type %d",                                              \
          donor->stellar_type)                                          \
        X(donor->mass > 3.0,                                            \
          "Mdonor %g",                                                  \
          donor->mass)                                                  \
        X(donor->v_eq_ratio > 0.7,                                      \
          "v/vkep %g",                                                  \
          donor->v_eq_ratio)                                            \
        X(donor->radius < donor->roche_radius * (1.0 - stardata->common.orbit.eccentricity), \
          "R=%g<RL=%g",                                                 \
          donor->radius,donor->roche_radius * (1.0 - stardata->common.orbit.eccentricity)) \
        X(stardata->common.orbit.period * YEAR_LENGTH_IN_DAYS > 7.0,    \
          "P=%g>7d",                                                    \
          stardata->common.orbit.period * YEAR_LENGTH_IN_DAYS)          \
        X(cs_decretion_disc_size(stardata,donor) > donor->roche_radius * (1.0 - stardata->common.orbit.eccentricity), \
          "Rdisc=%g>RL=%g",                                             \
          cs_decretion_disc_size(stardata,donor),donor->roche_radius * (1.0 - stardata->common.orbit.eccentricity))

    if(vb)
    {
        printf("Check : q = donor %g accretor %g : Rdisc = %g : RL = %g\n",
               donor->q,
               accretor->q,
               cs_decretion_disc_size(stardata,donor)/stardata->common.orbit.separation,
               donor->roche_radius/stardata->common.orbit.separation);
    }

    // cannot be true unless qdonor = Mdonor/Maccretor <~ 1.114

    /*
     * Make debugging format string
     */
#undef X
#define X(CRITERION,STRING,...) STRING " %s%s%s, "
    static const char * format_string = "%d Be? " Be_CRITERIA "%s";

    /*
     * Now define the X-macro to output debugging information
     * to the screen through printf
     */
#undef X
#define X(CRITERION,STRING,...)                 \
    __VA_ARGS__ ,                               \
        Colouryesno( CRITERION ),

    if(vb)
    {
        printf(
            format_string,
            donor->starnum,
            Be_CRITERIA
            "\n");
    }

#undef X
#define X(CRITERION,STRING,...) (CRITERION) &&
    return
        Be_CRITERIA TRUE;

#undef X
}
