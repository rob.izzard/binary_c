#include "../binary_c.h"
No_empty_translation_unit_warning;

int cs_Be_XRB_type(struct stardata_t * const stardata,
                   struct star_t * const donor,
                   struct star_t * const accretor)
{
    /*
     * Be X-ray burst type, assuming we're a Be-star system
     *
     * Be_XRAY_TYPE_I (ADAF)
     * Be_XRAY_TYPE_II (warped disc)
     *
     * according to Boyuan Liu's notes
     * https://www.overleaf.com/project/632c916bd7878c40e2dc9191
     */
    int type;
    const Boolean warp = cs_Be_tidal_warp_criterion(stardata,
                                                    donor,
                                                    accretor);
    const Boolean rate = cs_Be_accretion_rate_criterion(stardata,
                                                        accretor);

#undef X
#define X(TYPE) #TYPE,
    static const char * strings[] = {
        Be_XRAY_TYPES_LIST
    };
#undef X

    if(warp == TRUE
       &&
       rate == TRUE)
    {
        type = Be_XRAY_TYPE_II;
    }
    else
    {
        type = Be_XRAY_TYPE_I;
    }
    Dprint("type %s (warp? %s rate? %s)\n",
           strings[type],
           Yesno(warp),
           Yesno(rate));
    return type;
}
