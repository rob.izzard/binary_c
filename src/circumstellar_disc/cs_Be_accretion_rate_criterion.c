#include "../binary_c.h"
No_empty_translation_unit_warning;

Boolean cs_Be_accretion_rate_criterion(struct stardata_t * const stardata,
                                       struct star_t * const accretor)
{
    /*
     * Accretion rate criterion
     *
     * See Boyuan Liu's notes Eq. 9
     */
    const double eta = 0.05;
    const double epsilon = accretor->stellar_type == BLACK_HOLE ? 0.1 : 0.2;
    const double Mdot_acc = cs_peak_Be_accretion_rate(stardata, accretor);
    const double Mdot_edd = 1.5e-8 * eta * (0.2/epsilon) * (accretor->mass / 1.4);
    Dprint("Mdot acc %g >? Mdot edd %g\n",
           Mdot_acc,
           Mdot_edd);
    return Mdot_acc > Mdot_edd;
}
