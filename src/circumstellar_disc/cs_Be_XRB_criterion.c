#include "../binary_c.h"
No_empty_translation_unit_warning;


Boolean cs_Be_XRB_criterion(struct stardata_t * const stardata,
                            struct star_t * const donor,
                            struct star_t * const accretor)
{
    /*
     * Test if the system is a Be-X-ray binary
     */

    if(cs_Be_criterion(stardata,donor,accretor) == TRUE
       &&
       cs_Be_tidal_warp_criterion(stardata,donor,accretor) == TRUE
       &&
       cs_Be_accretion_rate_criterion(stardata,accretor) == TRUE
       &&
       cs_XRB_duty_cycle_criterion(stardata,donor,accretor) == TRUE
        )
    {
        return TRUE;
    }
    else
    {
        return FALSE;
    }
}
