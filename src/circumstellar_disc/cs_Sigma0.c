#include "../binary_c.h"
No_empty_translation_unit_warning;

double cs_Sigma0(struct stardata_t * const stardata,
                 struct star_t * const accretor)
{
    /*
     * There are two fits for Sigma0, the mass scaling
     * of the circumstellar disc, for the
     * Milky Way (Vieira et al. 2017 MNRAS 464, 3071)
     * and the Small Magellanic Cloud
     * (Rimulo et al. 2018 MNRAS 476, 3555)
     *
     * Result return in cgs units, g cm^-2
     *
     * See Boyuan Liu's notes
     * https://www.overleaf.com/project/632c916bd7878c40e2dc9191
     */
    const double logSigma0s[] =
        {
            1.03 * log10(accretor->mass) - 0.99, // SMC
            1.44 * log10(accretor->mass) - 2.37 // MW
        };

    const double Z[] =
        {
            0.004,
            0.02
        };

    const double fz =
        Limit_range(
            (stardata->common.effective_metallicity - Z[1]) / (Z[0] - Z[1]),
            0.0,
            1.0
            );

    const double logSigma0 =
        logSigma0s[0] + fz * (logSigma0s[1] - logSigma0s[0]);

    const double Sigma0 = exp10(logSigma0);

    return Sigma0;
}
