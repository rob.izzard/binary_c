#include "../binary_c.h"
No_empty_translation_unit_warning;

double cs_tidal_warp_semi_major_axis(struct stardata_t * const stardata,
                                     struct star_t * const donor,
                                     struct star_t * const accretor)
{
    /*
     * Tidal warp criterion for Be-star XRB
     *
     * Boyuan Liu's Eq. 8
     * https://www.overleaf.com/project/632c916bd7878c40e2dc9191
     *
     * returns a_crit in cm
     */
    const double n = 3.5;
    const double gamma = 2.0/(11.0 - 2.0*n);
    const double alpha = 0.63;

    const double Rdonor = donor->radius * R_SUN * r_rot(donor->omega/donor->omega_crit); // cm
    const double ftrunc = cs_truncation_fraction(stardata, accretor); // no units
    const double ee = Limit_range(stardata->common.orbit.eccentricity, 0.0, 1.0 - 1e-6);
    const double Teff_donor = Teff_from_star_struct(donor); // K
    const double Td = 0.6 * Teff_donor; // K
    const double vkep = 1e5 * donor->v_crit_eq; // cm/s
    const double cs = sqrt(2.0 * BOLTZMANN_CONSTANT * Td / M_PROTON); // cm/s
    const double Hstar = sqrt(2.0) * cs / vkep * Rdonor; // cm

    const double f1 = ftrunc * (1.0 - ee) * pow(1.0 - Pow2(ee), -1.5*gamma);
    const double f2 =
        3.0/2.0 *
        accretor->mass / donor->mass *
        pow(Rdonor, n - 0.5) /
        ( alpha * Pow2(Hstar) );

    const double acrit = pow(f1 * pow(f2,gamma), 1.0/(3.0*gamma-1.0));

    return acrit;
}
