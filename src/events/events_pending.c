#include "../binary_c.h"
No_empty_translation_unit_warning;


/*
 * Return TRUE if there is an event pending, FALSE otherwise
 */
Boolean Pure_function events_pending(struct stardata_t * Restrict const stardata)
{
    return stardata->common.n_events > 0 ? TRUE : FALSE;
}
