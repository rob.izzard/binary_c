#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "binary_c_events.h"

void catch_events(struct stardata_t * Restrict const stardata)
{
    /*
     * Catch system-wide events.
     *
     * The events are in a stack at stardata->common.events
     * and we handle them one by one in the order they are
     * stacked.
     *
     * Each has a handler function assigned to it, which is
     * called with stardata and the associated data (which may
     * be NULL) as arguments.
     *
     * Note: if we're on an intermediate solution step, we cannot
     * process events. Instead, we simply delete the event stack
     * and let the final step deal with it.
     */

    if(stardata->model.intermediate_step == FALSE)
    {
        /*
         * If wanted, copy stardata to pre_events_stardata
         * so it can be used for logging purposes (or otherwise).
         *
         * Note: we don't copy everything, as
         * stardata->pre_events_stardata->previous_stardata
         * would just be the same as
         * stardata->previous_stardata
         */
        if(stardata->preferences->save_pre_events_stardata == TRUE)
        {
            if(stardata->pre_events_stardata != NULL)
            {
                /* this should not happen! */
                free_stardata(&stardata->pre_events_stardata);
            }
            stardata->pre_events_stardata = new_stardata(stardata->preferences);
            copy_stardata(stardata,
                          stardata->pre_events_stardata,
                          COPY_STARDATA_PREVIOUS_NONE,
                          COPY_STARDATA_PERSISTENT_NONE);
            /* prevent recursion */
            stardata->pre_events_stardata->pre_events_stardata = NULL;
        }

        if(stardata->preferences->function_hooks[BINARY_C_HOOK_catch_events] != NULL)
        {
            /*
             * Use custom events function:
             * note, we set the preferences->function_hooks[BINARY_C_HOOK_catch_events]
             * pointer to NULL here so we can then call the default
             * function (catch_events) from the calling function
             * if required. We then set the function back.
             *
             * If we don't do this, we hit a race condition, oops!
             */
            void (*f)(struct stardata_t * stardata)
                = stardata->preferences->function_hooks[BINARY_C_HOOK_catch_events];
            stardata->preferences->function_hooks[BINARY_C_HOOK_catch_events] = NULL;
            f(stardata);
            stardata->preferences->function_hooks[BINARY_C_HOOK_catch_events] = f;
        }
        else
        {
            /*
             * Use binary_c's default event trigger handlers
             */
            call_default_triggers(stardata);
        }

        /*
         * Post-events sync
         */
        post_events_sync(stardata);
    }
    else
    {
        Eprint("Events cannot be caught on intermediate steps\n");
        erase_events(stardata);
    }
    return;
}
