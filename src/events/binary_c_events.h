#pragma once
#ifndef BINARY_C_EVENTS_H
#define BINARY_C_EVENTS_H

/*
 * Header file to be included in all events code functions
 */

/*
 * Eprint is the events debugging tool
 */

#define Eprint(...)                                     \
    if(stardata->preferences->event_logging == TRUE)    \
    {                                                   \
        fprintf(stdout,                                 \
                "EVENT [model %d] %s:%d : ",            \
                stardata->model.model_number,           \
                __func__ ,                              \
                __LINE__                                \
            );                                          \
        fprintf(stdout,                                 \
                __VA_ARGS__);                           \
        fflush(stdout);                                 \
    }

#undef Eprint
#define Eprint(...) /* do nothing */

#endif // BINARY_C_EVENTS_H
