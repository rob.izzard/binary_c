#pragma once
#ifndef EVENTS_PROTOTYPES_H
#define EVENTS_PROTOTYPES_H

void catch_events(struct stardata_t * Restrict const stardata);

void erase_events(struct stardata_t * Restrict const stardata);
void erase_event(struct stardata_t * Restrict const stardata,
                 struct binary_c_event_t ** Restrict const event);

int new_event(const char * Restrict const caller,
              struct stardata_t * const stardata,
              const Event_type type,
              Event_handler_function func(EVENT_HANDLER_ARGS),
              Event_handler_function erase_func(EVENT_HANDLER_ARGS),
              void * data,
              const Boolean unique);

Event_counter Pure_function events_pending_of_type(struct stardata_t * Restrict const stardata,
                                             const Event_type type);
Boolean Pure_function events_pending(struct stardata_t * Restrict const stardata);

char * events_stack_string(struct stardata_t * Restrict const stardata);

void erase_events_of_type(struct stardata_t * Restrict const stardata,
                          const Event_type type,
                          const struct binary_c_event_t * Restrict const except);
void shift_event_stack(struct stardata_t * Restrict const stardata,
                       const int n);

void trigger_event(struct stardata_t * const stardata,
                   const Event_counter i);
void events_replace_handler_functions(struct stardata_t * const stardata,
                                      const Event_type type,
                                      Event_handler_function func(EVENT_HANDLER_ARGS));

Event_handler_function generic_event_handler(
    void * const eventp Maybe_unused,
    struct stardata_t * const stardata,
    void * data Maybe_unused);

void call_default_triggers(struct stardata_t * Restrict const stardata);

void post_events_sync(struct stardata_t * Restrict const stardata);
void erase_events_except(struct stardata_t * Restrict const stardata,
                         const Event_counter not_this_one);
void events_clean_log_stack(struct stardata_t * const stardata);
struct binary_c_event_log_t * next_event_log(struct stardata_t * const stardata,
                                             ...);
#endif // EVENTS_PROTOTYPES_H
