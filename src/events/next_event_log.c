#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "binary_c_events.h"

/*
 * Return a pointer to the next event in the event-log stack
 * that matches one of the types passed in (in a va_list)
 * or NULL if none are found.
 */

struct binary_c_event_log_t * next_event_log(struct stardata_t * const stardata,
                                             ...)
{
    struct binary_c_event_log_t * elog = NULL;
    if(stardata->common.event_log_stack_n == 0)
    {
        return NULL;
    }
    else
    {
        va_list args;
        va_start(args,stardata);
        Event_type type;
        while((type = va_arg(args,Event_type)) &&
              type != BINARY_C_EVENT_NUMBER)
        {
            for(Event_counter n = 0; n < stardata->common.event_log_stack_n; n++)
            {
                elog = stardata->common.event_log_stack[n];
                if(elog->type == type &&
                   elog->processed == FALSE)
                {
                    elog->processed = TRUE;
                    return elog;
                }
            }
        }
    }
    return NULL;
}
