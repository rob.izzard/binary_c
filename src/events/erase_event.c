#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "binary_c_events.h"

/*
 * Erase the event at *eventp.
 *
 * eventp is a pointer to a pointer to the event structure.
 *
 * If its erase_func is set, call it.
 *
 * Then erase the event data and the event structure itself.
 *
 * This function does not change the event stack.
 *
 * NOTE:
 * You should not call this function directly: use
 * erase_events(), erase_events_of_type() or erase_events_except()
 * instead, otherwise the event stack will be very confused!
 */

void erase_event(struct stardata_t * Restrict const stardata,
                 struct binary_c_event_t ** Restrict const eventp)
{
    struct binary_c_event_t * event = eventp != NULL ? *eventp : NULL;

    Eprint("Erase event %p (from eventp = %p): erase_func = %p\n",
           (void*)event,
           (void*)eventp,
           (event ? Cast_function_pointer(event->erase_func) : NULL));

    if(eventp != NULL && event != NULL)
    {
        /*
         * If there is one, call the erase function
         */
        if(*event->erase_func != NULL)
        {
            Eprint("Call custom erase function\n");
            (*event->erase_func)(event,
                                 stardata,
                                 event->data);
        }

        /*
         * Free the data
         */
        Eprint("Free event data %p\n",(void*)event->data);
        Safe_free(event->data);

        /*
         * Free the event itself
         */
        Eprint("Free event %p\n",(void*)event);
        Safe_free(event);
    }
}
