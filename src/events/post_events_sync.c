#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "binary_c_events.h"

/*
 * After events are caught, this function is called
 * and it synchronizes things such as orbits, flags, etc.
 */
void post_events_sync(struct stardata_t * Restrict const stardata)
{

#ifdef ORBITING_OBJECTS
    /*
     * Update orbits
     */
    update_orbiting_objects(stardata);
#endif // ORBITING_OBJECTS

}
