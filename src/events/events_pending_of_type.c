#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "binary_c_events.h"

/*
 * Return number of events pending of given type,
 * 0 otherwise.
 */
Event_counter Pure_function events_pending_of_type(struct stardata_t * Restrict const stardata,
                                                   const Event_type type)
{
    Event_counter ret = 0;
    /*

        Eprint("check for events pending of type %d %s : n_events %u, events %p\n",
               type,
               Binary_c_event_string(type),
               stardata->common.n_events,
               (void*)stardata->common.events);
    */
    if(stardata->common.n_events > 0)
    {
        Event_counter i;
        for(i=0;i<stardata->common.n_events;i++)
        {
            struct binary_c_event_t * const ev = stardata->common.events[i];
            if(ev != NULL && ev->type == type)
            {
                ret++;
            }
        }
    }
    //Eprint("Found ? %u\n",ret);
    return ret;
}
