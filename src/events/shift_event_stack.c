#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "binary_c_events.h"

void shift_event_stack(struct stardata_t * stardata,
                       const int n)
{
    /*
     * Shift event stack by one, starting at n
     *
     * First, reduce number of events.
     */
    stardata->common.n_events--;

    if(stardata->common.n_events > 0)
    {
        /*
         * Move the stack's events.
         */
        Event_counter j;
        for(j=n; j<stardata->common.n_events; j++)
        {
            stardata->common.events[j] = stardata->common.events[j+1];
        }

        /*
         * Shrink the stack.
         */
        stardata->common.events = Realloc(
            stardata->common.events,
            stardata->common.n_events*
            sizeof(struct binary_c_event_t*)
            );
    }
    else
    {
        /*
         * Empty the stack
         */
        Eprint("Free the event stack\n");
        Safe_free(stardata->common.events);
    }
}
