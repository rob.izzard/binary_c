#pragma once
#ifndef EVENTS_MACROS_H
#define EVENTS_MACROS_H

#include "events.def"

#undef X
#define X(CODE,NUM,STRING,UNIQUE) BINARY_C_EVENT_##CODE = (NUM),
enum {
    BINARY_C_EVENTS_LIST
};
#undef X

#define X(CODE,NUM,STRING,UNIQUE) STRING,
static const char * const events_strings[] Maybe_unused = {
    BINARY_C_EVENTS_LIST
};
#undef X

#define X(CODE,NUM,STRING,UNIQUE) UNIQUE,
static const Boolean unique_event[] = {
    BINARY_C_EVENTS_LIST
};
#undef X

#define Binary_c_event_string(N) (events_strings[(N)])

/*
 * Macros for unique and non-unique events
 */
#define UNIQUE_EVENT (TRUE)
#define NORMAL_EVENT (FALSE)

/*
 * Add event log data
 *
 * TYPE is the event type (see events.def)
 * LABEL is an integer of your choice to act as a label
 * DATA is a pointer to data of your choice (which is freed automatically)
 * FREEFUNC is, if not NULL, a function pointer used to free your data
 *  [ Takes a void ** pointer in ]
 * You can use FREEFUNC to free a star_t or stardata_t struct by
 * setting it to &free_star or &free_stardata.
 */
#define Add_event_log_data(TYPE,                                        \
                           LABEL,                                       \
                           DATA,                                        \
                           FREEFUNC)                                    \
    {                                                                   \
        stardata->common.event_log_stack =                              \
            Realloc(stardata->common.event_log_stack,                   \
                    sizeof(struct binary_c_event_log_t *) *             \
                    (stardata->common.event_log_stack_n+1));            \
        struct binary_c_event_log_t * const __e =                       \
            stardata->common.event_log_stack[stardata->common.event_log_stack_n] = \
            Malloc(sizeof(struct binary_c_event_log_t));                \
        __e->data = (void*)(DATA);                                      \
        __e->freefunc = (FREEFUNC);                                     \
        __e->label = (int)(LABEL);                                      \
        __e->processed = FALSE;                                         \
        __e->type = (TYPE);                                             \
        stardata->common.event_log_stack_n++;                           \
    }

/*
 * Macro to get the next event log data of
 * a type given in the arguments
 */
#define Next_event_log(...) (                   \
        next_event_log(stardata,                \
                       __VA_ARGS__,             \
                       BINARY_C_EVENT_NUMBER)   \
        )

#endif // EVENTS_MACROS_H
