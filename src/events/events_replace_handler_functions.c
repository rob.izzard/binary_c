#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "binary_c_events.h"

void events_replace_handler_functions(struct stardata_t * const stardata,
                                      const Event_type type,
                                      Event_handler_function func(EVENT_HANDLER_ARGS))
{
    /*
     * Replace the handler functions of events of the given type
     * (or all events if type == BINARY_C_EVENT_ALL) with given
     * func.
     */
    unsigned int i;
    for(i=0; i<stardata->common.n_events; i++)
    {
        if(type == BINARY_C_EVENT_ALL ||
           stardata->common.events[i]->type == type)
        {
            stardata->common.events[i]->func = func;
        }
    }
}
