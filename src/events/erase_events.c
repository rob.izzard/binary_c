#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "binary_c_events.h"
void erase_events(struct stardata_t * Restrict const stardata)
{
    /*
     * Erase any pending events with or without triggering them,
     * and free the events stack.
     *
     * If they are triggered, they are sent EVENT_TRIGGER_ACTION_ERASE
     * so they know they shouldn't really do anything.
     */
    Eprint("Erase all events\n");

    while(stardata->common.n_events > 0)
    {
        /*
         * Reduce the counter
         */
        stardata->common.n_events--;

        /*
         * Erase the event
         */
        Eprint("call erase_event %d\n",(int)stardata->common.n_events);
        erase_event(stardata,
                    &stardata->common.events[stardata->common.n_events]);
    }

    /*
     * Free the stack
     */
    Safe_free(stardata->common.events);

}
