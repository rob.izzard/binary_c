#pragma once
#ifndef BINARY_C_STELLAR_CORES_H
#define BINARY_C_STELLAR_CORES_H

/*
 * The binary_c stellar population nucleosynthesis framework.
 *
 * Copyright Robert Izzard 2000-2025
 *
 * Contact rob.izzard@gmail.com
 *
 * Code repository:
 * https://gitlab.com/binary_c
 *
 * Documentation is in doc/
 * Code is in src/
 *
 * To build, run
 *
 * ---
 *
 * meson setup builddir
 * cd builddir
 * ninja binary_c_install
 *
 * ---
 *
 * Please see the files README, LICENCE and CHANGES,
 * and the doc/ directory for documentation which is mirrored
 * at https://binary_c.gitlab.io
 *
 **********************
 *
 * Purpose:
 *
 * Macros to handle the stellar cores
 *
 * In the original BSE, and hence binary_c, code, there was
 * just one variable to handle the core. The type of this core then
 * dependend on the stellar type.
 *
 * Now, instead we can have multiple cores in each star,
 * with known types based on their composition and each with
 * its own mass. This is more flexible and (hopefully) better.
 */


#include "binary_c_stellar_cores.def"

/*
 * Define core enumerated types. Note that CORE_NONE must be -1
 * so it's not looped over when, e.g., merging cores.
 */
#undef X
#define X(CODE,LONG_LABEL,SHORT_LABEL,DERIVATIVE_LABEL) CODE,
enum { BINARY_C_STELLAR_CORE_LIST NUMBER_OF_CORES, CORE_NONE = -1 };

/*
 * Stellar core labels
 */
#undef X
#define X(CODE,LONG_LABEL,SHORT_LABEL,DERIVATIVE_LABEL) #LONG_LABEL,
static char * binary_c_long_core_strings[] Maybe_unused = { BINARY_C_STELLAR_CORE_LIST "None" };

#undef X
#define X(CODE,LONG_LABEL,SHORT_LABEL,DERIVATIVE_LABEL) #SHORT_LABEL,
static char * binary_c_short_core_strings[] Maybe_unused = { BINARY_C_STELLAR_CORE_LIST "None" };
#undef X

#define Core_string(TYPE) ((TYPE)==CORE_NONE ? "None" : binary_c_long_core_strings[(TYPE)])
#define Core_string_short(TYPE) ((TYPE)==CORE_NONE ? "None" : binary_c_short_core_strings[(TYPE)])

/*
 * Mass of a core that does not exist
 */
#define UNDEFINED_CORE -1.0


/*
 * macro function to determine if there is a core
 * of a given type
 */
#define Core_exists(STAR,TYPE)                          \
    (((STAR)->core_mass[(TYPE)] > TINY) ? TRUE : FALSE)


/*
 * The outermost core mass and type
 */
#define Outermost_core_mass(STAR)                                       \
    (                                                                   \
        Core_exists((STAR),(CORE_He)) ? (STAR)->core_mass[CORE_He] :    \
        Core_exists((STAR),(CORE_CO)) ? (STAR)->core_mass[CORE_CO] :    \
        Core_exists((STAR),(CORE_ONe)) ? (STAR)->core_mass[CORE_ONe] :  \
        Core_exists((STAR),(CORE_Si)) ? (STAR)->core_mass[CORE_Si] :    \
        Core_exists((STAR),(CORE_Fe)) ? (STAR)->core_mass[CORE_Fe] :    \
        Core_exists((STAR),(CORE_NEUTRON)) ? (STAR)->core_mass[CORE_NEUTRON] : \
        Core_exists((STAR),(CORE_BLACK_HOLE)) ? (STAR)->core_mass[CORE_BLACK_HOLE] : \
        0.0                                                             \
        )

/*
 * Ditto for baryonic mass
 */
#define Outermost_baryonic_core_mass(STAR)                              \
    (                                                                   \
        POST_SN_OBJECT((STAR)->stellar_type) ?                          \
        (Outermost_core_mass(STAR) + ((STAR)->baryonic_mass + (STAR)->mass)) : \
        Outermost_core_mass(STAR)                                       \
        )

#define Outermost_core_mass_pointer(STAR)                               \
    (                                                                   \
    Core_exists((STAR),(CORE_He)) ? &(STAR)->core_mass[CORE_He] :       \
    Core_exists((STAR),(CORE_CO)) ? &(STAR)->core_mass[CORE_CO] :       \
    Core_exists((STAR),(CORE_ONe)) ? &(STAR)->core_mass[CORE_ONe] :     \
    Core_exists((STAR),(CORE_Si)) ? &(STAR)->core_mass[CORE_Si] :       \
    Core_exists((STAR),(CORE_Fe)) ? &(STAR)->core_mass[CORE_Fe] :       \
    Core_exists((STAR),(CORE_NEUTRON)) ? &(STAR)->core_mass[CORE_NEUTRON] : \
    Core_exists((STAR),(CORE_BLACK_HOLE)) ? &(STAR)->core_mass[CORE_BLACK_HOLE] : \
    NULL                                                                \
        )

#define Outermost_core_type(STAR)                                   \
    (                                                               \
        Core_exists((STAR),(CORE_He)) ? CORE_He :                   \
        Core_exists((STAR),(CORE_CO)) ? CORE_CO :                   \
        Core_exists((STAR),(CORE_ONe)) ? CORE_ONe :                 \
        Core_exists((STAR),(CORE_Si)) ? CORE_Si :                   \
        Core_exists((STAR),(CORE_Fe)) ? CORE_Fe :                   \
        Core_exists((STAR),(CORE_NEUTRON)) ? CORE_NEUTRON :         \
        Core_exists((STAR),(CORE_BLACK_HOLE)) ? CORE_BLACK_HOLE :   \
        CORE_NONE                                                   \
        )

/*
 * Map stellar type to outermost core type so we can use
 * the old-fashioned functions.
 */
#define ID_core(STELLAR_TYPE)                               \
    (                                                       \
        (STELLAR_TYPE) <= MAIN_SEQUENCE ? CORE_NONE :       \
        (STELLAR_TYPE) <= GIANT_BRANCH ? CORE_He :          \
        (STELLAR_TYPE) <= TPAGB ? CORE_He :                 \
        (STELLAR_TYPE) == HeMS ? CORE_NONE :                \
        (STELLAR_TYPE) <= HeGB ? CORE_CO :                  \
        (STELLAR_TYPE) == HeWD ? CORE_He :                  \
        (STELLAR_TYPE) == COWD ? CORE_CO :                  \
        (STELLAR_TYPE) == ONeWD ? CORE_ONe :                \
        (STELLAR_TYPE) == NEUTRON_STAR ? CORE_NEUTRON :     \
        (STELLAR_TYPE) == BLACK_HOLE ? CORE_BLACK_HOLE :    \
        CORE_NONE                                           \
        )

#define Copy_core_masses(FROM,TO)               \
    memcpy(                                     \
        (TO)->core_mass,                        \
        (FROM)->core_mass,                      \
        sizeof(double)*NUMBER_OF_CORES          \
        );


/*
 * Mass of the star that has completely burnt hydrogen
 *
 * Main sequence stars have "no" hydrogen burnt,
 * while for all other stars this is either the
 * helium core mass or the total mass.
 */
#define Hydrogen_burnt_core_mass(STAR)          \
    (                                           \
        (STAR)->stellar_type <= MAIN_SEQUENCE ?\
        0.0 :                                   \
                                                \
        (STAR)->stellar_type <= TPAGB ?         \
        (STAR)->core_mass[CORE_He] :            \
                                                \
        (STAR)->mass                            \
        )

/*
 * Mass of the star that has completely
 * burnt helium.
 *
 * In stars burning helium, or made entirely
 * of helium, this is zero.
 *
 * In stars with a CO core, this is the CO
 * core mass.
 *
 * In neutron stars and black holes, this is
 * the total mass.
 */
#define Helium_burnt_core_mass(STAR)            \
    (                                           \
        (                                       \
            (STAR)->stellar_type <= CHeB ||     \
            (STAR)->stellar_type == HeMS ||     \
            (STAR)->stellar_type == HeWD        \
            ) ?                                 \
        0.0 :                                   \
                                                \
        (                                       \
            (STAR)->stellar_type == EAGB ||     \
            (STAR)->stellar_type == TPAGB ||    \
            (STAR)->stellar_type == HeHG ||     \
            (STAR)->stellar_type == HeGB        \
            ) ?                                 \
        (STAR)->core_mass[CORE_CO] :            \
                                                \
        (STAR)->mass                            \
        )

#endif // BINARY_C_STELLAR_CORES_H
