#include "binary_c_code_options.h"
/*
 * The binary_c stellar population nucleosynthesis framework.
 *
 * Copyright Robert Izzard 2000-2025
 *
 * Contact rob.izzard@gmail.com
 *
 * Code repository:
 * https://gitlab.com/binary_c
 *
 * Documentation is in doc/
 * Code is in src/
 *
 * To build, run
 *
 * ---
 *
 * meson setup builddir
 * cd builddir
 * ninja binary_c_install
 *
 * ---
 *
 * Please see the files README, LICENCE and CHANGES,
 * and the doc/ directory for documentation which is mirrored
 * at https://binary_c.gitlab.io
 *
 **********************
 *
 * Purpose:
 *
 * This file contains the exit function prototypes.
 *
 * If you define __HAVE_STARDATA_T then the
 * exit functions that require stardata are also included.
 *
 * This option is provided so that you can include
 * the exit prototypes from code locations that lack
 * stardata. If you include binary_c.h (as is recommended
 * from general code) then you'll always have __HAVE_STARDATA_T
 * defined by binary_c_structures.h
 */

#ifdef __HAVE_STARDATA_T
void No_return
binary_c_API_function
exit_binary_c_with_stardata(struct stardata_t * stardata,
                            const char * const filename,
                            const int fileline,
                            const int errsv Maybe_unused,
                            const int binary_c_error_code,
                            const char * const format,
                            ...) Gnu_format_args(6,7);

void No_return
binary_c_API_function
vexit_binary_c_with_stardata(struct stardata_t * stardata,
                             const char * const filename,
                             const int fileline,
                             const int errsv Maybe_unused,
                             const int binary_c_error_code,
                             const char * const pre_format,
                             va_list pre_args,
                             const char * const format,
                             ...) Gnu_format_args(6,0) Gnu_format_args(8,9);
#endif//__HAVE_STARDATA_T

void No_return
binary_c_API_function
exit_binary_c_no_stardata(const char * const filename,
                          const int fileline,
                          const int errsv Maybe_unused,
                          const int binary_c_error_code,
                          const char * const format,
                          ...) Gnu_format_args(5,6);



void No_return
binary_c_API_function
vexit_binary_c_no_stardata(const char * const filename,
                           const int fileline,
                           const int errsv Maybe_unused,
                           const int binary_c_error_code,
                           const char * const format,
                           ...) Gnu_format_args(5,6);
