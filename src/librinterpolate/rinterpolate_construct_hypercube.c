#define __BINARY_C_LINT_SKIP

#include "../binary_c_code_options.h"
#include "../binary_c_error_codes.h"
#include "../binary_c_exit_macros.h"
#include "../breakpoints/breakpoints_prototypes.h"
#include "../binary_c_exit_prototypes.h"

#ifndef __HAVE_LIBRINTERPOLATE__

#include "rinterpolate.h"
#include "rinterpolate_internal.h"

void rinterpolate_construct_hypercube(struct rinterpolate_data_t * const rinterpolate_data MAYBE_UNUSED,
                                      struct rinterpolate_table_t * RESTRICT const table)
{
    /*
     * Construct hypercube
     */
    Rinterpolate_print("construct hypercube for %p\n",(void*)table);
    struct rinterpolate_hypertable_t * const hypertable = table->hypertable;

    /* easily vectorized loop */
    rinterpolate_counter_t i;
    for(i=0;i<table->hypertable_length;i++)
    {
        Rinterpolate_print("SUM %u was %u now ",i,hypertable->sum[i]);
        hypertable->sum[i] *= table->line_length;
        Rinterpolate_print("%u (lnl = %u)\n",hypertable->sum[i],table->line_length);
    }

    rinterpolate_counter_t k = 0;
    for(i=0;i<table->hypertable_length;i++)
    {
#ifdef RINTERPOLATE_DEBUG
        Rinterpolate_print("memcpy k=%u i=%u : from %p to %p ( = %p + %u )\n",
                           k,
                           i,
                           (void*)(hypertable->data + k),
                           (void*)(table + hypertable->sum[i]),
                           (void*)table,
                           hypertable->sum[i]
            );
#endif//RINTERPOLATE_DEBUG

        memcpy(hypertable->data + k,
               table->data + hypertable->sum[i],
               table->line_length_sizeof);

        k += table->line_length;

#ifdef RINTERPOLATE_DEBUG
        {
            Rinterpolate_print("Line %u : ",i);FLUSH;
            rinterpolate_counter_t j;
            for(j=0;j<table->n;j++)
            {
                Rinterpolate_print("% 3.3e ",*(hypertable->data+i*table->line_length+j));FLUSH;
            }
            Rinterpolate_print(" | ");FLUSH;
            for(j=table->n;j<table->line_length;j++)
            {
                Rinterpolate_print("% 3.3e ",*(hypertable->data+i*table->line_length+j));FLUSH;
            }
            Rinterpolate_print(" %u/%u\n",i,table->hypertable_length-1);FLUSH;
        }
#endif

    }

#ifdef RINTERPOLATE_DEBUG
    {
        Rinterpolate_print("done hypertable\n");
        Rinterpolate_print("Interpolation (f) factors: ");
        rinterpolate_counter_t j;
        for(j=0;j<table->n;j++)
        {
            Rinterpolate_print("% 3.3e ",hypertable->f[j]);
        }
        Rinterpolate_print("\n");
    }
#endif
}

#endif // __HAVE_LIBRINTERPOLATE__

typedef int prevent_ISO_C_warning;

        