#define __BINARY_C_LINT_SKIP
#define _GNU_SOURCE

#include "../binary_c_code_options.h"
#include "../binary_c_error_codes.h"
#include "../binary_c_exit_macros.h"
#include "../breakpoints/breakpoints_prototypes.h"
#include "../binary_c_exit_prototypes.h"

#ifndef __HAVE_LIBRINTERPOLATE__


#include <stdlib.h>

#include "rinterpolate.h"
#include "rinterpolate_internal.h"

#ifdef RINTERPOLATE_HAVE_GNU_QSORT_R
static int __cmpfunc(const void * const a,
                     const void * const b,
                     void * const arg);
#endif
#ifdef RINTERPOLATE_HAVE_BSD_QSORT_R
static int __cmpfunc(void * const arg,
                     const void * const a,
                     const void * const b);
#endif // __HAVE_BSD_QSORT_R


struct rinterpolate_table_t *
rinterpolate_sort_table(struct rinterpolate_data_t * RESTRICT const rinterpolate_data,
                        struct rinterpolate_table_t * const in)
{
    /*
     * Function to sort the table (in) and return a pointer
     * to the sorted table (out).
     *
     * If rinterpolate_data is non-NULL, allocate space
     * for the sorted table in rinterpolate_data.
     */

    const rinterpolate_counter_t nd = in->n + in->d;
    const size_t size = (size_t)nd * sizeof(rinterpolate_float_t);

    /*
     * Make sure the input table is analyzed
     */
    rinterpolate_analyse_table(rinterpolate_data,
                               in,
                               "sort 2 in");

    /*
     * Make a new table which is a copy of in
     */
    struct rinterpolate_table_t * const out =
        rinterpolate_copy_table(rinterpolate_data,
                                in);

    /*
     * Sort using qsort
     */
    qsort_r(out->data,
            out->l,
            size,
#ifdef RINTERPOLATE_HAVE_GNU_QSORT_R
            __cmpfunc,
            (void*)in
#endif
#ifdef RINTERPOLATE_HAVE_BSD_QSORT_R
            (void*)in,
            __cmpfunc
#endif
        );

    return out;
}


/*
 * Line comparison function for qsort_r
 */
#ifdef RINTERPOLATE_HAVE_GNU_QSORT_R
static int __cmpfunc(const void * const a,
                     const void * const b,
                     void * const arg)
#endif
#ifdef RINTERPOLATE_HAVE_BSD_QSORT_R
static int __cmpfunc(void * const arg,
                     const void * const a,
                     const void * const b)
#endif
{
    const struct rinterpolate_table_t * const table =
        (struct rinterpolate_table_t const *)arg;
    const rinterpolate_float_t * const xa = (rinterpolate_float_t const *)a;
    const rinterpolate_float_t * const xb = (rinterpolate_float_t const *)b;

    /* xa and xb are two lines in the table, with n parameters to compare */

    /* prefetch is marginally faster */
    prefetch(xa);
    prefetch(xb);

    rinterpolate_counter_t i = 0;
    while(i<table->n)
    {
        const rinterpolate_float_t x = xa[i];
        const rinterpolate_float_t y = xb[i];
        if(unlikely(Fequal(x,y)))
        {
            ++i;
        }
        else
        {
            return x > y ? 1 : -1;
        }
    }
    return 0; /* fallback */
}

#endif // __HAVE_LIBRINTERPOLATE__

typedef int prevent_ISO_C_warning;

        