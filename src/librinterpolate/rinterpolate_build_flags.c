#define __BINARY_C_LINT_SKIP

#include "../binary_c_code_options.h"
#include "../binary_c_error_codes.h"
#include "../binary_c_exit_macros.h"
#include "../breakpoints/breakpoints_prototypes.h"
#include "../binary_c_exit_prototypes.h"

#ifndef __HAVE_LIBRINTERPOLATE__

#ifdef __RINTERPOLATE_BUILD_BUILD_FLAGS__

#include "rinterpolate.h"
#include "rinterpolate_internal.h"
#include <stdarg.h>
#include <string.h>

/*
 * Macro to convert a macro to a string
 */
#ifndef Stringify
#define Stringify(item) "" #item
#endif

/*
 * Macro to expand a macro and then convert to a string
 */
#ifndef Stringify_macro
#define Stringify_macro(item) (Stringify(item))
#endif

#ifndef Macrotest
/*
 * Macro to test a macro
 */
#define Macrotest(macro)                                                \
    printf("%s is %s\n",                                                \
           "" #macro,                                                   \
           (strcmp("" #macro,                                           \
                   Stringify(macro)) ? "on" : "off" ));
#endif

void rinterpolate_build_flags(struct rinterpolate_data_t * RESTRICT const rinterpolate_data MAYBE_UNUSED)
{
    /*
     * Test build flags
     */
    Macrotest(RINTERPOLATE_USE_REALLOC);
    Macrotest(RINTERPOLATE_USE_POINTER_ARITHMETIC);
    Macrotest(RINTERPOLATE_POINTER_ARITHMETIC_J_LOOP);
    Macrotest(RINTERPOLATE_DEBUG);
    Macrotest(RINTERPOLATE_DEBUG_SHOW_TABLE);
    Macrotest(RINTERPOLATE_CACHE);
    Macrotest(RINTERPOLATE_CACHE_USE_MEMCPY);
    Macrotest(RINTERPOLATE_CACHE_USE_MEMCMP);
}

#endif

typedef int prevent_ISO_C_warning;

#endif // __HAVE_LIBRINTERPOLATE__

typedef int prevent_ISO_C_warning;

        