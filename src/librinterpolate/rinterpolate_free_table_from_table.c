#define __BINARY_C_LINT_SKIP

#include "../binary_c_code_options.h"
#include "../binary_c_error_codes.h"
#include "../binary_c_exit_macros.h"
#include "../breakpoints/breakpoints_prototypes.h"
#include "../binary_c_exit_prototypes.h"

#ifndef __HAVE_LIBRINTERPOLATE__

#include "rinterpolate.h"
#include "rinterpolate_internal.h"

/*
 * Given a pointer to a table, find it in rinterpolate_data
 * and free it if it is there.
 *
 * Nested min_max tables are also freed.
 */

void rinterpolate_free_table_from_table(
    struct rinterpolate_data_t * RESTRICT const rinterpolate_data,
    struct rinterpolate_table_t * const table)
{

    if(rinterpolate_data != NULL &&
       table != NULL)
    {
        for(rinterpolate_counter_t i=0;
            i < rinterpolate_data->number_of_interpolation_tables;
            i++)
        {
            struct rinterpolate_table_t * t = rinterpolate_data->tables[i];
            if(likely(t != NULL) &&
               unlikely(t == table))
            {
                if(t->min_max_table != NULL)
                {
                    for(rinterpolate_counter_t j=0; j<t->n; j++)
                    {
                        if(t->min_max_table[j] != NULL)
                        {
                            rinterpolate_free_table_contents(t->min_max_table[j]);
                            Safe_free(t->min_max_table[j]);
                        }
                    }
                }
                rinterpolate_free_table_contents(t);
                Safe_free(rinterpolate_data->tables[i]);
            }
        }
    }
}

#endif // __HAVE_LIBRINTERPOLATE__

typedef int prevent_ISO_C_warning;

        