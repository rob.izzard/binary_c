#!/usr/bin/env perl
use rob_misc;
use strict;

# script to sync the latest librinterpolate to binary_c

my $src = "$ENV{HOME}/git/librinterpolate/src/";
my $dest = '.';

`rsync -avP $src/*.c ./`;
`rsync -avP $src/*.h ./`;
`rsync -avP CHANGELOG LICENCE ./`;
unlink 'test_rinterpolate.c';
unlink 'rinterpolate-config.c';

foreach my $cfile (`ls *.c`)
{
    chomp $cfile;
    my $code = slurp($cfile);
    my $preheader = '';
    if($code=~s/\#define\s+_GNU_SOURCE//)
    {
        $preheader = "#define _GNU_SOURCE\n";
    }

    $code = '#define __BINARY_C_LINT_SKIP
'.$preheader."\n".'#include "../binary_c_code_options.h"
#include "../binary_c_error_codes.h"
#include "../binary_c_exit_macros.h"
#include "../breakpoints/breakpoints_prototypes.h"
#include "../binary_c_exit_prototypes.h"

#ifndef __HAVE_LIBRINTERPOLATE__

' . $code . '
#endif // __HAVE_LIBRINTERPOLATE__

typedef int prevent_ISO_C_warning;

        ';

    $code =~s/exit\((\(int\))?errnum\)/Exit_binary_c_no_stardata(BINARY_C_IMPORTED_CODE_ERROR,\"Error in librinterpolate in binary_c\")/g;

    dumpfile($cfile,$code);

}
