#define __BINARY_C_LINT_SKIP

#include "../binary_c_code_options.h"
#include "../binary_c_error_codes.h"
#include "../binary_c_exit_macros.h"
#include "../breakpoints/breakpoints_prototypes.h"
#include "../binary_c_exit_prototypes.h"

#ifndef __HAVE_LIBRINTERPOLATE__

#include "rinterpolate.h"
#include "rinterpolate_internal.h"

void rinterpolate_show_table(struct rinterpolate_data_t * const rinterpolate_data,
                             struct rinterpolate_table_t * const table,
                             FILE * const stream,
                             const rinterpolate_signed_counter_t nlines)
{
    /*
     * Show table contents to appropriate stream,
     * if nlines >0 show only the first nlines
     */
    if(stream == NULL) return;

    const rinterpolate_counter_t nd =
        table->n + table->d;

    const rinterpolate_counter_t showlines =
        nlines > 0 ? (rinterpolate_counter_t)nlines : table->l;

    /*
     * Count chars required for break "*****" lines
     */
    unsigned int count = 6 + 3 + 4 + 4 + 4 + 3 + 13 * (table->n + table->d) + 3;
    for(rinterpolate_counter_t j=0;j<table->n;j++)
    {
        if(table->column_is_mapped[j])
        {
            count++;
        }
    }

#define __tablebreak                                                    \
    {for(unsigned int break_i = 0; break_i<count; break_i++) { printf("*!"); } printf("\n");}


    __tablebreak;

    fprintf(stream,
            "Table %p ",
            (void*)table);

    if(table != NULL)
    {
        if(rinterpolate_data != NULL)
        {
            const int index = rinterpolate_table_index(rinterpolate_data,
                                                       table->data,
                                                       NULL);
            fprintf(stream,
                    "index %d ",
                    index);
        }

        fprintf(stream,
                "stats %p analysed? %s : n %d d %d l %d\n",
                (void*)table->stats ? (void*)table->stats : NULL,
                table->analysed ? "y" : "n",
                (int)table->n,
                (int)table->d,
                (int)table->l);

        if(table->stats != NULL)
        {
            fprintf(stream,
                    "  min %p max %p nvalues %p valuelist %p : orthogonal %s",
                    (void*)table->stats->min,
                    (void*)table->stats->max,
                    (void*)table->stats->nvalues,
                    (void*)table->stats->valuelist,
                    rinterpolate_is_table_orthogonal(rinterpolate_data,table,FALSE) ? "Y" : "N");
        }
        fprintf(stream,"\n");

        if(table->stats != NULL &&
           table->analysed == TRUE)
        {
            for(rinterpolate_counter_t j=0;j<table->n;j++)
            {
                fprintf(stream,
                        "Col %u : ",
                        j);
                if(table->stats != NULL)
                {
                    fprintf(stream,
                            "min=%12.6g max=%12.6g : %4u values -> ",
                            table->stats->min[j],
                            table->stats->max[j],
                            table->stats->nvalues[j]
                        );
                    for(rinterpolate_counter_t i=0;i<table->stats->nvalues[j];i++)
                    {
                        fprintf(stream,
                                "%12g ",
                                table->stats->valuelist[j][i]);
                    }
                }

                fprintf(stream,"mapped? %c",
                        table->column_is_mapped[j] ? 'Y' : 'N'
                    );

                if(table->column_is_mapped[j] == TRUE)
                {
                    fprintf(stream,
                            " at %p ",
                            (void*)table->min_max_table[j]);
                }

                fprintf(stream,"\n");
            }
        }

        __tablebreak;

        for(rinterpolate_counter_t i=0; i<showlines; i++)
        {
            const rinterpolate_counter_t offset = i * nd;
            const rinterpolate_float_t * const x = table->data + offset;

            fprintf(stream,"%6.2f %% , %4u (offset %4u) : ",
                    100.0*(float)i/(float)table->l,
                    i,
                    offset);

            for(rinterpolate_counter_t j=0;j<table->n;j++)
            {
                fprintf(stream,"%12g ",*(x+j));
                if(table->column_is_mapped[j])
                {
                    fprintf(stream, "⊳");

                    if(1)
                    {
                        double minmax[2];
                        const struct rinterpolate_table_t * const t = table->min_max_table[j];
                        rinterpolate(t->data,
                                     rinterpolate_data,
                                     t->n,
                                     t->d,
                                     t->l,
                                     x,
                                     minmax,
                                     0);
                        rinterpolate_float_t y = (minmax[1]-minmax[0]) * x[j] + minmax[0];
                        fprintf(stream,"%12g ",y);
                    }
                }
                if(table->n>1 && j<table->n-1) fprintf(stream,",");
            }
            fprintf(stream,": ");
            for(rinterpolate_counter_t j=table->n;j<nd;j++)
            {
                fprintf(stream,"%12g ",*(x + j));
            }
            fprintf(stream,"\n");
        }

        if(showlines < table->l)
        {
            fprintf(stream,"... %u more lines not shown ...\n",table->l - showlines);
        }
    }
    else
    {
        fprintf(stream,
                "Table contains no data (is NULL)\n");
    }

    __tablebreak;
}

#endif // __HAVE_LIBRINTERPOLATE__

typedef int prevent_ISO_C_warning;

        