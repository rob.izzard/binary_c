#pragma once
#ifndef RINTERPOLATE_STRUCTURES_H
#define RINTERPOLATE_STRUCTURES_H



/************************************************************
 * librinterpolate's structures
 ************************************************************/

struct rinterpolate_vector_t {
    rinterpolate_counter_t n;
    rinterpolate_float_t * data;
};

struct rinterpolate_hypertable_t {
    struct rinterpolate_table_t * table;
    rinterpolate_float_t * data;
    rinterpolate_float_t * f;
    rinterpolate_counter_t  * sum;
#ifdef RINTERPOLATE_USE_REALLOC
    size_t RINTERPOLATE_ALLOCD;
#endif
};

struct rinterpolate_table_t {
    struct rinterpolate_data_t * parent;
    struct rinterpolate_hypertable_t * hypertable;
    struct rinterpolate_stats_t * stats;
    struct rinterpolate_stats_t * parent_stats;
    rinterpolate_float_t * data;

#ifdef RINTERPOLATE_CACHE
    rinterpolate_float_t * RESTRICT cache;
    rinterpolate_counter_t cache_match_line;
    rinterpolate_signed_counter_t cache_spin_line;
#endif
    rinterpolate_counter_t * RESTRICT steps;
    rinterpolate_float_t ** RESTRICT presearch;
    rinterpolate_counter_t  presearch_n;
    size_t d_float_sizeof;
    size_t n_float_sizeof;
    size_t line_length_sizeof;
    size_t sum_sizeof;
    rinterpolate_counter_t * RESTRICT varcount;
    rinterpolate_counter_t n;
    rinterpolate_counter_t d;
    rinterpolate_counter_t l;
    rinterpolate_counter_t line_length;
    rinterpolate_counter_t hypertable_length;
    rinterpolate_counter_t table_number;
#ifndef RINTERPOLATE_PRESEARCH
    rinterpolate_counter_t g;
#endif
#ifdef RINTERPOLATE_CACHE
    rinterpolate_counter_t cache_length;
#endif
    rinterpolate_Boolean_t * column_is_mapped;
    rinterpolate_Boolean_t auto_free_data;
    rinterpolate_Boolean_t analysed;
    struct rinterpolate_table_t ** min_max_table;
    char * label;
};


struct rinterpolate_data_t {
    struct rinterpolate_table_t  *  * RESTRICT tables;
    rinterpolate_counter_t number_of_interpolation_tables;
    void (*errfunc)(void *data);
    void * errdata;
    int (*error_handler)(void *,
                         const int ,
                         const char * const ,
                         va_list );
};

struct rinterpolate_stats_t {
    rinterpolate_float_t ** valuelist;
    rinterpolate_counter_t * valuelist_alloced;
    rinterpolate_float_t ** valuemax;
    rinterpolate_float_t ** valuemin;
    rinterpolate_float_t * max;
    rinterpolate_float_t * min;
    rinterpolate_counter_t * nvalues;

};

#endif // RINTERPOLATE_STRUCTURES_H
