#define __BINARY_C_LINT_SKIP
#define _GNU_SOURCE

#include "../binary_c_code_options.h"
#include "../binary_c_error_codes.h"
#include "../binary_c_exit_macros.h"
#include "../breakpoints/breakpoints_prototypes.h"
#include "../binary_c_exit_prototypes.h"

#ifndef __HAVE_LIBRINTERPOLATE__

#include "rinterpolate.h"
#include "rinterpolate_internal.h"

#include <stdlib.h>

void rinterpolate_sort_table_in_place(struct rinterpolate_data_t * RESTRICT const rinterpolate_data,
                                      struct rinterpolate_table_t * const table)
{
    /*
     * Sort table in place
     */
    struct rinterpolate_table_t * sorted_table =
        rinterpolate_sort_table(rinterpolate_data,
                                table);

    /*
     * free the old table struct's contents, but leave
     * the struct allocated in rinterpolate_data
     */
    rinterpolate_free_table_contents(table);

    /*
     * so we can copy over the top of it
     * with sorted_table (and set auto_free_data
     * because the copy is newly allocated)
     */
    memcpy(table,
           sorted_table,
           sizeof(struct rinterpolate_table_t));

    sorted_table->auto_free_data = TRUE;

    /*
     * and remove the reference to sorted_table from
     * rinterpolate_data's list
     */
    rinterpolate_free_table_ref(rinterpolate_data,
                                sorted_table);

    /*
     * Free sorted_table now it is copied into table
     */
    Safe_free(sorted_table);

    return;
}

#endif // __HAVE_LIBRINTERPOLATE__

typedef int prevent_ISO_C_warning;

        