#define __BINARY_C_LINT_SKIP

#include "../binary_c_code_options.h"
#include "../binary_c_error_codes.h"
#include "../binary_c_exit_macros.h"
#include "../breakpoints/breakpoints_prototypes.h"
#include "../binary_c_exit_prototypes.h"

#ifndef __HAVE_LIBRINTERPOLATE__

#include "rinterpolate.h"
#include "rinterpolate_internal.h"

struct rinterpolate_table_t * rinterpolate_orthogonalise_expand_table(
    struct rinterpolate_data_t * const rinterpolate_data,
    struct rinterpolate_table_t * table_in
    )
{
    /*
     * Orthogonalise a table by expansion.
     *
     * Not recommended for large tables.
     */
    const rinterpolate_Boolean_t vb = FALSE;
    struct rinterpolate_table_t * table_out = NULL; /* to be filled */
    const rinterpolate_Boolean_t orth_in =
        rinterpolate_is_table_orthogonal(rinterpolate_data,
                                         table_in,
                                         FALSE);
    if(vb)
    {
        Rinterpolate_print("Orth minmax table %p\n",
                          (void*)table_in);
        Rinterpolate_print("is orth? %d\n",orth_in);
    }

    if(orth_in == FALSE)
    {
        /*
         * Requires orthogonalization
         */
        if(vb)
        {
            Rinterpolate_print("Requires orth\n");
        }

        //rinterpolate_show_table(rinterpolate_data, table_in, stdout, -1);
        rinterpolate_analyse_table(rinterpolate_data, table_in, "orth min max");


        rinterpolate_counter_t new_linecount = 1;
        for(rinterpolate_counter_t i = 0; i<table_in->n; i++)
        {
            if(vb)
            {
                Rinterpolate_print(
                        "Col %u has %u values : ",
                        i,
                        table_in->stats->nvalues[i]);
            }
            new_linecount *= table_in->stats->nvalues[i];
            if(vb)
            {
                for(rinterpolate_counter_t j = 0; j<table_in->stats->nvalues[i]; j++)
                {
                    Rinterpolate_print("%g ",table_in->stats->valuelist[i][j]);
                }
                Rinterpolate_print("\n");
            }
        }
        if(vb)
        {
            Rinterpolate_print("New table will have %u lines\n",
                   new_linecount);
        }

        /*
         * Construct new min/max table
         */
        const size_t line_bytes = sizeof(rinterpolate_float_t)* table_in->line_length;
        rinterpolate_float_t * data = Rinterpolate_malloc(line_bytes * new_linecount);

        /* next value in the table */
        rinterpolate_counter_t * current_value_counter =
            Rinterpolate_calloc((size_t)table_in->n,
                                sizeof(rinterpolate_float_t));
        if(current_value_counter == NULL)
        {
            rinterpolate_error(RINTERPOLATE_CALLOC_FAILED,
                               "Failed to calloc current_value_counter of size %zu * %zu = %zu\n",
                               rinterpolate_data,
                               (size_t)table_in->n,
                               sizeof(rinterpolate_float_t),
                               ((size_t)table_in->n) * sizeof(rinterpolate_float_t));
        }

        rinterpolate_float_t * rightdata = Rinterpolate_malloc(sizeof(rinterpolate_float_t) * (size_t)table_in->d);

        if(rightdata == NULL)
        {
            rinterpolate_error(RINTERPOLATE_MALLOC_FAILED,
                               "Failed to malloc rightdata of size %zu\n",
                               rinterpolate_data,
                               sizeof(rinterpolate_float_t) * (size_t)table_in->d);
        }

        rinterpolate_Boolean_t loop = TRUE;
        rinterpolate_counter_t line = 0;
        struct rinterpolate_vector_t * thisline = rinterpolate_new_vector(table_in->n,NULL);

        if(thisline == NULL)
        {
            rinterpolate_error(RINTERPOLATE_VECTOR_ALLOC_FAILED,
                               "Failed to make vector for thisline in rinterpolate_orthogonalise_expand_table() from data table_in->n=%u\n",
                               rinterpolate_data,
                               table_in->n);
        }

        while(loop == TRUE)
        {
            /*
             * Show the line of the new table
             */
            if(vb)
            {
                Rinterpolate_print("X %u : ",line);
            }
            {
                rinterpolate_float_t * const pl = thisline->data;
                rinterpolate_float_t ** const pr = table_in->stats->valuelist;
                for(rinterpolate_counter_t k = 0; k < table_in->n; k++)
                {
                    pl[k] = pr[k][current_value_counter[k]];
                }
            }
            if(vb)
            {
                rinterpolate_print_vector(thisline, " ");
                Rinterpolate_print("--");
            }

            /*
             * Find a matching line if we can
             */
            const rinterpolate_counter_t right = table_in->n;

            rinterpolate_counter_t match_line = table_in->l+1;
            for(rinterpolate_counter_t i = 0; i < table_in->l; i++)
            {
                if(rinterpolate_vector_equal_floatlist(thisline,
                                                       table_in->data + i * table_in->line_length))
                {
                    match_line = i;
                    break;
                }
            }


            if(match_line < table_in->l)
            {
                /*
                 * One line: use this data directly (easy!)
                 */
                if(vb)
                {
                    Rinterpolate_print("exact match at %u -- ",match_line);
                }
                memcpy(rightdata,
                       table_in->data + match_line * table_in->line_length + table_in->n,
                       table_in->d * sizeof(rinterpolate_float_t));
            }
            else
            {
                /*
                 * No matching lines: what to do?
                 *
                 * First, try to match as many columns as we can.
                 * Matching table_in->n failed, so try n-1, n-2 etc.
                 * until failure.
                 *
                 * Then choose the nearest line.
                 */
                rinterpolate_counter_t n = table_in->n-1;
                rinterpolate_float_t min = RINTERPOLATE_FLOAT_MAX;
                rinterpolate_float_t max = RINTERPOLATE_FLOAT_MIN;
                rinterpolate_counter_t min_line = 0;
                rinterpolate_counter_t max_line = 0;

                rinterpolate_counter_t nearest_line = table_in->l;

                while(n>0)
                {
                    rinterpolate_Boolean_t match = FALSE;
                    rinterpolate_float_t diff = RINTERPOLATE_FLOAT_MAX;
                    nearest_line = table_in->l;
                    for(rinterpolate_counter_t i = 0; i < table_in->l; i++)
                    {
                        /*
                         * Lines that match up to column n
                         * should have their min and max values of the next
                         * column checked.
                         */
                        rinterpolate_float_t * const p = table_in->data + i * table_in->line_length;
                        const rinterpolate_Boolean_t match_to_n =
                            rinterpolate_vector_equal_floatlist_n(thisline,
                                                                  p,
                                                                  n);
                        if(match_to_n == TRUE)
                        {
                            /*
                             * we have a matching line up to column n
                             */
                            match = TRUE;
                            rinterpolate_float_t f = *(p+n);
                            if(f<min)
                            {
                                min = f;
                                min_line = i;
                            }
                            if(f>max)
                            {
                                max = f;
                                max_line = i;
                            }

                            /*
                             * Get the nearest line to the data we want
                             */
                            const rinterpolate_float_t d = fabs(thisline->data[n] - *(table_in->data + i * table_in->line_length + table_in->n));
                            if(d < diff)
                            {
                                nearest_line = i;
                                diff = d;
                            }
                        }
                    }

                    if(match == FALSE)
                    {
                        /*
                         * No match, try previous column
                         */
                        n--;
                    }
                    else
                    {
                        /*
                         * partial match : replace the column data
                         * with either min or max
                         */
                        break;
                    }
                }

                /*
                 * Get the min/max to be put as data in the new minmax table
                 * from the best matching line
                 */
                match_line =
                    thisline->data[n] < min ? min_line :
                    thisline->data[n] > max ? max_line :
                    nearest_line;
                memcpy(rightdata,
                       table_in->data + match_line * table_in->line_length + table_in->n,
                       table_in->d * sizeof(rinterpolate_float_t));

                if(vb)
                {
                    Rinterpolate_print("matched to col %u : next col min=%g max=%g from lines %u to %u : our new value %g means use %s = %g data from line %u hence ",
                           n,
                           min,
                           max,
                           min_line,
                           max_line,
                           thisline->data[n],
                           thisline->data[n] < min ? "min" : "max",
                           thisline->data[n] < min ? min : max,
                           match_line);
                    for(rinterpolate_counter_t jj=0; jj<table_in->d; jj++)
                    {
                        Rinterpolate_print("%g ",rightdata[jj]);
                    }
                    Rinterpolate_print("-- ");
                }
            }

            {
                /*
                 * Put the data in the new minmax table
                 *
                 * Get the parameters from the vector
                 */
                rinterpolate_float_t * const offset = data + table_in->line_length * line;
                memcpy(offset,
                       thisline->data,
                       table_in->n * sizeof(rinterpolate_float_t));

                /*
                 * And the data from the table_in
                 */
                memcpy(offset + table_in->n,
                       rightdata,
                       table_in->d * sizeof(rinterpolate_float_t));

            }

            if(vb)
            {
#ifdef RINTERPOLATE_DEBUG
                rinterpolate_float_t * const pp = data + table_in->line_length * line;
                Rinterpolate_print("\nY %u : ",line);
                for(rinterpolate_counter_t k = 0; k < table_in->line_length; k++)
                {
                    Rinterpolate_print("%g ",
                           *(pp + k));
                }
                Rinterpolate_print("\n");
#endif
            }

            /*
             * Loop to next line
             */
            rinterpolate_signed_counter_t changecol = right - 1;
            current_value_counter[changecol]++;
            while(loop == TRUE &&
                  current_value_counter[changecol] >= table_in->stats->nvalues[changecol])
            {
                current_value_counter[changecol] = 0;
                changecol--;
                if(changecol < 0)
                {
                    loop = FALSE;
                }
                else
                {
                    current_value_counter[changecol]++;
                    if(changecol==0
                       &&
                       current_value_counter[0] >= table_in->stats->nvalues[0])
                    {
                        loop = FALSE;
                    }
                }
            }

            line++;
        }

        /*
         * Free memory
         */
        rinterpolate_free_vector(&thisline);
        Safe_free(rightdata);

        /*
         * Make replacement table
         */
        table_out = rinterpolate_new_table(rinterpolate_data,
                                           data,
                                           table_in->n,
                                           table_in->d,
                                           new_linecount,
                                           0,
                                           TRUE);
        Rinterpolate_print("Replacement table %p with min_max_table %p column_is_mapped %p\n",
                           (void*)table_out,
                           (void*)table_out->min_max_table,
                           (void*)table_out->column_is_mapped);
        table_out->auto_free_data = TRUE;

        /*
         * Check it is orthogonal
         */
        const rinterpolate_Boolean_t orth_out =
            rinterpolate_is_table_orthogonal(rinterpolate_data,
                                             table_out,
                                             FALSE);
        if(orth_out == FALSE)
        {
            Rinterpolate_print("rinterpolate_orthogonalise_min_max_table failed\n");
        }
        else if(vb)
        {
            Rinterpolate_print("replacement table is orthogonal\n");
        }

        Safe_free(current_value_counter);
    }
    else
    {
        table_out = table_in;
    }

   if(vb)
   {
       Rinterpolate_print("orth ret table_in %p\n",(void*)table_in);
   }

   return table_out;
}

#endif // __HAVE_LIBRINTERPOLATE__

typedef int prevent_ISO_C_warning;

        