#define __BINARY_C_LINT_SKIP

#include "../binary_c_code_options.h"
#include "../binary_c_error_codes.h"
#include "../binary_c_exit_macros.h"
#include "../breakpoints/breakpoints_prototypes.h"
#include "../binary_c_exit_prototypes.h"

#ifndef __HAVE_LIBRINTERPOLATE__

#include "rinterpolate.h"
#include "rinterpolate_internal.h"

struct rinterpolate_stats_t * rinterpolate_make_stats(
    const rinterpolate_counter_t n)
{
    /*
     * Build and return a stats struct and its contents.
     *
     * Return NULL on error.
     */
#define stats_malloc(X,SIZE)                    \
    stats->X = Rinterpolate_malloc(SIZE);       \
    if(stats->X==NULL) return NULL;
#define stats_calloc(X,N,SIZE)                  \
    stats->X = Rinterpolate_calloc(N,SIZE);     \
    if(stats->X==NULL) return NULL;

    struct rinterpolate_stats_t * const stats =
        Rinterpolate_malloc(sizeof(struct rinterpolate_stats_t));

    if(stats)
    {
        stats_malloc(valuelist,
                     n * sizeof(rinterpolate_float_t *));
        stats_calloc(nvalues,
                     n,
                     sizeof(rinterpolate_counter_t));
        stats_malloc(valuelist_alloced,
                     n * sizeof(rinterpolate_counter_t));
        stats_malloc(min,
                     n * sizeof(rinterpolate_float_t));
        stats_malloc(max,
                     n * sizeof(rinterpolate_float_t));
        stats_calloc(valuemax,
                     n,
                     sizeof(rinterpolate_float_t *));
        stats_calloc(valuemin,
                     n,
                     sizeof(rinterpolate_float_t *));
    }
    return stats;
}

#endif // __HAVE_LIBRINTERPOLATE__

typedef int prevent_ISO_C_warning;

        