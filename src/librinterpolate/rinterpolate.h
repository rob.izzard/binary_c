#pragma once
#ifndef RINTERPOLATE_H
#define RINTERPOLATE_H

/*
 * Header file for librinterpolate
 */

/************************************************************
 * include a selection of standard libraries
 ************************************************************/
#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif // _GNU_SOURCE
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <float.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include "rinterpolate_compiler.h"
#include "rinterpolate_types.h"

/************************************************************
 * rinterpolate's macros
 *************************************************************/

#define RINTERPOLATE_VERSION "1.12"
#define RINTERPOLATE_MAJOR_VERSION 1
#define RINTERPOLATE_MINOR_VERSION 12

/* error codes */
enum {
    RINTERPOLATE_NO_ERROR,
    RINTERPOLATE_CALLOC_FAILED,
    RINTERPOLATE_ALLOCATE_OVER,
    RINTERPOLATE_MAP_BAD_COLUMN,
    RINTERPOLATE_MAP_ZERO_LINES,
    RINTERPOLATE_REALLOC_FAILED,
    RINTERPOLATE_VECTOR_ALLOC_FAILED,
    RINTERPOLATE_MALLOC_FAILED,
    RINTERPOLATE_1D_INTERPOLATION_FAILED
};

/* enable nanchecks */
#define RINTERPOLATE_NANCHECKS

/* debugging */
//#define RINTERPOLATE_DEBUG

#ifdef RINTERPOLATE_DEBUG
#define RINTERPOLATE_DEBUG_STREAM stderr
#define Rinterpolate_print(...)                                         \
    fprintf(RINTERPOLATE_DEBUG_STREAM,"rinterpolate %u @ %s %d : ",      \
            rinterpolate_data?rinterpolate_data->number_of_interpolation_tables:0, \
            Rinterpolate_Filename(__FILE__),                            \
            __LINE__);                                                  \
    fflush(RINTERPOLATE_DEBUG_STREAM);                                  \
    if(rinterpolate_data)                                               \
    {                                                                   \
        fprintf(RINTERPOLATE_DEBUG_STREAM,"data=%p ",(void*)rinterpolate_data); \
        for(rinterpolate_counter_t _i=0; _i<rinterpolate_data->number_of_interpolation_tables; _i++) \
        {                                                               \
            if(rinterpolate_data->tables &&                             \
               rinterpolate_data->tables[_i] &&                         \
               rinterpolate_data->tables[_i]->column_is_mapped==NULL)   \
            {                                                           \
                fprintf(stderr,                                         \
                        "Table %u %p column_is_mapped is NULL\n",       \
                        _i,                                             \
                        (void*)rinterpolate_data->tables[_i]            \
                    );                                                  \
                exit(1);                                                \
            }                                                           \
        }                                                               \
    }                                                                   \
    fprintf(RINTERPOLATE_DEBUG_STREAM,__VA_ARGS__);                     \
    fflush(RINTERPOLATE_DEBUG_STREAM);
#else
#define Rinterpolate_print(...) /* do nothing */
#endif//RINTERPOLATE_DEBUG

/* enable this to show the whole table */
//#define RINTERPOLATE_DEBUG_SHOW_TABLE

/*
 * Perhaps use realloc instead of an alternative algorithm?
 * On a modern i7 this is a little faster.
 */
#define RINTERPOLATE_USE_REALLOC

/*
 * Use standard *alloc functions by default
 */
#define Rinterpolate_malloc(A) malloc(A)
#define Rinterpolate_calloc(A,B) calloc((A),(B))
#define Rinterpolate_realloc(A,B) realloc((A),(B))

/*
 * In some places I have different algorithms based on either
 * pointers or arrays. The speed of execution probably depends on
 * how your compiler deals with these...  The code for pointers is smaller
 * with gcc 3.4.6 and very slightly faster. With later gcc (4.1) the
 * pointer arithmetic is even faster. Try it and see.
 */
#define RINTERPOLATE_USE_POINTER_ARITHMETIC

/* ditto for the j loop (rinterpolate.c) */
#define RINTERPOLATE_POINTER_ARITHMETIC_J_LOOP

/*
 * with the cache we use macros to
 * access cache items, rather than accessing
 * them directly
 */

/* use interpolation cache? should speed up interpolation in many cases */
#define RINTERPOLATE_CACHE

/* length of a line in the cache */
#define RINTERPOLATE_CACHE_LINE (table->n+table->d)

/* pointer to cache result A */
#define Rinterpolate_cache_param(A) (table->cache+RINTERPOLATE_CACHE_LINE*(A))

/* pointer to the location of cache result A */
#define RINTERPOLATE_CACHE_RESULT(A) (table->cache+RINTERPOLATE_CACHE_LINE*(A)+table->n)

/* memcpy is usually faster for copying interpolation results to the cache */
#define RINTERPOLATE_CACHE_USE_MEMCPY

/*
 * Use memcmp to compare cache lines?
 * The alternative is !Fequal, which is usually a bit faster.
 * Your mileage may vary.
 */
#define RINTERPOLATE_CACHE_USE_MEMCMP

/*
 * Wrapper to interpolate TABLE at array COORDS
 * into array RESULT using NCACHE cache lines.
 */
#define Rinterpolate(TABLE,COORDS,RESULT,NCACHE)    \
    rinterpolate((TABLE)->data,                     \
                 (TABLE)->parent,                   \
                 (TABLE)->n,                        \
                 (TABLE)->d,                        \
                 (TABLE)->l,                        \
                 (COORDS),                          \
                 (RESULT),                          \
                 (NCACHE))

/************************************************************
 * rinterpolate's structures and prototypes
 ************************************************************/
#include "rinterpolate_structures.h"
#include "rinterpolate_prototypes.h"

#endif // RINTERPOLATE_H
