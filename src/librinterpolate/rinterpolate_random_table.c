#define __BINARY_C_LINT_SKIP

#include "../binary_c_code_options.h"
#include "../binary_c_error_codes.h"
#include "../binary_c_exit_macros.h"
#include "../breakpoints/breakpoints_prototypes.h"
#include "../binary_c_exit_prototypes.h"

#ifndef __HAVE_LIBRINTERPOLATE__

#include "rinterpolate.h"
#include "rinterpolate_internal.h"

struct rinterpolate_table_t * rinterpolate_random_table(
    struct rinterpolate_data_t * const rinterpolate_data,
    const rinterpolate_counter_t n,
    const rinterpolate_counter_t d,
    const rinterpolate_signed_counter_t * const nx,
    rinterpolate_float_t (*func)(const rinterpolate_float_t * const,
                                 const rinterpolate_counter_t,
                                 const rinterpolate_counter_t)
    )
{
    /*
     * Generate a table with pseudo-random coordinates
     *
     * n = number of parameters
     * d = number of data
     *
     * nx = { ..., ..., ... } is a list of resolutions
     * in each parameter. Each resolution is set from a
     * fixed array of coordinate values if positive,
     * from a random number if negative.
     *
     * func is a function that takes the parameters vector
     * and column number as input and returns a float function of them.
     */


    /* count number of lines */
    rinterpolate_counter_t l = 1;
    for(rinterpolate_counter_t i = 0; i<n; i++)
    {
        l *= abs(nx[i]);
    }

    /* make coordinate lists */
    rinterpolate_float_t ** x = Rinterpolate_malloc(sizeof(rinterpolate_float_t)*n);
    for(rinterpolate_counter_t i = 0; i<n; i++)
    {
        if(nx[i]>0)
        {
            x[i] = Rinterpolate_malloc(sizeof(rinterpolate_float_t)*nx[i]);
            for(rinterpolate_counter_t j=0; j<(rinterpolate_counter_t)abs(nx[i]); j++)
            {
                x[i][j] = rinterpolate_random_coordinate(i);
            }
        }
    }

    rinterpolate_float_t * table_data =
        Rinterpolate_malloc((n+d)*l*sizeof(rinterpolate_float_t));

    const size_t vector_length = sizeof(rinterpolate_float_t) * (size_t)n;
    rinterpolate_float_t * vector = Rinterpolate_malloc(vector_length);
    rinterpolate_counter_t line = 0;
    rinterpolate_counter_t * step = Rinterpolate_calloc(1,sizeof(rinterpolate_counter_t) * n);
    while(line < l)
    {
        rinterpolate_signed_counter_t col = n-1;
        while(line>0 && col>=0)
        {
            step[col]++;
            if(step[col] >= (rinterpolate_counter_t)abs(nx[col]))
            {
                step[col--] = 0;
            }
            else
            {
                break;
            }
        }

        /* parameters */
        for(rinterpolate_counter_t i = 0; i < n; i++)
        {
            if(nx[i] > 0)
            {
                *(vector + i) = x[i][step[i]];
            }
            else
            {
                *(vector + i) = rinterpolate_random_coordinate(i);
            }
        }

        rinterpolate_float_t * p = table_data + line * (n+d);
        memcpy(p,vector,vector_length);
        p += n;

        /* data */
        for(rinterpolate_counter_t i = 0; i<d; i++)
        {
            *(p++) = func(vector,n,i);
        }

        line++;
    }

    const rinterpolate_counter_t table_number =
        rinterpolate_add_new_table(
            rinterpolate_data,
            table_data,
            n,
            d,
            l,
            0,
            FALSE);

    struct rinterpolate_table_t * const table =
        rinterpolate_data->tables[table_number];

    table->auto_free_data = TRUE;

    return table;
}

#endif // __HAVE_LIBRINTERPOLATE__

typedef int prevent_ISO_C_warning;

        