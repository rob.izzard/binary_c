#define __BINARY_C_LINT_SKIP

#include "../binary_c_code_options.h"
#include "../binary_c_error_codes.h"
#include "../binary_c_exit_macros.h"
#include "../breakpoints/breakpoints_prototypes.h"
#include "../binary_c_exit_prototypes.h"

#ifndef __HAVE_LIBRINTERPOLATE__

#include "rinterpolate.h"
#include "rinterpolate_internal.h"

/*
 * rinterpolate_map_right_column
 *
 * Takes a table which has a column which is possibly
 * non-orthogonal (i.e. spaced "randomly") and maps it
 * to a new table with the column in the range 0 - 1,
 * with a given resolution, such that this new table can
 * be interpolated using the normal rinterpolate() call.
 *
 * See also rinterpolate_map_coordinate().
 *
 * rinterpolate_data = the rinterpolate general data struct, if NULL
 *    then table->parent is used
 * table = the table to be mapped
 * new_resolution = the number of points used in the newly-mapped table
 * resolution_list = a list of new_resolution rinterpolate_float_t which
 *    are where the newly-mapped table is sampled, or NULL to just use
 *    linearly-spaced points.
 *
 * On error, returns NULL and will leak memory (so
 * you should catch this!).
 *
 * Warning: this code is still experimental!
 */

//#define RINTERPLOATE_MAP_DEBUG

#undef Concat3
#define Concat3(A,B,C) A##B##C
#define Foreach_matching_line(I,J)                                      \
    Foreach_matching_line_implementation(I,J,matching_line,__COUNTER__)
#define Foreach_matching_line_implementation(I,J,LABEL,LINE)            \
    for(                                                                \
        rinterpolate_counter_t I =                                      \
            matching_line_list[0],                                      \
            J MAYBE_UNUSED = 0,                                         \
            Concat3(__i,LABEL,LINE) = 0                                 \
            ;                                                           \
                                                                        \
        (                                                               \
            Concat3(__i,LABEL,LINE) < n_matching_lines                  \
            &&                                                          \
            ((I = matching_line_list[Concat3(__i,LABEL,LINE)]) || 1)    \
            )                                                           \
            ;                                                           \
                                                                        \
        J++, Concat3(__i,LABEL,LINE)++                                  \
        )


#ifdef RINTERPLOATE_MAP_DEBUG
#define Mapprintf(...) printf(__VA_ARGS__);fflush(stdout);
#else
#define Mapprintf(...) /* do nothing */
#endif

static void map_column(
    struct rinterpolate_data_t * const rinterpolate_data,
    struct rinterpolate_table_t * const table,
    rinterpolate_float_t ** new_data,
    rinterpolate_counter_t * new_data_length,
    rinterpolate_float_t ** new_min_max_data,
    rinterpolate_counter_t * new_min_max_length,
    const rinterpolate_counter_t col,
    const rinterpolate_counter_t new_resolution,
    rinterpolate_float_t * resolution_list
    );

struct rinterpolate_table_t * rinterpolate_map_right_column(
    struct rinterpolate_data_t * const rinterpolate_data,
    struct rinterpolate_table_t * const table,
    const rinterpolate_counter_t new_resolution,
    rinterpolate_float_t * resolution_list
    )
{
    /*
     * Map the rightmost column
     */
    const rinterpolate_counter_t right =
        table->n - 1;
    rinterpolate_counter_t new_data_length;
    rinterpolate_float_t * new_data;
    rinterpolate_counter_t min_max_data_length;
    rinterpolate_float_t * min_max_data;

    Mapprintf("Map table at %p (analysed? %s), column %u with new resolution %u, %u values from %g to %g\n",
              (void*)table,
              table->analysed ? "yes" : "no",
              right,
              new_resolution,
              table->stats->nvalues[right],
              table->stats->min[right],
              table->stats->max[right]
        );

    /*
     * Call map_column to map the right column
     */
    map_column(rinterpolate_data,
               table,
               &new_data,
               &new_data_length,
               &min_max_data,
               &min_max_data_length,
               right,
               new_resolution,
               resolution_list);

    if(new_data == NULL)
    {
        Mapprintf("error detected : returning NULL\n");
        return NULL;
    }

    Mapprintf("mapped to new table with %u lines (rinterpolate_data = %p)\n",
              new_data_length,
              (void*)rinterpolate_data);

    /*
     * make the new, mapped table
     */
    const rinterpolate_counter_t new_table_number =
        rinterpolate_add_new_table(
            rinterpolate_data != NULL ? rinterpolate_data : table->parent,
            new_data,
            table->n,
            table->d,
            new_data_length,
            0,
            FALSE);


    /*
     * Convenience pointer
     */
    struct rinterpolate_table_t * const newtable =
        table->parent->tables[new_table_number];

    Mapprintf("added new table length=%u, number %u, pointer %p\n",
              new_data_length,
              new_table_number,
              (void*)newtable);

    /*
     * copy mapping data
     */
    for(rinterpolate_counter_t i = 0; i<table->n; i++)
    {
        if(table->min_max_table[i]!=NULL)
        {
            newtable->min_max_table[i] =
                rinterpolate_copy_table(NULL,
                                        table->min_max_table[i]);
        }
        newtable->column_is_mapped[i] = table->column_is_mapped[i];
    }


    /*
     * We generate the mapped data table, so we need to free
     * it automatically
     */
    newtable->auto_free_data = TRUE;

    /*
     * Re-generate table's stats
     */
    newtable->analysed = FALSE;
    rinterpolate_analyse_table(rinterpolate_data,
                               newtable,
                               "map right column 1");

    /*
     * Set mapped Booleans
     */
    newtable->column_is_mapped[right] = TRUE;

    /*
     * save a reference to the original table stats
     * (required for coordinate mapping)
     */
    newtable->parent_stats = table->stats;

    /*
     * Make a table for the min,max data and link it
     * to newtable (but not rinterpolate_data)
     */
    struct rinterpolate_table_t * min_max_table =
        rinterpolate_new_table(
            rinterpolate_data,
            min_max_data,
            right, // all but the rightmost column (so if we're mapping column n, te number of columns of parameters in the min_max_table == n)
            2, // 2 data: min and max
            min_max_data_length,
            0,
            FALSE);

    min_max_table->auto_free_data = TRUE;

    Rinterpolate_print("Mapped right col : orthogonalize minmax\n");
    struct rinterpolate_table_t * was = min_max_table;
    min_max_table = rinterpolate_orthogonalise_expand_table(rinterpolate_data,
                                                            min_max_table);
    if(was != min_max_table)
    {
        Rinterpolate_print("Free table %p\n",(void*)was);
        rinterpolate_free_table_contents(was);
        Safe_free(was);
    }

    min_max_table->auto_free_data = TRUE;

    /*
     * If the table is no longer orthogonal, this is a bug.
     */
    if(rinterpolate_is_table_orthogonal(rinterpolate_data,
                                        min_max_table,
                                        FALSE) == FALSE)
    {
        fprintf(stderr,
                "min max table %p is not orthogonal when it should be : this is a bug that you need to fix because rinterpolate_orthogonalise_expand_table should have fixed it for you..\n",
               (void*)min_max_table);
    }

    /*
     * Link the min_max_table in newtable
     */
    Rinterpolate_print("Set minmax table of %p to %p\n",
                       (void*)newtable,
                       (void*)min_max_table);
    newtable->min_max_table[right] = min_max_table;

    Rinterpolate_print("Return newtable %p with mapped right column, min_max_table = %p, column_is_mapped %p\n",
                       (void*)newtable,
                       (void*)newtable->min_max_table,
                       (void*)newtable->column_is_mapped);
    return newtable;
}

static void map_column(
    struct rinterpolate_data_t * const rinterpolate_data,
    struct rinterpolate_table_t * const table,
    rinterpolate_float_t ** new_data,
    rinterpolate_counter_t * new_data_length,
    rinterpolate_float_t ** new_min_max_data,
    rinterpolate_counter_t * new_min_max_length,
    const rinterpolate_counter_t right,
    const rinterpolate_counter_t new_resolution,
    rinterpolate_float_t * resolution_list
    )
{
    const rinterpolate_counter_t vb = 0;

    Mapprintf("map_column table = %p, n=%u d=%u l=%u, right column = %u, new_resolution %u\n",
              (void*)table,
              table->n,
              table->d,
              table->l,
              right,
              new_resolution);


    /*
     * Map column right (>=1) of the table
     */
    if(right == 0)
    {
        rinterpolate_error(RINTERPOLATE_MAP_BAD_COLUMN,
                           "Cannot map column 0 of a table at %p : the table should have at least two coordinate columns\n",
                           rinterpolate_data,
                           (void*)table);
    }

    /*
     * Analyse the table in
     */
    rinterpolate_analyse_table(rinterpolate_data,
                               table,
                               "map right column 2 (forced)");

    /*
     * Foreach left value, we take all the right values
     * and convert their range to 0 to 1
     */
    const size_t datalinesize =
        table->d * sizeof(rinterpolate_float_t);
    const size_t new_line_size =
        (table->n + table->d) * sizeof(rinterpolate_float_t);

    rinterpolate_float_t * new_table_data = NULL;
    rinterpolate_float_t * min_max_table_data = NULL;
    size_t new_table_alloced = 0;
    size_t min_max_alloced = 0;
    rinterpolate_float_t * last_new_data = NULL;
    rinterpolate_float_t * last_min_max_data = NULL;
    rinterpolate_counter_t new_table_linecount = 0;
    rinterpolate_counter_t min_max_table_linecount = 0;
    rinterpolate_counter_t * current_value_counter =
        Rinterpolate_calloc(right - 1 + 1,
                            sizeof(rinterpolate_float_t));
    rinterpolate_Boolean_t loop = TRUE;

    /*
     * If resolution_list == NULL, make a list of the points at
     * which the mapped table will be constructed.
     *
     * If resolution_list != NULL, just use the list passed in.
     */
    rinterpolate_Boolean_t resolution_list_alloced;
    if(resolution_list == NULL)
    {
        resolution_list = Rinterpolate_malloc(
            sizeof(rinterpolate_float_t) * new_resolution);

        if(vb)printf("resolution list (new_resolution = %u) : ",new_resolution);
        for(rinterpolate_counter_t i = 0; i < new_resolution; i++)
        {
            resolution_list[i] = i/(1.0*(new_resolution-1));
            if(vb)printf("%u = %g, ",i,resolution_list[i]);
        }
        if(vb)printf("\n");
        resolution_list_alloced = TRUE;
    }
    else
    {
        resolution_list_alloced = FALSE;
    }

    while(loop == TRUE)
    {
        /* set vector data */
        struct rinterpolate_vector_t * thisline = rinterpolate_new_vector(right,NULL);

        if(thisline == NULL)
        {
            rinterpolate_error(RINTERPOLATE_VECTOR_ALLOC_FAILED,
                               "Failed to make vector for thisline in map_right_column() from right=%u\n",
                               rinterpolate_data,
                               right);
        }

        for(rinterpolate_counter_t k = 0; k < right; k++)
        {
            thisline->data[k] = table->stats->valuelist[k][current_value_counter[k]];
        }

        if(vb)
        {
            printf("Match vector: ");
            for(rinterpolate_counter_t k = 0; k < right; k++)
            {
                printf("%g ",thisline->data[k]);
            }
            printf("\n");
        }

        /*
         * Make a list of matching lines
         */
        rinterpolate_counter_t * matching_line_list = NULL;
        rinterpolate_counter_t matching_line_list_length = 0;
        rinterpolate_counter_t n_matching_lines = 0;

        for(rinterpolate_counter_t i = 0; i < table->l; i++)
        {
            if(rinterpolate_vector_equal_floatlist(thisline,
                                                   table->data + i * table->line_length))
            {
                if(vb)
                {
                    printf("MATCH %u at line %u: ",
                           n_matching_lines,
                           i);
                    rinterpolate_print_vector(thisline," ");
                    printf("\n");
                }

                n_matching_lines++;

                /* perhaps allocate more space for the line list? */
                if(n_matching_lines > matching_line_list_length)
                {
                    matching_line_list_length = Max((rinterpolate_counter_t)2,
                                                    2*n_matching_lines);
                    matching_line_list =
                        Rinterpolate_realloc(matching_line_list,
                                             matching_line_list_length * sizeof(rinterpolate_counter_t));

                    if(matching_line_list == NULL)
                    {
                        rinterpolate_error(RINTERPOLATE_REALLOC_FAILED,
                                           "Failed to realloc matching_line_list of size %zu\n",
                                           rinterpolate_data,
                                           matching_line_list_length * sizeof(rinterpolate_counter_t));
                    }
                }

                matching_line_list[n_matching_lines-1] = i;
            }
        }


        if(vb)
        {
            printf("Found %u lines matching ",n_matching_lines);
            rinterpolate_print_vector(thisline," ");
            printf("\n");
        }


        if(n_matching_lines > 0)
        {
            /* find min and max of the true right column */
            rinterpolate_float_t valuemin = +RINTERPOLATE_FLOAT_MAX;
            rinterpolate_float_t valuemax = -RINTERPOLATE_FLOAT_MAX;
            Foreach_matching_line(i,j)
            {
                const rinterpolate_float_t value =
                    *(table->data + i * table->line_length + right);
                valuemin = Min(value,valuemin);
                valuemax = Max(value,valuemax);
            }
#ifdef RINTERPOLATE_NANCHECKS
            if(isnan(valuemin))
            {
                fprintf(stderr,
                        "rinterpolate map right column : valuemin is nan\n");
                return;
            }
            if(isnan(valuemax))
            {
                fprintf(stderr,
                        "rinterpolate map right column : valuemax is nan\n");
                return;
            }
#endif//RINTERPOLATE_NANCHECKS

            /*
             * Note:
             * delta doesn't matter if valuemax==valuemin
             */
            const rinterpolate_Boolean_t delta_is_zero = Fequal(valuemax,valuemin);
            const rinterpolate_float_t delta =
                delta_is_zero == TRUE  ? 1.0 : (valuemax - valuemin);
#ifdef RINTERPOLATE_NANCHECKS
            if(isnan(delta))
            {
                fprintf(stderr,
                        "rinterpolate map right column : delta is nan\n");
                *new_data = NULL;
                return;
            }
#endif//RINTERPOLATE_NANCHECKS
            if(vb)
            {
                printf("delta = %g - %g = %g\n",
                       valuemax,
                       valuemin,
                       delta);
            }

            /*
             * Add min and max for this line to the min_max_data
             * data. Note there are n - 1 + 2 data items per line
             * of the min_max_data table: n-1 parameters, 2 for min and max.
             */
            min_max_table_linecount++;

            const size_t min_max_offset =
                last_min_max_data == NULL ? 0 : (last_min_max_data - min_max_table_data);
            min_max_alloced += (table->n - 1 + 2) * sizeof(rinterpolate_float_t);
            min_max_table_data = Rinterpolate_realloc(min_max_table_data,
                                                      min_max_alloced);
            if(min_max_table_data == NULL)
            {
                rinterpolate_error(RINTERPOLATE_REALLOC_FAILED,
                                   "Realloc of min_max_table_data failed and returned NULL\n",
                                   rinterpolate_data);
            }
            last_min_max_data = min_max_table_data + min_max_offset;

            /*
             * add thisline's vector as lookup coordinates
             */
            memcpy(last_min_max_data,
                   thisline->data,
                   (table->n - 1) * sizeof(rinterpolate_float_t));
            last_min_max_data += table->n - 1;

            /*
             * add min,max for coordinate interpolation
             * (see rinterpolate_map_coordinate())
             */
            *last_min_max_data++ = valuemin;
            *last_min_max_data++ = valuemax;


            /*
             * Construct the mapped right-column values
             */
            rinterpolate_float_t * mapped_rightvalues =
                Rinterpolate_malloc(sizeof(rinterpolate_float_t) * n_matching_lines);
            if(mapped_rightvalues == NULL)
            {
                rinterpolate_error(RINTERPOLATE_MALLOC_FAILED,
                                   "Failed to malloc mapped_rightvalues of size %zu\n",
                                   rinterpolate_data,
                                   (size_t)(sizeof(rinterpolate_float_t) * n_matching_lines));
            }

            /*
             * force NaNs: note that this requires GNU extensions
             * which we should already have activated, and should not
             * trigger a floating-point exception if using the NAN macro.
             * (I've tested this with gcc 11.3.0.)
             */
            for(rinterpolate_counter_t i=0;i<n_matching_lines;i++)
            {
                mapped_rightvalues[i] = NAN;
            }

            if(vb)
            {
                printf("delta is zero? %s\n",delta_is_zero?"Yes":"No");
            }

            double prev = 0.0;
            Foreach_matching_line(i,j)
            {
                const rinterpolate_float_t rightvalue =
                    *(table->data +
                      i * table->line_length +
                      right);

                mapped_rightvalues[j] = (rightvalue - valuemin) / delta;

                if(j>1 && mapped_rightvalues[j] == prev)
                {
                    /* set to impossible value */
                    mapped_rightvalues[j] = -1.0;
                }

                if(vb)
                {
                    printf("map rightvalue %u from (%g - %g) / %g = %g\n",
                           j,
                           rightvalue,
                           valuemin,
                           delta,
                           mapped_rightvalues[j]);
                }
#ifdef RINTERPOLATE_NANCHECKS
                if(isnan(mapped_rightvalues[j]))
                {
                    fprintf(stderr,
                            "rinterpolate_map_right_column : mapped right value at i=%u j=%u is nan\n",
                           i,
                           j);
                    *new_data = NULL;
                    return;
                }
#endif//RINTERPOLATE_NANCHECKS
            }

            /*
             * Construct the data for 1D interpolation of the
             * right column
             */
            rinterpolate_float_t * data_1D =
                Rinterpolate_malloc((size_t)(datalinesize * n_matching_lines));
            if(data_1D == NULL)
            {
                rinterpolate_error(RINTERPOLATE_MALLOC_FAILED,
                                   "Failed to malloc data_1D of size %zu\n",
                                   rinterpolate_data,
                                   (size_t)(datalinesize * n_matching_lines));
            }

            Foreach_matching_line(i,j)
            {
                memcpy(data_1D     + j * table->d,
                       table->data + i * (table->n + table->d) + table->n,
                       datalinesize);
                if(vb)
                {
                    printf("add 1D data from line %u to line %u : ",i,j);
                    for(rinterpolate_counter_t k=0; k<table->d; k++)
                    {
                        printf("%g ",*(data_1D + j*table->d + k));
                    }
                    printf("\n");
                }
#ifdef RINTERPOLATE_NANCHECKS
                for(rinterpolate_counter_t k=0; k<table->d; k++)
                {
                    if(isnan(*(data_1D + j*table->d + k)))
                    {
                        fprintf(stderr,
                                "rinterpolate_map_right_column : mapped right value at i=%u j=%u k=%u is nan\n",
                                i,
                                j,
                                k);
                        *new_data = NULL;
                        return;
                    }
                }
#endif
            }

            if(delta_is_zero)
            {
                if(n_matching_lines == 1)
                {
                    /*
                     * We have only one matching line:
                     * set the rightvalues at 0 and 1
                     */
                    n_matching_lines++;
                    mapped_rightvalues = Rinterpolate_realloc(mapped_rightvalues,
                                                              sizeof(rinterpolate_float_t) * n_matching_lines);
                    mapped_rightvalues[0] = 0.0;
                    mapped_rightvalues[1] = 1.0;
                    /*
                     * And just copy the data
                     */
                    data_1D = Rinterpolate_realloc(data_1D,
                                                   (size_t)(datalinesize * n_matching_lines));
                    memcpy(data_1D + table->d,
                           data_1D,
                           datalinesize);
                }
                else
                {
                    /*
                     * delta == zero and we have at least two lines
                     * which means the first line has
                     * either mapped to 0.0 or 1.0 : if 0 then we map the
                     * next line to 1 (as all the others are 1), if 1 (i.e.
                     * all are 1) set it to zero.
                     */
                    if(Is_zero(mapped_rightvalues[0]))
                    {
                        mapped_rightvalues[1] = 1.0;
                    }
                    else
                    {
                        mapped_rightvalues[0] = 0.0;
                    }
                }
                if(vb)
                {
                    printf("the map can not work if delta is zero : applying kludge\n");
                }
            }

            /*
             * Construct new subtable chunk
             */
            const size_t new_data_offset =
                last_new_data == NULL ? 0 : (last_new_data - new_table_data);
            new_table_alloced += new_resolution * new_line_size;
            new_table_data = Rinterpolate_realloc(new_table_data,
                                                  new_table_alloced);
            if(min_max_table_data == NULL)
            {
                rinterpolate_error(RINTERPOLATE_REALLOC_FAILED,
                                   "Realloc of last_new_data failed and returned NULL\n",
                                   rinterpolate_data);
            }
            last_new_data = new_table_data + new_data_offset;

            /*
             * Check for doubled lines in the 1D data
             */
#ifdef __TESTING_ONLY
            if(0)
            {
                /*
                 * First we look for a repeat: most tables
                 * will have no repeat, so it makes sense to check
                 * this first before doing any mallocs or memcpys.
                 */
                rinterpolate_Boolean_t found = FALSE;
                for(rinterpolate_counter_t i=0; i<n_matching_lines; i++)
                {
                    if(i>0 && mapped_rightvalues[i] == mapped_rightvalues[i-1])
                    {
                        found = TRUE;
                        i = n_matching_lines+1;
                    }
                }
                if(found == TRUE)
                {
                    double * data_1D_x = Rinterpolate_malloc((size_t)(datalinesize * n_matching_lines));
                    double * mapped_rightvalues_x = Rinterpolate_malloc((size_t)(sizeof(rinterpolate_float_t) * n_matching_lines));
                    rinterpolate_counter_t next = 0;
                    for(rinterpolate_counter_t i=0; i<n_matching_lines; i++)
                    {
                        printf("mapfound %u %g : ",
                               i,
                               mapped_rightvalues[i]);
                        for(rinterpolate_counter_t j=0; j<table->d; j++)
                        {
                            printf("%g ",
                                   *(data_1D + table->d * i + j));
                        }
                        printf("\n");
                        if(i>0 && mapped_rightvalues[i] == mapped_rightvalues[i-1])
                        {
                            /* same mapped_rightvalue : do nothing */
                        }
                        else
                        {
                            mapped_rightvalues_x[next] = mapped_rightvalues[i];
                            //data_1D_x[next] = data_1D[i];
                            memcpy(data_1D_x + table->d * next,
                                   data_1D   + table->d * i,
                                   datalinesize);
                            next++;
                        }
                    }
                    n_matching_lines = next;

                    /*
                     * Free existing mapped_rightvalues and data_1D,
                     * replace these with the new data that has no repeated
                     * values.
                     */
                    Safe_free(mapped_rightvalues);
                    Safe_free(data_1D);
                    mapped_rightvalues = mapped_rightvalues_x;
                    data_1D = data_1D_x;

                    printf("exiting because this is bad\n");

                    {
                        rinterpolate_Boolean_t orth =
                            rinterpolate_is_table_orthogonal(rinterpolate_data,
                                                             table,
                                                             FALSE);
                        printf("is orth? %d\n",orth);
                    }



                    rinterpolate_show_table(rinterpolate_data, table, stdout, 0);
                    _exit(0);
                }
            }
#endif // __TESTING_ONLY

            for(rinterpolate_counter_t j = 0; j < new_resolution; j++)
            {
                if(vb)
                {
                    printf("rinterpolate map right column %u\n",j);
                    printf("data 1D : interpolate 1D at x = %g on %u lines\n",
                           resolution_list[j],
                           n_matching_lines);

                    for(rinterpolate_counter_t k = 0; k<n_matching_lines; k++)
                    {
                        printf("line %u : %g : ",
                               k,
                               mapped_rightvalues[k]);
                        for(rinterpolate_counter_t m=0; m<table->d;m++)
                        {
                            printf("%g ",
                                   *(data_1D + k*(1+table->d) + m));
                        }
                        printf("\n");
                    }
                }

                /*
                 * Check that two lines in the 1D interpolation
                 * values are not identical: if they are, we cannot
                 * interpolate.
                 */
                if(n_matching_lines > 1)
                {
                    for(rinterpolate_counter_t k = 1; k < n_matching_lines; k++)
                    {
                        if(Fequal(mapped_rightvalues[k],
                                  mapped_rightvalues[k-1]))
                        {
                            fprintf(stderr,
                                    "ERR1 two values, %g and %g at positions %d and %d in mapped_rightvalues are identical so cannot be interpolated.",
                                    mapped_rightvalues[k-1],
                                    mapped_rightvalues[k],
                                    ((int)(k))-1,
                                    (int)(k));
                            fflush(NULL);
                            rinterpolate_error(RINTERPOLATE_1D_INTERPOLATION_FAILED,
                                               "ERR2 two values, %g and %g at positions %d and %d in mapped_rightvalues are identical so cannot be interpolated.",
                                               rinterpolate_data,
                                               mapped_rightvalues[k-1],
                                               mapped_rightvalues[k],
                                               ((int)(k))-1,
                                               (int)(k)
                                );
                        }
                    }
                }

                /*
                 * Interpolate to construct new data lines
                 * at location 0 < resolution_list[j] < 1
                 */
                rinterpolate_float_t * p =
                    rinterpolate_1D(mapped_rightvalues,
                                    data_1D,
                                    resolution_list[j],
                                    n_matching_lines,
                                    table->d,
                                    0);

                if(p==NULL)
                {
                    rinterpolate_error(RINTERPOLATE_1D_INTERPOLATION_FAILED,
                                       "1D interpolation returned NULL : this is bad!",
                                       rinterpolate_data);
                }

#ifdef RINTERPOLATE_NANCHECKS
                /*
                 * Check for nans
                 */
                for(rinterpolate_counter_t kk=0; kk<table->d; kk++)
                {
                    if(vb)
                    {
                        printf("check p=%p kk=%u\n",(void*)p,kk);
                        fflush(stdout);
                    }
                    if(isnan(p[kk]))
                    {
                        fprintf(stderr,
                                "rinterpolate_map_right_column: NaN found in 1D interpolated data position %u at x=%g\n",
                                kk,
                                resolution_list[j]);

                        fprintf(stderr,
                                "1D coords : ");
                        for(rinterpolate_counter_t m=0; m<table->d; m++)
                        {
                            fprintf(stderr,
                                    "%g ",*(mapped_rightvalues+m));
                        }
                        fprintf(stderr,
                                "\n");
                        fprintf(stderr,
                               "1D data : ");
                        for(rinterpolate_counter_t m=0; m<table->d; m++)
                        {
                            fprintf(stderr,
                                    "%g ",*(data_1D+m));
                        }
                        fprintf(stderr,
                                "\n");
                        *new_data = NULL;
                        return;
                    }
                }
#endif//RINTERPOLATE_NANCHECKS

                /*
                 * Copy new data into new table
                 */
                memcpy(last_new_data,
                       thisline->data,
                       sizeof(rinterpolate_float_t)*thisline->n);
                last_new_data += thisline->n;

#ifdef RINTERPOLATE_NANCHECKS
                for(rinterpolate_counter_t kk=0; kk<thisline->n; kk++)
                {
                    if(isnan(*(thisline->data+kk)))
                    {
                        fprintf(stderr,
                                "rinterpolate_map_right_column: New data has nan at parameter position %u\n",kk);
                        *new_data = NULL;
                        return;
                    }
                }
#endif // RINTERPOLATE_NANCHECKS

                *last_new_data++ = resolution_list[j];
                memcpy(last_new_data,
                       p,
                       datalinesize);
                last_new_data += table->d;
                new_table_linecount++;

#ifdef RINTERPOLATE_NANCHECKS
                for(rinterpolate_counter_t kk=0; kk<table->d; kk++)
                {
                    if(isnan(*(p+kk)))
                    {
                        printf("rinterpolate_map_right_column: New data has nan at parameter position %u\n",kk);
                        *new_data = NULL;
                        return;
                    }
                }
#endif // RINTERPOLATE_NANCHECKS

                /*
                 * Free interpolated data
                 */
                Safe_free(p);
            }

            /*
             * Free tmp storage
             */
            Safe_free(mapped_rightvalues);
            Safe_free(data_1D);
        }

        Safe_free(matching_line_list);
        rinterpolate_free_vector(&thisline);

        /* next value in the table */
        rinterpolate_signed_counter_t changecol = right - 1;
        current_value_counter[changecol]++;


        while(loop == TRUE &&
              current_value_counter[changecol] >= table->stats->nvalues[changecol])
        {
            current_value_counter[changecol] = 0;
            changecol--;
            if(changecol < 0)
            {
                loop = FALSE;
            }
            else
            {
                current_value_counter[changecol]++;
                if(changecol==0
                   &&
                   current_value_counter[0] >= table->stats->nvalues[0])
                {
                    loop = FALSE;
                }
            }
        }
    }
    Safe_free(current_value_counter);
    if(resolution_list_alloced == TRUE)
    {
        Safe_free(resolution_list);
    }

    /*
     * Set the pointers and return
     */
    *new_data = new_table_data;
    *new_data_length = new_table_linecount;
    *new_min_max_data = min_max_table_data;
    *new_min_max_length = min_max_table_linecount;

}

#endif // __HAVE_LIBRINTERPOLATE__

typedef int prevent_ISO_C_warning;

        