#define __BINARY_C_LINT_SKIP

#include "../binary_c_code_options.h"
#include "../binary_c_error_codes.h"
#include "../binary_c_exit_macros.h"
#include "../breakpoints/breakpoints_prototypes.h"
#include "../binary_c_exit_prototypes.h"

#ifndef __HAVE_LIBRINTERPOLATE__

#include "rinterpolate.h"
#include "rinterpolate_internal.h"

struct rinterpolate_table_t * rinterpolate_map_right_columns(
    struct rinterpolate_data_t * const rinterpolate_data,
    struct rinterpolate_table_t * const table,
    const rinterpolate_Boolean_t * const columns_to_map,
    const rinterpolate_float_t * const new_resolutions,
    rinterpolate_float_t ** resolution_list)
{
    /*
     * Wrapper function to map the columns:
     *
     * columns_to_map is an array of Booleans
     * each one corresponding to a column of data in table.
     *
     * If TRUE the column is remapped to the resolution
     * given in new_resolutions for that column.
     *
     * Note: column 0 is never remapped.
     *
     * Returns a pointer to the mapped table.
     *
     * If no columns are mapped, or there is an error,
     * returns a NULL pointer.
     */
    if(table->n <= 1) return NULL;

    const rinterpolate_counter_t right = table->n - 1;
    struct rinterpolate_table_t * mapped_table =
        rinterpolate_copy_table(rinterpolate_data,
                                table);
    Rinterpolate_print("new mapped table %p\n",
                       (void*)mapped_table);

    for(rinterpolate_counter_t col = right; col > 0; col--)
    {
        if(columns_to_map[col] == TRUE)
        {
            const rinterpolate_Boolean_t swap = col != right;
            mapped_table->analysed = FALSE;
            rinterpolate_analyse_table(rinterpolate_data,
                                       mapped_table,
                                       "map right cols");

            if(swap)
            {
                rinterpolate_swap_columns(rinterpolate_data,
                                          mapped_table,
                                          col,
                                          right,
                                          FALSE);
            }

            struct rinterpolate_table_t * was = mapped_table;

            Rinterpolate_print("do map\n");

            mapped_table =
                rinterpolate_map_right_column(
                    rinterpolate_data,
                    mapped_table,
                    new_resolutions != NULL ? new_resolutions[col] : 0.0,
                    resolution_list != NULL ? resolution_list[col] : NULL
                    );

            Rinterpolate_print("replaced mapped table %p was %p\n",
                   (void*)mapped_table,
                   (void*)was);

            Rinterpolate_print("done map\n");

#ifdef RINTERPOLATE_NANCHECKS
            if(rinterpolate_check_table_for_nans(mapped_table)==TRUE)
            {
                fprintf(stderr,
                        "rinterpolate_map_right_columns: mapped table has a nan\n");
                return NULL;
            }
#endif //RINTERPOLATE_NANCHECKS

            if(mapped_table != table &&
               mapped_table != was)
            {
                Rinterpolate_print("free was = %p\n",(void*)was);
                rinterpolate_delete_table(rinterpolate_data,
                                          was,
                                          NULL);
                Safe_free(was);
                Rinterpolate_print("free was = %p\n",(void*)was);
            }

            if(swap)
            {
                Rinterpolate_print("pre swap\n");
                rinterpolate_swap_columns(rinterpolate_data,
                                          mapped_table,
                                          col,
                                          right,
                                          FALSE);
                Rinterpolate_print("post swap\n");
            }

            /*
             * Sort the table again : we can then free the old mapped data
             */
            Rinterpolate_print("pre sort\n");
            rinterpolate_sort_table_in_place(rinterpolate_data,
                                             mapped_table);
            Rinterpolate_print("post sort\n");
        }
    }
    rinterpolate_analyse_table(rinterpolate_data,
                               mapped_table,
                               "map right columns");

    return mapped_table;
}

#endif // __HAVE_LIBRINTERPOLATE__

typedef int prevent_ISO_C_warning;

        