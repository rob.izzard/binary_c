#define __BINARY_C_LINT_SKIP

#include "../binary_c_code_options.h"
#include "../binary_c_error_codes.h"
#include "../binary_c_exit_macros.h"
#include "../breakpoints/breakpoints_prototypes.h"
#include "../binary_c_exit_prototypes.h"

#ifndef __HAVE_LIBRINTERPOLATE__

#include "rinterpolate.h"
#include "rinterpolate_internal.h"


rinterpolate_counter_t rinterpolate_bisearch(
    const rinterpolate_float_t * const coords,
    const rinterpolate_float_t x,
    const rinterpolate_counter_t nlines)
{
    /*
     * Simple binary search: returns the lower
     * spanning coordinate.
     *
     * If nlines <= 1 there will be problems!
     * (so we return 0 in this case)
     */
    if(nlines<=1 || x < coords[0])
    {
        return 0;
    }
    else if(x > coords[nlines - 1])
    {
        /*
         * Off the far end: return
         * one left of (nlines-1)
         */
        return nlines - 2;
    }
    else
    {
        /*
         * Intermediate point : interpolate
         */
        rinterpolate_counter_t a = 0;
        rinterpolate_counter_t b = nlines;
        if(likely(b>1))
        {
            while(likely(b-a > 1))
            {
                const rinterpolate_counter_t c = (a+b)>>1;
                if(x > *(coords + c))
                {
                    a = c;
                }
                else
                {
                    b = c;
                }
            }
        }
        return a;
    }
}

#endif // __HAVE_LIBRINTERPOLATE__

typedef int prevent_ISO_C_warning;

        