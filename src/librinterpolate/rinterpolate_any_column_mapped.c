#define __BINARY_C_LINT_SKIP

#include "../binary_c_code_options.h"
#include "../binary_c_error_codes.h"
#include "../binary_c_exit_macros.h"
#include "../breakpoints/breakpoints_prototypes.h"
#include "../binary_c_exit_prototypes.h"

#ifndef __HAVE_LIBRINTERPOLATE__

#include "rinterpolate.h"
#include "rinterpolate_internal.h"

/*
 * Return TRUE if any column in the table is
 * mapped, FALSE otherwise.
 */
rinterpolate_Boolean_t rinterpolate_any_column_mapped(struct rinterpolate_data_t * const rinterpolate_data MAYBE_UNUSED,
                                                      struct rinterpolate_table_t * const table)
{

    Rinterpolate_print("any column mapped table %p label %s\n",
                       (void*)table,
                       table->label);
    if(table)
    {
        Rinterpolate_print("n = %u array %p\n",
                           table->n,
                           (void*)table->column_is_mapped);
        if(table->column_is_mapped)
        {
            for(rinterpolate_counter_t i=0; i<table->n; i++)
            {
                Rinterpolate_print("Check col %u = %d\n",
                                   i,
                                   table->column_is_mapped[i]);
                if(table->column_is_mapped[i] == TRUE)
                {
                    Rinterpolate_print("Column %u is mapped\n",i);
                    return TRUE;
                }
            }
        }

        Rinterpolate_print("No columns mapped\n");
    }
    else
    {
        Rinterpolate_print("No table\n");
    }

    return FALSE;
}

#endif // __HAVE_LIBRINTERPOLATE__

typedef int prevent_ISO_C_warning;

        