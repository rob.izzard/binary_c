#pragma once

#ifndef RINTERPOLATE_PROTOTYPES_H
#define RINTERPOLATE_PROTOTYPES_H
#include "rinterpolate.h"

struct rinterpolate_data_t * rinterpolate(
    const rinterpolate_float_t * RESTRICT const table, // (const pointer to) the data table
    struct rinterpolate_data_t * rinterpolate_data, // where rinterpolate stores data
    const rinterpolate_counter_t n, // the number of parameters (i.e. dimensions)
    const rinterpolate_counter_t d, // the number of data items
    const rinterpolate_counter_t l, // the number of lines of data
    const rinterpolate_float_t * RESTRICT const x, // the values of the parameters
    rinterpolate_float_t * RESTRICT const r,  // the result of the interpolation
    const rinterpolate_counter_t cache_length // tells us to use the cache, or not
    );

void rinterpolate_free_data(struct rinterpolate_data_t * RESTRICT const rinterpolate_data);
void rinterpolate_free_table_contents(struct rinterpolate_table_t * const table);
void rinterpolate_free_table(struct rinterpolate_table_t ** table);

rinterpolate_counter_t rinterpolate_alloc_dataspace(struct rinterpolate_data_t ** RESTRICT const r);
void rinterpolate_build_flags(struct rinterpolate_data_t * RESTRICT const rinterpolate_data);

/*
 * Internal functions
 */

int rinterpolate_error(const rinterpolate_counter_t errnum,
                       const char * RESTRICT const format,
                       struct rinterpolate_data_t * RESTRICT const rinterpolate_data,
                       ...) Gnu_format_args(2,4);
rinterpolate_signed_counter_t Pure_function rinterpolate_id_table(
    struct rinterpolate_data_t * RESTRICT const rinterpolate_data,
    const rinterpolate_float_t * RESTRICT const data
    );



rinterpolate_Boolean_t rinterpolate_check_cache(
    struct rinterpolate_data_t * RESTRICT const rinterpolate_data MAYBE_UNUSED,
    struct rinterpolate_table_t * RESTRICT const table,
    const rinterpolate_float_t * RESTRICT const x,
    rinterpolate_float_t * RESTRICT const r);
#ifdef RINTERPOLATE_CACHE
void rinterpolate_alloc_cacheline(struct rinterpolate_data_t * RESTRICT const rinterpolate_data,
                                  struct rinterpolate_table_t * RESTRICT const table);
#endif
void rinterpolate_make_steps(struct rinterpolate_data_t * RESTRICT const rinterpolate_data,
                             struct rinterpolate_table_t * RESTRICT const table);
void rinterpolate_alloc_hypertable(struct rinterpolate_data_t * const rinterpolate_data,
                                   struct rinterpolate_table_t * RESTRICT const table);
void rinterpolate_alloc_varcount(struct rinterpolate_data_t * RESTRICT const rinterpolate_data,
                                 struct rinterpolate_table_t * RESTRICT const table);
void rinterpolate_search_table(
    struct rinterpolate_data_t * const rinterpolate_data MAYBE_UNUSED,
    struct rinterpolate_table_t * RESTRICT const table,
    const rinterpolate_float_t * RESTRICT const x
    );
void rinterpolate_construct_hypercube(
    struct rinterpolate_data_t * const rinterpolate_data MAYBE_UNUSED,
    struct rinterpolate_table_t * RESTRICT const table
    );

void rinterpolate_interpolate(
    struct rinterpolate_data_t * RESTRICT const rinterpolate_data,
    const struct rinterpolate_table_t * RESTRICT const table,
    const rinterpolate_float_t * RESTRICT const x,
    rinterpolate_float_t * RESTRICT const r);

void rinterpolate_store_cache(struct rinterpolate_data_t * RESTRICT const rinterpolate_data,
                              struct rinterpolate_table_t * RESTRICT const table,
                              const rinterpolate_float_t * RESTRICT const x,
                              const rinterpolate_float_t * RESTRICT const r);

void rinterpolate_make_presearch(struct rinterpolate_data_t * RESTRICT const rinterpolate_data,
                                 struct rinterpolate_table_t * RESTRICT const table);

#ifdef RINTERPOLATE_CACHE
void rinterpolate_resize_cache(struct rinterpolate_data_t * rinterpolate_data,
                               struct rinterpolate_table_t * RESTRICT const table,
                               const rinterpolate_counter_t cache_length);
#endif


struct rinterpolate_table_t * rinterpolate_new_table(
    struct rinterpolate_data_t * RESTRICT const rinterpolate_data,
    const rinterpolate_float_t * RESTRICT const data,
    const rinterpolate_counter_t n,
    const rinterpolate_counter_t d,
    const rinterpolate_counter_t l,
    const rinterpolate_counter_t cache_length,
    const rinterpolate_Boolean_t analyse
    );


rinterpolate_counter_t rinterpolate_add_new_table(
    struct rinterpolate_data_t * RESTRICT const rinterpolate_data,
    const rinterpolate_float_t * RESTRICT const table,
    const rinterpolate_counter_t n,
    const rinterpolate_counter_t d,
    const rinterpolate_counter_t line_length,
    const rinterpolate_counter_t cache_length,
    const rinterpolate_Boolean_t analyse);

rinterpolate_counter_t rinterpolate_add_new_table_from_pointer(
    struct rinterpolate_data_t * RESTRICT const rinterpolate_data,
    struct rinterpolate_table_t * RESTRICT const table
    );

void rinterpolate_free_hypertable(struct rinterpolate_hypertable_t * RESTRICT hypertable);

struct rinterpolate_table_t * rinterpolate_map_right_column(
    struct rinterpolate_data_t * const rinterpolate_data,
    struct rinterpolate_table_t * const table,
    const rinterpolate_counter_t new_resolution,
    rinterpolate_float_t * resolution_list
    );

void rinterpolate_analyse_table(
    struct rinterpolate_data_t * const rinterpolate_data,
    struct rinterpolate_table_t * RESTRICT const table,
    const char * const caller);
void rinterpolate_show_table(struct rinterpolate_data_t * const rinterpolate_data,
                             struct rinterpolate_table_t * const table,
                             FILE * const stream,
                             const rinterpolate_signed_counter_t nlines);


rinterpolate_float_t * rinterpolate_1D(const rinterpolate_float_t * const coords,
                                       const rinterpolate_float_t * const data,
                                       const rinterpolate_float_t x,
                                       const rinterpolate_counter_t nlines,
                                       const rinterpolate_counter_t d,
                                       const rinterpolate_counter_t offset);

rinterpolate_float_t * rinterpolate_map_coordinate(
    struct rinterpolate_data_t * const rinterpolate_data,
    struct rinterpolate_table_t * const table,
    const rinterpolate_float_t * const RESTRICT x);

rinterpolate_counter_t rinterpolate_bisearch(
    const rinterpolate_float_t * const coords,
    const rinterpolate_float_t x,
    const rinterpolate_counter_t nlines);

void rinterpolate_clear_stats(struct rinterpolate_table_t * RESTRICT const table);


/* vector functions */

rinterpolate_Boolean_t
rinterpolate_vectors_equal(const struct rinterpolate_vector_t * const v1,
                           const struct rinterpolate_vector_t * const v2);

void rinterpolate_free_vector(struct rinterpolate_vector_t ** v);

struct rinterpolate_vector_t * rinterpolate_new_clear_vector(const rinterpolate_counter_t n);

struct rinterpolate_vector_t * rinterpolate_new_vector(const rinterpolate_counter_t n,
                                                       const rinterpolate_float_t * const data);

void rinterpolate_set_vector_data(struct rinterpolate_vector_t * const v,
                                  const rinterpolate_float_t * const data);

rinterpolate_Boolean_t
rinterpolate_vector_equal_floatlist(const struct rinterpolate_vector_t * const v,
                                    const rinterpolate_float_t * const floatlist);

void rinterpolate_print_vector(const struct rinterpolate_vector_t * const v,
                               const char * const separator);

int rinterpolate_table_index(struct rinterpolate_data_t * rinterpolate_data,
                             const void * const d,
                             const char * label);
struct rinterpolate_table_t * rinterpolate_table_struct(struct rinterpolate_data_t * rinterpolate_data,
                                                        const void * const d,
                                                        const char * label);


struct rinterpolate_table_t *
rinterpolate_sort_table(struct rinterpolate_data_t * RESTRICT const rinterpolate_data,
                        struct rinterpolate_table_t * const in);

struct rinterpolate_table_t *
rinterpolate_copy_table(struct rinterpolate_data_t * RESTRICT const rinterpolate_data,
                        const struct rinterpolate_table_t * const in);

void rinterpolate_copy_table_data(const struct rinterpolate_table_t * const in,
                                  struct rinterpolate_table_t * const out);

rinterpolate_Boolean_t rinterpolate_is_table_orthogonal(
    struct rinterpolate_data_t * const rinterpolate_data,
    struct rinterpolate_table_t * const table,
    const rinterpolate_Boolean_t vb
    );

void rinterpolate_swap_columns(
    struct rinterpolate_data_t * const rinterpolate_data,
    struct rinterpolate_table_t * const table,
    const rinterpolate_counter_t i,
    const rinterpolate_counter_t j,
    const rinterpolate_Boolean_t sort);

rinterpolate_Boolean_t rinterpolate_any_column_mapped(
    struct rinterpolate_data_t * const rinterpolate_data,
    struct rinterpolate_table_t * const table);

struct rinterpolate_table_t * rinterpolate_random_table(
    struct rinterpolate_data_t * const rinterpolate_data,
    const rinterpolate_counter_t n,
    const rinterpolate_counter_t d,
    const rinterpolate_signed_counter_t * const nx,
    rinterpolate_float_t (*func)(const rinterpolate_float_t * const,
                                 const rinterpolate_counter_t,
                                 const rinterpolate_counter_t)
    );

rinterpolate_float_t rinterpolate_random_coordinate(
    const rinterpolate_counter_t col
    );
struct rinterpolate_table_t * rinterpolate_linear_table(
    struct rinterpolate_data_t * const rinterpolate_data,
    const rinterpolate_counter_t n,
    const rinterpolate_counter_t d,
    const rinterpolate_float_t * const min,
    const rinterpolate_float_t * const max,
    const rinterpolate_signed_counter_t * const nx,
    rinterpolate_float_t (*func)(const rinterpolate_float_t * const,
                                 const rinterpolate_counter_t,
                                 const rinterpolate_counter_t)
    );
struct rinterpolate_table_t * rinterpolate_map_right_columns(
    struct rinterpolate_data_t * const rinterpolate_data,
    struct rinterpolate_table_t * const table,
    const rinterpolate_Boolean_t * const columns_to_map,
    const rinterpolate_float_t * const new_resolutions,
    rinterpolate_float_t ** resolution_list);

void rinterpolate_sort_table_in_place(struct rinterpolate_data_t * RESTRICT const rinterpolate_data,
                                      struct rinterpolate_table_t * const table);


void rinterpolate_free_table_from_table(struct rinterpolate_data_t * RESTRICT const rinterpolate_data,
                                        struct rinterpolate_table_t * const table);

void rinterpolate_free_table_from_data(struct rinterpolate_data_t * RESTRICT const rinterpolate_data,
                                       void * const data);

struct rinterpolate_stats_t * rinterpolate_make_stats(
    const rinterpolate_counter_t n);

void rinterpolate_new_table_contents(
    struct rinterpolate_data_t * const rinterpolate_data,
    struct rinterpolate_table_t * const table,
    const rinterpolate_float_t * RESTRICT const data,
    const rinterpolate_counter_t n,
    const rinterpolate_counter_t d,
    const rinterpolate_counter_t l,
    const rinterpolate_counter_t cache_length,
    const rinterpolate_Boolean_t analyse
    );

void rinterpolate_free_table_ref(
    struct rinterpolate_data_t * RESTRICT const rinterpolate_data,
    struct rinterpolate_table_t * const table);

#ifdef RINTERPOLATE_NANCHECKS
rinterpolate_Boolean_t rinterpolate_check_table_for_nans(struct rinterpolate_table_t * const table);
#endif

struct rinterpolate_table_t * rinterpolate_orthogonalise_expand_table(
    struct rinterpolate_data_t * const rinterpolate_data,
    struct rinterpolate_table_t * table_in);


rinterpolate_float_t
rinterpolate_vector_absdiff_floatlist(const struct rinterpolate_vector_t * const v,
                                      const rinterpolate_float_t * const floatlist);
rinterpolate_float_t
rinterpolate_vector_diff_floatlist(const struct rinterpolate_vector_t * const v,
                                   const rinterpolate_float_t * const floatlist);
rinterpolate_Boolean_t
rinterpolate_vector_equal_floatlist_n(const struct rinterpolate_vector_t * const v,
                                      const rinterpolate_float_t * const floatlist,
                                      const rinterpolate_counter_t n);
rinterpolate_Boolean_t rinterpolate_delete_table(
    struct rinterpolate_data_t * const rinterpolate_data,
    struct rinterpolate_table_t * const table,
    rinterpolate_float_t * data
    );


#ifndef RINTERPOLATE_HAVE_NATIVE_EXP10
double exp10(double x);
#endif // RINTERPOLATE_HAVE_NATIVE_EXP10

size_t rinterpolate_write_table_to_file(struct rinterpolate_table_t * RESTRICT const table,
                                        FILE * const fp);
struct rinterpolate_table_t *rinterpolate_read_table_from_file(
    struct rinterpolate_data_t * RESTRICT const rinterpolate_data,
    FILE * const fp
    );
#endif//RINTERPOLATE_PROTOTYPES_H
