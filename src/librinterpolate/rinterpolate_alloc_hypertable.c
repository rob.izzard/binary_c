#define __BINARY_C_LINT_SKIP

#include "../binary_c_code_options.h"
#include "../binary_c_error_codes.h"
#include "../binary_c_exit_macros.h"
#include "../breakpoints/breakpoints_prototypes.h"
#include "../binary_c_exit_prototypes.h"

#ifndef __HAVE_LIBRINTERPOLATE__

#include "rinterpolate.h"
#include "rinterpolate_internal.h"

void rinterpolate_alloc_hypertable(
    struct rinterpolate_data_t * const rinterpolate_data MAYBE_UNUSED,
    struct rinterpolate_table_t * RESTRICT const table)
{
    /*
     * make space for a rinterpolate hypertable
     */
    table->hypertable = Rinterpolate_malloc(sizeof(struct rinterpolate_hypertable_t));

    if(unlikely(table->hypertable==NULL))
    {
        rinterpolate_error(RINTERPOLATE_MALLOC_FAILED,
                           "Failed to allocate hypertable in table at %p\n",
                           rinterpolate_data,
                           (void*)table);
    }

#ifdef RINTERPOLATE_DEBUG
    Rinterpolate_print("Interpolate hypertable: memory allocation\n");FLUSH;
    Rinterpolate_print("Interpolate hypertable: length %u\n",table->hypertable_length);FLUSH;
    Rinterpolate_print("Interpolate hypertable: line length sizeof %zu\n",table->line_length_sizeof);FLUSH;
    Rinterpolate_print("Interpolate hypertable: n float sizeof %zu\n",table->n_float_sizeof);FLUSH;
#endif
    table->hypertable->table = table;
    table->hypertable->data = Rinterpolate_malloc(table->hypertable_length*table->line_length_sizeof);
    table->hypertable->f = Rinterpolate_malloc(table->n_float_sizeof);
    table->hypertable->sum = Rinterpolate_calloc(1,table->sum_sizeof);

#ifdef RINTERPOLATE_DEBUG
    Rinterpolate_print("MALLOC data at %p size %zu, f at %p size %zu, sum at %p size %zu\n",
                       (void*)table->hypertable->data,
                       table->hypertable_length*table->line_length_sizeof,
                       (void*)table->hypertable->f,
                       table->n_float_sizeof,
                       (void*)table->hypertable->sum,
                       table->sum_sizeof);
#endif

    if(unlikely((table->hypertable->data==NULL)||
                (table->hypertable->f==NULL)||
                (table->hypertable->sum==NULL)))
    {
        rinterpolate_error(RINTERPOLATE_CALLOC_FAILED,
                           "Error allocating data, f or sum in rinterpolate_alloc_hypertable. Table = %p, table->parent = %p, table->hypertable = %p.\n",
                           rinterpolate_data,
                           (void*)table,
                           table ? (void*)table->parent : NULL,
                           table ? (void*)table->hypertable : NULL);
    }

#ifdef RINTERPOLATE_DEBUG
    Rinterpolate_print("table->hypertable->data alloc 2 * %u * %zu\n",table->hypertable_length,table->line_length_sizeof);
#endif

#ifdef RINTERPOLATE_USE_REALLOC
    /* remember to clear sum if it wasn't set by calloc */
    memset(table->hypertable->sum,
           0,
           table->sum_sizeof);
#endif // RINTERPOLATE_USE_REALLOC

}

#endif // __HAVE_LIBRINTERPOLATE__

typedef int prevent_ISO_C_warning;

        