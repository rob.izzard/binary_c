#define __BINARY_C_LINT_SKIP

#include "../binary_c_code_options.h"
#include "../binary_c_error_codes.h"
#include "../binary_c_exit_macros.h"
#include "../breakpoints/breakpoints_prototypes.h"
#include "../binary_c_exit_prototypes.h"

#ifndef __HAVE_LIBRINTERPOLATE__

#include "rinterpolate.h"
#include <stdarg.h>
#include "rinterpolate_internal.h"

int Gnu_format_args(2,4) rinterpolate_error(const rinterpolate_counter_t errnum,
                                            const char * RESTRICT const format,
                                            struct rinterpolate_data_t * RESTRICT const rinterpolate_data,
                                            ...)
{
    /*
     * Rinterpolate error handling function.
     *
     * We either exit with the warning passed in,
     * or call the error_handler function if it is
     * set. This way, your parent code can handle the
     * error in its standard wya.
     */
    va_list args;
    va_start(args,rinterpolate_data);

    /*
     * Use error_handler if given
     */

    if(rinterpolate_data != NULL &&
       rinterpolate_data->error_handler != NULL)
    {
        va_list args_copy;
        va_copy(args_copy,args);
        const int doexit = (*rinterpolate_data->error_handler)(rinterpolate_data->errdata,
                                                               errnum,
                                                               format,
                                                               args_copy);
        if(doexit >= 0)
        {
            vfprintf(stderr,format,args);
        }
        va_end(args_copy);
        if(doexit > 0)
        {
            Exit_binary_c_no_stardata(BINARY_C_IMPORTED_CODE_ERROR,"Error in librinterpolate in binary_c");
        }
        return doexit;
    }



    /*
     * Report an error in librinterpolate and exit
     */
    fflush(NULL);
    fprintf(stderr,
            "librinterpolate error %u: ",
            errnum);
    fprintf(stderr,format,args);
    fflush(NULL);
    va_end(args);
    Exit_binary_c_no_stardata(BINARY_C_IMPORTED_CODE_ERROR,"Error in librinterpolate in binary_c");
}

#endif // __HAVE_LIBRINTERPOLATE__

typedef int prevent_ISO_C_warning;

        