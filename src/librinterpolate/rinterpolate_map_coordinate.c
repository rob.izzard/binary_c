#define __BINARY_C_LINT_SKIP

#include "../binary_c_code_options.h"
#include "../binary_c_error_codes.h"
#include "../binary_c_exit_macros.h"
#include "../breakpoints/breakpoints_prototypes.h"
#include "../binary_c_exit_prototypes.h"

#ifndef __HAVE_LIBRINTERPOLATE__

#include "rinterpolate.h"
#include "rinterpolate_internal.h"

rinterpolate_float_t * rinterpolate_map_coordinate(
    struct rinterpolate_data_t * const rinterpolate_data,
    struct rinterpolate_table_t * const table,
    const rinterpolate_float_t * const RESTRICT x
    )
{
    const rinterpolate_counter_t vb = 0;

    /*
     * Map coordinates at x to table's real coordinates.
     * This is only to be called by mapped tables.
     *
     * Returns an array of mapped coordinates,
     * which should be freed by the caller.
     */
    rinterpolate_float_t * const newx =
        Rinterpolate_malloc(sizeof(rinterpolate_float_t) * table->n);

    /*
     * Copy in the old coordinates
     */
    memcpy(newx,
           x,
           sizeof(rinterpolate_float_t) * table->n);

    /*
     * Check if each column is mapped, interpolate
     * the new coordinates if so.
     */
    const rinterpolate_counter_t right = table->n - 1;
    for(rinterpolate_counter_t i = right;
        i > 0;
        i--)
    {
        if(table->column_is_mapped[i] == TRUE)
        {
            rinterpolate_float_t minmax[2];
            const struct rinterpolate_table_t * const t = table->min_max_table[i];
            if(vb)
            {
                printf("\nmap coord %u (map t->n=%u)\n",i,t->n);
                if(vb>=2)
                {
                    rinterpolate_show_table(rinterpolate_data,
                                            table->min_max_table[i],
                                            stdout,
                                            -1);
                }

                printf("Map col %u : ",i);
                for(rinterpolate_counter_t j = 0; j<table->n;j++)
                {
                    printf("%g ",newx[j]);
                }
                printf(" -> ");
            }

            /*
             * This should be the final column in newx
             */
            if(vb)printf("Swap cols %u %u -> ",right,i);
            rinterpolate_float_t tmp = newx[i];
            newx[i] = newx[right];
            newx[right] = tmp;

            if(vb)
            {
                printf("Map col %u : ",i);
                for(rinterpolate_counter_t j = 0; j<table->n;j++)
                {
                    printf("%g ",newx[j]);
                }
                printf(" -> interpolate min/map ");
            }

            rinterpolate(t->data,
                         rinterpolate_data,
                         t->n,
                         t->d,
                         t->l,
                         newx,
                         minmax,
                         0);

            newx[right] =
                x[i] < minmax[0] ? 0.0 :
                x[i] > minmax[1] ? 1.0 :
                (
                    Fequal(minmax[0],minmax[1]) ?
                    0.5 :
                    ((x[i] - minmax[0]) / (minmax[1] - minmax[0]))
                    );

            if(vb)printf("x[i]=%g min=%g max=%g -> ",x[i],minmax[0],minmax[1]);

            /*
             * Swap back
             */
            tmp = newx[i];
            newx[i] = newx[table->n-1];
            newx[table->n-1] = tmp;

            if(vb)
            {
                printf("newx = ");
                for(rinterpolate_counter_t j = 0; j<table->n;j++)
                {
                    printf("%g ",newx[j]);
                }
                printf("\n");
            }
        }
    }

    return newx;
}

#endif // __HAVE_LIBRINTERPOLATE__

typedef int prevent_ISO_C_warning;

        