#define __BINARY_C_LINT_SKIP

#include "../binary_c_code_options.h"
#include "../binary_c_error_codes.h"
#include "../binary_c_exit_macros.h"
#include "../breakpoints/breakpoints_prototypes.h"
#include "../binary_c_exit_prototypes.h"

#ifndef __HAVE_LIBRINTERPOLATE__

#include "rinterpolate.h"
#include "rinterpolate_internal.h"
static void _floatsort(rinterpolate_float_t * const v,
                       const rinterpolate_counter_t n);

void rinterpolate_analyse_table(struct rinterpolate_data_t * const rinterpolate_data,
                                struct rinterpolate_table_t * RESTRICT const table,
                                const char * const caller)
{
    /*
     * Slow, but more complete, table analysis function
     * for librinterpolate
     *
     *
     */
    const rinterpolate_counter_t vb = 0;

#define _vbprintf(N,...)                                                \
    if(vb>=(N))                                                         \
    {                                                                   \
        printf("Analyse table (table %p, called by %s): ",(void*)table,caller); \
        printf(__VA_ARGS__);                                            \
    }

    _vbprintf(1,
              "in called by %s: %p with n=%u d=%u l=%u, analysed? %s stats %p\n",
              caller,
              (void*)table,
              table->n,
              table->d,
              table->l,
              table->analysed ? "y" : "n",
              (void*)table->stats);

    if(table->l==0)
    {
        rinterpolate_error(RINTERPOLATE_MAP_ZERO_LINES,
                           "cannot analyse a table (%p) with zero lines of data : this is almost certainly a bug\n",
                           rinterpolate_data,
                           (void*)table
                           );
    }

    /*
     * If we're analysed, and stats exists,
     */
    if(table->analysed &&
       table->stats != NULL)
    {
        _vbprintf(1,"called by %s : Already analysed\n",caller);
        return; /* don't do it twice */
    }

    if(table->stats)
    {
        rinterpolate_clear_stats(table);
        Safe_free(table->stats);
    }

    /*
     * i == line number
     * j == column number
     */
    struct rinterpolate_stats_t * const s =
        rinterpolate_make_stats(table->n);

    _vbprintf(2,
              "allocated memory at %p %p %p %p %p\n",
              (void*)s->valuelist,
              (void*)s->nvalues,
              (void*)s->valuelist_alloced,
              (void*)s->min,
              (void*)s->max);

    /*
     * First line sets the first value in the value list,
     * and set values for min and max
     */
    for(rinterpolate_counter_t j = 0;
        j < table->n;
        j++)
    {
        s->nvalues[j] = 1;
        s->valuelist_alloced[j] = 4;
        _vbprintf(2,"Alloc valuelist %u\n",j);
        s->valuelist[j] =
            Rinterpolate_malloc(s->valuelist_alloced[j] *
                                sizeof(rinterpolate_float_t));
        if(unlikely(s->valuelist[j] == NULL))
        {
            rinterpolate_error(RINTERPOLATE_MALLOC_FAILED,
                               "Failed to malloc s->valuelist[j=%u] for table analysis of size %zu\n",
                               rinterpolate_data,
                               j,
                               s->valuelist_alloced[j] * sizeof(rinterpolate_float_t));
        }
        s->valuelist[j][0] =
            *(table->data + j);
        s->min[j] = +RINTERPOLATE_FLOAT_MAX;
        s->max[j] = -RINTERPOLATE_FLOAT_MAX;
    }
    _vbprintf(2,"allocated valuelists\n");

    if(table->l > 0)
    {
        /*
         * count number of different values
         */
        for(rinterpolate_counter_t i = 1;
            i < table->l;
            i++)
        {
            for(rinterpolate_counter_t j = 0;
                j < table->n;
                j++)
            {
                /*
                 * Check column j with given value
                 */
                bool match = FALSE;
                const rinterpolate_float_t value =
                    *(table->data +
                      i * table->line_length +
                      j);
                s->min[j] = Min(s->min[j],value);
                s->max[j] = Max(s->max[j],value);
                for(rinterpolate_counter_t k = 0;
                    match == FALSE && k < s->nvalues[j];
                    k++)
                {
                    if(Fequal(s->valuelist[j][k],value))
                    {
                        match = TRUE;
                    }
                }
                if(match == FALSE)
                {
                    /*
                     * New value in column j
                     */
                    s->nvalues[j]++;
                    if(s->nvalues[j] > s->valuelist_alloced[j])
                    {
                        s->valuelist_alloced[j] *= 2;
                        s->valuelist[j] =
                            Rinterpolate_realloc(s->valuelist[j],
                                                 sizeof(rinterpolate_float_t)*
                                                 s->valuelist_alloced[j]);
                        if(unlikely(s->valuelist[j] == NULL))
                        {
                            rinterpolate_error(RINTERPOLATE_REALLOC_FAILED,
                                               "Failed to realloc s->valuelist[j=%u] for table analysis of size %zu\n",
                                               rinterpolate_data,
                                               j,
                                               s->valuelist_alloced[j] * sizeof(rinterpolate_float_t));
                        }
                    }
                    s->valuelist[j][s->nvalues[j]-1] = value;
                }
            }
        }
    }

    /*
     * Realloc to actually used memory
     */
    for(rinterpolate_counter_t j = 0;
        j < table->n;
        j++)
    {
        if(s->nvalues[j] != s->valuelist_alloced[j])
        {
            s->valuelist[j] =
                Rinterpolate_realloc(s->valuelist[j],
                                     sizeof(rinterpolate_float_t)*s->nvalues[j]);
            if(unlikely(s->valuelist[j] == NULL))
            {
                rinterpolate_error(RINTERPOLATE_REALLOC_FAILED,
                                   "Failed to realloc s->valuelist[j=%u] for table analysis of size %zu\n",
                                   rinterpolate_data,
                                   j,
                                   s->nvalues[j] * sizeof(rinterpolate_float_t));
            }
        }
    }

    /*
     * Sort value list
     */
    for(rinterpolate_counter_t j = 0;
        j < table->n;
        j++)
    {
        _floatsort(s->valuelist[j],
                   s->nvalues[j]);
    }

    /*
     * Get the range in the next column (right) for each value
     */
    for(rinterpolate_counter_t left = 0, right = 1;
        right < table->n;
        left++,right++)
    {
        const size_t valuesize = s->nvalues[left]*sizeof(rinterpolate_float_t);
        s->valuemax[left] = Rinterpolate_malloc(valuesize);
        if(unlikely(s->valuemax[left] == NULL))
        {
            rinterpolate_error(RINTERPOLATE_MALLOC_FAILED,
                               "Failed to alloc s->valuemax[left=%u] size %zu\n",
                               rinterpolate_data,
                               left,
                               valuesize);
        }
        s->valuemin[left] = Rinterpolate_malloc(valuesize);
        if(unlikely(s->valuemin[left] == NULL))
        {
            rinterpolate_error(RINTERPOLATE_MALLOC_FAILED,
                               "Failed to alloc s->valuemin[left=%u] size %zu\n",
                               rinterpolate_data,
                               left,
                               valuesize);
        }
        for(rinterpolate_counter_t k = 0;
            k < s->nvalues[left];
            k++)
        {
            s->valuemin[left][k] = +RINTERPOLATE_FLOAT_MAX;
            s->valuemax[left][k] = -RINTERPOLATE_FLOAT_MAX;

            const rinterpolate_float_t wantleftvalue = s->valuelist[left][k];
            for(rinterpolate_counter_t i = 0;
                i < table->l;
                i++)
            {
                const rinterpolate_float_t leftvalue =
                    *(table->data +
                      i * table->line_length +
                      left);

                if(Fequal(wantleftvalue,leftvalue))
                {
                    const rinterpolate_float_t rightvalue =
                        *(table->data +
                          i * table->line_length +
                          right);
                    s->valuemin[left][k] = Min(s->valuemin[left][k],
                                               rightvalue);
                    s->valuemax[left][k] = Max(s->valuemax[left][k],
                                               rightvalue);
                }
            }
        }
    }

    /*
     * set table->stats struct
     */
    if(table->stats != NULL)
    {
        rinterpolate_clear_stats(table);
        Safe_free(table->stats);
    }
    table->stats = s;
    table->analysed = TRUE;

    return;
}

/*
 * Float sort from
 * https://codegolf.stackexchange.com/questions/24192/golfedfast-sorting-in-c
 */
static void _floatsort(rinterpolate_float_t * const v,
                       const rinterpolate_counter_t n)
{
    rinterpolate_counter_t l=0;
    rinterpolate_float_t t, *w=v, *z=v+(n-1)/2;
    if(n>0)
    {
        t=*v; *v=*z; *z=t;
        for(;++w<v+n;)
        {
            if(*w<*v)
            {
                t=v[++l]; v[l]=*w; *w=t;
            }
        }
        t=*v; *v=v[l]; v[l]=t;
        _floatsort(v, l++);
        _floatsort(v+l, n-l);
    }
}

#endif // __HAVE_LIBRINTERPOLATE__

typedef int prevent_ISO_C_warning;
