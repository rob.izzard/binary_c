#define __BINARY_C_LINT_SKIP

#include "../binary_c_code_options.h"
#include "../binary_c_error_codes.h"
#include "../binary_c_exit_macros.h"
#include "../breakpoints/breakpoints_prototypes.h"
#include "../binary_c_exit_prototypes.h"

#ifndef __HAVE_LIBRINTERPOLATE__

#include "rinterpolate.h"
#include "rinterpolate_internal.h"

#ifdef RINTERPOLATE_CACHE

void rinterpolate_resize_cache(struct rinterpolate_data_t * rinterpolate_data,
                               struct rinterpolate_table_t * RESTRICT const table,
                               const rinterpolate_counter_t cache_length)
{
    /*
     * Change the size of the rinterpolate_cache to cache_length, which
     * could be zero.
     *
     * Note that this wipes the cache in the process.
     */
    Safe_free(table->cache);
    table->cache_length = cache_length;
    if(cache_length>0)
    {
        rinterpolate_alloc_cacheline(rinterpolate_data,
                                     table);
    }
}
#endif // RINTERPOLATE_CACHE

#endif // __HAVE_LIBRINTERPOLATE__

typedef int prevent_ISO_C_warning;

        