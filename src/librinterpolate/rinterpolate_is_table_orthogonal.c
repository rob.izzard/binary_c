#define __BINARY_C_LINT_SKIP

#include "../binary_c_code_options.h"
#include "../binary_c_error_codes.h"
#include "../binary_c_exit_macros.h"
#include "../breakpoints/breakpoints_prototypes.h"
#include "../binary_c_exit_prototypes.h"

#ifndef __HAVE_LIBRINTERPOLATE__

#include "rinterpolate.h"
#include "rinterpolate_internal.h"

rinterpolate_Boolean_t rinterpolate_is_table_orthogonal(struct rinterpolate_data_t * const rinterpolate_data,
                                                        struct rinterpolate_table_t * const table,
                                                        const rinterpolate_Boolean_t vb)
{
    /*
     * Test if table is sorted and orthogonal,
     * if so return TRUE, otherwise FALSE.
     */

    rinterpolate_analyse_table(rinterpolate_data,
                               table,
                               "is table orthogonal");

    rinterpolate_Boolean_t is_orthogonal = TRUE;
    if(table->stats != NULL)
    {
        rinterpolate_counter_t * step =
            Rinterpolate_calloc(table->n,sizeof(rinterpolate_counter_t));
        const size_t vector_length = sizeof(rinterpolate_float_t) * (size_t)table->n;
        rinterpolate_float_t * vector =
            Rinterpolate_malloc(vector_length);

        rinterpolate_counter_t line = 0;
        while(line < table->l &&
              is_orthogonal == TRUE)
        {
            rinterpolate_signed_counter_t col = table->n-1;
            while(line>0 && col>=0)
            {
                step[col]++;
                if(step[col] >= table->stats->nvalues[col])
                {
                    /* wrap to next column */
                    step[col] = 0;
                    col--;
                }
                else
                {
                    break;
                }
            }

            for(rinterpolate_counter_t i = 0; i < table->n; i++)
            {
                vector[i] = table->stats->valuelist[i][step[i]];
            }

            if(vb)
            {
                printf("LINE %u : \n",line);
                printf("Steps : ");
                for(rinterpolate_counter_t i = 0; i < table->n; i++)
                {
                    printf("%u/%u ",step[i],table->stats->nvalues[col]);
                }
                printf("\nExpect: ");
                for(rinterpolate_counter_t i = 0; i < table->n; i++)
                {
                    printf("%.10e ",vector[i]);
                }
                printf("\nGot   : ");
                for(rinterpolate_counter_t i = 0; i < table->n; i++)
                {
                    printf("%.10e ",*(table->data+line*table->line_length + i));
                }
            }

            if(memcmp(table->data + line * table->line_length,
                      vector,
                      vector_length) != 0)
            {
                /*
                 * Error: incorrect values found
                 */
                is_orthogonal = FALSE;
            }

            line++;
        }

        Safe_free(step);
        Safe_free(vector);
    }
    else
    {
        is_orthogonal = FALSE;
    }

    return is_orthogonal;
}

#endif // __HAVE_LIBRINTERPOLATE__

typedef int prevent_ISO_C_warning;

        