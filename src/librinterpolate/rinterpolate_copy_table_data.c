#define __BINARY_C_LINT_SKIP

#include "../binary_c_code_options.h"
#include "../binary_c_error_codes.h"
#include "../binary_c_exit_macros.h"
#include "../breakpoints/breakpoints_prototypes.h"
#include "../binary_c_exit_prototypes.h"

#ifndef __HAVE_LIBRINTERPOLATE__

#include "rinterpolate.h"
#include "rinterpolate_internal.h"

void rinterpolate_copy_table_data(const struct rinterpolate_table_t * const in,
                                  struct rinterpolate_table_t * const out)
{
    /*
     * Copy in's data into out's data.
     *
     * note that out->data is a copy of in->data,
     * so we set auto_free_data to be TRUE
     */
    const int vb = 0;

#undef __DATA_ITEMS

#define __ALLOCATED_DATA_ITEMS                                          \
    X(hypertable->data, in->hypertable_length*in->line_length_sizeof)   \
        X(hypertable->sum, in->sum_sizeof)                              \
        X(hypertable->f, in->n_float_sizeof)                            \
        X(column_is_mapped, in->n*sizeof(rinterpolate_Boolean_t))

#define __UNALLOCATED_DATA_ITEMS                            \
        X(steps, in->n*sizeof(rinterpolate_counter_t))      \
        X(varcount, in->n*sizeof(rinterpolate_counter_t))   \
        X(cache, in->line_length*in->cache_length)

    /*
     * Copy data for which space has already been allocated
     */
    if(vb)
    {
        printf("Copy table data START in = %p (presearch %p) out = %p (presearch %p)\n",
               (void*)in,
               (void*)in->presearch,
               (void*)out,
               (void*)out->presearch);
    }

#undef X
#define X(ITEM,SIZE)                            \
    if(in->ITEM != NULL)                        \
    {                                           \
        if(vb)printf("copy table data %s\n", #ITEM);  \
        memcpy(out->ITEM,                       \
               in->ITEM,                        \
               (SIZE));                         \
    }
    X(hypertable->data, in->hypertable_length*in->line_length_sizeof);
    X(hypertable->sum, in->sum_sizeof);
    X(hypertable->f, in->n_float_sizeof);
    X(column_is_mapped, in->n*sizeof(rinterpolate_Boolean_t));

#undef X
#define X(ITEM,SIZE)                                \
    if(in->ITEM != NULL)                            \
    {                                               \
        if(vb)printf("copy %s size %zu\n",          \
                     #ITEM,                         \
                     (size_t)(SIZE));               \
        const size_t size = (SIZE);                 \
        out->ITEM = Rinterpolate_malloc(size);      \
        memcpy(out->ITEM,                           \
               in->ITEM,                            \
               size);                               \
    }

    __UNALLOCATED_DATA_ITEMS;

    /*
     * Special cases
     */
    if(in->varcount != NULL &&
       in->presearch != NULL)
    {
        if(vb)printf("copy table data in %p (presearch %p) out %p (presearch %p) \n",
                     (void*)in,
                     (void*)in->presearch,
                     (void*)out,
                     (void*)out->presearch);
        if(out->presearch)
        {
            /* clear existing presearch */
            for(rinterpolate_counter_t j=0;j<in->presearch_n;j++)
            {
                Safe_free(out->presearch[j]);
            }
            Safe_free(out->presearch);
        }
        out->presearch = Rinterpolate_calloc(in->presearch_n,
                                             sizeof(rinterpolate_float_t *));
        if(out->presearch)
        {
            out->presearch_n = in->presearch_n;
            for(rinterpolate_counter_t j=0;j<out->presearch_n;j++)
            {
                if(in->presearch[j] != NULL)
                {
                    const size_t size = in->varcount[j] * sizeof(rinterpolate_float_t);
                    out->presearch[j] = Rinterpolate_malloc(size);
                    if(vb)
                    {
                        printf("alloc presearch %u out=%p out->presearch=%p\n",
                               j,
                               (void*)out,
                               (void*)out->presearch[j]);
                    }
                    memcpy(out->presearch[j],
                           in->presearch[j],
                           size);
                }
            }
        }
    }

    for(rinterpolate_counter_t i = 0; i<in->n; i++)
    {
        if(in->min_max_table[i] != NULL)
        {
            if(vb)printf("copy min max %u : was %p\n",
                         i,
                         (void*)out->min_max_table[i]);

            out->min_max_table[i] =
                rinterpolate_copy_table(NULL,
                                        in->min_max_table[i]);
            if(vb)printf("ALLOC MINMAX %u : %p (in %p)\n",
                         i,
                         (void*)out->min_max_table[i],
                         (void*)out
                );
        }
    }

    if(in->stats != NULL)
    {
        /*
         * Copy the stats of the table
         * (faster than regenerating it)
         */
        if(out->stats)
        {
            rinterpolate_clear_stats(out);
            Safe_free(out->stats);
        }
        out->stats = rinterpolate_make_stats(out->n);

        // 1D arrays
#define _copy(X,TYPE)                                       \
        memcpy(out->stats->X,                               \
               in->stats->X,                                \
               sizeof(TYPE)*in->n);

        _copy(nvalues,rinterpolate_counter_t);
        _copy(min,rinterpolate_float_t);
        _copy(max,rinterpolate_float_t);
        _copy(valuelist_alloced,rinterpolate_counter_t);

        // 2D arrays
#undef _copy
#define _copy(X,TYPE,ALLOC_SIZER,COPY_SIZER)                        \
                                                                    \
        for(rinterpolate_counter_t col=0; col<in->n; col++)         \
        {                                                           \
            if(in->stats->X[col])                                   \
            {                                                       \
                out->stats->X[col] =                                \
                    Rinterpolate_malloc(                            \
                        sizeof(TYPE)*                               \
                        in->stats->ALLOC_SIZER[col]);               \
                memcpy(out->stats->X[col],                          \
                       in->stats->X[col],                           \
                       sizeof(TYPE)*in->stats->COPY_SIZER[col]);    \
            }                                                       \
        }


        for(rinterpolate_counter_t col=0; col<in->n; col++)
        {
            if(in->stats->valuelist[col])
            {
                out->stats->valuelist[col] =
                    Rinterpolate_malloc(
                        sizeof(rinterpolate_float_t)*
                        in->stats->nvalues[col]);
                memcpy(out->stats->valuelist[col],
                       in->stats->valuelist[col],
                       sizeof(rinterpolate_float_t)*in->stats->nvalues[col]);
            }
        }

        //_copy(valuelist,rinterpolate_float_t,valuelist_alloced,nvalues);
        _copy(valuemin,rinterpolate_float_t,nvalues,nvalues);
        _copy(valuemax,rinterpolate_float_t,nvalues,nvalues);
    }

    if(vb)
    {
        printf("copy table data END %p out->presearch %p\n",
               (void*)out,
               (void*)out->presearch);
    }
}

#endif // __HAVE_LIBRINTERPOLATE__

typedef int prevent_ISO_C_warning;

        