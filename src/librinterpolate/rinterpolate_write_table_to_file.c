#define __BINARY_C_LINT_SKIP

#include "../binary_c_code_options.h"
#include "../binary_c_error_codes.h"
#include "../binary_c_exit_macros.h"
#include "../breakpoints/breakpoints_prototypes.h"
#include "../binary_c_exit_prototypes.h"

#ifndef __HAVE_LIBRINTERPOLATE__

#include "rinterpolate.h"
#include "rinterpolate_internal.h"

#define _write_scalar(X) nwrite += fwrite(&(X),sizeof(X),1,fp)
#define _write_array(X,NITEMS)  nwrite += fwrite((X),sizeof(*(X)),(NITEMS),fp)
#define _write_chunk(X,SIZE)  nwrite += fwrite((X),(SIZE),1,fp)

size_t rinterpolate_write_table_to_file(struct rinterpolate_table_t * RESTRICT const table,
                                        FILE * const fp)
{
    /*
     * Write the passed-in table to a file
     */
    size_t nwrite = 0;
    _write_scalar(table->n);
    _write_scalar(table->d);
    _write_scalar(table->l);
    const size_t datalen = (table->n + table->d) * table->l;
    _write_scalar(datalen);
    _write_array(table->data,datalen);
    _write_scalar(table->cache_length);
    _write_scalar(table->analysed);
    _write_array(table->column_is_mapped,table->n);
    for(rinterpolate_counter_t i=0; i<table->n; i++)
    {
        const rinterpolate_Boolean_t has = table->min_max_table[i] != NULL;
        _write_scalar(has);
        if(has == TRUE)
        {
            rinterpolate_write_table_to_file(table->min_max_table[i],fp);
        }
    }
    _write_scalar(table->auto_free_data);
    const size_t label_len = table->label == NULL ? 0 : strlen(table->label);
    _write_scalar(label_len);
    if(label_len > 0)
    {
        _write_array(table->label,label_len);
    }
    return nwrite;
}

#endif // __HAVE_LIBRINTERPOLATE__

typedef int prevent_ISO_C_warning;

        