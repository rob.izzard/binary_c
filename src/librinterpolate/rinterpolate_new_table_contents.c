#define __BINARY_C_LINT_SKIP

#include "../binary_c_code_options.h"
#include "../binary_c_error_codes.h"
#include "../binary_c_exit_macros.h"
#include "../breakpoints/breakpoints_prototypes.h"
#include "../binary_c_exit_prototypes.h"

#ifndef __HAVE_LIBRINTERPOLATE__

#include "rinterpolate.h"
#include "rinterpolate_internal.h"

void rinterpolate_new_table_contents(
    struct rinterpolate_data_t * const rinterpolate_data,
    struct rinterpolate_table_t * const table,
    const rinterpolate_float_t * RESTRICT const data,
    const rinterpolate_counter_t n,
    const rinterpolate_counter_t d,
    const rinterpolate_counter_t l,
    const rinterpolate_counter_t cache_length,
    const rinterpolate_Boolean_t analyse
    )
{
    if(table)
    {
        /* set up column mapping */
        table->column_is_mapped = Rinterpolate_calloc(n,sizeof(rinterpolate_Boolean_t));

        if(table->column_is_mapped == NULL)
        {
            rinterpolate_error(RINTERPOLATE_CALLOC_FAILED,
                               "Failed to allocate table->column_is_mapped of size %zu * %zu = %zu\n",
                               rinterpolate_data,
                               (size_t)n,
                               sizeof(rinterpolate_Boolean_t),
                               n*sizeof(rinterpolate_Boolean_t));
        }

        Rinterpolate_print("SET COL MAPPED %p in table %p with data %p\n",
                           (void*)table->column_is_mapped,
                           (void*)table,
                           (void*)table->data);

        if(data)
        {
            table->data = (rinterpolate_float_t *) data;
        }

        /*
         * Set counters
         */
        table->n = n;
        table->d = d;
        table->l = l;
        table->line_length = n + d;
        table->hypertable_length = Integer_power_of_two(n);
#ifdef RINTERPOLATE_CACHE
        table->cache_length = cache_length;
#endif

        /*
         * Set sizes
         */
        table->d_float_sizeof =  sizeof(rinterpolate_float_t) * d;
        table->n_float_sizeof = sizeof(rinterpolate_float_t) * n;
        table->line_length_sizeof = table->d_float_sizeof + table->n_float_sizeof;
        table->sum_sizeof = sizeof(rinterpolate_counter_t) * table->hypertable_length;

        if(analyse == TRUE)
        {
            /* make various data */
            rinterpolate_make_steps(rinterpolate_data,
                                    table);
            rinterpolate_alloc_varcount(rinterpolate_data,
                                        table);
#ifdef RINTERPOLATE_CACHE
            rinterpolate_alloc_cacheline(rinterpolate_data,
                                         table);
#endif//RINTERPOLATE_CACHE
            rinterpolate_make_presearch(rinterpolate_data,
                                        table);
        }

        /*
         * Make hypertable
         */
        rinterpolate_alloc_hypertable(rinterpolate_data,
                                      table);

        table->min_max_table = Rinterpolate_calloc(n,
                                                   sizeof(struct rinterpolate_table_t*));

        if(table->min_max_table == NULL)
        {
            rinterpolate_error(RINTERPOLATE_CALLOC_FAILED,
                               "Failed to allocate table->min_max_table of size %zu\n",
                               rinterpolate_data,
                               sizeof(struct rinterpolate_table_t*));
        }

        Rinterpolate_print("NEW TABLE CONTENTS OUT COL MAPPED %p in table %p with data %p\n",
                           (void*)table->column_is_mapped,
                           (void*)table,
                           (void*)table->data);
    }
}

#endif // __HAVE_LIBRINTERPOLATE__

typedef int prevent_ISO_C_warning;

        