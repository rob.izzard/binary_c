#define __BINARY_C_LINT_SKIP

#include "../binary_c_code_options.h"
#include "../binary_c_error_codes.h"
#include "../binary_c_exit_macros.h"
#include "../breakpoints/breakpoints_prototypes.h"
#include "../binary_c_exit_prototypes.h"

#ifndef __HAVE_LIBRINTERPOLATE__

#include "rinterpolate.h"
#include "rinterpolate_internal.h"

#define _random_number ((rinterpolate_float_t)rand()/RAND_MAX)

rinterpolate_float_t rinterpolate_random_coordinate(
    const rinterpolate_counter_t col
    )
{
    /*
     * Return a random coordinate in column col
     *
     * Note that these cannot overlap, so any coord
     * in col 0 cannot be in col 1, etc.
     */
    return (0.9 * _random_number + 0.1) * exp10((rinterpolate_float_t)col);
}

#endif // __HAVE_LIBRINTERPOLATE__

typedef int prevent_ISO_C_warning;

        