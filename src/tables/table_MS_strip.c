#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef MAIN_SEQUENCE_STRIP

#include "../binary_star_functions/radius_stripped.h"

void table_radius_stripped(struct store_t * Restrict const store)
{
    NewDataTable_from_Array(MASSLOSS_RADIUS_TABLE_DATA,
                            store->MS_strip,
                            3,
                            1,
                            MASSLOSS_RADIUS_TABLE_LINES);

    
#ifdef MAIN_SEQUENCE_STRIP_USELOG

/* log the table */
    int i;
    double * table = store->MS_strip->data;
    for(i=0;i<MASSLOSS_RADIUS_TABLE_LINES;i++)
    {
        table[i*4+3]=log10(table[i*4+3]);
    }

#endif // MAIN_SEQUENCE_STRIP_USELOG

}
#endif//MAIN_SEQUENCE_STRIP
