#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "Karakas_ncal.h"

void table_Karakas_ncal(struct store_t * Restrict const store)
{
    //Const_data_table ncal_table[]

    NewDataTable_from_Array(
        KARAKAS_NCAL_TABLE,
        store->Karakas_ncal,
        2,
        1,
        92);
}
