#include "../binary_c.h"
No_empty_translation_unit_warning;


void table_Temmink2022(struct stardata_t * const stardata,
                       struct store_t * const store Maybe_unused)
{
    size_t lines, length, maxwidth;
    double * rawdata = load_data_as_doubles(
        stardata,
        "RLOF/Temmink_table_B2.csv",
        &lines,
        &length,
        &maxwidth,
        ',',
        1);

    Dprint("Loaded Temmink 2022 data : lines %zu length %zu maxwidth %zu\n",
           lines,length,maxwidth);

    /*
     * Convert to data we'll want to use
     */
    double * data = Malloc(sizeof(double) * lines * 8);
    for(size_t line=0; line<lines; line++)
    {
        double * d = data + line * 8;
        //double * was = d;
        double * raw = rawdata + line * maxwidth;

        /* parameters */
        *d++ = *(raw+0); /* mass_ZAMS */
        *d++ = *(raw+2); /* log R */

        /* data */
        *d++ = *(raw+6); /* q_qad */
        *d++ = *(raw+7); /* dq_qad */
        *d++ = *(raw+8); /* q_OLOF */
        *d++ = (double)line; /* counter */
        *d++ = *(raw+5); /* log Mdot_crit */
        *d++ = *(raw+9); /* q_dyn */
    }

    NewDataTable_from_Pointer(
        data,
        store->Temmink2022_RLOF,
        2,
        6,
        lines);

    Safe_free(rawdata);

}
