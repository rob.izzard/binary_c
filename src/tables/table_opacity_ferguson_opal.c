#include "../binary_c.h"
No_empty_translation_unit_warning;


#if defined OPACITY_ALGORITHMS &&               \
    defined OPACITY_ENABLE_ALGORITHM_FERGUSON_OPAL

#include "../opacity/opacity_ferguson_opal.h"

void table_opacity_ferguson_opal(struct store_t * Restrict const store)
{
    extern const unsigned char _binary_ferguson_opal_16062017_dat_start[];
    NewDataTable_from_Pointer((double*) &_binary_ferguson_opal_16062017_dat_start,
                              store->opacity_ferguson_opal,
                              TABLE_OPACITY_FERGUSON_OPAL_NPARAMS,
                              TABLE_OPACITY_FERGUSON_OPAL_NDATA,
                              TABLE_OPACITY_FERGUSON_OPAL_LINES);
}

#endif // OPACITY_ALGORITHMS
