#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef COMENV_POLYTROPES

/*
 * Include common envelope polytrope tables
 */

#include "../common_envelope/common_envelope_polytrope_2d.h"
#include "../common_envelope/common_envelope_polytrope.h"

void table_comenv_polytropes(struct store_t * Restrict const store)
{
    /* data tables from Adam Jermyn */
    NewDataTable_from_Array(TABLE_COMMON_ENVELOPE_POLYTROPE2_DATA,
                            store->comenv_polytrope_2d,
                            TABLE_COMMON_ENVELOPE_POLYTROPE2_NPARAMS,
                            TABLE_COMMON_ENVELOPE_POLYTROPE2_NDATA,
                            TABLE_COMMON_ENVELOPE_POLYTROPE2_LINES);


    extern const unsigned char _binary_common_envelope_polytrope_3d_dat_start[];
    NewDataTable_from_Pointer((double*) &_binary_common_envelope_polytrope_3d_dat_start,
                              store->comenv_polytrope_3d,
                              TABLE_COMMON_ENVELOPE_POLYTROPE3_NPARAMS,
                              TABLE_COMMON_ENVELOPE_POLYTROPE3_NDATA,
                              TABLE_COMMON_ENVELOPE_POLYTROPE3_LINES);
/*
    int i;
    double * p = store->comenv_polytrope_3d->data;
    for(i=0; i<TABLE_COMMON_ENVELOPE_POLYTROPE3_LINES; i++)
    {
        printf("% 5d : %lf %lf %lf : %lf %lf %lf\n",
               i,
               *p,
               *(p+1),
               *(p+2),
               *(p+3),
               *(p+4),
               *(p+5));
        p+=6;
    }
*/

    /*
     * Rs(Ms) table : data are set (repeatedly) later,
     *                but malloc it now
     */
    NewDataTable_from_Pointer(NULL,
                              store->comenv_polytrope_rsms,
                              1,
                              1,
                              POLYTROPE_MSRS_TABLE_LENGTH);
    store->comenv_polytrope_rsms->data =
        Malloc(sizeof(double)*POLYTROPE_MSRS_TABLE_LENGTH*2);

}
#endif
