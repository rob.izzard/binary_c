#include "../binary_c.h"
No_empty_translation_unit_warning;


#if defined NUCSYN && defined NUCSYN_S_PROCESS

#include "../nucsyn/nucsyn_s_process.h"


#define UNDECAY(A,B,C)                          \
    {                                           \
        const double y = table[j+(A)];          \
        table[j+(A)]  *=  exp(gallino_tip/(B)); \
        table[j+(C)]  +=  y-table[j+(A)];       \
    }



void table_s_process(struct store_t * Restrict const store)
{
    const int lnl =
        NUCSYN_EXTENDED_S_PROCESS_NUM_FITTED_ISOTOPES +
        NUCSYN_EXTENDED_S_PROCESS_FREE_PARAMETERS;
    extern double Aligned _binary_nucsyn_extended_s_process_dat_start[];

    /*
     * The data in the table must be COPIED prior to use
     * because we undecay it below. This is... annoying. I wish there
     * were a way to avoid this.
     */
    size_t s = sizeof(double) * lnl * NUCSYN_EXTENDED_S_PROCESS_TABLE_LINES;
    double * Aligned table = Malloc(s);
#undef memcpy
    memcpy(table,_binary_nucsyn_extended_s_process_dat_start,s);
    NewDataTable_from_Pointer(table,
                              store->s_process_Gallino,
                              NUCSYN_EXTENDED_S_PROCESS_FREE_PARAMETERS,
                              NUCSYN_EXTENDED_S_PROCESS_NUM_FITTED_ISOTOPES,
                              NUCSYN_EXTENDED_S_PROCESS_TABLE_LINES);

    /*
     * Radioactive undecay of the isotopes so we fix the
     * results from Robert Gallino's tables.
     */

    // first undecay by 3.492d+4 for M=1.5 and M=3
    int i;

    for(i=0;i<NUCSYN_EXTENDED_S_PROCESS_TABLE_LINES;i++)
    {
        // mass is table[i*lnl+1]
        // isotopes are from table[i*lnl+NUCSYN_EXTENDED_S_PROCESS_FREE_PARAMETERS]
        // to table[(i+1)*lnl-1]

        // set interpulse as f(mass) (in years)
        const double gallino_tip = Fequal(table[i*lnl+1],5.0) ? 6.250e3 : 3.492e4;

        // j is the start of the isotopes
        const int j = i * lnl + NUCSYN_EXTENDED_S_PROCESS_FREE_PARAMETERS;

        /*
         * now undecay them: these are the decay constants
         * in the envelope (tau) and intershell (tau1) respectively:
         * here we want to use the (hot) intershell value (tau1).
         * In [...] is the isotope number in the table, use j+this,
         * and the isotope number it decays to, and its name.
         * Ni59     1.08e5    1.41e5 [165] [38] Co59
         * Fe60     2.2e6     2.2e6  [76] [166] Ni60
         * Se79     9.38e4    4.59e3 [234] [19] Br79
         * Kr81     3.03e5    2.94e5 [129] [21] Br81
         * Zr93     2.16e6    2.35e5 [330] [151] Nb93
         * Tc99     3.03e5    1.8e5  [277] [225] Ru99
         * Cs135    8.29e6    4.06e6 [42]  [10] Ba135
         * Os187    stable    3.81e3 [172] [213] Re187
         *
         * The code would look like
         // Ni59 -> Co59
         // undecay
         y=table[j+165];
         table[j+165] *= exp(gallino_tip/1.41e5);
         // difference is actually [j+38]
         table[j+38] += y-table[j+165];

         * but I have written the UNDECAY macro to do it for you :)
         *
         * A,B,C are the unstable isotope number in the table, the
         * real interpulse period (in years), and the stable isotope
         * number in the table. All are given above.
         */

       UNDECAY(165,1.41e5,38);
        UNDECAY(76,2.2e6,166);
        //UNDECAY(234,4.59e3,19); // Gallino says eliminato Sep, 25, 1998
        UNDECAY(129,2.94e5,21);
        UNDECAY(330,2.35e5,151);
        UNDECAY(277,1.8e5,225);
        UNDECAY(42,4.06e6,10);
        UNDECAY(172,3.81e3,213);
    }
}

#endif


#ifdef ___NOT_YET_TESTED


    /*
     * The s-process table is too large for intel's compiler to build it.
     * You will need to do something special... but instructions are below.
     */
//#define NUCSYN_GENERATE_SBIN
#ifdef NUCSYN_GENERATE_SBIN
    /*
     * Workaround for icc (Intel's C compiler)
     *
     * generate binary data from Maria's table
     * idea taken from
     * https://balau82.wordpress.com/2012/02/19/linking-a-binary-blob-with-gcc/
     *
     * 1: compile with gcc and NUCSYN_GENERATE_SBIN defined
     *    (see above), and run binary_c for a star that has thermal pulses.
     *    This generates the file /tmp/s.bin containing a binary dump of the data table.
     *         (ignore any segfault as long as it says "file closed successfully")
     *
     * 2: clean the build with "make clean" (removes the object files)
     *
     * 3: you now have the /tmp/s.bin file, so run (from the binary_c main directory)
     *     objcopy -I binary -O elf64-x86-64 -B i386:x86-64 --redefine-sym  /tmp/s.bin src/nucsyn/nucsyn_extended_s_process_table.obin
     *    (this is for 64-bit Linux architecture - yours may be different!)
     *
     * 4: comment out the line above: "#define NUCSYN_GENERATE_SBIN"
     *
     * 5: build with icc
     *
     *
     * You may have to redefine SPROC_SYMBOL_NAME (see below)
     *
     * A version of the obin file is now saved in SVN as
     * nucsyn_extended_s_process_table.obin.bak
     * but this may not be compatible with your architecture.
     */
#ifdef __INTEL_COMPILER
    // this will fail!
    Exit_binary_c_no_stardata(1,"Cannot generate s-process data using icc :(");
#endif
    printf("Generating s-process binary data in /tmp/s.bin\n");
    double *  table[]={NUCSYN_EXTENDED_S_PROCESS_TABLE_V1}; // in nucsyn_extended_s_process.h: data from Maria
    FILE * sfp = fopen("/tmp/s.bin","w");

    if(sfp != NULL)
    {
        printf("write data...\n");fflush(stdout);
        fwrite(table,
    (sizeof(double) *
    NUCSYN_EXTENDED_S_PROCESS_TABLE_LINES*
    (NUCSYN_EXTENDED_S_PROCESS_NUM_FITTED_ISOTOPES+NUCSYN_EXTENDED_S_PROCESS_FREE_PARAMETERS)),
    1,
    sfp);
    printf("close file...\n");fflush(stdout);
    fflush(sfp);
    if(fclose(sfp))
    {
            fprintf(stderr,"fclose reported an error :(\n");
        }
    else
    {
            printf("file closed successfully\n");
        }
    }
    else
    {
       fprintf(stderr,"Failed to open /tmp/s.bin :(");
    }

    Exit_binary_c_no_stardata(BINARY_C_NORMAL_EXIT,"Exit after s-process data has been generated");
#else


//double *  table[]={NUCSYN_EXTENDED_S_PROCESS_TABLE_V1}; // in nucsyn_extended_s_process.h: data from Maria


#endif // GENERATE_SBIN


#endif  // ___NOT_YET_TESTED
