#include "../binary_c.h"
No_empty_translation_unit_warning;



void No_return test_table(struct stardata_t * Restrict const stardata,
                          struct data_table_t * Restrict const table)
{
    /*
     * Methods to test whether a data table interpolates correctly
     */


    /*
     * Worst allowed tolerance
     */
    const double tolerance = 10.0 * DBL_EPSILON;

    const size_t ll = table->nparam + table->ndata;
    printf("Test table at %p with nlines = %zu, nparam = %zu, ndata = %zu, ll = %zu, data at %p\n",
           (void*)table,
           table->nlines,
           table->nparam,
           table->ndata,
           ll,
           (void*)table->data);

    /*
     * Loop over all the parameters in the table, make
     * sure Interpolate() gives the same as the raw data
     */
    size_t nl,np,nd;
    double * param = Malloc(sizeof(double)*table->nparam);
    double * data = Malloc(sizeof(double)*table->ndata);
    double * min = Calloc(1,sizeof(double)*table->nparam);
    double * max = Calloc(1,sizeof(double)*table->nparam);

    /*
     * Find min and max
     */

    for(nl=0;nl<table->nlines;nl++)
    {
        if(nl==0)
        {
            for(np=0;np<table->nparam;np++)
            {
                min[np] = *(table->data + np);
                max[np] = *(table->data + np);
            }
        }
        else
        {
            for(np=0;np<table->nparam;np++)
            {
                double x = *(table->data + ll * nl + np);
                min[np] = Min(min[np],x);
                max[np] = Max(min[np],x);
            }
        }
    }
    for(np=0;np<table->nparam;np++)
    {
        printf("Parameter %zu : %g to %g\n",
               np,
               min[np],
               max[np]);
    }

    for(nl=0;nl<table->nlines;nl++)
    {
        printf("Line %zu/%zu\nParameters:  ",nl,table->nlines);

        for(np=0;np<table->nparam;np++)
        {
            const double p = *(table->data + nl * ll + np);
            printf("%zu = %g ",np,p);
            *(param+np) = p;
            if(np<table->nparam-1) printf(": ");
        }
        printf("\n%12s: ","Data");
        double * table_data = table->data + nl * ll + table->nparam;
        for(nd=0;nd<table->ndata;nd++)
        {
            printf("%zu = %g : ",nd,*(table_data + nd));
        }
        printf("\n%12s: ","Interpolated");
        Interpolate(table,
                    param,
                    data,
                    FALSE);
        int error = FALSE;
        for(nd=0;nd<table->ndata;nd++)
        {
            const double err = Abs_diff(*(table_data+nd),*(data+nd));
            printf("%zu = %g (%g) ",nd,*(data+nd),err);
            if(err>tolerance)
            {
                error = TRUE;
            }
            if(nd<table->ndata-1) printf(": ");
        }
        printf("\n");
        if(error > tolerance)
        {
            Exit_binary_c(BINARY_C_INTERPOLATION_ERROR,
                          "Error is greater than tolerance (%g)\n",
                          tolerance);
        }
    }

    Safe_free(param);
    Safe_free(data);
    Safe_free(max);
    Safe_free(min);
    Exit_binary_c(BINARY_C_NORMAL_EXIT,
                  "Test table exit");

}
