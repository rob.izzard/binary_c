#include "../binary_c.h"
No_empty_translation_unit_warning;


#if defined NUCSYN && defined NUCSYN_STRIP_AND_MIX

#include "table_TAMS.h"

void table_TAMS(struct store_t * Restrict const store)
{
    NewDataTable_from_Array(TABLE_TAMS_DATA,
                            store->TAMS,
                            TAMS_NPARAM,
                            TAMS_NDATA,
                            TABLE_TAMS_LINES);
}

#endif
