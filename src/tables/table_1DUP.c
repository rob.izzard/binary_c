#include "../binary_c.h"
No_empty_translation_unit_warning;


void table_1DUP(struct store_t * Restrict const store Maybe_unused)
{

#ifdef FIRST_DREDGE_UP_EVERT
#include "table_Evert_1DUP.h"
    NewDataTable_from_Array(MENV_1DUP_EVERT,
                            store->Evert_1DUP_table,
                            2,
                            1,
                            MENV_1DUP_EVERT_LINES,
        );
#endif// FIRST_DREDGE_UP_EVERT

#ifdef FIRST_DREDGE_UP_HOLLY
    {
#include "table_Holly_1DUP.h"
#include "Holly_first_DUP_depths.h"
        NewDataTable_from_Array(
            TABLE_FIRST_DUP_DATA,
            store->Holly_1DUP_table,
            NPARAM,
            NDATA,
            TABLE_FIRST_DUP_LINES
            );
        /*
         * Convert Z to log10(Z)
         */
        double * data = store->Holly_1DUP_table->data;
        int i;
        for(i=0;i<TABLE_FIRST_DUP_LINES;i++)
        {
            int offset = PARAM_METALLICITY + i*(NPARAM+NDATA);
            double * p = data + offset;
            *p = log10(*p);
        }
    }
#endif // FIRST_DREDGE_UP_HOLLY
}
