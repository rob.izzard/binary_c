
/*
 * Define TAMS_SOURCE_HOLLY to use Holly's models, 
 * or -if undefined- use Rob's older models.
 */
#define TAMS_SOURCE_HOLLY

/*
 * Define columns in the big data table and number of them.
 */
#ifdef TAMS_SOURCE_HOLLY

/*
 * Holly's models made with Stars
 */
#include "../nucsyn/nucsyn_TAMS_Holly.h"
#define TAMS_NDATA 5
#define TAMS_NPARAM 3
#else
/*
 * Rob's models made with TWIN
 */
#include "../nucsyn/nucsyn_TAMS.h"
#define TAMS_NDATA 5
#define TAMS_NPARAM 3
#endif

/*
 * Table length and memory size macros
 */
#define TAMS_NDOUBLES (TABLE_TAMS_LINES*(TAMS_NPARAM+TAMS_NDATA))
#define TAMS_TABLE_MEMSIZE (TAMS_NDOUBLES*sizeof(double))
