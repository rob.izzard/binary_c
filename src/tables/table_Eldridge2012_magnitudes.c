
#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef STELLAR_COLOURS

#include "../stellar_colours/eldridge2012_magnitudes.h"

void table_Eldridge2012_magnitudes(struct store_t * Restrict const store)
{
    NewDataTable_from_Array(ELDRIDGE2012_DATA_ARRAY,
                            store->Eldridge2012_colours,
                            2,
                            8,
                            1292);
}

#endif
