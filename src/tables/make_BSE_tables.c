#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef MAKE_BSE_TABLES

#define Parameter_loop(VAR,Min,Max,N)           \
    for(VAR = (Min);                            \
        VAR <= ((Max)+TINY);                    \
        VAR += (((Max)-(Min))/((double)(N-1))))

#define Backwards_parameter_loop(VAR,Min,Max,N) \
    for(VAR = (Max);                            \
        VAR >= ((Min)-TINY);                    \
        VAR -= (((Max)-(Min))/((double)(N-1))))


void make_BSE_tables(struct stardata_t * Restrict const stardata)
{

    printf("stardata %p\n",stardata);

    /*
     * Main sequence L,R as f(mass,metallicity,tau)
     * where tau = age / main sequence lifetime
     */

    {
        /*
         * Space for timescales, etc.
         */
        double GB[GB_ARRAY_SIZE];
        double lums[LUMS_ARRAY_SIZE];
        double tscls[TSCLS_ARRAY_SIZE];

        /* loop variables */
        double logmass,logmetallicity,logomtau;

        /*
         * Stellar information
         */
        struct star_t * star = &stardata->star[0];
        star->stellar_type = MAIN_SEQUENCE;
        star->stellar_timestep = 0.0;

        /* dummy star in star 1 */
        stardata->star[1].stellar_type = MAIN_SEQUENCE;
        stardata->star[1].mass = 0.1;
        stardata->star[1].stellar_timestep = 1e10;

        Parameter_loop(logmetallicity,
                       log10(0.0001),
                       log10(0.03),
                       20)
        {
            double metallicity = exp10( logmetallicity);
            metallicity = Min(0.03,metallicity);

            /*
             * Update fitting parameters with new metallicity
             */
            stardata->common.metallicity = metallicity;
            set_metallicity_parameters(stardata);

            Parameter_loop(logmass,log10(0.1),log10(100.0),50)
            {
                double mass = exp10(logmass);
                star->mass = mass;

                /*
                 * Update stellar lifetimes for this combination
                 * of mass and metallicity.
                 */
                struct star_t * s = New_clear_star;
                s->phase_start_mass = mass;
                s->mass = mass;
                s->stellar_type = MAIN_SEQUENCE;
                s->timescales = tscls;
                s->luminosities = lums;
                s->GB = GB;

                /*
                 * Calculate timescales
                 */
                call_stellar_timescalesvar(s);

                /*
                 * Perturb mass, see if timescales or luminosities changerg, if so, fail
                 */
                {
                    struct star_t * spert = New_clear_star;
                    copy_star(s,spert);

                    spert->mass = 0.95 * spert->phase_start_mass;
                    spert->core_mass[CORE_He] = 0.95 * spert->core_mass[CORE_He];
                    star->mass = spert->mass;
                    call_stellar_timescalesvar(spert);

                    int i;
                    for(i=T_MS; i<=T_TMCMAX; i++)
                    {
                        if(!Fequal(s->timescales[i],spert->timescales[i]))
                        {
                            printf("Timescale %d should be %g but is %g with mass changed\n",
                               i,
                               s->timescales[i],
                               spert->timescales[i]);

                            Exit_binary_c(2,"Timescales changed when mass or core mass changed : fail");
                        }
                    }
                }

                {
                    printf("TABLE_TIMESCALES_H %g %g ",
                           logmetallicity,
                           logmass);
                    int i;
                    for(i=0; i<TSCLS_ARRAY_SIZE; i++)
                    {
                        printf("%g ",log10(Max(1e-50,s->timescales[i])));
                        if(s->timescales[i] < 0.0)
                        {
                            Exit_binary_c(2,
                                          "timescales[%d] < 0 yet we are taking the log : this is not good!\n",
                                          i,s->timescales[i]);
                        }
                    }
                    for(i=0; i<LUMS_ARRAY_SIZE; i++)
                    {
                        printf("%g ",log10(Max(1e-50,s->luminosities[i])));
                        if(s->luminosities[i] < 0.0)
                        {
                            Exit_binary_c(2,
                                          "luminosities[%d] = %g < 0 yet we are taking the log : this is not good!\n",
                                          i,s->luminosities[i]);
                        }
                    }

                    for(i=0; i<GB_ARRAY_SIZE; i++)
                    {
                        printf("%g ",log10(Max(1e-50,s->GB[i])));
                        if(s->GB[i] < 0.0)
                        {
                            Exit_binary_c(2,
                                          "GB[%d] = %g < 0 yet we are taking the log : this is not good!\n",
                                          i,s->GB[i]);
                        }
                    }
                    printf("\n");

                }
                /*
                 * Not required : can use the table below instead
                 * although it's likely slower
                 */

                /*
                  printf("TABLE_MS_L_ZAMS_TAMS %g %g %g %g %g %g\n",
                  logmetallicity,
                  logmass,
                  log10(s->luminosities[L_ZAMS]),
                  log10(s->luminosities[L_END_MS]),
                  log10(s->luminosities[L_BAGB]),
                  log10(s->luminosities[L_HE_IGNITION])
                  );
                */

                /*
                 * (log) Loop over 1-tau to zoom in on
                 * the end of the MS where the hook appears
                 */
                Backwards_parameter_loop(logomtau,log10(1e-4),log10(1.0),101)
                {
                    double tau = 1.0 - exp10(logomtau);
                    /*
                     * Construct age from fractional age
                     */
                    s->age = tau * s->timescales[T_MS];

                    /*
                     * Call BSE function to calculate L,R
                     */
                    stellar_structure_BSE_given_timescales(s,
                                          star,
                                          stardata,
                                          STELLAR_STRUCTURE_CALLER_make_BSE_tables);

                    printf("TABLE_MS_LR %g %g %g %g %g\n",
                           logmetallicity,
                           logmass,
                           tau,
                           log10(s->luminosity),
                           log10(s->radius)
                        );


                    if(isnan(s->luminosity) ||
                       isnan(s->radius))
                    {
                        Exit_binary_c(2,"NAN after MS structure calc");
                    }
                }

                Safe_free(s);
            }
        }
    }


    Exit_binary_c(0,"Exit from make_BSE_tables");
}

#endif
