#include "../binary_c.h"
No_empty_translation_unit_warning;


#if defined NUCSYN && defined NUCSYN_SIGMAV_PRE_INTERPOLATE


void table_sigmav(struct store_t * store)
{

    /*
     * Pre-make a table of sigma-v values for interpolation, rather than
     * calculate them each time
     */
    Reaction_rate_index i;

    const size_t table_width = (size_t)(1 + SIGMAV_SIZE);

    Reaction_rate * const data =
        Malloc((size_t)
               sizeof(Reaction_rate)
               *table_width
               *2*NUCSYN_SIGMAV_INTERPOLATION_RESOLUTION);

    /*
     * Loop through t9 values and set the sigmav values
     */
    double t9 = NUCSYN_SIGMAV_INTERPOLATION_MIN;
    double dt9 =
        (NUCSYN_SIGMAV_INTERPOLATION_MID - NUCSYN_SIGMAV_INTERPOLATION_MIN)/
        (double)(NUCSYN_SIGMAV_INTERPOLATION_RESOLUTION-1);
    int location = 0;
    i=0;

    while(t9 <= NUCSYN_SIGMAV_INTERPOLATION_MAX*(1.0 + TINY))
    {
        const double logT = log10(t9);
        Reaction_rate * p = data + i * table_width;
        i++;

        /* set the first element to t9 */
#ifdef NUCSYN_SIGMAV_INTERPOLATE_LOGT9
        *(p++) = logT;
#else
        *(p++) = t9;
#endif
        nucsyn_set_sigmav(NULL,
                          store,
                          -(logT+9.0), // log10(T)
                          p, // fill at next element
                          NULL // no multipliers!
#ifdef RATES_OF_AMANDA_NE22PG_FIX
                          /* this should be fixed! */
                          ,FALSE
#endif
            );

#ifdef NUCSYN_SIGMAV_INTERPOLATE_LOGSIGMAV
        /* convert sigmav to log10(sigmav) */
        for(size_t j=0; j<SIGMAV_SIZE; j++)
        {
            *(p+j) = log10(*(p+j));
        }
#endif // NUCSYN_SIGMAV_INTERPOLATE_LOGSIGMAV

        if(location == 0 && t9 > NUCSYN_SIGMAV_INTERPOLATION_MID)
        {
            /*
             * Change resolution above 0.13
             */
            location = 1;
            dt9 = (double)(NUCSYN_SIGMAV_INTERPOLATION_MAX-NUCSYN_SIGMAV_INTERPOLATION_MID)/
                (double)(NUCSYN_SIGMAV_INTERPOLATION_RESOLUTION-1);
        }
        t9 += dt9;
    }

    NewDataTable_from_Pointer(
        data,
        store->sigmav,
        1,
        SIGMAV_SIZE,
        NUCSYN_SIGMAV_INTERPOLATION_RESOLUTION
        );
}


#endif
