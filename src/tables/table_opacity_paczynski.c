#include "../binary_c.h"
No_empty_translation_unit_warning;


#if defined OPACITY_ALGORITHMS &&               \
    defined OPACITY_ENABLE_ALGORITHM_PACZYNSKI

void table_opacity_paczynski(struct store_t * Restrict const store)
{

#define kap store->kap_paczynski
#include "../opacity/set_opacity_paczynski.h"

}

#endif // OPACITY_ALGORITHMS
