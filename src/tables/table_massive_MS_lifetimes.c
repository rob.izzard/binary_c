#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef USE_2012_MAIN_SEQUENCE_LIFETIMES_TABLE
#include "../zfuncs/MS_ages.h"


void table_massive_MS_lifetimes(struct store_t * Restrict const store)
{
    NewDataTable_from_Array(MAIN_SEQUENCE_AGES_TABLE_DATA,
                            store->massive_MS_lifetimes,
                            2,
                            1,
                            MAIN_SEQUENCE_AGES_TABLE_LINES);
}
#endif
