#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "timestep_debug.h"

void timestep_CHeB(Timestep_prototype_args)
{
    if(stardata->preferences->stellar_structure_algorithm == STELLAR_STRUCTURE_ALGORITHM_MINT
#ifdef MINT
       &&
       MINT_has_table(MINT_TABLE_CHeB_TA)
#endif//MINT
        )
    {
#ifdef MINT
        double phase_lifetime;
        MINT_CHeB_TA(stardata,
                     star->phase_start_mass,
                     &phase_lifetime,
                     NULL);

        Limit_timestep(*dt,
                       stardata->preferences->timestep_multipliers[DT_LIMIT_CHeB]*phase_lifetime,
                       star,
                       DT_LIMIT_CHeB);
#endif//MINT
    }
    else
    {
#ifdef BSE
        if(Use_timestep(T_HE_BURNING))
        {
            Limit_timestep(*dt,
                           stardata->preferences->timestep_multipliers[DT_LIMIT_CHeB]*tscls[T_HE_BURNING],
                           star,
                           DT_LIMIT_CHeB);
        }
#ifdef HRDIAG
        stardata->model.hrdiag_dt_guess = HRDIAG_FRAC* tscls[T_HE_BURNING];
#ifdef HRDIAG_DTLOG
        printf("HRDT (deltat) CHeB %g\n",stardata->model.hrdiag_dt_guess);
#endif // HRDIAG_DTLOG
        HRDIAG_DT_NEGATIVE_CHECK;
#endif // HRDIAG
    }

    double accretion_rate = Mdot_net(star);
    if(Is_zero(accretion_rate))
    {
        /*
         * Make sure there's at least 1000 years
         * remaining
         */
        *time_remaining =
            Max(1e-3,tscls[T_HE_BURNING] + tscls[T_HE_IGNITION] - age);
    }
    else
    {
        /*
         * Accretion CHeB star has an unknown lifetime,
         * because it depends on the accretion timescale,
         * which is generally unknown. Assume the mass
         * transfer rate timescale limiter will resolve
         * the timescale.
         */
        *time_remaining = LONG_TIME_REMAINING;
    }
#endif
}
