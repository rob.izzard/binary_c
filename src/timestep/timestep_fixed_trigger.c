#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "timestep_debug.h"

Boolean timestep_fixed_trigger(struct stardata_t * Restrict const stardata,
                               const int i)
{
    /*
     * Check if fixed timestep i should be triggered.
     * Return TRUE if it should be, FALSE otherwise.
     *
     * If the timestep is disabled, return FALSE.
     *
     * If we're beyond the end time, or before the start time,
     * this can never be true.
     *
     * If we're at max_evolution_time and t->final is TRUE, then return TRUE.
     */
    Set_debug_dt;
    Boolean ret;
    struct binary_c_fixed_timestep_t * const t = stardata->model.fixed_timesteps + i;

    if(t->enabled == FALSE)
    {
        Dprint("at t=%30.20e fixed_timestep %d is disabled\n",
               stardata->model.time,
               i);
        ret = FALSE;
    }
    else
    {
        const double T = t->logarithmic ? log10(stardata->model.time) : stardata->model.time;

        if(!In_range(T,t->begin,t->end))
        {
            /*
             * Out of time range of the trigger so cannot trigger
             */
            ret = FALSE;
        }
        else if(t->final == TRUE &&
                Times_are_equal(stardata->model.time,
                                stardata->model.max_evolution_time))
        {
            /*
             * Final timestep forced trigger.
             */
            ret = TRUE;
        }
        else
        {
            /*
             * Check if the time equals, or is more than, the next
             * trigger time
             */
            ret = Boolean_(Times_more_or_equal(T,t->next));

            /*
             * Check we haven't overrun... this is a bug if we have.
             */
            if(ret==TRUE && !Times_are_equal(T,t->next))
            {
                Exit_binary_c(BINARY_C_OUT_OF_RANGE,
                              "error fixed timestep %d missed its trigger : T = %30.20e > next %30.20e ! (diff %g, absdiff %g, epsilon %g) Times_are_equal = %s : >? %s\n",
                              i,
                              T,
                              t->next,
                              T - t->next,
                              Abs_diff(T,t->next),
                              stardata->preferences->timestep_epsilon,
                              Yesno(Times_are_equal(T,t->next)),
                              Yesno(T>t->next)
                    );
            }
        }
        Dprint("at t=%30.20e T=%30.20e (enabled? %d log? %d, next %30.20e, diff %g, begin %g, end %g) trigger? %d\n",
               stardata->model.time,
               isinf(fabs(T)) ? -1e10 : T,
               t->enabled,
               t->logarithmic,
               t->next,
               isinf(fabs(T - t->next)) ? -1e10 : (T-t->next),
               t->begin,
               t->end,
               ret);
    }

    return ret;
}
