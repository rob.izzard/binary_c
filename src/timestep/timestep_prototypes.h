#pragma once
#ifndef TIMESTEP_PROTOTYPES_H
#define TIMESTEP_PROTOTYPES_H

#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "timestep.h"

void stellar_timestep(Timestep_prototype_args);
void timestep_MS(Timestep_prototype_args);
void timestep_HG(Timestep_prototype_args);
void timestep_FGB(Timestep_prototype_args);
void timestep_CHeB(Timestep_prototype_args);
void timestep_EAGB(Timestep_prototype_args);
void timestep_TPAGB(Timestep_prototype_args);
void timestep_HeMS(Timestep_prototype_args);
void timestep_HeHG_HeGB(Timestep_prototype_args);
void timestep_massless_remnant(Timestep_prototype_args);
void timestep_other(Timestep_prototype_args);
void timestep_limits(Timestep_prototype_args);
void timestep_logging(Timestep_prototype_args);
void timestep_modulation(Timestep_prototype_args);
void timestep_hard_limits(Timestep_prototype_args);
void timestep_RLOF(Timestep_prototype_args);
void nonstellar_timestep(struct stardata_t * const stardata,
                         double * const dt);

#ifdef DISCS
double Pure_function timestep_disc(struct stardata_t * const stardata,
                                   struct disc_t * const disc);
#endif // DISCS

void setup_fixed_timesteps(struct stardata_t * Restrict const stardata);
void timestep_fixed_timesteps(struct stardata_t * const stardata,
                              struct star_t * const star,
                              double * const dt);
void timestep_increment_fixed_timesteps(struct stardata_t * Restrict const stardata);
Boolean timestep_fixed_trigger(struct stardata_t * Restrict stardata,
                               const int i);
void timestep_set_default_multipliers(struct preferences_t * Restrict const preferences);

Boolean modulate_zoomfac(struct stardata_t * Restrict const stardata,
                         const unsigned int action);
double target_times_next(struct stardata_t * const stardata);

Boolean timestep_trigger_any_fixed_timestep(struct stardata_t * const stardata);

#endif // TIMESTEP_PROTOTYPES_H
