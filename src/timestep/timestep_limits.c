#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "timestep_debug.h"

#if defined NUCSYN && defined NUCSYN_ANGELOU_LITHIUM
#include "../nucsyn/nucsyn_angelou.h"
#endif

//#define SECOND_DERIVATIVES
#define SECOND_DERIVATIVE_F 0.1

void timestep_limits(Timestep_prototype_args)
{
    /*
     * Perhaps moderate the timestep depending on the solver?
     */
    /*
      const double fsolver =
      stardata->preferences->solver == SOLVER_FORWARD_EULER ? 1.0 :
      stardata->preferences->solver == SOLVER_RK2 ? 1.0 :
      stardata->preferences->solver == SOLVER_RK4 ? 1.0 :
      stardata->preferences->solver == SOLVER_PREDICTOR_CORRECTOR ? 1.0 :
      1.0;
    */
    const double dt_in = *dt;
    const double fsolver = stardata->preferences->timestep_solver_factor;

    if(Is_zero(fsolver))
    {
        Exit_binary_c(BINARY_C_OUT_OF_RANGE,
                      "fsolver (stardata->preferences->timestep_solver_factor) must be > 0\n");
    }

#ifdef SECOND_DERIVATIVES
    struct star_t * prevstar =
        &stardata->previous_stardata->star[star->starnum];
#endif

#define Second_derivative(N) (                  \
        (star->derivative[(N)]                  \
         -                                      \
         prevstar->derivative[(N)])/            \
        stardata->model.dt                      \
        )


    /*
     * Limit timestep to prevent too much angular
     * momentum change (<0.5%) by magnetic braking.
     *
     * The original BSE code took 3% changes as the maximum,
     * but this causes oscillation with the tides.
     */
    if(Use_timestep(DT_LIMIT_STELLAR_MAGNETIC_BRAKING) &&
       stellar_type<HeWD &&
       fabs(star->derivative[DERIVATIVE_STELLAR_ANGMOM_MAGNETIC_BRAKING])>1e-30 &&
       star->angular_momentum > MIN_ANGMOM_FOR_TIMESTEP_AND_REJECTION_TESTS)
    {
        const double dtmb =
            fsolver * stardata->preferences->timestep_multipliers[DT_LIMIT_STELLAR_MAGNETIC_BRAKING] * star->angular_momentum * 1e-6 /
            Max(1e-50,fabs(star->derivative[DERIVATIVE_STELLAR_ANGMOM_MAGNETIC_BRAKING]));
        Dprint("dtmb = %g from fsolver=%g * fmult=%g * J=%g / dJ/dt=%g\n",
               dtmb,
               fsolver,
               stardata->preferences->timestep_multipliers[DT_LIMIT_STELLAR_MAGNETIC_BRAKING],
               star->angular_momentum,
               star->derivative[DERIVATIVE_STELLAR_ANGMOM_MAGNETIC_BRAKING]);

        Limit_timestep(*dt,dtmb,star,DT_LIMIT_STELLAR_MAGNETIC_BRAKING);
    }

    if(Use_timestep(DT_LIMIT_STELLAR_MASS_LOSS)
       &&
       stellar_type<HeWD)
    {
        /*
         * Limit timestep to prevent mass changing by > 1%
         */
        const double mdot = fabs(star->derivative[DERIVATIVE_STELLAR_MASS]);
        if(mdot>1e-14)
        {
            /* representative mass to be resolved */
            const double m = Min(0.1*envelope_mass(star),0.01*star->mass);

            if(m > 0.01)
            {
                /* hence mass loss timescale */
                double tmdot = fsolver * stardata->preferences->timestep_multipliers[DT_LIMIT_STELLAR_MASS_LOSS] * m/Max(1e-50,mdot) * 1e-6;

                /* resolve mass loss */
                Limit_timestep(*dt,tmdot,star,DT_LIMIT_STELLAR_MASS_LOSS);
            }
        }
    }

    if(Use_timestep(DT_LIMIT_THERMAL)
       &&
       stellar_type<HeWD)
    {
        Limit_timestep(*dt,1e-6*star->tkh,star,DT_LIMIT_THERMAL);
    }

    /*
     * Limit timestep to prevent angular momentum changing by > 1%
     */
    if(Use_timestep(DT_LIMIT_STELLAR_ANGMOM))
    {
        const double jdot = fabs(star->derivative[DERIVATIVE_STELLAR_ANGMOM]);

        if(jdot>1e-20 && star->angular_momentum > MIN_ANGMOM_FOR_TIMESTEP_AND_REJECTION_TESTS)
        {
            const double dtj = fsolver *
                stardata->preferences->timestep_multipliers[DT_LIMIT_STELLAR_ANGMOM] *
                star->angular_momentum/Max(1e-50,jdot) * 1e-6;
            Limit_timestep(*dt,dtj,star,DT_LIMIT_STELLAR_ANGMOM);

            Dprint("Jdotstar%d %g (wind %g %g RLOF %g %g tides %g MB %g) J = %g -> timescale %g y -> dtj = %g Myr (multiplier %g)\n",
                   star->starnum,
                   jdot,
                   star->derivative[DERIVATIVE_STELLAR_ANGMOM_WIND_LOSS],
                   star->derivative[DERIVATIVE_STELLAR_ANGMOM_WIND_GAIN],
                   star->derivative[DERIVATIVE_STELLAR_ANGMOM_RLOF_LOSS],
                   star->derivative[DERIVATIVE_STELLAR_ANGMOM_RLOF_GAIN],
                   star->derivative[DERIVATIVE_STELLAR_ANGMOM_TIDES],
                   star->derivative[DERIVATIVE_STELLAR_ANGMOM_MAGNETIC_BRAKING],
                   star->angular_momentum,
                   star->angular_momentum/jdot,
                   dtj,
                   stardata->preferences->timestep_multipliers[DT_LIMIT_STELLAR_ANGMOM]
                );

            /*
             * Resolve second derivative
             */
#ifdef SECOND_DERIVATIVES
            if(Use_timestep(DT_LIMIT_STELLAR_ANGMOM2))
            {
                const double dtj2 = 1e-6 * fsolver * stardata->preferences->timestep_multipliers[DT_LIMIT_STELLAR_ANGMOM2] * SECOND_DERIVATIVE_F *
                    fabs(star->derivative[DERIVATIVE_STELLAR_ANGMOM] /
                         Second_derivative(DERIVATIVE_STELLAR_ANGMOM));
                Limit_timestep(*dt,dtj2,star,DT_LIMIT_STELLAR_ANGMOM2);
            }
#endif
        }
    }


    /*
     * Mass gain: this usually requires that we are
     * in a binary, but artificial accretion could
     * also be set, so check it here.
     */

    {
        const double accretion_rate = Mdot_gain(star);

        if(Is_not_zero(accretion_rate))
        {
            if(Use_timestep(DT_LIMIT_MASS_GAIN))
            {
                const double dtlim = fsolver * 1e-6 *
                    stardata->preferences->timestep_multipliers[DT_LIMIT_MASS_GAIN] *
                    star->mass / accretion_rate;
                Limit_timestep(*dt,
                               dtlim,
                               star,
                               DT_LIMIT_MASS_GAIN);
            }

#ifdef SECOND_DERIVATIVES
            if(Use_timestep(DT_LIMIT_MASS_GAIN2))
            {
                const double dtlim2 =
                    stardata->preferences->timestep_multipliers[DT_LIMIT_MASS_GAIN2] *
                    SECOND_DERIVATIVE_F * Second_derivative(DT_LIMIT_MASS_GAIN) * 1e-6;
                Limit_timestep(*dt,
                               dtlim2,
                               star,
                               DT_LIMIT_MASS_GAIN2);
            }
#endif//SECOND_DERIVATIVES
        }
    }


    if(System_is_binary)
    {
        /*
         * Resolve stellar tidal changes
         */
        if(fabs(star->omega) > 1e-20)
        {
            /*
             * Estimate the tidal timescale from
             * omega / domega/dt and e / de/dt
             */
            if(Use_timestep(DT_LIMIT_TIDES))
            {
                const double ttid_omega =
                    fabs(star->omega) /
                    Max(1e-50,fabs(star->derivative[DERIVATIVE_STELLAR_ANGULAR_VELOCITY_TIDES]))
#ifdef EXPONENTIAL_TIDES
                    * 1.0
#else
                    /* ad-hoc term to take into account tidal strength */
                    * Min(1.0,1.0/Pow3d4(stardata->preferences->tidal_strength_factor))
#endif
                    ;

                /*
                 * Hence the tidal timescale in y
                 */
                const double tidal_timescale = Min3(1e100,
                                                    stardata->common.orbit.tcirc,
                                                    ttid_omega);

                const double dt_tides =
                    envelope_mass(star) < 0.01 ?
                    1e100 :
                    (fsolver * stardata->preferences->timestep_multipliers[DT_LIMIT_TIDES] * tidal_timescale * 1e-6);
                Dprint("DT TIDES %d at %35.20e model %d : omega %g, v_eq %g, v_sph %g,  circ %g -> %g Myr \n",
                       star->starnum,
                       stardata->model.time,
                       stardata->model.model_number,
                       ttid_omega,
                       star->v_eq,
                       star->v_sph,
                       stardata->common.orbit.tcirc,
                       dt_tides
                    );

                Limit_timestep(*dt,dt_tides,star,DT_LIMIT_TIDES);
            }

#ifdef SECOND_DERIVATIVES
            if(Use_timestep(DT_LIMIT_TIDES2))
            {
                const double dt_tides2 = fsolver * stardata->preferences->timestep_multipliers[DT_LIMIT_TIDES2] * SECOND_DERIVATIVE_F *
                    fabs(star->derivative[DERIVATIVE_STELLAR_ANGULAR_VELOCITY_TIDES] /
                         Second_derivative(DERIVATIVE_STELLAR_ANGULAR_VELOCITY_TIDES));
                Limit_timestep(*dt,dt_tides2,star,DT_LIMIT_TIDES2);
                Dprint("DT second derivative\n");
            }
#endif

            /*
             * Also try to resolve changes in the stellar angular velocity
             * becauses of stellar evolution (i.e. changes in stellar radius).
             */
            if(Use_timestep(DT_LIMIT_RADIUS_CHANGES) &&
               Is_not_zero(star->derivative[DERIVATIVE_STELLAR_RADIUS]))
            {
                const double dtR = fabs(star->radius / (star->derivative[DERIVATIVE_STELLAR_RADIUS]));
                const double dt_stellar_radius_changes = fsolver *
                    stardata->preferences->timestep_multipliers[DT_LIMIT_RADIUS_CHANGES] *
                    Max(1e-50,dtR) * 1e-6;

                Dprint("check radius limit star %d type %d : R = %g, prevR = %g, dt = %g, dR/dt = %g\n",
                       star->starnum,
                       star->stellar_type,
                       star->radius,
                       stardata->previous_stardata->star[star->starnum].radius,
                       stardata->model.dt,
                       star->derivative[DERIVATIVE_STELLAR_RADIUS]);
                if(Limit_timestep(*dt,
                                  dt_stellar_radius_changes,
                                  star,
                                  DT_LIMIT_RADIUS_CHANGES))
                {
                    Dprint("Radius limited R = %g, dR/dt=%g -> dtR = %g\n",
                           star->radius,
                           star->derivative[DERIVATIVE_STELLAR_RADIUS],
                           dtR);
                }
                else
                {
                    Dprint("Radius not limited R = %g, dR/dt=%g -> dtR = %g\n",
                           star->radius,
                           star->derivative[DERIVATIVE_STELLAR_RADIUS],
                           dtR);
                }
            }

#ifdef __LOGGING
            if(star->starnum==1)
            {
                printf("dt_tides %g omega = %g vs omegadot = %g vs omega_orb %g : dt_tides %g\n",
                       stardata->model.time,
                       fabs(star->omega),
                       Max(1e-50,fabs(star->derivative[DERIVATIVE_STELLAR_ANGULAR_VELOCITY_TIDES])),
                       stardata->common.orbit.angular_frequency,
                       dt_tides
                    );
            }
#endif // __LOGGING
        }

        timestep_RLOF(Timestep_call_args);

        /*
         * Check whether the star has novae
         */
#ifdef NOVA_DEBUG
        struct star_t * const donor = Other_star_struct(star);
#endif // NOVA_DEBUG
        if(stardata->preferences->individual_novae == TRUE &&
           WHITE_DWARF(star->stellar_type) &&
           (star->nova != NOVA_STATE_NONE ||
            stardata->model.time < stardata->model.nova_timeout))
        {
            /*
             * When novae are switched on and off because the system
             * becomes occasionally detached it's hard to know
             * dtnova (because mdot is sometimes zero).
             *
             * If dtnova is zero, this is the first time, then assume
             * a mass transfer rate of 1e-9 Msun/yr if nothing else.
             *
             * Note that nova_recurrence_time returns in years.
             */
            const double accretion_rate = Mdot_gain(star);
            Time dtnova = 1e-6 * nova_recurrence_time(star,
                                                      accretion_rate,
                                                      star->mass);


            if(dtnova > 1e20 && Is_not_zero(star->nova_recurrence_time))
            {
                /*
                 * Nova system that has become disconnected,
                 * use the previous dtnova
                 */
                dtnova = 1e-6 * star->nova_recurrence_time;
            }
            else
            {
                /*
                 * time is short : use it to set te recurrence time
                 */
#ifdef NOVA_DEBUG
                printf("H-nova recurrence_time %g y (timestep_limits) accretion rate %g\n",
                       1e6*dtnova,
                       accretion_rate);
#endif // NOVA_DEBUG
                star->nova_recurrence_time = dtnova;
            }
#ifdef NOVA_DEBUG
            Dprint("H-nova DTNOVA t=%g (timeout=%g) dtnova = %g Myr (time since prev nova %g), nova state %s, star %d, stellar type %d, donor type %d : donor R/RL=%g, accretor R/RL=%g\n",
                   stardata->model.time,
                   stardata->model.nova_timeout,
                   dtnova,
                   stardata->model.time - star->time_prev_nova,
                   nova_state_strings[star->nova],
                   star->starnum,
                   star->stellar_type,
                   donor->stellar_type,
                   donor->radius/donor->roche_radius,
                   star->radius/star->roche_radius
                );
#endif // NOVA_DEBUG

            /*
             * Hence the timestep limit in Myr
             */
#ifdef KEMP_NOVAE
            const Time dtlim =
                Max(1e-3,
                    stardata->preferences->timestep_multipliers[DT_LIMIT_NOVAE] *
                    dtnova);
#else
            /* not KEMP_NOVAE */
            const Time dtlim =
                stardata->preferences->timestep_multipliers[DT_LIMIT_NOVAE] *
                dtnova;
#endif // KEMP_NOVAE

            /*
             * Modulate to combine multiple novae
             *
             * The factor f is applied only if we have already had more than
             * nova_timestep_accelerator_num novae.
             *
             * If accelerator_num <= 0, f = 1.0.
             *
             * If Nnovae < accelerator_num, f = 1.0.
             *
             * Then, if acclerator_max > 0.0 we have:
             *
             * f = Max(accelerator_max, pow(Nnovae, accelerator_index))
             *
             * otherwise
             *
             * f = pow(Nnovae, accelerator_index).
             */
            const double f =

                stardata->preferences->nova_timestep_accelerator_num < +TINY ?
                /* num < 0.0 : f = 1.0 */
                1.0 :
                (
                    /* the cap is LARGE_FLOAT (i.e. very large) if acclerator_max <= 0.0 */
                    Max(
                        (stardata->preferences->nova_timestep_accelerator_max < +TINY ?
                         LARGE_FLOAT :
                         stardata->preferences->nova_timestep_accelerator_max),

                        /* check we've had sufficient novae */
                        (star->num_novae > stardata->preferences->nova_timestep_accelerator_num ?
                         /* apply the pow(Nnovae, index) */
                         pow(star->num_novae,stardata->preferences->nova_timestep_accelerator_index) :
                         /* or don't */
                         1.0)
                        )
                    );

            /*
             * limit timestep because of novae
             */
            Limit_timestep(*dt,
                           dtlim*f,
                           star,
                           DT_LIMIT_NOVAE);
        }

        if(star->starnum == 0)
        {
            /*
             * Orbital limits
             */

            /*
             * Limit timestep to prevent angular momentum changing by > 1%
             */
            if(Use_timestep(DT_LIMIT_ORBITAL_ANGMOM))
            {
                const double jdot = fabs(stardata->model.derivative[DERIVATIVE_ORBIT_ANGMOM]);
                if(jdot>1e-20 && stardata->common.orbit.angular_momentum > MIN_ANGMOM_FOR_TIMESTEP_AND_REJECTION_TESTS)
                {
                    /*
                     * factor to phase in shrinkage of the timestep
                     * as the separation drops below 1.0
                     */
                    const double fclose = stardata->common.orbit.separation < 1.0 ?
                        Max(1e-5,stardata->common.orbit.separation) : 1.0;
                    const double dtj =
                        stardata->preferences->timestep_multipliers[DT_LIMIT_ORBITAL_ANGMOM] // default = 0.01
                        * fsolver * fclose * stardata->common.orbit.angular_momentum/
                        Max(1e-50,jdot) * 1e-6;
                    Dprint("Jdotorb = %g Jorb=%g -> timescale %g y, fclose %g\n",
                           jdot,
                           stardata->common.orbit.angular_momentum,
                           stardata->common.orbit.angular_momentum/jdot,
                           fclose
                        );
                    Limit_timestep(*dt,dtj,star,DT_LIMIT_ORBITAL_ANGMOM);
                }
            }

            if(use_gravitational_wave_radiation(stardata)
               &&
               Less_or_equal(stardata->model.derivative[DERIVATIVE_ORBIT_ANGMOM],0.0)
               &&
               Less_or_equal(stardata->model.derivative[DERIVATIVE_ORBIT_SEMI_MAJOR_AXIS],0.0)
               &&
               Use_timestep(DT_LIMIT_GRAVITATIONAL_WAVE_RADIATION))
            {
                /*
                 * Resolve spiral in and hence gravitational wave mergers.
                 *
                 * We require dJorb/dt <= 0.0 because if dJorb/dt > 0 then
                 * the binary is (probably) widening.
                 *
                 * Also, we use the Max(dtspiral, a/adot) because if there
                 * is mass transfer then the orbit may be stable (widening
                 * because of mass transfer, while shrinking because of
                 * gravitational wave radiation) in which case we can choose
                 * a longer timestep.
                 */
                const double jdot = fabs(stardata->model.derivative[DERIVATIVE_ORBIT_ANGMOM]);
                const double adot = fabs(stardata->model.derivative[DERIVATIVE_ORBIT_SEMI_MAJOR_AXIS]);
                const double dtj = star->angular_momentum / Max(1e-50,jdot);
                const double dta = stardata->common.orbit.separation / Max(1e-50,adot);
                const double dtspiral = Peters_grav_wave_merger_time(
                        stardata,
                        stardata->star[0].mass,
                        stardata->star[1].mass,
                        stardata->common.orbit.separation,
                        stardata->common.orbit.eccentricity
                    );
                const double dt_grav_wave_merge =
                    fsolver *
                    1e-6 *
                    Max(dtspiral, dta) *
                    stardata->preferences->timestep_multipliers[DT_LIMIT_GRAVITATIONAL_WAVE_RADIATION];

                Dprint("DTMERGE GRAV WAVE : dtJ = %g, dta = %g, dtspiral %g -> dtmerge = %g * dtspiral * %g = %g : J = %g, a = %g, Jdot = %g, adot = %g\n",
                       dtj,
                       dta,
                       dtspiral,
                       fsolver,
                       stardata->preferences->timestep_multipliers[DT_LIMIT_GRAVITATIONAL_WAVE_RADIATION],
                       dt_grav_wave_merge,
                       stardata->common.orbit.angular_momentum,
                       stardata->common.orbit.separation,
                       stardata->model.derivative[DERIVATIVE_ORBIT_ANGMOM],
                       stardata->model.derivative[DERIVATIVE_ORBIT_SEMI_MAJOR_AXIS]
                    );

                Limit_timestep(*dt,dt_grav_wave_merge,star,DT_LIMIT_GRAVITATIONAL_WAVE_RADIATION);
            }
        }

    }//Binary check

#ifdef CIRCUMBINARY_DISK_DERMINE
    /*
     * Reduce timestep to resolve circumbinary disk physics
     */
    if(Use_timestep(DT_LIMIT_CIRCUMBINARY_DISC) &&
       stardata->common.m_circumbinary_disk>0.0)
    {
        double dtcbd = 0.1*circumbinary_disk_massloss_timescale(stardata)*1e-6;
        Limit_timestep(*dt,dtcbd,star,DT_LIMIT_CIRCUMBINARY_DISC);
    }
#endif

#ifdef RESOLVE_POSTAGBX
    if(stardata->star[0].stellar_type==TPAGB ||
       (WHITE_DWARF(stardata->star[0].stellar_type)
        &&
        stardata->star[0].age < 10.0))
    {
        /*
         * Don't let the temperature change by ~100K, or
         * 1% of Teff, whichever is greater.
         */
        double x;
        if(Is_not_zero(stardata->star[0].derivative[DERIVATIVE_STELLAR_TEMPERATURE]) &&
           !isnan(stardata->star[0].derivative[DERIVATIVE_STELLAR_TEMPERATURE]))
        {
            x = 1e-6 * //0.01*
                Max(0.01*Teff(0),100.0)/
                Min(1e-3,fabs(stardata->star[0].derivative[DERIVATIVE_STELLAR_TEMPERATURE]));
        }
        else
        {
            x = 1e50;
        }
        Limit_timestep(*dt,x,star,DT_LIMIT_RESOLVE_POSTAGB);

        /*
         * Estimate nuclear burning lifetime
         */
        double dmcdt =
            Max3(star->derivative[DERIVATIVE_STELLAR_CO_CORE_MASS],
                 star->derivative[DERIVATIVE_STELLAR_He_CORE_MASS],
                 star->derivative[DERIVATIVE_STELLAR_GB_CORE_MASS]);
        if(dmcdt > REALLY_TINY)
        {
            const double menv = envelope_mass(star);
            if(menv > REALLY_TINY)
            {
                const double dtnuc = 1e-6 * menv / dmcdt;
                Limit_timestep(*dt,dtnuc*0.01,star,DT_LIMIT_RESOLVE_POSTAGB);
            }
        }
    }
#endif

#ifdef DISCS
    /*
     * limit to 10% of the disc's natural evolutionary timescale.
     */
    {
        int i;
        for(i=0; i<stardata->common.ndiscs; i++)
        {
            if(stardata->common.discs[i].M >
               M_SUN * stardata->preferences->cbdisc_minimum_mass)
            {
                double dtdisc = 0.1 * timestep_disc(stardata,
                                                    &stardata->common.discs[i]);
                Limit_timestep(*dt,
                               dtdisc,
                               star,
                               DT_LIMIT_DISC);
            }
        }
    }
#endif

    if(stardata->preferences->PN_resolve == TRUE)
    {
        /*
         * Reduce timestep to better resolve the fast wind at the end of the AGB
         * or in post-common envelope.
         * Also reduce the timestep for stars which are new-born white dwarfs
         */
        if(/* giant stars : anything with a small envelope */
            (
                ON_GIANT_BRANCH(star->stellar_type)
                &&
                envelope_mass(star) < stardata->preferences->PN_resolve_maximum_envelope_mass
                )
            ||
            /* white dwarfs : anything bright (i.e. young) */
            (
                WHITE_DWARF(star->stellar_type)&&
                star->luminosity > stardata->preferences->PN_resolve_minimum_luminosity
                )
            )
        {
            /*
             * For delta t = 1% * delta m / mdot ~ 0.01 * 1e-3 / 1e-8 we
             * have dt<1e3 years.
             *
             * Or assume crossing time is ~1e4 years and we want 1000 points in
             * there i.e. 10 years ... but these may not all be in the horizontal
             * part of the HRD.
             *
             * Note that we assume a mass loss rate of 1e-8 for these stars:
             * if you're using the PN_FAST_WIND this is the rate in any case.
             */
            const double mdot = AGB(star->stellar_type)
                ? stardata->preferences->PN_fast_wind_mdot_AGB
                : stardata->preferences->PN_fast_wind_mdot_GB;

            /*
             * A menv/mass^n (n=2) dependence ensures we resolve even
             * massive AGBs moving across the HRD
             */
            /*
             * Enhance the number of grid points in the high-luminosity
             * horizontal passage across the HRD, but only in the PN phase
             */
            const double teff = Teff_from_star_struct(star);

            /*
             * NB the premultiplier used to be 1e-3 but there were times
             * when this crippled performance...
             */
            const double dtfastwind = 0.1 * envelope_mass(star)
                /(Pow2(star->phase_start_mass) * mdot)
                * 1e-6 // convert to MYr
                / (teff > (stardata->preferences->PN_resolve_minimum_effective_temperature)
                   ? Min(5.0,Max(1.0,10.0*(log10(star->luminosity)-1.5)))
                   : 1.0);

            Limit_timestep(*dt,dtfastwind,star,DT_LIMIT_FASTWIND);

            Dprint("DT < 0.1 because of PN : menv=%g teff=%g L=%g : dt=%g\n",
                   envelope_mass(star),teff,star->luminosity,*dt);
        }
    }

    {
        const double decretion_rate = Mdot_loss(star);
        if(Is_not_zero(decretion_rate))
        {
            const double dtlim = -0.1 * star->mass / decretion_rate;
            Limit_timestep(*dt,
                           dtlim,
                           star,
                           DT_LIMIT_MASS_LOSS);
#ifdef SECOND_DERIVATIVES
            const double dtlim2 = SECOND_DERIVATIVE_F * Second_derivative(DT_LIMIT_MASS_LOSS)
                *1e-6;
            Limit_timestep(*dt,
                           dtlim2,
                           star,
                           DT_LIMIT_MASS_LOSS2);
#endif//SECOND_DERIVATIVES
        }
    }


#if defined NUCSYN && defined NUCSYN_ANGELOU_LITHIUM

    {
        double lithium_time = angelou_factor(time,star->stellar_type);


        if(Is_not_zero(lithium_time))
        {
            double dtli = fabs(lithium_time - stardata->model.time + star->stellar_type_tstart);
            if(Is_not_zero(dtli))
            {
                Limit_timestep(*dt,
                               Max(1e-3,0.1 * dtli),
                               star,
                               DT_LIMIT_NUCSYN_ANGELOU_LITHIUM);
            }
        }

        if(0)
            fprintf(stderr,
                    ">>>TIMESTEP star %d at time %g this %d prev %d XLi7 %g\n",
                    star->starnum,
                    stardata->model.time,
                    star->angelou_lithium_boost_this_timestep,
                    prevstar->angelou_lithium_boost_this_timestep,
                    Observed_abundance(star,XLi7));

        if(
            star->angelou_lithium_boost_this_timestep == FALSE
            &&
            (
                prevstar->angelou_lithium_boost_this_timestep == TRUE
                ||
                Observed_abundance(star,XLi7) > (1.0+TINY)*NUCSYN_ANGELOU_LITHIUM_MINIMUM_Li7)
            )
        {
            /*
             * You might think we should only apply this timestep
             * limiter if the Li7 abundance spikes, but it *may*
             * spike in the next timestep, we just don't know.
             */
            double decay_time =
                angelou_factor(decay_time,star->stellar_type);

            if(Is_not_zero(decay_time))
            {
                Limit_timestep(*dt,
                               (1.0-TINY) * NUCSYN_ANGELOU_LITHIUM_MAX_TIMESTEP_FACTOR * decay_time,
                               star,
                               DT_LIMIT_NUCSYN_ANGELOU_LITHIUM);
            }

            if(0)
                fprintf(stderr,
                        "USE DECAY TIMESTEP star %d at time %g use decay time %g -> %g (this %d prev %d >lowerli7 %d [Li7=%g min=%g] reject %d)\n",
                        star->starnum,
                        stardata->model.time,
                        NUCSYN_ANGELOU_LITHIUM_MAX_TIMESTEP_FACTOR * decay_time,
                        *dt,
                        star->angelou_lithium_boost_this_timestep,
                        prevstar->angelou_lithium_boost_this_timestep,
                        (int)(Observed_abundance(star,XLi7) > (1.0+TINY)*NUCSYN_ANGELOU_LITHIUM_MINIMUM_Li7),
                        Observed_abundance(star,XLi7),
                        (1.0+TINY)*NUCSYN_ANGELOU_LITHIUM_MINIMUM_Li7,
                        star->reject
                    );
        }
    }
#endif



    /*
     * if dt_zoomfac < 1, don't let the timestep be >
     * 1.2 * dt_zoomfac * (this or previous timestep)
     */
    if(0 && Use_timestep(DT_LIMIT_ZOOMFAC) &&
       stardata->previous_stardata &&
       stardata->model.dt_zoomfac - 1.0 < -TINY)
    {
        const double dtz = stardata->preferences->timestep_multipliers[DT_LIMIT_ZOOMFAC] *
            Min(stardata->model.dtm,
                stardata->previous_stardata->model.dtm) * 1.2 * stardata->model.dt_zoomfac;
        Limit_timestep(*dt,dtz,star,DT_LIMIT_ZOOMFAC);
    }

    if(stardata->preferences->timestep_logging == TRUE
       &&
       !Fequal(*dt,dt_in))
    {
        fprintf(stdout,
                "%s timestep limited to %g because %s%s\n",
                stardata->store->colours[YELLOW],
                *dt,
                Dt_limit_string(abs(star->dtlimiter)),
                stardata->store->colours[COLOUR_RESET]);
    }
}
