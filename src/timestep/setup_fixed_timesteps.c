#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "timestep_debug.h"

void setup_fixed_timesteps(struct stardata_t * Restrict const stardata)
{
    struct binary_c_fixed_timestep_t * t =
        &stardata->model.fixed_timesteps[FIXED_TIMESTEP_ENSEMBLE];

#ifdef STELLAR_POPULATIONS_ENSEMBLE
    /*
     * Stellar population ensemble fixed timesteps :
     * only use these if the ensemble is turned on
     */
    if(stardata->preferences->ensemble == TRUE &&
       stardata->preferences->use_fixed_timestep[FIXED_TIMESTEP_ENSEMBLE] == TRUE)
    {
        t->enabled = TRUE;
        t->begin =
            stardata->preferences->ensemble_logtimes == TRUE ?
            log10(stardata->preferences->ensemble_startlogtime) :
            0.0;
        t->end =
            stardata->preferences->ensemble_logtimes == TRUE ?
            log10(stardata->model.max_evolution_time) :
            stardata->model.max_evolution_time;
        t->step =
            stardata->preferences->ensemble_logtimes == TRUE ?
            stardata->preferences->ensemble_logdt :
            stardata->preferences->ensemble_dt;
        t->next = t->begin + t->step;
        t->logarithmic = stardata->preferences->ensemble_logtimes;
        t->final = TRUE;
        t->previous_trigger = t->previous_test = t->begin;
    }
    else
    {
        t->enabled = FALSE;
    }
#else
    /* disable */
    t->enabled = FALSE;
#endif

    /*
     * The test timestep outputs every 1Myr. It is disabled by default.
     */
    t = &stardata->model.fixed_timesteps[FIXED_TIMESTEP_TEST];
    if(stardata->preferences->use_fixed_timestep[FIXED_TIMESTEP_TEST] == TRUE)
    {
        t->enabled = FALSE;
        t->begin = 0.0;
        t->end = stardata->model.max_evolution_time;
        t->step = 1.0;
        t->next = t->begin + t->step;
        t->logarithmic = FALSE;
        t->final = TRUE;
        t->previous_trigger = t->previous_test = t->begin;
    }
    else
    {
        t->enabled = FALSE;
    }

    /*
     * Others should be put here...
     *
     * When you add another fixed timestep remember to set a macro
     * for it in timestep.h and increment the FIXED_TIMESTEP_NUMBER
     * appropriately.
     */


    /*
     * Times set by a command-line argument
     */
    t = &stardata->model.fixed_timesteps[FIXED_TIMESTEP_ARG];
    if(stardata->preferences->use_fixed_timestep[FIXED_TIMESTEP_ARG] == TRUE)
    {
        if(stardata->preferences->target_times[0] != '\0' &&
           stardata->tmpstore->target_times == NULL)
        {
            struct string_array_t * strings = new_string_array(0);
            if(strstr(stardata->preferences->target_times,"-"))
            {
                /*
                 * Format x-y-z i.e.
                 * x to y in steps z
                 */
                string_split(stardata,
                             stardata->preferences->target_times,
                             strings,
                             '-',
                             0,
                             0,
                             TRUE);
                struct double_array_t * xyz = string_array_to_double_array(strings,NAN);

                const ssize_t n = (xyz->doubles[1] - xyz->doubles[0]) / xyz->doubles[2] + 1;
                stardata->tmpstore->target_times = new_double_array(n);
                double * const _times = stardata->tmpstore->target_times->doubles;
                for(ssize_t i=0; i<n; i++)
                {
                    _times[i] = xyz->doubles[0] + i * xyz->doubles[2];
                }
            }
            else
            {
                /*
                 * Comma-separated list
                 */
                string_split(stardata,
                             stardata->preferences->target_times,
                             strings,
                             ',',
                             0,
                             0,
                             TRUE);
                if(stardata->tmpstore->target_times == NULL)
                {
                    stardata->tmpstore->target_times = string_array_to_double_array(strings,NAN);
                }
                else
                {
                    struct double_array_t * d = Malloc(sizeof(struct double_array_t));
                    append_double_array(stardata,
                                        stardata->tmpstore->target_times,
                                        d);
                    free_double_array(&d);
                }
            }
            free_string_array(&strings);

            /*
             * Set things up
             */
            t->nextfunc = &target_times_next;
            t->next = target_times_next(stardata);
            t->enabled = TRUE;
            t->begin = 0.0;
            t->end = LONG_TIME_REMAINING;
            t->step = 0.0;
            t->final = FALSE;
            t->previous_trigger = t->previous_test = t->begin;
        }
        else
        {
            t->enabled = FALSE;
        }
    }
    else
    {
        t->enabled = FALSE;
    }

    /*
     * Evolutionary command times
     */
    if(stardata->preferences->commands != NULL)
    {
        struct double_array_t * array = evolutionary_command_times(stardata);
        if(array != NULL)
        {
            if(stardata->tmpstore->target_times == NULL)
            {
                stardata->tmpstore->target_times = array;
            }
            else
            {
                append_double_array(stardata,
                                    stardata->tmpstore->target_times,
                                    array);
                free_double_array(&array);
            }
            t->nextfunc = &target_times_next;
            t->next = target_times_next(stardata);
            t->enabled = TRUE;
            t->begin = 0.0;
            t->end = LONG_TIME_REMAINING;
            t->step = 0.0;
            t->final = FALSE;
            t->previous_trigger = t->previous_test = t->begin;
        }
    }
}
