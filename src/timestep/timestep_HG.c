#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "timestep_debug.h"

void timestep_HG(Timestep_prototype_args)
{
    if(stardata->preferences->stellar_structure_algorithm == STELLAR_STRUCTURE_ALGORITHM_MINT)
    {
#ifdef MINT
        if(Use_timestep(DT_LIMIT_HG))
        {
            const double timescales[] = {
                star->core_mass[CORE_He] / star->derivative[DERIVATIVE_STELLAR_He_CORE_MASS],
                star->mint->central_degeneracy / star->derivative[DERIVATIVE_STELLAR_CENTRAL_DEGENERACY]
            };
            for(size_t i=0; i<Array_size(timescales); i++)
            {
                Limit_timestep(*dt,
                               1e-6*stardata->preferences->timestep_multipliers[DT_LIMIT_HG]*fabs(timescales[i]),
                               star,
                               DT_LIMIT_HG);
            }
            printf("HG timestep %g\n",*dt);
        }
#endif//MINT
    }
    else
    {
#ifdef BSE
        /* HG timescale ~ tKH */
        const double tHG = tscls[T_BGB] - tm;

        Dprint("DTHG dtt=%g (star %d, age = %g, star->age = %g, TBGB = %g, tm = %g, t=%g)\n",
               tHG,
               (int)star->starnum,
               age,
               star->age,
               tscls[T_BGB],
               tm,
               stardata->model.time);

        if(Use_timestep(DT_LIMIT_HG))
        {
            Limit_timestep(*dt,stardata->preferences->timestep_multipliers[DT_LIMIT_HG]*tHG,star,DT_LIMIT_HG);
        }
        *time_remaining = Max(0.0,tscls[T_BGB] - age);

#ifdef HRDIAG
        stardata->model.hrdiag_dt_guess = HRDIAG_FRAC * tHG;

#ifdef HRDIAG_DTLOG
        printf("HRDT (deltat) HG %g\n",stardata->model.hrdiag_dt_guess);
#endif
        HRDIAG_DT_NEGATIVE_CHECK;
#endif

#ifdef SLOW_DOWN_PREROCHE_HG
        if(stardata->model.sgl == FALSE)
        {
            const double rr=star->radius / star->roche_radius;
            if(Use_timestep(DT_LIMIT_PREROCHE_HG) && rr<1.0)
            {
                const double newdt = *dt * Min(1.0, Max(1e-2,Pow2(1.0/rr-1.0)))
                    * stardata->preferences->timestep_multipliers[DT_LIMIT_PREROCHE_HG];
                Limit_timestep(*dt,newdt,star,DT_LIMIT_PREROCHE_HG);
            }
        }
#endif // SLOW_DOWN_PREROCHE_HG

        const double accretion_rate = Mdot_net(star);
        if(accretion_rate > 0.0)
        {
            /*
             * Use the accretion rate limit, not the
             * stellar evolution limit if an AGB star is accreting.
             */
            *time_remaining = LONG_TIME_REMAINING;
        }

        *time_remaining = LONG_TIME_REMAINING;
#endif//BSE
    }
}
