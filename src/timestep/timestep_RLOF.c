#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "timestep_debug.h"

void timestep_RLOF(Timestep_prototype_args)
{
    /*
     * Limit timestep because of RLOF
     */
#ifdef RLOF_REDUCE_TIMESTEP
    if(stellar_type<HeWD)
    {
        if(stardata->model.sgl==FALSE)
        {
            /* binary star conditions */
            const double fr = star->radius / star->roche_radius;

            if(fr > RLOF_REDUCE_TIMESTEP_THRESHOLD)
            {
                double rr;

                /*
                 * rr is the factor by which we reduce the timestep.
                 * If fr<RLOF_STABILITY_FACTOR (pre-RLOF) then gradually
                 * phase in the smaller timestep, depending on fr
                 * the fraction of the Roche lobe that is filled
                 */
                if(fr < RLOF_STABILITY_FACTOR)
                {
                    rr=pow(fr, RLOF_REDUCE_TIMESTEP_AGGRESSION);
                    rr=Max(RLOF_REDUCE_TIMESTEP_MINIMUM_FACTOR,1.0-rr);
                }
                else
                {
                    /* Roche lobe full: do what we can */
                    rr=RLOF_REDUCE_TIMESTEP_MINIMUM_FACTOR;
                }
                *dt *= rr;

#undef ADAPTIVE_RLOF
#ifdef ADAPTIVE_RLOF
                /*
                 * Close to, and during, RLOF use the actual
                 * RLOF timestep
                 */
                if(fr > RLOF_STABILITY_FACTOR)
                {
                    double dtm_was = stardata->model.dtm;
                    set_RLOF_timestep(stardata);
                    *dt = Min(1e-2**dt, 1e6 * stardata->model.dtm);
                    stardata->model.dtm = dtm_was;
                }

                double mdot =stardata->common.mdot_RLOF_adaptive;
                double dt_mdot =  1e-4/mdot;
                if(mdot>TINY)
                {
                    Dprint("DELTAT limit according to RLOF mdot=%g : dt_mdot\n",mdot,dt_mdot);
                    /* try to lose no more than 1e-4 Msun in a timestep */

                    /* convert to Myr */
                    dt_mdot *= 1e-6;
                    /* only apply if R>RL */
                    if(fr>RLOF_STABILITY_FACTOR) *dt = Min(*dt,dt_mdot);
                }
#endif
            }
        }

#ifdef ADAPTIVE_RLOF
#ifdef MODULATE_TIMESTEP_TO_RESOLVE_ROTATION_MASS_LOSS
        /*
         * When rotating quickly, and using the rotationally enhanced
         * mass loss, reduce the timestep to resolve a drop in the
         * spin rate.
         */
        if(stardata->preferences->rotationally_enhanced_mass_loss!=0)
        {
            double vratio=Max(0.0,Min(1.0,star->omega_ratio));
            *dt *= Max(1.0-vratio,0.001);
        }
#endif
#endif
    }
#endif

#ifdef VISCOUS_RLOF_TRIGGER_SMALLER_TIMESTEP
    /*
     * If viscous RLOF has been triggered, make the timestep very
     * small : we really do not want viscous RLOF to trigger often!
     */
    if(stardata->common.viscous_RLOF_wants_smaller_timestep==TRUE)
    {
        printf("RLOF VISCOUS TRIGGER\n");
        *dt = stardata->preferences->minimum_timestep;
        stardata->common.viscous_RLOF_wants_smaller_timestep=FALSE;
    }
#endif

    /*
     * Use the RLOF recommended timestep if it's non-zero
     */
    Dprint("RLOF limiter %g (reject shorten %u same %u)\n",
           stardata->model.RLOF_recommended_timestep,
           stardata->model.reject_shorten_timestep,
           stardata->model.reject_same_timestep
        );
    if(Is_not_zero(stardata->model.RLOF_recommended_timestep))
    {
        Limit_timestep(*dt,
                       stardata->preferences->timestep_multipliers[DT_LIMIT_RLOF]*
                       stardata->model.RLOF_recommended_timestep,
                       star,
                       DT_LIMIT_RLOF);
        Dprint("Limit RLOF timestep : %g\n",*dt);
    }
}
