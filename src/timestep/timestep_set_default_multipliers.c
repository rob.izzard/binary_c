#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "timestep_debug.h"

void timestep_set_default_multipliers(struct preferences_t * preferences)
{
    /*
     * Default timestep multipliers.
     *
     * Timesteps are generally based on
     *
     * f * dX/dt * 1/X
     *
     * where f < 1.
     *
     * Here we define the default set of multipliers for the various
     * timestep limiters.
     *
     * For details of how these are used, please see the other functions
     * in this directory.
     */

    preferences->timestep_multipliers[DT_LIMIT_NONE] = 1.0; /* 0 */
    preferences->timestep_multipliers[DT_LIMIT_MS] = 0.01; /* 1 */
    preferences->timestep_multipliers[DT_LIMIT_PREMS] = 0.01; /* 2 */
    preferences->timestep_multipliers[DT_LIMIT_PREROCHE_MS] = 1.0; /* 3 */
    preferences->timestep_multipliers[DT_LIMIT_PREROCHE_HG] = 1.0; /* 4 */
    preferences->timestep_multipliers[DT_LIMIT_HG] = 0.1; /* 5 */
    preferences->timestep_multipliers[DT_LIMIT_FGB] = 0.01; /* 6 */
    preferences->timestep_multipliers[DT_LIMIT_CHeB] = 0.01; /* 7 */
    preferences->timestep_multipliers[DT_LIMIT_EAGB] = 0.01; /* 8 */
    preferences->timestep_multipliers[DT_LIMIT_EAGB_AXEL] = 1.0; /* 9 */
    preferences->timestep_multipliers[DT_LIMIT_EAGB_PREROCHE] = 1.0; /* 10 */
    preferences->timestep_multipliers[DT_LIMIT_TPAGB] = 0.01; /* 11 */
    preferences->timestep_multipliers[DT_LIMIT_TPAGB_NUCSYN_INTERPULSE] = 1.0; /* 12 */
    preferences->timestep_multipliers[DT_LIMIT_TPAGB_NUCSYN_SPEEDUP] = 0.01; /* 13 */
    preferences->timestep_multipliers[DT_LIMIT_TPAGB_NUCSYN_PREROCHE] = 1.0; /* 14 */
    preferences->timestep_multipliers[DT_LIMIT_TPAGB_NUCSYN_KARAKAS_SMOOTH] = 0.05; /* 15 */
    preferences->timestep_multipliers[DT_LIMIT_HeMS] = 0.01; /* 16 */
    preferences->timestep_multipliers[DT_LIMIT_HeHG_GB] = 0.02; /* 17 */
    preferences->timestep_multipliers[DT_LIMIT_OTHER_STELLAR_TYPES] = 1e3; /* 18 */
    preferences->timestep_multipliers[DT_LIMIT_STELLAR_ANGMOM] = 0.01; /* 19 */
    preferences->timestep_multipliers[DT_LIMIT_STELLAR_MASS_LOSS] = 1.0; /* 20 */
    preferences->timestep_multipliers[DT_LIMIT_STELLAR_MAGNETIC_BRAKING] = 1.0; /* 21 */
    preferences->timestep_multipliers[DT_LIMIT_CIRCUMBINARY_DISC] = 1.0; /* 22 */
    preferences->timestep_multipliers[DT_LIMIT_RESOLVE_POSTAGB] = 1.0; /* 23 */
    preferences->timestep_multipliers[DT_LIMIT_DISC] = 1.0; /* 24 */
    preferences->timestep_multipliers[DT_LIMIT_FASTWIND] = 1.0; /* 25 */
    preferences->timestep_multipliers[DT_LIMIT_SELMA] = 1.0; /* 26 */
    preferences->timestep_multipliers[DT_LIMIT_CEMP_POSTMS] = 1.0; /* 27 */
    preferences->timestep_multipliers[DT_LIMIT_CEMP_NOTEMP] = 1.0; /* 28 */
    preferences->timestep_multipliers[DT_LIMIT_CEMP_EMP] = 1.0; /* 29 */
    preferences->timestep_multipliers[DT_LIMIT_CEMP_NEARLY] = 1.0; /* 30 */
    preferences->timestep_multipliers[DT_LIMIT_CEMP_FLOOR] = 1.0; /* 31 */
    preferences->timestep_multipliers[DT_LIMIT_FABIAN_IMF_LOG] = 1.0; /* 32 */
    preferences->timestep_multipliers[DT_LIMIT_HRD1] = 1.0; /* 33 */
    preferences->timestep_multipliers[DT_LIMIT_HRD2] = 1.0; /* 34 */
    preferences->timestep_multipliers[DT_LIMIT_BLUE_STRAGGLER] = 1.0; /* 35 */
    preferences->timestep_multipliers[DT_LIMIT_YVT] = 1.0; /* 36 */
    preferences->timestep_multipliers[DT_LIMIT_MINIMUM_TIMESTEP] = 1.0; /* 37 */
    preferences->timestep_multipliers[DT_LIMIT_MAXIMUM_TIMESTEP] = 1.0; /* 38 */
    preferences->timestep_multipliers[DT_LIMIT_NOVAE] = 0.1; /* 39 */
    preferences->timestep_multipliers[DT_LIMIT_ARTIFICIAL_ACCRETION] = 1.0; /* 40 */
    preferences->timestep_multipliers[DT_LIMIT_SN] = 1.0; /* 41 */
    preferences->timestep_multipliers[DT_LIMIT_MASS_GAIN] = 0.01; /* 42 */
    preferences->timestep_multipliers[DT_LIMIT_MASS_LOSS] = 1.0; /* 43 */
#ifdef EXPONENTIAL_TIDES
    /* exponential tides are more stable, so can have a longer multiplier */
    preferences->timestep_multipliers[DT_LIMIT_TIDES] = 0.5; /* 44 */
#else
    preferences->timestep_multipliers[DT_LIMIT_TIDES] = 0.01; /* 44 */
#endif
    preferences->timestep_multipliers[DT_LIMIT_NUCSYN_ANGELOU_LITHIUM] = 1.0; /* 45 */
    preferences->timestep_multipliers[DT_LIMIT_CARBON_BURNING] = 1.0; /* 46 */
    preferences->timestep_multipliers[DT_LIMIT_BURN_IN] = 1.0; /* 47 */
    preferences->timestep_multipliers[DT_LIMIT_RADIUS_CHANGES] = 0.1; /* 48 */
    preferences->timestep_multipliers[DT_LIMIT_MASSLESS_REMNANT] = 1.0; /* 49 */
    preferences->timestep_multipliers[DT_LIMIT_ORBITAL_ANGMOM] = 0.05; /* 50 */
    preferences->timestep_multipliers[DT_LIMIT_STELLAR_ANGMOM2] = 1.0; /* 51 */
    preferences->timestep_multipliers[DT_LIMIT_TIDES2] = 1.0; /* 52 */
    preferences->timestep_multipliers[DT_LIMIT_MASS_GAIN2] = 1.0; /* 53 */
    preferences->timestep_multipliers[DT_LIMIT_MASS_LOSS2] = 1.0; /* 54 */
    preferences->timestep_multipliers[DT_LIMIT_GRAVITATIONAL_WAVE_RADIATION] = 1.0; /* 55 */
    preferences->timestep_multipliers[DT_LIMIT_RLOF] = 1.0; /* 56  */
    preferences->timestep_multipliers[DT_LIMIT_FIXED_TIMESTEP] = 1.0; /* 57  */
    preferences->timestep_multipliers[DT_LIMIT_TIME_REMAINING_0] = 1.0; /* 58 */
    preferences->timestep_multipliers[DT_LIMIT_TIME_REMAINING_1] = 1.0; /* 59 */
    preferences->timestep_multipliers[DT_LIMIT_TIME_REMAINING_OTHER] = 1.0; /* 60 */
    preferences->timestep_multipliers[DT_LIMIT_ZOOMFAC] = 1.0; /* 61 */
    preferences->timestep_multipliers[DT_LIMIT_THERMAL] = 0.0; /* 62 */
}
