#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "timestep_debug.h"

void timestep_modulation(Timestep_prototype_args)
{
#ifdef TIMESTEP_MODULATION
    /*
     * Perhaps modulate the timestep? Only for non-remnants
     */
    if(star->stellar_type<HeWD &&
       !Fequal(1.0,stardata->preferences->timestep_modulator))
    {
        Modulate_timestep(*dt,
                          stardata->preferences->timestep_modulator,
                          star);
    }
#endif

}
