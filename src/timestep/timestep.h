#pragma once
#ifndef TIMESTEP_H
#define TIMESTEP_H
#include "../binary_c_parameters.h"
#include "timestep.def"

/*
 * A list of fixed timesteps :
 * these should be set up in setup_fixed_timesteps.c
 * and are handled in timestep_fixed_timesteps.c
 */
#undef X
#define X(CODE) FIXED_TIMESTEP_##CODE,
enum { FIXED_TIMESTEPS_LIST };
#undef X
#define X(CODE) #CODE,
static const char * const fixed_timestep_strings[] = { FIXED_TIMESTEPS_LIST };
#undef X

/********************************************************************/


#ifdef HRDIAG
#define HRDIAG_FRAC 0.01
#define HRDIAG_DT_NEGATIVE_CHECK if(stardata->model.hrdiag_dt_guess<0.0){Exit_binary_c(BINARY_C_OUT_OF_RANGE,"hrdiag dt guess < 0 error\n");}

/*
 * timestep should be at least half the width of the time bin
 * (1.0 also seems to work but we want to be sure)
 */
#define HRDIAG_DT_SAMPLE_FRAC (0.5 * HRDIAG_BIN_SIZE_LOGTIME)
#endif

#define Use_timestep(N)                                                 \
    (Is_not_zero(stardata->preferences->timestep_multipliers[(N)]))

/*
 * Compare two times, A and B, to see if they are equal for the
 * fixed timestepping scheme.
 *
 * Given that the time is incremented, it is liable to integration
 * errors, so comparing the times to within a very small epsilon (like
 * TINY == DBL_EPSILON) will not work, we have to be smarter,
 * so instead define TIMESTEP_EPSILON_DEFAULT here and use
 * Float_same_within_eps to compare the times.
 *
 * TIMESTEP_EPSILON_DEFAULT is 1e-12 which is two orders of
 * magnitude bigger than DBL_EPSLION (~1e-12).
 * This allows for considerable error in time integration.
 *
 * The above method will fail if times are close to zero,
 * so we also check Fequal(A,B) in this case : if they are
 * both zero, this will work.
 */
#define TIMESTEP_EPSILON_DEFAULT 1e-12

#define Times_are_equal(A,B)                            \
    (                                                   \
        Fequal((A),(B)) ||                              \
        Float_same_within_eps(                          \
            (A),                                        \
            (B),                                        \
            stardata->preferences->timestep_epsilon)    \
        )

#define Times_more_or_equal(A,B)                        \
    (                                                   \
        More_or_equal((A),(B)) ||                       \
        Float_same_within_eps(                          \
            (A),                                        \
            (B),                                        \
            stardata->preferences->timestep_epsilon)    \
        )

#define Timestep_call_args                      \
    stellar_type,                               \
        age,                                    \
        tm,                                     \
        tn,                                     \
        tscls,                                  \
        dt,                                     \
        time_remaining,                         \
        star,                                   \
        stardata

#define Timestep_prototype_args                             \
    const Stellar_type stellar_type Maybe_unused,           \
        const double  age Maybe_unused,                     \
        const double  tm Maybe_unused,                      \
        const double  tn Maybe_unused,                      \
        double  tscls[] Maybe_unused,                       \
        double * const  dt Maybe_unused,                    \
        double * const  time_remaining Maybe_unused,        \
        struct star_t * const  star Maybe_unused,           \
        struct stardata_t * const  stardata Maybe_unused
/*
  #else

  #define Timestep_call_args                      \
  stellar_type,                               \
  dt,                                     \
  time_remaining,                         \
  star,                                   \
  stardata

  #define Timestep_prototype_args                         \
  const Stellar_type  stellar_type Maybe_unused,      \
  double * const  dt Maybe_unused,                \
  double * const  time_remaining Maybe_unused,    \
  struct star_t * const  star Maybe_unused,       \
  struct stardata_t * const  stardata
  #endif
*/

/*
 * Macros to set and/or limit the timestep and save which limiter
 * is responsible for the current timestep.
 *
 * Limit_timestep reduces the timestep (this is normally the case) while
 * Floor_timestep sets a floor below which it cannot fall.
 *
 * Modulate_timestep is a bit different : it multiplies a timestep
 * and makes the dtlimiter reason negative - although does not save
 * where this modulation is done. Such modulations are rare, so not usually
 * an issue, but if you want to track them you will have to introduce
 * a new logging variable.
 */

#define Set_timestep(DT,TO,IN,WHY)                          \
    {                                                       \
        (DT) = Max(stardata->preferences->minimum_timestep, \
                   (TO));                                   \
        Dprint("Set timestep to %g why %s [%d]\n",          \
               (TO),                                        \
               Dt_limit_string(WHY),                        \
               (WHY));                                      \
        if(WHY!=DT_LIMIT_MAXIMUM_TIMESTEP &&                \
           WHY!=DT_LIMIT_MINIMUM_TIMESTEP)                  \
        {                                                   \
            (IN)->dtlimiter=(WHY);                          \
        }                                                   \
    }

#define Set_timestep_no_minimum(DT,TO,IN,WHY)       \
    {                                               \
        (DT)=(TO);                                  \
        Dprint("Set timestep to %g why %s [%d]\n",  \
               (TO),                                \
               Dt_limit_string(WHY),                \
               (WHY));                              \
        if(WHY!=DT_LIMIT_MAXIMUM_TIMESTEP &&        \
           WHY!=DT_LIMIT_MINIMUM_TIMESTEP)          \
        {                                           \
            (IN)->dtlimiter=(WHY);                  \
        }                                           \
    }

#define Limit_timestep(DT,TO,IN,WHY)                            \
    __extension__                                               \
    ({                                                          \
        Boolean _limited;                                       \
        if((DT)>Max(stardata->preferences->minimum_timestep,    \
                    (TO)))                                      \
        {                                                       \
            Dprint("Limit timestep to %g why %s [%d]\n",        \
                   (TO),                                        \
                   Dt_limit_string(WHY),                        \
                   (WHY));                                      \
            Set_timestep((DT),(TO),(IN),(WHY));                 \
            _limited = TRUE;                                    \
        }                                                       \
        else                                                    \
        {                                                       \
            _limited = FALSE;                                   \
        }                                                       \
        _limited;                                               \
    })

#define Limit_timestep_no_minimum(DT,TO,IN,WHY)             \
    __extension__                                           \
    ({                                                      \
        Boolean _limited;                                   \
        if((DT)>(TO))                                       \
        {                                                   \
            Dprint("Limit timestep to %g why %s [%d]\n",    \
                   (TO),                                    \
                   Dt_limit_string(WHY),                    \
                   (WHY));                                  \
            Set_timestep_no_minimum((DT),(TO),(IN),(WHY));  \
            _limited = TRUE;                                \
        }                                                   \
        else                                                \
        {                                                   \
            _limited = FALSE;                               \
        }                                                   \
        _limited;                                           \
    })

#define Floor_timestep(DT,TO,IN,WHY)                        \
    __extension__                                           \
    ({                                                      \
        Boolean _floored;                                   \
        if((DT)<(TO))                                       \
        {                                                   \
            Dprint("Floor timestep to %g why %s [%d]\n",    \
                   (TO),                                    \
                   Dt_limit_string(WHY),                    \
                   (WHY));                                  \
            Set_timestep((DT),(TO),(IN),(WHY));             \
            _floored = TRUE;                                \
        }                                                   \
        else                                                \
        {                                                   \
            _floored = FALSE;                               \
        }                                                   \
        _floored;                                           \
    })

#define Modulate_timestep(DT,BY,IN)                     \
    {                                                   \
        if(!Fequal(1.0,(BY)))                           \
        {                                               \
            (DT)*=(BY);                                 \
            Dprint("Modulate timestep by %g to %g\n",   \
                   (BY),                                \
                   (DT));                               \
            (IN)->dtlimiter=-abs((IN)->dtlimiter);      \
        }                                               \
    }
#else

#endif // TIMESTEP_H
