#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "timestep_debug.h"

void timestep_logging(Timestep_prototype_args)
{
    /*
     * Limit timestep because of logging requirements
     */

#ifdef HRDIAG
    if(stellar_type<NEUTRON_STAR)
    {
        /* set global hrdiag logging timestep */

        /* guess for star 1: just set directly from the guess */
        if(stardata->model.hrdiag_log_dt < TINY)
        {
            stardata->model.hrdiag_log_dt = stardata->model.hrdiag_dt_guess;
#ifdef HRDIAG_DTLOG
            printf("HRDT (deltat) guess for star 1 %g\n",stardata->model.hrdiag_dt_guess);
#endif
        }
        else
            /* second guess (star2) : take the minimum of previous dt and this one */
        {
#ifdef HRDIAG_DTLOG
            printf("HRDT (deltat) guess for star 2 (guess %g, for star 1 %g)\n",
                   stardata->model.hrdiag_dt_guess,
                   stardata->model.hrdiag_log_dt);
#endif
            stardata->model.hrdiag_log_dt = Min(stardata->model.hrdiag_dt_guess,stardata->model.hrdiag_log_dt);
        }

        HRDIAG_DT_NEGATIVE_CHECK;

        /* force at least 0.05 (in practice 0.049 - Nyquist/Shannon!)
         * resolution in log t
         */
        double hrdt_sample =
            exp10(log10(stardata->model.prev_hrdiag_log_time) +
                HRDIAG_DT_SAMPLE_FRAC)-
            stardata->model.prev_hrdiag_log_time;

#ifdef HRDIAG_DTLOG
        printf("HRDT (deltat) time sample limit Min(dt %g, sample %g) takes us to logt=%g\n",
               stardata->model.hrdiag_log_dt,
               hrdt_sample,
               log10(stardata->model.prev_hrdiag_log_time + hrdt_sample)
            );
#endif
        stardata->model.hrdiag_log_dt = Min(stardata->model.hrdiag_log_dt,hrdt_sample);
    }
    else
    {
        /* we're a remnant and the first star : set the timestep to be huge */
        if(stardata->model.hrdiag_log_dt < TINY)
        {
            stardata->model.hrdiag_log_dt = 1e6;
        }
    }

#endif
#ifdef SELMA
    Limit_timestep(*dt,0.1,star,DT_LIMIT_SELMA);
#endif

#ifdef FABIAN_IMF_LOG
    /*
     * Log at exact times; therefore check whether normal timestep *dt
     * is smaller than time to next logging point. If so change *dt
     */
    if(ON_MAIN_SEQUENCE(stellar_type))
    {
        *dt = Min(stardata->model.fabian_imf_log_timestep, *dt);

        /*
         * change next timestep? (i.e. next logging time closer to actual model time than *dt?)
         */
        Limit_timestep(*dt, stardata->model.next_fabian_imf_log_time-stardata->model.time,star,DT_LIMIT_FABIAN_IMF_LOG);
        *time_remaining = tm - age;
    }
#endif


#ifdef HRDIAG
    if(stardata->preferences->hrdiag_output)
    {
        /* constrain timestep to hit the next hrdiag log time */
        double hrdt=
            exp10(stardata->model.next_hrdiag_log_time) -
            stardata->model.time;

#ifdef HRDIAG_DTLOG
        printf("HRDT (deltat) check next %g now %g : delta = %g\n",
               exp10(stardata->model.next_hrdiag_log_time),
               stardata->model.time,
               hrdt);
#endif

        if(hrdt < TINY)
        {
            /*
             * If hrdt = 0 then we're on the timestep and cannot
             * use the time difference to constrain the next timestep.
             * However, we can guess what it should be.
             */
            Limit_timestep(*dt,stardata->model.hrdiag_log_dt,star,DT_LIMIT_HRD1);
#ifdef HRDIAG_DTLOG
            printf("HRDT (deltat) at log time set dt=%g (%d from %g)\n",*dt,stellar_type,stardata->model.hrdiag_log_dt);
#endif
        }
        else
        {
            /*
             * If hrdt > 0 then we're not exactly on the timestep
             * and must set the timestep to hit it exactly the next time.
             */
            Limit_timestep(*dt,hrdt,star,DT_LIMIT_HRD2);
#ifdef HRDIAG_DTLOG
            printf("HRDT (deltat) off timestep set dt=%g (%d)\n",*dt,stellar_type);
#endif
        }

        HRDIAG_DT_NEGATIVE_CHECK;

        star->tn = tn;
    }
#endif //HRDIAG


#ifdef BLUE_STRAGGLER_PROJECT
    /*
     * Limit the timestep if we're in the final 10% of the main sequence
     * because this is when we're likely to have a blue straggler phase,
     * or (of course) if we're already a blue straggler
     */
    const double original_mass = stardata->common.zero_age.mass[star->starnum];
    if((star->blue_straggler==TRUE) &&
       (ON_MAIN_SEQUENCE(star->stellar_type) &&
        (*dt>BLUE_STRAGGLER_MAX_TIMESTEP) &&
        (stardata->model.time > 0.9*star->tms)&&
        (star->phase_start_mass > original_mass + BLUE_STRAGGLER_ACCRETED_MASS_THRESHOLD)))
    {
        Limit_timestep(*dt,BLUE_STRAGGLER_MAX_TIMESTEP,star,DT_LIMIT_BLUE_STRAGGLER);
    }
#endif// BLUE_STRAGGLER_PROJECT
}
