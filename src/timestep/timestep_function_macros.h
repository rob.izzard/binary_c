#pragma once
#ifndef TIMESTEP_FUNCTION_MACROS_H
#define TIMESTEP_FUNCTION_MACROS_H

/* timestep limiters */

#define Limit_timestep_old(T)                                   \
    (Min(stardata->model.dt,                                    \
         (Clamp((T),                                            \
                1e6*stardata->preferences->minimum_timestep,    \
                1e6*stardata->preferences->maximum_timestep))))

#define Limit_timestep_Myr_old(T)                               \
    (Min(stardata->model.dt,                                    \
         (Clamp((T),                                            \
                stardata->preferences->minimum_timestep,        \
                stardata->preferences->maximum_timestep))))


#endif // TIMESTEP_FUNCTION_MACROS_H
