#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "timestep_debug.h"

void timestep_HeMS(Timestep_prototype_args)
{
#ifdef BSE
    if(Use_timestep(DT_LIMIT_HeMS))
    {
        Limit_timestep(*dt,stardata->preferences->timestep_multipliers[DT_LIMIT_HeMS]*tm,star,DT_LIMIT_HeMS);
    }
#ifdef HRDIAG
    stardata->model.hrdiag_dt_guess = HRDIAG_FRAC* tm;
#ifdef HRDIAG_DTLOG
    printf("HRDT (deltat) HeMS %g\n",stardata->model.hrdiag_dt_guess);
#endif
    HRDIAG_DT_NEGATIVE_CHECK;
#endif

    Dprint("Time remaining : tm = %g - age = %g\n",tm,age);
    *time_remaining = tm - age;
#endif//BSE
}
