#ifndef TIMESTEP_DEBUG_H
#define TIMESTEP_DEBUG_H

/*
 * Set DEBUG to be 1 here if you want debugging
 * statements throughout the timestep code.
 */

//#undef DEBUG
//#define DEBUG 1

#undef Debug_show_expression
#define Debug_show_expression "%d dt=%g dtm=%g *dt=%g ",    \
        stardata->model.model_number,                       \
        stardata->model.dt,                                 \
        stardata->model.dtm,                                \
        (dt!=NULL ? *dt : -666.0)

/*
 * Some functions don't have a dt variable: to allow
 * Dprint() to work we need a dummy variable.
 *
 * We mark this as Maybe_unused to prevent compiler warnings.
 */
#define Set_debug_dt                                            \
    const double * const dt Maybe_unused = &stardata->model.dt;

#endif
