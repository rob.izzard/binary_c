#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "timestep_debug.h"

void timestep_FGB(Timestep_prototype_args)
{
#ifdef MINT
    struct mint_table_metadata_t * const metadata =
        star->mint != NULL &&
        star->mint->GB_table != NULL ?
        star->mint->GB_table->metadata : NULL;
#endif // MINT

    if(stardata->preferences->stellar_structure_algorithm == STELLAR_STRUCTURE_ALGORITHM_MINT
#ifdef MINT
       && metadata != NULL
       && metadata->data_available != NULL
#endif // MINT
        )
    {
#ifdef MINT
        if(Use_timestep(DT_LIMIT_FGB))
        {
            const double timescales[] = {
                star->core_mass[CORE_He] / star->derivative[DERIVATIVE_STELLAR_He_CORE_MASS],
                star->mint->central_degeneracy / star->derivative[DERIVATIVE_STELLAR_CENTRAL_DEGENERACY]
            };

            for(size_t i=0; i<Array_size(timescales); i++)
            {
                const double _dt = 1e-6*stardata->preferences->timestep_multipliers[DT_LIMIT_FGB]*fabs(timescales[i]);
                Limit_timestep(*dt,
                               _dt,
                               star,
                               DT_LIMIT_FGB);
            }
        }
#endif//MINT
    }
    else
    {
#ifdef BSE
        double dtt = (age<tscls[T_GIANT_TX] ? tscls[T_GIANT_TINF_1] : tscls[T_GIANT_TINF_2]) - age;
        if(Use_timestep(DT_LIMIT_FGB))
        {
            Limit_timestep(*dt,
                           stardata->preferences->timestep_multipliers[DT_LIMIT_FGB]*dtt,
                           star,
                           DT_LIMIT_FGB);
        }
#ifdef HRDIAG
        stardata->model.hrdiag_dt_guess = 0.1 * dtt;
#ifdef HRDIAG_DTLOG
        printf("HRDT (deltat) GB %g\n",stardata->model.hrdiag_dt_guess);
#endif
        HRDIAG_DT_NEGATIVE_CHECK;
#endif
        *time_remaining = Min(tscls[T_HE_IGNITION],tn) - age;
        Dprint("Set time remaining from tscls[T_HE_IGNITION]=%g, tn=%g, age=%g -> %g\n",
               tscls[T_HE_IGNITION],
               tn,
               age,
               *time_remaining
            );

        double accretion_rate = Mdot_net(star);
        if(accretion_rate > 0.0)
        {
            /*
             * Use the accretion rate limit, not the
             * stellar evolution limit if an AGB star is accreting.
             */
            *time_remaining = LONG_TIME_REMAINING;
        }
        /*
         * Do not trust time remaining
         */
        if(stardata->preferences->stellar_structure_algorithm == STELLAR_STRUCTURE_ALGORITHM_MINT)
        {
            *time_remaining = LONG_TIME_REMAINING;
        }

#endif//BSE
    }
}
