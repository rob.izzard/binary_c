#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "timestep_debug.h"

void timestep_HeHG_HeGB(Timestep_prototype_args)
{
#ifdef BSE

    /*
     * Time remaining in this burning phase
     */
    const double dtt = (age<tscls[T_GIANT_TX] ? tscls[T_GIANT_TINF_1] :tscls[T_GIANT_TINF_2])  - age;
    if(dtt > TINY && Use_timestep(DT_LIMIT_HeHG_GB))
    {
        Dprint("dtt age = %g TX %g TINF1 %g TINF2 %g mass %g core %g\n",
               age,
               tscls[T_GIANT_TX],
               tscls[T_GIANT_TINF_1],
               tscls[T_GIANT_TINF_2],
               star->mass,
               star->core_mass[CORE_CO]);

        Limit_timestep(*dt,
                       stardata->preferences->timestep_multipliers[DT_LIMIT_HeHG_GB]*dtt,
                       star,
                       DT_LIMIT_HeHG_GB);

#ifdef HRDIAG
        stardata->model.hrdiag_dt_guess = HRDIAG_FRAC* dtt;
#ifdef HRDIAG_DTLOG
        printf("HRDT (deltat) He Post-MS %g\n",stardata->model.hrdiag_dt_guess);
#endif
        HRDIAG_DT_NEGATIVE_CHECK;
#endif
        /* the dtt check above should be sufficient */
        *time_remaining =  dtt;
    }
#endif//BSE
}
