#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "timestep_debug.h"

void timestep_other(Timestep_prototype_args)
{
    /*
     * Timestep for stellar types that don't have their
     * own function, e.g. remnants (WD, NS, BH) and
     * MASSLESS_REMNANTs. These stars live forever,
     * so there shouldn't be a stellar evolutionary
     * reason for the timestep to be short, except that
     * you might want to resolve (say) the WD cooling
     * track (this should be done elsewhere).
     */

    if(Use_timestep(DT_LIMIT_OTHER_STELLAR_TYPES))
    {
        Limit_timestep(*dt,
                       stardata->preferences->timestep_multipliers[DT_LIMIT_OTHER_STELLAR_TYPES],
                       star,
                       DT_LIMIT_OTHER_STELLAR_TYPES);
    }

    /*
     * Force double detonations to explode on time
     */
#define _DDet_limit(TYPE)                                               \
    if(star->stellar_type == TYPE##WD &&                                \
       star->age < stardata->preferences->minimum_age_for_##TYPE##WD_DDet) \
    {                                                                   \
        const double _dt = star->age - stardata->preferences->minimum_age_for_##TYPE##WD_DDet; \
        Limit_timestep(*dt,                                             \
                       -_dt*0.9,                                        \
                       star,                                            \
                       DT_LIMIT_##TYPE##WD_DDet);                       \
    }
    _DDet_limit(CO);
    _DDet_limit(ONe);

    /* these stars live forever */
    *time_remaining = LONG_TIME_REMAINING;

#ifdef HRDIAG
    stardata->model.hrdiag_dt_guess = (star->luminosity > 0.1) ? 0.01 : 100.0;
#ifdef HRDIAG_DTLOG
    printf("HRDT (deltat) remnant %g\n",stardata->model.hrdiag_dt_guess);
#endif
    HRDIAG_DT_NEGATIVE_CHECK;
#endif
}
