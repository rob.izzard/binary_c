#pragma once
#ifndef TIMESTEP_PARAMETERS_H
#define TIMESTEP_PARAMETERS_H

#include "timestep_parameters.def"

#undef X
#define X(CODE) DT_LIMIT_##CODE,
enum { TIMESTEP_LIMITERS_LIST };
#undef X
#define X(CODE) #CODE,
static const char* timestep_limiter_strings[] Maybe_unused = { TIMESTEP_LIMITERS_LIST };
#undef X

#define Dt_limit_string(N) (timestep_limiter_strings[(N)])

#endif // TIMESTEP_PARAMETERS_H
