#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "timestep_debug.h"

void timestep_increment_fixed_timesteps(struct stardata_t * Restrict const stardata)
{
    /*
     * Increment the various fixed timesteps, to be performed at the
     * end of the timestep after everything else (e.g. logging) has
     * been done.
     */
    int i;
    Set_debug_dt;

    stardata->model.fixed_timestep_triggered = FALSE;

    for(i=0;i<FIXED_TIMESTEP_NUMBER;i++)
    {
        struct binary_c_fixed_timestep_t * t = stardata->model.fixed_timesteps + i;
        if(t->enabled == TRUE)
        {
            Boolean trigger = timestep_fixed_trigger(stardata,i);

            if(trigger == TRUE)
            {
                t->previous_trigger = t->logarithmic ? log10(stardata->model.time) : stardata->model.time;
                stardata->model.fixed_timestep_triggered = TRUE;
            }

            if(More_or_equal(stardata->model.time,t->logarithmic ? exp10(t->end) : t->end)
               ||
               Less_or_equal(stardata->model.time,t->logarithmic ? exp10(t->begin) : t->begin))
            {
                /*
                 * Outside valid time range, do nothing
                 */
            }
            else if(trigger == TRUE)
            {
                if(t->nextfunc)
                {
                    /*
                     * Use the next function to compute the
                     * next stop time
                     */
                    t->next = t->nextfunc(stardata);
                }
                else
                {
                    /*
                     * this should work, but is liable to numerical errors
                     */
                    //t->next += t->step;

                    /*
                     * instead, use the current time and increment it, this is still
                     * prone to errors, but less so
                     */
                    t->next = (t->logarithmic ? log10(stardata->model.time) : stardata->model.time)
                        + t->step;
                }
                Dprint("%sincrement t->next to %30.20g%s\n",
                       stardata->store->colours[MAGENTA],
                       t->next,
                       stardata->store->colours[COLOUR_RESET]);
            }
            t->previous_test = t->logarithmic ? log10(stardata->model.time) : stardata->model.time;
        }
    }
}
