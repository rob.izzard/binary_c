#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "timestep_debug.h"

void timestep_massless_remnant(Timestep_prototype_args)
{
    /*
     * Massless remnants should have long timesteps
     */
    if(Use_timestep(DT_LIMIT_MASSLESS_REMNANT))
    {
        Limit_timestep(*dt,stardata->preferences->timestep_multipliers[DT_LIMIT_MASSLESS_REMNANT]*LONG_TIMESTEP,star,DT_LIMIT_MASSLESS_REMNANT);
    }
    *time_remaining = LONG_TIMESTEP;
}
