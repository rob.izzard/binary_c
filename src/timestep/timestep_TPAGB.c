#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "timestep_debug.h"

void timestep_TPAGB(Timestep_prototype_args)
{
#ifdef BSE
    const double accretion_rate = Mdot_net(star);
    const double mc = Outermost_core_mass(star);
    const double menv = star->mass - mc;

    /*
     * Resolve changes the envelope down to 1e-4 Msun,
     * and all changes to the core
     */
    const double tmenv = 1e-6 * fabs(Max(menv,1e-4) / Max(fabs(star->derivative[DERIVATIVE_STELLAR_MASS]),1e-12));
    const double tcore = 1e-6 * fabs(mc / Max(1e-12,star->derivative[DERIVATIVE_STELLAR_CORE_MASS]));
    const double tstar = Min(tmenv,tcore); /* Myr */
    Limit_timestep(*dt,stardata->preferences->timestep_multipliers[DT_LIMIT_TPAGB]*tstar,star,DT_LIMIT_TPAGB);

    if(accretion_rate > 0.0)
    {
        /*
         * AGB star is accreting, so is likely
         * a converted white dwarf. In this case, use the
         * limit from the mass transfer, rather than
         * attempting to calculate the nuclear timescale
         * here (we cannot possibly know it).
         */
        *time_remaining = LONG_TIME_REMAINING;
#ifdef NUCSYN
#ifdef TPAGB_SPEEDUP
        if(star->num_thermal_pulses>TPAGB_SPEEDUP_AFTER)
        {
            Limit_timestep(*dt,0.01,star,DT_LIMIT_TPAGB_NUCSYN_SPEEDUP);
        }
        else
#endif //TPAGB_SPEEDUP
        {
            const double tip = star->interpulse_period*stardata->preferences->dtfac*0.9999;
            Limit_timestep(*dt,tip,star,DT_LIMIT_TPAGB_NUCSYN_INTERPULSE);
        }
#endif//NUCSYN
    }
    else
    {
        const int core_algorithm = AGB_Core_Algorithm;
        if(core_algorithm == AGB_CORE_ALGORITHM_HURLEY)
        {

            /*
             * Time left estimate : it's usually very bad
             */
            const double dtt = tscls[age<tscls[T_TPAGB_TX] ? 10 : 11] - age;
            if(dtt > 1e-4 && Use_timestep(DT_LIMIT_TPAGB))
            {
                Limit_timestep(*dt,Min(stardata->preferences->timestep_multipliers[DT_LIMIT_TPAGB]*dtt,0.005),star,DT_LIMIT_TPAGB);
            }

            /*
             * Use the star's shortest timescale: mass loss or core growth.
             */

#ifdef HRDIAG
            stardata->model.hrdiag_dt_guess = HRDIAG_FRAC* dtt;
#ifdef HRDIAG_DTLOG
            printf("HRDT (deltat) TPAGB %g\n",stardata->model.hrdiag_dt_guess);
#endif
            HRDIAG_DT_NEGATIVE_CHECK;
#ifdef HRDIAG_EXTRA_RESOLUTION_AT_END_AGB

            if(menv < 0.1)
            {
                stardata->model.hrdiag_dt_guess = Min(stardata->model.hrdiag_dt_guess,
                                                      0.1*exp10(-log10(Max(1.0,star->luminosity))));
            }
#endif //HRDIAG_EXTRA_RESOLUTION_AT_END_AGB
#endif  //HRDIAG

            *time_remaining = tn - age;

#ifdef DTR_CHECKS
            // Rob's fudge for small envelopes, sometimes things get
            // unstable
            if(*time_remaining<1e-9)
            {
                *time_remaining = Max(*dt,*time_remaining);
            }
#endif //DTR_CHECKS
        }
        else
        {
            /*
             * We know the interpulse period, so we can use it to determine
             * a minimum timestep
             *
             * If we're at a large thermal pulse number, assume
             * the AGB star has really become asymptotic in all
             * regards, including nucleosynthesis. In this case we
             * can extend the timestep.
             */
#ifdef TPAGB_SPEEDUP
            if(star->num_thermal_pulses>TPAGB_SPEEDUP_AFTER)
            {
                if(Use_timestep(DT_LIMIT_TPAGB_NUCSYN_SPEEDUP))
                {
                    Limit_timestep(*dt,stardata->preferences->timestep_multipliers[DT_LIMIT_TPAGB_NUCSYN_SPEEDUP],star,DT_LIMIT_TPAGB_NUCSYN_SPEEDUP);
                }
            }
            else
#endif // TPAGB_SPEEDUP
            if(Use_timestep(DT_LIMIT_TPAGB_NUCSYN_INTERPULSE))
            {
                double tip = star->interpulse_period;

                if(tip < 1e-6)
                {
                    /*
                     * recalculate interpulse
                     */
                    tip = Karakas2002_interpulse_period(stardata,
                                                        star->mc_1tp,
                                                        star->mass,
                                                        star->mc_1tp,
                                                        stardata->common.metallicity,
                                                        0.0,
                                                        star);
                }

                tip *= stardata->preferences->dtfac*
                    stardata->preferences->timestep_multipliers[DT_LIMIT_TPAGB_NUCSYN_INTERPULSE];


                // NB tip is in Myr
                Limit_timestep(*dt,tip,star,DT_LIMIT_TPAGB_NUCSYN_INTERPULSE);
            }

#if defined SLOW_DOWN_PREROCHE_TPAGB && !defined RLOF_REDUCE_TIMESTEP
            /*
             * Slow down as we approach RLOF, we want to make this
             * as accurate as possible.
             */
            if(stardata->model.sgl == FALSE &&
               Use_timestep(DT_LIMIT_TPAGB_NUCSYN_PREROCHE))
            {
                const double tip=star->interpulse_period*stardata->preferences->dtfac*0.9999;
                const double rr = Limit_range(1.0 - star->radius / star->roche_radius,0.1,1.0);
                Limit_timestep(*dt,rr*tip*stardata->preferences->timestep_multipliers[DT_LIMIT_TPAGB_NUCSYN_PREROCHE],star,DT_LIMIT_TPAGB_NUCSYN_PREROCHE);
            }
#endif// SLOW_DOWN_PREROCHE_TPAGB

            /*
             * Ignore time_remaining : we don't really know this because
             * the core mass determines what happens, not the time
             */

            *time_remaining = LONG_TIME_REMAINING;

#ifdef KARAKAS2002_SMOOTH_AGB_RADIUS_TRANSITION
            /* Approach second pulse slowly */

            if(Use_timestep(DT_LIMIT_TPAGB_NUCSYN_KARAKAS_SMOOTH) &&
               star->age-TPAGB_start_time(star)<1e-6*KARAKAS2002_SMOOTH_AGB_RADIUS_TRANSITION_SMOOTHING_TIME)
            {
                double newdt= stardata->preferences->timestep_multipliers[DT_LIMIT_TPAGB_NUCSYN_KARAKAS_SMOOTH] * *dt;
                Limit_timestep(*dt,newdt,star,DT_LIMIT_TPAGB_NUCSYN_KARAKAS_SMOOTH);
            }
#endif
        }
    }
#endif//BSE
    Dprint("star %d stellar_type %d tmenv %g, tcore %g -> tstar %g -> dt suggest %g Myr, now %g\n",
           star->starnum,
           star->stellar_type,
           tmenv,
           tcore,
           tstar,
           stardata->preferences->timestep_multipliers[DT_LIMIT_TPAGB]*tstar,*dt);
}
