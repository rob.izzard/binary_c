#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "timestep_debug.h"

/*
 * Return TRUE if any fixed timestep is triggered
 */
Boolean timestep_trigger_any_fixed_timestep(struct stardata_t * const stardata)
{
    for(int i=0;i<FIXED_TIMESTEP_NUMBER;i++)
    {
        struct binary_c_fixed_timestep_t * const t = stardata->model.fixed_timesteps + i;

        if(t->enabled == TRUE)
        {
            const double T = t->logarithmic ? log10(stardata->model.time) : stardata->model.time;
            if(Less_or_equal(T,t->end) &&
               timestep_fixed_trigger(stardata,i))
            {
                return TRUE;
            }
        }
    }
    return FALSE;
}
