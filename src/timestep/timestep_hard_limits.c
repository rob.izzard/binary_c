#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "timestep.h"

void timestep_hard_limits(Timestep_prototype_args)
{
#ifdef DTR_CHECKS
    if(*time_remaining < 0.0)
    {
        /*
         * This can happen in the case of common envelope evolution, when
         * a small envelope is left over: evolution after this should be
         * quick
         */
        *time_remaining = stardata->preferences->minimum_timestep;
    }
#endif

    /*
     * Impose global maximum and minimum timesteps:
     * NB do not impose these limit reasons because
     * these are *always* applied except when the fixed-timestep
     * algorithm requires something shorter.
     */
    if(star->dtlimiter != DT_LIMIT_FIXED_TIMESTEP)
    {
        Floor_timestep(*dt,
                       stardata->preferences->minimum_timestep,
                       star,
                       DT_LIMIT_MINIMUM_TIMESTEP);
    }

    Limit_timestep(*dt,
                   stardata->preferences->maximum_timestep,
                   star,
                   DT_LIMIT_MAXIMUM_TIMESTEP);

    if(Is_really_not_zero(stardata->preferences->maximum_timestep_by_stellar_type[star->stellar_type]))
    {
        Limit_timestep(*dt,
                       stardata->preferences->maximum_timestep_by_stellar_type[star->stellar_type],
                       star,
                       DT_LIMIT_MAXIMUM_TIMESTEP_BY_STELLAR_TYPE);
    }

    /*
     * If there has been an event, perhaps limit the next timestep?
     */
    if(0&&stardata->common.n_events > 0)
    {
        Limit_timestep(*dt,
                       stardata->preferences->minimum_timestep,
                       star,
                       DT_LIMIT_MAXIMUM_TIMESTEP);
    }

    if(NUCLEAR_BURNING(star->stellar_type))
    {
        Limit_timestep(*dt,
                       stardata->preferences->maximum_nuclear_burning_timestep,
                       star,
                       DT_LIMIT_MAXIMUM_TIMESTEP);
    }

    /*
     * If we want to start accreting soon, resolve the start time
     */
    if(artificial_accretion(stardata))
    {
        const double dtart = stardata->preferences->artificial_accretion_start_time - stardata->model.time;
        if(More_or_equal(dtart, 0.0))
        {
            Limit_timestep(*dt,
                           dtart,
                           star,
                           DT_LIMIT_ARTIFICIAL_ACCRETION);
        }
    }

    /*
     * If there has been an event, force a small timestep
     */
    if((star->SN_type != SN_NONE ||
        stardata->previous_stardata->star[star->starnum].SN_type != SN_NONE))
    {
        Limit_timestep(*dt,
                       stardata->preferences->minimum_timestep,
                       star,
                       DT_LIMIT_SN);
    }


    /*
     * Burn in by running the first BURN_IN_TIMESTEPS
     *
     */
    if(stardata->model.model_number < BURN_IN_TIMESTEPS)
    {
        Limit_timestep(*dt,
                       stardata->preferences->minimum_timestep,
                       star,
                       DT_LIMIT_BURN_IN);
    }

    /*
     * Resolve time remaining : this means we don't
     * overshoot the end of the stellar phase
     */

    if(star->stellar_type != MASSLESS_REMNANT &&
       *dt > *time_remaining)
    {
        if(star->starnum == 0)
        {
            Limit_timestep(*dt,
                           *time_remaining,
                           star,
                           DT_LIMIT_TIME_REMAINING_0);
        }
        else if(star->starnum == 1)
        {
            Limit_timestep(*dt,
                           *time_remaining,
                           star,
                           DT_LIMIT_TIME_REMAINING_1);
        }
        else
        {
            Limit_timestep(*dt,
                           *time_remaining,
                           star,
                           DT_LIMIT_TIME_REMAINING_OTHER);
        }
    }
}
