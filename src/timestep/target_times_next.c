#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "timestep_debug.h"

double target_times_next(struct stardata_t * const stardata)
{
    /*
     * Find the next target time.
     *
     * Note: this is a linear search. The numbers in the list are
     * not guaranteed to be sorted, and anyway should (hopefully)
     * not be too long... we can do better!
     */
    double mintime = LONG_TIME_REMAINING;
    if(stardata->tmpstore->target_times != NULL)
    {

        for(ssize_t i=0; i<stardata->tmpstore->target_times->n; i++)
        {
            const double _time = stardata->tmpstore->target_times->doubles[i];
            if(_time > stardata->model.time &&
               _time < mintime)
            {
                mintime = _time;
            }
        }
    }
    return mintime;
}
