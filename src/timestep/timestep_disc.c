#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "timestep_debug.h"

#ifdef DISCS

double Pure_function timestep_disc(struct stardata_t * const stardata,
                                   struct disc_t * const disc)
{
    /*
     * Timestep for the evolution code based on its disc.
     */
    double dt = 0.0;

    if(Use_timestep(DT_LIMIT_CIRCUMBINARY_DISC))
    {
        dt = Disc_is_disc(disc) ?
            (1e-6 * stardata->preferences->timestep_multipliers[DT_LIMIT_CIRCUMBINARY_DISC] * disc->dt / YEAR_LENGTH_IN_SECONDS) :
            LONG_TIMESTEP;

        if(Is_zero(dt)) dt = 1e-6;
    }
    else
    {
        dt = LONG_TIMESTEP;
    }
    return dt;
}

#endif // DISCS
