#include "binary_c_main.h"
/*
 * The binary_c stellar population nucleosynthesis framework.
 *
 * Copyright Robert Izzard 2000-2025
 *
 * Contact rob.izzard@gmail.com
 *
 * Code repository:
 * https://gitlab.com/binary_c
 *
 * Documentation is in doc/
 * Code is in src/
 *
 * To build, run
 *
 * ---
 *
 * meson setup builddir
 * cd builddir
 * ninja binary_c_install
 *
 * ---
 *
 * Please see the files README, LICENCE and CHANGES,
 * and the doc/ directory for documentation which is mirrored
 * at https://binary_c.gitlab.io
 *
 **********************
 *
 * Subroutine: main
 *
 * Purpose: The main function of binary_c, called when the binary_c
 *          executable is run.
 *
 * Arguments:
 * int argc - command-line argument count (int)
 * char ** argv - array of command-line argument strings
 *
 * Returns:
 * status - 0 means everything worked, other return codes
 *          are in binary_c_error_codes.def
 *
 **********************
 */

int main(int argc,
         char **  argv)
{
    /*
     * This is binary_c's entry point when called on the
     * command line.
     *
     * In main() we set up signals and floating-point
     * options, perhaps an alternative running stack,
     * make a stardata that contains all binary_c's data,
     * then send this to binary_c_main.
     */
#ifndef VALGRIND
    test_for_valgrind();
#endif

#ifdef ALTSTACK
    char * altstack = setup_segfaults();
#endif

    Set_FPU_precision;
    //Set_FPU_policy;
    Set_stack_size;

    struct stardata_t * stardata Aligned = NULL;

    const int ret = binary_c_main(argc,
                                  argv,
                                  &stardata);

#ifdef ALTSTACK
    Safe_free(altstack);
#endif
    Output_codestats;

    return ret;
}


static int binary_c_main(int argc,
                         char ** argv,
                         struct stardata_t ** Restrict s)
{
    ticks start_tick = getticks();
    struct stardata_t * stardata Aligned = *s = NULL;
    struct preferences_t * preferences = NULL;
    struct store_t * store = NULL;

    /*
     * binary_c_main is where you end up if you call binary_c
     * from the command line.
     *
     * Most of what is in here is setup then a loop to run
     * binary stellar evolution, possibly in batchmode.
     *
     * First, do the main memory allocations and build the store
     */
    main_allocations(&stardata,
                     NULL,
                     &preferences,
                     &store,
                     NULL);

    *s = stardata;
    stardata->common.start_tick = start_tick;

    /*
     * set up GSL library
     */
    setup_GSL_handlers(stardata);

    /*
     * Check that mathematics works correcly
     */
    maths_checks(stardata);

    /* welcome message */
    Dprint("Welcome to the Binary Star Evolution program binary_c/nucsyn version %s; written by Robert Izzard %d-%d \n",
            BINARY_C_VERSION,
            BINARY_C_START_YEAR,
            BINARY_C_END_YEAR);

    /* examine command line arguments */
    set_up_variables(argc,
                     argv,
                     NULL,
                     stardata);

#ifdef MAKE_BSE_TABLES
    make_BSE_tables(stardata);
#endif



    /*
     * save argument pointer and count
     */
    stardata->common.argv = argv;
    stardata->common.argc = argc;

    /*
     * Detect whether a system list is being used
     */
    const Boolean use_system_list = (stardata->preferences->system_list_path[0] != '\0');
    if(use_system_list)
    {
        chomp(stardata->preferences->system_list_path);
    }

    /*
     * set up repeat-run loop
     *
     * binary_c has a parameter preferences->repeat which you can
     * use to run a systems repeatedly.
     */
    const Boolean multisystem =
        stardata->preferences->repeat > 1 ||
        use_system_list == TRUE;

    Dprint("Repeat loop: multisystem = %s stardata->preferences=%p, use_system_list = %s\n",
           Yesno(multisystem),
           (void*)stardata->preferences,
           Yesno(use_system_list));

    int repeat = 0;
    char * next_arg_string = use_system_list == TRUE ? read_system_list(stardata,TRUE) : NULL;
    while(repeat < stardata->preferences->repeat ||
          next_arg_string != NULL)
    {
        /*
         * if we're repeating we need to reset and reload vars
         */
        if(multisystem == TRUE && repeat)
        {
            if(repeat > 0)
            {
                /*
                 * Free the old stardata
                 */
                struct preferences_t * p = stardata->preferences;
                struct persistent_data_t * persistent = stardata->persistent_data;
                free_memory(
                    &stardata,
                    FALSE, // preferences
                    TRUE, // stardata
                    FALSE, // store
                    TRUE, // raw buffer
                    FALSE // persistent should be kept
                    );
                *s = stardata = NULL;
                /*
                 * Make a new stardata
                 */
                main_allocations(&stardata,
                                 NULL,
                                 &preferences,
                                 &store,
                                 &persistent);
                *s = stardata;
                stardata->preferences = p;
                stardata->common.start_tick = start_tick;
                stardata->common.argv = argv;
                stardata->common.argc = argc;
                stardata->cpu_is_warm = TRUE;
            }
            set_default_preferences(stardata);
            set_up_variables(argc,argv,NULL,stardata);
#ifdef LOG_REPEAT_NUMBER
            stardata->model.repeat_number = repeat;
#endif //LOG_REPEAT_NUMBER
            stardata->model.model_number = 0;
        }

#ifdef DISC_TESTING
        tbdisc_testing(stardata);
#endif

#ifdef RANDOM_SYSTEMS
        /* perhaps randomize input parameters */
        if(stardata->preferences->random_systems)
        {
            set_random_system(stardata,
                              RANDOM_SYSTEM_OUTPUT_WITH_TIME);
        }
#endif //RANDOM_SYSTEMS

        if(next_arg_string != NULL)
        {
            parse_arguments_from_string(next_arg_string,
                                        stardata);
            Safe_free(next_arg_string);
        }

#ifdef BATCHMODE
        Dprint("Entering batch processing (batchmode=%d)\n",
               stardata->preferences->batchmode);
#else
        Dprint("Calling evolve_system(stardata)\n");
#endif //BATCHMODE

#ifdef BATCHMODE
        if(Batchmode_is_on(stardata->preferences->batchmode))
        {
            /*
             * Call the batchmode loop handler to evolve many stars
             */
            Call_batchmode_loop;
        }
        else
        {
#endif // BATCHMODE
            /*
             * Call evolve_system() to evolve the stars
             */
            evolve_system(stardata);
#ifdef BATCHMODE
        }
#endif // BATCHMODE

        if(multisystem == TRUE &&
           check_stopfile(stardata) == TRUE)
        {
            /*
             * stopfile exists, prevent repeats
             */
            repeat = stardata->preferences->repeat + 1;
        }

        if(multisystem == TRUE &&
           stardata->preferences->pause_after_repeat)
        {
            printf("Waiting for stdin\n");
            fflush(stdout);
            getchar();
        }
        repeat++;

        if(use_system_list == TRUE)
        {
            next_arg_string = read_system_list(stardata,TRUE);
        }
    }

    if(use_system_list == TRUE)
    {
        binary_c_fclose(&stardata->store->system_list);
    }

    Dprint ("End of binary star evolution program.\n");

    /* show timers */
    Show_timers;

    /* free all allocated memory */
    free_all_memory(s);

    return BINARY_C_NORMAL_EXIT;
}

#ifdef CODESTATS
static void output_codestats(void)
{
    Boolean same = FALSE;
    unsigned int n,i;
    fflush(NULL);
    for(n=0;n<CODESTAT_NUMBER;n++)
    {
        fprintf(stderr,
                "%25s : %lu -> %u\n",
                Codestat_string(n),
                codestats.counters[n],
                codestats.nentries[n]
            );
        for(i=0;i<codestats.nentries[n];i++)
        {
            if(same == FALSE)
            {
                printf("  %25s called %lu time%s in %s line(s) %u",
                       Codestat_string(n),
                       codestats.entries[n][i].count,
                       codestats.entries[n][i].count==1 ? "" : "s",
                       codestats.entries[n][i].file,
                       codestats.entries[n][i].line);
            }
            else
            {
                printf(",%u",
                       codestats.entries[n][i].line);
            }

            if(i+1<codestats.nentries[n] &&
               codestats.entries[n][i].count ==
               codestats.entries[n][i+1].count &&
               Strings_equal(codestats.entries[n][i].file,
                             codestats.entries[n][i+1].file))
            {
                /* same filenames */
                same = TRUE;
            }
            else
            {
                printf("\n");
                same = FALSE;
            }
            Safe_free(codestats.entries[n][i].file);
        }
        Safe_free(codestats.entries[n]);
    }
    fflush(NULL);
}
#endif//CODESTATS
