#include "../binary_c.h"
No_empty_translation_unit_warning;
#include "pulsation_macros.h"

/*
 * Free pulsator store data
 */
void free_pulsator_store(struct store_t * const Restrict store)
{
    if(store->pulsator_ellipses != NULL)
    {
        Pulsator_ID i;
        for(i=0; i<store->num_pulsator_ellipses; i++)
        {
            Safe_free(store->pulsator_ellipses[i].name);
        }
        Safe_free(store->pulsator_ellipses);
    }
}
