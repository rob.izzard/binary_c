#include "../binary_c.h"
No_empty_translation_unit_warning;
#include "pulsation_macros.h"

/*
 * Make pulsator store data in the binary_c store
 * Thanks to Simon Jeffery for the data.
 */
void make_pulsator_store(struct store_t * const Restrict store)
{
    if(store->pulsator_ellipses == NULL)
    {
#define _N 5

#undef X
#define X(MACRO,STRING,LOGTEFF,LOGL,A,B,P) LOGTEFF,LOGL,A,B,P,
        const double data[] = { CSJ_PULSATOR_LIST };

#undef X
#define X(MACRO,STRING,LOGTEFF,LOGL,A,B,P) STRING,
        static const char * strings[] = { CSJ_PULSATOR_LIST };
#undef X


        store->num_pulsator_ellipses = Array_size(strings);
        store->pulsator_ellipses =
            Malloc(sizeof(struct pulsator_ellipse_t) * store->num_pulsator_ellipses);
        Pulsator_ID i;
        for(i=0; i<store->num_pulsator_ellipses; i++)
        {
            store->pulsator_ellipses[i].logTeff = data[i*_N + 0];
            store->pulsator_ellipses[i].logL = data[i*_N + 1];
            store->pulsator_ellipses[i].a = data[i*_N + 2];
            store->pulsator_ellipses[i].b = data[i*_N + 3];
            // note CSJ's angles are negative of what they should be
            store->pulsator_ellipses[i].p = data[i*_N + 4];
            store->pulsator_ellipses[i].name = strdup(strings[i]);
            /*
            printf("pulsator %d \"%s\" : (%g,%g) a=%g b=%g angle=%g\n",
                   (int)i,
                   store->pulsator_ellipses[i].name,
                   store->pulsator_ellipses[i].logTeff,
                   store->pulsator_ellipses[i].logL,
                   store->pulsator_ellipses[i].a,
                   store->pulsator_ellipses[i].b,
                   store->pulsator_ellipses[i].p);
            */
        }
    }
}
