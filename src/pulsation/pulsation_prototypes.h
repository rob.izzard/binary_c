#pragma once
#ifndef PULSATION_PROTOTYPES_H
#define PULSATION_PROTOTYPES_H

void id_pulsator(struct stardata_t * const stardata,
                 const double logTeff,
                 const double logL,
                 Pulsator_ID * const n,
                 Pulsator_ID ** ids);

Boolean in_pulsator_ellipse(const double logTeff,
                            const double logL,
                            const struct pulsator_ellipse_t * const ellipse);

void make_pulsator_store(struct store_t * const Restrict store);
void free_pulsator_store(struct store_t * const Restrict store);

#endif // PULSATION_PROTOTYPES_H
