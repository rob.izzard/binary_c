#include "../binary_c.h"
No_empty_translation_unit_warning;

Boolean in_pulsator_ellipse(const double logTeff,
                            const double logL,
                            const struct pulsator_ellipse_t * const ellipse)
{
    /*
     * Test if a point (logTeff,logL) is in the
     * ellipse: return TRUE if so, FALSE otherwise.
     *
     * see e.g.
     * https://en.wikipedia.org/wiki/Ellipse#rotated
     *
     * The angle is zero parallel to the y (logL) axis,
     * then is anticlockwise rotated (radians)
     *
     * beward that the following page has the opposite sign
     * https://stackoverflow.com/questions/7946187/point-and-ellipse-rotated-position-test-algorithm
     */
    const double dx = logTeff - ellipse->logTeff;
    const double dy = logL - ellipse->logL;
    const double c = cos(ellipse->p);
    const double s = sin(ellipse->p); // same as sin(ellipse->p) but faster
    const double distance =
        Pow2((dx * c + dy * s)/ellipse->a) +
        Pow2((dx * s - dy * c)/ellipse->b);
    const Boolean in_ellipse = distance <= 1.0 ? TRUE : FALSE;

    /*
    printf("At %g,%g test in ellipse (%g,%g, angle %g -> c=%g, s=%g) a=%g b=%g : ",
           logTeff,
           logL,
           ellipse->logTeff,
           ellipse->logL,
           ellipse->p,
           c,
           s,
           ellipse->a,
           ellipse->b);
    printf("dx = %g, dy = %g -> %g + %g : distance (sq %g) %g -> %s\n",
           dx,
           dy,
           Pow2((dx * c - dy * s)/ellipse->a),
           Pow2((dx * s + dy * c)/ellipse->b),
           distance,
           sqrt(distance),
           Yesno(in_ellipse));
    */

    return in_ellipse;
}
