#include "../binary_c.h"
No_empty_translation_unit_warning;

/*
 *       BSE library function
 *
 *       Computes the characteristic luminosities and timescales
 *       at different stages of stellar evolution.
 *
 *       Ref: P.P. Eggleton, M.J. Fitchett & C.A. Tout (1989) Ap.J. 347, 998.
 *
 *       Revised 27th March 1995 by C. A. Tout
 *       and 24th October 1995 to include metallicity
 *       and 13th December 1996 to include naked helium stars
 *
 *       Revised 5th April 1997 by J. R. Hurley
 *       to include Z=0.001 as well as Z=0.02, convective overshooting,
 *       MS hook and more elaborate CHeB. It now also sets the Giant
 *       Branch parameters relevant to the mass of the star.
 *
 *       ------------------------------------------------------------
 *       Times: 0: Main sequence T_MS
 *              1: BGB  T_BGB
 *              2: He ignition T_HE_IGNITION (set only if hydrogen burning)
 *              3: He burning T_HE_BURNING (set only if hydrogen burning)
 *              4: Giant t(inf1) T_GIANT_TINF_1
 *              5: Giant t(inf2) T_GIANT_TINF_2
 *              6: Giant t(Mx) T_GIANT_TX
 *              7: FAGB t(inf1) T_EAGB_TINF_1
 *              8: FAGB t(inf2) T_EAGB_TINF_2
 *              9: FAGB  t(Mx) T_EAGB_T
 *             10: SAGB t(inf1) T_TPAGB_TINF_1
 *             11: SAGB t(inf2) T_TPAGB_TINF_2
 *             12: SAGB  t(Mx) T_TPAGB_TX
 *             13: TP T_TPAGB_FIRST_PULSE
 *             14: t(Mcmax) T_TMCMAX
 *
 *       Luminosities:  1: ZAMS L_ZAMS
 *                      2: End MS L_END_MS
 *                      3: BGB L_BGB
 *                      4: He ignition L_HE_IGNITION (set only if hydrogen burning)
 *                      5: He burning L_HE_BURNING (set only if hydrogen burning)
 *                      6: L(Mx) L_LMX
 *                      7: BAGB L_BAGB
 *                      8: TP L_TPAGB_FIRST_PULSE
 *
 *       GB:    1: effective A(H) GB_EFFECTIVE_AH
 *              2: A(H,He) GB_A_H_HE
 *              3: B GB_B
 *              4: D GB_D
 *              5: p GB_p
 *              6: q GB_q
 *              7: Mx GB_Mx
 *              8: A(He) GB_A_HE
 *              9: Mc,BGB GB_MC_BGB
 *             10: Axel parameter 1 GB_AXEL_1
 *             11: Axel parameter 2 GB_AXEL_2
 *
 *       ------------------------------------------------------------
 */
#ifdef MINT
#include "../MINT/MINT.h"
#endif


#undef Cprint
#if (DEBUG==1)
#define Cprint(...)                                       \
    if((DEBUG) && (Debug_expression))                     \
        debug_fprintf(__FILE__,__LINE__,__VA_ARGS__);
//#undef STELLAR_TIMESCALES_CACHE
#else
#define Cprint(...) /**/;
#endif

#define OUTLOOP(A,B) {                                                  \
        unsigned int i;                                                 \
        for(i=1;i<=A;i++)                                               \
        {                                                               \
            Dprint_no_newline("%u=%12.12e %s",i,B[i],i==A?"":":");      \
        }                                                               \
        Dprint("%s","");                                                \
    }
#define OUTLOOP_STRING(A,S,B) {                                         \
        unsigned int i;                                                 \
        for(i=1;i<A;i++)                                                \
        {                                                               \
            Dprint_no_newline("%10u = %20s = %s%12.12e%s %s",           \
                              i,                                        \
                              S[i],                                     \
                              ((i>0 && B[i]<B[i-1]) ? stardata->store->colours[BRIGHT_YELLOW] : ""), \
                              B[i],                                     \
                              stardata->store->colours[COLOUR_RESET],                             \
                              i==A?"":":");                             \
        }                                                               \
        Dprint("%s","");                                                \
    }

#ifdef STELLAR_TIMESCALES_CACHE_DEBUG
static void stellar_timescales_dump(struct BSE_data_t * const bse);
#endif // STELLAR_TIMESCALES_CACHE_DEBUG

void stellar_timescales(struct stardata_t * Restrict const stardata,
                        struct star_t * Restrict const star,
                        struct BSE_data_t * const bse,
                        double mass, /* phase start mass */
                        double mt, /* mass at the current time */
                        const Stellar_type stellar_type
    )
{
#ifdef STELLAR_TIMESCALES_CACHE
    Boolean setcache = FALSE;
#endif // STELLAR_TIMESCALES_CACHE

    /* shortcuts */
    double * const timescales = bse->timescales;
    double * const luminosities = bse->luminosities;
    double * const GB = bse->GB;

    Dprint("stellar timescales star %d in st=%d %d m0=%g mt=%g age=%g\ntimescales = %p, luminosities = %p, GB = %p\n",
           star->starnum,
           stellar_type,
           star->stellar_type,
           mass,
           mt,
           star->age,
           (void*)timescales,
           (void*)luminosities,
           (void*)GB);

    /*
     * set arrays to 0 to prevent errors during debugging
     */
    memset(timescales,0,sizeof(double)*TSCLS_ARRAY_SIZE);
    memset(luminosities,0,sizeof(double)*LUMS_ARRAY_SIZE);
    memset(GB,0,sizeof(double)*GB_ARRAY_SIZE);

    /*
     * If mass is zero, just return, there's nothing we can do
     */
    if(Is_zero(mass)) return;

    mass = fabs(mass);
    mt = fabs(mt);
#ifdef MINIMUM_STELLAR_MASS
    mass = Max(MINIMUM_STELLAR_MASS,mass);
    mt = Max(MINIMUM_STELLAR_MASS,mt);
#endif

#ifdef STELLAR_TIMESCALES_CACHE
    /*
     * The STELLAR_TIMESCALES cache caches the parameters into the function
     * and returns saved results (saving recalculation)...
     *
     * NB only activate the cache for non-remnants
     */

#define STELLAR_TIMESCALES_CACHE_SIZE 2
#define CACHE_LEN (GB_ARRAY_SIZE+LUMS_ARRAY_SIZE+TSCLS_ARRAY_SIZE)

#define stellar_timescales_cache_stellar_type (stardata->tmpstore->stellar_timescales_cache_stellar_type)
#define stellar_timescales_cache_mass (stardata->tmpstore->stellar_timescales_cache_mass)
#define stellar_timescales_cache_mt (stardata->tmpstore->stellar_timescales_cache_mt)
#define stellar_timescales_cache_mc (stardata->tmpstore->stellar_timescales_cache_mc)
#define stellar_timescales_cache_GB (stardata->tmpstore->stellar_timescales_cache_GB)
#define stellar_timescales_cache_luminosities (stardata->tmpstore->stellar_timescales_cache_luminosities)
#define stellar_timescales_cache_timescales (stardata->tmpstore->stellar_timescales_cache_timescales)
#define stellar_timescales_cache_tm (stardata->tmpstore->stellar_timescales_cache_tm)
#define stellar_timescales_cache_tn (stardata->tmpstore->stellar_timescales_cache_tn)

    int i;
#endif // STELLAR_TIMESCALES_CACHE



    if(stellar_type<HeWD)
    {
#ifdef BSE
#ifdef STELLAR_TIMESCALES_CACHE
        Boolean setcache = TRUE;
        Cprint("STELLAR_TIMESCALES args %d %g %g %g\n",stellar_type,mass,mt,Outermost_core_mass(star));

        /* check cache */
        for(i=0;i<STELLAR_TIMESCALES_CACHE_SIZE;i++)
        {
            if((stellar_timescales_cache_stellar_type[i]==stellar_type) &&
               Fequal(stellar_timescales_cache_mt[i],mt) &&
               Fequal(stellar_timescales_cache_mc[i],Outermost_core_mass(star)) &&
               Fequal(stellar_timescales_cache_mass[i],mass))
            {

                Cprint("STELLAR_TIMESCALES from cache line %d\n",i);
#ifdef STELLAR_TIMESCALES_CACHE_DEBUG
                stellar_timescales_dump(bse);
#endif //STELLAR_TIMESCALES_CACHE_DEBUG

                /* return cache-matched results */
                memcpy(GB,&(stellar_timescales_cache_GB[i][0]),sizeof(double)*GB_ARRAY_SIZE);
                memcpy(luminosities,&(stellar_timescales_cache_luminosities[i][0]),sizeof(double)*LUMS_ARRAY_SIZE);
                memcpy(timescales,&(stellar_timescales_cache_timescales[i][0]),sizeof(double)*TSCLS_ARRAY_SIZE);
                bse->tm = stellar_timescales_cache_tm[i];
                bse->tn = stellar_timescales_cache_tn[i];
                star->tm = bse->tm;
                star->tn = bse->tn;
                Dprint("cache return age=%g\n",star->age);
                return;
            }
        }
        /* no cache match, do calculations */
#endif // STELLAR_TIMESCALES_CACHE

        Dprint("Check stellar type %d\n",stellar_type);

        /*
         * Decide the rest of the timescales based on
         * whether the star is hydrogen or helium rich
         */
        if(NAKED_HELIUM_STAR(stellar_type))
        {
            stellar_timescales_helium_stars(stardata,
                                            star,
                                            bse,
                                            mass,
                                            mt);
        }
        else
        {
            stellar_timescales_hydrogen_stars(stardata,
                                              star,
                                              bse,
                                              mass,
                                              mt,
                                              stellar_type);
        }
#endif // BSE
    }
    else
    {
#ifdef STELLAR_TIMESCALES_CACHE
        setcache = FALSE;
#endif // STELLAR_TIMESCALES_CACHE
        bse->tn = 1e10;
        bse->tm = 1e10;
    }

    Dprint("Check sanity\n");

#ifdef STELLAR_TIMESCALES_SANITY
    if(stellar_type < 7 &&
       timescales[T_BGB] < bse->tm)
    {
        Exit_binary_c(BINARY_C_TIMESCALE_ERROR,
                      "star %d : stellar type %d : tms < tbgb error : Timescales MS=%30.20e BGB=%30.20e\n",
                      star->starnum,
                      stellar_type,
                      bse->tm,
                      timescales[T_BGB]);
    }
#endif

    /*
     * Save timescales for this star
     */
    star->tm = bse->tm;
    star->tn = bse->tn;

#ifdef STELLAR_TIMESCALES_CACHE
    if(setcache == TRUE)
    {
        /* save results to the cache */

        /* shift cache and arg list down one */
        memmove(stellar_timescales_cache_mass+1,stellar_timescales_cache_mass,sizeof(double)*(STELLAR_TIMESCALES_CACHE_SIZE-1));
        memmove(stellar_timescales_cache_mt+1,stellar_timescales_cache_mt,sizeof(double)*(STELLAR_TIMESCALES_CACHE_SIZE-1));
        memmove(stellar_timescales_cache_mc+1,stellar_timescales_cache_mc,sizeof(double)*(STELLAR_TIMESCALES_CACHE_SIZE-1));
        memmove(stellar_timescales_cache_stellar_type+1,stellar_timescales_cache_stellar_type,sizeof(Stellar_type)*(STELLAR_TIMESCALES_CACHE_SIZE-1));

        memmove(stellar_timescales_cache_tm+1,stellar_timescales_cache_tm,sizeof(double)*(STELLAR_TIMESCALES_CACHE_SIZE-1));
        memmove(stellar_timescales_cache_tn+1,stellar_timescales_cache_tn,sizeof(double)*(STELLAR_TIMESCALES_CACHE_SIZE-1));
        memmove(&(stellar_timescales_cache_GB[1][0]),stellar_timescales_cache_GB,sizeof(double)*GB_ARRAY_SIZE*(STELLAR_TIMESCALES_CACHE_SIZE-1));
        memmove(&(stellar_timescales_cache_luminosities[1][0]),stellar_timescales_cache_luminosities,sizeof(double)*LUMS_ARRAY_SIZE*(STELLAR_TIMESCALES_CACHE_SIZE-1));
        memmove(&(stellar_timescales_cache_timescales[1][0]),stellar_timescales_cache_timescales,sizeof(double)*TSCLS_ARRAY_SIZE*(STELLAR_TIMESCALES_CACHE_SIZE-1));

        /* add new args to arg list */
        stellar_timescales_cache_mass[0]=mass;
        stellar_timescales_cache_mt[0]=mt;
        stellar_timescales_cache_mc[0]=Outermost_core_mass(star);
        stellar_timescales_cache_stellar_type[0]=stellar_type;

        /* save current result in the cache */
        memcpy(stellar_timescales_cache_GB,GB,sizeof(double)*GB_ARRAY_SIZE);
        memcpy(stellar_timescales_cache_luminosities,luminosities,sizeof(double)*LUMS_ARRAY_SIZE);
        memcpy(stellar_timescales_cache_timescales,timescales,sizeof(double)*TSCLS_ARRAY_SIZE);
        stellar_timescales_cache_tm[0]=*tm;
        stellar_timescales_cache_tn[0]=*tn;

        Cprint("STELLAR_TIMESCALES save to cache\n");
#ifdef STELLAR_TIMESCALES_CACHE_DEBUG
        stellar_timescales_dump(bse);
#endif //STELLAR_TIMESCALES_CACHE_DEBUG
        Cprint("STELLAR_TIMESCALES set cache %g (%g) %g (%g) %g (%g), %g (%g), %g (%g)\n",
               stellar_timescales_cache_GB[0][GB_EFFECTIVE_AH],GB[GB_EFFECTIVE_AH],
               stellar_timescales_cache_luminosities[0][L_ZAMS],luminosities[L_ZAMS],
               stellar_timescales_cache_timescales[0][T_MS],timescales[T_MS],
               stellar_timescales_cache_tm[0],*tm,
               stellar_timescales_cache_tn[0],*tn
            );
    }
#endif //STELLAR_TIMESCALES_CACHE




#if (DEBUG==1)
    Dprint("stellar timescales out star %d st=%d m=%g mt=%g Z=%g tm=%30.22e tn=%g age=%g\n",
           star->starnum,
           stellar_type,
           mass,
           mt,
           stardata->common.metallicity,
           bse->tm,
           bse->tn,
           star->age);

#undef X
#define X(CODE) "T_"#CODE ,
    static const char * const stellar_timescales_strings[] =
        { STELLAR_TIMESCALES_LIST };
#undef X
#define X(CODE) "L_"#CODE ,
    static const char * const stellar_luminosities_strings[] =
        { STELLAR_LUMINOSITIES_LIST };
#undef X
#define X(CODE) "GB_"#CODE ,
    static const char * const stellar_GB_strings[] =
        { STELLAR_GB_LIST };
#undef X

    OUTLOOP_STRING(TSCLS_ARRAY_SIZE,
                   stellar_timescales_strings,
                   timescales);

    Dprint("EAGB starts at T_HE_IGNITION + T_HE_BURNING = %g + %g = %g\n",
           timescales[T_HE_IGNITION],
           timescales[T_HE_BURNING],
           timescales[T_HE_IGNITION]+timescales[T_HE_BURNING]);

    OUTLOOP_STRING(LUMS_ARRAY_SIZE,
                   stellar_luminosities_strings,
                   luminosities);

    OUTLOOP_STRING(GB_ARRAY_SIZE,
                   stellar_GB_strings,
                   GB);

#endif // DEBUG

}

#ifdef STELLAR_TIMESCALES_CACHE_DEBUG
void stellar_timescales_dump(struct BSE_data_t * const bse)
{
    int i;
    printf("STELLAR_TIMESCALES dump: tm=%g tn=%g ",
           bse->tm,
           bse->tn);
    for(i=0;i<GB_ARRAY_SIZE;i++)
    {
        printf("%g ",bse->GB[i]);
    }
    for(i=0;i<LUMS_ARRAY_SIZE;i++)
    {
        printf("%g ",bse->lums[i]);
    }
    for(i=0;i<TSCLS_ARRAY_SIZE;i++)
    {
        printf("%g ",bse->tscls[i]);
    }
    printf("\n");
}
#endif
