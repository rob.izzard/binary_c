#ifndef STELLAR_TIMESCALES_PROTOTYPES_H
#define STELLAR_TIMESCALES_PROTOTYPES_H

#include "../binary_c_parameters.h"

#include "../binary_c_structures.h"
void stellar_timescales(struct stardata_t * Restrict const stardata,
                        struct star_t * Restrict const star,
                        struct BSE_data_t * const bse,
                        double mass, /* phase start mass */
                        double mt, /* instantaenous mass */
                        const Stellar_type stellar_type);
#ifdef BSE
void stellar_timescales_low_mass_AGB(struct stardata_t * const stardata Maybe_unused,
                                     struct star_t * const star Maybe_unused,
                                     struct BSE_data_t * const bse,
                                     const double * Restrict const giant_branch_parameters,
                                     const double * Restrict const metallicity_parameters,
                                     const double phase_start_mass,
                                     const double mass,
                                     const double mcbagb,
                                     const double ip,
                                     const double iq,
                                     const double mp,
                                     const double mq);

void stellar_timescales_massive_AGB(struct star_t * Restrict const star,
                                    struct BSE_data_t * const bse,
                                    const double mass,
                                    const double mt,
                                    const double mcbagb,
                                    const double ip,
                                    const double iq,
                                    const double mp,
                                    const double mq,
                                    const Stellar_type stellar_type);

void stellar_timescales_GB_parameters(struct BSE_data_t * Restrict const bse,
                                      const double mass,
                                      const double mp6);

int stellar_timescales_post_HG(struct stardata_t * const stardata,
                               struct star_t * const star,
                               struct BSE_data_t * const bse,
                               const double * const giant_branch_parameters,
                               const double * const metallicity_parameters,
                               const double tbagb,
                               const double ip,
                               const double iq,
                               const double GBp,
                               const double GBq,
                               const double mp,
                               const double mq,
                               const double mcbagb,
                               const double phase_start_mass,
                               const double mass,
                               const Stellar_type stellar_type);

void stellar_timescales_high_mass_GB(struct stardata_t * const stardata,
                                     struct BSE_data_t * const bse,
                                     const double * Restrict const giant_branch_parameters,
                                     const double mass,
                                     const double mp2);

void stellar_timescales_helium_stars(struct stardata_t * const stardata,
                                     struct star_t * const star,
                                     struct BSE_data_t * const bse,
                                     const double mass,
                                     const double mt);

int stellar_timescales_hydrogen_stars(struct stardata_t * const stardata,
                                      struct star_t * const star,
                                      struct BSE_data_t * const bse,
                                      const double phase_start_mass,
                                      const double mass,
                                      const Stellar_type stellar_type);

void stellar_timescales_low_mass_GB(struct stardata_t * const stardata,
                                    struct star_t * const star,
                                    struct BSE_data_t * const bse,
                                    const double * Restrict const giant_branch_parameters,
                                    const double * Restrict const metallicity_parameters,
                                    const double GBp,
                                    const double GBq,
                                    const double ip,
                                    const double iq,
                                    const double mass);
#endif // BSE
#endif// STELLAR_TIMESCALES_PROTOTYPES_H
