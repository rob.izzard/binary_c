#pragma once
#ifndef STELLAR_TIMESCALES_H
#define STELLAR_TIMESCALES_H


/* timescale macros - corresponding to those in calc_lum_and_evol_time */
#include "stellar_timescales/stellar_timescales.def"
#undef X
#define X(CODE) T_##CODE,
enum { STELLAR_TIMESCALES_LIST };
#undef X

/* ditto for luminosity macros */
#include "stellar_timescales/stellar_luminosities.def"
#define X(CODE) L_##CODE,
enum { STELLAR_LUMINOSITIES_LIST };
#undef X

/* and GB */
#include "stellar_timescales/stellar_GB.def"
#define X(CODE) GB_##CODE,
enum { STELLAR_GB_LIST };
#undef X

#define GB_ARRAY_SIZE 14
#define LUMS_ARRAY_SIZE 9
#define TSCLS_ARRAY_SIZE 15

#endif // STELLAR_TIMESCALES_H
