#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef BSE
void stellar_timescales_high_mass_GB(struct stardata_t * const stardata Maybe_unused,
                                     struct BSE_data_t * const bse,
                                     const double * Restrict const giant_branch_parameters,
                                     const double mass,
                                     const double mp2)
{
#ifndef USE_BSE_TIMESCALES_H
    /*
     * Note that for M>metallicity_parameters(3) there is no
     * GB as the star goes from
     * HG -> CHeB -> AGB. So in effect timescales(1) refers to the time of
     * Helium ignition and not the BGB.
     */
    bse->timescales[T_HE_IGNITION] = bse->timescales[T_BGB];
    bse->timescales[T_HE_BURNING] = thef(mass,
                                         1.0,
                                         mp2,
                                         giant_branch_parameters)
        *bse->timescales[T_BGB];

#endif

    Boolean have_set = FALSE;
#ifdef MINT
    if(stardata->preferences->stellar_structure_algorithm == STELLAR_STRUCTURE_ALGORITHM_MINT &&
       MINT_has_table(MINT_TABLE_CHeB))
    {
        have_set = TRUE;

        MINT_CHeB_ZA(stardata,
                     mass,
                     &bse->luminosities[L_HE_BURNING]);
        MINT_CHeB_TA(stardata,
                     mass,
                     &bse->timescales[T_HE_BURNING],
                     NULL);
    }
#endif

    if(have_set == FALSE)
    {
        /*
         * This now represents the luminosity at the end of CHeB, ie. BAGB
         */
        bse->luminosities[L_HE_BURNING] = bse->luminosities[L_BAGB];
    }

    /*
     * We set luminosities(3) to be the luminosity at the end of the HG
     */
    bse->luminosities[L_BGB] = bse->luminosities[L_HE_IGNITION];

}
#endif
