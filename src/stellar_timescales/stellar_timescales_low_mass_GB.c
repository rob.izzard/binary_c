#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef BSE
void stellar_timescales_low_mass_GB(struct stardata_t * const stardata,
                                    struct star_t * const star Maybe_unused,
                                    struct BSE_data_t * const bse,
                                    const double * Restrict const giant_branch_parameters,
                                    const double * Restrict const metallicity_parameters,
                                    const double GBp,
                                    const double GBq,
                                    const double ip,
                                    const double iq,
                                    const double mass)
{
    /*
     * Set giant branch timescales
     */
    Dprint("Set GB timescales\n");

    /*
     * Base of the giant branch luminosity
     */
    bse->luminosities[L_BGB] = lbgbf(mass,giant_branch_parameters);
    Dprint("LBGB = %30.20e\n",bse->luminosities[L_BGB]);

    const double y=iq/(bse->GB[GB_EFFECTIVE_AH]*bse->GB[GB_B]);
    const double y2=ip/(bse->GB[GB_EFFECTIVE_AH]*bse->GB[GB_D]);
    Dprint("y = %30.20e ... y2 = %30.20e\n",y,y2);

    /* Eq. 40 */
    bse->timescales[T_GIANT_TINF_1] = bse->timescales[T_BGB] +
        y2*pow((bse->GB[GB_D]/bse->luminosities[L_BGB]),GBp);
    Dprint("T_GIANT_TINF_1 = %30.20e + %30.20e * pow(%30.20e / %30.20e, %30.20e) = %30.20e\n",
           bse->timescales[T_BGB],
           y2,
           bse->GB[GB_D],
           bse->luminosities[L_BGB],
           GBp,
           bse->timescales[T_GIANT_TINF_1]);

    const double ilX=1.0/bse->luminosities[L_LMX];

    /* this is tx of Eq. 41 */
    bse->timescales[T_GIANT_TX] = bse->timescales[T_GIANT_TINF_1] -
        (bse->timescales[T_GIANT_TINF_1] - bse->timescales[T_BGB])*pow(bse->luminosities[L_BGB]*ilX,GBp);
    Dprint("T_GIANT_TX = %30.20e - ( %30.20e - %30.20e ) * pow(%30.20e * %30.20e, %30.20e)\n",
           bse->timescales[T_GIANT_TINF_1],
           bse->timescales[T_BGB],
           bse->luminosities[L_BGB],
           ilX,
           GBp,
           bse->timescales[T_GIANT_TX]);

/* Eq. 43 */
    bse->timescales[T_GIANT_TINF_2] = bse->timescales[T_GIANT_TX] +y*pow(bse->GB[GB_B]*ilX,GBq);
    Dprint("T_GIANT_TINF_2 = %30.20e + %30.20e * pow(%30.20e * %30.20e, %30.20e) = %30.20e\n",
           bse->timescales[T_GIANT_TX],
           y,
           bse->GB[GB_B],
           ilX,
           GBq,
           bse->timescales[T_GIANT_TINF_2]);
    Dprint("GB timescales %30.20e %30.20e %30.20e\n",
           bse->timescales[T_GIANT_TINF_1],
           bse->timescales[T_GIANT_TINF_2],
           bse->timescales[T_GIANT_TX]);

    /*
     * Set Helium ignition time
     */
    Dprint("Calc He burning luminosity and timescale\n");

    const double ilHe = 1.0/bse->luminosities[L_HE_IGNITION];
    if(Less_or_equal(bse->luminosities[L_HE_IGNITION],bse->luminosities[L_LMX]))
    {
        bse->timescales[T_HE_IGNITION] = bse->timescales[T_GIANT_TINF_1] -
            y2*pow(bse->GB[GB_D]*ilHe,GBp);
    }
    else
    {
        bse->timescales[T_HE_IGNITION] = bse->timescales[T_GIANT_TINF_2] -
            y*pow(bse->GB[GB_B]*ilHe,GBq);
    }
    bse->timescales[T_HE_IGNITION] = Max(0.0, bse->timescales[T_HE_IGNITION]);

    Dprint("HE_IGNITION star %d branch %d t=%30.20e L=%30.23e\n",
           star->starnum,
           Less_or_equal(bse->luminosities[L_HE_IGNITION],bse->luminosities[L_LMX]) ? 1: 2,
           bse->timescales[T_HE_IGNITION],
           bse->luminosities[L_HE_IGNITION]);



    Boolean have_set = FALSE;

#ifdef MINT
    if(stardata->preferences->stellar_structure_algorithm ==
       STELLAR_STRUCTURE_ALGORITHM_MINT &&
       MINT_has_table(MINT_TABLE_CHeB))
    {
        have_set = TRUE;
        MINT_CHeB_ZA(stardata,
                     mass,
                     &bse->luminosities[L_HE_BURNING]);
        MINT_CHeB_TA(stardata,
                     mass,
                     &bse->timescales[T_HE_BURNING],
                     NULL);
    }
#endif

    if(have_set == FALSE)
    {
        if(Less_or_equal(mass,metallicity_parameters[ZPAR_MASS_HE_FLASH]))
        {
            const double mc1 =
                mcgbf(bse->luminosities[L_HE_IGNITION],
                      bse->GB,
                      bse->luminosities[L_LMX]);

            bse->luminosities[L_HE_BURNING] =
                lzahbf(mass,
                       mc1,
                       metallicity_parameters[ZPAR_MASS_HE_FLASH],
                       giant_branch_parameters);

            bse->timescales[T_HE_BURNING] =
                thef(mass,
                     mc1,
                     metallicity_parameters[ZPAR_MASS_HE_FLASH],
                     giant_branch_parameters);

            Dprint("M<=Mflash : L=%30.20e timescale=%30.20e\n",
                   bse->luminosities[L_HE_BURNING],
                   bse->timescales[T_HE_BURNING]);
        }
        else
        {
            bse->luminosities[L_HE_BURNING] =
                lhef(mass,
                     giant_branch_parameters)
                *bse->luminosities[L_HE_IGNITION];

            bse->timescales[T_HE_BURNING] =
                thef(mass,
                     1.0,
                     metallicity_parameters[ZPAR_MASS_HE_FLASH],
                     giant_branch_parameters)
                *bse->timescales[T_BGB];

            Dprint("M>Mflash : L=%30.20e timescale=%30.20e\n",
                   bse->luminosities[L_HE_BURNING],
                   bse->timescales[T_HE_BURNING]);
        }
    }
}
#endif//BSE
