#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef BSE
void stellar_timescales_low_mass_AGB(struct stardata_t * const stardata Maybe_unused,
                                     struct star_t * const star Maybe_unused,
                                     struct BSE_data_t * const bse,
                                     const double * Restrict const giant_branch_parameters,
                                     const double * Restrict const metallicity_parameters,
                                     const double phase_start_mass,
                                     const double mass,
                                     const double mcbagb,
                                     const double ip,
                                     const double iq,
                                     const double mp,
                                     const double mq)
{
    if(phase_start_mass>metallicity_parameters[ZPAR_MASS_FGB])
    {
        const double mc1 = mcheif(phase_start_mass,
                                  metallicity_parameters[ZPAR_MASS_HE_FLASH],
                                  metallicity_parameters[10],
                                  giant_branch_parameters);

        bse->tn = bse->timescales[T_HE_IGNITION];

        if(mass>mc1) bse->tn += bse->timescales[T_HE_BURNING]*((mass - mc1)/(mcbagb - mc1));
    }
    else if(Less_or_equal(phase_start_mass,metallicity_parameters[ZPAR_MASS_HE_FLASH]))
    {
        const double mc1 = mcgbf(bse->luminosities[L_BGB],
                                 bse->GB,
                                 bse->luminosities[L_LMX]);

        if(Less_or_equal(mass,mc1))
        {
            bse->tn = bse->timescales[T_BGB];
        }
        else
        {
            const double mc2 = mcgbf(bse->luminosities[L_HE_IGNITION],
                                     bse->GB,
                                     bse->luminosities[L_LMX]);

            if(Less_or_equal(mass,mc2))
            {
                if(Less_or_equal(mass,bse->GB[GB_Mx]))
                {
                    bse->tn = bse->timescales[T_GIANT_TINF_1] - (ip/(bse->GB[GB_EFFECTIVE_AH]*bse->GB[GB_D]))*pow(mass,mp);
                }
                else
                {
                    bse->tn = bse->timescales[T_GIANT_TINF_2] - (iq/(bse->GB[GB_EFFECTIVE_AH]*bse->GB[GB_B]))*pow(mass,mq);
                }
            }
            else
            {
                bse->tn = bse->timescales[T_HE_IGNITION] + bse->timescales[T_HE_BURNING]*((mass - mc2)/(mcbagb - mc2));
            }
        }
    }
    else
    {
        const double mc1 = mcheif(phase_start_mass,
                                  metallicity_parameters[ZPAR_MASS_HE_FLASH],
                                  metallicity_parameters[9],
                                  giant_branch_parameters);

        if(Less_or_equal(mass,mc1))
        {
            bse->tn = bse->timescales[T_BGB];
        }
        else
        {
            const double mc2 = mcheif(phase_start_mass,
                                      metallicity_parameters[ZPAR_MASS_HE_FLASH],
                                      metallicity_parameters[10],
                                      giant_branch_parameters);

            if(Less_or_equal(mass,mc2))
            {
                bse->tn = bse->timescales[T_BGB] +
                    (bse->timescales[T_HE_IGNITION] - bse->timescales[T_BGB])
                    *((mass - mc1)/(mc2 - mc1));
            }
            else
            {
                bse->tn = bse->timescales[T_HE_IGNITION] + bse->timescales[T_HE_BURNING]
                    *((mass - mc2)/(mcbagb - mc2));
            }
        }
    }
}

#endif//BSE
