#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef BSE

void stellar_timescales_massive_AGB(struct star_t * Restrict const star,
                                    struct BSE_data_t * const bse,
                                    const double mass,
                                    const double mt,
                                    const double mcbagb,
                                    const double ip,
                                    const double iq,
                                    const double mp,
                                    const double mq,
                                    const Stellar_type stellar_type)
{
    const double mc1 = mcbagb;
    double mcmax;
    if(stellar_type==TPAGB)
    {
        /*
         * artificially stop mc1 from growing too large
         *
         * lambda is computed (and saved) by the stellar evolution
         * algorithm, so use it if it is non-zero, otherwise the BSE
         * approximation.
         */
        const double lambda =
            Is_zero(star->lambda_3dup) ? star->lambda_3dup :
            Min(0.9, 0.3 + 1e-3 * Pow5(mass));

        mcmax = (mt - lambda*mc1)/(1.0 - lambda);
    }
    else
    {
        mcmax = mcbagb;
    }

    bse->tn =
        Less_or_equal(mc1,bse->GB[GB_Mx]) ?
        (bse->timescales[T_TPAGB_TINF_1] -
         (ip/(bse->GB[GB_A_H_HE]*bse->GB[GB_D]))*pow(mcmax,mp)) :
        (bse->timescales[T_TPAGB_TINF_2] -
         (iq/(bse->GB[GB_A_H_HE]*bse->GB[GB_B]))*pow(mcmax,mq));
}
#endif
