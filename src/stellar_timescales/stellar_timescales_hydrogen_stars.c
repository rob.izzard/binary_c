#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef MINT
#include "../MINT/MINT.h"
#endif//MINT

#ifdef BSE

/*
 * Stellar timescale caclulations in hydrogen-rich stars.
 *
 * The following depend only on phase_start_mass:
 * GB:
 * GB_EFFECTIVE_AH,GB_A_H_HE,GB_A_HE,GB_B,GB_D,GB_p,GB_q

 * timescales:
 * T_MS, T_BGB, T_GIANT_TX, T_GIANT_TINF_1, T_GIANT_TINF_2,
 * T_HE_IGNITION, T_HE_BURNING,
 *
 *
 * luminosities:
 * L_BGB, L_HE_IGNITION, L_HE_BURNING
 *

 * Note that the giant_branch_parameters, main_sequence_parameters
 * and metallicity parameters only depend on metallicity,
 * so can be considered constants in this subroutine.
 */

int stellar_timescales_hydrogen_stars(struct stardata_t * const stardata,
                                      struct star_t * const star,
                                      struct BSE_data_t * const bse,
                                      const double phase_start_mass,
                                      const double mass,
                                      const Stellar_type stellar_type)
{
    /* speed improvement (dummy) variables */
    double GBp,GBq,ip,iq,ipq,mp,mq;
    const double * const giant_branch_parameters=
        stardata->common.giant_branch_parameters;
    const double * const main_sequence_parameters=
        stardata->common.main_sequence_parameters;
    const double * const metallicity_parameters=
        stardata->common.metallicity_parameters;

    Dprint("Stellar timescales not helium : phase_start_mass=%g MSP=%p\n",
           phase_start_mass,
           (void*)main_sequence_parameters);

#ifdef MINT
    /*
     * Get MINT data at the ZAMS and TAMS
     */
    double * mint_ZAMS;
    double * mint_TAMS;
    if(stardata->preferences->stellar_structure_algorithm ==
       STELLAR_STRUCTURE_ALGORITHM_MINT)
    {
        mint_ZAMS = Malloc(MINT_result_size(MINT_TABLE_MS));
        mint_TAMS = Malloc(MINT_result_size(MINT_TABLE_MS));
        const double Xc0 = MINT_initial_XHc(stardata);
        const double logM = log10(star->phase_start_mass);
        Interpolate(stardata->store->MINT_tables[MINT_TABLE_MS],
                    ((double[]){
                        logM,
                        Xc0
                    }),
                    mint_ZAMS,
                    FALSE);
        Interpolate(stardata->store->MINT_tables[MINT_TABLE_MS],
                    ((double[]){
                        logM,
                        0.0
                    }),
                    mint_TAMS,
                    FALSE);
    }
    else
    {
        mint_ZAMS = NULL;
        mint_TAMS = NULL;
    }
#endif//MINT


#ifdef USE_BSE_TIMESCALES_H
    {
        int i;
        double params[2] = {
            log10(stardata->common.metallicity),
            log10(phase_start_mass)
        };
        double result[TSCLS_ARRAY_SIZE + LUMS_ARRAY_SIZE + GB_ARRAY_SIZE];
        Interpolate(stardata->store->BSE_TIMESCALES_H,params,result,FALSE);
        for(i=0;i<TSCLS_ARRAY_SIZE;i++)
        {
            bse->timescales[i] = exp10(result[i]);
        }

#undef _OFFSET
#define _OFFSET (TSCLS_ARRAY_SIZE)
        /*
         * Set the luminosities that depend only on
         * the phase_start_mass
         */
        bse->luminosities[L_BGB] = exp10(result[L_BGB + _OFFSET]);
        bse->luminosities[L_LMX] = exp10(result[L_LMX + _OFFSET]);
        bse->luminosities[L_HE_IGNITION] = exp10(result[L_HE_IGNITION + _OFFSET]);
        bse->luminosities[L_HE_BURNING] = exp10(result[L_HE_BURNING + _OFFSET]);
        bse->luminosities[L_BAGB] = exp10(result[L_BAGB + _OFFSET]);
        bse->luminosities[L_TPAGB_FIRST_PULSE] = exp10(result[L_BAGB + _OFFSET]);

#undef _OFFSET
#define _OFFSET (TSCLS_ARRAY_SIZE + LUMS_ARRAY_SIZE)
        for(i=0;i<GB_ARRAY_SIZE;i++)
        {
            bse->GB[i] = exp10(result[i + _OFFSET]);
        }
    }
#undef _OFFSET
#else
    /*
     * MS and BGB times
     */
#ifdef MINT
    /*
     * If we're using MINT, we know tms
     */
    if(stardata->preferences->stellar_structure_algorithm ==
       STELLAR_STRUCTURE_ALGORITHM_MINT)
    {
        /*
         * KH time (Myr)
         *
         * Note: if there's no stellar structure yet, this
         * is long, so we ignore it in this case.
         */
        const double tkh =
            Max(0.01,1e-6 * kelvin_helmholtz_time(star));

        if(Is_zero(star->tms))
        {
            /*
             * First time estimates
             */
            bse->timescales[T_MS] = MINT_MS_lifetime(stardata,star);
            bse->timescales[T_BGB] = bse->timescales[T_MS] * 1.05;
        }
        else
        {

            /*
             * MINT already computes our main-sequence lifetime
             */
            bse->timescales[T_MS] = star->tms;

            /*
             * Estimate time to BGB by assuming ~ tKH timescale
             * across the Hertzsprung gap
             */
            const double tbgb =
                bse->timescales[T_MS] + (tkh > 1e10 ? 0.0 : tkh);

            bse->timescales[T_BGB] =
                Is_zero(star->mint->mint_tbgb)
                ? tbgb
                : Max(star->mint->mint_tbgb,
                      tbgb);
            star->mint->mint_tbgb = bse->timescales[T_BGB];
        }
    }
    else

#endif // MINT
    {
        /* BSE */
        bse->timescales[T_BGB] = tbgbf(stardata,
                                       phase_start_mass,
                                       main_sequence_parameters,
                                       stardata->common.metallicity);
        bse->timescales[T_MS] = Max(metallicity_parameters[8],
                                    thookf(phase_start_mass,
                                           main_sequence_parameters))
            *bse->timescales[T_BGB];
    }
#endif

    /* store bse->tm for legacy code */
    bse->tm = bse->timescales[T_MS];

    Dprint("TM CALC zpar[6]=%30.22e thookf(%30.22e)=%30.22e, tscls[T_BGB]=%30.22e hence tm=%30.22e\n",
           metallicity_parameters[8],
           phase_start_mass,
           thookf(phase_start_mass,main_sequence_parameters),
           bse->timescales[T_BGB],bse->tm);


    Dprint("Calc T BGB =%g hence tm=%30.22e (mult=Max(%g,%g))\n",
           bse->timescales[T_BGB],bse->tm,
           metallicity_parameters[8],
           thookf(phase_start_mass,main_sequence_parameters));
    /*
     * Zero- and terminal age main sequence luminosity
     */
#ifdef MINT
    /*
     * If we're using MINT, we know tms
     */
    if(stardata->preferences->stellar_structure_algorithm ==
       STELLAR_STRUCTURE_ALGORITHM_MINT)
    {
        bse->luminosities[L_ZAMS] = exp10(mint_ZAMS[MINT_MS_LUMINOSITY]);
        bse->luminosities[L_END_MS] = exp10(mint_TAMS[MINT_MS_LUMINOSITY]);
        Safe_free(mint_ZAMS);
        Safe_free(mint_TAMS);
    }
    else
#endif // MINT
    {
        /* BSE */
        bse->luminosities[L_ZAMS] = lzamsf(phase_start_mass,main_sequence_parameters);
        bse->luminosities[L_END_MS] = ltmsf(phase_start_mass,main_sequence_parameters);
    }

    /*
     * very low-mass stars: approximate "evolution"
     */
    if(stellar_type<HERTZSPRUNG_GAP && phase_start_mass<0.1)
    {
#ifndef USE_BSE_TIMESCALES_H
        bse->timescales[T_HE_IGNITION] = 1.1 * bse->timescales[T_BGB];
        bse->timescales[T_HE_BURNING]  = 0.1 * bse->timescales[T_BGB];
        bse->luminosities[L_BGB] = lbgbf(phase_start_mass,giant_branch_parameters);
#endif
        bse->tn = 1.0e10;
        Dprint("calc_lum (Low mass star <0.1) out st=%d m=%g tm=%g tn=%g\n",
               stellar_type,phase_start_mass,bse->tm,bse->tn);
        return 1;
    }
    else
    {
#ifndef USE_BSE_TIMESCALES_H
        stellar_timescales_GB_parameters(bse,
                                         phase_start_mass,
                                         metallicity_parameters[6]);
#endif

        mp=1.0-bse->GB[GB_p];
        mq=1.0-bse->GB[GB_q];
        GBp=-mp/bse->GB[GB_p];
        GBq=-mq/bse->GB[GB_q];
        ip=-1.0/mp;
        iq=-1.0/mq;

#ifndef USE_BSE_TIMESCALES_H
        //ipq=1.0/(-(1.0-bse->GB[GB_p])+1.0-bse->GB[GB_q]);
        ipq = 1.0/(mq-mp);
        bse->GB[GB_Mx] = pow(bse->GB[GB_B]/bse->GB[GB_D], ipq);

        Dprint("Set GB_Mx(star=%d) from %g/%g, ipq=%g -> %g\n",star->starnum,bse->GB[GB_B],bse->GB[GB_D],ipq,bse->GB[GB_Mx]);


        /*
         * Change in slope of giant L-Mc relation.
         */
        bse->luminosities[L_LMX] = bse->GB[GB_D]*pow(bse->GB[GB_Mx],bse->GB[GB_p]);

        /*
         * HeI ignition luminosity
         */
        bse->luminosities[L_HE_IGNITION] = lheif(phase_start_mass,
                                                 metallicity_parameters[ZPAR_MASS_HE_FLASH],
                                                 giant_branch_parameters);
        bse->luminosities[L_BAGB] = lbagbf(phase_start_mass,
                                           metallicity_parameters[ZPAR_MASS_HE_FLASH],
                                           giant_branch_parameters);

        Dprint("compare phase_start_mass=%g vs metallicity_parameter[3]=%g\n",phase_start_mass,metallicity_parameters[ZPAR_MASS_FGB]);

        if(Less_or_equal(phase_start_mass,
                         metallicity_parameters[ZPAR_MASS_FGB]))
        {
            stellar_timescales_low_mass_GB(stardata,
                                           star,
                                           bse,
                                           giant_branch_parameters,
                                           metallicity_parameters,
                                           GBp,
                                           GBq,
                                           ip,iq,
                                           phase_start_mass);
        }
        else
        {
            stellar_timescales_high_mass_GB(stardata,
                                            bse,
                                            giant_branch_parameters,
                                            phase_start_mass,
                                            metallicity_parameters[ZPAR_MASS_HE_FLASH]);
        }

#endif // USE_BSE_TIMESCALES_H

        Dprint("Done compare stellar_type %d\n",stellar_type);

        {
            const double mcbagb = mcagbf(phase_start_mass,giant_branch_parameters);
            const double tbagb = bse->timescales[T_HE_IGNITION] + bse->timescales[T_HE_BURNING];

            if(stellar_type < EAGB &&
               Fequal(mass, mcbagb))
            {
                Dprint("Set tn=tbagb=%g\n",bse->tn);
                bse->tn = tbagb;
                return 1;
            }
            else
            {
                Dprint("Set post HG tscls stellar_type=%d\n",stellar_type);
                const int kp =
                    stellar_timescales_post_HG(stardata,
                                               star,
                                               bse,
                                               giant_branch_parameters,
                                               metallicity_parameters,
                                               tbagb,
                                               ip,
                                               iq,
                                               GBp,
                                               GBq,
                                               mp,
                                               mq,
                                               mcbagb,
                                               phase_start_mass,
                                               mass,
                                               stellar_type);
                return kp;
            }
        }
    }
}
#endif
