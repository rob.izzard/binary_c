#include "../binary_c.h"
No_empty_translation_unit_warning;

/*
 * A set of functions to perform the stellar-merger/common-envelope
 * calculations in Ivanova et al. (2013)
 * arXiv 1301.5897
 *
 * Input parameters:
 *
 * initial_radius : Initial radius (Rsun)
 *
 * kinetic_energy : Kinetic energy at late times of the mass that escapes the potential well
 *                  (erg, i.e. cgs) =  their E^{\deg}_\mathrm{k}
 *
 * unbound_mass   : Mass that is unbound (Msun)
 *
 * ejecta_opacity : opacity (cm^2 g^-1, i.e. cgs) = their M_\mathrm{unb}
 *   (note: if 0, 0.32 cm^2 g^-1 is used, the default in the paper)
 *
 * recombination_temperature : recombination temperature (K)
 *   (note: if 0.0, 4500K is used, the deafult in the paper)
 *
 * Results are returned in cgs units.
 */

#define _VB 0

#if _VB == 0
#define _Puref Pure_function
#else
#define _Puref
#endif

#define _CHECK_RANGES FALSE

void Ivanova2013_transient(struct stardata_t * const stardata,
                           const struct star_t * const primary, // primary (single) star after merger
                           const struct star_t * const previous_primary, // primary star before merger
                           const struct star_t * const previous_secondary, // secondary star before merger
                           void * input_data,
                           double * const timescale_days,
                           double * const luminosity_Lsun)
{
    const struct star_t * const secondary = Other_star_struct(primary);
    const double total_mass = previous_primary->mass + previous_secondary->mass;
    const double unbound_mass = total_mass - primary->mass - secondary->mass;
    struct Ivanova2013_data_t * data = (struct Ivanova2013_data_t *) input_data;

    if(unbound_mass > 0.0)
    {
        const double KE = Ivanova2013_kinetic_energy(data->zeta,
                                                     previous_primary->mass,
                                                     unbound_mass,
                                                     previous_primary->radius);

        const double opacity = 0.0; /* 0 = default */
        const double Trecon = 0.0; /* 0 = default */
        *timescale_days =
            Ivanova2013_plateau_duration(previous_primary->radius,
                                         KE,
                                         unbound_mass,
                                         opacity,
                                         Trecon) / DAY_LENGTH_IN_SECONDS;

        *luminosity_Lsun =
            Ivanova2013_plateau_luminosity(previous_primary->radius,
                                           KE,
                                           unbound_mass,
                                           opacity,
                                           Trecon) / L_SUN;

        if(_CHECK_RANGES &&
           (*timescale_days < 0.1 || *timescale_days > 1e3 ||
            *luminosity_Lsun < 1.0 || *luminosity_Lsun > 1e8))
        {
            printf("crazy L,tscl of transient!\n");
            printf("tscl/days = %g, L/Lsun = %g\n",
                   *timescale_days,
                   *luminosity_Lsun);
            printf("previous primary mass %g\n",previous_primary->mass);
            printf("previous primary radius %g\n",previous_primary->radius);
            printf("total mass %g\n",total_mass);
            printf("unbound mass %g\n",unbound_mass);
            printf("fm = %g\n",unbound_mass / previous_primary->mass);
            printf("primary (donor) is star %d of previous type %d mass %g\n",
                   primary->starnum,
                   previous_primary->stellar_type,
                   previous_primary->mass);
            printf("secondary (accretor) is star %d of previous type %d mass %g\n",
                   secondary->starnum,
                   previous_secondary->stellar_type,
                   previous_secondary->mass);

            Exit_binary_c(BINARY_C_OUT_OF_RANGE,
                          "Transient luminosity or timescale is out of sensible range.");
        }
    }
    else
    {
        *timescale_days = 0.0;
        *luminosity_Lsun = 0.0;
    }
}


_Puref
double Ivanova2013_plateau_luminosity(const double initial_radius,
                                      const double kinetic_energy,
                                      const double unbound_mass,
                                      const double ejecta_opacity,
                                      const double recombination_temperature)
{
    /*
     * Equation 1 (erg/s)
     */
    if(_VB)
    {
        printf("Plateau luminosity r0=%g KE=%g deltaM=%g opacity=%g Trec=%g\n",
               initial_radius,
               kinetic_energy,
               unbound_mass,
               ejecta_opacity,
               recombination_temperature);
    }
    return
           1.7e4 * L_SUN *
           pow(initial_radius/3.5, 2.0/3.0) *
           pow(kinetic_energy * 1e-46, 5.0/6.0) *
           1.0/sqrt(unbound_mass/0.03) *
           (Is_zero(ejecta_opacity) ? 1.0 : pow(ejecta_opacity/0.32, -1.0/3.0)) *
           (Is_zero(recombination_temperature) ? 1.0 : pow(recombination_temperature/4500.0, 4.0/3.0));
}

_Puref
double Ivanova2013_plateau_duration(const double initial_radius,
                                    const double kinetic_energy,
                                    const double unbound_mass,
                                    const double ejecta_opacity,
                                    const double recombination_temperature)
{
    /*
     * Equation 2 (s)
     */
    return
        17.0 * DAY_LENGTH_IN_SECONDS *
        pow(initial_radius / 3.5, 1.0/6.0) *
        pow(kinetic_energy * 1e-46, -1.0/6.0) *
        sqrt(unbound_mass / 0.03) *
        (Is_zero(ejecta_opacity) ? 1.0 : pow(ejecta_opacity/0.32, 1.0/6.0)) *
        (Is_zero(recombination_temperature) ? 1.0 : pow(recombination_temperature/4500.0, -2.0/3.0));
}

_Puref
double Ivanova2013_recombination_energy(const double X,
                                        const double Y,
                                        const double helium_ionization_fraction,
                                        const double unbound_mass)
{
    /*
     * Equation 3 (erg)
     */
    return
        2.6e46 *
        (X + 1.5 * Y * helium_ionization_fraction) *
        unbound_mass;
}

_Puref
double Ivanova2013_zeta(const double primary_mass,
                        const double initial_radius,
                        const double kinetic_energy,
                        const double unbound_mass)
{
    /*
     * zeta as described in the text, given a kinetic energy
     */
    const double fm = unbound_mass / primary_mass;
    return
        kinetic_energy /
        (GRAVITATIONAL_CONSTANT * Pow2(primary_mass * M_SUN) * fm / (initial_radius * R_SUN));
}


_Puref
double Ivanova2013_Popov_lambda(const double initial_radius,
                                const double kinetic_energy,
                                const double unbound_mass,
                                const double ejecta_opacity,
                                const double recombination_temperature)
{
    /*
     * Popov's lambda in the supplementary material (dimensionless)
     */
    return
        49.0 *
        1.0/(initial_radius/3.5) *
        1.0/Pow2(kinetic_energy * 1e-46) *
        Pow1p5(unbound_mass/0.03) *
        (Is_zero(ejecta_opacity) ? 1.0 : Pow2(ejecta_opacity)) *
        (Is_zero(recombination_temperature) ? 1.0 : Pow4(recombination_temperature/4500.0));
}

_Puref
double Ivanova2013_Popov_lambda_for_comenv(const double primary_mass,
                                           const double lambda_ce,
                                           const double initial_radius,
                                           const double unbound_mass,
                                           const double ejecta_opacity,
                                           const double recombination_temperature)
{
    /*
     * Popov's lambda in the supplementary material,
     * modified version for common-envelope evolution
     * (dimensionless)
     */
    return
        1700.0 *
        1.0/sqrt(initial_radius) *
        unbound_mass * sqrt(lambda_ce/(primary_mass - unbound_mass)) *
        (Is_zero(ejecta_opacity) ? 1.0 : Pow2(ejecta_opacity)) *
        (Is_zero(recombination_temperature) ? 1.0 : Pow4(recombination_temperature/4500.0));
}

double Ivanova2013_kinetic_energy(
    const double zeta, // dimensionless
    const double primary_mass, // M_SUN
    const double unbound_mass, // M_SUN
    const double initial_radius // R_SUN
    )
{
    /*
     * kinetic energy, given zeta, in erg
     */
    if(Is_zero(primary_mass) ||
       Is_zero(unbound_mass) ||
       Is_zero(initial_radius))
    {
        return 0.0;
    }
    else
    {
        const double fm = unbound_mass / primary_mass;
        if(_VB)
        {
            printf("Plateau zeta = %g, primary mass = %g Msun, unbound mass = %g Msun, fm = %g, R0 = %g Rsun\n",
                   zeta,
                   primary_mass,
                   unbound_mass,
                   fm,
                   initial_radius);
        }
        return zeta *
            GRAVITATIONAL_CONSTANT *
            Pow2(primary_mass * M_SUN)  *
            fm /
            (initial_radius * R_SUN);
    }
}
