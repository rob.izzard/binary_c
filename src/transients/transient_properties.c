#include "../binary_c.h"
No_empty_translation_unit_warning;


void transient_properties(struct stardata_t * const stardata,
                          const struct star_t * const primary, // primary (single) star after merger
                          const struct star_t * const previous_primary, // primary star before merger
                          const struct star_t * const previous_secondary, // secondary star before merger
                          void * input_data,
                          double * const timescale_days,
                          double * const luminosity_Lsun
    )
{
    /*
     * Return transient properties:
     *
     * timescale should be in days
     *
     * luminosity should be in Lsun
     */
    if(stardata->preferences->transient_method == TRANSIENT_METHOD_NONE)
    {
        *timescale_days = 0.0;
        *luminosity_Lsun = 0.0;
    }
    else if(stardata->preferences->transient_method == TRANSIENT_METHOD_IVANOVA2013)
    {
        Ivanova2013_transient(stardata,
                              primary,
                              previous_primary,
                              previous_secondary,
                              input_data,
                              timescale_days,
                              luminosity_Lsun);
    }
    else if(stardata->preferences->transient_method == TRANSIENT_METHOD_MATSUMOTO2022)
    {
        Matsumoto2022_transient(stardata,
                                primary,
                                previous_primary,
                                previous_secondary,
                                input_data,
                                timescale_days,
                                luminosity_Lsun);
    }
    else
    {
        Exit_binary_c(BINARY_C_ALGORITHM_OUT_OF_RANGE,
                      "Transient algorithm %d is unknown.",
                      stardata->preferences->transient_method);
    }
    /*
    printf("trans time = %g, model %d, t = %g d, L = %g Lsun [ pre 0 M %g L %g R %g st %d ] [ post 0 M %g L %g R %g st %d ] [ post 0 M %g L %g R %g st %d ] \n",
           stardata->model.time,
           stardata->model.model_number,
           *timescale_days,
           *luminosity_Lsun,

           previous_primary->mass,
           previous_primary->luminosity,
           previous_primary->radius,
           previous_primary->stellar_type,

           previous_secondary->mass,
           previous_secondary->luminosity,
           previous_secondary->radius,
           previous_secondary->stellar_type,

           primary->mass,
           primary->luminosity,
           primary->radius,
           primary->stellar_type
        );
    */
}
