#ifndef TRANSIENTS_PROTOTYPES_H
#define TRANSIENTS_PROTOTYPES_H

#include "../binary_c_code_options.h"

#include <sys/types.h>
#include "../binary_c_parameters.h"
#include "../binary_c_macros.h"
#include "../binary_c_structures.h"

void transient_properties(struct stardata_t * const stardata,
                          const struct star_t * const primary, // primary (single) star after merger
                          const struct star_t * const previous_primary, // primary star before merger
                          const struct star_t * const previous_secondary, // secondary star before merger
                          void * input_data,
                          double * const timescale_days,
                          double * const luminosity_Lsun
    );


/*
 * Ivanova et al. 2013
 */
void Ivanova2013_transient(struct stardata_t * const stardata,
                           const struct star_t * const primary, // primary (single) star after merger
                           const struct star_t * const previous_primary, // primary star before merger
                           const struct star_t * const previous_secondary, // secondary star before merger
                           void * input_data,
                           double * const timescale_days,
                           double * const luminosity_Lsun);


double Ivanova2013_plateau_luminosity(const double initial_radius,
                                      const double kinetic_energy,
                                      const double unbound_mass,
                                      const double ejecta_opacity,
                                      const double recombination_temperature);


double Ivanova2013_plateau_duration(const double initial_radius,
                                    const double kinetic_energy,
                                    const double unbound_mass,
                                    const double ejecta_opacity,
                                    const double recombination_temperature);


double Ivanova2013_recombination_energy(const double X,
                                        const double Y,
                                        const double helium_ionization_fraction,
                                        const double mass_unbound);


double Ivanova2013_zeta(const double primary_mass,
                        const double initial_radius,
                        const double kinetic_energy,
                        const double unbound_mass);


double Ivanova2013_Popov_lambda(const double initial_radius,
                                const double kinetic_energy,
                                const double unbound_mass,
                                const double ejecta_opacity,
                                const double recombination_temperature);


double Ivanova2013_Popov_lambda_for_comenv(const double primary_mass,
                                           const double lambda_ce,
                                           const double initial_radius,
                                           const double unbound_mass,
                                           const double ejecta_opacity,
                                           const double recombination_temperature);

double Ivanova2013_kinetic_energy(const double zeta,
                                  const double primary_mass,
                                  const double unbound_mass,
                                  const double initial_radius);

void Matsumoto2022_transient(struct stardata_t * const stardata,
                             const struct star_t * const primary, // primary (single) star after merger
                             const struct star_t * const previous_primary, // primary star before merger
                             const struct star_t * const previous_secondary, // secondary star before merger
                             void * input_data,
                             double * const timescale_days,
                             double * const luminosity_Lsun);

double Matsumoto2022_plateau_luminosity(const double unbound_mass,
                                        const struct Matsumoto2022_data_t * const data);

double Matsumoto2022_plateau_duration(const double unbound_mass,
                                      const struct Matsumoto2022_data_t * const data);

double Matsumoto2022_shock_luminosity(const double unbound_mass,
                                      const double primary_mass,
                                      const struct Matsumoto2022_data_t * const data);


#endif /*TRANSIENTS_PROTOTYPES_H */
