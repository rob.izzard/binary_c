#include "../binary_c.h"
No_empty_translation_unit_warning;

/*
 * A set of functions to perform the stellar-merger/common-envelope
 * calculations in Matsumoto et al. (2022)
 * arXiv 2202.10478
 *
 * Input parameters:
 *
 * initial_radius : Initial radius (Rsun)
 *
 * unbound_mass   : Mass that is unbound (Msun)
 *
 * recombination completion density : value from paper of 1e-11 used as      *  const
 *
 * Results are returned in cgs units.
 */

#define _VB 0

#if _VB == 0
#define _Puref Pure_function
#else
#define _Puref
#endif

#define _CHECK_RANGES FALSE

_Puref
double Matsumoto2022_plateau_luminosity(const double unbound_mass,
                                        const struct Matsumoto2022_data_t * const data)
{
    if(_VB)
    {
        printf("Plateau luminosity deltaM=%g\n",
               unbound_mass);
    }

    /*
     * Return plateau luminosity in erg/s (Eq. 18, erg/s)
     */
    return 4.8e38 *
        data->f_adiabatic/0.3 *
        cbrt(data->rho_i/1e-11) *
        Pow2d3(unbound_mass) *
        (data->vbar_E/300.0);
}

_Puref
double Matsumoto2022_plateau_duration(const double unbound_mass,
                                      const struct Matsumoto2022_data_t * const data)
{
    /*
     * Plateau duration in days (Eq. 16)
     */
    return 140.0 /
        cbrt(data->rho_i/1e-11) *
        cbrt(unbound_mass) /
        (data->vbar_E / 300.0);
}

_Puref
double Matsumoto2022_shock_luminosity(const double unbound_mass,
                                      const double pre_ejected_mass,
                                      const struct Matsumoto2022_data_t * const data)
{
    /*
     * Shock luminosity eq. 31
     */
    return 7e39 /
        cbrt(data->rho_i/1e-11) *
        (pre_ejected_mass / (0.1 * unbound_mass)) *
        Pow3(data->vbar_E / 300.0) *
        pow(unbound_mass, 2.0/3.0);
}

void Matsumoto2022_transient(struct stardata_t * const stardata,
                             const struct star_t * const primary, // primary (single) star after merger
                             const struct star_t * const previous_primary, // primary star before merger
                             const struct star_t * const previous_secondary, // secondary star before merger
                             void * input_data,
                             double * const timescale_days,
                             double * const luminosity_Lsun)
{
    const struct star_t * const secondary = Other_star_struct(primary);
    const double total_mass = previous_primary->mass + previous_secondary->mass;
    const double unbound_mass = total_mass - primary->mass - secondary->mass;
    struct Matsumoto2022_data_t * data = (struct Matsumoto2022_data_t *) input_data;

    if(unbound_mass > 0.0)
    {
        /*
         * Compute plateau timescale and luminosity
         */
        *timescale_days = Matsumoto2022_plateau_duration(
            unbound_mass,
            data
            );

        *luminosity_Lsun = (
            stardata->preferences->Matsumoto2022_fplateau *
            Matsumoto2022_plateau_luminosity(
                unbound_mass,
                data
                ) +

            stardata->preferences->Matsumoto2022_fshock *
            Matsumoto2022_shock_luminosity(
                unbound_mass,
                stardata->preferences->Matsumoto2022_shock_mass_fraction * previous_secondary->mass,
                data
                )
            ) / L_SUN;

        if(_CHECK_RANGES &&
           (*timescale_days < 0.1 || *timescale_days > 1e3 ||
            *luminosity_Lsun < 1.0 || *luminosity_Lsun > 1e8))
        {
            printf("crazy L,tscl of transient!\n");
            printf("tscl/days = %g, L/Lsun = %g\n",
                   *timescale_days,
                   *luminosity_Lsun);
            printf("previous primary mass %g\n",previous_primary->mass);
            printf("previous primary radius %g\n",previous_primary->radius);
            printf("total mass %g\n",total_mass);
            printf("unbound mass %g\n",unbound_mass);
            printf("fm = %g\n",unbound_mass / previous_primary->mass);
            printf("primary (donor) is star %d of previous type %d mass %g\n",
                   primary->starnum,
                   previous_primary->stellar_type,
                   previous_primary->mass);
            printf("secondary (accretor) is star %d of previous type %d mass %g\n",
                   secondary->starnum,
                   previous_secondary->stellar_type,
                   previous_secondary->mass);

            Exit_binary_c(BINARY_C_OUT_OF_RANGE,
                          "Transient luminosity or timescale is out of sensible range.");
        }
    }
    else
    {
        *timescale_days = 0.0;
        *luminosity_Lsun = 0.0;
    }
}
