#pragma once
#ifndef STELLAR_TYPES_H
#define STELLAR_TYPES_H
/*
 * The binary_c stellar population nucleosynthesis framework.
 *
 * Copyright Robert Izzard 2000-2025
 *
 * Contact rob.izzard@gmail.com
 *
 * Code repository:
 * https://gitlab.com/binary_c
 *
 * Documentation is in doc/
 * Code is in src/
 *
 * To build, run
 *
 * ---
 *
 * meson setup builddir
 * cd builddir
 * ninja binary_c_install
 *
 * ---
 *
 * Please see the files README, LICENCE and CHANGES,
 * and the doc/ directory for documentation which is mirrored
 * at https://binary_c.gitlab.io
 *
 **********************
 *
 * Purpose:
 *
 * Stellar types for binary_c, based on the definitions of
 * Hurley et al. "Evolution of Binary Stars and the
 * Effect of Tides on Binary Populations", MNRAS 2002 and
 * references therein.
 *
 * Also included are function macros for indentifying sets
 * of stellar types.
 */
#include "binary_c_parameters.h"
#include "binary_c_macros.h"

#define STELLAR_TYPES_LIST                                                                                                    \
    X(      LOW_MASS_MS,                    LOW_MASS_MAIN_SEQUENCE,  0,  "LMMS",                    "Convective low mass MS") \
    X(               MS,                             MAIN_SEQUENCE,  1,    "MS",                             "Main Sequence") \
    X(               HG,                           HERTZSPRUNG_GAP,  2,    "HG",                           "Hertzsprung Gap") \
    X(     GIANT_BRANCH,                        FIRST_GIANT_BRANCH,  3,    "GB",                        "First Giant Branch") \
    X(             CHeB,                       CORE_HELIUM_BURNING,  4,  "CHeB",                       "Core Helium Burning") \
    X(             EAGB,             EARLY_ASYMPTOTIC_GIANT_BRANCH,  5,  "EAGB",             "Early Asymptotic Giant Branch") \
    X(            TPAGB, THERMALLY_PULSING_ASYMPTOTIC_GIANT_BRANCH,  6, "TPAGB", "Thermally-Pulsing Asymptotic Giant Branch") \
    X(             HeMS,           NAKED_MAIN_SEQUENCE_HELIUM_STAR,  7,  "HeMS",                      "Helium Main Sequence") \
    X(             HeHG,         NAKED_HELIUM_STAR_HERTZSPRUNG_GAP,  8,  "HeHG",                    "Helium Hertzsprung Gap") \
    X(             HeGB,            NAKED_HELIUM_STAR_GIANT_BRANCH,  9,  "HeGB",                       "Helium Giant Branch") \
    X(             HeWD,                        HELIUM_WHITE_DWARF, 10,  "HeWD",                        "Helium White Dwarf") \
    X(             COWD,                 CARBON_OXYGEN_WHITE_DWARF, 11,  "COWD",                 "Carbon/Oxygen White Dwarf") \
    X(            ONeWD,                   OXYGEN_NEON_WHITE_DWARF, 12, "ONeWD",                   "Oxygen/Neon White Dwarf") \
    X(               NS,                              NEUTRON_STAR, 13,    "NS",                              "Neutron star") \
    X(               BH,                                BLACK_HOLE, 14,    "BH",                                "Black hole") \
    X( MASSLESS_REMNANT,                         STAR_WITH_NO_MASS, 15,     "γ",                          "Massless remnant")
#define NUMBER_OF_STELLAR_TYPES 16

/* short stellar types */
#undef X
#define X(SHORT,LONG,NUM,SHORT_STRING,LONG_STRING) SHORT = NUM,
enum { STELLAR_TYPES_LIST };

/* long stellar types */
#undef X
#define X(SHORT,LONG,NUM,SHORT_STRING,LONG_STRING) LONG = NUM,
enum {STELLAR_TYPES_LIST};


#undef X
#define X(SHORT,LONG,NUM,SHORT_STRING,LONG_STRING) #SHORT,
static const char * const short_stellar_type_macro_strings[] = { STELLAR_TYPES_LIST };

#undef X
#define X(SHORT,LONG,NUM,SHORT_STRING,LONG_STRING) SHORT_STRING,
static const char * const short_stellar_types[] = { STELLAR_TYPES_LIST };

#undef X
#define X(SHORT,LONG,NUM,SHORT_STRING,LONG_STRING) LONG_STRING,
static const char * const long_stellar_types[] = { STELLAR_TYPES_LIST };


/*
 * Stellar type comparison functions
 *
 * Usually these return TRUE or FALSE for a
 * range of stellar types.
 */

/* White dwarf */
#define WHITE_DWARF(A) (((A)>HeGB)&&((A)<NEUTRON_STAR))

/* White dwarf CO or ONe */
#define CONeWD(A)                               \
    ((A) == ONeWD ||                            \
     (A) == COWD)

/* Later than a white dwarf */
#define LATER_THAN_WHITE_DWARF(A) ((A)>HeGB)

/* stars with "cores" according to the BSE definition */
#define HAS_BSE_CORE(A)                         \
    ((A) >= HG &&                               \
     (A) <  NEUTRON_STAR &&                     \
     (A) != HeMS)

/* Naked He Star 7,8,9 */
#define NAKED_HELIUM_STAR(A)                    \
    ((A)>TPAGB &&                               \
     (A)<HeWD)

/* stars with cores */
#define CORED_STAR(A)                           \
    (                                           \
        (A)>MAIN_SEQUENCE                       \
        &&                                      \
        ((A)<HeWD && (A)!=HeMS)                 \
        )

/* helium-core star */
#define HELIUM_CORE_STAR(A)                     \
    (((A)==HERTZSPRUNG_GAP ||                   \
      (A)==FIRST_GIANT_BRANCH))

/* carbon-core stars */
#define CARBON_CORE_STAR(A)                     \
    ((A)>=CHeB &&                               \
     (A)<HeWD &&                                \
     (A)!=HeMS)

/* dammit! I wish we could call two things "MAIN_SEQENCE" but we can't! */
/* NB Helium main sequence is separate from the hydrogen main sequence */
/* Main sequence 0,1 */
#define ON_MAIN_SEQUENCE(A)                     \
    ((A) == LOW_MASS_MS ||                      \
     (A) == MAIN_SEQUENCE)

/* Either MS 0,1,7 */
#define ON_EITHER_MAIN_SEQUENCE(A)              \
    ((A) == LOW_MASS_MS ||                      \
     (A) == MAIN_SEQUENCE ||                    \
     (A) == HeMS)

/* Post MS He star 8,9 */
#define POST_MS_NAKED_He_STAR(A)                \
    ((A) == HeHG ||                             \
     (A) == HeGB)

/* Giant 2,3,4,5,6,8,9 (cf. Hurley+2002 Eq.61) */
#define GIANT_LIKE_STAR(A)                      \
    ((A) > MAIN_SEQUENCE &&                     \
     (A) < HeWD &&                              \
     (A) != HeMS)

/* Asymptotic Giant Branch 5,6 */
#define AGB(A)                                  \
    ((A) == EAGB ||                             \
     (A) == TPAGB)

/* Giant branch 3,5,6 */
#define ON_GIANT_BRANCH(A)                      \
    ((A) == FIRST_GIANT_BRANCH ||               \
     AGB(A))

/* either giant branch 3,5,6,9 */
#define ON_EITHER_GIANT_BRANCH(A)               \
    (ON_GIANT_BRANCH(A) ||                      \
     (A) == HeGB)

#define HELIUM_CORED(A)                         \
    (A == CHeB ||                               \
     NAKED_HELIUM_STAR(A) ||                    \
     A == HeWD)

#define COMENV_OFFSET (+100)

#define COMPACT_OBJECT(A)                       \
    ((A)>HeGB &&                                \
     (A)<MASSLESS_REMNANT)

#define NUCLEAR_BURNING(A) ((A)<HeWD)

#define POST_NUCLEAR_BURNING(A) ((A)>HeGB)

#define MASSIVE_REMNANT(A)                      \
    ((A)>HeGB &&                                \
     (A)<MASSLESS_REMNANT)

#define GRB_PROGENITOR(A)                       \
    ((A) == NS ||                               \
     (A) == BH)

#define POST_SN_OBJECT(A)                       \
    ((A) == NS ||                               \
     (A) == BH)

#define IS_A_PLANET(A)                                  \
    (stardata->star[(A)].stellar_type == LMMS &&        \
     stardata->star[(A)].mass<MAXIMUM_BROWN_DWARF_MASS))

#define WAS_A_STAR(A)                                           \
    (stardata->common.zero_age.mass[(A)] > MAXIMUM_BROWN_DWARF_MASS)

#define MASSLESS(A)                                             \
    (stardata->star[(A)].stellar_type  ==  MASSLESS_REMNANT)

#define EVOLVING(A) (!(MASSLESS(A)))

#define IS_A_STAR(A) (EVOLVING(A) &&                                    \
                      stardata->star[(A)].mass>MAXIMUM_BROWN_DWARF_MASS)

#define NUCLEAR_BURNING_BINARY                          \
    (NUCLEAR_BURNING(stardata->star[0].stellar_type) || \
     NUCLEAR_BURNING(stardata->star[1].stellar_type))

#define Is_luminous(A)                                  \
    (stardata->star[(A)].stellar_type < BLACK_HOLE)

#define Is_not_luminous(A)                      \
    (!(Is_luminous(A)))


/* Filter for convective envelopes */
#define CONVECTIVE_ENVELOPE(A)                  \
    (ON_EITHER_GIANT_BRANCH(A) ||               \
     (A) == HG ||                               \
     (A) == HeHG)

/**************************************************/

/* Here are the system types */

/* You can define these any way you like! */

/* k is the star number - assumes you have access to stardata */

/* These are the ways to define the systems */

/* Extended stellar types string array */
#define EXT_STELLAR_TYPES                       \
    STELLAR_TYPES,"Blue Straggler"

/************************************************************/
/* These are the types for the individual stars */
#define SINGLE_UNDEF 666
#define CHECK_BLUE_STRAGGLER(k)                                 \
    (stardata->common.tm<stardata->model.time                   \
     &&                                                         \
     ON_MAIN_SEQUENCE(stardata->star[(k)].stellar_type))
#define BLUE_STRAGGLER 16

/************************************************************/
/* These are the binary system types */
enum {
    SYSTEM_UNDEF = 0,
    BINARY_MS_MS = 1,
    BINARY_MS_HG = 2,
    BINARY_MS_GB = 3,
    BINARY_MS_EAGB = 4,
    BINARY_MS_TPAGB = 5,
    BINARY_STAR_SYSTEM = -2,
    SINGLE_STAR_SYSTEM = -1
};
/************************************************************/

#define EITHER_STAR_EVOLVING                    \
    (EVOLVING(0) ||                             \
     EVOLVING(1))

#define EITHER_STAR_MASSLESS                    \
    (MASSLESS(0) ||                             \
     MASSLESS(1))

#define NEITHER_STAR_MASSLESS                   \
    ( (!MASSLESS(0)) &&                         \
      (!MASSLESS(1)) )

#define NOTHING_LEFT (MASSLESS(0) && MASSLESS(1))

#define OSTAR(A)                                                \
    ((spectral_type(&(stardata->star[(A)])) == SPECTRAL_TYPE_O) \
     &&                                                         \
     (stardata->star[(A)].stellar_type<TPAGB))


#define Core_stellar_type(S) (                                          \
        (ON_MAIN_SEQUENCE(S) || COMPACT_OBJECT(S)) ? MASSLESS_REMNANT : \
        (CARBON_CORE_STAR(S)) ? COWD :                                  \
        (HELIUM_CORE_STAR(S)) ? HeWD :                                  \
        MASSLESS_REMNANT                                                \
        )

/*
 * Post-AGB stars: anything from 6-20kK with a thin envelope
 */
#define POST_AGB_MIN_TEFF 6.0e3
#define POST_AGB_MAX_TEFF 20.0e3
#define POST_AGB_MAX_MENV 0.1
#define Post_AGB(STAR)                                                  \
    (                                                                   \
        ON_EITHER_GIANT_BRANCH((STAR)->stellar_type) &&                 \
        envelope_mass(STAR) < POST_AGB_MAX_MENV && \
        In_range(Teff((STAR)->starnum),                                 \
                 POST_AGB_MIN_TEFF,                                     \
                 POST_AGB_MAX_TEFF)                                     \
    )


#define Stellar_type_string(S) ((char*)long_stellar_types[(S)])
#define Stellar_type_string_from_star(S) (                              \
        ((S)->TZO==TRUE && (S)->stellar_type==EAGB) ? "TZH" :           \
        ((S)->TZO==TRUE && POST_MS_NAKED_He_STAR((S)->stellar_type)) ? "TZHe" :   \
        (char*)long_stellar_types[(S)->stellar_type]                    \
        )
#define Short_stellar_type_string(S) ((char*)short_stellar_types[(S)])
#define Short_stellar_type_string_from_star(S) (                        \
        ((S)->TZO==TRUE && (S)->stellar_type==EAGB) ? "TZH" :           \
        ((S)->TZO==TRUE && POST_MS_NAKED_He_STAR((S)->stellar_type)) ? "TZHe" :   \
        (char*)short_stellar_types[(S)->stellar_type]                   \
        )
#endif /* STELLAR_TYPES_H */
