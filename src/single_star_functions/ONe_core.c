#include "../binary_c.h"
No_empty_translation_unit_warning;


/*
 * Return TRUE if we think we have an ONe core:
 * This happens if
 * 1) mcbagb > stardata->preferences->minimum_CO_core_mass_for_carbon_ignition (1.6Msun)
 *
 * and
 *
 * 2) CO core mass > stardata->preferences->minimum_CO_core_mass_for_carbon_ignition (1.08Msun)
 */

Boolean ONe_core(const struct stardata_t * const stardata Maybe_unused,
                 const struct star_t * const star Maybe_unused)
{
#ifdef BSE
    const double mcbagb =
        mcagbf(star->phase_start_mass,
               stardata->common.giant_branch_parameters);
    return
        (mcbagb > stardata->preferences->maximum_mcbagb_for_degenerate_carbon_ignition && star->core_mass[CORE_CO] > stardata->preferences->minimum_CO_core_mass_for_carbon_ignition)
        ? TRUE : FALSE;
#endif

#ifdef MINT
    /* TODO : fix me! */
    return FALSE;
#endif

}
