#include "../binary_c.h"
No_empty_translation_unit_warning;


/*
 * Erase all cores in a star
 */
void set_no_core(struct star_t * const star)
{
    Core_type i = 0;
    while(i<NUMBER_OF_CORES)
    {
        star->core_mass[i] = 0.0;
        i++;
    }
}
