#include "../binary_c.h"
No_empty_translation_unit_warning;


double convective_turnover_time(const double radius,
                                struct star_t * const star,
                                struct stardata_t * const stardata Maybe_unused)
{
    /*
     * convective turnover time, years
     *
     * note that "radius" is a radius passed in or,
     * if <=0, we use the stellar radius.
     */
    const double r = radius > TINY ? radius : star->radius;
    return MR23YR*cbrt(star->menv*star->renv*
                       (r-0.50*star->renv)/
                       (3.0*star->luminosity));
}
