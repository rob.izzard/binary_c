#include "binary_c.h"

#define Use_MCh_from_preferences                        \
    (stardata->preferences->chandrasekhar_mass > -TINY)

double chandrasekhar_mass(struct stardata_t * const Restrict stardata,
                          struct star_t * const Restrict star Maybe_unused)
{
    /*
     * Function to return the Chandrasekhar mass (MCh)
     * of the given star.
     *
     * Note that star could be NULL, so you will have
     * to deal with this case should the need arise.
     */
    double MCh;
    if(Use_MCh_from_preferences)
    {
        /*
         * Mass is given as a parameter
         */
        MCh = stardata->preferences->chandrasekhar_mass;
    }
    else
    {
        /*
         * Use an algorithm to determine MCh
         */
        MCh = 0.0;
        Exit_binary_c(BINARY_C_ALGORITHM_OUT_OF_RANGE,
                      "Invalid MCh algorithm %g\n",
                      stardata->preferences->chandrasekhar_mass);
    }
    return MCh;
}

double chandrasekhar_mass_wrapper(struct stardata_t * const Restrict stardata,
                                  const double mass,
                                  const double core_mass,
                                  const Stellar_type stellar_type)
{
    /*
     * We need to compare to MCh, so set up
     * a dummy star struct ("wd") and fill this
     * with basic information, like the stellar type,
     * mass and core mass.
     *
     * If you want to use a more "accurate" MCh,
     * e.g. to use the stellar abundances (in NUCSYN mode)
     * or other properties, call chandrasekhar_mass (above)
     * with a full star struct.
     *
     * This function is only to provide backwards compatibility
     * with the BSE stellar structure algorithm.
     */
    double MCh;
    if(Use_MCh_from_preferences)
    {
        MCh = stardata->preferences->chandrasekhar_mass;
    }
    else
    {
        struct star_t * wd = New_star;
        wd->stellar_type = stellar_type;
        wd->mass = mass;
        set_no_core(wd);
        const Core_type core_id = ID_core(stellar_type);
        if(core_id != CORE_NONE)
        {
            wd->core_mass[core_id] = core_mass;
        }
        MCh = chandrasekhar_mass(stardata,wd);
        free_star(&wd);
    }
    return MCh;
}
