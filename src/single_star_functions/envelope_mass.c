#include "../binary_c.h"
No_empty_translation_unit_warning;

double envelope_mass(const struct star_t * const star)
{
    /*
     * Compute the "envelope mass" of the star.
     *
     * In most cases, this is just mass - Outermost_core_mass
     * but in some special cases, e.g. very stripped HeHG/GB
     * Thorne-Zytkow objects, we have to be more careful and
     * instead treat the central NS/BH as the compact core of
     * the star and the envelope as everything else.
     */
    double menv;
    const double mc = Outermost_core_mass(star);
    if(star->TZO == TRUE)
    {
        menv = star->mass - star->core_mass[CORE_NEUTRON];
    }
    else
    {
        menv = star->mass - mc;
    }
    return menv;
}
