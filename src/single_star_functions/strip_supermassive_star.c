#include "../binary_c.h"
No_empty_translation_unit_warning;


void strip_supermassive_star(struct stardata_t * Restrict const stardata Maybe_unused,
                             struct star_t * Restrict const star Maybe_unused)
{
    /*
     * Handle supermassive stars, i.e. those more
     * massive than binary_c can realistically model.
     *
     * We yield the appropriate mass, then strip it.
     */


    /*
     * Compute how much mass we should lose
     */
#ifdef STRIP_SUPERMASSIVE_STARS
    const double dm = star->mass - MAXIMUM_STELLAR_MASS;
#ifdef NUCSYN
    /*
     * Compute its abundance mixture, Xej
     */
    Abundance * Xej = NULL;
    Boolean allocated;
    nucsyn_remove_dm_from_surface(stardata,
                                  star,
                                  dm,
                                  &Xej,
                                  &allocated);
#endif // NUCSYN
    /*
     * Yield the excess mass with the current
     * surface abundance
     */
    calc_yields(stardata,
                star,
                dm,
#ifdef NUCSYN
                Xej,
#endif
                0.0,
#ifdef NUCSYN
                NULL,
#endif
                star->starnum,
                YIELD_NOT_FINAL,
                SOURCE_OTHER);
#if defined NUCSYN && defined NUCSYN_STRIP_AND_MIX
    strip_and_mix_remove_mass(star, dm);
#endif // NUCSYN && NUCSYN_STRIP_AND_MIX

#ifdef NUCSYN
    if(allocated == TRUE)
    {
        Safe_free(Xej);
    }
#endif

    /*
     * Actually remove the mass from the stellar surface
     */
    update_mass(stardata,star,-dm);
#endif // STRIP_SUPERMASSIVE_STARS
}
