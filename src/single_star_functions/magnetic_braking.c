#include "../binary_c.h"
No_empty_translation_unit_warning;


void magnetic_braking(struct stardata_t * Restrict const stardata,
                      struct star_t * Restrict const star)
{

    if(stardata->preferences->magnetic_braking_algorithm ==
       MAGNETIC_BRAKING_ALGORITHM_HURLEY_2002)
    {
        if(star->mass>0.35 && star->stellar_type<HeWD)
        {
            /*
             * We include magnetic braking for stars that have appreciable
             * convective envelopes. This includes MS stars with M < 1.25,
             * HG stars near the GB and Giants.
             *
             * This is Eq. 50 in Hurley et al. (2002) based on
             * the mass-transfer rates of Rappaport, Verbunt and Joss (1983)
             * and the Skumanich (1972) braking law.
             *
             * We do not allow MB for fully convective MS stars.
             * NB this derivative is negative, because it is mass loss.
             *
             * Please note: I (RGI) cannot rederive this expression,
             * and having asked Chris Tout, he cannot either. So please
             * use with caution. It's clearly based on the Rappaport paper,
             * but beyond that ... I have no idea.
             *
             */
            star->derivative[DERIVATIVE_STELLAR_ANGMOM_MAGNETIC_BRAKING] =
                -5.83E-16 * star->menv/star->mass * Pow3(star->radius*star->omega);

            /*
             * The expression is -3.8e-30 M R_sun^4 (R/Rsun)^gamma omega^3
             * dyne cm (Eq. 36, p719) = g cm^2 s^-2
             *
             * we choose gamma = 3
             *
             * dyne is 1 g cm s^-2
             */
/*
  printf("MBdot %g ",
  star->derivative[DERIVATIVE_STELLAR_ANGMOM_MAGNETIC_BRAKING]);
  const double gamma=3.0;

  star->derivative[DERIVATIVE_STELLAR_ANGMOM_MAGNETIC_BRAKING] =
  -3.8e-30 *
  (star->mass * M_SUN) *
  Pow4(R_SUN)*
  pow(star->radius,gamma) *
  Pow3(star->omega / YEAR_LENGTH_IN_SECONDS) // s^-1

  // convert to code units
  / ANGULAR_MOMENTUM_CGS
  ;


  printf("%g convfac %g\n",
  star->derivative[DERIVATIVE_STELLAR_ANGMOM_MAGNETIC_BRAKING],
  ANGULAR_MOMENTUM_CGS
  );
  Exit_binary_c_no_stardata(0,
  "Magnetic braking check");
*/
        }
        else
        {
            star->derivative[DERIVATIVE_STELLAR_ANGMOM_MAGNETIC_BRAKING] = 0.0;
        }
    }
    else if(stardata->preferences->magnetic_braking_algorithm ==
            MAGNETIC_BRAKING_ALGORITHM_ANDRONOV_2003)
    {
        /*
         * Algorithm of  Andronov, Pinsonneault and Sills 2003
         * ApJ 582 358
         * based on
         * Sills, Pinsonneault and Terndrup 2000
         * ApJ 534 335
         * which is based on a modified Kawaler angular momentum
         * loss rate with an N=1.5 wind law (Chaboyer, Demarque &
         * Pinsonneault 1995).
         *
         * We apply this to non-remnant stars.
         */

        if(star->stellar_type < HeWD)
        {
            /*
             * Kw is calibrated to reproduce the solar rotation period
             * (2 km/s) at the age of the Sun (4.57Gyr, see also
             * Kawaler 1988)
             */
            const double Kw = 2.7e47; // g cm s

            /*
             *  omega in s^-1
             */
            const double omega = star->omega / YEAR_LENGTH_IN_SECONDS;

            /*
             * omega_crit is from Sills+2000 Eq.9
             *
             * it is the critical angular frequency at which
             * the angular momentum loss rate enters the saturated
             * regime.
             *
             * Table 1 of Andronov et al. 2003 gives omega_crit and
             *  can be fitted to
             *
             * log10(omega_crit) = -5.97780 + 1.54640 * (M/M_sun)
             * [RMS error 0.0868]
             *
             * in the range 0.1 to 1.2 Msun
             * (so we limit M to this range)
             */
            const double M = Max(0.1, Min(1.2, star->mass));
            const double omega_crit = Max(0.0,exp10(-5.97780 + 1.54640 * M));

            /*
             * hence dJ/dt, in cgs
             */
            star->derivative[DERIVATIVE_STELLAR_ANGMOM_MAGNETIC_BRAKING]
                = - Kw * sqrt(star->radius/star->mass) *
                (omega<omega_crit ? Pow3(omega) : (Pow2(omega)*omega_crit));

            /*
             * convert to code units
             */
            star->derivative[DERIVATIVE_STELLAR_ANGMOM_MAGNETIC_BRAKING]
                *= YEAR_LENGTH_IN_SECONDS/ANGULAR_MOMENTUM_CGS;

            Dprint("JdotMB%d %g (APS2003) : Jdot MB starJ %g omega = %g, omega_crit = %g, Jdot = %g cgs\n",
                   star->starnum,
                   stardata->model.time,
                   star->angular_momentum,
                   omega,
                   omega_crit,
                   star->derivative[DERIVATIVE_STELLAR_ANGMOM_MAGNETIC_BRAKING]);
        }
        else
        {
            /*
             * Star is a WD, NS or BH : assume no magnetic braking
             */
            star->derivative[DERIVATIVE_STELLAR_ANGMOM_MAGNETIC_BRAKING] = 0.0;
        }
    }
    else if(stardata->preferences->magnetic_braking_algorithm ==
            MAGNETIC_BRAKING_ALGORITHM_BARNES_2010)
    {
        /*
         * Algorithm of Barnes and Kim 2010
         * ApJ 721 675
         */

        /* TODO */
        Exit_binary_c(BINARY_C_ALGORITHM_OUT_OF_RANGE,
                      "Barnes and Kim 2010 algorithm : TODO\n");

    }
    else if(stardata->preferences->magnetic_braking_algorithm ==
            MAGNETIC_BRAKING_ALGORITHM_RAPPAPORT_1983)
    {
        /*
         * Algorithm of Rappaport et al. 1983 applied to
         * non-remnants
         */

        if(star->stellar_type < HeWD)
        {

            /*
             *  omega in s^-1
             */
            double omega = star->omega / YEAR_LENGTH_IN_SECONDS;

            /*
             * Magnetic braking rate
             */
            star->derivative[DERIVATIVE_STELLAR_ANGMOM_MAGNETIC_BRAKING] =
                -3.8e-30 * M_SUN * Pow4(R_SUN) *
                star->mass * pow(star->radius,
                                 stardata->preferences->magnetic_braking_gamma)
                *
                Pow3(omega);

            /*
             * convert to code units
             */
            star->derivative[DERIVATIVE_STELLAR_ANGMOM_MAGNETIC_BRAKING]
                *= YEAR_LENGTH_IN_SECONDS/ANGULAR_MOMENTUM_CGS;
        }
        else
        {
            /*
             * Star is a WD, NS or BH, assume no magnetic braking
             */
            star->derivative[DERIVATIVE_STELLAR_ANGMOM_MAGNETIC_BRAKING] = 0.0;
        }

    }
    else
    {
        Exit_binary_c(BINARY_C_ALGORITHM_OUT_OF_RANGE,
                      "Magnetic braking algorithm is unknown.\n");
    }


    star->derivative[DERIVATIVE_STELLAR_ANGMOM_MAGNETIC_BRAKING] *=
        stardata->preferences->magnetic_braking_factor;

#ifdef __MBLOGGING
    if(star->starnum==0)
    {
        printf("djmb want %g,  timescale %g cf. timestep %g\n",
               star->derivative[DERIVATIVE_STELLAR_ANGMOM_MAGNETIC_BRAKING],
               star->angular_momentum/star->derivative[DERIVATIVE_STELLAR_ANGMOM_MAGNETIC_BRAKING],
               stardata->model.dt
            );

    }

    if(0 && star->starnum==0)
    {
        const double tmb = fabs(star->angular_momentum /
                                star->derivative[DERIVATIVE_STELLAR_ANGMOM_MAGNETIC_BRAKING]);
        printf("djMB star %d = %30.20e from R=%g omega=%g mass=%g core_mass=%g env_mass=%g tmb=%g y (separation %g)\n",
               star->starnum,
               star->derivative[DERIVATIVE_STELLAR_ANGMOM_MAGNETIC_BRAKING],
               star->radius,
               star->omega,
               star->mass,
               Outermost_core_mass(star),
               envelope_mass(star),
               tmb,
               stardata->common.orbit.separation
            );
    }
#endif//__MBLOGGING
}
