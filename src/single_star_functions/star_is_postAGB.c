#include "../binary_c.h"
No_empty_translation_unit_warning;

/*
 * Identify a star as a post-(A)GB star
 *
 * We use Teff_postAGB_min to detect them,
 * but note we ignore Teff_postAGB_max because
 * often the Hurley prescription seems to make
 * very hot stars that would not be classed
 * as "post-AGB". These should be!
 */

Boolean star_is_postAGB(struct stardata_t * const stardata,
                        struct star_t * const star)
{
    const double teff =
        Teff_from_star_struct(star);
    const Boolean post_AGB =
        (Boolean)
        (
            (star->stellar_type == GIANT_BRANCH ||
             star->stellar_type == EAGB ||
             star->stellar_type == TPAGB ||
             WHITE_DWARF(star->stellar_type))
            &&
            teff > stardata->preferences->Teff_postAGB_min
            );
    return post_AGB;
}
