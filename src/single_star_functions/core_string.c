#include "../binary_c.h"
No_empty_translation_unit_warning;


/*
 * Make a string describing the star's core structure
 *
 * If full == TRUE, show all cores even if they are of zero mass.
 * If full == FALSE, do not include cores of zero mass.
 */

char * core_string(const struct star_t * const star,
                   const Boolean full)
{
    char * s;

    double * mc_bary = baryonic_core_masses(star);

    if(asprintf(&s,"Mc") > 0)
    {
        Boolean comma = FALSE;
        for(Core_type i=0; i<NUMBER_OF_CORES; i++)
        {
            char * t;
            if(full == TRUE || Is_not_zero(star->core_mass[i]))
            {
                if(!Fequal(star->core_mass[i],
                           mc_bary[i]))
                {
                    /*
                     * baryonic core mass != gravitational core mass
                     */
                    if(asprintf(&t,
                                "%s%s=%g=baryonic %g,",
                                s,
                                Core_string_short(i),
                                star->core_mass[i],
                                mc_bary[i])
                        )
                    {
                        Safe_free(s);
                        s = t;
                        comma = TRUE;
                    }
                }
                else
                {
                    /*
                     * baryonic core mass == gravitational core mass
                     */
                    if(asprintf(&t,
                                "%s%s=%g,",
                                s,
                                Core_string_short(i),
                                star->core_mass[i])
                        )
                    {
                        Safe_free(s);
                        s = t;
                        comma = TRUE;
                    }
                }
            }
        }
        if(comma == TRUE)
        {
            /* strip trailing comma */
            s[strlen(s)-1] = '\0';
        }
    }

    Safe_free(mc_bary);

    return s;
}
