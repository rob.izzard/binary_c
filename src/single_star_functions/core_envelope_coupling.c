#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef OMEGA_CORE

void core_envelope_coupling(struct stardata_t * stardata,
                            struct star_t * star)
{
#define jdot star->derivative[DERIVATIVE_STELLAR_ANGMOM_CORE_ENVELOPE_COUPLING]

    /*
     * Calculate the coupling between the core and the
     * envelope, i.e. the angular momentum transfer rate
     * between the two.
     */
    jdot = 0.0;
}

#endif
