#include "../binary_c.h"
No_empty_translation_unit_warning;


/*
 * Approximate conversion between a neutron star's
 * baryonic mass (mb/Msun) and graviational mass (mg/Msun)
 *
 * From https://arxiv.org/pdf/1904.11995.pdf Eq. 3
 * i.e. a private communication from J. Latimer to that
 * paper's author(s).
 *
 * This is a fit to the inverse of their function
 * between 1 and 3 Msun
 */
double Pure_function neutron_star_gravitational_mass(const double mb)
{
    return (-5.73050e-03)+(1.01590e+00)*mb+(-7.55780e-02)*mb*mb+(3.52420e-03)*mb*mb*mb;
}
