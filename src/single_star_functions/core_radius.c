#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef BSE
/***
 *
 * A function to determine the radius of the core of a giant-like star.
 * NOTE: this is out of date so rc should be obtained using stellar_structure() calls.
 * It is still OK to use but bear in mind that the core radius calculated
 * for non-degenerate giant cores is only a rough estimate.
 *
 *     Author : C. A. Tout
 *     Date :   26th February 1997
 *     Updated 6/1/98 by J. Hurley
 *
 * Note from RGI:
 *
 * This is an annoying function because, as stated above
 * the stellar_structure() library should be used instead.
 *
 * However, given that this is used a lot, I have tried instead
 * to keep it up to date in line with changes to the stellar
 * structure algorithm, in particular the addition of the
 * Hall and Tout (2014) core radius formulae.
 *
 * This could be replaced with a call (direct) to
 * stellar_structure_remnant_and_perturbations?
 * Or that could call *this* function. Either way is
 * an improvement on doubling up code.
 */

Constant_function double core_radius(struct stardata_t * const stardata,
                                     struct star_t * const star,
                                     const Stellar_type stellar_type,
                                     const Stellar_type core_stellar_type,
                                     const double core_mass,
                                     const double m0,
                                     const double phase_start_core_mass,
                                     const double mflash)
{
    double rc = 0.0;

    Dprint("In core_radius... ");

    if(stellar_type==BLACK_HOLE)
    {
        rc = rbh(core_mass);
    }
    else if(stellar_type==NEUTRON_STAR)
    {
        rc = rns(core_mass);
    }
    else if(ON_EITHER_MAIN_SEQUENCE(stellar_type))
    {
        /*
         * Main sequence stars.
         */
        rc = 0.0;
    }
    else if(stellar_type==CHeB ||
            stellar_type==EAGB ||
            (stellar_type<=FIRST_GIANT_BRANCH && m0>mflash))
    {
        /*
         * Core-helium-burning stars, FAGB stars and non-degenerate giant cores.
         */
        rc = 0.22390*pow(core_mass,0.620);
    }
    else
    {
#ifdef HALL_TOUT_2014_RADII
        /* Hall and Tout 2014 radii */
        if(stellar_type<=GIANT_BRANCH && Less_or_equal(m0,mflash))
        {
            rc = Hall_Tout_2014_low_mass_HG_RGB_radius(stardata,
                                                       star,
                                                       core_mass);
        }
        else if(stellar_type == TPAGB)
        {
            rc = Hall_Tout_2014_AGB_radius(stardata,
                                           star,
                                           core_mass,
                                           core_stellar_type);
        }
        else if(stellar_type == HeHG)
        {
            rc = Hall_Tout_2014_HeHG_radius(phase_start_core_mass);
        }
        else if(stellar_type == HeGB)
        {
            rc = Hall_Tout_2014_HeGB_radius(stardata,
                                            star,
                                            core_mass);
        }
        else
        {
            /* BSE default for other stellar types */
            rc = (stellar_type<=HeGB ? 5.0 : 1.0) * rwd(stardata,
                                                        star,
                                                        core_mass,
                                                        stellar_type,
                                                        TRUE);
        }
#else
        /* BSE prescription */

        /*
         * The degenerate giants and white dwarfs.
         */
        rc = rwd(stardata,star,core_mass,stellar_type);

        /*
         * Degenerate giants have hot subdwarf cores.
         */
        if(stellar_type<=HeGB) rc *= 5.0;
#endif

    }
    Dprint(" rc: %lf\n",rc);

    return(rc);
}
#endif//BSE
