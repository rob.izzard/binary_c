#include "../binary_c.h"
No_empty_translation_unit_warning;


/*
 * Stack cores
 *
 * This means, for example, if we have a CO core,
 * we put the helium core just outside it if it
 * doesn't exist.
 */

#define Stackprint(...)
//#define Stackprint(...) printf("STACKCORES ");printf(__VA_ARGS__);


void stack_cores(struct star_t * const star,
                 const double m)
{
    /*
     * Find the innermost core
     */
    {
        char * corestring = core_string(star,TRUE);
        Stackprint("STACK in  M=%g cores %s\n",
                   star->mass,
                   corestring);
        Safe_free(corestring);
    }
    Core_type innermost = NUMBER_OF_CORES-1;
    while(innermost>=0)
    {
        if(Core_exists(star,innermost))
        {
            break;
        }
        else
        {
            innermost--;
        }
    }
    Stackprint("innermost core %s\n",
           Core_string_short(innermost));

    if(innermost > -1)
    {
        /*
         * Layer cores
         */
        if(innermost > 0)
        {
            for(Core_type i=innermost-1; i>=0; i--)
            {
                Stackprint("stack core %i %s at %g, deeper at %g\n",
                       i,
                       Core_string_short(i),
                       star->core_mass[i],
                       star->core_mass[i+1]);
                star->core_mass[i] = Max(
                    star->core_mass[i+1], // deeper core
                    star->core_mass[i] // this core
                    );
            }
        }

        {
            char * corestring = core_string(star,TRUE);
            Stackprint("STACK layered  M=%g cores %s\n",
                   star->mass,
                   corestring);
            Safe_free(corestring);
        }

        /*
         * Make sure no core mass exceeds the total mass
         */
        trim_cores(star,m);

        /*
         * Special cases
         */
        if(NAKED_HELIUM_STAR(star->stellar_type))
        {
            star->core_mass[CORE_He] = 0.0;
        }
        else if(star->stellar_type == COWD)
        {
            set_no_core_below(star,CORE_CO);
        }
        else if(star->stellar_type == ONeWD)
        {
            set_no_core_below(star,CORE_ONe);
        }
        else if(star->stellar_type == NEUTRON_STAR)
        {
            set_no_core_below(star,CORE_NEUTRON);
        }
        else if(star->stellar_type == BLACK_HOLE)
        {
            set_no_core_below(star,CORE_BLACK_HOLE);
        }
        else if(star->stellar_type == MASSLESS_REMNANT)
        {
            set_no_core(star);
        }
    }
    {
        char * corestring = core_string(star,TRUE);
        Stackprint("STACK out M=%g stellar type %s cores %s\n",
                   star->mass,
                   Stellar_type_string_from_star(star),
                   corestring);
        Safe_free(corestring);
    }
}
