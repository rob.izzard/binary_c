#include "../binary_c.h"
No_empty_translation_unit_warning;


/*
 * Calculate the mean stellar density
 */

double Pure_function stellar_density(struct star_t * star)
{
    return 3.0 * star->mass /
        (4.0 * PI * Pow3(star->radius));
}
