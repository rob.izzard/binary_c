#include "../binary_c.h"
No_empty_translation_unit_warning;

void update_mass(struct stardata_t * const stardata,
                 struct star_t * const star,
                 const double dM)
{
    /*
     * Add mass dM to given star, taking into account
     * that the baryonic and gravitational masses may
     * differ.
     *
     * Note that there should be no Dprint statements
     * between the update of the baryonic mass and the
     * update of the gravitational mass. This is just
     * in case you want to check these two masses should
     * be the same.
     */

    /*
     * Update baryonic mass of given star by dM
     */
    star->baryonic_mass += dM;

    /*
     * Now compute the gravitational mass corresponding
     * to the baryonic mass.
     */
    if(star->stellar_type == NEUTRON_STAR ||
        star->stellar_type == BLACK_HOLE)
    {
        star->mass = baryonic_to_gravitational_remnant_mass(stardata,
                                                            star,
                                                            star->baryonic_mass);
    }
    else
    {
        /*
         * Neglect binding energy
         */
        star->mass += dM;
    }
}
