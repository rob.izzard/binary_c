/* Prototypes for the single star functions library */

#ifndef SINGLE_STAR_FUNCTIONS_PROTOTYPES_H
#define SINGLE_STAR_FUNCTIONS_PROTOTYPES_H
#include "../binary_c_structures.h"

double calc_stellar_wind_mass_loss(const Stellar_type stellar_type,
                                   const double luminosity,
                                   const double radius,
                                   const double mt,
                                   const double roche_lobe_radius,
                                   const double metallicity,
                                   const double omega,
                                   struct stardata_t * Restrict const stardata,
                                   const Star_number k);


Constant_function double core_radius(struct stardata_t * const stardata,
                                     struct star_t * const star,
                                     const Stellar_type stellar_type,
                                     const Stellar_type core_stellar_type,
                                     const double core_mass,
                                     const double m0,
                                     const double phase_start_core_mass,
                                     const double mflash);


void giant_age(double * Restrict const mc,
               const double mt,
               Stellar_type * Restrict const stellar_type,
               double * Restrict const m0,
               double * Restrict const age,
               struct stardata_t * const stardata,
               struct star_t * const star);



void magnetic_braking(struct stardata_t * Restrict const stardata,
                      struct star_t * Restrict const star);

double E2(struct stardata_t * Restrict const stardata,
          const struct star_t * Restrict const star);

double Pure_function dynamical_timescale(const struct star_t * Restrict const star);
double Pure_function dynamical_timescale_envelope(const struct star_t * const Restrict star);
void strip_supermassive_star(struct stardata_t * Restrict const stardata Maybe_unused,
                             struct star_t * Restrict const star Maybe_unused);


double Pure_function stellar_density(struct star_t * star);

Time Pure_function kelvin_helmholtz_time_from_LMR(const double L,
                                                  const double M,
                                                  const double Menv,
                                                  const double R);
Time Pure_function kelvin_helmholtz_time(struct star_t * Restrict star);
double Pure_function logg(const struct star_t * Restrict const star );
double Pure_function g(const struct star_t * Restrict const star );
double Pure_function logLeff(const struct star_t * Restrict const star );

double Mira_pulsation_period(const double mass,
                             const double radius,
                             struct stardata_t * stardata);

double chandrasekhar_mass(struct stardata_t * const Restrict stardata,
                          struct star_t * const Restrict star Maybe_unused);

double chandrasekhar_mass_wrapper(struct stardata_t * const Restrict stardata,
                                  const double mass,
                                  const double core_mass,
                                  const Stellar_type stellar_type);

double Pure_function neutron_star_baryonic_mass(const double mg);
double Pure_function neutron_star_gravitational_mass(const double mb);
double chirp_mass(struct star_t * Restrict const star);

double radius_mass_derivative(struct stardata_t * const stardata,
                              struct star_t * const star,
                              const double mass);

void set_no_core(struct star_t * const star);
void set_no_core_below(struct star_t * const star,
                       Core_type j);
void trim_cores(struct star_t * const star,const double m);
void stack_cores(struct star_t * const star,const double m);
char * core_string(const struct star_t * const star,
                   const Boolean full);

Boolean ONe_core(const struct stardata_t * const stardata,
                 const struct star_t * const star);
Boolean star_is_LBV(const struct stardata_t * const stardata,
                    const struct star_t * const star);
Boolean star_is_WR(const struct star_t * const star);
Boolean star_is_postAGB(struct stardata_t * const stardata,
                        struct star_t * const star);

double convective_turnover_time(const double radius,
                                struct star_t * star,
                                struct stardata_t * stardata);

double inner_core_mass(struct stardata_t * const stardata,
                       struct star_t * const star,
                       Core_type i);
double envelope_mass(const struct star_t * const star);
void update_mass(struct stardata_t * const stardata,
                 struct star_t * const star,
                 const double dM);
double * baryonic_core_masses(const struct star_t * const star);
double baryonic_mass_offset(const struct star_t * const star);

#endif /* SINGLE_STAR_FUNCTIONS_PROTOTYPES_H */
