#include "../binary_c.h"
No_empty_translation_unit_warning;

double Pure_function g(const struct star_t * Restrict const star)
{
    /* calculate g for a star */
    return(TINY+GRAVITATIONAL_CONSTANT*M_SUN*star->mass/Pow2(star->radius*R_SUN));
}
