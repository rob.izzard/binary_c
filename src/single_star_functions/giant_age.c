/*
 *       Determine the age and phase start mass of a giant
 *       from its core mass and type.
 *
 *       Note: sometimes this routine will change the core mass.
 *       This happens when the core mass is too large or small
 *       for a particular stellar type, e.g. red giants cannot
 *       have cores more massive than possible in the red
 *       (super)giant phase.
 *
 *       Author : C. A. Tout
 *       Date   : 24th September 1996
 *       Revised: 21st February 1997 to include core-helium-burning stars
 *
 *       Rewritten: 2nd January 1998 by J. R. Hurley to be compatible with
 *                  the new evolution routines and to include new stellar
 *                  types.
 *
 *       Rewritten: 2023, Robert Izzard.
 *
 */
#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef BSE

#define MAX_ITERATIONS 30
#define MASS_TOLERANCE 0.00001
#define LUMINOSITY_TOLERANCE 0.0001

#define  GIANT_AGE_FATAL_ERRORS
/* dummy stellar types */
#define COWD_TO_CHeB -1
#define EAGB_TO_CHeB -2

void giant_age(double * Restrict const core_mass, /** core mass in/out **/
               const double current_mass, /* current mass */
               Stellar_type * Restrict const stellar_type, /* stellar type */
               double * Restrict const phase_start_mass, /* mass at start of this stellar phase */
               double * Restrict const age, /* stellar age  */
               struct stardata_t * Restrict const stardata,
               struct star_t * Restrict const star)
{
    const double * const metallicity_parameters = stardata->common.metallicity_parameters;
    const double * const giant_branch_parameters = stardata->common.giant_branch_parameters;
    struct BSE_data_t * bse = new_BSE_data();


#if (DEBUG==1)
    Stellar_type stellar_typein = *stellar_type;
#endif /*DEBUG*/
    Dprint("giant_age in, stellar type %d, core_mass=%g, M now=%g, phase_start_mass=%g, menv = %g \n",
           *stellar_type,
           *core_mass,
           current_mass,
           *phase_start_mass,
           current_mass - *core_mass
        );

    if(Is_zero(*core_mass) ||
       Is_zero(current_mass) ||
       Is_zero(*phase_start_mass))
    {
        Backtrace;
        Exit_binary_c(BINARY_C_ALGORITHM_OUT_OF_RANGE,
                      "giant_age cannot be called with this core or total mass, they must all be non-zero (core_mass=%g current_mass=%g phase_start_mass=%g)\n",
                      *core_mass,
                      current_mass,
                      *phase_start_mass);
    }

    /* First we check that we don't have a CheB star
     * with too small a core mass. */
    if(*stellar_type==CHeB)
    {
        /** Set the minimum CHeB core mass using M = Mflash **/
        const double _mcheif = mcheif(metallicity_parameters[ZPAR_MASS_HE_FLASH],
                                      metallicity_parameters[ZPAR_MASS_HE_FLASH],
                                      metallicity_parameters[10],
                                      giant_branch_parameters);

        if(Less_or_equal(*core_mass,_mcheif))
        {
            *stellar_type = FIRST_GIANT_BRANCH;
            Dprint("Set stellar type to GB\n");
        }
    }

    /* Next we check that we don't have a GB star for M => Mfgb */
    if(*stellar_type == FIRST_GIANT_BRANCH)
    {
        /* Set the maximum GB core mass using M = Mfgb */
        const double _mcheif = mcheif(metallicity_parameters[ZPAR_MASS_FGB],
                                      metallicity_parameters[ZPAR_MASS_HE_FLASH],
                                      metallicity_parameters[9],
                                      giant_branch_parameters);

        Dprint("On GB : set core_massy = %g cf. core_mass = %g\n",
               _mcheif,
               *core_mass);
        if(More_or_equal(*core_mass,_mcheif))
        {
            *stellar_type = CHeB;
            *age = 0.0;
            Dprint("Set Zero-age CHeB\n");
        }
    }

    /* TPAGB stars */
    else if(*stellar_type == TPAGB)
    {
        /*
         * We try to start the star from the start of the SAGB by
         * setting Core_Mass = Core_Mass,TP.
         */
        const double _mc2dup = 0.44*stardata->common.max_mass_for_second_dredgeup + 0.448;
        Dprint("start AGB _mc2dup = %g, cf. core_mass now %g\n",
               _mc2dup,
               *core_mass);

#ifdef __DEPRECATED
        if(0)
        {
            /*
             * Old BSE options for the core mass,
             * we should use the guess_mc1tp function
             * to do this more reliably.
             */
            if(*core_mass>_mc2dup)
            {
                /*
                 * A TPAGB with this sized core mass cannot exist as it should
                 * already have become a NS or BH as an EAGB star.
                 * We set it up so that it will.
                 */
                core_massx = (*core_mass + 0.35)/0.773;
            }
            else if(More_or_equal(*core_mass,0.80))
            {
                core_massx = (*core_mass - 0.448)/0.44;
            }
            else
            {
                core_massx = *core_mass;
            }
        }
#endif //__DEPRECATED
        const double _mc1tp = guess_mc1tp(current_mass,
                                          current_mass,
                                          star,
                                          stardata);
        Dprint("_mc1tp = %g\n",_mc1tp);

        *phase_start_mass = mbagbf(_mc1tp,
                                   giant_branch_parameters);

        Dprint("new phase_start_mass = %g\n",*phase_start_mass);
        if(Less_or_equal(*phase_start_mass,0.0))
        {
            /*
             * Carbon core mass is less then the minimum for the start of SAGB.
             * This must be the case of a low-mass C/O or O/Ne WD with only a
             * very small envelope added or possibly the merger of a helium star
             * with a main sequence star. We will set (*phase_start_mass) = current_mass and then reset the
             * core mass to allow for some helium to be added to the C/O core.
             */
            *stellar_type = COWD_TO_CHeB;
            Dprint("CO core mass < start of SAGB : low-mass COWD or ONeWD with thin envelope\n");
        }
        else
        {
            stellar_timescales(stardata,
                               star,
                               bse,
                               *phase_start_mass,
                               current_mass,
                               *stellar_type);
            *age = Max(bse->timescales[T_TPAGB_TX],
                       bse->timescales[T_TPAGB_FIRST_PULSE]);
            Dprint("Hence age = Max(T_TPAGB_TX = %g,T_TPAGB_FIRST_PULSE = %g) = %g\n",
                   bse->timescales[T_TPAGB_TX],
                   bse->timescales[T_TPAGB_FIRST_PULSE],
                   *age);
        }
    }

    else if(*stellar_type==EAGB)
    {
        /*
         * We fit a Helium core mass at the base of the AGB.
         */
        *phase_start_mass = mbagbf(*core_mass,giant_branch_parameters);
        Dprint("EAGB : set phase_start_mass = %g from *core_mass = %g\n",*phase_start_mass,*core_mass);

        if(Less_or_equal(*phase_start_mass,TINY))
        {
            /*
             * Helium core mass is less then the BAGB minimum.
             *
             * What has happened here is that a helium star has
             * accreted hydrogen, but it is of such low mass that
             * it would never normally reach the AGB.
             *
             * The BSE algorithm converts it to a CHeB star instead.
             */

            /*
             * experimental :
             * we shouldn't convert to a CHeB, just
             * leave as an EAGB / He-star
             */
            *phase_start_mass = current_mass;
            *stellar_type = EAGB;
            Dprint("set to EAGB_TO_CHeB\n");
        }

        stellar_timescales(stardata,
                           star,
                           bse,
                           *phase_start_mass,
                           current_mass,
                           *stellar_type);
        *age = bse->timescales[T_HE_IGNITION] + bse->timescales[T_HE_BURNING];

        Dprint("Set new age %g\n",*age);

    }

    if(*stellar_type==CHeB)
    {
        /*
         * We should work with the fractional age
         *
         * Note: old versions of this code had the fractional
         *       age passed in. We do not do this: instead we
         *       have consistent parameters passed in for all
         *       stellar types, so we must compute the fractional
         *       age here.
         */
        const double fractional_age = Limit_range(*age / star->bse->tm,
                                                  0.0, 1.0);

        /*
         * The supplied age is actually the fractional age, fage, of CHeB lifetime
         * that has been completed, ie. 0 <= age <= 1.
         */
        Dprint("giant_age : CHeB : age = %g, burning time %g %g, fractional age=%g\n",
               *age,
               bse->tm,
               bse->timescales[T_HE_BURNING],
               fractional_age);

#ifdef GIANT_AGE_FATAL_ERRORS
        if(fractional_age<0.0 || fractional_age>1.0)
        {
            /*
             * we cannot have a fractional age that's
             * not between 0 and 1
             */
            Exit_binary_c(BINARY_C_GIANT_AGE_FRACTIONAL_AGE_OUT_OF_BOUNDS,
                          " FATAL ERROR! giant_age: fractional age out of bounds : %g\n",
                          fractional_age);
        }
#endif

        /*
         * Get the minimum, fage=1, and maximum, fage=0, allowable masses
         */
        const double _mcagb = mcagbf(metallicity_parameters[ZPAR_MASS_HE_FLASH],
                                     giant_branch_parameters);
        Dprint("_mcagb = %g\n",_mcagb);

        const double mmin =
            More_or_equal(*core_mass,_mcagb) ?
            mbagbf(*core_mass,giant_branch_parameters) :
            metallicity_parameters[ZPAR_MASS_HE_FLASH];

        const double mmax = mheif(*core_mass,
                                  metallicity_parameters[ZPAR_MASS_HE_FLASH],
                                  metallicity_parameters[10],
                                  giant_branch_parameters);
        Dprint("giant_age : CHeB : mmin=%g mmax=%g\n",mmin,mmax);

        Dprint("age = %g\n",fractional_age);
        if(Fequal(fractional_age,0.0))
        {
            *phase_start_mass = mmax;
        }
        else if(Fequal(fractional_age,1.0))
        {
            *phase_start_mass = mmin;
        }
        else
        {
            /* Use the bisection method to find phase_start_mass */
            const double x = 1.0 - fractional_age;
            Dprint("x = 1 - fractional_age = %g\n",x);

            const double fmid =
                x * mcheif(mmax,
                           metallicity_parameters[ZPAR_MASS_HE_FLASH],
                           metallicity_parameters[10],
                           giant_branch_parameters)
                +
                fractional_age * mcagbf(mmax,giant_branch_parameters)
                -
                *core_mass;
            Dprint("fmid = %g\n",fmid);

            const double f = x * mcheif(mmin,
                           metallicity_parameters[ZPAR_MASS_HE_FLASH],
                           metallicity_parameters[10],
                           giant_branch_parameters) +
                fractional_age*mcagbf(mmin,giant_branch_parameters) - *core_mass;
            Dprint("f = %g : f*fmid = %g\n",f,f*fmid);

            if(More_or_equal(f*fmid,0.0))
            {
                /*
                 * This will probably occur if core_mass is just greater
                 * than the minimum allowed mass
                 * for a CHeB star and fage > 0.
                 *
                 * Rob : this isn't good. The star moves backwards
                 * in its stellar type, when it shouldn't!
                 */
                *stellar_type = FIRST_GIANT_BRANCH;
                Dprint("Convert to FGB\n");
            }
            else
            {
                *phase_start_mass = mmin;
                double dm = mmax - mmin;
                for(unsigned int j=1; j<MAX_ITERATIONS; j++)
                {
                    dm *= 0.5;
                    const double mmid = *phase_start_mass + dm;
                    const double _fmid =
                        (1.0-fractional_age) * mcheif(mmid,
                                                     metallicity_parameters[ZPAR_MASS_HE_FLASH],
                                                     metallicity_parameters[10],
                                                     giant_branch_parameters) +
                        fractional_age * mcagbf(mmid,giant_branch_parameters) - *core_mass;
                    if(_fmid < 0.0)
                    {
                        *phase_start_mass = mmid;
                    }
                    if((Abs_less_than(dm,MASS_TOLERANCE))||(Is_zero(_fmid)))
                    {
                        /* converged */
                        j = MAX_ITERATIONS + 1;
                    }
#ifdef GIANT_AGE_FATAL_ERRORS
                    else if(j >= MAX_ITERATIONS - 1)
                    {
                        Exit_binary_c(BINARY_C_GIANT_AGE_ROOT_NOT_FOUND,"FATAL ERROR! giant_age: root not found\n");
                    }
#endif
                }
                Dprint("converged on phase start mass = %g\n",
                       *phase_start_mass);
            }
        }

        Dprint("giant_age : CHeB : phase_start_mass=%g\n",*phase_start_mass);

        stellar_timescales(stardata,
                           star,
                           bse,
                           *phase_start_mass,
                           current_mass,
                           *stellar_type);
        *age = bse->timescales[T_HE_IGNITION] + fractional_age * bse->timescales[T_HE_BURNING];

        Dprint("giant_age : CHeB : age=%g\n",*age);
        Dprint("This is from %g+%g*%g\n",
               bse->timescales[T_HE_IGNITION],
               fractional_age,
               bse->timescales[T_HE_BURNING]);
    }

    /*
     * RGI assume HG stars behave like RGB stars,
     * then evolution is on the timescale of the core mass
     * of the HG star rather than the timescale corresponding
     * to the total mass of the new star.
     */
    if(*stellar_type == FIRST_GIANT_BRANCH ||
       *stellar_type == HERTZSPRUNG_GAP)
    {
        /*
         * Again check that we don't have a
         * red giant that is more massive than the most
         * massive red supergiant possible. If we do,
         * limit the core mass.
         */
        if(*stellar_type == FIRST_GIANT_BRANCH)
        {
            const double _mcheif = mcheif(metallicity_parameters[ZPAR_MASS_FGB],
                                          metallicity_parameters[ZPAR_MASS_HE_FLASH],
                                          metallicity_parameters[9],
                                          giant_branch_parameters);
            Dprint("FGB or HG : _mcheif = %g\n",_mcheif);
            if(More_or_equal(*core_mass,_mcheif))
            {
                *core_mass = Min(current_mass,0.99*_mcheif);
                Dprint("Limit core mass for red (super)giants to %g\n",
                       *core_mass);
            }
        }

        /* Next we find an phase_start_mass so as to place the star at the BGB */
        const double _mcheif = mcheif(metallicity_parameters[ZPAR_MASS_HE_FLASH],
                                      metallicity_parameters[ZPAR_MASS_HE_FLASH],
                                      metallicity_parameters[9],
                                      giant_branch_parameters);
        Dprint("cf core_mass = %g to _mcheif = %g\n",
               *core_mass,
               _mcheif);

        if(*core_mass>_mcheif)
        {
            *phase_start_mass = mheif(*core_mass,
                                      metallicity_parameters[ZPAR_MASS_HE_FLASH],
                                      metallicity_parameters[9],
                                      giant_branch_parameters);
            Dprint("phase_start_mass = %g\n",*phase_start_mass);
        }
        else
        {
            /* Use Newton-Raphson to find phase_start_mass from Lbgb */
            *phase_start_mass = metallicity_parameters[ZPAR_MASS_HE_FLASH];
            Dprint("NR for Lbgb, phase_start_mass = %g\n",*phase_start_mass);
            stellar_timescales(stardata,
                               star,
                               bse,
                               *phase_start_mass,
                               current_mass,
                               *stellar_type);

            const double lum = lmcgbf(*core_mass,bse->GB);
            double m = *phase_start_mass;
            Dprint("GB NR in m = %g, looking for L = %g\n",*phase_start_mass,lum);
            for(unsigned int j=0; j<MAX_ITERATIONS; j++)
            {
                if(j >= MAX_ITERATIONS-1)
                {
#ifdef GIANT_AGE_FATAL_ERRORS
                    Exit_binary_c(BINARY_C_NEWTON_RAPHSON_FAILURE,
                                  "Fatal error! giant_age (stellar_type==%d, iteration %u): root not found (looking for luminosity)\n",
                                  *stellar_type,
                                  j);
#endif
                    break;
                }

                const double dell = lbgbf(m,giant_branch_parameters) - lum;
                const double derl = lbgbdf(m,giant_branch_parameters);
                Dprint("GB j=%u NR(m=%g) dell=%g derl=%g\n",j,m,dell,derl);

                if(Abs_less_than(dell/lum,LUMINOSITY_TOLERANCE))
                {
                    /* converged */
                    j = MAX_ITERATIONS + 1;
                }
                else
                {
                    m -= dell/derl;
                    Dprint("m now %g\n",m);
                }

            }
            *phase_start_mass = m;
            Dprint("NR gave phase_start_mass = %g\n",*phase_start_mass);
        }

        Dprint("Final GB timescales");
        stellar_timescales(stardata,
                           star,
                           bse,
                           *phase_start_mass,
                           current_mass,
                           *stellar_type);
        *age = bse->timescales[T_BGB] + 1.0E-06*(bse->timescales[T_HE_IGNITION] - bse->timescales[T_BGB]);
        Dprint("new age %g\n",*age);
    }

    if(POST_MS_NAKED_He_STAR(*stellar_type))
    {
        /*
         * We make a post-MS naked helium star.
         * To make things easier we put the star at the TMS point
         * so it actually begins as type 8.
         */
        *stellar_type = HeHG;
        Dprint("post-MS naked He star : stellar type %d,  core_mass = %g\n",
               *stellar_type,
               *core_mass);

        /* we need bracketing masses */

        /* first, mmin */
        const double mmin = *core_mass;
        stellar_timescales(stardata,
                           star,
                           bse,
                           mmin,
                           *core_mass,
                           *stellar_type);
        const double _mcTAMS = mcgbf(bse->luminosities[L_END_MS],
                                        bse->GB,
                                        bse->luminosities[L_LMX]);
        if(More_or_equal(_mcTAMS,*core_mass))
        {
#ifdef GIANT_AGE_FATAL_ERRORS
            Exit_binary_c(BINARY_C_GIANT_AGE_MMIN_TOO_BIG,"Fatal error: gntage9: mmin too big\n");
#endif
             /* 2007 change */
            *phase_start_mass = current_mass;
            goto breakloop;
        }
        Dprint("Bracket low : mmin = %g gives _mcTAMS = %g (cf core_mass %g) LTAMS = %g\n",
               mmin,
               _mcTAMS,
               *core_mass,
               bse->luminosities[L_END_MS]);

        /* now want mmax */
        double mmax = current_mass;
        Dprint("Find mmax, start at %g\n",mmax);
        {
            double _mcgbf;
            for(unsigned int j=0; j<MAX_ITERATIONS; j++)
            {
                stellar_timescales(stardata,
                                   star,
                                   bse,
                                   mmax,
                                   *core_mass,
                                   *stellar_type);
                _mcgbf = mcgbf(bse->luminosities[L_END_MS],
                               bse->GB,
                               bse->luminosities[L_LMX]);
                if(_mcgbf > *core_mass)
                {
                    j = MAX_ITERATIONS + 1; /* break out */
                }
                else
                {
                    mmax *= 2.0;
                    if(j == MAX_ITERATIONS-1)
                    {
#ifdef GIANT_AGE_FATAL_ERRORS
                        Exit_binary_c(BINARY_C_GIANT_AGE_MMAX_NOT_FOUND,
                                      "Fatal error! Gntage9: mmax not found\n");
#endif
                        goto breakloop;
                    }
                }
            }
            Dprint("Bracket high : mmax = %g, _mcgbf = %g, LTAMS %g\n",
                   mmax,
                   _mcgbf,
                   bse->luminosities[L_END_MS]);

            /*
             * Bisect to find phase_start_mass, the "phase start mass"
             * that gives us the correct core mass at the
             * start of the HeHG.
             */
            const double fmid = _mcgbf - *core_mass;
            const double f = _mcTAMS - *core_mass;

            /*
             * In this comparison, f can be very
             * close to zero, so do NOT use More_or_equal()
             */
            if(f*fmid>=0.0)
            {
                Dprint("Error: f=%g * fmid=%g = %g >= 0 [_mcgbf = %g, _mcTAMS = %g, core_mass = %g]\n",
                       f,
                       fmid,
                       f*fmid,
                       _mcgbf,
                       _mcTAMS,
                       *core_mass);
#ifdef GIANT_AGE_FATAL_ERRORS
                Exit_binary_c(BINARY_C_GIANT_AGE_ROOT_NOT_BRACKETED,
                              "Fatal error! giant_age 9: root not bracketed\n");
#endif
                goto breakloop;
            }
        }

        Dprint("Loop for phase_start_mass\n");
        *phase_start_mass = mmin;

        double dm = mmax - mmin;
        for(unsigned int j=0 ; j<MAX_ITERATIONS; j++)
        {
            dm *= 0.5;
            const double mmid = *phase_start_mass + dm;
            stellar_timescales(stardata,
                               star,
                               bse,
                               mmid,
                               *core_mass,
                               *stellar_type);

            const double _mcgbf = mcgbf(bse->luminosities[L_END_MS],
                                        bse->GB,
                                        bse->luminosities[L_LMX]);
            Dprint("With phase start mass %g TAMS (age %g) luminosity %g gives core mass %g Lcheck %g\n",
                   mmid,
                   bse->tm,
                   bse->luminosities[L_END_MS],
                   _mcgbf,
                   lgbtf(bse->tm,
                         bse->GB[GB_A_HE],
                         bse->GB,
                         bse->timescales[T_GIANT_TINF_1],
                         bse->timescales[T_GIANT_TINF_2],
                         bse->timescales[T_GIANT_TX])
                );

            const double fmid = _mcgbf - *core_mass;
            if(fmid < 0.0)
            {
                *phase_start_mass = mmid;
            }
            if(Abs_less_than(dm,MASS_TOLERANCE)||
               Fequal(fmid,0.0))
            {
                Dprint("converged %g < %g : phase_start_mass = %g\n",
                       dm,
                       MASS_TOLERANCE,
                       *phase_start_mass);
                goto breakloop;
            }
            else if(j == MAX_ITERATIONS-1)
            {
                Dprint("Error: hit MAX_ITERATIONS\n");
#ifdef GIANT_AGE_FATAL_ERRORS
                Exit_binary_c(BINARY_C_GIANT_AGE_ROOT_NOT_FOUND,
                              "Fatal error! GNTAGE9: root not found\n");
#endif
                goto breakloop;
            }
        }
        Dprint("Converged on phase start mass %g\n",
               *phase_start_mass);

    breakloop:
        stellar_timescales(stardata,
                           star,
                           bse,
                           *phase_start_mass,
                           *core_mass,
                           *stellar_type);

        /*
         * We want to be slightly after the end
         * of the helium main sequence. I tried 1e-10
         * but that failed because of numerics, so lets try
         * 1e-6 instead.
         */
        *age = bse->tm * (1.0 + 1.0e-6);
        Dprint("new age %g\n",*age);
    }

    if(*stellar_type==COWD_TO_CHeB ||
       *stellar_type==EAGB_TO_CHeB)
    {
        /*
         * we are converting from an EAGB or COWD to a CHeB star
         */
        Dprint("%s converted to CHeB\n",
               *stellar_type==COWD_TO_CHeB ? "COWD" : "EAGB");
        *stellar_type = CHeB;
        *phase_start_mass = current_mass;

        const double _mcagbf = mcagbf(*phase_start_mass,giant_branch_parameters);
        const double fractional_age = *core_mass/_mcagbf;
        Dprint("New fractional_age %g, _mcagbf = %g\n",fractional_age,_mcagbf);

        stellar_timescales(stardata,
                           star,
                           bse,
                           *phase_start_mass,
                           current_mass,
                           *stellar_type);

        const double _mc0 =
            Less_or_equal(*phase_start_mass,metallicity_parameters[ZPAR_MASS_HE_FLASH]) ?
            mcgbf(bse->luminosities[L_HE_IGNITION],
                  bse->GB,
                  bse->luminosities[L_LMX]) :
            mcheif(*phase_start_mass,
                   metallicity_parameters[ZPAR_MASS_HE_FLASH],
                   metallicity_parameters[10],
                   giant_branch_parameters);

        *core_mass = Min(current_mass, _mc0 + (_mcagbf - _mc0)* fractional_age);
        *age = bse->timescales[T_HE_IGNITION] +
            fractional_age * bse->timescales[T_HE_BURNING];
        Dprint("new core mass %g, age %g\n",
               *core_mass,
               *age);
    }

    Dprint("Finished in giant_age\n");
#if (DEBUG==1)
    Dprint("It got new stellar type: %d (in was %d)\nInitial Mass: %f\naj: %f\n",
           *stellar_type,
           stellar_typein,
           *phase_start_mass,*age);
#endif
    Dprint("giant age : out stellar type %d, phase_start_mass=%g current_mass=%g, core_mass=%g, age=%g\n",
           *stellar_type,
           *phase_start_mass,
           current_mass,
           *core_mass,
           *age);

    free_BSE_data(&bse);

    return;
}

#endif//BSE
