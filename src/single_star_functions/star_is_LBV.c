#include "../binary_c.h"
No_empty_translation_unit_warning;


/*
 * Boolean to report whether a star is a
 * luminous blue variable.
 *
 * Note that Hurley+2002 also suggest using
 * the condition that the star is post-main-sequence,
 * but we do not do this because presumably any LBV
 * properties (variability, wind, blueness) apply
 * regardless of actual evolutionary phase.
 */

Boolean star_is_LBV(const struct stardata_t * const stardata,
                    const struct star_t * const star)
{

    return
        Boolean_(star->luminosity > stardata->preferences->wind_LBV_luminosity_lower_limit
                 &&
                 1e-5*star->radius*sqrt(star->luminosity) > 1.0);
}
