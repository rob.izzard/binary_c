#include "../binary_c.h"
No_empty_translation_unit_warning;

double baryonic_mass_offset(const struct star_t * const star)
{
    /*
     * The (positive or zero) mass offset between a star's
     * baryonic mass and its gravitational mass.
     *
     * The gravitational mass is less because binding energy
     * is subtracted from it.
     */
    return star->baryonic_mass - star->mass;
}
