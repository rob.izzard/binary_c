#include "../binary_c.h"
No_empty_translation_unit_warning;


double Pure_function dynamical_timescale(const struct star_t * const star)
{
    /*
     * Return the dynamical timescale of a star in years
     */
    return 5.05e-05 * sqrt( Pow3(star->radius) / star->mass );
}
