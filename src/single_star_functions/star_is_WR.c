#include "../binary_c.h"
No_empty_translation_unit_warning;


/*
 * Hurley+2002 definition of a Wolf-Rayet star.
 * Return TRUE if the star is a Wolf-Rayet star, otherwise false.
 */

Boolean star_is_WR(const struct star_t * const star)
{
    if(star->stellar_type > MAIN_SEQUENCE &&
       star->stellar_type < HeWD)
    {
        if(WR_mu(star) < 1.0)
        {
            return TRUE;
        }
    }
    return FALSE;
}
