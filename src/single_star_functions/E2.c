#include "../binary_c.h"
No_empty_translation_unit_warning;


double E2(struct stardata_t * Restrict const stardata,
          const struct star_t * Restrict const star)
{
    /*
     * Return the E2 structural parameter, used in tidal calculations
     */
    double E2;
    if(stardata->preferences->E2_prescription==E2_HURLEY_2002)
    {
        // E2 fit to Zahn's models from Hurley et al 2002
        E2 = 1.592E-09*pow(star->mass,2.84);
    }
    else if(stardata->preferences->E2_prescription==E2_IZZARD)
    {
        /* newer fit from Siess, Izzard et al 2013 */
        if(star->stellar_type<HERTZSPRUNG_GAP)
        {
            const double fburn = 1.0;
            const   double x = Is_zero(star->tms) ? 0.0 : (star->age / star->tms);
            Dprint("x = %g / %g = %g\n",star->age,star->tms,x);
            if(x<fburn)
            {
                /* log mass and Z */
                double logm=log10(star->mass);
                double logz=log10(stardata->common.metallicity);

                /* fits for am and E20 */
                double am = 0.15*sin(3.2*logm)+0.31*logm;
                double E20=
                    -1.23250e+01+
                    1.04550e+01*logm+
                    -4.15420e-01*logz+
                    -7.18650e+00*logm*logm+
                    1.97940e+00*logm*logm*logm;
                E20=exp10(E20);

                /* calc log(E2/E20) */
                E2 = -pow(x+am,4.0)*pow(Max(1,x/0.95),30.0);

                /* hence E2 */
                E2 = E20 * exp10(E2);
            }
            else
            {
                /* no conv core */
                E2=0.0;
            }
        }
        else
        {
            E2=0.0;
        }
    }
#ifdef MINT
    else if(stardata->preferences->E2_prescription==E2_MINT)
    {
        /*
         * Use E2 directly from the MINT tables
         */
        E2 = star->E2;
    }
#endif
    else
    {
        E2=0.0;
        Exit_binary_c(BINARY_C_SETUP_UNKNOWN_ARGUMENT,
                      "E2 prescription unknown\n");
    }

    return E2;
}
