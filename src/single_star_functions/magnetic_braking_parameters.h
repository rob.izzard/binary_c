#pragma once
#ifndef MAGNETIC_BRAKING_H
#define MAGNETIC_BRAKING_H

#include "magnetic_braking_parameters.def"

#undef X
#define X(CODE) MAGNETIC_BRAKING_ALGORITHM_##CODE,
enum { MAGNETIC_BRAKING_ALGORITHM_LIST };
#undef X



#endif // MAGNETIC_BRAKING_H
