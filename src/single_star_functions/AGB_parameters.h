#pragma once
#ifndef AGB_PARAMETERS_H
#define AGB_PARAMETERS_H

#include "AGB_parameters.def"

#undef X
#define X(CODE) AGB_CORE_##CODE,
enum { AGB_CORE_ALGORITHM_LIST };
#undef X

#define X(CODE) AGB_RADIUS_##CODE,
enum { AGB_RADIUS_ALGORITHM_LIST };
#undef X

#define X(CODE) AGB_LUMINOSITY_##CODE,
enum { AGB_LUMINOSITY_ALGORITHM_LIST };
#undef X

#define X(CODE) AGB_THIRD_DREDGE_UP_##CODE,
enum { AGB_THIRD_DREDGE_UP_ALGORITHM_LIST };
#undef X

#define X(CODE) TPAGB_LUMINOSITY_##CODE,
enum { TPAGB_LUMINOSITY_ALGORITHM_LIST };
#undef X





#endif // AGB_PARAMETERS_H
