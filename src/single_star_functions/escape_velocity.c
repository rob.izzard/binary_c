#include "../binary_c.h"
No_empty_translation_unit_warning;

double escape_velocity(const struct star_t * Restrict const star)
{
    /*
     * Surface escape velocity in cm/s
     */
    return sqrt(2.0 *
                GRAVITATIONAL_CONSTANT *
                M_SUN * star->mass / (R_SUN * star->effective_radius));
}
