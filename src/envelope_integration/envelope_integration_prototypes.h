#ifndef ENVELOPE_INTEGRATION_PROTOTYPES_H
#define ENVELOPE_INTEGRATION_PROTOTYPES_H

int calculate_stellar_envelope_structure(struct stardata_t * stardata,
                                         struct star_t * star,
                                         struct envelope_t * envelope,
                                         const double depth,
                                         const double mass_resolution);

void envelope_properties(struct stardata_t * stardata,
                         struct star_t * star,
                         struct envelope_t * envelope);

#endif // ENVELOPE_INTEGRATION_PROTOTYPES_H

