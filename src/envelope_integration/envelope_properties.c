#include "../binary_c.h"
No_empty_translation_unit_warning;


void envelope_properties(struct stardata_t * stardata,
                         struct star_t * star,
                         struct envelope_t * envelope)
{
    /*
     * Calculate secondary properties on a
     * stellar envelope which has previous been calculated
     * and is in the envelope struct.
     *
     * NB shells are surface (0) to inner point (nshells-1)
     */
    int i;

    printf("envelope : Mstar = %g, nshells = %d\n",
           star->mass,
           envelope->nshells);

#ifdef NUCSYN
    Abundance * X = New_isotope_array_from(star->Xenv);
#endif

    /* ionisation energy per gram */
    double chi =
#ifdef NUCSYN
        nucsyn_recombination_energy_per_gram(stardata,X)
#else
        1.3 * RYDBERG_ENERGY / M_PROTON
#endif
        ;
    /*
     * Menv and binding energy
     */
    double E1 = 0.0 , E2 = 0.0 , E3 = 0.0;
    double Menv = 0.0;
    for(i=0; i<envelope->nshells; i++)
    {
        struct envelope_shell_t * shell = & envelope->shells[i];
        Menv += shell->dm;
        E1 += shell->m * shell->dm / shell->r;
        E3 += shell->dm * shell->pressure / shell->density;
    }
    E1 = -E1;
    E2 = chi * Menv * M_SUN;

    /* unit conversions */
    E1 *= GRAVITATIONAL_CONSTANT * M_SUN * M_SUN / R_SUN;
    E3 *= M_SUN;

    /* multipliers  */
    E1 *= 1.0;
    E2 *= stardata->preferences->lambda_ionisation[stardata->model.comenv_count];
    E3 *= stardata->preferences->lambda_enthalpy[stardata->model.comenv_count];

    /* save in envelope struct */
    envelope->E1 = E1;
    envelope->E2 = E2;
    envelope->E3 = E3;
    envelope->E_bind = E1 + E2 + E3;

    /*
     * Hence lambda binding parameter
     */
    envelope->lambda_bind = - GRAVITATIONAL_CONSTANT * Menv * M_SUN *
        star->mass * M_SUN / (star->radius * R_SUN * envelope->E_bind);

    /*
     * Calculate moment of inertia
     */
    double I = 0.0;
    for(i=0; i<envelope->nshells; i++)
    {
        struct envelope_shell_t * shell = & envelope->shells[i];
        I += shell->dm * Pow2(shell->r);
    }
    I *= 2.0 * M_SUN * Pow2(R_SUN);
    envelope->moment_of_inertia = I;


#ifdef NUCSYN
    Safe_free(X);
#endif
}
