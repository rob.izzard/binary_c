#pragma once
#ifndef BINARY_C_CODESTATS_H
#define BINARY_C_CODESTATS_H

/*
 * The binary_c stellar population nucleosynthesis framework.
 *
 * Copyright Robert Izzard 2000-2025
 *
 * Contact rob.izzard@gmail.com
 *
 * Code repository:
 * https://gitlab.com/binary_c
 *
 * Documentation is in doc/
 * Code is in src/
 *
 * To build, run
 *
 * ---
 *
 * meson setup builddir
 * cd builddir
 * ninja binary_c_install
 *
 * ---
 *
 * Please see the files README, LICENCE and CHANGES,
 * and the doc/ directory for documentation which is mirrored
 * at https://binary_c.gitlab.io
 *
 **********************
 *
 * Purpose:
 *
 * This header file contains the codestats_entry_t and codestats_t
 * structure definitions. These are used to count things (usually
 * memory allocations and function calls) and acquire statistics
 * for optimization purposes.
 *
 **********************
 */

#include "binary_c_code_options.h"

#ifdef CODESTATS
struct codestats_entry_t {
    char * file;
    unsigned int line;
    Codestats_counter count;
};

struct codestats_t {
    Codestats_counter counters[CODESTAT_NUMBER];
    struct codestats_entry_t * entries[CODESTAT_NUMBER];
    unsigned int nentries[CODESTAT_NUMBER];
};

extern struct codestats_t codestats;

#endif//CODESTATS

#endif // BINARY_C_CODESTATS_H
