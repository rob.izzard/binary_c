#pragma once
#ifndef BINARY_STRUCTURES_H
#define BINARY_STRUCTURES_H

/*
 * The binary_c stellar population nucleosynthesis framework.
 *
 * Copyright Robert Izzard 2000-2025
 *
 * Contact rob.izzard@gmail.com
 *
 * Code repository:
 * https://gitlab.com/binary_c
 *
 * Documentation is in doc/
 * Code is in src/
 *
 * To build, run
 *
 * ---
 *
 * meson setup builddir
 * cd builddir
 * ninja binary_c_install
 *
 * ---
 *
 * Please see the files README, LICENCE and CHANGES,
 * and the doc/ directory for documentation which is mirrored
 * at https://binary_c.gitlab.io
 *
 **********************
 *
 * Purpose:
 *
 * This header file contains the definitions of
 * the memory structures used by binary_c/nucsyn,
 * so is where variables and their types are defined.
 *
 *
 * Please try to order structures efficently:
 *
 * 1) pack long items first (e.g. doubles and pointers),
 * 2) pack short items at the end (e.g. ints and Booleans).
 *
 */
#include <stdio.h>
#include <setjmp.h>
#include "binary_c_code_options.h"
#include "binary_c_parameters.h"
#include "binary_c_prescriptions.h"
#include "binary_c_macros.h"
#include "binary_c_types.h"
#include "binary_c_cdict.h"
#ifdef CODESTATS
#include "binary_c_codestats.h"
#endif //CODESTATS
#include "memory/memory_function_macros.h"
#include "evolution/evolution_structures.h"

#ifdef NUCSYN
/* Grab the settings for the nucleosynthesis */
#include "nucsyn/nucsyn_functions.h"
#include "nucsyn/nucsyn_parameters.h"
#include "nucsyn/nucsyn_macros.h"
#include "nucsyn/nucsyn_isotopes.h"
#endif /* NUCSYN */
#include "ensemble/ensemble.h"

#ifdef DISCS
#include "disc/disc_parameters.h"
#include "disc/disc_macros.h"
#include "disc/disc_function_macros.h"
#include "disc/disc_power_laws.h"
#include "disc/disc_constraints.h"
#endif//DISCS
#include "timestep/timestep.h"
#include <gsl/gsl_linalg.h>

#ifndef __CDICT_MAP__
#define Cdict_var /* do nothing */
#define Cdict_var_nest /* do nothing */
#else
#define Cdict_var __is_Cdict_var__
#define Cdict_var __is_Cdict_var_nest__
#endif // __CDICT_MAP__

#ifdef EVENT_BASED_LOGGING
#include "event_based_logging/event_based_logging.h"
#endif // EVENT_BASED_LOGGING

/*
 * Data table plus
 */
struct data_table_t
{
    struct data_table_analysis_t * analysis;
    struct data_table_defer_instructions_t * defer;
    double * data;
    char * label;
    void * metadata;
    void (*metadata_free_function)(void*);
    size_t nlines;
    size_t nparam;
    size_t ndata;
};

struct data_table_analysis_t
{
    double ** values;
    size_t * nvalues;
    size_t * data_line_offset_bytes;
    size_t * skip;
    size_t data_start_offset_bytes;
    size_t nparameters;
};


#ifdef MINT

struct mint_table_metadata_t
{
    struct binary_c_stream_t * stream;
    struct mint_header_t * header;
    FILE * data_cache;

    char ** missing_from_file_strings;
    char ** missing_MINT_types_strings;
    char ** MINT_parameter_names;
    char ** MINT_datatype_names;

    double * table;
    char * filename;
    char * data_cache_filename;
    char * header_string;
    int * parameter_actions;
    int * data_actions;
    int *  file_to_MINT_map;
    int *  MINT_to_file_map;
    unsigned int * data_map;
    Boolean * data_available;

    size_t n_MINT_parameter_names;
    size_t n_MINT_datatype_names;
    size_t missing_from_file_strings_n;
    size_t missing_MINT_types_strings_n;
    size_t data_available_size;
    size_t lower_line_number;
    size_t upper_line_number;
    int file_to_MINT_map_size;
    int MINT_to_file_map_size;

    size_t data_map_size;
    size_t nl;
    size_t nl_full_table;
    unsigned int nparam;
    unsigned int ndata;
    unsigned int nscalars;
    unsigned int nperline;
    unsigned int table_id;
};

#endif // MINT

struct data_table_defer_instructions_t
{
    size_t lower_line_number;
    size_t upper_line_number;
};

struct string_array_t
{
    char ** strings;
    size_t * lengths;
    ssize_t n;
    size_t nalloc;
    Boolean one_alloc;
};
struct double_array_t
{
    double * doubles;
    ssize_t n;
};
struct int_array_t
{
    int * ints;
    ssize_t n;
};
struct long_int_array_t
{
    long int * long_ints;
    ssize_t n;
};
struct unsigned_int_array_t
{
    unsigned int * unsigned_ints;
    ssize_t n;
};
struct Boolean_array_t
{
    Boolean * Booleans;
    ssize_t n;
};

#ifdef USE_MERSENNE_TWISTER
#include "maths/mersenne_twister_macros.h"
/*
 * Mersenne random number generator data
 */
struct mersenne_twister_data_t
{
    /* The array for the state vector */
    unsigned long long int mt[MERSENNE_TWISTER_NN];
    int mti;
};

#endif // USE_MERSENNE_TWISTER

/*
 * Generic random buffer type
 */
struct binary_c_random_buffer_t
{
    Random_buffer buffer;
    long int count;
    Boolean set;
};

struct phase_t
{
    double mass; /* gravitational mass */
    double maximum_mass;
    double radius;
    double maximum_radius;
    double luminosity;
    double maximum_luminosity;
    double time;
    double drdm;

    double phase_time;
    double time_weighted_mass;
    double time_weighted_radius;
    double time_weighted_luminosity;
};


#ifdef STELLAR_POPULATIONS_ENSEMBLE

struct ensemble_cdict_metadata_t
{
    char * label;
    Ensemble_type type;
};

#endif // STELLAR_POPULATIONS_ENSEMBLE

struct orbit_t
{
    double period; /* Orbital period in years */
    double angular_frequency; /* Orbital angular frequency, y^-1 */
    double angular_momentum; /* Orbital angular momentum, code units */
    double eccentricity; /* Orbital eccentricity */
    double separation; /* Orbital separtaion in Rsun */
    double tcirc; // circularisation timescale (years)
    double pseudo_sync_angular_frequency; /* pseudo-synchronization frequency */
    double inclination;
    Boolean couple_tides;
};


struct pulsator_ellipse_t
{
    double logTeff;
    double logL;
    double a;
    double b;
    double p;
    char * name;
};


#ifdef GAIAHRD
struct gaiaHRD_chunk_t
{
    double Teff_binned;
    double L_binned;
    double gaia_magnitude_binned;
    double gaia_colour_binned;
    double dtp;
    Star_number primary;
    Boolean do_log;
    Boolean is_single;
};
#endif // GAIAHRD

/*
 * stream type for reading
 */
struct binary_c_stream_t
{
    FILE * fp;
    char * name;
    int format;
    Stream_type type;
};

/*
 * custom file struct: use this with
 * binary_c_fopen, binary_c_fclose
 * binary_c_fprintf and binary_c_fflush
 *
 * This enables you to specify your own
 * maximum file lengths and monitor file size.
 */
struct binary_c_file_t
{
    char path[STRING_LENGTH];
#ifdef MEMOIZE
    struct memoize_hash_t * memo;
#endif//MEMOIZE
    FILE * fp;
    struct binary_c_stream_t * stream;
    size_t size; // bytes
    size_t maxsize; // bytes
    int format; // format: see file_macros.h
};


/*
 * Generic 3D vector
 */
union vector_t
{
    struct
    {
        double x;
        double y;
        double z;
    } Packed_struct;
    double component[3];
};


#if defined MEMOIZE && !defined __HAVE_LIBMEMOIZE__
#include "libmemoize/memoize.h"
#endif // MEMOIZE && !__HAVE_LIBMEMOIZE__

/* data structure to handle fixed timestepping */
struct binary_c_fixed_timestep_t
{
    /*
     * Note: times are linear if log is FALSE, otherwise they are log10(times)
     */
    double (*nextfunc)(struct stardata_t * const stardata); /* function to set next */
    double previous_test; /* previous time the trigger was checked */
    double previous_trigger; /* previous time it triggered */
    double step; /* the fixed timestep */
    double next; /* next time */
    double begin; /* start time of this being active */
    double end;  /* end time of this being active */
    Boolean logarithmic; /* if TRUE work in log time, if FALSE (default) linear time */
    Boolean enabled; /* disabled by default (when enable == 0 == FALSE) */
    Boolean final; /* if TRUE always trigger on final timestep */
};

#ifdef NUCSYN
struct nuclear_network_range_t
{
    Isotope min;
    Isotope max;
};
#endif // NUCSYN

struct binary_c_mutex_t
{
    Boolean locked;
    pthread_mutex_t mutex;
};

/*
 * store_t is for data that cannot be altered by threads
 *
 * note that this struct is set up by binary_c when run,
 * and is NOT required to be copied around, hence the data
 * in it is always assigned on the heap via Calloc, Malloc or
 * Realloc.
 *
 * Data in here may be stored between threads, so it is
 * up to you to ensure that only one thread actually sets
 * the data in it.
 */
struct store_t
{
    struct argstore_t * argstore;
    struct cdict_t * argcdict;
#ifdef __HAVE_LIBPTHREAD__
    struct binary_c_mutex_t pthread_mutexes[BINARY_C_MUTEX_NUMBER];
#endif //__HAVE_LIBPTHREAD__
#ifdef NUCSYN
    Alignas Nuclear_mass * mnuc; /* Nuclear masses */
    Alignas Nuclear_mass * imnuc; /* 1.0/ Nuclear masses */
    Alignas Nuclear_mass * mnuc_amu; /* Nuclear masses divided by AMU */
    Alignas Nuclear_mass * imnuc_amu; /* 1.0 / (Nuclear masses divided by AMU) */
    Alignas double * molweight; /* factor used in molecular weight calculation */
    Alignas double * ZonA; /* factor used in molecular weight calculation */
    Alignas Atomic_number * atomic_number; /* Atomic numbers */
    Alignas Nucleon_number * nucleon_number; /* Nucleon numbers (mass numbers) */
    Alignas unsigned int * nisotopes;
    Alignas double * ionised_molecular_weight_multiplier; /* imnuc_amu * (1+atomic_number) */
    struct hsearch_data * atomic_number_hash;
    struct element_info_t * element_info;
    Isotope ** icache;
    char ** isotope_strings;/* strings describing each element */
    struct nuclear_network_range_t nuclear_network_range[NUCSYN_NETWORK_NUMBER];
    nucsyn_burnfunc nucsyn_network_function[NUCSYN_SOLVER_NUMBER][NUCSYN_NETWORK_NUMBER];
    nucsyn_checkfunc nucsyn_network_checkfunction[NUCSYN_NETWORK_NUMBER];
#endif // NUCSYN
    struct preferences_t * fallback_preferences;
    struct binary_c_file_t * system_list;

    /*
     * The collision matrix - i.e. what to do when the binary
     * star collides - see instar.c for details and setup.
     * NB This is a square array of width COLLISION_MATRIX_SIZE.
     */
    Stellar_type *collision_matrix[COLLISION_MATRIX_SIZE];

    /* log file labels */
    const char *label[NLOG_LABEL];
    char * upchar;
    char * downchar;
    char * nochangechar;
    char ** ANSI_colours_table;
    char ** no_colours_table;
    char ** colours;

    /* spectral type strings */
    char *** static_spectral_type_strings;

    /*
     * Data tables
     */
    struct data_table_t * jaschek_jaschek_dwarf;
    struct data_table_t * jaschek_jaschek_giant;
    struct data_table_t * jaschek_jaschek_supergiant;
    struct data_table_t * miller_bertolami;
    struct data_table_t * miller_bertolami_coeffs_L;
    struct data_table_t * miller_bertolami_coeffs_R;
    struct data_table_t * Temmink2022_RLOF;

#ifdef MAIN_SEQUENCE_STRIP
    struct data_table_t * MS_strip;
#endif // MAIN_SEQUENCE_STRIP
#ifdef FIRST_DREDGE_UP_HOLLY
    struct data_table_t * Holly_1DUP_table;
#endif // FIRST_DREDGE_UP_HOLLY
#ifdef FIRST_DREDGE_UP_EVERT
    struct data_table_t * Evert_1DUP_table;
#endif //FIRST_DREDGE_UP_EVERT
#ifdef USE_2012_MAIN_SEQUENCE_LIFETIMES_TABLE
    struct data_table_t * massive_MS_lifetimes;
#endif // USE_2012_MAIN_SEQUENCE_LIFETIMES_TABLE
#ifdef STELLAR_COLOURS
    struct data_table_t * Eldridge2012_colours;
#endif // STELLAR_COLOURS
    struct data_table_t * carrasco2014_table3;
    struct data_table_t * carrasco2014_table4;
    struct data_table_t * carrasco2014_table5;
    struct data_table_t * carrasco2014_cooling_table3;
    struct data_table_t * carrasco2014_cooling_table4;
    struct data_table_t * carrasco2014_cooling_table5;

#ifdef NUCSYN
#ifdef NUCSYN_SIGMAV_PRE_INTERPOLATE
    struct data_table_t * sigmav;
#endif //NUCSYN_SIGMAV_PRE_INTERPOLATE
#ifdef NUCSYN_STRIP_AND_MIX
    struct data_table_t * TAMS;
#endif//NUCSYN_STRIP_AND_MIX
#ifdef NUCSYN_S_PROCESS
    struct data_table_t * s_process_Gallino;
#endif//NUCSYN_S_PROCESS
#ifdef NUCSYN_NOVAE
    struct data_table_t * novae_JH98_CO;
    struct data_table_t * novae_JH98_ONe;
    struct data_table_t * novae_Jose2022_ONe;
#endif //NUCSYN_NOVAE
    double * condensation_temperatures;
#endif //NUCSYN

#ifdef COMENV_WANG2016
    double *tableh1, *tableh2, *tableh3,
           *tableb1, *tableb2, *tableb3,
           *tableg1, *tableg2, *tableg3;
    struct data_table_t * comenv_maxR_table;
#endif//COMENV_WANG2016
#ifdef COMENV_POLYTROPES
    struct data_table_t * comenv_polytrope_2d;
    struct data_table_t * comenv_polytrope_3d;
    struct data_table_t * comenv_polytrope_rsms;
#endif//COMENV_POLYTROPES
#ifdef STELLAR_COLOURS
    double * zgr;
    double * tgr;
    double * ggr;
    double ****ubv;
#endif//STELLAR_COLOURS

#ifdef USE_BSE_TIMESCALES_H
    struct data_table_t * BSE_TIMESCALES_H;
#endif//USE_BSE_TIMESCALES_H
    struct data_table_t * Karakas_ncal;
    struct data_table_t * Karakas2002_lumfunc;
    struct data_table_t * Karakas2002_radius;
#ifdef OPACITY_ALGORITHMS
#ifdef OPACITY_ENABLE_ALGORITHM_PACZYNSKI
    double kap_paczynski[52][32];
    struct data_table_t * opacity_STARS;
#endif//OPACITY_ENABLE_ALGORITHM_PACZYNSKI
#ifdef OPACITY_ENABLE_ALGORITHM_FERGUSON_OPAL
    struct data_table_t * opacity_ferguson_opal;
#endif//
#endif // OPACITY_ALGORITHMS
#ifdef MINT
    Boolean MINT_loaded;
    struct string_array_t * MINT_dirs;
    struct data_table_t * MINT_tables[MINT_TABLE_NUMBER];
    //struct mint_table_metadata_t * MINT_metadata[MINT_TABLE_NUMBER];
    Abundance MINT_initial_coordinate[MINT_TABLE_NUMBER];
#endif//MINT
    int * MINT_generic_map[NUMBER_OF_STELLAR_TYPES];


    struct pulsator_ellipse_t * pulsator_ellipses;
    size_t num_pulsator_ellipses;
    unsigned int system_list_counter;

    Boolean built;
    Boolean debug_stopping;
} Aligned;

struct unit_t
{
    char * string;
    char * longstring;
    double unit_value;
    double code_unit_value;
    int code_unit_type;
};

struct unit_alpha_t
{
    struct unit_t * unit;
    size_t nunits;
    char alpha;
};

#ifdef MEMORY_USE
struct binary_c_memuse_t
{
#undef X
#define X(TYPE) RAM TYPE;
    __RAM_TYPE_LIST__
#undef X
};
#endif //MEMORY_USE

/*
 * The tmpstore contains chunks of memory that
 * can be deleted at any time (except during stellar evolution),
 * but are useful to allocate just once as they are used a lot.
 *
 * The tmpstore is not shared between threads.
 *
 * The tmpstore is emptied after a stellar system finishes
 * its evolution.
 *
 * Subroutines which use tmpstore should check if the
 * pointer they are looking at is NULL, and if so they
 * should allocate memory.
 *
 * The free_tmpstore function frees memory and zeros the
 * tmpstore structure. This is only called outside of
 * the evolution loop, when a system is not being evolved.
 * Thus the tmpstore will not disappear during stellar evolution.
 *
 * You should not free the tmpstore yourself, use free_tmpstore.
 *
 * The tmpstore should be considered as non-persistent storage,
 * so any data that is not required outside a subroutine should
 * only be allocated and freed inside that subroutine.
 *
 *
 */
struct tmpstore_t
{
    struct preferences_t * default_preferences;
#ifdef GAIAHRD
    struct gaiaHRD_chunk_t * gaiaHRD;
#endif
    struct unit_alpha_t * unit_alphas;
    size_t n_unit_alphas;
#ifdef KICK_CDF_TABLE
    struct data_table_t * cdf_table;
#endif//KICK_CDF_TABLE
#ifdef DISCS
    struct binary_c_file_t * disc_logfile;
    struct binary_c_file_t * disc_logfile2d;
#endif//DISCS
    struct difflogstack_t * logstack;
    struct star_t * stellar_structure_newstar;
    struct star_t * stellar_evolution_newstar;
    struct lsoda_t * lsoda;
#ifdef NUCSYN
    struct cvode_context_t * cvode[NUCSYN_NETWORK_NUMBER];
#endif//NUCSYN
    char * file_log_prevstring;
    char * buffer_string;
#ifdef STELLAR_TIMESCALES_CACHE
    double Aligned stellar_timescales_cache_mass[Aligned_size(STELLAR_TIMESCALES_CACHE_SIZE)];
    double Aligned stellar_timescales_cache_mt[Aligned_size(STELLAR_TIMESCALES_CACHE_SIZE)];
    double Aligned stellar_timescales_cache_mc[Aligned_size(STELLAR_TIMESCALES_CACHE_SIZE)];
    double Aligned stellar_timescales_cache_GB[Aligned_size(STELLAR_TIMESCALES_CACHE_SIZE)][Aligned_size(GB_ARRAY_SIZE)];
    double Aligned stellar_timescales_cache_luminosities[Aligned_size(STELLAR_TIMESCALES_CACHE_SIZE)][Aligned_size(LUMS_ARRAY_SIZE)];
    double Aligned stellar_timescales_cache_timescales[Aligned_size(STELLAR_TIMESCALES_CACHE_SIZE)][Aligned_size(TSCLS_ARRAY_SIZE)];
    double Aligned stellar_timescales_cache_tm[Aligned_size(STELLAR_TIMESCALES_CACHE_SIZE)];
    double Aligned stellar_timescales_cache_tn[Aligned_size(STELLAR_TIMESCALES_CACHE_SIZE)];
    Stellar_type Aligned stellar_timescales_cache_stellar_type[Aligned_size(STELLAR_TIMESCALES_CACHE_SIZE)];
#endif
    struct data_table_t * tidal_multipliers;
    struct double_array_t * target_times;
    struct double_array_t * evolutionary_command_times;

    char Aligned * raw_buffer;
    char Aligned * line_buffer;
    char Aligned * error_buffer;
#ifdef BATCHMODE
    char Aligned * batchstring;
    char Aligned * batchstring2;
    char Aligned ** batchargs;
#endif // BATCHMODE
#ifdef YBC
    struct string_array_t * YBC_instruments_list;
    Boolean * YBC_use_instrument;
#endif // YBC
#ifdef WTTS_LOG
    FILE *fp_sys;
    FILE *fp_star[NUMBER_OF_STARS];
#endif // WTTS_LOG
    char * random_system_argstring;
#ifdef STELLAR_POPULATIONS_ENSEMBLE
    Ensemble_type * ensemble_type;
    struct cdict_entry_t * ensemble_type_nd;
#endif // STELLAR_POPULATIONS_ENSEMBLE
    char * c_log_prefix;

#ifdef NUCSYN

#ifdef NUCSYN_STRIP_AND_MIX
    FILE * strip_and_mix_fp[NUMBER_OF_STARS];
#endif // NUCSYN_STRIP_AND_MIX

    /*
     * Memory used in nucsyn_update_abundances
     */
    Alignas Abundance *Xacc[NUMBER_OF_STARS];
    Alignas Abundance *Xenv[NUMBER_OF_STARS];
    Alignas Aligned Abundance *nXacc[NUMBER_OF_STARS];
    Alignas Abundance *nXenv[NUMBER_OF_STARS];
    Alignas Abundance *Xwind_gain;
#ifdef NUCSYN_NOVAE
    Alignas Abundance *Xnovae;
#endif // NOVAE


    double ** network_jacobians[NUCSYN_NETWORK_NUMBER];
    unsigned int network_jacobians_n[NUCSYN_NETWORK_NUMBER];

#endif // NUCSYN
    unsigned int kaps_rentrop_GSL_n;
    gsl_matrix * kaps_rentrop_GSL_m;
    gsl_permutation * kaps_rentrop_GSL_p;

    unsigned int kaps_rentrop_LU_backsub_n;
    gsl_vector * kaps_rentrop_LU_backsub_b;
    gsl_vector * kaps_rentrop_LU_backsub_x;

    double * unresolved_magnitudes;
    double * stellar_magnitudes[NUMBER_OF_STARS];
    size_t raw_buffer_size;
    size_t raw_buffer_alloced;
#ifdef DISCS
    int disc_logfilecount;
    int disc_logfilecount2d;
#endif // DISCS
#ifdef MEMORY_USE
    FILE * proc_self_status;
    struct binary_c_memuse_t * memuse;
#endif // MEMORY_USE

#ifdef LOG_SUPERNOVAE
    Boolean sn_header_printed;
#endif // LOG_SUPERNOVAE
    Boolean error_buffer_set;
} Aligned;

/*
 * Initial system struct
 */
struct zero_age_system_t
{
    double age[NSTARS_CMDLINE];
    double mass[NSTARS_CMDLINE] Cdict_var; /* gravitational mass */
    double core_mass[NSTARS_CMDLINE] Cdict_var;
    double vrot[NSTARS_CMDLINE] Cdict_var;
    double vrot_multiplier[NSTARS_CMDLINE] Cdict_var;
    double Prot[NSTARS_CMDLINE] Cdict_var;
    double fKerr[NSTARS_CMDLINE] Cdict_var;
    double inclination[NSTARS_CMDLINE] Cdict_var; // stellar inclination
    double magnetic_field[NSTARS_CMDLINE] Cdict_var;
    double magnetic_field_inclination[NSTARS_CMDLINE] Cdict_var;
    double separation[NSTARS_CMDLINE];
    double orbital_period[NSTARS_CMDLINE]; // days
    double eccentricity[NSTARS_CMDLINE];
    double orbital_inclination[NSTARS_CMDLINE];
    double orbital_phase[NSTARS_CMDLINE];
    double argument_of_periastron[NSTARS_CMDLINE];
    Stellar_type stellar_type[NSTARS_CMDLINE];

#ifdef ORBITING_OBJECTS
    char ** orbiting_object;
    size_t n_orbiting_objects;
#endif // ORBITING_OBJECTS

    /*
     * hierarchy defined as in
     * http://articles.adsabs.harvard.edu/pdf/1968QJRAS...9..388E
     */
    Hierarchy hierarchy;
    Multiplicity multiplicity;
#ifdef NUCSYN
    Abundance Aligned XZAMS[ISOTOPE_ARRAY_SIZE]; // ZAMS abundances
    Abundance Aligned Xsolar[ISOTOPE_ARRAY_SIZE]; // Solar (Z=0.02) abundances
#endif // NUCSYN
} Aligned;

/*
 * The preferences struct, deals with physics options, output files etc.
 */
#include "timestep/timestep.h"
struct preferences_t
{
    int (*stellar_structure_hook)(const Caller_id caller_id,
                                  ...);
    void (*custom_supernova_kick_hook)(struct stardata_t * const stardata,
                                       struct stardata_t * const pre_explosion_stardata,
                                       struct star_t * const star,
                                       struct star_t * const pre_explosion_star);
    void (*extra_stellar_evolution_hook)(struct stardata_t * stardata,
                                         const Evolution_system_type system_type);

    /*
     * Deprecated functions hook : these are here for backwards
     * compatibility with old code
     */
    /*
    void (*custom_output_function)(struct stardata_t * stardata) Deprecated_function;
    void (*catch_events_function)(struct stardata_t * stardata) Deprecated_function;
    void (*extra_calculate_derivatives_function)(struct stardata_t * stardata) Deprecated_function;
    void (*extra_apply_derivatives_function)(struct stardata_t * stardata) Deprecated_function;
    void (*extra_update_binary_star_variables_function)(struct stardata_t * stardata) Deprecated_function;
    void (*pre_time_evolution_function)(struct stardata_t * stardata) Deprecated_function;
    void (*post_time_evolution_function)(struct stardata_t * stardata) Deprecated_function;
    */
#undef X
#define X(F,...) void ( * F##_function )(struct stardata_t * stardata) Deprecated_function;
    FUNCTION_HOOK_LIST
#undef X

    /*
     * Function hooks in an array
     */
    void (*function_hooks[NUMBER_OF_FUNCTION_HOOKS])(struct stardata_t * stardata);

#ifdef MINT
    char MINT_dir[STRING_LENGTH];
    char MINT_data_tables[STRING_LENGTH];
    char MINT_load_state_file[STRING_LENGTH];
    char MINT_save_state_file[STRING_LENGTH];
#endif // MINT

    char system_list_path[STRING_LENGTH];
    char tides_variable_filename[STRING_LENGTH];
    char JSON_commands_filename[STRING_LENGTH];

#ifdef EVOLUTION_SPLITTING
    struct splitinfo_t * splitinfo[EVOLUTION_SPLITTING_HARD_MAX_DEPTH];
#endif // EVOLUTION_SPLITTING

    struct cdict_t * commands;
    struct zero_age_system_t zero_age;

    /* double parameters */
    double initial_probability;
    Abundance metallicity;
    Abundance effective_metallicity;
    Abundance nucsyn_metallicity;
#ifdef MINT
    Abundance MINT_metallicity;
#endif // MINT
    double max_evolution_time;

    double timestep_epsilon;
    double timestep_multipliers[DT_LIMIT_NUMBER];
    /* critical mass ratios per stellar type */
    double qcrit[NUMBER_OF_STELLAR_TYPES];
    double qcrit_degenerate[NUMBER_OF_STELLAR_TYPES];
    double sn_kick_dispersion[SN_NUM_TYPES];
    double sn_kick_companion[SN_NUM_TYPES];

    double ensemble_dt;
    double ensemble_starttime;
    double ensemble_logdt;
    double ensemble_startlogtime;

#ifdef NUCSYN
    Abundance the_initial_abundances[ISOTOPE_ARRAY_SIZE];
    Abundance initial_abundance_multiplier[ISOTOPE_ARRAY_SIZE];

#ifdef NUCSYN_THIRD_DREDGE_UP_MULTIPLIERS
    double third_dup_multiplier[ISOTOPE_ARRAY_SIZE];
#endif // NUCSYN_THIRD_DREDGE_UP_MULTIPLIERS

    // reaction rate multipliers
    double reaction_rate_multipliers[SIGMAV_SIZE];

    double hbbtfac;// HBB temperature factor
#ifdef NUCSYN_S_PROCESS
    double c13_eff, mc13_pocket_multiplier;
#endif // NUCSYN_S_PROCESS

#ifdef USE_TABULAR_INTERSHELL_ABUNDANCES_KARAKAS_2012
    double pmz_mass; //CAB
#endif // USE_TABULAR_INTERSHELL_ABUNDANCES_KARAKAS_2012

    /* EMP logging, see ensemble_log.c */
    double CEMP_cfe_minimum;
    double NEMP_nfe_minimum;
    double EMP_feh_maximum;
    double EMP_minimum_age;
    double EMP_logg_maximum;
    double core_collapse_rprocess_mass;

#ifdef NUCSYN_GCE_OUTFLOW_CHECKS
    double escape_velocity; // galactic escape velocity
    double escape_fraction; // for stuff over escape velocity, what fraction
#endif // NUCSYN_GCE_OUTFLOW_CHECKS

#ifdef CN_THICK_DISC
    double thick_disc_start_age, thick_disc_end_age;
    double thick_disc_logg_min, thick_disc_logg_max;
#endif // CN_THICK_DISC


#ifdef LITHIUM_TABLES
    double lithium_hbb_multiplier;
    double lithium_GB_post_1DUP;
    double lithium_GB_post_Heflash;
#endif // LITHIUM_TABLES

#ifdef NUCSYN_ANGELOU_LITHIUM
    /* start times */
    double angelou_lithium_MS_time;
    double angelou_lithium_LMMS_time;
    double angelou_lithium_HG_time;
    double angelou_lithium_GB_time;
    double angelou_lithium_CHeB_time;
    double angelou_lithium_EAGB_time;
    double angelou_lithium_TPAGB_time;

    /* or use a vrot (or vrot/vkep) trigger */
    double angelou_lithium_vrot_trigger;
    double angelou_lithium_vrotfrac_trigger;

    /* decay times */
    double angelou_lithium_MS_decay_time;
    double angelou_lithium_LMMS_decay_time;
    double angelou_lithium_HG_decay_time;
    double angelou_lithium_GB_decay_time;
    double angelou_lithium_CHeB_decay_time;
    double angelou_lithium_EAGB_decay_time;
    double angelou_lithium_TPAGB_decay_time;

    /* mass fraction of lithium when it is created */
    double angelou_lithium_MS_massfrac;
    double angelou_lithium_LMMS_massfrac;
    double angelou_lithium_HG_massfrac;
    double angelou_lithium_GB_massfrac;
    double angelou_lithium_CHeB_massfrac;
    double angelou_lithium_EAGB_massfrac;
    double angelou_lithium_TPAGB_massfrac;
#endif // NUCSYN_ANGELOU_LITHIUM
    double nucsyn_network_error[NUCSYN_NETWORK_NUMBER];

#endif // NUCSYN

    // minimum and maximum timesteps
    double minimum_timestep, maximum_timestep;
    double maximum_timestep_by_stellar_type[NUMBER_OF_STELLAR_TYPES];

    // maximum timestep when nuclear burning
    double maximum_nuclear_burning_timestep;
    // maximum factor between timestep from step to step
    double maximum_timestep_factor;
    // factors to decrease and increase timestep when zooming
    double zoomfac_multiplier_decrease;
    double zoomfac_multiplier_increase;
    // timestep solver factor (see timestep_limits.c)
    double timestep_solver_factor;
    // Choice for mira period at which the superwind switches on during TPAGB
    double tpagb_superwind_mira_switchon;
    // Choice for Reimers eta on the TPAGB
    double tpagb_reimers_eta;
    // wind velocity multiplier (not for TPAGB stars)
    double vwind_multiplier;
    /* 0.125 by default, as in BSE Hurley et al. (2002) */
    double vwind_beta;
    // wind gas/dust ratio (typically 200-500)
    double wind_gas_to_dust_ratio;
    /* Tout and Pringle 1992 wind multiplier */
    double Tout_Pringle_1992_multiplier;

    /* artificial accretion rates */
    double artificial_mass_accretion_rate[NUMBER_OF_STARS];
    double artificial_angular_momentum_accretion_rate[NUMBER_OF_STARS];
    double artificial_orbital_angular_momentum_accretion_rate;
    double artificial_accretion_start_time;
    double artificial_accretion_end_time;
    double artificial_mass_accretion_rate_by_stellar_type[NUMBER_OF_STELLAR_TYPES];
    double minimum_donor_menv_for_comenv;
    double magnetic_braking_factor;
    double magnetic_braking_gamma;

    // Wolf Rayet wind selection
    /* Wolf-Rayet wind multiplication factor */
    double wr_wind_fac;
    double wind_multiplier[NUMBER_OF_STELLAR_TYPES];
    double wind_type_multiplier[WIND_TYPE_COUNT];
    double wind_LBV_luminosity_lower_limit;
    double wind_Nieuwenhuijzen_luminosity_lower_limit;

    /*
     * Temperature range of post-(A)GB stars
     */
    double Teff_postAGB_min;
    double Teff_postAGB_max;

    /*
     * post-common envelope transition time in years (1e2 - 1e4)
     */
    double PN_comenv_transition_time;
    double PPN_envelope_mass;
    double minimum_time_between_PNe;

    /*
     * PN timestep resolution
     */
    double PN_resolve_maximum_envelope_mass;
    double PN_resolve_minimum_luminosity;
    double PN_resolve_minimum_effective_temperature;

    /*
     * PN fast wind parameters
     */
    double PN_fast_wind_mdot_AGB;
    double PN_fast_wind_mdot_GB;
    double PN_fast_wind_dm_AGB;
    double PN_fast_wind_dm_GB;

    /* Common envelope parameters */
    double alpha_ce[MAX_NUMBER_OF_COMENVS];
    double lambda_ce[MAX_NUMBER_OF_COMENVS];
    double lambda_ionisation[MAX_NUMBER_OF_COMENVS];
    double lambda_enthalpy[MAX_NUMBER_OF_COMENVS];

    /* return of angular momentum */
    double comenv_pre_angmom_return_fraction;
    double comenv_post_angmom_return_fraction;

    /*
     * multiplier for the period when using
     * COMENV_CONVECTION
     */
    double comenv_convection_multiplier;

    /*
     * Scatter comenv algorithm parameters
     */
    double comenv_Scatter_dim;
    double comenv_Scatter_eta;

    /*
     * Accretion limit multipliers
     */
    double accretion_limit_eddington_steady_multiplier;
    double accretion_limit_eddington_LMMS_multiplier;
    double accretion_limit_eddington_WD_to_remnant_multiplier;
    double accretion_limit_thermal_multiplier;
    double accretion_limit_dynamical_multiplier;

    /*
     * Donor limit multipliers
     */
    double donor_limit_thermal_multiplier;
    double donor_limit_dynamical_multiplier;
    double donor_limit_envelope_multiplier;

    // more free parameters
    // fraction of matter retained in a nova explosion
    double nova_retention_fraction_H; // 0.001
    double nova_retention_fraction_He; // 0.001
    double beta_reverse_nova;
    double nova_irradiation_multiplier;
    double nova_faml_multiplier;

    double nova_timestep_accelerator_num;
    double nova_timestep_accelerator_index;
    double nova_timestep_accelerator_max;

#ifdef KEMP_NOVAE
    double nova_eta_shift;
#endif // KEMP_NOVAE

    /* gamma is the angular momentum factor for mass lost during Roche (-1.0) */
    double rlof_angmom_gamma; // 0.0
    /* gamma for non-conservative mass transfer */
    double nonconservative_angmom_gamma;

#ifdef CIRCUMBINARY_DISK_DERMINE
    /* fraction of mass loss which is feeding the circumbinary disk */
    double alphaCB;
#endif // CIRCUMBINARY_DISK_DERMINE

    /* Remiers GB mass loss eta factor */
    double gb_reimers_eta;
    /* Binary enhanced wind loss factor */
    double CRAP_parameter;
    /* Bondi-Hoyle accretion factor (1.5) */
    double Bondi_Hoyle_accretion_factor;

    /* various limiting core masses */
    double chandrasekhar_mass;
    double max_neutron_star_mass;
    double maximum_mcbagb_for_degenerate_carbon_ignition;
    double minimum_mcbagb_for_nondegenerate_carbon_ignition;
    double minimum_CO_core_mass_for_carbon_ignition;
    double minimum_CO_core_mass_for_neon_ignition;
    double max_HeWD_mass;
    double minimum_helium_ignition_core_mass;
    double HeWD_HeWD_ignition_mass;

    /* multipliers */
    double gbwindfac, tpagbwindfac, eagbwindfac, nieuwenhuijzen_windfac;
    double tidal_strength_factor;
    double merger_angular_momentum_factor;
    double merger_mass_loss_fraction;
    double merger_mass_loss_fraction_by_stellar_type[NUMBER_OF_STELLAR_TYPES];
    double wind_djorb_fac;
    /* orbital angular momentum loss factor from winds */
    double lw;

    /* mass required for an edge-lit detonation */
    double mass_accretion_for_COWD_DDet;
    double mass_accretion_for_ONeWD_DDet;

    /* accretion rate to convert a COWD to ONeWD */
    double COWD_to_ONeWD_accretion_rate;

    // WD mass accretion limits
    double WD_accretion_rate_novae_upper_limit_hydrogen_donor;
    double WD_accretion_rate_novae_upper_limit_helium_donor;
    double WD_accretion_rate_novae_upper_limit_other_donor;
    double WD_accretion_rate_new_giant_envelope_lower_limit_hydrogen_donor;
    double WD_accretion_rate_new_giant_envelope_lower_limit_helium_donor;
    double WD_accretion_rate_new_giant_envelope_lower_limit_other_donor;

    // He Star Ia limits
    double mass_for_Hestar_Ia_lower;
    double mass_for_Hestar_Ia_upper;

#ifdef VW93_MIRA_SHIFT
    double vw93_mira_shift;
#endif //  VW93_MIRA_SHIFT

#ifdef VW93_MULTIPLIER
    double vw93_multiplier;
#endif //  VW93_MULTIPLIER

#ifdef COMENV_NS_ACCRETION
    // accretion onto neutron stars during comenv
    double comenv_ns_accretion_fraction;
    double comenv_ns_accretion_mass;
#endif //  COMENV_NS_ACCRETION

#ifdef COMENV_MS_ACCRETION
    // accretion onto MS stars during comenv
    double comenv_ms_accretion_mass;
#endif //  COMENV_MS_ACCRETION

#ifdef COMENV_POLYTROPES
    double comenv_splitmass;
#endif // COMENV_POLYTROPES
    double nelemans_gamma;
    double nelemans_minq;
    double nelemans_max_frac_j_change;

    double SN_mean_anomaly;

    double comenv_post_eccentricity;
    double dedmRLOF;

    /* Hachisu 1996 disk wind for Ia Supernovae,
     * Their limit is multiplied by this factor, which
     * is 1e20 by default
     */
    double hachisu_qcrit;
    double hachisu_limiter_multiplier;

    double gravitational_radiation_modulator_J;
    double gravitational_radiation_modulator_e;
#ifdef TIMESTEP_MODULATION
    double timestep_modulator;
#endif // TIMESTEP_MODULATION

#ifdef RLOF_MDOT_MODULATION
    double RLOF_mdot_factor;
#endif //RLOF_MDOT_MODULATION

#ifdef RLOF_RADIATION_CORRECTION
    // ratio of radiation to gravitational force in Roche lobe
    double RLOF_f;
#endif //  RLOF_RADIATION_CORRECTION

    /*
     * parameter regulating the amount of mixing during the merger of two MS stars
     * It is equal to the fraction of the "envelope" that is mixed (besides the core which is always mixed).
     * Note that the "envelope" may contain he rich material of the core of the secondary [0-1]
     */
    double mixingpar_MS_mergers;

    /* third dredge up */
    double minimum_envelope_mass_for_third_dredgeup;

    /* third dredge-up calibration parameters */
    double lambda_min;
    double delta_mcmin;
    double lambda_multiplier;

    // AGB timestep multiplication factor
    double dtfac;

#ifdef DISCS
    double cbdisc_max_lifetime;
    double disc_timestep_factor;
    double cbdisc_gamma;
    double cbdisc_alpha;
    double cbdisc_kappa;
    double cbdisc_torquef;
    double cbdisc_init_dM;
    double cbdisc_init_dJdM;
    double cbdisc_albedo;
#ifdef DISCS_CIRCUMBINARY_FROM_COMENV
    double comenv_disc_angmom_fraction;
    double comenv_disc_mass_fraction;
#endif // DISCS_CIRCUMBINARY_FROM_COMENV

#ifdef DISCS_CIRCUMBINARY_FROM_WIND
    double wind_disc_mass_fraction;
    double wind_disc_angmom_fraction;
#endif // DISCS_CIRCUMBINARY_FROM_WIND

    double cbdisc_minimum_evaporation_timescale;
    double cbdisc_mass_loss_constant_rate;
    double cbdisc_mass_loss_inner_viscous_multiplier;
    double cbdisc_mass_loss_inner_viscous_angular_momentum_multiplier;
    double cbdisc_mass_loss_inner_L2_cross_multiplier;
    double cbdisc_mass_loss_ISM_ram_pressure_multiplier;
    double cbdisc_mass_loss_ISM_pressure;
    double cbdisc_mass_loss_FUV_multiplier;
    double cbdisc_mass_loss_Xray_multiplier;
    double cbdisc_minimum_luminosity;
    double cbdisc_minimum_mass;
    double cbdisc_resonance_multiplier;
    double cbdisc_minimum_fRing;
    double disc_log_dt;

#endif // DISCS

    double observable_radial_velocity_minimum;
    double start_time;
    double max_stellar_angmom_change;

    double rotationally_enhanced_exponent;

    double helium_flash_mass_loss;
    double degenerate_core_merger_dredgeup_fraction;

#ifdef GAIAHRD
    double gaia_L_binwidth;
    double gaia_Teff_binwidth;
#endif //GAIAHRD

#ifdef MINT
    double MINT_minimum_shell_mass;
    double MINT_maximum_shell_mass;
#endif // MINT

#ifdef ORBITING_OBJECTS
    double orbiting_objects_tides_multiplier;
    double orbiting_objects_wind_accretion_multiplier;
    double orbiting_objects_close_pc_threshold;
#endif // ORBITING_OBJECTS

#ifdef EVOLUTION_SPLITTING
    double evolution_splitting_sn_eccentricity_threshold;
#endif
    double COWD_COWD_explode_above_mass;
    double HeWD_COWD_explode_above_mass;
    double eta_violent_WDWD_merger;
#ifdef SAVE_MASS_HISTORY
    double save_mass_history_n_thermal;
#endif

#ifdef PPISN
    /* PPISN/PISN prescription */
    double PPISN_additional_massloss;
    double PPISN_core_mass_range_shift;
    double PPISN_massloss_multiplier;
#endif // PPISN
    double fixed_beta_mass_transfer_efficiency;
    double minimum_mass_for_COWD_DDet;
    double minimum_age_for_COWD_DDet;
    double minimum_mass_for_ONeWD_DDet;
    double minimum_age_for_ONeWD_DDet;

    double Matsumoto2022_fplateau;
    double Matsumoto2022_fshock;
    double Matsumoto2022_shock_mass_fraction;

    /*
     * Strings (e.g. Filenames)
     * Note: most strings are of length STRING_LENGTH
     *       to allow a preferences struct to simply
     *       be copied, however they can be pointers.
     *
     * In your cmd_line_args_list.def file fixed-length
     * strings are the default, type ARG_STRING, while
     * unbounded string buffers are type ARG_UNBOUNDED_STRING.
     *
     * Unbounded strings should be freed in free_memory().
     */
#ifdef BINARY_C_API
    char api_log_filename_prefix[STRING_LENGTH];
#endif // BINARY_C_API
    char stardata_dump_filename[STRING_LENGTH];
    char stardata_load_filename[STRING_LENGTH];
    char * JSON_commands_string;

#ifdef YBC
    char YBC_path[STRING_LENGTH];
    char YBC_listfile[STRING_LENGTH];
    char YBC_instruments[STRING_LENGTH];
#endif // YBC

#ifdef FILE_LOG
    /* file log name */
    char log_filename[STRING_LENGTH];
    char log_separator[STRING_LENGTH];
#endif // FILE_LOG

#ifdef LOG_SUPERNOVAE
    char sn_dat_filename[STRING_LENGTH];
#endif // LOG_SUPERNOAVE

#ifdef BATCHMODE
    //char batchmode_string[BATCHMODE_STRING_LENGTH];
#endif // BATCHMODE

    char stopfile[STRING_LENGTH];
    char target_times[STRING_LENGTH];
    char hierarchy[STRING_LENGTH];

#ifdef NUCSYN
    char Seitenzahl2013_model[12];
#endif // NUCSYN

#if defined DISCS &&                            \
    (defined DISC_LOG ||                        \
     defined DISC_LOG_2D)
    char disc_log_directory[STRING_LENGTH];
#endif // DISCS && (DISC_LOG || DISC_LOG_2D)

    /* integer parameters */

    /* random number seeds */
    Random_seed cmd_line_random_seed;
    Random_seed cmd_line_random_systems_seed;
    long int random_skip;

    Multiplicity multiplicity;
    int nelemans_n_comenvs;

    int PN_Hall_fading_time_algorithm;
    int colours[NUM_ANSI_COLOURS];
    int log_period_unit;
    int skip_bad_args;

#ifdef DISCS
    int cbdisc_eccentricity_pumping_method;
    int cbdisc_mass_loss_inner_viscous_accretion_method;
    int cbdisc_viscous_photoevaporative_coupling;
    int cbdisc_viscous_L2_coupling;
    int cbdisc_inner_edge_stripping_timescale;
    int cbdisc_outer_edge_stripping_timescale;
    int disc_log;
    int disc_log2d;
    int disc_n_monte_carlo_guesses;
#endif // DISCS

#ifdef EVOLUTION_SPLITTING
    int current_splitdepth;
    int evolution_splitting_maxdepth;
    /* number of splits for various cases */
    int evolution_splitting_sn_n;
#endif // EVOLUTION_SPLITTING

    /* maximum number of models */
    int max_model_number;

#ifdef OPACITY_ALGORITHMS
    int opacity_algorithm;
#endif //  OPACITY_ALGORITHMS

#ifdef EQUATION_OF_STATE_ALGORITHMS
    int equation_of_state_algorithm;
#endif // EQUATION_OF_STATE_ALGORITHMS

    /* different wind choices */
    int gbwind, eagbwind, tpagbwind, wr_wind, postagbwind;

    /* algorithm to handle overspin */
    int overspin_algorithm;

#ifdef WD_KICKS
    /* when do we apply WD kicks? */
    int wd_kick_when;
    int wd_kick_pulse_number;
    /* WD kick direction */
    int wd_kick_direction;
#endif // WD_KICKS

    /* SN kick speed distribution:
     * 0=KICK_VELOCITY_FIXED=fixed (but random direction),
     * 1=KICK_VELOCITY_MAXWELLIAN = maxwellian as in Hurley et al 2002
     * 2=custom function (see monte_carlo_kick.c for setup)
     */
    Kick_distribution sn_kick_distribution[SN_NUM_TYPES];

    /* Radice 2018 EOS */
    int Radice2018_EOS;

    /* orbital angular momentum loss prescription */
    int wind_angular_momentum_loss;

    /* angular momentum transfer model (RLOF) */
    int RLOF_angular_momentum_transfer_model;

    /* gravitational radiation model */
    int gravitational_radiation_model;

    /* Black hole formation prescription */
    int BH_prescription;

    /* Baryonic to gravitational remnant mass prescription */
    int baryonic_to_gravitational_remnant_mass_prescription;

#ifdef PPISN
    /* PPISN/PISN prescription */
    int PPISN_prescription;
#endif // PPISN

    /* E2 (conv. core tides) prescription */
    int E2_prescription;

    /* tidal computation algorithm */
    int tides_algorithm;

    /* tides prescription: BSE or MINT */
    int tides_convective_damping;

    /*
     * RLOF interpolation method
     */
    int RLOF_interpolation_method;

    /*
     * Method for calculating the amount of
     * material accreted during novae
     */
    int nova_retention_method;

#ifdef NUCSYN
    /*
     * Nova yield algorithm. We have two
     * options: one for CO and one for ONe
     * white dwarf accretors.
     */
    int nova_yield_CO_algorithm;
    int nova_yield_ONe_algorithm;
#endif // NUCSYN

#ifdef TIDES_DIAGNOSIS_OUTPUT
    int tides_logging_function;
#endif //TIDES_DIAGNOSIS_OUTPUT

    /*
     * Type of solver used by the time-evolution loop
     */
    int solver;

    /*
     * System ID number
     */
    int id_number;

    /* Wind-RLOF parameter */
#ifdef WRLOF_MASS_TRANSFER
    int WRLOF_method;
#endif // WRLOF_MASS_TRANSFER



    /* Wind mass loss options */
    Wind_loss_algorithm wind_mass_loss;
    int wind_mass_loss_algorithm;

    /* common envelope prescription changer */
    int comenv_prescription[MAX_NUMBER_OF_COMENVS];
    int comenv_algorithm;
    int comenv_envelope_response;
    int qcrit_giant_branch_method;

    /* common envelope spin behaviour */
    int comenv_ejection_spin_method;
    int comenv_merger_spin_method;

    /* magnetic braking algorithm */
    int magnetic_braking_algorithm;

    int post_SN_orbit_method;


    int transient_method;

    int decretion_disc_radius_algorithm;

#ifdef RANDOM_SYSTEMS
    int random_systems;
#endif // RANDOM_SYSTEMS

#ifdef BATCHMODE
    int batchmode;
    int batch_submode;
#endif // BATCHMODE

    int rotationally_enhanced_mass_loss;

#ifdef STELLAR_COLOURS
    int gaia_colour_transform_method;
    int gaia_white_dwarf_colour_method;
#endif // STELLAR_COLOURS

    int white_dwarf_cooling_model;
    int white_dwarf_radius_model;

    int repeat;
#ifdef MATTSSON_MASS_LOSS
    /* the wind to use when Mattsson's C-rich prescription is
       not appropriate */
    int mattsson_Orich_tpagbwind;
#endif //MATTSSON_MASS_LOSS

    int RLOF_method;
    int small_envelope_method;
    int WDWD_merger_algorithm;
    int MSMS_merger_age_algorithm;

#ifdef NUCSYN
    /* which abundance mixture on the ZAMS? */
    Abundance_mix initial_abundance_mix;
    int type_Ia_MCh_supernova_algorithm;
    int type_Ia_sub_MCh_supernova_algorithm;
    int triggered_SNIa_algorithm;
    int type_Ia_DDet_yield_algorithm;
    int core_collapse_supernova_algorithm;
    int core_collapse_rprocess_algorithm;
    int electron_capture_supernova_algorithm;
    int nucsyn_solver;
#ifdef NUCSYN_ANGELOU_LITHIUM
    int angelou_lithium_decay_function;
#endif // NUCSYN_ANGELOU_LITHIUM
#endif // NUCSYN
    int NS_merger_yield_algorithm;
    int core_collapse_energy_prescription;

    int internal_buffering;
    int eccentric_RLOF_model;
#ifdef STELLAR_POPULATIONS_ENSEMBLE
    int ensemble_dummy_n;
#endif // STELLAR_POPULATIONS_ENSEMBLE

#ifdef GALACTIC_MODEL
    int galactic_model;
#endif // GALACTIC_MODEL

#ifdef MINT
    int MINT_cache_tables;
    int MINT_Kippenhahn;
    Stellar_type MINT_Kippenhahn_stellar_type;
    Stellar_type MINT_Kippenhahn_companion_stellar_type;
    int MINT_nshells;
    int MINT_maximum_nshells;
    int MINT_minimum_nshells;
#endif // MINT

    int float_overflow_checks;
    int defaults_set;
    int cannot_shorten_timestep_policy;

    Stellar_type max_stellar_type[4];

    Stellar_structure_algorithm stellar_structure_algorithm;
    Core_algorithm AGB_core_algorithm;
    Radius_algorithm AGB_radius_algorithm;
    Luminosity_algorithm AGB_luminosity_algorithm;
    Third_dredge_up_algorithm AGB_3dup_algorithm;

    /* Boolean parameters */
    Boolean show_version;
    Boolean tasks[BINARY_C_TASK_NUMBER];
    Boolean require_drdm;
    Boolean save_pre_events_stardata;
    Boolean PN_fast_wind;
    Boolean PN_resolve;
    Boolean clean_log;
    Boolean exit_backtrace;
#ifdef SOFT_NANCHECK
    Boolean nanchecks;
#endif // SOFT_NANCHECK
#ifdef LAU2024_THERMAL_EXPANSION
    Boolean Lau2024_thermal_expansion;
#endif // LAU2024_THERMAL_EXPANSION

#ifdef ORBITING_OBJECTS
    Boolean orbiting_objects_log;
    Boolean RLOF_transition_objects_escape;
    Boolean evaporate_escaped_orbiting_objects;
#endif // ORBITING_OBJECTS
#ifdef WTTS_LOG
    Boolean wtts_log;
#endif // WTTS_LOG
    Boolean flush_log;
    Boolean vandenHeuvel_logging;
    Boolean timestep_logging;
    Boolean derivative_logging;
    Boolean disable_end_logging;
    Boolean rejects_in_log;
    Boolean progress_bar;
    Boolean log_all_reject_timestep_failures;
    Boolean log_overspin_events;
    Boolean pause_after_repeat;
    Boolean hrdiag_output;
    Boolean pause_at_evolution_end;
    Boolean individual_novae;
    Boolean He_nova_DDets;
    Boolean use_fixed_timestep[FIXED_TIMESTEP_NUMBER];

    /* disable events */
    Boolean disable_events;
    /* allow events to be handled */
    Boolean handle_events;
    /* stop evolution and return on event */
    Boolean stop_at_event;

    Boolean unstable_RLOF_can_trigger_SNIa;

#ifdef FILE_LOG
    Boolean log_legacy_stellar_types;
    Boolean colour_log;
    Boolean log_arrows;
    Boolean log_groups;
#endif // FILE_LOG
    Boolean resolve_stellar_type_changes;
    Boolean force_circularization_on_RLOF;
    Boolean speedtests;

    Boolean VW93_EAGB_wind_speed;
    Boolean VW93_TPAGB_wind_speed;

    /* Boolean to specify monte-carlo SN kicks or not */
    Boolean monte_carlo_sn_kicks;
    Boolean third_dup; // set third dredge up on and off
    Boolean use_periastron_Roche_radius;
    Boolean hachisu_disk_wind;
    Boolean show_minimum_separation_for_instant_RLOF;
    Boolean show_minimum_orbital_period_for_instant_RLOF;
    Boolean show_maximum_mass_ratio_for_instant_RLOF;
#ifdef DISCS
    Boolean cbdisc_resonance_damping;
    Boolean cbdisc_fail_ring_inside_separation;
    Boolean cbdisc_inner_edge_stripping;
    Boolean cbdisc_outer_edge_stripping;
    Boolean cbdisc_no_wind_if_cbdisc;
    Boolean disc_legacy_logging;
    Boolean cbdisc_end_evolution_after_disc;
#endif // DISCS

#ifdef LOG_POSTAGB_STARS
    Boolean postagb_legacy_logging;
#endif // LOG_POSTAGB_STARS

    Boolean degenerate_core_merger_nucsyn;
    Boolean degenerate_core_helium_merger_ignition;

#ifdef NUCSYN
    Boolean degenerate_core_merger_dredgeup;
    Boolean initial_abunds_only;

#ifdef NUCSYN_THIRD_DREDGE_UP_MULTIPLIERS
    Boolean boost_third_dup;
#endif // NUCSYN_THIRD_DREDGE_UP_MULTIPLIERS
#ifdef NUCSYN_TPAGB_HBB
    Boolean NeNaMgAl;
#endif // NUCSYN_TPAGB_HBB
#ifdef NUCSYN_ALLOW_NO_PRODUCTION
    Boolean no_production;
#endif // NUCSYN_ALLOW_NO_PRODUCTION
    Boolean no_thermohaline_mixing;
#ifdef NUCSYN_YIELDS
    Boolean legacy_yields;
#endif // NUCSYN_YIELDS
    Boolean cf_amanda_log;
    Boolean nucsyn_network[NUCSYN_NETWORK_NUMBER];
#endif // NUCSYN
#ifdef SAVE_MASS_HISTORY
    Boolean adjust_structure_from_mass_changes;
#endif

    Boolean ensemble_logtimes;
#ifdef STELLAR_POPULATIONS_ENSEMBLE
    Boolean ensemble;
    Boolean ensemble_filters_off;
    Boolean ensemble_filter[STELLAR_POPULATIONS_ENSEMBLE_FILTER_NUMBER];
    Boolean ensemble_filter_override[STELLAR_POPULATIONS_ENSEMBLE_FILTER_NUMBER];
    Boolean ensemble_defer;
    Boolean ensemble_output_lines;
    Boolean ensemble_legacy_ensemble;
    Boolean ensemble_enforce_maxtime;
    Boolean ensemble_notimebins;
    Boolean ensemble_alltimes;
#endif // STELLAR_POPULATIONS_ENSEMBLE

#ifdef PRE_MAIN_SEQUENCE
    Boolean pre_main_sequence, pre_main_sequence_fit_lobes;
#endif // PRE_MAIN_SEQUENCE

    Boolean disable_debug;
    Boolean event_logging;
    Boolean debug_filter_pointers;
    Boolean allow_debug_nan;
    Boolean allow_debug_inf;

#ifdef REVERSE_TIME
    Boolean reverse_time;
#endif //  REVERSE_TIME

#ifdef EVOLUTION_SPLITTING
    Boolean evolution_splitting;
#endif // EVOLUTION_SPLITTING
    Boolean nelemans_recalc_eccentricity;

    Boolean post_ce_objects_have_envelopes;
    Boolean post_ce_adaptive_menv;
    Boolean Politano2021_comenv_criterion;
    Boolean Politano2021_comenv_criterion_use_cores;

#ifdef EVENT_BASED_LOGGING
    Boolean event_based_logging[EVENT_BASED_LOGGING_NUMBER];
#endif // EVENT_BASED_LOGGING


#ifdef MINT
    /*
     * MINT options
     */
    Boolean MINT_data_cleanup;
    Boolean MINT_disable_warnings;
    Boolean MINT_disable_grid_load_warnings;
    Boolean MINT_MS_rejuvenation;
    Boolean MINT_nuclear_burning;
    Boolean MINT_remesh;
    Boolean MINT_use_ZAMS_profiles;
    Boolean MINT_fallback_to_test_data;
    Boolean MINT_use_fallback_comenv;
    Boolean MINT_filename_vb;
    Boolean MINT_show_metadata;
#endif // MINT

    Boolean load_data_grids;

    Boolean apply_Darwin_Radau_correction;
#ifdef YBC
    Boolean YBC_all_instruments;
#endif // YBC
    Boolean log_transients_2024;
};

#ifdef EVOLUTION_SPLITTING

struct splitinfo_t
{
    struct stardata_t * stardata;
    struct stardata_t ** previous_stardatas;
    int n_previous_stardatas;
    int depth;
    int count;
    int start_count;
};

#endif // EVOLUTION_SPLITTING

struct probability_distribution_t
{
    double (*func)(double*); // the generating functions
    double xmin; // the n range minima
    double xmax; // the n range maxima
    double x; // the generated co-ordinate
    double *table; // interpolation tables
    int nlines; // number of lines in the table
    int maxcount; // max number of guesses at reconstruction (100)
    int error; // error code (0)
} Aligned;


#ifdef DISCS

/* structure to define a power law */
struct power_law_t
{
    /* power law A(R) = A0 * (R/Rstart)^exponent from Rstart to Rend */
    double R0, R1, A0, A1, exponent;

} Aligned;

/*
 * Structure to defined the contents of a disc, circumstellar or
 * circumbinary
 */
struct disc_thermal_zone_t
{
    /*
     * Tlaw is required. This is the T(R) power law, it is
     * a special case.
     */
    struct power_law_t Tlaw;

    /*
     * Alternative power laws, e.g. sigma(R)
     */
    struct power_law_t power_laws[DISCS_NUMBER_OF_POWER_LAWS];

    double rstart, rend; // start and end radii

    int type; // must not be unsigned (-1 is undef)

    Boolean valid; // TRUE if zone is valid (used during construction)
} Aligned;


/*
 * Mass(etc) transfer struct
 */
struct disc_loss_t
{
    double mdot; // mass loss rate
    double jdot; // angular momentum loss rate
    double edot; // eccentricity change rate
} Aligned;

/*
 * Structure for disc information logging
 */
struct disc_info_t
{
    double M;
    double J;
    double Tin;
    double Tout;
    double Rin;
    double Rout;
    double Rin_a;
    double Rout_a;
    double Hin_Rin; // Hin/Rin
    double Hout_Rout; // Hout/Rout
    double L;
    double lifetime;
    double Mdot_visc;
} Aligned;

struct disc_t
{
    struct memoize_hash_t * memo; /* memoize data */
    double M; // disc total mass
    double alpha; // disc viscosity constant (dimensionless)
    double gamma; // adiabatic exponent (dimensionless)
    double albedo; // albedo
    double torqueF; // binary torque multiplication factor (dimensionless)
    double torqueF_for_Rin; // binary torque multiplication factor used to fix Rin (dimensionless)
    double fuv; // Far UV mass flux in g/cm^2/s
    double kappa; // Opacity in cm^2/g (assumed constant, only valid at low T)
    double Rin; // inner radius (cm)
    double Rout; // outer radius (cm)
    double Rin_min; // minimum inner radius
    double Rout_max; // maximum outer radius
    double lifetime; // lifetime of the disc (seconds)
    double dt; // natural timestep (seconds)
    double J; // total angular momentum
    double sigma0; // to be removed
    double mu;
    double Tvisc0;
    double F;
    double dM_binary, Mdot_binary;
    double dM_ejected, Mdot_ejected;
    double dJ_binary, Jdot_binary;
    double dJ_ejected, Jdot_ejected;
    double dT;
    double de_binary, edot_binary;
    double Tvisc, Tradin, Tradout;
    double Hin; // inner edge scale height
    double RJhalf; // half angular momentum radius
    double HRJhalf; // H/R at half angular momentum radius
    double dRindt, dRoutdt; // derivatives
    double PISM, RISM;
    double LX; // X-ray irradiation onto the disc
#ifdef NUCSYN
    double X[ISOTOPE_ARRAY_SIZE];
#endif // NUCSYN
    double Owen2012_norm;
    double Revap_in, Revap_out;
    double F_stripping_correction;
    double nextlog;
    double fRing;
#ifdef DISC_SAVE_EPSILONS
    double epstorquef;
    double epsilon[DISC_NUMBER_OF_CONSTRAINTS];
#endif //DISC_LOG
#ifdef DISC_LOG_POPSYN
    double lastlogtime;
#endif // DISC_LOG_POPSYN
    double t_m;
    double t_m_slow;
    double t_j;
    double t_e;
#ifdef DISC_EQUATION_CHECKS
    double equations_T_error_pc;
    double equations_mass_loss_term_pc;
#endif // DISC_EQUATION_CHECKS

    /* mass,ang mom and eccentricty transfer */
    struct disc_loss_t loss[DISC_LOSS_N];

    /* allow a maximum of 3 thermal zones */
    Disc_zone_counter n_thermal_zones;
    struct disc_thermal_zone_t thermal_zones[DISCS_MAX_N_ZONES + 1];
    unsigned int converged;
    int vb;
    int ndisc;
    int type;
    int delete_count;
    int solver;
    int guess;
    int iteration;
    int end_count;
    Star_number overflower_starnum;
    Stellar_type overflower_stellar_type;
    Boolean first;
    Boolean firstlog;
    Boolean append;
    Boolean suppress_viscous_inflow;

} Aligned;

#endif // DISCS

/*
 * Stellar derivative structure
 */
struct derivative_t
{
    double mass;
    double eccentricity;
    double specific_angular_momentum;
    double other;
} Aligned;

#ifdef MINT

struct mint_header_t
{
    struct cdict_t * cdict;
    char * filename;
} Aligned;

struct mint_shell_t
{
#ifdef NUCSYN
    double X[ISOTOPE_ARRAY_SIZE] Cdict_var;
#endif // NUCSYN
    double dm Cdict_var; /* mass in the shell */
    double m Cdict_var; /* m(r) */
    double T Cdict_var; /* temperature(r) */
    double rho Cdict_var; /* density(r) */
    double gas_pressure Cdict_var; /* Pgas(r) */
    double radiation_pressure Cdict_var; /* Prad(r) */
    double total_pressure Cdict_var; /* P(r) */
    double radius Cdict_var; /* r */
    double gamma1 Cdict_var; /* gamma1 (from equation of state) */
    double pressure_scale_height Cdict_var; /* -dlnr/dlnP */
    Boolean convective Cdict_var; /* TRUE if convective */
} Aligned;

struct mint_t
{
    /*
     * Information for the MINT interpolation
     * scheme which should be in star_t.
     */
    struct data_table_t * GB_table;
    Abundance XHc Cdict_var; /* central hydrogen abundance */
    Abundance XHec Cdict_var; /* central helium abundance */
    Abundance XCc Cdict_var; /* central carbon abundance */
    Abundance XOc Cdict_var; /* central oxygen abundance */
    double central_degeneracy; /* log10(central degeneracy) */
    double central_temperature; /* central tempeature (used in COWDs) */
    double central_density; /* central density (used in COWDs) */
    double central_pressure; /* central pressure (used in COWDs) */
    double helium_luminosity; /* helium-burning luminosity */

    /*
     * molecular weights of the whole star
     * and at its centre
     */
    Molecular_weight mean_molecular_weight_star;
    Molecular_weight mean_molecular_weight_core;

    double mint_tbgb; /* fudge to marry BSE and MINT */
    double warning;
    double log_helium_luminosity_fraction;

    Shell_index nshells Cdict_var;
    struct mint_shell_t * shells Cdict_var_nest;
} Aligned;

#endif // MINT

/************************************************************/

#ifdef ORBITING_OBJECTS
/*
 * simple object that has no "stellar evolution" but can
 * orbit and accrete/lose mass.
 */
struct orbiting_object_t
{
    struct orbit_t orbit;
    double derivative[DERIVATIVE_ORBITING_OBJECT_NUMBER];
    char * name;
    void * central_object;
    double mass; /* gravitational mass */
    double spin_angular_momentum;
    Orbiting_object_type type;
    size_t index;
    Boolean in_disc;
} Aligned;
#endif // ORBITING_OBJECTS

/************************************************************/
#ifdef BSE
struct BSE_data_t
{
    double * timescales;
    double * luminosities;
    double * GB;
    double tm;
    double tn;
} Aligned;
#endif // BSE

struct star_pointers_t
{
#ifdef MINT
    struct mint_t * mint;
    struct mint_shell_t * mint_shells;
    int mint_nshells;
#endif // MINT
#ifdef BSE
    struct BSE_data_t * bse;
#endif // BSE
#if defined NUCSYN && \
    defined NUCSYN_ID_SOURCES
    Abundance * oldsource[SOURCE_NUMBER];
    Abundance * Xsource[SOURCE_NUMBER];
#endif // NUCSYN
} Aligned;

/*
 * The star struct contains the data relevant to a star.
 */
struct star_t
{
    struct stardata_t * parent;
    struct stardata_t * ancestor;
    struct stardata_t * stardata;
#ifdef MINT
    struct mint_t * mint;
#endif // MINT
#ifdef BSE
    struct BSE_data_t * bse;
#endif // BSE
#ifdef DISCS
    struct disc_t **discs;
#endif //DISCS
    struct phase_t phase[NUMBER_OF_STELLAR_TYPES];
    struct phase_t phase_start[NUMBER_OF_STELLAR_TYPES];
    struct phase_t phase_end[NUMBER_OF_STELLAR_TYPES];

    double derivative[Aligned_size(DERIVATIVE_STELLAR_NUMBER)];
#ifdef STELLAR_COLOURS
    double stellar_colour[STELLAR_MAGNITUDE_NUMBER];
#endif // STELLAR_COLOURS
#ifdef NUCSYN
    Abundance Xenv[(ISOTOPE_ARRAY_SIZE)] Cdict_var; /* stellar envelope abundances array */
    Abundance Xacc[(ISOTOPE_ARRAY_SIZE)] Cdict_var;  /* accreted material layer */
    Abundance Xinit[(ISOTOPE_ARRAY_SIZE)]; /* "initial" abundances, can be defined by accretion */
    Abundance Xyield[(ISOTOPE_ARRAY_SIZE)];/* mass yielded as a function of isotope number */
    Abundance Xhbb[(ISOTOPE_ARRAY_SIZE)];
#ifdef NUCSYN_STRIP_AND_MIX
    double X[NUCSYN_STRIP_AND_MIX_NSHELLS_MAX][ISOTOPE_ARRAY_SIZE];
    double mshell[NUCSYN_STRIP_AND_MIX_NSHELLS_MAX];
#endif // NUCSYN_STRIP_AND_MIX

#ifdef NUCSYN_WR_ACCRETION
    Abundance XWR0[ISOTOPE_ARRAY_SIZE];
#endif //NUCSYN_WR_ACCRETION
    /* optional arrays */
#ifdef NUCSYN_ID_SOURCES
    Abundance * Xsource[SOURCE_NUMBER]; /* yield by source */
    Abundance * oldsource[SOURCE_NUMBER]; /* ... at previous timestep */
#endif //  NUCSYN_ID_SOURCES

#ifdef NUCSYN_LOG_BINARY_MPYIELDS
    Abundance mpyield[ISOTOPE_ARRAY_SIZE];
#endif //  NUCSYN_LOG_BINARY_MPYIELDS
#ifdef NUCSYN_FIRST_DREDGE_UP_ACCRETION_CORRECTION_FROM_TAMS
    Abundance XTAMS[ISOTOPE_ARRAY_SIZE]; /* Terminal-age MS abundances */
#endif // NUCSYN_FIRST_DREDGE_UP_ACCRETION_CORRECTION_FROM_TAMS
#endif // NUCSYN
    double core_mass[NUMBER_OF_CORES] Cdict_var; /* (gravitational) core masses in the star */
    double mass_after_nova;
    double time_prev_nova;
    double mass Cdict_var; /* gravitational mass, in Solar masses */
    double baryonic_mass; /* baryonic mass, in Solar masses */
    double radius Cdict_var; /* radius, unknown units! */
    double effective_radius; /* Max(roche_radius,radius) */
    double roche_radius; /* Roche lobe radius */
    double roche_radius_at_periastron; /* Roche lobe radius at periastron */
    double rmin; /* minimum radius of RLOF accretion stream */
    double phase_start_mass; /* Mass at the beginning of this stage in the star's life */
    double phase_start_core_mass;/* ditto for the core mass */
    double age Cdict_var; /* stellar age */
    double stellar_type_tstart; /* age at the start of this stellar type */
    double epoch; /** the epoch of the star **/
    double max_MS_core_mass; /* max core mass on the MS */
    double core_radius Cdict_var; /* core radius */
    double luminosity Cdict_var; /* total luminosity of the star */
    double accretion_luminosity; /* accretion luminosity */
    double omega Cdict_var; /* star's angular frequency, y^-1 */
    double omega_crit; /* critical angular frequency assuming 50% equatorial bulge */
    double omega_crit_sph; /* critical angular frequency assuming spherical */
#ifdef OMEGA_CORE
    double omega_core; /* core angular frequency, y^-1 */
    double omega_core_crit; /* core critical angular frequency assuming 50% equatorial bulge */
    double omega_core_crit_sph; /* core critical angular frequency assuming spherical */
    double core_angular_momentum;
#endif // OMEGA_CORE
    double l_acc; /* specific angular momentum of accreted material (in RLOF) */
    double angular_momentum; /**  spin angular momentum **/
    double q; /** mass ratio **/
    double tbgb; /** giant branch time **/
    double tms; /** main sequence time **/
    double dM_in_timestep; /* mass change in a timestep */
    double vwind; /* wind velocity (cm/s) */
    double mdot; /* wind mass loss rate */
    double mcxx; /* er... */
#ifdef ADAPTIVE_RLOF2
    double adaptive2_mdot;
    double prevR0, prevR1;
    double prevRL0, prevRL1;
    double prevM0, prevM1;
    double prevMdot0, prevMdot1;
    double prev_radius_excess;
    double prev_target_radius;
    double adaptive2_Mdot_low;
    double adaptive2_Mdot_high;
    double adaptive2_Rexcess_low;
    double adaptive2_Rexcess_high;
    Boolean adaptive2_converged;
#endif // ADAPTIVE_RLOF2
    double rol0; /* roche lobe radius at the start of RLOF */
    double aj0; /* age at the start of RLOF */
    double mass_at_RLOF_start; /* used in RLOF interpolation */
    double rdot; /* abs(dr/dt) - dRL/dt */
    double drdt; /* (dr/dt) */
    double drdm; /* dr/dm at constant everything else */
    double stellar_timestep; /* star's timestep */
    double tm, tn; /* Main sequence and nuclear timescales */
    Time tkh; /* Kelvin-Helmholtz timescale */
    double max_EAGB_He_core_mass;
    double menv; /* convective envelope mass */
    double renv; /* convective envelope radius */
    double moment_of_inertia_factor; /* I/(MR^2) [== k2 in BSE] */
    double E2; /* set in grid data */
    double E_Zahn; /* set in grid data, used to calculate lambda2_Zahn */
    double effective_zams_mass; /* "main sequence" mass : can change by accretion */
    double mcx_EAGB; // CO core mass during EAGB and it's initial value
    double rzams; /* radius at the ZAMS */
    double TAMS_radius; /* radius at the end of the MS */
    double TAMS_luminosity; /* luminosity at the end of the MS */
    double TAMS_core_mass; /* He core mass at the TAMS, or zero if unknown */

    double convective_core_mass;
    double convective_core_mass_time_derivative;
    double convective_core_radius;

#ifdef WIND_ENERGY_LOGGING
    double Ewind, Elum;
#endif // WIND_ENERGY_LOGGING

#if defined SELMA_FIX_ACCRETED_MASS_IN_REJUVENATION     \
    || defined SELMA_BETTER_TREATMENT_OF_MS_ACCRETORS
    double last_mass;
#endif //  SELMA_FIX_ACCRETED_MASS_IN_REJUVENATION || SELMA_BETTER_TREATMENT_OF_MS_ACCRETORS

    double A0; // for Chen+Han 2008 qcrit prescription (ln(BGB radius))

#ifdef XRAY_LUMINOSITY
    double Xray_luminosity; /* luminosity in Xrays */
#endif // XRAY_LUMINOSITY
    double vrot0; /* initial spin in km/s */
    /*
     * the stellar velocity assuming it's a sphere,
     * the stellar equatorial velocity,
     * critical velocity assuming it's a sphere,
     * critical equatorial velocity.
     */
    double v_sph, v_eq, v_crit_sph, v_crit_eq;
    /* ratio of omega/omega_crit, and v_../v_crit_.. */
    double omega_ratio, v_sph_ratio, v_eq_ratio;

    double disc_mdot; /* mass lost from a disc by wind */
    double rotfac; /* rotationally enhanced wind factor */

#ifdef ADAPTIVE_RLOF
    double prev_r; // previous timestep radius
    double prev_dm; /* mass strip from previous call to RLOF */
    double roldot; /* d(Roche Radius)/dt */
    double dmdr;
    double nth;
    double RLOF_tkh;
    double RLOF_starttime;
#endif // ADAPATIVE_RLOF

    double main_sequence_radius; /* radius (stored while star is on the main sequence) */
#ifdef MAIN_SEQUENCE_STRIP
    double tms_at_start_caseA;
#endif // MAIN_SEQUENCE_STRIP

    double num_thermal_pulses Cdict_var;
    double num_thermal_pulses_since_mcmin Cdict_var;
    double num_novae Cdict_var;
    double dntp;
    double menv_1tp;
    double Mc_prev_pulse;
    double Mc_prev_pulse_no_3dup;
    double dm_3dup;
    double core_mass_no_3dup; /* core mass in the absence of dredge up */
    double mc_bgb; /* core mass at the base of the GB */
    double initial_mcx_EAGB; /* initial CO core mass on the EAGB */
    double time_first_pulse;
    double time_prev_pulse;
    double time_next_pulse;
    double model_time_first_pulse;
    double interpulse_period;
    double mc_1tp; /* core mass at first pulse */
    double lambda_3dup;
    double dm_novaH;
#ifdef KEMP_NOVAE
    double dm_novaHe;
    double dm_novaHcrit;
    double dm_novaHecrit;
    double nova_eta;
#endif // KEMP_NOVAE
    double nova_beta, nova_faml;

#ifdef RUNAWAY_STARS
    double v_Rwx, v_Rwy, v_Rwz, v_Rw;
#endif //  RUNAWAY_STARS

    double spiky_luminosity; // L modulated by pulse rise/fall
    double prev_tagb;
    double dmacc Cdict_var; /* mass of accreted layer, used in NUCSYN and novae */
    double SN_mass_ejected;

#ifdef NUCSYN
    /* Nucleosynthesis stuff */
    /* stuff you might want to log */
    double rho; /* HBB layer  density */
    double temp; /* HBB layer burning temperature */
    double start_HG_mass;
    // zero age helium star abundances
    double hezamsmetallicity; // metallicity at the start of the helium MS
    // time at which the star becomes a helium star
    double he_t, he_f; // and fraction of HeMS completed
    // HBB stuff
    double f_burn;
    double f_hbb;
    double temp_mult;
    double temp_rise_fac;
    double fmdupburn;
    double ftimeduphbb;
    double mixdepth, mixtime;
    double dmmix; /* mass of mixed region at the stellar surface */
    double conv_base; /* mass coord of surface convection zone (on MS) */
    double dm_companion_SN; /* mass change caused by companion supernova */
    double max_mix_depth;
    double prev_depth;
    double other_yield;

#ifdef KARAKAS2002_SMOOTH_AGB_RADIUS_TRANSITION
    double rTEAGB; // terminal AGB radius
#endif // KARAKAS2002_SMOOTH_AGB_RADIUS_TRANSITION

#ifdef NUCSYN_WR
    double he_mcmax;
#endif //NUCSYN_WR
    double mconv, thickest_mconv;

#ifdef NUCSYN_FIRST_DREDGE_UP_ACCRETION_CORRECTION
    double MS_mass; /* save the (terminal) main sequence mass */
#endif //  NUCSYN_FIRST_DREDGE_UP_ACCRETION_CORRECTION

#ifndef NUCSYN_WR_TABLES
    double prevHeaj;
#endif // NUCSYN_WR_TABLES

#ifdef NUCSYN_FIRST_DREDGE_UP_PHASE_IN
    double mc_gb_was; /* previous GB core mass */
#endif // NUCSYN_FIRST_DREDGE_UP_PHASE_IN

#ifdef  NS_BH_AIC_LOG
    double prev_luminosity, prev_radius, prev_mass, prev_core_mass;
#endif // NS_BH_AIC_LOG

#ifdef LOG_COMENV_RSTARS
    double r_star_postvrot;
    double r_star_postvrot2;
    double r_star_postvrot3;
    double r_star_postomega;
    double r_star_postvcrit;
    double r_star_postjspin;
    double r_star_prejorb;
    double r_star_prejtot;
    double r_star_precomenv_m1;
    double r_star_precomenv_m2;
    double r_star_precomenv_mc1;
    double r_star_precomenv_mc2;
    double r_star_precomenv_l1;
    double r_star_precomenv_l2;
    double r_star_postcomenv_l;
    double r_star_postcomenv_m;
    double r_star_postcomenv_mc;
    double r_star_comenv_time;
#endif // LOG_COMENV_RSTARS

#endif /* NUCSYN */

#ifdef LOG_SUPERNOVAE
    double sn_v, sn_rel_v;
#endif // LOG_SUPERNOVAE

#ifdef EVENT_BASED_LOGGING
    double sn_kick_speed, sn_kick_omega, sn_kick_phi;
    double fallback; // fallback fraction at SN explosion
    double fallback_mass; // total mass fallback
#endif // EVENT_BASED_LOGGING

#ifdef ANTI_TZ
    double TZ_mass;
    double TZ_NS_mass;
#endif // ANTI_TZ

#ifdef FABIAN_COSMOLOGY
    double final_carbon_core_mass;
    double final_helium_core_mass;
    double final_nuclear_burning_mass;
    double nuclear_burning_lifetime;
#endif //  FABIAN_COSMOLOGY

    double nova_recurrence_time;

#ifdef SAVE_MASS_HISTORY
    double Ith;
#endif // SAVE_MASS_HISTORY

    /*
     * Integer types
     */
#ifdef MINT
    int stellar_structure_algorithm[STELLAR_STRUCTURE_ALGORITHM_NUMBER];
#endif//MINT

    Stellar_type stellar_type Cdict_var; /* stellar type */
    Stellar_type core_stellar_type; /* core stellar type : this is the stellar type if the stellar envelope is stripped (e.g. COWD for a TPAGB star) */
    Stellar_type detached_stellar_type; /* stellar type when detached */
    Star_number starnum Cdict_var; // set to 0 for primary, 1 for secondary. useful for logging
    Star_number system_star_number; // number of the star in the total stellar system
    Star_type type;

    int merge_state;
#ifdef DISCS
    int ndiscs;
#endif // DISCS

#ifdef PPISN
    int undergone_ppisn_type;
#endif // PPISN

    int dtlimiter;
    Supernova_type SN_type, was_SN; // 0 if no SN this timestep, otherwise SN number (see supernovae/sn.h)
    int prev_kick_pulse_number;
#ifdef ANTI_TZ
    int TZ_channel;
#endif // ANTI_TZ
    int sn_last_time; /* true if star went SN in last timestep */
    int white_dwarf_atmosphere_type;
#ifdef NUCSYN
#ifdef NUCSYN_STRIP_AND_MIX
    int topshell;
#endif // NUCSYN_STRIP_AND_MIX

#ifdef LOG_COMENV_RSTARS
    Stellar_type r_star_postcomenv_stellar_type1;
    Stellar_type r_star_postcomenv_stellar_type2;
    Stellar_type r_star_precomenv_stellar_type1;
    Stellar_type r_star_precomenv_stellar_type2;
#endif //LOG_COMENV_RSTARS

#ifdef  NS_BH_AIC_LOG
    Stellar_type prev_stellar_type;
#endif //NS_BH_AIC_LOG

#endif // NUCSYN

    Reject_index reject_shorten_timestep; /* reject with timestep shortening */
    Reject_index reject_same_timestep; /* reject without timestep shortening */
    Stellar_type compact_core_type;
    Nova_state nova;
    unsigned int deny_SN;

    Boolean overspin;
    Boolean hybrid_HeCOWD;
    Boolean RLOFing;
    Boolean TZO;
#ifdef WD_KICKS
    /* flags to force kick of a star */
    Boolean kick_WD, already_kicked_WD;
#endif // WD_KICKS
#ifdef LOG_SUPERNOVAE
    Boolean supernova;
#endif // LOG_SUPERNOAVE

#ifdef NUCSYN
    Boolean dont_change_stellar_type;
    Boolean rstar; /* true if star is R type */
    Boolean first_dredge_up; /* stores whether we've had first dredge up */
    Boolean second_dredge_up; /* stores whether we've had second dredge up */
    Boolean newhestar; // TRUE if never been a He star, FALSE if we have

#ifdef NUCSYN_ANGELOU_LITHIUM
    Boolean angelou_lithium_boost_this_timestep;
#endif //  NUCSYN_ANGELOU_LITHIUM

#ifdef LOG_COMENV_RSTARS
    Boolean r_star_progenitor;
#endif

#ifdef NUCSYN_FIRST_DREDGE_UP_PHASE_IN
    Boolean first_dup_phase_in_firsttime;
#endif //  NUCSYN_FIRST_DREDGE_UP_PHASE_IN

#endif  //NUCSYN


#ifdef KEMP_NOVAE
    Accretion_regime WD_accretion_regime;
#endif // KEMP_NOVAE

    Boolean blue_straggler;

#ifdef ANTI_TZ
    Boolean started_EAGB;
    Boolean TZ_object;
#endif // ANTI_TZ

#ifdef PRE_MAIN_SEQUENCE
    Boolean PMS;
#endif // PRE_MAIN_SEQUENCE

#ifdef NUCSYN

#ifdef CN_THICK_DISC
    Boolean was_blue_straggler;
#endif // CN_THICK_DISC

#ifdef NUCSYN_STRIP_AND_MIX
    Boolean strip_and_mix_disabled;
#endif // NUCSYN_STRIP_AND_MIX
#ifdef MINT
    Boolean do_burn;
#endif // MINT
#endif // NUCSYN
    Boolean in_PN;
    Boolean tide_spin_lock;
} Aligned;

struct persistent_data_t
{
    /*
     * Persistent data that can be stored between
     * stardata allocations in the same thread.
     *
     * This is useful for
     * - Output data summed over the stars (e.g. ensembles)
     * - Input data that depends on preferences
     *
     * If you add variables, you should ensure that
     * copy_persistent has been updated so that
     * deep copies of the persistent_data pointer
     * are possible.
     */
    struct cdict_t * ensemble_cdict;

    struct rinterpolate_data_t * rinterpolate_data;

#ifdef MINT
    double * MINT_MS_result;
    double * MINT_MS_result_chebyshev;
    double * MINT_MS_result_conv;
    double * MINT_GB_result;
    double * MINT_GB_result_chebyshev;
    double * MINT_GB_result_conv;
    double * MINT_CHeB_result;
    double * MINT_CHeB_result_chebyshev;
    double * MINT_CHeB_result_conv;
    double * MINT_COWD_result;
    double * MINT_COWD_result_chebyshev;
    double * MINT_COWD_result_conv;
#endif // MINT
    struct data_table_t ** cc_sn_tables;
#ifdef MINT
    struct data_table_t * MINT_mapped_tables[MINT_TABLE_NUMBER];
#endif // MINT
    struct data_table_t * Temmink2022_RLOF_mapped;
#ifdef NUCSYN
    struct data_table_t * Limongi_Chieffi_2018_mapped;
#endif // NUCSYN
    //struct data_table_t * comenv_Klencki_2020_mapped;
    struct data_table_t * comenv_Klencki_2020_Rmax;
    struct data_table_t * comenv_Klencki_2020_loglambda;
    struct data_table_t * Marassi_2019_mapped;

    struct data_table_t * Peters_grav_wave_merger_time_table;
#ifdef COMENV_WANG2016
    struct data_table_t * comenv_lambda_table;
    double * comenv_lambda_data;
#endif//COMENV_WANG2016
    double * Peters_grav_wave_merger_time_data;
#ifdef YBC
    struct rinterpolate_table_t *** YBC_tables;
    char * YBC_path;
    char * YBC_table_dataset;
#endif // YBC
#ifdef NUCSYN
    struct data_table_t * Gronow2021a_shell_ejecta;
    struct data_table_t * Gronow2021b_shell_ejecta;
    struct data_table_t * Gronow2021a_core_ejecta;
    struct data_table_t * Gronow2021b_core_ejecta;
    Isotope * Gronow2021a_isotope_list;
    Isotope * Gronow2021b_isotope_list;
    size_t Gronow2021a_nisotopes;
    size_t Gronow2021b_nisotopes;
#endif // NUCSYN
    struct data_table_t * Radice2018_ejecta;
} Aligned;


/* the model struct contains data about the model run */
/* e.g. run-time parameters such as star types,       */
/* evolution time etc. and parameters to be read in   */
/* from a config file                                 */
struct model_t
{
    /* fixed timestep algorithm settings */
    struct binary_c_fixed_timestep_t fixed_timesteps[FIXED_TIMESTEP_NUMBER];
    struct stardata_t * restore_mask;
    struct stardata_t * restore_mask_contents;
#ifdef STELLAR_POPULATIONS_ENSEMBLE
    struct cdict_t * ensemble_cdict;
#endif // STELLAR_POPULATIONS_ENSEMBLE
#ifdef SAVE_MASS_HISTORY
    struct cdict_t * mass_history;
    double I_list[10];
    unsigned int I_n;
#endif // SAVE_MASS_HISTORY

#ifdef NUCSYN

#if defined NUCSYN_YIELD_COMPRESSION || defined NUCSYN_LOG_BINARY_DX_YIELDS
    Abundance oldyields[ISOTOPE_ARRAY_SIZE];

#ifdef NUCSYN_ID_SOURCES
//    Abundance oldyieldsources[NUMBER_OF_STARS][SOURCE_NUMBER][ISOTOPE_ARRAY_SIZE];
#endif //NUCSYN_ID_SOURCES

#endif // NUCSYN_YIELD_COMPRESSION || NUCSYN_LOG_BINARY_DX_YIELDS


#endif // NUCSYN

    char logflagstrings[LOG_NFLAGS][STRING_LENGTH];
    int logorder[LOG_NFLAGS];
    int prevlogn;

    /* file pointers */
#ifdef FILE_LOG
    FILE *log_fp;
#endif //FILE_LOG
#ifdef BINARY_C_API
    FILE *api_log_fp;
#endif //BINARY_C_API

#ifdef EVENT_BASED_LOGGING
    int event_based_logging_SN_counter;
#endif // EVENT_BASED_LOGGING

    double derivative[DERIVATIVE_SYSTEM_NUMBER];
    double probability; /* the probability of the current model run */
    double time; /* the  time, was tphys in the fortran version */
    double max_evolution_time; /* Maximum evolution time, in Myr */
    double dt, dtm, true_dtm, time_remaining, dtmwas; /* timestepping */
    double dt_zoomfac, RLOF_recommended_timestep, nova_timeout;
    double prev_log_t;
#ifdef ADAPTIVE_RLOF2
    double adaptive_RLOF2_separation;
#endif
#ifdef FABIAN_IMF_LOG
    double next_fabian_imf_log_time;
    double fabian_imf_log_timestep;
#endif //FABIAN_IMF_LOG
#ifdef STELLAR_POPULATIONS_ENSEMBLE
    double lastensembleouttime;
    double gce_prevt;
#endif // STELLAR_POPULATIONS_ENSEMBLE

#ifdef HRDIAG
    double hrdiag_dt_guess;
#endif
    double previous_common_envelope_mass;
    double previous_common_envelope_angmom;

    /* Variables for RLOF interpolation */
    double tphys0, tphys00, dtm0;

#ifdef PHASE_VOLUME
    double phasevol; /* grid phase volume */
#endif // PHASE_VOLUME

#ifdef HRDIAG
    double next_hrdiag_log_time;
    double prev_hrdiag_log_time;
    double hrdiag_log_dt;
#endif //HRDIAG

#ifdef NUCSYN
#ifdef NUCSYN_YIELDS
    double next_yield_out, prev_yield_out;
#endif

#ifdef NUCSYN_GIANT_ABUNDS_LOG
    double giant_prevt, giant_dt;
#endif
#ifdef LOG_JJE
    double log_jje_tprev;
#endif
#ifdef NUCSYN_LOG_JL
    double log_jl_tprev;
#endif
#ifdef LOG_RSTARS
    double rstar_prevtime;
#endif // LOG_RSTARS
#endif // NUCSYN
    double nova_aggression_factor;
    double comenv_times[MAX_NUMBER_OF_COMENVS];

#ifdef STELLAR_POPULATIONS_ENSEMBLE
    double ensemble_store[NUMBER_OF_STARS][ENSEMBLE_NUMBER];
    double ensemble_yield[SOURCE_NUMBER];
#endif // STELLAR_POPULATIONS_ENSEMBLE


    unsigned int logstack[LOG_NFLAGS];

    Reject_index reject_shorten_timestep;
    Reject_index reject_same_timestep;
    unsigned int coalesce;

#ifdef EVENT_BASED_LOGGING
    Boolean event_based_logging_RLOF_prev_in_RLOF; // use this to check for whether we're in RLOF phase

    // RLOF stuff
    Boolean event_based_logging_RLOF;
    Boolean event_based_logging_RLOF_unstable;
    Boolean event_based_logging_RLOF_starting;
    Boolean event_based_logging_RLOF_ending;
    Boolean event_based_logging_RLOF_disk_accretion;
#endif // EVENT_BASED_LOGGING

#ifdef LOG_REPEAT_NUMBER
    int repeat_number;
#endif // LOG_REPEAT_NUMBER

    int intpol, iter; // interpolation counter
    int comenv_type; // non-zero if common envelope occurred.
    int current_solver;
    int solver_step;
    int model_number;

    /* number of rejected models */
    int n_rejected_models;
    int n_rejected_models_without_shorten;

    /* number of common envelope evolutions this star has gone through */
    int comenv_count;
    Star_number comenv_overflower;

#ifdef NO_IMMEDIATE_MERGERS
    int evolution_number;
#endif //NO_IMMEDIATE_MERGERS

    /* id number, for use in multi-star systems */
    int id_number;
    char id_string[STRING_LENGTH];
    char uuid[UUID_STR_LEN + 1];

    int rlof_type, rlof_stability, rlof_count;

    Star_number ndonor, naccretor;
#ifdef ZW2019_SNIA_LOG
    Star_number ZW2019_Hestar;
    Star_number ZW2019_COWD;
#endif //  ZW2019_SNIA_LOG

#ifdef ADAPTIVE_RLOF2
    unsigned int adaptive_RLOF2_adapt_count;
    unsigned int adaptive_RLOF2_reject_count;
    Star_number adaptive_RLOF2_overflower;
    double adaptive_RLOF2_drdm;
#endif // ADAPTIVE_RLOF2

    Boolean timestep_rejected_without_shorten;
    Boolean RLOF_start, RLOF_end;
    Boolean supernova, merged, inttry;
    Boolean sgl; /* True if the star is not a binary, false if it is */
    Boolean prec; /* precision exceeded */
    Boolean com; /* contact/comenv system */
    Boolean supedd; /* are we super-eddington? */
    Boolean restart_run[NUMBER_OF_STARS];
    Boolean rerun;
    Boolean exit_after_end;
    Boolean in_RLOF; // use this to check for whether we're in RLOF phase
    Boolean intermediate_step;
    Boolean deny_new_events;
    Boolean fixed_timestep_triggered;
    Boolean logflags[LOG_NFLAGS];
    Boolean evolution_stop;

#ifdef SYSTEM_LOGGING
    Boolean syslog_header_printed;
#endif // SYSTEM_LOGGING

#ifdef RLOF_ABC
    Boolean do_rlof_log;
#endif //RLOF_ABC

    Boolean hachisu;
#if defined NUCSYN_NOVAE && defined FILE_LOG
    Boolean novalogged;
#endif //NUCSYN_NOVAE && FILE_LOG

#ifdef EVOLUTION_SPLITTING
    Boolean doingsplit;
    Boolean split_last_time;
#endif // EVOLUTION_SPLITTING

#ifdef ADAPTIVE_RLOF2
    Boolean adapting_RLOF2;
#endif // ADAPTIVE_RLOF2

};


/*
 * The common struct contains data common to both stars.
 *
 * There should be no dynamically allocated memory, i.e.
 * no pointers, in the common struct.
 */
struct common_t
{
    /* events stack */
    struct binary_c_event_t ** events;
    struct binary_c_event_t * current_event;
    struct binary_c_event_log_t ** event_log_stack;

    /* initial system information */
    struct zero_age_system_t zero_age;

    /* orbital information */
    struct orbit_t orbit;

    /* stats for logging */
    struct diffstats_t Aligned diffstats[2];
    struct diffstats_t * diffstats_now;
    struct diffstats_t * diffstats_prev;

#ifdef ORBITING_OBJECTS
    struct orbiting_object_t * orbiting_object;
    size_t n_orbiting_objects;
#endif //  ORBITING_OBJECTS

#ifdef DISCS
    struct disc_t discs[NDISCS];
#endif //DISCS

#ifdef MEMOIZE
    struct memoize_hash_t * memo;
#endif //MEMOIZE
    struct binary_c_random_buffer_t random_buffer;
#ifdef RANDOM_SYSTEMS
    struct binary_c_random_buffer_t random_systems_buffer;
#endif
#if defined NUCSYN && defined NUCSYN_STRIP_AND_MIX
    struct data_table_t strip_and_mix_table;
#endif // NUCSYN && NUCSYN_STRIP_AND_MIX

#ifdef EVENT_BASED_LOGGING
    char event_based_logging_logstrings[EVENT_BASED_LOGGING_MAX_EVENTS][4095 + 1];
    int event_based_logging_logstring_counter;
#endif // EVENT_BASED_LOGGING

#ifdef FILE_LOG
    char * file_log_prevlabel;
#endif // FILE_LOG
    double last_reject_time;
#ifdef BSE
    double Aligned main_sequence_parameters[Aligned_size(MAIN_SEQUENCE_PARAMETERS_SIZE)];
    double Aligned giant_branch_parameters[Aligned_size(GIANT_BRANCH_PARAMETERS_SIZE)];
    double Aligned metallicity_parameters[Aligned_size(NUMBER_OF_METALLICITY_PARAMETERS)];
#endif // BSE
#ifdef NUCSYN//NUCSYN
    double Aligned dtguess[NUCSYN_NETWORK_NUMBER];
#ifdef NUCSYN_STRIP_AND_MIX
    double strip_and_mix_table_data[TAMS_NDOUBLES];
#endif // NUCSYN_STRIP_AND_MIX
#endif // NUCSYN

    Random_seed random_seed, init_random_seed;
#ifdef RANDOM_SYSTEMS
    Random_seed random_systems_seed;
#endif
    char ** argv;
#ifdef BSE
    double parameters_metallicity;
#endif // BSE
#ifdef HRDIAG
    double hrdiag_prevlog, hrdiag_filltot, hrdiag_prevbintb;
#endif // HRDIAG
    double test; // remove me after testing
#ifdef LOG_SUPERNOVAE
    double double_sn_prob;
    FILE *snfp;
#endif // LOG_SUPERNOVAE
#ifdef DETAILED_LOG
    FILE *detailed_log_fp;
#endif // DETAILED_LOG
#ifdef FILE_LOG
    double file_log_prev_rlof_exit;

#endif // FILE_LOG
    Abundance metallicity; /* The metallicity, Z, range 0-1 */
    Abundance effective_metallicity;
    double RLOF_speed_up_factor; // used in RLOF interpolation
    double max_mass_for_second_dredgeup;

    double mdot_RLOF;
    double prevt_mdot_RLOF;
#ifdef ADAPTIVE_RLOF
    /* RLOF */
    double mdot_RLOF_adaptive;
    double mdot_RLOF_BSE;
    double suggested_timestep; /* years */
    double RLOF_rwas, RLOF_rolwas;
#ifdef ADAPTIVE_RLOF_LOG
    double RLOF_log_m1was;
#endif //  ADAPTIVE_RLOF_LOG
#endif // ADAPTIVE_RLOF
    double monte_carlo_kick_normfac;
    Abundance nucsyn_metallicity; // Z to pass to SNe routines for GCE
#ifdef NUCSYN


#ifdef LOG_BARIUM_STARS
    double barium_prevt;
#endif // LOG_BARIUM_STARS

#ifdef NUCSYN_R_STAR_LOG
    double lastcallrlog;
#endif // NUCSYN_R_STAR_LOG

#ifdef NUCSYN_LONG_LOG
    double tpagbtime;
#endif // NUCSYN_LONG_LOG

#endif //NUCSYN

#ifdef DETAILED_COMPACT_LOG
    double detailed_compact_log_count
#endif // DETAILED_COMPACT_LOG

#ifdef GALACTIC_MODEL
    double galatic_coords[3];
    double distance;
#endif // GALACTIC_MODEL

#ifdef PHASE_VOLUME
    double phase_volume; // do we still need this?
#endif // PHASE_VOLUME

#ifdef CIRCUMBINARY_DISK_DERMINE
    double m_circumbinary_disk;
#endif // CIRCUMBINARY_DISK_DERMINE

#ifdef COMENV_LECTURE1
    double minrad, maxrad;
#endif // COMENV_LECTURE1

#ifdef GAIAHRD
    double gaia_Teff, gaia_L;
#endif // GAIAHRD
    double t_last_PN;

    /* properties of RLOF events */
#ifdef EVENT_BASED_LOGGING
    /* Counter */
    int event_based_logging_RLOF_counter;

    /* New RLOF episode information */
    int event_based_logging_RLOF_episode_number;

    /* initial values RLOF episode */
    double event_based_logging_RLOF_episode_initial_mass_accretor;
    double event_based_logging_RLOF_episode_initial_mass_donor;

    double event_based_logging_RLOF_episode_initial_radius_accretor;
    double event_based_logging_RLOF_episode_initial_radius_donor;

    double event_based_logging_RLOF_episode_initial_separation;
    double event_based_logging_RLOF_episode_initial_orbital_period;

    int event_based_logging_RLOF_episode_initial_stellar_type_accretor;
    int event_based_logging_RLOF_episode_initial_stellar_type_donor;

    double event_based_logging_RLOF_episode_initial_orbital_angular_momentum;
    int event_based_logging_RLOF_episode_initial_stability;

    int event_based_logging_RLOF_episode_initial_starnum_accretor;
    int event_based_logging_RLOF_episode_initial_starnum_donor;

    double event_based_logging_RLOF_episode_initial_time;
    int event_based_logging_RLOF_episode_initial_disk;

    /* final values RLOF episode */
    double event_based_logging_RLOF_episode_final_mass_accretor;
    double event_based_logging_RLOF_episode_final_mass_donor;

    double event_based_logging_RLOF_episode_final_radius_accretor;
    double event_based_logging_RLOF_episode_final_radius_donor;

    double event_based_logging_RLOF_episode_final_separation;
    double event_based_logging_RLOF_episode_final_orbital_period;

    int event_based_logging_RLOF_episode_final_stellar_type_accretor;
    int event_based_logging_RLOF_episode_final_stellar_type_donor;

    double event_based_logging_RLOF_episode_final_orbital_angular_momentum;
    int event_based_logging_RLOF_episode_final_stability;

    int event_based_logging_RLOF_episode_final_starnum_accretor;
    int event_based_logging_RLOF_episode_final_starnum_donor;

    double event_based_logging_RLOF_episode_final_time;
    int event_based_logging_RLOF_episode_final_disk;

    /* Cumulative total values RLOF episode */
    double event_based_logging_RLOF_episode_total_time_spent_masstransfer;

    double event_based_logging_RLOF_episode_total_mass_lost_from_common_envelope;
    double event_based_logging_RLOF_episode_total_mass_lost_from_accretor;
    double event_based_logging_RLOF_episode_total_mass_lost;
    double event_based_logging_RLOF_episode_total_mass_accreted;
    double event_based_logging_RLOF_episode_total_mass_transferred;

    double event_based_logging_RLOF_episode_total_mass_transferred_through_disk;
    double event_based_logging_RLOF_episode_total_time_spent_disk_masstransfer;
#endif // EVENT_BASED_LOGGING

    ticks start_tick;
    ticks system_start_tick;

    Event_counter n_events;
    Event_counter event_log_stack_n;
#ifdef DISCS
    int ndiscs;
#endif // DISCS

#ifdef ADAPTIVE_RLOF_LOG
    int used_rate, runaway_rlof, thermal_cap;
#endif // ADAPTIVE_RLOF_LOG

#ifdef LOG_SUPERNOVAE
    int sn_number, sn_count, sn_last_time_type[NUMBER_OF_STARS];
#endif // LOG_SUPERNOVAE

#ifdef DETAILED_LOG
    int detailed_log_count;
#endif // DETAILED_LOG

#ifdef FILE_LOG
    int file_log_n_rlof;
#endif // FILE_LOG

#ifdef NUCSYN

#ifdef NUCSYN_LONG_LOG
    int tpagbcount;
#endif // NUCSYN_LONG_LOG

#ifdef NUCSYN_NETWORK_STATS
    int nucsyn_network_total_success_count,
        nucsyn_network_total_failure_count;
#endif //  NUCSYN_NETWORK_STATS

    int nucsyn_log_count;
    int nucsyn_short_log_count;
#endif // NUCSYN

    int argc;



    Boolean prevflags[NLOG_LABEL];
    Boolean lockflags[NUMBER_OF_STARS][LOCK_NFLAGS + 1];
    Boolean interpolate_debug;
    Boolean diffstats_set;
    Boolean logflag;

    Boolean had_event;
#ifdef SINGLE_STAR_LIFETIMES
    Boolean done_single_star_lifetime;
#endif // SINGLE_STAR_LIFETIMES

#ifdef CANBERRA_PROJECT
    Boolean canberra_done_first_timestep;
#endif // CANBERRA_PROJECT

#ifdef FABIAN_IMF_LOG
    Boolean fabian_logged_zero;
#endif //  FABIAN_IMF_LOG

#ifdef PRE_MAIN_SEQUENCE
    Boolean pms[NUMBER_OF_STARS];
#endif // PRE_MAIN_SEQUENCE

#ifdef NS_NS_BIRTH_LOG
    Boolean nsns;
#endif // NS_NS_BIRTH_LOG

#ifdef COMENV_LECTURE1
    Boolean star1_agb;
#endif // COMENV_LECTURE1

#ifdef HRDIAG
    Boolean hrdiag_done_zams, hrdiag_use2;
#endif //HRDIAG

#ifdef VISCOUS_RLOF_TRIGGER_SMALLER_TIMESTEP
    Boolean viscous_RLOF_wants_smaller_timestep;
#endif // VISCOUS_RLOF_TRIGGER_SMALLER_TIMESTEP

#ifdef LOG_SUPERNOVAE
    Boolean gone_sn[NUMBER_OF_STARS];
#endif // LOG_SUPERNOVAE

    Boolean sn_sc_header;
#ifdef FILE_LOG
    Boolean file_log_cached;
#endif // FILE_LOG

#if defined NUCSYN && defined NUCSYN_STRIP_AND_MIX
    Boolean strip_and_mix_is_setup ;
#endif // NUCSYN && NUCSYN_STRIP_AND_MIX

    Boolean RLOF_do_overshoot;
} Aligned;

/* The stardata struct contains the data we're interested in */
struct stardata_t
{
    struct stardata_t * parent;
    struct stardata_t * ancestor;
    struct persistent_data_t * persistent_data;
    struct preferences_t * preferences;
    Alignas struct store_t * store;
    struct tmpstore_t * tmpstore;
    struct stardata_t * previous_stardata;
    struct stardata_t ** previous_stardatas;
    struct stardata_t ** stardata_stack;
    struct stardata_t * pre_events_stardata;
#ifdef CODESTATS
    struct codestats_t * codestats;
#endif // CODESTATS
    struct common_t common;
    struct star_t star[NUMBER_OF_STARS];
    struct model_t model;
#ifdef BATCHMODE
    jmp_buf batchmode_setjmp_buf;
#endif // BATCHMODE
    double warmup_timer_offset;
    int error_code;
    Star_type type;
    unsigned int n_previous_stardatas;
    unsigned int n_stardata_stack;
    Boolean cpu_is_warm;
    Boolean evolving;
} Aligned;
#define __HAVE_STARDATA_T


struct RLOF_orbit_t
{
    /*
     * RLOF mass transfer in one orbit.
     *
     * dM_RLOF_lose (was dm1 in BSE) is the mass that is lost from star 1
     * during an orbit. This number is negative (mass is lost).
     *
     * dM_RLOF_transfer (was dm2 in BSE) is the mass transferred in RLOF
     * in an orbit, i.e. this is what the accreting star tries
     * to accrete. This number is positive.
     *
     * dM_RLOF_accrete (was dm22) is the mass actually accreted
     * during RLOF in an orbit, which is not the same as
     * dM_RLOF_transfer if there are, e.g., novae.
     * This number is positive.
     *
     * Usually dm_RLOF_transfer == dm_RLOF_accrete
     */
    double dM_RLOF_lose, dM_RLOF_transfer, dM_RLOF_accrete;

    /*
     * Also required are the other mass losses during an orbit,
     * e.g. wind loss,
     * although these are generally small compared to the RLOF rate.
     *
     * NB These are negative in the case of (wind) loss.
     */
    double dM_other[NUMBER_OF_STARS];
} Aligned;



#ifdef DISCS
struct binary_system_t
{
    double m1;// greater of the masses (grams)
    double m2;// lesser of the masses (grams)
    double mtot; // total mass (grams)
    double reduced_mass; // grams
    double q; // m2/m1
    double separation; // orbital semi-major axis, cm

    /* NB distances are relative to the centre of mass */
    double aL1; // L1 radius, cm
    double aL2; // L2 radius, cm
    double aL3; // L3 radius, cm
    double a1; // a1 = position of star 1
    double a2; // a2 = position of star 2
    double Jorb; // cgs
    double eccentricity; // no units
    double L; // erg/s
    double flux; // erg / s / cm^2
    double LFUV; // FUV luminosity, erg/s
    double LEUV; // EUV luminosity, erg/s
    double LX; // X-ray luminosity, erg/s
    double omega; // angular frequency (rad/s)
    double orbital_period;
    double Teff[NUMBER_OF_STARS];
    double luminosity[NUMBER_OF_STARS];
    double Rstar;
    double torqueF;
    Boolean RLOF;
} Aligned;
#endif // DISCS


struct coordinate_t
{
    double x;
    double y;
    double z;
};


struct kick_system_t
{
    /* separation at the moment of explosion */
    double separation;

    /* eccentric and mean anomalies */
    double eccentric_anomaly;
    double mean_anomaly;

    /* relative stellar velocity : magntiude and square */
    double orbital_speed;
    double orbital_speed_squared;

    /* beta is the angle between r and v */
    double sinbeta;
    double cosbeta;

    /* kick speed and squared */
    double kick_speed;
    double kick_speed_squared;

    /* phi is the angle between the kick and the orbital plane */
    double phi;
    double sinphi;
    double cosphi;

    /*
     * theta is the angle perpendicular to the angle
     * between the kick and the line between the stars.
     * NB in Hurley+2002 this is the angle omega.
     */
    double omega;
    double sinomega;
    double cosomega;

    /* new orbital speed */
    double new_orbital_speed;
    double new_orbital_speed_squared;

    /* new specific angular momentum */
    double hn;
    double hn_squared;

    /* bound before explosion? */
    Boolean bound_before_explosion;
} Aligned;

struct opacity_t
{
    double temperature;
    double density;
    double Z;
    double Fe;
    double H;
    double He;
    double C;
    double N;
    double O;
    double alpha;
};

struct equation_of_state_t
{
    double error;

    double X; // hydrogen mass fraction
    double Y; // helium mass fraction
    double Z; // metallicity
    double temperature;
    double density;
    double lnf;
    double P; // pressure (cgs)
    double Pgas; // gas pressure (cgs)
    double Prad; // radiation pressure (cgs)

    /* ionisation fractions */
    double xh1;
    double xhe1;
    double xhe2;

    /* number of densities of species */
    double nH2; // hydrogen molecules
    double nHI; // neutral hydrogen
    double nHII; // ionized hydrogen
    double nHeI; // neutral helium
    double nHeII; // ionized helium
    double nHeIII; // doubly-ionized helium
    double ne; // free electrons
    double nt; // total number of particles

    /* temperature gradients */
    double gradad; // dlnT/dlnP at const S
    double gradrad;

    /* adiabatic sound speed */
    double cs_ad;

    /* Cp, Cv : specific heats */
    double Cp; // dU/dt const P
    double Cv; // dU/dT const rho (or volume)

    /* internal energy */
    double U;

    /* thermodynamic derivatives */
    double dP_dT_rho; // dP/dT const rho (or volume)
    double dP_drho_T;// dP/drho const T
    double dlnrho_dlnT_P; // d(ln rho)/d(ln T) const P

    int include_radiation_in_ionization_region;
};



struct envelope_shell_t
{
    double m; /* M(r) */
    double dm; /* dM */
    double r;
    double tau;
    double temperature;
    double density;
    double pressure;
    double grad;
    double gradad;
    double gradrad;
    double xion_h1;
    double xion_he1;
    double xion_he2;
    double X;
    double Y;
    double Z;
};

struct envelope_t
{
    struct envelope_shell_t * shells;
    double logl;
    double logt0;
    double logteff;
    double m;
    double fm;
    double x;
    double y;
    double z;
    double alpha;
    double surface_density;
    double surface_tau;
    double tmax;
    double tauf;
    double E_bind, E1, E2, E3;
    double lambda_bind;
    double moment_of_inertia;
    int nshells;
};

struct stardata_dump_t
{
    int dump_format;
    int svn_revision;
    char git_revision[STRING_LENGTH];
    char versionstring[50];
    char versioninfo[MEGABYTE];
    size_t sizeof_stardata_t;
    size_t sizeof_preferences_t;
    struct preferences_t preferences;
    struct stardata_t stardata;
    struct store_t store;
    struct tmpstore_t tmpstore;
};


struct GSL_args_t
{
    va_list args;
    brent_function func;
    int error;
    Boolean uselog;
};

#include "binary_c_event_structures.h"


/* derivative checker function */
typedef Boolean (derivative_check_function) (struct stardata_t *,
                                             void *, /* data, set by user */
                                             const double, /* previous_value */
                                             const double, /* new value */
                                             const double, /* derivative */
                                             const double, /* timestep */
                                             const Derivative, /* derivative number */
                                             const Derivative_group /* derivative group */);

/* lsoda struct */
struct lsoda_t
{
    int max_steps;
    int      g_nyh /* = 0*/, g_lenyh /* = 0*/;

    /* newly added  variables */

    int      ml, mu, imxer;
    int      mord[3];// = {0, 12, 5};
    double   sqrteta, *yp1, *yp2;
    double   sm1[13];// = {0., 0.5, 0.575, 0.55, 0.45, 0.35, 0.25, 0.2, 0.15, 0.1, 0.075, 0.05, 0.025};

    /*  variables for lsoda() */

    double   ccmax, el0, h, hmin, hmxi, hu, rc, tn;
    int      illin /*= 0*/, init /*= 0*/, mxstep, mxhnil, nhnil, ntrep /*= 0*/, nslast, nyh, ierpj, iersl,
             jcur, jstart, kflag, l, meth, miter, maxord, maxcor, msbp, mxncf, n, nq, nst,
             nfe, nje, nqu;
    double   tsw, pdnorm;
    int      ixpr /*= 0*/, jtyp, mused, mxordn, mxords;

    /* no  variable for prja(), solsy() */
    /*  variables for stoda() */

    double   conit, crate, el[14], elco[13][14], hold, rmax, tesco[13][4];
    int      ialth, ipup, lmax, nslp;
    double   pdest, pdlast, ratio, cm1[13], cm2[6];
    int      icount, irflag;

    /*  variables for various vectors and the Jacobian. */

    double **yh, **wm, *ewt, *savf, *acor;
    int     *ipvt;

    /* function to use to calculate the Jacobian */
    _lsoda_J Jacobian;
};

/*
 * Context struct for CVODE calls
 */
#ifdef __HAVE_LIBSUNDIALS_CVODE__
struct cvode_context_t
{
    N_Vector y;
    void * cvode_mem;
    SUNLinearSolver LS;
    SUNMatrix A;
    N_Vector  abstol;
};
#endif // __HAVE_LIBSUNDIALS_CVODE__

struct Ivanova2013_data_t
{
    double zeta;
};

struct Matsumoto2022_data_t
{
    double vbar_E;
    double vbar_E_factor;
    double R0;
    double rho_i;
    double f_adiabatic;
};

struct binary_c_stack_t
{
    void ** items;
    size_t count;
    size_t allocated;
};


/*
 * Structure to hold the information about each argument
 */
struct argstore_t
{
    struct preferences_t * preferences;
    struct cmd_line_arg_t * cmd_line_args;
    unsigned int arg_count;
    unsigned int ** cmd_line_args_on_parse;
    unsigned int * n_cmd_line_args_on_parse;
};
#endif /*BINARY_STRUCTURES_H*/
