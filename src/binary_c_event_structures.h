#pragma once
#ifndef BINARY_C_EVENT_STRUCTURES_H
#define BINARY_C_EVENT_STRUCTURES_H
/*
 * The binary_c stellar population nucleosynthesis framework.
 *
 * Copyright Robert Izzard 2000-2025
 *
 * Contact rob.izzard@gmail.com
 *
 * Code repository:
 * https://gitlab.com/binary_c
 *
 * Documentation is in doc/
 * Code is in src/
 *
 * To build, run
 *
 * ---
 *
 * meson setup builddir
 * cd builddir
 * ninja binary_c_install
 *
 * ---
 *
 * Please see the files README, LICENCE and CHANGES,
 * and the doc/ directory for documentation which is mirrored
 * at https://binary_c.gitlab.io
 *
 **********************
 *
 * Purpose:
 *
 * Header file containing the binary_c event-handling structures.
 *
 * When you define a new type of event, you should set a
 * structure in here to contain the event data.
 *
 **********************
 */

typedef unsigned int Event_notice;

#define EVENT_HANDLER_ARGS                              \
    void * const /* event pointer */,                   \
        struct stardata_t * const ,                     \
        void * /* data */

/* event handler type */
typedef void (*Event_handler_function)(EVENT_HANDLER_ARGS);

/* new event structure */
struct binary_c_event_t {
    Event_type type;
    Event_handler_function (*func)(EVENT_HANDLER_ARGS);
    Event_handler_function (*erase_func)(EVENT_HANDLER_ARGS);
    void * data;
    char * caller;
    Boolean unique;
    Boolean skip;
};

/* event log structure */
struct binary_c_event_log_t {
    Event_type type;
    void * data;
    void (*freefunc)(void**);
    int label;
    Boolean processed;
};

/* data structure to handle supernova events */
struct binary_c_new_supernova_event_t {
    struct stardata_t * pre_explosion_stardata;
    struct star_t * pre_explosion_star;
    struct star_t * post_explosion_star;
    double mass_ejected;
};

/* data structure to handle common envelope events */
struct binary_c_new_common_envelope_event_t {
    struct star_t * donor;
    struct star_t * accretor;
};

struct binary_c_unstable_RLOF_event_t {
    struct star_t * donor;
    struct star_t * accretor;
    int instability;
};

struct binary_c_nova_event_t {
    struct star_t * donor;
    struct star_t * accretor;
    double accretion_rate;
    double steady_burn_rate;
    double new_envelope_rate;
    double f;
    double dMH;
    double dMHe;
    double recurrence_time;
    Nova_type type;
};

struct binary_c_overspin_event_t {
    struct star_t * star;
};

#endif // BINARY_C_EVENT_STRUCUTRES_H
