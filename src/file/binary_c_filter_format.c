#include "../binary_c.h"
No_empty_translation_unit_warning;


/*
 * Function to reformat a format string according to
 * the given algorithm. Returns a pointer to the new
 * format string or the original string if there is
 * no change.
 *
 * The memory allocated by the new format string (returned)
 * must later be freed by the user.
 */

//#define __DEBUG

char * binary_c_filter_format(const int algorithm,
                              const char * const Restrict format)
{
    char * format_copy; // returned

    if(algorithm == FILE_FILTER_NOTHING)
    {
        /*
         * algorithm is FILE_FILTER_NOTHING : do nothing
         */
        format_copy = (char*) format;
    }
    else
    {
        /*
         * Some other algorithm.
         */
        size_t count = strlen(format) + 1; // current length of format string (+1 for NULL)
        const size_t size = sizeof(char)*count;
        format_copy = Malloc(size);

        if(format_copy == NULL)
        {
            Exit_binary_c_no_stardata(
                BINARY_C_ALLOC_FAILED,
                "Failed to allocate memory for memo in binary_c_filter_format\n");
        }
        else
        {

            memcpy(format_copy,format,size);
#ifdef __DEBUG
            /* macro to return 1 if string X is NULL-terminated, 0 otherwise */
#define NULLterm(X) ((*((X)+strlen(X))=='\0') ? 1 : 0)

            printf("format_copy = %p\n",format_copy);
#endif

            if(algorithm == FILE_FILTER_STRIP_ARGUMENTS)
            {
                /* remove "x=" from "x=%..." style arguments */
#ifdef __DEBUG
                int i = 0;
#endif
                char * arg_start = NULL;

                /*
                 * Pointers to the start and end of the string
                 */
                char * this = format_copy;
                char * end  = this + count - 1;

                /*
                 * Loop while we have characters to scan
                 */
                while(this < end && *this != '\0')
                {
#ifdef __DEBUG
                    printf("i=%d count=%ld strlen=%ld PRE  : \"%s\" NULL term ? %d\n",
                           i++,
                           count,
                           strlen(format_copy),
                           format_copy,
                           NULLterm(format_copy));
#endif
                    const char * prev = this > format_copy ? (this - 1) : format_copy;

#ifdef __DEBUG
                    printf("this \"%c\" (prev = %c) arg_start=%p, at start? %d\n",
                           *this,
                           prev ? (*prev) : '0',
                           arg_start,
                           this==format_copy ? 1 : 0
                        );
#endif
                    if(*this != ' ' &&
                       (this==format_copy || *prev == ' '))
                    {
                        arg_start = this;
#ifdef __DEBUG
                        printf("ARG_START at %p\n",arg_start);
#endif
                    }
                    else if(arg_start!=NULL && (*this) == '=')
                    {
                        /* end of argument */
                        const char * arg_end = this;
                        const size_t n = arg_end - arg_start + 1;
                        memmove(arg_start,arg_end+1,end - arg_end);//strlen(arg_end));
                        end -= n;
                        count -= n;
                        this = arg_start;
                        arg_start = NULL;
#ifdef __DEBUG
                        printf("ARG_END at %p, remove %ld chars\n",arg_end,n);
#endif
                    }

                    this++;
#ifdef __DEBUG
                    printf("i=%d count=%ld strlen=%ld POST : \"%s\" NULL term? %d\n",
                           i,
                           count,
                           strlen(format_copy),
                           format_copy,
                           NULLterm(format_copy));
#endif
                }
            }
        }
    }
    return format_copy;
}
