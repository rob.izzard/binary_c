#include "../binary_c.h"
No_empty_translation_unit_warning;

/*
 * Restart a stream.
 *
 * Usually this just means fseek, but if
 * it's a pipe, we have to restart it.
 */

void restart_stream(struct stardata_t * const stardata,
                    struct binary_c_stream_t * const stream)
{
    if(stream->type == STREAM_TYPE_PIPE)
    {
        /*
         * Pipe: seek is impossible, so restart
         */
        fclose(stream->fp);
        struct binary_c_stream_t * newstream =
            open_stream(stardata,
                        stream->name);
        stream->fp = newstream->fp;
        Safe_free(newstream);
    }
    else if(stream->type == STREAM_TYPE_FILE)
    {
        /*
         * Normal file, just seek
         */

        fseek(stream->fp,0,SEEK_SET);
    }
}
