#include "../binary_c.h"
No_empty_translation_unit_warning;

/*
 * Subroutine to load data into a table,
 * and call rinterpolate for later interpolation.
 *
 * You should not call this subroutine directly:
 * use the Load_table() macro.
 *
 * input:
 *
 * table : a pointer to a data_table_t pointer where
 *                the final table is to be stored
 *                (note: is a pointer to a pointer)
 *
 * filename : location of the file containing the data
 *
 * wanted_columns : an array of integers which are the
 *                  columns to be selected from the data.
 *                  If NULL or first column is -1, use all
 *                  columns.
 *
 * nparams : number of parameter columns
 *
 * setup_rinterpolate: if TRUE set up the table in
 *                     rinterpolate_data
 */

void load_table(struct stardata_t * const stardata,
                struct data_table_t ** const table,
                char * const filename,
                int * wanted_columns,
                int n_wanted_columns,
                const int nparams,
                const size_t skiplines,
                const Boolean setup_rinterpolate)
{
    /*
     * Load data
     */
    size_t lc,n,maxwidth;
    Dprint("load data as doubles\n");
    double * all_data = load_data_as_doubles(stardata,
                                             filename,
                                             &lc,
                                             &n,
                                             &maxwidth,
                                             ' ',
                                             skiplines);

    if(all_data == NULL)
    {
        Exit_binary_c(BINARY_C_FILE_OPEN_ERROR,
                      "Could not load file \"%s\" as doubles. Does the file exist? Is it buggy?\n",
                      filename);
    }
    else
    {
        const size_t ndata =
            (wanted_columns == NULL || wanted_columns[0] == -1) ?
            (maxwidth - (size_t)nparams) :
            (n_wanted_columns - (size_t)nparams);
        Dprint("maxwidth = %zu, n_wanted_columns = %d, nparams = %d, ndata = %zu\n",
               maxwidth,
               n_wanted_columns,
               nparams,
               ndata);
        Boolean alloc_wanted_columns;
        if(wanted_columns == NULL ||
           wanted_columns[0] == -1)
        {
            /*
             * We want all columns
             */
            Dprint("want all columns : set n_wanted_columns = %zu\n",maxwidth);
            n_wanted_columns = maxwidth;
            wanted_columns = Malloc(sizeof(int) * maxwidth);
            for(int j=0; j<n_wanted_columns; j++)
            {
                wanted_columns[j] = j;
            }
            alloc_wanted_columns = TRUE;
        }
        else
        {
            alloc_wanted_columns = FALSE;
        }
        Dprint("made wanted cols, alloced? %s\n",Yesno(alloc_wanted_columns));

        /*
         * Reduce to columns we want
         */
        double * reduced_data = Malloc(n_wanted_columns * lc * sizeof(double));
        double * reduced_p = reduced_data;
        Dprint("reduce\n");
        for(size_t i=0; i<lc; i++)
        {
            double * const all_line = all_data + i * maxwidth;
            for(int j=0; j<n_wanted_columns; j++)
            {
                *(reduced_p++) = *(all_line + wanted_columns[j]);
            }
        }
        Safe_free(all_data);

        /*
         * Convert to a rinterpolate data table
         */
        Dprint("make rinterpolate table (nparams %d, ndata %zu, lc %zu)\n",
               nparams,
               ndata,
               lc);
        NewDataTable_from_Pointer(
            reduced_data,
            *table,
            nparams,
            ndata,
            lc
            );

        if(setup_rinterpolate == TRUE)
        {
            /*
             * make rinterpolate version
             */
            Dprint("call Interpolate to set up the table\n");
            Interpolate(*table,
                        NULL,
                        NULL,
                        FALSE);
        }

        Dprint("free memory\n");
        if(alloc_wanted_columns == TRUE)
        {
            Safe_free(wanted_columns);
        }
        Dprint("loaded\n");
    }
}
