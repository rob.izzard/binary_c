#include "../binary_c.h"
No_empty_translation_unit_warning;

/*
 * Function to load a table of floating-point numbers
 * into a chunk of memory from file at filename.
 *
 * returns a pointer to the array of doubles containing the
 * data
 *
 * *lines and *length are set to the number of lines in the
 * file and the number of doubles read in
 *
 * The delimiter defaults to ' ' (space) if not set
 *
 * skiplines is the number of lines we should skip at the
 * start of the file. Set this to zero to skip none.
 *
 * maxwidth is the maximum number of data items in a line
 * of the table.
 */
double * load_data_as_doubles(struct stardata_t * const stardata,
                              char * filename_in,
                              size_t * const lines,
                              size_t * const length,
                              size_t * const maxwidth,
                              char delimiter,
                              const size_t skiplines)
{
    /*
     * Defaults
     */
    if(maxwidth!=NULL)
    {
        *maxwidth = 0;
    }
    if(lines!=NULL)
    {
        *lines = 0;
    }
    if(length!=NULL)
    {
        *length = 0;
    }
    if(delimiter == 0)
    {
        delimiter = ' ';
    }

    /*
     * Determine location of file
     */
    char * filename = search_file(stardata,
                                  filename_in);
    Dprint("open stream at %s (located from %s)\n",
           filename,
           filename_in);

    if(filename != NULL)
    {
        struct binary_c_stream_t * stream =
            open_stream(stardata,
                        filename);

        if(stream == NULL)
        {
            Exit_binary_c(BINARY_C_FILE_OPEN_ERROR,
                          "could not open file at %s as a stream",
                          filename);
        }

        Dprint("get linecount...");
        const long int flc = file_linecount(stardata,
                                            filename,
                                            '#',
                                            skiplines);
        Dprint("linecount = %ld\n",flc);



        if(flc < 0)
        {
            Exit_binary_c(BINARY_C_FILE_OPEN_ERROR,
                          "file at %s has %ld lines of data: this should be positive",
                          filename,
                          flc);
        }
        const size_t nl = (size_t) flc;
        if(lines != NULL)
        {
            *lines = nl;
        }

        restart_stream(stardata,stream);

        /*
         * Allocate space for the data table
         */
        double * data = NULL;
        struct string_array_t * string_array = new_string_array(0);
        char * line = NULL;
        size_t data_size = 0;
        size_t file_line_number = 0;
        size_t data_line_number = 0;
        size_t prealloc = 0;
        while(data_line_number < nl)
        {
            Dprint("file %s line %zu data %zu max %zu : read? %s\n",
                   filename_in,
                   file_line_number,
                   data_line_number,
                   nl,
                   Yesno(file_line_number >= skiplines));

            if(file_line_number < skiplines)
            {
                /* skip until next newline */
                while(1)
                {
                    const int i = getc(stream->fp);
                    if(i == EOF || i < 0)
                    {
                        /* end of file */
                        break;
                    }
                    else
                    {
                        /* newline check */
                        const char c = (i > CHAR_MAX) ? (char)(i - (UCHAR_MAX + 1)) : (char)i;
                        if(c == '\n')
                        {
                            break;
                        }
                    }
                }
            }
            else
            {
                char * nextline = file_splitline(stardata,
                                                 stream->fp,
                                                 &line,
                                                 string_array,
                                                 0,
                                                 delimiter,
                                                 prealloc,
                                                 0);

                if(line != NULL)
                {
                    Dprint("Have line of strings (len %zd)",
                           string_array->n);

                    const size_t was = prealloc;
                    prealloc = Max((size_t)string_array->n,
                                   string_array->nalloc);
                    Dprint("Prealloc was %zu now %zu (nstrings = %zd, alloced = %zu)\n",
                           was,
                           prealloc,
                           string_array->n,
                           string_array->nalloc);

                    if(string_array->n <= -1)
                    {
                        Exit_binary_c(BINARY_C_DATA_FAILED_TO_LOAD,
                                      "Found string_array->n < 0 when trying to load data from %s line %zu\n",
                                      filename,
                                      data_line_number);
                    }
                    else if(string_array->n > 0 && line[0] == '#')
                    {
                        /* comment line : skip */
                        Dprint("line %zu is a comment",
                               data_line_number);
                    }
                    else
                    {
                        /*
                         * Convert array of strings to array of doubles.
                         *
                         * Strings that are unable to be converted are set to NAN.
                         */
                        struct double_array_t * double_array =
                            string_array_to_double_array(string_array,NAN);

                        Dprint("load double array %p %p : doubles n=%zd, strings n=%zd\n",
                               (void*)double_array,
                               (void*)double_array->doubles,
                               double_array->n,
                               string_array->n);

                        /*
                         * Check for nan and inf
                         */
#ifdef NANCHECKS
                        if(check_double_array_for_nan(double_array) == TRUE)
                        {
                            Exit_binary_c(BINARY_C_EXIT_NAN,
                                          "Found NaN when loading line %zu from %s, first nan at index %zd\n",
                                          data_line_number,
                                          filename,
                                          first_nan_in_double_array(double_array));
                        }
#endif//NANCHECKS
                        if(check_double_array_for_inf(double_array) == TRUE)
                        {
                            Exit_binary_c(BINARY_C_EXIT_INF,
                                          "Found inf when loading line %zu from %s, first inf at index %zd\n",
                                          data_line_number,
                                          filename,
                                          first_inf_in_double_array(double_array));
                        }

                        /*
                         * Set the data in place
                         */
                        data = Realloc(data,
                                       sizeof(double)*(data_size + (size_t)double_array->n));
                        /*
                          memset(data + data_size,
                          0,
                          sizeof(double) * double_array->n);
                        */
                        memcpy(data + data_size,
                               double_array->doubles,
                               sizeof(double) * double_array->n);
                        data_size += double_array->n;

                        *maxwidth = Max(*maxwidth,
                                        (size_t)double_array->n);


                        free_double_array(&double_array);
                        data_line_number++;
                    }
                    Safe_free(nextline);
                }
                else
                {
                    data_line_number++;
                }
            }
            file_line_number++;
        }


        if(length != NULL)
        {
            *length = data_size;
        }

        /*
         * Close and free pointers and return the data
         */
        close_stream(&stream);
        Safe_free(filename);
        free_string_array(&string_array);
        return data;
    }
    else
    {
        return NULL;
    }
}
