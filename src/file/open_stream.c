#include "file/file_macros.h"
#include "../binary_c.h"
No_empty_translation_unit_warning;



struct binary_c_stream_t *
open_stream(struct stardata_t * const Restrict stardata,
            const char * const Restrict filename)
{
    /*
     * Open file at filename for reading, automatically using
     * gzip, bzip etc. if it has an appropriate extension.
     *
     * The commands for these extensions are defined
     * in file_macros.def.
     *
     * Returns the stream and sets type to the
     * appropriate STREAM_TYPE_*
     *
     * Also, you may have to escape the filename
     * if it contains odd characters, as we use a shell
     * to open external commands when decompressing.
     *
     * Note: we do not check if the file exists,
     *       use check_file_exists() for this.
     */

    /*
     * Get file extension
     */
    char * extension = strrchr(filename,'.');

    /*
     * Open the file
     */
    struct binary_c_stream_t * stream =
        Malloc(sizeof(struct binary_c_stream_t));
    stream->name = (char*)filename;
    stream->format = FILE_FORMAT_NATIVE;

    Boolean found = FALSE;
    if(extension != NULL)
    {
        extension++;
        Dprint("File extension %s\n",extension);

#undef X
#define X(ID,NAME,CMD,...)                                              \
        if(found==FALSE)                                                \
        {                                                               \
            const char * const file_extensions[] = { __VA_ARGS__ };     \
            for(size_t i = 0;                                           \
                found == FALSE &&                                       \
                    i < Array_size(file_extensions);                    \
                i++)                                                    \
            {                                                           \
                if(Strings_equal(file_extensions[i],                    \
                                 extension))                            \
                {                                                       \
                    found = TRUE;                                       \
                    char * cmd;                                         \
                    if(asprintf(&cmd,                                   \
                                "%s \"%s\"",                            \
                                #CMD,                                   \
                                filename)<0)                            \
                    {                                                   \
                        Exit_binary_c(BINARY_C_MALLOC_FAILED,           \
                                      "File \"%s\" has extension \"%s\" failed to assign open command string", \
                                      filename,                         \
                                      extension);                       \
                    }                                                   \
                    else                                                \
                    {                                                   \
                        stream->fp = popen(cmd,"r");                    \
                        stream->type = STREAM_TYPE_PIPE;                \
                        stream->format = FILE_FORMAT_##ID;              \
                        Safe_free(cmd);                                 \
                    }                                                   \
                }                                                       \
            }                                                           \
        }                                                               \

        MAKE_FILE_FORMATS_LIST;
        FILE_FORMATS_LIST;
    }

    if(found==FALSE)
    {
        /*
         * Unknown extension : open as normal file
         */
        stream->fp = fopen(filename,"r");
        stream->type = STREAM_TYPE_FILE;
        stream->format = FILE_FORMAT_NATIVE;
    }

    /*
     * On error, free the stream struct to return NULL
     */
    if(stream->fp == NULL)
    {
        Safe_free(stream);
    }

    /*
     * Return the stream
     */
    return stream;
}
