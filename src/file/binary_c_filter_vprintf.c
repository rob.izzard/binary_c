#include "../binary_c.h"
No_empty_translation_unit_warning;


/*
 * Wrapper around binary_c_fprintf which processes
 * the input format string according to some required algorithm,
 * then sends the new format string to binary_c_fprintf.
 */

int Gnu_format_args(3,0) binary_c_filter_vprintf(struct binary_c_file_t * const Restrict file,
                                                 const int algorithm,
                                                 const char * const Restrict format,
                                                 va_list args)
{
    va_list args_copy;
    va_copy(args_copy,args);

    /*
     * Reformat
     */
    char * format_copy = binary_c_filter_format(algorithm,
                                                format);


    /*
     * Call binary_c_fprintf with the updated
     * format string
     */
    int ret = binary_c_vprintf(file,
                               format_copy,
                               args_copy);

    /*
     * Clean up
     */
    va_end(args_copy);
    va_end(args);
    Safe_free(format_copy);

    return ret;
}
