#include "../binary_c.h"
No_empty_translation_unit_warning;


//#define __DEBUG
/*
 * Wrapper around binary_c_fprintf which processes
 * the input format string according to some required algorithm,
 * then sends the new format string to binary_c_fprintf.
 */
int binary_c_filter_fprintf(struct binary_c_file_t * const Restrict file,
                            const int algorithm,
                            const char * const Restrict format,
                            ...)
{
    int ret = 0;
    if(file!=NULL)
    {
        va_list args;
        va_start(args,format);

#ifdef MEMOIZE
        /*
         * If we can, memozie the call. Note that Memoize returns
         * a pointer to the hashed content, so we can just free the pointer
         * returned by binary_c_filter_format.
         */
        if(file->memo==NULL)
        {
            /*
             * Make space for the memo, and set to free results pointers
             */
            if(memoize_initialize(&file->memo) != NULL)
            {
                file->memo->free_results_pointers = 1;
            }
            else
            {
                Exit_binary_c_no_stardata(
                    BINARY_C_ALLOC_FAILED,
                    "Failed to allocate memory for memo in binary_c_filter_fprintf\n");
            }
        }

        const char * const format_copy =
            Memoize(
                file->memo,
                "filtf",
                10,
                array,
                char,
                1,
                format,
                char*,
                1,
                binary_c_filter_format(algorithm,format)
                );

        ret = binary_c_vprintf(file,format_copy,args);
#else
        /*
         * Memoize not available.
         */
        ret = binary_c_filter_vprintf(file,algorithm,format,args);
#endif // MEMOIZE

        va_end(args);
    }
    return ret;
}
