#pragma once
#ifndef BINARY_C_FILE_MACROS_H
#define BINARY_C_FILE_MACROS_H

#include "file_function_macros.h"
#include "file_macros.def"

#define Safe_fclose(X) if((X) != NULL)fclose(X)

/*
 * File formats
 */
MAKE_FILE_FORMATS_LIST
#undef X
#define X(ID,NAME,CMD,...) FILE_FORMAT_##ID,
enum {
    FILE_FORMATS_LIST
    FILE_FORMAT_NUMBER
};


#endif // BINARY_C_FILE_MACROS_H
