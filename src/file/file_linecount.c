#include "../binary_c.h"
No_empty_translation_unit_warning;


long int file_linecount(struct stardata_t * const Restrict stardata,
                        const char * const Restrict filename,
                        const char comment_char,
                        const size_t skiplines)
{
    /*
     * Count lines in a file, ignoring those
     * that start with comment_char, if given,
     * and skipping the first skiplines (which do
     * not go into the count)
     *
     * If comment_char is 0 it is ignored.
     *
     * Returns the number of lines, or -1 on error.
     */
    long int nlines;
    struct binary_c_stream_t * stream =
        open_stream(stardata,
                    filename);
    if(stream != NULL)
    {
        nlines = stream_linecount(stream,
                                  comment_char,
                                  skiplines);
        close_stream(&stream);
    }
    else
    {
        /*
         * Error: return -1
         */
        nlines = -1;
    }
    return nlines;
}
