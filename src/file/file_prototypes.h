#ifndef FILE_H
#define FILE_H

struct binary_c_file_t * binary_c_fopen(struct stardata_t * Restrict stardata,
                                        const char * Restrict const path,
                                      const char * Restrict const mode,
                                      const size_t maxsize);
int binary_c_fprintf(struct binary_c_file_t * const Restrict file,
                     const char * const Restrict format,
                     ...) Gnu_format_args(2,3);
int binary_c_fclose(struct binary_c_file_t ** const file);
int binary_c_fflush(const struct binary_c_file_t * const Restrict file);
int binary_c_filter_fprintf(struct binary_c_file_t * const Restrict file,
                            const int filter_algorithm,
                            const char * const Restrict format,
                            ...) Gnu_format_args(3,4);
int binary_c_vprintf(struct binary_c_file_t * Restrict file,
                     const char * const Restrict format,
                     va_list args) Gnu_format_args(2,0);
int binary_c_filter_vprintf(struct binary_c_file_t * const Restrict file,
                            const int algorithm,
                            const char * const Restrict format,
                            va_list args) Gnu_format_args(3,0);
char * binary_c_filter_format(const int algorithm,
                              const char * const Restrict format);
long int file_linecount(struct stardata_t * const Restrict stardata,
                        const char * const Restrict filename,
                        const char comment_char,
                        const size_t skiplines);
char * file_splitline(struct stardata_t * const stardata,
                      FILE * const Restrict fp,
                      char ** line,
                      struct string_array_t * const string_array,
                      const char comment_char,
                      const char delimiter,
                      const size_t prealloc,
                      const size_t max);

char * check_file_exists(char * const Restrict filename);

/* stream functions */
struct binary_c_stream_t *
open_stream(struct stardata_t * const Restrict stardata,
            const char * const Restrict filename);
int close_stream(struct binary_c_stream_t ** stream);
long int stream_linecount(struct binary_c_stream_t * const stream,
                          const char comment_char,
                          const size_t skiplines);
void restart_stream(struct stardata_t * const stardata,
                    struct binary_c_stream_t * const stream);
char * data_filename(struct stardata_t * const stardata Maybe_unused,
                     const char * const data_directory,
                     const char * const filename);
double * load_data_as_doubles(struct stardata_t * const stardata,
                              char * const filename,
                              size_t * const lines,
                              size_t * const length,
                              size_t * const maxwidth,
                              char delimiter,
                              const size_t skiplines);

struct string_array_t ** load_data_as_strings(struct stardata_t * const stardata,
                                              char * const filename_in,
                                              size_t * const lines,
                                              size_t * const length,
                                              size_t * const maxwidth,
                                              char delimiter,
                                              const size_t skiplines,
                                              const size_t max);
char * search_file(struct stardata_t * const stardata,
                   char * const filename);
Boolean check_dir_exists(char * const Restrict path);

char * binary_c_getline(struct binary_c_file_t * const fp);

void load_and_remap_table(struct stardata_t * const stardata,
                          struct data_table_t ** const mapped_table,
                          char * const filename,
                          int * wanted_columns,
                          int * const new_resolutions,
                          int n_wanted_columns,
                          int nparams,
                          const size_t skiplines,
                          const Boolean setup_rinterpolate);
void load_table(struct stardata_t * const stardata,
                struct data_table_t ** const table,
                char * const filename,
                int * wanted_columns,
                int n_wanted_columns,
                const int nparams,
                const size_t skiplines,
                const Boolean setup_rinterpolate);


#endif// FILE_H
