#include "../binary_c.h"
No_empty_translation_unit_warning;

/*
 * Close the stream, automatically using
 * pclose or fclose depending on the type.
 */

int close_stream(struct binary_c_stream_t ** stream)
{
    int ret = -1;
    if(stream != NULL &&
       *stream != NULL &&
       (*stream)->fp != NULL)
    {
        if((*stream)->type == STREAM_TYPE_PIPE)
        {
            ret = pclose((*stream)->fp);
        }
        else if((*stream)->type == STREAM_TYPE_FILE)
        {
            ret = fclose((*stream)->fp);
        }
        Safe_free(*stream);
    }
    return ret;
}
