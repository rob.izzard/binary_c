#include "../binary_c.h"
No_empty_translation_unit_warning;

/*
 * Subroutine to load data and then remap it
 * using rinterpolate.
 *
 * You should not call this subroutine directly:
 * use the Load_and_remap_table() macro.
 *
 * input:
 *
 * mapped_table : a pointer to a data_table_t pointer where
 *                the final table is to be stored
 *                (note: is a pointer to a pointer)
 *
 * filename : location of the file containing the data
 *
 * wanted_columns : an array of integers which are the
 *                  columns to be selected from the data.
 *                  If NULL or first column is -1, use all
 *                  columns.
 *
 * new_resolutions : an array of integers specifying
 *                   the new resolution of tabular parameters.
 *
 * setup_rinterpolate : if true, call Interpolate() to set the
 *                      table in rinterpolate_data
 *
 * On error, mapped_table is NULL and we exit.
 */

void load_and_remap_table(struct stardata_t * const stardata,
                          struct data_table_t ** const mapped_table,
                          char * const filename,
                          int * wanted_columns,
                          int * const new_resolutions,
                          int n_wanted_columns,
                          int nparams,
                          const size_t skiplines,
                          const Boolean setup_rinterpolate)
{
    /*
     * Load data
     */
    size_t lc,n,maxwidth;
    Dprint("load data as doubles : filename \"%s\", n_wanted_columns %d, nparams %d, skiplines %zu, setup_rinterpolate? %s\n",
           filename,
           n_wanted_columns,
           nparams,
           skiplines,
           Truefalse(setup_rinterpolate));
    double * all_data = load_data_as_doubles(stardata,
                                             filename,
                                             &lc,
                                             &n,
                                             &maxwidth,
                                             ' ',
                                             skiplines);

    if(all_data == NULL)
    {
        Exit_binary_c(BINARY_C_FILE_OPEN_ERROR,
                      "Could not load file \"%s\" as doubles. Does the file exist? Is it buggy?\n",
                      filename);
    }
    else
    {
        const size_t ndata =
            (wanted_columns == NULL || wanted_columns[0] == -1) ?
            (maxwidth - (size_t)nparams) :
            (n_wanted_columns - (size_t)nparams);
        Dprint("maxwidth = %zu, n_wanted_columns = %d, nparams = %d, ndata = %zu\n",
               maxwidth,
               n_wanted_columns,
               nparams,
               ndata);
        Boolean alloc_wanted_columns;
        if(wanted_columns == NULL ||
           wanted_columns[0] == -1)
        {
            /*
             * We want all columns
             */
            Dprint("want all columns : set n_wanted_columns = %zu\n",maxwidth);
            n_wanted_columns = maxwidth;
            wanted_columns = Malloc(sizeof(int) * maxwidth);
            for(int j=0; j<n_wanted_columns; j++)
            {
                wanted_columns[j] = j;
            }
            alloc_wanted_columns = TRUE;
        }
        else
        {
            alloc_wanted_columns = FALSE;
        }
        Dprint("made wanted cols, alloced? %s\n",Yesno(alloc_wanted_columns));

        /*
         * Reduce to columns we want
         */
        double * reduced_data = Malloc(n_wanted_columns * lc * sizeof(double));
        double * reduced_p = reduced_data;
        Dprint("reduce\n");
        for(size_t i=0; i<lc; i++)
        {
            double * const all_line = all_data + i * maxwidth;
            for(int j=0; j<n_wanted_columns; j++)
            {
                *(reduced_p++) = *(all_line + wanted_columns[j]);
            }
        }
        Safe_free(all_data);

        /*
         * Convert to a rinterpolate data table
         */
        Dprint("make rinterpolate table (nparams %d, ndata %zu, lc %zu)\n",
               nparams,
               ndata,
               lc);
        struct data_table_t * non_orthogonal_data_table = NULL;
        NewDataTable_from_Pointer(
            reduced_data,
            non_orthogonal_data_table,
            nparams,
            ndata,
            lc
            );

        if(setup_rinterpolate == TRUE)
        {
            /*
             * make rinterpolate version
             */
            Dprint("call Interpolate to set up the table\n");
            Interpolate(non_orthogonal_data_table,
                        NULL,
                        NULL,
                        FALSE);
        }

        /*
         * extract correct rinterpolate table
         */
        Dprint("get rinterpolate table struct pointer\n");
        struct rinterpolate_table_t * unmapped_table_pointer =
            rinterpolate_table_struct(stardata->persistent_data->rinterpolate_data,
                                      non_orthogonal_data_table->data,
                                      NULL);



        /*
         * construct new resolution array in appropriate way
         * for librinterpolate
         */
        Dprint("make new resolution array\n");
        rinterpolate_Boolean_t * change_resolution = Malloc(sizeof(rinterpolate_Boolean_t)*nparams);
        rinterpolate_float_t * new_resolutions_f = Malloc(sizeof(rinterpolate_float_t)*nparams);
        for(int j=0; j<nparams; j++)
        {
            new_resolutions_f[j] = (rinterpolate_float_t) new_resolutions[j];
            change_resolution[j] = new_resolutions[j] > 0 ? TRUE : FALSE;
            Dprint("col %d : change resolution %s to %g\n",
                   j,
                   Yesno(change_resolution[j]),
                   new_resolutions_f[j]);
        }

        /*
         * map to orthogonal table
         */
        Dprint("map to orthogonal table\n");
        struct rinterpolate_table_t * const orthogonal_table =
            rinterpolate_map_right_columns(stardata->persistent_data->rinterpolate_data,
                                           unmapped_table_pointer,
                                           change_resolution,
                                           new_resolutions_f,
                                           NULL);


        /*
         * set orthogonal table and its parameters
         */
        Dprint("set table and its parameters\n");
        NewDataTable_from_Pointer(orthogonal_table->data,
                                  *mapped_table,
                                  orthogonal_table->n,
                                  orthogonal_table->d,
                                  orthogonal_table->l);

        /*
         * clean original non-orthogonal table (no longer required)
         */
        Dprint("delete unmapped table\n");
        rinterpolate_delete_table(stardata->persistent_data->rinterpolate_data,
                                  unmapped_table_pointer,
                                  NULL);
        Safe_free(unmapped_table_pointer->label);
        Safe_free(unmapped_table_pointer);
        Delete_data_table(non_orthogonal_data_table);

        Dprint("free memory\n");
        if(alloc_wanted_columns == TRUE)
        {
            Safe_free(wanted_columns);
        }
        Safe_free(change_resolution);
        Safe_free(new_resolutions_f);
        Dprint("loaded and remapped\n");
    }
}
