#include "../binary_c.h"
No_empty_translation_unit_warning;

char * check_file_exists(char * const Restrict filename)
{
    /*
     * Check that a file exists at filename.
     *
     * If it does not, check files that have the same
     * name with an added extension.
     *
     * Returns a pointer to a string storing the location at
     * which the file was found, or NULL on failure.
     *
     * The string should be freed by the user.
     */
    char * ret = NULL;
    if(access(filename,F_OK) == 0)
    {
        /*
         * found immediately
         */
        if(asprintf(&ret,"%s",filename) == -1)
        {
            /*
             * failure
             */
            ret = NULL;
        }
    }
    else
    {
        /*
         * check extensions
         */
        MAKE_FILE_FORMATS_LIST;
#undef X
#define X(ID,NAME,CMD,...)                                              \
        if(ret == NULL)                                                 \
        {                                                               \
            char * file_extensions[] = { __VA_ARGS__ };                 \
            for(size_t i=0;                                             \
                ret == NULL &&                                          \
                i<Array_size(file_extensions);                          \
                i++)                                                    \
            {                                                           \
                char * test;                                            \
                const int j = asprintf(&test,                           \
                                       "%s.%s",                         \
                                       filename,                        \
                                       file_extensions[i]);             \
                if(j != -1 &&                                           \
                   access(test,R_OK) == 0)                              \
                {                                                       \
                    ret = test;                                         \
                }                                                       \
                else                                                    \
                {                                                       \
                    Safe_free(test);                                    \
                }                                                       \
            }                                                           \
        }                                                               \

        FILE_FORMATS_LIST;
    }
    return ret;
}
