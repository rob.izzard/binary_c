#include "../binary_c.h"
No_empty_translation_unit_warning;


int Gnu_format_args(2,0) binary_c_vprintf(struct binary_c_file_t * const Restrict file,
                                          const char * const Restrict format,
                                          va_list args)
{
    /*
     * like fprintf to a binary_c file
     *
     * checks whether we're allowed to write more
     * characters to the file, if so, do it.
     * Returns the number of characters written.
     *
     * On failure, just don't write (return 0).
     */
    int written = 0;
    if(file != NULL &&
       file->fp != NULL)
    {
        va_list copy;
        va_copy(copy,args);

        /*
         * Determine how many characters
         * we would write
         */
        const int n = (file->maxsize==0) ? -1 :
            vsnprintf(NULL,0,format,copy);
        va_end(copy);

        /*
         * If allowed, write
         */
        if(n==-1 ||
           n + file->size < file->maxsize)
        {
            vfprintf(file->fp,format,args);
            file->size += n;
            written += n;
        }
    }
    return written;
}
