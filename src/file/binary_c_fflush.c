#include "../binary_c.h"
No_empty_translation_unit_warning;


int binary_c_fflush(const struct binary_c_file_t * const file)
{
    /*
     * Flush a binary_c_file_t pointer.
     *
     * Return what is returned from fflush,
     * or -1 if file or file->fp is NULL
     */
    return
        (file == NULL || file->fp == NULL)
        ? -1
        : fflush(file->fp);
}
