#include "../binary_c.h"
No_empty_translation_unit_warning;

char * search_file(struct stardata_t * const stardata Maybe_unused,
                   char * const filename)
{
    /*
     * Function to search for filename in various places
     * e.g. $BINARY_C/, the current directory, and the
     * directory in which binary_c was built,.
     *
     * Returns the filename that is found in a string
     * which should be freed by the user.
     *
     * Returns NULL if not found.
     */

    /*
     * Check the file using its full path
     */
    char * correct_filename = check_file_exists(filename);
    if(correct_filename != NULL)
    {
        return correct_filename;
    }
    else
    {
        const char * const paths[] = {
            getenv("BINARY_C_DATA"),
            getenv("BINARY_C"),
            ".",
            BINARY_C_SRC,
            BINARY_C_SRC
        };

        for(int i=0; i<(int)Array_size(paths); i++)
        {
            if(paths[i] != NULL)
            {
                /*
                 * n is the number of bytes of paths[i] that is used
                 *
                 * Usually this is all of them (strlen(paths[i]))
                 * but in one case we try BINARY_C_SRC with /src truncated.
                 */
                const size_t n = (i==4 && strlen(paths[4])>4) ? (strlen(paths[4])-4) : strlen(paths[i]);
                char * f = NULL;
                char * path = strndup(paths[i],n);
                if(path != NULL &&
                   asprintf(&f,
                            "%s%c%s",
                            path,
                            PATH_SEPARATOR,
                            filename) > 0)
                {
                    Dprint("Search for file at path \"%s\"\n",paths[i]);
                    /*
                     * Check the file exists: this also
                     * will try other files with known
                     * extensions.
                     */
                    correct_filename = check_file_exists(f);

                    if(correct_filename != NULL)
                    {
                        Safe_free(f);
                        Safe_free(path);
                        return correct_filename;
                    }
                }
                if(i==0)
                {
                    Exit_binary_c(BINARY_C_FILE_OPEN_ERROR,
                                  "You have set BINARY_C_DATA to \"%s\" and yet I cannot find the file at \"%s\". This is an error.",
                                  getenv("BINARY_C_DATA"),
                                  f);
                }
                Safe_free(path);
                Safe_free(f);
            }
        }
    }
    return NULL;
}
