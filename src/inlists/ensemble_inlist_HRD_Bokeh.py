############################################################
#
# Population ensemble inlist for binary_c's ensemble_manager
#
############################################################
#
# This is an inlist to make HRDs so Rob can test the Bokeh
# plotter.
#
############################################################
from os.path import expanduser
import sys
project = 'HRD-Bokeh-binary'

home = expanduser("~")
YBC_path = home + '/git/ybc_tables'
YBC_listfile = home + '/git/ybc_tables/YBC/Avodonnell94Rv3.1YBC_Tlusty.list'
YBC_instruments = "GAIA,JWST,SLOAN"

############################################################
#
# binary_c arguments : these control the physics and ensemble
#
#
# binary_c_args are passed into binary_c as arguments,
#
# e.g.
#    binary_c ... alpha_ce 1.0 lambda_ce 0.5 ...
#
# you should give each a list of possible values.
#
# Note that 'defaults' should take an empty list, because this
# just uses all the binary_c defaults.
#
# You can specify the default value of each variable your
# lists, but this will not be set hence we run as few
# populations as possible.
#
# The entries in binary_c_args are:
# 1) A key,value pair where the key is the arg name, and the
#    value is a list of values to which this arg is set.
#
# 2) A key,value pair where the key is the arg name, and the
#    value is a tuple which in turn contains as elements:
#    [0] a list of values to which this arg is set
#    [1] a dictionary which is expanded for each of these values
#
# This way you can easily nest your variables to any extent.
#
# e.g.
#
# This changes the metallicity of each run
#
# { 'metallicity' : [1e-4, 1e-3, 1e-2] }
#
# i.e. 3 combinations.
#
# This changes the alpha_ce for each of those metallicities,
# i.e. 9 combinations.
#
# { 'metallicity' :
#    (
#       [ 1e-4, 1e-3, 1e-2 ],
#       {
#           'alpha_ce' : [0.1,0.5,1.0]
#       }
#    )
# }
#
############################################################
# YOU NEED TO decide which parameters you want to set and change
variable_binary_c_args = {
    'metallicity' : [1e-4,1e-3,1e-2,2e-2], # just an example
    'force_circularization_on_RLOF' : False,
    'tidal_strength_factor' : 1.0,
    #'tides_variable_filename' : None,
    'qcrit_nuclear_burning' : 'QCRIT_TEMMINK2022',
}

##################################################
#
# ensemble arguments, these control the ensemble's grid and
# other miscellaneous things
#
############################################################
# YOU NEED TO check these parameters make sense for you
#
binaries = False
ensemble_args = {
    'dists' : 'Moe', # Moe, 2008(+), 2018(+)
    'binaries' : binaries,
    'r1' : 1000 if binaries == False else 10, # M1
    'r2' : 30, # q
    'rP' : 50, # period
    'recc' : 30, # resolution in eccentricity
    'M1spacing' : 'logM1', # const_dt, logM1
    'fsample' : 0.25,
    'normalize' : True,
    'mmin' : 0.1,
    'mmax' : 80.0,
    'log_runtime_systems' : 0,
    'run_zero_probability_system' : 0,
    'combine_ensemble_with_thread_joining' : True,

    # if using Moe's distribution, we want a
    # list of mass spacings that's a bit different to normal
    'const_dt_mass_spacings' : '(({mmin},1,-0.1),(0.8,1.5,0.05),(1,30,-0.5),(30,{mmax},1))',

    #################################################
    'YBC_path' : YBC_path,
    'YBC_listfile' :  YBC_listfile,
    'YBC_instruments' : YBC_instruments,

    ##################################################
    'verbosity' : 1,
    'log_dt' : 5, # 300, # log to stdout every log_dt seconds
    'log_args' : 1, # do not save args by default
    'log_args_dir' :  '/tmp/bincargs' , # save args to here

    ############################################################
    # ensemble options
    'ensemble' : 1,
    'ensemble_defer' : 1,

    # we don't want time-resolved output
    'ensemble_logtimes' : 0,
    'ensemble_startlogtime' : 0.1, # start at log10(t/Myr)=0.1
    'ensemble_dt' : 1e3, # not required when ensemble_logtimes==1
    'ensemble_logdt' : 0.1, # not required when ensemble_logtimes==0

    # all filters off except Bokeh CMD and HRD
    'ensemble_filters_off' : 1,
    'ensemble_filter_HRD_BOKEH' : 1,
    'ensemble_filter_CMD_BOKEH' : 1,

    'save_ensemble_chunks' : False,
    'num_cores' :  """dict hostname: {
        'default' : 1,
        'starling' : 8,
        'astro1' : 24,
        'astro2' : 24,
        'CLWS00203' : 5,
    }""",
    'do_dry_run' : True, # not required
}


############################################################
#
# HPC arguments
#
############################################################
# YOU NEED TO check these make sense on your cluster
#
HPC_options = {
    'njobs' : 8,
    'memory' : '30000MB',
    'warn_max_memory' : '40000MB',
    'max_time' : '48:00:00',# HH:MM::SS
    'directory' : None,
    'condor_requirements' : '',
    'slurm_partition' : 'shared',
    'cluster_type' : None,
}
