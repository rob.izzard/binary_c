############################################################
#
# Population ensemble inlist for binary_c's ensemble_manager
#
############################################################

############################################################
#
# This is the generic "GCE" (Galactic Chemical Evolution)
# /cosmology yieldsets project
#
project = "GCE"

############################################################
#
# binary_c arguments : these control the physics and ensemble
#
#
# binary_c_args are passed into binary_c as arguments,
#
# e.g.
#    binary_c ... alpha_ce 1.0 lambda_ce 0.5 ...
#
# you should give each a list of possible values.
#
# Note that 'defaults' should take an empty list, because this
# just uses all the binary_c defaults.
#
# You can specify the default value of each variable your
# lists, but this will not be set hence we run as few
# populations as possible.
#
# The entries in binary_c_args are:
# 1) A key,value pair where the key is the arg name, and the
#    value is a list of values to which this arg is set.
#
# 2) A key,value pair where the key is the arg name, and the
#    value is a tuple which in turn contains as elements:
#    [0] a list of values to which this arg is set
#    [1] a dictionary which is expanded for each of these values
#
# This way you can easily nest your variables to any extent.
#
# e.g.
#
# This changes the metallicity of each run
#
# { 'metallicity' : [1e-4, 1e-3, 1e-2] }
#
# i.e. 3 combinations.
#
# This changes the alpha_ce for each of those metallicities,
# i.e. 9 combinations.
#
# { 'metallicity' :
#    (
#       [ 1e-4, 1e-3, 1e-2 ],
#       {
#           'alpha_ce' : [0.1,0.5,1.0]
#       }
#    )
# }
#
############################################################

# binaries is a local variable
#
# if True we include parameters that change binary systems
#    and set up the Moe distributions for binaries.
#
# if False we include only standard single-(and binary-) star
#    physics, and run only single stars.
#
binaries = True

binary_c_args = {
    'metallicity' : (
        # range of metallicities
        #[1e-4,1e-3,0.004,0.008,0.01,0.02,0.03],
        [0.02], # for testing
        {

            # winds and mass loss
            'wind_mass_loss' : [
                #'WIND_ALGORITHM_HURLEY2002',
                #'WIND_ALGORITHM_SCHNEIDER2018',
                'WIND_ALGORITHM_BINARY_C_2020'
                                ],
            'gbwind' : (
                [
                    'GB_WIND_REIMERS',
                 #   'GB_WIND_SCHROEDER_CUNTZ_2005',
                 #   'GB_WIND_GOLDMAN_ETAL_2017'
                ],
                {
                    'gb_reimers_eta' : [
                        #0.1,
                        0.5,
                        #1.0
                    ]
                }
            ),
            'tpagbwind' : [
                'TPAGB_WIND_VW93_KARAKAS',
            #    'TPAGB_WIND_VW93_ORIG',
             #   'TPAGB_WIND_REIMERS',
              #  'TPAGB_WIND_BLOECKER'
            ],

            'rotationally_enhanced_mass_loss' : [
                'ROTATIONALLY_ENHANCED_MASSLOSS_NONE',
               # 'ROTATIONALLY_ENHANCED_MASSLOSS_LANGER_FORMULA',
                #'ROTATIONALLY_ENHANCED_MASSLOSS_ANGMOM',
                #'ROTATIONALLY_ENHANCED_MASSLOSS_LANGER_FORMULA_AND_ANGMOM'
            ],

            # massive-star supernovae
            'BH_prescription' : [
                #'BH_HURLEY2002',
                #'BH_BELCZYNSKI',
                #'BH_SPERA2015',
                'BH_FRYER12_DELAYED',
                #'BH_FRYER12_RAPID',
                #'BH_FRYER12_STARTRACK'
            ],
            'sn_kick_dispersion_IBC' : [
                190.0,
                #250.0
            ],
            'sn_kick_dispersion_II' : [
                190.0,
                #250.0
            ],

            # nucleosynthesis
            'delta_mcmin' : [
                -0.1,
                #0.0
            ],
            'lambda_min' : [
                #0.0,
                0.5,
                #0.8
            ],
            'mc13_pocket_multiplier' : [
                #0.1,
                1.0,
                #10.0
            ],

            # binary physics:
            # this is only included if binaries==True
            **({
                # Companion-reinforced attrition
                'CRAP_parameter' : [
                    0.0,
                    #1e2,
                    #1e3,
                    #1e4
                ],

                # low/intermediate-mass-star Ia supernovae
                'mass_accretion_for_eld' : [
                    -1.0,
                    #0.05,
                    #0.15
                ],
                'type_Ia_MCh_supernova_algorithm' : [
                    #'TYPE_IA_MCH_SUPERNOVA_ALGORITHM_DD2',
                    'TYPE_IA_MCH_SUPERNOVA_ALGORITHM_SEITENZAHL2013_AUTOMATIC'
                ],

                # NSNS, NSBH mergers
                'NS_merger_yield_algorithm' : [
                    'NS_MERGER_ALGORITHM_RADICE2018'
                    ],
                'merger_mass_loss_fraction_by_stellar_type_NS' : [
                    'MERGER_MASS_LOSS_FRACTION_RADICE2018'
                    ],
                'Radice2018_EOS' : [
                    'RADICE2018_EOS_DD2'
                    ],

                # turn thermohaline mixing on/off when accreting
                'no_thermohaline_mixing' : [
                    0,
                    #1
                ],

                # combinations of different alpha,lambda for common envelope
                'alpha_ce' : (
                    [
                        #0.1,
                        0.5,
                        #1.0
                    ],
                    {
                        'lambda_ce' : [
                            -1.0,
                            #0.5
                        ],
                        'lambda_ionisation' : [
                            0.0,
                            #0.1,
                            #0.5
                        ],
                    }
                ),
                'post_ce_adaptive_menv' : [
                    'False',
                    #    'True'
                ],

                # tides
                'tidal_strength_factor' : [
                    #1e-2,
                    1.0,
                    #1e2
                ],
                'E2_prescription' : [
                    #'E2_HURLEY_2002',
                    'E2_IZZARD'
                ],

                # RLOF
                'RLOF_method' : [
                    #'RLOF_METHOD_BSE',
                    'RLOF_METHOD_CLAEYS'
                ],
                'use_periastron_Roche_radius' : [
                    'False',
                    #'True'
                ],


                'WRLOF_method' : [
                    #'WRLOF_NONE',
                    'WRLOF_Q_DEPENDENT',
                    #'WRLOF_QUADRATIC'
                ],
            } if binaries else {}),
        }
    )
}

##################################################
#
# ensemble arguments, these control the ensemble's grid and
# other miscellaneous things
#
############################################################
# YOU NEED TO check these parameters make sense for you
#
ensemble_args = {
    'dists' : 'Moe', # Moe, 2008(+), 2018(+)
    'binaries' : binaries,
    'r' : 40, # resolution of M1 (if not const_dt), M2 and period, e.g. 100
    'M1spacing' : 'const_dt', # const_dt, logM1
    'fsample' : 0.25,
    'normalize' : True,
    'mmax' : 80.0,
    'log_runtime_systems' : 0,
    'run_zero_probability_system' : 0,
    'combine_ensemble_with_thread_joining' : True,

    # if using Moe's distribution, we want a
    # list of mass spacings that's a bit different to normal
    'const_dt_mass_spacings' : '((0.07,1,-0.1),(0.8,1.5,0.05),(1,30,-0.5),(30,80,1))',

    ##################################################
    'verbosity' : 1,
    'log_dt' : 300, # 300, # log to stdout every log_dt seconds
    'log_args' : 1, # do not save args by default
    'log_args_dir' : '/tmp/binc',
    'log_failed_systems' : True,
    'log_failed_systems_dir': "/tmp/binerror",

    ############################################################
    # ensemble options
    'ensemble' : 1,
    'ensemble_defer' : 1,
    'ensemble_logtimes' : 1,
    'ensemble_startlogtime' : 0.1, # only want old stars
    #'ensemble_dt' : 1e3, # not required when ensemble_logtimes==1
    'ensemble_logdt' : 0.1, # not required when ensemble_logtimes==0
    'ensemble_filters_off' : 1,
    'save_ensemble_chunks' : False,

    # filters required for GCE
    'ensemble_filter_CHEMICAL_YIELDS' : 1, # require chemical yields
    'ensemble_filter_SCALARS' : 1, # required scalar number counts
    'ensemble_filter_TEST' : 1, # include test data
    'ensemble_filter_INITIAL_DISTRIBUTIONS' : 1, # always save initial distributions
    'ensemble_filter_SUPERNOVAE' : 1,

    'dry_run_num_cores' : 8,
    'num_cores' : 18,
    'do_dry_run' : True,
    'comenv_algorithm' : 'COMENV_ALGORITHM_BINARY_C',
    'exit_immediately' : True,
    #'verbosity' : 2,
}

# SNIa jobs

# 1:
# try Perets
data = {
    # 'perets' : {
    #     'WDWD_merger_algorithm' : 'WDWD_MERGER_ALGORITHM_PERETS2019',
    #     'postfix' : 'WDWD_merger_algorithm_Perets_2019',
    # },

    # # 2:
    # # try triggering sub-MCh with unstable RLOF
    # 'unstable_trigger' : {
    #     'unstable_RLOF_can_trigger_SNIa' : True,
    #     'triggered_SNIa_algorithm' : 'TYPE_IA_SUPERNOVA_ALGORITHM_LIVNE_ARNETT_1995',
    #     'postfix' : 'unstable_RLOF_trigger'
    # },

    # # 3:
    # # try Chen
    # 'chen' : {
    #     'WDWD_merger_algorithm' : 'WDWD_MERGER_ALGORITHM_CHEN2016',
    #     'postfix' : 'WDWD_merger_algorithm_Chen2016',
    # },

    # # 4:
    # # try Ruiter
    # 'ruiter' : {
    #     'WDWD_merger_algorithm' : 'WDWD_MERGER_ALGORITHM_RUITER2013',
    #     'eta_violent_WDWD_merger' : 0.75,
    #     'postfix' : 'WDWD_merger_algorithm_Ruiter',
    # },

    # # 5:
    # # try Temmink's qcrit
    # 'temmink' : {
    #     'qcrit_HG' : 'QCRIT_TEMMINK2022',
    #     'qcrit_GB' : 'QCRIT_TEMMINK2022',
    #     'qcrit_CHeB' : 'QCRIT_TEMMINK2022',
    #     'qcrit_EAGB' : 'QCRIT_TEMMINK2022',
    #     'qcrit_TPAGB' : 'QCRIT_TEMMINK2022',
    #     'qcrit_HeHG' : 'QCRIT_TEMMINK2022',
    #     'qcrit_HeGB' : 'QCRIT_TEMMINK2022',
    #     'postfix' : 'qcrit_Temmink',
    # },

    # # 6:
    # # Check whether COWD_to_ONeWD_accretion_rate
    # # should be implemented
    # 'COONe_disabled' : {
    #     'COWD_to_ONeWD_accretion_rate' : 0.0,
    #     'postfix' : 'No_COWD_to_ONeWD_accretion',
    # },

    # # 7: Disable DD
    # 'DD_disabled' : {
    #     'mass_accretion_for_COWD_DD' : -1.0,
    #     'mass_accretion_for_ONeWD_DD' : -1.0,
    #     'postfix' : 'noDD',
    # },

    # # 8: low-mass merger explosions
    # 'COCO_explosions_above_M0.9' : {
    #     'COWD_COWD_explode_above_mass' : 0.9,
    #     'postfix' : 'COWDCOWD_explode_above_0.9',
    # },

    # 9: try new SNIa yields on COWDs
    'DD-COWD' : {
        'type_Ia_DD_yield_algorithm' : 'TYPE_IA_SUPERNOVA_ALGORITHM_GRONOW_2021',
        'mass_accretion_for_COWD_DD' : 0.05,
        'minimum_age_for_COWD_DD' : 1e2, # Myr
        'minimum_mass_for_COWD_DD' : 0.8,
        'postfix' : 'DD-COWD'
    },

    # 10: try new SNIa yields on COWDs and ONeWDs
    'DD-COWD-ONeWD' : {
        'type_Ia_DD_yield_algorithm' : 'TYPE_IA_SUPERNOVA_ALGORITHM_GRONOW_2021',
        'mass_accretion_for_COWD_DD' : 0.05,
        'minimum_age_for_COWD_DD' : 1e2, # Myr
        'minimum_mass_for_COWD_DD' : 0.8,
        'mass_accretion_for_ONeWD_DD' : 0.05,
        'minimum_age_for_ONeWD_DD' : 1e2, # Myr
        'minimum_mass_for_ONeWD_DD' : 0.9,
        'postfix' : 'DD-COWD-ONeWD'
    },

    # 11: try new SNIa yields on COWDs with Temmink
    'DD-COWD-Temmink' : {
        'type_Ia_DD_yield_algorithm' : 'TYPE_IA_SUPERNOVA_ALGORITHM_GRONOW_2021',
        'mass_accretion_for_COWD_DD' : 0.05,
        'minimum_age_for_COWD_DD' : 1e2, # Myr
        'minimum_mass_for_COWD_DD' : 0.8,
        'qcrit_HG' : 'QCRIT_TEMMINK2022',
        'qcrit_GB' : 'QCRIT_TEMMINK2022',
        'qcrit_CHeB' : 'QCRIT_TEMMINK2022',
        'qcrit_EAGB' : 'QCRIT_TEMMINK2022',
        'qcrit_TPAGB' : 'QCRIT_TEMMINK2022',
        'qcrit_HeMS' : 'QCRIT_TEMMINK2022',
        'qcrit_HeHG' : 'QCRIT_TEMMINK2022',
        'qcrit_HeGB' : 'QCRIT_TEMMINK2022',
        'postfix' : 'DD-COWD + Temmink'
    },

    # 12: try new SNIa yields on COWDs and ONeWDs with Temmink
    'DD-COWD-ONeWD-Temmink' : {
        'type_Ia_DD_yield_algorithm' : 'TYPE_IA_SUPERNOVA_ALGORITHM_GRONOW_2021',
        'mass_accretion_for_COWD_DD' : 0.05,
        'minimum_age_for_COWD_DD' : 1e2,
        'minimum_mass_for_COWD_DD' : 0.8,
        'mass_accretion_for_ONeWD_DD' : 0.05,
        'minimum_age_for_ONeWD_DD' : 1e2,
        'minimum_mass_for_ONeWD_DD' : 0.9,
        'qcrit_HG' : 'QCRIT_TEMMINK2022',
        'qcrit_GB' : 'QCRIT_TEMMINK2022',
        'qcrit_CHeB' : 'QCRIT_TEMMINK2022',
        'qcrit_EAGB' : 'QCRIT_TEMMINK2022',
        'qcrit_TPAGB' : 'QCRIT_TEMMINK2022',
        'qcrit_HeMS' : 'QCRIT_TEMMINK2022',
        'qcrit_HeHG' : 'QCRIT_TEMMINK2022',
        'qcrit_HeGB' : 'QCRIT_TEMMINK2022',
        'postfix' : 'DD-COWD-ONeWD + Temmink'
    },


}

if os.getenv('SNIA_JOB_LIST'):
    print(" ".join(data.keys()))
    sys.exit()

SNIa_job = os.getenv('SNIA_JOB')
if SNIa_job:
#    print(f"SN IA JOB \"{SNIa_job}\"");
    del data[SNIa_job]['postfix']
    _update = data[SNIa_job]
    ensemble_args.update(_update)
#print(f"UPDATE {_update}")

############################################################
#
# HPC arguments
#
############################################################
HPC_options = {
    'njobs' : 16,
    'memory' : '2000MB',
    'warn_max_memory' : '4000MB',
    'max_time' : 10000,
    'directory' : None,
    'condor_requirements' : '',
    'slurm_partition' : 'shared',
    'cluster_type' : None,
}
