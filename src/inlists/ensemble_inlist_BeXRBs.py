############################################################
#
# Population ensemble inlist for binary_c's ensemble_manager
#
############################################################

############################################################
#
# Be-XRB project with
#
project = "BeXRBs"

############################################################
#
# binary_c arguments : these control the physics and ensemble
#
#
# binary_c_args are passed into binary_c as arguments,
#
# e.g.
#    binary_c ... alpha_ce 1.0 lambda_ce 0.5 ...
#
# you should give each a list of possible values.
#
# Note that 'defaults' should take an empty list, because this
# just uses all the binary_c defaults.
#
# You can specify the default value of each variable your
# lists, but this will not be set hence we run as few
# populations as possible.
#
# The entries in binary_c_args are:
# 1) A key,value pair where the key is the arg name, and the
#    value is a list of values to which this arg is set.
#
# 2) A key,value pair where the key is the arg name, and the
#    value is a tuple which in turn contains as elements:
#    [0] a list of values to which this arg is set
#    [1] a dictionary which is expanded for each of these values
#
# This way you can easily nest your variables to any extent.
#
# e.g.
#
# This changes the metallicity of each run
#
# { 'metallicity' : [1e-4, 1e-3, 1e-2] }
#
# i.e. 3 combinations.
#
# This changes the alpha_ce for each of those metallicities,
# i.e. 9 combinations.
#
# { 'metallicity' :
#    (
#       [ 1e-4, 1e-3, 1e-2 ],
#       {
#           'alpha_ce' : [0.1,0.5,1.0]
#       }
#    )
# }
#
############################################################

binary_c_args = {
    # range of metallicities
    'metallicity' :(
        [0.02]
    )
}

##################################################
#
# ensemble arguments, these control the ensemble's grid and
# other miscellaneous things
#
############################################################
# YOU NEED TO check these parameters make sense for you
#

test = False

if test:
    # 5000 systems
    r1 = 10
    r2 = 5
    rP = 10
    recc = 10
    repeat = 1
else:
    # 9.6e5 systems
    r1 = 40
    r2 = 20
    rP = 40
    recc = 10
    repeat = 3

ensemble_args = {
    'dists' : 'Moe', # Moe, 2008(+), 2018(+)
    'binaries' : True,
    'r1' : r1,
    'r2' : r2,
    'rP' : rP,
    'recc' : recc,
    'repeat' : repeat,
    'M1spacing' : 'logM1', # const_dt, logM1
    'fsample' : 0.25,
    'normalize' : True,
    'mmin' : 2.0,
    'mmax' : 80.0,
    'log_runtime_systems' : 0,
    'run_zero_probability_system' : 0,
    'combine_ensemble_with_thread_joining' : True,

    # if using Moe's distribution, we want a
    # list of mass spacings that's a bit different to normal
    'const_dt_mass_spacings' : '(({mmin},1,-0.1),(0.8,1.5,0.05),(1,30,-0.5),(30,{mmax},1))',

    ##################################################
    'verbosity' : 1,
    'log_dt' : 5, # 300, # log to stdout every log_dt seconds
    'log_args' : 0, # do not save args by default

    ############################################################
    # ensemble options
    'ensemble' : 1,
    'ensemble_defer' : 1,
    'ensemble_logtimes' : 1,
    'ensemble_startlogtime' : 0.1, # only want old stars
    #'ensemble_dt' : 1e3, # not required when ensemble_logtimes==1
    'ensemble_logdt' : 0.1, # not required when ensemble_logtimes==0
    'ensemble_filters_off' : 1,
    'save_ensemble_chunks' : False,

    # filters required for GCE
    'ensemble_filter_BeXRB' : 1, # require Be-X-ray binaries
    'ensemble_filter_INITIAL_DISTRIBUTIONS' : 1, # require Be-X-ray binaries

    'nova_retention_method' : 'NOVA_RETENTION_ALGORITHM_WANGWU',
    'WD_accretion_rate_novae_upper_limit_hydrogen_donor' : 'DONOR_RATE_ALGORITHM_WANGWU',
    'WD_accretion_rate_novae_upper_limit_helium_donor' : 'DONOR_RATE_ALGORITHM_WANGWU',
    'WD_accretion_rate_novae_upper_limit_other_donor' : 'DONOR_RATE_ALGORITHM_WANGWU',
    'WD_accretion_rate_new_giant_envelope_lower_limit_hydrogen_donor' : 'DONOR_RATE_ALGORITHM_WANGWU',
    'WD_accretion_rate_new_giant_envelope_lower_limit_helium_donor' : 'DONOR_RATE_ALGORITHM_WANGWU',
    'WD_accretion_rate_new_giant_envelope_lower_limit_other_donor' : 'DONOR_RATE_ALGORITHM_WANGWU',
    'WDWD_merger_algorithm' : 'WDWD_MERGER_ALGORITHM_BSE', # do not use Kemp's merger algorithm
    'AGB_core_algorithm' : 'AGB_CORE_ALGORITHM_KARAKAS',
    'AGB_radius_algorithm' : 'AGB_RADIUS_ALGORITHM_KARAKAS',
    'AGB_luminosity_algorithm' : 'AGB_LUMINOSITY_ALGORITHM_KARAKAS',
    'AGB_3dup_algorithm' : 'AGB_THIRD_DREDGE_UP_ALGORITHM_KARAKAS',
    'accretion_limit_eddington_steady_multiplier' : -1,
    'accretion_limit_eddington_WD_to_remnant_multiplier' : -1,
    'accretion_limit_eddington_LMMS_multiplier' : 1,
    'alpha_ce' : 0.2,
    'lambda_ce' : "LAMBDA_CE_WANG_2016",
    'RLOF_method' : 'RLOF_METHOD_CLAEYS',
    'wind_angular_momentum_loss' : 'WIND_ANGMOM_LOSS_SPHERICALLY_SYMMETRIC', #sepherically symmetric.
    'RLOF_angular_momentum_transfer_model' : 'RLOF_ANGULAR_MOMENTUM_TRANSFER_MODEL_CONSERVATIVE',
    'nonconservative_angmom_gamma' : 'RLOF_NONCONSERVATIVE_GAMMA_ISOTROPIC',
    'hachisu_disk_wind' : 'True',
    'WRLOF_method' : 'WRLOF_Q_DEPENDENT',
    'max_neutron_star_mass' : 2.5, #2.5
    'BH_prescription' : 'BH_FRYER12_RAPID',
    'nova_eta_shift' : 0,
    'nova_timestep_accelerator_num' : -1,
    'mass_accretion_for_COWD_DD' : -1.0,
    'mass_accretion_for_ONeWD_DD' : -1.0,
    'qcrit_all' : 'QCRIT_TEMMINK2022', # critical mass ratios for unstable mass transfer

    # Be X-ray binaries
    'decretion_disc_radius_algorithm' : 'DECRETION_DISC_RADIUS_ZHANG_RIMULO',

    # kicks
    'SN_kick_distribution_II' : 'KICK_VELOCITY_GIACOBBO_MAPELLI_2020',
    'SN_kick_distribution_Ibc' : 'KICK_VELOCITY_GIACOBBO_MAPELLI_2020',
    'SN_kick_distribution_ECAP' : 'KICK_VELOCITY_FIXED',
    'SN_kick_distribution_AIC_BH' : 'KICK_VELOCITY_FIXED',
    'SN_kick_dispersion_II' : 0.0, # 250.0,
    'SN_kick_dispersion_Ibc' : 0.0, # 250.0,
    'SN_kick_dispersion_ECAP' : 0.0,
    'SN_kick_dispersion_AIC_BH' : 0.0,

    # supernova companion kicks
    'SN_kick_companion_II' : 'SN_IMPULSE_LIU2015',
    'SN_kick_companion_IBC' : 'SN_IMPULSE_LIU2015',
    'SN_kick_companion_ECAP' : 'SN_IMPULSE_NONE',
    'SN_kick_companion_AIC_BH' : 'SN_IMPULSE_AIC_BH',
}

############################################################
#
# HPC arguments
#
############################################################
HPC_options = {
    'njobs' : 8,
    'memory' : '2000MB',
    'warn_max_memory' : '4000MB',
    'max_time' : 10000,
    'directory' : None,
    'condor_requirements' : '',
    'slurm_partition' : 'shared',
    'cluster_type' : None,
}
