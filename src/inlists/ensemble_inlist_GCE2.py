############################################################
#
# Population ensemble inlist for binary_c's ensemble_manager
#
############################################################

############################################################
#
# This is the generic "GCE" (Galactic Chemical Evolution)
# /cosmology yieldsets project, version 2.0
#
project = "GCE"
mmin=0.1
mmax=120.0

############################################################
#
# binary_c arguments : these control the physics and ensemble
#
#
# binary_c_args are passed into binary_c as arguments,
#
# e.g.
#    binary_c ... alpha_ce 1.0 lambda_ce 0.5 ...
#
# you should give each a list of possible values.
#
# Note that 'defaults' should take an empty list, because this
# just uses all the binary_c defaults.
#
# You can specify the default value of each variable your
# lists, but this will not be set hence we run as few
# populations as possible.
#
# The entries in binary_c_args are:
# 1) A key,value pair where the key is the arg name, and the
#    value is a list of values to which this arg is set.
#
# 2) A key,value pair where the key is the arg name, and the
#    value is a tuple which in turn contains as elements:
#    [0] a list of values to which this arg is set
#    [1] a dictionary which is expanded for each of these values
#
# This way you can easily nest your variables to any extent.
#
# e.g.
#
# This changes the metallicity of each run
#
# { 'metallicity' : [1e-4, 1e-3, 1e-2] }
#
# i.e. 3 combinations.
#
# This changes the alpha_ce for each of those metallicities,
# i.e. 9 combinations.
#
# { 'metallicity' :
#    (
#       [ 1e-4, 1e-3, 1e-2 ],
#       {
#           'alpha_ce' : [0.1,0.5,1.0]
#       }
#    )
# }
#
############################################################

# TODO:
# Chabrier IMF

# binaries is a local variable
#
# if True we include parameters that change binary systems
#    and set up the Moe distributions for binaries.
#
# if False we include only standard single-(and binary-) star
#    physics, and run only single stars.
#
binaries = True

variable_binary_c_args = {
    'metallicity' :
    [
        0.02,#1e-4,1e-3,0.004,0.008,0.01,0.02,0.03
    ],
}

fixed_binary_c_args = {
    # winds and mass loss
    'wind_mass_loss' : [
        'WIND_ALGORITHM_BINARY_C_2020'
    ],
    'gbwind' : [
        'GB_WIND_REIMERS'
    ],
    'gb_reimers_eta' : [
        0.3
    ],
    'tpagbwind' : [
        'TPAGB_WIND_VW93_KARAKAS'
    ],
    'rotationally_enhanced_mass_loss' : [
        'ROTATIONALLY_ENHANCED_MASSLOSS_NONE'
    ],

    # massive-star supernovae
    'BH_prescription' : [
        #'BH_FRYER12_RAPID'
    ],
    'sn_kick_dispersion_IBC' : [
        265.0
    ],
    'sn_kick_dispersion_II' : [
        265.0
    ],

    # nucleosynthesis
    'initial_abundance_mix' : [
        'NUCSYN_INIT_ABUND_MIX_ASPLUND2009'
    ],
    'delta_mcmin' : [
        -0.1
    ],
    'lambda_min' : [
        'THIRD_DREDGE_UP_LAMBDA_MIN_AUTO'
    ],
    'mc13_pocket_multiplier' : [
        1.0
    ],

    # binary physics:
    # this is only included if binaries==True
    **({
        # qcrit for common envelope
        'qcrit_nuclear_burning' : 'QCRIT_TEMMINK2022',

        # Companion-reinforced attrition
        'CRAP_parameter' : [
            0.0
        ],

        # low/intermediate-mass-star Ia supernovae
        'mass_accretion_for_eld' : [
            0.15
        ],

        'type_Ia_MCh_supernova_algorithm' : [
            'TYPE_IA_SUPERNOVA_ALGORITHM_SEITENZAHL2013_AUTOMATIC'
        ],

        # turn thermohaline off when accreting?
        'no_thermohaline_mixing' : [
            0
        ],

        # combinations of different alpha,lambda for common envelope
        'alpha_ce' : [
            1.0
        ],
        'lambda_ce' : [
            'LAMBDA_CE_KLENCKI_2020'
        ],
        'lambda_ionisation' : [
            0.1
        ],

        'post_ce_adaptive_menv' : [
            'False'
        ],

        # tides
        'tidal_strength_factor' : [
            1.0
        ],
        'E2_prescription' : [
            'E2_IZZARD'
        ],

        # (W)RLOF
        'RLOF_method' : [
            'RLOF_METHOD_CLAEYS'
        ],
        'use_periastron_Roche_radius' : [
            'False'
        ],
        'WRLOF_method' : [
            'WRLOF_NONE'
        ]
    } if binaries else {})
}

ensemble_args = {

    'dists' : 'Moe', # Moe, 2008(+), 2018(+)
    'Moe2017_options' : {
        #'IMF_distribution' : 'chabrier2003',
        'IMF_distribution' : 'kroupa2001',
        '_update' : True,
        },
    'binaries' : binaries,
    'r' : 10, # resolution of M1 (if not const_dt), M2 and period, e.g. 100
    'M1spacing' : 'const_dt', # const_dt, logM1
    'fsample' : 0.25,
    'normalize' : True,
    'mmax' : 120.0,
    'log_runtime_systems' : 0,
    'run_zero_probability_system' : 0,
    'combine_ensemble_with_thread_joining' : True,

    # if using Moe's distribution, we want a
    # list of mass spacings that's a bit different to normal
    'const_dt_mass_spacings' : f'(({mmin},1,-0.1),(0.8,1.5,0.05),(1,30,-0.5),(30,{mmax},1))',

    ##################################################
    'verbosity' : 1,
    'log_dt' : 5, # 300, # log to stdout every log_dt seconds
    'log_args' : 0, # do not save args by default

    ############################################################
    # ensemble options
    'ensemble' : 1,
    'ensemble_defer' : 1,
    'ensemble_logtimes' : 1,
    'ensemble_startlogtime' : 0.1, # only want old stars
    #'ensemble_dt' : 1e3, # not required when ensemble_logtimes==1
    'ensemble_logdt' : 0.1, # not required when ensemble_logtimes==0
    'ensemble_filters_off' : 1,
    'save_ensemble_chunks' : False,

    # filters required for GCE
    'ensemble_filter_CHEMICAL_YIELDS' : 1, # require chemical yields
    'ensemble_filter_SCALARS' : 1, # required scalar number counts
    'ensemble_filter_TEST' : 1, # include test data
    'ensemble_filter_INITIAL_DISTRIBUTIONS' : 1, # always save initial distributions
    'num_cores' : 0,
    'nanchecks' : 0,
    'do_dry_run' : False
}

############################################################
#
# HPC arguments
#
############################################################
HPC_options = {
    'njobs' : 16,
    'num_cores' : 0,
    'memory' : '2000MB',
    'warn_max_memory' : '4000MB',
    'max_time' : 10000,
    'directory' : None,
    'condor_requirements' : '',
    'slurm_partition' : 'shared',
    'cluster_type' : None,
}
