############################################################
#
# Population ensemble inlist for binary_c's ensemble_manager
#
############################################################
#
# This is an inlist to make populations containing all the
# data that the ensemble can make. It is for testing only.
#
############################################################
project = 'all'

############################################################
#
# binary_c arguments : these control the physics and ensemble
#
#
# binary_c_args are passed into binary_c as arguments,
#
# e.g.
#    binary_c ... alpha_ce 1.0 lambda_ce 0.5 ...
#
# you should give each a list of possible values.
#
# Note that 'defaults' should take an empty list, because this
# just uses all the binary_c defaults.
#
# You can specify the default value of each variable your
# lists, but this will not be set hence we run as few
# populations as possible.
#
# The entries in binary_c_args are:
# 1) A key,value pair where the key is the arg name, and the
#    value is a list of values to which this arg is set.
#
# 2) A key,value pair where the key is the arg name, and the
#    value is a tuple which in turn contains as elements:
#    [0] a list of values to which this arg is set
#    [1] a dictionary which is expanded for each of these values
#
# This way you can easily nest your variables to any extent.
#
# e.g.
#
# This changes the metallicity of each run
#
# { 'metallicity' : [1e-4, 1e-3, 1e-2] }
#
# i.e. 3 combinations.
#
# This changes the alpha_ce for each of those metallicities,
# i.e. 9 combinations.
#
# { 'metallicity' :
#    (
#       [ 1e-4, 1e-3, 1e-2 ],
#       {
#           'alpha_ce' : [0.1,0.5,1.0]
#       }
#    )
# }
#
############################################################
# YOU NEED TO decide which parameters you want to set and change
binary_c_args = {
    'metallicity' : [2e-2] # just an example
}

##################################################
#
# ensemble arguments, these control the ensemble's grid and
# other miscellaneous things
#
############################################################
# YOU NEED TO check these parameters make sense for you
#
ensemble_args = {
    'dists' : 'Moe', # Moe, 2008(+), 2018(+)
    'binaries' : False,
    'r' : 100, # resolution, e.g. 100
    'M1spacing' : 'const_dt', # const_dt, logM1
    'fsample' : 0.25,
    'normalize' : True,
    'mmax' : 80.0,
    'log_runtime_systems' : 0,
    'run_zero_probability_system' : 0,
    'combine_ensemble_with_thread_joining' : True,

    # if using Moe's distribution, we want a
    # list of mass spacings that's a bit different to normal
    'const_dt_mass_spacings' : '(({mmin},1,-0.1),(0.8,1.5,0.05),(1,30,-0.5),(30,{mmax},1))',
    'max_evolution_time' : 15000,

    ##################################################
    'verbosity' : 2,
    'log_dt' : 30, # 300, # log to stdout every log_dt seconds
    'log_args' : 1, # do not save args by default
    'log_args_dir' :  '/tmp/args' , # save args to here

    ############################################################
    # ensemble options
    'ensemble' : 1,
    'ensemble_defer' : 1,
    'ensemble_logtimes' : 0,
    'ensemble_startlogtime' : 0.1, # start at log10(t/Myr)=0.1
    #'ensemble_dt' : 1e3, # not required when ensemble_logtimes==1
    'ensemble_logdt' : 0.1, # not required when ensemble_logtimes==0
    'ensemble_filters_off' : 0, # THIS SHOULD BE 0 TO TEST ALL
    'save_ensemble_chunks' : False,
}


############################################################
#
# HPC arguments
#
############################################################
# YOU NEED TO check these make sense on your cluster
#
HPC_options = {
    'njobs' : 1,
    'memory' : '30000MB',
    'warn_max_memory' : '40000MB',
    'max_time' : '48:00:00',# HH:MM::SS
    'directory' : None,
    'condor_requirements' : '',
    'slurm_partition' : 'shared',
    'cluster_type' : None,
}
