#pragma once
#ifndef SPECTRA_PROTOTYPES_H
#define SPECTRA_PROTOTYPES_H

double blackbody_volume_density_of_radiation(const double T,
                                             const double frequency)
    Constant_function;

double blackbody_integral(const double T,
                          const double low,
                          const double high);

double blackbody_fraction(const double minfreq,
                          const double T)
    Constant_function;

Spectral_type spectral_type(struct stardata_t * const stardata,
                            struct star_t * const star);
Long_spectral_type long_spectral_type(struct stardata_t * const stardata,
                                      struct star_t * const star);
void spectral_type_string(char * const c,
                          struct stardata_t * const stardata,
                          struct star_t * const star);

char * spectral_type_static_string(struct stardata_t * const stardata,
                                   struct star_t * const star);
void build_spectral_type_static_strings(struct store_t * const store);

#endif // SPECTRA_PROTOTYPES_H
