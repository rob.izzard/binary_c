#include "../binary_c.h"
No_empty_translation_unit_warning;

/*
 * Default is to the table provided by Jaschek & Jaschek (The Behaviour of
 * Chemical Elements in Stars, CUP 1995) which is based on Lang (1992)
 * You can find it on google at
 * http://books.google.com.au/books?id=ncG_HU1PjaEC&pg=PA300&lpg=PA300&dq=jaschek+jaschek+Effective+temperature+as+a+function+of+spectral+type+and+luminosity+class&source=bl&ots=Y15neV_RiQ&sig=m8kUdeg-Cmjk1jNotz7ZgXh1o3g&hl=en&sa=X&ei=zxcjUILSMaaViQeh_oDoAg&redir_esc=y#v=onepage&q=jaschek%20jaschek%20Effective%20temperature%20as%20a%20function%20of%20spectral%20type%20and%20luminosity%20class&f=false
 */


Long_spectral_type long_spectral_type(struct stardata_t * const stardata,
                                      struct star_t * const star)
{
    /*
     * Determine the spectral type and subtype of a star.
     *
     * The result returned is a floating point number X.Y
     * where X is the (integer) spectral type (see binary_c_macros.h for
     * SPECTRAL_TYPE_* definitions) and Y is 0.1* the subtype.
     * E.g. subtype 8 is X.8 and so on.
     *
     * You should access the integer spectral type and subclass using the
     * macros: Float_spectral_type_to_integer(..) and
     *         Float_spectral_type_to_subclass(..)
     *
     * The simpler spectral_type function calls this one and converts to
     * the appropriate integer spectral type using the above macro.
     */

    /* determine type: dwarf, giant or supergiant (log L>4.9) */
#define DWARF 0
#define GIANT 1
#define SUPERGIANT 2

    const int type = GIANT_LIKE_STAR(star->stellar_type) ?
        (star->luminosity >79432.8234724 ? SUPERGIANT : GIANT) : DWARF;
    struct store_t * const store = stardata->store;

    struct data_table_t * const data_table =
        type == DWARF ? store->jaschek_jaschek_dwarf :
        type == GIANT ? store->jaschek_jaschek_giant :
        store->jaschek_jaschek_supergiant;

    double x[1]={ Teff_from_star_struct(star) }; // input i.e. effective temperature
    double r[1]; // result

//#define TEST_SPECTRAL_TYPES
#ifdef TEST_SPECTRAL_TYPES
    /*
     * Code to test the spectral types as a function of temperature
     */
    double teff;
    for(teff=2500; teff<50000;teff+=100)
    {
        x[0]=teff;
        Interpolate(data_table,x,r,FALSE);
        const int spec_type = Float_spectral_type_to_integer(r[0]);
        const int sub_type = Float_spectral_type_to_subclass(r[0]);

        printf("Teff %g  > %s %d, float=%g, old func %d\n",
               teff,
               spectral_type_strings[spec_type],
               sub_type,
               r[0],
               Float_spectral_type_to_integer(r[0])
            );
    }
    Safe_free(jaschek_jaschek_interpolation_table);
    Exit_binary_c(BINARY_C_NORMAL_EXIT,"");
#else

    /* interpolate to find floating-point spectral type */
    Interpolate(data_table,x,r,FALSE);

// K+M dwarfs : either use the temperature, or a mass-based relation
//#define USE_MASS_BASED_SPECTRAL_TYPE
#ifdef  USE_MASS_BASED_SPECTRAL_TYPE
    /*
     * Observations of eta Cas B suggest it is an M0 with mass
     * 0.56Msun (Popper 1980) however MS radii given in Bohm-Vitense from
     * Schmidt-Kaler 1982 and Popper 1980 say M-type is <0.65Msun ...
     * K5 is also 0.65Msun! Could do with improvement...
     *
     * Rebassa-Mansergas etal 2007 (MNRAS 382,1377) give an upper mass
     * for M dwarfs of 0.472.
     */
    if(star->stellar_type <= MAIN_SEQUENCE)
    {
        if(star->mass < 0.472)
        {
            spectral_type = SPECTRAL_TYPE_M;
        }
        else
        {
            spectral_type = SPECTRAL_TYPE_K;
        }
    }
#endif // USE_MASS_BASED_SPECTRAL_TYPE

    return r[0];

#endif // TEST_SPECTRAL_TYPES
}

Spectral_type spectral_type(struct stardata_t * stardata,
                            struct star_t *star)
{
    /* return the integer part of the spectral type */
    return Float_spectral_type_to_integer(long_spectral_type(stardata,star));
}

void spectral_type_string(char * const c,
                          struct stardata_t * const stardata,
                          struct star_t * const star)
{
    /*
     * Return a spectral type string in human-readable form
     *
     * Note: this assumes you want the string in the pointer
     *       c which you have already assigned.
     *
     * If you want a static string, i.e. one pre-assigned,
     * call spectral_type_static_string which returns the
     * string from a location in tmpstore.
     */
    const double spectype = long_spectral_type(stardata,star);
    snprintf(c,
             3,
            "%s%d",
             spectral_type_strings[Float_spectral_type_to_integer(spectype)],
             Float_spectral_type_to_subclass(spectype));
}


char * spectral_type_static_string(struct stardata_t * const stardata,
                                   struct star_t * const star)
{
    /*
     * return a static spectral type string
     */
    const double spectype = long_spectral_type(stardata,star);
    const int i = Max(0,Min(NUM_SPECTRAL_TYPES-1,Float_spectral_type_to_integer(spectype)));
    const int j = Max(0,Min(NUM_LUMINOSITY_SUBCLASSES-1,Float_spectral_type_to_subclass(spectype)));
    return stardata->store->static_spectral_type_strings[i][j];
}
