#include "../binary_c.h"
No_empty_translation_unit_warning;

void build_spectral_type_static_strings(struct store_t * const store)
{
    /*
     * Build a table of pre-allocated spectral type strings
     * in the store.
     */

    store->static_spectral_type_strings =
        Malloc(sizeof(char**) * (NUM_SPECTRAL_TYPES+1));

    for(int i=0;i<NUM_SPECTRAL_TYPES;i++)
    {
        store->static_spectral_type_strings[i] =
            Malloc((NUM_LUMINOSITY_SUBCLASSES+1) * sizeof(char*));

        for(int j=0;j<NUM_LUMINOSITY_SUBCLASSES;j++)
        {
            if(asprintf(&store->static_spectral_type_strings[i][j],
                        "%s%d",
                        spectral_type_strings[i],
                        j) <= 0)
            {
                store->static_spectral_type_strings[i][j] = NULL;
            }
        }
    }
}
