#include "../binary_c.h"
No_empty_translation_unit_warning;


/*
 * Functions to generate a black-body spectrum
 */
static double Constant_function blackbody_indefinite_integral(const double A,
                                                              const double B,
                                                              const double x);

double Constant_function blackbody_volume_density_of_radiation(const double T,
                                                            const double frequency)
{
    /*
     * Blackbody volume density of radiation
     */
    double A = 8.0*PI*PLANCK_CONSTANT / Pow3(SPEED_OF_LIGHT);
    double B = PLANCK_CONSTANT / (BOLTZMANN_CONSTANT * T);
    
    return A*Pow3(frequency)/(exp(B*frequency)-1.0);
}

double blackbody_integral(const double T,
                          const double low,
                          const double high)
{
    /*
     * Integral of the black body spectrum 
     * between frequencies low and high
     */
    double A = 8.0*PI*PLANCK_CONSTANT / Pow3(SPEED_OF_LIGHT);
    double B = PLANCK_CONSTANT / (BOLTZMANN_CONSTANT * T);
    printf("BB A=%g B=%g : integral low=%g high=%g\n",A,B,low,high);
    return blackbody_indefinite_integral(A,B,high) -
        blackbody_indefinite_integral(A,B,low);
}

double Constant_function blackbody_indefinite_integral(const double A,
                                                       const double B,
                                                       const double x) 
{
    /*
     * Indefinite integral of the Planck function
     * (calculated on Wolfram Alpha)
     */
    double integral;
    if(Is_zero(x))
    {
        integral = 0.0;
    }
    else
    {
        // NB this will often overflow and is NOT reliable
        double eBx = exp(B*x);
        printf("BB INDEF B=%g x=%g -> B*x = %g -> eBx = %g\n",B,x,B*x,eBx);
        integral = 
            A * (
                6.0 * Li(4,eBx) / Pow4(B) 
                - 6.0 * x * Li(3,eBx) / Pow3(B)
                + 3.0 * Pow2(x) * Li(2,eBx) / Pow2(B) 
                + Pow3(x) * log(1.0 - eBx) / B 
                - Pow4(x) /4.0
                );
    }
    return integral;
}


double Constant_function blackbody_fraction(const double minfreq,
                                            const double T)
{ 

    /*
     * Integrate the blackbody function from minfreq to infinity,
     * and return the fraction of the flux in that energy range
     *
     * Useful if you want to calculate the fraction of flux that
     * is ionizing (>14eV). Just call
     *
     * blackbody_fraction(14.0*ELECTRON_VOLT/PLANCK_CONSTANT,T);
     *
     * Multiply by the total luminosity to obtain the fraction of the 
     * luminosity that is ionizing.
     *
     * Method taken from "The Blackbody Fraction, Infinite Series and
     * Spreadsheets" by Duncan Lawson (2004, International Journal of
     * Engingeering Education Vol 20, No. 6 pp 984-990).
     *
     * http://www.ijee.ie/articles/Vol20-6/IJEE1557.pdf
     */
    const double C2 = PLANCK_CONSTANT * SPEED_OF_LIGHT / BOLTZMANN_CONSTANT;
    double wavelength = SPEED_OF_LIGHT / minfreq;
    double z = C2 / (wavelength * T);
    double F = 0.0;
    int n;
    double dF;
#define THRESH 1e-8
#define NZERO_TERMS 10
    const double ez = exp(-z); // do exp only once!
    double zz = ez;
    const double a3 = Pow3(z);
    const double a2 = 3.0*Pow2(z);
    const double a1 = 6.0*z;
    for(n=1;n<100;n++) // usually <~10 are required for 1e-8 accuracy
    {
        /* add dF t o F */
        const double nn = (double)n;
        const double inn = 1.0/nn;
        dF = zz*inn * (
            a3 +
            inn*(
                a2 +
                inn* (
                    a1 +
                    6.0 * inn
                    )
                )
            );
        F += dF;
        /* check for convergence */
        if(Nonzero(F))
        {
            if(Abs_less_than(dF/F,THRESH)) break;
        }
        else
        {
            /*
             * F = 0 : don't allow after NZERO_TERMS terms
             * will just be zero forever
             */
            if(n>NZERO_TERMS) break;
        }
        zz *= ez;
    }
    F *= 15.0/Pow4(PI);
    return F;
}
                          
