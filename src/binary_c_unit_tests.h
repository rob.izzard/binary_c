#pragma once
#ifndef BINARY_C_UNIT_TESTS_H
#define BINARY_C_UNIT_TESTS_H
/*
 * The binary_c stellar population nucleosynthesis framework.
 *
 * Contact:  rob.izzard@gmail.com
 *
 * https://binary_c.gitlab.io/
 * https://gitlab.com/binary_c/binary_c
 * https://groups.google.com/forum/#!forum/binary_c-nucsyn-announce
 * https://groups.google.com/forum/#!forum/binary_c-nucsyn-devel
 * https://twitter.com/binary_c_code
 * https://www.facebook.com/groups/149489915089142/
 *
 * Please see the files README, LICENCE and CHANGES,
 * and the doc/ directory for documentation.
 */

#include "binary_c_version_macros.h"
/*
 * A series of unit test macros for binary_c,
 * the aim
 *
 * UNIT_TEST_HEADER is a list of strings which form the
 * header to the output
 *
 * UNIT_TEST_PREFIX is the string prefix for unit test
 * output lines.
 *
 * Next are the UNIT_TEST_n, where n=1,2,3..., macros.
 *
 * Next is a test for the version which allows a
 * UNIT_TEST_DATA and UNIT_TEST_time for each version of binary_c
 *
 * If you find that you have no UNIT_TEST_n macro for your
 * version, you have to chose one, or define a new one.
 */

#define UNDEF -666.6

#define UNDEF_STARS UNDEF,UNDEF

#define UNIT_TEST_PREFIX                        \
    "BINARY_C_UNIT_TEST"

#define UNIT_TEST_HEADER                                \
    "Time",                                             \
        "M1",                                           \
        "M2",                                           \
        "Mc1",                                          \
        "Mc2",                                          \
        "stellar_type1",                                \
        "stellar_type2",                                \
        "sgl",                                          \
        "R1",                                           \
        "R2",                                           \
        "RL1",                                          \
        "RL2",                                          \
        "R1/RL1",                                       \
        "R2/RL2",                                       \
        "age1",                                         \
        "age2",                                         \
        "epoch1",                                       \
        "epoch2",                                       \
        "L1",                                           \
        "L2",                                           \
        "J1",                                           \
        "J2",                                           \
        "omega1",                                       \
        "omega2",                                       \
        "omegacrit1",                                   \
        "omegacrit2",                                   \
        "vwind1",                                       \
        "vwind2",                                       \
        "stellar_timestep1",                            \
        "stellar_timestep2",                            \
        "NTP1",                                         \
        "NTP2",                                         \
        "tm1",                                          \
        "tm2",                                          \
        "tn1",                                          \
        "tn2",                                          \
        "dt",                                           \
        "dtm",                                          \
        "Jorb",                                         \
        "separation",                                   \
        "eccentricity",                                 \
        "dJorb_dt",                                     \
        "dJorb_dt_gravitational_radiation",             \
        "dJorb_dt_wind_loss",                           \
        "dJorb_dt_wind_gain",                           \
        "dJorb_dt_RLOF_loss",                           \
        "dJorb_dt_RLOF_gain",                           \
        "dJorb_dt_cbdisc",                              \
        "dJorb_dt_tides",                               \
        "dJorb_dt_nonconservative_loss",                \
        "dJorb_dt_nova",                                \
        "dJorb_dt_artificial",                          \
        "deccentricity_dt",                             \
        "deccentricity_dt_gravitational_radiation",     \
        "deccentricity_dt_tides",                       \
        "deccentricity_dt_winds",                       \
        "deccentricity_dt_cbdisc",                      \
        "mdot1_wind_loss",                              \
        "mdot2_wind_loss",                              \
        "mdot1_wind_gain",                              \
        "mdot2_wind_gain",                              \
        "mdot1_RLOF_loss",                              \
        "mdot2_RLOF_loss",                              \
        "mdot1_RLOF_gain",                              \
        "mdot2_RLOF_gain",                              \
        "mdot1_nova",                                   \
        "mdot2_nova",                                   \
        "mdot1_nonconservative_loss",                   \
        "mdot2_nonconservative_loss",                   \
        "mdot1_cbdisc_gain",                            \
        "mdot2_cbdisc_gain",                            \
        "mdot1_decretion_disc",                         \
        "mdot2_decretion_disc",                         \
        "mdot1_disc_loss",                              \
        "mdot2_disc_loss",                              \
        "Jdot1_wind_loss",                              \
        "Jdot2_wind_loss",                              \
        "Jdot1_wind_gain",                              \
        "Jdot2_wind_gain",                              \
        "Jdot1_RLOF_loss",                              \
        "Jdot2_RLOF_loss",                              \
        "Jdot1_RLOF_gain",                              \
        "Jdot2_RLOF_gain",                              \
        "Jdot1_nova",                                   \
        "Jdot2_nova",                                   \
        "Jdot1_nonconservative_loss",                   \
        "Jdot2_nonconservative_loss",                   \
        "Jdot1_cbdisc_gain",                            \
        "Jdot2_cbdisc_gain",                            \
        "Jdot1_decretion_disc",                         \
        "Jdot2_decretion_disc",                         \
        "Jdot1_tide",                                   \
        "Jdot2_tide",                                   \
        "Jdot1_magnetic_braking",                       \
        "Jdot2_magnetic_braking",                       \
        "XH1_1",                                        \
        "XH1_2",                                        \
        "XHe4_1",                                       \
        "XHe4_2",                                       \
        "XC12_1",                                       \
        "XC12_2",                                       \
        "XC13_1",                                       \
        "XC13_2",                                       \
        "XN14_1",                                       \
        "XN14_2",                                       \
        "XO16_1",                                       \
        "XO16_2",                                       \
        "XNe20_1",                                      \
        "XNe20_2",                                      \
        "XNa23_1",                                      \
        "XNa23_2",                                      \
        "XMg24_1",                                      \
        "XMg24_2",                                      \
        "XFe56_1",                                      \
        "XFe56_2",                                      \
        "end"                                           \
/* end */


#define UNIT_TEST_HEADER_FORMAT                                         \
    "%s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s"

#define UNIT_TEST_FORMAT                                                \
    "%g %g %g %g %g %d %d %d %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g"

/* V2.0pre28 */
#if BINARY_C_MAJOR_VERSION >= 2 &&              \
    BINARY_C_MINOR_VERSION >= 0 &&              \
    BINARY_C_PRE_VERSION >= 28

#define                                         \
    Stars(X)                                    \
    (stardata->star[0].X),                      \
    (stardata->star[1].X)

#define \
    Stellar_property_ratio(N,X,Y)                                                \
    (                                                           \
        (Is_not_zero(stardata->star[(N)].Y))                    \
        ?                                                       \
        ((stardata->star[(N)].X)/(stardata->star[(N)].Y)) :     \
        (UNDEF)                                                 \
        )

#define \
    Stars_ratio(X,Y)                             \
    Stellar_property_ratio(0,X,Y),                            \
    Stellar_property_ratio(1,X,Y)

#ifdef NUCSYN
#define UNIT_TESTS_NUCSYN                       \
    Stars(Xenv[XH1]),                           \
        Stars(Xenv[XHe4]),                      \
        Stars(Xenv[XC12]),                      \
        Stars(Xenv[XC13]),                      \
        Stars(Xenv[XN14]),                      \
        Stars(Xenv[XO16]),                      \
        Stars(Xenv[XNe20]),                     \
        Stars(Xenv[XNa23]),                     \
        Stars(Xenv[XMg24]),                     \
        Stars(Xenv[XFe56])
#else
/*
 * No nucsyn, set to undefined
 */
#define UNIT_TESTS_NUCSYN UNDEF_STARS,UNDEF_STARS,UNDEF_STARS,UNDEF_STARS,UNDEF_STARS,UNDEF_STARS,UNDEF_STARS,UNDEF_STARS,UNDEF_STARS,UNDEF_STARS

#endif

#define UNIT_TEST_time stardata->model.time
#define UNIT_TEST_DATA                                                  \
    stardata->model.time,                                               \
        Stars(mass),                                                    \
        Stars(core_mass[CORE_He]),                                               \
        Stars(stellar_type),                                            \
        stardata->model.sgl,                                            \
        Stars(radius),                                                  \
        Stars(roche_radius),                                            \
        Stars_ratio(radius,roche_radius),                               \
        Stars(age),                                                     \
        Stars(epoch),                                                   \
        Stars(luminosity),                                              \
        Stars(angular_momentum),                                        \
        Stars(omega),                                                   \
        Stars(omega_crit),                                              \
        Stars(vwind),                                                   \
        Stars(stellar_timestep),                                        \
        Stars(num_thermal_pulses),                                      \
        Stars(tm),                                                      \
        Stars(tn),                                                      \
        stardata->model.dt,                                             \
        stardata->model.dtm,                                            \
        stardata->common.orbit.angular_momentum,                      \
        (stardata->model.sgl==FALSE ? stardata->common.orbit.separation : UNDEF), \
        (stardata->model.sgl==FALSE ? stardata->common.orbit.eccentricity : UNDEF), \
        Jdot_orbit(stardata),                                           \
        stardata->model.derivative[DERIVATIVE_ORBIT_ANGMOM_GRAVITATIONAL_RADIATION], \
        stardata->model.derivative[DERIVATIVE_ORBIT_ANGMOM_WIND_LOSS],  \
        stardata->model.derivative[DERIVATIVE_ORBIT_ANGMOM_WIND_GAIN],  \
        stardata->model.derivative[DERIVATIVE_ORBIT_ANGMOM_RLOF_LOSS],  \
        stardata->model.derivative[DERIVATIVE_ORBIT_ANGMOM_RLOF_GAIN],  \
        stardata->model.derivative[DERIVATIVE_ORBIT_ANGMOM_CBDISC],     \
        stardata->model.derivative[DERIVATIVE_ORBIT_ANGMOM_TIDES],      \
        stardata->model.derivative[DERIVATIVE_ORBIT_ANGMOM_NONCONSERVATIVE_LOSS], \
        stardata->model.derivative[DERIVATIVE_ORBIT_ANGMOM_NOVA],       \
        stardata->model.derivative[DERIVATIVE_ORBIT_ANGMOM_ARTIFICIAL], \
        edot_orbit(stardata),                                           \
        stardata->model.derivative[DERIVATIVE_ORBIT_ECCENTRICITY_GRAVITATIONAL_RADIATION], \
        stardata->model.derivative[DERIVATIVE_ORBIT_ECCENTRICITY_TIDES], \
        stardata->model.derivative[DERIVATIVE_ORBIT_ECCENTRICITY_WINDS], \
        stardata->model.derivative[DERIVATIVE_ORBIT_ECCENTRICITY_CBDISC], \
        Stars(derivative[DERIVATIVE_STELLAR_MASS_WIND_LOSS]),           \
        Stars(derivative[DERIVATIVE_STELLAR_MASS_WIND_GAIN]),           \
        Stars(derivative[DERIVATIVE_STELLAR_MASS_RLOF_LOSS]),           \
        Stars(derivative[DERIVATIVE_STELLAR_MASS_RLOF_GAIN]),           \
        Stars(derivative[DERIVATIVE_STELLAR_MASS_NOVA]),                \
        Stars(derivative[DERIVATIVE_STELLAR_MASS_NONCONSERVATIVE_LOSS]), \
        Stars(derivative[DERIVATIVE_STELLAR_MASS_CBDISC_GAIN]),         \
        Stars(derivative[DERIVATIVE_STELLAR_MASS_DECRETION_DISC]),      \
        Stars(derivative[DERIVATIVE_STELLAR_MASS_DISC_LOSS]),           \
        Stars(derivative[DERIVATIVE_STELLAR_ANGMOM_WIND_LOSS]),         \
        Stars(derivative[DERIVATIVE_STELLAR_ANGMOM_WIND_GAIN]),         \
        Stars(derivative[DERIVATIVE_STELLAR_ANGMOM_RLOF_LOSS]),         \
        Stars(derivative[DERIVATIVE_STELLAR_ANGMOM_RLOF_GAIN]),         \
        Stars(derivative[DERIVATIVE_STELLAR_ANGMOM_NOVA]),              \
        Stars(derivative[DERIVATIVE_STELLAR_ANGMOM_NONCONSERVATIVE_LOSS]), \
        Stars(derivative[DERIVATIVE_STELLAR_ANGMOM_CBDISC_GAIN]),       \
        Stars(derivative[DERIVATIVE_STELLAR_ANGMOM_DECRETION_DISC]),    \
        Stars(derivative[DERIVATIVE_STELLAR_ANGMOM_TIDES]),              \
        Stars(derivative[DERIVATIVE_STELLAR_ANGMOM_MAGNETIC_BRAKING]),  \
        UNIT_TESTS_NUCSYN,                                              \
        666.0



/* V 2.0pre27 */
#elif  BINARY_C_MAJOR_VERSION >= 2 &&           \
    BINARY_C_MINOR_VERSION >= 0 &&              \
    BINARY_C_PRE_VERSION == 27

#define Stars(X)                                \
    (stardata->star[0].X),                      \
        (stardata->star[1].X)

#ifdef NUCSYN
#define UNIT_TESTS_NUCSYN                       \
    Stars(Xenv[XH1]),                           \
        Stars(Xenv[XHe4]),                      \
        Stars(Xenv[XC12]),                      \
        Stars(Xenv[XC13]),                      \
        Stars(Xenv[XN14]),                      \
        Stars(Xenv[XO16]),                      \
        Stars(Xenv[XNe20]),                     \
        Stars(Xenv[XNa23]),                     \
        Stars(Xenv[XMg24]),                     \
        Stars(Xenv[XFe56])
#else
/*
 * No nucsyn, set to undefined
 */
#define UNIT_TESTS_NUCSYN UNDEF_STARS,UNDEF_STARS,UNDEF_STARS,UNDEF_STARS,UNDEF_STARS,UNDEF_STARS,UNDEF_STARS,UNDEF_STARS,UNDEF_STARS,UNDEF_STARS,UNDEF_STARS,UNDEF_STARS,UNDEF_STARS,UNDEF_STARS,UNDEF_STARS,UNDEF_STARS,UNDEF_STARS,UNDEF_STARS,UNDEF_STARS,UNDEF_STARS,UNDEF_STARS
#endif

#define UNIT_TEST_time stardata->model.time
#define UNIT_TEST_DATA                                                  \
    stardata->model.time,                                               \
        Stars(mass),                                                    \
        Stars(core_mass[CORE_He]),                                               \
        Stars(stellar_type),                                            \
        stardata->model.sgl,                                            \
        Stars(radius),                                                  \
        Stars(roche_radius),                                            \
        Stars_ratio(radius,roche_radius),                               \
        Stars(age),                                                     \
        Stars(epoch),                                                   \
        Stars(luminosity),                                              \
        Stars(angular_momentum),                                        \
        Stars(omega),                                                   \
        Stars(omega_crit),                                              \
        Stars(vwind),                                                   \
        Stars(dtmi),                                                    \
        Stars(num_thermal_pulses),                                      \
        Stars(tm),                                                      \
        Stars(tn),                                                      \
        stardata->model.dt,                                             \
        stardata->model.dtm,                                            \
        stardata->common.orbit.angular_momentum,                      \
        stardata->common.orbit.separation,                                    \
        stardata->common.orbit.eccentricity,                                  \
        Jdot_orbit(stardata),                                           \
        stardata->model.derivative[DERIVATIVE_ORBIT_ANGMOM_GRAVITATIONAL_RADIATION], \
        UNDEF,                                                    \
        UNDEF,                                                    \
        UNDEF,                                                    \
        UNDEF,                                                    \
        UNDEF,                                                    \
        UNDEF,                                                    \
        UNDEF,                                                    \
        UNDEF,                                                    \
        UNDEF,                                                    \
        edot_orbit(stardata),                                           \
        stardata->model.derivative[DERIVATIVE_ORBIT_ECCENTRICITY_GRAVITATIONAL_RADIATION], \
        stardata->model.derivative[DERIVATIVE_ORBIT_ECCENTRICITY_TIDES], \
        stardata->model.derivative[DERIVATIVE_ORBIT_ECCENTRICITY_WINDS], \
        UNDEF,                                                    \
        Stars(derivative[DERIVATIVE_STELLAR_MASS_WIND_LOSS]),           \
        Stars(derivative[DERIVATIVE_STELLAR_MASS_WIND_GAIN]),           \
        Stars(derivative[DERIVATIVE_STELLAR_MASS_RLOF_LOSS]),           \
        Stars(derivative[DERIVATIVE_STELLAR_MASS_RLOF_GAIN]),           \
        UNDEF_STARS,                                                    \
        UNDEF_STARS,                                                    \
        UNDEF_STARS,                                                    \
        UNDEF_STARS,                                                    \
        UNDEF_STARS,                                                    \
        Stars(derivative[DERIVATIVE_STELLAR_ANGMOM_WIND_LOSS]),         \
        Stars(derivative[DERIVATIVE_STELLAR_ANGMOM_WIND_GAIN]),         \
        Stars(derivative[DERIVATIVE_STELLAR_ANGMOM_RLOF_LOSS]),         \
        Stars(derivative[DERIVATIVE_STELLAR_ANGMOM_RLOF_GAIN]),         \
        UNDEF_STARS,                                                    \
        UNDEF_STARS,                                                    \
        UNDEF_STARS,                                                    \
        UNDEF_STARS,                                                    \
        Stars(derivative[DERIVATIVE_STELLAR_ANGMOM_TIDES]),              \
        Stars(derivative[DERIVATIVE_STELLAR_ANGMOM_MAGNETIC_BRAKING]),  \
        UNIT_TESTS_NUCSYN,                                              \
        666.0


#endif

/*
 * Error if there is no data macro defined
 */
#ifndef UNIT_TEST_DATA
#error No unit test data are defined : you must define the appropriate macros in binary_c_unit_tests.h
#endif // UNIT_TEST_DATA


/*
 * binary_c uses Printf for output, but if this
 * is not defined instead use printf
 */
#undef Printf
#ifndef Printf
#define Printf printf
#endif


/*
 * Test macro that returns true on the first timestep
 */
#define UNIT_TEST_FIRST_TIMESTEP_TEST (Is_zero(UNIT_TEST_time))




#endif // BINARY_C_UNIT_TESTS_H
