#pragma once
#ifndef BINARY_C_ERROR_CODES_H
#define BINARY_C_ERROR_CODES_H
/*
 * The binary_c stellar population nucleosynthesis framework.
 *
 * Copyright Robert Izzard 2000-2025
 *
 * Contact rob.izzard@gmail.com
 *
 * Code repository:
 * https://gitlab.com/binary_c
 *
 * Documentation is in doc/
 * Code is in src/
 *
 * To build, run
 *
 * ---
 *
 * meson setup builddir
 * cd builddir
 * ninja binary_c_install
 *
 * ---
 *
 * Please see the files README, LICENCE and CHANGES,
 * and the doc/ directory for documentation which is mirrored
 * at https://binary_c.gitlab.io
 *
 **********************
 *
 * Purpose:
 *
 * This file contains exit codes for binary_c :
 * these should be integers <255 with 0 the normal exit.
 *
 * Because these are defined as X-macros, the error codes are
 * automatically generated in the enum{...} with 0 being no error.
 *
 **********************
 */
#include "binary_c_error_codes.def"

/*
 * Construct general binary_c error codes
 */
#undef X
#define X(CODE,TEXT) CODE,
enum { BINARY_C_ERROR_CODES BINARY_C_NUM_ERROR_CODES };

#undef X
#define X(CODE,TEXT) #CODE,
static char * binary_c_error_code_macros[] Maybe_unused = { BINARY_C_ERROR_CODES };

#undef X
#define X(CODE,TEXT) TEXT,
static char * binary_c_error_code_strings[] Maybe_unused = { BINARY_C_ERROR_CODES };

#define Binary_c_error_string(N) Array_string(binary_c_error_code_strings,(N))

#undef X

#define Binary_c_error_code_is_bad(CODE) (          \
        (CODE)!=BINARY_C_NORMAL_EXIT &&             \
        (CODE)!=BINARY_C_QUIET_EXIT &&              \
        (CODE)!=BINARY_C_SPECIAL_EXIT &&            \
        (CODE)!=BINARY_C_NORMAL_BATCHMODE_EXIT &&   \
        (CODE)!=BINARY_C_SILENT_EXIT                \
        )


#endif // BINARY_C_ERROR_CODES_H
