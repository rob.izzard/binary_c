#ifndef TIMERS_PROTOTYPES_H
#define TIMERS_PROTOTYPES_H

void calc_ticks_timer(struct stardata_t * const stardata,
                      ticks * const start_tick,
                      Boolean * const first);
void final_timing_output(struct stardata_t * const stardata,
                         const ticks start_tick,
                         const int repeat);

#ifdef __INTEL_COMPILER
#include <ia32intrin.h>
#endif //__INTEL_COMPILER

ticks getticks(void);
void warmup_cpu(struct stardata_t * const stardata,
                const double dtsecs);

#endif // TIMERS_PROTOTYPES_H
