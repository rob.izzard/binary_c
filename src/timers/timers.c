#include "../binary_c.h"
No_empty_translation_unit_warning;

/*
 * getticks is a function to return the number of CPU cycles,
 * known as "ticks", that have elapsed relative to some point
 * (usually the execution of binary_c).
 *
 * A "ticks" type is returned (this is defined elsewhere).
 *
 * It is valid to return 0 : this means no timer is working.
 *
 * Various methods are given for fast timers, e.g. RDTSC
 * on Intel chips, and a fallback using a C-library clock
 * routine.
 *
 * Note that if VIRTUAL_MACHINE is defined, we don't use
 * RDTSC as it's probably either unsupported or gives
 * nonsense.
 *
 * You can disable the timer, and hence always return 0,
 * by turning off TIMER in binary_c_code_options.h
 */

#ifdef TIMER
#ifdef __INTEL_COMPILER
#include <ia32intrin.h>
#endif //__INTEL_COMPILER
#include <time.h>
#include <stdint.h>
#if (defined __i386__ || defined __x86_64__ || defined __ia64__)
#include <x86intrin.h>
#endif
#endif //TIMER


ticks getticks(void)
{

#ifdef TIMER

#ifndef VIRTUAL_MACHINE
    /*
     * Tick counter via RDTSC_TIMER
     *
     * You can get arch-dependent flags from
     * https://sourceforge.net/p/predef/wiki/Architectures/
     */
#ifdef __INTEL_COMPILER
    /*
     * intel's compiler use built in function call to RDTSC
     */
    return _rdtsc();
#endif // __INTEL_COMPILER


#if defined __GNUC__ &&                                             \
    (defined __i386__ || defined __x86_64__ || defined __ia64__)
#ifdef TIMER_USE_ASM
    /*
     * gcc (and clang) on x86_64 chips e.g. core7
     * assembly call to RDTSC.
     *
     * Note: it's better to use the __rdtsc function,
     * as the compiler should automatically generate the
     * appropriate assembly code.
     */
    unsigned a, d;
    asm volatile("rdtsc" : "=a" (a), "=d" (d));
    return ((ticks)a) | (((ticks)d) << 32);
#else
    /*
     * most modern compilers should use __rdtsc
     */
    return __rdtsc();
#endif // TIMER_USE_ASM
#endif // x86_64
#endif // !VIRTUAL_MACHINE

    /*
     * No machine timer available, default to
     * C library's clock_gettime
     */
    struct timespec tp;
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID,&tp);

    /* return time converted to ticks */
    const ticks t = (ticks) (
               ((uint64_t)tp.tv_sec  * (1000000000U * 1e-3 * CPUFREQ))
               +
               ((uint64_t)tp.tv_nsec * (1e-3 * CPUFREQ))
               );
    return t;

#else
    /* fallback : do nothing */
    return (ticks) 0;
#endif//TIMER
}



void warmup_cpu(struct stardata_t * stardata,
                const double dtsecs)
{
#ifdef TIMER
    /* warm up CPU */
    if(stardata->cpu_is_warm==FALSE && getticks()!=0)
    {
    stardata->warmup_timer_offset += dtsecs;
    printf("Warm up CPU ...");fflush(stdout);
    double ddt=0.0;
    ticks start_tick=getticks();
    while(ddt<dtsecs)
    {
#ifdef CPUFREQ
        ddt=((float)(getticks()-start_tick))/((float)1e6*((float)CPUFREQ));
#else
        /* assume 1GHz! */
        ddt=((float)(getticks()-start_tick))/((float)1e9);
#endif//CPUFREQ
    }
    printf("done dt=%g\n",ddt);fflush(stdout);
    stardata->cpu_is_warm=TRUE;
    }
#endif//TIMER
}
