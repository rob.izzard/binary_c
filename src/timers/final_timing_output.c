#include "../binary_c.h"
No_empty_translation_unit_warning;


void final_timing_output(struct stardata_t * stardata,
                         const ticks start_tick,
                         const int repeat)
{
    const ticks end_tick = getticks();

    /* calculate clock ticks */
    const ticks t = end_tick - start_tick;

#ifdef CPUFREQ
    /* calculate total run time in seconds */
    const double runtime = (double)Timer_seconds(t) - stardata->warmup_timer_offset;

    Printf("Tick count %llu, ~ runtime = %g repeat=%d ",
       t,
       runtime,
       repeat);

    if(repeat>1)
    {
      /* calculate ticks per run */
      const double ticks_per_run = ((double)t)/(double)repeat;

      /* hence calculate the time per run */
      const double perrun = runtime/((double)repeat);

      Printf(" per run = %g : ticks per run %g ",
             perrun,
             ticks_per_run);
    }

#else

    Printf("Tick count %llu (CPUFREQ undefined)\n",t);

#endif // CPUFREQ

    if(stardata->preferences->log_all_reject_timestep_failures == TRUE)
    {
        Printf("; %d timesteps rejected of which %d could not be shortened ",
               stardata->model.n_rejected_models,
               stardata->model.n_rejected_models_without_shorten);
    }

    Printf("\n");
    fflush(stdout);
}
