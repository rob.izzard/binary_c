#include "../binary_c.h"
No_empty_translation_unit_warning;


#if defined TIMER_PER_SYSTEM
void calc_ticks_timer(struct stardata_t * const stardata,
                      ticks * const start_tick,
                      Boolean * const first)
{
    ticks end_tick=getticks();

    /* calculate clock ticks */
    long int _ticks=((long int)end_tick-(long int) (*start_tick));

    if(*first==FALSE)
    {
    /*
     * never output the first call to evolve_system(),
     * this contains setup time which is spurious
     */
#ifdef CPUFREQ
    /* calculate total run time */
    double runtime=((float)_ticks)/(1e6*((float)CPUFREQ));
    Printf("Tick count %ld, ~ runtime = %g\n",_ticks,runtime);
#else
    Printf("Tick count %ld (CPUFREQ undefined)\n",_ticks);
#endif // CPUFREQ
    //fflush(stdout);
    }

    /* reset the ticker */
    *start_tick = getticks();

    *first = FALSE;
}
#endif // TIMER_PER_SYSTEM
