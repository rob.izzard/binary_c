#pragma once
#ifndef BINARY_C_MAIN_H
#define BINARY_C_MAIN_H
/*
 * The binary_c stellar population nucleosynthesis framework.
 *
 * Copyright Robert Izzard 2000-2025
 *
 * Contact rob.izzard@gmail.com
 *
 * Code repository:
 * https://gitlab.com/binary_c
 *
 * Documentation is in doc/
 * Code is in src/
 *
 * To build, run
 *
 * ---
 *
 * meson setup builddir
 * cd builddir
 * ninja binary_c_install
 *
 * ---
 *
 * Please see the files README, LICENCE and CHANGES,
 * and the doc/ directory for documentation which is mirrored
 * at https://binary_c.gitlab.io
 *
 **********************
 *
 * Purpose:
 *
 * This header file is used by the main() function
 * of binary_c (see main.c).
 */
#include "binary_c.h"

#if defined FPU_PRECISION && defined FPU_CONTROL
#include <fpu_control.h>
#endif//FPU_PRECISION

#include "binary_c_main_prototypes.h"
#include "binary_c_main_macros.h"
#ifdef STACK_SIZE
#include <sys/resource.h>
#endif//STACK_SIZE
#ifdef USE_MCHECK
#include <mcheck.h>
#endif//USE_MCHECK


#ifdef CODESTATS
struct codestats_t codestats;
static void output_codestats(void);

#define Output_codestats output_codestats();
#else
#define Output_codestats /* do nothing */
#endif//CODESTATS

static int binary_c_main(int argc,
                         char ** argv,
                         struct stardata_t ** Restrict s);

#endif // BINARY_C_MAIN_H
