#include "../binary_c.h"
No_empty_translation_unit_warning;

/*
 * Call all event-based logging handlers
 */
void event_based_logging_handle_events(struct stardata_t * const stardata)
{
#undef X
#define X(TYPE) Dprint("event_based_log log %s\n",#TYPE);event_based_logging_handle_##TYPE##_event(stardata);
    EVENT_BASED_LOGGING_TYPES_LIST;
#undef X
}
