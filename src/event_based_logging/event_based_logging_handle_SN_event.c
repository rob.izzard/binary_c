#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef EVENT_BASED_LOGGING

/* Function to add the supernova event to the event logstring handling */
void event_based_logging_handle_SN_event(struct stardata_t * stardata)
{
    /* Set up stuff */
    struct stardata_t * pre_SN_stardata_object;
    Star_number sn_k;

    /* Check if binary system*/
    if(stardata->preferences->zero_age.multiplicity == 2)
    {
        /* Loop over the stars and check if a star is going SN */
        Starloop(sn_k)
        {
            if(stardata->star[sn_k].SN_type != SN_NONE)
            {
                /* Get the pre-SN object */
                if(stardata->preferences->save_pre_events_stardata == TRUE
                        &&
                        stardata->pre_events_stardata != NULL)
                {
                    pre_SN_stardata_object = stardata->pre_events_stardata;
                }
                else
                {
                    pre_SN_stardata_object = stardata->previous_stardata;
                }

                /* Construct all the string and fill with info */
                const Core_type core_id = ID_core(pre_SN_stardata_object->star[sn_k].stellar_type);
                char *supernova_string;
                if(asprintf(
                            &supernova_string,
                            "SN_BINARY "
                            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                            // ZAMS: 5
                            "%g %g %g %g %g "       // ZAMS 1-5
                            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                            // SYSTEM COMMON: 3
                            "%30.12e %g %llu "      // SYSTEM COMMON: 1-3
                            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                            // SN INFO: 30
                            "%g %d %d "             // SN INFO: 1-3
                            "%g %g %g %g "          // SN INFO: 4-7
                            "%g %g %d %g "          // SN INFO: 8-11
                            "%g %g %g %g "          // SN INFO: 12-15
                            "%g %g %g %g "          // SN INFO: 16-19
                            "%g %d %d %d "          // SN INFO: 20-23
                            "%g %g %g",             // SN INFO: 24-26
                            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                            // ZAMS INFO: 5
                            stardata->common.zero_age.mass[0],                          // 1: ZAMS mass 1
                            stardata->common.zero_age.mass[1],                          // 2: ZAMS mass 2
                            stardata->common.zero_age.orbital_period[0],                               // 3: ZAMS orbital period
                            stardata->common.zero_age.separation[0],                           // 4: ZAMS separation
                            stardata->common.zero_age.eccentricity[0],                         // 5: ZAMS eccentricity
                            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                            // SYSTEM COMMON: 3
                            stardata->model.time,                                       // 1: time of SN
                            stardata->common.metallicity,                               // 2: metallicity
                            stardata->common.random_seed,                               // 3: random seed
                            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                            // SN_BINARY INFO: 25
                            stardata->star[sn_k].mass,                                  // 1: post SN mass
                            stardata->star[sn_k].stellar_type,                          // 2: post SN stellar type
                            stardata->star[sn_k].SN_type,                               // 3: SN type

                            stardata->star[sn_k].fallback,                              // 4: SN fallback fraction
                            stardata->star[sn_k].fallback_mass,                         // 5: SN fallback mass
                            stardata->common.orbit.eccentricity,                        // 6: post SN eccentricity
                            stardata->common.orbit.period,                              // 7: post SN orbital period

                            stardata->common.orbit.separation,                          // 8: post SN separation
                            pre_SN_stardata_object->star[sn_k].mass,                    // 9: pre SN mass
                            pre_SN_stardata_object->star[sn_k].stellar_type,            // 10: pre SN stellar type
                            pre_SN_stardata_object->star[sn_k].radius,                  // 11: pre SN radius

                            core_id != CORE_NONE ? pre_SN_stardata_object->star[sn_k].core_mass[core_id] : 0.0, // 12: pre SN core mass
                            pre_SN_stardata_object->star[sn_k].core_mass[CORE_CO],      // 13: pre SN CO core mass
                            pre_SN_stardata_object->star[sn_k].core_mass[CORE_He],      // 14: pre SN He core mass
                            pre_SN_stardata_object->star[sn_k].omega /
                            pre_SN_stardata_object->star[sn_k].omega_crit,    // 15: pre SN fraction of critical rotation

                            pre_SN_stardata_object->common.orbit.eccentricity,          // 16: pre SN eccentricity
                            pre_SN_stardata_object->common.orbit.period,                // 17: pre SN orbital period
                            pre_SN_stardata_object->common.orbit.separation,            // 18: pre SN separation
                            pre_SN_stardata_object->star[Other_star(sn_k)].mass,        // 19: pre SN companion mass

                            pre_SN_stardata_object->star[Other_star(sn_k)].radius,      // 20: pre SN companion radius
                            pre_SN_stardata_object->star[Other_star(sn_k)].stellar_type,// 21: pre SN stellar type
                            stardata->star[sn_k].starnum,                               // 22: starnum of exploding star
                            stardata->model.event_based_logging_SN_counter,             // 23: SN counter

                            /* Kick */
                            stardata->star[sn_k].sn_kick_speed,                         // 24: supernova kick speed
                            stardata->star[sn_k].sn_kick_omega,                         // 25: supernova kick omega
                            stardata->star[sn_k].sn_kick_phi                            // 26: supernova kick phi
                        ) == 0)
                {
                    Exit_binary_c(BINARY_C_ALLOC_FAILED,
                                  "Could not alloc with asprintf in event_based_logging_handle_SN_event (1)\n");
                }

                /* Add event logstring list */
                if(stardata->preferences->event_based_logging[EVENT_BASED_LOGGING_SN] == 1)
                {
                    event_based_logging_add_event_logstring_to_list(stardata,
                                                                    supernova_string);
                }
                Safe_free(supernova_string);
            }
        }
    }

    /* Or handle single system supernova */
    else if(stardata->preferences->zero_age.multiplicity == 1)
    {
        sn_k = 0;
        if(stardata->star[sn_k].SN_type != SN_NONE)
        {
            /* Get the pre-SN object */
            if(stardata->preferences->save_pre_events_stardata == TRUE &&
                    stardata->pre_events_stardata != NULL)
            {
                pre_SN_stardata_object = stardata->pre_events_stardata;
            }
            else
            {
                pre_SN_stardata_object = stardata->previous_stardata;
            }

            /* Construct all the string and fill with info */
            char * supernova_string;
            if(asprintf(
                        &supernova_string,
                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        "SN_SINGLE "
                        // ZAMS: 1
                        "%g "                   // ZAMS 1
                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        // SYSTEM COMMON: 3
                        "%30.12e %g %llu "   // SYSTEM COMMON: 1-3
                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        // SN INFO: 30
                        "%g %d %d "             // SN INFO: 1-3
                        "%g %g %g %d "          // SN INFO: 4-7
                        "%g %g %g %g "          // SN INFO: 8-11
                        "%g %d %d "             // SN INFO: 12-14
                        "%g %g %g"              // SN INFO: 15-18
                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        // ZAMS INFO: 5
                        , stardata->common.zero_age.mass[0],                        // 1: ZAMS mass 1
                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        // SYSTEM COMMON: 3
                        stardata->model.time,                                       // 1: time of SN
                        stardata->common.metallicity,                               // 2: metallicity
                        stardata->common.random_seed,                               // 3: random seed
                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        // SN_SINGLE INFO: 25
                        stardata->star[sn_k].mass,                                  // 1: post SN mass
                        stardata->star[sn_k].stellar_type,                          // 2: post SN stellar type
                        stardata->star[sn_k].SN_type,                               // 3: SN type

                        stardata->star[sn_k].fallback,                              // 4: SN fallback fraction
                        stardata->star[sn_k].fallback_mass,                         // 5: SN fallback mass
                        pre_SN_stardata_object->star[sn_k].mass,                    // 6: pre SN mass
                        pre_SN_stardata_object->star[sn_k].stellar_type,            // 7: pre SN stellar type

                        pre_SN_stardata_object->star[sn_k].radius,                  // 8: pre SN radius
                        pre_SN_stardata_object->star[sn_k].core_mass[ID_core(
                                    pre_SN_stardata_object->star[sn_k].stellar_type)], // 9: pre SN core mass
                        pre_SN_stardata_object->star[sn_k].core_mass[CORE_CO],      // 10: pre SN CO core mass
                        pre_SN_stardata_object->star[sn_k].core_mass[CORE_He],      // 11: pre SN He core mass

                        pre_SN_stardata_object->star[sn_k].omega /
                        pre_SN_stardata_object->star[sn_k].omega_crit,    // 12: pre SN fraction of critical rotation
                        stardata->star[sn_k].starnum,                               // 13: starnum of exploding star
                        stardata->model.event_based_logging_SN_counter,             // 14: SN counter

                        /* Kick */
                        stardata->star[sn_k].sn_kick_speed,                         // 15: supernova kick speed
                        stardata->star[sn_k].sn_kick_omega,                         // 16: supernova kick omega
                        stardata->star[sn_k].sn_kick_phi                            // 17: supernova kick phi
                    ) == 0)
            {
                Exit_binary_c(BINARY_C_ALLOC_FAILED,
                              "Could not alloc with asprintf in event_based_logging_handle_SN_event (2)\n");
            }

            /* Add event logstring list */
            if(stardata->preferences->event_based_logging[EVENT_BASED_LOGGING_SN] == TRUE)
            {
                event_based_logging_add_event_logstring_to_list(stardata,
                                                                supernova_string);
            }
            Safe_free(supernova_string);
        }
    }
}
#endif // EVENT_BASED_LOGGING
