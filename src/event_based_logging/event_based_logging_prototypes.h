#pragma once
#ifndef EVENT_BASED_LOGGING_PROTOTYPES_H
#define EVENT_BASED_LOGGING_PROTOTYPES_H

/* event based logging */
void event_based_logging_add_event_logstring_to_list(struct stardata_t * const stardata,
                                                     char * event_string);
void event_based_logging_print_event_logstrings(struct stardata_t * const stardata);


void event_based_logging_write_RLOF_event(struct stardata_t * const stardata);
void event_based_logging_set_final_RLOF_parameters(struct stardata_t * const stardata);
void event_based_logging_reset_RLOF_episode_values(struct stardata_t * const stardata);
void event_based_logging_set_initial_RLOF_parameters(struct stardata_t * const stardata);
void event_based_logging_update_cumulative_RLOF_parameters(struct stardata_t * const stardata);
void event_based_logging_set_all_RLOF_parameters(struct stardata_t * const stardata);

#undef X
#define X(TYPE) void event_based_logging_handle_##TYPE##_event(struct stardata_t * const stardata);
    EVENT_BASED_LOGGING_TYPES_LIST
#undef X

Boolean event_based_logging_on(struct stardata_t * const stardata);
void event_based_logging_handle_events(struct stardata_t * const stardata);

#endif // EVENT_BASED_LOGGING_PROTOTYPES_H
