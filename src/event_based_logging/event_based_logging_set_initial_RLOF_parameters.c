#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef EVENT_BASED_LOGGING

/*
 * Function to set all the initial RLOF episode values
 */

void event_based_logging_set_initial_RLOF_parameters(struct stardata_t * const stardata)
{
    /*
     * Do some checks to be sure
     */

    /*
     * Set initial values
     */
    stardata->common.event_based_logging_RLOF_episode_number = stardata->common.event_based_logging_RLOF_counter;

    /* RLOF episode initial values */
    stardata->common.event_based_logging_RLOF_episode_initial_mass_accretor = stardata->previous_stardata->star[stardata->model.naccretor].mass;
    stardata->common.event_based_logging_RLOF_episode_initial_mass_donor = stardata->previous_stardata->star[stardata->model.ndonor].mass;

    stardata->common.event_based_logging_RLOF_episode_initial_radius_accretor = stardata->previous_stardata->star[stardata->model.naccretor].radius;
    stardata->common.event_based_logging_RLOF_episode_initial_radius_donor = stardata->previous_stardata->star[stardata->model.ndonor].radius;

    stardata->common.event_based_logging_RLOF_episode_initial_separation = stardata->previous_stardata->common.orbit.separation;
    stardata->common.event_based_logging_RLOF_episode_initial_orbital_period = stardata->previous_stardata->common.orbit.period;

    stardata->common.event_based_logging_RLOF_episode_initial_stellar_type_accretor = stardata->previous_stardata->star[stardata->model.naccretor].stellar_type;
    stardata->common.event_based_logging_RLOF_episode_initial_stellar_type_donor = stardata->previous_stardata->star[stardata->model.ndonor].stellar_type;

    stardata->common.event_based_logging_RLOF_episode_initial_orbital_angular_momentum = stardata->previous_stardata->common.orbit.angular_momentum;
    stardata->common.event_based_logging_RLOF_episode_initial_stability = stardata->model.rlof_stability;

    stardata->common.event_based_logging_RLOF_episode_initial_starnum_accretor = stardata->previous_stardata->model.naccretor;
    stardata->common.event_based_logging_RLOF_episode_initial_starnum_donor = stardata->previous_stardata->model.ndonor;

    stardata->common.event_based_logging_RLOF_episode_initial_time = stardata->previous_stardata->model.time * 1e6;
    stardata->common.event_based_logging_RLOF_episode_initial_disk = stardata->model.event_based_logging_RLOF_disk_accretion;

    /* Debug prints */
    Dprint("event_based_logging_set_initial_RLOF_parameters\n");
    Dprint("stardata->common.event_based_logging_RLOF_episode_number: %d\n", stardata->common.event_based_logging_RLOF_episode_number);

    Dprint("stardata->common.event_based_logging_RLOF_episode_initial_mass_accretor: %g\n", stardata->common.event_based_logging_RLOF_episode_initial_mass_accretor);
    Dprint("stardata->common.event_based_logging_RLOF_episode_initial_mass_donor: %g\n", stardata->common.event_based_logging_RLOF_episode_initial_mass_donor);

    Dprint("stardata->common.event_based_logging_RLOF_episode_initial_radius_accretor: %g\n", stardata->common.event_based_logging_RLOF_episode_initial_radius_accretor);
    Dprint("stardata->common.event_based_logging_RLOF_episode_initial_radius_donor: %g\n", stardata->common.event_based_logging_RLOF_episode_initial_radius_donor);

    Dprint("stardata->common.event_based_logging_RLOF_episode_initial_separation: %g\n", stardata->common.event_based_logging_RLOF_episode_initial_separation);
    Dprint("stardata->common.event_based_logging_RLOF_episode_initial_orbital_period: %g\n", stardata->common.event_based_logging_RLOF_episode_initial_orbital_period);

    Dprint("stardata->common.event_based_logging_RLOF_episode_initial_stellar_type_accretor: %d\n", stardata->common.event_based_logging_RLOF_episode_initial_stellar_type_accretor);
    Dprint("stardata->common.event_based_logging_RLOF_episode_initial_stellar_type_donor: %d\n", stardata->common.event_based_logging_RLOF_episode_initial_stellar_type_donor);

    Dprint("stardata->common.event_based_logging_RLOF_episode_initial_orbital_angular_momentum: %g\n", stardata->common.event_based_logging_RLOF_episode_initial_orbital_angular_momentum);
    Dprint("stardata->common.event_based_logging_RLOF_episode_initial_stability: %d\n", stardata->common.event_based_logging_RLOF_episode_initial_stability);

    Dprint("stardata->common.event_based_logging_RLOF_episode_initial_starnum_accretor: %d\n", stardata->common.event_based_logging_RLOF_episode_initial_starnum_accretor);
    Dprint("stardata->common.event_based_logging_RLOF_episode_initial_starnum_donor: %d\n", stardata->common.event_based_logging_RLOF_episode_initial_starnum_donor);

    Dprint("stardata->common.event_based_logging_RLOF_episode_initial_time: %g\n", stardata->common.event_based_logging_RLOF_episode_initial_time);
    Dprint("stardata->common.event_based_logging_RLOF_episode_initial_disk: %d\n", stardata->common.event_based_logging_RLOF_episode_initial_disk);
    Dprint("\n");
}

#endif // EVENT_BASED_LOGGING
