#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef EVENT_BASED_LOGGING

/*
 * Function to add the DCO (WD, NS, BH) formation event to the event logstring handling
 * TODO: ensure that we only log whenever a stellar type change occurs, otherwise we print this continously.
 */

void event_based_logging_handle_DCO_event(struct stardata_t * const stardata)
{
    double formation_time_in_years;
    double inspiral_time_in_years;
    double merger_time_in_years;
    int total_rlof_episodes;
    int stable_rlof_episodes;
    int unstable_rlof_episodes;

    /*
     * Only log when either star has changed its stellar type compared
     * to the previous timestep
     */
    Boolean add_DCO_formation_event = FALSE;
    if(
        (stardata->star[0].stellar_type != stardata->previous_stardata->star[0].stellar_type)
        ||
        (stardata->star[1].stellar_type != stardata->previous_stardata->star[1].stellar_type)
    )
    {
        /*
         * and when both are compact object stellar types
         */
        if(
            ((stardata->star[0].stellar_type >= EVENT_BASED_LOGGING_MIN_DCO_ST) &&
             (stardata->star[0].stellar_type <= EVENT_BASED_LOGGING_MAX_DCO_ST))
            &&
            ((stardata->star[1].stellar_type >= EVENT_BASED_LOGGING_MIN_DCO_ST) &&
             (stardata->star[1].stellar_type <= EVENT_BASED_LOGGING_MAX_DCO_ST))
        )
        {
            add_DCO_formation_event = TRUE;
        }
    }

    /* log when system passed the above requirements */
    if(add_DCO_formation_event == TRUE)
    {
        /* Calculate relevant timescales */
        formation_time_in_years = stardata->model.time * 1e6;
        inspiral_time_in_years = Peters_grav_wave_merger_time(
                                     stardata,
                                     stardata->star[0].mass,
                                     stardata->star[1].mass,
                                     stardata->common.orbit.separation,
                                     stardata->common.orbit.eccentricity
                                 );
        merger_time_in_years = formation_time_in_years + inspiral_time_in_years;

        // // Count stable rlofs NOTE: this subtraction doesnt always remain positive. I'm not sure why, either the above counter isnt working, or the comenv counter sometimes counts more than it actually should?
        // stable_rlofs = Max(0, stardata->common.rlof_counter - stardata->model.comenv_count);

        total_rlof_episodes = stardata->common.event_based_logging_RLOF_counter;
        unstable_rlof_episodes = stardata->model.comenv_count;
        stable_rlof_episodes = Max(0, total_rlof_episodes - unstable_rlof_episodes);

        /* Construct all the string and fill with info */
        char * DCO_formation_string;
        if(asprintf(
                    &DCO_formation_string,
                    "DCO_formation "
                    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    // ZAMS: 5
                    "%g %g %g %g %g "       // ZAMS 1-5
                    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    // SYSTEM COMMON: 3
                    "%30.12e %g %llu "           // SYSTEM COMMON: 1-4
                    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    // DCO FORMATION:
                    "%d %d "                // DCO FORMATION: 1-2 stellar types
                    "%g %g "                // DCO FORMATION: 3-4 masses
                    "%g %g %g "             // DCO FORMATION: 5-7 orbital properties
                    "%g %g %g "             // DCO FORMATION: 8-10 previous orbital properties
                    "%g %g %g "             // DCO FORMATION: 11-13 relevant GW times
                    "%d %d %d"              // DCO FORMATION: 14-16 RLOF events count
                    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    // ZAMS INFO: 5
                    , stardata->common.zero_age.mass[0],                        // 1: ZAMS mass 1
                    stardata->common.zero_age.mass[1],                          // 2: ZAMS mass 2
                    stardata->common.zero_age.orbital_period[0],                               // 3: ZAMS orbital period
                    stardata->common.zero_age.separation[0],                           // 4: ZAMS separation
                    stardata->common.zero_age.eccentricity[0],                         // 5: ZAMS eccentricity
                    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    // SYSTEM COMMON: 3
                    stardata->model.time,                                       // 1: Time
                    stardata->common.metallicity,                               // 2: metallicity
                    stardata->common.random_seed,                                // 4: random seed
                    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    // DCO FORMATION:
                    /* stellar types */
                    stardata->star[0].stellar_type,                             // 1: stellar type star 1
                    stardata->star[1].stellar_type,                             // 2: stellar type star 2
                    /* masses */
                    stardata->star[0].mass,                                     // 3: mass star 1
                    stardata->star[1].mass,                                     // 4: mass star 2
                    /* orbital properties */
                    stardata->common.orbit.separation,                          // 5: system separation
                    stardata->common.orbit.eccentricity,                        // 6: system eccentricity
                    stardata->common.orbit.period,                              // 7: system period
                    /* previous orbital properties */
                    stardata->previous_stardata->common.orbit.separation,       // 8: previous system separation
                    stardata->previous_stardata->common.orbit.eccentricity,     // 9: previous system
                    stardata->previous_stardata->common.orbit.period,           // 10: previous system period
                    /* relevant times */
                    formation_time_in_years,                                    // 11: formation time in years
                    inspiral_time_in_years,                                     // 12: inspiral time in years
                    merger_time_in_years,                                       // 13: merger time in years
                    /* RLOF numbers */
                    total_rlof_episodes,                                        // 14: total RLOF episodes
                    stable_rlof_episodes,                                       // 15: stable RLOF episodes
                    unstable_rlof_episodes                                      // 16: unstable RLOF episodes
                ) == 0)
        {
            Exit_binary_c(BINARY_C_ALLOC_FAILED,
                          "Could not alloc with asprintf in event_based_logging_handle_DCO_formation_event\n");
        }

        /* Add event logstring list */
        if(stardata->preferences->event_based_logging[EVENT_BASED_LOGGING_DCO] == TRUE)
        {
            event_based_logging_add_event_logstring_to_list(stardata, DCO_formation_string);
        }
        Safe_free(DCO_formation_string);
    }
}
#endif // EVENT_BASED_LOGGING
