#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef EVENT_BASED_LOGGING

/* Function to print all the event logstrings */
void event_based_logging_print_event_logstrings(struct stardata_t * const stardata)
{
    /* Set the event header */
    static const char * const pre_string = "EVENT";

    /* Print all the events */
    //if(stardata->preferences->event_based_logging == TRUE)
    {
        if(stardata->common.event_based_logging_logstring_counter > 0)
        {
            for(int event_i = 0;
                    event_i < stardata->common.event_based_logging_logstring_counter;
                    event_i++)
            {
                Printf("%s %s %g %d %s\n",
                       pre_string,
                       stardata->model.uuid,
                       stardata->model.probability,
                       event_i,
                       stardata->common.event_based_logging_logstrings[event_i]);
            }
        }
    }
}
#endif // EVENT_BASED_LOGGING
