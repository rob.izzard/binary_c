#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef EVENT_BASED_LOGGING

/*
 * Function to add the event log string to the stardata
 */
void event_based_logging_add_event_logstring_to_list(struct stardata_t * const stardata,
        char * const event_string)
{
    /* Check if we can still add things */
    if(stardata->common.event_based_logging_logstring_counter < EVENT_BASED_LOGGING_MAX_EVENTS - 1)
    {
        /* Print some info */
        Dprint(
            "Putting %s in the stardata->common.event_based_logging_logstrings on place %d. Current uuid: %s\n",
            event_string,
            stardata->common.event_based_logging_logstring_counter,
            stardata->model.uuid
        );

        /* Copy the string into the array of event logstrings */
        const size_t length_string = strlen(event_string) + 1;
        strlcpy(
            stardata->common.event_based_logging_logstrings[stardata->common.event_based_logging_logstring_counter],
            event_string,
            length_string
        );
        stardata->common.event_based_logging_logstring_counter += 1;

        /* test print */
        Dprint(
            "Test_print: position %d in stardata->common.event_based_logging_logstrings contains %s\n",
            stardata->common.event_based_logging_logstring_counter - 1,
            stardata->common.event_based_logging_logstrings[stardata->common.event_based_logging_logstring_counter - 1]
        );
    }
    else
    {
        /* We're trying to add more than we can handle. This will be flagged and the system will fail */
        Dprint("Attempting to add more events than allowed. Cancelling the system and raising a failure\n")
        Exit_binary_c(BINARY_C_EVENT_BASED_LOGGING_ARRAY_OVERFILLED, "Event array overfilled.\n");
    }
}

#endif // EVENT_BASED_LOGGING
