#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef EVENT_BASED_LOGGING

/* Function to add the RLOF event */
void event_based_logging_write_RLOF_event(struct stardata_t * stardata)
{
    /* Construct all the string and fill with info */
    char * RLOF_string;
    if(asprintf(
                &RLOF_string,
                "RLOF "
                ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                // ZAMS: 5
                "%g %g %g %g %g "           // ZAMS 1-5
                ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                // SYSTEM COMMON: 3
                "%30.12e %g %llu "               // SYSTEM COMMON: 1-4
                ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                // RLOF INFO: 60
                /* initial values RLOF episode */
                "%g %g "                    // 1-2:     masses
                "%g %g "                    // 3-4:     radii
                "%g %g "                    // 5-6:     separation and orbital period
                "%d %d "                    // 7-8:     stellar types
                "%g %d "                    // 9-10:    orbital angular momentum and RLOF stability
                "%d %d "                    // 11-12:   star numbers
                "%30.12e %d "               // 13-14:   time and disk accretion

                /* final values */
                "%g %g "                    // 15-16:   masses
                "%g %g "                    // 17-18:   radii
                "%g %g "                    // 19-20:   separation and orbital period
                "%d %d "                    // 21-22:   stellar types
                "%g %d "                    // 23-24:   orbital angular momentum and RLOF stability
                "%d %d "                    // 25-26:   star numbers
                "%30.12e %d "               // 27-28:   time and disk accretion

                /* Cumulative total values RLOF episode */
                "%g %g %g %g %g %g "           // 29-34:   mass lost, mass accreted and mass transferred etc

                "%d"                       // 34:      episode_number
                ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                // ZAMS INFO: 5
                ,
                stardata->common.zero_age.mass[0],                      // 1: ZAMS mass 1
                stardata->common.zero_age.mass[1],                      // 2: ZAMS mass 2
                stardata->common.zero_age.orbital_period[0],                           // 3: ZAMS orbital period
                stardata->common.zero_age.separation[0],                       // 4: ZAMS separation
                stardata->common.zero_age.eccentricity[0],                     // 5: ZAMS eccentricity
                ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                // SYSTEM COMMON: 3
                stardata->model.time,                                   // 1: Time
                stardata->common.metallicity,                           // 2: metallicity
                stardata->common.random_seed,                           // 4: random seed
                ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                // RLOF INFO: 60

                /* initial values RLOF episode */
                // 1-2:                        masses
                stardata->common.event_based_logging_RLOF_episode_initial_mass_accretor,
                stardata->common.event_based_logging_RLOF_episode_initial_mass_donor,
                // 3-4:                        radii
                stardata->common.event_based_logging_RLOF_episode_initial_radius_accretor,
                stardata->common.event_based_logging_RLOF_episode_initial_radius_donor,
                // 5-6:                       separation and orbital period
                stardata->common.event_based_logging_RLOF_episode_initial_separation,
                stardata->common.event_based_logging_RLOF_episode_initial_orbital_period,
                // 7-8:                      stellar types
                stardata->common.event_based_logging_RLOF_episode_initial_stellar_type_accretor,
                stardata->common.event_based_logging_RLOF_episode_initial_stellar_type_donor,
                // 9-10:                      orbital angular momentum and RLOF stability
                stardata->common.event_based_logging_RLOF_episode_initial_orbital_angular_momentum,
                stardata->common.event_based_logging_RLOF_episode_initial_stability,
                // 11-12:                      star numbers
                stardata->common.event_based_logging_RLOF_episode_initial_starnum_accretor,
                stardata->common.event_based_logging_RLOF_episode_initial_starnum_donor,
                // 13-14:                      time and disk accretion
                stardata->common.event_based_logging_RLOF_episode_initial_time,
                stardata->common.event_based_logging_RLOF_episode_initial_disk,

                /* final values RLOF episode */
                // 15-16:                      masses
                stardata->common.event_based_logging_RLOF_episode_final_mass_accretor,
                stardata->common.event_based_logging_RLOF_episode_final_mass_donor,
                // 17-18:                      radii
                stardata->common.event_based_logging_RLOF_episode_final_radius_accretor,
                stardata->common.event_based_logging_RLOF_episode_final_radius_donor,
                // 19-20:                      separation and orbital period
                stardata->common.event_based_logging_RLOF_episode_final_separation,
                stardata->common.event_based_logging_RLOF_episode_final_orbital_period,
                // 21-22:                      stellar types
                stardata->common.event_based_logging_RLOF_episode_final_stellar_type_accretor,
                stardata->common.event_based_logging_RLOF_episode_final_stellar_type_donor,
                // 23-24:                      orbital angular momentum and RLOF stability
                stardata->common.event_based_logging_RLOF_episode_final_orbital_angular_momentum,
                stardata->common.event_based_logging_RLOF_episode_final_stability,
                // 25-26:                      star numbers
                stardata->common.event_based_logging_RLOF_episode_final_starnum_accretor,
                stardata->common.event_based_logging_RLOF_episode_final_starnum_donor,
                // 27-28:                      time and disk accretion
                stardata->common.event_based_logging_RLOF_episode_final_time,
                stardata->common.event_based_logging_RLOF_episode_final_disk,

                /* Cumulative total values RLOF episode */
                // 29-34:                      mass lost, mass accreted and mass transferred and time spent
                stardata->common.event_based_logging_RLOF_episode_total_mass_lost,
                stardata->common.event_based_logging_RLOF_episode_total_mass_accreted,
                stardata->common.event_based_logging_RLOF_episode_total_mass_transferred,
                stardata->common.event_based_logging_RLOF_episode_total_mass_lost_from_accretor,
                stardata->common.event_based_logging_RLOF_episode_total_mass_lost_from_common_envelope,
                stardata->common.event_based_logging_RLOF_episode_total_time_spent_masstransfer,

                /* 35: episode_number */
                stardata->common.event_based_logging_RLOF_episode_number
            ) == 0)
    {
        Exit_binary_c(BINARY_C_ALLOC_FAILED,
                      "Unable to construct string with asprintf in event_based_logging_write_RLOF_event.c");
    }

    Dprint("event_based_logging_write_RLOF_event\n");
    Dprint("stardata->common.event_based_logging_RLOF_episode_total_mass_lost: %g\n",
           stardata->common.event_based_logging_RLOF_episode_total_mass_lost);
    Dprint("stardata->common.event_based_logging_RLOF_episode_total_mass_accreted: %g\n",
           stardata->common.event_based_logging_RLOF_episode_total_mass_accreted);
    Dprint("stardata->common.event_based_logging_RLOF_episode_total_mass_transferred: %g\n",
           stardata->common.event_based_logging_RLOF_episode_total_mass_transferred);
    Dprint("stardata->common.event_based_logging_RLOF_episode_total_mass_lost_from_accretor: %g\n",
           stardata->common.event_based_logging_RLOF_episode_total_mass_lost_from_accretor);
    Dprint("stardata->common.event_based_logging_RLOF_episode_total_time_spent_masstransfer: %g\n",
           stardata->common.event_based_logging_RLOF_episode_total_time_spent_masstransfer);
    Dprint("\n");

    /* Add event logstring list */
    if(stardata->preferences->event_based_logging[EVENT_BASED_LOGGING_RLOF] == TRUE)
    {
        event_based_logging_add_event_logstring_to_list(stardata,
                                                        RLOF_string);
    }
    Safe_free(RLOF_string);
}

#endif // EVENT_BASED_LOGGING
