#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef EVENT_BASED_LOGGING

/*
 * Function to set all the RLOF parameters
 */
void event_based_logging_set_all_RLOF_parameters(struct stardata_t * stardata)
{
    /* Reset values */
    stardata->model.event_based_logging_RLOF = FALSE;
    stardata->model.event_based_logging_RLOF_unstable = FALSE;
    stardata->model.event_based_logging_RLOF_starting = FALSE;
    stardata->model.event_based_logging_RLOF_ending = FALSE;
    stardata->model.event_based_logging_RLOF_disk_accretion = FALSE;

    /* Check for rlof status and set the values */
    if(stardata->model.model_number > 0)
    {
        // Set the prev value from the prev stardata
        stardata->model.event_based_logging_RLOF_prev_in_RLOF = stardata->previous_stardata->model.in_RLOF;

        /* Some logic to register the start or end of the RLOF */
        stardata->model.event_based_logging_RLOF = (stardata->model.in_RLOF == TRUE && stardata->model.event_based_logging_RLOF_prev_in_RLOF == TRUE) ? TRUE : FALSE;
        stardata->model.event_based_logging_RLOF_starting = (stardata->model.in_RLOF == TRUE && stardata->model.event_based_logging_RLOF_prev_in_RLOF == TRUE && stardata->previous_stardata->model.event_based_logging_RLOF_prev_in_RLOF == FALSE) ? TRUE : FALSE;
        stardata->model.event_based_logging_RLOF_ending = (stardata->model.in_RLOF == FALSE && stardata->model.event_based_logging_RLOF_prev_in_RLOF == TRUE && stardata->previous_stardata->model.event_based_logging_RLOF_prev_in_RLOF == TRUE) ? TRUE : FALSE;

        /* The above checks work better for unstable MT. This method is to try to fix the unstable MT registration*/
        stardata->model.event_based_logging_RLOF_unstable = (stardata->model.comenv_count > stardata->previous_stardata->model.comenv_count) ? TRUE : FALSE;
        if(stardata->model.event_based_logging_RLOF_unstable == TRUE)
        {
            stardata->model.event_based_logging_RLOF = TRUE;
            if(stardata->previous_stardata->model.event_based_logging_RLOF_unstable == FALSE)
            {
                stardata->model.event_based_logging_RLOF_starting = TRUE;
            }
        }
        else
        {
            if(stardata->previous_stardata->model.event_based_logging_RLOF_unstable == TRUE)
            {
                stardata->model.event_based_logging_RLOF_ending = TRUE;
            }
        }

        // /* Check if the RLOF is unstable */
        // if(test_for_roche_lobe_overflow(stardata,
        //                                  TEST_RLOF_THIS_AND_PREVIOUS_TIMESTEP) ==
        //         EVOLUTION_ROCHE_OVERFLOW ? TRUE : FALSE)
        // {
        //     int stability = RLOF_stability_tests(stardata);

        //     if(stability > 0)
        //     {
        //         printf("UNSTABLE FOUND! stability: %d time: %g. comenv counter: %d \n", stability, stardata->model.time, stardata->model.comenv_count);
        //         printf("stardata->model.event_based_logging_RLOF: %d, stardata->model.event_based_logging_RLOF_starting: %d, stardata->model.event_based_logging_RLOF_ending: %d\n\n", stardata->model.event_based_logging_RLOF, stardata->model.event_based_logging_RLOF_starting, stardata->model.event_based_logging_RLOF_ending);
        //     }
        // }

        // if(stardata->model.event_based_logging_RLOF == TRUE)
        // {
        //     printf("RLOF time: %g stardata->model.event_based_logging_RLOF: %d.\n", stardata->model.time, stardata->model.event_based_logging_RLOF);
        //     printf("stability now: %d stability previous step: %d\n\n", RLOF_stability_tests(stardata), RLOF_stability_tests(stardata->previous_stardata));
        // }

        // if(stardata->model.event_based_logging_RLOF_starting == TRUE)
        // {
        //     printf("RLOF starting time: %g stardata->model.event_based_logging_RLOF: %d.\n", stardata->model.time, stardata->model.event_based_logging_RLOF);
        //     printf("stability now: %d stability previous step: %d\n\n", RLOF_stability_tests(stardata), RLOF_stability_tests(stardata->previous_stardata));
        // }

        // if(stardata->model.event_based_logging_RLOF_ending == TRUE)
        // {
        //     printf("RLOF ending time: %g stardata->model.event_based_logging_RLOF: %d.\n", stardata->model.time, stardata->model.event_based_logging_RLOF);
        //     printf("stability now: %d stability previous step: %d\n\n", RLOF_stability_tests(stardata), RLOF_stability_tests(stardata->previous_stardata));
        // }
    }

    /* Some sanity checks to prevent unwanted behaviour*/
    /* TODO: this might lead to solving some of the weird values that i see, but we should check if we still get all the unstable MTs*/
    if(stardata->model.event_based_logging_RLOF_unstable == FALSE)
    {
        if(stardata->model.event_based_logging_RLOF == TRUE)
        {
            if(stardata->common.orbit.separation == 0)
            {
                stardata->model.event_based_logging_RLOF = FALSE;
            }

            /* TODO: Add more checks here, radius etc */
            Foreach_star(star)
            {
                if(star->radius == 0 ||
                   star->luminosity == 0 ||
                   star->mass == 0)
                {
                    stardata->model.event_based_logging_RLOF = FALSE;
                }
            }
        }
    }

    /* If there is no RLOF set things to 0 again */
    if(stardata->model.event_based_logging_RLOF == FALSE)
    {
        stardata->model.rlof_type = RLOF_NO_RLOF;
        stardata->model.rlof_stability = RLOF_IMPOSSIBLE;
        stardata->star[0].RLOFing = FALSE;
        stardata->star[1].RLOFing = FALSE;
    }
    else
    {
        /* Set up extra checks for stability, type and disk accretion */
        //stardata->model.rlof_stability = RLOF_stability_tests(stardata->previous_stardata);

        // this should be the same?
        stardata->model.rlof_stability = stardata->previous_stardata->model.rlof_stability;

        // Printf("STABLE at t=%g? RLOF %s, stability %d \"%s\"\n",
        //        stardata->model.time,
        //        Yesno(RLOF),
        //        stability,
        //        RLOF_stability_string(stability)
        //     );

        // accretion disc check
        stardata->model.event_based_logging_RLOF_disk_accretion = RLOF_test_for_accretion_disc(stardata);

        // set_ndonor_and_naccretor
        set_ndonor_and_naccretor(stardata);

        // Increment counter if prev was not true
        if(stardata->model.event_based_logging_RLOF_starting == TRUE)
        {
            stardata->common.event_based_logging_RLOF_counter = stardata->common.event_based_logging_RLOF_counter + 1;
            // printf("Time: %g rlof_counter: %d naccretor: %d ndonor: %d\n",
            //     stardata->model.time,
            //     stardata->common.RLOF_episode_number,
            //     stardata->model.naccretor,
            //     stardata->model.ndonor
            // );
        }
    }
}

#endif // EVENT_BASED_LOGGING
