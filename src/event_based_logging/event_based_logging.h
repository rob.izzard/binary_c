#pragma once
#ifndef EVENT_BASED_LOGGING_MACROS_H
#define EVENT_BASED_LOGGING_MACROS_H

#ifdef EVENT_BASED_LOGGING

#include "event_based_logging.def"

#define EVENT_BASED_LOGGING_MAX_EVENTS 12
#define EVENT_BASED_LOGGING_MIN_DCO_ST HeWD
#define EVENT_BASED_LOGGING_MAX_DCO_ST BH

/*
 * List of event types : defined in
 * event_based_logging.def
 */
#undef X
#define X(TYPE) EVENT_BASED_LOGGING_##TYPE,
enum {
    EVENT_BASED_LOGGING_TYPES_LIST
    EVENT_BASED_LOGGING_NUMBER
};
#undef X

#endif // EVENT_BASED_LOGGING

#endif // EVENT_BASED_LOGGING_MACROS_H
