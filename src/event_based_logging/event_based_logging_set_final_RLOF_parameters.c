#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef EVENT_BASED_LOGGING

/*
 * Function to set all the final RLOF episode values
 */

void event_based_logging_set_final_RLOF_parameters(struct stardata_t * const stardata)
{
    /* RLOF episode final values */
    stardata->common.event_based_logging_RLOF_episode_final_mass_accretor = stardata->star[stardata->previous_stardata->model.naccretor].mass;
    stardata->common.event_based_logging_RLOF_episode_final_mass_donor = stardata->star[stardata->previous_stardata->model.ndonor].mass;

    stardata->common.event_based_logging_RLOF_episode_final_radius_accretor = stardata->star[stardata->previous_stardata->model.naccretor].radius;
    stardata->common.event_based_logging_RLOF_episode_final_radius_donor = stardata->star[stardata->previous_stardata->model.ndonor].radius;

    stardata->common.event_based_logging_RLOF_episode_final_separation = stardata->common.orbit.separation;
    stardata->common.event_based_logging_RLOF_episode_final_orbital_period = stardata->common.orbit.period;

    stardata->common.event_based_logging_RLOF_episode_final_stellar_type_accretor = stardata->star[stardata->previous_stardata->model.naccretor].stellar_type;
    stardata->common.event_based_logging_RLOF_episode_final_stellar_type_donor = stardata->star[stardata->previous_stardata->model.ndonor].stellar_type;

    stardata->common.event_based_logging_RLOF_episode_final_orbital_angular_momentum = stardata->common.orbit.angular_momentum;
    stardata->common.event_based_logging_RLOF_episode_final_stability = stardata->previous_stardata->model.rlof_stability;
    stardata->common.event_based_logging_RLOF_episode_final_starnum_accretor = stardata->previous_stardata->model.naccretor;
    stardata->common.event_based_logging_RLOF_episode_final_starnum_donor = stardata->previous_stardata->model.ndonor;

    stardata->common.event_based_logging_RLOF_episode_final_time = stardata->model.time * 1e6;
    stardata->common.event_based_logging_RLOF_episode_final_disk = stardata->model.event_based_logging_RLOF_disk_accretion;

    /* Debug prints */
    Dprint("event_based_logging_set_final_RLOF_parameters\n");
    Dprint("stardata->common.event_based_logging_RLOF_episode_number: %d\n", stardata->common.event_based_logging_RLOF_episode_number);

    Dprint("stardata->common.event_based_logging_RLOF_episode_final_mass_accretor: %g\n", stardata->common.event_based_logging_RLOF_episode_final_mass_accretor);
    Dprint("stardata->common.event_based_logging_RLOF_episode_final_mass_donor: %g\n", stardata->common.event_based_logging_RLOF_episode_final_mass_donor);

    Dprint("stardata->common.event_based_logging_RLOF_episode_final_radius_accretor: %g\n", stardata->common.event_based_logging_RLOF_episode_final_radius_accretor);
    Dprint("stardata->common.event_based_logging_RLOF_episode_final_radius_donor: %g\n", stardata->common.event_based_logging_RLOF_episode_final_radius_donor);

    Dprint("stardata->common.event_based_logging_RLOF_episode_final_separation: %g\n", stardata->common.event_based_logging_RLOF_episode_final_separation);
    Dprint("stardata->common.event_based_logging_RLOF_episode_final_orbital_period: %g\n", stardata->common.event_based_logging_RLOF_episode_final_orbital_period);

    Dprint("stardata->common.event_based_logging_RLOF_episode_final_stellar_type_accretor: %d\n", stardata->common.event_based_logging_RLOF_episode_final_stellar_type_accretor);
    Dprint("stardata->common.event_based_logging_RLOF_episode_final_stellar_type_donor: %d\n", stardata->common.event_based_logging_RLOF_episode_final_stellar_type_donor);

    Dprint("stardata->common.event_based_logging_RLOF_episode_final_orbital_angular_momentum: %g\n", stardata->common.event_based_logging_RLOF_episode_final_orbital_angular_momentum);
    Dprint("stardata->common.event_based_logging_RLOF_episode_final_stability: %d\n", stardata->common.event_based_logging_RLOF_episode_final_stability);

    Dprint("stardata->common.event_based_logging_RLOF_episode_final_starnum_accretor: %d\n", stardata->common.event_based_logging_RLOF_episode_final_starnum_accretor);
    Dprint("stardata->common.event_based_logging_RLOF_episode_final_starnum_donor: %d\n", stardata->common.event_based_logging_RLOF_episode_final_starnum_donor);

    Dprint("stardata->common.event_based_logging_RLOF_episode_final_time: %g\n", stardata->common.event_based_logging_RLOF_episode_final_time);
    Dprint("stardata->common.event_based_logging_RLOF_episode_final_disk: %d\n", stardata->common.event_based_logging_RLOF_episode_final_disk);

    Dprint("\n");
}

#endif // EVENT_BASED_LOGGING
