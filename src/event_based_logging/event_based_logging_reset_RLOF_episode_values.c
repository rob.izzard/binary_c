#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef EVENT_BASED_LOGGING

/*
 * Function to reset all the RLOF episode values
 */
void event_based_logging_reset_RLOF_episode_values(struct stardata_t * const stardata)
{
    /* New RLOF episode information */
    stardata->common.event_based_logging_RLOF_episode_number = 0;

    /* RLOF episode initial values */
    stardata->common.event_based_logging_RLOF_episode_initial_mass_accretor = 0.0;
    stardata->common.event_based_logging_RLOF_episode_initial_mass_donor = 0.0;

    stardata->common.event_based_logging_RLOF_episode_initial_radius_accretor = 0.0;
    stardata->common.event_based_logging_RLOF_episode_initial_radius_donor = 0.0;

    stardata->common.event_based_logging_RLOF_episode_initial_separation = 0.0;
    stardata->common.event_based_logging_RLOF_episode_initial_orbital_period = 0.0;

    stardata->common.event_based_logging_RLOF_episode_initial_stellar_type_accretor = 0;
    stardata->common.event_based_logging_RLOF_episode_initial_stellar_type_donor = 0;

    stardata->common.event_based_logging_RLOF_episode_initial_orbital_angular_momentum = 0.0;
    stardata->common.event_based_logging_RLOF_episode_initial_stability = 0;

    stardata->common.event_based_logging_RLOF_episode_initial_starnum_accretor = 0;
    stardata->common.event_based_logging_RLOF_episode_initial_starnum_donor = 0;

    stardata->common.event_based_logging_RLOF_episode_initial_time = 0.0;
    stardata->common.event_based_logging_RLOF_episode_initial_disk = 0;

    /* RLOF episode final values */
    stardata->common.event_based_logging_RLOF_episode_final_mass_accretor = 0.0;
    stardata->common.event_based_logging_RLOF_episode_final_mass_donor = 0.0;

    stardata->common.event_based_logging_RLOF_episode_final_radius_accretor = 0.0;
    stardata->common.event_based_logging_RLOF_episode_final_radius_donor = 0.0;

    stardata->common.event_based_logging_RLOF_episode_final_separation = 0.0;
    stardata->common.event_based_logging_RLOF_episode_final_orbital_period = 0.0;

    stardata->common.event_based_logging_RLOF_episode_final_stellar_type_accretor = 0;
    stardata->common.event_based_logging_RLOF_episode_final_stellar_type_donor = 0;

    stardata->common.event_based_logging_RLOF_episode_final_orbital_angular_momentum = 0.0;
    stardata->common.event_based_logging_RLOF_episode_final_stability = 0;

    stardata->common.event_based_logging_RLOF_episode_final_starnum_accretor = 0;
    stardata->common.event_based_logging_RLOF_episode_final_starnum_donor = 0;

    stardata->common.event_based_logging_RLOF_episode_final_time = 0.0;
    stardata->common.event_based_logging_RLOF_episode_final_disk = 0;

    /* During RLOF */
    stardata->common.event_based_logging_RLOF_episode_total_time_spent_masstransfer = 0;

    stardata->common.event_based_logging_RLOF_episode_total_mass_lost_from_common_envelope = 0.0;
    stardata->common.event_based_logging_RLOF_episode_total_mass_lost_from_accretor = 0.0;
    stardata->common.event_based_logging_RLOF_episode_total_mass_lost = 0.0;
    stardata->common.event_based_logging_RLOF_episode_total_mass_accreted = 0.0;
    stardata->common.event_based_logging_RLOF_episode_total_mass_transferred = 0.0;

    /* MT through disk categories */
    stardata->common.event_based_logging_RLOF_episode_total_mass_transferred_through_disk = 0.0;
    stardata->common.event_based_logging_RLOF_episode_total_time_spent_disk_masstransfer = 0.0;
}

#endif // EVENT_BASED_LOGGING
