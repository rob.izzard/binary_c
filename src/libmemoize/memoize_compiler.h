#pragma once
#ifndef MEMOIZE_COMPILER_H
#define MEMOIZE_COMPILER_H


/*
 * Compiler-dependent code options
 */
#ifndef RESTRICT
#if defined __GNUC__ && __GNUC__ >= 4
#define RESTRICT __restrict__
#endif // __GNUC__
#endif

#ifndef RESTRICT
#define RESTRICT /* do nothing */
#endif


/*
 * Do we have an RDTSC timer?
 */
#ifndef MEMOIZE_HAVE_RDTSC
#if defined __GNUC__ &&                                                 \
    (defined __i386__ || defined __x86_64__ || defined __ia64__)
#define MEMOIZE_HAVE_RDTSC
#endif
#endif

#ifndef MEMOIZE_HAVE_RDTSC
/*
 * Need other headers for backup timer function
 */
#include <time.h>
#include <stdint.h>
#endif

/*
 * Compiler-specific settings
 */
#ifdef __clang__
#define MEMOIZE_EXPLICIT_RESULT_SET
#endif // __clang__


#ifndef Pure_function
#if defined __GNUC__ && __GNUC__ >=3
#define Pure_function __attribute__((pure))
#endif
#endif
#ifndef Constant_function
#if defined __GNUC__ && __GNUC__ >=3
#define Constant_function __attribute__((const))
#endif
#endif
#ifndef No_return
#if defined __GNUC__ && __GNUC__ >=4
#define No_return __attribute__((noreturn))
#endif
#endif
#ifndef Gnu_format_args
#if (__GNUC__ ==4 && __GNU_MINOR__>=7) || __GNUC__ > 4
#define Gnu_format_args(...) __attribute__((format (gnu_printf,__VA_ARGS__)))
#endif
#endif


#ifndef Constant_function
#define Constant_function
#endif
#ifndef Pure_function
#define Pure_function
#endif
#ifndef No_return
# define No_return
#endif
#ifndef Gnu_format_args
# define Gnu_format_args(...) 
#endif





#endif // MEMOIZE_COMPILER
