#pragma once
#ifndef MEMOIZE_H
#define MEMOIZE_H

#ifndef DISABLE_LIBMEMOIZE
#define MEMOIZE
#endif // DISABLE_LIBMEMOIZE

#ifdef MEMOIZE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef MEMOIZE_STATS
#define MEMOIZE_GETTICKS
#include <x86intrin.h>
#endif
#include "memoize_compiler.h"

/*
 * Header file for the memoize library.
 *
 * For details please see memoize.c
 *
 * Please see the LICENCE file for licensing conditions.
 */

#define MEMOIZE_VERSION "1.6"
#define MEMOIZE_MAJOR_VERSION 1
#define MEMOIZE_MINOR_VERSION 6

/*
 * Variable types
 */
#ifdef MEMOIZE_GETTICKS
typedef unsigned long long ticks;
#endif // MEMOIZE_STATS

struct memoize_hash_item_t {
    char * funcname;
    char * parameter; // was void*
    char * result; // was void*
    size_t parameter_memsize;
    size_t result_memsize;
    size_t n;
#ifdef MEMOIZE_STATS
    ticks tmisses;
    ticks thits;
    long int nhits;
    long int nmisses;
#endif //MEMOIZE_STATS
};

struct memoize_hash_t {
    size_t n;
    unsigned int free_results_pointers;
    struct memoize_hash_item_t * item;
#ifdef MEMOIZE_STATS
    /* counters */
    long int nhits;
    long int nmisses;
    /* timers */
    ticks toverheads;
    ticks tsearch;
    ticks tstore;
#endif //MEMOIZE_STATS
};

/*
 * Type for the memo hash and pointer to a memo hash
 */
#define MEMO_TYPE struct memoize_hash_t * const RESTRICT
#define MEMO_PTYPE struct memoize_hash_t * RESTRICT * const RESTRICT

#ifdef __MEMOIZE__

#define ALLOC_CHECKS




#ifdef MEMOIZE_GETTICKS
/*
 * timer function : currently we use the rdtsc timer
 */
#ifdef _MSC_VER
#include <intrin.h>
#else
#include <x86intrin.h>
#endif //_MSC_VER

ticks getticks(void) {

#ifdef MEMOIZE_HAVE_RDTSC
    /*
     * Use inbuilt __rdtsc function as a timer
     */
    return __rdtsc();
#else
    /*
     * Backup: use clock_gettime() to estimate the
     * number of "ticks".
     */
    struct timespec tp;
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID,&tp);
    ticks t = (ticks) (
        ((uint64_t)tp.tv_sec  * (1000000000U * 1e-3 * CPUFREQ))
        +
        ((uint64_t)tp.tv_nsec * (1e-3 * CPUFREQ))
        );
    return t;
#endif // MEMOIZE_HAVE_RDTSC


}
#endif // MEMOIZE_GETTICKS

#endif // __MEMOIZE__




/*
 * If we have GNU C extensions, we can use the native Elvis
 * operator.
 *
 * Otherwise, use the usual ternary with a temporary variable.
 *
 * Note: we can use typeof here because the calls to memoize_search_result
 *       and memoize_store_results are cast to RESULT_TYPE* pointers in
 *       the Memoize macros.
 */
#ifdef __GNUC__
#define Memoize_Elvis(A,B) ((A) ?: (B))
#else
#define Memoize_Elvis(A,B) __extension__        \
    ({typeof(A) * __x = (A); __x ? __x : (B);})
#endif//__GNUC__



#ifdef MEMOIZE_STATS
/*
 * Timers to measure performance
 */
#define Memoize_timer_define                    \
    ticks _memoize_timer,_memoize_timer_search,_memoize_timer_store
#define Memoize_timer_start                     \
    _memoize_timer = getticks()
#define Memoize_timer_end_overhead(MEMO)        \
    ((MEMO_TYPE)MEMO)->toverheads += getticks() - _memoize_timer
#define Memoize_timer_end_search(MEMO)          \
    ((MEMO_TYPE)MEMO)->tsearch += _memoize_timer_search - _memoize_timer
#define Memoize_timer_end_store(MEMO)           \
    ((MEMO_TYPE)MEMO)->tstore += _memoize_timer_store - _memoize_timer
#define Memoize_timer_arg_search                        \
    ,&_memoize_timer_search
#define Memoize_timer_arg_store                         \
    ,&_memoize_timer_store
#define Memoize_result_set _memoize_result =
#define Memoize_result_declare(RESULT_TYPE)     \
    RESULT_TYPE _memoize_result
#define Memoize_result                          \
    _memoize_result

#else
/* versions of the above macros that do nothing */
#define Memoize_timer_define
#define Memoize_timer_start
#define Memoize_timer_end_overhead(MEMO)
#define Memoize_timer_end_search(MEMO)
#define Memoize_timer_end_store(MEMO)
#define Memoize_timer_arg_search
#define Memoize_timer_arg_store
#define Memoize_result_set
#define Memoize_result_declare(RESULT_TYPE)
#define Memoize_result
#endif // MEMOIZE_STATS


#ifdef MEMOIZE_EXPLICIT_RESULT_SET
/*
 * GCC does not require that we explicitly return
 * a variable of the RESULT_TYPE, but clang does.
 */
#undef Memoize_result_set
#undef Memoize_result_declare
#undef Memoize_result
#define Memoize_result_set _memoize_result =
#define Memoize_result_declare(RESULT_TYPE)     \
    RESULT_TYPE _memoize_result
#define Memoize_result                          \
    _memoize_result
#endif // MEMOIZE_EXPLICIT_RESULT_SET



/*
 * number of * required for scalar or array data
 */
#define Memoize_pointer(SCALAR_OR_ARRAY) Memoize_pointer_primitive(SCALAR_OR_ARRAY)
#define Memoize_pointer_primitive(SCALAR_OR_ARRAY) Memoize_pointer_##SCALAR_OR_ARRAY
#define Memoize_pointer_scalar *
#define Memoize_pointer_array **

#define Memoize_set_pp(SCALAR_OR_ARRAY,PARAMETER_TYPE,NPARAMETERS,PARAMETER) \
    Memoize_pp_primitive(SCALAR_OR_ARRAY,PARAMETER_TYPE,NPARAMETERS,PARAMETER)
#define Memoize_pp_primitive(SCALAR_OR_ARRAY,PARAMETER_TYPE,NPARAMETERS,PARAMETER) \
    Memoize_pp_##SCALAR_OR_ARRAY(SCALAR_OR_ARRAY,PARAMETER_TYPE,NPARAMETERS,PARAMETER)

/* set pointer to scalar type */
#define Memoize_pp_scalar(SCALAR_OR_ARRAY,PARAMETER_TYPE,NPARAMETERS,PARAMETER) \
    Memoize_pp_scalar_primitive(SCALAR_OR_ARRAY,PARAMETER_TYPE,NPARAMETERS,PARAMETER)
#define Memoize_pp_scalar_primitive(SCALAR_OR_ARRAY,PARAMETER_TYPE,NPARAMETERS,PARAMETER) \
    PARAMETER_TYPE _x = (PARAMETER);                                    \
    _memoize_pp = (void*)((PARAMETER_TYPE Memoize_pointer(SCALAR_OR_ARRAY)) &_x);

/* set pointer to array type */
#define Memoize_pp_array(SCALAR_OR_ARRAY,PARAMETER_TYPE,NPARAMETERS,PARAMETER) \
    Memoize_pp_array_primitive(SCALAR_OR_ARRAY,PARAMETER_TYPE,NPARAMETERS,PARAMETER)
#define Memoize_pp_array_primitive(SCALAR_OR_ARRAY,PARAMETER_TYPE,NPARAMETERS,PARAMETER) \
    _memoize_pp = (void*)((PARAMETER_TYPE Memoize_pointer(SCALAR_OR_ARRAY)) &(PARAMETER));


/*
 * Include prototypes
 */
#include "memoize_prototypes.h"





/*
 * Wrapper macro for Memoizing
 */
#define Memoize(                                                        \
    MEMO,                                                               \
    FUNCNAME,                                                           \
    N,                                                                  \
    SCALAR_OR_ARRAY,                                                    \
    PARAMETER_TYPE,                                                     \
    NPARAMETERS,                                                        \
    PARAMETER,                                                          \
    RESULT_TYPE,                                                        \
    NRESULTS,                                                           \
    RESULT                                                              \
    )                                                                   \
    __extension__                                                       \
    ({                                                                  \
        Memoize_result_declare(RESULT_TYPE);                            \
        Memoize_timer_define;                                           \
        Memoize_timer_start;                                            \
                                                                        \
        /* store a pointer to the parameter(s) */                        \
        char * _memoize_pp;                                             \
        Memoize_set_pp(SCALAR_OR_ARRAY,                                 \
                       PARAMETER_TYPE,                                  \
                       NPARAMETERS,                                     \
                       PARAMETER);                                      \
                                                                        \
        /* initialize memo stack if required */                         \
        if((MEMO)==NULL) memoize_initialize(                            \
            (MEMO_PTYPE)&(MEMO));                                       \
                                                                        \
        Memoize_timer_end_overhead((MEMO_TYPE)MEMO);                    \
        Memoize_timer_start;                                            \
                                                                        \
        Memoize_result_set                                              \
            (*(RESULT_TYPE*)                                            \
             (                                                          \
                 Memoize_Elvis(                                         \
                     /* first do the search */                          \
                     (RESULT_TYPE*)memoize_search_result(               \
                         (MEMO_TYPE)(MEMO),                             \
                         (FUNCNAME),                                    \
                         _memoize_pp                                    \
                         Memoize_timer_arg_search                       \
                         ),                                             \
                     /* on failure, evaluate and store the result */    \
                     __extension__                                      \
                     ({                                                 \
                         Memoize_timer_start;                           \
                         const RESULT_TYPE _r = (RESULT);               \
                         (RESULT_TYPE*)memoize_store_result(            \
                             (MEMO_TYPE)(MEMO),                         \
                             (FUNCNAME),                                \
                             (N),                                       \
                             (NPARAMETERS)*sizeof(PARAMETER_TYPE),      \
                             _memoize_pp,                               \
                             (NRESULTS)*sizeof(RESULT_TYPE),            \
                             &_r                                        \
                             Memoize_timer_arg_store                    \
                             );                                         \
                 })                                                     \
                 )                                                      \
             )                                                          \
            );                                                          \
                                                                        \
        Memoize_timer_end_search((MEMO_TYPE)MEMO);                      \
        Memoize_timer_end_store((MEMO_TYPE)MEMO);                       \
        Memoize_result;                                                 \
    })









#else

/*
 * Version of the above macro for when MEMOIZE is disabled, just
 * returns the RESULT.
 */
#define Memoize(MEMO,                           \
                FUNCNAME,                       \
                N,                              \
                SCALAR_OR_ARRAY,                \
                PARAMETER_TYPE,                 \
                NPARAMETERS,                    \
                PARAMETER,                      \
                RESULT_TYPE,                    \
                NRESULTS,                       \
                RESULT)                         \
    (RESULT)

#endif // MEMOIZE

#endif // MEMOIZE_H
