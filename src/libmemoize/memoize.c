#define __BINARY_C_LINT_SKIP
#include "../binary_c_code_options.h"
#include "../binary_c_error_codes.h"
#include "../binary_c_exit_macros.h"
#include "../breakpoints/breakpoints_prototypes.h"
#include "../binary_c_exit_prototypes.h"

#ifndef __HAVE_LIBMEMOIZE__

#include "memoize.h"
#ifdef MEMOIZE
#include "memoize_internal.h"
/*
 * memoize.c, part of libmemoize
 *
 * A wrapper around C functions to memoize them, i.e. only call them
 * once for a given set of parameters.
 *
 * Please see the LICENCE file for licensing conditions.
 *
 * Usage:
 *
 * #include <memoize.h>
 *
 * Memoize(
 *      struct memoize_hash_t * RESTRICT memo,
 *      const char * RESTRICT funcname,
 *      const size_t nmax,
 *      (scalar|array),
 *      <parameter variable type>,
 *      const size_t n_parameters,
 *      const void * parameter,
 *      <parameter result type>,
 *      const size_t n_results,
 *      result
 *        );
 *
 * Where
 *
 * memo is a memoize_hash_t pointer that can be allocated.
 *      Note: to allocate memory manually, call memoize_initialize(memo);
 *      Note: to free memory call memoize_free(&memo);
 *
 * funcname is a string to identify your function. Use only one
 *      string per function.
 *
 * nmax is the maximum number of results that are stored for each function.
 *      Typically you want this number to be small, e.g. if you expect repeated
 *      calls with the same result, set it to 1.
 *
 * (scalar|array) : choose one of scalar or array. If you choose scalar,
 *      you can send the (single) parameter directly into parameter,
 *      if you choose array you have to wrap the parameter data in
 *      ((parameter_type[]){...}) (see example 2 below).
 *
 * <parameter variable type> is the C variable type of the parameters,
 *      e.g. int, char, double. We assume all parameters are of the same
 *      type, sorry (if you require different types, consider wrapping them
 *      in a struct: this will then be compared byte-by-byte to the memoize hash).
 *
 * n_parameters is the number of parameters pointed to by * parameter (below),
 *      e.g. for a scalar this is 1.
 *
 * parameter is the parameter data.
 *
 * <result variable type> is an C variable type of the results,
 *      e.g. int, char, double.
 *
 * n_results is the number of results provided by the function, .
 *      e.g. for a scalar this is 1.
 *
 * result is the result data. This can be an expression, a call to a
 *      function, etc. and can be a pointer to memory (e.g. to
 *      return an array of data or a struct).
 *
 *
 * Generally, Memoize is designed for scalar input (n_parameters == n_results == 1)
 * but by setting larger numbers there's no reason why arrays of data won't work.
 *
 * Similarly, Memoize has been tested on intrinsic C data types (e.g. double, int)
 * but should work on any complex data type.
 *
 *
 * Note: because we use a macro, RESULT is not calcualted until
 *       it is required, hence we gain some speed. The down side
 *       to this is that search_result and store_result must both
 *       check the hash for an existing key. This is (presumably) faster
 *       than the function to be memoized. You can only know by trying!
 *
 * Example: scalar in, scalar out
 *
 * double x = Memoize(
 *      disc->memo,
 *      "discT",
 *      1,
 *      scalar,
 *      double,
 *      1,
 *      radius,
 *      double,
 *      1,
 *      Max(0.0,generic_power_law(radius,disc,POWER_LAW_TEMPERATURE))
 *       );
 *
 * This example sets the result in the Max(...) call, where the result
 * is a single double and the parameter is also a single double ("radius").
 * The hash table at disc->memo is stored elsewhere, as is required.
 *
 * Example 2: array in, scalar out, to replace the pow function.
 *
 * #define POW(x,y) Memoize( \
 *       binary_c_memo,\
 *       "pow",\
 *       10, \
 *       array,\
 *       double,\
 *       2, \
 *       ((double[]){x,y}), \
 *       double,
 *       1,\
 *       pow(x,y))
 *
 * In this case, we replace calls to pow(x,y), a standard C-library function,
 * with a macro POW(x,y) which instead calls the memoized version of pow.
 * You can see that two double-precision parameters are passed in, x and y.
 * These must be encapsulated in an anonymous array, which is the
 * ((double)[]{x,y}). The outer brackets are required to avoid this being
 * treated as part of the POW macro. 10 results are stored in the memo
 * hash.
 * Now, instead of calling pow(x,y) you should call POW(x,y) to use the
 * memoized version.
 *
 * NB: you can override the pow library call with something like the following,
 * and remember to link with "-ldl":
 *
 * #include <dlfcn.h>
 * double (*orig_pow)(double,double) orig_pow = dlsym(RTLD_NEXT, "pow");
 * #define pow(x,y) Memoize(
 *       binary_c_memo,
 *       "pow",
 *       10,
 *       array,
 *       double,
 *       2,
 *       ((double[]){x,y}),
 *       double,
 *       1,
 *       orig_pow(x,y))
 *
 *
 * Three versions of the memoize library are built, libmemoize, libmemoize-debug
 * and libmemoize-stats.
 * libmemoize is the version to use in practice for maximum speed.
 * libmemoize-debug is the debugging version, which includes extra output.
 * libmemoize-stats includes statistics output and should be used for testing
 *
 *
 * The memoize library is based on the general idea, best expounded by
 *    https://perldoc.perl.org/Memoize.html
 * many thanks to the author of Memoize-perl.
 *
 * Note:
 *    Memory allocation is done with Malloc and checking for success is
 *    thus only done if ALLOC_CHECKS is defined. There is no checking here.
 *
 * Author: Robert Izzard
 * You have no guarantee that this works, and the author accepts no liability.
 * Originally part of the binary_c project https://gitlab.com/binary_c
 */

/* activate debugging if MEMOIZE_DEBUG is set */
#ifdef MEMOIZE_DEBUG
#define verbose
#define show_memcmp
#define show_status
#endif // MEMOIZE_DEBUG

#ifdef verbose
#define vb(...)                                 \
    printf("MEMO:");                            \
    fflush(stdout);                             \
    printf(__VA_ARGS__);                        \
    fflush(stdout);
#else
#define vb(...)
#endif // verbose

/*
 * Look up pointer to element J in hash H of type TYPE
 * (which is "result" or "parameter"). Note that this
 * is cast to a generic pointer type (void*).
 */
#define item(H,TYPE,J)                                  \
    (void*)((H)->TYPE + (H)->TYPE##_memsize * ((size_t)(J)))

void * memoize_store_result(struct memoize_hash_t * RESTRICT const memo,
                            const char * RESTRICT const funcname,
                            const size_t nmax,
                            const size_t parameter_memsize,
                            const void * const parameter,
                            const size_t result_memsize,
                            const void * const result
#ifdef MEMOIZE_STATS
                            ,ticks * const timer
#endif
    )
{
#ifdef MEMOIZE_STATS
    const ticks tstart = getticks();
#endif // MEMOIZE_STATS

    /*
     * Given function "funcname", a "parameter" and "result",
     * store in the memo hash. This assumes the result
     * was not found previously, so you should only call
     * this function if memoize_search_result has
     * already been called.
     *
     * Returns a pointer to the stored result, and cannot fail to do so.
     */
    vb("store_result: memo=%p funcname=\"%s\" nmax=%zu parameter=%p result=%p\n",
       (void*)memo,
       funcname,
       nmax,
       (void*)parameter,
       (void*)result
        );

    /*
     * If there's an exisiting hash key, use it.
     */
    struct memoize_hash_item_t * h =
        memoize_search_hash(memo,funcname);

    vb("store_result: h = %p\n",(void*)h);

    /*
     * No hash key was found for this function name.
     */
    if(h==NULL)
    {
        /*
         * Need a new hash key for this funcname.
         *
         * This should be a rare occurrance (~once for each function).
         */
        vb("store_result: need new hash item, realloc memo stack (sizeof %zu) at %p\n",
           sizeof(struct memoize_hash_item_t) * (memo->n+1),
           (void*)memo->item);

        /*
         * Increase the size of the list of hash items
         */
        memo->n++;
        void * const p =
            realloc(memo->item,
                    sizeof(struct memoize_hash_item_t) * (memo->n+1));
        if(p == NULL)
        {
            /* error */
            return NULL;
        }
        memo->item = p;
        vb("store_result: reallocd for new item at %p\n",(void*)memo->item);

        /*
         * h is the new item location (remember the -1!)
         */
        h = & memo->item[memo->n - 1];

        /*
         * Store the sizes and counts of parameters and results in h
         */
        h->parameter_memsize = parameter_memsize;
        h->result_memsize = result_memsize;

        /*
         * Add storage space for the parameters and results.
         */
        h->parameter = Malloc(nmax * h->parameter_memsize);
        if(h->parameter == NULL)
        {
            return NULL;
        }

        h->result    = Malloc(nmax * h->result_memsize);
        if(h->result == NULL)
        {
            return NULL;
        }
        /*
         * We don't have any parameters or results stored yet
         */
        h->n = 0;

#ifdef MEMOIZE_STATS
        /*
         * Set stats to zero
         */
        h->nhits = 0;
        h->nmisses = 0;
        h->tmisses = 0;
        h->thits = 0;
#endif // MEMOIZE_STATS

        /*
         * Store the funcname, remember the extra NULL character
         * and that strlcpy requires the length including the NULL
         * character.
         */
        const size_t s = strlen(funcname);
        h->funcname = Malloc((s+1)*sizeof(char));
        if(h->funcname == NULL)
        {
            return NULL;
        }
        strlcpy(h->funcname,funcname,s+1);

        vb("store_result: new is at # %zu, at %p (lists: parameter=%p (memsize %zu (double=%zu int=%zu char=%zu)) result=%p (memsize %zu)), h=%p, h->funcname=\"%s\", h->n=%zu, strlen %zu)\n",
           memo->n-1,
           (void*)memo->item,
           (void*)h->parameter,
           h->parameter_memsize,
           sizeof(double),
           sizeof(int),
           sizeof(char),
           (void*)h->result,
           h->result_memsize,
           (void*)h,
           h->funcname,
           h->n,
           s);
    }
    else
    {
        /*
         * A hash key already exists for this function.
         *
         * Shift elements by 1 in the stack.
         *
         * NB we shift n elements, where the stack is size nmax+1
         * so we never shift off the end (the n+1th element is just
         * overwritten)
         */
        vb("store_result: have hash item already\n");

#ifdef show_status
        memoize_status(memo);
#endif
        if(nmax>1)
        {
            vb("store_result: mv parameter %p to %p size %zu*%zu=%zu\n",
               (void*)h->parameter,
               (void*)(h->parameter+1),
               h->parameter_memsize,
               nmax-1,
               h->parameter_memsize*(nmax-1));
            memmove((char*)h->parameter + h->parameter_memsize,
                    h->parameter,
                    h->parameter_memsize*(nmax-1));

            vb("store_result: mv result %p to %p size %zu*%zu=%zu\n",
               (void*)h->result,
               (void*)(h->result   +1),
               h->result_memsize,nmax-1,h->result_memsize*(nmax-1));

            if(memo->free_results_pointers &&
               h->n+1 > nmax)
            {
                /*
                 * Free pointers which are shifted off
                 */
                Safe_free(*(char **)item(h,result,h->n-1));
            }

            memmove((char*)h->result + h->result_memsize,
                    h->result,
                    h->result_memsize*(nmax-1));
        }
        else
        {
            if(memo->free_results_pointers)
            {
                /*
                 * Free pointer which is shifted off
                 */
                Safe_free(*(char **)item(h,result,h->n-1));
            }
        }
    }

    /*
     * Increase number of stored items
     */
    h->n = Min(nmax,h->n+1);
    vb("store_result: h->n increased to %zu\n",h->n);

    /*
     * Store in the top of the stack
     */
    memcpy(item(h,parameter,0), parameter, h->parameter_memsize);
    memcpy(item(h,result,0),    result,    h->result_memsize);

    vb("store_result: Return pointer at %p (h=%p h->n=%zu)\n\n",
       (void*)item(h,result,0),
       (void*)h,
       h->n);

#ifdef show_status
    memoize_status(memo);
#endif

#ifdef MEMOIZE_STATS
    h->nmisses++;
    *timer = getticks();
    h->tmisses += *timer - tstart;
    memo->nmisses ++;
#endif // MEMOIZE_STATS

    return item(h,result,0);
}

struct memoize_hash_item_t Pure_function * memoize_search_hash(const struct memoize_hash_t * RESTRICT const memo,
                                                               const char * RESTRICT const funcname)
{
    /*
     * Search for a hash item matching given funcname.
     * Return a pointer to this item, or NULL if not found.
     */
    vb("search: try to match against memo=%p, funcname = \"%s\"\n",
       (void*)memo,
       funcname);
    vb("search: memo has n=%zu entries\n",
       memo->n);

    size_t i;
    for(i=0;i<memo->n;i++)
    {
        struct memoize_hash_item_t * h = & memo->item[i];

        vb("search : h = %p from i = %zu\n",(void*)h,i);
        vb("search: cf. item %zu at %p : item with %zu parameters, funcname = \"%s\" to required funcname = \"%s\"\n",
           i,
           (void*)h,
           h->n,
           h->funcname,
           funcname);

        if(strcmp(h->funcname,funcname)==0)
        {
            vb("search: found matching item : return %p\n",(void*)h);
            return h;
        }
    }
    vb("search: found no matching items, return NULL\n");
    return NULL;
}
struct memoize_hash_t * memoize_initialize(struct memoize_hash_t * RESTRICT * RESTRICT const m)
{
    /*
     * Initialize memo given a pointer to its new location memo (m),
     * with suitable defaults.
     *
     * Returns the pointer to the memo, or NULL on failure.
     */
    vb("initialize: Try to malloc %zu bytes\n",sizeof(struct memoize_hash_t));
    *m = Malloc(sizeof(struct memoize_hash_t));
    if(*m != NULL)
    {
        vb("initialize: Malloc'd memo at %p\n",(void*)*m);
        (*m)->n = 0;
        (*m)->item = NULL;
        (*m)->free_results_pointers = 0;
#ifdef MEMOIZE_STATS
        (*m)->nmisses = (*m)->nhits = 0;
        (*m)->tsearch = (*m)->tstore = (*m)->toverheads = 0;
#endif // MEMOIZE_STATS
    }
    return *m;
}
void memoize_clear_hash_contents(struct memoize_hash_t * RESTRICT const memo)
{
    /*
     * Do not free memory, just "clear" the hash contents
     * by settings n = 0 to ignore the function lists.
     */
    size_t i;
    for(i=0;i<memo->n;i++)
    {
       memo->item[i].n=0;
    }
}

void memoize_free(struct memoize_hash_t ** RESTRICT m)
{
    /*
     * Free memo
     */
    struct memoize_hash_t * memo = *m;
    size_t i;
    for(i=0;i<memo->n;i++)
    {
        struct memoize_hash_item_t * h = &memo->item[i];

#ifdef MEMOIZE_STATS
        printf("Memoize: function \"%s\" : %ld hits (%g s), %ld misses (%g s)\n",
               h->funcname,
               h->nhits,
               (double)h->thits,
               h->nmisses,
               (double)h->tmisses);
#endif // MEMOIZE_STATS
        if(memo->free_results_pointers)
        {
            /*
             * Optionally free pointers stored in
             * results hash items.
             */
            size_t j;
            for(j=0;j<h->n;j++)
            {
                char ** p = item(h,result,j); // was void **
                Safe_free(*p);
            }
        }
        Safe_free(h->parameter);
        Safe_free(h->result);
        Safe_free(h->funcname);
    }
#ifdef MEMOIZE_STATS
    /*
     * How efficient is memoize?
     *
     * We want tsearch to be large, and tstore+toverheads to be small,
     * define the efficency score as 100*(tsearch/(tstore+toverheads)-1)
     *
     * Note that toverheads is typically small.
     */
    double efficiency = 100.0 *
        (
        ((double)memo->tsearch)/
        ((double)memo->tstore +
         (double)memo->toverheads)-1.0);

    printf("Memoize: nhits = %ld, nmisses = %ld : tsearch = %g, tstore = %g, toverheads = %g : efficiency %5.2f %%\n",
           memo->nhits,
           memo->nmisses,
           (double)memo->tsearch,
           (double)memo->tstore,
           (double)memo->toverheads,
           efficiency
        );
#endif // MEMOIZE_STATS
    Safe_free(memo->item);
    Safe_free(*m);
}

void * Pure_function memoize_search_result(
#ifndef MEMOIZE_STATS
    const
#endif
    struct memoize_hash_t * RESTRICT const memo,
    const char * RESTRICT const funcname,
    const void * const parameter
#ifdef MEMOIZE_STATS
    ,ticks * const timer
#endif // MEMOIZE_STATS
    )
{
#ifdef MEMOIZE_STATS
    ticks tstart = getticks();
#endif // MEMOIZE_STATS
    /*
     * Search for a call to function "funcname"
     * with given parmaeter. Returns a pointer to the result,
     * or NULL if not found.
     */
    vb("search_result, memo = %p, funcname = \"%s\", parameter is at %p\n",
       (void*)memo,
       funcname,
       (void*)parameter);

    /*
     * First search for matching hash item.
     */
    vb("search_result : look for matching hash entry in memo=%p\n",
       (void*)memo);

    struct memoize_hash_item_t * h =
        memoize_search_hash(memo,funcname);

    vb("search_result : hash entry = %p in memo=%p\n",(void*)h,(void*)memo);

    if(h)
    {
        /*
         * found funcname in the hash, check we can match the
         * parameter, if so return a pointer to the result
         */
        size_t j;
        const size_t ncmp = h->parameter_memsize;

        for(j=0; j<h->n; j++)
        {
            vb("search_result: Match paramter at %p to h->parameter[%zu] at %p, memsize %zu : equal? %d\n",
               (void*)parameter,
               j,
               (void*)item(h,parameter,j),
               h->parameter_memsize,
               (int)((memcmp(parameter,item(h,parameter,j),ncmp)==0) ? 1 : 0)
                );

#ifdef show_memcmp
            {
                size_t k;
                unsigned char * x = (unsigned char *) parameter;
                unsigned char * y = (unsigned char *) item(h,parameter,j);
                printf("CMP pointers %p %p\n",(void*)x,(void*)y);
                for(k=0;k<ncmp;k++)
                {
                    printf("CMP byte %5zu/%5zu : input = %5hhu vs item = %5hhu : %s\n",
                           k,
                           ncmp,
                           *(x+k),
                           *(y+k),
                           *(x+k)==*(y+k) ? "same" : "different");
                }
            }
#endif


            /*
             * Use memcmp to compare : this seems to work fine for
             * floating point.
             */
            if(memcmp(parameter,item(h,parameter,j),ncmp)==0)
            {
                /*
                 * Matched: return pointer to the result
                 */
                vb("search_result: return pointer %zu at %p\n",
                   j,
                   (void*)item(h,result,j));
#ifdef MEMOIZE_STATS
                memo->nhits++;
                h->nhits++;
                *timer = getticks();
                h->thits += *timer - tstart;
#endif // MEMOIZE_STATS
                return item(h,result,j);
            }
        }
    }

    vb("search_result: return NULL (failed to match parameter to any in the hash)\n");

#ifdef show_status
    memoize_status(memo);
#endif
#ifdef MEMOIZE_STATS
    const ticks _dt = getticks() - tstart;
    *timer += _dt;
#endif // MEMOIZE_STATS

    /*
     * return NULL if there were no matches,
     * next memoize_store_result will be called
     */
    return NULL;
}


void memoize_status(const struct memoize_hash_t * const memo)
{
    /*
     * Show the status of the memoize stack
     */
    printf("Memoize status: \n");
    if(memo!=NULL)
    {
        printf("   memo = %p and contains %zu items\n",
               (void*)memo,
               memo->n);
        size_t i;
        for(i=0;i<memo->n;i++)
        {
            struct memoize_hash_item_t * h = &memo->item[i];
            printf("   %5zu at %p contains %zu parameter/result pairs for function %s : { ",
                   i,
                   (void*)h,
                   h->n,
                   h->funcname);
            fflush(stdout);

            size_t j;
            for(j=0;j<h->n;j++)
            {
                void * p = item(h,parameter,j);
                void * r = item(h,result,j);
                printf("%zu p=%p r=%p ",
                       j,(void*)p,(void*)r);
                if(j != h->n-1) printf(" : ");
            }
            printf(" }\n");
        }
    }
    else
    {
        printf(" ... nothing allocated :( \n");
    }
    printf("\n");
    fflush(stdout);
}

size_t Pure_function memoize_sizeof(struct memoize_hash_t * const memo)
{
    /*
     * Return the size of the memo hash and all its contents
     */
    size_t s=0;
    size_t i;
    for(i=0;i<memo->n;i++)
    {
        struct memoize_hash_item_t * h = &memo->item[i];
        s += (h->n+1) * (h->parameter_memsize + h->result_memsize);
        s += sizeof(char) * sizeof(h->funcname);
    }
    s += sizeof(memo);
    return s;
}


#endif // MEMOIZE

#endif // __HAVE_LIBMEMOIZE__
typedef int prevent_ISO_compile_warning;
        

