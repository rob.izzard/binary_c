#include "memoize_compiler.h"

/*
 * Estimate CPU frequency: we won't know this,
 * so "guess" at 1000Mhz. It doesn't really matter
 * for our purposes, we only care about relative numbers
 * of clock ticks.
 */
#ifndef CPUFREQ
#define CPUFREQ 1000
#endif

/*
 * Memoize function called "FUNCNAME" up to N times in the MEMO.
 * Initialize or store the result for parameter PARAMETER with
 * result RESULT.
 */

/*
 * Autotype(X) should use __auto_type if available,
 * but typeof if not.
 */
#if defined __GNUC__ && !defined Autotype
#if (__GNUC__ ==4 && __GNU_MINOR__>9) || __GNUC__ > 4
#define Autotype(X) __auto_type
#endif
#endif

#ifndef Autotype
#define Autotype(X) typeof(X)
#endif

/*
 * Branch prediction: use "likely" and "unlikely"
 */
#ifdef __GNUC__
#ifndef likely
#define likely(x)      __builtin_expect(!!(x), 1)
#endif
#ifndef unlikely
#define unlikely(x)    __builtin_expect(!!(x), 0)
#endif
#else
#ifndef likely
#define likely(x) (x)
#endif
#ifndef unlikely
#define unlikely(x) (x)
#endif
#endif // __GNUC__

#ifndef likely
#define likely(x)
#endif
#ifndef unlikely
#define unlikely(x)
#endif

/* macros to define less and more than operations */
#ifndef Less_than
#define Less_than(A,B) ((A)<(B))
#endif
#ifndef More_than
#define More_than(A,B) ((A)>(B))
#endif

/*
 * Min and Max of two numbers of generic type with only
 * one evalutation of each, taken from
 * https://gcc.gnu.org/onlinedocs/gcc/Typeof.html
 */
#ifndef Max
#define Max(A,B) __extension__                  \
    ({                                          \
            const Autotype(A) _a = (A);         \
            const Autotype(B) _b = (B);         \
            More_than(_a,_b) ? _a : _b;         \
        })
#endif
#ifndef Min
#define Min(A,B) __extension__                  \
    ({                                          \
            const Autotype(A) _a = (A);         \
            const Autotype(B) _b = (B);         \
            Less_than(_a,_b) ? _a : _b;         \
        })
#endif

/*
 * Check if a pointer has been allocated correctly.
 */
#ifndef _Alloc_check
#ifdef ALLOC_CHECKS
#define _Alloc_check(P)                                                 \
    if(unlikely((P)==NULL))                                             \
    {                                                                   \
        Exit_binary_c_no_stardata(BINARY_C_ALLOC_FAILED,"libmemoize memory allocation failed");                                                        \
    }
#endif // ALLOC_CHECKS
#endif //_Alloc_check

#ifndef _Alloc_check
#define _Alloc_check(P) /* _Alloc_check(P) do nothing */
#endif

/*
 * Memory allocation and freeing
 */
#ifndef Safe_free
#define Safe_free(PTR)                                                  \
    if((PTR)!=NULL)                                                     \
    {                                                                   \
        free(PTR);                                                      \
        (PTR)=NULL;                                                     \
    };
#endif
#ifndef Malloc
#define Malloc(S) (Memoize_malloc(S))
#endif
#ifndef Realloc
#define Realloc(P,S) (Memoize_realloc((P),(S)))
#endif

#ifndef strlcpy
#define strlcpy(d,s,n) snprintf((d),(n),"%s",(s))
#endif

static inline void *Memoize_malloc(size_t size);
static inline void *Memoize_realloc(void *ptr,
                                    size_t size);

static inline void *Memoize_malloc(size_t size)
{
    void * p = malloc(size);
    _Alloc_check(p);
    return p;
}

static inline void *Memoize_realloc(void *ptr,
                                    size_t size)
{
    void * p = realloc(ptr,size);
    _Alloc_check(p);
    return p;
}

typedef int prevent_ISO_compile_warning;

