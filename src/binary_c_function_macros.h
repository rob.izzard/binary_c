#pragma once
#ifndef BINARY_C_FUNCTION_MACROS_H
#define BINARY_C_FUNCTION_MACROS_H
#include <stddef.h>
/*
 * The binary_c stellar population nucleosynthesis framework.
 *
 * Copyright Robert Izzard 2000-2025
 *
 * Contact rob.izzard@gmail.com
 *
 * Code repository:
 * https://gitlab.com/binary_c
 *
 * Documentation is in doc/
 * Code is in src/
 *
 * To build, run
 *
 * ---
 *
 * meson setup builddir
 * cd builddir
 * ninja binary_c_install
 *
 * ---
 *
 * Please see the files README, LICENCE and CHANGES,
 * and the doc/ directory for documentation which is mirrored
 * at https://binary_c.gitlab.io
 *
 **********************
 *
 * Subroutine:
 *
 * Purpose:
 *
 * This file contains the main function macros
 * defined for binary_c, and several other included
 * files have groups of more specific function macros.
 */

#include "./events/events_function_macros.h"
#include "./control/control_function_macros.h"
#include "./data/data_table_function_macros.h"
#include "./buffering/buffering_function_macros.h"
#include "./maths/maths_function_macros.h"
#include "./string/string_function_macros.h"
#include "./binary_c_unit_function_macros.h"
#include "./binary_c_macro_macros.h"

/*
 * If we have GNU C extensions, we can use the native Elvis
 * operator.
 *
 * Otherwise, use the usual ternary with a temporary variable.
 *
 * Note: we can use typeof here because the calls to memoize_search_result
 *       and memoize_store_results are cast to RESULT_TYPE* pointers in
 *       the Memoize macros.
 */
#ifdef __GNUC__
#define Elvis(A,B) __extension__                \
    ((A) ?: (B))
#else
#define Elvis(A,B) Elvis_implementation((A),(B),Elvis,__COUNTER__)
#define Elvis_implementation(A,B,LABEL,LINE)        \
    __extension__                                   \
    ({                                              \
        typeof(A) * Concat3(__x,LABEL,LINE) = (A);  \
        Concat3(__x,LABEL,LINE) ?                   \
            Concat3(__x,LABEL,LINE) :               \
            (B);                                    \
    })
#endif//__GNUC__

#define Elvis3(A,B,C) (                         \
        Elvis((A),                              \
              Elvis((B),(C)))                   \
        )

#ifdef STACK_CHECKS
#define Set_stack_size                                                  \
    if(STACK_ACTION != STACK_DO_NOTHING)                                \
    {                                                                   \
        struct rlimit rl;                                               \
        if(likely(getrlimit(RLIMIT_STACK,&rl)==0))                      \
        {                                                               \
            unsigned long int stacksize = STACK_SIZE*1000L*1000L;       \
            if(rl.rlim_cur!=RLIM_INFINITY && rl.rlim_cur < stacksize)   \
            {                                                           \
                printf("Stack is too small: currently %ld (max %ld)\n", \
                       (long int) rl.rlim_cur,                          \
                       (long int)rl.rlim_max);                          \
                if(STACK_ACTION == STACK_SET)                           \
                {                                                       \
                    rl.rlim_cur = stacksize;                            \
                    if(setrlimit(RLIMIT_STACK,&rl)!=0)                  \
                    {                                                   \
                        fprintf(stderr,                                 \
                                "Unable to set stack to %ld MB (%ld bytes)\n", \
                                (long int)STACK_SIZE,                   \
                                (long int)STACK_SIZE*1024L*1024L);      \
                        Exit_binary_c_no_stardata(                      \
                            BINARY_C_STACK_FAIL,                        \
                            "Unable to set stack to %ld MB (%ld bytes)\n", \
                            (long int)STACK_SIZE,                       \
                            (long int)STACK_SIZE*1024L*1024L);          \
                    }                                                   \
                    else                                                \
                    {                                                   \
                        getrlimit(RLIMIT_STACK,&rl);                    \
                        printf("Stack set to %ld\n",                    \
                               (long int) rl.rlim_cur);                 \
                    }                                                   \
                }                                                       \
            }                                                           \
        }                                                               \
        else                                                            \
        {                                                               \
            fprintf(stderr,"Failed to determine current stack limit\n"); \
        }                                                               \
    }
#else
#define Set_stack_size /* */
#endif

/* Convert a C (integer) expression to Boolean type */
#define Boolean_(EXPR) ((EXPR) ? (TRUE) : (FALSE))

/*
 * Effective core mass
 */
#define Effective_core_mass(S) (                        \
        (ON_EITHER_MAIN_SEQUENCE((S)->stellar_type)) ?  \
        ((S)->mass) :                                   \
        ((S)->core_mass[CORE_He])                       \
        )

/* T_eff given L and R */
#define Teff_from_luminosity_and_radius(L,R)        \
    Pow1d4(                                         \
        (L)*L_SUN                                   \
        / (                                         \
            4.0 * PI * STEFAN_BOLTZMANN_CONSTANT *  \
            Pow2( (R)*R_SUN )                       \
            )                                       \
        )

/* T_eff - the effective temperature of star N */
#define Teff(N) (Teff_from_luminosity_and_radius(       \
                     stardata->star[(N)].luminosity,    \
                     stardata->star[(N)].radius))

/* same for a star struct (STAR) */
#define Teff_from_star_struct(STAR)             \
    (Teff_from_luminosity_and_radius(           \
        ((STAR)->luminosity),                   \
        ((STAR)->radius)))

#define HRdiagram(N)                            \
    log10(Teff(N)),                             \
        (log10(stardata->star[(N)].luminosity))

#define Envelope_mass(N) (stardata->star[(N)].mass-stardata->star[(N)].core_mass[CORE_He])

#define Q(N) (                                                          \
        Is_zero(stardata->star[Other_star(N)].mass) ?                   \
        1e50 :                                                          \
        (stardata->star[(N)].mass/stardata->star[Other_star(N)].mass)   \
        )

#define Mass_squared(B) (stardata->star[(B)].mass*stardata->star[(B)].mass)

#define Convective_star(N) (stardata->star[(N)].mass<MAX_CONVECTIVE_MASS && \
                            stardata->star[(N)].mass>MIN_CONVECTIVE_MASS)

#ifdef RLOF_RADIATION_CORRECTION
#define Roche_radius(Q,N) ((Q)<TINY ? 1e100 : ((N)*rL1((Q),stardata->preferences->RLOF_f)))
#else
#define Roche_radius(Q,N) ((Q)<TINY ? 1e100 : ((N)*rL1(Q)))
#endif
#define Roche_radius3(M1,M2,N) ((M2)<TINY ? 1e100 : Roche_radius(((M1)/(M2)),(N)))

/*
 * Surface hydrogen of given star.
 *
 * Note: if NUCSYN is not defined, this is approximate.
 */
#ifdef NUCSYN
#define Hydrogen_mass_fraction(STAR)                    \
    ((nucsyn_observed_surface_abundances(STAR))[XH1])
#else
#define Hydrogen_mass_fraction(STAR)                                    \
    ((STAR)->stellar_type <= TPAGB ? (0.760 - 3.0 * stardata->common.metallicity) : 0.0)
#endif


/*
 * Decimal spectral type converter
 */
#define Float_spectral_type_to_integer(N)       \
    ((int)                                      \
     ((N)+0.05))
#define Float_spectral_type_to_subclass(N)                          \
    ((int)                                                          \
     ((int)(10.0 *((N)-Float_spectral_type_to_integer(N))+0.5)))

/*
 * Code timing
 */
#define Timer_seconds(N) (((float)(N))/((1e6*(float)CPUFREQ)))
#define Timer_microseconds(N) (((float)(N))/(((float)CPUFREQ)))

/*
 * The fraction of blackbody flux between energies E0 and E1
 * given a blackbody at temperature T
 */

#define Blackbody_flux_fraction(T,E0,E1)            \
    (blackbody_fraction((E0)/PLANCK_CONSTANT,(T)) - \
     blackbody_fraction((E1)/PLANCK_CONSTANT,(T)))


/*
 * Check for an eccentricity that is valid
 */
#define Valid_eccentricity(E) Boolean_(In_range((E),0.0,1.0))


/*
 * Macro to determine the AGB core algorithm
 */
#ifdef NUCSYN
#define AGB_Core_Algorithm                                              \
    ((Core_algorithm)                                                   \
     ((stardata->preferences->AGB_core_algorithm==AGB_CORE_ALGORITHM_DEFAULT || \
       stardata->preferences->AGB_core_algorithm==AGB_CORE_ALGORITHM_KARAKAS) \
      ? AGB_CORE_ALGORITHM_KARAKAS : AGB_CORE_ALGORITHM_HURLEY)         \
        )
#else//NUCSYN
#define AGB_Core_Algorithm                                              \
    ((Core_algorithm)                                                   \
     ((stardata->preferences->AGB_core_algorithm==AGB_CORE_ALGORITHM_DEFAULT) \
      ? AGB_CORE_ALGORITHM_HURLEY : AGB_CORE_ALGORITHM_KARAKAS)         \
        )
#endif//NUCSYN

/*
 * Macro to determine the AGB radius algorithm
 */
#ifdef NUCSYN
#define AGB_Radius_Algorithm                                            \
    ((Radius_algorithm)                                                 \
     ((stardata->preferences->AGB_radius_algorithm==AGB_RADIUS_ALGORITHM_DEFAULT || \
       stardata->preferences->AGB_radius_algorithm==AGB_RADIUS_ALGORITHM_KARAKAS) \
      ? AGB_RADIUS_ALGORITHM_KARAKAS : AGB_RADIUS_ALGORITHM_HURLEY)     \
        )
#else//NUCSYN
#define AGB_Radius_Algorithm                                            \
    ((Radius_algorithm)                                                 \
     ((stardata->preferences->AGB_radius_algorithm==AGB_RADIUS_ALGORITHM_DEFAULT) \
      ? AGB_RADIUS_ALGORITHM_HURLEY : AGB_RADIUS_ALGORITHM_KARAKAS)     \
        )
#endif//NUCSYN

/*
 * Macro to determine the AGB luminosity algorithm
 */
#ifdef NUCSYN
#define AGB_Luminosity_Algorithm                                        \
    ((Luminosity_algorithm)                                             \
     ((stardata->preferences->AGB_luminosity_algorithm==AGB_LUMINOSITY_ALGORITHM_DEFAULT || \
       stardata->preferences->AGB_luminosity_algorithm==AGB_LUMINOSITY_ALGORITHM_KARAKAS) \
      ? AGB_LUMINOSITY_ALGORITHM_KARAKAS : AGB_LUMINOSITY_ALGORITHM_HURLEY) \
        )
#else//NUCSYN
#define AGB_Luminosity_Algorithm                                        \
    ((Luminosity_algorithm)                                             \
     ((stardata->preferences->AGB_luminosity_algorithm==AGB_LUMINOSITY_ALGORITHM_DEFAULT) \
      ? AGB_LUMINOSITY_ALGORITHM_HURLEY : AGB_LUMINOSITY_ALGORITHM_KARAKAS) \
        )
#endif//NUCSYN

/*
 * Macro to determine the AGB third dredge up algorithm
 */
#ifdef NUCSYN
#define AGB_Third_Dredge_Up_Algorithm                                   \
    ((Third_dredge_up_algorithm)(                                       \
        stardata->preferences->AGB_3dup_algorithm==AGB_THIRD_DREDGE_UP_ALGORITHM_STANCLIFFE ? \
        AGB_THIRD_DREDGE_UP_ALGORITHM_STANCLIFFE :                      \
        ((stardata->preferences->AGB_3dup_algorithm==AGB_THIRD_DREDGE_UP_ALGORITHM_DEFAULT || \
          stardata->preferences->AGB_3dup_algorithm==AGB_THIRD_DREDGE_UP_ALGORITHM_KARAKAS) \
         ? AGB_THIRD_DREDGE_UP_ALGORITHM_KARAKAS : AGB_THIRD_DREDGE_UP_ALGORITHM_HURLEY) \
        ))
#else//NUCSYN
#define AGB_Third_Dredge_Up_Algorithm                                   \
    ((Third_dredge_up_algorithm)(                                       \
        stardata->preferences->AGB_3dup_algorithm==AGB_THIRD_DREDGE_UP_ALGORITHM_STANCLIFFE ? \
        AGB_THIRD_DREDGE_UP_ALGORITHM_STANCLIFFE :                      \
        ((stardata->preferences->AGB_3dup_algorithm==AGB_THIRD_DREDGE_UP_ALGORITHM_DEFAULT) \
         ? AGB_THIRD_DREDGE_UP_ALGORITHM_HURLEY : AGB_THIRD_DREDGE_UP_ALGORITHM_KARAKAS)))
#endif//NUCSYN


/*
 * Macros to change the state of buffering
 */
#define Save_buffering_state                                        \
    const int __buf = stardata->preferences->internal_buffering;    \
    const int __batchmode = stardata->preferences->batchmode;

#define Restore_buffering_state                         \
    stardata->preferences->internal_buffering = __buf;  \
    stardata->preferences->batchmode = __batchmode;


#ifdef TRIPLE

/*
 * Macros to give outer/inner label depending
 * on the component integer, X
 */
#define Component_string(X) (                           \
            (X) == TRIPLE_COMPONENT_OUTER ? "Outer" :   \
            (X) == TRIPLE_COMPONENT_INNER ? "Inner" :   \
            "Unknown"                                   \
            )
#define component_string(X) (                           \
            (X) == TRIPLE_COMPONENT_OUTER ? "outer" :   \
            (X) == TRIPLE_COMPONENT_INNER ? "inner" :   \
            "unknown"                                   \
            )

#endif//TRIPLE


/*
 * How many previous stardatas do we need?
 */
#define Solver_n_previous_stardatas(S)          \
    ((S)==SOLVER_FORWARD_EULER ? 1 :            \
     (S)==SOLVER_RK2 ? 1 :                      \
     1)

#ifdef CODESTATS
#define Increment_codestat(N)                   \
    increment_codestat((const int)(N),          \
                       (const char*)__FILE__,   \
                       (const int)__LINE__);
#else
#define Increment_codestat(N) /* do nothing */
#endif//CODESTATS


/*
 * We cannot map RLOF_stability_string to an
 * array of strings, because some of the options
 * for X are negative. Sorry!
 */
#define RLOF_stability_string(X)                                        \
    (                                                                   \
        (X)==RLOF_IMPOSSIBLE ? "Impossible" :                           \
        (X)==RLOF_INSTABILITY_BLOCKED ? "Instability blocked" :         \
        (X)==RLOF_STABLE ? "Stable" :                                   \
        (X)==RLOF_UNSTABLE ? "Unstable" :                               \
        (X)==RLOF_UNSTABLE_LOW_MASS_MS_COMENV ? "Unstable (low mass MS comenv)" : \
        (X)==RLOF_UNSTABLE_GIANT_COMENV ? "Unstable (giant comenv)" :   \
        (X)==RLOF_UNSTABLE_WD_COMENV ? "Unstable (WD comenv)" :         \
        (X)==RLOF_UNSTABLE_NS ? "Unstable (Neutron star)" :             \
        (X)==RLOF_UNSTABLE_BH ? "Unstable (Black hole)" :               \
        (X)==RLOF_UNSTABLE_MS_MERGER ? "Unstable (MS merger)" :         \
        (X)==RLOF_UNSTABLE_VERY_LARGE_DONOR ? "Unstable (Very large donor)" : \
        "RLOF stability unknown!"                                       \
        )

#define Map_float_algorithm(X)                  \
    ((int)((X)-0.4))


/*
 * Macro to define whether a system is single (or not)
 */
/*
  #define _System_is_single(S) \
  (Boolean_(                                                          \
  (                                                               \
  (S)->model.sgl == TRUE ||                                   \
  (S)->star[1].stellar_type == MASSLESS_REMNANT ||            \
  (S)->star[0].stellar_type == MASSLESS_REMNANT ||            \
  (S)->common.orbit.eccentricity < -TINY ||                   \
  (S)->common.orbit.separation <                              \
  MINIMUM_SEPARATION_TO_BE_CALLED_BINARY ||                   \
  (S)->common.orbit.separation >                              \
  MAXIMUM_SEPARATION_TO_BE_CALLED_BINARY ||                   \
  Less_or_equal((S)->common.orbit.angular_momentum,           \
  MINIMUM_ORBITAL_ANGMOM) ||                    \
  More_or_equal((S)->common.orbit.eccentricity,1.0))))
  #define _System_is_binary(S) (((_System_is_single(S))==TRUE) ? FALSE : TRUE)
*/


/*
 * The same logic, but using A OR B = NOT (A AND B)
 * which is usually faster. We define the binary first, then
 * single is just Not(single)
 */
#define _System_is_binary(S)                                    \
    (Boolean_(                                                  \
        (                                                       \
            (S)->preferences->zero_age.multiplicity>1 &&        \
            (S)->model.sgl == FALSE &&                          \
            (S)->star[1].stellar_type != MASSLESS_REMNANT &&    \
            (S)->star[0].stellar_type != MASSLESS_REMNANT &&    \
            (S)->common.orbit.eccentricity > -TINY &&           \
            (S)->common.orbit.separation >                      \
            MINIMUM_SEPARATION_TO_BE_CALLED_BINARY &&           \
            (S)->common.orbit.separation <                      \
            MAXIMUM_SEPARATION_TO_BE_CALLED_BINARY &&           \
            ((S)->common.orbit.angular_momentum >               \
             MINIMUM_ORBITAL_ANGMOM) &&                         \
            (S)->common.orbit.eccentricity < 1.0                \
            )))
#define _System_is_single(S) (Not(_System_is_binary(S)))

#define System_is_single _System_is_single(stardata)
#define System_is_binary _System_is_binary(stardata)
#define System_was_single _System_is_single(stardata->previous_stardata)
#define System_was_binary _System_is_binary(stardata->previous_stardata)

/*
 * Define an observable binary as one in which
 * either star has a radial velocity above
 * the threshold, and that System_is_binary is TRUE.
 */
#define Observable_binary                                               \
    (                                                                   \
        System_is_binary &&                                             \
        (                                                               \
            (                                                           \
                radial_velocity_K(stardata,90,1)>                       \
                stardata->preferences->observable_radial_velocity_minimum \
                )                                                       \
            ||                                                          \
            (                                                           \
                radial_velocity_K(stardata,90,2)>                       \
                stardata->preferences->observable_radial_velocity_minimum \
                )                                                       \
            )                                                           \
        )


#define Angular_momentum_from_stardata                              \
    (Pow2(stardata->common.orbit.separation) *                      \
     sqrt(Max(1.0-Pow2(stardata->common.orbit.eccentricity),0.0)) * \
     stardata->common.orbit.angular_frequency *                     \
     stardata->star[0].mass * stardata->star[1].mass /              \
     (stardata->star[0].mass + stardata->star[1].mass))


/* Time of tpagb start */
#define TPAGB_start_time(star) ((star)->time_first_pulse)

/* Time of the next pulse on the TPAGB */
#define Next_pulse(star) (More_or_equal(tagb, (star)->time_next_pulse))

/* this evaluates to TRUE if we're on the first pulse */
#define First_pulse(star) ((star)->num_thermal_pulses<-0.5)


#ifdef CODESTATS
#define Codestat_string(N)                      \
    (                                           \
        (N)==CODESTAT_MEMCPY ? "memcpy" :       \
        (N)==CODESTAT_MALLOC ? "malloc" :       \
        (N)==CODESTAT_CALLOC ? "calloc" :       \
        (N)==CODESTAT_REALLOC ? "realloc" :     \
        (N)==CODESTAT_MEMSET ? "memset" :       \
        "unknown")
#endif


#define AGB_Luminosity_Algorithm_String(N) (                        \
            (N) == AGB_LUMINOSITY_ALGORITHM_DEFAULT ? "Default" :   \
            (N) == AGB_LUMINOSITY_ALGORITHM_HURLEY ? "Hurley" :     \
            (N) == AGB_LUMINOSITY_ALGORITHM_KARAKAS ? "Karakas" :   \
            "Unknown" )

#define AGB_Third_Dredge_Up_Algorithm_String(N) (                       \
        (N) == AGB_THIRD_DREDGE_UP_ALGORITHM_DEFAULT ? "Default" :      \
        (N) == AGB_THIRD_DREDGE_UP_ALGORITHM_HURLEY ? "Hurley" :        \
        (N) == AGB_THIRD_DREDGE_UP_ALGORITHM_KARAKAS ? "Karakas" :      \
        (N) == AGB_THIRD_DREDGE_UP_ALGORITHM_STANCLIFFE ? "Stancliffe" : \
        "Unknown" )

#define AGB_Core_Algorithm_String(N) (                  \
        (N) == AGB_CORE_ALGORITHM_DEFAULT ? "Default" : \
        (N) == AGB_CORE_ALGORITHM_HURLEY ? "Hurley" :   \
        (N) == AGB_CORE_ALGORITHM_KARAKAS ? "Karakas" : \
        "Unknown" )

#define AGB_Radius_Algorithm_String(N) (                    \
        (N) == AGB_RADIUS_ALGORITHM_DEFAULT ? "Default" :   \
        (N) == AGB_RADIUS_ALGORITHM_HURLEY ? "Hurley" :     \
        (N) == AGB_RADIUS_ALGORITHM_KARAKAS ? "Karakas" :   \
        "Unknown" )

#define Batchmode_is_on(N) ((N)>(BATCHMODE_OFF))
#define Batchmode_is_off(N) ((N)<=(BATCHMODE_OFF))

/*
 * fflush everything and exit
 */
#define Flexit                                  \
    {                                           \
        fflush(NULL);                           \
        _exit(0);                               \
    }





/*
 * map stellar_structure calls to interface_stellar_evolution
 */
#define stellar_structure(...)                  \
    interface_stellar_structure(__VA_ARGS__)


/*
 * Dummy variable to prevent
 * "warning: ISO C forbids an empty translation unit"
 */
#define No_empty_translation_unit_warning typedef int make_iso_compilers_happy



/*
 * Tidal synchronization timescale of star N
 */
#define Sync_timescale(N)                                               \
    (                                                                   \
        Is_zero(stardata->star[(N)].derivative[DERIVATIVE_STELLAR_ANGULAR_VELOCITY_TIDES]) ? \
        1e100 :                                                         \
        Min(1e100,                                                      \
            fabs(stardata->star[(N)].omega/                             \
                 fabs(stardata->star[(N)].derivative[DERIVATIVE_STELLAR_ANGULAR_VELOCITY_TIDES]) \
                )                                                       \
            )                                                           \
        )

/*
 * Macro to return the size of array A
 */
#define Array_size(A) (sizeof(A)/sizeof((A)[0]))

/*
 * Choose element N from array A, otherwise return U
 */
#define Array_choice(A,N,U) (((long long int)N)<(long long int)Array_size(A) ? (A)[N] : (U))

/*
 * Choose element N from array A otherwise 0 (NULL)
 */
#define Array_string(A,N) (Array_choice((A),(N),0))

/*
 * A forever loop. Could be while(1) but that sometimes
 * compiles to more code.
 */
#define Loop_forever for(;;)



#ifdef __HAVE_LIBPTHREAD__
/*
 * libpthread errors as strings
 */
#define Mutex_error_string(X)                   \
    (X) == EINVAL ? "EINVAL" :                  \
        (X) == EBUSY ? "EBUSY" :                \
        (X) == EAGAIN ? "EAGAIN" :              \
        (X) == EPERM ? "EPERM" :                \
        (X) == EDEADLK ? "EDEADLK"  :           \
        "unknown"
#endif // __HAVE_LIBPTHREAD__

/*
 * Duplicate a file pointer in "w" mode
 */
#define Duplicate_writable_file_pointer(FP) ((FP) == NULL ? NULL : fdopen (dup (fileno (FP)), "w"))

/*
 * Test if a macro is empty
 */
#define Empty_macro(...) (true __VA_OPT__(&& false))
#define Nonempty_macro(...) (!Empty_macro(__VA_ARGS__))

/*
 * Perhaps call a function hook
 */
#define Call_function_hook(F)                                           \
    if(stardata->preferences->function_hooks[BINARY_C_HOOK_##F] != NULL) \
    {                                                                   \
        Dprint("call custom hook %s at %p\n",                           \
               #F,                                                      \
               Cast_function_pointer(stardata->preferences->function_hooks[BINARY_C_HOOK_##F])); \
        stardata->preferences->function_hooks[BINARY_C_HOOK_##F](stardata); \
    }


/*
 * Set stardata's uuid
 */
#define Set_UUID(STARDATA)                                  \
    {                                                       \
        set_uuid(STARDATA);                                 \
        strlcpy((STARDATA)->previous_stardata->model.uuid,  \
                (STARDATA)->model.uuid,                     \
                UUID_STR_LEN);                              \
}

/*
 * Macro to return an arg list with prepending comma
 * only if there is an arg list, otherwise nothing.
 */
#define Comma_arglist(...) __VA_OPT__(,) __VA_ARGS__

/*
 * Pointer or: given two pointers, A and B, return
 * whichever is non-NULL, or NULL if both are NULL.
 *
 * Note: we only evaluate B if A is NULL.
 */
#define Por(A,B) __extension__                                  \
    ({                                                          \
        void * const a = (void*)(A);                            \
        void * b;                                               \
        (a!=NULL ? a : ((b=((void*)(B)))==NULL) ? NULL : b);    \
    })


/*
 * Repeat a macro
 * https://stackoverflow.com/questions/8551418/c-preprocessor-macro-for-returning-a-string-repeated-a-certain-number-of-times
 */
#define Repeat0(X)
#define Repeat1(X) X
#define Repeat2(X) Repeat1(X) X
#define Repeat3(X) Repeat2(X) X
#define Repeat4(X) Repeat3(X) X
#define Repeat5(X) Repeat4(X) X
#define Repeat6(X) Repeat5(X) X
#define Repeat7(X) Repeat6(X) X
#define Repeat8(X) Repeat7(X) X
#define Repeat9(X) Repeat8(X) X
#define Repeat10(X) Repeat9(X) X

#define Repeat(HUNDREDS,TENS,ONES,X) \
  Repeat##HUNDREDS(Repeat10(Repeat10(X))) \
  Repeat##TENS(Repeat10(X)) \
  Repeat##ONES(X)

#endif // BINARY_C_FUNCTION_MACROS_H
