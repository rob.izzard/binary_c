#include <errno.h>
#include "../binary_c.h"
No_empty_translation_unit_warning;

/*
 * strtoi, strtoshort, strtoschar etc.
 * from
 * https://stackoverflow.com/questions/6181432/why-is-there-no-strtoi-in-stdlib-h
 */
static long int strto_subrange(const char * const Restrict s,
                               char ** Restrict endptr,
                               const int base,
                               const long int min,
                               const long int max);
static long int strto_subrange(const char * const Restrict s,
                               char ** Restrict endptr,
                               const int base,
                               const long int min,
                               const long int max)
{
    const long int y = strtol(s,
                              endptr,
                              base);
    if(y > max)
    {
        errno = ERANGE;
        return max;
    }
    else if(y < min)
    {
        errno = ERANGE;
        return min;
    }
    return y;
}

int strtoi(const char * const Restrict s,
           char **endptr,
           const int base)
{
#if INT_MAX == LONG_MAX && INT_MIN == LONG_MIN
    return (int) strtol(s,
                        endptr,
                        base);
#else
    return (int) strto_subrange(s,
                                endptr,
                                base,
                                INT_MIN,
                                INT_MAX);
#endif
}

short int strtoshort(const char * const Restrict s,
                     char ** endptr,
                     const int base)
{
    return (short int) strto_subrange(s,
                                      endptr,
                                      base,
                                      SHRT_MIN,
                                      SHRT_MAX);
}

signed char strtoschar(const char * const Restrict s,
                       char **endptr,
                       const int base)
{
    return (signed char) strto_subrange(s, endptr, base, SCHAR_MIN, SCHAR_MAX);
}

int16_t strtoint16(const char * const Restrict s,
                   char **endptr,
                   const int base)
{
    return (int16_t) strto_subrange(s, endptr, base, INT16_MIN, INT16_MAX);
}
