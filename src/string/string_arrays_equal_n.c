#include "../binary_c.h"
No_empty_translation_unit_warning;

/*
 * Check if the first n strings
 * in string_arrays a and b are equal.
 * If so, return TRUE, if not, FALSE.
 */

Boolean string_arrays_equal_n(struct string_array_t * const a,
                              struct string_array_t * const b,
                              const size_t n)
{
    return char_pointer_arrays_equal(a->strings,
                                     b->strings,
                                     n);
}
