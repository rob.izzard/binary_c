#pragma once
#ifndef STRING_FUNCTION_MACROS_H
#define STRING_FUNCTION_MACROS_H

/*
 * Foreach over a list of strings
 */
#define Foreach_string(STRING,...)                  \
    Foreach_string_implementation(Foreach_string,   \
                                  __COUNTER__,      \
                                  (STRING),         \
                                  __VA_ARGS__)

/*
 * The outer for loop sets the char* [] array of strings,
 * the label string and a char* pointer that controls the loop
 *
 * The inner loop is over the strings
 */
#define Foreach_string_implementation(LABEL,LINE,STRING,...)            \
    for(char *                                                          \
            STRING,                                                     \
            * Concat3(__outer,LABEL,LINE) = NULL,                       \
            * Concat3(__array,LABEL,LINE)[] = { __VA_ARGS__ };          \
        Concat3(__outer,LABEL,LINE) == NULL;                            \
        Concat3(__outer,LABEL,LINE) = (STRING))                         \
                                                                        \
        for(size_t Concat3(__i,LABEL,LINE) = 0,                         \
                Concat3(__n,LABEL,LINE) =                               \
                Array_size(Concat3(__array,LABEL,LINE));                \
                                                                        \
            (Concat3(__i,LABEL,LINE) < Concat3(__n,LABEL,LINE) &&       \
             ((STRING) = (char*)Concat3(__array,LABEL,LINE)[Concat3(__i,LABEL,LINE)])); \
                                                                        \
            Concat3(__i,LABEL,LINE)++                                   \
            )

/*
 * ASCII convenience macros
 */
#define ASCII_upper_case(N) Boolean_((N)>64 && (N)<91)
#define ASCII_lower_case(N) Boolean_((N)>96 && (N)<123)
#define ASCII_letter(N) (ASCII_lower_case(N) || ASCII_upper_case(N))

/*
 * ASCII letters to integer index mapping, where a-z are 0-25, and
 * A-Z are 26-51.
 */
#define ASCII_letter_to_index(A) \
    ASCII_letter_to_index_implementation(A,ASCII_letter_to_index,__COUNTER__)
#define ASCII_letter_to_index_implementation(A,LABEL,LINE)              \
    __extension__                                                       \
    ({                                                                  \
        const unsigned int Concat3(u,LABEL,LINE) =                      \
            ASCII_lower_case(arg[0])==TRUE ? 0 : 1;                     \
        const unsigned int offset =                                     \
            (const unsigned int) Concat3(u,LABEL,LINE) == 0 ? 'a' : 'A'; \
        (A) - offset + NUMBER_OF_LETTERS_IN_THE_ALPHABET * Concat3(u,LABEL,LINE); \
    })


/*
 * Reduce a 'T' or 'F' string depending on
 * whether X is TRUE or FALSE
 */
#define True_or_false(X) ((X)==TRUE ? "T" : "F")

/*
 * Ditto for yes or no
 */
#define Yes_or_no(X) ((X)==TRUE ? "Y" : "N")

/*
 * Choose your function to check is a char is
 * an integer.
 */
#define is_integer(C) is_integer1(C)

#endif // STRING_FUNCTION_MACROS_H
