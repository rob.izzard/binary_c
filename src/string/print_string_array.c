#include "../binary_c.h"
No_empty_translation_unit_warning;

/*
 * Print a string array to stdout using ' ' as a delimiter
 */
int print_string_array(struct string_array_t * Restrict const string_array)
{
    return fprint_string_array(stdout,
                               string_array,
                               " ");
}
