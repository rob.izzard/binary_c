#include "../binary_c.h"
No_empty_translation_unit_warning;

/*
 * Check if the first n strings
 * in arrays a and b are equal.
 * If so, return TRUE, if not, FALSE.
 */

Boolean char_pointer_arrays_equal(char * Restrict const * Restrict const a,
                                  char * Restrict const * Restrict const b,
                                  const size_t n)
{
    for(size_t i=0; i<n; i++)
    {
        if(!Strings_equal(a[i],b[i]))
        {
            return FALSE;
        }
    }
    return TRUE;
}
