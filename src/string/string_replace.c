#include "../binary_c.h"
No_empty_translation_unit_warning;

Boolean string_replace(char ** string,
                       char * match,
                       char * replacement)
{
    /*
     * Given "string", find "match" and replace with "replacement".
     *
     * Returns TRUE on replacement, FALSE if nothing happened.
     *
     * Note: destroys what's in string (much like strtok)
     *       and makes a new string to replace it.
     *
     * Note that "**string" must be a pointer to allocated
     * char* : you cannot replace in a static string.
     */
    char * location = strstr(*string,match);

    if(location)
    {
        char * new;
        /*
         * Save location of the match
         */
        const char was = *location;

        /*
         * Terminate the original string at the match
         */
        *location = '\0';

        /*
         * Build new string
         */
        const int n = asprintf(&new,
                               "%s%s%s",
                               *string,
                               replacement,
                               location + strlen(match));

        /*
         * Check for failure
         */
        if(n==-1)
        {
            /*
             * Failure : restore the original
             * string and return
             */
            *location = was;
            return FALSE;
        }
        else
        {
            /*
             * Free the original string
             */
            Safe_free(*string);

            /*
             * Set pointer to to the new string
             * and return TRUE
             */
            *string = new;
            return TRUE;
        }
    }
    else
    {
        /*
         * No match found : return FALSE
         */
        return FALSE;
    }
}
