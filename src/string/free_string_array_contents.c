#include "../binary_c.h"
No_empty_translation_unit_warning;

/*
 * Free strings in the string_array.
 *
 * Input:
 * Pointer to the string array pointer.
 */

void free_string_array_contents(struct string_array_t * const string_array)
{
    if(string_array != NULL &&
       string_array->nalloc > 0 &&
       string_array->n > 0)
    {
        for(ssize_t i=0; i<string_array->n; i++)
        {
            Safe_free(string_array->strings[i]);
        }
        memset(string_array->lengths,
               0,
               sizeof(size_t) * string_array->n);
        string_array->n = 0;
    }
}
