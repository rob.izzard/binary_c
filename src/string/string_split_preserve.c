#include "../binary_c.h"
No_empty_translation_unit_warning;

void string_split_preserve(struct stardata_t * const stardata,
                           const char * const Restrict string,
                           struct string_array_t * string_array,
                           const char delimiter,
                           const size_t prealloc,
                           const size_t max,
                           const Boolean shrink)
{
    /*
     * Like string_split, but copies the string
     * passed in and works on the copy, thus does not
     * change the string.
     */
    char * string_copy = strdup(string);
    if(string_copy != NULL)
    {
        string_split(stardata,
                     string_copy,
                     string_array,
                     delimiter,
                     prealloc,
                     max,
                     shrink);
        Safe_free(string_copy);
    }
}
