#include "../binary_c.h"
No_empty_translation_unit_warning;

/*
 * A set of functions which do the same as isdigit but
 * possibly more efficiently.
 *
 * These take an unsigned char and test whether it is
 * an integer in the range 0-9.
 *
 * I assume you want these inline. Almost certainly
 * you do, and the compiler will (probably) do this for
 * you.
 *
 * Which is fastest depends very much on your compiler.
 *
 * gcc 11.4.0 likes is_integer, isdigit and is_integer_table.
 *
 * gcc 12.3.0 likes is_integer.
 *
 * clang-15 likes is_integer and is_integer_table.
 *
 * Thus we *usually* go for is_integer.
 */

inline Pure_function Boolean is_integer1(const unsigned char c)
{
    // this gets compiled to (uint8_t)(c - '0') <= 9 on all decent compilers
    return unlikely(c >= '0' && c <= '9');
}

Pure_function Boolean is_integer2(const unsigned char c)
{
    // as is_integer with < rather than <= (etc.)
    return c > ('0'-1) && c < ('9'+1);
}

Pure_function Boolean is_integer3(const unsigned char c)
{
    /*
     * Only one comparison, but the cast may make things slower?
     */
    return (unsigned char)(c - ('9'+1)) > 245;
}

Pure_function Boolean is_integer_or(const unsigned char c)
{
    /*
     * A horrid set of ors which must be slower (surely?!)
     */
    return c=='0' || c=='1' || c=='2' || c=='3' || c=='4'|| c=='5' || c=='6' || c=='7' || c=='8' || c=='9';
}

Pure_function Boolean is_integer_table(const unsigned char c)
{
    /*
     * Lookup table. Crude, but sometimes fast.
     */
    return (Boolean[]){FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,TRUE,TRUE,TRUE,TRUE,TRUE,TRUE,TRUE,TRUE,TRUE,TRUE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE}[(int)c];
}
