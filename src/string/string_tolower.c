#include "../binary_c.h"
No_empty_translation_unit_warning;

/*
 * Convert string s to lower case, and return it.
 *
 * What is returned will have to be freed.
 */

char * string_tolower(const char * const s)
{
    const size_t n = strlen(s);
    if(n > 0)
    {
        char * const c = Malloc(sizeof(char) * (n+1));
        if(c != NULL)
        {
            for(size_t i=0; i<=n; i++)
            {
                c[i] = tolower(s[i]);
            }
        }
        return c;
    }
    return NULL;
}
