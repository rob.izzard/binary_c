#include "../binary_c.h"
No_empty_translation_unit_warning;

/*
 * Print a string array to file pointer fp using a given delimiter
 */
int fprint_string_array(FILE * const fp,
                        struct string_array_t * Restrict const string_array,
                        const char * const delimiter)
{
    int r = 0;
    const ssize_t last = string_array->n - 1;
    for(ssize_t i=0; i<string_array->n; i++)
    {
        r += fprintf(fp,
                     "%s%s",
                     string_array->strings[i],
                     i==last ? "\n" : delimiter);
    }
    return r;
}
