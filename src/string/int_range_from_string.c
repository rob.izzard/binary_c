#include "../binary_c.h"
No_empty_translation_unit_warning;

/*
 * Given a string, split on "-" and return
 * the part before and next after, as integers.
 *
 * This means you can split a number x-y
 * into x=low and y=high, or a number x into just x
 * (both low and high will be x in this case).
 *
 * On failure, both pointers low and high will be
 * set to 0.
 */

void int_range_from_string(struct stardata_t * const stardata,
                           const char * const Restrict string,
                           int * const low,
                           int * const high)
{
    struct string_array_t * substring_array = new_string_array(0);
    string_split(stardata,
                 string,
                 substring_array,
                 '-',
                 2,
                 0,
                 FALSE);

    if(substring_array->n > 0)
    {
        *low = strtoi(substring_array->strings[0],NULL,10);
        *high = substring_array->n>=2 ? strtoi(substring_array->strings[1],NULL,10) : *low;
    }
    else
    {
        *low = 0;
        *high = 0;
    }

    free_string_array(&substring_array);
}
