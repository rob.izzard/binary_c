#include "../binary_c.h"
No_empty_translation_unit_warning;

/*
 * Like asnprintf for variadic arguments.
 */

int vasnprintf(
    char ** Restrict const string_pointer,
    const size_t size,
    const char * Restrict const format,
    va_list args)
{


    /*
     * First, make the string without any limits
     */
    int ret;
    char * p;
    ret = vasprintf(&p,format,args);
    if(ret > 0)
    {
        /*
         * Second, copy size-1 chars into *string_pointer
         * and null-terminate.
         */
        *string_pointer = Malloc(sizeof(char)*(size));
        if(*string_pointer)
        {
            memcpy(*string_pointer,
                   p,
                   size-1);
            *string_pointer[size-1] = '\0';
        }
    }
    Safe_free(p);
    return ret;
}
