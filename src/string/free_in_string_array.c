#include "../binary_c.h"
No_empty_translation_unit_warning;

/*
 * Free the content of an array of strings
 *
 * Input:
 * strings = pointer to the array of strings (char ***)
 * n = number of strings
 */

void free_in_string_array(char ** const array,
                          const size_t n)
{
    if(array != NULL)
    {
        for(size_t j=0; j<n; j++)
        {
            Safe_free(*(array+j));
        }
    }
}
