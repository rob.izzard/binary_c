#include "../binary_c.h"
No_empty_translation_unit_warning;

struct unsigned_int_array_t * string_array_to_unsigned_int_array(struct string_array_t * Restrict string_array)
{
    /*
     * Convert string_array to unsigned_int_array
     * using strtod.
     */
    struct unsigned_int_array_t * const unsigned_int_array =
        new_unsigned_int_array(string_array->n);

    for(ssize_t i=0; i<string_array->n; i++)
    {
        unsigned_int_array->unsigned_ints[i] =
            (unsigned int)strtoul(string_array->strings[i],
                                  NULL,
                                  10);
    }
    unsigned_int_array->n = string_array->n;
    return unsigned_int_array;
}
