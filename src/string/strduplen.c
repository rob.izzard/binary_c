#include "../binary_c.h"
No_empty_translation_unit_warning;


/*
 * Combined strdup and strlen, based on
 * glibc's function (released under the
 * Gnu lesser public licence, available
 * at https://www.gnu.org/licenses/)
 * but with len as an argument that is
 * set.
 *
 * Returns the new string copy, sets its
 * length in len.
 *
 * Note that we assume s and len are both
 * not NULL.
 */
Nonnull_all_arguments
char * strduplen(const char * const s,
                 size_t * const len)
{
    *len = strlen(s);
    void * const new = Malloc((*len)+1);
    return
        new == NULL
        ? NULL
        : ((char *) memcpy(new,
                           s,
                           (*len)+1));
}
