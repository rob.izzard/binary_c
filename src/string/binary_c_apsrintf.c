#include "../binary_c.h"
No_empty_translation_unit_warning;


int Gnu_format_args(2,3) binary_c_asprintf(
    char ** Restrict const string_pointer,
    const char * Restrict const format,
    ...)
{
    /*
     * binary_c's asprintf, to be used if libbsd or gnu's
     * asprintf is not available.
     *
     * Returns the number of bytes copied successfully,
     * just as sprintf (and friends) would.
     *
     * *string_pointer needs freeing by the calling function
     * to avoid a memory leak.
     */
    va_list args,args_copy;
    va_start(args,format);
    va_copy(args_copy,args);
    int len = vsnprintf(0,
                        0,
                        format,
                        args_copy);
    va_end(args_copy);
    if(len<0)
    {
        va_end(args);
        return len;
    }
    else
    {
        * string_pointer = Malloc(sizeof(char)*(len+2));
        if(*string_pointer==NULL)
        {
            va_end(args);
            return 0;
        }
        else
        {
            len = vsnprintf(*string_pointer,
                            len+1,
                            format,
                            args);
            return len;
        }
    }

}
