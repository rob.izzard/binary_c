#include "../binary_c.h"
No_empty_translation_unit_warning;

char * fast_double_parser(const char * p,
                          double * const outDouble)
{
    /*
     * Wrapper for fast_double_parser_with_len()
     */
    return fast_double_parser_with_len(p,
                                       strlen(p),
                                       outDouble);
}
