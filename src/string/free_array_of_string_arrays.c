#include "../binary_c.h"
No_empty_translation_unit_warning;

/*
 * Free an array of string arrays
 */

void free_array_of_string_arrays(struct string_array_t *** const array_of_string_arrays,
                                 const size_t n)
{
    if(n>0 &&
       array_of_string_arrays != NULL &&
       *array_of_string_arrays != NULL)
    {
        struct string_array_t ** strings = *array_of_string_arrays;
        for(size_t i=0; i<n; i++)
        {
            free_string_array(&strings[i]);
        }
        Safe_free(*array_of_string_arrays);
    }
}
