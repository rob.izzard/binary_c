#include "../binary_c.h"
No_empty_translation_unit_warning
;
/*
 * Check if the strings
 * in arrays a and b are equal.
 * If so, return TRUE, if not, FALSE.
 */

Boolean string_arrays_equal(struct string_array_t * const a,
                            struct string_array_t * const b)
{
    if(a->n != b->n)
    {
        return FALSE;
    }
    else
    {
        /*
         * First check lengths are all the same,
         * these are integer comparisons that should
         * be fast and hence done first.
         */
        for(ssize_t i=0; i<a->n; i++)
        {
            if(a->lengths[i] != b->lengths[i])
            {
                return FALSE;
            }
        }

        /*
         * Now check the contents of each string.
         */
        return
            char_pointer_arrays_equal(a->strings,
                                      b->strings,
                                      a->n);
    }
}
