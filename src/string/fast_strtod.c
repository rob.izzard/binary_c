#include "../binary_c.h"
No_empty_translation_unit_warning;

/*
 * Wrapper for fast_double_parser to mimic the strtod
 * function of the standard C library.
 */
double fast_strtod(const char * nptr,
                   char ** endptr)
{
    double x;
    char * const y = fast_double_parser(nptr,&x);
    if(endptr) *endptr = y;
    return x;
}
