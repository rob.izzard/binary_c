#include "../binary_c.h"
No_empty_translation_unit_warning;

/*
 * Free an array of strings and its contents
 *
 * Input:
 * Pointer to the string array pointer.
 */

void free_string_array(struct string_array_t ** const string_array)
{
    if(string_array != NULL &&
       *string_array != NULL)
    {
        free_string_array_contents(*string_array);
        Safe_free((*string_array)->strings);
        Safe_free((*string_array)->lengths);
        Safe_free_nocheck(*string_array);
    }
}
