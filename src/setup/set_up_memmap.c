#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef MEMMAP

/*
 *
 * This code appears to not be used anymore :
 * It will be removed at some point in the near future.
 * You may protest now.
 *
 */
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

void set_up_memmap(struct stardata_t * Restrict const stardata)
{

    /* Use mmap to grab a large chunk of shared memory */
    stardata->biglog=mmap(0, /* Start address anywhere */
                          BIGLOG_SIZE, /* defined in binary_c_macros.h and
                                        *  local_binary_macros.h */
                          PROT_READ|PROT_WRITE, /* read/write access */
                          MAP_SHARED|MAP_ANON, /*
                                                * Share the memory between forked
                                                * processes
                                                */
                          -1, /* File descriptor */
                          0 /* No offset */
        );

    /* check this worked as well */
    if((int)stardata->biglog==-1)
    {
        /* mmap failed */
        Exit_binary_c(MEMMAP_FAILED,"set_up_memmap failed on mmap");
    }

    /* Start at the top of the mapped memory region */
    stardata->biglog_pos=0;
    /* The first line stores the biglog_pos */
    sprintf(stardata->biglog,
            "0");
}

#endif /* MEMMAP */
