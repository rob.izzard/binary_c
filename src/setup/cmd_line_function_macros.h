#pragma once
#ifndef CMD_LINE_FUNCTION_MACROS_H
#define CMD_LINE_FUNCTION_MACROS_H

/*
 * Function macros used in the command line
 * argument parsing code of binary_c
 */

/* code to check if the next arg exists when it's required */
#define Next_arg_check                                                  \
    success[c] = TRUE;                                                  \
    if(c+1>argc || argv[c+1] == NULL)                                   \
    {                                                                   \
        Exit_binary_c(                                                  \
            BINARY_C_SETUP_UNKNOWN_ARGUMENT,                            \
            "Exit because cmd line arg #%d \"%s\" requires a value which is not the next argument (c+1=%d argc=%d prev arg =\"%s\" next arg = \"%s\").", \
            c,                                                          \
            arg,                                                        \
            c+1,                                                        \
            argc,                                                       \
            argv[c == 0?0:c-1],                                         \
            (c<argc ? argv[c+1] : "NULL")                               \
            );                                                          \
    }                                                                   \
    else                                                                \
    {                                                                   \
        success[c+1] = TRUE;                                            \
    }

#define Arg_set_double                                                  \
    {                                                                   \
        errno = 0;                                                      \
        Boolean __match = FALSE;                                        \
        const struct cmd_line_arg_t * const a = cmd_line_args + i;      \
        c++;                                                            \
        if(isdigit((unsigned char)argv[c][0]) ||                        \
           argv[c][0] == '.' ||                                         \
           argv[c][0] == '-' ||                                         \
           argv[c][0] == '+')                                           \
        {                                                               \
            /* numeric argument: use string_to_code_units */            \
            double value;                                               \
            struct unit_t unit;                                         \
            int errnum;                                                 \
            string_to_code_units(stardata,                              \
                                 argv[c],                               \
                                 &value,                                \
                                 &unit,                                 \
                                 &errnum);                              \
            if(errnum)                                                  \
            {                                                           \
                char errstring[100];                                    \
                if(strerror_r(errnum,errstring,100)==0)                 \
                {                                                       \
                    Exit_binary_c(                                      \
                        BINARY_C_ARGUMENT_OVERFLOW,                     \
                        "Argument %d (string %s after %s) has caused a floating point %sflow (errno = %d which is %s)\n", \
                        c,                                              \
                        argv[c],                                        \
                        argv[c-1],                                      \
                        ((value<0.0)?"under":"over"),                   \
                        errnum,                                         \
                        errstring);                                     \
                }                                                       \
                else                                                    \
                {                                                       \
                    Exit_binary_c(                                      \
                        BINARY_C_ARGUMENT_OVERFLOW,                     \
                        "Argument %d (string %s after %s) has caused a floating point %sflow (errno = %d, error string unavailable - strerror failed)\n", \
                        c,                                              \
                        argv[c],                                        \
                        argv[c-1],                                      \
                        ((value<0.0)?"under":"over"),                   \
                        errnum                                          \
                        );                                              \
                }                                                       \
            }                                                           \
                                                                        \
                                                                        \
            *((double*) a->pointer) = a->modulate * value;              \
            __match=TRUE;                                               \
        }                                                               \
        else                                                            \
        {                                                               \
            /* Macro argument, has no units (just an integer) */        \
            for(unsigned int __i=0;__i<a->npairs;__i++)                 \
            {                                                           \
                const struct arg_pair_t * const p = a->pairs + __i;     \
                if(Strings_equal(p->string,argv[c]))                    \
                {                                                       \
                    Cprint("Matched double %s -> set %s to %g\n",       \
                           p->string,                                   \
                           a->name,                                     \
                           (double)p->value);                           \
                    *((double*)a->pointer) = (double)p->value;          \
                    __match = TRUE;                                     \
                }                                                       \
            }                                                           \
        }                                                               \
        if(__match == FALSE)                                            \
        {                                                               \
            Exit_binary_c(BINARY_C_WRONG_ARGUMENT,                      \
                          "Argument \"%s\" following %s is neither a double precision not matched an appropriate macro. Please check it!\n", \
                          argv[c],                                      \
                          argv[c-1]);                                   \
        }                                                               \
        Cprint("Set %s to %g\n",                                        \
               a->name,                                                 \
               *((double*) a->pointer));                                \
    }

#define Arg_test_int_under_or_overflow(__MIN,__MAX)                     \
    if((__MIN!=0 && test==__MIN) || test==__MAX)                        \
    {                                                                   \
        Exit_binary_c(                                                  \
            BINARY_C_ARGUMENT_OVERFLOW,                                 \
            "Argument %d (string %s after %s) has caused a (long) integer %sflow\n", \
            c,argv[c],argv[c-1],((test==__MAX)?"under":"over")          \
            );                                                          \
    }


/*
 * Set an integer of type __TYPE
 * which could be int, long int, unsigned int etc.
 */
#define Arg_set_int_of_type(__TYPE,__MIN,__MAX) _Arg_set_int_of_type(__TYPE,,"%lld",__MIN,__MAX)
#define Arg_set_unsigned_int_of_type(__TYPE,__MIN,__MAX) _Arg_set_int_of_type(__TYPE,unsigned,"%llu",__MIN,__MAX)

#define _Arg_set_int_of_type(__TYPE,__SIGN,__FORMAT,__MIN,__MAX)        \
    {                                                                   \
        errno = 0;                                                      \
        c++;                                                            \
        Boolean __match = FALSE;                                        \
        const struct cmd_line_arg_t * const a = cmd_line_args + i;      \
        if(isdigit((unsigned char)argv[c][0])                           \
           ||                                                           \
           (strlen(argv[c]) > 0 &&                                      \
            argv[c][0] == '-' &&                                        \
            isdigit((unsigned char)argv[c][1])))                        \
        {                                                               \
            const __SIGN long long int test = strtoll(argv[c],NULL,10); \
            Arg_test_int_under_or_overflow(__MIN,__MAX);                \
            if(test != (__SIGN __TYPE)test)                             \
            {                                                           \
                Exit_binary_c(                                          \
                    BINARY_C_ARGUMENT_OVERFLOW,                         \
                    "The value provided for argument \"%s\" overflowed or underflowed.\n", \
                    a->name);                                           \
            }                                                           \
            *((__SIGN __TYPE*)a->pointer) = (__SIGN __TYPE)test;        \
            __match = TRUE;                                             \
        }                                                               \
        else if(Arg_is_true(argv[c]))                                   \
        {                                                               \
            *((__SIGN __TYPE*)a->pointer) = TRUE;                       \
            __match = TRUE;                                             \
        }                                                               \
        else if(Arg_is_false(argv[c]))                                  \
        {                                                               \
            *((__SIGN __TYPE*)a->pointer) = FALSE;                      \
            __match = TRUE;                                             \
        }                                                               \
        else                                                            \
        {                                                               \
            /* Macro argument */                                        \
            for(unsigned int __i=0;__i<a->npairs;__i++)                 \
            {                                                           \
                const struct arg_pair_t * const p = a->pairs + __i;     \
                if(Strings_equal(p->string,argv[c]))                    \
                {                                                       \
                    Cprint("Matched %s -> set %s to "__FORMAT"\n",      \
                           p->string,                                   \
                           a->name,                                     \
                           (__SIGN long long int)((__SIGN __TYPE) p->value)); \
                    *((__SIGN __TYPE*)a->pointer) = (__SIGN __TYPE)p->value; \
                    __match = TRUE;                                     \
                }                                                       \
            }                                                           \
        }                                                               \
        if(__match == FALSE)                                            \
        {                                                               \
            Exit_binary_c(BINARY_C_WRONG_ARGUMENT,                      \
                          "Argument \"%s\" following %s is neither integer not matched an appropriate macro. Please check it!\n", \
                          argv[c],                                      \
                          argv[c-1]);                                   \
        }                                                               \
        Cprint("Set %s to %lld\n",                                \
               a->name,                                                 \
               (long long int)*((__SIGN __TYPE*) a->pointer));          \
    }

/*
 * Hence macros for setting int, unsigned int, long int
 */
#define Arg_set_int Arg_set_int_of_type(int,INT_MIN,INT_MAX)
#define Arg_set_unsigned_int Arg_set_unsigned_int_of_type(int,0,USHRT_MAX)
#define Arg_set_long_int Arg_set_int_of_type(long int,LONG_MIN,LONG_MAX)
#define Arg_set_unsigned_long_int Arg_set_unsigned_int_of_type(long int,0,ULONG_MAX)
#define Arg_set_long_long_int Arg_set_int_of_type(long long int,LLONG_MIN,LLONG_MAX)
#define Arg_set_unsigned_long_long_int Arg_set_unsigned_int_of_type(long long int,0,ULLONG_MAX)

/*
 * Match a format statement containing a single %d to represent
 * an integer. Currently only works for (signed) int.
 */
#define Arg_scanf_set_int                                               \
    {                                                                   \
        errno = 0;                                                      \
        Boolean __match = FALSE;                                        \
        ++c;                                                            \
        const struct cmd_line_arg_t * const a = cmd_line_args + i;      \
        int * const __p =                                               \
            (int * const)a->pointer + offset;                           \
        if(isdigit((unsigned char)argv[c][0])                           \
           ||                                                           \
           (strlen(argv[c]) > 0 &&                                      \
            argv[c][0] == '-' &&                                        \
            isdigit((unsigned char)argv[c][1])))                        \
        {                                                               \
            const long int test = strtol(argv[c],NULL,10);              \
            Arg_test_int_under_or_overflow(INT_MIN,INT_MAX);            \
           if(a->max_pointer != NULL &&                                \
               __p >= (int * const)a->max_pointer)                      \
            {                                                           \
                Exit_binary_c(                                          \
                    BINARY_C_ARGUMENT_OVERFLOW,                         \
                    "Value provided in scanf integer argument \"%s\", given as \"%s\", is outside the possible range.", \
                    a->name,                                            \
                    argv[c-1]);                                         \
            }                                                           \
            *__p = (int)test;                                           \
            if(*__p != test)                                            \
            {                                                           \
                Exit_binary_c(                                          \
                    BINARY_C_ARGUMENT_OVERFLOW,                         \
                    "The value provided for argument \"%s\" overflowed or underflowed.\n", \
                    a->name);                                           \
            }                                                           \
            __match = TRUE;                                             \
        }                                                               \
        else                                                            \
        {                                                               \
            /* might be a macro */                                      \
            for(unsigned int __i=0;__i<a->npairs;__i++)                 \
            {                                                           \
                const struct arg_pair_t * const p = a->pairs + __i;     \
                if(Strings_equal(p->string,argv[c]))                    \
                {                                                       \
                    Cprint("Matched %s -> set %s to %lld\n",            \
                           p->string,                                   \
                           a->name,                                     \
                           (long long int)((int) p->value));            \
                    if(a->max_pointer != NULL &&                        \
                       __p >= (int * const)a->max_pointer)              \
                    {                                                   \
                        Exit_binary_c(                                  \
                            BINARY_C_ARGUMENT_OVERFLOW,                 \
                            "Value provided in scanf integer argument \"%s\", given as \"%s\", is outside the possible range.", \
                            a->name,                                    \
                            argv[c-1]);                                 \
                    }                                                   \
                    /* assume macro replacement does not overflow */    \
                    *__p = (int)p->value;                               \
                    __match = TRUE;                                     \
                }                                                       \
            }                                                           \
        }                                                               \
        if(__match == FALSE)                                            \
        {                                                               \
            Exit_binary_c(BINARY_C_WRONG_ARGUMENT,                      \
                          "Argument \"%s\" following %s is neither integer not matched an appropriate macro. Please check it!\n", \
                          argv[c],                                      \
                          argv[c-1]);                                   \
        }                                                               \
        Cprint("Set %s + %zu to %d\n",                                  \
               cmd_line_args[i].name,offset,                            \
               *((int*) cmd_line_args[i].pointer + offset));            \
    }

/*
 * Match a format statement containing a single %d to represent
 * an integer, and put it in a Boolean. Note that if the argument
 * fails to match a boolean type, we set it to FALSE.
 */
#define Arg_scanf_set_Boolean                                           \
    {                                                                   \
        errno = 0;                                                      \
        c++;                                                            \
        Boolean * const __p =                                           \
            (Boolean * const )cmd_line_args[i].pointer + offset;        \
        if(cmd_line_args[i].max_pointer != NULL &&                      \
           __p >= (Boolean * const)cmd_line_args[i].max_pointer)        \
        {                                                               \
            Exit_binary_c(                                              \
                BINARY_C_ARGUMENT_OVERFLOW,                             \
                "Value provided in scanf Boolean argument \"%s\", given as \"%s\", is outside the possible range.", \
                cmd_line_args[i].name,                                  \
                argv[c-1]);                                             \
        }                                                               \
        *__p = Arg_is_true(argv[c]) ? TRUE : FALSE;                     \
        Cprint("Set %s to %s\n",                                        \
               cmd_line_args[i].name,                                   \
               Truefalse(*__p));                                        \
    }

/*
 * Match a format statement containing a single %d to represent
 * an integer. Next arg is a double.
 */
#define Arg_scanf_set_double                                            \
    {                                                                   \
        errno = 0;                                                      \
        double test;                                                    \
        c++;                                                            \
        const struct cmd_line_arg_t * const a = cmd_line_args + i;      \
        double * const __p =  ((double * const)a->pointer + offset);    \
        Boolean __match = FALSE;                                        \
        if(isdigit((unsigned char)argv[c][0]) ||                        \
           argv[c][0] == '.' ||                                         \
           argv[c][0] == '-' ||                                         \
           argv[c][0] == '+')                                           \
        {                                                               \
            char * const endptr = fast_double_parser(argv[c],           \
                                                     &test);            \
            const int errnum = errno;                                   \
            if(endptr == NULL || errno)                                 \
            {                                                           \
                char errstring[100];                                    \
                if(strerror_r(errnum,errstring,100)==0)                 \
                {                                                       \
                    Exit_binary_c(                                      \
                        BINARY_C_ARGUMENT_OVERFLOW,                     \
                        "Argument %d (string %s after %s) has caused a floating point %sflow (errno = %d which is %s)\n", \
                        c,                                              \
                        argv[c],                                        \
                        argv[c-1],                                      \
                        ((test<0.0)?"under":"over"),                    \
                        errnum,                                         \
                        errstring);                                     \
                }                                                       \
                else                                                    \
                {                                                       \
                    Exit_binary_c(                                      \
                        BINARY_C_ARGUMENT_OVERFLOW,                     \
                        "Argument %d (string %s after %s) has caused a floating point %sflow (errno = %d, error string unavailable because strerror failed)\n", \
                        c,                                              \
                        argv[c],                                        \
                        argv[c-1],                                      \
                        ((test<0.0)?"under":"over"),                    \
                        errnum);                                        \
                }                                                       \
            }                                                           \
            if(a->max_pointer != NULL &&                                \
               __p >= (double * const)cmd_line_args[i].max_pointer)     \
            {                                                           \
                Exit_binary_c(                                          \
                    BINARY_C_ARGUMENT_OVERFLOW,                         \
                    "Value provided in scanf float argument \"%s\", given as \"%s\", is outside the possible range.", \
                    a->name,                                            \
                    argv[c-1]);                                         \
            }                                                           \
            *__p = (double)test;                                        \
            Cprint("Set %s to %g\n",                                    \
                   a->name,                                             \
                   *__p);                                               \
            __match = TRUE;                                             \
        }                                                               \
        else                                                            \
        {                                                               \
            /* macro argument */                                        \
            for(unsigned int __i=0;__i<a->npairs;__i++)                 \
            {                                                           \
                const struct arg_pair_t * const p = a->pairs + __i;     \
                if(Strings_equal(p->string,argv[c]))                    \
                {                                                       \
                    Cprint("Matched double %s -> set %s to %g\n",       \
                           p->string,                                   \
                           a->name,                                     \
                           (double)p->value);                           \
                    *__p = (double)p->value;                            \
                    __match = TRUE;                                     \
                }                                                       \
            }                                                           \
        }                                                               \
        if(__match == FALSE)                                            \
        {                                                               \
            Exit_binary_c(BINARY_C_WRONG_ARGUMENT,                      \
                          "Argument \"%s\" following %s is neither a double precision not matched an appropriate macro. Please check it!\n", \
                          argv[c],                                      \
                          argv[c-1]);                                   \
        }                                                               \
    }



/*
 * Boolean tests
 */
#define Arg_is_true(X) String_is_true(X)
#define Arg_is_false(X) String_is_false(X)
#define Arg_is_boolean(X) String_is_boolean(X)

/*
 * generic boolean setter. X is the default, either TRUE or FALSE.
 * Only sets the boolean if the next argument matches Arg_is_boolean above.
 */
#define Arg_set_boolean(X)                                          \
    {                                                               \
        Boolean test = (X);                                         \
        Cprint("c+1 = %d < argc = %d : \"%s\" is boolean ? %s\n",   \
               c+1,                                                 \
               argc,                                                \
               argv[c+1],                                           \
               Yesno(Arg_is_boolean(argv[c+1])));                   \
        success[c] = TRUE;                                          \
        if(c+1<argc && Arg_is_boolean(argv[c+1]))                   \
        {                                                           \
            c++;                                                    \
            success[c] = TRUE;                                      \
            test =                                                  \
                Arg_is_true(argv[c]) ? TRUE :                       \
                Arg_is_false(argv[c]) ? FALSE :                     \
                (Boolean)Long_int_to_boolean(strtol(argv[++c],      \
                                                    NULL,           \
                                                    10));           \
            Cprint("test %d (c=%d vs argc=%d arg is %s)\n",         \
                   test,c,argc,argv[c]);                            \
        }                                                           \
                                                                    \
        *((Boolean*)cmd_line_args[i].pointer) = test;               \
        Cprint("Set %s to %s\n",                                    \
               cmd_line_args[i].name,                               \
               Truefalse(*((Boolean*)cmd_line_args[i].pointer))     \
            );                                                      \
    }

/* hence some that default to TRUE and FALSE */
#define Arg_set_boolean_true                    \
    Arg_set_boolean(TRUE)

#define Arg_set_boolean_false                   \
    Arg_set_boolean(FALSE)

/* assumes STRING_LENGTH */
#define Arg_set_string                                      \
    {                                                       \
        strlcpy(cmd_line_args[i].pointer,                   \
                argv[++c],                                  \
                (size_t)STRING_LENGTH);                     \
        Cprint("Set string \"%s\" from \"%s\" to \"%s\"\n", \
               (char*)cmd_line_args[i].name,                \
               (char*)argv[c],                              \
               (char*)cmd_line_args[i].pointer);            \
    }

/* no string length assumed */
#define Arg_set_unbounded_string                                        \
    {                                                                   \
        cmd_line_args[i].pointer = strdup(argv[++c]);                   \
        Cprint("Set unbounded string \"%s\" from \"%s\" to \"%s\"\n",   \
               (char*)cmd_line_args[i].name,                            \
               (char*)argv[c],                                          \
               (char*)cmd_line_args[i].pointer);                        \
    }


#define Arg_call_subroutine                                     \
    {                                                           \
        success[c] = TRUE;                                      \
        const int cwas = c;                                     \
        cmd_line_args[i].function_pointer(ARG_SUBROUTINE_ARGS); \
        for(int cc = cwas; cc<=c; cc++)                         \
        {                                                       \
            success[cc] = TRUE;                                 \
        }                                                       \
        break;                                                  \
    }

#define Argtype_is_boolean(TYPE)                \
    (                                           \
        (TYPE) == ARG_BOOLEAN ||                \
        (TYPE) == ARG_NOT_BOOLEAN ||            \
        (TYPE) == BATCH_ARG_BOOLEAN ||          \
        (TYPE) == BATCH_ARG_NOT_BOOLEAN ||      \
        (TYPE) == ARG_BOOLEAN_SCANF  ||         \
        (TYPE) == ARG_BOOLEAN_OFFSET_SCANF      \
        )

#define Argtype_is_double(TYPE)                 \
    (                                           \
        (TYPE) == ARG_DOUBLE ||                 \
        (TYPE) == ARG_DOUBLE_SCANF ||           \
        (TYPE) == ARG_DOUBLE_OFFSET_SCANF       \
        )

#define Argtype_is_integer(TYPE)                \
    (                                           \
        (TYPE) == ARG_INTEGER ||                \
        (TYPE) == ARG_INTEGER_SCANF ||          \
        (TYPE) == ARG_INTEGER_OFFSET_SCANF      \
        )

#define Argtype_is_long_integer(TYPE)           \
    (                                           \
        (TYPE) == ARG_LONG_INTEGER              \
        )


#define Argtype_is_batch(TYPE)                  \
    (                                           \
        (TYPE)==BATCH_ARG_SUBROUTINE ||         \
        (TYPE)==BATCH_ARG_BOOLEAN               \
        )

#define Argtype_is_variable(TYPE)               \
    (                                           \
        (TYPE) == ARG_DOUBLE ||                 \
        (TYPE) == ARG_INTEGER ||                \
        (TYPE) == ARG_UNSIGNED_INTEGER ||       \
        (TYPE) == ARG_LONG_INTEGER ||           \
        (TYPE) == ARG_STRING ||                 \
        (TYPE) == ARG_BOOLEAN ||                \
        (TYPE) == ARG_NOT_BOOLEAN               \
        )

#define Argtype_is_subroutine(TYPE)             \
    (                                           \
        (TYPE) == BATCH_ARG_SUBROUTINE ||       \
        (TYPE) == ARG_SUBROUTINE                \
        )

#define Argtype_is_function_pointer(TYPE)       \
    (                                           \
        (TYPE) == ARG_SUBROUTINE                \
        )

#define Argtype_is_scanf(TYPE)                  \
    (                                           \
        Argtype_is_nonoffset_scanf(TYPE) ||     \
        Argtype_is_offset_scanf(TYPE)           \
        )

#define Argtype_scanf_varsize(TYPE)                             \
    (                                                           \
        (TYPE)==ARG_DOUBLE_SCANF ? sizeof(double) :             \
        (TYPE)==ARG_DOUBLE_OFFSET_SCANF ? sizeof(double) :      \
        (TYPE)==ARG_INTEGER_SCANF ? sizeof(int) :               \
        (TYPE)==ARG_INTEGER_OFFSET_SCANF ? sizeof(int) :        \
        (TYPE)==ARG_BOOLEAN_SCANF ? sizeof(Boolean) :           \
        (TYPE)==ARG_BOOLEAN_OFFSET_SCANF ? sizeof(Boolean) :    \
        0                                                       \
        )

#define Argtype_is_nonoffset_scanf(TYPE)        \
    (                                           \
        (TYPE) == ARG_INTEGER_SCANF ||          \
        (TYPE) == ARG_BOOLEAN_SCANF ||          \
        (TYPE) == ARG_DOUBLE_SCANF              \
        )

#define Argtype_is_offset_scanf(TYPE)           \
    (                                           \
        (TYPE) == ARG_INTEGER_OFFSET_SCANF ||   \
        (TYPE) == ARG_BOOLEAN_OFFSET_SCANF ||   \
        (TYPE) == ARG_DOUBLE_OFFSET_SCANF       \
        )

#define Argtype_is_string(TYPE)                                         \
    (                                                                   \
        (TYPE)==ARG_DOUBLE ? "Float" :                                  \
        (TYPE)==ARG_STRING ? "String" :                                 \
        (TYPE)==ARG_NONE ? "" :                                         \
        (TYPE)==ARG_INTEGER ? "Integer" :                               \
        (TYPE)==ARG_SUBROUTINE ? "*" :                                  \
        (TYPE)==ARG_BOOLEAN ? "True|False" :                            \
        (TYPE)==ARG_LONG_INTEGER ? "Integer" :                          \
        (TYPE)==ARG_NOT_BOOLEAN ? "False|True" :                        \
        (TYPE)==BATCH_ARG_SUBROUTINE ? "*" :                            \
        (TYPE)==BATCH_ARG_BOOLEAN ? "True|False" :                      \
        (TYPE)==ARG_INTEGER_SCANF ? "Int(scanf)" :                      \
        (TYPE)==ARG_BOOLEAN_SCANF ? "Boolean(scanf)" :                  \
        (TYPE)==ARG_DOUBLE_SCANF ? "Float(scanf)" :                     \
        (TYPE)==ARG_INTEGER_OFFSET_SCANF ? "Int(scanf) offset -1" :     \
        (TYPE)==ARG_BOOLEAN_OFFSET_SCANF ? "Boolean(scanf) offset -1" : \
        (TYPE)==ARG_DOUBLE_OFFSET_SCANF ? "Float(scanf) offset -1" :    \
        (TYPE)==ARG_UNSIGNED_INTEGER ? "Unsigned integer" :             \
        "*"                                                             \
        )

#define Argtype_is_format(TYPE)                 \
    (                                           \
        (TYPE)==ARG_DOUBLE ? "%g" :             \
        (TYPE)==ARG_STRING ? "%s" :             \
        (TYPE)==ARG_INTEGER ? "%d" :            \
        (TYPE)==ARG_BOOLEAN ? "%s" :            \
        (TYPE)==ARG_LONG_INTEGER ? "%ld" :      \
        (TYPE)==ARG_NOT_BOOLEAN ? "%s" :        \
        (TYPE)==BATCH_ARG_BOOLEAN ? "%s" :      \
        (TYPE)==ARG_UNSIGNED_INTEGER ? "%u" :   \
        NULL                                    \
        )


#endif // CMD_LINE_FUNCTION_MACROS_H
