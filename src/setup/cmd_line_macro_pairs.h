#pragma once
#ifndef CMD_LINE_MACRO_PAIRS_H
#define CMD_LINE_MACRO_PAIRS_H

/*
 * VALUES of arguments
 *
 * Add a macro-value pair where the macro is MACRO
 * and the variable to be set is named by string VARNAME
 *
 * The straight loop through cmd_line_args2 is inefficient,
 * but this is done only once so it's hard to care enough to optimize.
 */
#define __eval(X) (X)
#define Add_value_macro_pair(VARNAME,MACRO)                             \
    {                                                                   \
        struct cmd_line_arg_t * const a =                               \
            match_a(VARNAME,                                            \
                    &__prevarg,                                         \
                    &__prevstring,                                      \
                    cmd_line_args2,                                     \
                    arg_count);                                         \
                                                                        \
        /* found a match : process */                                   \
        if(a != NULL)                                                   \
        {                                                               \
            a->npairs++;                                                \
            a->pairs =                                                  \
                Realloc(a->pairs,                                       \
                        sizeof(struct arg_pair_t)*                      \
                        a->npairs);                                     \
            struct arg_pair_t * const p = a->pairs + a->npairs - 1;     \
            p->string = #MACRO ;                                        \
            p->macro = #MACRO;                                          \
            p->value = __eval(MACRO);                                   \
            Cprint("p string %s, value %g\n",                           \
                   p->string,                                           \
                   p->value);                                           \
        }                                                               \
    }

#define Add_value_macro_pair_to_value(VARNAME,MACRO,TO_VALUE)           \
    {                                                                   \
        struct cmd_line_arg_t * const a =                               \
            match_a(VARNAME,                                            \
                    &__prevarg,                                         \
                    &__prevstring,                                      \
                    cmd_line_args2,                                     \
                    arg_count);                                         \
                                                                        \
        /* found a match : process */                                   \
        if(a != NULL)                                                   \
        {                                                               \
            a->npairs++;                                                \
            a->pairs =                                                  \
                Realloc(a->pairs,                                       \
                        sizeof(struct arg_pair_t)*                      \
                        a->npairs);                                     \
            struct arg_pair_t * const p = a->pairs + a->npairs - 1;     \
            p->string = #MACRO ;                                        \
            p->macro = #MACRO;                                          \
            p->value = (TO_VALUE);                                      \
            Cprint("p string %s, value %g\n",                           \
                   p->string,                                           \
                   p->value);                                           \
        }                                                               \
    }


#define Add_value_macro_pair_string(VARNAME,STRING,CODE)                \
    {                                                                   \
        struct cmd_line_arg_t * const a =                               \
            match_a(VARNAME,                                            \
                    &__prevarg,                                         \
                    &__prevstring,                                      \
                    cmd_line_args2,                                     \
                    arg_count);                                         \
                                                                        \
        /* found a match : process */                                   \
        if(a != NULL)                                                   \
        {                                                               \
            a->npairs++;                                                \
            a->pairs =                                                  \
                Realloc(a->pairs,                                       \
                        sizeof(struct arg_pair_t)*                      \
                        a->npairs);                                     \
            struct arg_pair_t * const p = a->pairs + a->npairs - 1;     \
            p->string = STRING ;                                        \
            p->value = CODE;                                            \
            p->macro = #CODE;                                           \
            Cprint("p string %s, value %g\n",                           \
                   p->string,                                           \
                   p->value);                                           \
        }                                                               \
    }

/*
 * ARGUMENT macro pairs
 *
 *
 */
#define Add_argument_macro_pair(VARNAME,MACRO,PREFIX)                   \
    for(__i=0; __i<arg_count; __i++)                                    \
    {                                                                   \
        struct cmd_line_arg_t * a = &cmd_line_args2[__i];               \
        if(Strings_equal((VARNAME),a->name))                            \
        {                                                               \
            a->nargpairs++;                                             \
            a->argpairs = Realloc(a->argpairs,                          \
                                  sizeof(struct arg_pair_t)*            \
                                  a->nargpairs);                        \
            Cprint("matched arg %s %s : nargpairs upped to %u : argpairs = %p\n", \
                   (VARNAME),                                           \
                   a->name,                                             \
                   a->nargpairs,                                        \
                   (void*)a->argpairs);                                 \
            struct arg_pair_t * p = a->argpairs+a->nargpairs-1;         \
            size_t len = strlen(a->name);                               \
            Cprint("Target string %s :: len %zu :: a = %p\n",           \
                   a->name,                                             \
                   len,                                                 \
                   (void*)a);                                           \
            char * format = Malloc(sizeof(char)*(len+1));               \
            strlcpy(format,a->name,len+1);                              \
            Cprint("format %s\n",format);                               \
            size_t __j;                                                 \
            for(__j=0; __j<len-1; __j++)                                \
            {                                                           \
                if(format[__j] == '%' &&                                \
                   format[__j+1] == 'd')                                \
                {                                                       \
                    Cprint("found %%d at %zu\n",__j);                   \
                    format[__j+1] = 's';                                \
                    __j = len + 1;                                      \
                }                                                       \
            }                                                           \
            Cprint("hence new format \"%s\", len %zu\n",format,len);    \
            Cprint("Set string %s\n",(#MACRO));                         \
            len += sizeof(#MACRO);                                      \
            const int x = asprintf(&p->string,                          \
                                   len,                                 \
                                   format,                              \
                                   (#MACRO)) ;                          \
            if(x<0)                                                     \
            {                                                           \
                Exit_binary_c(BINARY_C_XPRINTF_FAILED,                  \
                              "snprintf failed in cmd_line_macros_pairs.h"); \
            }                                                           \
            p->macro = #MACRO;                                          \
            p->value = (PREFIX##MACRO);                                 \
            Cprint("p string \"%s\", value %g, d = %d\n",               \
                   p->string,                                           \
                   p->value,                                            \
                   x);                                                  \
            Safe_free(format);                                          \
        }                                                               \
    }

/*
 * As above, but we don't concatenate the "macro" with the
 * prefix, we're simply given the enum version.
 */
#define Add_argument_Xmacro_pair(VARNAME,MACRO,ENUM)                    \
    for(__i=0; __i<arg_count; __i++)                                    \
    {                                                                   \
        struct cmd_line_arg_t * a = &cmd_line_args2[__i];               \
        if(Strings_equal((VARNAME),a->name))                            \
        {                                                               \
            a->nargpairs++;                                             \
            a->argpairs = Realloc(a->argpairs,                          \
                                  sizeof(struct arg_pair_t)*            \
                                  a->nargpairs);                        \
            Cprint("matched arg %s %s : nargpairs upped to %u : argpairs = %p\n", \
                   (VARNAME),                                           \
                   a->name,                                             \
                   a->nargpairs,                                        \
                   (void*)a->argpairs);                                 \
            struct arg_pair_t * p = a->argpairs+a->nargpairs-1;         \
            size_t len = strlen(a->name);                               \
            Cprint("Target string %s :: len %zu :: a = %p\n",           \
                   a->name,                                             \
                   len,                                                 \
                   (void*)a);                                           \
            char * format = Malloc(sizeof(char)*(len+1));               \
            strlcpy(format,a->name,len+1);                              \
            Cprint("format %s\n",format);                               \
            size_t __j;                                                 \
            for(__j=0; __j<len-1; __j++)                                \
            {                                                           \
                if(format[__j] == '%'&&                                 \
                   format[__j+1] == 'd')                                \
                {                                                       \
                    Cprint("found %%d at %zu\n",__j);                   \
                    format[__j+1] = 's';                                \
                    __j = len + 1;                                      \
                }                                                       \
            }                                                           \
            Cprint("hence new format \"%s\", len %zu\n",format,len);    \
            Cprint("Set string %s\n",(#MACRO));                         \
            len += sizeof(#MACRO);                                      \
            p->string = Malloc(len + 1);                                \
            p->macro = #MACRO;                                          \
            const int x = snprintf(p->string,                           \
                                   len,                                 \
                                   format,                              \
                                   (#MACRO)) ;                          \
            if(x<0)                                                     \
            {                                                           \
                Exit_binary_c(BINARY_C_XPRINTF_FAILED,                 \
                              "snprintf failed in cmd_line_macros_pairs.h"); \
            }                                                           \
            p->value = (ENUM);                                          \
            Cprint("p string \"%s\", value %g, d = %d\n",               \
                   p->string,                                           \
                   p->value,                                            \
                   x);                                                  \
            Safe_free(format);                                          \
        }                                                               \
    }

#endif // CMD_LINE_MACRO_PAIRS_H
