#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef RANDOM_SYSTEMS

/*
 * Function to output a list of random systems, then exit
 */

void random_system_list(struct stardata_t * const Restrict stardata)
{
    stardata->preferences->random_systems = TRUE;
    for(int i=0; i<stardata->preferences->repeat; i++)
    {
        set_random_system(stardata,
                          RANDOM_SYSTEM_OUTPUT_ARGS_ONLY);
    }
    Exit_binary_c(BINARY_C_SILENT_EXIT,
                  NULL);
}

#endif //RANDOM_SYSTEMS
