#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef STARDATA_STATUS

/* useful macros */
#define OUTARRAY(S,P,N) printf("Array %s: ",S);fflush(stdout);if(P!=NULL){for(i=0;i<N;i++){printf("%d=%g, ",i,(double)P[i]);}printf("\n");}else{printf("NULL\n");}
#define OUTXARRAY(S,P) OUTARRAY(S,P,ISOTOPE_ARRAY_SIZE)

void stardata_status(struct stardata_t * Restrict const stardata,
                     const Star_number k)
{
    int i;
    if(k>0)
    {
        struct star_t * Restrict SETstar(k);


        /* status function for each star */
        printf("Star %d structure\n",k);
        printf("Mass %3.3e (phase_start_mass=%g)\n",star->mass,
               star->phase_start_mass);
        printf("Stellar type=%d starnum=%d\n",star->stellar_type,star->starnum);
        printf("Radius %3.3e (roche_radius=%g rol0=%g)\n",star->radius,
               star->roche_radius,star->rol0);
        printf("age=%g aj0=%g epoch=%g\n",star->age,star->aj0,star->epoch);
        printf("core_mass[CORE_He]=%g core_radius=%g\n",star->core_mass[CORE_He],star->core_radius);
        printf("luminosity=%g Xray_luminosity=%g\n",star->luminosity,star->Xray_luminosity);
        printf("q=%g tbgb=%g tms=%g dmr=%g dmt=%g dms=%g\n",
               star->q,
               star->tbgb,
               star->tms,
               star->derivative[STELLAR_MASS_WIND_LOSS],
               star->derivative[STELLAR_MASS_WIND_GAIN],
               star->dM_in_timestep);
        printf("dspint=%g djspint=%g, mcxx=%g mass_at_RLOF_start=%g rdot=%g stellar_timestep=%g effective_radius=%g tkh=%g\n",
               star->dspint,
               star->derivative[DERIVATIVE_STELLAR_ANGMOM],
               star->mcxx,
               star->mass_at_RLOF_start,
               star->rdot,
               star->stellar_timestep,
               star->effective_radius,
               star->tkh);
        printf("vwind=%g max_EAGB_He_core_mass=%g he_mcmax=%g\n",
               star->vwind,
               star->max_EAGB_He_core_mass,
               star->he_mcmax);
        printf("went_sn_last_time=%d, supernova=%d\n",
               star->went_sn_last_time,star->supernova);
        printf("menv=%g renv=%g k2=%g\n",
               star->menv,
               star->renv,
               star->k2);
        printf("effective_zams_mass=%g pms_mass=%g\n",
               star->effective_zams_mass,
               stardata->common.zero_age.mass[star->starnum]);

        printf("mcx_EAGB=%g initial_mcx_EAGB=%g time_inf_EAGB=%g former_deltat=%g lum_dmc_correction=%g\n",
               star->mcx_EAGB,
               star->initial_mcx_EAGB);

#ifdef NUCSYN
        printf("NUCSYN\nfirst dup=%d second dup=%d\n",
               star->first_dredge_up,
               star->second_dredge_up);
        printf("time_first_pulse = %g\n",
               star->time_first_pulse);
        printf("time_prev_pulse = %g prev_tagb=%g\n",
               star->time_prev_pulse,star->prev_tagb);
        printf("time_next_pulse = %g\n",
               star->time_next_pulse);
        printf("interpulse_period = %g\n",
               star->interpulse_period);
        printf("mc_1tp=%g num_thermal_pulses=%g num_thermal_pulses_since_mcmin=%g\n",
               star->mc_1tp,
               star->num_thermal_pulses,
               star->num_thermal_pulses_since_mcmin);
        printf("menv_1tp=%g  core_mass_no_3dup=%g\n",
               star->menv_1tp,
               star->core_mass_no_3dup
            );
        printf("rho=%g temp=%g start_HG_mass=%g\n",
               star->rho,
               star->temp,
               star->start_HG_mass);
        printf("newhestar=%d hezamsmetallicity=%g he_t=%g he_f=%g\n",
               star->newhestar,
               star->hezamsmetallicity,
               star->he_t,
               star->he_f);
        printf("dmacc=%g spiky_luminosity=%g \n",
               star->dmacc,
               star->spiky_luminosity);
        printf("f_burn=%g f_hbb=%g temp_mult=%g temp_rise_fac=%g fmdupburn=%g ftimeduphbb=%g\n",
               star->f_burn,star->f_hbb,star->temp_mult,star->temp_rise_fac,star->fmdupburn,star->ftimeduphbb);

        printf("tm=%g tn=%g mixdepth=%g mixtime=%g\n",
               star->tm,
               star->tn,
               star->mixdepth,
               star->mixtime);

        OUTXARRAY("Xenv",star->Xenv);
        OUTXARRAY("Xsurfs",star->Xsurfs);
        OUTXARRAY("Xyield",star->Xyield);
        OUTXARRAY("mpyield",star->mpyield);
        OUTXARRAY("Xinit",star->Xinit);
        OUTXARRAY("Xacc",star->Xacc);
        OUTXARRAY("Xhbb",star->Xhbb);
#endif
    }
    else
    {
        // general status message

        printf("stardata is at %p\n",stardata);

        struct model_t * Restrict model=&(stardata->model);
        struct common_t * Restrict common=&(stardata->common);
        struct preferences_t * Restrict prefs=stardata->preferences;

        printf("pointers: model=%p, common=%p, preferences=%p, star=%p\n",
               model,common,prefs,stardata->star);

        printf("Existence probability %g\n",model->probability);
        printf("Maximum evolution time %g\n",model->max_evolution_time);
        printf("Period %g (separation %g)\n",common->orbit.period,common->separation);
        printf("Metallicity %g\n",common->metallicity);
        printf("Eccentricity %g\n",common->eccentricity);


        printf("Physics:\neddfac=%3.3e gamma=%3.3e, gbwindfac=%3.3e, lambda_min=%g delta_mcmin=%g\n",
               model->accretion_limit_eddington_multiplier,

               prefs->rlof_angmom_gamma,
               model->gbwindfac,
               common->lambda_min,
               common->delta_mcmin);
        printf("Model array\n");
        printf("probability=%g time=%g tphys0=%g tphys00=%g\n",
               model->probability,
               model->time,
               model->tphys0,
               model->tphys00);
        printf("dt=%g dtm=%g dtm0=%g time_remaining=%g\n",
               model->dt,
               model->dtm,
               model->dtm0,
               model->time_remaining);
        printf("supernova=%d coalesce=%d inttry=%d sgl=%d change=%d prec=%d com=%d bflag=%d intpol=%d iter=%d\n",
               model->supernova,
               model->coalesce,
               model->inttry,
               model->sgl,
               model->change,
               model->prec,
               model->com,
               model->bflag,
               model->intpol,
               model->iter);
        printf("tflag=%d comenv=%d\n",
               model->tflag,
               model->comenv);
        printf("accretion_limit_eddington_multiplier=%g  gamma=%g\n",
               model->accretion_limit_eddington_multiplier,

               prefs->rlof_angmom_gamma);
        printf("supedd=%d rmin=%g \n",
               model->supedd,
               model->rmin);
        printf("skip_initialization=%d roche_overflow=%d\n",
               model->skip_initialization,
               model->roche_overflow);
        printf("ndonor=%d naccretor=%d kw1=%d kw2=%d\n",
               model->ndonor,
               model->naccretor,
               model->kw1,
               model->kw2);
        printf("gbwindfac=%g\n",
               model->gbwindfac);

        /**** COMMON *****/

        printf("COMMON struct\n");
        printf("orbit.period=%g metallicity=%g eccentricity=%g\n",
               common->orbit.period,
               common->metallicity,
               common->eccentricity);
        printf("orbit: separation=%g orbital_angular_momentum=%g orbit.angular_frequency=%g\n",
               common->orbit.separation,
               common->orbit.angular_momentum,
               common->orbit.angular_frequency);
        printf("djorb/dt=%g de/dt=%g\n",
               model->derivative[DERIVATIVE_ORBIT_ANGMOM],
               model->derivative[DERIVATIVE_ORBIT_ECCENTRICITY]);
        printf("dM_RLOF_lose=%g dM_RLOF_transfer=%g RLOF_speed_up_factor=%g\n",
               common->dM_RLOF_lose,
               common->dM_RLOF_transfer,
               common->RLOF_speed_up_factor);
        printf("zero_age.separation[0]=%g zero_age.eccentricity[0]=%g\n",
               common->zero_age.sepration[0],
               common->zero_age.eccentricity[0]);
        printf("dM_RLOF_accrete=%g\n",
               common->dM_RLOF_accrete);
        printf("lambda_min=%g delta_mcmin=%g\n",
               common->lambda_min,
               common->delta_mcmin);
        printf("max_mass_for_second_dredgeup=%g\n",
               common->max_mass_for_second_dredgeup);

#ifdef NUCSYN
        OUTXARRAY("mnuc",common->mnuc);
        OUTXARRAY("mnuc_amu",common->mnuc_amu);
        OUTXARRAY("molweight",common->molweight);
        OUTXARRAY("XZAMS",preferences->zero_age.XZAMS);
        OUTXARRAY("Xsolar",preferences->zero_age.Xsolar);
        OUTXARRAY("atomic_number",common->atomic_number);
        OUTXARRAY("nucleon_number",common->nucleon_number);
        OUTARRAY("sigmav",common->sigmav,SIGMAV_SIZE);
#endif

        printf("PREFERENCES struct\n");
        printf("tpagbwind=%d superwind_mira_switchon=%g tpagb_reimers_eta=%g wr_wind=%d wr_wind_fac=%g\n",
               prefs->tpagbwind,
               prefs->tpagb_superwind_mira_switchon,
               prefs->tpagb_reimers_eta,
               prefs->wr_wind,
               prefs->wr_wind_fac);
        printf("alpha_ce=%g lambda_ce=%g lambda_ionisation=%g BH_prescription=%d nova_retention_fraction=%g \n",
               prefs->alpha_ce,
               prefs->lambda_ce,
               prefs->lambda_ionisation,
               prefs->BH_prescription,
               prefs->nova_retention_fraction);


        printf("rlof_angmom_gamma=%g sn_sigma=%g reimers_gb_neta=%g bb=%g Bondi-Hoyle accretion factor=%g\n",
               prefs->rlof_angmom_gamma,prefs->sn_sigma,prefs->reimers_gb_neta,
               prefs->bb,prefs->Bondi_Hoyle_accretion_factor);

        printf("\n");
    }
}

#endif //STARDATA_STATUS
