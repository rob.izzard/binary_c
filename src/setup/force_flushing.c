#include "../binary_c.h"
No_empty_translation_unit_warning;

/*
 * torture technique for stdin, stdout and stderr : 
 * force I/O streams to be line- or un-buffered
 */


void force_flushing(void)
{
    /* force stdout to be line buffered */
    if(setvbuf(stdout,NULL,_IOLBF,0))
    {
        fprintf(stderr,"failed to change stdout to line-buffered\n");
    }

    /* force stdin to be line buffered */
    if(setvbuf(stdin,NULL,_IOLBF,0))
    {
        fprintf(stderr,"failed to change stdin to line-buffered\n");
    }
        
    /* force stderr to be unbuffered */
    if(setvbuf(stdout,NULL,_IONBF,0))
    {
        fprintf(stderr,"failed to change stderr to non-buffered\n");
    }
}
