#pragma once
#ifndef DEFAULTS_H
#define DEFAULTS_H

/*
 * Define the default preferences sets
 */
#include "defaults_sets.def"

/*
 * This is where you define the fallback set
 * of defaults which is used if no set is given
 * as a command-line argument.
 */
#define DEFAULTS_FALLBACK_SET 2024a
#define DEFAULTS_FALLBACK_FUNCTION set_default_preferences_2024a


/*
 * enum of labels for the sets
 */
#undef X
#define X(LABEL) DEFAULTS_SET_##LABEL,
enum { DEFAULTS_SET_FALLBACK, DEFAULTS_SETS_LIST DEFAULTS_SETS_NUMBER };
#undef X

/*
 * Array of strings
 */
#define X(LABEL) #LABEL,
static const char * const defaults_sets_strings[] = { Stringof(DEFAULTS_FALLBACK_SET), DEFAULTS_SETS_LIST };
#undef X

/*
 * Array of functions
 */
#define X(LABEL) set_default_preferences_##LABEL,
Maybe_unused static void (*default_preferences_functions[])(
    struct preferences_t * Restrict const preferences
    ) = {
    DEFAULTS_FALLBACK_FUNCTION, /* fallback option */
    DEFAULTS_SETS_LIST
};
#undef X



#endif // DEFAULTS_H
