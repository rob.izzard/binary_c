#include "../binary_c.h"
No_empty_translation_unit_warning;


/*
 * Free memory allocated in split_commandline : see
 * split_commandline()
 */
void split_commandline_free(char ** Restrict argv,
                            const int argc)
{
    free_in_string_array(argv,(size_t)argc);
    Safe_free(argv);
}
