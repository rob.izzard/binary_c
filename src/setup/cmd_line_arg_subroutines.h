#pragma once

#ifndef CMD_LINE_ARG_SUBROUTINES_H
#define CMD_LINE_ARG_SUBROUTINES_H
/*
 * Arguments to automated subroutine calls
 * and equivalent declarations are defined here.
 * Given that stardata is included, as is the
 * array of arguments and the counter position (*c)
 * you shouldn't need to change these.
 */
#define ARG_SUBROUTINE_ARGS                     \
    cmd_line_args,                              \
        stardata,                               \
        &c,                                     \
        i,                                      \
        stardata->store->argstore->arg_count,   \
        argv,                                   \
        argc

#define ARG_SUBROUTINE_ARGS2                    \
    cmd_line_args,                              \
        stardata,                               \
        c,                                      \
        i,                                      \
        stardata->store->argstore->arg_count,   \
        argv,                                   \
        argc

#define ARG_SUBROUTINE_TYPES                                        \
    struct cmd_line_arg_t * const,                                  \
        struct stardata_t * Restrict const ,                        \
        int * const,                                                \
        int,                                                        \
        int,                                                        \
        char **,                                                    \
        int

#define ARG_SUBROUTINE_DECLARATION                                  \
    struct cmd_line_arg_t * const cmd_line_args Maybe_unused,       \
        struct stardata_t * Restrict const stardata Maybe_unused,   \
        int * const c Maybe_unused,                                 \
        int i Maybe_unused,                                         \
        int arg_count Maybe_unused,                                 \
        char ** argv Maybe_unused,                                  \
        int argc Maybe_unused

/*
 * function pointer type
 */
typedef void (*arg_funcptr_t)(ARG_SUBROUTINE_TYPES);


#endif // CMD_LINE_ARG_SUBROUTINES_H
