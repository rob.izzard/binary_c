#pragma once
#ifndef ARGUMENT_SETTING_FUNCTIONS_H
#define ARGUMENT_SETTING_FUNCTIONS_H

#include "cmd_line_function_macros.h"



static char * arg_variable_default_string(
    const struct cmd_line_arg_t * Restrict const arg);

static void _show_image_ascii_art(const unsigned char * const imagedata,
                                  const size_t imagesize);

static void set_merger_mass_loss_fractions(struct stardata_t * const stardata,
                                           const char * const arg,
                                           const Stellar_type low,
                                           const Stellar_type high);

static void _set_qcrit(struct stardata_t * const stardata,
                       const char * const arg,
                       const Stellar_type low,
                       const Stellar_type high);

static void _scanf_options(struct stardata_t * const stardata,
                           const struct cmd_line_arg_t * const arg);


#endif //  ARGUMENT_SETTING_FUNCTIONS_H
