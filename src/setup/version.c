#include "../binary_c.h"
No_empty_translation_unit_warning;

#include <sys/resource.h>
static unsigned long int __test_milliseconds(void);
#include "version.h"
#include "../setup/defaults_sets.h"

#include <limits.h>     /* for CHAR_BIT */

#include "control/loop_unroller.h"


void version(struct stardata_t * Restrict const stardata)
{
    /*
     * Version information for binary_c
     */

    /*
     * First, prevent output to the logfile
     */
    if(stardata)
    {
        snprintf(stardata->preferences->log_filename,
                 10,
                 "/dev/null");
    }

    Printf("\nBinary_c/nucsyn by Robert Izzard, originally based on BSE of Hurley, Pols, Tout, Aarseth, but with many updates and rewrites.\nEmail r.izzard@surrey.ac.uk, rob.izzard@gmail.com\nVersion %s\nBuild: %s %s\n",
           BINARY_C_VERSION,
           __DATE__,
           __TIME__);
    Show_string_macro(BINARY_C_VERSION);

    /* binary_c version strings */

    Show_string_macro(BINARY_C_MAJOR_VERSION);
    Show_string_macro(BINARY_C_MINOR_VERSION);
    Show_string_macro(BINARY_C_PATCH_VERSION);
    Show_string_macro(BINARY_C_PRE_VERSION);
    Show_string_macro(BINARY_C_STABLE_VERSION);
    Show_string_macro(BINARY_C_PYTHON_VERSION);

    /* SVN : deprecated from early 2018 but will do no harm if left in */
#if defined SVN_REVISION && (SVN_REVISION+0)
    Printf("SVN revision %d\n", SVN_REVISION + 0);
#else
    Printf("SVN revision unknown\n");
#endif
    Show_string_macro(SVN_REVISION);
#ifdef SVN_URL
    Printf_deslash("SVN URL \"%s\"\n", Stringof(SVN_URL));
#else
    Printf("SVN URL unknown\n");
#endif
    Show_string_macro(SVN_URL);

    /* git : replaces SVN from 2018 */
#ifdef GIT_REVISION
    Printf_deslash("git revision \"%s\"\n", Stringof(GIT_REVISION));
#else
    Printf("git revision unknown\n");
#endif
#ifdef GIT_URL
    Printf_deslash("git URL %s\n", Stringof(GIT_URL));
#else
    Printf("git URL unknown\n");
#endif
#ifdef GIT_BRANCH
    Printf_deslash("git branch %s\n", Stringof(GIT_BRANCH));
#else
    Printf("git branch unknown\n");
#endif
    Show_string_macro(GIT_BRANCH);
    Show_string_macro(GIT_REVISION);
    Show_string_macro(GIT_URL);


    Macrotest(__VERSION__);
#ifdef __VERSION__
    Show_string_macro(__VERSION__);
#endif

    Macrotest(__OPTIMIZE__);
#ifdef __OPTIMIZE__
    Show_int_macro(__OPTIMIZE__);
#endif

    /* Meson build information */
#ifdef MESON_PROJECT_NAME
    Show_string_macro(MESON_PROJECT_NAME);
#endif
#ifdef MESON_PROJECT_VERSION
    Show_string_macro(MESON_PROJECT_VERSION);
#endif

    /* CFLAGS, CC, LD etc. are set by configure */
    Macrotest(CFLAGS);
#ifdef CFLAGS
    Show_string_macro(CFLAGS);
#endif
    Macrotest(COMPILER_ID);
#ifdef COMPILER_ID
    Show_string_macro(COMPILER_ID);
#endif
    Macrotest(CC);
#ifdef CC
    Show_string_macro(CC);
#endif
    Macrotest(CCCOMMAND);
#ifdef CCCOMMAND
    Show_string_macro(CCCOMMAND);
#endif
    Macrotest(LINKER_ID);
#ifdef LINKER_ID
    Show_string_macro(LINKER_ID);
#endif
    Macrotest(LD);
#ifdef LD
    Show_string_macro(LD);
#endif
    Macrotest(LIBS);
#ifdef LIBS
    Show_string_macro(LIBS);
#endif
    Macrotest(INCDIRS);
#ifdef INCDIRS
    Show_string_macro(INCDIRS);
#endif
    Macrotest(LIBDIRS);
#ifdef LIBDIRS
    Show_string_macro(LIBDIRS);
#endif
    Macrotest(BJORNFUN);
    Macrotest(BINARY_C_LOGO);
    Macrotest(__OPTIMIZE_SIZE__);
    Macrotest(__NO_INLINE__);
    Macrotest(__GNUC__);
#ifdef __GNUC__
    Show_int_macro(__GNUC__);
    Show_int_macro(__GNUC_MINOR__);
    Show_int_macro(__GNUC_PATCHLEVEL__);
#endif
    Macrotest(__clang__);
    Macrotest(__SUNPRO_C);
    Macrotest(__INTEL_COMPILER);
    Macrotest(__STDC__);
    Macrotest(__STDC_VERSION__);
    Macrotest(BINUTILS_VERSION);
#ifdef __STDC_VERSION__
    Show_long_int_macro(__STDC_VERSION__);
#endif
    Macrotest(__STRICT_ANSI__);
    Macrotest(__ELF__);
    Macrotest(_POSIX_C_SOURCE);
#ifdef _POSIX_C_SOURCE
    Show_long_int_macro(_POSIX_C_SOURCE);
#endif
    Macrotest(USE_GCC_EXTENSIONS);
    Macrotest(_XOPEN_SOURCE);
#ifdef _XOPEN_SOURCE
    Show_long_int_macro(_XOPEN_SOURCE);
#endif
    Macrotest(_ISOC11_SOURCE);
#ifdef _ISOC11_SOURCE
    Show_long_int_macro(_ISOC11_SOURCE);
#endif
    Macrotest(_ISOC99_SOURCE);
#ifdef _ISOC11_SOURCE
    Show_long_int_macro(_ISOC99_SOURCE);
#endif
    Macrotest(_C89);
    Macrotest(_C90);
    Macrotest(_C94);
    Macrotest(_C99);
    Macrotest(_C11);

    /*
     * Obtain macros defined locally with:
     * gcc -march=native -dM -E - < /dev/null | egrep "SSE|AVX" | sort
     *
     * or run src/perl/scripts2/cflags.pl
     */
    Macrotest(__AVX2__);
    Macrotest(__AVX512BITALG__);
    Macrotest(__AVX512BW__);
    Macrotest(__AVX512CD__);
    Macrotest(__AVX512DQ__);
    Macrotest(__AVX512F__);
    Macrotest(__AVX512IFMA__);
    Macrotest(__AVX512VBMI2__);
    Macrotest(__AVX512VBMI__);
    Macrotest(__AVX512VL__);
    Macrotest(__AVX512VNNI__);
    Macrotest(__AVX512VP2INTERSECT__);
    Macrotest(__AVX512VPOPCNTDQ__);
    Macrotest(__AVX__);
    Macrotest(__MMX_WITH_SSE__);
    Macrotest(__SSE2_MATH__);
    Macrotest(__SSE2__);
    Macrotest(__SSE3__);
    Macrotest(__SSE4__);
    Macrotest(__SSE4A__);
    Macrotest(__SSE4_1__);
    Macrotest(__SSE4_2__);
    Macrotest(__SSE_MATH__);
    Macrotest(__SSE__);
    Macrotest(__SSSE3__);

#ifdef __BYTE_ORDER__
    const char * const byte_endianness =
        __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__ ? "little" :
        __BYTE_ORDER__ == __ORDER_BIG_ENDIAN__ ? "big" :
        __BYTE_ORDER__ == __ORDER_PDP_ENDIAN__ ? "PDP" :
        "unknown";
    Printf("Byte order is %s endian\n", byte_endianness);
#else
    Printf("Byte order is unknown (__BYTE_ORDER__ undefined)\n");
#endif
    Macrotest(__FLOAT_WORD_ORDER__);
#ifdef __FLOAT_WORD_ORDER__
    const char * const float_endianness =
        __FLOAT_WORD_ORDER__ == __ORDER_LITTLE_ENDIAN__ ? "little" :
        __FLOAT_WORD_ORDER__ == __ORDER_BIG_ENDIAN__ ? "big" :
        "unknown";
    Printf("Float order is %s endian\n", float_endianness);
#else
    Printf("Float order is unknown (__FLOAT_WORD_ORDER__ undefined)\n");
#endif // __FLOAT_WORD_ORDER__
    Macrotest(__STDC_IEC_559__);
    Macrotest(ACML);
    Macrotest2(__NO_MATH_ERRNO__,
               "(probably via -ffast-math)");
    Macrotest2(__FAST_MATH__,
               "(probably via -ffast-math)");
    Macrotest2(__SUPPORT_SNAN__,
               "(signalling NaNs)");
    Macrotest(__HAVE_POSIX_FADVISE);
    Macrotest(__HAVE_LINK_H);

    Show_string_macro(FPU_PRECISION);
    Macrotest(FPU_CAPTURE_INVALID);
    Macrotest(FPU_CAPTURE_INEXACT);
    Macrotest(FPU_CAPTURE_UNDERFLOWS);
    Macrotest(FPU_CAPTURE_OVERFLOWS);
    Macrotest(FPU_CAPTURE_DIVIDE_BY_ZERO);
    Macrotest(FPU_CONTROL);
    Macrotest(FP_FAST_FMA);
    Macrotest(FP_FAST_FMAF);
    Macrotest(FP_FAST_FMAL);
    Macrotest(CHECK_ISNAN_WORKS);
    Macrotest(__HAVE_NATIVE_EXP10);
    Macrotest(__HAVE_BSD_QSORT_R);
    Macrotest(__HAVE_GNU_QSORT_R);
#ifdef AVX2_MINIMUM_ALIGNSIZE
    Show_int_macro(AVX2_MINIMUM_ALIGNSIZE);
#endif
    Macrotest(AVX2_EXTENSIONS);
    Macrotest(AVX2_ASSUME_ALIGNED);

    Show_float_macro_as_string(ZERO);
    Show_float_macro_as_string(TINY);
    Show_float_macro_as_string(VERY_TINY);
    Show_float_macro_as_string(REALLY_TINY);
    Show_float_macro_as_string(LARGE_FLOAT);
    Show_float_macro_as_string(PI);
    Show_float_macro_as_string(TWOPI);
    Show_float_macro_as_string(FLT_MIN);
    Show_float_macro_as_string(FLT_MAX);
    Show_float_macro_as_string(DBL_MIN);
    Show_float_macro_as_string(DBL_MAX);
    Show_float_macro_as_string(LDBL_MIN);
    Show_float_macro_as_string(LDBL_MAX);
    Show_float_macro_as_string(FLT_EPSILON);
    Show_float_macro_as_string(DBL_EPSILON);
    Show_float_macro_as_string(LDBL_EPSILON);
    Show_int_macro_as_string(SHRT_MIN);
    Show_int_macro_as_string(SHRT_MAX);
    Show_int_macro_as_string(INT_MIN);
    Show_int_macro_as_string(INT_MAX);
    Show_int_macro_as_string(USHRT_MAX);
    Show_unsigned_int_macro_as_string(UINT_MAX);
    Show_int_macro_as_string(LONG_MIN);
    Show_int_macro_as_string(LONG_MAX);

    /* todo : represent these as strings */
    Show_int_macro_as_string(LLONG_MIN);
    Show_int_macro_as_string(LLONG_MAX);
    Show_unsigned_int_macro_as_string(ULONG_MAX);
    Show_unsigned_int_macro_as_string(ULLONG_MAX);

    Show_string_macro(Pure_function);
    Show_string_macro(Constant_function);
    Show_string_macro(Restrict);
    Show_string_macro(inline);
    Show_string_macro(Fastcall);
    Show_string_macro(Hot_function);
    Show_string_macro(Nonnull_all_arguments);
    Show_string_macro_takes_varargs(Nonnull_some_arguments(1,2,3));
    Show_string_macro(Alloc_size_first);
    Show_string_macro(Alloc_size_second);
    Show_string_macro(Alloc_size_product);
    Show_string_macro(Returns_nonnull);
    Show_string_macro(Deprecated(dummy_feature));
    Show_string_macro(Deprecated_function);
    Show_string_macro(Stringify_macro(Autotype));
    Show_string_macro(Stringify(Autotype));
    Macrotest(__USE_C99_BOOLEAN__);

    Show_string_macro_takes_varargs(unlikely(something_unlikely));
    Show_string_macro_takes_varargs(likely(something_likely));
    Show_string_macro(Maybe_unused);

    Macrotest(__HAVE_ZLIB__);
#ifdef __HAVE_ZLIB__
    Show_string_macro(ZLIB_VERSION);
#endif//__HAVE_ZLIB__

    Macrotest(__HAVE_LIBRINTERPOLATE__);
    Macrotest(__HAVE_LIBRINTERPOLATE_BINARY_C__);
    Show_string_macro(RINTERPOLATE_VERSION);
    Show_int_macro(RINTERPOLATE_MAJOR_VERSION);
    Show_int_macro(RINTERPOLATE_MINOR_VERSION);
    Macrotest(RINTERPOLATE_NANCHECKS);
    Show_string_macro(GSL_VERSION);
    Show_int_macro(GSL_MAJOR_VERSION);
    Show_int_macro(GSL_MINOR_VERSION);
    Show_string_macro(GSL_ROOT_FSOLVER);
    Show_int_macro(GSL_INTEGRATOR_WORKSPACE_SIZE);
    Macrotest(__HAVE_LIBSUNDIALS_VCODE__);
    Macrotest(__HAVE_LIBSUNDIALS_VCODES__);
    Macrotest(DEFINE_DATA_OBJECTS);

    Show_string_macro(__int__);
    Show_string_macro(__double__);
    Show_string_macro(__long__);
    Show_string_macro(__short__);
    Show_string_macro(__unsigned__);
    Show_string_macro(__const__);
    Show_string_macro(__static__);

    const size_t opt_sizes[] =
    {
#ifdef DISCS
        sizeof(struct binary_system_t),
        sizeof(struct power_law_t),
        sizeof(struct disc_thermal_zone_t),
        sizeof(struct disc_loss_t),
        sizeof(struct disc_t),
#else
        (size_t)0,
        (size_t)0,
        (size_t)0,
        (size_t)0,
        (size_t)0,
#endif//DISCS
#ifdef USE_MERSENNE_TWISTER
        sizeof(struct mersenne_twister_data_t),
#else
        (size_t)0,
#endif//USE_MERSENNE_TWISTER
#ifdef EVOLUTION_SPLITTING
        sizeof(struct splitinfo_t),
#else
        (size_t)0,
#endif//EVOLUTION_SPLITTING
#ifdef MINT
        sizeof(struct mint_t),
#else
        (size_t)0,
#endif//MINT

    };

    Printf("Size of : short int %zu, unsigned short int %zu, int %zu, unsigned int %zu, long int %zu, unsigned long int %zu, long long int %zu, unsigned long long int %zu, size_t %zu, float %zu, double %zu, long double %zu, char %zu, Boolean %zu, stardata_t %zu, preferences_t %zu, star_t %zu, common_t %zu, model_t %zu, diffstats_t %zu, probability_distribution_t %zu, RLOF_orbit_t %zu, store_t %zu, tmpstore_t %zu, data_table_t %zu, stardata_dump_t %zu, GSL_args_t %zu, envelope_t %zu, envelope_shell_t %zu, equation_of_state_t %zu, opacity_t %zu, kick_system_t %zu, coordinate_t %zu, binary_system_t %zu, power_law_t %zu, disc_thermal_zone_t %zu, disc_loss_t %zu, disc_t %zu, mersenne_twister_t %zu, binary_c_random_buffer_t %zu, binary_c_file_t %zu, difflogitem_t %zu, difflogstack_t %zu, binary_c_fixed_timestep_t %zu, splitinfo_t %zu, derivative_t %zu, mint_t %zu, orbit_t %zu, Random_seed %zu, Random_buffer %zu, FILE %zu, void* %zu, short int* %zu, unsigned short int* %zu, int* %zu, unsigned int* %zu, long int* %zu, unsigned long int* %zu, long long int* %zu, unsigned long long int* %zu, float* %zu, double* %zu, long double* %zu, char* %zu, Boolean* %zu, stardata_t* %zu, star_t* %zu, FILE* %zu, __int__ %zu, __double__ %zu, __unsigned__ __int__ %zu, __short__ __int__ %zu, __long__ __int__ %zu, Hash_key_type %zu, hash_entry_t %zu ",
           sizeof(short int),
           sizeof(unsigned short int),
           sizeof(int),
           sizeof(unsigned int),
           sizeof(long int),
           sizeof(unsigned long int),
           sizeof(long long int),
           sizeof(unsigned long long int),
           sizeof(size_t),
           sizeof(float),
           sizeof(double),
           sizeof(long double),
           sizeof(char),
           sizeof(Boolean),
           sizeof(struct stardata_t),
           sizeof(struct preferences_t),
           sizeof(struct star_t),
           sizeof(struct common_t),
           sizeof(struct model_t),
           sizeof(struct diffstats_t),
           sizeof(struct probability_distribution_t),
           sizeof(struct RLOF_orbit_t),
           sizeof(struct store_t),
           sizeof(struct tmpstore_t),
           sizeof(struct data_table_t),
           sizeof(struct stardata_dump_t),
           sizeof(struct GSL_args_t),
           sizeof(struct envelope_t),
           sizeof(struct envelope_shell_t),
           sizeof(struct equation_of_state_t),
           sizeof(struct opacity_t),
           sizeof(struct kick_system_t),
           sizeof(struct coordinate_t),
           opt_sizes[0], //discs 0-4
           opt_sizes[1],
           opt_sizes[2],
           opt_sizes[3],
           opt_sizes[4],
           opt_sizes[5], // mersenne


           sizeof(struct binary_c_random_buffer_t),
           sizeof(struct binary_c_file_t),
           sizeof(struct difflogitem_t),
           sizeof(struct difflogstack_t),
           sizeof(struct binary_c_fixed_timestep_t),
           opt_sizes[6], // splitinfo_t
           sizeof(struct derivative_t),
           opt_sizes[7], // mint_t
           sizeof(struct orbit_t),
           sizeof(Random_seed),
           sizeof(Random_buffer),
           sizeof(FILE),
           sizeof(void *),
           sizeof(short int *),
           sizeof(unsigned short int *),
           sizeof(int *),
           sizeof(unsigned int *),
           sizeof(long int *),
           sizeof(unsigned long int *),
           sizeof(long long int *),
           sizeof(unsigned long long int *),
           sizeof(float *),
           sizeof(double *),
           sizeof(long double *),
           sizeof(char *),
           sizeof(Boolean *),
           sizeof(struct stardata_t *),
           sizeof(struct star_t *),
           sizeof(FILE *),
           sizeof(__int__),
           sizeof(__double__),
           sizeof(__unsigned__ __int__),
           sizeof(__short__ __int__),
           sizeof(__long__ __int__),
           sizeof(CDict_key_type),
           sizeof(struct cdict_entry_t)
          );

#ifdef DISCS
    Printf(", disc_t %zu, disc_thermal_zone_t %zu, power_law_t %zu",
           sizeof(struct disc_t),
           sizeof(struct disc_thermal_zone_t),
           sizeof(struct power_law_t)
          );
#endif
    Printf("\n");

    Show_string_macro(stardata_t);
    Show_string_macro(star_t);
    Show_string_macro(preferences_t);
    Show_string_macro(store_t);
    Show_string_macro(tmpstore_t);
    Show_string_macro(model_t);
    Show_string_macro(common_t);
    Show_string_macro(printf);
    Show_string_macro(_printf);

    Printf("\n");

    {
        struct rlimit limit;
        getrlimit (RLIMIT_STACK, &limit);
        Printf("Stack limit current = %ju, max = %ju (-1 means no limit)\n",
               (uintmax_t)limit.rlim_cur,
               (uintmax_t)limit.rlim_max);
    }
    Macrotest(SEGFAULTS);
    Macrotest(ALTSTACK);
    Macrotest(CATCH_SIGVTALRM);
    Show_string_macro(OPERATING_SYSTEM);
    Macrotest(LINUX);
    Macrotest(WINDOWS);
    Macrotest(DARWIN);
    Macrotest(UNKNOWN_OPERATING_SYSTEM);
    Macrotest(__APPLE__);
    Macrotest(__arm__);
    Macrotest(__i386__);
    Macrotest(VIRTUAL_MACHINE);
    Show_string_macro(VIRTUAL_MACHINE);

    Macrotest(USE_SPLIT_COMMANDLINE);
    Show_string_macro(MEMORY_ALLOCATION_MODEL);
    Show_string_macro(MEMORY_ALLOCATION_MODEL_STRING);
    Macrotest(MEMORY_USE);
#ifdef ALIGNSIZE
    Printf("Memory is aligned to %d bytes (set in ALIGNSIZE)\n", ALIGNSIZE);
#else
    Printf("ALIGNSIZE is not defined\n");
#endif
    Show_string_macro(Builtin_aligned);
    Show_string_macro(Assume_aligned);
    Macrotest(ALLOC_CHECKS);
    Macrotest(HAVE_MALLOC_USABLE_SIZE);
    Macrotest(CHECK_MEMCPY_ALIGNMENT);
    Show_string_macro(Malloc_usable_size);
    Macrotest(MEMCPY_USE_SKYWIND_FAST);
    Macrotest(MEMCPY_USE_APEX);
    Macrotest(MEMCPY_USE_ASMLIB);
    Macrotest(MEMCPY_USE_STRIDED);
    Macrotest(TARGET_STACK_PROTECT_RUNTIME_ENABLED_P);
    Show_string_macro(STACK_SIZE);
    Macrotest(STACK_ACTION);
    Show_string_macro(STACK_ACTION);
    Macrotest(STACK_CHECK_BUILTIN);
    Macrotest(STACK_CHECK_STATIC_BUILTIN);
    Macrotest(STACK_CHECKS);
    Macrotest(BAN_UNSAFE_FUNCTIONS);
    Macrotest(MEMOIZE);
    Macrotest(__HAVE_LIBMEMOIZE__);
    Show_string_macro(MEMOIZE_VERSION);
    Show_string_macro(MEMOIZE_HEADER_FILE);
    Macrotest(__HAVE_LIBCDICT__);
    Show_string_macro(CDICT_VERSION);
    if((int)sizeof(long int) == 4)
    {
        Printf("Compiled for 32 bit architecture\n");
    }
    else if((int)sizeof(long int) == 8)
    {
        Printf("Compiled for 64 bit architecture\n");
    }
    else
    {
        Printf("Compiled for an architecture with an unknown number of bits\n");
    }

    Macrotest(LTO);
    Macrotest(PGO_off);
    Macrotest(PGO_generate);
    Macrotest(PGO_use);
    Macrotest(VALGRIND);
    Macrotest(GPROF);
    Macrotest(__HAVE_VALGRIND__);
    Macrotest(__HAVE_LIBGSL__);
    Macrotest(__HAVE_LIBBFD__);
    Macrotest(__HAVE_LIBBSD__);
    Macrotest(__HAVE_LIBIBERTY__);
    Macrotest(__HAVE_LIBIBERTYH__);
    Macrotest(__HAVE_LIBIBERTY_LIBIBERTYH__);
    Macrotest(__HAVE_LIBBACKTRACE__);
    Macrotest(__HAVE_LIBPTHREAD__);
    Macrotest(__HAVE_LIBM__);
    Macrotest(__HAVE_LIBJEMALLOC__);
    Macrotest(__HAVE_ZCAT__);
    Macrotest(__HAVE_BZCAT__);

    Macrotest(__HAVE_MALLOC_H__);
    Macrotest(__HAVE_DRAND48__);
    Macrotest(__HAVE_SETITIMER__);
    Macrotest(__HAVE_PKG_CONFIG__);
    Macrotest(BACKTRACE);
#ifdef BACKTRACE
#undef X
#define X(N) #N,
    char * backtrace_method_strings[] =
    {
        BACKTRACE_METHOD_LIST
    };
#undef X
    Printf("BACKTRACE_METHOD %d %s\n",
           BACKTRACE_METHOD,
           backtrace_method_strings[BACKTRACE_METHOD]);
    Show_string_macro(Backtrace);
    Show_string_macro(BACKTRACE_GNU_BUFFER_STREAM);
#endif
    Macrotest(__STRLCPY_IS_MACRO__);
    Macrotest(__STRLCAT_IS_BSD_COPY__);


    Printf("TRUE tests - should all give Yes : ");
#undef X
#define X(STRING)                                               \
    Printf("%s = %s, ",#STRING,Yesno(String_is_true(STRING)));
    BOOLEAN_TEST_LIST_TRUE;
    Printf("\nTRUE tests - should all give No : ");
#undef X
#define X(STRING)                                               \
    Printf("%s = %s, ",#STRING,Yesno(String_is_true(STRING)));
    BOOLEAN_TEST_LIST_FALSE;

    Printf("FALSE tests - should all give Yes : ");
#undef X
#define X(STRING)                                               \
    Printf("%s = %s, ",#STRING,Yesno(String_is_false(STRING)));
    BOOLEAN_TEST_LIST_FALSE;
    Printf("\nTRUE tests - should all give No : ");
#undef X
#define X(STRING)                                               \
    Printf("%s = %s, ",#STRING,Yesno(String_is_false(STRING)));
    BOOLEAN_TEST_LIST_TRUE;


    Macrotest(TIMER);
    Macrotest(TIMER_USE_ASM);
    Macrotest(TIMER_PER_SYSTEM);
    Show_int_macro(TIMEOUT_SECONDS);
    Show_int_macro(TIMEOUT_SECONDS_WITH_VALGRIND);
    Macrotest(CPUFREQ);
    Show_int_macro(CPUFREQ);
    Macrotest(CLOCKS_PER_SEC);
    Show_int_macro(CLOCKS_PER_SEC);
    Printf("_SC_CLK_TCK = %g\n", (double)sysconf (_SC_CLK_TCK));

    Macrotest(millisecond);
    Show_float_macro(millisecond);

    /* end of compiler/environment macros */
    /************************************************************/
    /* now come binary_c macros */

    Macrotest(BINARY_C_API);
    Macrotest(BINARY_C_API_FORTRAN);
    Show_string_macro(binary_c_API_function);
    Show_int_macro(STARDATA_DUMP_FORMAT);
    Macrotest(__ACCURATE_BINARY_C__)
    Macrotest(DEBUG);
#ifdef DEBUG
    Show_int_macro(DEBUG);
#endif
    Macrotest(DEBUG_CALLER);
    Macrotest(DEBUG_CALLER_LINE);
    Macrotest(DEBUG_REMOVE_NAN_FROM_CALLER);
    Macrotest(DEBUG_REMOVE_NAN_FROM_FILESNAMES);
    Show_string_macro(Debug_expression);
#ifdef Debug_stop_expression
    Show_string_macro(Debug_stop_expression);
#endif
    Show_int_macro(NUM_ANSI_COLOURS);


#undef X
#define X(COLOUR,STRING) #COLOUR,
    static char * const colour_enums[] = { ANSI_COLOURS_LIST };
#undef X
#define X(COLOUR,STRING) STRING,
    static const char * const ansi_colour_strings[] = { ANSI_COLOURS_LIST };
#undef X
    for (int i = 0; i < NUM_ANSI_COLOURS; i++)
    {
        Printf("ANSI colour %d %s : Default is %s%s%s.\n",
               i,
               colour_enums[i],
               ansi_colour_strings[i],
               ansi_colour_strings[i] + 1,
               ansi_colour_strings[COLOUR_RESET]
              );
    }
    Macrotest(USE_POINTER_LOOPS);

    Macrotest(__NATIVE_PRINTF__);
    Macrotest(__NATIVE_EXIT__);
    Macrotest(MAX_EXIT_STATEMENT_PRINT_SIZE);
#ifdef MAX_EXIT_STATEMENT_PRINT_SIZE
    Show_int_macro(MAX_EXIT_STATEMENT_PRINT_SIZE);
#endif
    Macrotest(FLUSH_LOG);
    Macrotest(DISABLE_DPRINT);
    Macrotest(DEBUG_LINENUMBERS);
    Macrotest(DEBUG_SHOW_FILENAMES);
    Macrotest(DEBUG_FAIL_ON_NAN);

#undef X
#define X(CODE,TEXT) Printf("Error code %s = %d \"%s\"\n",  \
                            #CODE,                          \
                            CODE,                           \
                            TEXT);
    BINARY_C_ERROR_CODES;
#undef X

    Show_int_macro(TRUE);
    Show_int_macro(FALSE);

    Printf("Compiled in parameters:\n");
    Show_int_macro(NUMBER_OF_STARS);
    Show_int_macro(NSTARS_CMDLINE);
    Show_int_macro(NUMBER_OF_PREFIX_MACROS_ARGUMENTS);
    Macrotest(HASH_ARGUMENTS);
    Macrotest(ORBITING_OBJECTS);
    Macrotestif2(CFAMANDA,
                 "We use values to compare to Amanda's models\n");
    Macrotest(BATCHMODE);
    Macrotest(NANCHECKS);
    Macrotest(USE_NATIVE_ISNAN);
    Macrotest(__HAVE_ATTRIBUTE_BUILTIN_ISNAN__);
    Macrotest(SOFT_NANCHECK);
    Macrotest(__HAVE_BUILTIN_EXPECT__);
    Macrotest(__HAVE_ATTRIBUTE_CONST__);
    Macrotest(__HAVE_ATTRIBUTE_DEPRECATED__);
    Macrotest(__HAVE_ATTRIBUTE_ALLOC_SIZE__);
    Macrotest(__HAVE_ATTRIBUTE_RETURNS_NONNULL__);
    Macrotest(__HAVE_ATTRIBUTE_NONNULL__);
    Macrotest(__HAVE_ATTRIBUTE_FASTCALL__);
    Macrotest(__HAVE_ATTRIBUTE_PACKED__);
    Macrotest(__HAVE_ATTRIBUTE_HOT__);
    Macrotest(__HAVE_ATTRIBUTE_AUTO_TYPE__);
    Macrotest(__HAVE_ATTRIBUTE_UNUSED__);
    Macrotest(__HAVE_ATTRIBUTE_PURE__);
    Macrotest(__HAVE_ATTRIBUTE_MALLOC__);
    Macrotest(__HAVE_ATTRIBUTE_GNU_PRINTF__);
    Macrotest(__HAVE_ATTRIBUTE_NORETURN__);
    Macrotest(__HAVE_ATTRIBUTE_RESTRICT__);

    /* check that NaN checking works */
#ifdef NANCHECKS
    {
        const double _nan = 0.0 / 0.0;
        Printf("NaN checker gives %d\n", _Nanchecker(_nan));
#ifdef __HAVE_ATTRIBUTE_BUILTIN_ISNAN__
        Printf("builtin_isnan gives %d\n",
               __builtin_isnan(_nan));
#endif // __HAVE_ATTRIBUTE_BUILTIN_ISNAN__
    }
#endif // NANCHECKS

    Show_string_macro(DEFAULTS_FALLBACK_SET);
    Show_string_macro(DEFAULTS_FALLBACK_FUNCTION);

    Macrotest(KEMP_NOVAE);
    Macrotest(KEMP_SUPERNOVAE);
    Macrotest(KEMP_MERGERS);
    Macrotest(USE_2011_MASS_LOSS);
    Macrotest(USE_2012_MAIN_SEQUENCE_LIFETIMES_TABLE);
    Macrotest(MODULATE_TIMESTEP_TO_RESOLVE_ROTATION_MASS_LOSS);
    Show_float_macro(TIDAL_STRENGTH_FACTOR_DEFAULT);
    Macrotest(EXPONENTIAL_TIDES);
    Show_float_macro(MAGNETIC_BRAKING_GAMMA_DEFAULT);
    Show_float_macro(SPHERICAL_ANGMOM_ACCRETION_FACTOR);
    Show_float_macro(MINIMUM_STELLAR_ANGMOM);
    Show_float_macro(MAXIMUM_STELLAR_ANGMOM);
    Show_float_macro(MINIMUM_ORBITAL_ANGMOM);
    Show_float_macro(MAXIMUM_ORBITAL_ANGMOM);
    Show_float_macro(UNDEFINED_ORBITAL_ANGULAR_FREQUENCY);
    Show_string_macro(ANGULAR_MOMENTUM_CGS);
    Show_float_macro(ANGULAR_MOMENTUM_CGS);
    Show_int_macro(BURN_IN_TIMESTEPS);
    Macrotest(TRIPLE);
    Macrotest(REVERSE_TIME);

    Macrotest(EVENT_BASED_LOGGING);
#ifdef EVENT_BASED_LOGGING
    Show_int_macro(EVENT_BASED_LOGGING_MAX_EVENTS);
    Macrotest(EVENT_BASED_LOGGING_MIN_DCO_ST);
    Macrotest(EVENT_BASED_LOGGING_MAX_DCO_ST);
#endif // EVENT_BASED_LOGGING

    Show_string_macro(Total_mass);
    Show_float_macro(K2_GYRATION);
    Show_float_macro(CORE_MOMENT_OF_INERTIA_FACTOR);
    Show_float_macro(MINIMUM_SEPARATION_TO_BE_CALLED_BINARY);
    Show_float_macro(MAXIMUM_SEPARATION_TO_BE_CALLED_BINARY);
    Show_string_macro(System_is_single);
    Show_string_macro(System_is_binary);
    Show_float_macro(MIN_OBSERVATIONAL_STELLAR_MASS);
    Show_float_macro(MAX_OBSERVATIONAL_BINARY_ORBITAL_PERIOD);
    Show_float_macro(OBSERVABLE_RADIAL_VELOCITY_MIN_DEFAULT);
    Show_string_macro(Observable_binary);
    Show_string_macro(Angular_momentum_from_stardata);
    Show_float_macro(TINY_ECCENTRICITY);
    Macrotest(OVERSPIN_EVENTS);

    Printf("TIDES is deprecated! (always on)\n");
    Macrotest(FILE_LOG);
    Macrotest(FILE_LOG_REWIND);
    Macrotest(FILE_LOG_FLUSH);
    Macrotest(FILE_LOG_COLOUR);
    Macrotest(HRDIAG);
#ifdef HRDIAG
    Printf("HRDIAG logging : start time %g, time bin size=%g, bin sizes Teff=%g logL=%g logg=%g\n",
           HRDIAG_START_LOG,
           HRDIAG_BIN_SIZE_LOGTIME,
           HRDIAG_BIN_SIZE_TEFF,
           HRDIAG_BIN_SIZE_LOGL,
           HRDIAG_BIN_SIZE_LOGG
          );
#endif

    Macrotest(PPISN);
    Show_float_macro(MIN_SEP_FOR_CLOSE_ANG_MOM_LOSS1);
    Show_float_macro(MIN_SEP_FOR_CLOSE_ANG_MOM_LOSS2);
    Show_float_macro(MIN_SEP_FOR_CLOSE_ANG_MOM_LOSS);
    Show_float_macro(NOVA_RETENTION_FRACTION_DEFAULT);
    Show_float_macro(ACCRETION_RATE_NOVAE_UPPER_LIMIT_HYDROGEN_DONOR_DEFAULT);
    Show_float_macro(ACCRETION_RATE_NEW_GIANT_ENVELOPE_LOWER_LIMIT_HYDROGEN_DONOR_DEFAULT);
    Show_float_macro(ACCRETION_RATE_NOVAE_UPPER_LIMIT_HELIUM_DONOR_DEFAULT);
    Show_float_macro(ACCRETION_RATE_NEW_GIANT_ENVELOPE_LOWER_LIMIT_HELIUM_DONOR_DEFAULT);
    Show_float_macro(ACCRETION_RATE_NOVAE_UPPER_LIMIT_OTHER_DONOR_DEFAULT);
    Show_float_macro(ACCRETION_RATE_NEW_GIANT_ENVELOPE_LOWER_LIMIT_OTHER_DONOR_DEFAULT);

    // RLOF-specific options
    Show_float_macro(NONCONSERVATIVE_ANGMOM_GAMMA_DEFAULT);
    Show_float_macro(RLOF_ENTRY_THRESHOLD);
    Show_float_macro(RLOF_OVERFLOW_THRESHOLD);
    Show_float_macro(RLOF_STABILITY_FACTOR);
    Show_float_macro(ADAPTIVE_RLOF_TARGET_FACTOR);
#ifdef ADAPTIVE_RLOF
    {
        int i = 1;
        Printf("ADAPTIVE_RLOF ADAPTIVE_RLOF_IMIN=%d ADAPTIVE_RLOF_IMAX=%d ADAPTIVE_RLOF_ALPHA=%g ADAPTIVE_RLOF_MINIMUM_RADIUS_ERROR=%g ADAPTIVE_RLOF_MAX_ENV_FRAC=%g ", ADAPTIVE_RLOF_IMIN, ADAPTIVE_RLOF_IMAX, ADAPTIVE_RLOF_ALPHA, ADAPTIVE_RLOF_MINIMUM_RADIUS_ERROR, ADAPTIVE_RLOF_MAX_ENV_FRAC);
    }
    {
        double mass = 10.0;
        Printf("[ADAPTIVE_RLOF_DM_MAX=%g for M=10 and ", ADAPTIVE_RLOF_DM_MAX);
        mass = 20.0;
        Printf("%g for M=20] ", ADAPTIVE_RLOF_DM_MAX);
    }
    Macrotest(ADAPTIVE_RLOF_CONVECTIVE_CHECK);
    Macrotest(ADAPTIVE_RLOF_LOG);
    Macrotest(RLOF_REDUCE_TIMESTEP);
    Macrotest(WTTS_LOG);
#ifdef RLOF_REDUCE_TIMESTEP
    Show_float_macro(RLOF_REDUCE_TIMESTEP_AGGRESSION);
    Show_float_macro(RLOF_REDUCE_TIMESTEP_MINIMUM_FACTOR);
    Show_float_macro(RLOF_REDUCE_TIMESTEP_THRESHOLD);
#endif
    Macrotest(SLOW_DOWN_PREROCHE_EAGB);
    Macrotest(SLOW_DOWN_PREROCHE_TPAGB);
#ifdef RLOF_STABILITY_FACTOR
    Show_float_macro(RLOF_STABILITY_FACTOR);
#endif
#ifdef RLOF_MINIMUM_TIMESTEP
    Show_float_macro(RLOF_MINIMUM_TIMESTEP);
#endif
#ifdef RLOF_MINIMUM_SPEEDUP_FACTOR
    Show_float_macro(RLOF_MINIMUM_SPEEDUP_FACTOR);
#endif

    /* print RLOF type */
    /* print RLOF stability type */
    Show_int_macro(NUMBER_OF_STELLAR_TYPES);
    Macrotest(STELLAR_TYPE_CHANGE_EVENTS);
    Macrotest(STELLAR_TYPE_CHANGE_ZOOM);
    Macrotest(ADAPTIVE_RLOF_CALC_TIMESCALES_EVERY_TIME);
    Macrotest(RLOF_MDOT_MODULATION);
    Macrotest(RLOF_NO_STELLAR_TYPE_CHANGE_IF_DTM_NEGATIVE);
    Macrotest(LIMIT_RLOF_ACCRETION_TO_ACCRETORS_THERMAL_RATE);
    Macrotest(RLOF_RADIATION_CORRECTION);
    Macrotest(STELLAR_TIMESCALES_SANITY);
    Macrotest(STELLAR_TIMESCALES_CACHE);
#ifdef RLOF_RADIATION_CORRECTION_F_DEFAULT
    Show_float_macro(RLOF_RADIATION_CORRECTION_F_DEFAULT);
#endif
#endif // ADAPTIVE_RLOF
    Show_float_macro(CHEN_HAN_FIRST_TIMESTEP_BETA);

    Macrotest(PATHOLOGICAL_RLOF_CHECK);
    Show_float_macro(MERGER_ANGULAR_MOMENTUM_FACTOR_DEFAULT);
    Show_int_macro2(CEFLAG, "(spin-energy correction in common envelope)");
    Macrotest(DISCS);
#ifdef DISCS
    Show_int_macro(DISCS_MAX_N_ZONES);
    Show_int_macro(DISCS_NUMBER_OF_POWER_LAWS);
    Show_int_macro(NDISCS);
    Show_int_macro(DISC_DEBUG);
    Macrotest(DISC_EQUATION_CHECKS);
    Show_float_macro(DISC_MAX_LIFETIME);
    Show_float_macro(DISC_MIN_TIMESTEP);
    Show_float_macro(DISC_MAX_TIMESTEP);
    Show_float_macro(DISC_STRIP_FRAC_M_INNER);
    Show_float_macro(DISC_STRIP_FRAC_J_INNER);
    Show_float_macro(DISC_STRIP_FRAC_M_OUTER);
    Show_float_macro(DISC_STRIP_FRAC_J_OUTER);
    Show_int_macro(DISC_LOSS_ISM_ALGORITHM);
    Show_float_macro(DISC_MINIMUM_TORQUE);
    Show_float_macro(DISC_MAXIMUM_TORQUE);
    Show_int_macro(DISC_ADAM_RESOLUTION);
    Show_int_macro(DISC_ROOT_FINDER_MAX_ATTEMPTS);
    Show_float_macro(DISC_MINIMUM_DISC_F);
    Show_float_macro(DISC_MINIMUM_DISC_MASS_MSUN);
    Show_float_macro(DISC_RIN_MIN);
    Show_float_macro(DISC_ROUT_MAX);
    Show_float_macro(DISC_TVISC0_MIN);
    Show_float_macro(DISC_TVISC0_MAX);
    Show_float_macro(DISC_LARGE_RADIUS);
    Show_float_macro(DISC_LONG_TIME);
    Show_float_macro(DISC_LARGE_MASS);
    Show_float_macro(DISC_MINIMUM_WIDTH);
    Show_float_macro(DISC_BISECT_RM_TOLERANCE);
    Show_int_macro(DISC_BISECT_RM_ATTEMPTS);
    Show_float_macro(DISC_BISECT_RJ_TOLERANCE);
    Show_int_macro(DISC_BISECT_RJ_ATTEMPTS);
    Show_float_macro(DISC_PRESSURE_RADIUS_TOLERANCE);
    Show_float_macro(DISC_EDGE_EPS);
    Show_float_macro(DISC_TOLERANCE);
    Show_float_macro(DISC_MASS_TOLERANCE);
    Show_int_macro(DISC_BISECTION_MASS_MAX_ITERATIONS);
    Show_float_macro(DISC_ANGMOM_TOLERANCE);
    Show_int_macro(DISC_BISECTION_ANGMOM_MAX_ITERATIONS);
    Show_float_macro(DISC_ANGMOM_FLUX_TOLERANCE);
    Show_int_macro(DISC_BISECTION_ANGMOM_FLUX_MAX_ITERATIONS);
    Show_float_macro(DISC_TORQUEF_TOLERANCE);
    Show_int_macro(DISC_BISECTION_TORQUEF_MAX_ITERATIONS);
    Show_float_macro(DISC_TIMESTEP_LOW_FACTOR);
    Show_float_macro(DISC_TIMESTEP_HIGH_FACTOR);
    Show_float_macro(DISC_MINIMUM_EVAPORATION_TIMESCALE);
    Macrotest(DISC_MONTE_CARLO_GUESSES_USE_LOG);
    Show_int_macro(DISC_BISECT_USELOG);
    Show_int_macro(DISC_BISECT_OWEN_RADIUS_USELOG);
    Show_int_macro(DISC_BISECT_RJ_USELOG);
    Show_int_macro(DISC_BISECT_RM_USELOG);
    Show_int_macro(DISC_BISECT_PRESSURE_RADIUS_USELOG);
    Show_int_macro(DISC_BISECT_VISCOUS_USELOG);

#endif
    Macrotest(DISCS_CIRCUMBINARY_FROM_COMENV);
    Macrotest(DISCS_COMENV_APPEND);
    Macrotest(CBDISC_ECCENTRICITY_PUMPING);
    Macrotest(DISC_EVAPORATE_ON_FAILURE);
    Macrotest(__HAVE_LIBSPINDLER__);

    Macrotest(RESOLVE_POSTAGB);
    Macrotest(DISC_LOG_POPSYN);
    Macrotest(DISC_LOG);
    Macrotest(DISC_LOG_2D);
    Macrotest(DISC_SAVE_EPSILONS);
    Macrotest(DISC_RESIDUAL_WARNING);
#ifdef DISC_RESIDUAL_WARNING
    Show_float_macro(DISC_RESIDUAL_WARNING_THRESHOLD);
#endif
    Show_float_macro(SN_SIGMA_DEFAULT);
    Macrotest(EVOLUTION_SPLITTING);
    Show_float_macro(WD_SIGMA_DEFAULT);
    Show_float_macro(REIMERS_ETA_DEFAULT);
    Macrotest(VW93_MIRA_SHIFT);
    Macrotest(VW93_MULTIPLIER);
    Show_float_macro2(CRAP_PARAMETER_DEFAULT, "(Enhanced RLOF wind loss)");
    Show_float_macro(LUM0);
    Show_float_macro(KAP);
    Show_float_macro(AHE);
    Show_float_macro(ACO);
    Show_float_macro(TAUMIN);
    Show_float_macro(MLP);
    Show_float_macro(BONDI_HOYLE_ACCRETION_FACTOR_DEFAULT);
    Show_float_macro(DTFAC_DEFAULT);
    Show_float_macro(MINIMUM_TIMESTEP_DEFAULT);
    Show_float_macro(MAXIMUM_STELLAR_MASS);
    Macrotest(MINIMUM_STELLAR_MASS);
#ifdef MINIMUM_STELLAR_MASS
    Show_float_macro(MINIMUM_STELLAR_MASS);
#endif
    Macrotest(STRIP_SUPERMASSIVE_STARS);
    Show_float_macro(BINARY_C_MINIMUM_STELLAR_MASS);
    Show_float_macro(BINARY_C_MAXIMUM_STELLAR_MASS);

    Macrotest(OMEGA_CORE);

    Show_float_macro2(HeWD_HeWD_IGNITION_MASS,
                      "more massive HeWDs with reignite helium on accretion and/or merger");
    Macrotestif2(ALLOW_HeWD_SUPERNOVAE,
                 "HeWDs exlpode as 'HeIa' supernovae (ALLOW_HeWD_SUPERNOVAE defined)");
    Macrotestif2(ALLOW_HeWD_SUPERNOVAE,
                 "HeWDs do not explode (ALLOW_HeWD_SUPERNOVAE not defined)\n");
    Show_float_macro(MASS_ACCRETION_FOR_DDet_DEFAULT);
    Macrotest(MILLISECOND_RANDOMNESS);
    Macrotest(NANOSECOND_RANDOMNESS);
    Macrotest(RANDOM_SYSTEMS);
    Macrotest(RANDOM_SYSTEMS_SHOW_ARGS);
    Macrotest(RANDOM_SYSTEMS_SHOW_ARGS_AND_START_TIME);
    Show_string_macro(RANDOM_SYSTEMS_SHOW_ARGS_STREAM);
    Macrotest(USE_DRAND48_R);
    Macrotest(USE_MERSENNE_TWISTER);
    Printf("Random seed format \"%s\", cast \"%s\"\n",
           Random_seed_format,
           Stringify_macro(Random_seed_cast));

    /*
     * Simple random number test.
     *
     * Of course this is not a robust test, but it detects
     * a basic failure of the routine.
     */
    if(stardata)
    {
        static const int maxn = 10000;
        int n = maxn;
        double sum = 0.0;
        struct stardata_t * s = new_stardata(stardata->preferences);
        if(s)
        {
            s->common.random_seed = random_seed();
            while (n-- > 0)
            {
                sum += random_number(s, &s->common.random_seed);
            }
            sum /= (double)(maxn + 0.1);
            Printf("Random number mean %g\n", sum);
            Safe_free_nocheck(s);
        }
    }

    Macrotest(RANDOM_SEED_LOG);
#ifdef RANDOM_SEED_LOG
    Show_string_macro(RANDOM_SEED_LOG_STREAM);
#endif
    Macrotest(COMENV_LOG);
    Macrotest(GIANT_AGE_FATAL_ERRORS);
    Macrotest(DTR_CHECKS);
    Macrotest(ACQUIRE_FIX_NEGATIVE_AGES);
    Macrotest(ACQUIRE_TEST_FOR_NEGATIVE_AGES);
    Show_float_macro(ACCRETION_LIMIT_EDDINGTON_MULTIPLIER_STEADY_DEFAULT);
    Show_float_macro(ACCRETION_LIMIT_EDDINGTON_MULTIPLIER_WD_TO_REMNANT_DEFAULT);
    Show_float_macro(ACCRETION_LIMIT_EDDINGTON_MULTIPLIER_LMMS_DEFAULT);
    Show_int_macro(MAX_NUMBER_OF_COMENVS);
    Show_int_macro(COMENV_BSE_AFTER);
    Show_float_macro(DEFAULT_LAMBDA_IONISATION);
    Show_float_macro(DEFAULT_LAMBDA_CE);
    Show_float_macro(DEFAULT_ALPHA_CE);
    Show_float_macro(DEFAULT_COMENV_PRESCRIPTION);
    Show_float_macro(DEFAULT_NELEMANS_GAMMA);
    Show_float_macro(NELEMANS_MIN_Q_DEFAULT);
    Show_float_macro(NELEMANS_MAX_FRAC_J_CHANGE_DEFAULT);
    Show_float_macro(NELEMANS_N_COMENVS_DEFAULT);
    Show_float_macro(MAX_CONVECTIVE_MASS);
    Show_float_macro(MIN_CONVECTIVE_MASS);

    Macrotest(ANG_MOM_CHECKS);
    Macrotest(NO_IMMEDIATE_MERGERS);

    /************************************************************/
    Macrotest(NUCSYN);
#ifdef NUCSYN
    Show_int_macro(ISOTOPE_ARRAY_SIZE);
    Macrotest(NUCSYN_ALL_ISOTOPES);
    Macrotest(NUCSYN_STRIDED_ISOTOPE_LOOPS);
    Show_float_macro(MR23YR);

    if(stardata != NULL)
    {
        /*
         * List isotopes and elements, and test timing of loops
         */
        const long int nms = __test_milliseconds();
        {
            static char * elnames[] = NUCSYN_SHORT_ELEMENT_STRINGS;

            {
                unsigned long int forward = 0;
                unsigned long int reverse = 0;
                if(stardata != NULL &&
                   stardata->preferences->speedtests == TRUE)
                {

                    const ticks start_tick = getticks();
                    while (Timer_microseconds(getticks() - start_tick) < (nms * 1e3))
                    {
                        Forward_isotope_loop(i)
                        {
                            forward++;
                        }
                    }
                    Printf("forward-ordered isotope loop nop/s : %5.2f x10^6 j = %lu\n",
                           (float)forward / ((float)(nms * 1e3)),
                           forward);
                }
                else
                {
                    Forward_isotope_loop(i)
                    {
                    }
                }

                if(stardata != NULL && stardata->preferences->speedtests == TRUE)
                {
                    const ticks start_tick = getticks();
                    while (Timer_microseconds(getticks() - start_tick) < (nms * 1e3))
                    {
                        Reverse_isotope_loop(i)
                        {
                            reverse++;
                        }
                    }
                    Printf("reverse-ordered isotope loop nop/s : %5.2f x10^6 j = %lu\n",
                           (float)reverse / ((float)(nms * 1e3)),
                           reverse);
                }
                else
                {
                    Isotope_loop(i)
                    {
                    }
                }

                if(stardata != NULL && stardata->preferences->speedtests == TRUE)
                {
                    Printf("%s is faster by %5.2f%%\n",
                           reverse > forward ? "reverse" : "forward",
                           reverse > forward ?
                           (1.0 + (double)reverse / (double)forward) :
                           (1.0 + (double)forward / (double)reverse));
                }
            }

            /*
             * Show isotopes
             */
            Printf("Isotopes are : \n");
            for (Isotope i = 0; i < ISOTOPE_ARRAY_SIZE; i++)
            {
                Printf("Isotope %u is %s (mass=%20.12e g (/amu=%20.12e, /MeV=%20.12e), Z=%d, A=%d)\n",
                       i,
                       stardata->store->isotope_strings[i],
                       stardata->store->mnuc[i],
                       stardata->store->mnuc[i] / AMU_GRAMS,
                       stardata->store->mnuc[i]*Pow2(SPEED_OF_LIGHT) / MEGA_ELECTRON_VOLT,
                       (int)stardata->store->atomic_number[i],
                       stardata->store->nucleon_number[i]
                      );
            }

            /*
             * show elements
             */
            Element Z;
            Boolean alloced = FALSE;
            if(stardata->store->icache == NULL)
            {
                nucsyn_build_store_contents(stardata->store);
            }
            for (Z = 0; Z < NUMBER_OF_ELEMENTS; Z++)
            {
                Printf("Element %u is %s : %u isotopes %s ",
                       Z,
                       elnames[Z],
                       stardata->store->nisotopes[Z],
                       (stardata->store->nisotopes[Z] == 0 ? "" : "=")
                      );
                unsigned int c;
                for (c = 0; c < stardata->store->nisotopes[Z]; c++)
                {
                    const Isotope nisotope = stardata->store->icache[Z][c];
                    Printf("%u=%s%d ",
                           nisotope,
                           elnames[Z],
                           stardata->store->nucleon_number[nisotope]
                          );
                }
                Printf("\n");
            }
            if(alloced == TRUE)
            {
                nucsyn_free_store_contents(stardata->store);
            }
        }
    }
    else
    {
        Printf("No isotope information because stardata is NULL\n");
    }


    Macrotestif2(NUCSYN_STRIP_AND_MIX,
                 "Main sequence stripping (i.e. CN in Algols, case A/B RLOF)");
    Macrotest(NUCSYN_FIRST_DREDGE_UP);
    Macrotest(NUCSYN_FIRST_DREDGE_UP_AMANDAS_TABLE);
    Macrotest2(NUCSYN_FIRST_DREDGE_UP_RICHARDS_TABLE,
               "Richard Stancliffe's table in preference to Amanda's when available\n");
    Macrotest(NUCSYN_PREGUESS_FIRST_DREDGE_UP);
    Macrotest(NUCSYN_FIRST_DREDGE_UP_ACCRETION_CORRECTION);
    Macrotest(NUCSYN_FIRST_DREDGE_UP_ACCRETION_CORRECTION_FROM_TAMS);
    Macrotest(NUCSYN_FIRST_DREDGE_UP_PHASE_IN);
    Macrotest(NUCSYN_FORCE_DUP_IN_MERGERS);
    Macrotest(NUCSYN_SECOND_DREDGE_UP);
    Macrotest(NUCSYN_TPAGB);
    Show_int_macro(TPAGB_LUMTYPE);
    Show_float_macro(PULSE_LUM_DROP_N_PULSES);
    Show_float_macro(THERMAL_PULSE_LUM_DROP_FACTOR);
    Show_float_macro(THERMAL_PULSE_LUM_DROP_TIMESCALE);
    Show_float_macro(KARAKAS2002_TPAGB_MIN_LUMINOSITY);
    Macrotest(PADOVA_MC1TP);
    Macrotest(NUCSYN_THIRD_DREDGE_UP);
    Macrotest(THIRD_DREDGE_UP_KARAKAS2002);
    Macrotest(NUCSYN_THIRD_DREDGE_UP_TABULAR_NCAL);
    Macrotest(THIRD_DREDGE_UP_STANCLIFFE);
    Show_float_macro(MINIMUM_ENVELOPE_MASS_FOR_THIRD_DREDGEUP_DEFAULT);
    Macrotest(NUCSYN_HUGE_PULSE_CRISTALLO);
    Macrotest(USE_TABULAR_INTERSHELL_ABUNDANCES_KARAKAS_2002);
    Macrotest(USE_TABULAR_INTERSHELL_ABUNDANCES_KARAKAS_2012);
    Macrotest(NUCSYN_HS_LS_LOG);
    Macrotest(NUCSYN_THIRD_DREDGE_UP_MULTIPLIERS);
    Macrotest(NUCSYN_BURN_TDUP_MATERIAL);
    Macrotest(NUCSYN_THIRD_DREDGE_UP_HYDROGEN_SHELL);
    Macrotest(KARAKAS2002_REFITTED_TPAGB_INTERPULSES);
    Show_float_macro(MAX_TPAGB_TIME);
    Show_float_macro(MINIMUM_INTERPULSE_PERIOD_MYR);
    Macrotest(NUCSYN_TPAGB_RUNTIME);
    Macrotest(NUCSYN_ANAL_BURN);
    Macrotest(NUCSYN_NUMERICAL_BURN);

    /*
     * Disable -Waddress warnings, otherwise gcc-5 would
     * say that function pointers are always non-NULL... which
     * is true, but annoying.
     */
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Waddress"
    {
        int nnet = 0;
#undef X
#define X(CODE,ONOFF,STRING,KAPS_RENTROP,LSODA,CVODE,CHECKFUNC)     \
        Printf("Network %d==%s is %s : implemented in %s%s%s (checkfunc %s)\n",    \
               nnet++,                                              \
               #CODE,                                               \
               STRING,                                              \
               KAPS_RENTROP!=NULL ? "Kaps-Rentrop " : "",           \
               LSODA!=NULL ? "LSODA " : "",                         \
               CVODE!=NULL ? "CVODE " : "",                         \
               CHECKFUNC!=NULL ? #CHECKFUNC : "none"                \
            );
        NUCSYN_NETWORKS_LIST;
#undef X
    }
#pragma GCC diagnostic pop

    Show_float_macro(NUCSYN_NETWORK_BURN_MAX_FAILURE_COUNT);
    Macrotest(NUCSYN_TPAGB_HBB);
    Show_float_macro(HBBTFAC_DEFAULT);
    Macrotest(NUCSYN_HBB_RENORMALIZE_MASS_FRACTIONS);
    Macrotest(NUCSYN_NORMALIZE_NUCLEONS);
    Macrotest2(RATES_OF_AMANDA,
               "Using Amanda's nuclear reaction rates");
    Macrotest2(RATES_OF_AMANDA_NE22PG_FIX, "with Ne22(pg) fix");
    Macrotest2(RATES_OF_MARIA, "Using Maria's nuclear reaction rates\n");
    Macrotest2(RATES_OF_AREND_JAN, "Using Arend Jan's nuclear reaction rates\n");
    Macrotest(NUCSYN_SIGMAV_PRE_INTERPOLATE);
#ifdef NUCSYN_SIGMAV_PRE_INTERPOLATE
    Show_int_macro(NUCSYN_SIGMAV_INTERPOLATION_RESOLUTION);
    Show_float_macro(NUCSYN_SIGMAV_INTERPOLATION_MIN);
    Show_float_macro(NUCSYN_SIGMAV_INTERPOLATION_MID);
    Macrotestif2(NUCSYN_SIGMAV_INTERPOLATE_LOGT9, "interpolate log T9");
    Macrotestifnot2(NUCSYN_SIGMAV_INTERPOLATE_LOGT9, "interpolate linear T9");
    Macrotestif2(NUCSYN_SIGMAV_INTERPOLATE_LOGSIGMAV, "interpolate log sigmav");
    Macrotestifnot2(NUCSYN_SIGMAV_INTERPOLATE_LOGSIGMAV, "interpolate linear sigmav");
#endif
#include "../nucsyn/nucsyn_sigmav.h"
    Show_enum(SIGMAV_SIZE);
    Show_float_macro(SIGMAV_TINY);
    Macrotest(NUCSYN_HOT_SIGMAV);
    Macrotest(NUCSYN_THERMALIZED_CORRECTIONS);
    Macrotest(HELIUM_BURNING_REACTIONS);
    Macrotest(NUCSYN_S_PROCESS_LOG);

    Macrotest(NUCSYN_LITHIUM);
    Macrotest(NUCSYN_LITHIUM_TABLES);
    Macrotest(NUCSYN_ANGELOU_LITHIUM);
#ifdef NUCSYN_ANGELOU_LITHIUM
    Show_float_macro(NUCSYN_ANGELOU_LITHIUM_MINIMUM_Li7);
    Show_float_macro(NUCSYN_ANGELOU_LITHIUM_MAX_TIMESTEP_FACTOR);
#endif
    Macrotest(NUCSYN_RADIOACTIVE_DECAY);
    Macrotest(NUCSYN_WR);
    Show_float_macro(NUCSYN_WR_MASS_BREAK);
    Macrotest2(NUCSYN_WR_TABLES,
               "interpolate from Lynnette's tables");
    Macrotest2(NUCSYN_WR_RS_TABLE,
               "interpolate from Richard's tables ");
    Macrotest2(NUCSYN_WR_METALLICITIY_CORRECTIONS,
               "with metallicity corrections (if non-tabular) ");
    Macrotest2(NUCSYN_WR_ACCRETION,
               "with NUCSYN_WR_ACCRETION (if tabular) ");
    Macrotest(NUCSYN_LOWZ_SUPERNOVAE);
#ifdef NUCSYN_LOWZ_SNE_THRESHOLD
    Show_float_macro(NUCSYN_LOWZ_SNE_THRESHOLD);
#endif

    for (int i_sn = 0; i_sn < NUM_SN_STRINGS; i_sn++)
    {
        Printf("Supernova type %s = %d, argument \"%s\"\n",
               supernova_type_strings[i_sn],
               i_sn,
               supernova_cmdline_strings[i_sn]);
    }
    Macrotest(NUCSYN_CCSNE_CHIEFFI_LIMONGI_2004_PORTINARI);
    Macrotest(NUCSYN_CCSNE_CHIEFFI_LIMONGI_2004_EXTRAPOLATE);
    Macrotest(NUCSYN_CCSNE_CHIEFFI_LIMONGI_ELEMENTAL_Na);
    Macrotest(NUCSYN_LIMONGI_CHIEFFI_2006_Al26);
    Macrotest(NUCSYN_SN_ELECTRON_CAPTURE_WANAJO_2008);
    Macrotest(NUCSYN_SN_ELECTRON_CAPTURE_WANAJO_2008_ST);
    Macrotest(NUCSYN_SN_ELECTRON_CAPTURE_WANAJO_2008_FP3);
    Macrotest(NUCSYN_HETEROGENEOUS_HE_STAR);
    Macrotest(NUCSYN_NOVAE);
    Macrotest(COMENV_NORMALIZE_ABUNDS);
    Macrotest(NUCSYN_LOGGING);
    Macrotest(NUCSYN_YIELDS);
    Macrotest(NUCSYN_LOG_YIELDS);
    Macrotest(NUCSYN_SPARSE_YIELDS);
    Macrotest(NUCSYN_YIELDS_COMPRESSION);
    Macrotest(NUCSYN_YIELDS_VS_TIME);
    Macrotest(NUCSYN_YIELD_COMPRESSION);
    Macrotest(NUCSYN_COMRPESS_YIELD_AND_ENSEMBLE_OUTPUT);
    Macrotest(NUCSYN_LOG_BINARY_X_YIELDS);
    Macrotest(NUCSYN_LOG_BINARY_MPYIELDS);
    Macrotest(NUCSYN_LOG_BINARY_DX_YIELDS);
    Macrotest(NUCSYN_LOG_INDIVIDUAL_DX_YIELDS);
    Macrotest(NUCSYN_LOG_MPYIELDS);
    Macrotest(NUCSYN_LOG_SINGLE_X_YIELDS);
    Macrotest(NUCSYN_IGNORE_ZERO_BINARY_DX_YIELD);
    Macrotest(NUCSYN_LOG_INDIVIDUAL_DX_YIELDS);
    Macrotest(NUCSYN_LOG_YIELDS_EVERY_TIMESTEP);
    Macrotest(NUCSYN_LONG_YIELD_FORMAT);
    Macrotest(NUCSYN_ABUNDANCE_LOG);
    Macrotest(NUCSYN_GCE);
    Macrotest(NUCSYN_GCE_OUTFLOW_CHECKS);
#ifdef NUCSYN_GCE_OUTFLOW_CHECKS
    Show_float_macro(NUCSYN_GCE_OUTFLOW_FACTOR);
    Show_float_macro(NUCSYN_GCE_SUPERNOVA_OUTFLOW_VELOCITY);
    Show_float_macro(NUCSYN_GCE_OUTFLOW_ESCAPE_FRACTION);
#endif

    Macrotest(NUCSYN_ID_SOURCES);
    Macrotest(NUCSYN_ID_SOURCES_GCE);
    Show_enum(SOURCE_NUMBER);
    {
        Sourceloop(j)
        {
            Printf("Nucleosynthesis source %d is %s\n",
                   j,
                   Source_string(j));
        }
    }

    Macrotest(NUCSYN_XTOT_CHECKS);
    Macrotest(NUCSYN_STRUCTURE_LOG);
    Macrotest(NUCSN_MU_FUZZ);
#ifdef NUCSYN_MU_FUZZ
    Show_float_macro(NUCSYN_MU_FUZZ);
#endif
    Macrotest(NUCSYN_CF_AMANDA_LOG);
    Macrotest(NUCSYN_SHORT_LOG);
    Macrotest(NUCSYN_LONG_LOG);
    Macrotest(NUCSYN_GIANT_ABUNDS_LOG);
    Macrotest(NUCSYN_R_STAR_LOG);
    Macrotest(NUCSYN_MERGER_DREDGEUP);
    Macrotest(NUCSYN_PLANETARY_NEBULAE);
    Macrotest(LOG_BARIUM_STARS);
    Macrotest(NUCSYN_J_LOG);
    Macrotest(NUCSYN_Globular_Cluster_LOG);
    Macrotest(NUCSYN_LOG_JL);
    Macrotest(NO_MERGER_NUCLEOSYNTHESIS);
    Macrotest(TPAGB_SPEEDUP);
#ifdef TPAGB_SPEEDUP
    Show_float_macro(TPAGB_SPEEDUP_AFTER);
#endif  //TPAGB_SPEEDUP
    Macrotest(ZAMS_MENV_METALLICITY_CORRECTION);
    Macrotest(NUC_MASSES_DEBUG);
#ifdef CEMP_VERSION
    Printf("Equivalent CEMP version %s (code version %s)\n",
           CEMP_VERSION, BINARY_C_VERSION);
#endif
#endif //NUCSYN
    Macrotest(STELLAR_POPULATIONS_ENSEMBLE);
#ifdef STELLAR_POPULATIONS_ENSEMBLE
    Show_enum(ENSEMBLE_NUMBER);
#endif
    Macrotest(STELLAR_POPULATIONS_ENSEMBLE_SPARSE);
    Show_enum(STELLAR_POPULATIONS_ENSEMBLE_FILTER_NUMBER);
#ifdef STELLAR_POPULATIONS_ENSEMBLE
    {
        unsigned int i = 0;
#undef X
#define X(FILTER) Printf("Ensemble filter %u is %s\n",  \
                         i++,                           \
                         Stringize(FILTER));
        STELLAR_POPULATIONS_ENSEMBLE_FILTER_LIST;
#undef X
        for (i = 0; i < ENSEMBLE_NUMBER; i++)
        {
            Printf("Ensemble %u is %s\n",
                   i,
                   ensemble_1D_strings[i]);
        }
    }
#endif
    Show_enum(BINARY_C_TASK_NUMBER);


    Show_long_int_macro(BUFFERED_PRINTF_MAX_BUFFER_SIZE);
    Show_long_int_macro(BUFFERED_PRINTF_ERROR_BUFFER_SIZE);
    Macrotest(BUFFER_MEMORY_DEBUGGING);
    Show_string_macro(BUFFER_MEMORY_DEBUGGING_STREAM);
    Macrotest(BUFFERED_STRING_OVERRUN_WARNINGS);

    Show_int_macro(STRING_LENGTH);
    Show_int_macro(BATCHMODE_STRING_LENGTH);

    Show_string_macro(BUFFERED_STRING_OVERRUN_WARNINGS_STREAM);
    Show_float_macro(BUFFERED_PRINTF_INCREASE_RATIO);

    Show_float_macro(EVOLUTION_DIFFLOG_INCREASE_RATIO);

    Macrotest(__SHOW_STARDATA__);
    Macrotest(SHOW_STARDATA);

    Show_float_macro(FUV_eV_MIN);
    Show_float_macro(FUV_eV_MAX);
    Show_float_macro(EUV_eV_MIN);
    Show_float_macro(EUV_eV_MAX);
    Show_float_macro(XRAY_eV_MIN);
    Show_float_macro(XRAY_eV_MAX);

    Macrotest(SHORT_SUPERNOVA_LOG);
    Macrotest(BINARY_STARS_ONLY);
    Macrotest(ZW2019_SNIA_LOG);
    Macrotest(CGI_EXTENSIONS);
    Macrotest(SDB_CHECKS);
    Macrotest(BI_LYN);
    Macrotest(XRAY_BINARIES);
    Macrotest(STELLAR_TYPE_LOG);
    Macrotest(STELLAR_COLOURS);
    Macrotest(YBC);
    Macrotest2(LOG_JJE,
               "JJE (Eldridge) log activated");
    Macrotestif2(SELMA,
                 "Selma's logging activated");
    Macrotestif2(FORM_STARS_AT_BREAKUP_ROTATION,
                 "Form stars at breakup rotation rate");
    Macrotestif2(AXEL_RGBF_FIX,
                 "Use Axel's RGB radius function fix");
    Printf("Comenv accretion: ");
#ifdef COMENV_NS_ACCRETION
    Printf("NS : default fraction %g, default mass %g ... ",
           COMENV_NS_ACCRETION_FRACTION_DEFAULT,
           COMENV_NS_ACCRETION_MASS_DEFAULT);
#endif
#ifdef COMENV_MS_ACCRETION
    Printf("MS: default mass %g ",
           COMENV_MS_ACCRETION_MASS_DEFAULT);
#endif
    Printf("\n");
    Macrotestif2(NS_BH_AIC_LOG,
                 "NS/BH AIC log enabled");
    Macrotestif2(NS_NS_BIRTH_LOG,
                 "NS/NS birth log enabled");
    Macrotest(RS_LOG_ACCREITON_LOG);
    Macrotest(DETMERS_LOG);
    Macrotest(LOG_HERBERT);
    Macrotestif2(LOG_COMENV_RSTARS,
                 "Log comenv R-star formation enabled");
    Macrotestif2(HALL_TOUT_2014_RADII,
                 "Using core radii from Hall and Tout 2014");
#ifdef HRDIAG
    Printf("HRDIAG logging (for Peter Anders) is on: HRDIAG_BIN_SIZE_LOGTIME=%g HRDIAG_START_LOG=%g HRDIAG_BIN_SIZE_TEFF=%g HRDIAG_BIN_SIZE_LOGL=%g HRDIAG_BIN_SIZE_LOGG=%g\n",
           HRDIAG_BIN_SIZE_LOGTIME,
           HRDIAG_START_LOG,
           HRDIAG_BIN_SIZE_TEFF,
           HRDIAG_BIN_SIZE_LOGL,
           HRDIAG_BIN_SIZE_LOGG);
    Macrotest(HRDIAG_EXTRA_RESOLUTION_AT_END_AGB);
#endif//HRDIAG

    Macrotest(FINAL_MASSES_LOG);
    Macrotest(HeWD_HeCORE_MERGER_INITION);
    Macrotest(LOG_RSTARS);
    Macrotest2(RSTARS, "RSTARS main switch is on");
    Macrotest2(MESTEL_COOLING, "WD Mestel cooling");
    Macrotest2(MODIFIED_MESTEL_COOLING, "WD Modified Mestel cooling");
    Macrotest(COMENV_CO_MERGER_GIVES_IIa);
    Macrotestif2(COMENV_CO_MERGER_GIVES_IIa,
                 "Comenv COWD-COWD mergers give SN IIa");
    Macrotestifnot2(COMENV_CO_MERGER_GIVES_IIa,
                    "Comenv COWD-COWD mergers give electron-capture SN");
    Macrotest(NO_HeWD_NOVAE);
    Show_float_macro(He_REIGNITION_LAYER_THICKNESS);
    Macrotest(STARDATA_STATUS);
    Macrotest(ORBITING_OBJECTS);

    Macrotest(FABIAN_IMF_LOG);
#ifdef FABIAN_IMF_LOG
    Show_float_macro(NEXT_FABIAN_IMF_LOG_TIME_DEFAULT);
    Show_float_macro(FABIAN_IMF_LOG_TIMESTEP_DEFAULT);
#endif
    Macrotest(TIMESTEP_MODULATION);
    Macrotest(RLOF_TIMESTEP_MODULATION);

    if(stardata)
    {
        int i;
        for (i = 0; i < DT_LIMIT_NUMBER; i++)
        {
            Printf("DTlimit %d : %s : %g\n",
                   i,
                   timestep_limiter_strings[i],
                   stardata->preferences->timestep_multipliers[i]);
        }
    }

    Macrotestif(STOP_ACCRETION_OF_MATERIAL_BEYOND_BREAKUP,
                "Material is not accreted beyond breakup according to Hurley et al algorithm");
    Macrotest(SELMA_EXTRA_FEATURES);
    Macrotest(SELMA_FIX_ACCRETED_MASS_IN_REJUVENATION);
    Macrotest(SELMA_BETTER_TREATMENT_OF_MS_ACCRETORS);
    Show_float_macro(MIXINGPAR_MS_MERGERS_DEFAULT);
    Macrotest(SELMA_NEW_QCRIT);
#ifdef SELMA_NEW_QCRIT
    Show_float_macro(QCRIT_ON_MAIN_SEQUENCE_DEFAULT);
    Show_float_macro(QCRIT_HERTZSPRUNG_GAP_DEFAULT);
    Show_float_macro(QCRIT_POST_MS_NAKED_HE_STAR_DEFAULT);
    Show_float_macro(QCRIT_ELSE_DEFAULT);
#endif

    Macrotest(HE_STAR_ACCRETING_H_FIX);
    Macrotest(NOVA_BACK_ACCRETION);

    Show_float_macro(POST_CE_ENVELOPE_DM_GB);
    Show_float_macro(POST_CE_ENVELOPE_DM_EAGB);
    Show_float_macro(POST_CE_ENVELOPE_DM_TPAGB);

#undef X
#define X(TYPE) Printf("Wind MDOT_%s = %d\n",#TYPE,_i++);
    {
        int _i = 0;
        WIND_MDOTS_LIST;
    }
#undef X
    Macrotest2(AGB_ROTATIONAL_WIND_ENHANCEMENT_DS06, "(Experiental rotational wind enhancement of Dijkstra & Speck 2006)");
    Macrotest2(AGB_ROTATIONAL_WIND_ENHANCEMENT_MOE, "(Experiental rotational wind enhancement of Moe)");
    Macrotest2(WRLOF_MASS_TRANSFER, "(Carlo Abate's Wind-RLOF)");
    Macrotest(FORCE_SINGLE_STAR_EVOLUTION_WHEN_SECONDARY_IS_A_PLANET);
    Macrotest(CN_THICK_DISC);

#ifdef EVOLUTION_SPLITTING
    Printf("EVOLUTION_SPLITTING is on : EVOLUTION_SPLITTING_HARD_MAX_DEPTH=%d EVOLUTION_SPLITTING_MAX_SPLITDEPTH_DEFAULT=%d EVOLUTION_SPLITTING_SUPERNOVA_N=%d\n",
           EVOLUTION_SPLITTING_HARD_MAX_DEPTH,
           EVOLUTION_SPLITTING_MAX_SPLITDEPTH_DEFAULT,
           EVOLUTION_SPLITTING_SUPERNOVA_N);
#else
    Printf("EVOLUTION_SPLITTING is off\n");
#endif

    Macrotest(BSE);
    Macrotest(MINT);

    Macrotest(BSE_EMULATION);
    Macrotest(MAKE_BSE_TABLES);
    Macrotest(USE_BSE_TABLES);
    Macrotest(UNIT_TESTS);
    Macrotest(REJUVENATE_HERZTSPRUNG_GAP_STARS);
    Macrotest(UNIT_TESTS);

    Show_float_macro(MIN_MASS_He_FOR_HYBRID_COWD);
    Macrotest(LOG_HYBRID_WHITE_DWARFS);

    Show_float_macro(ACCRETION_LIMIT_H_ENV_GB);
    Show_float_macro(ACCRETION_LIMIT_H_ENV_CHeB);

    /* opacity tables */
    Macrotest(OPACITY_ALGORITHMS);
    Macrotest(OPACITY_ENABLE_ALGORITHM_PACZYNSKI);
    Macrotest(OPACITY_ENABLE_ALGORITHM_FERGUSON_OPAL);
    Macrotest(OPACITY_ENABLE_ALGORITHM_STARS);

    /* mathematics */
    Macrotest(BISECT_FAIL_EXIT);
    Macrotest(BISECT_DO_MONOCHECKS);
    Macrotest(MIN_MAX_ARE_FUNCTIONS);
    Macrotest(MIN_MAX_ARE_MACROS);
    Macrotest(USE_FABS);
    Macrotest(ZERO_CHECKS_ARE_FUNCTIONS);
    Macrotest(POWER_OPERATIONS_ARE_FUNCTIONS);

    /* derivatives */
    for (int d = 0; d < DERIVATIVE_STELLAR_NUMBER; d++)
    {
        Printf("Stellar derivative %d is %s\n",
               d,
               Stellar_derivative_string(d));
    }
    for (int d = 0; d < DERIVATIVE_SYSTEM_NUMBER; d++)
    {
        Printf("System derivative %d is %s\n",
               d,
               System_derivative_string(d));
    }

    {
        int _n = 0;
#undef X
#define X(C) Printf("Timestep multiplier %d is %s\n",   \
                    _n++,                               \
                    #C);
        TIMESTEP_LIMITERS_LIST;
    }

    /* constants */
    Show_physical_constants(SPEED_OF_LIGHT);
    Show_physical_constants(GRAVITATIONAL_CONSTANT);
    Show_physical_constants(PLANCK_CONSTANT);
    Show_physical_constants(BOLTZMANN_CONSTANT);
    Show_physical_constants(ELECTRON_CHARGE);
    Show_float_macro(AVOGADRO_CONSTANT);
    Show_physical_constants(R_SUN);
    Show_physical_constants(ASTRONOMICAL_UNIT);
    Show_float_macro(DAY_LENGTH_IN_SECONDS);
    Show_physical_constants(L_SUN);
    Show_physical_constants(M_SUN);
    Show_physical_constants(R_SUN);
    Show_physical_constants(M_MERCURY);
    Show_physical_constants(M_VENUS);
    Show_physical_constants(M_EARTH);
    Show_physical_constants(M_MARS);
    Show_physical_constants(M_JUPITER);
    Show_physical_constants(M_SATURN);
    Show_physical_constants(M_URANUS);
    Show_physical_constants(M_NEPTUNE);
    Show_physical_constants(M_PLUTO);
    Show_physical_constants(A_MERCURY);
    Show_physical_constants(A_VENUS);
    Show_physical_constants(A_EARTH);
    Show_physical_constants(A_MARS);
    Show_physical_constants(A_JUPITER);
    Show_physical_constants(A_SATURN);
    Show_physical_constants(A_URANUS);
    Show_physical_constants(A_NEPTUNE);
    Show_physical_constants(A_PLUTO);
    Show_physical_constants(M_PROTON);
    Show_physical_constants(M_ELECTRON);
    Show_physical_constants(M_NEUTRON);
    Show_float_macro(J_ORBITAL_EARTH);
    Show_float_macro(J_ORBITAL_JUPITER);
    Show_float_macro(J_SPIN_EARTH);
    Show_float_macro(J_SPIN_JUPITER);
    Show_float_macro(SOLAR_APPARENT_MAGNITUDE);
    Show_float_macro(CMB_TEMPERATURE);
    Show_physical_constants(BOLOMETRIC_L0);
    Show_physical_constants(PARSEC);
    Show_physical_constants(KILOPARSEC);
    Show_physical_constants(MEGAPARSEC);
    Macrotest(LIKE_ASTROPY);

    /* derived constants */
    Show_physical_constants(ELECTRON_VOLT);
    Show_physical_constants(MEGA_ELECTRON_VOLT);
    Show_float_macro(AMU_GRAMS);
    Show_float_macro(AMU_MEV);
    Show_physical_constants(RYDBERG_ENERGY);
    Show_float_macro(YEAR_LENGTH_IN_SECONDS);
    Show_float_macro(YEAR_LENGTH_IN_DAYS);
    Show_float_macro(YEAR_LENGTH_IN_HOURS);
    Show_float_macro(R_SUN_KM);
    Show_float_macro(AU_IN_SOLAR_RADII);
    Show_physical_constants(PLANCK_CONSTANT_BAR);
    Show_float_macro(GMRKM);
    Show_float_macro(OMEGA_FROM_VKM);
    Show_float_macro(G_CODE_UNIT);
    Show_physical_constants(GAS_CONSTANT);
    Show_physical_constants(FINE_STRUCTURE_CONSTANT);
    Show_physical_constants(SIGMA_THOMPSON);
    Show_physical_constants(STEFAN_BOLTZMANN_CONSTANT);

    /*
     * Units
     */
#undef X
    Printf("\nBinary_c standard input units: × these to get cgs\n");
#define X(TYPE) Show_float_macro(TYPE##_CODE_UNIT);
    CODE_UNIT_TYPE_LIST;
#undef X
    Printf("\n");
#define X(LONG,SHORT,MULT,CODEUNIT,CODETYPE)                            \
    Printf("Unit \"%s\" is \"%s\", to cgs ×%30.20e, code unit type %d \"%s\" (code unit cgs value %30.20e)\n", \
           (LONG),                                                      \
           (SHORT),                                                     \
           (MULT),                                                      \
           (CODETYPE),                                                  \
           Stringize(CODETYPE),                                         \
           CODEUNIT                                                     \
        );
    ALL_UNITS_LIST;
#undef X
    Printf("Units: ");
    {
        Boolean comma = FALSE;

#define X(LONG,SHORT,MULT,CODEUNIT,CODETYPE)                \
        Printf("%s %s", comma==TRUE ? "," : "",(SHORT));    \
        comma = TRUE;

        ALL_UNITS_LIST;
#undef X
    }
    Printf("\n");

    /*
     * Symbols
     */
    Show_string_macro(SOLAR_SYMBOL);
    Show_string_macro(L_SOLAR);
    Show_string_macro(M_SOLAR);
    Show_string_macro(R_SOLAR);

    /*
     * Cmd-line macro pairs
     */
    if(stardata &&
            stardata->tmpstore != NULL)
    {
        unsigned int i;
        for (i = 0; i < stardata->store->argstore->arg_count; i++)
        {
            unsigned int j;
            for (j = 0; j < stardata->store->argstore->cmd_line_args[i].npairs; j++)
            {
                Printf("ArgPair %s %s %g\n",
                       stardata->store->argstore->cmd_line_args[i].name,
                       stardata->store->argstore->cmd_line_args[i].pairs[j].string,
                       stardata->store->argstore->cmd_line_args[i].pairs[j].value);
            }
        }
    }

    /*
     * Output definition lists
     *  below we configure some name - number lists that are useful for binary_c-python to parse.
     *  TODO: handle a space at the end of the prepend
     */

    /* construct prepend */
    char *definition_list_prepend;
    char *definition_list_prepend_tmp;
    if(( definition_list_prepend_tmp = getenv( "BINARY_C_DEFLIST_HEADER" )) != NULL )
        definition_list_prepend = definition_list_prepend_tmp;
    else
        definition_list_prepend = "";

    /*
     * Stellar type list
     */
    for (int st_i = 0; st_i < NUMBER_OF_STELLAR_TYPES; st_i++)
    {
        Printf("%sstellar_type %d is %s\n",
               definition_list_prepend,
               st_i,
               short_stellar_type_macro_strings[st_i]);
    }

    /*
     * Supernova type list
     */
    for (int sn_i = 0; sn_i < NUM_SN_STRINGS; sn_i++)
    {
        Printf("%ssn_type %d is %s\n",
               definition_list_prepend,
               sn_i,
               supernova_type_strings[sn_i]);
    }

    /*
     * RLOF stablity case list
     */
#undef X
#define X(STRING,NUMBER) Printf("%sRLOF_stability_case %d is %s\n",definition_list_prepend,NUMBER,#STRING);
    RLOF_STABILITY_CASES_LIST
#undef X

    /*
     * RLOF type list
     */
#define X(MACRO,STRING) Printf("%sRLOF_type %d is %s\n",definition_list_prepend,RLOF_##MACRO,#MACRO);
    RLOF_CASES_LIST
#undef X

    if(stardata != NULL && stardata->preferences->speedtests == TRUE)
    {
        version_speedtests(stardata);
    }

    /* Exit binary_c*/
    Exit_or_return_void(BINARY_C_SPECIAL_EXIT, "version exit");
}


static unsigned long int __test_milliseconds(void)
{
    /* length of speedtests, in milliseconds */
    const char * t = getenv("NMS");
    return t == NULL ? 1 : strtol(t, NULL, 10);
}

/*
static Abundance _strided_Xsum(const Abundance * const X,
                               const int stride)
{
    Abundance * Xsum = Calloc(stride,sizeof(Abundance));
    const Isotope max = ((unsigned int)ISOTOPE_ARRAY_SIZE/stride) * stride;
    for(Isotope i=0;i<max;)
    {
        for(int j=0; j<stride; j++)
        {
            Xsum[j] += X[i++];
        }
    }
    for(Isotope i=max;i<ISOTOPE_ARRAY_SIZE;i++)
    {
        Xsum[0] += X[i];
    }

    Abundance Xsummed = 0.0;
    for(int j=0; j<stride; j++)
    {
        Xsummed += Xsum[j];
    }
    Safe_free(Xsum);
    return Xsummed;
}
*/
