#include "../binary_c.h"
No_empty_translation_unit_warning;

static void _parse(struct stardata_t * const stardata);

static Pure_function inline Boolean is_bracket(const char c) Maybe_unused;

void parse_hierarchy(struct stardata_t * const stardata)
{
    /*
     * Parse the hierarchy string provided if
     * if multiplicity is 1 or 2
     */
    _parse(stardata);

    if(stardata->preferences->multiplicity > 2)
    {
        _parse(stardata);
    }
    else if(stardata->preferences->multiplicity == 1)
    {
        /*
         * Single star: trivial
         */
    }
    else if(stardata->preferences->multiplicity == 2)
    {
        /*
         * Binary star: trivial
         */
    }
    else
    {
        Exit_binary_c(BINARY_C_ALGORITHM_OUT_OF_RANGE,
                      "Multiplicity %d has caused a problem when parsing hierarchy.",
                      (int)stardata->preferences->multiplicity);
    }
}

static void _parse(struct stardata_t * const stardata)
{
    char * hierarchy = strdup(stardata->preferences->hierarchy);
    stardata->preferences->multiplicity = 0;

    printf("parse hierarchy %s with ancestor %p\n",
           hierarchy,
           (void*)stardata);

#define _print(...)                                                     \
    {                                                                   \
        printf("At %d.%d (%d), current_stardata %p : ",                 \
               depth,                                                   \
               current_starnum[depth],                                  \
               stardata->preferences->multiplicity,                     \
               (void*)current_stardata);                                \
        printf(__VA_ARGS__);                                            \
        printf("\n");                                                   \
    }

    /*
     * Parse the given hierarchy, starting with the outermost
     * which is set in stardata.
     */
    char * pos = hierarchy;
    char * const maxpos = hierarchy + strlen(hierarchy);
    char * number_start = NULL;
    int depth = 0;
    struct stardata_t * current_stardata = stardata;
    Star_number * current_starnum = Calloc(1,sizeof(Star_number));
    int star_number = 0;
    while(pos<maxpos)
    {
        printf("Check '%c' %d\n",*pos,*pos=='(');

        if(is_integer((unsigned char)*pos))
        {
            /*
             * Number: this represents a star
             */
            if(number_start == NULL)
            {
                char * end_pointer = NULL;
                star_number = strtoi(pos,
                                     &end_pointer,
                                     10);
                printf("Depth %d : Found number %d\n",
                       depth,
                       star_number);
                _print("New star %d",current_starnum[depth]);
                current_stardata->star[current_starnum[depth]].system_star_number = stardata->preferences->multiplicity+1;
                stardata->preferences->multiplicity++;
                current_starnum[depth]++;
            }
        }

        if(*pos == '(' || *pos == '[')
        {
            /*
             * opening bracket :
             * this star should be a proxy stardata
             */
            struct star_t * const star = current_stardata->star + current_starnum[depth];
            star->type = STAR_PROXY;
            star->stardata = new_stardata(stardata->preferences);
            star->stardata->type = STARDATA_INNER;
            star->stardata->parent = current_stardata;
            star->stardata->ancestor = stardata;
            current_stardata = star->stardata;

            _print("Opening bracket : new proxy stardata %p (parent %p, ancestor %p)\n",
                   (void*)current_stardata,
                   (void*)current_stardata->parent,
                   (void*)current_stardata->ancestor);

            current_starnum[depth]++;
            depth++;
            current_starnum = Realloc(current_starnum,
                                      (depth+1) * sizeof(Star_number));
            current_starnum[depth] = 0;
        }
        else if(*pos == ')' || *pos == ']')
        {
            /*
             * closing bracket : back to parent
             */
            current_stardata = current_stardata->parent;
            _print("Closing bracket, reverted to parent\n");
            depth--;
        }
        else if(*pos == '+')
        {
            //_print("Increment current_star\n");
            //current_starnum[depth]++;
        }
        else
        {
            /*
             * Must be a star
             */
        }

        pos++;
    }

    printf("done parse hierarchy\n");
}

/*
static Boolean _has_bracket(char * const string);
static Boolean _has_bracket(char * string)
{
    if(strchr(string,'(') != NULL
       ||
       strchr(string,')') != NULL)
    {
        return TRUE;
    }
    else
    {
        return FALSE;
    }
}
*/
static Pure_function inline Boolean is_bracket(const char c)
{
    return (c == '(' || c == ')');
}
