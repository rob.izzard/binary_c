#include "../binary_c.h"
No_empty_translation_unit_warning;

/*
 * Data objects have not been built as separate object files,
 * so we make them here if DEFINE_DATA_OBJECTS is set (by meson).
 */

#ifdef DEFINE_DATA_OBJECTS

#ifdef NUCSYN

#include "../nucsyn/nucsyn_extended_s_process.h"
double _binary_nucsyn_extended_s_process_dat_start[] = { NUCSYN_EXTENDED_S_PROCESS_TABLE_V1 };


double _binary_nucsyn_novae_JH98_CO_dat_start[] = {
#include "../nucsyn/nucsyn_novae_JH98_CO.h"
};

double _binary_nucsyn_novae_JH98_ONe_dat_start[] = {
#include "../nucsyn/nucsyn_novae_JH98_ONe.h"
};

#include "../nucsyn/nucsyn_novae_Jose2022.h"
double _binary_nucsyn_novae_Jose2022_ONe_dat_start[] = {
NOVAE_JOSE2022_YIELDS
};
#endif //NUCSYN

#include "../stellar_structure/miller_bertolami_postagb.h"
#include "../stellar_structure/miller_bertolami_postagb_coeffs_L.h"
#include "../stellar_structure/miller_bertolami_postagb_coeffs_R.h"
double _binary_miller_bertolami_postagb_dat_start[] = { TABLE_MILLER_BERTOLAMI_DATA };
double _binary_miller_bertolami_postagb_coeffs_L_dat_start[] = { TABLE_MILLER_BERTOLAMI_COEFFS_L_DATA };
double _binary_miller_bertolami_postagb_coeffs_R_dat_start[] = { TABLE_MILLER_BERTOLAMI_COEFFS_R_DATA };

#include "../stellar_colours/eldridge2015-ostar.h"
#include "../stellar_colours/eldridge2015-basel.h"
double _binary_eldridge2015_basel_dat_start[] = {ELDRIDGE2015_DATA_ARRAY_BASEL};
double _binary_eldridge2015_ostar_dat_start[] = {ELDRIDGE2015_DATA_ARRAY_OSTAR};
#include "../stellar_colours/Kurucz.h"
double _binary_Kurucz_dat_start[] = {KURUCZ_DATA_ARRAY};

void define_data_objects(void)
{



}

#endif // DEFINE_DATA_OBJECTS
