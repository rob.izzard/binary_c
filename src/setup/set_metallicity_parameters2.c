#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef BSE

/*
 * Subroutine to set all the BSE fitting parameters for a given metallicity
 */


#include "metallicity_data.h"

#define C0 3.040581e-01
#define C1 8.049509e-02
#define C2 8.967485e-02
#define C3 8.780198e-02
#define C4 2.219170e-02

//#define UNUSED_ARRAY_CHECKS

void set_metallicity_parameters2 (struct stardata_t * Restrict const stardata)
{
    struct common_t *common = &(stardata->common);
    double z = common->metallicity;
    const double lz   = log10(z);
    const double dlzs = 1.0 / (z*log(10.0));
    const double lzs  = lz - log10(0.02); /* eta in Hurley+ 2000 */
    const double lzd  = lzs + 1.0;

    /* local pointers */
    double * const main_sequence_parameters = common->main_sequence_parameters;
    double * const giant_branch_parameters = common->giant_branch_parameters;
    double * const metallicity_parameters = common->metallicity_parameters;

    Dprint("Pointers %p %p %p\n",
           (void*)main_sequence_parameters,
           (void*)giant_branch_parameters,
           (void*)metallicity_parameters);

    /*
     * Fixed data: NB static const is thread safe.
     * In the comments are the equivalent common blocks
     * of BSE.
     */
    static const double xz[]={XZ_DATA}; /* ZAMS */
    static const double xt[]={XT_DATA}; /* TBGB */
    static const double xl[]={XL_DATA}; /* LTMS */
    static const double xr[]={XR_DATA}; /* RTMS */
    static const double xg[]={XG_DATA}; /* GAGB */
    static const double xh[]={XH_DATA}; /* CHEB */

    double m1,m2,mhefl,lhefl,thefl,rr,rb,lx,metallicity_parameter_2_over_3;

    Dprint("set met parameters %f\n",z);

    /*
     * Equation numbers are :
     * HPhD : Hurley's PhD dissertation
     * H00 : Hurley et al. (2000)
     * BSE : Hurley et al. (2002)
     */

    metallicity_parameters[ZPAR_METALLICITY] = z;
    metallicity_parameters[ZPAR_MASS_MS_HOOK] = 1.0185 + lzs*(0.16015 + lzs*0.0892);
    metallicity_parameters[ZPAR_MASS_HE_FLASH] = 1.995 + lzs*(0.25 + lzs*0.087);

    /*
     * MFGB : Eq. HPhd 2.3 / H00 3
     *
     * Maximum mass for which helium ignites on the giant branch
     */
    metallicity_parameters[ZPAR_MASS_FGB] = 16.5*pow(z,0.06)/(1.0 + pow((1.0e-04/z),1.27));
    metallicity_parameters[4] = Max(6.11044 + 1.02167*lzs,5.0);
    metallicity_parameters[5] = metallicity_parameters[4] + 1.8;
    metallicity_parameters[6] = 5.37 + lzs*0.135;
    metallicity_parameters[7] = C0 + lzs*(C1 + lzs*(C2 + lzs*(C3 + lzs*C4)));
    metallicity_parameters[8] = Max(0.95,
                                    Max(0.95-(10.0/3.0)*(z-0.01),
                                        Min(0.99,
                                            0.98-(100.0/7.0)*(z-0.001))));

    /**  Lzams **/
    main_sequence_parameters[0] = UNUSED_FLOAT;

    main_sequence_parameters[1] = xz[1]+lzs*(xz[2]+lzs*(xz[3]+lzs*(xz[4]+lzs*xz[5])));
    main_sequence_parameters[2] = xz[6]+lzs*(xz[7]+lzs*(xz[8]+lzs*(xz[9]+lzs*xz[10])));
    main_sequence_parameters[3] = xz[11]+lzs*(xz[12]+lzs*(xz[13]+lzs*(xz[14]+lzs*xz[15])));
    main_sequence_parameters[4] = xz[16]+lzs*(xz[17]+lzs*(xz[18]+lzs*(xz[19]+lzs*xz[20])));
    main_sequence_parameters[5] = xz[21]+lzs*(xz[22]+lzs*(xz[23]+lzs*(xz[24]+lzs*xz[25])));
    main_sequence_parameters[6] = xz[26]+lzs*(xz[27]+lzs*(xz[28]+lzs*(xz[29]+lzs*xz[30])));
    main_sequence_parameters[7] = xz[31]+lzs*(xz[32]+lzs*(xz[33]+lzs*(xz[34]+lzs*xz[35])));

    /**  Rzams **/
    main_sequence_parameters[8] = xz[36]+lzs*(xz[37]+lzs*(xz[38]+lzs*(xz[39]+lzs*xz[40])));
    main_sequence_parameters[9] = xz[41]+lzs*(xz[42]+lzs*(xz[43]+lzs*(xz[44]+lzs*xz[45])));
    main_sequence_parameters[10] = xz[46]+lzs*(xz[47]+lzs*(xz[48]+lzs*(xz[49]+lzs*xz[50])));
    main_sequence_parameters[11] = xz[51]+lzs*(xz[52]+lzs*(xz[53]+lzs*(xz[54]+lzs*xz[55])));
    main_sequence_parameters[12] = xz[56]+lzs*(xz[57]+lzs*(xz[58]+lzs*(xz[59]+lzs*xz[60])));
    main_sequence_parameters[13] = xz[61];
    main_sequence_parameters[14] = xz[62]+lzs*(xz[63]+lzs*(xz[64]+lzs*(xz[65]+lzs*xz[66])));
    main_sequence_parameters[15] = xz[67]+lzs*(xz[68]+lzs*(xz[69]+lzs*(xz[70]+lzs*xz[71])));
    main_sequence_parameters[16] = xz[72]+lzs*(xz[73]+lzs*(xz[74]+lzs*(xz[75]+lzs*xz[76])));

    /**  Tbgb  **/
    main_sequence_parameters[17] = xt[1]+lzs*(xt[2]+lzs*(xt[3]+lzs*xt[4]));
    main_sequence_parameters[18] = xt[5]+lzs*(xt[6]+lzs*(xt[7]+lzs*xt[8]));
    main_sequence_parameters[19] = xt[9]+lzs*(xt[10]+lzs*(xt[11]+lzs*xt[12]));
    main_sequence_parameters[20] = xt[13]+lzs*(xt[14]+lzs*(xt[15]+lzs*xt[16]));
    main_sequence_parameters[21] = xt[17];
    /**  dTbgb/dz **/
    main_sequence_parameters[117] = dlzs*(xt[2]+lzs*(2.00*xt[3]+3.00*lzs*xt[4]));
    main_sequence_parameters[118] = dlzs*(xt[6]+lzs*(2.00*xt[7]+3.00*lzs*xt[8]));
    main_sequence_parameters[119] = dlzs*(xt[10]+lzs*(2.00*xt[11]+3.00*lzs*xt[12]));
    main_sequence_parameters[120] = dlzs*(xt[14]+lzs*(2.00*xt[15]+3.00*lzs*xt[16]));
    /**  Thook **/
    main_sequence_parameters[22] = xt[18]+lzs*(xt[19]+lzs*(xt[20]+lzs*xt[21]));
    main_sequence_parameters[23] = xt[22];
    main_sequence_parameters[24] = xt[23]+lzs*(xt[24]+lzs*(xt[25]+lzs*xt[26]));
    main_sequence_parameters[25] = xt[27]+lzs*(xt[28]+lzs*(xt[29]+lzs*xt[30]));
    main_sequence_parameters[26] = xt[31];
    /**  Ltms  **/
    main_sequence_parameters[27] = xl[1]+lzs*(xl[2]+lzs*(xl[3]+lzs*(xl[4]+lzs*xl[5])));
    main_sequence_parameters[28] = xl[6]+lzs*(xl[7]+lzs*(xl[8]+lzs*(xl[9]+lzs*xl[10])));
    main_sequence_parameters[29] = xl[11]+lzs*(xl[12]+lzs*(xl[13]+lzs*xl[14]));
    main_sequence_parameters[30] = xl[15]+lzs*(xl[16]+lzs*(xl[17]+lzs*(xl[18]+lzs*xl[19])));
    main_sequence_parameters[27] = main_sequence_parameters[27]*main_sequence_parameters[30];
    main_sequence_parameters[28] = main_sequence_parameters[28]*main_sequence_parameters[30];
    main_sequence_parameters[31] = xl[20]+lzs*(xl[21]+lzs*(xl[22]+lzs*xl[23]));
    main_sequence_parameters[32] = xl[24]+lzs*(xl[25]+lzs*(xl[26]+lzs*xl[27]));
    /**  Lalpha **/
    m2 = 2.00;
    main_sequence_parameters[33] = xl[28]+lzs*(xl[29]+lzs*(xl[30]+lzs*xl[31]));
    main_sequence_parameters[34] = xl[32]+lzs*(xl[33]+lzs*(xl[34]+lzs*xl[35]));
    main_sequence_parameters[35] = xl[36]+lzs*(xl[37]+lzs*(xl[38]+lzs*xl[39]));
    main_sequence_parameters[36] = xl[40]+lzs*(xl[41]+lzs*(xl[42]+lzs*xl[43]));
    main_sequence_parameters[37] = Max(0.90,1.10640+lzs*(0.4150+0.180*lzs));
    main_sequence_parameters[38] = Max(1.00,1.190+lzs*(0.3770+0.1760*lzs));
    if(z > 0.01)
    {
        main_sequence_parameters[37] = Min(main_sequence_parameters[37],1.00);
        main_sequence_parameters[38] = Min(main_sequence_parameters[38],1.10);
    }
    main_sequence_parameters[39] = Max(0.1450,0.09770-lzs*(0.2310+0.07530*lzs));
    main_sequence_parameters[40] = Min(0.240+lzs*(0.180+0.5950*lzs),0.3060+0.0530*lzs);
    main_sequence_parameters[41] = Min(0.330+lzs*(0.1320+0.2180*lzs),0.36250+0.0620*lzs);
    main_sequence_parameters[42] = (main_sequence_parameters[33]+main_sequence_parameters[34]*pow(m2,main_sequence_parameters[36]))/(pow(m2,0.4)+main_sequence_parameters[35]*pow(m2,1.9));
    /**  Lbeta **/
    main_sequence_parameters[43] = xl[44]+lzs*(xl[45]+lzs*(xl[46]+lzs*(xl[47]+lzs*xl[48])));
    main_sequence_parameters[44] = xl[49]+lzs*(xl[50]+lzs*(xl[51]+lzs*(xl[52]+lzs*xl[53])));
    main_sequence_parameters[45] = xl[54]+lzs*(xl[55]+lzs*xl[56]);
    main_sequence_parameters[46] = Min(1.40,1.51350+0.37690*lzs);
    main_sequence_parameters[46] = Max(0.63550-0.41920*lzs,Max(1.250,main_sequence_parameters[46]));
    /**  Lhook **/
    main_sequence_parameters[47] = xl[57]+lzs*(xl[58]+lzs*(xl[59]+lzs*xl[60]));
    main_sequence_parameters[48] = xl[61]+lzs*(xl[62]+lzs*(xl[63]+lzs*xl[64]));
    main_sequence_parameters[49] = xl[65]+lzs*(xl[66]+lzs*(xl[67]+lzs*xl[68]));
    main_sequence_parameters[50] = xl[69]+lzs*(xl[70]+lzs*(xl[71]+lzs*xl[72]));
    main_sequence_parameters[51] = Min(1.40,1.51350+0.37690*lzs);
    main_sequence_parameters[51] = Max(0.63550-0.41920*lzs,Max(1.250,main_sequence_parameters[51]));
    /**  Rtms **/
    main_sequence_parameters[52] = xr[1]+lzs*(xr[2]+lzs*(xr[3]+lzs*(xr[4]+lzs*xr[5])));
    main_sequence_parameters[53] = xr[6]+lzs*(xr[7]+lzs*(xr[8]+lzs*(xr[9]+lzs*xr[10])));
    main_sequence_parameters[54] = xr[11]+lzs*(xr[12]+lzs*(xr[13]+lzs*(xr[14]+lzs*xr[15])));
    main_sequence_parameters[55] = xr[16]+lzs*(xr[17]+lzs*(xr[18]+lzs*xr[19]));
    main_sequence_parameters[56] = xr[20]+lzs*(xr[21]+lzs*(xr[22]+lzs*xr[23]));
    main_sequence_parameters[52] = main_sequence_parameters[52]*main_sequence_parameters[54];
    main_sequence_parameters[53] = main_sequence_parameters[53]*main_sequence_parameters[54];
    main_sequence_parameters[57] = xr[24];
    main_sequence_parameters[58] = xr[25]+lzs*(xr[26]+lzs*(xr[27]+lzs*xr[28]));
    main_sequence_parameters[59] = xr[29]+lzs*(xr[30]+lzs*(xr[31]+lzs*xr[32]));
    main_sequence_parameters[60] = xr[33]+lzs*(xr[34]+lzs*(xr[35]+lzs*xr[36]));
    main_sequence_parameters[61] = xr[37]+lzs*(xr[38]+lzs*(xr[39]+lzs*xr[40]));
    /**  **/
    main_sequence_parameters[62] = Max(0.0970-0.10720*(lz+3.00),Max(0.0970,Min(0.14610,0.14610+0.12370*(lz+2.00))));
    main_sequence_parameters[62] = exp10(main_sequence_parameters[62]);
    m2 = main_sequence_parameters[62] + 0.10;
    main_sequence_parameters[63] = (main_sequence_parameters[52]+main_sequence_parameters[53]*pow(main_sequence_parameters[62],main_sequence_parameters[55]))/(main_sequence_parameters[54]+pow(main_sequence_parameters[62],main_sequence_parameters[56]));
    main_sequence_parameters[64] = (main_sequence_parameters[57]*Pow3(m2)+main_sequence_parameters[58]*pow(m2,main_sequence_parameters[61])
                                    +main_sequence_parameters[59]*pow(m2,main_sequence_parameters[61]+1.5))
        /(main_sequence_parameters[60]+Pow5(m2));
    /**  Ralpha **/
    main_sequence_parameters[65] = xr[41]+lzs*(xr[42]+lzs*(xr[43]+lzs*xr[44]));
    main_sequence_parameters[66] = xr[45]+lzs*(xr[46]+lzs*(xr[47]+lzs*xr[48]));
    main_sequence_parameters[67] = xr[49]+lzs*(xr[50]+lzs*(xr[51]+lzs*xr[52]));
    main_sequence_parameters[68] = xr[53]+lzs*(xr[54]+lzs*(xr[55]+lzs*xr[56]));
    main_sequence_parameters[69] = xr[57]+lzs*(xr[58]+lzs*(xr[59]+lzs*(xr[60]+lzs*xr[61])));
    main_sequence_parameters[70] = Max(0.90,Min(1.00,1.1160+0.1660*lzs));
    main_sequence_parameters[71] = Max(1.4770+0.2960*lzs,Min(1.60,-0.3080-1.0460*lzs));
    main_sequence_parameters[71] = Max(0.80,Min(0.80-2.00*lzs,main_sequence_parameters[71]));
    main_sequence_parameters[72] = xr[62]+lzs*(xr[63]+lzs*xr[64]);
    main_sequence_parameters[73] = Max(0.0650,0.08430-lzs*(0.04750+0.03520*lzs));
    main_sequence_parameters[74] = 0.07360+lzs*(0.07490+0.044260*lzs);
    if(z < 0.004)
    {
        main_sequence_parameters[74] = Min(0.0550,main_sequence_parameters[74]);
    }
    main_sequence_parameters[75] = Max(0.0910,Min(0.1210,0.1360+0.0352*lzs));
    main_sequence_parameters[76] = (main_sequence_parameters[65]*pow(main_sequence_parameters[71],main_sequence_parameters[67]))/(main_sequence_parameters[66] + pow(main_sequence_parameters[71],main_sequence_parameters[68]));
    if(main_sequence_parameters[70] > main_sequence_parameters[71])
    {
        main_sequence_parameters[70] = main_sequence_parameters[71];
        main_sequence_parameters[75] = main_sequence_parameters[76];
    }
    /**  Rbeta **/
    main_sequence_parameters[77] = xr[65]+lzs*(xr[66]+lzs*(xr[67]+lzs*xr[68]));
    main_sequence_parameters[78] = xr[69]+lzs*(xr[70]+lzs*(xr[71]+lzs*xr[72]));
    main_sequence_parameters[79] = xr[73]+lzs*(xr[74]+lzs*(xr[75]+lzs*xr[76]));
    main_sequence_parameters[80] = xr[77]+lzs*(xr[78]+lzs*(xr[79]+lzs*xr[80]));
    main_sequence_parameters[81] = xr[81]+lzs*(xr[82]+lzs*lzs*xr[83]);
    if(z > 0.01)
    {
        main_sequence_parameters[81] = Max(main_sequence_parameters[81],0.950);
    }
    main_sequence_parameters[82] = Max(1.40,Min(1.60,1.60+lzs*(0.7640+0.33220*lzs)));
    /**  Rgamma **/
    main_sequence_parameters[83] = Max(xr[84]+lzs*(xr[85]+lzs*(xr[86]+lzs*xr[87])),xr[96]+lzs*(xr[97]+lzs*xr[98]));
    main_sequence_parameters[84] = Min(0.00,xr[88]+lzs*(xr[89]+lzs*(xr[90]+lzs*xr[91])));
    main_sequence_parameters[84] = Max(main_sequence_parameters[84],xr[99]+lzs*(xr[100]+lzs*xr[101]));
    main_sequence_parameters[85] = xr[92]+lzs*(xr[93]+lzs*(xr[94]+lzs*xr[95]));
    main_sequence_parameters[85] = Max(0.00,Min(main_sequence_parameters[85],7.4540+9.0460*lzs));
    main_sequence_parameters[86] = Min(xr[102]+lzs*xr[103],Max(2.00,-13.30-18.60*lzs));
    main_sequence_parameters[87] = Min(1.50,Max(0.40,2.4930+1.14750*lzs));
    main_sequence_parameters[88] = Max(1.00,Min(1.270,0.81090-0.62820*lzs));
    main_sequence_parameters[88] = Max(main_sequence_parameters[88],0.63550-0.41920*lzs);
    main_sequence_parameters[89] = Max(5.855420e-02,-0.27110-lzs*(0.57560+0.08380*lzs));
    /**  Rhook **/
    main_sequence_parameters[90] = xr[104]+lzs*(xr[105]+lzs*(xr[106]+lzs*xr[107]));
    main_sequence_parameters[91] = xr[108]+lzs*(xr[109]+lzs*(xr[110]+lzs*xr[111]));
    main_sequence_parameters[92] = xr[112]+lzs*(xr[113]+lzs*(xr[114]+lzs*xr[115]));
    main_sequence_parameters[93] = xr[116]+lzs*(xr[117]+lzs*(xr[118]+lzs*xr[119]));
    main_sequence_parameters[94] = Min(1.250,Max(1.10,1.98480+lzs*(1.13860+0.35640*lzs)));
    main_sequence_parameters[95] = 0.0630 + lzs*(0.04810 + 0.009840*lzs);
    main_sequence_parameters[96] = Min(1.30,Max(0.450,1.20+2.450*lzs));
    /**  Lneta **/
    main_sequence_parameters[97] = z>0.0009 ? 10.0 : 20.0;

    /*
     * New main sequence parameters which are constant and
     * depend only on the previously calculated main_sequence_parameters,
     * hence can be calculated here
     */
    main_sequence_parameters[121]=Min(
        main_sequence_parameters[47]*pow(main_sequence_parameters[51],-main_sequence_parameters[48]),
        main_sequence_parameters[49]*pow(main_sequence_parameters[51],-main_sequence_parameters[50]));


    main_sequence_parameters[122]=(main_sequence_parameters[90] + main_sequence_parameters[91]*ZPAR_M2_3_5)
        /(main_sequence_parameters[92]*ZPAR_M2_3 + pow(ZPAR_M2,main_sequence_parameters[93])) - 1.0;
    main_sequence_parameters[122]-=main_sequence_parameters[95];

    main_sequence_parameters[123]=1.0/(ZPAR_M2-main_sequence_parameters[94]);

    main_sequence_parameters[124]=((main_sequence_parameters[81]-1.06)/(main_sequence_parameters[82]-1.0));

    main_sequence_parameters[125]=(main_sequence_parameters[77]*Pow3(ZPAR_M2)*sqrt(ZPAR_M2))/
        (main_sequence_parameters[78] + pow(ZPAR_M2,main_sequence_parameters[79]))
        -main_sequence_parameters[81];

    main_sequence_parameters[126]=(main_sequence_parameters[125]/
                                   (ZPAR_M2-main_sequence_parameters[82]));


    main_sequence_parameters[127]=(main_sequence_parameters[77]*Pow3(ZPAR_M3)*sqrt(ZPAR_M3))/
        (main_sequence_parameters[78] + pow(ZPAR_M3,main_sequence_parameters[79]));

    main_sequence_parameters[128]=((main_sequence_parameters[74] - main_sequence_parameters[73])/0.15);
    main_sequence_parameters[129]=((main_sequence_parameters[75]-main_sequence_parameters[74])/(main_sequence_parameters[70]-0.65));


    /* will cause floating point failures */
    //main_sequence_parameters[130]=((main_sequence_parameters[76] - main_sequence_parameters[75])/
    //(main_sequence_parameters[71] - main_sequence_parameters[70]));

    main_sequence_parameters[131]=(main_sequence_parameters[65]*
                                   pow(main_sequence_parameters[72],
                                       main_sequence_parameters[67]))/
        (main_sequence_parameters[66] +
         pow(main_sequence_parameters[72],
             main_sequence_parameters[68]));

    main_sequence_parameters[132]=(main_sequence_parameters[64]-main_sequence_parameters[63])*10.0;

    main_sequence_parameters[133]=(main_sequence_parameters[41]-main_sequence_parameters[40])/
        (main_sequence_parameters[38]-main_sequence_parameters[37]);

    main_sequence_parameters[134]=(main_sequence_parameters[42]-main_sequence_parameters[41])
        /(LALPH_MCUT-main_sequence_parameters[38]);

    main_sequence_parameters[135]=((main_sequence_parameters[40]-0.3)/(main_sequence_parameters[37]-0.7));

    main_sequence_parameters[136]=((0.3 - main_sequence_parameters[39])*5.0);



    // 236032 0.00989957
    main_sequence_parameters[138]=Max(0.0,(main_sequence_parameters[83] + main_sequence_parameters[84]*
                                           pow((RGAMM_M1-main_sequence_parameters[85]), main_sequence_parameters[86])));
    main_sequence_parameters[137] = main_sequence_parameters[88]>RGAMM_M1 ? main_sequence_parameters[89] :main_sequence_parameters[138] ;
    main_sequence_parameters[139]=main_sequence_parameters[89] - main_sequence_parameters[138];
    main_sequence_parameters[140]=main_sequence_parameters[88]+0.1;
    main_sequence_parameters[141]=10.0*main_sequence_parameters[137];

    main_sequence_parameters[142] = main_sequence_parameters[88] - RGAMM_M1;
    main_sequence_parameters[143]=main_sequence_parameters[43]-main_sequence_parameters[44]*
        pow(main_sequence_parameters[46],main_sequence_parameters[45]);


    Dprint("Set giant parameters\n");

    /**  Lbgb  **/
    giant_branch_parameters[1] = xg[1]+lzs*(xg[2]+lzs*(xg[3]+lzs*xg[4]));
    giant_branch_parameters[2] = xg[5]+lzs*(xg[6]+lzs*(xg[7]+lzs*xg[8]));
    giant_branch_parameters[3] = xg[9]+lzs*(xg[10]+lzs*(xg[11]+lzs*xg[12]));
    giant_branch_parameters[4] = xg[13]+lzs*(xg[14]+lzs*(xg[15]+lzs*xg[16]));
    giant_branch_parameters[5] = xg[17]+lzs*(xg[18]+lzs*xg[19]);
    giant_branch_parameters[6] = xg[20]+lzs*(xg[21]+lzs*xg[22]);
    giant_branch_parameters[3] = pow(giant_branch_parameters[3],giant_branch_parameters[6]);
    giant_branch_parameters[7] = xg[23];
    giant_branch_parameters[8] = xg[24];
    /**  Lbagb **/
    /**  set giant_branch_parameters[16] = 1.00; until it is reset later with an initial **/
    /**  call to Lbagbf using mass = zpars[2]; and mhefl = 0.0; **/
    giant_branch_parameters[9] = xg[25] + lzs*(xg[26] + lzs*xg[27]);
    giant_branch_parameters[10] = xg[28] + lzs*(xg[29] + lzs*xg[30]);
    giant_branch_parameters[11] = 15.00;
    giant_branch_parameters[12] = xg[31]+lzs*(xg[32]+lzs*(xg[33]+lzs*xg[34]));
    giant_branch_parameters[13] = xg[35]+lzs*(xg[36]+lzs*(xg[37]+lzs*xg[38]));
    giant_branch_parameters[14] = xg[39]+lzs*(xg[40]+lzs*(xg[41]+lzs*xg[42]));
    giant_branch_parameters[15] = xg[43]+lzs*xg[44];
    giant_branch_parameters[12] = pow(giant_branch_parameters[12],giant_branch_parameters[15]);
    giant_branch_parameters[14] = pow(giant_branch_parameters[14],giant_branch_parameters[15]);
    giant_branch_parameters[16] = 1.00;
    /**  Rgb **/
    giant_branch_parameters[17] = -4.67390-0.93940*lz;
    giant_branch_parameters[17] = exp10(giant_branch_parameters[17]);
    giant_branch_parameters[17] = Max(giant_branch_parameters[17],-0.041670+55.670*z);
    giant_branch_parameters[17] = Min(giant_branch_parameters[17],0.47710-9329.210*pow(z,2.940));
    giant_branch_parameters[18] = Min(0.540,0.3970+lzs*(0.288260+0.52930*lzs));
    giant_branch_parameters[19] = Max(-0.14510,-2.27940-lz*(1.51750+0.2540*lz));
    giant_branch_parameters[19] = exp10(giant_branch_parameters[19]);
    if(z > 0.004)
    {
        giant_branch_parameters[19] = Max(giant_branch_parameters[19],0.73070+14265.10*pow(z,3.3950));
    }
    giant_branch_parameters[20] = xg[45]+lzs*(xg[46]+lzs*(xg[47]+lzs*(xg[48]+lzs*(xg[49]+lzs*xg[50]))));
    giant_branch_parameters[21] = xg[51]+lzs*(xg[52]+lzs*(xg[53]+lzs*(xg[54]+lzs*xg[55])));
    giant_branch_parameters[22] = xg[56]+lzs*(xg[57]+lzs*(xg[58]+lzs*(xg[59]+lzs*(xg[60]+lzs*xg[61]))));
    giant_branch_parameters[23] = xg[62]+lzs*(xg[63]+lzs*(xg[64]+lzs*(xg[65]+lzs*xg[66])));
    /**  Ragb **/
    giant_branch_parameters[24] = Min(0.991640-743.1230*pow(z,2.830),1.04220+lzs*(0.131560+0.0450*lzs));
    giant_branch_parameters[25] = xg[67]+lzs*(xg[68]+lzs*(xg[69]+lzs*(xg[70]+lzs*(xg[71]+lzs*xg[72]))));
    giant_branch_parameters[26] = xg[73]+lzs*(xg[74]+lzs*(xg[75]+lzs*(xg[76]+lzs*xg[77])));
    giant_branch_parameters[27] = xg[78]+lzs*(xg[79]+lzs*(xg[80]+lzs*(xg[81]+lzs*(xg[82]+lzs*xg[83]))));
    giant_branch_parameters[28] = xg[84]+lzs*(xg[85]+lzs*(xg[86]+lzs*(xg[87]+lzs*xg[88])));
    giant_branch_parameters[29] = xg[89]+lzs*(xg[90]+lzs*(xg[91]+lzs*(xg[92]+lzs*(xg[93]+lzs*xg[94]))));
    giant_branch_parameters[30] = xg[95]+lzs*(xg[96]+lzs*(xg[97]+lzs*(xg[98]+lzs*(xg[99]+lzs*xg[100]))));
    m1 = metallicity_parameters[ZPAR_MASS_HE_FLASH] - 0.20;
    giant_branch_parameters[31] = giant_branch_parameters[29] + giant_branch_parameters[30]*m1;
    giant_branch_parameters[32] = Min(giant_branch_parameters[25]*pow(metallicity_parameters[ZPAR_MASS_HE_FLASH],-giant_branch_parameters[26]),
                               giant_branch_parameters[27]*pow(metallicity_parameters[ZPAR_MASS_HE_FLASH],-giant_branch_parameters[28]));

#ifdef AXEL_RGBF_FIX

    /** axel table interpolation for radius of fgb and agb**/

    double ragb_param[][17]={R_AGB_DATA}; /* R_AGB */

    /*
     * Axel:*****for interpolating radii*********************
     * select the pair of metallicities, for which detailed models are known,
     * between which z is (0.03=[0],0.02=[1],0.01=[2],0.004=[3],0.001=[4],
     * 0.0003=[5],0.0001=[6])
     */

    int z_int;
    if(z>0.02)
    {
        z_int=0;
    }
    else if(z>0.01)
    {
        z_int=1;
    }
    else if(z>0.004)
    {
        z_int=2;
    }
    else if(z>0.001)
    {
        z_int=3;
    }
    else if(z>0.0003)
    {
        z_int=4;
    }
    else
    {
        z_int=5;
    }

    // map 7 to 3, 8 to 4, 9 to 5

    giant_branch_parameters[GBP_AXEL2]=z;
    /* parameters for value of lower z in interpolation interval */
    giant_branch_parameters[GBP_AXEL0] = ragb_param[z_int+1][0]; // 100
    giant_branch_parameters[GBP_AXEL7] = ragb_param[z_int+1][1]; // 107
    giant_branch_parameters[GBP_AXEL8] = ragb_param[z_int+1][2]; // 108
    giant_branch_parameters[GBP_AXEL9] = ragb_param[z_int+1][3]; // 109
    giant_branch_parameters[GBP2_AXEL0] = ragb_param[z_int+1][13]; // 200
    giant_branch_parameters[GBP2_AXEL1] = ragb_param[z_int+1][14]; // 201
    giant_branch_parameters[GBP2_AXEL2] = ragb_param[z_int+1][15]; // 202
    giant_branch_parameters[GBP2_AXEL3] = ragb_param[z_int+1][16]; // 203
    giant_branch_parameters[GBP2_AXEL4] = ragb_param[z_int+1][4];  // 204
    giant_branch_parameters[GBP2_AXEL5] = ragb_param[z_int+1][5]; // 205
    giant_branch_parameters[GBP2_AXEL6] = ragb_param[z_int+1][6]; // 206
    giant_branch_parameters[GBP2_AXEL7] = ragb_param[z_int+1][7]; // 207
    giant_branch_parameters[GBP2_AXEL8] = ragb_param[z_int+1][8]; // 208
    giant_branch_parameters[GBP2_AXEL9] = ragb_param[z_int+1][9]; // 209
    giant_branch_parameters[GBP2_AXEL30] = ragb_param[z_int+1][10]; // 230
    giant_branch_parameters[GBP2_AXEL31] = ragb_param[z_int+1][11]; // 231
    giant_branch_parameters[GBP2_AXEL32] = ragb_param[z_int+1][12]; // 232

    /* parameters for value of higher z in interpolation interval */
    giant_branch_parameters[GBP_AXEL1] = ragb_param[z_int][0]; // 101
    giant_branch_parameters[GBP_AXEL17] = ragb_param[z_int][1]; // 117
    giant_branch_parameters[GBP_AXEL18] = ragb_param[z_int][2];
    giant_branch_parameters[GBP_AXEL19] = ragb_param[z_int][3];
    giant_branch_parameters[GBP_AXEL20] = ragb_param[z_int][13];
    giant_branch_parameters[GBP_AXEL21] = ragb_param[z_int][14];
    giant_branch_parameters[GBP_AXEL22] = ragb_param[z_int][15];
    giant_branch_parameters[GBP_AXEL23] = ragb_param[z_int][16];
    giant_branch_parameters[GBP_AXEL24] = ragb_param[z_int][4];
    giant_branch_parameters[GBP_AXEL25] = ragb_param[z_int][5];
    giant_branch_parameters[GBP_AXEL26] = ragb_param[z_int][6];
    giant_branch_parameters[GBP_AXEL27] = ragb_param[z_int][7];
    giant_branch_parameters[GBP_AXEL28] = ragb_param[z_int][8];
    giant_branch_parameters[GBP_AXEL29] = ragb_param[z_int][9];
    giant_branch_parameters[GBP_AXEL30] = ragb_param[z_int][10];
    giant_branch_parameters[GBP_AXEL31] = ragb_param[z_int][11];
    giant_branch_parameters[GBP_AXEL32] = ragb_param[z_int][12];
#endif // AXEL_RGBF_FIX

    /**  Mchei **/
    giant_branch_parameters[33] = Pow4(xg[101]);
    giant_branch_parameters[34] = xg[102]*4.00;

    /**  Mcagb **/
    giant_branch_parameters[35] = xg[103]+lzs*(xg[104]+lzs*(xg[105]+lzs*xg[106]));
    giant_branch_parameters[36] = xg[107]+lzs*(xg[108]+lzs*(xg[109]+lzs*xg[110]));
    giant_branch_parameters[37] = xg[111]+lzs*xg[112];
    giant_branch_parameters[35] = Pow4(giant_branch_parameters[35]);
    giant_branch_parameters[36] = giant_branch_parameters[36]*4.00;
    giant_branch_parameters[37] = Pow4(giant_branch_parameters[37]);
    /**  Lhei **/
    /**  set giant_branch_parameters[41] = -1.00; until it is reset later with an initial **/
    /**  call to Lheif using mass = metallicity_parameters[ZPAR_MASS_HE_FLASH]; and mhefl = 0.0; **/
    giant_branch_parameters[38] = xh[1]+lzs*xh[2];
    giant_branch_parameters[39] = xh[3]+lzs*xh[4];
    giant_branch_parameters[40] = xh[5];
    giant_branch_parameters[41] = -1.00;
    giant_branch_parameters[42] = xh[6]+lzs*(xh[7]+lzs*xh[8]);
    giant_branch_parameters[43] = xh[9]+lzs*(xh[10]+lzs*xh[11]);
    giant_branch_parameters[44] = xh[12]+lzs*(xh[13]+lzs*xh[14]);
    giant_branch_parameters[42] = Pow2(giant_branch_parameters[42]);
    giant_branch_parameters[44] = Pow2(giant_branch_parameters[44]);
    /**  Lhe **/
    giant_branch_parameters[45] = xh[15]+lzs*(xh[16]+lzs*xh[17]);
    giant_branch_parameters[46] = lzs>-1.0 ? 1.00 - xh[19]*pow((lzs+1.00),xh[18]) : 1.0;
    giant_branch_parameters[47] = xh[20]+lzs*(xh[21]+lzs*xh[22]);
    giant_branch_parameters[48] = xh[23]+lzs*(xh[24]+lzs*xh[25]);
    giant_branch_parameters[GBP_LHEF]=giant_branch_parameters[48]+0.1;
    giant_branch_parameters[45] = pow(giant_branch_parameters[45],giant_branch_parameters[48]);
    giant_branch_parameters[47] = pow(giant_branch_parameters[47],giant_branch_parameters[48]);
    giant_branch_parameters[46] = (giant_branch_parameters[46]/(pow(metallicity_parameters[ZPAR_MASS_FGB],0.10)))
        +(giant_branch_parameters[46]*giant_branch_parameters[47]-giant_branch_parameters[45])/
        pow(metallicity_parameters[ZPAR_MASS_FGB],(giant_branch_parameters[48]+0.10));

    /**  Rmin **/
    giant_branch_parameters[49] = xh[26]+lzs*(xh[27]+lzs*(xh[28]+lzs*xh[29]));
    giant_branch_parameters[50] = xh[30]+lzs*(xh[31]+lzs*(xh[32]+lzs*xh[33]));
    giant_branch_parameters[51] = xh[34]+lzs*(xh[35]+lzs*(xh[36]+lzs*xh[37]));
    giant_branch_parameters[52] = 5.00+xh[38]*pow(z,xh[39]);
    giant_branch_parameters[53] = xh[40]+lzs*(xh[41]+lzs*(xh[42]+lzs*xh[43]));
    giant_branch_parameters[49] = pow(giant_branch_parameters[49],giant_branch_parameters[53]);
    giant_branch_parameters[51] = pow(giant_branch_parameters[51],(2.00*giant_branch_parameters[53]));
    /**  The **/
    /**  set giant_branch_parameters[57] = -1.00; until it is reset later with an initial **/
    /**  call to Thef using mass = metallicity_parameters[ZPAR_MASS_HE_FLASH],; mc = 0.0;  and mhefl = 0.0; **/
    giant_branch_parameters[54] = xh[44]+lzs*(xh[45]+lzs*(xh[46]+lzs*xh[47]));
    giant_branch_parameters[55] = xh[48]+lzs*(xh[49]+lzs*xh[50]);
    giant_branch_parameters[55] = Max(giant_branch_parameters[55],1.00);
    giant_branch_parameters[56] = xh[51];
    giant_branch_parameters[57] = -1.00;
    giant_branch_parameters[58] = xh[52]+lzs*(xh[53]+lzs*(xh[54]+lzs*xh[55]));
    giant_branch_parameters[59] = xh[56]+lzs*(xh[57]+lzs*(xh[58]+lzs*xh[59]));
    giant_branch_parameters[60] = xh[60]+lzs*(xh[61]+lzs*(xh[62]+lzs*xh[63]));
    giant_branch_parameters[61] = xh[64]+lzs*xh[65];
    giant_branch_parameters[58] = pow(giant_branch_parameters[58],giant_branch_parameters[61]);
    giant_branch_parameters[60] = Pow5(giant_branch_parameters[60]);
    /**  Tbl **/
    metallicity_parameter_2_over_3 =metallicity_parameters[ZPAR_MASS_HE_FLASH]/metallicity_parameters[ZPAR_MASS_FGB];

    giant_branch_parameters[62] = xh[66]+lzs*xh[67];
    giant_branch_parameters[62] = -giant_branch_parameters[62]*log10(metallicity_parameter_2_over_3);
    giant_branch_parameters[63] = xh[68];
    giant_branch_parameters[64] = lzd>0.0 ? 1.00-lzd*(xh[69]+lzd*(xh[70]+lzd*xh[71])) : 1.0;
    giant_branch_parameters[65] = 1.00-giant_branch_parameters[64]*pow(metallicity_parameter_2_over_3,giant_branch_parameters[63]);
    /**NB gbp[66] is altered later on as well **/
    giant_branch_parameters[66] = 1.00 - lzd*(xh[77] + lzd*(xh[78] + lzd*xh[79]));


    giant_branch_parameters[67] = xh[72] + lzs*(xh[73] + lzs*(xh[74] + lzs*xh[75]));
    giant_branch_parameters[68] = xh[76];
    /**  Lzahb **/
    giant_branch_parameters[69] = xh[80] + lzs*(xh[81] + lzs*xh[82]);
    giant_branch_parameters[70] = xh[83] + lzs*(xh[84] + lzs*xh[85]);
    giant_branch_parameters[71] = 15.00;
    giant_branch_parameters[72] = xh[86];
    giant_branch_parameters[73] = xh[87];
    /**  Rzahb **/
    giant_branch_parameters[75] = xh[88] + lzs*(xh[89] + lzs*(xh[90] + lzs*xh[91]));
    giant_branch_parameters[76] = xh[92] + lzs*(xh[93] + lzs*(xh[94] + lzs*xh[95]));
    giant_branch_parameters[77] = xh[96] + lzs*(xh[97] + lzs*(xh[98] + lzs*xh[99]));


    /**  finish Lbagb **/
    mhefl = 0.00;
    lx = lbagbf(metallicity_parameters[ZPAR_MASS_HE_FLASH],
                mhefl,
                giant_branch_parameters);
    giant_branch_parameters[16] = lx;
    /**  finish LHeI **/
    lhefl = lheif(metallicity_parameters[ZPAR_MASS_HE_FLASH],
                  mhefl,
                  giant_branch_parameters);
    giant_branch_parameters[41] = (giant_branch_parameters[38]*pow(metallicity_parameters[ZPAR_MASS_HE_FLASH],giant_branch_parameters[39])-lhefl)/(exp(metallicity_parameters[ZPAR_MASS_HE_FLASH]*giant_branch_parameters[40])*lhefl);
    /**  finish THe **/
    /** NB second argument here is 0, in the fortran, a dummy **/
    /** variable was set to 0 then passed in. That's silly! **/
    thefl = thef(metallicity_parameters[ZPAR_MASS_HE_FLASH],0.0,mhefl,giant_branch_parameters)*
        tbgbf(stardata,metallicity_parameters[ZPAR_MASS_HE_FLASH],main_sequence_parameters,z);

    giant_branch_parameters[57] = (thefl-giant_branch_parameters[54])/(giant_branch_parameters[54]*exp(giant_branch_parameters[56]*metallicity_parameters[ZPAR_MASS_HE_FLASH]));

    /**  finish Tblf **/
    rb = ragbf(metallicity_parameters[ZPAR_MASS_FGB],
               lheif(metallicity_parameters[ZPAR_MASS_FGB],
                     metallicity_parameters[ZPAR_MASS_HE_FLASH],
                     giant_branch_parameters),
               mhefl,
               giant_branch_parameters
#ifdef AXEL_RGBF_FIX
               ,stardata->common.metallicity
#endif
        );


    rr = Max(1.00 - rminf(metallicity_parameters[ZPAR_MASS_FGB],giant_branch_parameters)/rb,1e-12);
    giant_branch_parameters[66] = giant_branch_parameters[66]/
        (pow(metallicity_parameters[ZPAR_MASS_FGB],giant_branch_parameters[67])*
         pow(rr,giant_branch_parameters[68]));


    /**  finish Lzahb **/
    giant_branch_parameters[74] = lhefl*lhef(metallicity_parameters[ZPAR_MASS_HE_FLASH],giant_branch_parameters);
    giant_branch_parameters[GBP_LZAHBF1]=giant_branch_parameters[69]-giant_branch_parameters[74];
    giant_branch_parameters[GBP_LZAHBF2]=(giant_branch_parameters[72]+1.0)*giant_branch_parameters[69];

    struct BSE_data_t * bse = new_BSE_data();
    Dprint("Set He-ignition timescales\n");
    stellar_timescales(stardata,
                       &stardata->star[0],
                       bse,
                       metallicity_parameters[ZPAR_MASS_HE_FLASH],
                       metallicity_parameters[ZPAR_MASS_HE_FLASH],
                       0);

    metallicity_parameters[9] = mcgbf(bse->luminosities[L_BGB],
                                      bse->GB,
                                      bse->luminosities[L_LMX]);

    metallicity_parameters[10] = mcgbf(bse->luminosities[L_HE_IGNITION],
                                       bse->GB,
                                       bse->luminosities[L_LMX]);

    free_BSE_data(&bse);

    /*
     * set the hydrogen and helium abundances
     * If the NUCSYN library is used, its
     * abundances override these
     */
    metallicity_parameters[11] = 0.760 - 3.00*z;
    metallicity_parameters[12] = 0.240 + 2.00*z;

    /** set constant for low-mass CHeB stars**/
    metallicity_parameters[13] = rminf(metallicity_parameters[ZPAR_MASS_HE_FLASH],
                                       giant_branch_parameters)
        /
        rgbf(metallicity_parameters[ZPAR_MASS_HE_FLASH],
             lzahbf(metallicity_parameters[ZPAR_MASS_HE_FLASH],
                    metallicity_parameters[9],
                    metallicity_parameters[ZPAR_MASS_HE_FLASH],
                    giant_branch_parameters),
             giant_branch_parameters);

    metallicity_parameters[14] = pow(z,0.4);

    Dprint("Leaving function set_metallicity_parameters2\n");

#ifdef UNUSED_ARRAY_CHECKS
    for(i=0;i<GIANT_BRANCH_PARAMETERS_SIZE;i++)
    {
        if(Fequal(giant_branch_parameters[i],UNUSED_VALUE))
        {
            printf("giant branch parameter %d is unused\n",i);
        }
        else
        {
            printf("giant branch parameter %d is %g\n",i,giant_branch_parameters[i]);
        }
    }
    for(i=0;i<MAIN_SEQUENCE_PARAMETERS_SIZE;i++)
    {
        if(Fequal(main_sequence_parameters[i],UNUSED_VALUE))
        {
            printf("main sequence parameter %d is unused\n",i);
        }
        else
        {
            printf("main sequence parameter %d is %g\n",i,giant_branch_parameters[i]);
        }
    }
    for(i=0;i<NUMBER_OF_METALLICITY_PARAMETERS;i++)
    {
        if(Fequal(metallicity_parameters[i],UNUSED_VALUE))
        {
            printf("metallicity parameter %d is unused\n",i);
        }
        else
        {
            printf("metallicity parameter %d is %g\n",i,giant_branch_parameters[i]);
        }
    }
    Exit_binary_c(BINARY_C_NORMAL_EXIT,"");
#endif
}

#endif//BSE
