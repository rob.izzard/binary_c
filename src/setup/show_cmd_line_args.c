#include "../binary_c.h"
No_empty_translation_unit_warning;


void show_cmd_line_args(const struct stardata_t * const stardata,
                        FILE * stream)
{
    int i;
    if(stream==NULL) stream = stderr; 
    for(i=0;i<stardata->common.argc;i++)
    {
        fprintf(stream,"%s ",stardata->common.argv[i]);
    }
    fprintf(stream,"\n");
}

