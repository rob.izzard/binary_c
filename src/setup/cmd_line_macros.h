#pragma once
#ifndef CMD_LINE_MACROS_H
#define CMD_LINE_MACROS_H
#include "cmd_line_depends.h"

/*
 * Define macros that depend on code build options
 *
 * Note: most of this is deprecated and will be replaced
 * with Varif() and Varif2() macros directly in the
 * command line args list.
 */
#ifdef RANDOM_SYSTEMS
#define RANDOM_SYSTEMS_VAR Var(random_systems)
#else
#define RANDOM_SYSTEMS_VAR Not_used_arg
#endif

#ifdef FILE_LOG
#define COLOUR_LOG_VAR Var(colour_log)
#define LOG_FILENAME_VAR Var(log_filename)
#define LOG_ARROWS_VAR Var(log_arrows)
#define LOG_GROUPS_VAR Var(log_groups)
#define API_LOG_FILENAME_PREFIX_VAR Var(api_log_filename_prefix)
#define LOG_LEGACY_STELLAR_TYPES_VAR Var(log_legacy_stellar_types)
#define LOG_SEPARATOR_VAR Var(log_separator)
#else
#define COLOUR_LOG_VAR Not_used_arg
#define LOG_FILENAME_VAR Not_used_arg
#define LOG_ARROWS_VAR Not_used_arg
#define LOG_GROUPS_VAR Not_used_arg
#define API_LOG_FILENAME_PREFIX_VAR Not_used_arg
#define LOG_LEGACY_STELLAR_TYPES_VAR Not_used_arg
#define LOG_SEPARATOR_VAR Not_used_arg
#endif

#ifdef MINT
#define MINT_DATA_CLEANUP_VAR Var(MINT_data_cleanup)
#define MINT_METALLICITY_VAR Var(MINT_metallicity)
#define MINT_DIR_VAR Var(MINT_dir)
#define MINT_LOAD_STATE_FILE_VAR Var(MINT_load_state_file)
#define MINT_SAVE_STATE_FILE_VAR Var(MINT_save_state_file)
#define MINT_DISABLE_WARNINGS_VAR Var(MINT_disable_warnings)
#define MINT_DISABLE_GRID_LOAD_WARNINGS_VAR Var(MINT_disable_grid_load_warnings)
#define MINT_MS_REJUVENATION_VAR Var(MINT_MS_rejuvenation)
#define MINT_USE_ZAMS_PROFILES_VAR Var(MINT_use_ZAMS_profiles)
#define MINT_REMESH_VAR Var(MINT_remesh)
#define MINT_CACHE_TABLES_VAR Var(MINT_cache_tables)
#define MINT_FILENAME_VB_VAR Var(MINT_filename_vb)
#define MINT_KIPPENHAHN_VAR Var(MINT_Kippenhahn)
#define MINT_KIPPENHAHN_STELLAR_TYPE_VAR Var(MINT_Kippenhahn_stellar_type)
#define MINT_KIPPENHAHN_COMPANION_STELLAR_TYPE_VAR Var(MINT_Kippenhahn_companion_stellar_type)
#define MINT_NUCLEAR_BURNING_VAR Var(MINT_nuclear_burning)
#define MINT_MINIMUM_SHELL_MASS_VAR Var(MINT_minimum_shell_mass)
#define MINT_MAXIMUM_SHELL_MASS_VAR Var(MINT_maximum_shell_mass)
#define MINT_FALLBACK_TO_TEST_DATA_VAR Var(MINT_fallback_to_test_data)
#define MINT_USE_FALLBACK_COMENV_VAR Var(MINT_use_fallback_comenv)
#define MINT_NSHELLS_VAR Var(MINT_nshells)
#define MINT_MINIMUM_NSHELLS_VAR Var(MINT_minimum_nshells)
#define MINT_MAXIMUM_NSHELLS_VAR Var(MINT_maximum_nshells)
#define MINT_SHOW_BYTE_OFFSETS_VAR Var(MINT_show_metadata)
#define MINT_DATA_TABLES_VAR Var(MINT_data_tables)
#else
#define MINT_DATA_CLEANUP_VAR Not_used_arg
#define MINT_METALLICITY_VAR Not_used_arg
#define MINT_DIR_VAR Not_used_arg
#define MINT_LOAD_STATE_FILE_VAR Not_used_arg
#define MINT_SAVE_STATE_FILE_VAR Not_used_arg
#define MINT_DISABLE_GRID_LOAD_WARNINGS_VAR Not_used_arg
#define MINT_DISABLE_WARNINGS_VAR Not_used_arg
#define MINT_MS_REJUVENATION_VAR Not_used_arg
#define MINT_USE_ZAMS_PROFILES_VAR Not_used_arg
#define MINT_REMESH_VAR Not_used_arg
#define MINT_CACHE_TABLES_VAR Not_used_arg
#define MINT_FILENAME_VB_VAR Not_used_arg
#define MINT_KIPPENHAHN_VAR Not_used_arg
#define MINT_KIPPENHAHN_STELLAR_TYPE_VAR Not_used_arg
#define MINT_KIPPENHAHN_COMPANION_STELLAR_TYPE_VAR Not_used_arg
#define MINT_NUCLEAR_BURNING_VAR Not_used_arg
#define MINT_MINIMUM_SHELL_MASS_VAR Not_used_arg
#define MINT_MAXIMUM_SHELL_MASS_VAR Not_used_arg
#define MINT_FALLBACK_TO_TEST_DATA_VAR Not_used_arg
#define MINT_USE_FALLBACK_COMENV_VAR Not_used_arg
#define MINT_NSHELLS_VAR Not_used_arg
#define MINT_MINIMUM_NSHELLS_VAR Not_used_arg
#define MINT_MAXIMUM_NSHELLS_VAR Not_used_arg
#define MINT_SHOW_BYTE_OFFSETS_VAR Not_used_arg
#define MINT_DATA_TABLES_VAR Not_used_arg
#endif//MINT

#ifdef TIDES_DIAGNOSIS_OUTPUT
#define TIDES_DIAGNOSIS_LOG_VAR Var(tides_logging_function)
#else
#define TIDES_DIAGNOSIS_LOG_VAR Not_used_arg
#endif //TIDES_DIAGNOSIS_OUTPUT

#ifdef COMENV_MS_ACCRETION
#define CE_MS_ACCRETION_FRACTION_VAR Var(comenv_ns_accretion_fraction);
#define CE_MS_ACCRETION_MASS_VAR Var(comenv_ns_accretion_mass);
#else
#define CE_MS_ACCRETION_FRACTION_VAR Not_used_arg
#define CE_MS_ACCRETION_MASS_VAR Not_used_arg
#endif

#ifdef COMENV_NS_ACCRETION
#define CE_NS_ACCRETION_FRACTION_VAR Var(comenv_ns_accretion_fraction);
#define CE_NS_ACCRETION_MASS_VAR Var(comenv_ns_accretion_mass);
#else
#define CE_NS_ACCRETION_FRACTION_VAR Not_used_arg
#define CE_NS_ACCRETION_MASS_VAR Not_used_arg
#endif

#ifdef PHASE_VOLUME
#define PHASEVOL_VAR Var(stardata->model.phasevol)
#else
#define PHASEVOL_VAR Not_used_arg
#endif

#ifdef VW93_MIRA_SHIFT
#define VW93_MIRA_SHIFT_VAR Var(vw93_mira_shift)
#else
#define VW93_MIRA_SHIFT_VAR Not_used_arg
#endif

#ifdef VW93_MULTIPLIER
#define VW93_MULTIPLIER_VAR Var(vw93_multiplier)
#else
#define VW93_MULTIPLIER_VAR Not_used_arg
#endif

#ifdef HRDIAG
#define HRDIAG_OUTPUT_VAR Var(hrdiag_output)
#else
#define HRDIAG_OUTPUT_VAR Not_used_arg
#endif

#ifdef MATTSSON_MASS_LOSS
#define MATTSSON_ORICH_TPAGBWIND_VAR Var(mattsson_Orich_tpagbwind)
#else
#define MATTSSON_ORICH_TPAGBWIND_VAR Not_used_arg
#endif


#define ROTATIONALLY_ENHANCED_MASS_LOSS_VAR Var(rotationally_enhanced_mass_loss)
#define ROTATIONALLY_ENHANCED_EXPONENT_VAR Var(rotationally_enhanced_exponent)

#ifdef PRE_MAIN_SEQUENCE
#define PRE_MAIN_SEQUENCE_VAR Var(pre_main_sequence)
#define PRE_MAIN_SEQUENCE_FIT_LOBES_VAR Var(pre_main_sequence_fit_lobes)
#else
#define PRE_MAIN_SEQUENCE_VAR Not_used_arg
#define PRE_MAIN_SEQUENCE_FIT_LOBES_VAR Not_used_arg
#endif


#ifdef TIMESTEP_MODULATION
#define TIMESTEP_MODULATOR_VAR Var(timestep_modulator)
#else
#define TIMESTEP_MODULATOR_VAR Not_used_arg
#endif

#ifdef RLOF_RADATION_CORRECTION
#define RLOF_F_VAR Var(RLOF_f)
#else
#define RLOF_F_VAR Not_used_arg
#endif

#ifdef RLOF_MDOT_MODULATION
#define RLOF_MDOT_MODULATOR_VAR Var(RLOF_mdot_factor)
#else
#define RLOF_MDOT_MODULATOR_VAR Not_used_arg
#endif


#ifdef WD_KICKS
#define WD_SIGMA_VAR Var(sn_kick_dispersion[SN_WDKICK])
#define WD_KICK_DIRECTION_VAR Var(wd_kick_direction)
#define WD_KICK_WHEN_VAR Var(wd_kick_when)
#define WD_KICK_PULSE_NUMBER_VAR Var(wd_kick_pulse_number)
#else
#define WD_SIGMA_VAR Not_used_arg
#define WD_KICK_DIRECTION_VAR Not_used_arg
#define WD_KICK_WHEN_VAR Not_used_arg
#define WD_KICK_PULSE_NUMBER_VAR Not_used_arg
#endif

#ifdef WRLOF_MASS_TRANSFER
#define WRLOF_METHOD_VAR Var(WRLOF_method)
#else
#define WRLOF_METHOD_VAR Not_used_arg
#endif

#ifdef FABIAN_IMF_LOG
#define NEXT_FABIAN_IMF_LOG_TIME_VAR Var(stardata->model.next_fabian_imf_log_time)
#define NEXT_FABIAN_IMF_LOG_TIMESTEP_VAR Var(stardata->model.fabian_imf_log_timestep)
#else
#define NEXT_FABIAN_IMF_LOG_TIME_VAR Not_used_arg
#define NEXT_FABIAN_IMF_LOG_TIMESTEP_VAR Not_used_arg
#endif

#define NUCSYN_DTFAC_VAR Var(dtfac)

#ifdef NUCSYN
#define __CMD_LINE_NUCSYN_SIGMAV_CODE_STRINGS_LIST (char **)NUCSYN_SIGMAV_code_strings
#define __CMD_LINE_SIGMAV_SIZE SIGMAV_SIZE
#define __CMD_LINE_NUCLEAR_NETWORK_STRINGS (char **)nuclear_network_strings
#define __CMD_LINE_NUCSYN_NETWORK_NUMBER 0
#else
#define __CMD_LINE_NUCSYN_SIGMAV_CODE_STRINGS_LIST (char **)NULL
#define __CMD_LINE_SIGMAV_SIZE 0
#define __CMD_LINE_NUCLEAR_NETWORK_STRINGS (char**)NULL
#define __CMD_LINE_NUCSYN_NETWORK_NUMBER 0
#endif

#ifdef NUCSYN
#define ENSEMBLE_LEGACY_YIELDS_VAR Nucsyn_var(legacy_yields)
#else
#define ENSEMBLE_LEGACY_YIELDS_VAR Not_used_arg
#endif // NUCSYN

#ifdef STELLAR_POPULATIONS_ENSEMBLE
/* have ensemble */
#define ENSEMBLE_VAR Var(ensemble)
#define ENSEMBLE_FILTERS_OFF_VAR Var(ensemble_filters_off)
#define ENSEMBLE_DEFER_VAR Var(ensemble_defer)
#define ENSEMBLE_OUTPUT_LINES_VAR Var(ensemble_output_lines)
#define ENSEMBLE_LEGACY_ENSEMBLE_VAR Var(ensemble_legacy_ensemble)
#define ENSEMBLE_DT_VAR Var(ensemble_dt)
#define ENSEMBLE_LOGTIMES_VAR Var(ensemble_logtimes)
#define ENSEMBLE_LOGDT_VAR Var(ensemble_logdt)
#define ENSEMBLE_ENFORCE_MAXTIME_VAR Var(ensemble_enforce_maxtime)
#define ENSEMBLE_STARTTIME_VAR Var(ensemble_starttime)
#define ENSEMBLE_STARTLOGTIME_VAR Var(ensemble_startlogtime)
#define STELLAR_POPULATIONS_ENSEMBLE_FILTER_VAR Var(ensemble_filter_override)
#define STELLAR_POPULATIONS_ENSEMBLE_FILTER_VAR_MAX (stardata->preferences->ensemble_filter_override + STELLAR_POPULATIONS_ENSEMBLE_FILTER_NUMBER)
#define ENSEMBLE_ALLTIMES_VAR Var(ensemble_alltimes)
#define ENSEMBLE_NOTIMEBINS_VAR Var(ensemble_notimebins)
#define ENSEMBLE_DUMMY_N_VAR Var(ensemble_dummy_n)
#else
/* no ensemble */
#define ENSEMBLE_VAR Not_used_arg
#define ENSEMBLE_FILTERS_OFF_VAR Not_used_arg
#define ENSEMBLE_DEFER_VAR Not_used_arg
#define ENSEMBLE_OUTPUT_LINES_VAR Not_used_arg
#define ENSEMBLE_LEGACY_ENSEMBLE_VAR Not_used_arg
#define ENSEMBLE_LEGACY_YIELDS_VAR Not_used_arg
#define ENSEMBLE_LOGDT_VAR Not_used_arg
#define ENSEMBLE_DT_VAR Not_used_arg
#define ENSEMBLE_ENFORCE_MAXTIME_VAR Not_used_arg
#define ENSEMBLE_STARTTIME_VAR Not_used_arg
#define ENSEMBLE_STARTLOGTIME_VAR Not_used_arg
#define ENSEMBLE_LOGTIMES_VAR Not_used_arg
#define STELLAR_POPULATIONS_ENSEMBLE_FILTER_VAR Not_used_arg
#define STELLAR_POPULATIONS_ENSEMBLE_FILTER_VAR_MAX NULL
#define ENSEMBLE_ALLTIMES_VAR Not_used_arg
#define ENSEMBLE_NOTIMEBINS_VAR Not_used_arg
#define ENSEMBLE_DUMMY_N_VAR Not_used_arg
#endif // STELLAR_POPULATIONS_ENSEMBLE


#if defined NUCSYN && defined NUCSYN_TPAGB_HBB
#define NeNaMgAl_VAR Var(NeNaMgAl)
#else
#define NeNaMgAl_VAR Not_used_arg
#endif



#if (defined NUCSYN) && (defined NUCSYN_S_PROCESS)
#define NUCSYN_C13_EFF_VAR Var(c13_eff)
#define NUCSYN_MC13_POCKET_MULTIPLIER_VAR Var(mc13_pocket_multiplier)
#else
#define NUCSYN_C13_EFF_VAR Not_used_arg
#define NUCSYN_MC13_POCKET_MULTIPLIER_VAR Not_used_arg
#endif

#if (defined NUCSYN) && (defined USE_TABULAR_INTERSHELL_ABUNDANCES_KARAKAS_2012)
#define NUCSYN_MASS_OF_PMZ_VAR Var(pmz_mass)
#else
#define NUCSYN_MASS_OF_PMZ_VAR Not_used_arg
#endif

#if (defined NUCSYN) && (defined NUCSYN_GCE_OUTFLOW_CHECKS)
#define NUCSYN_ESCAPE_VELOCITY_VAR Var(escape_velocity)
#define NUCSYN_ESCAPE_FRACTION_VAR Var(escape_fraction)
#else
#define NUCSYN_ESCAPE_VELOCITY_VAR Not_used_arg
#define NUCSYN_ESCAPE_FRACTION_VAR Not_used_arg
#endif

#ifdef NUCSYN
#define EMP_LOGG_MAXIMUM_VAR Var(EMP_logg_maximum)
#define EMP_MINIMUM_AGE_VAR Var(EMP_minimum_age)
#define EMP_FEH_MAXIMUM_VAR Var(EMP_feh_maximum)
#define CEMP_CFE_MINIMUM_VAR Var(CEMP_cfe_minimum)
#define NEMP_NFE_MINIMUM_VAR Var(NEMP_nfe_minimum)
#else
#define EMP_LOGG_MAXIMUM_VAR Not_used_arg
#define EMP_MINIMUM_AGE_VAR Not_used_arg
#define EMP_FEH_MAXIMUM_VAR Not_used_arg
#define CEMP_CFE_MINIMUM_VAR Not_used_arg
#define NEMP_NFE_MINIMUM_VAR Not_used_arg
#endif

#if defined(NUCSYN) && defined(CN_THICK_DISC)
#define THICK_DISC_START_AGE Var(thick_disc_start_age)
#define THICK_DISC_END_AGE Var(thick_disc_end_age)
#define THICK_DISC_LOGG_MIN Var(thick_disc_logg_min)
#define THICK_DISC_LOGG_MAX Var(thick_disc_logg_max)
#else
#define THICK_DISC_START_AGE Not_used_arg
#define THICK_DISC_END_AGE Not_used_arg
#define THICK_DISC_LOGG_MIN Not_used_arg
#define THICK_DISC_LOGG_MAX Not_used_arg
#endif

#ifdef EVOLUTION_SPLITTING
#define EVOLUTION_SPLITTING_VAR Var(evolution_splitting)
#define EVOLUTION_SPLITTING_MAXDEPTH_VAR Var(evolution_splitting_maxdepth)
#define EVOLUTION_SPLITTING_SN_N_VAR Var(evolution_splitting_sn_n)
#define EVOLUTION_SPLITTING_SN_ECCENTRICITY_THRESHOLD_VAR Var(evolution_splitting_sn_eccentricity_threshold)
#else
#define EVOLUTION_SPLITTING_VAR Not_used_arg
#define EVOLUTION_SPLITTING_SN_N_VAR Not_used_arg
#define EVOLUTION_SPLITTING_MAXDEPTH_VAR Not_used_arg
#define EVOLUTION_SPLITTING_SN_ECCENTRICITY_THRESHOLD_VAR Not_used_arg
#endif

#ifdef DISCS
#ifdef DISC_LOG
#define DISC_LEGACY_LOGGING_VAR Var(disc_legacy_logging)
#define DISC_LOG_VAR Var(disc_log)
#define DISC_LOG2D_VAR Var(disc_log2d)
#define DISC_LOG_DT_VAR Var(disc_log_dt)
#else
#define DISC_LEGACY_LOGGING_VAR Not_used_arg
#define DISC_LOG_VAR Not_used_arg
#define DISC_LOG2D_VAR Not_used_arg
#define DISC_LOG_DT_VAR Not_used_arg
#endif//DISC_LOG

#if defined DISC_LOG || defined DISC_LOG_2D
#define DISC_LOG_DIRECTORY_VAR Var(disc_log_directory)
#else
#define DISC_LOG_DIRECTORY_VAR Not_used_arg
#endif

#define DISC_TIMESTEP_FACTOR_VAR Var(disc_timestep_factor)
#define DISC_N_MONTE_CARLO_GUESSES_VAR Var(disc_n_monte_carlo_guesses)
#define CBDISC_GAMMA_VAR Var(cbdisc_gamma)
#define CBDISC_ALBEDO_VAR Var(cbdisc_albedo)
#define CBDISC_MAX_LIFETIME_VAR Var(cbdisc_max_lifetime)
#define CBDISC_ALPHA_VAR Var(cbdisc_alpha)
#define CBDISC_KAPPA_VAR Var(cbdisc_kappa)
#define CBDISC_TORQUEF_VAR Var(cbdisc_torquef)
#define CBDISC_START_DM_VAR Var(cbdisc_init_dM)
#define CBDISC_START_DJDM_VAR Var(cbdisc_init_dJdM)
#define CBDISC_OUTER_EDGE_STRIPPING_VAR Var(cbdisc_outer_edge_stripping)
#define CBDISC_INNER_EDGE_STRIPPING_VAR Var(cbdisc_inner_edge_stripping)
#define CBDISC_OUTER_EDGE_TIMESCALE_VAR Var(cbdisc_outer_edge_stripping_timescale)
#define CBDISC_INNER_EDGE_TIMESCALE_VAR Var(cbdisc_inner_edge_stripping_timescale)

#define CBDISC_MASS_LOSS_RATE_VAR Var(cbdisc_mass_loss_constant_rate)
#define CBDISC_MASS_LOSS_INNER_VISCOUS_MULTIPLIER_VAR Var(cbdisc_mass_loss_inner_viscous_multiplier)
#define CBDISC_MASS_LOSS_INNER_VISCOUS_ANGULAR_MOMENTUM_MULTIPLIER_VAR Var(cbdisc_mass_loss_inner_viscous_angular_momentum_multiplier)
#define CBDISC_MASS_LOSS_INNER_L2_CROSS_MULTIPLIER_VAR Var(cbdisc_mass_loss_inner_L2_cross_multiplier)
#define CBDISC_MASS_LOSS_ISM_RAM_PRESSURE_MULTIPLIER_VAR Var(cbdisc_mass_loss_ISM_ram_pressure_multiplier)
#define CBDISC_MASS_LOSS_ISM_PRESSURE_VAR Var(cbdisc_mass_loss_ISM_pressure)
#define CBDISC_MINIMUM_LUMINOSITY_VAR Var(cbdisc_minimum_luminosity)
#define CBDISC_MINIMUM_MASS_VAR Var(cbdisc_minimum_mass)
#define CBDISC_MINIMUM_FRING_VAR Var(cbdisc_minimum_fRing)
#define CBDISC_MASS_LOSS_INNER_VISCOUS_ACCRETION_METHOD_VAR Var(cbdisc_mass_loss_inner_viscous_accretion_method)
#define CBDISC_RESONANCE_MULTIPLIER_VAR Var(cbdisc_resonance_multiplier)
#define CBDISC_RESONANCE_DAMPING_VAR Var(cbdisc_resonance_damping)
#define CBDISC_FAIL_RING_INSIDE_SEPARATION Var(cbdisc_fail_ring_inside_separation)
#define CBDISC_MASS_LOSS_FUV_MULTIPLIER_VAR Var(cbdisc_mass_loss_FUV_multiplier)
#define CBDISC_MASS_LOSS_XRAY_MULTIPLIER_VAR Var(cbdisc_mass_loss_Xray_multiplier)
#define CBDISC_VISCOUS_PHOTOEVAPORATIVE_COUPLING_VAR Var(cbdisc_viscous_photoevaporative_coupling)
#define CBDISC_VISCOUS_L2_COUPLING_VAR Var(cbdisc_viscous_L2_coupling)

#ifdef DISCS_CIRCUMBINARY_FROM_COMENV
#define COMENV_DISC_ANGMOM_FRACTION_VAR Var(comenv_disc_angmom_fraction)
#define COMENV_DISC_MASS_FRACTION_VAR Var(comenv_disc_mass_fraction)
#else
#define COMENV_DISC_ANGMOM_FRACTION_VAR Not_used_arg
#define COMENV_DISC_MASS_FRACTION_VAR Not_used_arg
#endif // DISCS_CIRCUMBINARY_FROM_COMENV

#ifdef DISCS_CIRCUMBINARY_FROM_WIND
#define WIND_DISC_ANGMOM_FRACTION_VAR Var(wind_disc_angmom_fraction)
#define WIND_DISC_MASS_FRACTION_VAR Var(wind_disc_mass_fraction)
#else
#define WIND_DISC_ANGMOM_FRACTION_VAR Not_used_arg
#define WIND_DISC_MASS_FRACTION_VAR Not_used_arg
#endif // DISCS_CIRCUMBINARY_FROM_WIND

#define CBDISC_MINIMUM_EVAPORATION_TIMESCALE_VAR Var(cbdisc_minimum_evaporation_timescale)
#define CBDISC_NO_WIND_IF_CBDISC_VAR Var(cbdisc_no_wind_if_cbdisc)
#define CBDISC_END_EVOLUTION_AFTER_DISC Var(cbdisc_end_evolution_after_disc)

#else // DISCS

#define DISC_LOG_VAR Not_used_arg
#define DISC_LOG_DT_VAR Not_used_arg
#define DISC_TIMESTEP_FACTOR_VAR Not_used_arg
#define DISC_N_MONTE_CARLO_GUESSES_VAR Not_used_arg
#define CBDISC_GAMMA_VAR Not_used_arg
#define CBDISC_ALBEDO_VAR Not_used_arg
#define CBDISC_MAX_LIFETIME_VAR Not_used_arg
#define CBDISC_ALPHA_VAR Not_used_arg
#define CBDISC_KAPPA_VAR Not_used_arg
#define CBDISC_TORQUEF_VAR Not_used_arg
#define CBDISC_START_DM_VAR Not_used_arg
#define CBDISC_START_DJDM_VAR Not_used_arg
#define CBDISC_MASS_LOSS_RATE_VAR Not_used_arg
#define CBDISC_MASS_LOSS_INNER_VISCOUS_MULTIPLIER_VAR Not_used_arg
#define CBDISC_MASS_LOSS_INNER_VISCOUS_ANGULAR_MOMENTUM_MULTIPLIER_VAR Not_used_arg
#define CBDISC_MASS_LOSS_INNER_L2_CROSS_MULTIPLIER_VAR Not_used_arg
#define CBDISC_MASS_LOSS_ISM_RAM_PRESSURE_MULTIPLIER_VAR Not_used_arg
#define CBDISC_MASS_LOSS_ISM_PRESSURE_VAR Not_used_arg
#define CBDISC_MASS_LOSS_FUV_MULTIPLIER_VAR Not_used_arg
#define CBDISC_MASS_LOSS_XRAY_MULTIPLIER_VAR Not_used_arg
#define CBDISC_MINIMUM_LUMINOSITY_VAR Not_used_arg
#define CBDISC_MINIMUM_MASS_VAR Not_used_arg
#define CBDISC_MINIMUM_FRING_VAR Not_used_arg
#define COMENV_DISC_ANGMOM_FRACTION_VAR Not_used_arg
#define COMENV_DISC_MASS_FRACTION_VAR Not_used_arg
#define WIND_DISC_ANGMOM_FRACTION_VAR Not_used_arg
#define WIND_DISC_MASS_FRACTION_VAR Not_used_arg
#define CBDISC_MASS_LOSS_INNER_VISCOUS_ACCRETION_METHOD_VAR Not_used_arg
#define CBDISC_RESONANCE_MULTIPLIER_VAR Not_used_arg
#define CBDISC_RESONANCE_DAMPING_VAR Not_used_arg
#define CBDISC_VISCOUS_PHOTOEVAPORATIVE_COUPLING_VAR Not_used_arg
#define CBDISC_VISCOUS_L2_COUPLING_VAR Not_used_arg
#define CBDISC_FAIL_RING_INSIDE_SEPARATION Not_used_arg
#define CBDISC_OUTER_EDGE_STRIPPING_VAR Not_used_arg
#define CBDISC_INNER_EDGE_STRIPPING_VAR Not_used_arg
#define CBDISC_OUTER_EDGE_TIMESCALE_VAR Not_used_arg
#define CBDISC_INNER_EDGE_TIMESCALE_VAR Not_used_arg
#define CBDISC_MINIMUM_EVAPORATION_TIMESCALE_VAR Not_used_arg
#define CBDISC_NO_WIND_IF_CBDISC_VAR Not_used_arg
#define DISC_LOG_VAR Not_used_arg
#define DISC_LOG2D_VAR Not_used_arg
#define DISC_LOG_DT_VAR Not_used_arg
#define DISC_LOG_DIRECTORY_VAR Not_used_arg
#define DISC_LEGACY_LOGGING_VAR Not_used_arg
#define CBDISC_END_EVOLUTION_AFTER_DISC Not_used_arg
#endif // DISCS


#ifdef CBDISC_ECCENTRICITY_PUMPING
#define CBDISC_ECCENTRICITY_PUMPING_METHOD_VAR Var(cbdisc_eccentricity_pumping_method)
#else
#define CBDISC_ECCENTRICITY_PUMPING_METHOD_VAR Not_used_arg
#endif

#ifdef COMENV_POLYTROPES
#define COMENV_SPLITMASS_VAR Var(comenv_splitmass)
#else
#define COMENV_SPLITMASS_VAR Not_used_arg
#endif

#ifdef EQUATION_OF_STATE_ALGORITHMS
#define EQUATION_OF_STATE_VAR Var(equation_of_state_algorithm)
#else
#define EQUATION_OF_STATE_VAR Not_used_arg
#endif

#define OPACITY_VAR Varif(OPACITY_ALGORITHMS,stardata->preferences->opacity_algorithm)
#define REVERSE_TIME_VAR Varif(REVERSE_TIME,stardata->preferences->reverse_time)

/*
#ifdef REVERSE_TIME
#define REVERSE_TIME_VAR Var(reverse_time)
#else
#define REVERSE_TIME_VAR Not_used_arg
#endif
*/
#ifdef CIRCUMBINARY_DISK_DERMINE
#define ALPHACB_VAR Var(alphaCB)
#else
#define ALPHACB_VAR Not_used_arg
#endif//CIRCUMBINARY_DISK_DERMINE

#ifdef GAIAHRD
#define GAIA_TEFF_BINWIDTH Var(gaia_Teff_binwidth)
#define GAIA_L_BINWIDTH Var(gaia_L_binwidth)
#else
#define GAIA_TEFF_BINWIDTH Not_used_arg
#define GAIA_L_BINWIDTH Not_used_arg
#endif//GAIAHRD

#ifdef STELLAR_COLOURS
#define GAIA_COLOUR_TRANSFORM_METHOD_VAR Var(gaia_colour_transform_method)
#else
#define GAIA_COLOUR_TRANSFORM_METHOD_VAR Not_used_arg
#endif

#ifdef LOG_POSTAGB_STARS
#define POSTAGB_LEGACY_LOGGING_VAR Var(postagb_legacy_logging)
#else
#define POSTAGB_LEGACY_LOGGING_VAR Not_used_arg
#endif // LOG_POSTAGB_STARS

#ifdef WTTS_LOG
#define WTTS_LOG_VAR Var(wtts_log)
#else
#define WTTS_LOG_VAR Not_used_arg
#endif // WTTS_LOG

#ifdef ORBITING_OBJECTS
#define ORBITING_OBJECTS_LOG_VAR Var(orbiting_objects_log)
#define EVAPORATE_ESCAPED_ORBITING_OBJECTS_VAR Var(evaporate_escaped_orbiting_objects)
#define RLOF_TRANSITION_OBJECTS_ESCAPE Var(RLOF_transition_objects_escape)
#define ORBITING_OBJECTS_TIDES_MULTIPLIER_VAR Var(orbiting_objects_tides_multiplier)
#define ORBITING_OBJECTS_WIND_ACCRETION_MULTIPLIER_VAR Var(orbiting_objects_wind_accretion_multiplier)
#define ORBITING_OBJECTS_CLOSE_PC_THRESHOLD_VAR Var(orbiting_objects_close_pc_threshold)
#else
#define ORBITING_OBJECTS_LOG_VAR Not_used_arg
#define EVAPORATE_ESCAPED_ORBITING_OBJECTS_VAR Not_used_arg
#define RLOF_TRANSITION_OBJECTS_ESCAPE Not_used_arg
#define ORBITING_OBJECTS_TIDES_MULTIPLIER_VAR Not_used_arg
#define ORBITING_OBJECTS_WIND_ACCRETION_MULTIPLIER_VAR Not_used_arg
#define ORBITING_OBJECTS_CLOSE_PC_THRESHOLD_VAR Not_used_arg

#endif // ORBITING_OBJECTS

#ifdef SAVE_MASS_HISTORY
#define ADJUST_STRUCTURE_FROM_MASS_CHANGES_VAR Var(adjust_structure_from_mass_changes)
#define SAVE_MASS_HISTORY_N_THERMAL_VAR Var(save_mass_history_n_thermal)
#else
#define ADJUST_STRUCTURE_FROM_MASS_CHANGES_VAR Not_used_arg
#define SAVE_MASS_HISTORY_N_THERMAL_VAR Not_used_arg
#endif//SAVE_MASS_HISTORY

#ifdef YBC
#define YBC_PATH_VAR Var(YBC_path)
#define YBC_LISTFILE_VAR Var(YBC_listfile)
#define YBC_INSTRUMENTS_VAR Var(YBC_instruments)
#define YBC_ALL_INSTRUMENTS_VAR Var(YBC_all_instruments)
#else
#define YBC_PATH_VAR Not_used_arg
#define YBC_LISTFILE_VAR Not_used_arg
#define YBC_INSTRUMENTS_VAR Not_used_arg
#define YBC_ALL_INSTRUMENTS_VAR Not_used_arg
#endif //YBC

#ifdef PPISN
#define PPISN_PRESCRIPTION_VAR Var(PPISN_prescription)
#define PPISN_ADDITIONAL_MASSLOSS_VAR Var(PPISN_additional_massloss)
#define PPISN_CORE_MASS_RANGE_SHIFT_VAR Var(PPISN_core_mass_range_shift)
#define PPISN_MASSLOSS_MULTIPLIER_VAR Var(PPISN_massloss_multiplier)
#else
#define PPISN_PRESCRIPTION_VAR Not_used_arg
#define PPISN_ADDITIONAL_MASSLOSS_VAR Not_used_arg
#define PPISN_CORE_MASS_RANGE_SHIFT_VAR Not_used_arg
#define PPISN_MASSLOSS_MULTIPLIER_VAR Not_used_arg
#endif //PPISN

#define FIXED_BETA_MT_VAR Var(fixed_beta_mass_transfer_efficiency)

#ifdef EVENT_BASED_LOGGING
#define EVENT_BASED_LOGGING_VAR Var(event_based_logging)
#define EVENT_BASED_LOGGING_MAX_POINTER (stardata->preferences->event_based_logging + EVENT_BASED_LOGGING_NUMBER)
#else
#define EVENT_BASED_LOGGING_VAR Not_used_arg
#define EVENT_BASED_LOGGING_MAX_POINTER NO_MAX_POINTER
#endif // EVENT_BASED_LOGGING



#endif// CMD_LINE_MACROS_H
