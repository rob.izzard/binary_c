#include "../binary_c.h"
No_empty_translation_unit_warning;

/*
 *  set_up_variables
 *
 * This function sets up the variables for binary_c/nucsyn, in particular
 * the various structures defining the choice of physics and parameters
 * (preferences), the stars (stardata->star[0,1]) and the common variables
 * (stardata->common, stardata->model)
 */

void set_up_variables(const int argc,
                      char ** argv,
                      struct preferences_t * preferences,
                      struct stardata_t * const stardata)
{
    Dprint("set_up_variables preferences=%p stardata->preferences=%p\n",
           (void*)preferences,
           (void*)stardata->preferences);

    if(preferences==NULL)
    {
        /*
         * This implies we should reset the stars because
         * this is the first time through.
         */
        preferences = stardata->preferences;


        Dprint("set_up_variables (2) preferences=%p stardata->preferences=%p\n",
               (void*)preferences,
               (void*)stardata->preferences);

        if(preferences==NULL)
        {
            Exit_binary_c(BINARY_C_POINTER_FAILURE,"Preferences double-null error!\n");
        }

        Dprint("Set up variables : first call! Reset stars...\n");
        set_defaults(stardata);
    }
    else
    {
        set_default_preferences(stardata);
    }

    Dprint("done\nargc: %d\n",argc);

#ifndef MAKE_BSE_TABLES
    if(argc<=1 && argc!=-1)
    {
        Exit_binary_c(BINARY_C_SETUP_UNKNOWN_ARGUMENT,
                      "You must run binary_c with arguments, try binary_c --help\n");
    }
#endif

#ifdef BATCHMODE
    // Identify whether we watch batchmode...
    if(preferences->batchmode == BATCHMODE_LIBRARY)
    {
        /* shared library call : do nothing */
    }
    else if(preferences->batchmode==BATCHMODE_ON ||
            (argv!=NULL &&
             argc>=2 &&
             Strings_equal(argv[1],"--batch")))
    {
        preferences->batchmode = BATCHMODE_ON;
        printf("set batchmode to 1\n");
        fflush(stdout);
    }
    else
    {
#endif//BATCHMODE
        /** Loop over command line arguments **/
        /** NB ignore argument 0, since that's just the name of **/
        /** the program **/

        if(argc>=0) parse_arguments(1,argc,argv,stardata);

#ifdef BATCHMODE
    }
#endif//BATCHMODE

#ifdef STELLAR_COLOUR_TESTS
    stellar_magnitude_tests(stardata,&(stardata->star[0]));
#endif

    const double min_dt = DBL_EPSILON * stardata->preferences->max_evolution_time;
    if(stardata->preferences->minimum_timestep < min_dt)
    {
        Exit_binary_c(BINARY_C_FLOATING_POINT_ERROR,
                      "You have chosen minimum_timestep to be < %g which will cause problems. Please increase minimum_timestep to avoid this problem.",
                      min_dt);
    }

    Dprint ("Leaving set_up_variables\n");
}
