#include "../binary_c.h"
No_empty_translation_unit_warning;

Boolean arg_match_scanf(const char * const arg,
                        struct cmd_line_arg_t * const cmd_line_arg,
                        size_t * const offset_p)
{
    /*
     * Use sscanf to test the argument, return TRUE if it matches
     * and set *offset_p.
     *
     * Note: offset_p is an int pointer so it matches the %d
     *       pattern that is commonly used. The integer should
     *       always be zero or positive, however.
     */
    int matching_int = -1;
    const int scantest = sscanf(arg,
                                cmd_line_arg->name,
                                &matching_int);
    if(scantest > 0 && matching_int >= 0)
    {
        /*
         * Normal sscanf succeeded
         */
        *offset_p = (size_t) matching_int;
        return TRUE;
    }
    else if(cmd_line_arg->nargpairs > 0)
    {
        /*
         * Scan argpairs
         */
        for(unsigned int i=0; i<cmd_line_arg->nargpairs; i++)
        {
            if(Strings_equal(arg,
                             cmd_line_arg->argpairs[i].string))
            {
                /*
                 * Matched : set the appropriate offset
                 */
                *offset_p = (size_t) (cmd_line_arg->argpairs[i].value + TINY);
                return TRUE;
            }
        }
        return FALSE;
    }
    else
    {
        return FALSE;
    }
}
