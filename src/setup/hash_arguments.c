#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef HASH_ARGUMENTS

#include "cmd_line_args.h"
#include "cmd_line_function_macros.h"
#include "cmd_line_macro_pairs.h"
/*
 * Function to set up the cmd-line args hash
 */

void hash_arguments(struct stardata_t * const Restrict stardata,
                    const Boolean force)
{
    /*
     * Make args hash
     */

    struct argstore_t * argstore = stardata->store->argstore;
    struct cdict_t * argcdict;

    if(unlikely(force == TRUE))
    {
        CDict_free(stardata->store->argcdict);
    }
    Boolean new;
    if(likely(stardata->store->argcdict != NULL))
    {
        argcdict = stardata->store->argcdict;
        new = FALSE;
    }
    else
    {
        new = TRUE;
        CDict_new(c);
        stardata->store->argcdict = argcdict = c;
        CDict_nest(argcdict,
                   "preferences",(void*)stardata->preferences);
    }

    const struct preferences_t * const p =
        (struct preferences_t *)CDict_nest_get_data_value(argcdict,
                                                          p,
                                                          "preferences");

    if(stardata->preferences != p)
    {
        /*
         * New preferences: pointers will be updated
         * so we need to reset everything.
         */
        new = TRUE;
        if(stardata->store->argcdict != NULL)
        {
            CDict_free(stardata->store->argcdict);
        }
        CDict_new(c);
        stardata->store->argcdict = argcdict = c;
        CDict_nest(argcdict,
                   "preferences",(void*)stardata->preferences);
    }

    if(force == TRUE ||
       new == TRUE)
    {
        /*
         * Make parse-number based arg lists
         */
        for(int parse_number=0; parse_number<ARG_NUM_PARSES; parse_number++)
        {
            for(unsigned int i=0; i<argstore->arg_count; i++)
            {

                if(argstore->cmd_line_args[i].parse_number == ARG_PARSE_ALL ||
                   argstore->cmd_line_args[i].parse_number == parse_number)
                {
                    if(CDict_nest_get_entry(argcdict,
                                            "parse",(int)parse_number,
                                            "arguments",(char*)argstore->cmd_line_args[i].name))
                    {
                        Exit_binary_c(BINARY_C_WRONG_ARGUMENT,
                                      "Argument \"%s\" already exists in the hash: check cmd_line_args_list.def to see why there are two entries with the same name.",
                                      argstore->cmd_line_args[i].name);
                    }

                    CDict_nest(argcdict,
                               "pointer",(void*)&argstore->cmd_line_args[i],
                               "parse",(int)parse_number,
                               "arguments",(char*)argstore->cmd_line_args[i].name
                               );
                    CDict_nest(argcdict,
                               "counter",(unsigned int)i,
                               "parse",(int)parse_number,
                               "arguments",(char*)argstore->cmd_line_args[i].name
                               );
                }
            }
        }
    }
}

#endif // HASH_ARGUMENTS
