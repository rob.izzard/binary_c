#include "../binary_c.h"
No_empty_translation_unit_warning;

int parse_arguments_from_string(const char * Restrict const argstring,
                                 struct stardata_t * Restrict const stardata)
{
    /*
     * Split the string into the argv and argc arrays, then
     * free the memory in those arrays.
     */
    Dprint("split string \"%s\" into args\n",argstring);
    int argc;
    int n = 0;
    while(argstring[n]==' ')
    {
        n++;
    }
#ifdef USE_SPLIT_COMMANDLINE
    /*
     * This is slower, but performs shell expansion
     */
    char ** argv = split_commandline(argstring+n, &argc);
    Dprint("parse %d args from split string\n",argc);
    parse_arguments(1,argc,argv,stardata);
    split_commandline_free(argv,argc);
#else
    struct string_array_t * string_array = new_string_array(0);
    string_split_preserve(stardata,
                          argstring+n,
                          string_array,
                          ' ',
                          1024,
                          0,
                          TRUE);
    argc = (int)string_array->n;
    parse_arguments(0,
                    string_array->n,
                    string_array->strings,
                    stardata);
    free_string_array(&string_array);
#endif // USE_SPLIT_COMMANDLINE

    return argc;
}
