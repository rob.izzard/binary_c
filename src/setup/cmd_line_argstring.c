#include "../binary_c.h"
No_empty_translation_unit_warning;


char * cmd_line_argstring(struct stardata_t * const stardata)
{
    /*
     * Return a string containing the command line arguments
     */
    char * s = NULL;
    int i;
    for(i=0;i<stardata->common.argc;i++)
    {
        char * s2;
        int n;
        if(s == NULL)
        {
            n = asprintf(&s,"%s",stardata->common.argv[i]);
        }
        else
        {
            n = asprintf(&s2,
                         "%s %s",
                         s,
                         stardata->common.argv[i]);
            Safe_free(s);
            s = s2;
        }

        if(n<0)
        {
            Exit_binary_c(2,
                          "Could not malloc in cmd_line_argstring\n");
        }
    }
    return s;
}
