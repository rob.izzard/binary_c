#pragma once
#ifndef SETUP_PROTOTYPES_H
#define SETUP_PROTOTYPES_H
#include "../binary_c_macros.h"
#include "../binary_c_parameters.h"
#include "../binary_c_structures.h"
#include "cmd_line_args.h"

void set_up_variables(const int argc,
              char ** argv,
              struct preferences_t * preferences,
              struct stardata_t * const stardata);

#ifdef BSE
void set_metallicity_parameters(struct stardata_t * Restrict const stardata);
void set_metallicity_parameters2(struct stardata_t * Restrict const stardata);
#endif
void set_defaults(struct stardata_t * Restrict const stardata);
void set_up_memmap(struct stardata_t * Restrict const stardata);

void parse_arguments(int start,
                     const int argc,
                     char ** argv,
                     struct stardata_t * Restrict const stardata);

void stardata_status(struct stardata_t * Restrict const stardata,
                     const Star_number k);

void version(struct stardata_t * Restrict const stardata);
void version_speedtests(struct stardata_t * Restrict const stardata);

void set_preferences(struct preferences_t * Restrict const preferences);
void set_default_preferences(struct stardata_t * const stardata);
void set_default_preferences_2022a(struct preferences_t * Restrict const preferences);
void set_default_preferences_2024a(struct preferences_t * Restrict const preferences);

void default_stardata(struct stardata_t * Restrict const stardata);

void force_flushing(void);


void binary_c_help_from_arg(ARG_SUBROUTINE_DECLARATION);
void binary_c_help_all_from_arg(ARG_SUBROUTINE_DECLARATION);
void binary_c_argopts_from_arg(ARG_SUBROUTINE_DECLARATION);
void binary_c_speedtests(ARG_SUBROUTINE_DECLARATION) No_return;
void binary_c_warmup_cpu(ARG_SUBROUTINE_DECLARATION);
void list_available_args(ARG_SUBROUTINE_DECLARATION);
void list_available_args_and_exit(ARG_SUBROUTINE_DECLARATION) No_return;
void dummyfunc(ARG_SUBROUTINE_DECLARATION);
void binary_c_version_internal(ARG_SUBROUTINE_DECLARATION);
void binary_c_set_version_boolean(ARG_SUBROUTINE_DECLARATION);
void version_only(ARG_SUBROUTINE_DECLARATION);
void dumpversion(ARG_SUBROUTINE_DECLARATION) No_return;
double set_effective_metallicity(ARG_SUBROUTINE_DECLARATION);
void set_init_abund(ARG_SUBROUTINE_DECLARATION);
void set_init_abund_mult(ARG_SUBROUTINE_DECLARATION);
void set_init_abund_dex(ARG_SUBROUTINE_DECLARATION);
void set_solar_abund(ARG_SUBROUTINE_DECLARATION);
void set_third_dup_multiplier(ARG_SUBROUTINE_DECLARATION);
void add_orbiting_object_from_argstring(ARG_SUBROUTINE_DECLARATION);

void bjorn(ARG_SUBROUTINE_DECLARATION);
void binary_c_logo(ARG_SUBROUTINE_DECLARATION);

#ifdef BATCHMODE
void batchmode_reset_stars(ARG_SUBROUTINE_DECLARATION);
void batchmode_bye(ARG_SUBROUTINE_DECLARATION) No_return;
void batchmode_reset_stars(ARG_SUBROUTINE_DECLARATION);
void batchmode_set_echo_on(ARG_SUBROUTINE_DECLARATION);
void batchmode_set_echo_off(ARG_SUBROUTINE_DECLARATION);
void batchmode_fin(ARG_SUBROUTINE_DECLARATION);
void batchmode_reset_stars_defaults(ARG_SUBROUTINE_DECLARATION);
void batchmode_status(ARG_SUBROUTINE_DECLARATION);
void batchmode_go(ARG_SUBROUTINE_DECLARATION);
void batchmode_gogo(ARG_SUBROUTINE_DECLARATION) No_return;
void batchmode_reset_prefs(ARG_SUBROUTINE_DECLARATION);
void batchmode_defaults(ARG_SUBROUTINE_DECLARATION);
#endif // BATCHMODE

void set_defaults_set(ARG_SUBROUTINE_DECLARATION);
void reset_stars_defaults(ARG_SUBROUTINE_DECLARATION);
Random_seed random_seed(void);
void show_cmd_line_args(const struct stardata_t * const stardata,
                        FILE * stream);

int parse_arguments_from_string(const char * Restrict const argstring,
                                struct stardata_t * Restrict stardata);
char **split_commandline(const char * Restrict const cmdline,
                         int * Restrict const argc);
void split_commandline_free(char ** argv,
                            const int argc);

void new_system(struct stardata_t ** stardata,
                struct stardata_t *** previous_stardatas,
                struct preferences_t ** preferences,
                struct store_t ** store,
                struct persistent_data_t ** persistent_data,
                char ** argv,
                const int argc);

void set_cmd_line_macro_pairs(struct stardata_t * Restrict const stardata,
                              struct cmd_line_arg_t * const cmd_line_args2,
                              unsigned int arg_count);
void set_metallicities(struct stardata_t * Restrict const stardata);
char * cmd_line_argstring(struct stardata_t * const stardata);

void derived_arguments(struct stardata_t * const stardata);

void set_masses_and_multiplicity(struct stardata_t * const stardata);


void set_merger_mass_loss_fraction_degenerate(ARG_SUBROUTINE_DECLARATION);
void set_merger_mass_loss_fraction_nondegenerate(ARG_SUBROUTINE_DECLARATION);

#ifdef RANDOM_SYSTEMS
void set_random_system(struct stardata_t * const Restrict stardata,
                       const int output_format);
void random_system_list(struct stardata_t * const Restrict stardata);
#endif //RANDOM_SYSTEMS


#ifdef DEFINE_DATA_OBJECTS
void define_data_objects(void);
#endif // DEFINE_DATA_OBJECTS

void set_qcrit_nuclear_burning(ARG_SUBROUTINE_DECLARATION);
void set_qcrit_WD(ARG_SUBROUTINE_DECLARATION);
void set_qcrit_all(ARG_SUBROUTINE_DECLARATION);
void set_WD_accretion_rate_algorithms(ARG_SUBROUTINE_DECLARATION);

void call_random_systems_list(ARG_SUBROUTINE_DECLARATION);
char * read_system_list(struct stardata_t * const stardata,
                        const Boolean show);

Boolean arg_match_scanf(const char * const arg,
                        struct cmd_line_arg_t * const cmd_line_arg,
                        size_t * const offset_p);

void parse_hierarchy(struct stardata_t * const stardata);

void refresh_arguments(struct stardata_t * Restrict const stardata,
                       const int argc,
                       const Boolean force);

#ifdef HASH_ARGUMENTS
void hash_arguments(struct stardata_t * const Restrict stardata,
                    const Boolean force);
#endif // HASH_ARGUMENTS

#endif /* SETUP_PROTOYPES_H */
