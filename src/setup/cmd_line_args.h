#pragma once
#ifndef BINARY_C_CMD_LINE_ARGS_H
#define BINARY_C_CMD_LINE_ARGS_H

/*
 * Header for binary_c's command-line reading module
 *
 *
 * See also :
 * cmd_line_function_macros.h : this contains macros to
 *                              load in arguments.
 * cmd_line_args_list.h : this contains the list of arguments
 *                        and is probably what you're looking for.
 */

/*
 * Suck in macros that depend on code build options
 */
#include "cmd_line_macros.h"
#include "cmd_line_arg_subroutines.h"
#include <errno.h>


/*
 * Wrapper macros to obtain information for VAR and Sub types,
 * as well as a not-used version
 */
#define Var(VAR)                                \
    (void*)&(stardata->preferences->VAR),/* pointer to the var */   \
        NULL,NULL, /* function pointer stuff */ \
        sizeof(stardata->preferences->VAR)
#define Sub(SUB)                                                \
    NULL, /* no pointer to no var */                            \
        (arg_funcptr_t)&SUB,#SUB, /* function pointer stuff */  \
        sizeof(void*)
#define Not_used_arg NULL,NULL,NULL,sizeof(NULL)

#ifdef NUCSYN
#define Nucsyn_var(X) Var(X)
#define Nucsyn_sub(X) Sub(X)
#define Nucsyn_or_NULL(X) (X)
#else
#define Nucsyn_var(X) Not_used_arg
#define Nucsyn_sub(X) Sub(X)
#define Nucsyn_or_NULL(X) (NULL)
#endif

/*
 * Argument types
 *
 * Most arguments are called as
 * "--argument <value>"
 * where the value is a float, integer or string.
 * These are types ARG_DOUBLE, ARG_INTEGER, ARG_STRING,
 * ARG_BOOLEAN etc.
 *
 * ARG_NONE is a special case which is ignored.

 * Finally ARG_SUBROUTINE calls a subroutine which
 * handles more complex cases, e.g. multi-value
 * settings. If you want to make one of these, just
 * copy-paste one of the existing functions and
 * hack it to your requirements.
 *
 * The ARG*SCANF functions take an integer as a suffix,
 * this is converted to an index to be added to the pointer
 * provided.
 *
 * ARG*OFFSET_SCANF set argument x1 to item 0,
 * x2 to item 1, etc. i.e. the index is offset by -1.
 */


/*
 * Arg types list:
 *
 * 1 Label
 * 2 sizeof
 * 3 options (newline separated list, strings should contain no spaces)
 */
#define ARG_TYPES_LIST                                                                                      \
    X(                     ARG_DOUBLE,                 sizeof(double),             "floating_point_number") \
    X(                     ARG_STRING,                              0,                            "string") \
    X(           ARG_UNBOUNDED_STRING,                              0,                  "unbounded string") \
    X(                       ARG_NONE,                              0,                              "none") \
    X(                    ARG_INTEGER,                    sizeof(int),                    "integer_number") \
    X(           ARG_UNSIGNED_INTEGER,           sizeof(unsigned int),           "unsigned_integer_number") \
    X(               ARG_LONG_INTEGER,               sizeof(long int),               "long_integer_number") \
    X(      ARG_UNSIGNED_LONG_INTEGER,      sizeof(unsigned long int),      "unsigned_long_integer_number") \
    X(          ARG_LONG_LONG_INTEGER,          sizeof(long long int),          "long_long_integer_number") \
    X( ARG_UNSIGNED_LONG_LONG_INTEGER, sizeof(unsigned long long int), "unsigned_long_long_integer_number") \
    X(                 ARG_SUBROUTINE,                              0,                        "subroutine") \
    X(                    ARG_BOOLEAN,                sizeof(Boolean),                       "TRUE\nFALSE") \
    X(                ARG_NOT_BOOLEAN,                sizeof(Boolean),                       "FALSE\nTRUE") \
    X(           BATCH_ARG_SUBROUTINE,                              0,                        "subroutine") \
    X(              BATCH_ARG_BOOLEAN,                sizeof(Boolean),                       "TRUE\nFALSE") \
    X(          BATCH_ARG_NOT_BOOLEAN,                sizeof(Boolean),                       "FALSE\nTRUE") \
    X(              ARG_INTEGER_SCANF,                              0,                    "integer_number") \
    X(               ARG_DOUBLE_SCANF,                              0,             "floating_point_number") \
    X(              ARG_BOOLEAN_SCANF,                              0,                       "TRUE\nFALSE") \
    X(       ARG_INTEGER_OFFSET_SCANF,                              0,                    "integer_number") \
    X(        ARG_DOUBLE_OFFSET_SCANF,                              0,             "floating_point_number") \
    X(       ARG_BOOLEAN_OFFSET_SCANF,                              0,                       "TRUE\nFALSE")


#undef X
#define X(TYPE,SIZEOF,OPTSTRING) TYPE,
enum { ARG_TYPES_LIST };
#undef X

#include "../binary_c_code_options.h"
#include "../binary_c_debug.h"

#define Cprint(...)
//#define Cprint(...) printf(__VA_ARGS__);fflush(stdout);

/*
 * Macros for setting argument values from the command
 * line strings.
 *
 * Under/overflows are caught for (long) integer and double
 * types.
 *
 * Strings are limited to STRING_LENGTH characters
 * (defined in binary_c_macros.h).
 */


/* define a logical section for each argument */
#define ARG_SECTIONS_LIST                    \
    X(      ARG_SECTION_STARS,      "Stars") \
    X(     ARG_SECTION_BINARY,     "Binary") \
    X(     ARG_SECTION_NUCSYN,     "Nucsyn") \
    X(     ARG_SECTION_OUTPUT,     "Output") \
    X(      ARG_SECTION_INPUT,      "Input") \
    X(         ARG_SECTION_IO,        "I/O") \
    X( ARG_SECTION_ALGORITHMS, "Algorithms") \
    X(       ARG_SECTION_MISC,       "Misc") \
    X( NUMBER_OF_ARG_SECTIONS,           "")

#undef X
#define X(LABEL,STRING) LABEL,
enum { ARG_SECTIONS_LIST };
#undef X

/*
 * strings labelled with WTTS_USE_DEFAULT should
 * output the default value, when available, rather
 * than a string specified here
 */
#define WTTS_USE_DEFAULT NULL

/*
 * The maximum pointer is usually NULL, i.e. ignored,
 * and there's no
 */
#define NO_MAX_POINTER NULL

/*
 * Or perhaps limit to multiplicity?
 */
#define NO_LIMIT_TO_MULTIPLICITY FALSE
#define LIMIT_TO_MULTIPLICITY TRUE

/*
 * Structure to hold a macro - value pair
 */
struct arg_pair_t
{
    char * string;
    char * macro;
    double value; /* NB ints and Booleans can be cast in here, we rely on sizeof(double)>=Max(sizeof(Boolean),sizeof(int)) which is always true */
};

union random_var_t {
    double _double;
    int _int;
};

/*
 * The arguments have a parse number which sets
 * on which parse they are checked, or all.
 */
#define ARG_PARSE_ALL -1
#define ARG_NUM_PARSES 3

/*
 * Random arg fluctuation types
 */

#define ARG_RANDOM_TYPE_LIST                                                          \
                  /* none */                                                          \
    X(                          NONE,                          "None")                \
                      /* random scalar */                                             \
    X(                           INT,                       "integer")                \
    X(                       BOOLEAN,                       "Boolean")                \
    X(                        DOUBLE,                        "double")                \
    X(                     LOGDOUBLE,             "log-spaced double")                \
                  /* choose from array */                                             \
    X(                     INT_ARRAY,                     "int array")                \
    X(                  DOUBLE_ARRAY,                  "double array")                \
                  /* choose from either array or scalar (50:50 chance) */             \
    X(       INT_ARRAY_OR_INT_SCALAR,       "int array or int scalar")                \
    X(    INT_ARRAY_OR_DOUBLE_SCALAR,    "int array or double scalar")                \
    X( DOUBLE_ARRAY_OR_DOUBLE_SCALAR, "double array or double scalar")


#undef X
#define X(TYPE,STRING) ARG_RANDOM_##TYPE,
enum {
    ARG_RANDOM_TYPE_LIST
};
#undef X

/*
 * Maximum number of random array options
 */
#define CMD_LINE_RANDOM_ARRAY_LENGTH 10

/*
 * Number of random levels (1,2,...)
 */
#define CMD_LINE_RANDOM_NUM_LEVELS 2


struct cmd_line_arg_t
{
    /* required arguments */
    int parse_number;
    int section;
    const char *name;
    const char *help;
    int type;
    const char * wtts_string;
    void * max_pointer;
    Boolean limit_to_multiplicity;
    void * pointer;
    arg_funcptr_t function_pointer;
    char * function_pointer_name;
    size_t varsize;
    double modulate;

    /* optional arguments */

    /* arg pairs : set in set_cmd_line_macro_pairs() */
    struct arg_pair_t * pairs;
    unsigned int npairs;
    struct arg_pair_t * argpairs;
    unsigned int nargpairs;
    char ** scanf_strings;
    unsigned int nscanf_strings;

    /* other data */
    char * data;

    /* random variation (see set_random_system.c) */
    unsigned int random_level; // 0 (unused),1,2,...
    unsigned int random_arg_type; /* see ARG_RANDOM_* above */
    union random_var_t random_lower; /* int or double */
    union random_var_t random_upper; /* int or double */
    unsigned int random_array_length;
    union random_var_t random_array[CMD_LINE_RANDOM_ARRAY_LENGTH];
};


/*
 * macro to fill in the random arg data
 * for an argument that isn't varied randomly
 *
 * Note: we require curly brackets around unions
 * and the array to avoid gcc and clang warnings
 */
#define CMD_LINE_ARG_T_NO_RANDOM_VARIATION ARG_RANDOM_NONE,0,{0},{0},0,{{1}/*dummy array*/}

/*
 * macro to fill in the data that is not set
 * in the declaration of CMD_LINE_ARGS below
 * (this is for pairs, npairs, argpairs, nargpairs, nscanf_strings, data)
 */
#define CMD_LINE_ARG_T_OPTIONAL_ARGS NULL,0,NULL,0,NULL,0,NULL

#endif // BINARY_C_CMD_LINE_ARGS_H
