#include "../binary_c.h"
No_empty_translation_unit_warning;
#include "defaults_sets.h"

void set_default_preferences_2024a(struct preferences_t * Restrict const preferences)
{
    /*
     * Reset the preferences struct to its default values
     * according to the 2024a set.
     *
     * This is the location where binary_c's defaults are set.
     *
     * You should be able to run (in a terminal):
     *
     * ./binary_c-config defaults
     *
     * to list the default parameters.
     *
     * The preferences struct is allocated using Calloc,
     * so everything is zero by default.
     */
    preferences->defaults_set = DEFAULTS_SET_2024a;

    timestep_set_default_multipliers(preferences);
#ifdef TIMESTEP_MODULATION
    preferences->timestep_modulator = 1.0;
#endif
#ifdef RLOF_MDOT_MODULATION
    preferences->RLOF_mdot_factor = 1.0;
#endif
    preferences->RLOF_interpolation_method = RLOF_INTERPOLATION_BINARY_C;
    preferences->monte_carlo_sn_kicks = TRUE;
    preferences->post_SN_orbit_method = POST_SN_ORBIT_TT98;
    {
        for (Supernova i = 0; i < SN_NUM_TYPES; i++)
        {
            preferences->sn_kick_distribution[i] = KICK_VELOCITY_FIXED;
            preferences->sn_kick_dispersion[i] = 0.0;
        }
    }
    preferences->sn_kick_distribution[SN_II] = KICK_VELOCITY_MAXWELLIAN;
    preferences->sn_kick_dispersion[SN_II] = SN_SIGMA_DEFAULT;
    preferences->sn_kick_distribution[SN_IBC] = KICK_VELOCITY_MAXWELLIAN;
    preferences->sn_kick_dispersion[SN_IBC] = SN_SIGMA_DEFAULT;
    preferences->sn_kick_distribution[SN_GRB_COLLAPSAR] = KICK_VELOCITY_MAXWELLIAN;
    preferences->sn_kick_dispersion[SN_GRB_COLLAPSAR] = SN_SIGMA_DEFAULT;
#ifdef WD_KICKS
    preferences->sn_kick_dispersion[SN_WDKICK] = WD_SIGMA_DEFAULT;
    preferences->wd_kick_when = WD_KICK_END_AGB;
#endif
    preferences->dtfac = DTFAC_DEFAULT;
#ifdef PRE_MAIN_SEQUENCE
    preferences->pre_main_sequence = FALSE;
#endif
    preferences->third_dup = TRUE;
#ifdef NUCSYN
#ifdef LITHIUM_TABLES
    preferences->lithium_hbb_multiplier = 1.0;
    preferences->lithium_GB_post_1DUP = 0.0;
#endif

#ifdef SOFT_NANCHECK
    preferences->nanchecks = TRUE;
#endif //SOFT_NANCHECK
    strlcpy(preferences->Seitenzahl2013_model, "N100", 5);

#ifdef NUCSYN_S_PROCESS
    /* C13 pocket s-process efficiency */
    preferences->c13_eff = DEFAULT_C13_EFFICIENCY;
    preferences->mc13_pocket_multiplier = 1.0;
#endif

#ifdef USE_TABULAR_INTERSHELL_ABUNDANCES_KARAKAS_2012
    preferences->pmz_mass = 0.0; //CAB let's put pmz_mass=0 as a default value?
#endif //USE_TABULAR_INTERSHELL_ABUNDANCES_KARAKAS_2012


#ifdef NUCSYN_GCE_OUTFLOW_CHECKS
    preferences->escape_velocity = 1e9; // galactic escape velocity
    preferences->escape_fraction = 0.0; // galactic escape fraction
#endif // NUCSYN_GCE_OUTFLOW_CHECKS
#ifdef NUCSYN_YIELDS
    preferences->legacy_yields = FALSE;
#endif//NUCSYN_YIELDS

    for (Reaction_network i = 0; i < NUCSYN_NETWORK_NUMBER; i++)
    {
        preferences->nucsyn_network_error[i] = NUCSYN_NETWORK_DEFAULT_ERROR;
    }
#undef X
#define X(CODE,ONOFF,STRING,FUNCTION,LSODA,CVODE,CHECKFUNC)             \
    if(NUCSYN_NETWORK_##CODE!=NUCSYN_NETWORK_NUMBER)                    \
    {                                                                   \
        preferences->nucsyn_network[NUCSYN_NETWORK_##CODE] = (ONOFF);   \
    }
    NUCSYN_NETWORKS_LIST;
#undef X

#endif//NUCSYN

#ifdef STELLAR_POPULATIONS_ENSEMBLE
    preferences->ensemble = STELLAR_POPULATIONS_ENSEMBLE_DEFAULT;
    preferences->ensemble_defer = STELLAR_POPULATIONS_ENSEMBLE_DEFER_DEFAULT;
    preferences->ensemble_output_lines = FALSE;
    preferences->ensemble_dt = STELLAR_POPULATIONS_ENSEMBLE_DT_DEFAULT;
    preferences->ensemble_logdt = STELLAR_POPULATIONS_ENSEMBLE_LOGDT_DEFAULT;
    preferences->ensemble_logtimes = STELLAR_POPULATIONS_ENSEMBLE_LOGTIMES_DEFAULT;
    preferences->ensemble_startlogtime = STELLAR_POPULATIONS_ENSEMBLE_STARTLOGTIME_DEFAULT;
    preferences->ensemble_legacy_ensemble = FALSE;
    preferences->ensemble_filters_off = TRUE; /* changed from 2022a */
    for (unsigned int i = 0; i < STELLAR_POPULATIONS_ENSEMBLE_FILTER_NUMBER; i++)
    {
        preferences->ensemble_filter[i] = FALSE; /* changed from 2022a */
        preferences->ensemble_filter_override[i] = FALSE;
    }

#endif // STELLAR_POPULATIONS_ENSEMBLE

    // initialize other free parameters
    preferences->nova_retention_fraction_H = NOVA_RETENTION_FRACTION_DEFAULT;
    preferences->nova_retention_fraction_He = NOVA_RETENTION_FRACTION_DEFAULT;
    preferences->accretion_limit_eddington_steady_multiplier = ACCRETION_LIMIT_EDDINGTON_MULTIPLIER_STEADY_DEFAULT;
    preferences->accretion_limit_eddington_LMMS_multiplier = ACCRETION_LIMIT_EDDINGTON_MULTIPLIER_LMMS_DEFAULT;
    preferences->accretion_limit_eddington_WD_to_remnant_multiplier = ACCRETION_LIMIT_EDDINGTON_MULTIPLIER_WD_TO_REMNANT_DEFAULT;
    preferences->accretion_limit_thermal_multiplier = ACCRETION_LIMIT_THERMAL_MULTIPLIER_DEFAULT;
    preferences->accretion_limit_dynamical_multiplier = ACCRETION_LIMIT_DYNAMICAL_MULTIPLIER_DEFAULT;
    preferences->donor_limit_thermal_multiplier = DONOR_LIMIT_THERMAL_MULTIPLIER_DEFAULT;
    preferences->donor_limit_dynamical_multiplier = DONOR_LIMIT_DYNAMICAL_MULTIPLIER_DEFAULT;
    preferences->donor_limit_dynamical_multiplier = DONOR_LIMIT_ENVELOPE_MULTIPLIER_DEFAULT;

    /* nova timestep accelerators */
    preferences->nova_timestep_accelerator_num = 100;
    preferences->nova_timestep_accelerator_max = -1.0;
    preferences->nova_timestep_accelerator_index = 0.5;

    preferences->rlof_angmom_gamma = NONCONSERVATIVE_ANGMOM_GAMMA_DEFAULT;
    preferences->nonconservative_angmom_gamma = NONCONSERVATIVE_ANGMOM_GAMMA_DEFAULT;
    preferences->wind_djorb_fac = 1.0;
    preferences->gb_reimers_eta = REIMERS_ETA_DEFAULT;
    preferences->magnetic_braking_factor = 1.0;
    preferences->magnetic_braking_gamma = MAGNETIC_BRAKING_GAMMA_DEFAULT;
    preferences->magnetic_braking_factor = 1.0;

    preferences->CRAP_parameter = CRAP_PARAMETER_DEFAULT;
    preferences->Bondi_Hoyle_accretion_factor = BONDI_HOYLE_ACCRETION_FACTOR_DEFAULT;
    preferences->vwind_beta = 1.0 / 8.0;
    preferences->vwind_multiplier = 1.0;

    // Set the agb winds to 0, whatever that is (see wind loss function)
    preferences->eagbwind = 0;
    preferences->tpagbwind = 0;
    // mira switch on defaults at 500 days
    preferences->tpagb_superwind_mira_switchon = 500.0;
    // remiers eta value on tpagb
    preferences->tpagb_reimers_eta = 1.0;
    // assume Reimers GB wind (Hurley etal 2002)
    preferences->gbwind = 0;
    // wr wind is standard by default (Hurley et al 2002)
    preferences->wr_wind = WR_WIND_BSE;
    preferences->VW93_EAGB_wind_speed = FALSE;
    preferences->VW93_TPAGB_wind_speed = FALSE;
    preferences->wind_gas_to_dust_ratio = 200.0;

    {
        for (int i = 0; i < WIND_TYPE_COUNT; i++)
        {
            preferences->wind_type_multiplier[i] = 1.0;
        }
    }
    preferences->wind_LBV_luminosity_lower_limit = 6e5;
    preferences->wind_Nieuwenhuijzen_luminosity_lower_limit = 4e3;

    /* common envelope parameters */
    {
        preferences->comenv_prescription[0] = DEFAULT_COMENV_PRESCRIPTION;
        preferences->alpha_ce[0] = DEFAULT_ALPHA_CE;
        preferences->lambda_ce[0] = DEFAULT_LAMBDA_CE;
        preferences->lambda_ionisation[0] = DEFAULT_LAMBDA_IONISATION;
        preferences->lambda_enthalpy[0] = DEFAULT_LAMBDA_ENTHALPY;
        preferences->comenv_convection_multiplier = 1.0;
        for (Comenv_counter i = 1; i < MAX_NUMBER_OF_COMENVS; i++)
        {
            preferences->comenv_prescription[i] = COMENV_UNDEF;
            preferences->alpha_ce[i] = -1.0;
            preferences->lambda_ce[i] = -1.0;
            preferences->lambda_ionisation[i] = -1.0;
            preferences->lambda_enthalpy[i] = -1.0;
        }
    }
    /* WR wind loss factor 1.0 by default */
    preferences->wr_wind_fac = 1.0;
    /* black hole prescription */
    preferences->BH_prescription = BH_HURLEY2002;
#ifdef  RLOF_RADIATION_CORRECTION
    preferences->RLOF_f = RLOF_RADIATION_CORRECTION_F_DEFAULT;
#endif // RLOF_RADIATION_CORRECTION

    preferences->tidal_strength_factor = TIDAL_STRENGTH_FACTOR_DEFAULT;
    preferences->merger_angular_momentum_factor = MERGER_ANGULAR_MOMENTUM_FACTOR_DEFAULT;

    /*
     * default to -1.0 for mass loss fractions, which means
     * unset
     */
    preferences->merger_mass_loss_fraction = 0.1;
    for (Stellar_type i = 0; i < NUMBER_OF_STELLAR_TYPES; i++)
    {
        /* set these to be ignored unless set manually */
        preferences->merger_mass_loss_fraction_by_stellar_type[i] = (double)MERGER_MASS_LOSS_FRACTION_UNUSED;
    }
    preferences->minimum_timestep = MINIMUM_TIMESTEP_DEFAULT;
    preferences->maximum_timestep = MAXIMUM_TIMESTEP_DEFAULT;
    preferences->maximum_nuclear_burning_timestep = MAXIMUM_TIMESTEP_DEFAULT;
    preferences->maximum_timestep_factor = MAXIMUM_TIMESTEP_FACTOR_DEFAULT;
    preferences->mixingpar_MS_mergers = MIXINGPAR_MS_MERGERS_DEFAULT;

    /* critical q ratios for dynamical mass transfer
     *
     * The defaults are based on Claeys et al 2014 (A&A)
     */

    /* non-degenerate */
    preferences->qcrit[LOW_MASS_MS] = 0.6944;
    preferences->qcrit[MAIN_SEQUENCE] = 1.6;
    preferences->qcrit[HERTZSPRUNG_GAP] = 4.0;
    preferences->qcrit[GIANT_BRANCH] = -1.0;
    preferences->qcrit[CHeB] = 3.0;
    preferences->qcrit[EAGB] = -1.0;
    preferences->qcrit[TPAGB] = -1.0;
    preferences->qcrit[HeMS] = 3.0;
    preferences->qcrit[HeHG] = 4.0;
    preferences->qcrit[HeGB] = 0.78125;
    preferences->qcrit[HeWD] = 3.0;
    preferences->qcrit[COWD] = 3.0;
    preferences->qcrit[ONeWD] = 3.0;
    preferences->qcrit[NEUTRON_STAR] = 3.0;
    preferences->qcrit[BLACK_HOLE] = 3.0;
    preferences->qcrit[MASSLESS_REMNANT] = 3.0; // ignored

    /* degenerate */
    preferences->qcrit_degenerate[LOW_MASS_MS] = 1.0;
    preferences->qcrit_degenerate[MAIN_SEQUENCE] = 1.0;
    preferences->qcrit_degenerate[HERTZSPRUNG_GAP] = 4.7619;
    preferences->qcrit_degenerate[GIANT_BRANCH] = 1.15;
    preferences->qcrit_degenerate[CHeB] = 3.0;
    preferences->qcrit_degenerate[EAGB] = 1.15;
    preferences->qcrit_degenerate[TPAGB] = 1.15;
    preferences->qcrit_degenerate[HeMS] = 3.0;
    preferences->qcrit_degenerate[HeHG] = 4.7619;
    preferences->qcrit_degenerate[HeGB] = 1.15;
    preferences->qcrit_degenerate[HeWD] = 0.625;
    preferences->qcrit_degenerate[COWD] = 0.625;
    preferences->qcrit_degenerate[ONeWD] = 0.625;
    preferences->qcrit_degenerate[NEUTRON_STAR] = 0.625;
    preferences->qcrit_degenerate[BLACK_HOLE] = 0.625;
    preferences->qcrit_degenerate[MASSLESS_REMNANT] = 3.0; // ignored

    preferences->E2_prescription = E2_HURLEY_2002;
#ifdef WRLOF_MASS_TRANSFER
    preferences->WRLOF_method = WRLOF_NONE;
#endif//WRLOF_MASS_TRANSFER
    preferences->nelemans_gamma = DEFAULT_NELEMANS_GAMMA;
    preferences->nelemans_minq = NELEMANS_MIN_Q_DEFAULT;
    preferences->nelemans_max_frac_j_change = NELEMANS_MAX_FRAC_J_CHANGE_DEFAULT;
    preferences->nelemans_n_comenvs = NELEMANS_N_COMENVS_DEFAULT;
    preferences->mass_accretion_for_COWD_DDet = MASS_ACCRETION_FOR_DDet_DEFAULT;
    preferences->mass_accretion_for_ONeWD_DDet = MASS_ACCRETION_FOR_DDet_DEFAULT;
    preferences->WD_accretion_rate_novae_upper_limit_hydrogen_donor = ACCRETION_RATE_NOVAE_UPPER_LIMIT_HYDROGEN_DONOR_DEFAULT;
    preferences->WD_accretion_rate_new_giant_envelope_lower_limit_hydrogen_donor = ACCRETION_RATE_NEW_GIANT_ENVELOPE_LOWER_LIMIT_HYDROGEN_DONOR_DEFAULT;
    preferences->WD_accretion_rate_novae_upper_limit_helium_donor = ACCRETION_RATE_NOVAE_UPPER_LIMIT_HELIUM_DONOR_DEFAULT;
    preferences->WD_accretion_rate_new_giant_envelope_lower_limit_helium_donor = ACCRETION_RATE_NEW_GIANT_ENVELOPE_LOWER_LIMIT_HELIUM_DONOR_DEFAULT;
    preferences->WD_accretion_rate_novae_upper_limit_other_donor = ACCRETION_RATE_NOVAE_UPPER_LIMIT_OTHER_DONOR_DEFAULT;
    preferences->WD_accretion_rate_new_giant_envelope_lower_limit_other_donor = ACCRETION_RATE_NEW_GIANT_ENVELOPE_LOWER_LIMIT_OTHER_DONOR_DEFAULT;

    preferences->hachisu_disk_wind = FALSE;
    preferences->hachisu_qcrit = HACHISU_DISK_WIND_QCRIT_DEFAULT;

    preferences->mass_for_Hestar_Ia_upper = 0.0;
    preferences->mass_for_Hestar_Ia_lower = 0.0;

    preferences->max_HeWD_mass = 0.7;

    preferences->rotationally_enhanced_mass_loss = ROTATIONALLY_ENHANCED_MASSLOSS_NONE;
    preferences->rotationally_enhanced_exponent = 1.0;

#ifdef VW93_MIRA_SHIFT
    preferences->vw93_mira_shift = 0.0;
#endif
#ifdef VW93_MULTIPLIER
    preferences->vw93_multiplier = 1.0;
#endif
#ifdef COMENV_NS_ACCRETION
    preferences->comenv_ns_accretion_fraction = COMENV_NS_ACCRETION_FRACTION_DEFAULT;
    preferences->comenv_ns_accretion_mass = COMENV_NS_ACCRETION_MASS_DEFAULT;
#endif
#ifdef COMENV_MS_ACCRETION
    preferences->comenv_ms_accretion_mass = COMENV_MS_ACCRETION_MASS_DEFAULT;
#endif

    preferences->RLOF_method = RLOF_METHOD_BSE;
    preferences->require_drdm = FALSE;

    preferences->qcrit_giant_branch_method = QCRIT_GB_DEFAULT;

    /* by default have no rotation effects, though this might change in the future */
    preferences->third_dup = TRUE;
    preferences->delta_mcmin = 0.0;
    preferences->lambda_min = 0.0;
    preferences->lambda_multiplier = 1.0;
#ifdef NUCSYN
#ifdef NUCSYN_THIRD_DREDGE_UP
    preferences->minimum_envelope_mass_for_third_dredgeup = MINIMUM_ENVELOPE_MASS_FOR_THIRD_DREDGEUP_DEFAULT;
#endif
    preferences->hbbtfac = HBBTFAC_DEFAULT;
    preferences->no_thermohaline_mixing = FALSE;
#if defined NUCSYN_THIRD_DREDGE_UP && \
    defined NUCSYN_THIRD_DREDGE_UP_MULTIPLIERS
    {
        preferences->boost_third_dup = FALSE;
        Nucsyn_isotope_stride_loop(
            preferences->third_dup_multiplier[i] = 1.0;
            );
    }
#endif
    nucsyn_reset_reaction_rates(preferences);

    /*
     * Set all initial override abundances to -1.0,
     * which means unset.
     */
    nucsyn_set_abunds_array(preferences->the_initial_abundances, -1.0, TRUE);
    nucsyn_set_abunds_array(preferences->initial_abundance_multiplier, -1.0, TRUE);

#ifdef NUCSYN_STAR_BY_STAR_MATCHING
    /* similarly, set ERRORS for the observed star to -1 */
    nucsyn_set_abunds_array(preferences->observed_star_error, -1.0);
#endif //NUCSYN_STAR_BY_STAR_MATCHING

    preferences->initial_abunds_only = FALSE;

    /* default to AG89 abundances */
    preferences->initial_abundance_mix = NUCSYN_INIT_ABUND_MIX_LODDERS2010;

    /* minimum age for EMP stars */
    preferences->EMP_minimum_age = 10.0;

    /* maximum [Fe/H] for EMP stars */
    preferences->EMP_feh_maximum = -2.0;

    /* maximum logg */
    preferences->EMP_logg_maximum = 4.0;

    /* minimum [X/Fe] for CEMPs, NEMPs */
    preferences->CEMP_cfe_minimum = 0.7;
    preferences->NEMP_nfe_minimum = 1.0;

#ifdef NUCSYN_TPAGB_HBB
    /* allow NeNa and MgAl burning */
    preferences->NeNaMgAl = TRUE;
#endif

#endif //NUCSYN

    /* multiplier for GB/EAGB/TPAGB wind loss rate */
    preferences->gbwindfac = 1.0;
    preferences->eagbwindfac = 1.0;
    preferences->tpagbwindfac = 1.0;
    preferences->nieuwenhuijzen_windfac = 1.0;
    preferences->cmd_line_random_seed = 0;

    preferences->repeat = 1;
#ifdef RANDOM_SYSTEMS
    preferences->random_systems = FALSE;
#endif

    /* default wind mass loss prescription */
    preferences->wind_mass_loss = WIND_ALGORITHM_BINARY_C_2020;

#ifdef FILE_LOG
    /*
     * Set default filename for the log file
     */
    strlcpy(preferences->log_filename,
            DEFAULT_LOG_OUTPUT_FILENAME,
            STRING_LENGTH);
    /*
     * Don't colour logs or add arrows by default
     */
    preferences->colour_log = FALSE;
    preferences->log_arrows = FALSE;
    /*
     * Use human-readable stellar types by default
     */
    preferences->log_legacy_stellar_types = FALSE;
    /*
     * Log separator : default to empty string
     */
    preferences->log_separator[0] = '\0';
#endif

    preferences->wind_angular_momentum_loss = WIND_ANGMOM_LOSS_BSE;
    preferences->lw = 1.0;

    {
        for (Star_number st = 0; st < NUMBER_OF_STARS; st++)
        {
            preferences->max_stellar_type[st] = NUMBER_OF_STELLAR_TYPES;
        }
    }

    preferences->internal_buffering = 0;
    preferences->use_periastron_Roche_radius = FALSE;
    preferences->chandrasekhar_mass = DEFAULT_CHANDRASEKHAR_MASS;
    preferences->max_neutron_star_mass = DEFAULT_MAX_NEUTRON_STAR_MASS;
    preferences->maximum_mcbagb_for_degenerate_carbon_ignition = DEFAULT_MAXIMUM_MCBAGB_FOR_DEGENERATE_CARBON_IGNITION;
    preferences->minimum_mcbagb_for_nondegenerate_carbon_ignition = DEFAULT_MINIMUM_MCBAGB_FOR_NONDEGENERATE_CARBON_IGNITION;
    preferences->minimum_CO_core_mass_for_carbon_ignition = DEFAULT_MINIMUM_CO_CORE_MASS_FOR_CARBON_IGNITION;
    preferences->minimum_CO_core_mass_for_neon_ignition = DEFAULT_MINIMUM_CO_CORE_MASS_FOR_NEON_IGNITION;
    preferences->HeWD_HeWD_ignition_mass = 0.3;


#ifdef NUCSYN
#ifdef CN_THICK_DISC
    /*
     * look back times for the start and end of
     * thick disk star formation
     */
    preferences->thick_disc_start_age = CN_THICK_DISC_START_AGE_DEFAULT;
    preferences->thick_disc_end_age = CN_THICK_DISC_END_AGE_DEFAULT;
    preferences->thick_disc_logg_min = CN_THICK_DISC_LOGG_MIN_DEFAULT;
    preferences->thick_disc_logg_max = CN_THICK_DISC_LOGG_MAX_DEFAULT;
#endif//CN_THICK_DISC

    preferences->AGB_3dup_algorithm = AGB_THIRD_DREDGE_UP_ALGORITHM_KARAKAS;
    preferences->AGB_core_algorithm = AGB_CORE_ALGORITHM_KARAKAS;
    preferences->AGB_radius_algorithm = AGB_RADIUS_ALGORITHM_KARAKAS;
    preferences->AGB_luminosity_algorithm = AGB_LUMINOSITY_ALGORITHM_KARAKAS;
    preferences->nucsyn_solver = NUCSYN_SOLVER_KAPS_RENTROP;
#else //NUCSYN
    preferences->AGB_3dup_algorithm = AGB_THIRD_DREDGE_UP_ALGORITHM_HURLEY;
    preferences->AGB_core_algorithm = AGB_CORE_ALGORITHM_HURLEY;
    preferences->AGB_radius_algorithm = AGB_RADIUS_ALGORITHM_HURLEY;
    preferences->AGB_luminosity_algorithm = AGB_LUMINOSITY_ALGORITHM_HURLEY;
#endif // !NUCSYN

    preferences->observable_radial_velocity_minimum = OBSERVABLE_RADIAL_VELOCITY_MIN_DEFAULT;

#ifdef EVOLUTION_SPLITTING
    preferences->evolution_splitting_sn_n = EVOLUTION_SPLITTING_SUPERNOVA_N;
    preferences->evolution_splitting_maxdepth = EVOLUTION_SPLITTING_MAX_SPLITDEPTH_DEFAULT;
    preferences->evolution_splitting_sn_eccentricity_threshold = 0.01;
#endif // EVOLUTION_SPLITTING
#ifdef DISCS
#if defined DISC_LOG || defined DISC_LOG_2D
    strlcpy(preferences->disc_log_directory, "/tmp/", 6);
#endif // DISC_LOG || DISC_LOG_2D
    preferences->disc_timestep_factor = 0.01;
    preferences->cbdisc_gamma = 1.4;
    preferences->cbdisc_max_lifetime = 0.0;
    preferences->cbdisc_alpha = 1e-3;
    preferences->cbdisc_kappa = 1e-2;
    preferences->cbdisc_albedo = 0.0;
    preferences->cbdisc_torquef = 1e-3;
    preferences->cbdisc_init_dM = 0.1;
    preferences->cbdisc_init_dJdM = 0.5;
    preferences->cbdisc_inner_edge_stripping = TRUE;
    preferences->cbdisc_outer_edge_stripping = TRUE;
    preferences->cbdisc_minimum_evaporation_timescale = 1.0; /* years */
    preferences->cbdisc_mass_loss_constant_rate = 0.0;
    preferences->cbdisc_mass_loss_inner_viscous_multiplier = 1.0;
    preferences->cbdisc_mass_loss_inner_viscous_angular_momentum_multiplier = 1.0;
    preferences->cbdisc_mass_loss_inner_L2_cross_multiplier = 1.0;
    preferences->cbdisc_mass_loss_ISM_ram_pressure_multiplier = 1.0;
    preferences->cbdisc_mass_loss_ISM_pressure = 3000.0;
    preferences->cbdisc_mass_loss_FUV_multiplier = 1.0;
    preferences->cbdisc_mass_loss_Xray_multiplier = 1.0;
    preferences->cbdisc_minimum_luminosity = 0.0;
    preferences->cbdisc_minimum_mass = 1e-6;
    preferences->cbdisc_minimum_fRing = 0.2;
    preferences->cbdisc_no_wind_if_cbdisc = FALSE;
    preferences->cbdisc_eccentricity_pumping_method = CBDISC_ECCENTRICITY_PUMPING_DERMINE;
    preferences->cbdisc_resonance_multiplier = 1.0;
    preferences->cbdisc_resonance_damping = TRUE;
    preferences->cbdisc_mass_loss_inner_viscous_accretion_method = CBDISC_MASS_LOSS_INNER_VISCOUS_ACCRETION_METHOD_YOUNG_CLARKE_2015;
    preferences->cbdisc_viscous_photoevaporative_coupling = TRUE;
    preferences->cbdisc_viscous_L2_coupling = TRUE;
    preferences->cbdisc_fail_ring_inside_separation = FALSE;
    preferences->cbdisc_inner_edge_stripping = TRUE;
    preferences->cbdisc_outer_edge_stripping = TRUE;
    preferences->cbdisc_inner_edge_stripping_timescale = DISC_STRIPPING_TIMESCALE_INSTANT;
    preferences->cbdisc_outer_edge_stripping_timescale = DISC_STRIPPING_TIMESCALE_INSTANT;
    preferences->cbdisc_max_lifetime = 1e6; /* years */

#ifdef DISCS_CIRCUMBINARY_FROM_COMENV
#endif // DISCS_CIRCUMBINARY_FROM_COMENV
#endif // DISCS
#ifdef COMENV_POLYTROPES
    preferences->comenv_splitmass = 1.0;
#endif // COMENV_POLYTROPES
    preferences->comenv_post_eccentricity = 1e-5;
    preferences->gravitational_radiation_modulator_J = 1.0;
    preferences->gravitational_radiation_modulator_e = 1.0;
    preferences->solver = SOLVER_FORWARD_EULER;
    preferences->timestep_solver_factor = 1.0;
    preferences->zoomfac_multiplier_decrease = DEFAULT_ZOOMFAC_DECREASE;
    preferences->zoomfac_multiplier_increase = DEFAULT_ZOOMFAC_INCREASE;
    preferences->force_circularization_on_RLOF = TRUE;
    preferences->resolve_stellar_type_changes = FALSE;
    preferences->eccentric_RLOF_model = RLOF_ECCENTRIC_AS_CIRCULAR;
    preferences->max_stellar_angmom_change = MAX_STELLAR_ANGMOM_CHANGE_DEFAULT;

    memset(&preferences->zero_age, 0, sizeof(struct zero_age_system_t));
    for (int i = 0; i < 4; i++)
    {
        preferences->zero_age.vrot_multiplier[i] = 1.0;
    }

    preferences->initial_probability = 1.0;
    preferences->metallicity = 0.02;
    preferences->effective_metallicity = DEFAULT_TO_METALLICITY;
    preferences->nucsyn_metallicity = DEFAULT_TO_METALLICITY;
    preferences->max_evolution_time = 15000.0;
    preferences->overspin_algorithm = OVERSPIN_BSE;
    {
        unsigned int i;
        for (i = 0; i < FIXED_TIMESTEP_NUMBER; i++)
        {
            preferences->use_fixed_timestep[i] = TRUE;
        }
        for (i = 0; i < BINARY_C_TASK_NUMBER; i++)
        {
            preferences->tasks[i] = TRUE;
        }
        for (i = 0; i < NUMBER_OF_STELLAR_TYPES; i++)
        {
            preferences->wind_multiplier[i] = 1.0;
        }
    }
#ifdef GAIAHRD
    preferences->gaia_Teff_binwidth = 0.1;
    preferences->gaia_L_binwidth = 0.1;
#endif//GAIAHRD
    preferences->disable_end_logging = FALSE;
    preferences->timestep_logging = FALSE;
    preferences->vandenHeuvel_logging = FALSE;
    preferences->rejects_in_log = FALSE;

    /* function pointers */
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
    preferences->custom_output_function = NULL;
    preferences->catch_events_function = NULL;
    preferences->extra_calculate_derivatives_function = NULL;
    preferences->extra_apply_derivatives_function = NULL;
    preferences->extra_update_binary_star_variables_function = NULL;
    preferences->pre_time_evolution_function = NULL;
    preferences->post_time_evolution_function = NULL;

#pragma GCC diagnostic pop

    for (unsigned int i = 0; i < NUMBER_OF_FUNCTION_HOOKS; i++)
    {
        preferences->function_hooks[i] = NULL;
    }
    preferences->custom_supernova_kick_hook = NULL;
    preferences->stellar_structure_hook = NULL;
    preferences->extra_stellar_evolution_hook = NULL;

#ifdef WTTS_LOG
    preferences->wtts_log = FALSE;
#endif // WTTS_LOG

#ifdef STELLAR_COLOURS
    preferences->gaia_colour_transform_method = GAIA_CONVERSION_UBVRI_UNIVARIATE_EVANS2018;
    preferences->gaia_white_dwarf_colour_method = GAIA_CONVERSION_WD_CARRASCO2014_TABLE;
#endif // STELLAR_COLOURS

    preferences->white_dwarf_cooling_model = WHITE_DWARF_COOLING_MESTEL;
    preferences->white_dwarf_radius_model = WHITE_DWARF_RADIUS_NAUENBERG1972;

#ifdef MINT
    {
        char * _b = getenv("MINT_DIR");
        if(_b != NULL)
        {
            strlcpy(preferences->MINT_dir,
                    _b,
                    sizeof(char)*Min(sizeof(_b), STRING_LENGTH - 1));
        }
    }
    preferences->MINT_metallicity = -1.0;
    preferences->MINT_minimum_shell_mass = MINT_DEFAULT_MINIMUM_SHELL_MASS;
    preferences->MINT_maximum_shell_mass = MINT_DEFAULT_MAXIMUM_SHELL_MASS;
    preferences->MINT_data_cleanup = FALSE;
    preferences->MINT_nuclear_burning = FALSE;
    preferences->MINT_MS_rejuvenation = TRUE;
    preferences->MINT_remesh = TRUE;
    preferences->MINT_Kippenhahn = 0;
    preferences->MINT_Kippenhahn_stellar_type = -1;
    preferences->MINT_Kippenhahn_companion_stellar_type = -1;
    preferences->MINT_disable_grid_load_warnings = FALSE;
    preferences->MINT_use_ZAMS_profiles = TRUE;
    preferences->MINT_fallback_to_test_data = FALSE;
    preferences->MINT_nshells = 200;
    preferences->MINT_minimum_nshells = 10;
    preferences->MINT_maximum_nshells = 1000;
#endif // MINT

    /* multiplicity is unset */
    preferences->multiplicity = 0;
    preferences->zero_age.multiplicity = 0;

    /* no stopfile by default */
    preferences->stopfile[0] = 0;

    /* PN algorithms */
    preferences->PN_comenv_transition_time = 1e2;
    preferences->PPN_envelope_mass = 1e-2;
    preferences->PN_Hall_fading_time_algorithm =
        PN_HALL_FADING_TIME_ALGORITHM_MINIMUM;
    preferences->minimum_time_between_PNe = 0.1; // Myr

    /*
     * Extra timestep resolution for PNe
     */
    preferences->PN_resolve = FALSE;
    preferences->PN_resolve_minimum_luminosity = 31.62;
    preferences->PN_resolve_maximum_envelope_mass = 0.1;
    preferences->PN_resolve_minimum_effective_temperature = 1.25e4;

    /* PN fast wind */
    preferences->PN_fast_wind_mdot_GB = 1e-6;
    preferences->PN_fast_wind_mdot_AGB = 1e-6;
    preferences->PN_fast_wind_dm_GB = 1e-2;
    preferences->PN_fast_wind_dm_AGB = 1e-3;

    /* we want data grids by default */
    preferences->load_data_grids = TRUE;

    /* we want to allow debugging */
    preferences->disable_debug = FALSE;

    /* default to having no envelopes on post-CE stars */
    preferences->post_ce_objects_have_envelopes = FALSE;

    /* use fixed envelope masses by default */
    preferences->post_ce_adaptive_menv = FALSE;

    /*
     * Artificial accretion start and end times
     */
    preferences->artificial_accretion_start_time = ARTIFICIAL_ACCRETION_IGNORE;
    preferences->artificial_accretion_end_time = ARTIFICIAL_ACCRETION_IGNORE;

#ifdef ORBITING_OBJECTS
    /*
     * Orbiting object parameters
     */
    preferences->orbiting_objects_tides_multiplier = 1.0;
    preferences->orbiting_objects_wind_accretion_multiplier = BONDI_HOYLE_ACCRETION_FACTOR_DEFAULT;
    preferences->orbiting_objects_close_pc_threshold = 1.0;
#endif//ORBITING_OBJECTS

    preferences->postagbwind = FALSE;
    preferences->Teff_postAGB_min = 6000.0;
    preferences->Teff_postAGB_max = 120000.0;

    preferences->apply_Darwin_Radau_correction = FALSE;

#ifdef NUCSYN
    preferences->degenerate_core_merger_dredgeup_fraction = 0.0;
#endif//NUCSYN
    preferences->degenerate_core_merger_nucsyn = FALSE;
    preferences->degenerate_core_helium_merger_ignition = TRUE;

    preferences->transient_method = TRANSIENT_METHOD_NONE;

    preferences->COWD_to_ONeWD_accretion_rate = 2.05e-6;
    preferences->eta_violent_WDWD_merger = 0.75;
#ifdef SAVE_MASS_HISTORY
    preferences->adjust_structure_from_mass_changes = FALSE;
    preferences->save_mass_history_n_thermal = 4.0;
#endif//SAVE_MASS_HISTORY

#ifdef NUCSYN
    preferences->COWD_COWD_explode_above_mass = 0.0;
    preferences->HeWD_COWD_explode_above_mass = 0.0;

    /*
     * default to latest SNe from Limongi & Chieffi
     * which do not require extra r-process
     */
    preferences->core_collapse_supernova_algorithm = NUCSYN_CCSN_LIMONGI_CHIEFFI_2018;
    preferences->core_collapse_rprocess_algorithm = NUCSYN_CCSN_RPROCESS_NONE;
    preferences->core_collapse_rprocess_mass = 0.0;
    preferences->electron_capture_supernova_algorithm = NUCSYN_ECSN_WANAJO_2008_ST;
#endif //NUCSYN

#ifdef YBC
    preferences->YBC_path[0] = '\0';
    preferences->YBC_listfile[0] = '\0';
    preferences->YBC_instruments[0] = '\0';
    preferences->YBC_all_instruments = FALSE;
#endif//YBC

#ifdef PPISN
    preferences->PPISN_prescription = PPISN_NONE;
    preferences->PPISN_additional_massloss = 0.0;
    preferences->PPISN_core_mass_range_shift = 0.0;
    preferences->PPISN_massloss_multiplier = 1.0;
    preferences->sn_kick_distribution[SN_PPI] = KICK_VELOCITY_FIXED;
    preferences->sn_kick_dispersion[SN_PPI] = 0.0;
#endif // PPISN
    preferences->fixed_beta_mass_transfer_efficiency = FIXED_BETA_MASS_TRANSFER_EFFICIENCY_DISABLED;
    preferences->core_collapse_energy_prescription = CORE_COLLAPSE_ENERGY_ONE_FOE;
    preferences->baryonic_to_gravitational_remnant_mass_prescription = BARY_TO_GRAV_NEUTRINO_EMISSION_LIMITED;
    preferences->SN_mean_anomaly = -1.0;
    preferences->hachisu_limiter_multiplier = 1.0;
    preferences->comenv_algorithm = COMENV_ALGORITHM_BINARY_C;
    preferences->MSMS_merger_age_algorithm = MSMS_MERGER_AGE_ALGORITHM_SELMA;

    preferences->comenv_Scatter_dim = 3.0;
    preferences->comenv_Scatter_eta = 1.0;
    preferences->log_groups = TRUE;

    preferences->Matsumoto2022_fplateau = 1.0;
    preferences->Matsumoto2022_fshock = 0.0;
    preferences->Matsumoto2022_shock_mass_fraction = 0.2;
    preferences->timestep_epsilon = TIMESTEP_EPSILON_DEFAULT;
}
