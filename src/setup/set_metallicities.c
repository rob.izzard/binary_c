/*
 * This function initializes the metallicties used in various parts
 * of binary_c
 */
#include "../binary_c.h"
No_empty_translation_unit_warning;


void set_metallicities(struct stardata_t * Restrict const stardata)
{
    /*
     * If the effective or nucsyn metallicity are < 0,
     * set it to the metallicity
     */
    if(Negative_nonzero(stardata->common.effective_metallicity))
    {
        stardata->common.effective_metallicity =
            stardata->common.metallicity;
    }

    if(Negative_nonzero(stardata->common.nucsyn_metallicity))
    {
        stardata->common.nucsyn_metallicity =
            stardata->common.metallicity;
    }

}
