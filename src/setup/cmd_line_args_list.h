#include "cmd_line_batchmode_args_list.h"

/*
 * List of command-line arguments used in binary_c
 *
 * These are stored in an X-macro list in cmd_line_args_list.def
 * which are loaded in here through an appropriate X macro.
 *
 * See cmd_line_args_list.def for more details.
 */
#define X(...) {__VA_ARGS__},

BATCHMODE_ARGS
#include "cmd_line_args_list.def"
