#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "cmd_line_macro_pairs.h"
#include "defaults_sets.h"
#include "../common_envelope/common_envelope_evolution_binary_c.h"

#include "cmd_line_args.h"
#include "cmd_line_function_macros.h"
#include "cmd_line_macro_pairs.h"

static struct cmd_line_arg_t * match_a(char * const VARNAME,
                                       struct cmd_line_arg_t ** const __prevarg,
                                       char ** const __prevstring,
                                       struct cmd_line_arg_t * const cmd_line_args2,
                                       unsigned int arg_count);

void __part1(struct stardata_t * Restrict const stardata,
             struct cmd_line_arg_t * const cmd_line_args2,
             unsigned int arg_count);
void __part2(struct stardata_t * Restrict const stardata,
             struct cmd_line_arg_t * const cmd_line_args2,
             unsigned int arg_count);

void set_cmd_line_macro_pairs(struct stardata_t * Restrict const stardata,
                              struct cmd_line_arg_t * const cmd_line_args2,
                              unsigned int arg_count)
{
    /*
     * Here we map a command line argument's value to a macro so you
     * can use the macro as the argument, rather than just a number.
     *
     * Usage:
     *
     * Add_value_macro_pair("the_argument",
     *                      C_MACRO);
     */

    __part1(stardata,cmd_line_args2,arg_count);
    __part2(stardata,cmd_line_args2,arg_count);

}

static struct cmd_line_arg_t * match_a(char * const var_name,
                                       struct cmd_line_arg_t ** const __prevarg,
                                       char ** const __prevstring,
                                       struct cmd_line_arg_t * const cmd_line_args2,
                                       unsigned int arg_count)
{
    struct cmd_line_arg_t * a = NULL;

    if(var_name != NULL &&
       __prevarg != NULL &&
       *__prevarg != NULL &&
       __prevstring != NULL &&
       *__prevstring != NULL &&
       Strings_equal(var_name,
                     *__prevstring))
    {
        /* cache match previous */
        a = *__prevarg;
    }
    else
    {
        /* loop to find next match or leave a as  NULL */
        for(unsigned int __i = 0; __i < arg_count; __i++)
        {
            if(Strings_equal(var_name,
                             cmd_line_args2[__i].name))
            {
                a = &cmd_line_args2[__i];
                __i = arg_count; /* end loop */
                /* update cache */
                *__prevarg = a;
                *__prevstring = (char*)a->name;
            }
        }
    }
    return a;
}


void __part1(struct stardata_t * Restrict const stardata,
             struct cmd_line_arg_t * const cmd_line_args2,
             unsigned int arg_count)
{

    unsigned int __i;
    struct cmd_line_arg_t * __prevarg = NULL;
    char * __prevstring = NULL;

#undef X
#define X(ARG) Add_value_macro_pair("stellar_structure_algorithm",STELLAR_STRUCTURE_ALGORITHM_##ARG);
    STELLAR_STRUCTURE_ALGORITHMS_LIST;

#undef X
#define X(CODE) Add_value_macro_pair("cannot_shorten_timestep_policy",CANNOT_SHORTEN_##CODE);
    CANNOT_SHORTEN_OPTIONS_LIST;


#undef X
#define X(SHORT,LONG,NUM,SHORT_STRING,LONG_STRING)  \
    Add_value_macro_pair("stellar_type_%d",SHORT);
    STELLAR_TYPES_LIST;
#undef X
#define X(SHORT,LONG,NUM,SHORT_STRING,LONG_STRING)  \
    Add_value_macro_pair("stellar_type_%d",LONG);
    STELLAR_TYPES_LIST;

#undef X
#define X(SHORT,LONG,NUM,SHORT_STRING,LONG_STRING)      \
    Add_value_macro_pair("max_stellar_type_%d",SHORT);
    STELLAR_TYPES_LIST;
#undef X
#define X(SHORT,LONG,NUM,SHORT_STRING,LONG_STRING)      \
    Add_value_macro_pair("max_stellar_type_%d",LONG);
    STELLAR_TYPES_LIST;
#undef X
#define X(SHORT,LONG,NUM,SHORT_STRING,LONG_STRING)      \
    Add_value_macro_pair("max_stellar_type_%d",-SHORT);
    STELLAR_TYPES_LIST;
#undef X
#define X(SHORT,LONG,NUM,SHORT_STRING,LONG_STRING)      \
    Add_value_macro_pair("max_stellar_type_%d",-LONG);
    STELLAR_TYPES_LIST;

#undef X
#define X(CODE,NUM)                             \
    Add_value_macro_pair("vrot%d",VROT_##CODE);
    VROT_PARAMETERS_LIST;


#undef X
#define X(CODE) Add_value_macro_pair("overspin_algorithm",OVERSPIN_##CODE);
    OVERSPIN_PARAMETERS_LIST;
#undef X

#define X(CODE) Add_value_macro_pair("wind_angular_momentum_loss",WIND_ANGMOM_LOSS_##CODE);
    WIND_ANGMOM_LIST;
#undef X

#define X(CODE) Add_value_macro_pair("gravitational_radiation_model",GRAVITATIONAL_RADIATION_##CODE);
    GRAVITATIONAL_RADIATION_MODELS_LIST;
#undef X

#define X(CODE) Add_value_macro_pair("magnetic_braking_algorithm",MAGNETIC_BRAKING_ALGORITHM_##CODE);
    MAGNETIC_BRAKING_ALGORITHM_LIST;
#undef X

#define X(CODE) Add_value_macro_pair("E2_prescription",E2_##CODE);
    E2_LIST;
#undef X

    Add_value_macro_pair("lambda_min", THIRD_DREDGE_UP_LAMBDA_MIN_AUTO);


#define X(CODE) Add_value_macro_pair("tides_convective_damping",TIDES_CONVECTIVE_DAMPING_##CODE);
    TIDES_CONVECTIVE_DAMPING_LIST;
#undef X

#define X(CODE) Add_value_macro_pair("tides_algorithm",TIDES_ALGORITHM_##CODE);
    TIDES_ALGORITHM_LIST;
#undef X

#define X(CODE) Add_value_macro_pair("RLOF_method",RLOF_METHOD_##CODE);
    RLOF_METHODS_LIST;
#undef X

#define X(CODE) Add_value_macro_pair("RLOF_interpolation_method",RLOF_INTERPOLATION_##CODE);
    RLOF_INTERPOLATION_METHODS_LIST;
#undef X

    Add_value_macro_pair("tidal_strength_factor",
                         TIDAL_STRENGTH_VARIABLE);

#define X(TYPE) Add_value_macro_pair("skip_bad_args",SKIP_BAD_ARGS_##TYPE);
    SKIP_BAD_ARGS_TYPE_LIST;
#undef X

#define X(CODE) Add_value_macro_pair("RLOF_angular_momentum_transfer_model",RLOF_ANGULAR_MOMENTUM_TRANSFER_MODEL_##CODE);
    RLOF_ANGULAR_MOMENTUM_TRANSFER_MODEL_LIST;
#undef X

#define X(CODE,NUM) Add_value_macro_pair("nonconservative_angmom_gamma",RLOF_NONCONSERVATIVE_GAMMA_##CODE);
    RLOF_NONCONSERVATIVE_GAMMA_LIST;
#undef X

#define X(CODE,NUM) Add_value_macro_pair("merger_mass_loss_fraction",MERGER_MASS_LOSS_FRACTION_##CODE);
    MERGER_MASS_LOSS_FRACTION_LIST;
#undef X
#define X(CODE,NUM) Add_value_macro_pair("merger_mass_loss_fraction%d",MERGER_MASS_LOSS_FRACTION_##CODE);
    MERGER_MASS_LOSS_FRACTION_LIST;
#undef X
#define X(CODE,NUM) Add_value_macro_pair("merger_mass_loss_fraction_by_stellar_type_%d",MERGER_MASS_LOSS_FRACTION_##CODE);
    MERGER_MASS_LOSS_FRACTION_LIST;
#undef X

    Add_value_macro_pair("hachisu_qcrit",
                         HACHISU_IGNORE_QCRIT);

#define X(TYPE,NUM) Add_value_macro_pair("comenv_algorithm",COMENV_ALGORITHM_##TYPE);
    COMENV_ALGORITHMS_LIST;
#undef X
#define X(TYPE,NUM) Add_value_macro_pair("comenv_envelope_response",COMENV_ENVELOPE_RESPONSE_##TYPE);
    COMENV_ENVELOPE_RESPONSES_LIST;
#undef X
#define X(CODE,NUM) Add_value_macro_pair("comenv_prescription",COMENV_##CODE);
    COMENV_PRESCRIPTIONS_LIST;
#undef X
#define X(CODE,NUM) Add_value_macro_pair("comenv_prescription%d",COMENV_##CODE);
    COMENV_PRESCRIPTIONS_LIST;
#undef X

#define X(CODE,NUM) Add_value_macro_pair("lambda_ce",LAMBDA_CE_##CODE);
    COMENV_LAMBDA_LIST;
#undef X
#define X(CODE,NUM) Add_value_macro_pair("lambda%d",LAMBDA_CE_##CODE);
    COMENV_LAMBDA_LIST;
#undef X

#define X(CODE,NUM) Add_value_macro_pair("comenv_Scatter_eta",COMENV_SCATTER_ETA_##CODE);
    COMENV_SCATTER_ETA_LIST;
#undef X

#define X(CODE) Add_value_macro_pair("comenv_merger_spin_method",COMENV_MERGER_SPIN_METHOD_##CODE);
    COMENV_MERGER_SPIN_METHODS_LIST;
#undef X

#define X(CODE) Add_value_macro_pair("comenv_ejection_spin_method",COMENV_EJECTION_SPIN_METHOD_##CODE);
    COMENV_EJECTION_SPIN_METHODS_LIST;
#undef X

    {
#define X(LONG,SHORT,...) Add_value_macro_pair_string("log_period_unit",SHORT,__k++)
        unsigned int __k = 0;
        Add_value_macro_pair_string("log_period_unit", "auto", __k++)
            TIME_UNIT_LIST;
#undef X
    }

#define X(CODE,NUM) Add_value_macro_pair("disc_log",DISC_LOG_LEVEL_##CODE);
    DISC_LOG_LEVEL_LIST;
#undef X

#define X(CODE,NUM) Add_value_macro_pair("disc_log2d",DISC_LOG_LEVEL_##CODE);
    DISC_LOG_LEVEL_LIST;
#undef X

#define X(CODE,NUM) Add_value_macro_pair("cbdisc_inner_edge_stripping_timescale",DISC_STRIPPING_TIMESCALE_##CODE);
    DISC_STRIPPING_TIMESCALE_LIST;
#undef X

#define X(CODE,NUM) Add_value_macro_pair("cbdisc_outer_edge_stripping_timescale",DISC_STRIPPING_TIMESCALE_##CODE);
    DISC_STRIPPING_TIMESCALE_LIST;
#undef X

#define X(CODE) Add_value_macro_pair("cbdisc_eccentricity_pumping_method",CBDISC_ECCENTRICITY_PUMPING_##CODE);
    CBDISC_ECCENTRICITY_PUMPING_LIST;
#undef X

#define X(CODE) Add_value_macro_pair("cbdisc_viscous_photoevaporative_coupling",CBDISC_VISCOUS_PHOTOEVAPORATIVE_COUPLING_##CODE);
    CBDISC_VISCOUS_PHOTOEVAPORATIVE_COUPLING_LIST;
#undef X

    Add_value_macro_pair("equation_of_state_algorithm",
                         EQUATION_OF_STATE_PACZYNSKI);

#define X(CODE) Add_value_macro_pair("opacity_algorithm",OPACITY_ALGORITHM_##CODE);
    OPACITY_ALGORITHMS_LIST;
#undef X

    Add_value_macro_pair("beta_reverse_nova",
                         BETA_REVERSE_NOVAE_GEOMETRY);

#define X(CODE,STRING) Add_value_macro_pair("wind_mass_loss",WIND_ALGORITHM_##CODE);
    WIND_ALGORITHMS_LIST;
#undef X
#define X(CODE) Add_value_macro_pair("gbwind",GB_WIND_##CODE);
    GB_WIND_PRESCRIPTIONS_LIST;
#undef X
#define X(CODE) Add_value_macro_pair("eagbwind",EAGB_WIND_##CODE);
    EAGB_WIND_PRESCRIPTIONS_LIST;
#undef X
#define X(CODE) Add_value_macro_pair("tpagbwind",TPAGB_WIND_##CODE);
    TPAGB_WIND_PRESCRIPTIONS_LIST;
#undef X
#define X(CODE) Add_value_macro_pair("postagbwind",POSTAGB_WIND_##CODE);
    POSTAGB_WIND_PRESCRIPTIONS_LIST;
#undef X
#define X(CODE) Add_value_macro_pair("wr_wind",WR_WIND_##CODE);
    WR_WIND_PRESCRIPTIONS_LIST;
#undef X
#define X(CODE) Add_value_macro_pair("rotationally_enhanced_mass_loss",ROTATIONALLY_ENHANCED_MASSLOSS_##CODE);
    ROTATIONALLY_ENHANCED_MASSLOSS_LIST;
#undef X
#define X(CODE) Add_value_macro_pair("WRLOF_method",WRLOF_##CODE);
    WRLOF_LIST;
#undef X

#define X(SHORT,LONG,NUM,SHORT_STRING,LONG_STRING)              \
    Add_argument_Xmacro_pair("wind_multiplier_%d",SHORT,NUM);
    STELLAR_TYPES_LIST;
#undef X

#define X(CODE) Add_value_macro_pair("BH_prescription",BH_##CODE);
    BH_PRESCRIPTIONS_LIST;
#undef X
#ifdef NUCSYN
#define X(CODE,VAL,TEXT) Add_value_macro_pair("type_Ia_MCh_supernova_algorithm",TYPE_IA_SUPERNOVA_ALGORITHM_##CODE);
    NUCSYN_IA_ALGORITHMS_CODES;
#undef X

#define X(CODE,VAL,TEXT) Add_value_macro_pair("type_Ia_sub_MCh_supernova_algorithm",TYPE_IA_SUPERNOVA_ALGORITHM_##CODE);
    NUCSYN_IA_ALGORITHMS_CODES;
#undef X

#define X(CODE,VAL,TEXT) Add_value_macro_pair("type_Ia_DD_yield_algorithm",TYPE_IA_SUPERNOVA_ALGORITHM_##CODE);
    NUCSYN_IA_ALGORITHMS_CODES;
#undef X

#define X(CODE,VAL,TEXT) Add_value_macro_pair("triggered_SNIa_algorithm",TYPE_IA_SUPERNOVA_ALGORITHM_##CODE);
    NUCSYN_IA_ALGORITHMS_CODES;
#undef X

#define X(CODE) Add_value_macro_pair("NS_merger_yield_algorithm",NS_MERGER_YIELD_ALGORITHM_##CODE);
    NS_MERGER_YIELD_ALGORITHMS_LIST;
#undef X

#define X(CCTYPE,STRING) Add_value_macro_pair("core_collapse_supernova_algorithm",NUCSYN_CCSN_##CCTYPE);
    CORE_COLLAPSE_SUPERNOVA_ALGORITHMS;
#undef X

#define X(CCTYPE,STRING) Add_value_macro_pair("core_collapse_rprocess_algorithm",NUCSYN_CCSN_RPROCESS_##CCTYPE);
    CORE_COLLAPSE_RPROCESS_ALGORITHMS;
#undef X

#define X(CCTYPE,STRING) Add_value_macro_pair("electron_capture_supernova_algorithm",NUCSYN_ECSN_##CCTYPE);
    ELECTRON_CAPTURE_SUPERNOVA_ALGORITHMS;
#undef X

#endif//NUCSYN

#define X(CODE) Add_value_macro_pair("Radice2018_EOS",RADICE2018_EOS_##CODE);
    RADICE2018_EOS_LIST;
#undef X

#define X(CODE) Add_value_macro_pair("core_collapse_energy",CORE_COLLAPSE_ENERGY_##CODE);
    CORE_COLLAPSE_ENERGY_LIST;
#undef X

#define X(CODE) Add_value_macro_pair("MSMS_merger_age_algorithm",MSMS_MERGER_AGE_ALGORITHM_##CODE)
    MSMS_MERGER_AGE_ALGORITHM_LIST;
#undef X

#define X(CODE) Add_value_macro_pair("WDWD_merger_algorithm",WDWD_MERGER_ALGORITHM_##CODE)
    WDWD_MERGER_ALGORITHM_LIST;
#undef X

#define X(CODE) Add_value_macro_pair("post_SN_orbit_method",POST_SN_ORBIT_##CODE);
    POST_SN_ORBIT_LIST;
#undef X

    for (__i = 0; __i < SN_NUM_TYPES; __i++)
    {
        char * s = NULL;
        if(asprintf(&s, "sn_kick_distribution_%s",
                    supernova_cmdline_strings[__i]) > 0)
        {
#define X(CODE) Add_value_macro_pair(s,KICK_VELOCITY_##CODE);
            SN_KICK_VELOCITY_DISTRIBUTION_LIST;
#undef X
            Safe_free(s);
        }
        s = NULL;
        if(asprintf(&s, "sn_kick_companion_%s",
                    supernova_cmdline_strings[__i]) > 0)
        {
#define X(CODE) Add_value_macro_pair(s,SN_IMPULSE_##CODE);
            SN_IMPULSE_LIST;
#undef X
            Safe_free(s);
        }
    }
}


void __part2(struct stardata_t * Restrict const stardata,
             struct cmd_line_arg_t * const cmd_line_args2,
             unsigned int arg_count)
{
    unsigned int __i;
    struct cmd_line_arg_t * __prevarg = NULL;
    char * __prevstring = NULL;

#define X(CODE) Add_value_macro_pair("wd_kick_direction",KICK_##CODE);
    KICK_DIRECTIONS_LIST;
#undef X
#define X(CODE) Add_value_macro_pair("wd_kick_when",WD_KICK_##CODE);
    WD_KICK_WHEN_LIST;
#undef X

#ifdef NUCSYN
#define X(CODE,TEXT) Add_value_macro_pair("initial_abundance_mix",CODE);
    NUCSYN_INIT_ABUNDS_CODES;
#undef X
#define X(CODE,TEXT,FUNC) Add_value_macro_pair("nucsyn_solver",NUCSYN_SOLVER_##CODE);
    NUCSYN_SOLVERS_LIST;
#undef X
#define X(CODE,TEXT,FUNC) Add_value_macro_pair("nucsyn_solver",NUCSYN_SOLVER_##CODE);
    NUCSYN_SOLVERS_LIST;
#undef X

#endif//NUCSYN

#define X(CODE) Add_value_macro_pair("decretion_disc_radius_algorithm",DECRETION_DISC_RADIUS_##CODE);
    DECRETION_DISC_RADIUS_ALGORITHMS_LIST;
#undef X

#define X(CODE) Add_value_macro_pair("small_envelope_method",SMALL_ENVELOPE_METHOD_##CODE);
    SMALL_ENVELOPE_METHODS_LIST;
#undef X

#define X(CODE) Add_value_macro_pair("AGB_core_algorithm",AGB_CORE_##CODE);
    AGB_CORE_ALGORITHM_LIST;
#undef X
#define X(CODE) Add_value_macro_pair("AGB_radius_algorithm",AGB_RADIUS_##CODE);
    AGB_RADIUS_ALGORITHM_LIST;
#undef X
#define X(CODE) Add_value_macro_pair("AGB_luminosity_algorithm",AGB_LUMINOSITY_##CODE);
    AGB_LUMINOSITY_ALGORITHM_LIST;
#undef X
#define X(CODE) Add_value_macro_pair("AGB_3dup_algorithm",AGB_THIRD_DREDGE_UP_##CODE);
    AGB_THIRD_DREDGE_UP_ALGORITHM_LIST;
#undef X

#ifdef NUCSYN
    Add_value_macro_pair("angelou_lithium_decay_function",
                         ANGELOU_LITHIUM_DECAY_FUNCTION_EXPONENTIAL);
#endif

#define X(CODE) Add_value_macro_pair("internal_buffering",INTERNAL_BUFFERING_##CODE);
    INTERNAL_BUFFERING_STATES_LIST;
#undef X

#define X(CODE) Add_value_macro_pair("cbdisc_mass_loss_inner_viscous_accretion_method",CBDISC_MASS_LOSS_INNER_VISCOUS_ACCRETION_METHOD_##CODE);
    CBDISC_MASS_LOSS_INNER_VISCOUS_ACCRETION_METHOD_LIST;
#undef X


    Add_value_macro_pair("artificial_accretion_start_time",
                         ARTIFICIAL_ACCRETION_IGNORE);
    Add_value_macro_pair("artificial_accretion_end_time",
                         ARTIFICIAL_ACCRETION_IGNORE);


#undef X
#define X(SHORT,LONG,NUMBER,SHORT_STRING,LONG_STRING) SHORT_STRING,
    Foreach_string(x, STELLAR_TYPES_LIST)
    {
#undef X
        char * s = NULL;
        if(asprintf(&s, "qcrit_%s", x) > 0)
        {
#define X(CODE,NUM,STRING) Add_value_macro_pair(s,QCRIT_##CODE);
            RLOF_QCRIT_LIST;
#undef X
            /* also _GB_ for backwards compatibility */
#define X(CODE,NUM,STRING) Add_value_macro_pair(s,QCRIT_GB_##CODE);
            RLOF_QCRIT_LIST;
#undef X
            Safe_free(s);
        }
        s = NULL;
        if(asprintf(&s, "qcrit_degenerate_%s", x) > 0)
        {
#define X(CODE,NUM,STRING) Add_value_macro_pair(s,QCRIT_##CODE);
            RLOF_QCRIT_LIST;
#undef X
            /* also _GB_ for backwards compatibility */
#define X(CODE,NUM,STRING) Add_value_macro_pair(s,QCRIT_GB_##CODE);
            RLOF_QCRIT_LIST;
#undef X
            Safe_free(s);
        }
    }

#undef X
#define X(CODE,NUM,STRING) Add_value_macro_pair("qcrit_nuclear_burning",QCRIT_##CODE);
    RLOF_QCRIT_LIST;
#undef X
#define X(CODE,NUM,STRING) Add_value_macro_pair("qcrit_nuclear_burning",QCRIT_GB_##CODE);
    RLOF_QCRIT_LIST;
#undef X

#define X(CODE) Add_value_macro_pair("solver",SOLVER_##CODE);
    SOLVERS_LIST;
#undef X


    Foreach_string(x, "novae_upper", "new_giant_envelope_lower" )
    {
        Foreach_string(y, "hydrogen", "helium", "other" )
        {
            char * s = NULL;
            if(asprintf(&s, "WD_accretion_rate_%s_limit_%s_donor", x, y) > 0)
            {
#define X(CODE,NUM,STRING) Add_value_macro_pair(s,DONOR_RATE_ALGORITHM_##CODE);
                RLOF_DONOR_RATE_ALGORITHMS_LIST;
#undef X
                Safe_free(s);
            }
        }
    }


#define X(CODE) Add_value_macro_pair("nova_retention_method",NOVA_RETENTION_ALGORITHM_##CODE);
    NOVA_RETENTION_ALGORITHMS;
#undef X

#if defined NUCSYN && defined NUCSYN_NOVAE
#define X(CODE,NUM,STRING) Add_value_macro_pair("nova_yield_CO_algorithm",NOVA_YIELD_CO_ALGORITHM_##CODE);
    NOVA_YIELD_CO_ALGORITHMS_CODES;
#undef X
#define X(CODE,NUM,STRING) Add_value_macro_pair("nova_yield_ONe_algorithm",NOVA_YIELD_ONe_ALGORITHM_##CODE);
    NOVA_YIELD_ONe_ALGORITHMS_CODES;
#undef X
#endif//NUCSYN && NUCSYN_NOVAE

    Add_value_macro_pair("effective_metallicity",
                         DEFAULT_TO_METALLICITY);
    Add_value_macro_pair("nucsyn_metallicity",
                         DEFAULT_TO_METALLICITY);

#define X(CODE) Add_value_macro_pair("gaia_colour_transform_method",GAIA_CONVERSION_##CODE);
    GAIA_CONVERSION_METHODS_LIST ;
#undef X

    Add_value_macro_pair("fixed_beta_mass_transfer_efficiency",
                         FIXED_BETA_MASS_TRANSFER_EFFICIENCY_DISABLED);

#define X(CODE) Add_value_macro_pair("white_dwarf_cooling_model",WHITE_DWARF_COOLING_##CODE);
    WHITE_DWARF_COOLING_MODELS_LIST ;
#undef X
#define X(CODE) Add_value_macro_pair("white_dwarf_radius_model",WHITE_DWARF_RADIUS_##CODE);
    WHITE_DWARF_RADIUS_MODELS_LIST ;
#undef X
#define X(CODE) Add_value_macro_pair("PN_Hall_fading_time_algorithm",PN_HALL_FADING_TIME_ALGORITHM_##CODE)
    PN_HALL_FADING_TIME_ALGORITHMS_LIST ;
#undef X
#define X(SHORT,LONG,NUM,SHORT_STRING,LONG_STRING)              \
    Add_value_macro_pair("MINT_Kippenhahn_stellar_type",SHORT);
    STELLAR_TYPES_LIST;
#undef X
#define X(SHORT,LONG,NUM,SHORT_STRING,LONG_STRING)                      \
    Add_value_macro_pair("MINT_Kippenhahn_companion_stellar_type",SHORT);
    STELLAR_TYPES_LIST;
#undef X
#define X(METHOD)                                                       \
    Add_value_macro_pair("transient_method",TRANSIENT_METHOD_##METHOD);
    TRANSIENT_METHODS_LIST;
#undef X

#define X(CODE)                                                 \
    Add_value_macro_pair("defaults_set",DEFAULTS_SET_##CODE);
    DEFAULTS_SETS_LIST;
#undef X

    /*
     * ARGUMENT MACROS
     *
     * Here we map the %d in an argument name to a macro.
     */
#undef X
#define X(CODE) Add_argument_Xmacro_pair("timestep_multiplier%d",CODE,DT_LIMIT_##CODE);
    TIMESTEP_LIMITERS_LIST;
#undef X
#define X(CODE) Add_argument_Xmacro_pair("use_fixed_timestep_%d",CODE,FIXED_TIMESTEP_##CODE);
    FIXED_TIMESTEPS_LIST;
#undef X
#define X(CODE) Add_argument_Xmacro_pair("task%d",CODE,BINARY_C_TASK_##CODE);
    BINARY_C_TASKS_LIST;
#undef X

#undef X
#define X(SHORT,LONG,NUMBER,SHORT_STRING,LONG_STRING) Add_argument_Xmacro_pair("maximum_timestep_by_stellar_type_%d",SHORT,NUMBER);
    STELLAR_TYPES_LIST;
#undef X
#define X(SHORT,LONG,NUMBER,SHORT_STRING,LONG_STRING) Add_argument_Xmacro_pair("merger_mass_loss_fraction_by_stellar_type_%d",SHORT,NUMBER)
    STELLAR_TYPES_LIST;
#undef X
#define X(SHORT,LONG,NUMBER,SHORT_STRING,LONG_STRING) Add_argument_Xmacro_pair("wind_type_multiplier_%d",SHORT,NUMBER)
    STELLAR_TYPES_LIST;
#undef X
#define X(SHORT,LONG,NUMBER,SHORT_STRING,LONG_STRING) Add_argument_Xmacro_pair("artificial_mass_accretion_rate_by_stellar_type%d",SHORT,NUMBER)
    STELLAR_TYPES_LIST;
#undef X
#define X(CODE) Add_argument_Xmacro_pair("ensemble_filter_%d",CODE,STELLAR_POPULATIONS_ENSEMBLE_FILTER_##CODE);
    STELLAR_POPULATIONS_ENSEMBLE_FILTER_LIST;
#undef X

#ifdef NUCSYN

#define X(CODE,ONOFF,STRING,FUNCTION,LSODA,CVODE,CHECKFUNC) Add_argument_Xmacro_pair("nucsyn_network%d",CODE,NUCSYN_NETWORK_##CODE);
    NUCSYN_NETWORKS_LIST;
#undef X
#define X(CODE,ONOFF,STRING,FUNCTION,LSODA,CVODE,CHECKFUNC) Add_argument_Xmacro_pair("nucsyn_network_error%d",CODE,NUCSYN_NETWORK_##CODE);
    NUCSYN_NETWORKS_LIST;
#undef X

#endif // NUCSYN

#ifdef PPISN
#define X(CODE) Add_value_macro_pair("PPISN_prescription",PPISN_##CODE);
    PPISN_PRESCRIPTIONS_LIST;
#undef X
#endif // PPISN

#define X(CODE) Add_value_macro_pair("baryonic_to_gravitational_remnant_mass_prescription",BARY_TO_GRAV_##CODE);
    BARY_TO_GRAV_PRESCRIPTIONS_LIST;
#undef X

#ifdef EVENT_BASED_LOGGING
#define X(TYPE) Add_argument_Xmacro_pair("event_based_logging_%d",TYPE,EVENT_BASED_LOGGING_##TYPE);
    EVENT_BASED_LOGGING_TYPES_LIST;
#undef X
#endif // EVENT_BASED_LOGGING
}
