#include "../binary_c.h"
No_empty_translation_unit_warning;

/*
 * Construct the multiplicity if it has not been set
 */
void set_masses_and_multiplicity(struct stardata_t * const stardata)
{
    struct zero_age_system_t * const zero_age = &stardata->preferences->zero_age;
    Star_number k;

    if(stardata->preferences->hierarchy[0] != '\0')
    {
        parse_hierarchy(stardata);
    }

    if(stardata->preferences->multiplicity != 0)
    {
        zero_age->multiplicity = stardata->preferences->multiplicity;
    }

    if(zero_age->multiplicity == 0)
    {
        Number_of_stars_Starloop(k)
        {
            stardata->star[k].mass = 0.0;
            stardata->star[k].baryonic_mass = 0.0;
            if(Is_not_zero(zero_age->mass[k]))
            {
                zero_age->multiplicity++;
                stardata->star[k].phase_start_mass = zero_age->mass[k];
                stardata->star[k].stellar_type = zero_age->stellar_type[k];
                update_mass(stardata,
                            &stardata->star[k],
                            zero_age->mass[k]);
            }
            else
            {
                stardata->star[k].phase_start_mass = 0.0;
                stardata->star[k].stellar_type = MASSLESS_REMNANT;
            }
        }
    }
    else
    {
        /*
         * Multiplicity has been set manually, check that
         * the masses, period, separation etc. have been set
         */
        Number_of_stars_Starloop(k)
        {
            stardata->star[k].stellar_type = MASSLESS_REMNANT;
            stardata->star[k].starnum = k;
            stardata->star[k].mass = 0.0;
            stardata->star[k].baryonic_mass = 0.0;
        }
        Starloop(k)
        {
            if(Is_zero(zero_age->mass[k]))
            {
                Exit_binary_c(BINARY_C_MASS_OUT_OF_RANGE,
                              "Mass of star %d is zero after you have set the multiplicity to %d : you need to set this star's mass also.",
                              k,
                              zero_age->multiplicity);
            }
            stardata->star[k].phase_start_mass = zero_age->mass[k];
            stardata->star[k].stellar_type = zero_age->stellar_type[k];
            update_mass(stardata,
                        &stardata->star[k],
                        zero_age->mass[k]);
        }
        if(zero_age->multiplicity > 1)
        {
            for(k=0;(int)k<=(int)(zero_age->multiplicity-2);k++)
            {
                if(Is_zero(zero_age->separation[k]) &&
                   Is_zero(zero_age->orbital_period[k]))
                {
                    Exit_binary_c(BINARY_C_OUT_OF_RANGE,
                                  "You have set the multiplicity to %d but not set separation%d or orbital_period%d. Please fix this problem!",
                                  zero_age->multiplicity,
                                  k,
                                  k);

                }
            }
        }
    }


    memcpy(&stardata->common.zero_age,
           zero_age,
           sizeof(struct zero_age_system_t));

    Dprint("Stars : st %d %d grav %g %g bary %g %g\n",
           stardata->star[0].stellar_type,
           stardata->star[1].stellar_type,
           stardata->star[0].mass,
           stardata->star[1].mass,
           stardata->star[0].baryonic_mass,
           stardata->star[1].baryonic_mass);
}
