#include "../binary_c.h"
No_empty_translation_unit_warning;


void derived_arguments(struct stardata_t * const stardata)
{
    /*
     * This function takes the parameters set in the
     * preferences structure and sets them in the stardata,
     * e.g. in the star, common and model structs. In this
     * way, options passed in by the user are mapped to
     * internal memory.
     *
     * We also do some checks on parameters which are arrays,
     * e.g. the common-envelope parameters. If these have the
     * first value set, but none of the others, then all are
     * set to the first value. This way old code is compatible
     * with the new code.
     *
     * We also check some paths exist.
     *
     * Note: we cannot set the spin rate of the star because
     *       we don't yet know its radius. This comes later.
     */
    struct preferences_t * const p = stardata->preferences;

    /*
     * Set ANSI colours in the store if overridden
     */
    for(int i=1; i<NUM_ANSI_COLOURS; i++)
    {
        if(p->colours[i] != 0)
        {
            Safe_free(stardata->store->colours[i]);
            if(asprintf(
                    &stardata->store->colours[i],
                    "\033[38;5;%dm",
                    p->colours[i])
               ==0)
            {

                /* ignore errors */
            }
        }
    }

    /*
     * Set the stellar masses and construct the
     * multiplicity if it has not been set
     */
    set_masses_and_multiplicity(stardata);

    /*
     * Stars of zero mass should be MASSLESS_REMNANTs
     */
    {
        Star_number k;
        Number_of_stars_Starloop(k)
        {
            if(Is_zero(stardata->star[k].mass))
            {
                stardata->star[k].stellar_type = MASSLESS_REMNANT;
            }
            else if(stardata->star[k].mass < 0.0 ||
                    isnan(stardata->star[k].mass) ||
                    isinf(stardata->star[k].mass))
            {
                Exit_binary_c(BINARY_C_WRONG_ARGUMENT,
                              "Mass of star %d is %g which is negative, nan or inf. Please fix it: it should be >0.",
                              k,
                              stardata->star[k].mass);
            }
        }
    }

    /*
     * Set ages
     */
    {
        Star_number k;
        Number_of_stars_Starloop(k)
        {
            stardata->star[k].age = p->zero_age.age[k];
        }
    }

    {
        /*
         * Comenvs should default to comenv_prescription[0] if not set
         */
        Comenv_counter i;
        for(i=0;i<MAX_NUMBER_OF_COMENVS;i++)
        {
            if(p->comenv_prescription[i] == COMENV_UNDEF)
            {
                p->comenv_prescription[i] = p->comenv_prescription[0];
            }
            if(p->alpha_ce[i] < 0.0)
            {
                p->alpha_ce[i] = p->alpha_ce[0];
            }
            if(p->lambda_ce[i] < 0.0)
            {
                p->lambda_ce[i] = p->lambda_ce[0];
            }
            if(p->lambda_ionisation[i] < 0.0)
            {
                p->lambda_ionisation[i] = p->lambda_ionisation[0];
            }
            if(p->lambda_enthalpy[i] < 0.0)
            {
                p->lambda_enthalpy[i] = p->lambda_enthalpy[0];
            }
        }
    }
    stardata->common.orbit.period = p->zero_age.orbital_period[0];
    stardata->common.orbit.separation = p->zero_age.separation[0];
    stardata->common.orbit.eccentricity = p->zero_age.eccentricity[0];
    stardata->model.probability = p->initial_probability;
    stardata->common.metallicity = p->metallicity;
    Clamp(stardata->common.metallicity,1e-4,0.03);
    stardata->common.effective_metallicity = p->effective_metallicity;
    stardata->common.nucsyn_metallicity = p->nucsyn_metallicity;
    stardata->model.max_evolution_time = p->max_evolution_time;

    stardata->common.orbit.angular_momentum = 0.0;
    Dprint("masses %g %g\n",
           stardata->star[0].mass,
           stardata->star[1].mass);

    if(Is_not_zero(stardata->star[0].mass) &&
       Is_not_zero(stardata->star[1].mass))
    {
        Dprint("preset : sep = %g per = %g : %d %d\n",
               stardata->common.orbit.separation,
               stardata->common.orbit.period,
               Is_zero(stardata->common.orbit.period),
               Is_not_zero(stardata->common.orbit.separation));

        /*
         * Units:
         * Separations are in Rsun.
         * orbital period is:
         * in stardata->common.orbit : years
         * in p->zero_age : days
         */

        if(Is_zero(stardata->common.orbit.period) &&
           Is_not_zero(stardata->common.orbit.separation))
        {
            /*
             * Period is set to zero, but separation is given.
             * Hence calculate the initial period.
             */
            stardata->common.orbit.period =
                calculate_orbital_period(stardata);
            if(Is_zero(p->zero_age.orbital_period[0]))
            {
                p->zero_age.orbital_period[0]
                    = stardata->common.orbit.period;
            }
        }
        else if(Is_not_zero(stardata->common.orbit.period))
        {
            /*
             * Update the separation here so that the "INITIAL" log is
             * correct. (This is reset later anyway.)
             */
            stardata->common.orbit.period /= YEAR_LENGTH_IN_DAYS;
            stardata->common.orbit.separation =
                calculate_orbital_separation(stardata);
            stardata->common.orbit.period *= YEAR_LENGTH_IN_DAYS;
            if(Is_zero(p->zero_age.separation[0]))
            {
                p->zero_age.separation[0] =
                    stardata->common.orbit.separation;
            }
        }
    }


    Dprint("postset: sep = %g per = %g\n",
           stardata->common.orbit.separation,
           stardata->common.orbit.period);

#ifdef STELLAR_POPULATIONS_ENSEMBLE
    /*
     * If we are using log times, choose the next
     * max_evolution_time that correponds to an
     * exact number of log timesteps, if
     * ensemble_enforce_maxtime is false.
     */
    if(p->ensemble_logtimes == TRUE &&
       p->ensemble_enforce_maxtime == FALSE)
    {
        stardata->model.max_evolution_time = Ensemble_max_linear_time;
    }

    /*
     * Apply ensemble_filters_off if given: this sets all the filters to
     * the value of the override, which is usually off (FALSE) unless a filter
     * has been manually set by an argument
     */
    if(p->ensemble_filters_off == TRUE)
    {
        unsigned int i;
        for(i=0;i<STELLAR_POPULATIONS_ENSEMBLE_FILTER_NUMBER;i++)
        {
            p->ensemble_filter[i] = p->ensemble_filter_override[i];
        }
    }
#endif // STELLAR_POPULATIONS_ENSEMBLE

    if(p->cmd_line_random_seed != 0)
    {
        stardata->common.random_seed = p->cmd_line_random_seed;
        stardata->common.init_random_seed = p->cmd_line_random_seed;
        stardata->common.random_buffer.set = FALSE;
    }

#ifdef MINT
    if(p->stellar_structure_algorithm ==
       STELLAR_STRUCTURE_ALGORITHM_MINT)
    {
        p->MINT_maximum_nshells =
            Min(p->MINT_maximum_nshells,
                MINT_HARD_MAX_NSHELLS);
        p->MINT_minimum_nshells =
            Max(p->MINT_minimum_nshells,
                MINT_HARD_MIN_NSHELLS);

        if(p->MINT_metallicity < -TINY)
        {
            p->MINT_metallicity =
                stardata->common.metallicity;
        }
    }
#endif//MINT

    if(p->RLOF_method == RLOF_METHOD_ADAPTIVE2)
    {
        p->require_drdm = TRUE;
    }

#ifdef ORBITING_OBJECTS
    initialize_orbiting_objects(stardata);
#endif // ORBITING_OBJECTS

    /*
     * Choose which colour table to use
     */
    if(p->colour_log == FALSE)
    {
        stardata->store->colours = stardata->store->no_colours_table;
    }
    else
    {
        stardata->store->colours = stardata->store->ANSI_colours_table;
    }
    Dprint("end parse arguments\n");

#ifdef YBC
    if(p->YBC_path[0] != '\0' &&
       check_dir_exists(p->YBC_path) == FALSE)
    {
        Exit_binary_c(BINARY_C_WRONG_ARGUMENT,
                      "The parameter YBC_path does not point to an existing directory and it must.");
    }
    YBC_set_instrument_list(stardata);
#endif//YBC

    if(p->JSON_commands_string != NULL &&
       p->JSON_commands_string[0] != '\0')
    {
        /*
         * Load in JSON commands from string
         */
        if(p->commands == NULL)
        {
            CDict_new(c);
            p->commands = c;
        }
        CDict_JSON_buffer_to_CDict(p->JSON_commands_string,
                                   p->commands);
    }

    if(p->JSON_commands_filename[0] != '\0')
    {
        /*
         * Load in JSON commands from file
         */
        if(p->commands == NULL)
        {
            CDict_new(c);
            p->commands = c;
        }
        CDict_JSON_file_to_CDict(p->commands,
                                 p->JSON_commands_filename);
    }

}
