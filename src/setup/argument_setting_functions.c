#include "../binary_c.h"
No_empty_translation_unit_warning;
#include "argument_setting_functions.h"
#include "defaults_sets.h"

/*
 * Functions to set non-standard arguments
 */

void binary_c_warmup_cpu(ARG_SUBROUTINE_DECLARATION)
{
    (*c)++;
    if(stardata->cpu_is_warm == FALSE)
    {
        const int secs = Long_int_to_int(strtol(argv[*c],NULL,10));
        if(secs!=0)
        {
            warmup_cpu(stardata,secs);
            stardata->cpu_is_warm = TRUE;
        }
    }
}



void No_return binary_c_speedtests(ARG_SUBROUTINE_DECLARATION)
{
    /*
     * Perform speed tests for binary_c
     */

    Printf("binary_c speed performance tests\n");

    ticks tick0,tick1;
    long int ntests;
    int j;
#define Start_test tick0=getticks(); for(j=0;j<ntests;j++)
#define TIME_TAKEN Timer_seconds(tick1-tick0)
#define End_test(...) tick1=getticks(); Printf(__VA_ARGS__);fflush(stdout);

    fflush(stdout);fflush(stderr);

    /************************************************************/

    ntests = 5000000;

    Start_test
    {
        for(i=0;i<ntests;i++)
        {
            double x Maybe_unused = (double)rand();
            x *= 1.0;
        }
    }
    End_test("Multiply by FLOAT_TYPE rand : %g s\n",TIME_TAKEN);

    Exit_binary_c(BINARY_C_NORMAL_EXIT,"Done speed performance tests.");
}



static char * arg_variable_default_string(
    const struct cmd_line_arg_t * Restrict const a)
{
    /*
     * Return a string of the default value.
     *
     * Returns "subroutine" for ARG_SUBROUTINE* types.
     */
    char * c = NULL;
    if(a->pointer != NULL)
    {
        if(Argtype_is_subroutine(a->type))
        {
            if(asprintf(&c,
                        "%s",
                        "Subroutine")<0)
            {
                c = NULL;
            }
        }
        else if(a->type == ARG_INTEGER)
        {
            if(asprintf(&c,
                        "%d",
                        *((int*)a->pointer))<0)
            {
                c = NULL;
            }
        }
        else if(a->type == ARG_UNSIGNED_INTEGER)
        {
            if(asprintf(&c,
                        "%u",
                        *((unsigned int*)a->pointer))<0)
            {
                c = NULL;
            }
        }
        else if(a->type == ARG_LONG_INTEGER)
        {
            if(asprintf(&c,
                        "%ld",
                        *((long int*)a->pointer))<0)
            {
                c = NULL;
            }
        }
        else if(a->type == ARG_DOUBLE)
        {
            if(asprintf(&c,
                        "%g",
                        *((double*)a->pointer))<0)
            {
                c = NULL;
            }
        }
        else if(a->type == ARG_BOOLEAN ||
                a->type == ARG_NOT_BOOLEAN)
        {
            if(asprintf(&c,
                        "%s",
                        Truefalse(*((Boolean*)a->pointer)))<0)
            {
                c = NULL;
            }
        }
        else if(a->type == ARG_STRING)
        {
            if(asprintf(&c,
                        "%s",
                        ((char*)a->pointer))<0)
            {
                c = NULL;
            }
        }
        else
        {
            c = NULL;
        }
    }

    /*
     * NULL pointer should give string "NULL"
     */
    if(c == NULL)
    {
        if(asprintf(&c,"NULL") < 0)
        {
            c = NULL;
        }
    }

    return c;
}

void binary_c_help_from_arg(ARG_SUBROUTINE_DECLARATION)
{
    /*
     * Output help: either a general help statement (binary_c --help),
     * or per-argument help (binary_c --help <arg1> <arg2> ...)
     *
     * Note that stardata should not be NULL.
     *
     * If argc < 0 and batchmode is off, we then exit after
     * showing help.
     */
    const Boolean exit_help = (argc >= 0 &&
                               stardata->preferences &&
                               Batchmode_is_off(stardata->preferences->batchmode)) ? TRUE : FALSE;
    if(argc < 0) argc = -argc;

    if(*c==argc-1)
    {
        /* no extra help required */
        Printf("Usage: binary_c <cmd line args>\n\nwhere the arguments are a selection of : \n\n");
        for(i=0;i<arg_count;i++)
        {
            if(cmd_line_args[i].type != BATCH_ARG_SUBROUTINE &&
               cmd_line_args[i].type != BATCH_ARG_BOOLEAN &&
               cmd_line_args[i].type != BATCH_ARG_NOT_BOOLEAN)
            {
                Printf("%s ",cmd_line_args[i].name);
            }

            _scanf_options(stardata,
                           &cmd_line_args[i]);

        }
        Printf("\n\nTry running\n\nbinary_c help <argname>\n\nfor more help on a particular argument.\n\n");

        Exit_binary_c(BINARY_C_QUIET_EXIT,"No command line args given.");
    }
    else
    {
        char * next = NULL;
        Boolean head = FALSE;
#undef X
#define X(TYPE,STRING) STRING,
        static const char * random_argument_type_strings[] = {
            ARG_RANDOM_TYPE_LIST
        };
#undef X
        (*c)++;
        while(*c<argc)
        {
            next = argv[*c];

            Boolean success = FALSE;
            for(i=0;i<arg_count;i++)
            {
                for(int k=-1; k<(int)cmd_line_args[i].nargpairs; k++)
                {
                    struct cmd_line_arg_t * a = &cmd_line_args[i];
                    char * argname =
                        k == -1 ?
                        (char*)a->name :
                        cmd_line_args[i].argpairs[k].string;

                    if(Strings_equal(next,argname))
                    {
                        /*
                         * Found matching help exactly
                         */
                        char * default_string = arg_variable_default_string(a);
                        char * help_string;
                        if(k >= 0)
                        {
                            /* expand <n> as macro */
                            char * help_format_string = strdup(a->help);
                            string_replace(&help_format_string,
                                           "<n>",
                                           "%s");
                            if(asprintf(&help_string,
                                        help_format_string,
                                        cmd_line_args[i].argpairs[k].macro) == 0)
                            {
                                Exit_binary_c(BINARY_C_ALLOC_FAILED,
                                              "Failed to asprintf format string when trying to help all.");
                            }
                        }
                        else
                        {
                            help_string = strdup(a->help);
                        }

                        Printf("binary_c help for variable : %s <%s>\n\n%s\n\nDefault : %s\n",
                               next,
                               Argtype_is_string(a->type),
                               help_string,
                               default_string
                            );
                        Safe_free(default_string);
                        Safe_free(help_string);

                        if(a->npairs > 0)
                        {
                            Printf("Available macros:\n\n");
                            for(unsigned int j=0;j<a->npairs;j++)
                            {
                                Printf("%s = %g\n",
                                       a->pairs[j].string,
                                       a->pairs[j].value
                                    );
                            }
                        }
                        if(a->random_arg_type != ARG_RANDOM_NONE)
                        {
                            Printf("Random variation : %s ",
                                   random_argument_type_strings[a->random_arg_type]);

                            if(a->random_arg_type == ARG_RANDOM_INT)
                            {
                                Printf("between %d and %d ",
                                       a->random_lower._int,
                                       a->random_upper._int);

                            }
                            else if(a->random_arg_type == ARG_RANDOM_BOOLEAN)
                            {
                            }
                            else if(a->random_arg_type == ARG_RANDOM_DOUBLE)
                            {
                                Printf("between %g and %g ",
                                       a->random_lower._double,
                                       a->random_upper._double);
                            }
                            else if(a->random_arg_type == ARG_RANDOM_LOGDOUBLE)
                            {
                                Printf("between %g and %g ",
                                       a->random_lower._double,
                                       a->random_upper._double);
                            }
                            else if(a->random_arg_type == ARG_RANDOM_INT_ARRAY)
                            {
                                Printf("from ");
                                for(unsigned int j=0; j<a->random_array_length; j++)
                                {
                                    Printf("%d%s",
                                           a->random_array[j]._int,
                                           j == a->random_array_length - 1 ? "" : ",");
                                }
                            }
                            else if(a->random_arg_type == ARG_RANDOM_DOUBLE_ARRAY)
                            {
                                Printf("from [");
                                for(unsigned int j=0; j<a->random_array_length; j++)
                                {
                                    Printf("%g%s",
                                           a->random_array[j]._double,
                                           j == a->random_array_length - 1 ? "" : ",");
                                }
                                Printf("]");
                            }
                            else if(a->random_arg_type == ARG_RANDOM_INT_ARRAY_OR_INT_SCALAR)
                            {
                                Printf("between %d and %d or from [",
                                       a->random_lower._int,
                                       a->random_upper._int);
                                for(unsigned int j=0; j<a->random_array_length; j++)
                                {
                                    Printf("%d%s",
                                           a->random_array[j]._int,
                                           j == a->random_array_length - 1 ? "" : ",");
                                }
                                Printf("]");
                            }
                            else if(a->random_arg_type == ARG_RANDOM_INT_ARRAY_OR_DOUBLE_SCALAR)
                            {
                                Printf("from [");
                                for(unsigned int j=0; j<a->random_array_length; j++)
                                {
                                    Printf("%d%s",
                                           a->random_array[j]._int,
                                           j == a->random_array_length - 1 ? "" : ",");
                                }
                                Printf("] or in range %g to %g",
                                       a->random_lower._double,
                                       a->random_upper._double);
                            }
                            else if(a->random_arg_type == ARG_RANDOM_DOUBLE_ARRAY_OR_DOUBLE_SCALAR)
                            {
                                Printf("from [");
                                for(unsigned int j=0; j<a->random_array_length; j++)
                                {
                                    Printf("%g%s",
                                           a->random_array[j]._double,
                                           j == a->random_array_length - 1 ? "" : ",");
                                }
                                Printf("] or in range %g to %g",
                                       a->random_lower._double,
                                       a->random_upper._double);
                            }
                            Printf("\n");
                        }
                        Printf("\n\n\n");
                        success = TRUE;
                    }
                }

                if(success == FALSE)
                {
                    /*
                     * Search for substrings
                     */
                    for(int k=-1; k<(int)cmd_line_args[i].nargpairs; k++)
                    {
                        struct cmd_line_arg_t * a = &cmd_line_args[i];
                        char * argname =
                            k == -1 ?
                            (char*)a->name :
                            cmd_line_args[i].argpairs[k].string;
                        if(strstr(cmd_line_args[i].name,next) != NULL)
                        {
                            if(head == FALSE)
                            {
                                Printf("Did you mean :\n\n");
                                head = TRUE;
                            }
                            Printf("  %s\n",argname);
                        }
                    }
                }
            }

            if(success==FALSE) Printf("\nBinary_c help : no help available for %s\n",next);
            (*c)++;
        }

        if(exit_help == TRUE)
        {
            Exit_binary_c(BINARY_C_NORMAL_EXIT,
                          "Exit after help\n");
        }
        else
        {
            return;
        }
    }
}


void binary_c_help_all_from_arg(ARG_SUBROUTINE_DECLARATION)
{
    /*
     * Show help for all arguments (into the buffer
     * so stardata should not be NULL)
     *
     * If argc < 0 and batchmode is off, we then exit.
     */
    if(stardata != NULL)
    {
        const Boolean exit_help_all =
            (argc >= 0 &&
             stardata->preferences &&
             Batchmode_is_off(stardata->preferences->batchmode)) ? TRUE : FALSE;
#undef X
#define X(LABEL,STRING) STRING,
        static const char * const cc[] = { ARG_SECTIONS_LIST };
#undef X

        int j;

        for(j=0;j<NUMBER_OF_ARG_SECTIONS;j++)
        {
            Printf("\n############################################################\n##### Section %s\n############################################################\n",cc[j]);

            for(i=0;i<arg_count;i++)
            {
                if(cmd_line_args[i].section == j)
                {
                    Printf("%s : %s : %s\n",
                           cmd_line_args[i].name,
                           cmd_line_args[i].help,
                           cmd_line_args[i].wtts_string
                        );

                    /*
                     * Expand macro lists
                     */
                    if(cmd_line_args[i].nargpairs > 0)
                    {
                        for(unsigned int k=0; k<cmd_line_args[i].nargpairs; k++)
                        {
                            /*
                             * In the help string, replace <n> with the
                             * macro.
                             */
                            char * help_format_string = strdup(cmd_line_args[i].help);
                            char * help_string = NULL;
                            string_replace(&help_format_string,
                                           "<n>",
                                           "%s");
                            if(asprintf(&help_string,
                                        help_format_string,
                                        cmd_line_args[i].argpairs[k].macro) == 0)
                            {
                                Exit_binary_c(BINARY_C_ALLOC_FAILED,
                                              "Failed to asprintf format string when trying to help all.");
                            }

                            Printf("%s : %s : %s\n",
                                   cmd_line_args[i].argpairs[k].string,
                                   help_string,
                                   cmd_line_args[i].wtts_string
                                );
                            Safe_free(help_format_string);
                            Safe_free(help_string);
                        }
                    }
                }
            }
        }

        if(exit_help_all == TRUE)
        {
            Exit_binary_c(BINARY_C_NORMAL_EXIT,NULL);
        }
    }
}


void binary_c_argopts_from_arg(ARG_SUBROUTINE_DECLARATION)
{
    /*
     * Output the options available to a given argument.
     */
    const Boolean exit_help = (argc >= 0 &&
                               stardata->preferences &&
                               Batchmode_is_off(stardata->preferences->batchmode)) ? TRUE : FALSE;
    if(argc < 0) argc = -argc;

    if(*c==argc-1)
    {
        /* no extra help required */
        Printf("Usage: binary_c <cmd line args>\n\nwhere the arguments are a selection of : \n\n");
        for(i=0;i<arg_count;i++)
        {
            if(cmd_line_args[i].type != BATCH_ARG_SUBROUTINE &&
               cmd_line_args[i].type != BATCH_ARG_BOOLEAN &&
               cmd_line_args[i].type != BATCH_ARG_NOT_BOOLEAN)
                Printf("%s ",cmd_line_args[i].name);

            _scanf_options(stardata,
                           &cmd_line_args[i]);
        }

        Printf("\n\nTry running\n\nbinary_c argopts <argname>\n\nfor more help on a particular argument.\n\n");

        Exit_binary_c(BINARY_C_QUIET_EXIT,"No command line args given.");
    }
    else
    {
        Boolean head = FALSE;
        (*c)++;
        while(*c<argc)
        {
            char * next = strdup(argv[*c]);

            Boolean success = FALSE;
            for(i=0;i<arg_count;i++)
            {
                struct cmd_line_arg_t * const a = &cmd_line_args[i];
                if(Strings_equal(next,a->name))
                {
                    if(a->npairs > 0)
                    {
                        /*
                         * Show macros, not the integers
                         * associated with them
                         */
                        for(unsigned int j=0;j<a->npairs;j++)
                        {
                            Printf("%s\n",
                                   a->pairs[j].string
                                );
                        }
                    }
                    else
                    {
#undef X
#define X(LABEL,SIZEOF,OPTIONS) OPTIONS,
                        static const char * const optstrings[] = { ARG_TYPES_LIST };
#undef X
                        /*
                         * Show the string associated with
                         * this arg type
                         */
                        Printf("\"%s\"\n", optstrings[a->type]);
                    }
                    success=TRUE;
                }

                if(success==FALSE)
                {
                    char * scanp = NULL;
                    if(Argtype_is_scanf(a->type))
                    {
                        /*
                         * scanf arguments:
                         * replace %d (or whatever) with \0
                         * to then scan a substring
                         */
                        char * name = strdup(a->name);
                        scanp = strstr(name,"%d");
                        const ptrdiff_t len = scanp - name;
                        *(name+len) = '\0';
                        if(memcmp(name,next,len)==0)
                        {
                            if(a->npairs > 0)
                            {
                                /*
                                 * Show macros, not the integers
                                 * associated with them
                                 */
                                for(unsigned int j=0;j<a->npairs;j++)
                                {
                                    Printf("%s\n",
                                           a->pairs[j].string
                                        );
                                }
                            }
                            else
                            {
#undef X
#define X(LABEL,SIZEOF,OPTIONS) OPTIONS,
                                static const char * const optstrings[] = { ARG_TYPES_LIST };
#undef X
                                /*
                                 * Show the string associated with
                                 * this arg type
                                 */
                                Printf("\"%s\"\n", optstrings[a->type]);
                            }

                            success=TRUE;
                        }
                        Safe_free(name);
                    }
                    else
                    {
                        /*
                         * Search for substrings
                         */
                        if(strstr(a->name,next) != NULL)
                        {
                            if(head == FALSE)
                            {
                                Printf("Did you mean :\n\n");
                                head = TRUE;
                            }
                            Printf("  %s\n",a->name);
                        }
                    }
                }
            }

            if(success==FALSE) Printf("\nBinary_c help : no help available for %s\n",next);
            (*c)++;
            Safe_free(next);
        }

        if(exit_help == TRUE)
        {
            /*
             * Quietly exit
             */
            Exit_binary_c(BINARY_C_QUIET_EXIT,
                          "Exit after help\n");
        }
        else
        {
            return;
        }
    }
}



#ifdef BATCHMODE

void toggle_batchmode(ARG_SUBROUTINE_DECLARATION)
{
    /* toggle batchmode Boolean */
    stardata->preferences->batchmode =
    stardata->preferences->batchmode == TRUE ? FALSE : TRUE;
}
#endif //BATCHMODE


void reset_stars_defaults(ARG_SUBROUTINE_DECLARATION)
{
    // reset_stars combined with defaults
    Star_number k;
    Number_of_stars_Starloop(k)
    {
        SETstar(k);
        init_star(stardata,star);
        star->starnum = k;
    }
    init_model(&(stardata->model));
    init_common(stardata);

    // defaults
    default_stardata(stardata);
}

void No_return list_available_args_and_exit(ARG_SUBROUTINE_DECLARATION)
{
    list_available_args(ARG_SUBROUTINE_ARGS2);
    Exit_binary_c(BINARY_C_NORMAL_EXIT,"Exit after listing arguments");
}

void list_available_args(ARG_SUBROUTINE_DECLARATION)
{
    /*
     * For binary_grid Perl module : list available args
     */
    Printf("__ARG_BEGIN\n");


    int buffering = stardata->preferences->internal_buffering;
    reset_stars_defaults(ARG_SUBROUTINE_ARGS2);
    stardata->preferences->internal_buffering = buffering;

    *c=-1;
    for(i=0;i<arg_count;i++)
    {
        struct cmd_line_arg_t * a = &cmd_line_args[i];
        if(! Argtype_is_batch(a->type))
        {
            Printf("%s = ",a->name);
            if(a->pointer == NULL)
            {
                if(Strings_equal(a->name, "defaults_set"))
                {
                    Printf("%s",
                           defaults_sets_strings[stardata->preferences->defaults_set]);
                }
                else
                {
                    Printf("NULL");
                }
            }
            else
            {
                switch(a->type)
                {
                case ARG_NONE:
                    /* do nothing */
                    break;
                case ARG_DOUBLE:
                case ARG_INTEGER:
                case ARG_UNSIGNED_INTEGER:
                case ARG_LONG_INTEGER:
                case ARG_STRING:
                case ARG_BOOLEAN:
                case ARG_NOT_BOOLEAN:
                {
                    char * cc = arg_variable_default_string(a);
                    if(strlen(cc)==0)
                    {
                        /* empty string */
                        Printf("empty_string");
                    }
                    else
                    {
                        Printf("%s",cc);
                    }
                    Safe_free(cc);
                }
                break;
                case ARG_SUBROUTINE:
                    if(Strings_equal(a->name, "defaults_set"))
                    {
                        Printf("%s",
                               defaults_sets_strings[stardata->preferences->defaults_set]);
                    }
                    else
                    {
                        Printf("Function %s",
                               a->function_pointer_name
                            );
                    }
                    break;
                }
            }
            Printf("\n");
        }
    }

    Printf("__ARG_END\n");
}



void dummyfunc(ARG_SUBROUTINE_DECLARATION)
{
    /* function to do nothing */
}

void binary_c_version_internal(ARG_SUBROUTINE_DECLARATION)
{
    /* show version information */
    version(stardata);
}

void binary_c_set_version_boolean(ARG_SUBROUTINE_DECLARATION)
{
    /* allocations are for the isotope list */
    stardata->preferences->show_version = TRUE;
}

void version_only(ARG_SUBROUTINE_DECLARATION)
{
    stardata->preferences->speedtests = FALSE;
    version(stardata);
}

void No_return dumpversion(ARG_SUBROUTINE_DECLARATION)
{
    Printf("%s\n",BINARY_C_VERSION);
    Exit_binary_c(BINARY_C_QUIET_EXIT,NULL);
}


void set_init_abund_mult(ARG_SUBROUTINE_DECLARATION)
{
    (*c)++;
    const Isotope n Maybe_unused = (Isotope)Long_int_to_unsigned_short_int(strtol(argv[*c],NULL,10));
    (*c)++;
#ifdef NUCSYN
    const double f = fast_strtod(argv[*c],NULL);
    if(n<ISOTOPE_ARRAY_SIZE) stardata->preferences->initial_abundance_multiplier[n]=f;
#endif
}

void set_init_abund_dex(ARG_SUBROUTINE_DECLARATION)
{
    (*c)++;
    const Isotope n Maybe_unused = (Isotope)Long_int_to_unsigned_short_int(strtol(argv[*c],NULL,10));
    (*c)++;
#ifdef NUCSYN
    const double f = exp10(fast_strtod(argv[*c],NULL));
    if(n<ISOTOPE_ARRAY_SIZE) stardata->preferences->initial_abundance_multiplier[n]=f;
    //fprintf(stderr,"SET MULT %d to %g\n",n,f);
#endif
}

void set_init_abund(ARG_SUBROUTINE_DECLARATION)
{
    (*c)++;
    const Isotope n Maybe_unused = (Isotope)Long_int_to_unsigned_short_int(strtol(argv[*c],NULL,10));
    (*c)++;
#ifdef NUCSYN
    const double X = fast_strtod(argv[*c],NULL);
    if(n<ISOTOPE_ARRAY_SIZE) stardata->preferences->the_initial_abundances[n]=X;
#endif
}

void set_third_dup_multiplier(ARG_SUBROUTINE_DECLARATION)
{
    (*c)++;
    const Isotope n Maybe_unused = (Isotope)Long_int_to_unsigned_short_int(strtol(argv[*c],NULL,10));
    (*c)++;
#if defined NUCSYN &&                           \
    defined THIRD_DREDGE_UP &&                  \
    defined NUCSYN_THIRD_DREDGE_UP_MULTIPLIERS
    const double X = fast_strtod(argv[*c],NULL);
    if(n<ISOTOPE_ARRAY_SIZE) stardata->preferences->third_dup_multiplier[n]=X;
    preferences->boost_third_dup=TRUE;
#endif
}


void add_orbiting_object_from_argstring(ARG_SUBROUTINE_DECLARATION)
{
    (*c)++;
#ifdef ORBITING_OBJECTS
    add_zero_age_orbiting_object_from_arg(stardata,argv[*c]);
#endif // ORBITING_OBJECTS
}


static void _show_image_ascii_art(const unsigned char * const imagedata,
                                  const size_t imagesize)
{
    /*
     * Show the image in "imagedata", of size "imagesize"
     * as ascii art using jp2a (if available!)
     */

#undef exit
#define __show_image_exit                                 \
    Safe_free(tmpfile);                         \
    fflush(NULL);                               \
    exit(1);

    char * tmp = getenv("TEMP");
    char * tmpfile = NULL;

    if(!tmp)
    {
        tmp = "/tmp"; /* default to /tmp */
    }
    if(asprintf(&tmpfile,"%s/XXXXXX",tmp) == -1)
    {
        /* error! */
        __show_image_exit;
    }
    const int fd = mkstemp(tmpfile);
    if(fd != -1)
    {
        FILE * fp = fdopen(fd,"w");
        if(fp)
        {
            for(size_t ii=0;ii<imagesize+1;ii++)
            {
                if(fprintf(fp,"%c",imagedata[ii])<0)
                {
                    /* error! */
                    __show_image_exit;
                }
            }
            fclose(fp);
        }
        else
        {
            /* error! */
            __show_image_exit;
        }

        char * tmpfile2 = NULL;
        if(asprintf(&tmpfile2,"%s/XXXXXX",tmp))
        {
            char * cmd = NULL;
            const int fd2 = mkstemp(tmpfile2);
            if(fd2 != -1 &&
               asprintf(&cmd,"jp2a  --term-fit --colors --fill %s > /dev/stdout ",tmpfile))
            {
                if(system(cmd)<0){}
            }
            Safe_free(cmd);
            Safe_free(tmpfile2);
        }
    }
    __show_image_exit;
}

void bjorn(ARG_SUBROUTINE_DECLARATION)
{
#ifdef BJORN_FUN
#include "bjorn.h"
    _show_image_ascii_art(bin2c_bjorn_trans_png,
                          Array_size(bin2c_bjorn_trans_png));
#endif // BJORN_FUN
}

void binary_c_logo(ARG_SUBROUTINE_DECLARATION)
{
#ifdef BINARY_C_LOGO
#include "binary_c_logo.h"
    _show_image_ascii_art(bin2c_logo3_1600x1439_png,
                          Array_size(bin2c_logo3_1600x1439_png));
#endif // BINARY_C_LOGO
}


void set_merger_mass_loss_fraction_nondegenerate(ARG_SUBROUTINE_DECLARATION)
{
    /*
     * Set the merger mass-loss fraction for non-degenerate types
     */
    (*c)++;
    set_merger_mass_loss_fractions(stardata,
                                   argv[*c],
                                   LOW_MASS_MAIN_SEQUENCE,
                                   HeHG);
}

void set_merger_mass_loss_fraction_degenerate(ARG_SUBROUTINE_DECLARATION)
{
    /*
     * Set the merger mass-loss fraction for degenerate types
     */
    (*c)++;
    set_merger_mass_loss_fractions(stardata,
                                   argv[*c],
                                   HeWD,
                                   BLACK_HOLE);
}

static void _set_defaults_set(struct stardata_t * const stardata,
                              struct cmd_line_arg_t * const cmd_line_args Maybe_unused,
                              char ** argv,
                              int i,
                              int c);
void set_defaults_set(ARG_SUBROUTINE_DECLARATION)
{
    /*
     * Set the merger mass-loss fraction for degenerate types
     */
    if(*c+1>=argc)
    {
        Exit_binary_c(BINARY_C_WRONG_ARGUMENT,
                      "No argument has been provided following \"%s\" and one is required.",
                      argv[*c]);

    }
    cmd_line_args[i].pointer = &stardata->preferences->defaults_set;
    _set_defaults_set(stardata,
                      cmd_line_args,
                      argv,
                      i,
                      *c);
    (*c)++;
    set_default_preferences(stardata);
}
static void _set_defaults_set(struct stardata_t * const stardata,
                              struct cmd_line_arg_t * const cmd_line_args Maybe_unused,
                              char ** argv,
                              int i,
                              int c)
{
    Arg_set_int;
}

static void set_merger_mass_loss_fractions(struct stardata_t * const stardata,
                                           const char * const arg,
                                           const Stellar_type low,
                                           const Stellar_type high)
{
#undef X
#define X(CODE,NUM)                                                     \
    else if(Strings_equal(Stringify(MERGER_MASS_LOSS_FRACTION_##CODE),  \
                          arg))                                         \
    {                                                                   \
        stardata->preferences->merger_mass_loss_fraction_by_stellar_type[ii] = \
            MERGER_MASS_LOSS_FRACTION_##CODE;                           \
    }

    for(Stellar_type ii=low; ii<=high; ii++)
    {
        if(0){} // do not remove this loop : it does nothing for a reason!
        /*
         * Check if the arg string matches any of the merger mass loss macros...
         */
        MERGER_MASS_LOSS_FRACTION_LIST
        /*
         * If not, set the fraction from the given number
         */
        else
        {
            stardata->preferences->merger_mass_loss_fraction_by_stellar_type[ii]
                = fast_strtod(arg,NULL);
        }
    }
#undef X
}

void set_qcrit_nuclear_burning(ARG_SUBROUTINE_DECLARATION)
{
    /*
     * Set qcrit for all nuclear-burning donors
     * irrespective of donor type
     */
    (*c)++;
    _set_qcrit(stardata,
               argv[*c],
               LOW_MASS_MAIN_SEQUENCE,
               HeGB);
}

void set_qcrit_WD(ARG_SUBROUTINE_DECLARATION)
{
    /*
     * Set qcrit for all WD donors
     * irrespective of accretor type
     */
    (*c)++;
    _set_qcrit(stardata,
               argv[*c],
               HeWD,
               ONeWD);
}

void set_qcrit_all(ARG_SUBROUTINE_DECLARATION)
{
    /*
     * Set qcrit for all stars irrespective of
     * (donor or) accretor type
     */
    (*c)++;
    _set_qcrit(stardata,
               argv[*c],
               LOW_MASS_MAIN_SEQUENCE,
               BLACK_HOLE);
}

static void _set_qcrit(struct stardata_t * const stardata,
                       const char * const arg,
                       const Stellar_type low,
                       const Stellar_type high)
{
    char * arglist[4];
    for(Stellar_type ii=low; ii<=high; ii++)
    {
        if(asprintf(&arglist[0],
                    "qcrit_%s",
                    short_stellar_types[ii])<0)
        {
            arglist[0] = NULL;
        }
        if(asprintf(&arglist[1],
                    "%s",
                    arg)<0)
        {
            arglist[1] = NULL;
        }
        if(asprintf(&arglist[2],
                    "qcrit_degenerate_%s",
                    short_stellar_types[ii])<0)
        {
            arglist[2] = NULL;
        }
        if(asprintf(&arglist[3],
                    "%s",
                    arg)<0)
        {
            arglist[3] = NULL;
        }
        parse_arguments(0,4,arglist,stardata);
        Safe_free(arglist[0]);
        Safe_free(arglist[1]);
        Safe_free(arglist[2]);
        Safe_free(arglist[3]);
    }
}

void set_WD_accretion_rate_algorithms(ARG_SUBROUTINE_DECLARATION)
{
    static char * args[] = {
        "WD_accretion_rate_novae_upper_limit_hydrogen_donor",
        "WD_accretion_rate_novae_upper_limit_helium_donor",
        "WD_accretion_rate_novae_upper_limit_other_donor",
        "WD_accretion_rate_new_giant_envelope_lower_limit_hydrogen_donor",
        "WD_accretion_rate_new_giant_envelope_lower_limit_helium_donor",
        "WD_accretion_rate_new_giant_envelope_lower_limit_other_donor"
    };
    (*c)++;
    for(size_t ii=0; ii<Array_size(args); ii++)
    {
        char * pair[2];
        if(asprintf(&pair[0],
                 "%s",
                    args[ii])<0)
        {
            pair[0] = NULL;
        }
        if(asprintf(&pair[1],
                 "%s",
                    argv[*c])<0)
        {
            pair[1] = NULL;
        }
        parse_arguments(0,2,pair,stardata);
        Safe_free(pair[0]);
        Safe_free(pair[1]);
    }
}

static void _scanf_options(struct stardata_t * const stardata,
                           const struct cmd_line_arg_t * const arg)
{
    if(Argtype_is_scanf(arg->type) &&
       arg->nscanf_strings > 0 &&
       arg->scanf_strings != NULL)
    {
        const size_t len = strlen(arg->name);
        char * format = Calloc(2+len,sizeof(char));
        memcpy(format,arg->name,len);
        format[len-1] = 's';
        format[len] = ' ';
        for(unsigned int j=0; j<arg->nscanf_strings; j++)
        {
            Printf(format,
                   arg->scanf_strings[j]);
        }
        Safe_free(format);
    }
}

void call_random_systems_list(ARG_SUBROUTINE_DECLARATION)
{
    random_system_list(stardata);
}
