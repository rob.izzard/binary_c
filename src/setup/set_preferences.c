#include "../binary_c.h"
No_empty_translation_unit_warning;
#include "defaults_sets.h"

void set_preferences(struct preferences_t * Restrict const preferences)
{
    /*
     * Set the preferences struct according to
     * preferences->defaults_set
     */
    const int n = (preferences->defaults_set < 0 ||
             preferences->defaults_set >= DEFAULTS_SETS_NUMBER)
        ? DEFAULTS_SET_FALLBACK
        : preferences->defaults_set;
    default_preferences_functions[n](preferences);
}
