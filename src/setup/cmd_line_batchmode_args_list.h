#pragma once
#ifndef BINARY_C_CMD_LINE_BATCHMOD_ARGS_LIST_H
#define BINARY_C_CMD_LINE_BATCHMOD_ARGS_LIST_H

/*
 * These args are only defined with BATCHMODE defined
 */

#ifdef BATCHMODE
#define BATCHMODE_ARGS                                                  \
    {                                                                   \
        0,                                                              \
        ARG_SECTION_IO,                                                 \
        "go",                                                           \
        "batchmode control command",                                    \
        BATCH_ARG_SUBROUTINE,                                           \
        "Ignore",                                                       \
        NO_MAX_POINTER,                                                 \
        NO_LIMIT_TO_MULTIPLICITY,                                       \
        Sub(batchmode_go) ,                                             \
        1.0,                                                            \
        CMD_LINE_ARG_T_OPTIONAL_ARGS,                                   \
        CMD_LINE_ARG_T_NO_RANDOM_VARIATION },                           \
    {                                                                   \
        0,                                                              \
            ARG_SECTION_IO,                                             \
            "gogo",                                                     \
            "batchmode control command",                                \
            BATCH_ARG_SUBROUTINE,                                       \
            "Ignore",                                                   \
            NO_MAX_POINTER,                                             \
            NO_LIMIT_TO_MULTIPLICITY,                                   \
            Sub(batchmode_gogo) ,                                       \
            1.0,                                                        \
            CMD_LINE_ARG_T_OPTIONAL_ARGS,                               \
            CMD_LINE_ARG_T_NO_RANDOM_VARIATION },                       \
{                                                                       \
    0,                                                                  \
        ARG_SECTION_IO,                                                 \
        "reset_stars",                                                  \
        "Reset the star structures. Used in batchmode",                 \
        BATCH_ARG_SUBROUTINE,                                           \
        "Ignore",                                                       \
        NO_MAX_POINTER,                                                 \
        NO_LIMIT_TO_MULTIPLICITY,                                       \
        Sub(batchmode_reset_stars) ,                                    \
        1.0,                                                            \
        CMD_LINE_ARG_T_OPTIONAL_ARGS,                                   \
        CMD_LINE_ARG_T_NO_RANDOM_VARIATION },                           \
{                                                                       \
    0,                                                                  \
        ARG_SECTION_IO,                                                 \
        "reset_stars_defaults",                                         \
        "Reset the star structures and set defaults. Used in batchmode", \
        BATCH_ARG_SUBROUTINE,                                           \
        "Ignore",                                                       \
        NO_MAX_POINTER,                                                 \
        NO_LIMIT_TO_MULTIPLICITY,                                       \
        Sub(batchmode_reset_stars_defaults) ,                           \
        1.0,                                                            \
        CMD_LINE_ARG_T_OPTIONAL_ARGS,                                   \
        CMD_LINE_ARG_T_NO_RANDOM_VARIATION },                           \
{                                                                       \
    0,                                                                  \
        ARG_SECTION_IO,                                                 \
        "defaults",                                                     \
        "Reset all defaults. Used in batchmode",                        \
        BATCH_ARG_SUBROUTINE,                                           \
        "Ignore",                                                       \
        NO_MAX_POINTER,                                                 \
        NO_LIMIT_TO_MULTIPLICITY,                                       \
        Sub(batchmode_defaults) ,                                       \
        1.0,                                                            \
        CMD_LINE_ARG_T_OPTIONAL_ARGS,                                   \
        CMD_LINE_ARG_T_NO_RANDOM_VARIATION },                           \
{                                                                       \
    0,                                                                  \
        ARG_SECTION_IO,                                                 \
        "echo",                                                         \
        "Activate batchmode command echoing, i.e. when you enter a command, binary_c repeats the command then executes it.", \
        BATCH_ARG_SUBROUTINE,                                           \
        "Ignore",                                                       \
        NO_MAX_POINTER,                                                 \
        NO_LIMIT_TO_MULTIPLICITY,                                       \
        Sub(batchmode_set_echo_on),                                     \
        1.0,                                                            \
        CMD_LINE_ARG_T_OPTIONAL_ARGS,                                   \
        CMD_LINE_ARG_T_NO_RANDOM_VARIATION },                           \
{                                                                       \
    0,                                                                  \
        ARG_SECTION_IO,                                                 \
        "noecho",                                                       \
        "Deactivate batchmode command echoing. See 'echo'.",            \
        BATCH_ARG_SUBROUTINE,                                           \
        "Ignore",                                                       \
        NO_MAX_POINTER,                                                 \
        NO_LIMIT_TO_MULTIPLICITY,                                       \
        Sub(batchmode_set_echo_off),                                    \
        1.0,                                                            \
        CMD_LINE_ARG_T_OPTIONAL_ARGS,                                   \
        CMD_LINE_ARG_T_NO_RANDOM_VARIATION },                           \
{                                                                       \
    0,                                                                  \
        ARG_SECTION_IO,                                                 \
        "noechonow",                                                    \
        "Deactivate batchmode command echoing. See 'echo'.",            \
        BATCH_ARG_SUBROUTINE,                                           \
        "Ignore",                                                       \
        NO_MAX_POINTER,                                                 \
        NO_LIMIT_TO_MULTIPLICITY,                                       \
        Sub(batchmode_set_echo_off),                                    \
        1.0,                                                            \
        CMD_LINE_ARG_T_OPTIONAL_ARGS,                                   \
        CMD_LINE_ARG_T_NO_RANDOM_VARIATION },                           \
{                                                                       \
    0,                                                                  \
        ARG_SECTION_IO,                                                 \
        "bye",                                                          \
        "Quit binary_c. Used in batchmode.",                            \
        BATCH_ARG_SUBROUTINE,                                           \
        "Ignore",                                                       \
        NO_MAX_POINTER,                                                 \
        NO_LIMIT_TO_MULTIPLICITY,                                       \
        Sub(batchmode_bye) ,                                            \
        1.0,                                                            \
        CMD_LINE_ARG_T_OPTIONAL_ARGS,                                   \
        CMD_LINE_ARG_T_NO_RANDOM_VARIATION },                           \
{                                                                       \
    0,                                                                  \
        ARG_SECTION_IO,                                                 \
        "fin",                                                          \
        "batchmode control command",                                    \
        BATCH_ARG_SUBROUTINE,                                           \
        "Ignore",                                                       \
        NO_MAX_POINTER,                                                 \
        NO_LIMIT_TO_MULTIPLICITY,                                       \
        Sub(batchmode_fin) ,                                            \
        1.0,                                                            \
        CMD_LINE_ARG_T_OPTIONAL_ARGS,                                   \
        CMD_LINE_ARG_T_NO_RANDOM_VARIATION },                           \
{                                                                       \
    0,                                                                  \
        ARG_SECTION_IO,                                                 \
        "reset_prefs",                                                  \
        "Reset preferences struct. Used in batchmode",                  \
        BATCH_ARG_SUBROUTINE,                                           \
        "Ignore",                                                       \
        NO_MAX_POINTER,                                                 \
        NO_LIMIT_TO_MULTIPLICITY,                                       \
        Sub(batchmode_reset_prefs) ,                                    \
        1.0,                                                            \
        CMD_LINE_ARG_T_OPTIONAL_ARGS,                                   \
        CMD_LINE_ARG_T_NO_RANDOM_VARIATION },                           \
{                                                                       \
    0,                                                                  \
        ARG_SECTION_IO,                                                 \
        "status",                                                       \
        "Output batchmode status information.",                         \
        BATCH_ARG_SUBROUTINE,                                           \
        "Ignore",                                                       \
        NO_MAX_POINTER,                                                 \
        NO_LIMIT_TO_MULTIPLICITY,                                       \
        Sub(batchmode_status) ,                                         \
        1.0,                                                            \
        CMD_LINE_ARG_T_OPTIONAL_ARGS,                                   \
        CMD_LINE_ARG_T_NO_RANDOM_VARIATION }                            \
    , /* final comma is required! */

#else // BATCHMODE
#define BATCHMODE_ARGS
#endif // BATCHMODE

#endif // BINARY_C_CMD_LINE_BATCHMOD_ARGS_LIST_H
