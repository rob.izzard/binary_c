#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef KEMP_NOVAE

Accretion_regime H_accretion_regime_WD(struct stardata_t * const Restrict stardata,
                                 struct star_t * const Restrict accretor)
{
    /*
     * Regime boundaries from Kato et al 2014
     * Returns a RegimeType identifier denoting accretion regime.
     *
     * 1. Red Giant (RG) Regime "RG_H"
     * 2. Steady Accretion Regime "STEADYBURN_H"
     * 3. Strong Flash Regime "STRONGFLASH_H"
     *
     * The regime you're in depends on your WD mass
     * and the accretion rate onto the WD
     */

    const double Mdot = Mdot_net(accretor);
    Accretion_regime regime = NO_REGIME_DETERMINABLE;

    if(Mdot > VERY_TINY)
    {
        const double M_WD = accretor->mass;
        const double Mdot_log10=log10(Mdot);

        /*
         * boundary curves
         */
        double OTWH_log10mdot = 0.0;
        double steadyburn_log10mdot = 0.0;

        /*
         * Steady burning upper limit (Optically thick wind H):
         */
        if(stardata->preferences->WD_accretion_rate_new_giant_envelope_lower_limit_hydrogen_donor==DONOR_RATE_ALGORITHM_K2014_P2014)
        {
            OTWH_log10mdot = 1.2909 * Pow3(M_WD) -4.8608 * Pow2(M_WD) + 6.5776 * M_WD - 9.4322;
        }
        else if(
            stardata->preferences->WD_accretion_rate_new_giant_envelope_lower_limit_hydrogen_donor==DONOR_RATE_ALGORITHM_WANGWU
            &&
            M_WD >= 0.5
            )
        {
            /* interppolate */
            OTWH_log10mdot = log10(0.27*1e-7 * (Pow2(M_WD) + 25.52*M_WD - 9.02));
        }
        else if(
            stardata->preferences->WD_accretion_rate_new_giant_envelope_lower_limit_hydrogen_donor==DONOR_RATE_ALGORITHM_WANGWU
            &&
            M_WD<0.5
            )
        {
            /* extrapolate */
            OTWH_log10mdot = 2.712119930154697 * M_WD -8.323723305231614;
        }


        //steady_burning lower limit (below which, may get novae):
        if(stardata->preferences->WD_accretion_rate_novae_upper_limit_hydrogen_donor==DONOR_RATE_ALGORITHM_K2014_P2014)
        {
            steadyburn_log10mdot = 0.8947 * Pow3(M_WD) - 3.929 * Pow2(M_WD) + 6.2237 * M_WD -9.9133;
        }
        else if(
            stardata->preferences->WD_accretion_rate_novae_upper_limit_hydrogen_donor==DONOR_RATE_ALGORITHM_WANGWU
            &&
            M_WD >= 0.5
            )
        {
            /* interpolate */
            steadyburn_log10mdot = log10(2.93*1e-7 * (Pow3(-M_WD) + 4.41*Pow2(M_WD)-3.38*M_WD + 0.84));
        }
        else if(
            stardata->preferences->WD_accretion_rate_novae_upper_limit_hydrogen_donor==DONOR_RATE_ALGORITHM_WANGWU
            &&
            M_WD<0.5
            )
        {
            /* extrapolate */
            steadyburn_log10mdot=2.5 * M_WD -8.677622194875916;
        }

        if(Mdot_log10 > OTWH_log10mdot)
        {
            regime = RG_H;
        }
        else if(
            Mdot_log10<OTWH_log10mdot
            &&
            Mdot_log10>steadyburn_log10mdot
            )
        {
            regime = STEADYBURN_H;
        }
        else if(Mdot_log10 < steadyburn_log10mdot)
        {
            regime = STRONGFLASH_H;
        }
    }
    return regime;
}
#endif //KEMP_NOVAE
