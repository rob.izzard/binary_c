
#include "../binary_c.h"
No_empty_translation_unit_warning;


void set_nuclear_timescale_and_slowdown_factor(struct stardata_t * Restrict const stardata)
{

    /*
     * Set the nuclear timescale in years and slow-down factor.
     *
     * the factor RLOF_speed_up_factor will be used to speed up (or slow down) the 
     * time evolution in the case that we go into RLOF 
     */
    stardata->common.RLOF_speed_up_factor = 1e3*stardata->model.dtm0/
        stardata->common.orbit.period;
  
    Dprint("RLOF_speed_up_factor=%g (dtm0=%g P=%g)\n",
           stardata->common.RLOF_speed_up_factor,
           stardata->model.dtm0,
           stardata->common.orbit.period
        );

    if(stardata->common.RLOF_speed_up_factor<TINY) stardata->common.RLOF_speed_up_factor = 0.50;

}
