#include "../binary_c.h"
No_empty_translation_unit_warning;

void artificial_angular_momentum_accretion_rates(struct stardata_t * const stardata)
{
    /*
     * Artificial angular momentum accretion, e.g. from the ISM
     */
    if(artificial_accretion(stardata))
    {
        stardata->model.derivative[DERIVATIVE_ORBIT_ANGMOM_ARTIFICIAL] =
            stardata->preferences->artificial_orbital_angular_momentum_accretion_rate;

        Foreach_star(star)
        {
            star->derivative[DERIVATIVE_STELLAR_ANGMOM_ARTIFICIAL] =
                stardata->preferences->artificial_angular_momentum_accretion_rate[star->starnum];
        }
    }
}
