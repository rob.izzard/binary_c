#include "../binary_c.h"
No_empty_translation_unit_warning;


void set_effective_radii(struct stardata_t * Restrict const stardata)
{

    /*
     * For each star, determine the effective radius, which
     * during RLOF is the Roche Lobe radius, because the actual
     * radius may be much greater.
     */
    Star_number k;
    Starloop(k)
    {
        SETstar(k);
        star->effective_radius =
            (star->roche_radius>TINY && star->radius>star->roche_radius) ?
            Max(star->roche_radius,star->core_radius) : star->radius;
    }
}
