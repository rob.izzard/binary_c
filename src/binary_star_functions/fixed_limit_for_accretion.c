#include "../binary_c.h"
No_empty_translation_unit_warning;


/*
 * Fixed mass transfer rate accretion limit
 *
 */
double Pure_function fixed_limit_for_accretion(
    struct star_t * RESTRICT donor,
    struct stardata_t * RESTRICT const stardata)
{
    double fixed_accretion_rate;
    if(-donor->derivative[DERIVATIVE_STELLAR_MASS_RLOF_LOSS] > 0)
    {
        fixed_accretion_rate = stardata->preferences->fixed_beta_mass_transfer_efficiency * -donor->derivative[DERIVATIVE_STELLAR_MASS_RLOF_LOSS];
    }
    else
    {
        fixed_accretion_rate = VERY_LARGE_MASS_TRANSFER_RATE;
    }

    // Printf("fixed_accretion_rate %g stardata->preferences->fixed_beta_MT %g donor->derivative[DERIVATIVE_STELLAR_MASS_RLOF_LOSS] %g Mdot_net(accretor): %g\n", fixed_accretion_rate, stardata->preferences->fixed_beta_MT, donor->derivative[DERIVATIVE_STELLAR_MASS_RLOF_LOSS], Mdot_net(accretor));
    return fixed_accretion_rate;
}
