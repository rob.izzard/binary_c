#include "../binary_c.h"
No_empty_translation_unit_warning;


/*
 * Set wind-accretion luminosities,
 * designed to detect symbiotic systems.
 */
void set_wind_accretion_luminosities(struct stardata_t * Restrict const stardata)
{
    Foreach_star(star)
    {
        star->accretion_luminosity =
            star->stellar_type == MASSLESS_REMNANT ? 0.0 :
            (Mdot_gain(star) * M_SUN/YEAR_LENGTH_IN_SECONDS
             * star->mass * M_SUN
             / (R_SUN * star->radius)
             / L_SUN);
    }
}
