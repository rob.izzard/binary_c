#include "../binary_c.h"
No_empty_translation_unit_warning;

static double merger_mass_loss_fraction_Glebbeek2013(struct stardata_t * const stardata);
static double merger_mass_loss_fraction_Radice2018(struct stardata_t * const stardata);

double merger_mass_loss_fraction(struct stardata_t * const stardata,
                                 const Stellar_type merged_stellar_type)
{
    /*
     * Choose the value to use, or algorithm.
     *
     * First we check if the _by_stellar_type is set,
     * if not use the global value.
     */
    const double x =
        !Fequal(stardata->preferences->merger_mass_loss_fraction_by_stellar_type[merged_stellar_type],
                MERGER_MASS_LOSS_FRACTION_UNUSED) ?
        /* _by_stellar_type is set */
        stardata->preferences->merger_mass_loss_fraction_by_stellar_type[merged_stellar_type] :
        /* ... _by_stellar_type not set, use global value */
        stardata->preferences->merger_mass_loss_fraction;

    /*
     * Hence the fraction of mass lost
     */
    const double f =
        /* if >=0, use the value given */
        More_or_equal(x, 0.0) ? x :

        /*
         * Special case: NSNS and NSBH mergers
         */
        (
            (
                (
                    merged_stellar_type == NS ||
                    merged_stellar_type == BH
                    )
                &&
                (
                    stardata->star[0].stellar_type == NS ||
                    stardata->star[1].stellar_type == NS
                    )
                )
            )
        &&
            Fequal(stardata->preferences->merger_mass_loss_fraction_by_stellar_type[NS],
                   MERGER_MASS_LOSS_FRACTION_RADICE2018) ?
        merger_mass_loss_fraction_Radice2018(stardata) :

        /* compact objects keep everything if not specified */
        COMPACT_OBJECT(merged_stellar_type) ? 0.0 :

        /*
         * Special case for non-compact stars : general Glebbeek algorithm
         */
        Fequal(x, MERGER_MASS_LOSS_FRACTION_GLEBBEEK2013) ? merger_mass_loss_fraction_Glebbeek2013(stardata) :

        /* or zero if we don't know what to do*/
        0.0;

    return Max(0.0, Min(1.0, f));;
}

static double merger_mass_loss_fraction_Glebbeek2013(struct stardata_t * const stardata)
{
    /*
     * Mass loss fraction from Glebbeek et al. (2013)
     * MNRAS 434, 3497
     *
     * q < 1
     */
    double q = stardata->star[0].mass / stardata->star[1].mass;
    if(q > 1.0)
    {
        q = 1.0 / q;
    }
    return 0.3 * q / Pow2( 1.0 + q );
}

static double merger_mass_loss_fraction_Radice2018(struct stardata_t * const stardata)
{
    /*
     * Use the Radice2018 data to estimate the mass loss rate
     */
    if(stardata->persistent_data->Radice2018_ejecta == NULL)
    {
        init_Radice2018(stardata);
    }

    const Star_number i = stardata->star[0].mass > stardata->star[1].mass ? 0 : 1;
    const double x[] = {
        stardata->star[i].mass, /* most massive mass */
        stardata->star[Other_star(i)].mass /* least massive mass */
    };
    double * XNSNS = Malloc(sizeof(double) * stardata->persistent_data->Radice2018_ejecta->ndata);
    Interpolate(stardata->persistent_data->Radice2018_ejecta,
                x,
                XNSNS,
                FALSE);
    const double fej = XNSNS[0] / Sum_of_stars(mass);
    Safe_free(XNSNS);
    return fej;
}
