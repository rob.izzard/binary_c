#include "../binary_c.h"
No_empty_translation_unit_warning;

Boolean derivative_checks(struct stardata_t * const stardata)
{
    /*
     * When derivatives are small, or clash, they may lead
     * to physics that makes no sense. Try to deal with these
     * situations.
     *
     * When triggered is TRUE, we call this function again to
     * make sure the problem is eradicated.
     *
     * Note: these derivatives are in "smooth" evolution, not
     * sudden events like supernovae or common-envelope evolution.
     */
    return FALSE;
    Boolean triggered = FALSE;
    Foreach_star(star)
    {
        /*
         * Check that GB and TPAGB stars
         * don't have their surfaces inside the core
         */
        if((star->stellar_type == GIANT_BRANCH ||
            star->stellar_type == TPAGB))
        {
            const double dm_star = stardata->model.dt * Mdot_net(star);
            const double dm_core = stardata->model.dt * star->derivative[DERIVATIVE_STELLAR_He_CORE_MASS];

            if(star->mass + dm_star < star->core_mass[CORE_He] + dm_core)
            {
                triggered = TRUE;
                Dprint("TPAGB core eaten into M will be %30.20e, Mc will be %30.20e, Menv will be %g : adjust mass loss derivatives to compensate\n",
                       star->mass + dm_star,
                       star->core_mass[CORE_He] + dm_core,
                       star->mass + dm_star - (star->core_mass[CORE_He] + dm_core)
                    );
                //show_derivatives(stardata);

                /*
                 * We want M + dm_star = Mc + dm_core.
                 *
                 * We achieve this by scaling the mass change rates
                 * and the core growth rates by f.
                 *
                 * M + f * dm_star = Mc + f * dm_core
                 *
                 * hence
                 *
                 * f = (M - Mc) / (dm_core - dm_star)
                 *
                 * Note that this f must also apply to
                 * DERIVATIVE_STELLAR_He_CORE_MASS_NO_TDUP and
                 * DERIVATIVE_STELLAR_He_CORE_MASS if we are a TPAGB star
                 */

                const double f = (star->mass - star->core_mass[CORE_He]) / (dm_core - dm_star);

                unsigned int i;
                static const int loss[] = { Mdot_loss_derivatives };
                static const int gain[] = { Mdot_gain_derivatives };
                for(i=0;i<sizeof(loss)/sizeof(loss[0]);i++)
                {
                    star->derivative[abs(loss[i])] *= f;
                }
                for(i=0;i<sizeof(gain)/sizeof(gain[0]);i++)
                {
                    star->derivative[abs(gain[i])] *= f;
                }
                star->derivative[DERIVATIVE_STELLAR_He_CORE_MASS] *= f;
                if(star->stellar_type == TPAGB)
                {
                    star->derivative[DERIVATIVE_STELLAR_He_CORE_MASS_NO_TDUP] *= f;
                    star->derivative[DERIVATIVE_STELLAR_CO_CORE_MASS] *= f;
                }
                Dprint("Now expect M = %30.20e Mc = %30.20e, i.e. Menv = %g \n",
                       (star->mass + stardata->model.dt * Mdot_net(star)),
                       star->core_mass[CORE_He] + stardata->model.dt * star->derivative[DERIVATIVE_STELLAR_He_CORE_MASS],
                       star->mass + stardata->model.dt * Mdot_net(star) - (star->core_mass[CORE_He] + stardata->model.dt * star->derivative[DERIVATIVE_STELLAR_He_CORE_MASS])
                    );

                Append_logstring(LOG_DERIVATIVE,
                                 "0DERIV warning: modulate mass and core mass derivatives of star %d by %g, cannot reject dt=%gy",
                                 star->starnum,
                                 f,
                                 stardata->model.dt
                    );
                printf(
                                 "0DERIV warning: modulate mass and core mass derivatives of star %d by %g, cannot reject dt=%gy",
                                 star->starnum,
                                 f,
                                 stardata->model.dt
                    );
                //Flexit;
            }
        }

        /*
         * Check that the star does not lose > (1-fmax) of its mass in
         * one timestep.
         */
        if(1)
        {
            Dprint("Mcheck is %g will be %g\n",
                   star->mass,
                   star->mass + stardata->model.dt * Mdot_net(star));
            const double fmax = 0.1;
            if(star->mass + stardata->model.dt * Mdot_net(star) < (1.0 - fmax) * star->mass)
            {
                /*
                 * We want
                 *
                 * M + dM >= 0.9 M
                 *
                 * so modulate dM -> f * dM
                 *
                 * M + f dM >= 0.9 M
                 *
                 * hence
                 *
                 * f >= -0.1 M /dM
                 *
                 * and we choose the smallest f to be as close as
                 * possible to the requested derivative.
                 */
                triggered = TRUE;
                const double f = -0.1 * star->mass / (Mdot_net(star) * stardata->model.dt);
                unsigned int i;
                static const int loss[] = { Mdot_loss_derivatives };
                static const int gain[] = { Mdot_gain_derivatives };
                for(i=0;i<sizeof(loss)/sizeof(loss[0]);i++)
                {
                    star->derivative[abs(loss[i])] *= f;
                }
                for(i=0;i<sizeof(gain)/sizeof(gain[0]);i++)
                {
                    star->derivative[abs(gain[i])] *= f;
                }

                Append_logstring(LOG_DERIVATIVE,
                                 "0DERIV warning: modulate mass derivative of star %d by %g so it doesn't lose >%5.2f%% in one step, cannot reject dt=%gy want dt=%gy",
                                 star->starnum,
                                 f,
                                 fmax * 100.0,
                                 stardata->model.dt,
                                 stardata->model.dt * stardata->model.dt_zoomfac
                    );

                Dprint("reduced derivatives so mass is now %g\n",star->mass + stardata->model.dt * Mdot_net(star));
            }
        }

        /*
         * Check stellar angular momentum is > 0.0
         */
        const double jdot_net = Jdot_net(star);
        const double newJ = star->angular_momentum + jdot_net * stardata->model.dt;
        Dprint("star %d angmom was %g will be %g, jdot = (loss=) %g + (gain=) %g = %g\n",
               star->starnum,
               star->angular_momentum,
               newJ,
               Jdot_loss(star),
               Jdot_gain(star),
               jdot_net
            );

        if(newJ<-TINY)
        {
            /*
             * Reduce angular momentum loss derivatives by f so the stellar angular
             * momentum is >= ~0.0 (the 1+TINY is the keep it just above zero)
             */
            triggered = TRUE;
            Dprint("MODULATE J derivs model %d time %g dt %g\n",
                   stardata->model.model_number,
                   stardata->model.time,
                   stardata->model.dt
                );
            static const int loss[] = { Jdot_loss_derivatives };
            static const int gain[] = { Jdot_gain_derivatives };
            const double f = fabs(star->angular_momentum / (Jdot_net(star)*stardata->model.dt)) / (1.0 + TINY);
            unsigned int i;
            for(i=0;i<sizeof(loss)/sizeof(loss[0]);i++)
            {
                star->derivative[abs(loss[i])] *= f;
            }
            for(i=0;i<sizeof(gain)/sizeof(gain[0]);i++)
            {
                star->derivative[abs(gain[i])] *= f;
            }
            Append_logstring(LOG_DERIVATIVE,
                             "0DERIV warning: modulate dJ/dt derivatives of star %d by %g, cannot reject dt=%gy",
                             star->starnum,
                             f,
                             stardata->model.dt
                );
        }
    }

    return triggered;
}
