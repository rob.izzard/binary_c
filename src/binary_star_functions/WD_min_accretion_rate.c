#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "donor_types.h"

/*
 * Minimum stable accretion rate on a white dwarf
 *
 * Returns the rate in Msun/year

 *
 * Note: these are rates limited by nuclear burning only.
 * Other limits (e.g. thermal, Eddington, etc.) are imposed elsehwere.
 */

double WD_min_accretion_rate(struct stardata_t * Restrict const stardata,
                             struct star_t * const donor,
                             struct star_t * const accretor)
{
    /*
     * Choose rate based on donor stellar type
     */
    double rate =
        HYDROGEN_DONOR ? stardata->preferences->WD_accretion_rate_novae_upper_limit_hydrogen_donor :
        HELIUM_DONOR ? stardata->preferences->WD_accretion_rate_novae_upper_limit_helium_donor :
        stardata->preferences->WD_accretion_rate_novae_upper_limit_other_donor;

    if(rate < -TINY)
    {
        /*
         * Use rate algorithm
         */
        const int algorithm = Map_float_algorithm(rate);
#undef X
#define X(X,Y,STRING) STRING,
        static const char * strings[] = { RLOF_DONOR_RATE_ALGORITHMS_LIST };
#undef X
        Dprint("mapped rate %g to algorithm %d \"%s\"\n",
               rate,
               algorithm,
               strings[-algorithm]);

        switch(algorithm)
        {

        case DONOR_RATE_ALGORITHM_BSE:
            /*
             * BSE (Hurley et al. 2002) algorithm
             */
            if(HYDROGEN_DONOR)
            {
                rate = 1.03e-7;
            }
            else
            {
                /*
                 * Rate may be limited elsewhere
                 */
                rate = VERY_SMALL_MASS_TRANSFER_RATE;
            }
            break;

        case DONOR_RATE_ALGORITHM_CLAEYS2014:
            /*
             * Claeys et al. (2014) algorithm
             */
            if(HYDROGEN_DONOR)
            {
                Abundance X;
#ifdef NUCSYN
                X = donor->Xenv[XH1];
#else
                X = 0.7;
#endif
                rate =
                    (1.0/8.0) *
                    5.3e-7 *
                    (1.7-X)/Max(1e-10,X) *
                    Max(0.0, accretor->mass - 0.4);
            }
            else if(HELIUM_DONOR)
            {
                /*
                 * 3.98107e-8, compiler will pre-compute this
                 */
                rate = exp10(-7.4);
            }
            else
            {
                /* what to do for other donors? */
                rate = 0.0;
            }
            break;

#ifdef KEMP_NOVAE
        case DONOR_RATE_ALGORITHM_K2014_P2014:
            /*
             * Kato2014 H, Piersanti 2014 He (using 'mild-flash' curve).
             */
        {
            const double M_WD = accretor->mass;
            if(HYDROGEN_DONOR)
            {
                /* steady_burn/strongflash bounary */
                rate = exp10(0.8947 * Pow3(M_WD) - 3.929 * Pow2(M_WD) + 6.2237 * M_WD -9.9133);
            }
            else if(HELIUM_DONOR)
            {
                /* mildflash/strongflash boundary */
                rate = pow(10,-8.233+2.022*M_WD);
            }
            else
            {
                /*
                 * burning other stuff is hard, probably not nuclear-limited.
                 */
                rate = VERY_SMALL_MASS_TRANSFER_RATE;
            }
        }
            break;

        case DONOR_RATE_ALGORITHM_WANGWU:
            /*
             * Hydrogen: Wang (2018)
             * Helium: Wu (2017)
             */
        {
            rate = VERY_LARGE_MASS_TRANSFER_RATE;
            const double M_WD = accretor->mass;
            if(HYDROGEN_DONOR)
            {
                if(M_WD>=0.5)
                {
                    /* interpolate */
                    rate = 2.93*1e-7 * (Pow3(-M_WD) + 4.41*Pow2(M_WD) - 3.38*M_WD + 0.84);
                }
                else if(M_WD<0.5)
                {
                    /* extrapolate */
                    rate = exp10(2.5 * M_WD -8.677622194875916);
                }
            }
            else if(HELIUM_DONOR)
            {
                if(M_WD>=0.6)
                {
                    /* interpolate */
                    rate = 1.46*1e-6 * (Pow3(-M_WD) + 3.45*Pow2(M_WD)- 2.60*M_WD + 0.85);
                }
                else if(HELIUM_DONOR && M_WD<0.6)
                {
                    /* extrapolate */
                    rate = exp10(0.6666654157433166 * M_WD - 6.73595931104315);
                }
            }
        }
        break;

#endif // KEMP_NOVAE

        default:
            rate = 0.0; // avoid compiler warnings
            Exit_binary_c(BINARY_C_SETUP_UNKNOWN_ARGUMENT,
                          "Unknown accretion rate novae upper limit method %g (mapped to algorithm %d) \n",
                          stardata->preferences->WD_accretion_rate_novae_upper_limit_hydrogen_donor,
                          algorithm);


        }
    }

    rate = Max(VERY_SMALL_MASS_TRANSFER_RATE,rate);
    return rate;
}
