#include "../binary_c.h"
No_empty_translation_unit_warning;


void set_ndonor_and_naccretor(struct stardata_t * Restrict const stardata)
{
    struct model_t * model = &(stardata->model);
    if((model->intpol)==0)
    {
        model->ndonor = More_or_equal(stardata->star[1].roche_radius*stardata->star[0].radius,
                                      stardata->star[1].radius*stardata->star[0].roche_radius)
            ? 0 : 1;
        model->naccretor = Other_star(model->ndonor);
    }
}
