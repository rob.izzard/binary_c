#include "../binary_c.h"
No_empty_translation_unit_warning;


double accretion_radius(struct stardata_t * const stardata,
                        struct star_t * const star)
{
    /*
     * Calculate and return the effective accretion radius
     */
    double r_acc;
    if(RLOF_test_for_accretion_disc(stardata) == TRUE)
    {
        /*
         * Accretion is through a circumstellar disc.
         *
         * Alter spin of the secondary by assuming that material falls onto
         * the star from the inner edge of a Keplerian accretion disk and that the
         * system is in a steady state. Adjust the primary spin accordingly so that
         * tidal interaction between primary and orbit will conserve total angular
         * momentum.
         */
        r_acc = star->effective_radius;
        Dprint("disc accretion : accretor radius %g, effective radius %g\n",
               star->radius,
               star->effective_radius);
    }
    else
    {
        /*
         * No accretion disk : direct impact accretion.
         *
         * Calculate the angular momentum of the transferred material by
         * using the radius of the disk (see Ulrich & Burger) that would
         * have formed if allowed.
         */
        r_acc = 1.7*star->rmin;
        Dprint("no accretion disk : accretor's rmin=%g vs radius=%g\n",
               star->rmin,
               star->radius);
    }
    return r_acc;
}
