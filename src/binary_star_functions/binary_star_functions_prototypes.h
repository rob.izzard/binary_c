/*  Binary_c */

/*  Header file for function prototypes */

/* filename: binary_star_functions_prototypes.h */

/*******************************************/

#ifndef BINARY_STAR_FUNCTIONS_H
#define BINARY_STAR_FUNCTIONS_H

#include "../binary_c_code_options.h"

#include <sys/types.h>
#include "../binary_c_parameters.h"
#include "../binary_c_macros.h"
#include "../binary_c_structures.h"

int degenerate_core_merger(struct stardata_t * const stardata,
                           struct star_t * const star1,
                           struct star_t * const star2,
                           const Stellar_type stellar_type1,
                           const Stellar_type stellar_type2,
                           Stellar_type * const stellar_type3,
                           const double m1,
                           const double m2,
                           double * const m3,
                           double * const ebinde);

void mix_stars(struct stardata_t * const stardata,
               const Boolean eject,
               const Yield_source source
    );

#ifdef BSE
void mix_stars_BSE(struct stardata_t * const Restrict stardata,
                   const Boolean eject);
#endif // BSE


double Constant_function rL1(const double q
#ifdef RLOF_RADIATION_CORRECTION
                            ,const double f
#endif
    );
double Constant_function rL2(const double q
#ifdef RLOF_RADIATION_CORRECTION
                            ,const double f
#endif
    );
double Constant_function rL3(const double q
#ifdef RLOF_RADIATION_CORRECTION
                            ,const double f
#endif
    );

int contact_system(struct stardata_t * Restrict const stardata,
                   const Boolean event_call,
                   const int why);

void adjust_radius_derivative(struct stardata_t  * Restrict const stardata);

Boolean check_for_collision_at_periastron(struct stardata_t * Restrict const stardata);

void calc_wind_loss_and_gain_rates(struct stardata_t * Restrict const stardata,
                                   const Boolean RLOF_bool);

void set_ndonor_and_naccretor(struct stardata_t * Restrict const stardata);

void angular_momentum_loss_mechanisms_for_close_systems(struct stardata_t * Restrict const stardata);


void determine_mass_ratios(struct stardata_t * Restrict const stardata);

void determine_roche_lobe_radii(struct stardata_t * Restrict const stardata,
                                struct orbit_t * const orbit);

void determine_roche_lobe_radius(struct stardata_t * const stardata,
                                 struct orbit_t * const orbit,
                                 struct star_t * const star);

void detached_limit_mass_loss_and_timestep(struct stardata_t * Restrict const stardata);

void save_detached_stellar_types(struct stardata_t * Restrict const stardata);

void adjust_radius_derivative(struct stardata_t * Restrict const stardata);

unsigned int test_for_roche_lobe_overflow(struct stardata_t * Restrict const stardata,
                                          const Boolean test_previous);

void make_roche_lobe_radii_huge(struct stardata_t * Restrict const stardata);

void set_nuclear_timescale_and_slowdown_factor(struct stardata_t * Restrict const stardata);

void calculate_spins(struct stardata_t * Restrict const stardata);

void set_kelvin_helmholtz_times(struct stardata_t * Restrict const stardata);

unsigned int test_if_primary_still_fills_roche_lobe(struct stardata_t * Restrict const stardata);

void set_effective_radii(struct stardata_t * Restrict const stardata);

void detached_wind_loss_and_angmom_derivatives(struct stardata_t * Restrict const stardata);

void limit_timestep_to_2percent_ang_mom_change(struct stardata_t * Restrict const stardata);

#ifdef BSE
void rejuvenate_MS_secondary_and_age_primary(struct stardata_t * Restrict const stardata);
#endif

void set_wind_accretion_luminosities(struct stardata_t * Restrict const stardata);

void angular_momentum_and_eccentricity_derivatives(struct stardata_t * Restrict const stardata,
                                                   const Boolean RLOF_boolean);

Constant_function double radius_closest_approach(const double sep,
                                                 const double q);

Boolean prevent_overspin(struct stardata_t * const stardata,
                         struct star_t * const star);

#ifdef CIRCUMBINARY_DISK_DERMINE
void circumbinary_disk(struct stardata_t * Restrict const stardata);
double circumbinary_disk_massloss_timescale(struct stardata_t  * Restrict const stardata);
#endif


double radius_stripped (const double phase_start_mass,
            const double tauMS,
            const double mass,
            const double rwas,
            struct star_t * const star,
            struct stardata_t * const stardata);

#ifdef WRLOF_MASS_TRANSFER
double WRLOF_rate(struct stardata_t * const stardata,
                  struct star_t * const donor,
                  struct star_t * const accretor);
#endif
double Pure_function gravitational_radiation_jdot(struct stardata_t * Restrict const stardata);
double Pure_function gravitational_radiation_edot(struct stardata_t * Restrict const stardata);
double Pure_function gravitational_radiation_timescale(const double separation,
                                                       const double M1,
                                                       const double M2);

void show_instant_RLOF(struct stardata_t * Restrict const stardata);

void stellar_wind_angmom(struct stardata_t * Restrict const stardata,
                         const Boolean RLOF_boolean,
                         const Star_number k);

double Pure_function wind_decc_dt(struct stardata_t * Restrict const stardata);

Boolean apply_orbital_angular_momentum_and_eccentricity_derivatives(struct stardata_t * Restrict const stardata,
                                                                    const Boolean RLOF_boolean);

int detached_apply_orbital_angular_momentum_and_eccentricity_derivatives(struct stardata_t * Restrict const stardata);

void detached_timestep_limits(struct stardata_t * Restrict const stardata);

int interpolate_R_to_RL(struct stardata_t * Restrict const stardata);
void handle_massless_remnants(struct stardata_t * Restrict const stardata Maybe_unused);

double Pure_function radial_velocity_K(struct stardata_t * Restrict const stardata,
                                       const double inclination,
                                       const Star_number n);

void lagrange_points(const double M1,
                     const double M2,
                     const double separation,
                     struct coordinate_t L[NUMBER_OF_LAGRANGE_POINTS],
                     double * Restrict const a1,
                     double * Restrict const a2
    );
void lagrange_points_from_stardata(struct stardata_t * Restrict const stardata,
                                   struct coordinate_t L[NUMBER_OF_LAGRANGE_POINTS],
                                   double * Restrict const a1,
                                   double * Restrict const a2);

Boolean apply_stellar_mass_and_angular_momentum_derivatives(struct stardata_t * stardata,
                                                            const Boolean RLOF);

double non_conservative_gamma(struct stardata_t * Restrict const stardata,
                              struct star_t * const donor,
                              struct star_t * const accretor

    );
void non_conservative_angular_momentum_loss(struct stardata_t * Restrict const stardata,
                                            const Boolean RLOF);
void compact_object_accretion_limits(struct stardata_t * const stardata,
                                     struct star_t * const accretor,
                                     struct star_t * const donor,
                                     double * Restrict const steady_burn_rate,
                                     double * Restrict const new_envelope_rate);

double WD_max_accretion_rate(struct stardata_t * Restrict const stardata,
                             struct star_t * const donor,
                             struct star_t * const accretor);

double WD_min_accretion_rate(struct stardata_t * Restrict const stardata,
                             struct star_t * const donor,
                             struct star_t * const accretor);


void limit_accretion_rates(struct stardata_t * Restrict const stardata);

int update_masses_angular_momenta_and_structure(struct stardata_t * Restrict const stardata,
                                                const Boolean RLOF);
double Pure_function eddington_limit_for_accretion(const double radius,
                                                   const double multiplier,
                                                   const Abundance X);
double Pure_function thermal_limit_for_accretion(struct star_t * Restrict accretor,
                                                 struct stardata_t * Restrict const stardata);

double Pure_function dynamical_limit_for_accretion(struct star_t * Restrict accretor,
                                                   struct stardata_t * Restrict const stardata);

double Pure_function fixed_limit_for_accretion(struct star_t * RESTRICT donor,
                                               struct stardata_t * RESTRICT const stardata);

double Pure_function white_dwarf_H_accretion_efficiency(struct stardata_t * const stardata,
                                                        struct star_t * Restrict const donor,
                                                        struct star_t * Restrict const accretor,
                                                        const double Mdot_tr);
double Pure_function white_dwarf_He_accretion_efficiency(struct stardata_t * Restrict const stardata,
                                                         struct star_t * Restrict const donor Maybe_unused,
                                                         struct star_t * Restrict const accretor,
                                                         const double transfer_rate);

double Pure_function binary_a_n(struct stardata_t * const stardata,
                                struct star_t * const star);
#ifdef BSE
void update_MS_lifetimes(struct stardata_t * Restrict const stardata);
#endif//BSE
double Peters_grav_wave_merger_time(const struct stardata_t * Restrict const stardata,
                                    const double m1,
                                    const double m2,
                                    const double a,
                                    const double ecc);


Boolean apply_other_stellar_derivatives(struct stardata_t * Restrict const stardata,
                                        const Boolean RLOF,
                                        const Boolean can_reject,
                                        const Boolean can_reject_and_shorten);

Event_handler_function contact_system_event_handler(void * eventp,
                                                    struct stardata_t * stardata,
                                                    void * data Maybe_unused);

double spiral_in_time(struct stardata_t * Restrict const stardata);

Boolean derivative_check_stellar_mass(struct stardata_t * const stardata,
                                      void * const data,
                                      const double previous_value Maybe_unused,
                                      const double value,
                                      const double derivative,
                                      const double dt,
                                      const Derivative nd,
                                      const Derivative_group derivative_group);

Pure_function
Boolean derivative_check_stellar_angmom(struct stardata_t * const stardata Maybe_unused,
                                        void * const data,
                                        const double previous_value Maybe_unused,
                                        const double value,
                                        const double derivative Maybe_unused,
                                        const double dt Maybe_unused,
                                        const Derivative nd Maybe_unused,
                                        const Derivative_group derivative_group Maybe_unused);

Pure_function
Boolean derivative_check_stellar_he_core_mass(struct stardata_t * const stardata Maybe_unused,
                                              void * const data,
                                              const double previous_value Maybe_unused,
                                              const double value,
                                              const double derivative Maybe_unused,
                                              const double dt Maybe_unused,
                                              const Derivative nd Maybe_unused,
                                              const Derivative_group derivative_group Maybe_unused);


Pure_function
Boolean derivative_check_stellar_CO_core_mass(struct stardata_t * const stardata Maybe_unused,
                                              void * const data Maybe_unused,
                                              const double previous_value Maybe_unused,
                                              const double value,
                                              const double derivative Maybe_unused,
                                              const double dt Maybe_unused,
                                              const Derivative nd Maybe_unused,
                                              const Derivative_group derivative_group Maybe_unused);


Pure_function
Boolean derivative_check_num_thermal_pulses(struct stardata_t * const stardata Maybe_unused,
                                            void * const data,
                                            const double previous_value Maybe_unused,
                                            const double value,
                                            const double derivative Maybe_unused,
                                            const double dt Maybe_unused,
                                            const Derivative nd Maybe_unused,
                                            const Derivative_group derivative_group Maybe_unused);

Pure_function
Boolean derivative_check_dm_novaH(struct stardata_t * const stardata Maybe_unused,
                                  void * const data,
                                  const double previous_value Maybe_unused,
                                  const double value,
                                  const double derivative Maybe_unused,
                                  const double dt Maybe_unused,
                                  const Derivative nd Maybe_unused,
                                  const Derivative_group derivative_group Maybe_unused);

#ifdef KEMP_NOVAE
Boolean derivative_check_dm_novaHe(struct stardata_t * const stardata Maybe_unused,
                                   void * const data Maybe_unused,
                                   const double previous_value Maybe_unused,
                                   const double value,
                                   const double derivative Maybe_unused,
                                   const double dt Maybe_unused,
                                   const Derivative nd Maybe_unused,
                                   const Derivative_group derivative_group Maybe_unused);
#endif // KEMP_NOVAE
Pure_function
Boolean derivative_check_num_novae(struct stardata_t * const stardata Maybe_unused,
                                   void * const data,
                                   const double previous_value Maybe_unused,
                                   const double value,
                                   const double derivative Maybe_unused,
                                   const double dt Maybe_unused,
                                   const Derivative nd Maybe_unused,
                                   const Derivative_group derivative_group Maybe_unused);
Boolean derivative_checks(struct stardata_t * const stardata);
Stellar_type merger_stellar_type(struct stardata_t * const stardata,
                                 struct star_t * const donor,
                                 struct star_t * const accretor);

double Pure_function Xray_luminosity(const struct star_t * Restrict const star);


double Pure_function system_gravitational_mass(const struct stardata_t * const stardata);

void calc_yields(struct stardata_t * Restrict const stardata,
                 struct star_t * const star,
                 const double dmlose, /* mass lost as wind/otherwise */
#ifdef NUCSYN
                 Abundance * const Xlose, /* abundance of this "wind" */
#endif
                 const double dmacc, /* mass gained by accretion */
#ifdef NUCSYN
                 Abundance * const Xacc, /* abundance of this accretion */
#endif //NUCSYN
                 const Star_number starnum,
                 const int final,
                 const Yield_source source
    );
void Hot_function update_abundances_and_yields(struct stardata_t * Restrict const stardata);

Yield_source id_wind_source(const struct star_t * const star,
                            const struct stardata_t * const stardata);

void merge_cores(struct stardata_t * const stardata,
                 const struct star_t * const a,
                 const struct star_t * const b,
                 struct star_t * const c);
double accretion_radius(struct stardata_t * const stardata,
                        struct star_t * const star);


double wind_orbital_angular_momentum_derivative(
    const struct stardata_t * Restrict const stardata,
    const struct orbit_t * const orbit,
    const double m[2],
    const double mdot_loss[2],
    const double mdot_gain[2],
    const double vwind[2]);

void artificial_angular_momentum_accretion_rates(struct stardata_t * const stardata);
void artificial_mass_accretion_rates(struct stardata_t * const stardata);
Boolean artificial_accretion(struct stardata_t * const stardata);
double Bondi_Hoyle_accretion_rate(struct stardata_t * const stardata,
                                  const double mass_accretor, // M_SUN
                                  const double vorb, // cm/s
                                  const double mdot_wind, // M_SUN / year
                                  const double vwind_donor, // cm/s
                                  const double separation, // R_SUN
                                  const double eccentricity);
void mean_HRD(const struct stardata_t * const stardata,
              double * const logTeff,
              double * const logg,
              double * const logL);

double merger_mass_loss_fraction(struct stardata_t * const stardata,
                                 const Stellar_type merged_stellar_type);

#ifdef KEMP_NOVAE

double Hedet_dMHe(struct star_t * const Restrict accretor);

Accretion_regime He_accretion_regime_WD(struct stardata_t * const Restrict stardata,
                                        struct star_t * const Restrict accretor);
Accretion_regime H_accretion_regime_WD(struct stardata_t * const Restrict stardata,
                                       struct star_t * const Restrict accretor);
#endif // KEMP_NOVAE

#ifdef SAVE_MASS_HISTORY
void save_mass_history(struct stardata_t * Restrict const stardata);
#endif // SAVE_MASS_HISTORY

Boolean Pure_function use_gravitational_wave_radiation(struct stardata_t * const stardata);

struct star_t * merge_stars(struct stardata_t * const Restrict stardata,
                            const Star_number merged_star_number,
                            const Boolean eject
#ifdef NUCSYN
                            ,
                            Abundance * X
#endif // NUCSYN
    );

void merge_into_star(struct stardata_t * const stardata,
                     struct star_t * const star_into,
                     const Boolean eject,
                     const Yield_source source);
void init_Radice2018(struct stardata_t * const stardata);
void stardata_to_star(struct star_t * const star);

#endif /*BINARY_STAR_FUNCTIONS_H */
