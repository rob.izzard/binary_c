#include "../binary_c.h"
No_empty_translation_unit_warning;

/*
 * Stellar mergers, e.g. in a contact system
 */
#ifdef BSE
static void contact_system_call_corerd(const Star_number i,
                                       struct stardata_t * Restrict const stardata);
#endif
static Boolean contact_system_comenv(const Star_number ndonor,
                                     const Star_number naccretor,
                                     struct stardata_t * Restrict const stardata);
#ifdef BSE
static inline void set_epoch(struct stardata_t * Restrict const stardata,
                             struct star_t * star);

#endif

/************************************************************/

#define X(REASON,STRING) #STRING,
static char * contact_reasons_strings[] = {
    CONTACT_REASONS_LIST
};
#undef X

int contact_system(struct stardata_t * Restrict const stardata,
                   const Boolean event_call,
                   const int why)
{
    /*
     * If event_call is TRUE then we're called as the post-contact
     * system event. Otherwise, we set up the event which will be
     * triggered at the end of this timestep.
     */
    stardata->common.RLOF_do_overshoot = FALSE;
    if(event_call == FALSE &&
       stardata->preferences->disable_events == FALSE)
    {
        /*
         * Set up the contact system event
         */
        Dprint("Contact system : add new contact event (intermediate? %d)\n",
               stardata->model.intermediate_step);
        int * data = Malloc(sizeof(int));
        *data = why;
        if(Add_new_event(stardata,
                         BINARY_C_EVENT_CONTACT_SYSTEM,
                         &contact_system_event_handler,
                         NULL,
                         data,
                         UNIQUE_EVENT) == BINARY_C_EVENT_DENIED)
        {
            Dprint("Event not allowed! (perhaps already set up?)");
            Safe_free(data);
        }
        return EVOLUTION_NO_CONTACT;
    }
    else
    {
        /*
         * Do the contact
         *
         * We should check that the system contains two stars
         * that are not massless remnants, and that the orbit is
         * such that the system has not previously merged.
         */
        int action;
        Dprint("Check for contact : sgl = %s System_is_single = %s (stellar types %d %d, sep %g)\n",
               Yesno(stardata->model.sgl),
               Yesno(System_is_single),
               stardata->star[0].stellar_type,
               stardata->star[1].stellar_type,
               stardata->common.orbit.separation);
        if(NEITHER_STAR_MASSLESS &&
           More_or_equal(stardata->common.orbit.separation, 0.0))
        {
            RLOF_stars;

            /*
             * Contact system
             */
            Dprint("in contact_system()\n");
            Append_logstring(LOG_CONTACT,"0Contact reached R/RL = %g %g [at periastron %g %g], st %d %d, %s",
                             donor->roche_radius > 0.0 ? (donor->radius/donor->roche_radius) : 0.0,
                             accretor->roche_radius > 0.0 ? (accretor->radius/accretor->roche_radius) : 0.0,
                             donor->roche_radius > 0.0 ? (donor->radius/((1.0-stardata->common.orbit.eccentricity)*donor->roche_radius)) : 0.0,
                             accretor->roche_radius > 0.0 ? (accretor->radius/((1.0-stardata->common.orbit.eccentricity)*accretor->roche_radius)) : 0.0,
                             accretor->stellar_type,
                             donor->stellar_type,
                             contact_reasons_strings[why]);
            stardata->model.coalesce = TRUE;

            /*
             * If either star is giant-like this will be common-envelope evolution,
             * but if the system is marked as single we should avoid
             * common-envelope and just merge.
             *
             * The stars either go through common envelope evolution,
             * triggered after this call, or they are true contact
             * systems which are merged here.
             */
            Dprint("EVRLOF Comenv or mix?\n");
            if(
                System_is_single
                ||
                (
                    contact_system_comenv(ndonor,naccretor,stardata)==FALSE
                    &&
                    contact_system_comenv(naccretor,ndonor,stardata)==FALSE
                    )
                )
            {
                Dprint("EVRLOF MIX\n");
                /*
                 * Choose the source type.
                 *
                 * Special case: SOURCE_NS is for a NSNS or NSBH merger.
                 *
                 * Otherwise SOURCE_CONTACT
                 */
                const int source =
                    (
                        (
                            POST_SN_OBJECT(stardata->star[0].stellar_type) &&
                            stardata->star[1].stellar_type == NS
                            )
                         ||
                        (
                            POST_SN_OBJECT(stardata->star[1].stellar_type) &&
                            stardata->star[0].stellar_type == NS
                            )
                        ) ?
                    SOURCE_NS :
                    SOURCE_CONTACT;
                mix_stars(stardata,
                          TRUE,
                          source);
                stardata->common.orbit.separation = 0.0;
            }
            else
            {
                Dprint("EVRLOF COMENV\n");
            }

#ifdef BSE
            Foreach_star(star)
            {
                set_epoch(stardata,star);
                Dprint("set epoch k=%d -> %g\n",star->starnum,star->epoch);
            }
#endif//BSE

            if(stardata->model.coalesce==FALSE)
            {
                Dprint("Contact system\n");
                if(stardata->common.orbit.eccentricity>1.0)
                {
#ifdef BSE
                    contact_system_call_corerd(0,stardata);
                    contact_system_call_corerd(1,stardata);
#endif//BSE
                    action = EVOLUTION_SYSTEM_IS_BROKEN_APART;
                }
                else
                {
                    stardata->model.dtm = 0.0;

                    /*
                     * Reset orbital parameters as separation may have changed.
                     */
                    stardata->common.orbit.period = (stardata->common.orbit.separation/AU_IN_SOLAR_RADII)*
                        sqrt(stardata->common.orbit.separation/(AU_IN_SOLAR_RADII*Total_mass));
                    stardata->common.orbit.angular_frequency = TWOPI/stardata->common.orbit.period;

                    action = EVOLUTION_END_ROCHE;
                }
            }
            else
            {
                action = BINARY_C_NORMAL_EXIT;
            }

            /*
             * Determine if the system is single: it might
             * not be if common-envelope left the cores
             * detached
             */
            stardata->model.sgl = System_is_single;
            Dprint("Contact %d %g -> sgl ? %s [%d %d, sep %g] binary? %s\n",
                   stardata->model.model_number,
                   stardata->model.time,
                   Yesno(stardata->model.sgl),
                   stardata->star[0].stellar_type,
                   stardata->star[1].stellar_type,
                   stardata->common.orbit.separation,
                   Yesno(System_is_binary)
                );
        }
        else
        {
            action = BINARY_C_NORMAL_EXIT;
        }
        return action;
    }

}


static inline void set_epoch(struct stardata_t * Restrict const stardata,
                             struct star_t * star)
{
    Dprint_no_newline(
        "Set epoch of star %d to t=%g - age=%g -> %g : was %g -> now ",
        star->starnum,
        stardata->model.time,
        star->age,
        stardata->model.time - star->age,
        star->epoch);
    star->epoch = stardata->model.time - star->age;
    Dprint("%g\n",star->epoch);
}


#ifdef BSE
static void contact_system_call_corerd(const Star_number i,
                                       struct stardata_t * Restrict const stardata)
{
    double rc;
    struct star_t * star = & stardata->star[i];
    if(star->stellar_type >= NEUTRON_STAR)
    {
        rc = core_radius(stardata,
                         star,
                         star->stellar_type,
                         star->core_stellar_type,
                         star->mass,
                         star->mass,
                         star->phase_start_core_mass,
                         stardata->common.metallicity_parameters[i]);
        star->omega = star->angular_momentum/(CORE_MOMENT_OF_INERTIA_FACTOR*rc*rc*star->mass);
    }
}
#endif//BSE

static Boolean contact_system_comenv(const Star_number ndonor,
                                     const Star_number naccretor,
                                     struct stardata_t * Restrict const stardata)

{
    RLOF_stars_structs;
    const double menv = donor->mass - donor->core_mass[CORE_He];

    /*
     * Check if either star is a giant, hence we have a common
     * envelope around the other(s), and that the envelope is massive
     * enough.
     *
     * Returns TRUE if common-envelope evolution happens,
     * FALSE otherwise.
     */
    Dprint("Contact->comenv : ndonor %d, naccretor %d, donor %p, accretor %p, giant? %d, menv = %g vs %g -> %d\n",
           ndonor,
           naccretor,
           (void*)donor,
           (void*)accretor,
           GIANT_LIKE_STAR(donor->stellar_type),
           menv,
           stardata->preferences->minimum_donor_menv_for_comenv,
           menv > stardata->preferences->minimum_donor_menv_for_comenv);

    if(GIANT_LIKE_STAR(donor->stellar_type)
#ifdef POST_CEE_WIND_ONLY
       &&(menv > MIN_MENV_FOR_CEE)
#endif
       && (menv > stardata->preferences->minimum_donor_menv_for_comenv)
        )
    {
        Dprint("Call comenv from contact_system for giant-like star\n");
        common_envelope_evolution(
            donor,
            accretor,
            stardata
            );
#ifdef BSE
        update_MS_lifetimes(stardata);
#endif//BSE
        Dprint("post comenv ndonor=%d m1=%g m01=%g\n",
               ndonor,
               stardata->star[ndonor].mass,
               stardata->star[ndonor].phase_start_mass);

        stardata->model.com = TRUE;
        return TRUE;
    }
    else
    {
        return FALSE;
    }
}
