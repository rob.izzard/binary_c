#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef XRAY_LUMINOSITY
double Pure_function Xray_luminosity(const struct star_t * Restrict const star)
{
    /*
     * Crude estimate of X-ray luminosity:
     * assume material falls from infinity, also only X-rays
     * if a WD, NS or BH
     */
    return (MASSIVE_REMNANT(star->stellar_type)) ?
        (GRAVITATIONAL_CONSTANT*M_SUN*M_SUN/YEAR_LENGTH_IN_SECONDS)*
        (star->mass*star->derivative[DERIVATIVE_STELLAR_MASS_WIND_GAIN])/(2.0*star->radius*R_SUN)/L_SUN
        : 0.0;
}
#endif
