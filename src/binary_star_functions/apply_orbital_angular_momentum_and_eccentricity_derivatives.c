#include "../binary_c.h"
No_empty_translation_unit_warning;

Boolean apply_orbital_angular_momentum_and_eccentricity_derivatives(struct stardata_t * Restrict const stardata,
                                                                    const Boolean RLOF_boolean)
{
    /*
     * Apply orbital angular momentum and eccentricity derivatives,
     * as well as update circumbinary discs.
     *
     * Returns:
     * TRUE on success, FALSE if there is an error
     *
     * Note: all derivatives are applied, regardless
     * of whether they are checked to be correct or
     * not. It's then up to the caller to decide whether
     * to reject the timestep.
     */

    /*
     * Make a list of checkfuncs
     */
#undef X
#define X(CODE,STRING,VAR,FUNC,GROUP,MINTCLEAR) FUNC,
    static Boolean (*derivative_checkfunc[])
        (struct stardata_t * const,
         void * const data,
         const double,
         const double,
         const double,
         const Derivative,
         const Derivative_group)
        Maybe_unused =
        { SYSTEM_DERIVATIVES_LIST };
#undef X

    Boolean retval;
    struct common_t *common = &stardata->common;
    const double awas = common->orbit.separation;
    double dt = stardata->model.dt;
#ifdef REVERSE_TIME
    if(stardata->preferences->reverse_time == TRUE)
    {
        dt *= -1.0;
    }
#endif//REVERSE_TIME

    /*
     * Eccentricity
     */
    Dprint("Apply de/dt=%g\n",stardata->model.derivative[DERIVATIVE_ORBIT_ECCENTRICITY]);
    double wanted;
    apply_derivative(stardata,
                     &common->orbit.eccentricity,
                     &wanted,
                     stardata->model.derivative,
                     dt,
                     DERIVATIVE_ORBIT_ECCENTRICITY,
                     DERIVATIVES_GROUP_SYSTEM,
                     NULL,
                     NULL);
    common->orbit.eccentricity = Max(common->orbit.eccentricity,0.0);

    /* close to zero, set the eccentricity to zero */
    if(Abs_less_than(common->orbit.eccentricity,TINY_ECCENTRICITY))
    {
        common->orbit.eccentricity = 0.0;
    }
    Nancheck(common->orbit.eccentricity);

    /* unbound system during RLOF */
    if(RLOF_boolean &&
       More_or_equal(common->orbit.eccentricity,1.0))
    {
        Dprint("End RLOF because e = %g > 1",common->orbit.eccentricity);
        retval = FALSE;
    }
    else
    {
        if(stardata->model.sgl==FALSE)
        {
            /*
             * Angular momentum
             */
            Dprint("pre-apply dJorb/dt : dJorb/dt = %g\n",
                   stardata->model.derivative[DERIVATIVE_ORBIT_ANGMOM]);

            apply_derivative(stardata,
                             &stardata->common.orbit.angular_momentum,
                             &wanted,
                             stardata->model.derivative,
                             dt,
                             DERIVATIVE_ORBIT_ANGMOM,
                             DERIVATIVES_GROUP_SYSTEM,
                             NULL,
                             NULL);

            Dprint("post-apply dJorb/dt : dJorb/dt = %g\n",
                   stardata->model.derivative[DERIVATIVE_ORBIT_ANGMOM]);

            /*
             * Sometimes the angular momentum is slightly less
             * than zero. This is often alleviated by setting
             * a smaller timestep, however the result is the same:
             * a contact system which is picked up after application
             * of the derivative.
             */
            Clamp(common->orbit.angular_momentum,
                  MINIMUM_ORBITAL_ANGMOM,
                  MAXIMUM_ORBITAL_ANGMOM);

            Nancheck(common->orbit.angular_momentum);
            Dprint("pre-update orbital variables: dJorb/dt = %g\n",
                   stardata->model.derivative[DERIVATIVE_ORBIT_ANGMOM]);
            update_orbital_variables(stardata,
                                     &common->orbit,
                                     &stardata->star[0],
                                     &stardata->star[1]);

            Dprint("pre-update orbital variables: dJorb/dt = %g\n",
                   stardata->model.derivative[DERIVATIVE_ORBIT_ANGMOM]);
        }
        else
        {
            /* single stars : eccentricity is "-1.0" */
            common->orbit.angular_momentum = 0.0;
            common->orbit.eccentricity = -1.0;
        }

#ifdef ORBITING_OBJECTS
        apply_orbiting_object_derivatives(stardata,dt);
#endif // ORBITING_OBJECTS

        /*
         * Store da/dt
         */
        stardata->model.derivative[DERIVATIVE_ORBIT_SEMI_MAJOR_AXIS] =
            (stardata->model.dt > TINY) ?
            ((common->orbit.separation - awas)/stardata->model.dt) :
            0.0;

        /*
         * Test variable : d/dt = 1, so this should just be the age
         */
        stardata->model.derivative[DERIVATIVE_SYSTEM_TEST] = 1.0;
        apply_derivative(stardata,
                         &common->test,
                         &wanted,
                         stardata->model.derivative,
                         dt,
                         DERIVATIVE_SYSTEM_TEST,
                         DERIVATIVES_GROUP_SYSTEM,
                         NULL,
                         NULL);

#ifdef DISCS
        /*
         * Evolve discs here
         */
#endif

        retval = TRUE;
    }
    return retval;
}
