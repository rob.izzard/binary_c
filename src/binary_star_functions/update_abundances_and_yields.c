#include "../binary_c.h"
No_empty_translation_unit_warning;


/*
 * Subroutine to update the abundances and yields of each star
 * due to mass mass transfer, gain and loss.
 *
 * Note: This function is not for MINT, see MINT_nucsyn for that.
 */

#include "update_abundances_and_yields.h"

//#undef MDEBUG
//#define MDEBUG(...) if(k==1){printf("star%d: ",k);printf(__VA_ARGS__);}
#undef MDEBUG
#define MDEBUG(...) Dprint(__VA_ARGS__);

void Hot_function update_abundances_and_yields(struct stardata_t * Restrict const stardata)
{
    /*
     * Abundance arrays are
     *
     * Xacc,Xenv [k], k=0,1
     * The material already in star k's accretion layer and the
     * material in star k's envelope.
     *
     * nXacc, nXenv [k], k=0,1 The abundances in the accretion layer and
     * envelope after accretion has taken place. At the end of the routine Xacc
     * and Xenv will be set to these values but NOT BEFORE because then things
     * will be all wrong!
     */
    Dprint("update abundances and yields at time %g, model %d\n",
           stardata->model.time,
           stardata->model.model_number);
    Star_number k;
    /* timestep in years */
    const double dt = stardata->model.dtm * 1e6;

#ifdef NUCSYN
    Boolean allocated_Xdonor;
    Boolean allocated_Xaccretor;
    Boolean allocated_XRLOF_gain;
    Boolean allocated_Xwind_loss;
    Boolean allocated_XRLOF_and_decretion_loss;

    /* accretion layer thicknesses */
    const double dmacc[NUMBER_OF_STARS] Maybe_unused = {
        stardata->star[0].dmacc,
        stardata->star[1].dmacc
    };
    double ndmacc[NUMBER_OF_STARS] = {
        0.0,
        0.0
    };

    /* Abundance arrays for pre-mix values */
    const Abundance * const Xacc[NUMBER_OF_STARS] = {
        stardata->star[0].Xacc,
        stardata->star[1].Xacc
    };
    const Abundance *Xenv[NUMBER_OF_STARS] = {
        stardata->star[0].Xenv,
        stardata->star[1].Xenv
    };

    /*
     * Use pointers in tmpstore rather than
     * local pointers when we can, this
     * saves the memory being allocated and freed repeatedly.
     */
#define Xwind_gain stardata->tmpstore->Xwind_gain
#ifdef NUCSYN_NOVAE
#define Xnovae stardata->tmpstore->Xnovae
#endif //NUCSYN_NOVAE
#define nXacc stardata->tmpstore->nXacc
#define nXenv stardata->tmpstore->nXenv
#include "memory/memory_alignment_checks.h"

    if(unlikely(nXacc[0] == NULL))
    {
        Xwind_gain = New_isotope_array;
#ifdef NUCSYN_NOVAE
        Xnovae = New_isotope_array;
#endif // NUCSYN_NOVAE
        Number_of_stars_Starloop(k)
        {
            nXacc[k] = New_isotope_array;
            nXenv[k] = New_isotope_array;
        }
    }

    /* pointers that may be allocated */
    Abundance *Xdonor = NULL;
    Abundance *Xaccretor = NULL;
    Abundance *Xwind_loss = NULL;
    Abundance *XRLOF_gain = NULL;
    Abundance *XRLOF_and_decretion_loss = NULL;
#endif // NUCSYN

    Evolving_Starloop(k)
    {
        SETstar(k);
#ifdef NUCSYN
        /* Set initial accretion layer thickness */
        ndmacc[k] = star->dmacc;

        /* set the abundance arrays that may be altered */
        Copy_abundances(Xacc[k], nXacc[k]);
        Copy_abundances(Xenv[k], nXenv[k]);

        MDEBUG("MIXING star %d type %d : dmacc=%g : env=%g : Dm(DERIVATIVE_STELLAR_MASS_WIND_LOSS)=%g (derivative %g dt %g) : Dm(DERIVATIVE_STELLAR_MASS_WIND_GAIN)=%g (Env H1 %g C12 %g 1-Xtot %g; Acc H1 %g C12 %g 1-Xtot %g)\n",
               k,
               star->stellar_type,
               dmacc[k],
               Envelope_mass(k),
               Dm(DERIVATIVE_STELLAR_MASS_WIND_LOSS),
               star->derivative[DERIVATIVE_STELLAR_MASS_WIND_LOSS],
               dt,
               Dm(DERIVATIVE_STELLAR_MASS_WIND_GAIN),
               Xenv[k][XH1],
               Xenv[k][XC12],
               1.0 - nucsyn_totalX(Xenv[k]),
               Xacc[k][XH1],
               Xacc[k][XC12],
               1.0 - nucsyn_totalX(Xacc[k])
            );
#endif // NUCSYN
    }

#ifdef NUCSYN
    MIXDEBUG_START;
#endif // NUCSYN

    Evolving_Starloop(k)
    {
        SETstars(k);
        Dprint("Star %d\n",k);
        struct star_t * const prev_star =
            stardata->previous_stardata != NULL ?
            &stardata->previous_stardata->star[star->starnum] :
            star;

        /*
         * Wind loss/gain masses
         */
        const double dm_wind_loss = -Dm(DERIVATIVE_STELLAR_MASS_WIND_LOSS);
        const double dm_wind_gain = +Dm(DERIVATIVE_STELLAR_MASS_WIND_GAIN);

        /*
         * Stellar winds
         *
         * Choose f, the fraction of the accreting wind which gets mixed
         * with the star's wind prior to accretion.
         *
         * f=0 is like having all the mass from the companion's wind
         *      hit the star.
         *
         * f=1 is like having a shock so all the star's wind
         *     hit the star.
         *
         * f is also limited by the accretion rate
         */

#ifdef NUCSYN
        const double f =
            stardata->star[Other_star(k)].stellar_type == MASSLESS_REMNANT ? 0.0 :
            Min(nucsyn_choose_wind_mixing_factor(stardata,k),
                dm_wind_loss/(dm_wind_gain+VERY_TINY));

        /* mass from the companion */
        const double Dm_companion_wind = dm_wind_gain * (1.0 - f);

        /* mass from the star */
        const double Dm_star_wind = dm_wind_gain * f;

        Dprint("dm_wind loss=%g gain=%g f=%g Dm companion=%g star=%g\n",
               dm_wind_loss,
               dm_wind_gain,
               f,
               Dm_companion_wind,
               Dm_star_wind);

        struct star_t * const prev_companion =
            stardata->previous_stardata != NULL ?
            &stardata->previous_stardata->star[companion->starnum] :
            companion;

        /* hence the abundances */
        nucsyn_remove_dm_from_surface(
            stardata,
            prev_star,
            Dm_star_wind,
            &Xaccretor,
            &allocated_Xaccretor);

        nucsyn_remove_dm_from_surface(
            stardata,
            prev_companion,
            Dm_companion_wind,
            &Xdonor,
            &allocated_Xdonor);

        /*
         * Set Xwind_gain to be the mixture of
         * companion and star wind
         */
        nucsyn_dilute_shell_to(Dm_companion_wind,
                               Xdonor,
                               Dm_star_wind,
                               Xaccretor,
                               Xwind_gain);
#endif // NUCSYN

        /*
         * other mass gain:
         * dm_RLOF_gain is from the companion star
         * dm_cbdisc_gain is accretion from the innermost circumbinary disc
         */
        const double dm_RLOF_gain = Dm(DERIVATIVE_STELLAR_MASS_RLOF_GAIN);
#ifdef DISCS
        const double dm_cbdisc_gain = Dm(DERIVATIVE_STELLAR_MASS_CBDISC_GAIN);

        /* CBdisc loss : only do this for star 0 */
        const double dm_cbdisc_loss = k==1 ? 0.0 :
            -stardata->model.derivative[DERIVATIVE_SYSTEM_CBDISC_MASS_LOSS];
#endif // DISCS

        /*
         * other mass loss:
         * dm_RLOF_loss, dm_excretion_loss are from this star
         * dm_disc_loss is from the circumstellar disc (not yet implemented)
         * dm_nonconservative_loss is from the other star
         */
        const double dm_RLOF_loss = -Dm(DERIVATIVE_STELLAR_MASS_RLOF_LOSS);
        const double dm_excretion_loss = -Dm(DERIVATIVE_STELLAR_MASS_DECRETION_DISC);
        const double Maybe_unused dm_disc_loss = -Dm(DERIVATIVE_STELLAR_MASS_DISC_LOSS);
        const double dm_nonconservative_loss = -Dm(DERIVATIVE_STELLAR_MASS_NONCONSERVATIVE_LOSS);

        Dprint("Gain RLOF %g, wind %g; Lose RLOF %g, wind %g; Decretion %g, nonconservative %g\n",
               dm_RLOF_gain,
               dm_wind_gain,
               dm_RLOF_loss,
               dm_wind_loss,
               dm_excretion_loss,
               dm_nonconservative_loss);

#ifdef NUCSYN
        /*
         * Wind accretion, abundance calculated above
         */
        nucsyn_add_mass_to_surface(k,
                                   dm_wind_gain,
                                   Xwind_gain,
                                   ndmacc,
                                   nXacc);

        /*
         * RLOF accretion and non-conservative loss:
         * take abundances from the companion
         */
        nucsyn_remove_dm_from_surface(
            stardata,
            prev_companion,
            dm_RLOF_gain + dm_nonconservative_loss,
            &XRLOF_gain,
            &allocated_XRLOF_gain);

        /* only the RLOF gain is accreted */
        nucsyn_add_mass_to_surface(k,
                                   dm_RLOF_gain,
                                   XRLOF_gain,
                                   ndmacc,
                                   nXacc);

#ifdef DISCS
        {
            /*
             * Get abundance from the first (assumed innermost)
             * circumbinary disc
             */
            if(stardata->common.ndiscs>=1 &&
               stardata->common.discs[0].type==DISC_CIRCUMBINARY &&
               Is_not_zero(dm_cbdisc_gain))
            {
                nucsyn_add_mass_to_surface(k,
                                           dm_cbdisc_gain,
                                           stardata->common.discs[0].X,
                                           ndmacc,
                                           nXacc);
            }
        }
#endif // DISCS

        /*
         * Wind loss
         */
        nucsyn_remove_mass_from_surface(
            stardata,
            prev_star,
            dm_wind_loss,
            &Xwind_loss,
            ndmacc,
            &allocated_Xwind_loss);
        Dprint("post-add wind loss\n");

        /*
         * RLOF and excretion disc loss
         */
        nucsyn_remove_mass_from_surface(
            stardata,
            prev_star,
            dm_RLOF_loss+dm_excretion_loss,
            &XRLOF_and_decretion_loss,
            ndmacc,
            &allocated_XRLOF_and_decretion_loss);
        Dprint("post-add RLOF loss\n");
#endif // NUCSYN

        /*
         * Yields : positive and negative
         *
         * Note: use Add_yield to calculate yields,
         *       this automatically handles whether
         *       NUCSYN is defined.
         */

        /* RLOF */
        Add_yield(dm_RLOF_loss, XRLOF_and_decretion_loss, // lost
                  dm_RLOF_gain, XRLOF_gain, // accreted
                  SOURCE_RLOF);

        Add_yield(dm_nonconservative_loss,XRLOF_gain, //lost
                  0.0,NULL,
                  SOURCE_RLOF);

        /* excretion disc */
        Add_yield(dm_excretion_loss, XRLOF_and_decretion_loss, // lost
                  0.0, NULL, // accreted
                  SOURCE_DECRETION);

        /* wind loss / accretion */
        Add_yield(dm_wind_loss,Xwind_loss, //lost
                  dm_wind_gain,Xwind_gain, //accreted
                  id_wind_source(prev_star,stardata));

#ifdef DISCS
        /* discs */
        Add_yield(dm_cbdisc_loss,stardata->common.discs[0].X,
                  dm_cbdisc_gain,stardata->common.discs[0].X,
                  SOURCE_CBDISC);
#endif // DISCS
        // to be implemented  : dm_disc_loss, SOURCE_DISC


        /*
         * Novae : handles accretion also by making sure
         * the nova abundances are set by the star that is losing
         * mass.
         *
         * Note: if dm_novae < 0 then we're dealing with
         * the white dwarf, otherwise the donor
         *
         * Really, we should call set_nova_abunds once!
         */
        const double dm_novae = Dm(DERIVATIVE_STELLAR_MASS_NOVA);

        if(Is_not_zero(dm_novae))
        {

#if defined NUCSYN &&                           \
    defined NUCSYN_NOVAE
            struct star_t * const white_dwarf_accretor =
                dm_novae < 0.0 ? star : companion;
            nucsyn_set_nova_abunds(stardata,
                                   white_dwarf_accretor,
                                   Xdonor,
                                   Xnovae);
#endif //NUCSYN && NUCSYN_NOVAE
            if(dm_novae < 0.0)
            {
                /*
                 * Nova loss from the white dwarf
                 */
#if defined NUCSYN &&                           \
    defined NUCSYN_NOVAE
                nucsyn_remove_mass_from_surface(
                    stardata,
                    white_dwarf_accretor,
                    -dm_novae,
                    NULL,
                    ndmacc,
                    NULL);
#endif // NUCSYN && NUCSYN_NOVAE

                Add_yield(-dm_novae,Xnovae,
                          0.0,NULL,
                          SOURCE_NOVAE);
            }
            else
            {
                /*
                 * Nova "reaccretion"
                 */
#if defined NUCSYN &&                           \
    defined NUCSYN_NOVAE
                nucsyn_add_mass_to_surface(k,
                                           dm_novae,
                                           Xnovae,
                                           ndmacc,
                                           nXacc);
#endif // NUCSYN && NUCSYN_NOVAE
                Add_yield(0.0,NULL,
                          dm_novae,Xnovae,
                          SOURCE_NOVAE);
            }
        }

#ifdef NUCSYN
        /*
         * Free possibly allocated memory
         */
        Free_if_allocated(wind_loss);
        Free_if_allocated(RLOF_gain);
        Free_if_allocated(RLOF_and_decretion_loss);
        Free_if_allocated(donor);
        Free_if_allocated(accretor);
#endif // NUCSYN
    }

#ifdef NUCSYN
    Evolving_Starloop(k)
    {
        SETstars(k);

        /*
         * Now do interior mixing
         */

#ifdef NUCSYN_STRIP_AND_MIX
        Boolean use_strip_and_mix = can_use_strip_and_mix(stardata,star);
        if(use_strip_and_mix==TRUE)
        {
            /*
             * In pre-CHeB stars, use a new "strip and mix" code
             * which requires that we put accreted material into
             * an accretion layer and deal with it in detail in
             * nucsyn_strip_and_mix
             */
        }
        else
        {
#endif // NUCSYN_STRIP_AND_MIX

            Dprint("accrete %g\n",ndmacc[k]);
            if(ndmacc[k] > 0.0)
            {
                /*
                 * Accreted material is in the new accretion layer.
                 *
                 * If the accreting star is convective, just mix accreted material
                 * into its envelope.
                 */
                if(CONVECTIVE_ENVELOPE(star->stellar_type))
                {
                    MDEBUG("MIXING accreting star is convective\n");
                    nucsyn_dilute_shell(Max(0.0,Envelope_mass(k) - ndmacc[k]),
                                        nXenv[k],
                                        ndmacc[k],
                                        nXacc[k]);
                    ndmacc[k]=0.0;

                    MDEBUG("MIXING accreted material mixed straight into the envelope (Xenv XC12 %g)\n",
                           nXenv[k][XC12]);
                }
                else
                {
                    /*******************************/
                    /* Accreting star is radiative */
                    /*******************************/

                    /* Now, does the new accretion layer sink or swim? */

                    /*
                     * Calculate the molecular weight of the new accretion layer
                     * and the "new" envelope (which is probably the same as the
                     * old envelope). We assume the material that has accreted
                     * is fully ionized.
                     */

                    const double mu_acc = nucsyn_molecular_weight(nXacc[k],
                                                                  stardata,
                                                                  1.0);
                    const double mu_env = nucsyn_molecular_weight(nXenv[k],
                                                                  stardata,
                                                                  1.0);
                    /*
                    printf("ACC H1=%g He4=%g C12=%g Fe56=%g : sum %g, mu %g\n",
                           nXacc[k][XH1],
                           nXacc[k][XHe4],
                           nXacc[k][XC12],
                           nXacc[k][XFe56],
                           nucsyn_totalX(stardata,nXacc[k]),
                           mu_acc);
                    printf("ENV H1=%g He4=%g C12=%g Fe56=%g : sum %g, mu %g\n",
                           nXenv[k][XH1],
                           nXenv[k][XHe4],
                           nXenv[k][XC12],
                           nXenv[k][XFe56],
                           nucsyn_totalX(stardata,nXenv[k]),
                           mu_env);
                    */
                    MDEBUG("MIXING Seeing if the accretion layer will sink or swim : mu_acc=%g c.f. mu_env=%g, DELTAmu=%g\n",
                           mu_acc,
                           mu_env,
                           mu_acc - mu_env);

                    /*
                     * Compare the molecular weight of the new accretion layer
                     * against the new envelope : if the new accretion layer is
                     * *heavier* it will sink and mix entirely with the envelope.
                     * If it is lighter it will just sit on the surface and hang
                     * about like the kids on the street corner with the baseball
                     * caps i.e. very annoying and might be best to burn it! For
                     * the case where mu_acc==mu_env assume the accreting material
                     * floats : if anything it'll be hotter (due to the impact)
                     * than the stellar envelope, so will float due to "normal"
                     * convection.
                     */

                    if(mu_acc - mu_env > -VERY_TINY)
                        /*
                         * NB use > -VERY_TINY so that if they're very similar we do
                         *  not have an accretion layer (this saves time)
                         */
                    {
                        MDEBUG("Mu_acc=%g mu_env=%g : call thermohaline mix of (acc: dm=%g XC12=%g) with (env: dm=%g XC12=%g)  ",
                               mu_acc,
                               mu_env,
                               ndmacc[k],
                               Xacc[k][XC12],
                               Envelope_mass(k),
                               nXenv[k][XC12]
                            );
                        nucsyn_thermohaline_mix(stardata,
                                                k,
                                                nXacc[k],
                                                ndmacc[k],
                                                Xenv[k],
                                                nXenv[k]);

                        ndmacc[k] = 0.0;

                        MDEBUG("\nafter mix C12 : acc %g env %g ",
                               Xacc[k][XC12],
                               nXenv[k][XC12]);

                        MDEBUG("MIXING        : Difference : %8e %8e %8e %8e %8e\nMIXING set ndmacc[%d] to %g\n",
                               nXenv[k][XH1]-Xenv[k][XH1],
                               nXenv[k][XHe4]-Xenv[k][XHe4],
                               nXenv[k][XC12]-Xenv[k][XC12],
                               nXenv[k][XN14]-Xenv[k][XN14],
                               nXenv[k][XO16]-Xenv[k][XO16],k,ndmacc[k]);
                        MDEBUG("MIX onto star %d (type %d, mu=%3g onto type %d, mu=%3g) : sink\n",
                               k,
                               companion->stellar_type,
                               mu_acc,
                               star->stellar_type,
                               mu_env);

                    }
                    else
                    {
                        /*
                         * Accretion layer floats : just leave it as it is
                         */
                        MDEBUG("MIXING Accretion layer floats on the radiative envelope\n MIXING sink or swim? SWIM!\nMIX onto star %d (type %d, mu=%3g onto type %d, mu=%3g) swim\n",
                               k,
                               companion->stellar_type,
                               mu_acc,
                               star->stellar_type,
                               mu_env);
                    }
                }
            }
#ifdef NUCSYN_STRIP_AND_MIX
        }
#endif//NUCSYN_STRIP_AND_MIX
    }
#endif // NUCSYN

/*
 * Update abundances for the next timestep
 */
#ifdef NUCSYN
    Evolving_Starloop(k)
    {
        MDEBUG("MIXING memcpy new abundances for next timestep + wind losses\n");

        MDEBUG("MIXING update envelope and accretion layer of star %d\n",k);
        SETstar(k);

        Copy_abundances(nXacc[k],star->Xacc);
        Copy_abundances(nXenv[k],star->Xenv);

        MDEBUG("New C12 : Xacc %g Xenv %g\n",nXacc[k][XC12],nXenv[k][XC12]);

        star->dmacc = star->stellar_type==MASSLESS_REMNANT ? 0.0 : ndmacc[k];

#ifndef NUCSYN_STRIP_AND_MIX
        if(ON_MAIN_SEQUENCE(star->stellar_type))
        {
            /* by memcpying we only set Xinit, but we also
             * want to set Xenv: for low and intermediate
             * mass stars this is fine, because the main sequence
             * abundances can only change due to mass accretion
             * ...
             * massive stars will then have their surface abundances
             * further changed (due to evolutionary effects)
             * in nucsyn_WR
             */
            if(stardata->preferences->no_thermohaline_mixing==FALSE)
            {
                /*
                 * In the case where thermohaline mixing is disabled,
                 * Xenv and Xinit cannot have changed because there
                 * is no mixing into the envelope
                 */
                Copy_abundances(nXenv[k],star->Xenv);
                Copy_abundances(star->Xenv,star->Xinit);
            }
        }
#endif // NUCSYN_STRIP_AND_MIX
        MDEBUG("MIXING star %d now has an accretion layer of size %g\n",k,star->dmacc);
        MDEBUG("XC12 out %d ACC %g ENV %g\n",
               k,
               star->Xacc[XC12],
               star->Xenv[XC12]);

    }

    MIXDEBUG_END;
#endif // NUCSYN

}
