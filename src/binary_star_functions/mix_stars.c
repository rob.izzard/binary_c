#include "../binary_c.h"
No_empty_translation_unit_warning;

#undef DEBUG
#define DEBUG 1

/*
 * Mix the two stars in stardata.
 */
void mix_stars(struct stardata_t *const stardata,
               const Boolean eject,
               const Yield_source source)
{
    stardata->model.merged = TRUE;

    switch(stardata->preferences->stellar_structure_algorithm)
    {
    case STELLAR_STRUCTURE_ALGORITHM_MODIFIED_BSE:
#ifdef BSE
        //replaces mix_stars_BSE(stardata,eject);
    {
        merge_into_star(stardata,
                        &stardata->star[0],
                        eject,
                        source);
        stellar_structure_make_massless_remnant(stardata,
                                                &stardata->star[1]);
#ifdef NUCSYN
        nucsyn_zero_yields(&stardata->star[1]);
#endif
    }
#endif // BSE
    break;

    case STELLAR_STRUCTURE_ALGORITHM_MINT:
#ifdef MINT
        MINT_mix_stars(stardata,
                       eject);
#endif // MINT
        break;
    default:
        Exit_binary_c(BINARY_C_ALGORITHM_OUT_OF_RANGE,
                      "Unknown stellar structure algorithm %d\n",
                      stardata->preferences->stellar_structure_algorithm);
    }
}
