#pragma once
#ifndef ACCRETION_REGIMES_H
#define ACCRETION_REGIMES_H
#ifdef KEMP_NOVAE

#include "accretion_regimes.def"

#undef X
#define X(REGIME) REGIME,
enum { ACCRETION_REGIMES_LIST };


#endif // KEMP_NOVAE
#endif // ACCRETION_REGIMES_H
