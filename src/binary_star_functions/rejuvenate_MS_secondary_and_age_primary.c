#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef BSE
static void rejuvenate_check(struct stardata_t * Restrict const stardata,
                             const int j,
                             const double dM_RLOF_transfer,
                             const double tphys);
#endif//BSE

void rejuvenate_MS_secondary_and_age_primary(struct stardata_t * Restrict const stardata Maybe_unused)
{
#ifdef BSE
    /*
     * Always rejuvenate the secondary and age the primary if they are on
     * the main sequence (or Hertzsprung gap)
     */
    RLOF_stars;

    rejuvenate_check(stardata,
                     ndonor,
                     0.0,
                     stardata->model.time);

    rejuvenate_check(stardata,
                     naccretor,
                     accretor->derivative[DERIVATIVE_STELLAR_MASS_RLOF_GAIN]*stardata->model.dt,
                     stardata->model.time);

#endif//BSE

}

#ifdef BSE
static void rejuvenate_check(struct stardata_t * Restrict const stardata,
                             const int j,
                             const double dM_RLOF_transfer,
                             const double tphys)
{
    struct star_t * const star = & stardata->star[j];

    Boolean do_rejuv = ON_EITHER_MAIN_SEQUENCE(star->stellar_type)
#ifdef REJUVENATE_HERTZSPRUNG_GAP_STARS
        ||
        star->stellar_type==HERTZSPRUNG_GAP
#endif
        ;

    Dprint("rejuv star %d ? %s\n",j,Yesno(do_rejuv));

    if(do_rejuv == TRUE)
    {
        struct BSE_data_t * bse = new_BSE_data();
        stellar_timescales(stardata,
                           star,
                           bse,
                           star->phase_start_mass,
                           star->mass,
                           star->stellar_type);

        Dprint("new MS timescale = %g\n",bse->tm);

        if(star->stellar_type==HERTZSPRUNG_GAP)
        {
            star->age = bse->tm +
                (bse->timescales[T_BGB] - bse->tm)*
                (star->age-star->tms)/(star->tbgb - star->tms);
        }

        /*
         * This bit should only be done for the accretor (star naccretor) i.e.
         * when dM_RLOF_transfer!=0.0
         */
        else if(Is_not_zero(dM_RLOF_transfer)
                &&
                (!(Convective_star(j)))&&
                (star->stellar_type!=HeMS))
        {
#if defined SELMA_FIX_ACCRETED_MASS_IN_REJUVENATION ||  \
    defined SELMA_BETTER_TREATMENT_OF_MS_ACCRETORS
            /*
             * Old implementation:
             * one could rejuvenate even if there
             * is no net gain of mass.  (for example if the star has
             * winds, or rotational mass loss, or is prevented from
             * accreting as it is near break-up`)
             */
            //star->age *= bse->tm/star->tms*(star->mass - dM_RLOF_transfer)/star->mass;  -SdM: this is wrong

            /*
             * New implementation, by SdM.  Not sure if this
             * is the best solution for every application
             */
            const double net_mass_gain = star->mass - star->last_mass;
            if(net_mass_gain > 0.0)
            {
                // mass gain --> rejuvenate

#ifdef SELMA_BETTER_TREATMENT_OF_MS_ACCRETORS

                /* define effective core masses */
                const double q_mceff_before =
                    effective_core_mass_fraction_of_MS_stars(star->last_mass,
                                                             stardata->common.metallicity);
                const double q_mceff_after =
                    effective_core_mass_fraction_of_MS_stars(star->last_mass + net_mass_gain,
                                                             stardata->common.metallicity);

                const double Mc_before = star->last_mass * q_mceff_before;

                const double Mc_after = q_mceff_after * (star->last_mass + net_mass_gain);

                // we may want to convert this into a setting
                const double mixingpar_ms_accretors = 0.0;
                const double mass_mixed_region =
                    Mc_after +
                    (Is_not_zero(mixingpar_ms_accretors) ? ((1.0-q_mceff_after)*mixingpar_ms_accretors) : 0.0);

                /* age multiplier */
                const double f =
                    bse->tm / star->tms *
                    Mc_before / mass_mixed_region;

                Dprint("AGE%d dm=%g (qc_prev=%g qc=%g Mc_after=%g)  f = (bse->tm=%g / tms=%g) * (Mc_before = %g / mass_mixed_region = %g) = %g\n",
                       star->starnum,
                       net_mass_gain,
                       q_mceff_before,
                       q_mceff_after,
                       Mc_after,
                       bse->tm,
                       star->tms,
                       Mc_before,
                       mass_mixed_region,
                       f);

                star->age *= f;

#elif defined SELMA_FIX_ACCRETED_MASS_IN_REJUVENATION
                /*
                 * this basically asumes that mc/m does not change which
                 * is not too bad if the star doesn't accrete too
                 * much. Otherwise it may under estimate the new core mass
                 * and thus over estmate the new (effective) age a little
                 * bit - SdM
                 */
                star->age *= (bse->tm/star->tms)*(star->last_mass)/(star->last_mass + net_mass_gain);
#endif //SELMA_BETTER_TREATMENT_OF_MS_ACCRETORS
            }
            else
            {
                /*
                 * net mass loss for accreting star: age the star
                 * (should be a rare phenomenon, but may happen at
                 * fast rotation?) age the star
                 */
                star->age *= bse->tm/star->tms;
            }
#else
            star->age *= bse->tm/star->tms*(star->mass - dM_RLOF_transfer)/star->mass;
#endif //SELMA_FIX_ACCRETED_MASS_IN_REJUVENATION || SELMA_BETTER_TREATMENT_OF_MS_ACCRETORS
        }
        else
        {
            star->age *= bse->tm/star->tms;
        }
        star->epoch = tphys - star->age;
        Dprint("Set epoch = %g - %g = %g\n",tphys,star->age,star->epoch);


        Dprint("Rejuvenated star %d t=%g age=%g m0=%g mt=%g\n",
               j,stardata->model.time,star->age,star->phase_start_mass,star->mass);

        /* free memory */
        free_BSE_data(&bse);
    }
}
#endif//BSE
