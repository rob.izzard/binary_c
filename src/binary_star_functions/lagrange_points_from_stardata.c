#include "../binary_c.h"
No_empty_translation_unit_warning;



void lagrange_points_from_stardata(struct stardata_t * Restrict const stardata,
                                   struct coordinate_t L[NUMBER_OF_LAGRANGE_POINTS],
                                   double *a1,
                                   double *a2
    )
{
    /*
     * Wrapper to calculate Lagrange points
     * from a stardata struct
     */
    lagrange_points(stardata->star[0].mass,
                    stardata->star[1].mass,
                    stardata->common.orbit.separation,
                    L,a1,a2);
}
