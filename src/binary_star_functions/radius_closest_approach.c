#include "../binary_c.h"
No_empty_translation_unit_warning;


/*
 * Radius of closest approach of a Roche stream 
 * to an accreting star.
 *
 * Eq.(1) of Ulrich & Burger (1976, ApJ, 206, 509) 
 *
 * fitted to the calculations of Lubow & Shu (1975, ApJ, 198, 383). 
 *
 * q is Mdetached/Mcontact = Maccretor/Mdonor
 */

Constant_function double radius_closest_approach(const double sep,
                                                 const double q)
{
    return 0.04250 * sep * Pow1d4( q * (1.0 + q) );
}
