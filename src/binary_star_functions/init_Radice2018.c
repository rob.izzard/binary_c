#include "../binary_c.h"
No_empty_translation_unit_warning;

void init_Radice2018(struct stardata_t * const stardata)
{
    if(stardata->persistent_data->Radice2018_ejecta == NULL)
    {
#ifndef ISOTOPE_ARRAY_SIZE
#define ISOTOPE_ARRAY_SIZE 0
#endif
        struct data_table_t * t = NULL;
        int wanted_columns[ISOTOPE_ARRAY_SIZE+3];
        /* we do not want column zero (table identifier) */
        wanted_columns[0] = 1; /* want col 1 (M1) */
        wanted_columns[1] = 2; /* want col 2 (M2) */
        wanted_columns[2] = 3; /* want col 3 (Mej) */
#ifdef NUCSYN
        for(size_t i=0;i<ISOTOPE_ARRAY_SIZE;i++)
        {
            /* want all the remaining columns (isotopes) */
            wanted_columns[3+i] = 4+i;
        }
#endif//NUCSYN
        /* list of strings for filenames */
#undef X
#define X(TYPE) #TYPE,
        static char * Radice2018_EOS_strings[] = {
            RADICE2018_EOS_LIST
        };

        char * file;
        if(asprintf(&file,
                    "src/nucsyn/Radice2018_%s.dat",
                    Radice2018_EOS_strings[stardata->preferences->Radice2018_EOS])>0)
        {
            Load_and_remap_table(t,
                                 file,
                                 wanted_columns,
                                 New_res(0,10), /* col 1 (M2) should be remapped */
                                 0,
                                 TRUE);
            stardata->persistent_data->Radice2018_ejecta = t;
            Safe_free(file);
        }
        else
        {
            Exit_binary_c(BINARY_C_ALLOC_FAILED,
                          "Failed to asprintf Radice2018 filename\n");
        }
    }
}
