#include "../binary_c.h"
No_empty_translation_unit_warning;


Stellar_type merger_stellar_type(struct stardata_t * const stardata,
                                 struct star_t * const donor,
                                 struct star_t * const accretor)
{
    Dprint("Get merger stellar type from donor=%p type %d, accretor=%p type %d\n",
           (void*)donor,
           donor->stellar_type,
           (void*)accretor,
           accretor->stellar_type);
    /*
     * When merging stars 1 and 2, set a new stellar type.
     *
     * First we set this according to BSE's collision matrix,
     * but then we add extra physics.
     */
    Stellar_type stellar_type =
        stardata->store->collision_matrix[donor->stellar_type][accretor->stellar_type];
    Dprint("Collision stellar type [%d][%d] = %d\n",
           donor->stellar_type,
           accretor->stellar_type,
           stellar_type);

    /*
     * > 100 means common-envelope: we don't care, so just
     * subtract the 100
     */
    if(stellar_type>100) stellar_type -= 100;
    Dprint("Set from collision matrix[%d][%d] = %d\n",
           donor->stellar_type,
           accretor->stellar_type,
           stellar_type);

    /* extra physics */

    /*
     * If we make a new TPAGB star according to BSE,
     * check for a hybrid He-COWD core: this should
     * actually form an EAGB star.
     */
    if(stellar_type == TPAGB &&
       ((accretor->stellar_type == COWD && accretor->hybrid_HeCOWD == TRUE) ||
        (donor->stellar_type == COWD && donor->hybrid_HeCOWD == TRUE)))
    {
        stellar_type = EAGB;
    }

    /*
     * HeWD - HeWD mergers explode in BSE,
     * but we ignite helium instead
     */
    if(accretor->stellar_type == HeWD &&
       donor->stellar_type == HeWD)
    {
        stellar_type = HeMS;
    }

#ifdef COWD_QUIET_MERGER
    if(accretor->stellar_type==COWD &&
       donor->stellar_type==COWD)
    {
        /*
         * assume COWD-COWD merger ignites carbon
         * and ejects the least massive WD
         */
        merged_stellar_type = ONeWD;
    }
#endif

    /*
     * If one star is a TZO, then the
     * merged star should be a TZO
     */
    if(donor->TZO != TZO_NONE ||
       accretor->TZO != TZO_NONE)
    {
        if(stellar_type <= TPAGB)
        {
            stellar_type = EAGB;
        }
        else
        {
            stellar_type = HeHG;
        }
    }

    Dprint("merged stellar type %d\n",stellar_type);
    return stellar_type;
}
