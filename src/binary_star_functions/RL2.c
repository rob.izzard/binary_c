#include "../binary_c.h"
No_empty_translation_unit_warning;


double Constant_function rL2(const double q
#ifdef RLOF_RADIATION_CORRECTION
                             ,const double f
#endif
    )
{
    /*
     * A function to evaluate rL2 = R_L2/a(Q), were Q=M1/M2,
     * from Mochnacki S. W., 1984, ApJS, 55, 551
     *
     * Note: this is for a circular binary system (e=0).
     *
     * M1 is the donor,
     * M2 is the accretor
     */
    double r;
    if(q < TINY)
    {
        r = 1e100;
    }
    else
    {
        const double x = q / (1.0 + q);
        r = rL1(q
#ifdef RLOF_RADIATION_CORRECTION
                ,f
#endif // RLOF_RADIATION_CORRECTION
            ) +
            (0.179 + 0.01 * x) *
            pow(x,0.625);
    }
    return r;
}
