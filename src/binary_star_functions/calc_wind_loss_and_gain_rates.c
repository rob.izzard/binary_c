#include "../binary_c.h"
No_empty_translation_unit_warning;

void calc_wind_loss_and_gain_rates(struct stardata_t * Restrict const stardata,
                                   const Boolean RLOF_bool)
{
    /*
     * Set the stellar wind mass loss and gain rates.
     *
     * If RLOF_bool is TRUE then you're in RLOF, which uses a
     * different radius (the Roche radius, rather than the stellar radius,
     * stored in star->effective_radius) rather than the usual star->radius.
     */
    const Boolean binary_system = System_is_binary;

    /*
     * Calculate wind velocities
     */
    Foreach_star(star)
    {
        star->vwind = wind_velocity(stardata,star); // cm/s
    }

    /*
     * Now the mass derivatives
     */
    if(stardata->preferences->wind_mass_loss != FALSE)
    {
        Foreach_star(donor,accretor)
        {
            /*
             * Wind mass loss rates.
             */
            const double rlperi = donor->roche_radius*
                (1.0-stardata->common.orbit.eccentricity);
            const double radius = RLOF_bool==FALSE ? donor->radius : donor->effective_radius;

            Dprint("CEE WIND donor is star %d: kw=%d, lum=%12.12g, radius=%12.12g, mass=%12.12g, mc=%12.12g, rlperi=%12.12g, Z=%12.12g, vwind = %g\n",
                   donor->starnum,
                   donor->stellar_type,
                   donor->luminosity,
                   radius,
                   donor->mass,
                   Outermost_core_mass(donor),
                   rlperi,
                   stardata->common.metallicity,
                   donor->vwind);

            double mdot = calc_stellar_wind_mass_loss(donor->stellar_type,
                                                      donor->luminosity,
                                                      radius,
                                                      donor->mass,
                                                      rlperi,
                                                      stardata->common.metallicity,
                                                      donor->omega,
                                                      stardata,
                                                      donor->starnum);

            /*
             * No wind from compact cores
             */
            {
                double compact_menv;
                if(donor->stellar_type == GIANT_BRANCH ||
                   donor->stellar_type == HERTZSPRUNG_GAP)
                {
                    compact_menv = donor->mass - donor->core_mass[CORE_He];
                }
                else if(donor->stellar_type == EAGB ||
                        donor->stellar_type == TPAGB ||
                        donor->stellar_type == HeHG ||
                        donor->stellar_type == HeGB)
                {
                    compact_menv = donor->mass - donor->core_mass[CORE_CO];
                }
                else
                {
                    compact_menv = 0.0;
                }
                compact_menv = Max5(compact_menv,
                                    donor->core_mass[CORE_Fe],
                                    donor->core_mass[CORE_Si],
                                    donor->core_mass[CORE_NEUTRON],
                                    donor->core_mass[CORE_BLACK_HOLE]);
                const double mdot_limit = compact_menv / stardata->model.dt;
                if(mdot > mdot_limit)
                {
                    mdot = mdot_limit;
                }
            }

            donor->derivative[DERIVATIVE_STELLAR_MASS_WIND_LOSS] = -mdot;
        }

        Foreach_star(donor,accretor)
        {
            if(donor->stellar_type != MASSLESS_REMNANT)
            {
                /*
                 * Wind accretion rate
                 */
                if(binary_system &&
                   donor->derivative[DERIVATIVE_STELLAR_MASS_WIND_LOSS] < -REALLY_TINY &&
                   donor->vwind > REALLY_TINY)
                {
                    /*
                     * Calculate how much of wind mass loss from companion will be
                     * accreted (see Hurley PhD dissertation Ch.3 for details,
                     * also Boffin & Jorissen, A&A 1988, 205, 155).
                     */
                    const double vorb_cgs = orbital_velocity_cgs(stardata);

                    accretor->derivative[DERIVATIVE_STELLAR_MASS_WIND_GAIN] =
                        Bondi_Hoyle_accretion_rate(stardata,
                                                   M_SUN*accretor->mass,
                                                   vorb_cgs,
                                                   M_SUN/YEAR_LENGTH_IN_SECONDS*donor->derivative[DERIVATIVE_STELLAR_MASS_WIND_LOSS],
                                                   donor->vwind,
                                                   R_SUN*stardata->common.orbit.separation,
                                                   stardata->common.orbit.eccentricity)
                        / (M_SUN / YEAR_LENGTH_IN_SECONDS);

#ifdef WRLOF_MASS_TRANSFER
                    accretor->derivative[DERIVATIVE_STELLAR_MASS_WIND_GAIN] =
                        Max(accretor->derivative[DERIVATIVE_STELLAR_MASS_WIND_GAIN],
                            WRLOF_rate(stardata,donor,accretor));
#endif // WRLOF_MASS_TRANSFER

                    /* limit accretion rate as in BSE */
                    accretor->derivative[DERIVATIVE_STELLAR_MASS_WIND_GAIN] =
                        Min(accretor->derivative[DERIVATIVE_STELLAR_MASS_WIND_GAIN],
                            (RLOF_bool==TRUE ? 1.0 : 0.8) * -donor->derivative[DERIVATIVE_STELLAR_MASS_WIND_LOSS]);

                    if(Is_not_zero(accretor->derivative[DERIVATIVE_STELLAR_MASS_WIND_GAIN]))
                    {
                        Dprint("Accretion from star %d (%s) to %d (%s) : rate %g (accretor mass %g, vorb_cgs %g cm/s, Mdot %g Msun/y, vwind %g cm/s)\n",
                               donor->starnum,
                               Stellar_type_string(donor->stellar_type),
                               accretor->starnum,
                               Stellar_type_string(accretor->stellar_type),
                               accretor->derivative[DERIVATIVE_STELLAR_MASS_WIND_GAIN],
                               accretor->mass,
                               vorb_cgs,
                               donor->derivative[DERIVATIVE_STELLAR_MASS_WIND_LOSS],
                               donor->vwind);

                    }
                }
                else
                {
                    /* detached system : no mass gain */
                    accretor->derivative[DERIVATIVE_STELLAR_MASS_WIND_GAIN] = 0.0;
                }
            }
            else
            {
                /*
                 * massless remnant : no wind and no accretion on the other star
                 */
                donor->derivative[DERIVATIVE_STELLAR_MASS_WIND_LOSS] = 0.0;
                accretor->derivative[DERIVATIVE_STELLAR_MASS_WIND_GAIN] = 0.0;
            }
        }
    }
    Dprint("Mass loss/gain rates : 0 %g %g : 1 %g %g\n",
           stardata->star[0].derivative[DERIVATIVE_STELLAR_MASS_WIND_LOSS],
           stardata->star[0].derivative[DERIVATIVE_STELLAR_MASS_WIND_GAIN],
           stardata->star[1].derivative[DERIVATIVE_STELLAR_MASS_WIND_LOSS],
           stardata->star[1].derivative[DERIVATIVE_STELLAR_MASS_WIND_GAIN]);
}
