#include "../binary_c.h"
No_empty_translation_unit_warning;


void compact_object_accretion_limits(struct stardata_t * const stardata,
                                     struct star_t * const accretor,
                                     struct star_t * const donor,
                                     double * Restrict const steady_burn_rate,
                                     double * Restrict const new_envelope_rate)
{
    /*
     * Compact object accretion limits.
     *
     * For some details see Claeys et al. (2014) Appendix B
     *
     * Thanks in advance to Karel Temmink for updating the
     * prescription to include carbon and oxygen burning.
     */

    if(WHITE_DWARF(accretor->stellar_type))
    {
        /*
         * White dwarfs
         */
        * steady_burn_rate = WD_min_accretion_rate(stardata,donor,accretor);
        * new_envelope_rate = WD_max_accretion_rate(stardata,donor,accretor);
    }
    else
    {
        /*
         * What about TZOs and X-ray bursts?
         * assume as WDs for now...
         */
        * steady_burn_rate = WD_min_accretion_rate(stardata,donor,accretor);
        * new_envelope_rate = WD_max_accretion_rate(stardata,donor,accretor);
//        * steady_burn_rate = VERY_SMALL_MASS_TRANSFER_RATE;
//        * new_envelope_rate = VERY_LARGE_MASS_TRANSFER_RATE;
    }
}
