#include "../binary_c.h"
No_empty_translation_unit_warning;

/*
 * Eddington accretion rate limit in Msun/year.
 * The radius (radius) and surface hydrogen mass fraction (X)
 * of the star must be passed in. If the multiplier
 * is negative, return a very large rate so this is ignored.
 */

double Pure_function eddington_limit_for_accretion(const double radius,
        const double multiplier,
        const Abundance X)
{

    return
        multiplier < (-TINY) ?
        /* disabled by negative multiplier */
        VERY_LARGE_MASS_TRANSFER_RATE :
        /* zero or positive multiplier */
        (multiplier * radius * (2.08e-3 / (1.0 + X)));
}
