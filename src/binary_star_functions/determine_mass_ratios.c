#include "../binary_c.h"
No_empty_translation_unit_warning;


void determine_mass_ratios(struct stardata_t * Restrict const stardata)
{
    /* Set the mass ratio for each star =
     * star->mass / other_star->mass :
     * this is the way the mass ratio should be
     * defined for Eggleton's formula
     */
    Star_number k;
    Starloop(k)
    {
        stardata->star[k].q = Q(k);
    }
}
