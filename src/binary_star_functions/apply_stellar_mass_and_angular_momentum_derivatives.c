#include "../binary_c.h"
No_empty_translation_unit_warning;

Boolean apply_stellar_mass_and_angular_momentum_derivatives(struct stardata_t * Restrict const stardata,
                                                            const Boolean RLOF)
{
    /*
     * Apply stellar mass and angular momentum derivatives
     *
     * Changes the masses and angular momenta of the stars,
     * and also (perhaps) the stellar type, epoch etc.
     * appropriate for the new mass and accreted material.
     *
     * Returns TRUE on success, FALSE on failure (e.g. if the
     * an unphysical value is constructed when derivatives
     * are applied).
     *
     * Note that the derivatives are all applied, whether
     * there is a "failure" or not. This failure test is done
     * in the various check functions (see the file
     * apply_stellar_mass_and_angular_momentum_checkfuncs.c).
     *af
     * It's then up to the caller to decide whether
     * to reject the timestep.
     */
    Star_number k;
    Boolean retval = TRUE;
    const Boolean can_reject = can_reject_and_shorten_timestep(stardata); // could be passed in?

    /*
     * Make a list of checkfuncs
     */
#undef X
#define X(CODE,STRING,VAR,FUNC,GROUP,MINTCLEAR) FUNC,
    static Boolean (*derivative_checkfunc[])
        (struct stardata_t * const,
         void * const data,
         const double,
         const double,
         const double,
         const double,
         const Derivative,
         const Derivative_group)
        Maybe_unused =
        { SYSTEM_DERIVATIVES_LIST };
#undef X

    /*
     * Timestep
     */
    const double dt =
        (RLOF==TRUE ? (stardata->model.dtm*1e6) : stardata->model.dt)
#ifdef REVERSE_TIME
        * (stardata->preferences->reverse_time == TRUE ? -1.0 : +1.0)
#endif
        ;
    const double dtm =
        stardata->model.dtm
#ifdef REVERSE_TIME
        * (stardata->preferences->reverse_time == TRUE ? -1.0 : +1.0)
#endif
        ;
    Dprint("Apply stellar mass and angular momentum derivs dt = %g\n",
           dt);

    Evolving_Starloop(k)
    {
        SETstar(k);
        Dprint("For star %d\n",k);
        /*
         * Change in angular momentum and mass
         */
        star->derivative[DERIVATIVE_STELLAR_ANGMOM] = Jdot_net(star);
        star->derivative[DERIVATIVE_STELLAR_MASS] = Mdot_net(star);

        Dprint("star %d apply ang mom deriv, was J = %g, dJ/dt=%g, dt=%g, dJ=%g\n",
               star->starnum,
               star->angular_momentum,
               star->derivative[DERIVATIVE_STELLAR_ANGMOM],
               dt,
               star->derivative[DERIVATIVE_STELLAR_ANGMOM]*dt);
        double wanted;
        if(apply_derivative(stardata,
                            &star->angular_momentum,
                            &wanted,
                            star->derivative,
                            dt,
                            DERIVATIVE_STELLAR_ANGMOM,
                            DERIVATIVES_GROUP_STELLAR,
                            star,
                            &derivative_check_stellar_angmom) == FALSE)
        {
            /*
             * angular momentum < 0 : suggests failure
             */
            if(can_reject)
            {
                /*
                 * We can can reject and shorten the timestep.
                 */
                stardata->model.reject_shorten_timestep = REJECT_STELLAR_ANGMOM_DERIVATIVE;
            }
            else
            {
                /*
                 * We cannot possibly resolve the angular momentum loss,
                 * so just remove it all instantly.
                 * This is formally incorrect, but the best we can do
                 * with the currently minimum timestep.
                 */
                star->angular_momentum = 0.0;
            }

            retval = FALSE;
        }

        /*
         * Preserve sanity in case something goes wrong.
         * Note that this breaks angular momentum conservation.
         */
        Clamp(star->angular_momentum,
              MINIMUM_STELLAR_ANGMOM,
              MAXIMUM_STELLAR_ANGMOM);

        /*
         * Core growth
         *
         * There are NUMBER_OF_CORES different stellar cores,
         * we should try to grow them all.
         */
        Dprint("core growth\n");

        /*
         * Map core types to derivatives, and apply those derivatives.
         * Note: we set the values to 666 to start with so we can then
         *       set them to something sensible.
         */
#define DUMMY_DERIVATIVE 666777
        static Derivative core_map[NUMBER_OF_CORES] = { DUMMY_DERIVATIVE };

        /*
         * Map core types to derivative rejection codes
         */
        static Reject_index reject_map[NUMBER_OF_CORES];

        /* first time setup */
        if(core_map[0] == DUMMY_DERIVATIVE)
        {
            if(Pthread_lock(BINARY_C_MUTEX_DERIVATIVE_MAP) == 0)
            {
                if(core_map[0] == DUMMY_DERIVATIVE)
                {
#undef X
#include "binary_c_stellar_cores.h"

#define X(TYPE,                                                         \
          LONG_LABEL,                                                   \
          SHORT_LABEL,                                                  \
          DERIV)                                                        \
                    if(TYPE != CORE_NONE && TYPE != NUMBER_OF_CORES)    \
                    {                                                   \
                        core_map[TYPE] =                                \
                            DERIVATIVE_STELLAR_ ## DERIV ## _CORE_MASS; \
                        reject_map[TYPE] =                              \
                            REJECT_STELLAR_ ## DERIV ## _CORE;          \
                    }

                    BINARY_C_STELLAR_CORE_LIST;
#undef X
                }
                Pthread_unlock(BINARY_C_MUTEX_DERIVATIVE_MAP);
            }
        }

        if(core_map[0] == DUMMY_DERIVATIVE)
        {
            /*
             * core_map setup failed
             */
            Exit_binary_c(BINARY_C_WRONG_ARGUMENT,
                          "Failed to set up core_map array, probably pthread locking failed and needs fixing.\n");
        }

        /*
         * Apply the derivative for each core type
         */
        for(int i=0; i<NUMBER_OF_CORES; i++)
        {
            if(apply_derivative(stardata,
                                &star->core_mass[i],
                                &wanted,
                                star->derivative,
                                dt,
                                core_map[i],
                                DERIVATIVES_GROUP_STELLAR,
                                star,
                                derivative_checkfunc[i]) == FALSE)
            {
                Dprint("%s core mass increase failed\n",
                       binary_c_long_core_strings[i]);
                stardata->model.reject_shorten_timestep = reject_map[i];
                retval = FALSE;
            }
        }

        /*
         * Special cores
         */
        if(apply_derivative(stardata,
                            &star->core_mass_no_3dup,
                            &wanted,
                            star->derivative,
                            dt,
                            DERIVATIVE_STELLAR_He_CORE_MASS_NO_TDUP,
                            DERIVATIVES_GROUP_STELLAR,
                            star,
                            NULL) == FALSE)
        {
            /* no test */
        }

        /*
         * Thermal pulses
         */
        if(apply_derivative(stardata,
                            &star->num_thermal_pulses,
                            &wanted,
                            star->derivative,
                            dt,
                            DERIVATIVE_STELLAR_NUM_THERMAL_PULSES,
                            DERIVATIVES_GROUP_STELLAR,
                            star,
                            &derivative_check_num_thermal_pulses) == FALSE)
        {
            stardata->model.reject_shorten_timestep =  REJECT_STELLAR_NUM_THERMAL_PULSES;
            retval = FALSE;
        }
        if(apply_derivative(stardata,
                            &star->num_thermal_pulses_since_mcmin,
                            &wanted,
                            star->derivative,
                            dt,
                            DERIVATIVE_STELLAR_NUM_THERMAL_PULSES_SINCE_MCMIN,
                            DERIVATIVES_GROUP_STELLAR,
                            star,
                            &derivative_check_num_thermal_pulses) == FALSE)
        {
            stardata->model.reject_shorten_timestep =  REJECT_STELLAR_NUM_THERMAL_PULSES;
            retval = FALSE;
        }

        /*
         * Core masses cannot exceed the total mass
         * or the outermost core mass
         */
        {
            const double mc_outer = Outermost_core_mass(star);
            for(Core_type i=0; i<NUMBER_OF_CORES; i++)
            {
                star->core_mass[i] = Min3(star->core_mass[i],
                                          star->mass,
                                          mc_outer);
            }
        }

        /*
         * Change in mass, e.g. stellar winds, RLOF
         */
        double mass_was = star->mass;

        if(Is_zero(star->baryonic_mass) &&
           star->stellar_type != MASSLESS_REMNANT)
        {
            /*
             * Baryonic mass not yet set: set it
             */
            star->baryonic_mass = star->mass;
        }

        /*
         * Alter the mass derivative to prevent RGB
         * and TPAGB stars from having their cores
         * eaten
         */
        Dprint("Mass loss: star %d st %d stellar mass is currently %20.12e, losing mass will give %20.12e core is %20.12e\n",
               star->starnum,
               star->stellar_type,
               star->mass,
               star->mass + star->derivative[DERIVATIVE_STELLAR_MASS]*dt,
               Outermost_core_mass(star));

        /*
         * Some types of stars should never have mass removed
         * from their cores. This is, naturally, rather arbitrary
         * and a total fudge to get around limited time resolution,
         * but it is probably reasonable.
         *
         * At the moment we only scale mass for TPAGB and RGB stars, i.e.
         * with a compact (pre-)white-dwarf core.
         */
        if((star->stellar_type == TPAGB ||
            star->stellar_type == GIANT_BRANCH) &&
           star->derivative[DERIVATIVE_STELLAR_MASS] < 0.0)
        {
            const double menv = star->mass - Outermost_core_mass(star); /* > 0.0 */
            const double dm = star->derivative[DERIVATIVE_STELLAR_MASS]*dt; /* < 0.0 */
            Dprint("Envelope mass %20.12e, want to remove %20.12e excess is %g\n",
                   menv,-dm,menv + dm);
            if(menv + dm < 0)
            {
                /*
                 * Maximum mass-loss rate possible in this timestep
                 */
                const double dmdt_max = -menv/dt;
                Dprint("Want mass derivative %g, max is %g\n",
                       star->derivative[DERIVATIVE_STELLAR_MASS],
                       dmdt_max);
                /*
                 * Hence scaling factor
                 *
                 * Note that menv>0 and dm<0
                 */
                const double f = -menv/dm;
                Dprint("Mass loss would strip the core : reduce mass loss rates by factor %g\n",f);

                /*
                 * We should modulate *all* the mass change rates.
                 *
                 * Yes, this is a fudge, but hopefully only
                 * for one timestep.
                 */
                Modulate_stellar_mass_derivatives(star,f);

                Dprint("New DERIVATIVE_STELLAR_MASS is %g\n",
                       star->derivative[DERIVATIVE_STELLAR_MASS]);
                Dprint("New mass will be %20.12e\n",
                       star->mass + star->derivative[DERIVATIVE_STELLAR_MASS]*dt);
            }
        }


        Dprint("Set mass derivative %d %g = gain %g + loss %g\n",
               star->starnum,
               star->derivative[DERIVATIVE_STELLAR_MASS],
               Mdot_gain(star),
               Mdot_loss(star));

        /*
         * Here we change the "mass" variable to avoid
         * issues with Dprint checking the baryonic mass
         * is the same as the total mass.
         */
        double mass = star->mass;
        if(apply_derivative(stardata,
                            &mass,
                            &wanted,
                            star->derivative,
                            dt,
                            DERIVATIVE_STELLAR_MASS,
                            DERIVATIVES_GROUP_STELLAR,
                            star,
                            &derivative_check_stellar_mass) == FALSE)
        {
            /*
             * mass < 0 or mass < core mass : fail
             */
            Dprint("mass check failed: might have m = %g < 0 or m = %g < mc = %g (m-mc = %g)\n",
                   wanted,
                   wanted,
                   Outermost_core_mass(star),
                   wanted - Outermost_core_mass(star));
            stardata->model.reject_shorten_timestep = REJECT_STELLAR_MASS_DERIVATIVE;
            retval = FALSE;

            if(can_reject == FALSE)
            {
                /*
                 * Cannot reject : what to do?
                 */
                if(wanted < 0.0)
                {
                    /*
                     * Negative mass : just evaporate the star
                     */
                    const double dm = star->derivative[DERIVATIVE_STELLAR_MASS]*dt; /* < 0.0 */
                    const double f = -star->mass/dm;
                    Modulate_stellar_mass_derivatives(star,f);
                    Dprint("Mass loss has evaporated the star, dm/dt now %g\n",
                           star->derivative[DERIVATIVE_STELLAR_MASS]*dt);
                }
                else if(wanted < Outermost_core_mass(star))
                {
                    /*
                     * don't want to trim the core
                     */
                    const double dm = Outermost_core_mass(star) - wanted; /* >0 */
                    star->derivative[DERIVATIVE_STELLAR_MASS] += dm/dt;
                    mass = Outermost_core_mass(star);
                    Dprint("Mass loss has eaten into the core: we're not allowing this\n");
                }
            }
        }

        /*
         * Update graviational and baryonic masses
         */
        const double dm = mass - mass_was;
        update_mass(stardata,star,dm);

#ifdef MINIMUM_STELLAR_MASS
        star->mass = Max(MINIMUM_STELLAR_MASS,
                         star->mass);
        star->baryonic_mass = Max(MINIMUM_STELLAR_MASS,
                                  star->baryonic_mass);
#endif // MINIMUM_STELLAR_MASS

        /*
         * Update core masses
         */
        {
            const double mc_outer = Outermost_core_mass(star);
            for(Core_type i=0; i<NUMBER_OF_CORES; i++)
            {
                star->core_mass[i] = Min3(star->core_mass[i],
                                          star->mass,
                                          mc_outer);
            }
        }

        /*
         * Check for evaporation
         */
        if(Less_or_equal(star->mass,0.0))
        {
            stellar_structure_make_massless_remnant(stardata,
                                                    star);
        }

#ifdef STRIP_SUPERMASSIVE_STARS
        if(star->stellar_type < NEUTRON_STAR &&
           star->mass > MAXIMUM_STELLAR_MASS)
        {
            strip_supermassive_star(stardata,
                                    star);
        }
#endif // STRIP_SUPERMASSIVE_STARS

        /*
         * Reset phase start mass on some kinds of accretion
         */
        if(ON_EITHER_MAIN_SEQUENCE(star->stellar_type) ||
           (star->stellar_type==HERTZSPRUNG_GAP &&
            RLOF==FALSE &&
#ifdef REJUVENATE_HERTZSPRUNG_GAP_STARS
            TRUE
#else
            FALSE
#endif
               ))
        {
            star->phase_start_mass = star->mass;

#ifdef MINIMUM_STELLAR_MASS
            star->phase_start_mass = Max(MINIMUM_STELLAR_MASS,
                                         star->phase_start_mass);
#endif // MINIMUM_STELLAR_MASS

            if(
                (RLOF==FALSE)
                ||
                (RLOF==TRUE &&
                 star->stellar_type==HERTZSPRUNG_GAP &&
                 stellar_structure_giant_branch_helium_ignition(stardata,star)
                    )
                )
            {
#ifdef BSE
                /*
                 * Update epoch
                 */
                double m0 = star->phase_start_mass;
                Dprint("Set initial mass to %g (was %g)\n",star->phase_start_mass,m0);

                struct BSE_data_t * bse = new_BSE_data();

                stellar_timescales(stardata,
                                   star,
                                   bse,
                                   star->phase_start_mass,
                                   star->mass,
                                   star->stellar_type);
                Dprint("stellar_timescales gave tm=%30.22e tn=%30.22e\n",bse->tm,bse->tn);

                if(RLOF==TRUE)
                {
                    if(bse->GB[GB_MC_BGB] < star->core_mass[CORE_He])
                    {
                        star->phase_start_mass = m0;
                    }
                }
                else
                {
                    if(star->stellar_type==HERTZSPRUNG_GAP)
                    {
                        if((bse->GB[GB_MC_BGB] < star->core_mass[CORE_He])||
                           (m0 > stardata->common.metallicity_parameters[ZPAR_MASS_FGB]))
                        {
                            star->phase_start_mass = m0;
                            Dprint("Overridden to m0=%g for HG star\n",m0);
                        }
                        else
                        {
                            const double agex =
                                bse->tm + (bse->timescales[T_BGB] - bse->tm)*
                                (star->age - star->tms)/
                                (star->tbgb - star->tms);
                            star->epoch = stardata->model.time - agex;
                        }
                        Dprint("set epoch (apply1) = %g (tm=%g tbgb=%g age=%g tms=%g)\n",
                               star->epoch,
                               bse->tm,
                               bse->timescales[T_BGB],
                               star->age,
                               star->tms
                            );
                    }
                    else
                    {
#ifdef MINT
                        if(stardata->preferences->stellar_structure_algorithm == STELLAR_STRUCTURE_ALGORITHM_MINT)
                        {
                            /* do nothing */
                        }
#else
                        {
                            /* BSE */
                            star->epoch = stardata->model.time -
                                star->age * bse->tm / star->tms;
                        }
#endif//MINT
                        Dprint("set epoch (apply2) star %d type %d = %30.22e - %30.22e * %30.22e/%30.22e = %30.22e\n",
                               star->starnum,
                               star->stellar_type,
                               stardata->model.time,
                               star->age,
                               bse->tm,
                               star->tms,
                               star->epoch);
                    }
                }

                free_BSE_data(&bse);
#endif//BSE
            }
        }

        if(stardata->preferences->individual_novae == TRUE)
        {
            /*
             * Grow the surface hydrogen layer mass when we have
             * individual novae
             */
            if(apply_derivative(stardata,
                                &star->dm_novaH,
                                &wanted,
                                star->derivative,
                                dt,
                                DERIVATIVE_STELLAR_H_LAYER_MASS,
                                DERIVATIVES_GROUP_STELLAR,
                                star,
                                &derivative_check_dm_novaH)==FALSE)
           {
                stardata->model.reject_shorten_timestep = REJECT_STELLAR_DM_NOVAH_DERIVATIVE;
                retval = FALSE;
            }

#ifdef KEMP_NOVAE
            if(apply_derivative(stardata,
                                &star->dm_novaHe,
                                &wanted,
                                star->derivative,
                                dt,
                                DERIVATIVE_STELLAR_He_LAYER_MASS,
                                DERIVATIVES_GROUP_STELLAR,
                                star,
                                &derivative_check_dm_novaHe)==FALSE)
           {
                stardata->model.reject_shorten_timestep = REJECT_STELLAR_DM_NOVAHe_DERIVATIVE;
                retval = FALSE;
            }
#endif // KEMP_NOVAE
        }

        /*
         * Count novae
         */
        if(apply_derivative(stardata,
                            &star->num_novae,
                            &wanted,
                            star->derivative,
                            dt,
                            DERIVATIVE_STELLAR_NUM_NOVAE,
                            DERIVATIVES_GROUP_STELLAR,
                            star,
                            &derivative_check_num_novae ) == FALSE)
        {
            stardata->model.reject_shorten_timestep = REJECT_STELLAR_NUM_NOVAE_DERIVATIVE;
            retval = FALSE;
        }

        /*
         * Update the stellar type as a result of accretion:
         *
         * This only happens if the accretion rate exceeds
         * the new envelope rate, or if hydrogen accretes onto
         * a naked helium star.
         *
         * Novae are dealt with elsewhere.
         */
        struct star_t * accretor = star;
        struct star_t * donor = Other_star_struct(star);
        double accretion_rate = Mdot_net(accretor);

        if(accretion_rate > 0.0 && dm > 0.0)
        {
            if(NAKED_HELIUM_STAR(accretor->stellar_type) &&
               donor->stellar_type <= TPAGB)
            {
                /*
                 * Hydrogen accretion onto a helium star
                 * forms a CHeB star
                 */
                Boolean alloc[3];
                stellar_structure_BSE_alloc_arrays(accretor,NULL,alloc);
                stellar_timescales(stardata,
                                   accretor,
                                   accretor->bse,
                                   accretor->phase_start_mass,
                                   accretor->mass,
                                   accretor->stellar_type);

                Stellar_type stellar_type =
                    // HeMS has a helium core -> CHeB
                    accretor->stellar_type == HeMS ? CHeB :
                    // HeHG has a CO core with helium shell -> EAGB
                    accretor->stellar_type == HeHG ? EAGB :
                    //HeGB has CO core with thin helium shell -> TPAGB
                    TPAGB;

                Dprint("Accrete to type %d\n",stellar_type);

                accretor->white_dwarf_atmosphere_type =
                    WHITE_DWARF_ATMOSPHERE_HYDROGEN;

                /*
                 * New TPAGB star: reset number of thermal pulses
                 */
                if(accretor->stellar_type == TPAGB)
                {
                    accretor->num_thermal_pulses = -1;
                }


                /*
                 * ~pure helium core has the same mass as the accretor
                 * not including the freshly accreted material.
                 *
                 * If there's a CO core, e.g. HeHG and HeGB types,
                 * this mass remains the same.
                 */
                accretor->core_mass[CORE_He] = accretor->mass - dm;

                Dprint("stellar type was %d mass was %g\n",
                       accretor->stellar_type,
                       accretor->mass);

                /*
                 * Core mass is the whole of the helium star.
                 * The envelope is the accreted hydrogen.
                 */
                accretor->phase_start_mass = Max(accretor->mass,
                                                 accretor->phase_start_mass);
#ifdef BSE
                giant_age(&accretor->core_mass[CORE_He],
                          accretor->mass,
                          &stellar_type,
                          &accretor->phase_start_mass,
                          &accretor->age,
                          stardata,
                          accretor);

                accretor->stellar_type = stellar_type;
                stellar_timescales(stardata,
                                   accretor,
                                   accretor->bse,
                                   accretor->phase_start_mass,
                                   accretor->mass,
                                   accretor->stellar_type);
                Dprint("To CHeB giant age st=%d age %g, mass %g, core_mass = %g %g, dm = %g\n",
                       stellar_type,
                       accretor->age,
                       accretor->mass,
                       accretor->core_mass[CORE_He],
                       accretor->core_mass[CORE_CO],
                       dm);
#endif//BSE
                accretor->core_mass[CORE_CO] = Min(accretor->core_mass[CORE_He],
                                                   accretor->core_mass[CORE_CO]);
                accretor->epoch = stardata->model.time +
                    dtm - accretor->age;
                Dprint("This is %d : M %g He %g CO %g\n",
                       accretor->stellar_type,
                       accretor->mass,
                       accretor->core_mass[CORE_He],
                       accretor->core_mass[CORE_CO]);
            }
            else if(COMPACT_OBJECT(accretor->stellar_type))
            {
                /*
                 * Accretion onto a compact object,
                 * i.e. white dwarf, neutron star or black hole
                 */
                double steady_burn_rate;
                double new_envelope_rate;
                compact_object_accretion_limits(stardata,
                                                accretor,
                                                donor,
                                                &steady_burn_rate,
                                                &new_envelope_rate);

                /*
                 * Set WD atmosphere type to match the donor
                 */
                accretor->white_dwarf_atmosphere_type =
                    NAKED_HELIUM_STAR(donor->stellar_type) ?
                    WHITE_DWARF_ATMOSPHERE_HELIUM :
                    WHITE_DWARF_ATMOSPHERE_HYDROGEN;

                /*
                 * Put new material into a "core" type
                 * if required
                 */
                if(accretor->white_dwarf_atmosphere_type == WHITE_DWARF_ATMOSPHERE_HELIUM)
                {
                    if(Is_zero(accretor->core_mass[CORE_He]))
                    {
                        accretor->core_mass[CORE_He] = Max(accretor->core_mass[CORE_CO],
                                                           accretor->core_mass[CORE_ONe]);
                    }
                    accretor->core_mass[CORE_He] += Mdot_net(accretor) * stardata->model.dt;
                }

                if(Mdot_net(accretor) > new_envelope_rate)
                {
                    /*
                     * WD accretion leads to a new envelope
                     */
                    if(WHITE_DWARF(accretor->stellar_type))
                    {
                        Stellar_type new_stellar_type =
                            donor->stellar_type <= TPAGB ?

                            /*
                             * Hydrogen accretion, new type depends
                             * on core type (He or HeCO hybrid or CO/ONe)
                             */
                            (accretor->stellar_type == HeWD ? GIANT_BRANCH :
                             (accretor->hybrid_HeCOWD == TRUE ? EAGB : TPAGB)) :

                            /*
                             * helium accretion
                             */
                            HeGB; // helium donor -> always a helium giant (core is CO)

                        /*
                         * Reset number of thermal pulses
                         */
                        if(accretor->stellar_type == TPAGB)
                        {
                            accretor->num_thermal_pulses = -1;
                        }
#ifdef BSE
                        double _mc = Outermost_core_mass(accretor);
                        giant_age(&_mc,
                                  accretor->mass,
                                  &new_stellar_type,
                                  &accretor->phase_start_mass,
                                  &accretor->age,
                                  stardata,
                                  accretor);
                        const Core_type core_id = ID_core(accretor->stellar_type);
                        if(core_id != CORE_NONE)
                        {
                            accretor->core_mass[core_id] = _mc;
                        }
#endif // BSE

                        /* set the appropriate core mass */
                        if(new_stellar_type == TPAGB)
                        {
                            accretor->core_mass[CORE_He] = Outermost_core_mass(accretor);
                            //accretor->core_mass[CORE_CO] = Outermost_core_mass(accretor);
                        }
                        else if(new_stellar_type == EAGB)
                        {
                            accretor->core_mass[CORE_He] = Outermost_core_mass(accretor);
                            // CO core remains unchanged
                        }
                        else if(new_stellar_type == HeGB)
                        {
                            accretor->core_mass[CORE_He] = 0.0;
                        }

                        /* make sure cores are in the right order */
                        accretor->core_mass[CORE_He] = Min(accretor->mass,
                                                           accretor->core_mass[CORE_He]);
                        accretor->core_mass[CORE_CO] = Min(accretor->core_mass[CORE_He],
                                                           accretor->core_mass[CORE_CO]);

                        accretor->epoch = stardata->model.time +
                            dtm - accretor->age;
                        Dprint("Set accretor (star %d) epoch = %g\n",
                               accretor->starnum,
                               accretor->epoch);

                        /*
                         * Set the new stellar type
                         */
#ifdef NUCSYN
#ifdef NUCSYN_STRIP_AND_MIX
                        accretor->strip_and_mix_disabled=TRUE;
#endif
                        if(new_stellar_type!=accretor->stellar_type &&
                           new_stellar_type < HeWD)
                        {
                            /*
                             * stellar type has been changed in GNTAGE,
                             * perhaps to a He star
                             *
                             * The accreted layer is now the new surface.
                             */
                            Copy_abundances(accretor->Xacc,
                                            accretor->Xenv);
                        }
#endif //NUCSYN
                        star->stellar_type = new_stellar_type;
                    }
                    else
                    {
                        /*
                         * A NS or BH can form a red giant, i.e. a Thorne-Zytkow
                         * object, by accretion.
                         */
                        if(0)
                        {
                            Dprint("Form TZO\n");
                            star->TZO = TZO_TRANSFER;

                            /*
                             * Should be an EAGB star or a helium giant,
                             * depending on whether the donor was hydrogen
                             * or helium rich.
                             */
                            star->stellar_type = donor->stellar_type <= TPAGB ? EAGB : HeHG;


                            if(star->stellar_type == EAGB)
                            {
                                /*
                                 * hydrogen-rich -> EAGB star
                                 */
                                star->core_mass[CORE_CO] = Max(star->core_mass[CORE_CO],
                                                               star->core_mass[CORE_NEUTRON]);
                            }
                            else
                            {
                                /*
                                 * helium-rich -> HeHG star
                                 */
                                star->core_mass[CORE_He] = 0.0;
                            }
                            stack_cores(star,star->mass);
                            Dprint("New stellar type %s : cores He %g CO %g n %g\n",
                                   Short_stellar_type_string(star->stellar_type),
                                   star->core_mass[CORE_He],
                                   star->core_mass[CORE_CO],
                                   star->core_mass[CORE_NEUTRON]);

#ifdef NUCSYN
                            /*
                             * We need to copy across abundances now,
                             * otherwise giant_age, which calls stellar
                             * structure, may fail.
                             */
                            Copy_abundances(donor->Xenv,
                                            accretor->Xenv);
#endif // NUCSYN
#ifdef BSE
                            Dprint("Compute new age\n");
                            struct star_t * newstar = New_star_from(accretor);
                            double core_mass = Max(star->core_mass[CORE_He],
                                                   star->core_mass[CORE_CO]);
                            giant_age(&core_mass,
                                      newstar->mass,
                                      &newstar->stellar_type,
                                      &newstar->phase_start_mass,
                                      &newstar->age,
                                      stardata,
                                      newstar);
                            accretor->age = newstar->age;
                            accretor->phase_start_mass = newstar->phase_start_mass;
                            free_star(&newstar);
#endif//BSE
                            Append_logstring(LOG_TZO,
                                             "TZO (accretion, %s, %s) : env %g, cores He %g CO %g n %g\n",
                                             star->stellar_type == EAGB ? "H" : "He",
                                             short_stellar_types[star->stellar_type],
                                             star->mass - star->core_mass[star->stellar_type == EAGB ? CORE_He : CORE_CO],
                                             star->core_mass[CORE_He],
                                             star->core_mass[CORE_CO],
                                             star->core_mass[CORE_NEUTRON]);
                            star->epoch = stardata->model.time - star->age;
                        }
                    }
                }
            }
        }
        Dprint("RETVAL %d star %d st %d Mgrav %20.12e Mbary %20.12e Mcore %20.12e Menv %20.12e\n",
               retval,
               star->starnum,
               star->stellar_type,
               star->mass,
               star->baryonic_mass,
               Outermost_core_mass(star),
               star->mass - Outermost_core_mass(star));
    }

    calculate_spins(stardata);

    return retval;
}
