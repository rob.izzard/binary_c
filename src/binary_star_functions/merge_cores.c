#include "../binary_c.h"
No_empty_translation_unit_warning;

/*
 * Merge the cores of stars a and b into star c
 *
 * Note: a,b,c can overlap (they cannot be Restrict type)
 */
void merge_cores(struct stardata_t * const stardata Maybe_unused,
                 const struct star_t * const a,
                 const struct star_t * const b,
                 struct star_t * const c)
{
    /*
     * Cores are stored as mass coordinates, but if
     * the coordinate is zero there is nothing of the
     * type there and it should be ignored.
     */

    for(Core_type i=0; i<NUMBER_OF_CORES; i++)
    {
        Dprint("CEE core %d %s a %g b %g\n",
               i,
               Core_string_short(i),
               a->core_mass[i],
               b->core_mass[i]);
    }

    /*
     * Compute mass of material of type i
     */
    for(Core_type i=0; i<NUMBER_OF_CORES; i++)
    {
        double da = 0.0, db = 0.0;
        if(Core_exists(a,i))
        {
            da = a->core_mass[i];
            for(int j=i+1; j<NUMBER_OF_CORES; j++)
            {
                if(!Is_zero(a->core_mass[j]))
                {
                    da -= a->core_mass[j];
                    j = NUMBER_OF_CORES + 1;
                }
            }
        }

        if(Core_exists(b,i))
        {
            db = b->core_mass[i];
            for(int j=i+1; j<NUMBER_OF_CORES; j++)
            {
                if(!Is_zero(b->core_mass[j]))
                {
                    db -= b->core_mass[j];
                    j = NUMBER_OF_CORES + 1;
                }
            }
        }

        /*
         * Hence the total mass of type i
         */
        c->core_mass[i] = da + db;
        Dprint("CEE mass in core material type %d %s = %g + %g = %g\n",
               i,
               Core_string_short(i),
               da,
               db,
               c->core_mass[i]);
    }

    /*
     * Convert back to outermost mass coordinates.
     * Note: start the loop at NUMBER_OF_CORES-1 as
     * NUMBER_OF_CORES-1 has nothing internal to it.
     */
    for(Core_type i=NUMBER_OF_CORES-2; i>=0; i--)
    {
        if(Core_exists(c,i+1))
        {
            c->core_mass[i] += c->core_mass[i+1];
        }
    }

    /*
     * Equal core masses should have the next set to zero
     * to indicate there is no core of this type.
     */
    if(0)
    {
        for(Core_type i=0; i<NUMBER_OF_CORES; i++)
        {
            if(Fequal(c->core_mass[i],
                      c->core_mass[i+1]))
            {
                c->core_mass[i] = 0.0;
            }
            Dprint("CEE coord of core %d %s = %g\n",
                   i,
                   Core_string_short(i),
                   c->core_mass[i]);
        }
    }
}
