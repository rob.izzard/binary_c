#include "../binary_c.h"
No_empty_translation_unit_warning;

struct _overspin_data_t {
    double dJorb;
    double dJorb_noncon;
    double dJstar_tides;
    double dJstar_decretion;
    double dMstar_decretion;
};
static Event_handler_function overspin_event_handler(void * eventp,
                                                     struct stardata_t * stardata,
                                                     void * data);
static void _compute_overspin_corrections(struct stardata_t * const stardata,
                                          struct star_t * const star,
                                          struct _overspin_data_t * const odata);
static Boolean _star_is_overspinning(struct stardata_t * const stardata,
                                     struct star_t * const star);
static Boolean _can_do_overspin_checks(struct stardata_t * const stardata,
                                       struct star_t * const star);

/*
 * Function to detect stellar spin beyond the breakup velocity
 *
 * The in_RLOF boolean activates slightly different code in the case
 * we are in RLOF (e.g. the accretion prevention code)
 *
 * Note:
 * You can CHANGE derivative[ORBIT_ANGMOM]
 * and
 * You can SET the stellar excretion disc derivative
 * but
 * do NOT set or change the STELLAR_ANGMOM derivative
 *
 * Returns TRUE if overspin occurs, FALSE otherwise.
 *
 * We currently can either treat overspin as an event,
 * or continuously. This is why we either do,
 * or do not, add Jdot_net(star).
 */

Boolean prevent_overspin(struct stardata_t * const stardata,
                         struct star_t * const star)
{
#ifdef NANCHECKS
    if(isnan(star->angular_momentum) || isnan(star->omega))
    {
        Backtrace;
        Exit_binary_c(BINARY_C_EXIT_NAN,
                      "NAN at entrance to prevent_overspin : star %d : Jspin=%g omega=%g \n",
                      star->starnum,
                      star->angular_momentum,
                      star->omega
            );
    }
#endif

    const Boolean do_overspin_checks = _can_do_overspin_checks(stardata,
                                                               star);
    Boolean overspin = FALSE;

    Dprint("star %d (R=%g, RL=%g) -> dt = %g; J = %g, omega = %g: do overspin checks? %s \n",
           star->starnum,
           star->radius,
           star->roche_radius,
           stardata->model.dt,
           star->angular_momentum,
           star->omega,
           Yesno(do_overspin_checks));

    if(do_overspin_checks == TRUE)
    {
        overspin = _star_is_overspinning(stardata,
                                         star);

        Dprint("t=%g Star %d, stellar type %d, Jcrit = %g, Jstar = %g + %g * %g = %g : J/Jcrit = %g, overspin? %s\n",
               stardata->model.time,
               star->starnum,
               star->stellar_type,
               breakup_angular_momentum(stardata,star),
               star->angular_momentum,
               Jdot_net(star),
               stardata->model.dt,
               star->angular_momentum,
               star->angular_momentum/breakup_angular_momentum(stardata,star),
               Yesno(overspin));

        if(overspin == TRUE)
        {
#ifdef OVERSPIN_EVENTS
            struct binary_c_overspin_event_t * event_data =
                Malloc(sizeof(struct binary_c_overspin_event_t));
            event_data->star = star;
            if(Add_new_event(stardata,
                             BINARY_C_EVENT_OVERSPIN_MASS_LOSS,
                             &overspin_event_handler,
                             NULL,
                             event_data,
                             NORMAL_EVENT) == BINARY_C_EVENT_DENIED)
            {
                Dprint("not allowed prevent_overspin event : disable? %d deny? %d\n",
                       stardata->preferences->disable_events,
                       stardata->model.deny_new_events
                    );
                Safe_free(event_data);
            }

#else
            /*
             * Overspin should be treated as smooth derivatives
             */
            struct _overspin_data_t * odata = Calloc(1,sizeof(struct _overspin_data_t));
            _compute_overspin_corrections(stardata,
                                          star,
                                          odata);
            stardata->model.derivative[DERIVATIVE_ORBIT_ANGMOM_NONCONSERVATIVE_LOSS] += odata->dJorb_noncon;
            stardata->model.derivative[DERIVATIVE_ORBIT_ANGMOM] += odata->dJorb;
            star->derivative[DERIVATIVE_STELLAR_ANGMOM_TIDES] += odata->dJstar_tides;
            star->derivative[DERIVATIVE_STELLAR_ANGMOM_DECRETION_DISC] += odata->dJstar_decretion;
            star->derivative[DERIVATIVE_STELLAR_MASS_DECRETION_DISC] += odata->dMstar_decretion;
            Safe_free(odata);
#endif // OVERSPIN_EVENTS
        }
    }

    Dprint("Was there overspin? %s\n",Truefalse(overspin));
    return overspin;
}




static Boolean _can_do_overspin_checks(struct stardata_t * const stardata,
                                       struct star_t * const star)
{
    /*
     * Check if we can do overspin checks
     */
    return !(
        /* we can do nothing if the timestep is zero */
        Less_or_equal(stardata->model.dt,0.0) ||
        stardata->model.intpol>0 ||

        /* we can do nothing if we have no spin */
        Is_zero(star->omega)
        );
}



/*
 * Event handling function for overspin
 */

static Event_handler_function overspin_event_handler(void * eventp Maybe_unused,
                                                     struct stardata_t * stardata,
                                                     void * data)
{
    //struct binary_c_event_t * const event = eventp;
    struct binary_c_overspin_event_t * event_data = (struct binary_c_overspin_event_t *) data;
    struct star_t * const star = event_data->star;

    if(_can_do_overspin_checks(stardata,star) &&
       _star_is_overspinning(stardata,star))
    {
        struct _overspin_data_t * odata = Calloc(1,sizeof(struct _overspin_data_t));
        _compute_overspin_corrections(stardata,
                                      star,
                                      odata);

        Dprint("overspin handler star %d type %d : t=%g, dt = %g, star M was %g, J was %g, Jcrit %g, dM = %g, dJ = %g : ",
               star->starnum,
               star->stellar_type,
               stardata->model.time,
               stardata->model.dt,
               star->mass,
               star->angular_momentum,
               breakup_angular_momentum(stardata,
                                        star),
               odata->dMstar_decretion * stardata->model.dt,
               odata->dJstar_decretion * stardata->model.dt);

        stardata->common.orbit.angular_momentum += (odata->dJorb_noncon + odata->dJorb)  * stardata->model.dt;

        Dprint("star %d : J was %g : add %g %g -> %g\n",
               star->starnum,
               star->angular_momentum,
               odata->dJstar_tides * stardata->model.dt,
               odata->dJstar_decretion * stardata->model.dt,
               star->angular_momentum +
               odata->dJstar_tides * stardata->model.dt +
               odata->dJstar_decretion * stardata->model.dt);

        star->angular_momentum += (odata->dJstar_tides + odata->dJstar_decretion) * stardata->model.dt;
        const double dM = -odata->dMstar_decretion * stardata->model.dt; /* positive mass lost */
        if(Is_not_zero(dM))
        {
            update_mass(stardata,star,-dM);

#ifdef NUCSYN
            /*
             * We need to count the material lost because of overpsin
             * through a decretion disk.
             */
            Boolean allocated;
            double * Xej;
            nucsyn_remove_dm_from_surface(stardata,star,dM,&Xej,&allocated);
            calc_yields(stardata,
                        star,
                        dM,
                        Xej,
                        0.0,
                        NULL,
                        star->starnum,
                        FALSE,
                        SOURCE_DECRETION);
            if(allocated == TRUE)
            {
                Safe_free(Xej);
            }
#endif // NUCSYN
        }

        star->overspin = TRUE;
        Safe_free(odata);

        Dprint("overspin handler: there was overspin, lose mass dM %g, post J %g\n",
               dM,
               star->angular_momentum);
    }


    return NULL;
}

static void _compute_overspin_corrections(struct stardata_t * const stardata,
                                          struct star_t * const star,
                                          struct _overspin_data_t * const odata)
{
    const double jcrit = breakup_angular_momentum(stardata,
                                                  star);

    double jstar =  star->angular_momentum
#ifndef OVERSPIN_EVENTS
        + Jdot_net(star)*stardata->model.dt
#endif
        ;
    jstar = Max(MINIMUM_STELLAR_ANGMOM,
                jstar);

    const double j_excess = jstar - jcrit;
    const double fm = 0.02; // max. fraction of stellar mass to remove in any timetep

    if(stardata->preferences->overspin_algorithm == OVERSPIN_BSE)
    {
        /*
         * BSE algorithm returns excess angular momentum to the orbit
         *
         * This is equivalent to saying the material forms a disc
         * around the accreting star, and this disc is tidally coupled
         * to the stars, hence can "give back" its angular momentum.
         *
         * It also assumes no mass is lost from the disc.
         *
         */
        if(System_is_binary)
        {
            const double djdt = j_excess/stardata->model.dt;
            Dprint("djdt =  j_excess=%g / dt=%g = %g\n",
                   j_excess,
                   stardata->model.dt,
                   djdt);
            odata->dJorb += djdt;
            odata->dJstar_tides -= djdt;
            Dprint("BSE (binary) : dJorb/dt = %g, dJstar_tides/dt = %g\n",
                   odata->dJorb,
                   odata->dJstar_tides);
        }
        else
        {
            /*
             * However, in the case that there is no companion,
             * the above is impossible. A disc MUST form and be
             * ejected.
             */
            const double r = star->radius > star->roche_radius ? star->effective_radius : star->radius;
            const double l_kep = Pow2(r) * star->omega_crit;
            const double m_eject_required = Min(fm*star->mass,j_excess / l_kep);
            odata->dJstar_decretion -= j_excess/stardata->model.dt;
            odata->dMstar_decretion -= m_eject_required/stardata->model.dt;
            Dprint("BSE decretion (single) : dJ/dt = %g, dM/dt = %g (mass to eject %g, dt %g)\n",
                   odata->dJstar_decretion,
                   odata->dMstar_decretion,
                   m_eject_required,
                   stardata->model.dt);
        }
    }
    else if(stardata->preferences->overspin_algorithm == OVERSPIN_MASSLOSS)
    {
        /*
         * Remove excess angular momentum by mass loss,
         * e.g. a decretion disc
         */
        if(stardata->preferences->rotationally_enhanced_mass_loss==ROTATIONALLY_ENHANCED_MASSLOSS_ANGMOM ||
           stardata->preferences->rotationally_enhanced_mass_loss==ROTATIONALLY_ENHANCED_MASSLOSS_LANGER_FORMULA_AND_ANGMOM)
        {
            /*
             * Calculate specific angular momentum of accreted material:
             * assume keplerian disk
             **** Update me : use accreted specific angular momentum!
             */
            const double r = star->radius > star->roche_radius ? star->effective_radius : star->radius;
            const double l_kep = Pow2(r) * star->omega_crit;

            /* calculate mass to be ejected */
            const double m_eject_required = j_excess / l_kep ;

            Dprint("Star %d : J excess = %g : r = %g, l_kep = %g,  must eject %g Msun\n",
                   star->starnum,
                   j_excess,
                   r,
                   l_kep,
                   m_eject_required);

            /*
             * Limit m_eject to the fm of the stellar mass
             * in any one timestep. In an ideal time-evolution
             * scheme, this would not be required. In theory we
             * could reject the stellar evolution step and redo
             * with a shorter timestep, but is this really necessary?
             */
            const double m_eject = Min(m_eject_required, fm*star->mass);

            Dprint("m_eject %g\n",m_eject);

            /*
             * Eject material in a decretion disc.
             *
             * Reduce the star's angular momentum by dj_eject.
             * This is only < dj if the 2% envelope limit is reached.
             */
            const double dj_eject = j_excess * m_eject / m_eject_required;

            Dprint("Actually ejecting %g Msun, with dJ = %g\n",
                   m_eject,dj_eject);

            /* rates */
            const double mdot = -  m_eject / stardata->model.dt;
            const double jdot = - dj_eject / stardata->model.dt;

            /* remove mass and angular momentum from the star */
            odata->dMstar_decretion += mdot;
            odata->dJstar_decretion += jdot;

            if(System_is_binary)
            {
                /*
                 * Use gamma just as we would with any other
                 * non-conservative mass loss
                 */
                const double gamma = non_conservative_gamma(stardata,
                                                            Other_star_struct(star),
                                                            star);
                const double Lorb = Angular_momentum_from_stardata / Total_mass;
                const double jdot1 = mdot * Lorb * gamma;

                Dprint("Remove Jorb at rate = %g\n",jdot1);

                /*
                 * This is "the same" based on Carlo's wind/angmom mass loss
                 * for only one star. Gives similar, but not identical, results.
                 * I do not (yet) know why.
                 */
                /*
                  double mtot = system_gravitational_mass(stardata);
                  double Jorb = Pow2(stardata->common.orbit.separation)*
                  stardata->common.orbit.angular_frequency*
                  Sqrt_eccfac*stardata->star[0].mass*stardata->star[1].mass/mtot;
                  double jdot2 =
                  Jorb * 1.0/(mtot * stardata->star[0].mass * stardata->star[1].mass)
                  * ( mdot * Mass_squared(1));

                  Dprint("jdot 1 = %g, 2 = %g\n",jdot1,jdot2);
                */

                odata->dJorb_noncon = jdot1;
                odata->dJorb = jdot1;
            }
            Dprint("Star %d rotates > breakup : j_accrete=%g j_excess=%g l_kep=%g m_eject=%g new J/Jcrit = %g : mdot noncon %g\n",
                   star->starnum,
                   jdot,
                   j_excess,
                   l_kep,
                   m_eject,
                   (star->angular_momentum - dj_eject)/breakup_angular_momentum(stardata,star),
                   odata->dJorb_noncon
                );
        }
    }
}


static Boolean _star_is_overspinning(struct stardata_t * const stardata,
                                     struct star_t * const star)
{
    const double jcrit = breakup_angular_momentum(stardata,
                                                  star);

    /*
     * Calculate the angular momentum the star would
     * like to have if all the previously calculated angular
     * momentum derivatives were applied.
     */
    double jstar = star->angular_momentum
#ifndef OVERSPIN_EVENTS
        + Jdot_net(star)*stardata->model.dt
#endif
        ;
    jstar = Max(MINIMUM_STELLAR_ANGMOM,
                jstar);
    /*
     * Detect overspin
     */
    return jstar > jcrit ? TRUE : FALSE;
}
