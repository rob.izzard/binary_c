#include "../binary_c.h"
No_empty_translation_unit_warning;


void angular_momentum_and_eccentricity_derivatives(
    struct stardata_t * Restrict const stardata,
    const Boolean RLOF_boolean)
{
    Star_number k;
    const Boolean system_is_binary = System_is_binary;
    /*
     * Calculate angular momentum and eccentricity derivatives
     * arising from the stars, the orbit and other orbiting
     * objects.
     *
     * 1) Orbital : Wind loss, circumbinary discs
     * 2) Stellar : a) Wind mass loss and gain
     *    Stellar : b) Magnetic braking
     *    Both    : c) artificial
     * 3) Both    : RLOF mass transfer
     *              novae
     * 4) Both    : Tidal interactions
     * 5) Orbital : non-conservative RLOF
     * 6) Orbital : Gravitational radiation
     * 7) Both    : wind-fed circumbinary discs
     * 8) All     : orbiting objects
     * 9) Orbital : Artificial
     * 10) Both   : Prevent breakup (overspin) of the stars
     *
     * The RLOF-specific parts are activated if RLOF_boolean==TRUE
     */

    /*
     * First, zero the angular momentum and eccentricity derivatives,
     * and assume a long timescale for tides
     */

    Zero_orbit_and_system_derivatives(stardata);
    Number_of_stars_Starloop(k)
    {
        SETstar(k);
        Zero_stellar_angmom_derivatives(star);
    }
    stardata->common.orbit.tcirc = LONG_TIDAL_TIME;

    if(system_is_binary == TRUE)
    {
        /*
         * Eccentricity changes because of wind loss/gain
         */
        stardata->model.derivative[DERIVATIVE_ORBIT_ECCENTRICITY_WINDS] =
            -wind_decc_dt(stardata);

        /*
         * Orbital angular momentum changes because of wind loss
         */
        stardata->model.derivative[DERIVATIVE_ORBIT_ANGMOM_WIND_LOSS] +=
            wind_orbital_angular_momentum_derivative(
                stardata,
                &stardata->common.orbit,
                (const double[]){stardata->star[0].mass,
                    stardata->star[1].mass},
                (const double[]){stardata->star[0].derivative[DERIVATIVE_STELLAR_MASS_WIND_LOSS],
                    stardata->star[1].derivative[DERIVATIVE_STELLAR_MASS_WIND_LOSS]},
                (const double[]){stardata->star[0].derivative[DERIVATIVE_STELLAR_MASS_WIND_GAIN],
                    stardata->star[1].derivative[DERIVATIVE_STELLAR_MASS_WIND_GAIN]},
                (const double[]){stardata->star[0].vwind,
                    stardata->star[1].vwind}
                );

#ifdef DISCS
        /*
         * Circumbinary disc(s)
         */
        if(stardata->common.ndiscs)
        {
            int i;
            Discdebug(2,
                      "derivs for %d discs (M=%g)\n",
                      stardata->common.ndiscs,
                      stardata->common.ndiscs?stardata->common.discs[0].M:0.0
                );
            for(i=0;i<stardata->common.ndiscs;i++)
            {
                /*
                 * Tides remove angular momentum from the binary
                 * and put it in the disc
                 *
                 * Factor of YEAR_LENGTH_IN_SECONDS / ANGULAR_MOMENTUM_CGS converts
                 * to code units.
                 *
                 * Note: what's stored are the total changes in the time
                 *       the disc has existed during binary_c's timestep,
                 *       which is disc->dT. Thus we must scale for the
                 *       complete binary_c timestep, which is where the
                 *       factor ftdisc comes in. If the disc exists for the
                 *       whole binary_c timestep, ftdisc == 1.
                 *
                 */
                struct disc_t * const disc = & stardata->common.discs[i];
                if(disc->dT > TINY &&
                   stardata->model.dt > TINY)
                {
                    const double ftdisc = disc->dT / (stardata->model.dt * YEAR_LENGTH_IN_SECONDS);

                    /*
                     * note that jdot_disc, the change in the orbital
                     * angular momentum due to the disc, is negative
                     */
                    const double jdot_disc = disc->Jdot_binary * ftdisc *
                        YEAR_LENGTH_IN_SECONDS / ANGULAR_MOMENTUM_CGS;

                    stardata->model.derivative[DERIVATIVE_ORBIT_ANGMOM_CBDISC] += jdot_disc;
                    Dprint("Disc deriv orbit += %g -> %g\n",
                           jdot_disc,
                           stardata->model.derivative[DERIVATIVE_ORBIT_ANGMOM_CBDISC]);

                    /*
                     * Decide how to split the mass
                     * (and angmom?) between the two stars
                     *
                     * Note that f is the fraction of the accretion rate from
                     * the disc that falls onto star 0 (the "primary") given
                     * q = M2/M1.
                     *
                     * If f < -0.5, set the accretion rate to zero.
                     */
                    const double f = disc_inner_edge_accretion_f(stardata);

                    if(f>-0.5)
                    {
                        double mdot = disc->Mdot_binary * ftdisc *
                            YEAR_LENGTH_IN_SECONDS / M_SUN;
#ifdef ___MOREDEBUGGING
                        if(0)printf("DMBIN deriv mdot %g Msun/y (ftdisc = %g/%g = %g, disc->M=%g) : DM=%g : f = %g : 1-f = %g\n",
                                    mdot,
                                    disc->dT/YEAR_LENGTH_IN_SECONDS,
                                    stardata->model.dt * YEAR_LENGTH_IN_SECONDS,
                                    ftdisc,
                                    disc->M/M_SUN,
                                    mdot * stardata->model.dt,
                                    f,
                                    1.0-f
                            );
#endif// ___MOREDEBUGGING
                        stardata->star[0].derivative[DERIVATIVE_STELLAR_MASS_CBDISC_GAIN] =
                            f * mdot;
                        stardata->star[1].derivative[DERIVATIVE_STELLAR_MASS_CBDISC_GAIN] =
                            (1.0 - f) * mdot;
                        Dprint("Star accretion rates %g %g\n",
                               stardata->star[0].derivative[DERIVATIVE_STELLAR_MASS_CBDISC_GAIN],
                               stardata->star[1].derivative[DERIVATIVE_STELLAR_MASS_CBDISC_GAIN]);

                        /*
                         * Stellar spin up: treat this just as we would RLOF
                         * (see RLOF_stellar_angmom_derivative for details)
                         */
                        Starloop(k)
                        {
                            SETstar(k);
                            const double r_acc = accretion_radius(stardata,star);

                            const double djdt_crit =
                                star->omega_crit_sph * // either omega_crit or omega_crit_sph
                                star->derivative[DERIVATIVE_STELLAR_MASS_CBDISC_GAIN] * Pow2(r_acc);

                            star->derivative[DERIVATIVE_STELLAR_ANGMOM_CBDISC_GAIN] = djdt_crit;
                            Dprint("Set stellar spin %d rate dJ/dt from discs %g\n",k,djdt_crit);
                        }
                    }
                    else
                    {
                        stardata->star[0].derivative[DERIVATIVE_STELLAR_MASS_CBDISC_GAIN] = 0.0;
                        stardata->star[1].derivative[DERIVATIVE_STELLAR_MASS_CBDISC_GAIN] = 0.0;
                        stardata->star[0].derivative[DERIVATIVE_STELLAR_ANGMOM_CBDISC_GAIN] = 0.0;
                        stardata->star[1].derivative[DERIVATIVE_STELLAR_ANGMOM_CBDISC_GAIN] = 0.0;
                    }


                    /*
                     * Limit spin up if the angular momentum required to spin up
                     * the stars exceeds the amount of angular momentum available
                     */
                    Dprint("CF %g > TINY + %g\n",
                           stardata->star[0].derivative[DERIVATIVE_STELLAR_ANGMOM_CBDISC_GAIN] +
                           stardata->star[1].derivative[DERIVATIVE_STELLAR_ANGMOM_CBDISC_GAIN],
                           jdot_disc);

                    if(Is_not_zero(stardata->star[0].derivative[DERIVATIVE_STELLAR_ANGMOM_CBDISC_GAIN] +
                                   stardata->star[1].derivative[DERIVATIVE_STELLAR_ANGMOM_CBDISC_GAIN]))
                    {
                        /* factor flim < 1 */
                        const double flim = jdot_disc /
                            (stardata->star[0].derivative[DERIVATIVE_STELLAR_ANGMOM_CBDISC_GAIN] +
                             stardata->star[1].derivative[DERIVATIVE_STELLAR_ANGMOM_CBDISC_GAIN]);
                        Starloop(k)
                        {
                            SETstar(k);
                            star->derivative[DERIVATIVE_STELLAR_ANGMOM_CBDISC_GAIN] *= flim;
                            Dprint("Limit stellar spin %d gain rate to %g (flim = %g)\n",
                                   k,
                                   star->derivative[DERIVATIVE_STELLAR_ANGMOM_CBDISC_GAIN],
                                   flim);
                        }
                    }

                    /*
                     * Remove the spin angular momentum that goes into the stars
                     * from the orbit: this is angular momentum that would otherwise
                     * have gone into orbital angular momentum.
                     */
                    Starloop(k)
                    {
                        SETstar(k);
                        stardata->model.derivative[DERIVATIVE_ORBIT_ANGMOM_CBDISC] -=
                            star->derivative[DERIVATIVE_STELLAR_ANGMOM_CBDISC_GAIN];
                        Dprint("remove stellar spin from orbit %g\n",
                               stardata->model.derivative[DERIVATIVE_ORBIT_ANGMOM_CBDISC]);
                    }

                    /*
                     * Resonant interaction with the circumbinary
                     * disc drives binary eccentricity
                     */
                    const double edot = disc->edot_binary * ftdisc * YEAR_LENGTH_IN_SECONDS;
                    stardata->model.derivative[DERIVATIVE_ORBIT_ECCENTRICITY_CBDISC] = edot;

                    /* systematic loss from the disc */
                    stardata->model.derivative[DERIVATIVE_SYSTEM_CBDISC_MASS_LOSS] =
                        disc->Mdot_ejected
                        * YEAR_LENGTH_IN_SECONDS / M_SUN;
                    Dprint("remove in cbdisc %g\n",
                           stardata->model.derivative[DERIVATIVE_SYSTEM_CBDISC_MASS_LOSS]);
                }
            } // loop over discs
        }
#endif // DISCS
    } // system_is_binary

    /*
     * Stellar angular momentum loss from winds, including
     * magnetic braking, and tides.
     */
    Foreach_evolving_star(star)
    {
        /* stellar spin changes from wind loss/gain */
        Dprint("Call wind star %d\n",star->starnum);

        stellar_wind_angmom(stardata,RLOF_boolean,star->starnum);

        /* spin down by magnetic braking */
        magnetic_braking(stardata,star);

#ifdef OMEGA_CORE
        /* couple core and envelope */
        Dprint("call core coupling %d\n",star->starnum);
        core_envelope_coupling(stardata,star);
#endif

        Dprint("ANGMOM%d %d jdot=%30.22e jspin=%30.22e dj=%30.22e dt=%30.22e\n",
               star->starnum,
               star->stellar_type,
               star->derivative[DERIVATIVE_STELLAR_ANGMOM],
               star->angular_momentum,
               star->derivative[DERIVATIVE_STELLAR_ANGMOM] * stardata->model.dt,
               stardata->model.dt
            );
    }

    /*
     * Artificial rates
     */
    artificial_angular_momentum_accretion_rates(stardata);

    if(system_is_binary == TRUE)
    {
        /*
         * Orbital angular momentum loss because of
         * nova explosions. Only activate if nova mass
         * derivative is < 0.0.
         *
         * Algorithm is in Shara+ 1986 Eq.12
         *
         * The nova_beta and nova_faml parameters are set
         * in limit_accretion_rates.c.
         */
        Foreach_star(star)
        {
            if(Is_not_zero(star->derivative[DERIVATIVE_STELLAR_MASS_NOVA]))
            {
                nova_angular_momentum_changes(stardata,
                                              star,
                                              star->derivative[DERIVATIVE_STELLAR_MASS_NOVA],
                                              &star->derivative[DERIVATIVE_STELLAR_ANGMOM_NOVA],
                                              &stardata->model.derivative[DERIVATIVE_ORBIT_ANGMOM_NOVA]);
            }
        }

        /*
         * Calculate stellar spin up/down because of accretion
         * and/or loss of mass during RLOF
         */
        if(RLOF_boolean)
        {
            RLOF_stellar_angmom_derivative(stardata);

            /*
             * Eccentricity changes because of RLOF
             */
            RLOF_eccentricity_derivative(stardata);
        }

        Foreach_evolving_star(star,companion)
        {
            /* tidal spin up/down and circularization */
            tides(stardata,
                  RLOF_boolean,
                  RLOF_boolean == TRUE ? star->effective_radius : star->radius,
                  star,
                  companion);
        }

        /* orbital angular momentum losses because of RLOF transfer */
        non_conservative_angular_momentum_loss(stardata,RLOF_boolean);

        /*
         * For very close systems include orbital angular
         * momentum loss mechanisms e.g. gravitational radiation.
         */
        angular_momentum_loss_mechanisms_for_close_systems(stardata);

        /*
         * Circumbinary discs from winds
         */
#ifdef DISCS_CIRCUMBINARY_FROM_WIND
        disc_stellar_wind_to_cbdisc(stardata);
#endif // DISCS_CIRCUMBINARY_FROM_WIND

    }

#ifdef ORBITING_OBJECTS
    /*
     * Orbiting objects
     */
    orbiting_objects_derivatives(stardata);
#endif // ORBITING_OBJECTS

    stardata->common.orbit.couple_tides =
        System_is_binary &&
        stardata->star[0].radius / stardata->star[0].roche_radius > 0.5 &&
        stardata->star[1].radius / stardata->star[1].roche_radius > 0.5;

    if(0 && stardata->common.orbit.couple_tides == TRUE)
    {
        printf("At model %d time %g couple tides? %s\n",
               stardata->model.model_number,
               stardata->model.time,
               Yesno(stardata->common.orbit.couple_tides));

        /*
         * Stellar spins are coupled to the orbit
         */
        Foreach_star(star)
        {
            star->derivative[DERIVATIVE_STELLAR_ANGMOM_TIDES] = 0.0;
            star->derivative[DERIVATIVE_STELLAR_ANGULAR_VELOCITY_TIDES] = 0.0;
            star->derivative[DERIVATIVE_STELLAR_ECCENTRICITY] = 0.0;
        }
        stardata->model.derivative[DERIVATIVE_ORBIT_ANGMOM_TIDES] = 0.0;
        stardata->model.derivative[DERIVATIVE_ORBIT_ECCENTRICITY_TIDES] = 0.0;
    }

    /*
     * Summed derivatives
     */
    stardata->model.derivative[DERIVATIVE_ORBIT_ANGMOM] =
        Jdot_orbit(stardata)
        +
        stardata->model.derivative[DERIVATIVE_ORBIT_ANGMOM_ARTIFICIAL];

    stardata->model.derivative[DERIVATIVE_ORBIT_ECCENTRICITY] =
        edot_orbit(stardata);

    Dprint("ANGMOM deriv %g\n", stardata->model.derivative[DERIVATIVE_ORBIT_ANGMOM]);

    /*
     * Calculate rotation variables and ensure that the stars
     * do not spin up beyond break-up, and
     * transfer the excess angular momentum back to the orbit.
     */
    calculate_spins(stardata);

    Dprint("ANGMOM deriv %g\n", stardata->model.derivative[DERIVATIVE_ORBIT_ANGMOM]);

    Evolving_Starloop(k)
    {
        SETstar(k);

        const double effective_radius =
            star->radius > star->roche_radius ? star->effective_radius : star->radius;
        calculate_rotation_variables(star,
                                     effective_radius);
        prevent_overspin(stardata,
                         star);
    }
    Dprint("ANGMOM deriv %g\n", stardata->model.derivative[DERIVATIVE_ORBIT_ANGMOM]);
}
