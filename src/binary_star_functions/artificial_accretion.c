#include "../binary_c.h"
No_empty_translation_unit_warning;

/*
 * Return TRUE if we have aritificial accretion
 */
Boolean artificial_accretion(struct stardata_t * const stardata)
{
    return Boolean_(
        /* start time must be > 0 to be valid */
        More_or_equal(stardata->preferences->artificial_accretion_start_time, 0.0)
        &&
        More_or_equal(stardata->model.time,
                      stardata->preferences->artificial_accretion_start_time)
        &&
        /* if end time < 0 we ignore it and accrete forever */
        (
            stardata->preferences->artificial_accretion_end_time < 0.0 ||
            stardata->model.time < stardata->preferences->artificial_accretion_end_time
            )
        );
}
