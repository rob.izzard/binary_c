#pragma once
#ifndef BINARY_C_DONOR_TYPES_H
#define BINARY_C_DONOR_TYPES_H

/*
 * Map stellar types to donor types, e.g.
 * hydrogen-rich, helium-rich, ...
 */
#define HYDROGEN_DONOR (donor->stellar_type <=TPAGB)
#define HELIUM_DONOR (donor->stellar_type>=HeMS && donor->stellar_type<=HeWD)
#define CARBON_DONOR (donor->stellar_type==COWD)
#define OXYGEN_DONOR (donor->stellar_type==ONeWD)
#define NEUTRON_DONOR (donor->stellar_type==NEUTRON_STAR)
#define LARGE_RATE (1e20)


#endif // BINARY_C_DONOR_TYPES_H
