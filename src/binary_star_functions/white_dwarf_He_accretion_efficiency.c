#include "../binary_c.h"
No_empty_translation_unit_warning;


double Pure_function white_dwarf_He_accretion_efficiency(struct stardata_t * Restrict const stardata,
                                                         struct star_t * Restrict const donor Maybe_unused,
                                                         struct star_t * Restrict const accretor,
                                                         const double transfer_rate)
{
    /*
     * We want to calculate the efficiency of helium accretion
     * onto a CO white dwarf surface (see Kato and Hachisu 2004)
     *
     * This is etaHe from Claeys et al 2014 (C14), Eq. B5
     *
     * Fits are from Kato and Hachisu 2004 (ApJ 613:L129)
     *
     * RGI update: use interpolate() for the interpolation
     */
    double efficiency;

    if(accretor->stellar_type == HeWD)
    {
        /* helium white dwarfs do not burn */
        efficiency = 0.0;
        Dprint("helium WDs do not burn helium\n");
    }
    else
    {
        double lograte = log10(transfer_rate);

        /* C14: Equations B.6, B.7 and B.8 */
        const double mdot_Up = Max(0.0, 7.2e-6 * (accretor->mass - 0.6));
        const double logmdot_cr_He = -5.8;
        const double logmdot_low = -7.4;

        /*
         * At very low rate : no accretion
         * C14 Eq B.5 line 4
         */
        if(lograte < logmdot_low)
        {
            efficiency = 0.0;
            Dprint("lograte = %g < logmdot_low = %g -> efficiency = %g\n",
                   lograte,
                   logmdot_low,
                   efficiency);
        }

        /*
         * Rate is intermediate between Mdot_low and Mdot_cr_He
         * Use KH04 fits, C14 Eq B.5 line 3
         */
        else if(lograte < logmdot_cr_He)
        {
            double data[] = {
                0.7, 1.0,
                0.8,  ((lograte>-6.34) ? 1.0 : (-0.35*Pow2(lograte +6.1) +1.02)),
                0.9,  ((lograte>-6.05) ? 1.0 : (-0.35*Pow2(lograte +5.6) +1.07)),
                1.0,  ((lograte>-5.93) ? 1.0 : (-0.35*Pow2(lograte +5.6) +1.01)),
                1.1,  ((lograte>-5.76) ? 1.0 : ((lograte>-5.95) ? (-0.54*Pow2(lograte +5.6) +1.01) : (0.54*lograte +4.16))),
                1.2,  ((lograte>-5.76) ? 1.0 : ((lograte>-5.95) ? (-0.54*Pow2(lograte +5.6) +1.01) : (0.54*lograte +4.16))),
                1.3,  ((lograte>-5.83) ? 1.0 : (-0.175*Pow2(lograte +5.35) +1.03)),
                1.35, ((lograte>-6.05) ? 1.0 : (-0.115*Pow2(lograte +5.7) +1.01))
            };
            int i;
            for(i=0;i<8;i++)
            {
                Clamp(data[i*2+1],0.0,1.0);
            }

            struct data_table_t * table = NULL;
            NewDataTable_from_Pointer(data,
                                      table,
                                      1,
                                      1,
                                      8);

            double x[1] = { accretor->mass };
            double r[1];

            /* interpolate the rate */
            Interpolate(table,x,r,FALSE);

            efficiency = r[0];
        }
        else
        {
            /*
             * High rate regime C14 Eq. B.5 lines one and two
             */
            efficiency = (transfer_rate < mdot_Up) ? 1.0 : mdot_Up / transfer_rate;
            Dprint("high rate regime : efficiency = %g\n",efficiency);
        }
    }

    /* must be in the range 0 .. 1 */
    Clamp(efficiency,0.0,1.0);

    return efficiency;
}
