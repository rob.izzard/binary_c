#include "../binary_c.h"
No_empty_translation_unit_warning;

/*
 * Function to determine whether we should use
 * gravitational-wave radiation.
 */

Boolean Pure_function use_gravitational_wave_radiation(struct stardata_t * const stardata)
{
    if(stardata->preferences->gravitational_radiation_model == GRAVITATIONAL_RADIATION_NONE
       ||
       System_is_binary == FALSE
       ||
       Is_zero(stardata->preferences->gravitational_radiation_modulator_J)
       ||
       (stardata->preferences->gravitational_radiation_model==GRAVITATIONAL_RADIATION_BSE_WHEN_NO_RLOF &&
        RLOF_overflowing(stardata,RLOF_ENTRY_THRESHOLD)))
    {
        return FALSE;
    }
    else
    {
        return TRUE;
    }
}
