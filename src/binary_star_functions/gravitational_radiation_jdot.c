#include "../binary_c.h"
No_empty_translation_unit_warning;



/*
 * Rate of orbital angular momentum loss from gravitational
 * radiation.
 * Rate returned is negative.
 *
 * Formulae from Eggleton, Landau and Lifshitz 1951
 */
double Pure_function gravitational_radiation_jdot(struct stardata_t * Restrict const stardata)
{
    double jdot;

    if(use_gravitational_wave_radiation(stardata) == TRUE)
    {
        if(stardata->preferences->gravitational_radiation_model==GRAVITATIONAL_RADIATION_BSE
            ||
            stardata->preferences->gravitational_radiation_model==GRAVITATIONAL_RADIATION_BSE_WHEN_NO_RLOF)
        {
            /*
             * Note: the RLOF check is done in use_gravitational_wave_radiation()
             */

            /*
             * Eggleton's weak field approximation
             */
            const double ecc2 = ECC_SQUARED;
            jdot = -8.315e-10 *
                MASS_PRODUCT*Total_mass/(Pow2(Separation_squared))*
                stardata->common.orbit.angular_momentum*
                (1.0 + 7.0/8.0 * ecc2)/Pow5( sqrt(1.0 - ecc2) );

            /*

             * Landau Lifschitz rate in cgs, almost identical to Eggleton's
             *
             * See also Peters 1964 Physical Review 136, 4B Eq. 5.5
             *
             */
            /*
            double LL =
                -32.5/5.0 * pow(GRAVITATIONAL_CONSTANT,7.0/2.0) *
                Pow2(stardata->star[0].mass * M_SUN) *
                Pow2(stardata->star[1].mass * M_SUN) *
                + sqrt(M_SUN*(stardata->star[0].mass + stardata->star[1].mass)) /
                (
                Pow5(SPEED_OF_LIGHT) *
                pow(stardata->common.orbit.separation * R_SUN, 7.0/2.0) *
                Pow2(1.0 - ecc2)
                )
                *
                (1.0 + 7.0/8.0 * Pow2(stardata->common.orbit.eccentricity));


            // convert to code units per year
            LL = Angular_momentum_derivative_cgs_to_code(LL);

            */
        }
        else if(
            stardata->preferences->gravitational_radiation_model==GRAVITATIONAL_RADIATION_LANDAU_LIFSHITZ
            ||
            (stardata->preferences->gravitational_radiation_model==GRAVITATIONAL_RADIATION_LANDAU_LIFSHITZ_WHEN_NO_RLOF &&
             !RLOF_overflowing(stardata,RLOF_ENTRY_THRESHOLD))
            )
        {
            /*
             * Landau and Lifshitz 1951 formula
             */
            jdot = (-32.0/2.0 * Pow3(GRAVITATIONAL_CONSTANT)/Pow5(SPEED_OF_LIGHT)*
                    stardata->star[0].mass * M_SUN *
                    stardata->star[1].mass * M_SUN *
                    (stardata->star[0].mass + stardata->star[1].mass) * M_SUN /
                    Pow4(stardata->common.orbit.separation * R_SUN))
                * stardata->common.orbit.angular_momentum * YEAR_LENGTH_IN_SECONDS;
        }
        else
        {
            jdot = 0.0;
        }
    }
    else
    {
        jdot = 0.0;
    }
    jdot *= stardata->preferences->gravitational_radiation_modulator_J;
    Dprint("JJJ mod %g -> %g\n",
           stardata->preferences->gravitational_radiation_modulator_J,
           jdot);
    return jdot;
}
