#include "../binary_c.h"
No_empty_translation_unit_warning;


double Constant_function rL3(const double q
#ifdef RLOF_RADIATION_CORRECTION
                             ,const double f
#endif
    )
{
    /*
     * A function to evaluate r_L3 = R_L3/a(Q), were Q=M1/M2,
     * from Pennington R., 1985, in Pringle J. E., Wade R. A.,
     * eds, Interacting Binary Stars. pp 197–199
     *
     * Note: this is for a circular binary system (e=0).
     *
     * M1 is the donor,
     * M2 is the accretor
     */
    double r;
    if(q < TINY)
    {
        r = 1e100;
    }
    else
    {
        const double x = q / (1.0 + q);
        r = rL1(q
#ifdef RLOF_RADIATION_CORRECTION
                ,f
#endif // RLOF_RADIATION_CORRECTION
            ) +
            (0.179 + 0.01 * x - 0.025 * (q - 1.0) / q) *
            pow(x,0.625);
    }
    return r;
}
