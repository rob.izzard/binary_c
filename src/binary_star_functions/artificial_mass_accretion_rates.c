#include "../binary_c.h"
#include "../binary_c.h"
No_empty_translation_unit_warning;

/*
 * Artificial mass accretion, e.g. from the ISM
 */

void artificial_mass_accretion_rates(struct stardata_t * const stardata)
{
    if(artificial_accretion(stardata))
    {
        Foreach_star(star)
        {
            star->derivative[DERIVATIVE_STELLAR_MASS_ARTIFICIAL] =
                Max(
                    /*
                     * Accrete at a fixed rate independent of stellar type
                     */
                    stardata->preferences->artificial_mass_accretion_rate[star->starnum],

                    /*
                     * Accrete at a fixed rate depending on stellar type
                     */
                    stardata->preferences->artificial_mass_accretion_rate_by_stellar_type[star->stellar_type]);
        }
    }
}
