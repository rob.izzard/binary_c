#include "../binary_c.h"
No_empty_translation_unit_warning;

/*
 * Yield source ID function for wind-type sources
 */

Yield_source id_wind_source(const struct star_t * const star,
                            const struct stardata_t * const stardata Maybe_unused)
{
    Yield_source source;
    if(star->TZO == TRUE)
    {
        source = SOURCE_THORNE_ZYTKOW;
    }
    else if(AGB(star->stellar_type))
    {
#if defined BSE && !defined MINT
        source = ONe_core(stardata,star) ? SOURCE_SAGB : SOURCE_AGB;
#endif // BSE
#ifdef MINT
        // TODO : ID ONe core
        source = SOURCE_AGB;
#endif // MINT
    }
    else if(star->stellar_type==GIANT_BRANCH)
    {
        source = SOURCE_GB;
    }
    else if(star->stellar_type < HeWD && WR_mu(star)<1.0)
    {
        source = SOURCE_WR;
    }
    else
    {
        source = SOURCE_WIND;
    }
    return source;
}
