#include "../binary_c.h"
No_empty_translation_unit_warning;


/*
 * Rate of orbital angular momentum circularization from gravitational
 * radiation.
 * Rate returned is negative.
 */
double Pure_function gravitational_radiation_edot(struct stardata_t * Restrict const stardata)
{
    double edot;

    if(stardata->preferences->gravitational_radiation_model!=
       GRAVITATIONAL_RADIATION_NONE
       &&
       System_is_binary
        )
    {
        if(
            stardata->preferences->gravitational_radiation_model==GRAVITATIONAL_RADIATION_BSE
            ||
            (stardata->preferences->gravitational_radiation_model==GRAVITATIONAL_RADIATION_BSE_WHEN_NO_RLOF &&
             !RLOF_overflowing(stardata,RLOF_ENTRY_THRESHOLD))
            )
        {
            const double ecc2 = ECC_SQUARED;
            const double f1 = (19.0/6.0) + (121.0/96.0)*ecc2;

            /*
             * Eggleton's weak field formula
             */
            edot = (double) -8.315e-10*MASS_PRODUCT*Total_mass/(Separation_squared*Separation_squared)
                *stardata->common.orbit.eccentricity*f1/Pow5(sqrt(1.0-ecc2));

            /*
             * Peters 1964 Physical Review 136, 4B Eq. 5.7
             */
            /*
            // Peters version in cgs
            double Peters = -304.0/15.0 *
                stardata->common.orbit.eccentricity *
                Pow3(GRAVITATIONAL_CONSTANT) *
                stardata->star[0].mass * M_SUN *
                stardata->star[1].mass * M_SUN *
                (system_gravitational_mass(stardata)) * M_SUN /
                (
                    Pow5(SPEED_OF_LIGHT) *
                    Pow4(stardata->common.orbit.separation * R_SUN) *
                    pow(1.0 - Pow2(stardata->common.orbit.eccentricity), 5.0/2.0)
                    ) *
                (1.0 + 121.0/304.0 * Pow2(stardata->common.orbit.eccentricity));

            // convert to per year
            Peters *= YEAR_LENGTH_IN_SECONDS;
            */
        }
        /*
         * Note that when using the Landau & Lifshitz (1951) formula
         * there is no change in eccentricity (it presumably assumes e=0)
         */
        else
        {
            edot = 0.0;
        }
    }
    else
    {
        edot = 0.0;
    }
    edot *= stardata->preferences->gravitational_radiation_modulator_e;
    return edot;
}
