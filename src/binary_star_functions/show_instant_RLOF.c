#include "../binary_c.h"
No_empty_translation_unit_warning;


#define ORBITAL_PERIOD 1
#define ORBITAL_SEPARATION 2
#define ORBITAL_BOTH 3

void show_instant_RLOF(struct stardata_t * Restrict const stardata_input)
{
    /*
     * A set of functions to show RLOF parameters on the
     * zero-age main sequence.
     */


    /*
     * Given M1, M2, return the orbital period or separation that
     * just avoids RLOF on the ZAMS
     */

    if(stardata_input->preferences->show_minimum_separation_for_instant_RLOF==TRUE ||
       stardata_input->preferences->show_minimum_orbital_period_for_instant_RLOF==TRUE)
    {
        struct stardata_t * stardata = New_stardata_from(stardata_input);

        /* check if we want separation or period */
        Star_number k;

        /*
         * Choose the separation such that R1>ROL1 and R2>ROL2.
         *
         * NB this assumes a CIRCULAR ORBIT!
         *
         * You have to correct if you want to start eccentric.
         */

        /*
         * If M1=M2=0.1 (the minimum mass) then the minimum separation
         * is 0.35days. We may as well start there (or at 0.3d) because
         * at higher mass the limit just increases.
         *
         * Set the step (dsep) to 0.01 days : accurate enough for government work!
         *
         * Note: you could set up some kind of Newton-like method to
         * zoom in more efficiently on the result. But why? This is very
         * quick and accurate enough.
         *
         * The overflow variable is true when either star overflows
         * its Roche lobe
         */
        stardata->common.orbit.separation = 0.3;
        const double dsep = 0.01;
        Boolean overflow=TRUE;

        while(overflow==TRUE)
        {
            Dprint("a=%g M1=%g R1=%g ROL1=%g : M2=%g R2=%g ROL2=%g : overflow? %d\n",
                   stardata->common.orbit.separation,
                   stardata->star[0].mass,
                   stardata->star[0].radius,
                   stardata->star[0].roche_radius,
                   stardata->star[1].mass,
                   stardata->star[1].radius,
                   stardata->star[1].roche_radius,
                   overflow);

            /* still overflowing: increase the separation */
            stardata->common.orbit.separation += dsep;

            /* recalculate the Roche radii */
            determine_roche_lobe_radii(stardata,&stardata->common.orbit);

            /* check for continuing overflow */
            overflow = FALSE;
            Starloop(k)
            {
                overflow = overflow ||
                    (stardata->star[k].radius>stardata->star[k].roche_radius);
            }
        }

        /* print the resulting period or separation */

        if(stardata->preferences->show_minimum_separation_for_instant_RLOF==TRUE)
        {
            Printf("MINIMUM SEPARATION %g\n",stardata->common.orbit.separation);
        }

        if(stardata->preferences->show_minimum_orbital_period_for_instant_RLOF==TRUE)
        {
            Printf("MINIMUM PERIOD %g\n",calculate_orbital_period(stardata));
        }

        /*
         * Free our temporary stardata
         */
        free_stardata(&stardata);

        /*
         * And restore the input stardata so memory can be freed if necessary
         */
        stardata = stardata_input;

        /* and exit */
        Exit_or_return_void(BINARY_C_SPECIAL_EXIT,
                            "Exit after showing period or separation that just avoids RLOF");
    }

    /*
     * Given M1 and orbital period, return the maximum mass ratio that
     * just avoids RLOF on the ZAMS (assuming mass ratios <1 ).
     * Designed for zero eccentricity.
     */

    if(stardata_input->preferences->show_maximum_mass_ratio_for_instant_RLOF==TRUE)
    {
        struct stardata_t * stardata = New_stardata_from(stardata_input);
        Star_number k;
#ifdef BSE
        set_metallicity_parameters(stardata);
#endif
        /*
         * Choose the mass ratio such that R1>ROL1 and R2>ROL2.
         *
         *
         */
        struct star_t * const primary = &stardata->star[0];
        struct star_t * const secondary = &stardata->star[1];
        const double M1 = primary->mass;

        /*
         * Init stars
         */
        init_star(stardata,primary);
        init_star(stardata,secondary);
        primary->mass = M1;
        primary->phase_start_mass = M1;

        /* we also require (constant) R1 */
        stellar_structure(stardata,
                          STELLAR_STRUCTURE_CALLER_show_instant_RLOF,
                          primary,
                          NULL,
                          FALSE);

        /*
         * Boolean to detect overflow
         */
        Boolean overflow = FALSE;

        /*
         * The period passed in should be in years.
         *
         * Save the original orbital period.
         */
        const double period = stardata->common.orbit.period;

        /*
         * Constant in Kepler's 3rd law: note
         * that this gives a in R_SUN
         */
        const double a0 =
            pow(GRAVITATIONAL_CONSTANT *
                primary->mass * M_SUN *
                Pow2(period*YEAR_LENGTH_IN_SECONDS) /
                (4.0 * PI * PI), 1.0/3.0) / R_SUN
            * (1.0 - stardata->common.orbit.eccentricity);

        /*
         * convert Eggleton's function to use our q
         * (our q is M2/M1, his is M1/M2) and not
         * have floating point 1/0 or 1e100.
         */
#define f(q) (rL1(Max(0.0,Min(1e10,1.0/(q)))))

        /* hence radii given a0, q and f(q) */
#define rad(q) (f(q) * a0 * pow(1.0+(q),1.0/3.0))

        /*
         * We first do a check on the bounds of R1, RL1, R2 and RL2
         * to see if mass transfer is completely avoided. If so, do
         * not seek a qmin.
         */
        const double RL1_min = rad(1.0);

        /*
         * First check if R1 > RL1_min. This is the
         * cheapest of checks, which is why we do it first.
         */
        if(primary->radius > RL1_min)
        {
            overflow = TRUE;
        }
        else
        {
            /*
             * Now check star 2, which requires a few stellar
             * structure calculations.
             */
            const double qstarmin = MAXIMUM_BROWN_DWARF_MASS / primary->mass;
            const double RL2min = rad(1.0/qstarmin);
            const double RL2max = rad(1.0);

            /*
             * at q=1
             */
            secondary->mass = primary->mass;
            secondary->phase_start_mass = secondary->mass;
            secondary->stellar_type = MAIN_SEQUENCE;
            set_no_core(secondary);
            secondary->age = 0.0;
            secondary->deny_SN++;
            stellar_structure(stardata,
                              STELLAR_STRUCTURE_CALLER_show_instant_RLOF,
                              secondary,
                              NULL);
            secondary->deny_SN--;
            const double R2max = secondary->radius;

            /* at qstarmin = MAXIMUM_BROWN_DWARF_MASS / M1 */
            secondary->mass = qstarmin * primary->mass; // should be MAXIMUM_BROWN_DWARF_MASS ~ 0.07
            secondary->phase_start_mass = secondary->mass;
            secondary->stellar_type = MAIN_SEQUENCE;
            set_no_core(secondary);
            secondary->age = 0.0;
            secondary->deny_SN++;
            stellar_structure(stardata,
                              STELLAR_STRUCTURE_CALLER_show_instant_RLOF,
                              secondary,
                              NULL);
            secondary->deny_SN--;
            const double R2min = secondary->radius;

            Dprint("R2min = %g cf RL2 min %g\n",R2min,RL2min);
            Dprint("R2max = %g cf RL2 max %g\n",R2max,RL2max);
            if(R2max > RL2max &&
               R2min > RL2min)
            {
                Dprint("OVERFLOW POSSIBLE of star 2\n");
                overflow = TRUE;
            }
        }


        if(overflow == FALSE)
        {
            Printf("NO MAXIMUM MASS RATIO < 1\n");
        }
        else
        {
            /*
             * Start on the ZAMS
             */
            secondary->stellar_type = MAIN_SEQUENCE;
            set_no_core(secondary);
            secondary->age = 0.0;
            stardata->model.intpol=0;

            /*
             * Start at the minimum stellar mass and shift q
             * upwards by something small: this is a crude
             * method, but I have two functions (R1=RL and R2=RL)
             * either of which being zero will be a root found.
             * I want to make sure I find the first root, with no
             * overshoot, so have stuck with this, but given the
             * monotone nature of the functions perhaps this can be
             * bisected instead (although also perhaps this method is
             * fast enough...?).
             */
            secondary->mass = MAXIMUM_BROWN_DWARF_MASS;
            secondary->phase_start_mass = secondary->mass;

            const double dq = 1e-4; // very small dq: I hope this is accurate enough!

            /* we require R1 */
            primary->deny_SN++;
            stellar_structure(stardata,
                              STELLAR_STRUCTURE_CALLER_show_instant_RLOF,
                              primary,
                              NULL);
            primary->deny_SN--;
            overflow = FALSE;
            while(overflow==FALSE &&
                  secondary->mass < primary->mass)
            {
                /* not yet overflowing: increase the mass */
                update_mass(stardata,secondary, dq * primary->mass);
                secondary->phase_start_mass = secondary->mass;
                secondary->stellar_type = MAIN_SEQUENCE;
                secondary->core_mass[CORE_He] = 0.0;
                secondary->age = 0.0;

                /* we need to recalculate the radius */
                secondary->deny_SN++;
                stellar_structure(stardata,
                                  STELLAR_STRUCTURE_CALLER_show_instant_RLOF,
                                  secondary,
                                  NULL);
                secondary->deny_SN--;

                /*
                 * update orbit and RL: note that the period
                 * stored internally is in years, while what's passed in is
                 * in days.
                 */
                stardata->common.orbit.period = period;
                stardata->common.orbit.separation = calculate_orbital_separation(stardata);
                stardata->common.orbit.angular_momentum = orbital_angular_momentum(stardata);
                update_orbital_variables(stardata,&stardata->common.orbit,primary,secondary);
                determine_mass_ratios(stardata);
                determine_roche_lobe_radii(stardata,&stardata->common.orbit);

                /* check for continuing overflow */
                overflow = FALSE;
                Starloop(k)
                {
                    overflow = overflow ||
                        (stardata->star[k].radius > stardata->star[k].roche_radius * (1.0 - stardata->common.orbit.eccentricity));
                }

                const double q = secondary->mass / primary->mass;
                const double Q Maybe_unused = 1.0/q; // Eggleton's 1983 mass ratio = 1/ours
                const double a Maybe_unused = a0 * pow(1+q,1.0/3.0); // R_SUN

                Dprint("M1=%g M2=%g R1=%g RL1=%g R2=%g RL2=%g : rL1(q)=%g rL1(1/q)=%g : RL1=%g RL2=%g   : a=%g =%g (ratio %g) ? P=%g J=%g q=%g overflow? %s\n",
                       primary->mass,
                       secondary->mass,
                       primary->radius,
                       primary->roche_radius,
                       secondary->radius,
                       secondary->roche_radius,

                       rL1(Q),
                       rL1(1.0/Q),

                       a*rL1(Q),
                       a*rL1(1.0/Q),

                       stardata->common.orbit.separation,
                       a,
                       stardata->common.orbit.separation/a,

                       stardata->common.orbit.period * YEAR_LENGTH_IN_DAYS,
                       stardata->common.orbit.angular_momentum,
                       secondary->mass/primary->mass,
                       Yesno(overflow));
                Dprint("PLOT %g %g %g %d\n",
                       secondary->mass/primary->mass,
                       primary->radius/primary->roche_radius,
                       secondary->radius/primary->roche_radius,
                       overflow);
            }

            /* print the resulting mass ratio */
            if(overflow == TRUE)
            {
                Printf("MAXIMUM MASS RATIO %.4f\n",secondary->mass/primary->mass);
            }
            else
            {
                Printf("NO MAXIMUM MASS RATIO < 1\n");
            }
        }

        /*
         * Free our temporary stardata
         */
        free_stardata(&stardata);

        /*
         * And restore the input stardata so memory can be freed if necessary
         */
        stardata = stardata_input;

        /* and exit */
        Exit_or_return_void(BINARY_C_SPECIAL_EXIT,
                            "Exit after showing mass ratio that just avoids RLOF");
    }
    fprintf(stderr,"show RLOF return %p %p\n",
            (void*)stardata_input,
            (void*)stardata_input->persistent_data);

}
