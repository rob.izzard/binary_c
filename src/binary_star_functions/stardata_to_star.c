#include "../binary_c.h"
No_empty_translation_unit_warning;

/*
 * Given a star_t struct containing a stardata_t,
 * fill the star_t struct appropriately.
 *
 * The star_t is not a real star: it represents
 * a stellar system.
 *
 * For example, add the masses to compute the "star"'s
 * mass: this is just stardata->star[0].mass + stardata->star[1].mass
 */


void stardata_to_star(struct star_t * const star)
{
    struct stardata_t * const stardata = star->stardata;

    /*
     * Trivial cases where one, or both, stars are massless
     * remnants.
     */
    if(stardata->star[0].stellar_type == MASSLESS_REMNANT)
    {
        if(stardata->star[1].stellar_type == MASSLESS_REMNANT)
        {
            /*
             * Both stars in stardata are massless, hence
             * star should be also
             */
            stellar_structure_make_massless_remnant(NULL, star);
            return;
        }
        else
        {
            /*
             * Star 0 is massless, star 1 is not: just copy star 0
             */
            copy_star(stardata,
                      &stardata->star[0],
                      star);
            return;
        }
    }
    else if(stardata->star[1].stellar_type == MASSLESS_REMNANT)
    {
        /*
         * Star 1 is massless, star 0 is not: just copy star 1
         */
        copy_star(stardata,
                  &stardata->star[1],
                  star);
        return;
    }

    star->mass = Sum_of_stars(mass);
    star->luminosity = Sum_of_stars(luminosity);
    star->radius = Max_of_stars(radius);

    /*
     * Some variables should represent the brightest star
     */
    struct star_t * const brightest =
        stardata->star[0].luminosity > stardata->star[1].luminosity ?
        &stardata->star[0] : &stardata->star[1];
    star->stellar_type = brightest->stellar_type;

    /*
     * Many variables we cannot represent in a merged star.
     *
     * We do our best with them!
     */


    /*
     * "Merge" the cores
     */
    merge_cores(stardata,
                &stardata->star[0],
                &stardata->star[1],
                star);
    star->core_radius = Max_of_stars(core_radius);

    /*
     * Derivatives
     */
    Zero_stellar_derivatives(star);

#define X(CODE,STRING,VAR,FUNC,GROUP,MINTCLEAR,ADDITION_METHOD)         \
    if((ADDITION_METHOD) != DERIVATIVE_ADD_NONE)                        \
    {                                                                   \
        if((ADDITION_METHOD) == DERIVATIVE_ADD_SUM)                     \
        {                                                               \
            star->derivative[DERIVATIVE_STELLAR_##CODE] =               \
                Sum_of_stars(derivative[DERIVATIVE_STELLAR_##CODE]);    \
        }                                                               \
        else if((ADDITION_METHOD) == DERIVATIVE_ADD_LUMINOSITY_WEIGHTED) \
        {                                                               \
            star->derivative[DERIVATIVE_STELLAR_##CODE] =               \
                Weighted_sum_of_stars(derivative[DERIVATIVE_STELLAR_##CODE], \
                                      luminosity);                      \
        }                                                               \
        else if((ADDITION_METHOD) == DERIVATIVE_ADD_MASS_WEIGHTED)      \
        {                                                               \
            star->derivative[DERIVATIVE_STELLAR_##CODE] =               \
                Weighted_sum_of_stars(derivative[DERIVATIVE_STELLAR_##CODE], \
                                      mass);                            \
        }                                                               \
        else if((ADDITION_METHOD) == DERIVATIVE_ADD_RADIUS_WEIGHTED)    \
        {                                                               \
            star->derivative[DERIVATIVE_STELLAR_##CODE] =               \
                Weighted_sum_of_stars(derivative[DERIVATIVE_STELLAR_##CODE], \
                                      radius);                          \
        }                                                               \
        else if((ADDITION_METHOD) == DERIVATIVE_ADD_TEFF_WEIGHTED)      \
        {                                                               \
            const double m[2] = {                                       \
                stardata->star[0].mass,                                 \
                stardata->star[1].mass                                  \
            };                                                          \
            stardata->star[0].mass =                                    \
                Teff_from_star_struct(&stardata->star[0]);              \
            stardata->star[1].mass =                                    \
                Teff_from_star_struct(&stardata->star[1]);              \
            star->derivative[DERIVATIVE_STELLAR_##CODE] =               \
                Weighted_sum_of_stars(                                  \
                    derivative[DERIVATIVE_STELLAR_##CODE],              \
                    mass                                                \
                    );                                                  \
            stardata->star[0].mass = m[0];                              \
            stardata->star[1].mass = m[1];                              \
        }                                                               \
    }

    STELLAR_DERIVATIVES_LIST;
}
