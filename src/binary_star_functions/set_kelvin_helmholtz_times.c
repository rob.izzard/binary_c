#include "../binary_c.h"
No_empty_translation_unit_warning;


void set_kelvin_helmholtz_times(struct stardata_t * Restrict const stardata)
{
    /*
     * Set Kelvin-Helmholtz times
     * (Hurley et al 2002, Eq.61)
     */
    Foreach_star(star)
    {
        star->tkh = kelvin_helmholtz_time(star);
        Dprint("t=%g star %d tkh = %g from M=%g R=%g L=%g k=%d\n",
               stardata->model.time,
               star->starnum,
               star->tkh,
               star->mass,
               star->radius,
               star->luminosity,
               star->stellar_type
            );
        Nancheck(star->tkh);
    }
}
