#include "../binary_c.h"
No_empty_translation_unit_warning;


/*
 * Chirp mass of a given star 
 * (requires star->q to be set, which it normally is)
 */ 
double chirp_mass(struct star_t * Restrict const star)
{
    return star->mass /
        (pow(star->q,
             -3.0/5.0) *
         pow(1.0+star->q,
             1.0/5.0));
}
