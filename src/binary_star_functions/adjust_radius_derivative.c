#include "../binary_c.h"
No_empty_translation_unit_warning;


void adjust_radius_derivative(struct stardata_t * Restrict const stardata)
{
    if(Is_not_zero(stardata->model.dtm))
    {
        Foreach_star(star)
        {
            Dprint("adjust_radius_derivative%d: rdot=%g roche_radius=%g rol0=%g (diff=%g)  dtm=%g -> ",
                   star->starnum,
                   star->rdot,
                   star->roche_radius,
                   star->rol0,
                   star->roche_radius-star->rol0,
                   stardata->model.dtm);

            star->rdot += (star->roche_radius - star->rol0)/stardata->model.dtm;
            star->rol0 = star->roche_radius;

            Dprint("rdot=%g m=%g mc=%g r=%g l=%g RL=%g RL0=%g dtm=%g\n",
                   star->rdot,
                   star->mass,
                   Outermost_core_mass(star),
                   star->radius,
                   star->luminosity,
                   star->roche_radius,
                   star->rol0,
                   stardata->model.dtm
                );
        }
    }
}
