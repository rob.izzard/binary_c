#include "../binary_c.h"
No_empty_translation_unit_warning;

/*
 * (approximate) gravitation radiation timescale, yr
 */

double Pure_function gravitational_radiation_timescale(const double separation,
                                                       const double M1,
                                                       const double M2)
{
    return 1e6*0.15e3*Pow4(Max(TINY,separation))/Max(TINY,(M1+M2)*M1*M2);
}
