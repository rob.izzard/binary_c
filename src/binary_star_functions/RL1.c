#include "../binary_c.h"
No_empty_translation_unit_warning;


double Constant_function rL1(const double q
#ifdef RLOF_RADIATION_CORRECTION
                             ,const double f
#endif
    )
{
    /*
     * A function to evaluate R_L1/a(Q), were Q=M1/M2, from
     * Eggleton (Astrophysical Journal 268, 368) 1983.
     *
     * Note: this is for a circular binary system (e=0).
     *
     * M1 is the donor,
     * M2 is the accretor
     */
    double rlf;
    if(q < TINY)
    {
        rlf = 1e100;
    }
    else
    {
        const double p = cbrt(q);
        const double pp = Pow2(p);

#ifdef RLOF_RADIATION_CORRECTION
        /*
         * Correction for radiation pressure : fit from Dermine Tyl's
         * pdf slides
         */
        if(f>TINY)
        {
            const double Af=0.49+
                0.08*f+
                -0.69*Pow2(f)+
                12.81*Pow3(f)+
                -75.50*Pow4(f)+
                208.14*Pow5(f)+
                -300.6*Pow6(f)+
                219.7*Pow7(f)+
                -64.27*Pow8(f);
            const double Bf=0.6*(1.0+0.5*f);
            const double Cf=1.0+f;
            rlf = Af*pp/(Bf*pp + log(1.0+Cf*p));
        }
        else
        {
            rlf = 0.49*pp/(0.6*pp + log(1.0+p));
        }
#else
        rlf = 0.49*pp/(0.6*pp + log(1.0+p));
#endif
    }
    return rlf;
}
