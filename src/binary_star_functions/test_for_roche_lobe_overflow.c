#include "../binary_c.h"
No_empty_translation_unit_warning;


/*
 * Function to check if the star has overflowed its Roche lobe, and
 * take appropriate action to interpolate back to the time where it
 * did.
 *
 * returns:
 * EVOLUTION_ROCHE_OVERFLOW_NONE  : there is no RLOF
 * EVOLUTION_ROCHE_OVERFLOW : indicates that we are just starting RLOF
 * EVOLUTION_ROCHE_OVERFLOW_AFTER_SPIRAL_IN : indicates that RLOF will start in the next timestep after some spiral in
 */

unsigned int test_for_roche_lobe_overflow(struct stardata_t * Restrict const stardata,
                                          const Boolean test_previous)
{
    unsigned int retval;
    Dprint("test for RLOF\n");

    if(!System_is_binary)
    {
        /*
         * If we're a single-star system, we cannot have RLOF
         */
        retval = EVOLUTION_ROCHE_OVERFLOW_NONE;
    }
    else
    {
        /*
         * Set model->ndonor    to the donor,
         *     model->naccretor to accretor
         */
        set_ndonor_and_naccretor(stardata);
        RLOF_stars;

        if(stardata->preferences->RLOF_interpolation_method ==
           RLOF_INTERPOLATION_BINARY_C)
        {
            /*
             * Test for RLOF on the previous timestep:
             * if we find it, overshoot by one model
             * albeit with (perhaps) zero RLOF
             */
            if(test_previous == TRUE &&
               stardata->previous_stardata &&
               stardata!=stardata->previous_stardata &&
               stardata->common.RLOF_do_overshoot == TRUE)
            {
                Dprint("call test for RLOF from test for RLOF\n");
                retval = test_for_roche_lobe_overflow(stardata->previous_stardata,
                                                      TEST_RLOF_ONLY_THIS_TIMESTEP);
                Dprint("double call ret %u\n",
                       retval);
            }
            else
            {
                /*
                 * Check for one star overflowing
                 */
                Dprint("intermediate? %s, in_RLOF = %s, overflowing? %s [R0/RL0 = %g, R1/RL1 = %g] spiral time %g vs mindt %g\n",
                       Yesno(stardata->model.intermediate_step),
                       Yesno(stardata->model.in_RLOF),
                       Yesno(RLOF_overflowing(stardata,RLOF_ENTRY_THRESHOLD)),
                       stardata->star[0].radius / stardata->star[0].roche_radius,
                       stardata->star[1].radius / stardata->star[1].roche_radius,
                       spiral_in_time(stardata),
                       stardata->preferences->minimum_timestep);

                if(spiral_in_time(stardata) < stardata->preferences->minimum_timestep)
                {
                    /*
                     * special case : compact object RLOF, which isn't time
                     * resolved, should occur in this timestep. Catch it
                     * now before contact happens.
                     *
                     * The donor should be "less evolved" than the accretor,
                     * if not swap the stars. Note: this leads to starnum
                     * swapping problems. Don't do this now! The latest
                     * merge_stars function should handle this without problems.
                     */
                    if(0 &&
                       COMPACT_OBJECT(donor->stellar_type)&&
                       COMPACT_OBJECT(accretor->stellar_type)&&
                       donor->stellar_type > accretor->stellar_type)
                    {
                        Dprint("swap stars donor=%d accretor=%d\n",
                               donor->starnum,
                               accretor->starnum
                            );
                        Swap_stars(donor,accretor);
                        Dprint("swapped donor=%d accretor=%d\n",
                               donor->starnum,
                               accretor->starnum);
                    }

                    retval = EVOLUTION_ROCHE_OVERFLOW_AFTER_SPIRAL_IN;
                    Dprint("RLOF spiral in time %g : min dt %g\n",
                           spiral_in_time(stardata),
                           stardata->preferences->minimum_timestep);
                }
                else if(RLOF_overflowing(stardata,1.0))
                {
                    /*
                     * We are overflowing a Roche lobe
                     */
                    retval = EVOLUTION_ROCHE_OVERFLOW;
                }
#ifdef ADAPTIVE_RLOF2
                else if(stardata->model.adapting_RLOF2 == TRUE)
                {
                    /*
                     * We're adapting the RLOF rate
                     */
                    retval = EVOLUTION_ROCHE_OVERFLOW;
                }
#endif
                else
                {
                    /* no RLOF */
                    retval = EVOLUTION_ROCHE_OVERFLOW_NONE;
                }
            }
            Dprint("RLOF check t = %20.12e J = %20.12e, a %20.12e : 0-> %d M=%g Mc=%g R=%g RL=%g : 1-> %d M=%g Mc=%g R=%g RL=%g : q %g %g : rlf %g %g -> %s %s\n",
                   stardata->model.time,
                   stardata->common.orbit.angular_momentum,
                   stardata->common.orbit.separation,
                   stardata->star[0].stellar_type,
                   stardata->star[0].mass,
                   Outermost_core_mass(&stardata->star[0]),
                   stardata->star[0].radius,
                   stardata->star[0].roche_radius,
                   stardata->star[1].stellar_type,
                   stardata->star[1].mass,
                   Outermost_core_mass(&stardata->star[1]),
                   stardata->star[1].radius,
                   stardata->star[1].roche_radius,
                   (stardata->star[0].q),
                   (stardata->star[1].q),
                   rL1(stardata->star[0].q),
                   rL1(stardata->star[1].q),
                   retval == EVOLUTION_REJECT_EVOLUTION ? "reject" :
                   retval == EVOLUTION_ROCHE_OVERFLOW ? "RLOF" :
                   retval == EVOLUTION_ROCHE_OVERFLOW_NONE ? "no RLOF" :
                   "unknown",
                   retval == EVOLUTION_ROCHE_OVERFLOW_AFTER_SPIRAL_IN ? "spiralin" : ""
                );
        }
        else
        {
            /*
             * Use the BSE method to interpolate R to RL
             */
            retval = interpolate_R_to_RL(stardata);
        }
    }

    return retval;
}
