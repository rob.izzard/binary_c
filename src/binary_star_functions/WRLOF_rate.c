#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef WRLOF_MASS_TRANSFER

double WRLOF_rate(struct stardata_t * const stardata,
                  struct star_t * const donor,
                  struct star_t * const accretor)
{
    /*
     * CAB - 27 Aug 2011
     *
     * uses Shazrene's simulation to define a dependence of
     * accretion efficiency (beta) from the ratio (x) between the
     * dustformation radius (Rd) and the radius of the Roche Lobe (RRL)
     * (see also Mohamed & Podsiadlowski, 2007).
     *
     * The best option (to fit the data from Shazrene's simulations)
     * is a parabola.
     *    beta = a*x^2 + b*x + c
     */

    /* returned rate: NB will be zero when there is no WRLOF */
    double rate;

    /*
     * You now have to choose which fitting formula you want to use....
     */
    if(stardata->preferences->WRLOF_method == WRLOF_NONE)
    {
        rate = 0.0;
    }
    else
    {
        double WRLOF_accretion_efficiency; /* "beta" */

        /* Rdust/RL for the wind-losing star */
        const double Rdust = dust_formation_radius(donor);
        const double Rd_RRL_ratio = Rdust/Max(TINY,donor->roche_radius);
        const double WRLOF_max_accretion_efficiency = 0.5;

        /* a, b, c are the result of a gnuplot-best-fit on Shazrene's data */
        const double a = -0.284, b = 0.918, c = -0.234;

        if(stardata->preferences->WRLOF_method == WRLOF_Q_DEPENDENT)
        {
            /*
             * CAB: if this is defined, we introduce an explicit dependence if dmt from q
             * This dependence is q**2, approx the same as in BHL if we consider v_w>>v_orb
             * Factor 25/9 is necessary to have 25/9 * q**2 = 1 when q=0.6 (Shazrene's data)
             */
            if(ON_EITHER_GIANT_BRANCH(donor->stellar_type))
            {
                const double q_fac = 25.0/9.0 * Pow2(accretor->mass/donor->mass);
                WRLOF_accretion_efficiency = q_fac * (a * Pow2(Rd_RRL_ratio) + b * Rd_RRL_ratio + c );
            }
            else
            {
                WRLOF_accretion_efficiency = 0.0;
            }
        }
        else if(stardata->preferences->WRLOF_method == WRLOF_QUADRATIC)
        {
            /*
             * In this case, dmt depends on q only through beta,
             * which depends on RL, which has a weak dependency on q
             */
            if(ON_EITHER_GIANT_BRANCH(donor->stellar_type))
            {
                WRLOF_accretion_efficiency = a * Pow2(Rd_RRL_ratio) + b * Rd_RRL_ratio + c ;
            }
            else
            {
                WRLOF_accretion_efficiency = 0.0;
            }
        }
        else
        {
            /* bad method : exit with warning to stderr */
            WRLOF_accretion_efficiency = 0.0;
            Exit_binary_c(BINARY_C_WRONG_ARGUMENT,
                          "Unknown WRLOF method %d in WRLOF_rate\n",
                          stardata->preferences->WRLOF_method);
        }

        Clamp(WRLOF_accretion_efficiency,
              0.0,
              WRLOF_max_accretion_efficiency);

        rate = WRLOF_accretion_efficiency * -donor->derivative[DERIVATIVE_STELLAR_MASS_WIND_LOSS];
    }

    Dprint("t=%g WRLOF mass transfer rate %g (star %d (type %d) to %d (type %d), method=%d)\n",
           stardata->model.time,
           rate,
           donor->starnum,
           donor->stellar_type,
           accretor->starnum,
           accretor->stellar_type,
           stardata->preferences->WRLOF_method);

    /* return calculated rate */
    return rate;
}

#endif /* WRLOF_MASS_TRANSFER */
