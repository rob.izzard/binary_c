#include "../binary_c.h"
No_empty_translation_unit_warning;


/*
 * Peters 1964 Physical review 136,4B
 * gravitational wave merger timescale
 *
 * Returns the result in years or a very long
 * time if m1+m2 = 0 or a = 0
 *
 * Pass in m1,m2 in Msun (same as code units)
 *
 */

static double integrand(double ecc,
                        void * params);
static double nasty_integral(double ecc);

double Peters_grav_wave_merger_time(const struct stardata_t * Restrict const stardata,
                                    const double m1,
                                    const double m2,
                                    const double a,
                                    const double ecc)
{

    if(stardata->persistent_data->Peters_grav_wave_merger_time_table == NULL)
    {
        /*
         * Make cache table of "nasty_integral" in the Peters
         * formula. This is faster than calling GSL each time,
         * and if there are any errors in the calculation they
         * are flagged immediately on first run, rather than
         * later during the evolution.
         *
         * Note that, for e < 0.0005 we have I = 0.395833
         * to within 6 significant figures, so we start the table
         * there and go up to nearly 1.0 - 1e-6 (at which point
         * the spiral time is likely very short or some other
         * kind of interaction will occur).
         */
#define __ECCMIN 1e-8
#define __ECCMAX (1.0 - 1e-8)

        double log_ecc = log10(__ECCMIN);
        const size_t N = 200;
        const double d_log_ecc = (log10(__ECCMAX) - log10(__ECCMIN))/(N-1);
        double ec;
        /*
         * allocate memory for the data and set in the
         * interpolation data table
         */
        double * const t = Malloc(N*sizeof(double)*2);
        stardata->persistent_data->Peters_grav_wave_merger_time_data = t;

        NewDataTable_from_Pointer(
            t,
            stardata->persistent_data->Peters_grav_wave_merger_time_table,
            1,
            1,
            N
            );

        size_t l = N-1;
        while(log_ecc < 0.0)
        {
            ec = 1.0 - exp10(log_ecc);
            /*
             * We store log10(ecc) and log10(integral) for
             * interpolation
             */
            t[l*2] = log10(ec);
            t[l*2+1] = log10(nasty_integral(ec));
            log_ecc += d_log_ecc;
            l--;
        }
    }

    /*
     * Raw calculation using only the integral through GSL.
     * This is slow!
     *
     const double tmerge =
     (Is_zero(m1+m2) || Is_zero(a)) ?
     LONG_TIME_REMAINING :
     (
     (15.0/304.0) * Pow4(a*R_SUN) * Pow5(SPEED_OF_LIGHT) /
     (
     Pow3(GRAVITATIONAL_CONSTANT * M_SUN) * m1 * m2 * (m1 + m2)
     )
     *
     nasty_integral(Max(ecc,Min(1.0-1e-6,1e-10)))/ YEAR_LENGTH_IN_SECONDS);
     return tmerge;
    */


    /*
     * Runtimes for 1000000 calls:
     * Integral : 7.34s
     * Table (cached, constant input) : 0.17s
     * Table (not cached, constant input) : 0.20s
     * Table (random input) : 0.80s
     *
     * So we should use the table: it is safer and
     * an order of magnitude (at least) faster.
     *
     * EXCEPT: when the eccentricity is very
     * close to 1.0 the table will not work,
     * so in this case use the formula directly.
     */

    double integral;
    if(ecc > 1.0 - 2.0*__ECCMAX)
    {
        const double p[1] = {log10(ecc)};
        double r[1];
        Interpolate(stardata->persistent_data->Peters_grav_wave_merger_time_table,
                    p,
                    r,
                TRUE);
        integral = exp10(r[0]);
    }
    else
    {
        integral = nasty_integral(Max(ecc,Min(1.0-1e-6,1e-10)));
    }

    const double tmerge2 =
        (Is_zero( m1 + m2 ) || Is_zero( a )) ?
        LONG_TIME_REMAINING :
        (
            (
                (15.0/304.0) * Pow4(a*R_SUN) * Pow5(SPEED_OF_LIGHT) /
                (
                    Pow3(GRAVITATIONAL_CONSTANT * M_SUN) * m1 * m2 * (m1 + m2)
                    )
                *
                integral / YEAR_LENGTH_IN_SECONDS)

            /*
             * Modulate by artificial factor on the assumption
             * of a linear response
             */
            / stardata->preferences->gravitational_radiation_modulator_J
            );

    return tmerge2 ;
}

static double nasty_integral(double ecc)
{
    const double ecc2 = Pow2(ecc);
    double x =
        (1.0 - ecc2) * pow(ecc, -12.0/19.0) *
        pow(1.0 + 121.0/304.0 * ecc2, -870.0/2299.0);
    x = Pow4(x);

    double error;
    const double integral = GSL_integrator(0.0,
                                           ecc,
                                           1e-6,/* 1e-8 gave errors */
                                           &error,
                                           GSL_INTEGRATOR_QAG,
                                           &integrand,
                                           ecc2);
    x *= integral;
    return x;
}

static double integrand(double ecc,
                        void * params)
{
    Map_GSL_params(params,args);
    Map_varg(const double,ecc2,args);
    va_end(args);
    const double dI =
        pow(ecc,29.0/19.0) *
        pow(1.0 + 121.0/304.0 * ecc2, -1181.0/2299.0) *
        pow(1.0 - ecc2, -1.5);
    return dI;
}
