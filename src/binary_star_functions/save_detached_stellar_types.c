
#include "../binary_c.h"
No_empty_translation_unit_warning;


void save_detached_stellar_types(struct stardata_t * Restrict const stardata)
{
    Star_number k;
    Evolving_Starloop(k)
    {
        stardata->star[k].detached_stellar_type = stardata->star[k].stellar_type;
    }
}
