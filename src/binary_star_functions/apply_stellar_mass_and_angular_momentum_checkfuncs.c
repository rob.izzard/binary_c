#include "../binary_c.h"
No_empty_translation_unit_warning;

/*
 * Functions to check whether applied derivatives give physically realistic
 * results. To be used with apply_derivative.
 *
 * The functions should be of type "derivative_check_function" and return
 * TRUE on success, FALSE on failure.
 *
 * Each function takes stardata, value, derivative, timestep, derivative_number  and derivative_group, i.e. should be declared with
 *
 * derivative_check_function stellar_mass(struct stardata_t * stardata Maybe_unused,
 *                                        void * data Maybe_unused,
 *                                        const double value,
 *                                        const double derivative Maybe_unused,
 *                                        const double dt Maybe_unused,
 *                                        const Derivative nd Maybe_unused,
 *                                        const Derivative_group derivative_group Maybe_unused);
 *
 * where the value is whatever is being tests, *data is a
 * void pointer so can be anything,
 * and the other parameters have whatever they are set in
 * apply_derivative (note that derivative
 * is the scalar derivative).
 */

Boolean derivative_check_stellar_mass(struct stardata_t * const stardata Maybe_unused,
                                      void * const data Maybe_unused,
                                      const double previous_value Maybe_unused,
                                      const double value,
                                      const double derivative Maybe_unused,
                                      const double dt Maybe_unused,
                                      const Derivative nd Maybe_unused,
                                      const Derivative_group derivative_group Maybe_unused)
{
    /*
     * stellar mass cannot be negative
     */
    Boolean x = Negative_nonzero(value) ? FALSE : TRUE;
    Dprint("stellar mass checkfunc %s%s%s : M = %g\n",Colourtruefalse(x),value);

    /*
     * some stellar types cannot eat into their cores
     */
    struct star_t * const star = (struct star_t * const) data;
#define CORE_CHECK_LIST                         \
    X( GIANT_BRANCH)                            \
    X(        TPAGB)

    /*
     * Mass accuracy (Msun) : we only reject if
     * the (previous) envelope mass exceeds _MASS_ACCURACY
     * and if the core is eaten into
     */
#define _MASS_ACCURACY 1e-6
#define X(TYPE)                                                         \
    {                                                                   \
        if(x == TRUE &&                                                 \
           star->stellar_type == TYPE)                                  \
        {                                                               \
            const double _mc = Outermost_core_mass(star);               \
            const double _menv = star->mass - _mc;                      \
            if(value < _mc &&                                           \
               fabs(_menv) > _MASS_ACCURACY)                            \
            {                                                           \
                x = FALSE;                                              \
            }                                                           \
            Dprint("check mass = %g (st %d %d) is not less than mass of core which is %g : %s\n", \
                   value,                                               \
                   star->stellar_type,     TYPE,                        \
                   _mc,                                                 \
                   x == FALSE ? "Failed" : "OK");                       \
        }                                                               \
    }

    CORE_CHECK_LIST;
#undef X

return x;
}

Pure_function
Boolean derivative_check_stellar_angmom(struct stardata_t * const stardata Maybe_unused,
                                        void * const data Maybe_unused,
                                        const double previous_value Maybe_unused,
                                        const double value,
                                        const double derivative Maybe_unused,
                                        const double dt Maybe_unused,
                                        const Derivative nd Maybe_unused,
                                        const Derivative_group derivative_group Maybe_unused)
{
    /*
     * angular momentum cannot be negative
     */
    //const struct star_t * const star = (struct star_t *)data;
    const Boolean x = value < -TINY ? FALSE : TRUE;
    Dprint("stellar angmom checkfunc %s%s%s\n",Colourtruefalse(x));
    return x;
}

Pure_function
Boolean derivative_check_stellar_he_core_mass(struct stardata_t * const stardata Maybe_unused,
                                              void * const data Maybe_unused,
                                              const double previous_value Maybe_unused,
                                              const double value,
                                              const double derivative Maybe_unused,
                                              const double dt Maybe_unused,
                                              const Derivative nd Maybe_unused,
                                              const Derivative_group derivative_group Maybe_unused)
{
    /*
     * Helium core mass cannot exceed the stellar mass or be negative.
     *
     * This can happen at the end of the TPAGB where the core growth would
     * overshoot outside the star if the timestep is too long.
     */
    const struct star_t * const star = (struct star_t *)data;
    const Boolean x = (
        ON_GIANT_BRANCH(star->stellar_type)
        &&
        (value > star->mass + TINY ||
         Negative_nonzero(value))
        ) ? FALSE : TRUE;
    Dprint("He core mass checkfunc %s%s%s (stellar type %d, Mc = %g > M = %g or Mc = %g < 0 ?)\n",
           Colourtruefalse(x),
           star->stellar_type,
           value,
           star->mass + TINY,
           value
        );
    return x;
}

Pure_function
Boolean derivative_check_stellar_CO_core_mass(struct stardata_t * const stardata Maybe_unused,
                                              void * const data Maybe_unused,
                                              const double previous_value Maybe_unused,
                                              const double value,
                                              const double derivative Maybe_unused,
                                              const double dt Maybe_unused,
                                              const Derivative nd Maybe_unused,
                                              const Derivative_group derivative_group Maybe_unused)
{
    /*
     * core mass cannot exceed the stellar mass or be negative
     */
    const struct star_t * const star = (struct star_t *)data;
    const Boolean x = ((star->stellar_type == TPAGB ||
                        star->stellar_type == GIANT_BRANCH) &&
                       (value > star->mass + TINY ||
                        Negative_nonzero(value) )) ? FALSE : TRUE;
    Dprint("CO core checkfunc %s%s%s (new core mass %g > stellar mass %g ? (diff is %g) or is %g negative?) previous stellar type %d \n",
           Colourtruefalse(x),
           value,
           star->mass,
           value - star->mass,
           value,
           stardata->previous_stardata->star[star->starnum].stellar_type
        );
    return x;
}

Pure_function
Boolean derivative_check_num_thermal_pulses(struct stardata_t * const stardata Maybe_unused,
                                            void * const data Maybe_unused,
                                            const double previous_value Maybe_unused,
                                            const double value,
                                            const double derivative Maybe_unused,
                                            const double dt Maybe_unused,
                                            const Derivative nd Maybe_unused,
                                            const Derivative_group derivative_group Maybe_unused)
{
    /*
     * number of thermal pulses cannot be negative except
     * when it is -1 (i.e. unset)
     */
    const struct star_t * const star = (struct star_t *)data;
    const Boolean x = (star->stellar_type == TPAGB &&
                       Negative_nonzero(value) &&
                       !Fequal(value,-1.0)) ? FALSE : TRUE;
    Dprint("NTP checkfunc %s%s%s\n",Colourtruefalse(x));
    return x;
}

Boolean derivative_check_dm_novaH(struct stardata_t * const stardata Maybe_unused,
                                  void * const data Maybe_unused,
                                  const double previous_value Maybe_unused,
                                  const double value,
                                  const double derivative Maybe_unused,
                                  const double dt Maybe_unused,
                                  const Derivative nd Maybe_unused,
                                  const Derivative_group derivative_group Maybe_unused)
{
    /*
     * dmnovaH cannot be >Mstar
     */
    const struct star_t * const star = (struct star_t *)data;
    const Boolean x = (value > star->mass) ? FALSE : TRUE;
#ifdef NOVA_DEBUG
    printf("H-nova checkfunc dmnovaH = %g * %g = %g vs mass %g %s%s%s\n",
           derivative,
           dt,
           value,
           star->mass,
           Colourtruefalse(x));
#endif
    return x;
}

#ifdef KEMP_NOVAE
Boolean derivative_check_dm_novaHe(struct stardata_t * const stardata Maybe_unused,
                                   void * const data Maybe_unused,
                                   const double previous_value Maybe_unused,
                                   const double value,
                                   const double derivative Maybe_unused,
                                   const double dt Maybe_unused,
                                   const Derivative nd Maybe_unused,
                                   const Derivative_group derivative_group Maybe_unused)
{
    /*
     * dmnovaHe cannot be >Mstar
     *
     * It can be negative!
     */
    const struct star_t * const star = (struct star_t *)data;
    const Boolean x = (value > star->mass) ? FALSE : TRUE;
#ifdef NOVA_DEBUG
    printf("dmnovaHe checkfunc : dmnovaHe = %g vs mass = %g and should be >0 %s%s%s\n",
           value,
           star->mass,
           Colourtruefalse(x));
#endif
    return x;
}
#endif // KEMP_NOVAE

Pure_function
Boolean derivative_check_num_novae(struct stardata_t * const stardata Maybe_unused,
                                   void * const data Maybe_unused,
                                   const double previous_value Maybe_unused,
                                   const double value,
                                   const double derivative Maybe_unused,
                                   const double dt Maybe_unused,
                                   const Derivative nd Maybe_unused,
                                   const Derivative_group derivative_group Maybe_unused)
{
    /*
     * num_novae cannot be negative
     */
    const Boolean x = Negative_nonzero(value) ? FALSE : TRUE;
    Dprint("num_novae checkfunc %s%s%s\n",Colourtruefalse(x));
    return x;
}
