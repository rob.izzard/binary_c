#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef KEMP_NOVAE

static double _144(const double Mdotlog10 Maybe_unused);
static double _138(const double Mdotlog10);
static double _135(const double Mdotlog10);
static double _130(const double Mdotlog10);
static double _125(const double Mdotlog10);
static double _120(const double Mdotlog10);
static double _110(const double Mdotlog10);
static double _102(const double Mdotlog10);
static double _92(const double Mdotlog10);
static double _81(const double Mdotlog10);
static double _70(const double Mdotlog10);
static double _60(const double Mdotlog10);
static double _20(const double Mdotlog10 Maybe_unused);

double Hedet_dMHe(struct star_t * const Restrict accretor)
{
    /*
     * Critical helium-layer mass for detotation.
     *
     * From formulae fitted to data from Kato et al 2018 (Figure 1,
     * data obtained from M. Kato, private communication) for M_WD>1.0.
     * From formulae fitted to data from Piersanti et al 2014
     * (supplementary data B7) for M_WD <1.0
     *
     * note on variable naming: higher && lower in the 'fit'
     * variable name refers to the mass of the WD curve
     * (e.g. a 1.38 Msun WD curve will be the 'higher',
     * compared to a 1.35 Msun WD curve.)
     *
     * returns: dMHe
     *
     * Required units for inputs: M_sun and M_sun/yr (for M_WD and the accretion rate respectively)
     *
     * Note: we assume MCh = 1.44 Msun
     */
    const double mdot = Mdot_net(accretor);
    if(mdot <= 0.0)
    {
        return 1e10; /* very large, i.e. no detonation possible */
    }
    else
    {
        const double Mdotlog10 = log10(mdot);
        const double M_WD = Limit_range(accretor->mass,0.2,1.44);

#define MASSLIST \
    X(  20)      \
    X(  60)      \
    X(  70)      \
    X(  81)      \
    X(  92)      \
    X( 102)      \
    X( 110)      \
    X( 120)      \
    X( 125)      \
    X( 130)      \
    X( 135)      \
    X( 138)      \
    X( 144)

        /*
         * Make mass and functions list
         */
#undef X
#define X(M) (0.01*M),
        static double masses[] = { MASSLIST };
#undef X
#define X(M) _ ## M,
        static double (*funcs[])(const double) = { MASSLIST };
#undef X

        /*
         * Find spanning indices based on current mdot
         */
        const rinterpolate_counter_t low =
            rinterpolate_bisearch(masses,
                                  M_WD,
                                  Array_size(masses));

        const rinterpolate_counter_t high = low + 1;

        /*
         * Hence interpreted detonation mass
         */
        return exp10(interp_lin(M_WD,
                                masses[low],
                                masses[high],
                                funcs[low](Mdotlog10),
                                funcs[high](Mdotlog10)));
    }
}


static double _144(const double Mdotlog10 Maybe_unused)
{
    return log10(8e-7);
}

static double _138(const double Mdotlog10)
{
    if(Mdotlog10 > -7.94)
    {
        return 0.54437094*Pow4(Mdotlog10) + 14.68324636*Pow3(Mdotlog10) +
            148.24381872*Pow2(Mdotlog10) + 663.22092056*Mdotlog10 + 1103.57417218;
    }
    else
    {
        return -0.1433*Mdotlog10 - 4.079;
    }
}
static double _135(const double Mdotlog10)
{
    if(Mdotlog10 >= -7.88)
    {
        return 0.47596972*Pow4(Mdotlog10) + 12.61002433*Pow3(Mdotlog10)
            + 125.08958464*Pow2(Mdotlog10) + 549.88858261*Mdotlog10 + 898.29715596;
    }
    else
    {
        return -0.0999*Mdotlog10 - 3.1982;
    }
}
static double _130(const double Mdotlog10)
{
    if(Mdotlog10 >= -7.6920763)
    {
        return 1.16410162*Pow4(Mdotlog10) + 30.86542279*Pow3(Mdotlog10)
            + 306.21947654*Pow2(Mdotlog10) + 1346.49353679*Mdotlog10 + 2208.95849065;
    }
    else
    {
        return -0.0888*Mdotlog10 - 2.8977;
    }
}

static double _125(const double Mdotlog10)
{
    if(Mdotlog10 >= -7.62)
    {
        return 1.68244304*Pow4(Mdotlog10) + 45.15739606*Pow3(Mdotlog10)
            + 453.76725009*Pow2(Mdotlog10) + 2022.35971770*Mdotlog10 + 3368.09376231;
    }
    else
    {
        return -0.0957*Mdotlog10 - 2.8734;
    }
}
static double _120(const double Mdotlog10)
{
    if(Mdotlog10 >= -7.67)
    {
        return 0.91755524*Pow4(Mdotlog10) + 24.10100929*Pow3(Mdotlog10)
            + 236.98730163*Pow2(Mdotlog10) + 1033.14600808*Mdotlog10 + 1680.17435446;
    }
    else
    {
        return -0.0646*Mdotlog10 - 2.1626;
    }
}
static double _110(const double Mdotlog10)
{
    if(Mdotlog10 >= -7.56)
    {
        return 1.72662751*Pow4(Mdotlog10) + 45.88762486*Pow3(Mdotlog10)
            + 456.63345197*Pow2(Mdotlog10) + 2015.69281770*Mdotlog10 + 3325.89047580;
    }
    else
    {
        return -0.0731*Mdotlog10 - 2.1163;
    }
}
static double _102(const double Mdotlog10)
{
    if(Mdotlog10>=-7.30)
    {
        return -2.0406*Mdotlog10 - 16.597;
    }
    else if(Mdotlog10>=-8.82)
    {
        return -0.52114281*Pow3(Mdotlog10) - 13.26176747*Pow2(Mdotlog10)
            - 112.71207541*Mdotlog10 - 320.54975268;
    }
    else
    {
        return-0.0282*Mdotlog10 - 0.7734;
    }
}
static double _92(const double Mdotlog10)
{
    if(Mdotlog10>=-7.52)
    {
        return -2.0714*Mdotlog10 - 16.929;
    }
    else if(Mdotlog10>=-8.82)
    {
        return -2.22457174*Pow4(Mdotlog10) - 74.06815881*Pow3(Mdotlog10) - 924.25967441*Pow2(Mdotlog10)
            - 5123.20522589*Mdotlog10 - 10644.68164309;
    }
    else
    {
        return -0.0238*Mdotlog10 - 0.6265;
    }
}
static double _81(const double Mdotlog10)
{
    if(Mdotlog10 >= -7.60)
    {
        return -2.7234*Mdotlog10 - 21.695;
    }
    else if(Mdotlog10 >= -8.82)
    {
        return -0.89021703*Pow3(Mdotlog10) - 22.38723555*Pow2(Mdotlog10)
            - 187.76315351*Mdotlog10 - 525.64755404;
    }
    else
    {
        return -0.0265*Mdotlog10 - 0.5781;
    }
}
static double _70(const double Mdotlog10)
{
    if(Mdotlog10 >= -7.70)
    {
        return-2.8393*Mdotlog10 - 22.535;
    }
    else if(Mdotlog10 >= -9.0)
    {
        return -0.68502223*Pow4(Mdotlog10) - 23.27090657*Pow3(Mdotlog10) - 296.28435839*Pow2(Mdotlog10)
            - 1675.83253716*Mdotlog10 - 3553.76035429;
    }
    else
    {
        return -0.0251*Mdotlog10 - 0.4658;
    }
}
static double _60(const double Mdotlog10)
{
    if(Mdotlog10 >= -7.82)
    {
        return -2.3314*Mdotlog10 - 18.668;
    }
    else if(Mdotlog10 >= -9.0)
    {
        return -0.13105692*Pow3(Mdotlog10) - 3.35859429*Pow2(Mdotlog10)
            - 28.84546767*Mdotlog10 - 83.28360749;
    }
    else
    {
        return -0.0303*Mdotlog10 - 0.4544;
    }
}
static double _20(const double Mdotlog10 Maybe_unused)
{
    return 0.2;
}
#endif // KEMP_NOVAE
