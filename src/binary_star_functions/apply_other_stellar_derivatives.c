#include "../binary_c.h"
No_empty_translation_unit_warning;

/*
 * Every timestep, apply derivatives which are not
 * mass and angular momentum.
 *
 * Returns TRUE on success, FALSE on failure.
 */

Boolean apply_other_stellar_derivatives(struct stardata_t * Restrict const stardata,
                                        const Boolean RLOF,
                                        const Boolean can_reject Maybe_unused,
                                        const Boolean can_reject_and_shorten Maybe_unused)
{
    /*
     * Timestep
     */
    double dt Maybe_unused = RLOF==TRUE ? (stardata->model.dtm*1e6) : stardata->model.dt;

    /*
     * Make a list of checkfuncs
     */
#undef X
#define X(CODE,STRING,VAR,FUNC,TYPE,MINTCLEAR) FUNC,
    static Boolean (*derivative_checkfunc[])
        (struct stardata_t * const,
         void * const data,
         const double,
         const double,
         const double,
         const Derivative,
         const Derivative_group)
        Maybe_unused =
        { SYSTEM_DERIVATIVES_LIST };
#undef X

#ifdef REVERSE_TIME
    if(stardata->preferences->reverse_time == TRUE)
    {
        dt *= -1;
        dtm *= -1;
    }
#endif//REVERSE_TIME

#ifdef MINT
    if(stardata->preferences->stellar_structure_algorithm ==
       STELLAR_STRUCTURE_ALGORITHM_MINT &&
       MINT_apply_stellar_derivatives(stardata,
                                      dt,
                                      can_reject,
                                      can_reject_and_shorten) == FALSE)
    {
        return FALSE;
    }
#endif //MINT

    return TRUE;
}
