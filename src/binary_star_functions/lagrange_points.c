#include "../binary_c.h"
No_empty_translation_unit_warning;



/*
 * Lagrange point positions where (x,y)=(0,0) is the
 * centre of mass.
 *
 * Units of M1,M2 are arbitrary.
 *
 * The distances are returned in the same
 * units as the separation.
 *
 * Based on http://www.tp.umu.se/space/Proj_08/T_Munch.pdf
 * page 5 etc. (which is also in doc/T_Munch.pdf) 
 *
 * The centre of mass is (0,0,0).
 *
 * The positions a1 and a2 are the x-coordinates of the stars
 * (which both have y = z = 0). NB a1,a2 can be NULL pointers, 
 * in which case they are not set.
 *
 * L1 is between the stars.
 * L2 is on star 2's side.
 * L3 is on star 1's side.
 * L4,5 are at y!=0.
 *
 * We require M1>M2. If this is not the case, the masses (and corresponding results)
 * are flipped.
 * TODO flip L4,5?
 */

void lagrange_points(double M1,
                     double M2,
                     const double separation,
                     struct coordinate_t L[NUMBER_OF_LAGRANGE_POINTS],
                     double * Restrict const a1,
                     double * Restrict const a2
    )
{
    /* 
     * Flip the masses if M1 < M2
     */
    Boolean flip = FALSE;

    int i2,i3;
    if(M2>M1)
    {
        Swap(M1,M2);
        flip = TRUE;
        i2 = 2;
        i3 = 1;
    }
    else
    {
        i2 = 1;
        i3 = 2;
    }
    double mtot = M1 + M2;
    double mu1 = M1/mtot;
    double mu2 = M2/mtot;
    double alpha = cbrt(mu2/(3.0*mu1));
    double beta = mu2/mu1;

    // L1 
    L[0].x = Quartic(alpha, mu1, -1.0, 1.0/3.0, 1.0/9.0, 23.0/81.0); // +O(alpha^5)
    L[0].y = 0.0;
    L[0].z = 0.0;

    // L2 
    L[i2].x = Quartic(alpha, mu1, +1.0, 1.0/3.0, -1.0/9.0, -31.0/81.0); // +O(alpha^5)
    L[i2].y = 0.0;
    L[i2].z = 0.0;

    // L3
    L[i3].x = mu1 - Cubic(beta, 2.0, -7.0/12.0, +7.0/12.0, -13223.0/20736.0); // +O(beta^4)
    L[i3].y = 0.0;
    L[i3].z = 0.0;

    // L4,5
    L[3].x = 0.5 - mu2;
    L[3].x = sqrt(3.0)/2.0;
    L[3].z = 0.0;

    L[4].x = 0.5 - mu2;
    L[4].y = -sqrt(3.0)/2.0;
    L[4].z = 0.0;

    /*
     * Locations of the stars : only set
     * if a1 and a2 are non-NULL
     */
    if(a1 != NULL && a2 != NULL)
    {
        if(flip == TRUE)
        {
            *a1 = + mu1;
            *a2 = - mu2;
        }
        else
        {
            *a1 = - mu2;
            *a2 = + mu1;
        }
        *a1 *= separation;
        *a2 *= separation;
    }

    /* scale L points by semi-major axis */
    int i;
    for(i=0; i<NUMBER_OF_LAGRANGE_POINTS; i++)
    {
        L[i].x *= separation;
        L[i].y *= separation;
        L[i].z *= separation;
    }
}
