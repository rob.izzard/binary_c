#include "../binary_c.h"
No_empty_translation_unit_warning;


/*
 * Dynamical mass transfer rate accretion limit
 * 
 * Ignored (set to VERY_LARGE_MASS_TRANSFER_RATE)
 * if the accretion_limit_dynamical_multiplier is negative. 
 */
double Pure_function dynamical_limit_for_accretion(struct star_t * Restrict accretor,
                                     struct stardata_t * Restrict const stardata)
{
    return
        stardata->preferences->accretion_limit_dynamical_multiplier < -TINY ?
        VERY_LARGE_MASS_TRANSFER_RATE : 
        (accretor->mass/dynamical_timescale(accretor)
         *
         stardata->preferences->accretion_limit_dynamical_multiplier);
}
