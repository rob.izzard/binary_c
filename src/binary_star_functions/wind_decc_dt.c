#include "../binary_c.h"
No_empty_translation_unit_warning;


double Pure_function wind_decc_dt(struct stardata_t * Restrict const stardata)
{
    const double M = system_gravitational_mass(stardata);

    /*
    Dprint("de/dt wind = %g * (%g * (0.5/%g + 1/%g) + (%g * (0.5/%g + 1/%g)))\n",
           stardata->common.orbit.eccentricity,
           stardata->star[0].derivative[DERIVATIVE_STELLAR_MASS_WIND_GAIN],
           stardata->star[0].mass,
           M,
           stardata->star[1].derivative[DERIVATIVE_STELLAR_MASS_WIND_GAIN],
           stardata->star[1].mass,
           M);
    */
    return stardata->common.orbit.eccentricity *
        (stardata->star[0].derivative[DERIVATIVE_STELLAR_MASS_WIND_GAIN]*(0.50/stardata->star[0].mass + 1.0/M) +
         stardata->star[1].derivative[DERIVATIVE_STELLAR_MASS_WIND_GAIN]*(0.50/stardata->star[1].mass + 1.0/M));
}
