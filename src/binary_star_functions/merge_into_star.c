#include "../binary_c.h"
No_empty_translation_unit_warning;

void merge_into_star(struct stardata_t * const stardata,
                     struct star_t * const star_into,
                     const Boolean eject,
                     const Yield_source source)
{
    /*
     * Merge stars in stardata into given star,
     * which can be one of the stars in stardata.
     *
     * If eject==TRUE and source>=0, compute the
     * yields also.
     */
    const int merged_starnum = star_into->starnum;
    Dprint("merge into star %d\n",star_into->starnum);

#ifdef NUCSYN
    Abundance * X = New_isotope_array;
#endif // NUCSYN
    struct star_t * merged_star =
        merge_stars(stardata,
                    star_into->starnum,
                    eject
#ifdef NUCSYN
                    ,X
#endif // NUCSYN
        );
    const double Min = Sum_of_stars(baryonic_mass);
    const double Mout = merged_star->baryonic_mass;
    const double Mej = Min - Mout;
    copy_star(stardata,
              merged_star,
              star_into);
    star_into->starnum = merged_starnum;
    free_star(&merged_star);
    Dprint("Baryonic tally: Min %g (%g + %g), Mout %g, Mej %g\n",
           Min,
           stardata->star[0].baryonic_mass,
           stardata->star[1].baryonic_mass,
           Mout,
           Mej);
    if(source != SOURCE_UNKNOWN)
    {
#ifdef NUCSYN
        Dprint("Call calc yields star %d, Mej %g, source %d, X H1 %g He4 %g C12 %g Fe56 %g n %g (1-tot %g)\n",
               star_into->starnum,
               Mej,
               source,
               X[XH1],
               X[XHe4],
               X[XC12],
               X[XFe56],
               X[Xn],
               1.0 - nucsyn_totalX(X));
#endif // NUCSYN
        calc_yields(stardata,
                    star_into,
                    Mej,
#ifdef NUCSYN
                    X,
#endif // NUCSYN
                    0.0,
#ifdef NUCSYN
                    X,
#endif // NUCSYN
                    star_into->starnum,
                    YIELD_FINAL,
                    source);
#ifdef NUCSYN
    Dprint("New yield total %g\n",
           nucsyn_total_yield(stardata));
#endif // NUCSYN
    }
#ifdef NUCSYN
    Safe_free(X);
#endif // NUCSYN
}
