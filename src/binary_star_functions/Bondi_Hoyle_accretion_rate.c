#include "../binary_c.h"
No_empty_translation_unit_warning;


/*
 * Return the Bondi-Hoyle accretion rate in cgs (g/s)
 *
 * See Hurley's PhD dissertation Eq. 3.41
 */
double Bondi_Hoyle_accretion_rate(struct stardata_t * const stardata,
                                  const double mass_accretor, // g
                                  const double vorb, // cm/s
                                  const double mdot_wind, // g/s (>0)
                                  const double vwind_donor, // cm/s
                                  const double separation, // g
                                  const double eccentricity)
{

    return
        stardata->preferences->Bondi_Hoyle_accretion_factor
        *
        -1.0 / sqrt(1.0 - Pow2(eccentricity))
        *
        Pow2( GRAVITATIONAL_CONSTANT * mass_accretor / Pow2(vwind_donor) )
        *
        1.0 / (2.0 * Pow2(separation))
        *
        1.0 / Pow1p5(1.0 + Pow2(vorb/vwind_donor))
        *
        mdot_wind;
}
