#include "../binary_c.h"
No_empty_translation_unit_warning;


/*
 * determine the Roche radius for a single star
 */

void determine_roche_lobe_radius(struct stardata_t * const stardata,
                                 struct orbit_t * const orbit,
                                 struct star_t * const star)
{
    if(stardata->model.sgl==TRUE ||
       stardata->star[Other_star(star->starnum)].stellar_type == MASSLESS_REMNANT ||
       Is_zero(star->q) ||
       Is_zero(stardata->star[Other_star(star->starnum)].mass))
    {
        /*
         * Single star : set very large radii
         */
        star->roche_radius = HUGE_ROCHE_MULTIPLIER*star->radius;
        star->rmin = star->roche_radius;
    }
    else
    {
#ifdef ADAPTIVE_RLOF
        const double rolwas = star->roche_radius;
#endif

        /* q = M(star) / M(other star) */
        star->roche_radius = Roche_radius(star->q,
                                          orbit->separation);

#ifdef RLOF_RADIATION_CORRECTION
        star->roche_radius *= stardata->preferences->RLOF_f;
#endif

        star->roche_radius_at_periastron = star->roche_radius *
            (1.0 - orbit->eccentricity);

        /* set radius of closest approach of Roche stream */
        star->rmin = radius_closest_approach(orbit->separation,
                                             star->q);

#ifdef ADAPTIVE_RLOF
        const double drol = star->roche_radius - rolwas;
        if(Is_not_zero(drol) && Is_not_zero(stardata->model.dtm))
        {
            star->roldot = Max(star->roldot,drol/(1e6*stardata->model.dtm));
        }
        else
        {
            star->roldot = 0.0;
        }
#endif

        Dprint("ROLSET 2 star %d st %d roche_radius=%12.12g rL1(q=%g/%g=%g)=%12.12g, sep=%12.12g, radius%d=%g\n",
               star->starnum,
               star->stellar_type,
               star->roche_radius,
               star->mass,
               stardata->star[Other_star(star->starnum)].mass,
               star->q,
               Roche_radius(star->q,1.0),
               orbit->separation,
               star->starnum,
               star->radius
            );
    }

}
