#include "../binary_c.h"
No_empty_translation_unit_warning;


void stellar_wind_angmom(struct stardata_t * Restrict const stardata,
                         const Boolean RLOF_boolean,
                         const Star_number k)
{
    /*
     * Calculate stellar angular momentum derivatives
     * because of wind mass loss and wind mass accretion
     */
    SETstars(k);

    Dprint("Stars of types %d %d\n",
           star->stellar_type,
           companion->stellar_type);

    if(star->stellar_type != MASSLESS_REMNANT)
    {
        /*
         * Calculate spin angular momentum change of star k:
         * sense is such that angular momentum LOSS (wind) is positive,
         * GAIN (accretion) is negative
         */
        const double r1 = RLOF_boolean==TRUE ? star->effective_radius : star->radius;
        Dprint("jdot wind st %d TZO %d M %g Menv %g : R = %g (RLOF? %s RL %g RLperi %g)\n",
               star->stellar_type,
               star->TZO,
               star->mass,
               envelope_mass(star),
               r1,
               Yesno(RLOF_boolean),
               star->roche_radius,
               star->roche_radius_at_periastron);
        const double r2 = companion->effective_radius;

        /* calculate mass lost from a disk due to rotation */
        const double dm_dt_lost_from_a_disk = star->disc_mdot;

        /* calculate mass lost in a spherical wind */
        const double dm_dt_lost_from_a_spherical_wind =
            -star->derivative[DERIVATIVE_STELLAR_MASS_WIND_LOSS]
            /*
             * star->derivative[DERIVATIVE_STELLAR_MASS_WIND_LOSS]
             * stores the wind lost (rate) from both the wind and the disk,
             * so subtract the disk contribution
             */
            -dm_dt_lost_from_a_disk;

        /*
         * calculate mass accretion rate from a wind
         */
        const double dm_dt_accreted = star->derivative[DERIVATIVE_STELLAR_MASS_WIND_GAIN];

        Dprint("star %d : OMEGA %g from J %g should be %g (rl=%g, I=%g)\n",
               star->starnum,
               star->omega,
               star->angular_momentum,
               star->angular_momentum / moment_of_inertia(stardata,star,r1),
               r1,
               moment_of_inertia(stardata,star,r1));

        /*
         * ... at this stellar surface
         */
        const double r2o = Pow2(r1) * star->omega;
        Dprint("r2o = %g * %g = %g\n",
               r1,
               star->omega,
               r2o);

        /*
         * maximum angular momentum loss rate : we cannot
         * lose more than the star has in one timestep
         */
        const double dj_dt_max = star->angular_momentum / stardata->model.dt;

        /*
         * Spherical mass loss from a wind.
         * Factor 2.0/3.0 is for pure shellular loss.
         */
        const double dj_dt_spherical_wind =
            Min(2.0/3.0 * dm_dt_lost_from_a_spherical_wind * r2o,
                dj_dt_max);

        Dprint("djdt wind %g (dmdt %g, r2o %g, djdtmax %g) hence timescale %g cf dyn %g %g\n",
               dj_dt_spherical_wind,
               dm_dt_lost_from_a_spherical_wind,
               r2o,
               dj_dt_max,
               Is_not_zero(dj_dt_spherical_wind) ? (star->angular_momentum / dj_dt_spherical_wind) : 0.0,
               dynamical_timescale(star),
               star->mass > Outermost_core_mass(star) ? dynamical_timescale_envelope(star) : 0.0);

        /*
         * Spherical accretion.
         * cf. Hurley et al 2002 Eq. 11
         */
        const double dj_dt_accretion = SPHERICAL_ANGMOM_ACCRETION_FACTOR *
            dm_dt_accreted * Pow2(r2) * companion->omega;

        Dprint("spherical wind: dM/dt wind %g disc %g accreted %g\n",
               dm_dt_lost_from_a_spherical_wind,
               dm_dt_lost_from_a_disk,
               dm_dt_accreted);
        Dprint("spherical wind: dJ/dt wind = R1=%g * omega=%g = %g\n",
               r1,
               star->omega,
               dj_dt_spherical_wind);

        Dprint("dj/dt accretion = %g * (dm = %g) * (r2=%g)^2 * (companion omega=%g)\n",
               SPHERICAL_ANGMOM_ACCRETION_FACTOR,
               dm_dt_accreted,
               r2,
               companion->omega);
        /*
         * Mass lost from a disc around the equator
         * of the mass-losing star in an excretion disc
         * due to us spinning at > break up
         */
        const double dj_dt_excretion_disc = dm_dt_lost_from_a_disk*r2o;
        const double Maybe_unused dj_dt = dj_dt_spherical_wind + dj_dt_excretion_disc - dj_dt_accretion;

        Dprint("dJ/dT budget (star %d) : spherical wind %30.22e : accretion %30.22e : excretion disc %30.22e : total %30.22e\n",
               k,
               dj_dt_spherical_wind,
               dj_dt_accretion,
               dj_dt_excretion_disc,
               dj_dt);

        star->derivative[DERIVATIVE_STELLAR_ANGMOM_WIND_LOSS] = -dj_dt_spherical_wind;
        star->derivative[DERIVATIVE_STELLAR_ANGMOM_WIND_GAIN] = dj_dt_accretion;
        star->derivative[DERIVATIVE_STELLAR_ANGMOM_DECRETION_DISC] = -dj_dt_excretion_disc;
    }
}
