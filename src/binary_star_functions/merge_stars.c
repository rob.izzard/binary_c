#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef BSE
/*
 * Function to merge the two stars in stardata,
 * and return the merged star struct.
 *
 * If NUCSYN is defined, the yields of both
 * stars are combined into the merged star.
 *
 * The ejected abundances are set in Xej, if NUCSYN
 * is defined and Xej != NULL. If Xej is NULL, the
 * ejecta (should there be any) are not set.
 */
#include "merge_stars.h"

struct star_t * merge_stars(struct stardata_t * const Restrict stardata,
                            const Star_number merged_star_number,
                            const Boolean eject
#ifdef NUCSYN
                            , Abundance * const Xej Maybe_unused
#endif
    )
{
    struct star_t ** stars = Calloc(NUMBER_OF_STARS + 1,
                                    sizeof(struct star_t *));

    /*
     * Choose the stars so that stars[0] has the larger
     * stellar type (i.e. is more evolved), and stars[1]
     * is less evolved.
     *
     * Note: we work on *copies* of these, just in case
     */
    const int i1 = stardata->star[0].stellar_type >= stardata->star[1].stellar_type ? 0 : 1;
    const int i2 = Other_star(i1);
    stars[MORE_EVOLVED] = New_star_from(&stardata->star[i1]);
    stars[LESS_EVOLVED] = New_star_from(&stardata->star[i2]);

    /*
     * Allocate arrays for the BSE algorithm
     */
    Boolean alloc[3];
    stellar_structure_BSE_alloc_arrays(stars[MORE_EVOLVED],NULL,alloc);
    stellar_structure_BSE_alloc_arrays(stars[LESS_EVOLVED],NULL,alloc);
    Mergeprint("merge stars in stardata types 0=%d 1=%d\n",
               stardata->star[0].stellar_type,
               stardata->star[1].stellar_type);

    int is[] = {i1,i2};

    Foreach_array(int,i,is)
    {
        char * corestring = core_string(stars[i],FALSE);
#ifdef NUCSYN
        Mergeprint("Star i%d : st %d : starnum %d : SN %d : Xenv %p : Xn %g 1-Xenv %g 1-Xacc %g : cores %s\n",
                   i+1,
                   stars[i]->starnum,
                   stars[i]->stellar_type,
                   stars[i]->SN_type,
                   (void*)stars[i]->Xenv,
                   stars[i]->Xenv[Xn],
                   1.0 - nucsyn_totalX(stars[i]->Xenv),
                   1.0 - nucsyn_totalX(stars[i]->Xacc),
                   corestring);
#else
        Mergeprint("Star i%d : st %d : starnum %d : SN %d : cores %s\n",
                   i+1,
                   stars[i]->starnum,
                   stars[i]->stellar_type,
                   stars[i]->SN_type,
                   corestring);
#endif // NUCSYN
        Safe_free(corestring);
    }


    /*
     * Check that neither star is a massless remnant:
     * such a binary can never merge.
     */
    if(stars[MORE_EVOLVED]->stellar_type == MASSLESS_REMNANT ||
       stars[LESS_EVOLVED]->stellar_type == MASSLESS_REMNANT)
    {
#ifdef NUCSYN
        _merge_yields(stars);
#endif // NUCSYN
        return stars[stars[LESS_EVOLVED]->stellar_type != MASSLESS_REMNANT ? 1 : 0];
        Exit_binary_c(BINARY_C_ALGORITHM_OUT_OF_RANGE,
                      "Stellar types are %d %d of which one is massless. This should not happen in merge_stars().",
                      stars[MORE_EVOLVED]->stellar_type,
                      stars[LESS_EVOLVED]->stellar_type);
    }

    /*
     * Compute timescales
     */
    _Compute_timescales(stars[MORE_EVOLVED]);
    _Compute_timescales(stars[LESS_EVOLVED]);

    /*
     * stars[MERGED] == stars[MERGED] is the merged star, based
     * on the more evolved star, star[0].
     */
    stars[MERGED] = new_star(stardata,stars[MORE_EVOLVED]);
    stars[MERGED]->merge_state = STAR_JUST_MERGED;
    Mergeprint("MERGE R=%g SN %d\n",
               stars[MERGED]->radius,
               stars[MERGED]->SN_type);

    /*
     * The merged star should replace the donor.
     */
    stars[MERGED]->starnum = merged_star_number;

    Stellar_type merged_stellar_type =
        merger_stellar_type(stardata,
                            stars[MORE_EVOLVED],
                            stars[LESS_EVOLVED]);
    if(merged_stellar_type >= 100)
    {
        Mergeprint("Forced merger instead of comenv (ok if envelope mass is small)\n");
        merged_stellar_type -= 100;
    }
    stars[MERGED]->stellar_type = merged_stellar_type;

    Mergeprint("merged type %d + %d -> %d (already TZO? %d)\n",
               stars[MORE_EVOLVED]->stellar_type,
               stars[LESS_EVOLVED]->stellar_type,
               merged_stellar_type,
               stars[MERGED]->TZO);

    /*
     * merge cores
     */
    Foreach_array(int,i,is)
    {
        char * corestring = core_string(stars[i],FALSE);
#ifdef NUCSYN
        Mergeprint("pre-core-merge Star i%d : starnum %d : SN %d : Xenv %p : Xn %g 1-Xenv=%g 1-Xacc=%g : cores %s\n",
                   i+1,
                   stars[i]->starnum,
                   stars[i]->SN_type,
                   (void*)stars[i]->Xenv,
                   stars[i]->Xenv[Xn],
                   1.0-nucsyn_totalX(stars[i]->Xenv),
                   1.0-nucsyn_totalX(stars[i]->Xacc),
                   corestring);
#else
        Mergeprint("pre-core-merge Star i%d : starnum %d : SN %d : cores %s\n",
                   i+1,
                   stars[i]->starnum,
                   stars[i]->SN_type,
                   corestring);
#endif // NUCSYN
        Safe_free(corestring);
    }
    Mergeprint("merge cores\n");
    _merge_cores(stardata,stars);
    merged_stellar_type = stars[MERGED]->stellar_type;

    {
        char * corestring = core_string(stars[MERGED],FALSE);
        Mergeprint("Merged cores %s\n",
                   corestring);
        Safe_free(corestring);
    }

    /*
     * Specify total mass of the merged object.
     *
     * We lose a fraction floss.
     *
     * This can be set either:
     *
     * * For all stellar types by setting only merger_mass_loss_fraction
     *
     * * For each stellar type by setting merger_mass_loss_fraction_by_stellar_type[merged_stellar_type]
     *   with merger_mass_loss_fraction as a fallback.
     */

    /*
     * Assume the two stars just merge and lose
     * the appropriate merger_mass_loss_fraction.
     */
    const double floss =
        eject == TRUE ?
        merger_mass_loss_fraction(stardata,
                                  merged_stellar_type) :
        0.0;
    const double fkept = 1.0 - floss;
    const double merged_mass =
        (stars[MORE_EVOLVED]->mass + stars[LESS_EVOLVED]->mass) * fkept;

    /*
     * Assume the ratio fkept applies also to the
     * baryonic mass.
     */
    const double merged_baryonic_mass =
        (stars[MORE_EVOLVED]->baryonic_mass + stars[LESS_EVOLVED]->baryonic_mass) * fkept;

#ifdef NUCSYN
    /*
     * Initial nucleosynthesis
     */
    if(Xej != NULL)
    {
        _initial_nucsyn(stardata,
                        stars,
                        merged_baryonic_mass,
                        Xej);
    }
#endif//NUCSYN

    /*
     * Merge the masses of the stars
     */
    stars[MERGED]->mass = merged_mass;
    stars[MERGED]->baryonic_mass = merged_baryonic_mass;
    stars[MERGED]->phase_start_mass = (stars[MORE_EVOLVED]->phase_start_mass + stars[LESS_EVOLVED]->phase_start_mass) * fkept;
    Mergeprint("Set merged gravitational mass %g, baryonic mass %g, phase start mass %g\n",
               stars[MERGED]->mass,
               stars[MERGED]->baryonic_mass,
               stars[MERGED]->phase_start_mass);

    /*
     * Assume age = 0 although the special case
     * functions may well override this.
     */
    stars[MERGED]->age = 0.0;

    /*
     * Default envelope abundances to the
     * same as the ejecta
     */
#ifdef NUCSYN
    if(Xej != NULL)
    {
        Copy_abundances(Xej,stars[MERGED]->Xenv);
        Mergeprint("Set envelope abunds H1=%g He4=%g\n",
                   stars[MERGED]->Xenv[XH1],
                   stars[MERGED]->Xenv[XHe4]);
    }
#endif // NUCSYN

    /*
     * Special cases : we have a function for
     * each merged stellar type.
     */
#define MERGER_FUNCLIST                                 \
    X(             _to_MS_merger)         /* 0 */       \
        X(             _to_MS_merger)         /* 1 */   \
        X( _to_hydrogen_giant_merger)         /* 2 */   \
        X( _to_hydrogen_giant_merger)         /* 3 */   \
        X( _to_hydrogen_giant_merger)         /* 4 */   \
        X( _to_hydrogen_giant_merger)         /* 5 */   \
        X( _to_hydrogen_giant_merger)         /* 6 */   \
        X(           _to_HeMS_merger)         /* 7 */   \
        X(   _to_helium_giant_merger)         /* 8 */   \
        X(   _to_helium_giant_merger)         /* 9 */   \
        X(             _to_WD_merger)         /* 10 */  \
        X(             _to_WD_merger)         /* 11 */  \
        X(             _to_WD_merger)         /* 12 */  \
        X(          _to_NS_BH_merger)         /* 13 */  \
        X(          _to_NS_BH_merger)         /* 14 */  \
        X(       _to_massless_merger)         /* 15 */

#undef X
#define X(A) &A,
    static void (*funcs[NUMBER_OF_STELLAR_TYPES])(
        struct stardata_t * const,
        struct star_t ** const
        _Xej_dec) =
        {
            MERGER_FUNCLIST
        };

#undef X
#define X(A) Stringify(A),
    static char * funcstrings[] = {
        MERGER_FUNCLIST
    };{
        char * corestring = core_string(stars[MERGED],FALSE);
        Mergeprint("2Merged cores %s\n",
                   corestring);
        Safe_free(corestring);
    }
    /* call the correct function */
    Mergeprint("Call merger function %d = %s\n",
               merged_stellar_type,
               funcstrings[merged_stellar_type]);
    funcs[merged_stellar_type](stardata,stars
#ifdef NUCSYN
                               ,Xej
#endif
        );
    {
        char * corestring = core_string(stars[MERGED],FALSE);
        Mergeprint("3Merged cores %s\n",
                   corestring);
        Safe_free(corestring);
    }

    /*
     * Update stellar timescales
     */
    Mergeprint("Update stellar timescales\n");
    _Compute_timescales(stars[MERGED]);

    /*
     * Update structure (L, R, etc.) without
     * allowing supernovae
     */
    {
        char * corestring = core_string(stars[MERGED],FALSE);
        Mergeprint("Update structure stellar type %d, Mgrav %g Mbary %g, cores %s\n",
                   stars[MERGED]->stellar_type,
                   stars[MERGED]->mass,
                   stars[MERGED]->baryonic_mass,
                   corestring);
        Safe_free(corestring);
    }
    stars[MERGED]->deny_SN++;
    stellar_structure(stardata,
                      STELLAR_STRUCTURE_CALLER_merge_stars,
                      stars[MERGED],
                      NULL);
    stars[MERGED]->deny_SN--;

    /*
     * Return merged star struct
     */
    {
        char * corestring = core_string(stars[MERGED],FALSE);
        Mergeprint("stars merged : mgrav %g mbary %g : cores %s : age %g\n",
                   stars[MERGED]->mass,
                   stars[MERGED]->baryonic_mass,
                   corestring,
                   stars[MERGED]->age);
        Safe_free(corestring);
    }


#ifdef NUCSYN
    /*
     * Combine the yields of the stars
     */
    _merge_yields(stars);
    const double mass_ejected =
        stars[MORE_EVOLVED]->baryonic_mass +
        stars[LESS_EVOLVED]->baryonic_mass -
        stars[MERGED]->baryonic_mass;
#endif // NUCSYN

    /*
     * Free temporary star structs and return
     * what's stored at the pointer stars[MERGED]
     */
    struct star_t * const retstar = stars[MERGED];
    free_star(&stars[MORE_EVOLVED]);
    free_star(&stars[LESS_EVOLVED]);
    Safe_free(stars);
    {
        char * corestring = core_string(retstar,TRUE);
        Mergeprint("Return merged star of type %d with Mgrav = %g Mbary = %g, cores %s [starnum %d]\n",
                   retstar->stellar_type,
                   retstar->mass,
                   retstar->baryonic_mass,
                   corestring,
                   retstar->starnum);
        Safe_free(corestring);
    }
#ifdef NUCSYN
    Mergeprint("Merged star envelope abundances Xn %g XH1 %g XHe4 %g XC12 %g (1-total %g)\n",
               retstar->Xenv[Xn],
               retstar->Xenv[XH1],
               retstar->Xenv[XHe4],
               retstar->Xenv[XC12],
               1.0-nucsyn_totalX(retstar->Xenv));
    if(Xej == NULL)
    {
        Mergeprint("Ejected abundances are not set, mass ejected %g Msun.\n",
                   mass_ejected);
    }
    else
    {
        Mergeprint("Ejected %g Msun with abundances Xn %g XH1 %g XHe4 %g XC12 %g (1-total %g)\n",
                   mass_ejected,
                   Xej[Xn],
                   Xej[XH1],
                   Xej[XHe4],
                   Xej[XC12],
                   1.0-nucsyn_totalX(Xej));
    }
#endif//NUCSYN

    return retstar;
}

static void _to_MS_merger(struct stardata_t * const stardata,
                          struct star_t ** const stars
                          _Xej)
{
    /*
     * Merge two hydrogen main-sequence stars
     */
    _Compute_timescales(stars[MORE_EVOLVED]);
    _Compute_timescales(stars[LESS_EVOLVED]);
    _Compute_timescales(stars[MERGED]);


    /*
     * Hence the mixed age
     */
    if(stardata->preferences->MSMS_merger_age_algorithm == MSMS_MERGER_AGE_ALGORITHM_TOUT)
    {
        _MS_merger_age_Tout(stardata,
                            stars);
    }
    else if(stardata->preferences->MSMS_merger_age_algorithm == MSMS_MERGER_AGE_ALGORITHM_SELMA)
    {
        _MS_merger_age_Selma(stardata,
                             stars);
    }
    else
    {
        Exit_binary_c(BINARY_C_ALGORITHM_BRANCH_FAILURE,
                      "MSMS merger age algorithm %d unknown\n",
                      MSMS_MERGER_AGE_ALGORITHM_TOUT);
    }
}

static void _MS_merger_age_Tout(struct stardata_t * const stardata Maybe_unused,
                                struct star_t ** const stars)
{
    /*
     * Chris Tout's BSE algorithm for the merged-star age
     */
    stars[MERGED]->age = 0.10 * stars[MERGED]->tm *
        (stars[MORE_EVOLVED]->age * stars[MORE_EVOLVED]->mass / Max(1.0,stars[MORE_EVOLVED]->bse->tm) +
         stars[LESS_EVOLVED]->age * stars[LESS_EVOLVED]->mass / Max(1.0,stars[LESS_EVOLVED]->bse->tm))
        / stars[MERGED]->mass;
}

static void _MS_merger_age_Selma(struct stardata_t * const stardata Maybe_unused,
                                 struct star_t ** const stars)
{
    double fracMSage_moreEvolvedStar;
    double fracMSage_lessEvolvedStar;
    double mass_moreEvolvedStar;
    double mass_lessEvolvedStar;
    double mcore_moreEvolvedStar;
    double mcore_lessEvolvedStar;
    double mcore_merger;

    /*
     * Check which star is the most evolved star (can be the
     * secondary, for example systems that experienced slow case A
     * mass transfer )
     */
    if(stars[MORE_EVOLVED]->age/stars[MORE_EVOLVED]->bse->tm > stars[LESS_EVOLVED]->age/stars[LESS_EVOLVED]->bse->tm)
    {
        fracMSage_moreEvolvedStar = stars[MORE_EVOLVED]->age/stars[MORE_EVOLVED]->bse->tm;
        fracMSage_lessEvolvedStar = stars[LESS_EVOLVED]->age/stars[LESS_EVOLVED]->bse->tm;
        mass_moreEvolvedStar = stars[MORE_EVOLVED]->mass;
        mass_lessEvolvedStar = stars[LESS_EVOLVED]->mass;
    }
    else
    {
        fracMSage_moreEvolvedStar = stars[LESS_EVOLVED]->age/stars[LESS_EVOLVED]->bse->tm;
        fracMSage_lessEvolvedStar = stars[MORE_EVOLVED]->age/stars[MORE_EVOLVED]->bse->tm;
        mass_moreEvolvedStar = stars[LESS_EVOLVED]->mass;
        mass_lessEvolvedStar = stars[MORE_EVOLVED]->mass;
    }

    /*
     * Get the size of the convective core at this moment.
     *
     * As a proxy I currently use the size that the core will have
     * at the end of the mains sequence / beginning of the
     * Hertzsprung gap.  This ignores the effect that the
     * convective core is typically larger at zero age and slowly
     * retreats.
     */
    mcore_moreEvolvedStar =
        effective_core_mass_fraction_of_MS_stars(mass_moreEvolvedStar,
                                                 stardata->common.metallicity);

    mcore_lessEvolvedStar =
        effective_core_mass_fraction_of_MS_stars(mass_lessEvolvedStar,
                                                 stardata->common.metallicity);

    mcore_merger = effective_core_mass_fraction_of_MS_stars(stars[MERGED]->mass,
                                                            stardata->common.metallicity);


    /*
     * Assume that the core of the most evolved star settles in
     * the center of the merger and that the other core will form
     * the layers on top of that.
     *
     * Then assume that the core of the merger produces will be
     * convective and mixes. Also assume that the relative age is
     * an indicator of the helium abundance in the core,
     * i.e. age/tms ~ Xhe_center and Xhe_center = helium present
     * in layer of the original star that will become part of the
     * new core / mcore_3
     *
     * Caveats: (I) in reality there may be more helium present in
     * the progenitor stars because we underestimate the current
     * core size, and their may be helium present in layers above
     * the core (II) This may in particular the case for the
     * primary star in slow case A mergers.
     *
     */

    /*
     * This parameter regulates an option for extra mixing of
     * the merger product. The new (convective) core is always
     * assumed to be mixed. If [mixing_in_MS_mergers > 0] an
     * extra fraction of the H-rich of the envelope of the
     * merger is mixed with the core. [mixing_in_MS_mergers = 1]
     * implies that the entire envelope (after mass loss during
     * the merger itself is mixed).
     */
    double mass_mixed_region = mcore_merger;
    if(stardata->preferences->mixingpar_MS_mergers > 0.0)
    {
        mass_mixed_region += (1.0 - mcore_merger)
            * stardata->preferences->mixingpar_MS_mergers;
    }

    Mergeprint("mass mixed region %g\n",mass_mixed_region);
    if(mass_mixed_region > mcore_moreEvolvedStar + mcore_lessEvolvedStar)
    {
        /* new core contains both progenitor cores and some fresh material */
        stars[MERGED]->age = stars[MERGED]->bse->tm / mass_mixed_region *
            ( mcore_moreEvolvedStar * fracMSage_moreEvolvedStar +
              mcore_lessEvolvedStar * fracMSage_lessEvolvedStar );
        Mergeprint("merged star age %g\n",stars[MERGED]->age);
    }

    /*
     * If a high fraction of mass is lost during the merger, the
     * new mixed region might be smaller than the original
     * cores. Assume that the core of the most evolved star sinks
     * to the center. (This might not be true for very extreme
     * mass ratios, where the entropy of the core of the companion
     * is lower than the evolved primary.) Evert Glebbeek find
     * that this may happen for mass ratioas < 0.1 (in models
     * without over shooting). I find that the mass ratios should
     * be more extreme, based on models with overshooting.  In
     * other words, these are binaries that I do not even consider
     * in the population synthesis.
     */
    else if(mass_mixed_region > mcore_moreEvolvedStar)
    {
        /*
         * new core contains core of more evolved star
         * plus part of the core of the less evolved star
         */
        stars[MERGED]->age = stars[MERGED]->bse->tm *
            ( mcore_moreEvolvedStar * fracMSage_moreEvolvedStar +
              (mass_mixed_region - mcore_moreEvolvedStar)*fracMSage_lessEvolvedStar)
            /mass_mixed_region;
        Mergeprint("merged star age %g from tms3 = %g, mcore_moreEvolvedStar = %g, fracMSage_moreEvolvedStar = %g, mass_mixed_region %g, mcore_moreEvolvedStar = %g fracMSage_lessEvolvedStar = %g \n",
                   stars[MERGED]->age,
                   stars[MERGED]->bse->tm,
                   mcore_moreEvolvedStar,
                   fracMSage_moreEvolvedStar,
                   mass_mixed_region,
                   mcore_moreEvolvedStar,
                   fracMSage_lessEvolvedStar);
    }
    else
    {
        /*
         * new core only contains material from the
         * core of the more evolved star (i.e. have the same relative age)
         */
        stars[MERGED]->age = stars[MERGED]->bse->tm * fracMSage_moreEvolvedStar;
        Mergeprint("merged star age = %g from %g, %g\n",
                   stars[MERGED]->age,
                   stars[MERGED]->bse->tm,
                   fracMSage_moreEvolvedStar);
    }
}

static void _to_HeMS_merger(struct stardata_t * const stardata,
                            struct star_t ** const stars
                            _Xej)
{
    set_no_core(stars[MERGED]);
    _Compute_timescales(stars[MERGED]);
    _Compute_timescales(stars[LESS_EVOLVED]);

    Mergeprint("age = %g * (%g / %g) * (%g / %g) \n",
               stars[LESS_EVOLVED]->age,
               stars[MERGED]->bse->tm,
               stars[LESS_EVOLVED]->bse->tm,
               stars[LESS_EVOLVED]->mass,
               stars[MERGED]->mass);

    stars[MERGED]->age = stars[LESS_EVOLVED]->age *
        stars[MERGED]->bse->tm / stars[LESS_EVOLVED]->bse->tm *
        stars[LESS_EVOLVED]->mass / stars[MERGED]->mass;
    Mergeprint("merged age %g\n",stars[MERGED]->age);
}

static void _to_WD_merger(struct stardata_t * const stardata,
                          struct star_t ** const stars
                          _Xej)
{
    /*
     * Assume any merging leaves the WD hot, i.e.
     * similar in energy content to when it is born
     * when age = 0.
     */
    stars[MERGED]->age = 0.0;

    if(WHITE_DWARF(stars[MORE_EVOLVED]->stellar_type) &&
       WHITE_DWARF(stars[LESS_EVOLVED]->stellar_type))
    {
        /*
         * White dwarf - white dwarf mergers
         */
        if(stardata->preferences->WDWD_merger_algorithm == WDWD_MERGER_ALGORITHM_BSE)
        {
            if(stars[MORE_EVOLVED]->stellar_type == COWD &&
               stars[LESS_EVOLVED]->stellar_type == COWD)
            {
                /*
                 * COWD - COWD merger should ignite and
                 * explode above a given mass, leaving no remnant
                 */
                if(Is_not_zero(stardata->preferences->COWD_COWD_explode_above_mass))
                {
                    if(stars[MERGED]->mass > stardata->preferences->COWD_COWD_explode_above_mass)
                    {
                        /*
                         * Star exceeds custom limit, now check if
                         * the star exceeds the Chandrasekhar limit
                         */
                        if(stars[MERGED]->mass > chandrasekhar_mass_wrapper(stardata,
                                                                            stars[MORE_EVOLVED]->mass,
                                                                            Outermost_core_mass(stars[MORE_EVOLVED]),
                                                                            stars[MERGED]->stellar_type))
                        {
                            stars[MERGED]->SN_type = SN_IA_CHAND_Coal;
                        }
                        else
                        {
                            stars[MERGED]->SN_type = SN_IA_SUBCHAND_CO_Coal;
                        }
                    }
                    struct star_t * const news = _New_merged_supernova;

                    if(news)
                    {
                        stellar_structure_make_massless_remnant(stardata,
                                                                news);
                    }
                }
            }
            else if(stars[MORE_EVOLVED]->stellar_type == COWD &&
                    stars[LESS_EVOLVED]->stellar_type == HeWD)
            {
                /*
                 * COWD - HeWD merger
                 */
                if(Is_not_zero(stardata->preferences->HeWD_COWD_explode_above_mass))
                {
                    if(stars[MERGED]->mass > stardata->preferences->HeWD_COWD_explode_above_mass)
                    {
                        if(stars[MERGED]->mass > chandrasekhar_mass_wrapper(stardata,
                                                                            stars[MORE_EVOLVED]->mass,
                                                                            Outermost_core_mass(stars[MORE_EVOLVED]),
                                                                            stars[MERGED]->stellar_type))
                        {
                            stars[MERGED]->SN_type = SN_IA_CHAND_Coal;
                        }
                        else
                        {
                            stars[MERGED]->SN_type = SN_IA_SUBCHAND_CO_Coal;
                        }
                        struct star_t * const news = _New_merged_supernova;
                        if(news)
                        {
                            stellar_structure_make_massless_remnant(stardata,
                                                                    news);
                        }
                    }
                }

                if(star_can_DDet(stardata,stars[MERGED]))
                {
                    stars[MERGED]->SN_type = SN_IA_COWD_DDet;
                    struct star_t * news = new_supernova(stardata,
                                                         stars[MERGED],
                                                         Other_star_struct(stars[MERGED]),
                                                         stars[MERGED]);
                    if(news)
                    {
                        stellar_structure_make_massless_remnant(stardata,
                                                                news);
                    }
                }

                /*
                  stars[MERGED]->stellar_type = HeWD;
                  double mc = stars[MERGED]->core_mass[CORE_He];
                  giant_age(&mc,
                  stars[MERGED]->mass,
                  &stars[MERGED]->stellar_type,
                  &stars[MERGED]->phase_start_mass,
                  &stars[MERGED]->age,
                  stardata,
                  stars[MERGED]);
                  stars[MERGED]->epoch = stardata->model.time - stars[MERGED]->age;
                */
            }
            else if(stars[MORE_EVOLVED]->stellar_type == HeWD &&
                    stars[LESS_EVOLVED]->stellar_type == HeWD)
            {
                /*
                 * BSE assumes HeWD-HeWD mergers explode, binary_c
                 * does not unless ALLOW_HeWD_SUPERNOVAE is defined.
                 */

#ifdef ALLOW_HeWD_SUPERNOVAE
                if(More_or_equal(stars[MERGED].mass,
                                 stardata->preferences->HeWD_HeWD_ignition_mass))
                {
                    stars[MERGED]->SN_type = SN_IA_He;
                    struct star_t * const news = _New_merged_supernova;
                    if(news)
                    {
                        stellar_structure_make_massless_remnant(stardata,
                                                                news);
                    }
                }
#else
                stars[MERGED]->stellar_type = HeWD;
                double mc = stars[MERGED]->core_mass[CORE_He];
                giant_age(&mc,
                          stars[MERGED]->mass,
                          &stars[MERGED]->stellar_type,
                          &stars[MERGED]->phase_start_mass,
                          &stars[MERGED]->age,
                          stardata,
                          stars[MERGED]);
                stars[MERGED]->epoch = stardata->model.time - stars[MERGED]->age;
#endif // ALLOW_HeWD_SUPERNOVAE
            }
        }
        else if(stardata->preferences->WDWD_merger_algorithm == WDWD_MERGER_ALGORITHM_PERETS2019)
        {
            if(stars[MORE_EVOLVED]->stellar_type == COWD &&
               stars[LESS_EVOLVED]->stellar_type == COWD &&
               (stars[MORE_EVOLVED]->hybrid_HeCOWD == TRUE ||
                stars[LESS_EVOLVED]->hybrid_HeCOWD == TRUE) &&
               stars[MORE_EVOLVED]->mass > 0.9 &&
               stars[MORE_EVOLVED]->mass < 1.9)
            {
                if(stars[MERGED]->mass < 1.2)
                {
                    stars[MERGED]->SN_type = SN_IA_Hybrid_HeCOWD_subluminous;
                }
                else
                {
                    stars[MERGED]->SN_type = SN_IA_Hybrid_HeCOWD;
                }
                struct star_t * const news = _New_merged_supernova;
                if(news)
                {
                    stellar_structure_make_massless_remnant(stardata,
                                                            news);
                }
            }
        }
        else if(stardata->preferences->WDWD_merger_algorithm == WDWD_MERGER_ALGORITHM_SATO2016)
        {
            /*
             * Violent COWD-COWD mergers
             */
            if(stars[MORE_EVOLVED]->stellar_type == COWD &&
               stars[LESS_EVOLVED]->stellar_type == COWD &&
               stars[MORE_EVOLVED]->mass >= 0.75 &&
               stars[LESS_EVOLVED]->mass >= 0.75)
            {
                const double Mbig = Max(stars[MORE_EVOLVED]->mass,
                                        stars[LESS_EVOLVED]->mass);
                const double Msmall = Min(stars[MORE_EVOLVED]->mass,
                                          stars[LESS_EVOLVED]->mass);
                const double q_cr = 0.80 * pow(Mbig,-0.84); /* Sato et al 2016, eq 4. */

                /*
                 * This represents (roughly) the case where energy release from C fusion is taken into account.
                 * The case where C fusion is NOT taken into account is 0.82 * Mbig**0.91 (eq 3).
                 * both cases assume co-rotating WDs.
                 */

                if(Msmall/Mbig >= q_cr)
                {
                    stars[MERGED]->SN_type = SN_IA_VIOLENT;
                    struct star_t * const news = _New_merged_supernova;
                    if(news)
                    {
                        stellar_structure_make_massless_remnant(stardata,
                                                                news);
                    }
                }
            }
        }
        else if(stardata->preferences->WDWD_merger_algorithm == WDWD_MERGER_ALGORITHM_HYBRID_PERETS2019_SATO2016)
        {
            if(stars[MORE_EVOLVED]->stellar_type == COWD &&
               stars[LESS_EVOLVED]->stellar_type == COWD)
            {
                /*
                 * SATO pre stuff:
                 * These violent mergers have violent ends
                 */
                const double Mbig = Max(stars[MORE_EVOLVED]->mass,
                                        stars[LESS_EVOLVED]->mass);
                const double Msmall = Min(stars[MORE_EVOLVED]->mass,
                                          stars[LESS_EVOLVED]->mass);
                const double q_cr = 0.80 * pow(Mbig, -0.84); // Sato et al 2016, eq 4.

                /*
                 * This represents (roughly) the case where energy release from C fusion is taken into account.
                 * The case where C fusion is NOT taken into account is 0.82 * Mbig**0.91 (eq 3).
                 *  both cases assume co-rotating WDs.
                 */
                if(
                    /* sato_not_perets */
                    (
                        /* SATOcondition */
                        stars[MORE_EVOLVED]->mass>=0.75 &&
                        stars[LESS_EVOLVED]->mass>=0.75 &&
                        Msmall/Mbig >= q_cr)
                    &&
                    !(
                        //PERETScondition
                        /* one must be a hybrid (or accreted a sufficiently large He shell)*/
                        (stars[MORE_EVOLVED]->hybrid_HeCOWD == TRUE ||
                         stars[LESS_EVOLVED]->hybrid_HeCOWD == TRUE ||
                         stars[MORE_EVOLVED]->dm_novaHe >= MIN_MASS_He_FOR_HYBRID_COWD ||
                         stars[LESS_EVOLVED]->dm_novaHe >= MIN_MASS_He_FOR_HYBRID_COWD) &&
                        /* and the merged mass must be 0.9<M<1.9 */
                        stars[MERGED]->mass > 0.9 &&
                        stars[MERGED]->mass < 1.9
                        // total M > 1.2 -> normal Ia
                        // < 1.2 subluminous Ia
                        // need the other star to exceed the hybrid mass to make a disc
                        //KEMP_NOVAE fixed apparent bug where the 0 in stars[MORE_EVOLVED]->hybrid_HeCOWD==TRUE was a 1.
                        //KEMP_NOVAE included check to see if COWD had ACCRETED a He shell larger
                        // than MIN_MASS_He_FOR_HYBRID_COWD
                        // (set in binary_c parameters (master version set to 1e-3 as of 24 Apr))
                        )
                    )
                {
                    stars[MERGED]->SN_type = SN_IA_VIOLENT;
                    struct star_t * const news = _New_merged_supernova;
                    if(news)
                    {
                        stellar_structure_make_massless_remnant(stardata,
                                                                news);
                    }
                }
                else if(
                    //perets_not_sato
                    !(
                        //SATOcondition
                        stars[MORE_EVOLVED]->mass>=0.75 && stars[LESS_EVOLVED]->mass>=0.75 &&
                        Msmall/Mbig >= q_cr)
                    &&
                    (
                        //PERETScondition
                        /* both must be COWDs */
                        /* one must be a hybrid (or accreted a sufficiently large He shell)*/
                        (stars[MORE_EVOLVED]->hybrid_HeCOWD == TRUE ||
                         stars[LESS_EVOLVED]->hybrid_HeCOWD == TRUE ||
                         stars[MORE_EVOLVED]->dm_novaHe >= MIN_MASS_He_FOR_HYBRID_COWD ||
                         stars[LESS_EVOLVED]->dm_novaHe >= MIN_MASS_He_FOR_HYBRID_COWD) &&
                        /* and the merged mass must be 0.9<M<1.9 */
                        stars[MERGED]->mass > 0.9 &&
                        stars[MERGED]->mass < 1.9
                        )
                    )
                {
                    /*
                     * Hagai says:
                     * If M>1.2Msun we get a Ia.
                     * Around 1.1-1.2 it's in between.
                     * If M<1.1Msun we get a subluminous Ia.
                     *
                     * We may need the mass of the non-hybrid star to exceed that
                     * of the hybrid, but for now ignore this condition.
                     */
                    if(stars[MERGED]->mass < 1.2)
                    {
                        stars[MERGED]->SN_type = SN_IA_Hybrid_HeCOWD_subluminous;
                    }
                    else
                    {
                        stars[MERGED]->SN_type = SN_IA_Hybrid_HeCOWD;
                    }
                    struct star_t * const news = _New_merged_supernova;
                    if(news)
                    {
                        stellar_structure_make_massless_remnant(stardata,
                                                                news);
                    }
                }
                else if(
                    //perets_and_sato
                    (
                        //SATOcondition
                        stars[MORE_EVOLVED]->mass >= 0.75 &&
                        stars[LESS_EVOLVED]->mass >= 0.75 &&
                        Msmall/Mbig >= q_cr)
                    &&
                    (
                        //PERETScondition
                        /* one must be a hybrid (or accreted a sufficiently large He shell) */
                        (stars[MORE_EVOLVED]->hybrid_HeCOWD == TRUE ||
                         stars[LESS_EVOLVED]->hybrid_HeCOWD == TRUE ||
                         stars[MORE_EVOLVED]->dm_novaHe >= MIN_MASS_He_FOR_HYBRID_COWD ||
                         stars[LESS_EVOLVED]->dm_novaHe >= MIN_MASS_He_FOR_HYBRID_COWD) &&
                        /* and the merged mass must be 0.9<M<1.9 */
                        stars[MORE_EVOLVED]->mass > 0.9 &&
                        stars[MORE_EVOLVED]->mass < 1.9
                        )
                    )
                {
                    // if both conditions are met, treat as if perets
                    // only had met but change the interesting flag to signal both were met
                    /*
                     * Hagai says:
                     * If M>1.2Msun we get a Ia.
                     * Around 1.1-1.2 it's in between.
                     * If M<1.1Msun we get a subluminous Ia.
                     *
                     * We may need the mass of the non-hybrid star to exceed that
                     * of the hybrid, but for now ignore this condition.
                     */
                    if(stars[MERGED]->mass < 1.2)
                    {
                        /*
                         * HeCO hybrid merger subluminous Type Ia that also met
                         * conditions for Violent merger
                         */
                        stars[MERGED]->SN_type = SN_IA_Hybrid_HeCOWD_subluminous;

                    }
                    else
                    {
                        /*
                         * flag for HeCO hybrid merger 'normal' Type Ia
                         * that also met conditions for Violent merger
                         */
                        stars[MERGED]->SN_type = SN_IA_Hybrid_HeCOWD;
                    }
                    struct star_t * const news = _New_merged_supernova;
                    if(news)
                    {
                        stellar_structure_make_massless_remnant(stardata,
                                                                news);
                    }
                }
            }
        }
        else if(stardata->preferences->WDWD_merger_algorithm == WDWD_MERGER_ALGORITHM_RUITER2013)
        {
            if(stars[MORE_EVOLVED]->stellar_type == COWD &&
               stars[LESS_EVOLVED]->stellar_type == COWD)
            {
                /*
                 * These violent mergers have violent ends
                 */
                const double Mbig = Max(stars[MORE_EVOLVED]->mass,
                                        stars[LESS_EVOLVED]->mass);
                const double Msmall = Min(stars[MORE_EVOLVED]->mass,
                                          stars[LESS_EVOLVED]->mass);
                const double q_cr = Min(0.8 * pow(Mbig / 0.9,
                                                  -stardata->preferences->eta_violent_WDWD_merger),
                                        1.0);

                /*
                 * ETA_RUITER2013 default (as of 27 apr) == 0.75
                 *
                 * This represents (roughly) the case where energy release from C fusion is taken into account.
                 * The case where C fusion is NOT taken into account is 0.82 * Mbig**0.91 (eq 3).
                 * both cases assume co-rotating WDs.
                 */
                if(Msmall/Mbig >= q_cr)
                {
                    stars[MERGED]->SN_type = SN_IA_VIOLENT;
                    struct star_t * const news = _New_merged_supernova;
                    if(news)
                    {
                        stellar_structure_make_massless_remnant(stardata,
                                                                news);
                    }
                }
            }
        }
        else if(stardata->preferences->WDWD_merger_algorithm == WDWD_MERGER_ALGORITHM_CHEN2016)
        {
            // TODO
        }
    }
}

static void _to_NS_BH_merger(struct stardata_t * const stardata,
                             struct star_t ** const stars
                             _Xej)
{
    /*
     * Most evolved star must be NS or BH.
     * Secondary can be anything.
     */
    if(!POST_SN_OBJECT(stars[LESS_EVOLVED]->stellar_type))
    {
        /*
         * Non-NS/BH merging with a NS/BH
         */
        if(stars[LESS_EVOLVED]->stellar_type<=TPAGB)
        {
            /*
             * Hydrogen-rich object: pretend it's
             * an EAGB star.
             */
            stars[MERGED]->TZO = TZO_MERGER;
            stars[MERGED]->stellar_type = EAGB;

            stack_cores(stars[MERGED],
                        stars[MERGED]->mass);

            {
                char * corestring = core_string(stars[MERGED],FALSE);
                Mergeprint("merged TZO star has mgrav=%g mbary=%g, cores %s : starnum %d, SN %d\n",
                           stars[MERGED]->mass,
                           stars[MERGED]->baryonic_mass,
                           corestring,
                           stars[MERGED]->starnum,
                           stars[MERGED]->SN_type);
                Safe_free(corestring);
            }

            /*
             * Compute age for BSE
             */
            giant_age(&stars[MERGED]->core_mass[CORE_He],
                      stars[MERGED]->mass,
                      &stars[MERGED]->stellar_type,
                      &stars[MERGED]->phase_start_mass,
                      &stars[MERGED]->age,
                      stardata,
                      stars[MERGED]);

            Mergeprint("post-giant_age merged star has mgrav %g mbary %g, cores He %g CO %g n %g : age %g, radius %g, starnum %d, SN %d\n",
                       stars[MERGED]->mass,
                       stars[MERGED]->baryonic_mass,
                       stars[MERGED]->core_mass[CORE_He],
                       stars[MERGED]->core_mass[CORE_CO],
                       stars[MERGED]->core_mass[CORE_NEUTRON],
                       stars[MERGED]->age,
                       stars[MERGED]->radius,
                       stars[MERGED]->starnum,
                       stars[MERGED]->SN_type);
        }
        else if(stars[LESS_EVOLVED]->stellar_type<=HeGB)
        {
            /*
             * Helium-rich object
             */
            stars[MERGED]->TZO = TZO_MERGER;
            stars[MERGED]->stellar_type = HeHG;

            stack_cores(stars[MERGED],stars[MERGED]->mass);

            /*
             * Compute age for BSE
             */
            giant_age(&stars[MERGED]->core_mass[CORE_CO],
                      stars[MERGED]->mass,
                      &stars[MERGED]->stellar_type,
                      &stars[MERGED]->phase_start_mass,
                      &stars[MERGED]->age,
                      stardata,
                      stars[MERGED]);
        }
        else
        {
            /*
             * White dwarf - neutron star merger
             *
             * It's not clear what this does! But it's
             * not a classical TZO.
             */
            stars[MERGED]->TZO = TZO_NONE;
        }
    }
    else if((stars[MORE_EVOLVED]->stellar_type == NEUTRON_STAR ||
             stars[MORE_EVOLVED]->stellar_type == BLACK_HOLE)
            &&
            (stars[LESS_EVOLVED]->stellar_type == NEUTRON_STAR ||
             stars[MORE_EVOLVED]->stellar_type == NEUTRON_STAR))
    {
        /*
         * NSNS, NSBH or BHBH merger
         *
         * We don't need to do anything peculiar,
         * the merged star just is.
         *
         * We do require specialist NSNS/NSBH yields,
         * however.
         */
#ifdef NUCSYN
        Dprint("Call nucsyn's NSNS function\n");
        nucsyn_NSNS(stardata,Xej);
        Dprint("nucsyn gives n=%g H1=%g He4=%g C12=%g 1-Xtot=%g\n",
               Xej[Xn],Xej[XH1],Xej[XHe4],Xej[XC12],1.0-nucsyn_totalX(Xej));
#endif // NUCSYN
    }
    else
    {
        /* BHBH merger */
    }
}

static void _merge_cores(struct stardata_t * const stardata,
                         struct star_t ** const stars)
{
    /*
     * Merge the stellar cores.
     *
     * There are some special cases with which
     * we should deal first.
     */
    if(stars[MERGED]->stellar_type <= CHeB)
    {
        /*
         * Special case : all CO and He must be
         * mixed. We do this by simply erasing the CO core.
         */
        stars[MERGED]->core_mass[CORE_CO] = 0.0;
    }

    if(stars[MORE_EVOLVED]->stellar_type == HeMS &&
       stars[LESS_EVOLVED]->stellar_type <= TPAGB)
    {
        /*
         * Hydrogen-rich star adds the HeMS star, which has
         * no defined core, to its helium core, so pretend the helium star
         * has a core.
         */
        stars[MORE_EVOLVED]->core_mass[CORE_He] = stars[MORE_EVOLVED]->mass;
    }

    if(stars[MERGED]->stellar_type == TPAGB)
    {
        if((stars[MORE_EVOLVED]->stellar_type == HeHG ||
            stars[MORE_EVOLVED]->stellar_type == HeGB) &&
           stars[LESS_EVOLVED]->stellar_type <= MAIN_SEQUENCE)
        {
            /*
             * Special case: helium giant and main sequence
             * star mix to make the TPAGB's (helium/hydrogen)
             * envelope.
             */
            stars[MORE_EVOLVED]->core_mass[CORE_He] = 0.0;
        }
        else if(NAKED_HELIUM_STAR(stars[MORE_EVOLVED]->stellar_type) ||
                stars[MORE_EVOLVED]->stellar_type == HeWD)
        {
            /*
             * Formation of a star with a hydrogen envelope
             * and a helium companion. The helium companion
             * has no helium core, i.e.
             * stars[MORE_EVOLVED]->core_mass[CORE_He] == 0.0,
             * so we set its helium core mass
             * equal to its total mass, then let merge_cores
             * deal with the summation.
             */
            stars[MORE_EVOLVED]->core_mass[CORE_He] = stars[MORE_EVOLVED]->mass;
        }
    }

    merge_cores(stardata,
                stars[MORE_EVOLVED],
                stars[LESS_EVOLVED],
                stars[MERGED]);

    /*
     * HeWD onto COWD could make a helium giant *or*
     * explode as a double detonation SNIa.
     *
     * We use the star_can_DDet to determine whether
     * an explosion is possible.
     */
    if(stars[MORE_EVOLVED]->stellar_type == COWD &&
       stars[LESS_EVOLVED]->stellar_type == HeWD)
    {
        const Stellar_type st_was = stars[MERGED]->stellar_type;
        const int deny_was = stars[MERGED]->deny_SN;

        /* set up star so star_can_DDet works */
        stars[MERGED]->stellar_type = COWD;
        stars[MERGED]->deny_SN = FALSE;

        if(star_can_DDet(stardata,stars[MERGED]))
        {
            /* leave as hybrid COWD */
            stars[MERGED]->hybrid_HeCOWD = TRUE;
            stars[MERGED]->core_mass[CORE_He] = stars[MORE_EVOLVED]->mass + stars[LESS_EVOLVED]->mass;
        }
        else
        {
            /* return to helium star */
            stars[MERGED]->stellar_type  = st_was;
        }
        stars[MERGED]->deny_SN = deny_was;
    }
}

static void _to_hydrogen_giant_merger(struct stardata_t * const stardata,
                                      struct star_t ** const stars
                                      _Xej)
{

    /*
     * Recompute new giant age for merged star
     */
    _Compute_timescales(stars[MERGED]);
    double mc = stars[MERGED]->core_mass[CORE_He];
    Mergeprint("Giant age with type = %d, M = %g, M0 = %g, Mc = %g\n",
               stars[MERGED]->stellar_type,
               stars[MERGED]->mass,
               stars[MERGED]->phase_start_mass,
               mc);
    giant_age(&mc,
              stars[MERGED]->mass,
              &stars[MERGED]->stellar_type,
              &stars[MERGED]->phase_start_mass,
              &stars[MERGED]->age,
              stardata,
              stars[MERGED]);
    stars[MERGED]->age *= (1.0 + 1e-6);
    {
        char * corestring = core_string(stars[MERGED],FALSE);
        Mergeprint("giant aged to age = %g, phase start mass %g, cores %s\n",
                   stars[MERGED]->age,
                   stars[MERGED]->phase_start_mass,
                   corestring);
        Safe_free(corestring);
    }
    stars[MERGED]->epoch = stardata->model.time - stars[MERGED]->age;

    if(stars[MERGED]->stellar_type == TPAGB)
    {
        /*
         * Take TPAGB properties from more-evolved star
         */
        stars[MERGED]->num_thermal_pulses = Max(stars[MORE_EVOLVED]->num_thermal_pulses,
                                                stars[LESS_EVOLVED]->num_thermal_pulses);
        stars[MERGED]->num_thermal_pulses_since_mcmin = Max(stars[MORE_EVOLVED]->num_thermal_pulses_since_mcmin,
                                                            stars[LESS_EVOLVED]->num_thermal_pulses_since_mcmin);
        stars[MERGED]->prev_tagb = Max(stars[MORE_EVOLVED]->prev_tagb,
                                       stars[LESS_EVOLVED]->prev_tagb);

    }
    else
    {
        stars[MERGED]->num_thermal_pulses = -2.0;
        stars[MERGED]->num_thermal_pulses_since_mcmin = -0.0;
        stars[MERGED]->prev_tagb = 0.0;
    }

    if(stars[MERGED]->stellar_type == EAGB ||
       stars[MERGED]->stellar_type == TPAGB)
    {
        stars[MERGED]->mcx_EAGB = stars[MORE_EVOLVED]->mcx_EAGB + stars[LESS_EVOLVED]->mcx_EAGB;
    }

}


static void _to_helium_giant_merger(struct stardata_t * const stardata,
                                    struct star_t ** const stars
                                    _Xej)
{
    /*
     * Recompute new giant age for merged star
     */
    _Compute_timescales(stars[MERGED]);

    /*
     * There is no helium core.
     */
    stars[MERGED]->core_mass[CORE_He] = 0.0;

    /*
     * ONeWD core should have a (thin) CO layer
     * just above it: this can happen when a
     * helium star or WD merges with an ONeWD.
     */
    if(!Core_exists(stars[MERGED],CORE_CO))
    {
        stars[MERGED]->core_mass[CORE_CO] = stars[MERGED]->core_mass[CORE_ONe];
    }

    /*
     * Acquire correct stellar age
     */
    double mc = stars[MERGED]->core_mass[CORE_CO];
    giant_age(&mc,
              stars[MERGED]->mass,
              &stars[MERGED]->stellar_type,
              &stars[MERGED]->phase_start_mass,
              &stars[MERGED]->age,
              stardata,
              stars[MERGED]);
    stars[MERGED]->age *= (1.0 + 1e-6);
    stars[MERGED]->epoch = stardata->model.time - stars[MERGED]->age;
#ifdef NUCSYN
    if(Xej != NULL)
    {
        Copy_abundances(Xej, stars[MERGED]->Xenv);
        Dprint("acquire Xej from donor 1-Xej=%g\n",
               1.0-nucsyn_totalX(Xej));
    }
#endif // NUCSYN
}



static void _to_massless_merger(struct stardata_t * const stardata Maybe_unused,
                                struct star_t ** const stars Maybe_unused
                                _Xej)
{
    stellar_structure_make_massless_remnant(stardata,
                                            stars[MERGED]);
}

#ifdef NUCSYN
Nonnull_some_arguments(4)
static void _initial_nucsyn(struct stardata_t * const stardata Maybe_unused,
                            struct star_t ** const stars Maybe_unused,
                            const double merged_mass,
                            Abundance * const Xej)
{
    /*
     * Mix the stars' accretion layers into their envelope
     * on the assumption that merging them will provide
     * the energy, hence mixing, to do this.
     *
     * Xej is passed in and is not NULL
     */
    const double menv[2] = {
        envelope_mass(stars[MORE_EVOLVED]),
        envelope_mass(stars[LESS_EVOLVED])
    };
    const double mass_ejected =
        stars[MORE_EVOLVED]->mass +
        stars[LESS_EVOLVED]->mass -
        merged_mass;


#define _mix(N)                                             \
    if(Is_not_zero(menv[N]))                                \
    {                                                       \
        nucsyn_mix_accretion_layer_and_envelope(stardata,   \
                                                stars[N],   \
                                                menv[N]);   \
    }
    _mix(0);
    _mix(1);
    Mergeprint("_mixed\n");

#ifdef NUCSYN_STRIP_AND_MIX
    mix_down_to_core(&stars[MORE_EVOLVED]);
    mix_down_to_core(&stars[LESS_EVOLVED]);
#endif // NUCSYN_STRIP_AND_MIX

    /*
     * Force first dredge up
     */
#if defined NUCSYN_FORCE_DUP_IN_MERGERS &&      \
    defined NUCSYN_FIRST_DREDGE_UP
    Foreach_star(star)
    {
        if(
            (star->stellar_type==HERTZSPRUNG_GAP ||
             star->stellar_type==GIANT_BRANCH )
            &&
            star->first_dredge_up==FALSE
#ifdef NUCSYN_WR
            && star->effective_zams_mass<NUCSYN_WR_MASS_BREAK
#endif
            )
        {
            nucsyn_set_1st_dup_abunds(star->Xenv,
                                      star->mass,
                                      star->stellar_type,
                                      star,
                                      stardata,
                                      TRUE);
            star->first_dredge_up = TRUE;
            Mergeprint("forced 1dup in star %d, 1-Xenv %g\n",
                       star->starnum,
                       1.0-nucsyn_totalX(star->Xenv));
        }
    }
#endif // NUCSYN_FORCE_DUP_IN_MERGERS && NUCSYN_FIRST_DREDGE_UP

    /*
     * Compute ejecta abundances
     */
    Clear_isotope_array(Xej);

    if(stars[MERGED]->stellar_type <= MAIN_SEQUENCE ||
       stars[MERGED]->stellar_type == HeMS)
    {
        /*
         * Main-sequence stars simply merge all their
         * mass before ejecting some.
         *
         * HeWD-HeWD and HeWD-HeMS mergers also make
         * HeMS stars, but then they're just helium
         * which is what we'll get.
         */
        nucsyn_dilute_shell_to(stars[MORE_EVOLVED]->mass,
                               stars[MORE_EVOLVED]->Xenv,
                               stars[LESS_EVOLVED]->mass,
                               stars[LESS_EVOLVED]->Xenv,
                               Xej);

        if(stars[MERGED]->stellar_type <= MAIN_SEQUENCE)
        {
            /*
             * Reset Xinit
             */
            Copy_abundances(Xej,
                            stars[MERGED]->Xinit);
        }
        Mergeprint("MS merger\n");
    }
    else if(stars[MERGED]->stellar_type >= HeWD)
    {
        /*
         * Compact object merger.
         *
         * First eject the least evolved/lease massive.
         */
        Mergeprint("Compact object merger\n");
        const Star_number first =
            stars[MORE_EVOLVED]->stellar_type == stars[LESS_EVOLVED]->stellar_type ?
            (stars[MORE_EVOLVED]->mass < stars[LESS_EVOLVED]->mass ? 0 : 1) :
            1;
        const Star_number second = Other_star(first);
        Mergeprint("first %d second %d\n",first,second);

        if(mass_ejected > stars[first]->mass)
        {
            /*
             * All of star "first" is ejected, and some
             * of star "second"
             */
            const double dm2 = mass_ejected - stars[first]->mass;
            nucsyn_dilute_shell_to(stars[first]->mass,
                                   stars[first]->Xenv,
                                   dm2,
                                   stars[second]->Xenv,
                                   Xej);
        }
        else
        {
            /*
             * Only star "first" is required.
             */
            Copy_abundances(stars[first]->Xenv,
                            Xej);
        }
    }
    else
    {
        /*
         * Mix envelopes assuming the cores are not
         * stripped. Here, set Xej to be the mixed
         * envelope abundance.
         */
        const double envelope_mass = menv[0] + menv[1];
        nucsyn_dilute_shell_to(menv[0],
                               stars[MORE_EVOLVED]->Xenv,
                               menv[1],
                               stars[LESS_EVOLVED]->Xenv,
                               Xej);
        Dprint("mix envelopes 1-X more evolved %g less evolved %g to 1-Xej = %g\n",
               1.0-nucsyn_totalX(stars[MORE_EVOLVED]->Xenv),
               1.0-nucsyn_totalX(stars[LESS_EVOLVED]->Xenv),
               1.0-nucsyn_totalX(Xej));

        /*
         * But if we eject more mass than is available
         * in the envelopes, we have to get some mass
         * from the cores. Follow the compact-object
         * prescription above in this case.
         *
         * Note that core stellar types are MASSLESS_REMNANT
         * if there is no core, in which case we use the the
         * actual stellar type (this should not happen
         * with stars that have envelopes).
         *
         * You can force core dredge up from stars[MORE_EVOLVED] using
         * stardata->preferences->degenerate_core_merger_dredgeup_fraction
         *
         */

#define _core_stellar_type(N)                                   \
        (                                                       \
            stars[N]->core_stellar_type == MASSLESS_REMNANT ?   \
            stars[N]->stellar_type :                            \
            stars[N]->core_stellar_type                         \
            )

        const Star_number least_compact =
            _core_stellar_type(0) == _core_stellar_type(1) ?
            (Outermost_core_mass(stars[MORE_EVOLVED]) < Outermost_core_mass(stars[LESS_EVOLVED]) ? 0 : 1) :
            1;
        const Star_number most_compact = Other_star(least_compact);
        Dprint("compact most %d least %d\n",most_compact,least_compact);

        /*
         * If we have two compact cores merging, dredge-up some carbon
         * into the envelope e.g. to make R stars.
         */
        if(!In_range(stardata->preferences->degenerate_core_merger_dredgeup_fraction,0.0,1.0))
        {
            Exit_binary_c(BINARY_C_ALGORITHM_OUT_OF_RANGE,
                          "degenerate_core_merger_dredgeup_fraction is %g but must be in the range 0.0 to 1.0.\n",
                          stardata->preferences->degenerate_core_merger_dredgeup_fraction);
        }

        if(Is_not_zero(stardata->preferences->degenerate_core_merger_dredgeup_fraction))
        {
            const double dmdredge = Outermost_core_mass(stars[most_compact]) *
                stardata->preferences->degenerate_core_merger_dredgeup_fraction;
            Abundance * Xdredge = New_isotope_array_from(Xej);
            nucsyn_burn_CNO_completely(Xdredge);
            Dprint("post CNO complete burn 1-Xdredge = %g\n",
                   1.0 - nucsyn_totalX(Xdredge));

            /*
             * Abundances from Onno for at the surface of the core
             */
            const double xc = 0.7;
            const double xo = 0.15;

            /*
             * Add C12, O16 and burn N14 to Ne22
             */
            Xdredge[XC12] = xc;
            Xdredge[XO16] = xo;
            Xdredge[XN14] = 0.0;
            Xdredge[XNe22] += Xdredge[XN14];
            Xdredge[XHe4] = 0.0;
            Xdredge[XHe4] = 1.0 - nucsyn_totalX(Xdredge);
            Dprint("post Ne22 1-Xdredge = %g\n",
                   1.0 - nucsyn_totalX(Xdredge));

            /*
             * Replace a mass dmdredge with dredged-up
             * material.
             */
            nucsyn_dilute_shell(envelope_mass - dmdredge,
                                Xej,
                                dmdredge,
                                Xdredge);

            Safe_free(Xdredge);
        }

        if(mass_ejected > envelope_mass)
        {
            const double dmcore = Max(0.0,
                                      mass_ejected - envelope_mass);
            if(dmcore > TINY)
            {
                /*
                 * We should eject some core material.
                 *
                 * Remove dm1 from "least_compact", dm2 from "most_compact"
                 */
                const double dm1 = Max(0.0,Min(dmcore,
                                               Outermost_core_mass(stars[least_compact])));
                const double dm2 = Max(0.0,Min(dmcore - dm1,
                                               Outermost_core_mass(stars[most_compact])));

                Dprint("Eject %g + %g of core material\n",dm1,dm2);
                if(Is_not_zero(dm1))
                {
                    Abundance * X1 = nucsyn_core_abundances(stardata,
                                                            stars[least_compact]);
                    Dprint("1-X1 = %g\n",
                           1.0 - nucsyn_totalX(X1));
                    nucsyn_dilute_shell(envelope_mass,
                                        Xej,
                                        dm1,
                                        X1);
                    Safe_free(X1);
                    Mergeprint("Xej 1-Xej=%g\n",
                               1.0-nucsyn_totalX(Xej));
                }
                if(Is_not_zero(dm2))
                {
                    Abundance * X2 = nucsyn_core_abundances(stardata,
                                                            stars[most_compact]);
                    Dprint("1-X2 = %g\n",
                           1.0 - nucsyn_totalX(X2));
                    nucsyn_dilute_shell(envelope_mass,
                                        Xej,
                                        dm2,
                                        X2);
                    Safe_free(X2);
                }
            }
        }
    }
    Mergeprint("Xej Xn=%g 1-Xej=%g\n",
               Xej[Xn],
               1.0-nucsyn_totalX(Xej));
}


static void _merge_yields(struct star_t ** const stars)
{
    /*
     * Merge the yields of the two stars passed in
     */
    Nucsyn_isotope_stride_loop(
        stars[MERGED]->Xyield[i] += stars[MASSLESS]->Xyield[i];
        );

#ifdef NUCSYN_ID_SOURCES
    Sourceloop(j)
    {
        if(stars[MASSLESS]->Xsource[j] != NULL)
        {
            if(stars[MERGED]->Xsource[j] == NULL)
            {
                stars[MERGED]->Xsource[j] =
                    New_isotope_array_from(stars[MASSLESS]->Xsource[j]);
            }
            else
            {
                Nucsyn_isotope_stride_loop(
                    stars[MERGED]->Xsource[j][i] +=
                        stars[MASSLESS]->Xsource[j][i];
                    );
            }
        }
#endif // NUCSYN_ID_SOURCES
    }
    stars[MERGED]->other_yield += stars[MASSLESS]->other_yield;

    /*
     * And zero the massless remnant's yields
     */
    nucsyn_zero_yields(stars[MASSLESS]);
}

#endif //NUCSYN

#endif//BSE
