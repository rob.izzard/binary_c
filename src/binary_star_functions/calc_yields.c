#include "../binary_c.h"
No_empty_translation_unit_warning;


/*
 * Wrapper for nucsyn_calc_yields that also saves the ensemble
 * variables so we can track mass loss sources
 */

void calc_yields(struct stardata_t * Restrict const stardata Maybe_unused,
                 struct star_t * const star Maybe_unused,
                 const double dmlose Maybe_unused, /* mass lost as wind/otherwise */
#ifdef NUCSYN
                 Abundance * const Xlose Maybe_unused, /* abundance of this "wind" */
#endif
                 const double dmacc Maybe_unused, /* mass gained by accretion */
#ifdef NUCSYN
                 Abundance * const Xacc Maybe_unused, /* abundance of this accretion */
#endif //NUCSYN
                 const Star_number starnum Maybe_unused,
                 const int final Maybe_unused,
                 const Yield_source source Maybe_unused
    )
{
#ifdef NUCSYN
    nucsyn_calc_yields(stardata,
                       star,
                       dmlose,
                       Xlose,
                       dmacc,
                       Xacc,
                       starnum,
                       final,
                       source);
#endif //NUCSYN

#ifdef STELLAR_POPULATIONS_ENSEMBLE
    if(source != SOURCE_UNKNOWN)
    {
        stardata->model.ensemble_yield[source] += dmlose - dmacc;
    }
#endif // STELLAR_POPULATIONS_ENSEMBLE

}
