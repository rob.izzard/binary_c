#pragma once
#ifndef BINARY_C_BSE_H
#define BINARY_C_BSE_H


/* Stuff used in hrdiag/hrdiag.c */
/* (LUM0 and KAP are used in calc_stellar_wind_mass_loss as well) */
#define LUM0 7.0E+04
#define KAP -0.5

/* 
 * for version prior to 2007 updates use these
 #define AHE 6.964
 #define ACO 48.503
*/
/* post-2007 updates, use these */
#define AHE 4.0
#define ACO 16.0

#define TAUMIN 5.0E-8
#define MLP 12.0

/* These are used in deltat.c for changing the timestep */
/* Main Sequence */
//#define PTS1 0.05
/* Giant Branch, Core Helium Branch, Asymptotic Giant Branch, Helium Giant
 * Branch */
//#define PTS2 0.01
/* Helium Giant, Helius Main Sequence */
//#define PTS3 0.02

#endif // BINARY_C_BSE_H
