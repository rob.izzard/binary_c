#pragma once
#ifndef CONTROL_PROTOTYPES_H
#define CONTROL_PROTOTYPES_H

struct star_t * next_nuclear_burning_star_struct(
    Star_number * const k,
    const struct stardata_t * Restrict const stardata);

struct star_t * next_evolving_star_struct(
    Star_number * const k,
    const struct stardata_t * Restrict const stardata);

Star_number Pure_function next_evolving_star(
    Star_number k,
    const struct stardata_t * Restrict const stardata);

int next_nuclear_burning_star(Star_number k,
                              const struct stardata_t * Restrict const stardata);

Boolean check_stopfile(struct stardata_t * const stardata);

#endif // CONTROL_PROTOTYPES_H
