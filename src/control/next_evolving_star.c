#include "../binary_c.h"
No_empty_translation_unit_warning;


/*
 * If we're currently looping over star k,
 * return the next star number which is evolving
 * (i.e. not a MASSLESS_REMNANT or genuinely massless).
 *
 * In case we run off the end of the zero_age.multiplicity
 * return -1, and if a negative number is passed in
 * also return -1.
 *
 * Note: this function *requires* that
 * stardata->preferences->zero_age.multiplicity
 * is set.
 *
 * The loop is limited to NUMBER_OF_STARS (==2)
 * because the algorithms always assume either
 * single or binary. The multiplicity can be
 * higher.
 */

Star_number Pure_function next_evolving_star(
    Star_number k,
    const struct stardata_t * Restrict const stardata)
{
    const Multiplicity max_multiplicity =
        Min((Multiplicity)NUMBER_OF_STARS,
            stardata->preferences->zero_age.multiplicity);
    Loop_forever
    {
        k++;
        if(k<(Multiplicity)max_multiplicity &&
           (stardata->star[k].stellar_type == MASSLESS_REMNANT ||
            Is_zero(stardata->star[k].mass)))
        {
            continue;
        }
        else
        {
            if(k>=(Multiplicity)max_multiplicity) k = -1;
            break;
        }
    }
    return k;
}
