#include "../binary_c.h"
No_empty_translation_unit_warning;


/*
 * If we're currently looping over star k,
 * return the next star number which is nuclear burning
 *
 * In case we run off the end of the max_multiplicity,
 * return -1, and if a negative number is passed in
 * also return -1.
 */

int Pure_function next_nuclear_burning_star(
    Star_number k,
    const struct stardata_t * Restrict const stardata)
{
    const Multiplicity max_multiplicity =
        Min((Multiplicity)NUMBER_OF_STARS,
            stardata->preferences->zero_age.multiplicity);
    Loop_forever
    {
        k++;
        if(k<(Multiplicity)max_multiplicity &&
           POST_NUCLEAR_BURNING(stardata->star[k].stellar_type))
        {
            continue;
        }
        else
        {
            if(k>=(Multiplicity)max_multiplicity) k = -1;
            break;
        }
    }
    return k;
}
