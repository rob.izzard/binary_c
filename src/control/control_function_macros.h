#pragma once
#ifndef CONTROL_FUNCTION_MACROS_H
#define CONTROL_FUNCTION_MACROS_H

/*
 * Function macros that control program flow involving
 * star and stardata structs, and similar, also loop unrolling.
 */

//#include "loop_unroller.h"

/*
 * Macros for making and copying stardata_t structs
 */
#define New_stardata_mask new_stardata(NULL)

/* move stardata */
#define Move_stardata(FROM,TO)                                  \
    ((struct stardata_t*)memmove((TO),                          \
                                 (FROM),                        \
                                 sizeof(struct stardata_t)))

/* New stardata from another stardata */
#define New_stardata_from(STARDATA)             \
    New_stardata_from_implementation(           \
        New_stardata_from,                      \
        __COUNTER__,                            \
        (STARDATA)                              \
        )
#define New_stardata_from_implementation(LABEL,LINE,STARDATA)   \
    __extension__                                               \
    ({                                                          \
        struct stardata_t * Concat3(s,LABEL,LINE)               \
            = new_stardata((STARDATA)->preferences);            \
        copy_stardata(                                          \
            (STARDATA),                                         \
            Concat3(s,LABEL,LINE),                              \
            COPY_STARDATA_PREVIOUS_NONE,                        \
            COPY_STARDATA_PERSISTENT_FROM_POINTER               \
            );                                                  \
        Concat3(s,LABEL,LINE);                                  \
            })

#define Copy_previous_stardatas(FROM,TO)        \
    Copy_previous_stardatas_implementation(     \
        (FROM),                                 \
        (TO),                                   \
        Copy_previous_stardatas,                \
        __COUNTER__)
#define Copy_previous_stardatas_implementation(FROM,                \
                                               TO,                  \
                                               LABEL,               \
                                               LINE)                \
    {                                                               \
        /* previous_stardatas is a new copy */                      \
        (TO)->previous_stardatas =                                  \
            Malloc(sizeof(struct stardata_t *) *                    \
                   (FROM)->n_previous_stardatas);                   \
                                                                    \
        /* copy previous stardatas from FROM to TO */               \
        Star_number Concat3(__i,LABEL,LINE);                        \
        for(Concat3(__i,LABEL,LINE)=0;                              \
            Concat3(__i,LABEL,LINE)<(FROM)->n_previous_stardatas;   \
            Concat3(__i,LABEL,LINE)++)                              \
        {                                                           \
            (TO)->previous_stardatas[                               \
                Concat3(__i,LABEL,LINE)] =                          \
                (FROM)->previous_stardatas[                         \
                    Concat3(__i,LABEL,LINE)] == NULL ?              \
                NULL :                                              \
                New_stardata_from((FROM)->previous_stardatas[       \
                                      Concat3(__i,LABEL,LINE)]);    \
        }                                                           \
                                                                    \
        /* administration */                                        \
        (TO)->n_previous_stardatas = (FROM)->n_previous_stardatas;  \
        (TO)->previous_stardata = (TO)->previous_stardatas[0];      \
                                                                    \
    }


/*
 * Macros as above, but including the option to copy
 * the previous_stardatas stack.
 */
#define New_deep_stardata_from(STARDATA)        \
    New_deep_stardata_from_implementation(      \
        (STARDATA),                             \
        New_deep_stardata_from,                 \
        __COUNTER__)

#define New_deep_stardata_from_implementation(          \
    STARDATA,                                           \
    LABEL,                                              \
    LINE)                                               \
    __extension__                                       \
    ({                                                  \
        struct stardata_t * Concat3(s,LABEL,LINE)       \
            = new_stardata((STARDATA)->preferences);    \
        copy_stardata((STARDATA),                       \
                      Concat3(s,LABEL,LINE),            \
                      COPY_STARDATA_PREVIOUS_COPY,      \
                      COPY_STARDATA_PERSISTENT_COPY);   \
        Concat3(s,LABEL,LINE);                          \
    })

#define Clear_deep_stardata(STARDATA)                   \
    {                                                   \
        free_previous_stardatas(STARDATA);              \
        erase_events(STARDATA);                         \
        (STARDATA)->n_previous_stardatas = 0 ;          \
        (STARDATA)->previous_stardata = NULL;           \
        memset((STARDATA),0,sizeof(struct stardata_t)); \
    }



/*
 * Macros for making and copying star_t structs.
 */
#define New_star new_star(stardata,NULL)
#define New_clear_star new_star(stardata,NULL)
#define New_star_from(STAR) new_star(stardata,(STAR))

#define Swap_stars(STAR1,STAR2) Swap_stars_implementation(  \
        (STAR1),                                            \
        (STAR2),                                            \
        Swap_stars,                                         \
        __COUNTER__)
#define Swap_stars_implementation(STAR1,STAR2,LABEL,LINE)   \
    {                                                       \
        struct star_t * Concat3(_s,LABEL,LINE) = New_star;  \
        copy_star(stardata,(STAR2),Concat3(_s,LABEL,LINE)); \
        copy_star(stardata,(STAR1),(STAR2));                \
        copy_star(stardata,Concat3(_s,LABEL,LINE),(STAR1)); \
        free_star(&Concat3(_s,LABEL,LINE));                 \
    }
#define Swap_star_pointers(STAR1,STAR2)         \
    {                                           \
        struct star_t * const __x = (STAR1);    \
        (STAR1) = (STAR2);                      \
        (STAR2) = __x;                          \
    }

/*
 * macro definition of the index of the other star
 */
#define Other_star(N) ((N)==0 ? 1 : 0)
#define Other_star_struct(S) (&stardata->star[Other_star((S)->starnum)])
#define Other_star_struct_in(STARDATA,S) (&(STARDATA)->star[Other_star((S)->starnum)])

/*
 * macro definition of the star on the previous timestep
 */
#define Star_previous(S) (stardata->previous_stardata->star[(S)->starnum])


/* wrapper for Concat : expands A,B */
#define ConcatC(A,B) Concat(A,B)

/*
 * loop over the stars using variable N (which
 * you have to declare as an integer type)
 *
 * Starloop(k), with one argument which is the star number, k,
 * loops over stars up to the minimum
 * of NUMBER_OF_STARS or the multiplicity. This is
 * what you will usually want to use.
 *
 * Starloop(k,START) starts the loop at START.
 * Starloop(k,START,END) starts at START and ends at END.
 *
 * Multiplicity_Starloop() loops up to multiplicity.
 *
 * Number_of_stars_Starloop() loops up to NUMBER_OF_STARS.
 *
 * Evolving_Starloop() is like Starloop() for stars
 * that are not MASSLESS_REMNANT.
 *
 * Nuclear_burning_Starloop() is like Starloop() for stars
 * that have ongoing nuclear burning.
 *
 * If preferences or stardata is NULL, default _Starloop_N
 * to NUMBER_OF_STARS
 */

#define _Starloop_N                                                     \
    ((Star_number)Min(                                                  \
        (Star_number)(NUMBER_OF_STARS),                                 \
        ((stardata != NULL && stardata->preferences != NULL)            \
         ? ((Star_number)stardata->preferences->zero_age.multiplicity)  \
         : ((Star_number)10000))                                        \
        ))

#define _Starloop(N,START,LIMIT)                    \
    for((N)=(Star_number)(START);                   \
        (((Star_number)(N)<(Star_number)(LIMIT)));  \
        (N)++)

#define Starloop(...)                           \
    ConcatC(Starloop,                           \
            NARGS(__VA_ARGS__)                  \
        )                                       \
        (__VA_ARGS__)

#define Starloop1(N)                            \
    _Starloop((N),0,_Starloop_N)
#define Starloop2(N,START)                      \
    _Starloop((N),(START),_Starloop_N)
#define Starloop3(N,END)                        \
    _Starloop((N),(START),(END))

#define Multiplicity_Starloop(N)                                    \
    _Starloop(                                                      \
        (N),                                                        \
        0,                                                          \
        (Star_number)stardata->preferences->zero_age.multiplicity   \
        )

#define Number_of_stars_Starloop(N)             \
    _Starloop(                                  \
        (N),                                    \
        0,                                      \
        (Star_number)(NUMBER_OF_STARS)          \
        )

#define Evolving_Starloop(N)                            \
    (N)=-1;                                             \
    while(                                              \
        (                                               \
            ((N) = next_evolving_star( (N),stardata ))  \
            )!=-1                                       \
        )


#define Nuclear_burning_Starloop(N)                             \
    (N)=-1;                                                     \
    while(                                                      \
        (                                                       \
            ((N) = next_nuclear_burning_star( (N),stardata ))   \
            )!=-1                                               \
        )

/*
 * As the above loops in a foreach format, allocating
 * the loop variable automatically.
 *
 * The variable name passed in, STAR, is declared prior
 * to the loop as a struct star_t *.
 *
 * If a companion is also given, set that.
 *
 * Foreach_star_in(...) takes a stardata first.
 */
#define Foreach_star_in(STARDATA,...)           \
    ConcatC(                                    \
        Foreach_star_in_implementation,         \
        NARGS(__VA_ARGS__)                      \
        )(Foreachstar,                          \
          __COUNTER__,                          \
          STARDATA,                             \
          __VA_ARGS__)
/* single argument version */
#define Foreach_star_in_implementation1(LABEL,              \
                                        LINE,               \
                                        STARDATA,           \
                                        STAR)               \
    Star_number Concat3(N,LABEL,LINE) = 0;                  \
    for(struct star_t * STAR;                               \
        (Concat3(N,LABEL,LINE) <_Starloop_N)                \
            && (STAR = (struct star_t *)                    \
                &(STARDATA)->star[Concat3(N,LABEL,LINE)]);  \
        Concat3(N,LABEL,LINE)++)
/* two argument version */
#define Foreach_star_in_implementation2(LABEL,                          \
                                        LINE,                           \
                                        STARDATA,                       \
                                        STAR,                           \
                                        COMPANION)                      \
    Star_number Concat3(N,LABEL,LINE) = 0;                              \
    for(struct star_t * STAR, * COMPANION;                              \
        (Concat3(N,LABEL,LINE) <_Starloop_N)                            \
            && (STAR = (struct star_t *)                                \
                &(STARDATA)->star[Concat3(N,LABEL,LINE)])               \
            && (COMPANION = Other_star_struct_in((STARDATA),            \
                                                 (STAR)));              \
        Concat3(N,LABEL,LINE)++)


/* wrapper */
#define Foreach_star(...)                       \
    ConcatC(                                    \
        Foreach_star_implementation,            \
        NARGS(__VA_ARGS__)                      \
        )(Foreachstar,                          \
          __COUNTER__,                          \
          __VA_ARGS__)

/* single argument version */
#define Foreach_star_implementation1(LABEL,                 \
                                     LINE,                  \
                                     STAR)                  \
    Star_number Concat3(N,LABEL,LINE) = 0;                  \
    for(struct star_t * STAR;                               \
        (Concat3(N,LABEL,LINE) <_Starloop_N)                \
            && (STAR = (struct star_t *)                    \
                &stardata->star[Concat3(N,LABEL,LINE)]);    \
        Concat3(N,LABEL,LINE)++)

#define Foreach_star_implementation2(LABEL,             \
                                     LINE,              \
                                     STAR,              \
                                     COMPANION)         \
    Star_number Concat3(N,LABEL,LINE) = 0;              \
    for(struct star_t * STAR, * COMPANION;              \
        (Concat3(N,LABEL,LINE) <_Starloop_N)            \
            && (STAR = (struct star_t *)                \
                &stardata->star[Concat3(N,LABEL,LINE)]) \
            && (COMPANION = Other_star_struct(STAR));   \
        Concat3(N,LABEL,LINE)++)

/*
 * Foreach_evolving_star(star)
 * Foreach_evolving_star(star,companion)
 */
#define Foreach_evolving_star(...)              \
    ConcatC(                                    \
        Foreach_evolving_star_implementation,   \
        NARGS(__VA_ARGS__)                      \
        )(                                      \
            Foreachstar,                        \
            __COUNTER__,                        \
            __VA_ARGS__                         \
            )

#define Foreach_evolving_star_implementation1(LABEL,LINE,STAR)  \
                                                                \
    Star_number Concat3(N,LABEL,LINE) =                         \
        next_evolving_star(                                     \
            -1,                                                 \
            stardata                                            \
            );                                                  \
    if(Concat3(N,LABEL,LINE)>-1)                                \
        for(                                                    \
            /* init */                                          \
            struct star_t * (STAR) =                            \
                (struct star_t *)                               \
                &stardata->star[Concat3(N,LABEL,LINE)] ;        \
                                                                \
            /* test */                                          \
            (STAR) != NULL;                                     \
                                                                \
            /* iterator */                                      \
            (STAR) =                                            \
                next_evolving_star_struct(                      \
                    &Concat3(N,LABEL,LINE),                     \
                    stardata                                    \
                    )                                           \
                                                                \
                /*end for */                                    \
            )

#define Foreach_evolving_star_implementation2(LABEL,LINE,               \
                                              STAR,COMPANION)           \
                                                                        \
    Star_number Concat3(N,LABEL,LINE) =                                 \
        next_evolving_star(                                             \
            -1,                                                         \
            stardata                                                    \
            );                                                          \
    if(Concat3(N,LABEL,LINE)>-1)                                        \
        for(                                                            \
            /* init */                                                  \
            struct star_t                                               \
                *(STAR) = (struct star_t *)                             \
                &stardata->star[Concat3(N,LABEL,LINE)],                 \
                *(COMPANION) = (struct star_t *)                        \
                Other_star_struct(STAR);                                \
                                                                        \
            /* test */                                                  \
            (STAR) != NULL;                                             \
                                                                        \
            /* iterators */                                             \
            (STAR) =                                                    \
                next_evolving_star_struct(                              \
                    &Concat3(N,LABEL,LINE),                             \
                    stardata                                            \
                    ) ,                                                 \
                (COMPANION) = ((STAR) ? Other_star_struct(STAR) : NULL) \
                                                                        \
                /*end for */                                            \
            )


#define Foreach_nuclear_burning_star(...)               \
    ConcatC(                                            \
        Foreach_nuclear_burning_star_implementation,    \
        NARGS(__VA_ARGS__)                              \
        )(                                            \
            Foreachstar,                                \
            __COUNTER__,                                \
            __VA_ARGS__                                 \
            )

#define Foreach_nuclear_burning_star_implementation1(LABEL,LINE,STAR)   \
                                                                        \
    Star_number Concat3(N,LABEL,LINE) =                                 \
        next_nuclear_burning_star(                                      \
            -1,                                                         \
            stardata                                                    \
            );                                                          \
    if(Concat3(N,LABEL,LINE)>-1)                                        \
        for(                                                            \
            /* init */                                                  \
            struct star_t * (STAR) =                                    \
                (struct star_t *)                                       \
                &stardata->star[Concat3(N,LABEL,LINE)] ;                \
                                                                        \
            /* test */                                                  \
            (STAR) != NULL;                                             \
                                                                        \
            /* iterator */                                              \
            (STAR) =                                                    \
                next_nuclear_burning_star_struct(                       \
                    &Concat3(N,LABEL,LINE),                             \
                    stardata                                            \
                    )                                                   \
                                                                        \
                /*end for */                                            \
            )

#define Foreach_nuclear_burning_star_implementation2(LABEL,LINE,        \
                                                     STAR,COMPANION)    \
                                                                        \
    Star_number Concat3(N,LABEL,LINE) =                                 \
        next_nuclear_burning_star(                                      \
            -1,                                                         \
            stardata                                                    \
            );                                                          \
    if(Concat3(N,LABEL,LINE)>-1)                                        \
        for(                                                            \
            /* init */                                                  \
            struct star_t                                               \
                *(STAR) = (struct star_t *)                             \
                &stardata->star[Concat3(N,LABEL,LINE)],                 \
                *(COMPANION) = (struct star_t *)                        \
                Other_star_struct(STAR);                                \
                                                                        \
            /* test */                                                  \
            (STAR) != NULL;                                             \
                                                                        \
            /* iterators */                                             \
            (STAR) =                                                    \
                next_nuclear_burning_star_struct(                       \
                    &Concat3(N,LABEL,LINE),                             \
                    stardata                                            \
                    ) ,                                                 \
                (COMPANION) = ((STAR) ? Other_star_struct(STAR) : NULL) \
                                                                        \
                /*end for */                                            \
            )


/*
 * Function to act on all stars that satisfy CONDITION,
 * calling FUNCTION(X,Y) for each that does.
 *
 * INIT is the initial value of the counter.
 */
#define Function_of_stars(VAR,                          \
                          STARDATA,                     \
                          INIT,                         \
                          CONDITION,                    \
                          FUNCTION)                     \
    Function_of_stars_implementation(                   \
        Function_of_stars,                              \
        __COUNTER__,                                    \
        VAR,                                            \
        STARDATA,                                       \
        INIT,                                           \
        CONDITION,                                      \
        FUNCTION                                        \
        )

#define Function_of_stars_implementation(LABEL,                         \
                                         LINE,                          \
                                         VAR,                           \
                                         STARDATA,                      \
                                         INIT,                          \
                                         CONDITION,                     \
                                         FUNCTION)                      \
    __extension__                                                       \
    ({                                                                  \
        typeof((STARDATA)->star[0].VAR+0) Concat3(__tmp,LABEL,LINE) =     \
            INIT;                                                       \
        Star_number Concat3(__n,LABEL,LINE) = 0;                        \
        for(;Concat3(__n,LABEL,LINE) < (Star_number)(NUMBER_OF_STARS);  \
            Concat3(__n,LABEL,LINE)++)                                  \
        {                                                               \
            if((CONDITION) == TRUE)                                     \
            {                                                           \
                Concat3(__tmp,LABEL,LINE) = FUNCTION(                   \
                    Concat3(__tmp,LABEL,LINE),                          \
                    ((STARDATA)->star[Concat3(__n,LABEL,LINE)].VAR)     \
                    );                                                  \
            }                                                           \
        }                                                               \
        Concat3(__tmp,LABEL,LINE);                                      \
    })


#define Weighted_function_of_stars(VAR,         \
                                   WEIGHT,      \
                                   STARDATA,    \
                                   INIT,        \
                                   CONDITION,   \
                                   FUNCTION)    \
    Weighted_function_of_stars_implementation(  \
        Function_of_stars,                      \
        __COUNTER__,                            \
        VAR,                                    \
        WEIGHT,                                 \
        STARDATA,                               \
        INIT,                                   \
        CONDITION,                              \
        FUNCTION                                \
        )

#define Weighted_function_of_stars_implementation(LABEL,                \
                                                  LINE,                 \
                                                  VAR,                  \
                                                  WEIGHT,               \
                                                  STARDATA,             \
                                                  INIT,                 \
                                                  CONDITION,            \
                                                  FUNCTION)             \
    __extension__                                                       \
    ({                                                                  \
        typeof((STARDATA)->star[0].WEIGHT+0) Concat3(__sum,LABEL,LINE)    \
            = 0.0;                                                      \
        typeof((STARDATA)->star[0].VAR+0) Concat3(__tmp,LABEL,LINE) =     \
            INIT;                                                       \
        Star_number Concat3(__n,LABEL,LINE) = 0;                        \
        for(;Concat3(__n,LABEL,LINE) < (Star_number)(NUMBER_OF_STARS);  \
            Concat3(__n,LABEL,LINE)++)                                  \
        {                                                               \
            if((CONDITION) == TRUE)                                     \
            {                                                           \
                Concat3(__tmp,LABEL,LINE) = FUNCTION(                   \
                    Concat3(__tmp,LABEL,LINE),                          \
                    ((STARDATA)->star[Concat3(__n,LABEL,LINE)].WEIGHT) * \
                    ((STARDATA)->star[Concat3(__n,LABEL,LINE)].VAR)     \
                    );                                                  \
            }                                                           \
        }                                                               \
        Concat3(__tmp,LABEL,LINE) / Concat3(__sum,LABEL,LINE);          \
    })



#define Increment_of_stars(VAR,                             \
                           STARDATA,                        \
                           INIT,                            \
                           CONDITION,                       \
                           FUNCTION)                        \
    Increment_of_stars_implementation(                      \
        Increment_of_stars,                                 \
        __COUNTER__,                                        \
        VAR,                                                \
        STARDATA,                                           \
        INIT,                                               \
        CONDITION,                                          \
        FUNCTION                                            \
        )
#define Increment_of_stars_implementation(LABEL,                        \
                                          LINE,                         \
                                          VAR,                          \
                                          STARDATA,                     \
                                          INIT,                         \
                                          CONDITION,                    \
                                          FUNCTION)                     \
    __extension__                                                       \
    ({                                                                  \
        typeof((STARDATA)->star[0].VAR+0) Concat3(__tmp,LABEL,LINE) =   \
            (INIT);                                                     \
        Star_number Concat3(__n,LABEL,LINE) = 0;                        \
        for(; Concat3(__n,LABEL,LINE) < (Star_number)(NUMBER_OF_STARS); \
            Concat3(__n,LABEL,LINE)++)                                  \
        {                                                               \
            if((CONDITION) == TRUE)                                     \
            {                                                           \
                Concat3(__tmp,LABEL,LINE) FUNCTION                      \
                    ((STARDATA)->star[Concat3(__n,LABEL,LINE)].VAR);    \
            }                                                           \
        }                                                               \
        Concat3(__tmp,LABEL,LINE);                                      \
    })


#define Weighted_increment_of_stars(VAR,                            \
                                    WEIGHT,                         \
                                    STARDATA,                       \
                                    INIT,                           \
                                    CONDITION,                      \
                                    FUNCTION)                       \
    Weighted_increment_of_stars_implementation(                     \
        Increment_of_stars,                                         \
        __COUNTER__,                                                \
        VAR,                                                        \
        WEIGHT,                                                     \
        STARDATA,                                                   \
        INIT,                                                       \
        CONDITION,                                                  \
        FUNCTION                                                    \
        )
#define Weighted_increment_of_stars_implementation(LABEL,               \
                                                   LINE,                \
                                                   VAR,                 \
                                                   WEIGHT,              \
                                                   STARDATA,            \
                                                   INIT,                \
                                                   CONDITION,           \
                                                   FUNCTION)            \
    __extension__                                                       \
    ({                                                                  \
        typeof((STARDATA)->star[0].WEIGHT+0) Concat3(__sum,LABEL,LINE)    \
            = 0.0;                                                      \
        typeof((STARDATA)->star[0].VAR+0) Concat3(__tmp,LABEL,LINE) =     \
            (INIT);                                                     \
        Star_number Concat3(__n,LABEL,LINE) = 0;                        \
        for(; Concat3(__n,LABEL,LINE) < (Star_number)(NUMBER_OF_STARS); \
            Concat3(__n,LABEL,LINE)++)                                  \
        {                                                               \
            if((CONDITION) == TRUE)                                     \
            {                                                           \
                Concat3(__sum,LABEL,LINE) +=                            \
                    (STARDATA)->star[Concat3(__n,LABEL,LINE)].WEIGHT;   \
                Concat3(__tmp,LABEL,LINE) FUNCTION                      \
                    (                                                   \
                        ((STARDATA)->star[Concat3(__n,LABEL,LINE)].WEIGHT) * \
                        ((STARDATA)->star[Concat3(__n,LABEL,LINE)].VAR) \
                        );                                              \
            }                                                           \
        }                                                               \
        Concat3(__tmp,LABEL,LINE) / Concat3(__sum,LABEL,LINE);          \
    })

/*
 * Hence we can define functions of the stars in an arbitrary
 * STARDATA struct.
 */
#define Sum_of_stars_in(VAR,STARDATA) (Increment_of_stars(VAR,STARDATA,0.0,TRUE,+=))
#define Weighted_sum_of_stars_in(VAR,WEIGHT,STARDATA) (Weighted_increment_of_stars(VAR,WEIGHT,STARDATA,0.0,TRUE,+=))

#define Negative_sum_of_stars_in(VAR,STARDATA) (Increment_of_stars(VAR,STARDATA,0.0,TRUE,-=))
#define Weighted_negative_sum_of_stars_in(VAR,WEIGHT,STARDATA) (Weighted_increment_of_stars(VAR,WEIGHT,STARDATA,0.0,TRUE,-=))

#define Product_of_stars_in(VAR,STARDATA) (Increment_of_stars(VAR,STARDATA,1.0,TRUE,*=))
#define Weighted_product_of_stars_in(VAR,WEIGHT,STARDATA) (Weighted_increment_of_stars(VAR,WEIGHT,STARDATA,1.0,TRUE,*=))

#define Dividor_of_stars_in(VAR,STARDATA) (Increment_of_stars(VAR,STARDATA,1.0,TRUE,/=))
#define Weighted_dividor_of_stars_in(VAR,WEIGHT,STARDATA) (Weighted_increment_of_stars(VAR,WEIGHT,STARDATA,1.0,TRUE,/=))

#define Max_of_stars_in(VAR,STARDATA) (Function_of_stars(VAR,STARDATA,-LARGE_FLOAT,TRUE,Max))
#define Weighted_Max_of_stars_in(VAR,WEIGHT,STARDATA) (Weighted_function_of_stars(VAR,WEIGHT,STARDATA,-LARGE_FLOAT,TRUE,Max))

#define Min_of_stars_in(VAR,STARDATA) (Function_of_stars(VAR,STARDATA,+LARGE_FLOAT,TRUE,Min))
#define Weighted_min_of_stars_in(VAR,WEIGHT,STARDATA) (Weighted_function_of_stars(VAR,WEIGHT,STARDATA,+LARGE_FLOAT,TRUE,Min))

/* shortcuts of the above list using stardata */
#define Sum_of_stars(VAR) Sum_of_stars_in(VAR,stardata)
#define Weighted_sum_of_stars(VAR,WEIGHT) Weighted_sum_of_stars_in(VAR,WEIGHT,stardata)
#define Negative_sum_of_stars(VAR) Negative_sum_of_stars_in(VAR,stardata)
#define Weighted_negative_sum_of_stars(VAR,WEIGHT) Weighted_negative_sum_of_stars_in(VAR,WEIGHT,stardata)
#define Product_of_stars(VAR) Product_of_stars_in(VAR,stardata)
#define Weighted_product_of_stars(VAR,WEIGHT) Weighted_product_of_stars_in(VAR,WEIGHT,stardata)
#define Dividor_of_stars(VAR) Dividor_of_stars_in(VAR,stardata)
#define Weighted_dividor_of_stars(VAR,WEIGHT) Weighted_dividor_of_stars_in(VAR,WEIGHT,stardata)
#define Max_of_stars(VAR) Max_of_stars_in(VAR,stardata)
#define Weighted_max_of_stars(VAR) Weighted_Max_of_stars_in(VAR,WEIGHT,stardata)
#define Min_of_stars(VAR) Min_of_stars_in(VAR,stardata)
#define Weighted_min_of_stars(VAR) Weighted_min_of_stars_in(VAR,WEIGHT,stardata)

/* legacy macro */
#define Total_mass Sum_of_stars(mass)


/*
 * SETstar sets a struct called 'star' to be star A
 * SETstars does the same, and calls the other star 'companion'
 */
#define SETstar(N)                                      \
    struct star_t * const star Maybe_unused =           \
        (struct star_t * const) &(stardata->star[(N)]);

#define SETstarp(N)                                         \
    struct star_t * const star Maybe_unused =               \
        (struct star_t * const)&((*stardata)->star[(N)]);

#define SETstars(N)                                                 \
    struct star_t * const star Maybe_unused =                       \
        (struct star_t * const)&(stardata->star[(N)]);              \
    struct star_t * const companion Maybe_unused =                  \
        (struct star_t * const)&(stardata->star[Other_star(N)]);


/*
 * structs and integers often used in RLOF routines
 * (with suppression of unused variable warnings)
 */
#define RLOF_stars_structs                      \
    struct star_t * accretor Maybe_unused =     \
        &(stardata->star[naccretor]);           \
    struct star_t * donor Maybe_unused =        \
        &(stardata->star[ndonor]);

#define RLOF_stars                              \
    const int ndonor Maybe_unused =             \
        stardata->model.ndonor ;                \
    const int naccretor Maybe_unused =          \
        stardata->model.naccretor;              \
    RLOF_stars_structs;


/*
 * Foreach over an array of values of given type
 *
 * e.g.
 *
 * double xs[] = { 0.1,1.0,10.0,100.0 };
 * Foreach_array(double,x,xs)
 * {
 *   ... do something with x ...
 * }
 */

#define Foreach_array(type,item,array)                                  \
    Foreach_array_implementation(type,item,(array),Foreacharray,__COUNTER__)
#define Foreach_array_implementation(type, item, array, LABEL, LINE)     \
    for(int Concat3(keep,LABEL,LINE) = 1,                               \
            Concat3(count,LABEL,LINE) = 0,                              \
            Concat3(size,LABEL,LINE) = sizeof (array) / sizeof *(array); \
        Concat3(keep,LABEL,LINE) && Concat3(count,LABEL,LINE) != Concat3(size,LABEL,LINE); \
        Concat3(keep,LABEL,LINE) = Concat3(!keep,LABEL,LINE), Concat3(count,LABEL,LINE)++) \
        for(type * Concat3(itemp,LABEL,LINE) = (array) + Concat3(count,LABEL,LINE), \
                item = * Concat3(itemp,LABEL,LINE);                     \
            Concat3(keep,LABEL,LINE);                                   \
            Concat3(keep,LABEL,LINE) = Concat3(!keep,LABEL,LINE))


#endif // CONTROL_FUNCTION_MACROS_H
