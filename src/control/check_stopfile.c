#include "../binary_c.h"
No_empty_translation_unit_warning;
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

/*
 * Check if a stopfile is set and if it exists,
 * in which case return TRUE, otherwise return FALSE.
 */
Boolean check_stopfile(struct stardata_t * const stardata)
{
    if(stardata->preferences->stopfile[0] != 0)
    {
        struct stat statbuf;
        if(stat(stardata->preferences->stopfile,
                &statbuf) == 0)
        {
            return TRUE;
        }
    }
    return FALSE;
}
