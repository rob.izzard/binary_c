#include "../binary_c.h"
No_empty_translation_unit_warning;

/*
 * Return the next nuclear burning star struct, starting at *k
 */

struct star_t * next_nuclear_burning_star_struct(
    Star_number * const k,
    const struct stardata_t * Restrict const stardata)
{
    Star_number i = next_nuclear_burning_star(*k,stardata);
    *k = i;
    return
        i > -1 ?
        (struct star_t *)&stardata->star[i] :
        NULL;
}
