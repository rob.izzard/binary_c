#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef SAVE_MASS_HISTORY
#include <string.h>
void stellar_structure_adjust_from_mass_history(struct stardata_t * const stardata,
                                                struct star_t * const star)
{
    /*
     * Adjust stellar luminosity and radius because of
     * recent mass-loss history.
     */
    if(stardata->model.mass_history != NULL &&
       star->stellar_type < HeWD)
    {
        const double tkh = 1e-6 * kelvin_helmholtz_time(star); /* Myr */
        const double last_thermal = Max(0.0,
                                        stardata->model.time - stardata->preferences->save_mass_history_n_thermal * tkh);

        void * const * const p = CDict_nest_get_data_pointer(stardata->model.mass_history,
                                                             "star",
                                                             star->starnum);
        if(p != NULL &&
           *p != NULL)
        {
            struct cdict_t * cdict = (struct cdict_t *)*p;

            double I = 0.0;
            CDict_loop(cdict,entry)
            {
                const double time = entry->key.key.double_data;
                if(time >= last_thermal)
                {
                    /* add to weighted integral */
                    const double lookback_time = stardata->model.time - time;
                    const double dm = entry->value.value.double_data;
                    I += dm * exp( - lookback_time / tkh );
                }
            }

            /* add I to the I_list */
            const unsigned int n_max = 10;
            if(stardata->model.I_n == n_max)
            {
                memmove(stardata->model.I_list,
                        stardata->model.I_list + 1,
                        sizeof(double)*n_max);
                stardata->model.I_list[n_max-1] = I;
            }
            else
            {

                stardata->model.I_list[stardata->model.I_n] = I;
                stardata->model.I_n++;
            }

            /* average I over the last ten calculations */
            I = 0.0;
            for(unsigned int i=0;i<stardata->model.I_n; i++)
            {
                I += stardata->model.I_list[i];
            }
            I /= stardata->model.I_n;

            printf("Star %d : Mass change in previous tkh %g\n",
                   star->starnum,
                   I);
            star->Ith = I;

            /* dln(L,R)/dlnM = d(L,R)/dM M/(L,R)*/
            const double dlnL_dlnM = 10.0;
            const double dlnR_dlnM = 10.0;

            /*
              L = 4pi R^2 T^4

              dL = 4pi ( 2R T^4 dR + 4T^3 R^2 dT)
              = 4pi ( 2/R R^2 T^4 dR + 4/T T^4 R^2 dT)
              = 4pi R^2 T^4 ( 2/R dR + 4/T dT)
              dL = L (2/R dR + 4/T dT)

              if dT = 0 ->
              dL = 2 L/R dR
              or
              dR = R/(2L) dL
            */

            /* d(L,R)/dM = (L,R)/M dln(L,R)/dlnM */
            const double dRdM = dlnR_dlnM * star->radius/star->mass;
            const double dLdM = dlnL_dlnM * star->luminosity/star->mass;

            /*
             * hence dR,dL
             */
            double dL = dLdM * I;
            double dR = dRdM * I; //star->radius / (2.0 * star->luminosity) * dL;

            /*
             * Hence change the structure because of mass
             * changes
             */
//            star->radius += dR;
//           star->luminosity += dL;

            printf("t=%g star %d I=%g dR=%g dL=%g : R=%g L=%g\n",
                   stardata->model.time,
                   star->starnum,
                   I,
                   dR,
                   dL,
                   star->radius,
                   star->luminosity);
        }
    }

    printf("ENDADJ %g\n",
           star->Ith);
}
#endif // SAVE_MASS_HISTORY
