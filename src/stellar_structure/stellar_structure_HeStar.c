#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef BSE
#include "stellar_structure_debug.h"

void stellar_structure_HeStar(struct star_t * const newstar,
                              double * const mtc,
                              double *const rg,
                              struct stardata_t * const stardata,
                              const Caller_id caller_id)
{
    /*
     * Naked Helium Star
     */
    Dprint("Naked He Star (star %d, model %d, stellar type %d, TZO? %s, M = %g, Menv = %g, McHe=%g, McCO=%g, phase_start_mass=%g, age = %g, epoch = %g) TZO? %s, caller %d, deny_SN %u, tau = %g\n",
           newstar->starnum,
           stardata->model.model_number,
           newstar->stellar_type,
           Yesno(newstar->TZO),
           newstar->mass,
           envelope_mass(newstar),
           newstar->core_mass[CORE_He],
           newstar->core_mass[CORE_CO],
           newstar->phase_start_mass,
           newstar->age,
           newstar->epoch,
           Yesno(newstar->TZO),
           caller_id,
           newstar->deny_SN,
           newstar->age / newstar->tm
        );

    /*
     * We have no helium core
     */
    newstar->core_mass[CORE_He] = 0.0;

    /*
     * How to compute relative timescale?
     * We should limit the range to 0 to 1: sometimes
     * the age will be inconsistent.
     */
//#define _tau newstar->age / newstar->tm;
#define _tau Limit_range(newstar->age / newstar->tm,0.0,1.0);

    double tau;
    const double * metallicity_parameters = stardata->common.metallicity_parameters;
    Stellar_type stellar_type_in = newstar->stellar_type;
    newstar->core_radius = 0.0;

    newstar->white_dwarf_atmosphere_type =
        WHITE_DWARF_ATMOSPHERE_HELIUM;

#ifdef NUCSYN
    if(!RLOF_CALL)
    {
        if(newstar->he_t < TINY)  newstar->he_t = newstar->age;

        // save in here the fraction of the HeMS we have completed
        newstar->he_f = Max(0.0,
                            Min((newstar->age - newstar->he_t) / (newstar->tm - newstar->he_t),
                                1.0));

        if(newstar->stellar_type > 7) newstar->he_f = 1.0;

        Dprint("he MS fraction = %g\n", newstar->he_f);
    }
#endif


    Dprint("Compare age=%20.12g to tm=%20.12g (menv = %g from M = %g - McoreCO = %g) really less? %s diff = %g\n",
           newstar->age,
           newstar->tm,
           envelope_mass(newstar),
           newstar->mass,
           newstar->core_mass[CORE_CO],
           Yesno(Really_less_than(newstar->age, newstar->tm)),
           Signed_diff(newstar->age , newstar->tm)
        );

    /* core mass above which He burning continues */
    double mc_Heburn = mc_heburn(newstar->mass);

    if(Signed_diff(newstar->age,
                   newstar->tm) < -1e-10)
    {
        /*
         * Main Sequence
         */
        Dprint("MS star, massless core\n");

        newstar->stellar_type = HeMS;
        newstar->core_stellar_type = MASSLESS_REMNANT;

        if(stardata->preferences->require_drdm == TRUE)
        {
            const double dm = 1e-7 * newstar->mass;
            update_mass(stardata,newstar,dm);
            newstar->phase_start_mass += dm;
            call_stellar_timescales2(newstar->stellar_type,
                                     newstar->mass,
                                     newstar->mass);
            tau = _tau;

            double am = Max(0.0, 0.40 - 0.220 * log10(newstar->mass));
            const double rm = rzhef(newstar->mass) * (1.0 + am * (tau - Pow6(tau)));

            update_mass(stardata,newstar,-dm);
            newstar->phase_start_mass -= dm;
            call_stellar_timescales2(newstar->stellar_type,
                                     newstar->mass,
                                     newstar->mass);
            tau = _tau;

            *rg = rzhef(newstar->mass);

            am = Max(0.0, 0.850 - 0.080 * newstar->phase_start_mass);
            newstar->luminosity = newstar->bse->luminosities[L_ZAMS] * (1.0 + 0.450 * tau + am * Pow2(tau));
            am = Max(0.0, 0.40 - 0.220 * log10(newstar->mass));
            newstar->radius = (*rg) * (1.0 + am * (tau - Pow6(tau)));

            newstar->drdm = (rm - newstar->radius) / dm;
            Dprint("Radius (1) = %g from rg = %g, am = %g, tau = %g\n",
                   newstar->radius,
                   *rg,
                   am,
                   tau);
        }
        else
        {
            Dprint("call stellar timescales with stellar type %d, mass %g, phase_start_mass %g\n",
                   newstar->stellar_type,
                   newstar->mass,
                   newstar->phase_start_mass);
            call_stellar_timescales2(newstar->stellar_type,
                                     newstar->mass,
                                     newstar->mass);
            tau = _tau;
            Dprint("tau = %g / %g = %g\n",
                   newstar->age,
                   newstar->tm,
                   tau);
            *rg = rzhef(newstar->mass);
            double am = Max(0.0, 0.850 - 0.080 * newstar->phase_start_mass);
            newstar->luminosity = newstar->bse->luminosities[L_ZAMS] * (1.0 + 0.450 * tau + am * Pow2(tau));
            am = Max(0.0, 0.40 - 0.220 * log10(newstar->mass));
            newstar->radius = (*rg) * (1.0 + am * (tau - Pow6(tau)));
            Dprint("Radius (2) = %g from rg = %g, am = %g, tau = %g\n",
                   newstar->radius,
                   *rg,
                   am,
                   tau);
        }

#if defined NUCSYN && defined NUCSYN_WR
        if(!RLOF_CALL)
        {
            /*
             * Guess the terminal core mass for this star, this
             * uses the shell-burning formulae given below. Of course
             * it is wrong but trial and error suggests a 10% factor
             * compensates nicely and by the start of shell burning
             * the match will be pretty good.
             */
            newstar->he_mcmax = 1.1 * Min(Min(newstar->mass, mc_Heburn),
                                          Max(stardata->preferences->chandrasekhar_mass, (0.7730 * (newstar->phase_start_mass) - 0.350)));
        }
#endif

        /*
         * Star has no core mass and hence no memory of its past
         * which is why we subject mass and mt to mass loss for
         * this phase.
         */
        set_no_core(newstar);

        /* Rob adds : this is not quite true.
         * Lynnette's models of (WR i.e. massive) helium
         * stars shows that the MS time is NOT just a function of
         * the initial stellar mass. We have to take into account the
         * previous history of the star!
         */

        /* evaporation of star if too much mass is lost */
        if(newstar->mass < metallicity_parameters[10] &&
           newstar->mass < stardata->preferences->max_HeWD_mass)
        {
            newstar->stellar_type = HeWD;

            /*
             * Give the HeWD a COWD core, which means it comes from
             * this channel, not a RG.
             */
            newstar->core_stellar_type = COWD;
            newstar->phase_end[HeMS].radius = newstar->radius;
            newstar->phase_end[HeMS].luminosity = newstar->luminosity;
            newstar->phase_end[HeMS].mass = newstar->mass;
            newstar->phase_end[HeMS].time = stardata->model.time;
        }
    }
    else
    {
        /*
         * Helium Shell Burning
         */
        Dprint("He Shell Burning m = %g, menv = %g\n",
               newstar->mass,
               envelope_mass(newstar)
            );

        newstar->stellar_type = HeHG;
        newstar->core_stellar_type = COWD;

        /*
         * Core radius : assume it's ~ a WD core, but...
         */
        newstar->core_radius = rwd(stardata,
                                   newstar,
                                   newstar->mass,
                                   newstar->stellar_type,
                                   FALSE);

        Dprint("lgbtf : branch %d  : age = %g tinf = %g : A=GB8=%g\n",
               newstar->age < newstar->bse->timescales[T_GIANT_TX] ? 0 : 1,
               newstar->age,
               (newstar->age < newstar->bse->timescales[T_GIANT_TX] ?
                newstar->bse->timescales[T_GIANT_TINF_1] :
                newstar->bse->timescales[T_GIANT_TINF_2]) - newstar->age,
               newstar->bse->GB[GB_A_HE]);

        newstar->luminosity = lgbtf(newstar->age,
                                    newstar->bse->GB[GB_A_HE],
                                    newstar->bse->GB,
                                    newstar->bse->timescales[T_GIANT_TINF_1],
                                    newstar->bse->timescales[T_GIANT_TINF_2],
                                    newstar->bse->timescales[T_GIANT_TX]);

        /*
         * Perhaps we're now a HeGB star?
         */
        const double rGB = rhegbf(newstar->luminosity);
        const double rHG = rhehgf(newstar->mass,
                                  newstar->luminosity,
                                  newstar->rzams,
                                  newstar->bse->luminosities[L_END_MS]);
        *rg = rGB;
        Dprint("Radii HG=%g GB=%g\n",rHG,rGB);

        if(More_or_equal(rHG, *rg)
           ||
           stellar_type_in == HeGB
            )
        {
            /*
             * On HeGB
             */
            newstar->stellar_type = HeGB;
            newstar->radius = *rg;

            if(stardata->preferences->require_drdm == TRUE)
            {
                /*
                 * What's dr/dm for HeGB stars?
                 */
                newstar->drdm = -1.0;
            }
        }
        else
        {
            /*
             * HeHG
             */
            if(stardata->preferences->require_drdm == TRUE)
            {
                const double dm = newstar->mass * 1e-7;
                update_mass(stardata,newstar,dm);
                const double rm = rhehgf(newstar->mass,
                                         newstar->luminosity,
                                         newstar->rzams,
                                         newstar->bse->luminosities[L_END_MS]);
                update_mass(stardata,newstar,dm);
                newstar->radius = rHG;
                newstar->drdm = (rm - rHG) / dm;
            }
            else
            {
                newstar->radius = rHG;
            }
        }

        Dprint("Radius %g (L=%g rg=%g stellar_type=%d mass=%g Teff=%g)\n",
               newstar->radius,
               newstar->luminosity,
               *rg,
               newstar->stellar_type,
               newstar->mass,
               Teff_from_star_struct(newstar)
            );


        /* no He core */
        newstar->core_mass[CORE_He] = 0.0;

        /*
         * CO core mass
         *
         * Cannot shrink, hence the use of inner_core_mass,
         * but use the BSE core mass if possible.
         */

        newstar->core_mass[CORE_CO] = Max(inner_core_mass(stardata,
                                                          newstar,
                                                          CORE_CO),
                                          mcgbf(newstar->luminosity,
                                                newstar->bse->GB,
                                                newstar->bse->luminosities[L_LMX]));
        Dprint("set mass %g core masses He %g CO %g\n",
               newstar->mass,
               newstar->core_mass[CORE_He],
               newstar->core_mass[CORE_CO]);

        /*
         * cannot exceed the total mass
         * and we should correct for innermost cores
         * (e.g. TZO with neutron core)
         */
        newstar->core_mass[CORE_CO] = Min(newstar->mass,
                                          newstar->core_mass[CORE_CO]);
        Dprint("CO core post-correction = %g\n",
               newstar->core_mass[CORE_CO]);

        *mtc = Min(newstar->mass, mc_Heburn);

        /*
         * what is the maximum possible CO core mass?
         * this is when carbon ignites, or burning stops,
         * but there's no limit for TZOs (so set it to 1e10).
         */
        const double mcmax =
            newstar->TZO ? 1e10 :
            Min(*mtc - TINY,
                Max(stardata->preferences->chandrasekhar_mass,
                    0.7730 * newstar->phase_start_mass - 0.350));

#if defined NUCSYN && defined NUCSYN_WR
        if(!RLOF_CALL)
            newstar->he_mcmax = mcmax; // save for use in nucsyn_WR
#endif

        Dprint("Helium star mc=%g m=%g menv=%g mtc=%g mcmax=%g\n",
               newstar->core_mass[CORE_CO],
               newstar->mass,
               newstar->mass - newstar->core_mass[CORE_CO],
               *mtc,
               mcmax);

        /*
         * Check if the burning shell has reached the surface
         * or the maximum core mass before carbon ignition
         */
        Dprint("CF Mc = %g to mcmax = %g (m = %g)\n",
               newstar->core_mass[CORE_CO],
               mcmax,
               newstar->mass);

        if(More_or_equal(newstar->core_mass[CORE_CO],
                         mcmax))
        {
            Dprint("core mass > mcmax, cf mc %g vs mcmax = %g and Mch = %g\n",
                   newstar->core_mass[CORE_CO],
                   mcmax,
                   stardata->preferences->chandrasekhar_mass
                );

            newstar->core_mass[CORE_CO] = Min(newstar->mass,
                                              mcmax);
            newstar->core_stellar_type = ONeWD;


            if(newstar->core_mass[CORE_CO] < chandrasekhar_mass(stardata, newstar))
            {
                /*
                 * Star becomes a new white dwarf or neutron star
                 */
                newstar->age = 0.0;

                Dprint("CO Core mass %g < MCh, but is it > M_(Cignition)? =%g\n",
                       newstar->core_mass[CORE_CO],
                       stardata->preferences->maximum_mcbagb_for_degenerate_carbon_ignition);

                if(newstar->phase_start_mass < stardata->preferences->maximum_mcbagb_for_degenerate_carbon_ignition)
                {
                    /*
                     * Form a new COWD ?
                     */
                    Dprint("Shell burning stops : Zero-age CO WD mt=%g\n", newstar->mass);

                    /*
                     * If M<0.7 the detailed models (Pols "in prep") show
                     * that shell He-burning stops before the whole envelope
                     * is converted into C and O.
                     *
                     * This is modelled by letting a He star become a COWD
                     * when its core mass reaches the value
                     * Min(1.45M-0.31,M)
                     * (Hurley+ 2002 Eq. 89)
                     *
                     * This calculation has been abstracted into the
                     * mc_heburn function (see above for call).
                     *
                     * It has been reported that this formula
                     * fails at low Z, but the helium star models
                     * are anyway only valid for Z=0.02.
                     */
                    newstar->stellar_type = COWD;
                    newstar->core_stellar_type = COWD;
                    if(!RLOF_CALL)
                    {
                        /*
                         * Reset the TPAGB parameters for this star
                         */
                        newstar->num_thermal_pulses = -1.0; /* resets */
                        newstar->num_thermal_pulses_since_mcmin = 0.0;
                    }
                    Dprint("Stellar type now %d\n", newstar->stellar_type);
                }
                else
                {
                    /*
                     * Zero-age Oxygen/Neon White Dwarf
                     */
                    Dprint("Zero-age O Ne WD\n");

                    newstar->stellar_type = ONeWD;
                    newstar->core_stellar_type = ONeWD;

                    newstar->core_mass[CORE_ONe] = newstar->core_mass[CORE_CO];
                    newstar->core_mass[CORE_CO] = 0.0;

                    if(!RLOF_CALL)
                    {
                        /*
                         * Reset the TPAGB parameters for this star
                         */
                        newstar->num_thermal_pulses_since_mcmin = 0.0;
                        newstar->num_thermal_pulses = -1.0; /* resets */
                    }
                }

                /*
                 * If there is > MIN_MASS_He_FOR_HYBRID_COWD of helium on top of the
                 * carbon-oxygen core, call the star a "hybrid" COWD
                 */
                if(newstar->mass - newstar->core_mass[CORE_CO] > MIN_MASS_He_FOR_HYBRID_COWD &&
                   newstar->stellar_type == COWD)
                {
                    newstar->hybrid_HeCOWD = TRUE;
#ifdef LOG_HYBRID_WHITE_DWARFS
                    Printf("HYBRIDHeCO %g %g %g\n",
                           stardata->model.probability,
                           newstar->mass,
                           newstar->core_mass[CORE_CO]);
#endif
                    newstar->core_mass[CORE_He] = newstar->mass;
                    // CO core stays the same
                    Dprint("New hybrid COWD %g %g\n",
                           newstar->core_mass[CORE_He],
                           newstar->core_mass[CORE_CO]);
                }
                else
                {
                    /*
                     * Whole star becomes the COWD
                     */
                    newstar->core_mass[CORE_He] = 0.0;
                    newstar->core_mass[CORE_CO] = newstar->mass;
                }

                newstar->phase_start_mass = newstar->mass;
                Dprint("Stellar type now %d\n", newstar->stellar_type);
            }
            else if(newstar->deny_SN == 0 &&
                    newstar->TZO == FALSE)
            {
                /*
                 * CO Core mass exceeds MCh :
                 * some kind of collapse and/or explosion follows
                 */
                Dprint("Check C ig m=%g at min mass for C ig=%g, conditions %d %d (lower =%g upper =%g)\n",
                       newstar->phase_start_mass,
                       stardata->preferences->maximum_mcbagb_for_degenerate_carbon_ignition,
                       ((stardata->preferences->mass_for_Hestar_Ia_upper > TINY) &&
                        (stardata->preferences->mass_for_Hestar_Ia_lower > TINY))
                       ,
                       (newstar->phase_start_mass >= stardata->preferences->mass_for_Hestar_Ia_lower) &&
                       (newstar->phase_start_mass <= stardata->preferences->mass_for_Hestar_Ia_upper),
                       stardata->preferences->mass_for_Hestar_Ia_lower,
                       stardata->preferences->mass_for_Hestar_Ia_upper
                    );

                if(newstar->phase_start_mass < stardata->preferences->maximum_mcbagb_for_degenerate_carbon_ignition
                   /* allow ignition in designated mass range */
                   ||
                   ( stardata->preferences->mass_for_Hestar_Ia_upper > TINY &&
                     stardata->preferences->mass_for_Hestar_Ia_lower > TINY &&
                     newstar->phase_start_mass > stardata->preferences->mass_for_Hestar_Ia_lower &&
                     newstar->phase_start_mass < stardata->preferences->mass_for_Hestar_Ia_upper ))
                {
                    Dprint("IA HE STAR M=%g lower %g %d upper %g %d\n",
                           newstar->phase_start_mass,
                           stardata->preferences->mass_for_Hestar_Ia_lower,
                           stardata->preferences->mass_for_Hestar_Ia_lower > TINY,
                           stardata->preferences->mass_for_Hestar_Ia_upper,
                           stardata->preferences->mass_for_Hestar_Ia_upper > TINY);

                    /*
                     * Star is not massive enough to ignite carbon non-degenerately,
                     * so no remnant is left after the SN
                     */
                    Dprint("No SN remn-ant\n");

                    newstar->SN_type = SN_IA_HeStar;
                    struct star_t * news =
                        new_supernova(stardata,
                                      newstar,
                                      Other_star_struct(newstar),
                                      newstar);
                    if(news)
                    {
                        stellar_structure_make_massless_remnant(stardata,
                                                                news);
                    }
                }
                else
                {
                    Dprint("STELLAR_STRUCTURE SN 2 stellar type %d\n",
                           newstar->stellar_type);
                    Dprint("Can star ignite neon? mcCO = %g vs minimum_ignition_mass %g -> %s\n",
                           newstar->core_mass[CORE_CO],
                           stardata->preferences->minimum_CO_core_mass_for_neon_ignition,
                           Yesno(newstar->core_mass[CORE_CO] > stardata->preferences->minimum_CO_core_mass_for_neon_ignition)
                        );

                    if(Is_zero(stardata->preferences->minimum_CO_core_mass_for_neon_ignition) ||
                       newstar->core_mass[CORE_CO] < stardata->preferences->minimum_CO_core_mass_for_neon_ignition)
                    {
                        /*
                         * Mass is too low to ignite neon, hence
                         * the supernova is an electron capture,
                         * not a canonical core collapse which
                         * ignites neon, ..., silicon and forms
                         * an iron core.
                         */
                        newstar->SN_type = SN_ECAP;
                        double remnant_baryonic_mass;
                        const double remnant_mass = ns_bh_mass(newstar->mass,
                                                               newstar->core_mass[CORE_CO],
                                                               stardata,
                                                               newstar,
                                                               &remnant_baryonic_mass);
                        struct star_t * const news = new_supernova(stardata,
                                                                   newstar,
                                                                   Other_star_struct(newstar),
                                                                   newstar);
                        if(news)
                        {
                            news->age = 0.0;
                            news->mass = remnant_mass;
                            news->baryonic_mass = remnant_baryonic_mass;
                            set_no_core(news);
                            if(Less_or_equal(news->mass,
                                             stardata->preferences->max_neutron_star_mass))
                            {
                                Dprint("Zero-age NS\n");
                                news->stellar_type = NEUTRON_STAR;
                                news->core_mass[CORE_NEUTRON] = newstar->mass;
                            }
                            else
                            {
                                Dprint("Zero-age Black Hole\n");
                                news->stellar_type = BLACK_HOLE;
                                news->core_mass[CORE_BLACK_HOLE] = newstar->mass;
                            }
                        }
                    }
                    else
                    {

                        /*
                         * Neon ignites.
                         */
                        newstar->SN_type = SN_IBC;

#ifndef PPISN // Begin wrapping here to prevent the wrong debug print
                        /* TODO: Can we wrap this entire thing in a function please. ? */
                        /*
                         * Check for collapsar!
                         */

                        /*
                         * First calculate angular momentum J
                         */
                        double J =
                            // envelope
                            newstar->moment_of_inertia_factor * M_SUN * (newstar->mass - newstar->core_mass[CORE_CO]) *
                            Pow2(newstar->radius * R_SUN) * (
                                newstar->omega
                                / YEAR_LENGTH_IN_SECONDS)
                            +
                            // core
                            CORE_MOMENT_OF_INERTIA_FACTOR * (newstar->core_mass[CORE_CO] * M_SUN) * Pow2(newstar->core_radius * R_SUN) * (
                                newstar->omega
                                / YEAR_LENGTH_IN_SECONDS);

                        /*
                         * Calculate specific angular momentum for the whole star
                         */
                        double j = J / (M_SUN * newstar->mass); // (cgs)

                        /*
                         * Calculate BH mass (gravitational)
                         */
                        double mbh_baryonic;
                        double mbh = ns_bh_mass(newstar->mass,
                                                newstar->core_mass[CORE_CO],
                                                stardata,
                                                newstar,
                                                &mbh_baryonic) * M_SUN;

                        /*
                         * Calculate *innermost* last stable orbit radius (cm)
                         *
                         * Schwarzschild
                         * r_LSO=6.0*GRAVITATIONAL_CONSTANT*mbh/(SPEED_OF_LIGHT*SPEED_OF_LIGHT);
                         *
                         * Kerr
                         */
                        double r_LSO = 1.23 * GRAVITATIONAL_CONSTANT * mbh / (SPEED_OF_LIGHT * SPEED_OF_LIGHT);

                        /*
                         * specific angular moment limit (cf. Heger + Woosley)
                         */
                        double jlim = sqrt(GRAVITATIONAL_CONSTANT * mbh * r_LSO);

                        Dprint("Collapsar : J=%g j=%g r_LSO=%g mbh=%g jlim=%g collapsar? %d\n",
                               J, j, r_LSO, mbh / M_SUN, jlim, (j > jlim));
                        /*
                         * We have a collapsar: TODO: (david) ask Rob what is really meant which 'collapsar' here
                         */
                        if(j > jlim)
                        {
                            newstar->SN_type = SN_GRB_COLLAPSAR;
                        }
#else // PPISN
                        /*
                         * Check for PPISN
                         * If the star is massive enough to ignite neon, it will be massive enough
                         * to be in the range of PPISN (which requires a Co core of about 30+ solar mass)
                         */
                        Dprint("Calling pulsational_pair_instability\n");

                        double ppisn_mremnant = -1.0;
                        double ppisn_mbaryonic = -1.0;
                        int ppisn_type = PPISN_TYPE_NONE;

#undef X
#define X(TYPE,STRING,SN_TYPE,REMNANT_TYPE) (((SN_TYPE) == (SN_II)) ? (SN_IBC) : (SN_TYPE)),
                        static const Supernova sn_types[] = { PPISN_TYPES_LIST };
#undef X
#define X(TYPE,STRING,SN_TYPE,REMNANT_TYPE) REMNANT_TYPE,
                        static const Stellar_type remnant_types[] = { PPISN_TYPES_LIST };
#undef X

                        /*
                         * This function call will use the function passed in by the user
                         * to determine whether the star undergoes core collapse, PPISN,
                         * PISN or PHDIS. The results are written to the ppisn_remnant
                         * and ppisn_type vars
                         */
                        pulsational_pair_instability(
                            stardata,
                            newstar,
                            newstar->mass,
                            newstar->core_mass[CORE_CO],
                            &ppisn_mremnant,
                            &ppisn_mbaryonic,
                            &ppisn_type
                            );
                        Dprint("ppisn_mremn-ant: %g ppisn_type: %d\n", ppisn_mremnant, ppisn_type);

                        /*
                         * Check that the remnant mass is ok
                         */
                        if(ppisn_mremnant < -TINY)
                        {
                            Exit_binary_c(BINARY_C_ALGORITHM_OUT_OF_RANGE,
                                          "ppisn_mremn-ant mass (=%g) not set correctly",
                                          ppisn_mremnant);
                        }

                        /*
                         * Decide what to do now:
                         * If PPISN happened, check which SN type it should be.
                         * This needs to happen before the new_supernova.
                         */
                        if(ppisn_type != PPISN_TYPE_NONE &&
                           ppisn_type != PPISN_TYPE_ERROR)
                        {
                            newstar->SN_type = sn_types[ppisn_type];
                            Dprint("Set SN type to %d = %s\n",
                                   newstar->SN_type,
                                   sn_strings[newstar->SN_type]
                                );
                        }
#endif // PPISN
                        struct star_t * const news = new_supernova(stardata,
                                                                   newstar,
                                                                   Other_star_struct(newstar),
                                                                   newstar);
#ifndef PPISN //
                        if(news)
                        {
                            news->mass = ns_bh_mass(newstar->mass,
                                                    newstar->core_mass[CORE_CO],
                                                    stardata,
                                                    newstar,
                                                    &news->baryonic_mass);
                            news->core_mass[CORE_CO] = news->mass;

                            if(Less_or_equal(news->mass,
                                             stardata->preferences->max_neutron_star_mass))
                            {
                                /*
                                 * Zero-age Neutron star
                                 */
                                Dprint("Zero-age NS\n");
                                news->stellar_type = NEUTRON_STAR;
                            }
                            else
                            {
                                /*
                                 * Zero-age Black hole
                                 */
                                Dprint("Zero-age Black Hole\n");
                                news->stellar_type = BLACK_HOLE;
                            }
                            news->core_stellar_type = MASSLESS_REMNANT;
                        }
#else
                        if(news)
                        {
                            /*
                             * Either a PPISN, PISN or PHDIS occured.
                             */
                            if(ppisn_type != PPISN_TYPE_NONE &&
                               ppisn_type != PPISN_TYPE_ERROR &&
                               ppisn_type != PPISN_TYPE_CORE_COLLAPSE)
                            {
                                news->stellar_type = remnant_types[ppisn_type];
                                news->undergone_ppisn_type = ppisn_type;
                                news->mass = ppisn_mremnant;
                                news->baryonic_mass = ppisn_mbaryonic;

                                Dprint("UNDERGONE %s: stellar type: %d undergone_ppisn_type: %d mass: %g\n",
                                       ppisn_type_strings[ppisn_type],
                                       news->stellar_type,
                                       news->undergone_ppisn_type,
                                       news->mass);

                                if(news->stellar_type == MASSLESS_REMNANT)
                                {
                                    stellar_structure_make_massless_remnant(stardata,
                                                                            news);
                                }
                            }
                            else
                            {
                                /*
                                 * So the PPISN routines either decided we are in the
                                 * range of core collapse, i.e. PPISN_TYPE_CORE_COLLAPSE,
                                 * or ppsin_type is PPISN_TYPE_ERROR.
                                 *
                                 * We can decide whether then to use the default method
                                 * for core collapse SN mass: ns_bh_mass
                                 *
                                 * But we can also decide to take the value that the ppisn routine provides.
                                 * For now, use the standard ns_bh_mass result
                                 * whenever there is a core collapse.
                                 */
                                news->mass = ns_bh_mass(newstar->mass,
                                                        newstar->core_mass[CORE_CO],
                                                        stardata,
                                                        newstar,
                                                        &news->baryonic_mass);
                                Dprint("Setting mass after Core collapse according to PPISN routine (using ns bh mass.c): %g", news->mass);

                                /*
                                 * Signal the core collapse to the ppisn
                                 * logging.
                                 */
                                news->undergone_ppisn_type = PPISN_TYPE_CORE_COLLAPSE;

                                if(Less_or_equal(news->mass,
                                                 stardata->preferences->max_neutron_star_mass))
                                {
                                    /*
                                     * Zero-age Neutron star
                                     */
                                    Dprint("Zero-age NS\n");
                                    news->stellar_type = NEUTRON_STAR;
                                }
                                else
                                {
                                    /*
                                     * Zero-age Black hole
                                     */
                                    Dprint("Zero-age Black Hole\n");
                                    news->stellar_type = BLACK_HOLE;
                                }
                            }
                            news->core_mass[CORE_CO] = news->mass;
                            news->core_stellar_type = MASSLESS_REMNANT;
                        }
#endif // PPISN
                    }
                }

            }
        }
    }
    Dprint("Stellar type now %d\n", newstar->stellar_type);
    Dprint("Naked He Star out (star %d, stellar type %d, M = %g, Menv = %g, age = %g, epoch = %g, L = %g, R = %g)\n",
           newstar->starnum,
           newstar->stellar_type,
           newstar->mass,
           envelope_mass(newstar),
           newstar->age,
           newstar->epoch,
           newstar->luminosity,
           newstar->radius);
}
#endif//BSE
