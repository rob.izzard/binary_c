#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef BSE
#include "stellar_structure_debug.h"

enum
{
    CARBON_IGNITION_NONE,
    CARBON_IGNITION_DEGENERATE,
    CARBON_IGNITION_PARTIALLY_DEGENERATE,
    CARBON_IGNITION_NONDEGENERATE
};

Stellar_type stellar_structure_AGB(struct star_t * const oldstar,
                                   struct star_t * Restrict const newstar,
                                   double * Restrict const rg,
                                   const double tbagb,
                                   struct stardata_t * Restrict const stardata,
                                   const Caller_id caller_id)
{
    /*
     * Asymptotic Red Giant or Thorne-Zytkow object.
     *
     * On the AGB the He core mass remains constant until at Ltp it
     * is caught by the C core mass and they grow together.
     */
    const double * const giant_branch_parameters =
        stardata->common.giant_branch_parameters;
    Boolean convert_to_helium_star = FALSE;

    newstar->white_dwarf_atmosphere_type =
        WHITE_DWARF_ATMOSPHERE_HYDROGEN;

    Dprint("Asymptotic Red Giant (star %d mass %g cores He %g CO %g)\n",
           newstar->starnum,
           newstar->mass,
           newstar->core_mass[CORE_He],
           newstar->core_mass[CORE_CO]);

    /*
     * Determine whether we ignite carbon
     */
    const double mcbagb = mcagbf(newstar->phase_start_mass,
                                 giant_branch_parameters);
    const Boolean ignite_carbon =
        mcbagb > stardata->preferences->maximum_mcbagb_for_degenerate_carbon_ignition &&
        newstar->core_mass[CORE_CO] > stardata->preferences->minimum_CO_core_mass_for_carbon_ignition;

    /*
     * Hence the core stellar type
     */
    newstar->core_stellar_type =
        ignite_carbon == TRUE ? ONeWD : COWD;

    if(newstar->starnum == 0)
    {
        Dprint("Mc1TP0 mcbagb = %g, ignite carbon? %d\n",
               mcbagb,
               ignite_carbon);
    }

    Dprint("Set mcbagb %12.12e from newstar->phase_start_mass %12.12e\n",
           mcbagb,
           newstar->phase_start_mass);

    /*
     * mc_CO here is the CO core mass
     * at the start of the AGB, i.e. at
     * the end of core helium burning
     */
    const double mc_CO_start_AGB
        = Min(newstar->mass,
              mcgbtf(tbagb,
                     newstar->bse->GB[GB_A_HE],
                     newstar->bse->GB,
                     newstar->bse->timescales[T_EAGB_TINF_1],
                     newstar->bse->timescales[T_EAGB_TINF_2],
                     newstar->bse->timescales[T_EAGB_T]));

    Dprint("MCX(age=%g,stellar_type=%d) = %g (tbagb=%g mcbagb=%g) Mc = %g\n",
           newstar->age,
           newstar->stellar_type,
           mc_CO_start_AGB,
           tbagb,
           mcbagb,
           newstar->core_mass[CORE_He]
        );

    /*
     * Obtain Chandrasekhar mass
     */
    const double MCh = chandrasekhar_mass(stardata, newstar);

    /*
     * Equivalent to Hurley et al. 2000's Eq. (75)
     */
    const double mcSN = Max(0.7730 * mcbagb - 0.350,
                            MCh);

    /*
     * How much must the AGB core grow before
     * it is allowed to explode? In Msun.
     *
     * This is a fudge in BSE: see below for implementation.
     */
    const double min_AGB_core_growth = 0.05;

    /*
     * CO core masses in excess of this value (in Msun) are not
     * allowed to have thermal pulses. Such stars will,
     * in any case, soon explode.
     *
     * The Hurley et al. 2000 (based on Pols) models estimate
     * that core with mass > 2.25Msun are non-degenerate, so these
     * should not have a TPAGB.
     */
    const double maximum_degenerate_CO_mcbagb = 2.25;

    /*
     * The following is in BSE: it makes the core
     * grow somewhat up the AGB before there is an explosion.
     *
     * This is not mentioned in the Hurley et al. (2000) paper.
     *
     * Presumably this is just a fudge of some sort to
     * make sure there is no immediate explosion after core helium
     * burning finishes, perhaps to simulate the time of subsequent
     * burning (e.g. He-shell, C-core, C-shell, etc.) before there
     * is an explosion.
     */
    double mcmax = Max(mcSN,
                       (1.0 + min_AGB_core_growth) * mc_CO_start_AGB);

    Dprint("Compare mcx=%g to mcxmax=%g (m=%g), mcSN = %g\n",
           mc_CO_start_AGB, mcmax, newstar->mass, mcSN);

    Dprint("age %14.14g c.f. tscls 13 (first pulse) %14.14g\n",
           newstar->age,
           newstar->bse->timescales[T_TPAGB_FIRST_PULSE]);

    Boolean force_supernova;

    /*
     * Set the CO core mass at this point in the evolution (in mc_CO),
     * luminosity, radius and age.
     */
    if(newstar->age < newstar->bse->timescales[T_TPAGB_FIRST_PULSE]
       &&
       newstar->stellar_type != TPAGB)
    {
        /* EAGB star : updates mc_CO */
        convert_to_helium_star =
            stellar_structure_EAGB(oldstar,
                                   newstar,
                                   mcbagb,
                                   stardata,
                                   caller_id,
                                   FALSE);
        Dprint("convert to helium star? %s\n",
               Yesno(convert_to_helium_star));
        force_supernova = FALSE;
    }
    /*
     * Only allow stars with CO core mass less than the
     * maximum_CO_mass_for_thermal_pulses to have thermal pulses, or
     * those that are already thermally pulsing.
     *
     * This is because more massive stars should explode
     * soon after the AGB begins, and probably don't ever
     * have thermal pulses.
     *
     * We also check that TPAGB stars have core masses
     * < MCh: if not, treat them as EAGB i.e. allow
     * them to explode
     */
    else if((newstar->stellar_type == TPAGB ||
             (Is_not_zero(newstar->core_mass[CORE_CO]) &&
              (newstar->stellar_type == EAGB ||
               newstar->core_mass[CORE_He] < MCh) &&
              newstar->core_mass[CORE_CO] < maximum_degenerate_CO_mcbagb)))
    {
        /* TPAGB star */
        stellar_structure_TPAGB(newstar,
                                &mcmax,
                                mcbagb,
                                stardata,
                                caller_id);
        force_supernova = FALSE;
    }
    else
    {
        /*
         * The star wants to start thermal pulses,
         * but we should prevent it from happening. The core
         * should be set to mcmax so there is a supernova as
         * soon as possible.
         */
        Dprint("Force EAGB to explosion\n");
        newstar->stellar_type = EAGB;
        newstar->core_mass[CORE_CO] = mcmax;
        force_supernova = TRUE;

        /*
         * call stellar structure to set up the star, otherwise
         * the pre-SN star will not be set, and L=R=0 will cause errors
         */
        stellar_structure_EAGB(oldstar,
                               newstar,
                               mcbagb,
                               stardata,
                               caller_id,
                               TRUE);
        newstar->core_mass[CORE_CO] = mcmax;
    }

    if(convert_to_helium_star == FALSE)
    {
        /*
         * Always check the core cannot exceed the total mass
         */
        newstar->core_mass[CORE_He] = Min(newstar->core_mass[CORE_He],
                                          newstar->mass);

        /*
         * And check that the CO core mass does not exceed the He core mass
         */
        newstar->core_mass[CORE_CO] = Min(newstar->core_mass[CORE_CO],
                                          newstar->core_mass[CORE_He]);

        /*
         * Determine how carbon ignites: see Section 6
         * of Hurley+ 2000
         *
         * Defaults:
         * stardata->preferences->maximum_mcbagb_for_degenerate_carbon_ignition = 1.6
         * stardata->preferences->minimum_mcbagb_for_nondegenerate_carbon_ignition = 2.25
         * stardata->preferences->minimum_CO_core_mass_for_carbon_ignition = 1.08
         * stardata->preferences->minimum_CO_core_mass_for_neon_ignition = 1.42
         */
        const unsigned int carbon_ignition =
            /* if MCO < 1.08, no carbon ignition */
            newstar->core_mass[CORE_CO] < stardata->preferences->minimum_CO_core_mass_for_carbon_ignition ? CARBON_IGNITION_NONE :
            /* if MCO > 1.08 and mcbagb < 1.6 : degenerate ignition */
            mcbagb < stardata->preferences->maximum_mcbagb_for_degenerate_carbon_ignition ? CARBON_IGNITION_DEGENERATE :
            /* MCO > 1.08 and 1.6 < mcbagb < 2.25  : partially degenerate */
            mcbagb < stardata->preferences->minimum_mcbagb_for_nondegenerate_carbon_ignition ? CARBON_IGNITION_PARTIALLY_DEGENERATE :
            /* MCO > 1.08 and mcbagb > 2.25 : non-degenerate ignition */
            CARBON_IGNITION_NONDEGENERATE;

        /*
         * Determine whether neon can ignite
         */
        const Boolean neon_ignition =
            carbon_ignition != CARBON_IGNITION_NONE ?
            Boolean_(newstar->core_mass[CORE_CO] > stardata->preferences->minimum_CO_core_mass_for_neon_ignition) :
            FALSE;

        /*
         * Determine if we have reached mcmax hence trigger a SN,
         * or have stripped the envelope
         */
        const Boolean supernova =
            newstar->deny_SN == 0 &&
            (force_supernova == TRUE ||
             (carbon_ignition != CARBON_IGNITION_NONE &&
              Boolean_(newstar->core_mass[CORE_CO] > mcmax)));

        if(supernova == TRUE &&
           newstar->stellar_type == TPAGB)
        {
            Append_logstring(LOG_MCHAND,
                             "0TPAGB core MCh reached");
        }

        Dprint("supernova? %d\n", supernova);

        const Boolean envelope_lost =
            More_or_equal(newstar->core_mass[CORE_He], newstar->mass) ||
            More_or_equal(newstar->core_mass[CORE_CO], newstar->mass);

        if(envelope_lost)
        {
            Dprint("TPAGB env lost MHe=%g MCO=%g M=%g\n",
                   newstar->core_mass[CORE_He],
                   newstar->core_mass[CORE_CO],
                   newstar->mass
                );
        }

        Dprint("CO core now %g start %g mcmax %g McHE=%g McCO=%g stellar_type %d (L=%g R=%g) : envelope lost? %s\n",
               newstar->core_mass[CORE_CO],
               mc_CO_start_AGB,
               mcmax,
               newstar->core_mass[CORE_He],
               newstar->core_mass[CORE_CO],
               newstar->stellar_type,
               newstar->luminosity,
               newstar->radius,
               Yesno(envelope_lost)
            );

        /*
         * Check that the core does not collapse or explode
         */
        *rg = newstar->radius;

        /*
         * test whether the CO core mass
         * exceeds either the total mass
         * or the maximum allowed core mass.
         */
        Dprint("COMPARE st=%d mc_CO=%12.12e vs mcmax=%12.12e (MCh=%g) r=%12.12e\n",
               newstar->stellar_type,
               newstar->core_mass[CORE_CO],
               mcmax,
               MCh,
               newstar->radius);

        Dprint("MC_CO=%g mcmax=%g mdot=%g dt=%g\n",
               newstar->core_mass[CORE_CO],
               mcmax,
               newstar->derivative[DERIVATIVE_STELLAR_MASS_WIND_LOSS],
               stardata->model.dt);

        Dprint("Mc1TP0 st=%d mc_CO=%12.12e (McCO start AGB = %g) vs mcmax=%12.12e (MCh=%g McSN=%g) r=%12.12e\n",
               newstar->stellar_type,
               newstar->core_mass[CORE_CO],
               mc_CO_start_AGB,
               mcmax,
               mcSN,
               MCh,
               newstar->radius);

        const double _envelope_mass = newstar->mass - newstar->core_mass[CORE_He];

        /*
         * Mass accretion rate: usually this is from the
         * previous timestep, so may not be accurate even if
         * it's the best we can do at this point.
         */
        const double net_accretion_rate = Mdot_net(newstar);

        /*
         * If net accretion rate > 0, and the star does not
         * explode, do not (yet?) become a WD. In this case,
         * the slow accretion will lead to flashes on the surface.
         * However, the surface will always be hydrogen or helium
         * rich, not a COWD or ONeWD.
         */
        if(supernova == FALSE &&
           net_accretion_rate < REALLY_TINY &&
           Less_or_equal(_envelope_mass, 1e-10))
        {
            /*
             * Envelope lost : nuclear burning stops, convert to WD
             */
            newstar->stellar_type =
                (ignite_carbon == FALSE || newstar->compact_core_type > COWD) ?
                COWD : ONeWD;

#ifdef WD_KICKS
            if(stardata->preferences->wd_kick_when == WD_KICK_END_AGB)
            {
                /* activate WD kick */
                newstar->kick_WD = TRUE;
            }
#endif // WD_KICKS

            newstar->num_thermal_pulses = -1.0; /* resets */
            newstar->num_thermal_pulses_since_mcmin = 0.0;
            newstar->phase_start_mass = newstar->mass;
            newstar->age = 0.0;
        }

        if(envelope_lost == TRUE && supernova == FALSE)
        {
            /*
             * Form a new white dwarf
             */
            newstar->age = 0.0;
            newstar->mass = Outermost_core_mass(newstar);

#ifdef WD_KICKS
            if(stardata->preferences->wd_kick_when == WD_KICK_END_AGB)
            {
                /* activate WD kick */
                newstar->kick_WD = TRUE;
            }
#endif
//            set_no_core(newstar);
            if(ignite_carbon == FALSE)
            {
                /*
                 * Zero-age Carbon/Oxygen White Dwarf
                 */
                newstar->stellar_type = COWD;
                //newstar->core_mass[CORE_CO] = newstar->mass;
                if(DEBUG)
                {
                    char * cores = core_string(newstar,FALSE);
                    Dprint("Zero-age CO WD mass = %g, cores %s \n",
                           newstar->mass,
                           cores);
                    Safe_free(cores);
                }
            }
            else
            {
                /*
                 * Zero-age Oxygen/Neon White Dwarf
                 *
                 * Burn the CO core to ONe
                 */
                newstar->stellar_type = ONeWD;
                newstar->core_mass[CORE_ONe] = newstar->core_mass[CORE_CO];
                Dprint("Zero-age O Ne White Dwarf\n");
            }

            /*
             * Reset the TPAGB parameters for this star
             */
            newstar->num_thermal_pulses = -1.0; /* resets */
            newstar->num_thermal_pulses_since_mcmin = 0.0;
            newstar->phase_start_mass = newstar->mass;
        }

        if(supernova == TRUE)
        {
            /*
             * If the star *was* a TPAGB star, and still is,
             * then we're fine. However, if the star was a CHeB
             * star then pretend it's still CHeB for the
             * purpose of calculating the remnant mass;
             */
            const int stype = newstar->stellar_type; /* save */
            if(oldstar->stellar_type == CHeB &&
               newstar->stellar_type == TPAGB)
            {
                newstar->stellar_type = CHeB;
            }

            if((carbon_ignition == CARBON_IGNITION_NONDEGENERATE ||
                carbon_ignition == CARBON_IGNITION_PARTIALLY_DEGENERATE) &&
               neon_ignition == TRUE)
            {
#ifndef PPISN
                /*
                 * Carbon ignites non-degenerately, then neon,
                 * then oxygen, silicon until an iron core forms.
                 * This collapses in a type II supernova.
                 */
                newstar->SN_type = SN_II;
                double remnant_baryonic_mass;
                const double MAYBE_UNUSED remnant_mass =
                    ns_bh_mass(newstar->mass,
                               newstar->core_mass[CORE_CO],
                               stardata,
                               newstar,
                               &remnant_baryonic_mass);
                newstar->stellar_type = stype; /* restore */
                Dprint("STELLAR_STRUCTURE core collapse SN, McHe = %g, McCO = %g -> M_remna nt = %g (baryonic %g)\n",
                       newstar->core_mass[CORE_He],
                       newstar->core_mass[CORE_CO],
                       remnant_mass,
                       remnant_baryonic_mass);
#else // PPISN
                /*
                 * Check for PPISN
                 * If the star is massive enough to ignite neon, it will be massive enough
                 * to be in the range of PPISN (which requires a Co core of about 30+ solar mass)
                 */
                double ppisn_mremnant = -1.0;
                double ppisn_mbaryonic = -1.0;
                int ppisn_type = PPISN_TYPE_NONE;

#undef X
#define X(TYPE,STRING,SN_TYPE,REMNANT_TYPE) SN_TYPE,
                static const Supernova ppisn_types[] = { PPISN_TYPES_LIST };
#undef X
#define X(TYPE,STRING,SN_TYPE,REMNANT_TYPE) REMNANT_TYPE,
                static const Stellar_type remnant_types[] = { PPISN_TYPES_LIST };
#undef X

                Dprint("Calling pulsational_pair_instability\n");

                /*
                 * This function call will use the function passed
                 * in by the user to determine whether the
                 * star undergoes CC, PPISN, PISN or PHDIS. The results
                 * are written to the ppisn_remnant and ppisn_type vars.
                 *
                 * Currently, if the pulsational_pair_instability routine returns the  &ppisn_type == PPISN_TYPE_CORE_COLLAPSE, we handle it via the normal method of core collapse (e.g fryer)
                 */
                pulsational_pair_instability(
                    stardata,
                    newstar,
                    newstar->mass,
                    newstar->core_mass[CORE_CO],
                    &ppisn_mremnant,
                    &ppisn_mbaryonic,
                    &ppisn_type
                    );

                if(ppisn_mremnant < -TINY)
                {
                    Exit_binary_c(BINARY_C_ALGORITHM_OUT_OF_RANGE,
                                  "ppisn_mremn-ant mass (=%g) not set correctly",
                                  ppisn_mremnant);
                }

                Dprint("ppisn_mremn-ant: %g ppisn_type: %d = %s\n",
                       ppisn_mremnant,
                       ppisn_type,
                       ppisn_type_strings[ppisn_type]);

                /*
                 * Decide what to do now:
                 * If PPISN happened, check which SN type
                 * it should be. This needs to happen before
                 * the new_supernova call.
                 */
                if(ppisn_type != PPISN_TYPE_NONE &&
                   ppisn_type != PPISN_TYPE_ERROR)
                {
                    /*
                     * Set the SN type
                     */
                    newstar->SN_type = ppisn_types[ppisn_type];
                    newstar->stellar_type = stype; /* restore */
                }
                else
                {
                    /*
                     * PPISN routine gave an error: assume
                     * normal core collapse.
                     *
                     * Carbon ignites non-degenerately, then neon,
                     * then oxygen, silicon until an iron core forms.
                     * This collapses in a type II supernova.
                     */
                    newstar->SN_type = SN_II;
                    double remnant_baryonic_mass;
                    const double MAYBE_UNUSED remnant_graviational_mass =
                        ns_bh_mass(newstar->mass,
                                   newstar->core_mass[CORE_CO],
                                   stardata,
                                   newstar,
                                   &remnant_baryonic_mass);
                    newstar->stellar_type = stype; /* restore */

                    Dprint("STELLAR_STRUCTURE core collapse SN, McHe = %g, McCO = %g -> M_remna nt = %g\n",
                           newstar->core_mass[CORE_He],
                           newstar->core_mass[CORE_CO],
                           remnant_graviational_mass);
                }
#endif // PPISN

                struct star_t * const news =
                    new_supernova(stardata,
                                  newstar,
                                  Other_star_struct(newstar),
                                  newstar);

#ifndef PPISN //
                if(news)
                {
                    news->age = 0.0;
                    news->mass = remnant_mass;
                    news->baryonic_mass = remnant_baryonic_mass;
                    set_no_core(news);

                    if(Less_or_equal(news->mass,
                                     stardata->preferences->max_neutron_star_mass))
                    {
                        /*
                         * Zero-age Neutron star
                         */
                        Dprint("Zero-age NS\n");
                        news->stellar_type = NEUTRON_STAR;
                        news->core_mass[CORE_NEUTRON] = news->mass;
                    }
                    else
                    {
                        /*
                         * Zero-age Black hole
                         */
                        Dprint("Zero-age Black Hole\n");
                        news->stellar_type = BLACK_HOLE;
                        news->core_mass[CORE_BLACK_HOLE] = news->mass;
                    }
                }
#else
                if(news)
                {
                    /*
                     * Either a CORE_COLLAPSE, PPISN, PISN or PHDIS occured.
                     */
                    if(ppisn_type != PPISN_TYPE_NONE &&
                       ppisn_type != PPISN_TYPE_ERROR &&
                       ppisn_type != PPISN_TYPE_CORE_COLLAPSE)
                    {
                        news->stellar_type = remnant_types[ppisn_type];
                        news->undergone_ppisn_type = ppisn_type;
                        news->mass = ppisn_mremnant;
                        news->baryonic_mass = ppisn_mbaryonic;

                        Dprint("UNDERGONE %s: creating %s of new stellar type %d ... undergone_ppisn_type: %d Mgrav %g, Mbary %g\n",
                               ppisn_type_strings[ppisn_type],
                               Stellar_type_string(news->stellar_type),
                               news->stellar_type,
                               news->undergone_ppisn_type,
                               news->mass,
                               news->baryonic_mass);

                        if(news->stellar_type == MASSLESS_REMNANT)
                        {
                            stellar_structure_make_massless_remnant(stardata,
                                                                    news);
                        }
                    }
                    else
                    {
                        /*
                         * So the PPISN routines either decided we are
                         * in the range of core collapse, or if the
                         * ppsin_type is still PPISN_TYPE_ERROR we somehow got here.
                         *
                         * We can decide whether then to use the default method
                         * for core collapse SN mass: ns_bh_mass
                         *
                         * But we can also decide to take the value that the
                         * ppisn routine provides.
                         *
                         * For now I decide to use the standard ns_bh_mass result
                         * whenever there is a core collapse.
                         */
                        news->mass = ns_bh_mass(newstar->mass,
                                                newstar->core_mass[CORE_CO],
                                                stardata,
                                                newstar,
                                                &news->baryonic_mass);
                        news->undergone_ppisn_type = PPISN_TYPE_CORE_COLLAPSE;

                        Dprint("Setting mass after Core collapse according to PPISN routine (using ns bh mass.c): %g",
                               news->mass);

                        if(Less_or_equal(news->mass,
                                         stardata->preferences->max_neutron_star_mass))
                        {
                            /*
                             * Zero-age Neutron star
                             */
                            Dprint("Zero-age NS\n");
                            news->stellar_type = NEUTRON_STAR;
                        }
                        else
                        {
                            /*
                             * Zero-age Black hole
                             */
                            Dprint("Zero-age Black Hole\n");
                            news->stellar_type = BLACK_HOLE;
                        }
                    }
                    news->age = 0.0;
                    set_no_core(news);
                }
#endif // PPISN
            }
            else if((carbon_ignition == CARBON_IGNITION_PARTIALLY_DEGENERATE ||
                     carbon_ignition == CARBON_IGNITION_NONDEGENERATE) &&
                    neon_ignition == FALSE)
            {
                /*
                 * Carbon ignites, we don't care how, but neon
                 * does not. This is an electron capture supernova.
                 */
                newstar->SN_type = SN_ECAP;
                double remnant_baryonic_mass;
                const double remnant_mass =
                    ns_bh_mass(newstar->mass,
                               newstar->core_mass[CORE_CO],
                               stardata,
                               newstar,
                               &remnant_baryonic_mass);
                newstar->stellar_type = stype; /* restore */

                Dprint("STELLAR_STRUCTURE core collapse SN, McHe = %g, McCO = %g -> remn_ant mgrav %g mbary %g\n",
                       newstar->core_mass[CORE_He],
                       newstar->core_mass[CORE_CO],
                       remnant_mass,
                       remnant_baryonic_mass);

                Dprint("make news stardata = %p newstar = %p other = %p\n",
                       (void*)stardata,
                       (void*)newstar,
                       (void*)Other_star_struct(newstar));

                struct star_t * const news =
                    new_supernova(stardata,
                                  newstar,
                                  Other_star_struct(newstar),
                                  newstar);
                if(news)
                {
                    news->age = 0.0;
                    news->mass = remnant_mass;
                    news->baryonic_mass = remnant_baryonic_mass;
                    Dprint("Set ECAP Mbary=%g\n",news->baryonic_mass);
                    set_no_core(news);
                    if(Less_or_equal(news->mass,
                                     stardata->preferences->max_neutron_star_mass))
                    {
                        /*
                         * Zero-age Neutron star
                         */
                        Dprint("Zero-age NS\n");
                        news->stellar_type = NEUTRON_STAR;
                        news->core_mass[CORE_NEUTRON] = news->mass;
                    }
                    else
                    {

                        /*
                         * Zero-age Black hole
                         */
                        Dprint("Zero-age Black Hole\n");
                        news->stellar_type = BLACK_HOLE;
                        news->core_mass[CORE_BLACK_HOLE] = newstar->mass;
                    }
                }
            }
            else if(carbon_ignition == CARBON_IGNITION_DEGENERATE)
            {
                /*
                 * Star is not massive enough to start C burning,
                 * which means mcbagb < 1.6 Msun,
                 * yet the star reaches MCh with a hydrogen envelope.
                 *
                 * The core is degenerate carbon and oxygen,
                 * so no remnant is left after the SN (type "IIa" SN)
                 */
                Dprint("STELLAR_STRUCTURE SN IIa\n");
                newstar->SN_type = SN_IIa;

                struct star_t * const news =
                    new_supernova(stardata,
                                  newstar,
                                  Other_star_struct(newstar),
                                  newstar);
                if(news)
                {
                    stellar_structure_make_massless_remnant(stardata,
                                                            news);
                }
            }
        }
    }

    Dprint("stellar_structure_AGB return stellar type %d radius %g mc He %g CO %g\n",
           newstar->stellar_type,
           newstar->radius,
           newstar->core_mass[CORE_He],
           newstar->core_mass[CORE_CO]);
    return newstar->stellar_type;

}
#endif//BSE
