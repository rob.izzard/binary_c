#include "../binary_c.h"
No_empty_translation_unit_warning;


/*
 * Function to return TRUE if the star ignites helium on the giant branch
 */
Boolean Pure_function stellar_structure_giant_branch_helium_ignition(struct stardata_t * const stardata Maybe_unused,
                                                                     struct star_t * const star Maybe_unused)
{
    const Boolean x =
    Less_or_equal(star->phase_start_mass,
                  stardata->common.metallicity_parameters[ZPAR_MASS_FGB])
        ;

    return x;
}
