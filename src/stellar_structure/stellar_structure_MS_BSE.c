#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef BSE
#include "stellar_structure_debug.h"

#define MSR_DEC_ARGS double mass Maybe_unused,double age Maybe_unused,double tau Maybe_unused,double tau1 Maybe_unused,double tau2 Maybe_unused,double tp1 Maybe_unused,double tp2 Maybe_unused,double lum Maybe_unused,double rzams Maybe_unused,double TAMS_radius Maybe_unused,double zp1 Maybe_unused,const double *main_sequence_parameters Maybe_unused,const double *metallicity_parameters Maybe_unused,struct star_t *newstar Maybe_unused,struct stardata_t * stardata Maybe_unused

#define MSR_ARGS newstar->age,tau,tau1,tau2,tp1,tp2,newstar->luminosity,newstar->rzams,newstar->TAMS_radius,zp1,main_sequence_parameters,metallicity_parameters,newstar,stardata

static double PMS_bisect_func(const double lum,
                              void * p);
static double main_sequence_radius(MSR_DEC_ARGS);

/*
 * BSE function to calculate main sequence
 * relative age, hence luminosity and
 * radius. Also sets core mass to zero.
 *
 * See Hurley et al 2000 (H00) for details.
 */

Stellar_type stellar_structure_MS_BSE(struct star_t * Restrict const newstar,
                                      struct stardata_t * Restrict const stardata)
{
    const double * main_sequence_parameters=stardata->common.main_sequence_parameters;
    const double * metallicity_parameters=stardata->common.metallicity_parameters;

    /************************/
    /** Main sequence star **/
    /************************/
    double tau,tau1,tau2,ithook,tp,tp1,tp2;
    double alpha,logl,zp1=metallicity_parameters[ZPAR_MASS_MS_HOOK];
    Dprint("MS star\n");

    /* MS stars have no He, CO or GB core */
    set_no_core(newstar);

    newstar->derivative[DERIVATIVE_STELLAR_He_CORE_MASS] = 0.0;
    newstar->derivative[DERIVATIVE_STELLAR_GB_CORE_MASS] = 0.0;
    newstar->derivative[DERIVATIVE_STELLAR_CO_CORE_MASS] = 0.0;

    /*
     * Calculate the maximum convective core
     * mass on the MS.
     * In low-mass stars this is zero.
     */
    const double fconvcore =
        effective_core_mass_fraction_of_MS_stars(
            newstar->mass,
            stardata->common.metallicity
            );

    newstar->max_MS_core_mass = Max(newstar->max_MS_core_mass,
                                    newstar->mass * fconvcore);

#if defined NUCSYN && defined NUCSYN_FIRST_DREDGE_UP_ACCRETION_CORRECTION
    /* save the main sequence mass, which will remain at the terminal MS value */
    newstar->MS_mass = newstar->phase_start_mass;
#endif

    /* convert to "low mass main sequence" star if mass is low enough */
    newstar->stellar_type = (newstar->phase_start_mass < zp1 - 0.3) ?
        LOW_MASS_MAIN_SEQUENCE : MAIN_SEQUENCE;
    newstar->core_stellar_type = MASSLESS_REMNANT;

    /*
     * tau is the fractional age (tm=MS lifetime)
     * H00 Eq. 11
     */
    tau = Is_zero(newstar->tm) ? 0.0 : (newstar->age/newstar->tm);


    /*
     * MS "hook" time, thook, H00 Eq. 7
     */
    double thook = newstar->bse->timescales[T_BGB] *
        thookf(newstar->phase_start_mass,
               main_sequence_parameters);

    /* calculate ratio of age to time to the MS hook */
    ithook=newstar->age/thook;

    /*
     * Times relative to thook, H00 Eqs. 14 and 15
     */
    tau1 = Min(1.0,ithook);
    tau2 = (ithook-1.0)*100.0 + 1.0;
    tau2 = Min(1.0,tau2);
    tau2 = Max(0.0,tau2);
    tp = Pow2(tau);
    tp1 = Pow2(tau1);
    tp2 = Pow2(tau2);

    /*
     * MS luminosity H00 Eq. 12.
     * alpha is calculted using H00 Eq.19
     */
    alpha=lalphf(newstar->phase_start_mass,main_sequence_parameters);
    Dprint("alpha = %g\n",alpha);
    logl = alpha*tau-lhookf(newstar->phase_start_mass,zp1,main_sequence_parameters)*(tp1-tp2)+
        (log10(newstar->bse->luminosities[L_END_MS]/newstar->bse->luminosities[L_ZAMS])-alpha)*tp;
    if(tau>TAUMIN)
    {
        Dprint("tau %g > TAUMIN %g\n",tau,TAUMIN);
        /*
         * beta is calculated using H00 Eq. 20
         */
        logl += lbetaf(newstar->phase_start_mass,main_sequence_parameters)
            *(pow(tau,lnetaf(newstar->phase_start_mass,main_sequence_parameters))-tp);
    }
    Dprint("LZAMS = %g\n",newstar->bse->luminosities[L_ZAMS]);

    /* hence calculate luminosity */
    newstar->luminosity = newstar->bse->luminosities[L_ZAMS]*exp10(logl);

    /* save end of MS luminosity */
    newstar->TAMS_luminosity = newstar->luminosity;

    /* calculate radius */
    Dprint("MS: phase_start_mass = %g, logl = %g, L = %g ",
           newstar->phase_start_mass,
           logl,
           newstar->luminosity
        )
    newstar->radius = main_sequence_radius(newstar->phase_start_mass,
                                           MSR_ARGS);

    if(stardata->preferences->require_drdm == TRUE)
    {
        const double dm = 1e-7 * newstar->phase_start_mass;
        newstar->drdm = (main_sequence_radius(newstar->phase_start_mass + dm,
                                              MSR_ARGS)
                         -
                         newstar->radius) / dm;
    }

    Dprint("Fitted R=%g L=%g\n",newstar->radius,newstar->luminosity);


#ifdef MAIN_SEQUENCE_STRIP
    /*
     * Calculate the radius the star would have in the absence of mass loss
     * and modify it
     */
    {
        const double pms_mass = stardata->common.zero_age.mass[star->starnum];
        double fr,r_no_massloss =
            main_sequence_radius(pms_mass,MSR_ARGS);

        if(stardata->model.in_RLOF==TRUE &&
           Is_zero(newstar->tms_at_start_caseA))
        {
            newstar->tms_at_start_caseA=tm;
#ifdef MAIN_SEQUENCE_STRIP_DEBUG
            printf("CASE A START AT t=%g (tms=%g)\n",
                   stardata->model.time,
                   newstar->tms_at_start_caseA);
#endif
        }

        double taur = stardata->model.time / newstar->tms_at_start_caseA;
        taur = Min(1.0,taur);

        fr= radius_stripped(pms_mass,
                            taur,
                            newstar->mass,
                            *r,
                            star,
                            stardata);

        *r = r_no_massloss * fr;

        /*
         * save the radius for the HG interpolation function
         */
        newstar->main_sequence_radius = *r;
    }
#endif


    Dprint("MS radius %g\n",newstar->radius);

    newstar->white_dwarf_atmosphere_type =
        WHITE_DWARF_ATMOSPHERE_HYDROGEN;

    return newstar->stellar_type;
}

static double main_sequence_radius(MSR_DEC_ARGS)
{
    /* main-sequence radius from BSE */
    double r;
    const double alpha = ralphf(mass,
                                main_sequence_parameters);

    const double tp3 = Pow3(tau);
    const double dtau3 = tp1*tau1-tp2*tau2;
    double logr = alpha*tau;
    logr += - rhookf(mass,zp1,main_sequence_parameters) * dtau3;
    logr += (log10(TAMS_radius/rzams)-alpha)*tp3;
    Dprint("logr(tau = %g) = %g\n",
           tau,
           logr);

    if(tau > TAUMIN)
    {
        const double xtmp  = pow(tau,10.0);
        const double beta  = rbetaf(mass,main_sequence_parameters);
        const double gamma = rgammf(mass,main_sequence_parameters);
        Dprint("xtmp = %g, beta = %g, gamma = %g\n",xtmp,beta,gamma);
        logr +=  xtmp*(beta + gamma*Pow3(xtmp)) - (beta + gamma)*tp3 ;
        Dprint("tau = %g > taumin = %g -> logr = %g\n",tau,TAUMIN,logr);
    }

    r = rzams * exp10(logr);
    Dprint("r = (rzams = %g) * 10^%g = %g\n",
           rzams,
           logr,
           r);

    if(mass<zp1-0.3)
    {
        /*
         * This following is given by Chris for low mass MS stars which will be
         * substantially degenerate. We need the Hydrogen abundance, X, which we
         * calculate from Z assuming that the helium abundance, Y, is calculated
         * according to Y = 0.24 + 2*Z.
         *
         * RGI: If we're following nucleosynthesis, we can use X from that.
         */
#ifdef NUCSYN
        const Abundance X = newstar->Xenv[XH1];
#else
        const Abundance X = metallicity_parameters[11];
#endif
        r = Max(r,
                0.0258*pow(1.0 + X, 5.0/3.0) / cbrt(mass));
    }

    if(In_range(mass, 0.0026, 0.077))
    {
        /*
         * From Burrows et al. 1993 for stars with
         * 0.0026 < M/Msun < 0.077
         */
        const double x = log10(mass/0.0026);
        r = Min(r,
                0.117 - 0.054*Pow2(x) + 0.024*Pow3(x));
    }

    newstar->TAMS_core_mass = mctmsf(newstar->phase_start_mass);

#ifdef PRE_MAIN_SEQUENCE
    if(stardata->preferences->pre_main_sequence == TRUE)
    {
        const double f = preMS_radius_factor(mass,1e6*age);
        r *= f;
        newstar->PMS = Boolean_(f>1.00001);

        Dprint("preMS %d f=%g r=%g roche_radius=%g roche_radius_at_periastron=%g PMS? %d\n",
               stardata->model.model_number,
               f,
               r,
               newstar->roche_radius,
               newstar->roche_radius_at_periastron,
               newstar->PMS);

        if(newstar->PMS == TRUE)
        {
            /*
             * We have R, what would L be if the star were a red giant?
             *
             * We bisect the red giant radius function as a function
             * of luminosity from BSE to work this out
             */
            const double * const giant_branch_parameters = stardata->common.giant_branch_parameters;
            const double a = giant_branch_parameters[20]*pow(newstar->mass,-giant_branch_parameters[21]);
            const double result = giant_branch_parameters[22]*pow(newstar->mass,-giant_branch_parameters[23]);
            const double _A = Min(a,result);
            const double _B = giant_branch_parameters[18];
            const double _C = giant_branch_parameters[17];
            const double _D = giant_branch_parameters[19];

            /*
             * rgbf() then is the following function (see bisect_func)
             */
            //0.0 = A * (pow(lum,B) + C*pow(lum,D)) / r - 1.0;

            int err;
            const double L = generic_bisect(&err,
                                            TRUE,
                                            100,
                                            PMS_bisect_func,
                                            newstar->radius,
                                            newstar->luminosity,//guess
                                            1e-4, //min L
                                            1e7, // max possible L
                                            100,//itmax
                                            FALSE,//uselog
                                            1.0,//alpha (use 1)
                                            /* arguments to the func */
                                            r,
                                            _A,
                                            _B,
                                            _C,
                                            _D);

            if(err == BINARY_C_BISECT_ERROR_NONE)
            {
                /*
                 * fx is a fudge factor so we smoothly intersect
                 * the ZAMS with the luminosity. It's not perfect
                 * but probably good enough until MINT comes online.
                 */
                const double fx = 1.0 / (1.0 + pow(0.01, f - 1.5 - 1.0));
                const double Lwas = newstar->luminosity;
                newstar->luminosity = (1.0 - fx) * Lwas + fx * L;
            }
        }
    }
#endif //` PRE_MAIN_SEQUENCE


#ifdef LAU2024_THERMAL_EXPANSION

    if(stardata->preferences->Lau2024_thermal_expansion)
    {
        /*
         * Correct for thermal expansion according to
         * Lau et al.'s 2024 model
         */
        const double mdot = Mdot_gain(newstar);


        if(mdot > 1e-20)
        {
            /*
             * data from Table 1
             */
            static struct data_table_t * table1 = NULL;
            if(table1==NULL)
            {
                static double _data[] = {
                    2,0.920,0.146,1.99,
                    3,1.17,0.276,1.36,
                    4,1.52,0.426,1.12,
                    5,2.58,0.844,0.722,
                    6,2.22,0.731,0.924,
                    8,3.77,1.37,0.727,
                    10,3.73,1.36,0.894,
                    12,5.45,2.06,0.763,
                    15,5.97,2.34,0.856,
                    20,6.26,2.42,1.19
                };
                NewDataTable_from_Pointer(
                    _data,
                    table1,
                    1,
                    3,
                    10
                    );
            }
            double params[1] = {newstar->mass};
            double result[3];
            Interpolate(table1,
                        params,
                        result,
                        FALSE);

            /*
             * Eq. 2
             */
            const double Reff = 2.0 * pow(newstar->mass,0.22);

            /*
             * Eq. 7a
             */
            const double Rmax1 = 540.0 * pow(mdot / 1e-3, 1.3);

            /*
             * Eq. 10
             * Note: a,b,c = result[0,1,2]
             */
            const double loglogRmax = exp10(result[0]) * pow(mdot,result[1]) - result[2];
            const double Rmax2 = exp10(exp10(loglogRmax)) * rzams;


            const double Rmax = Max(Rmax1, Rmax2);

            /*
             * Thermal adjustment timescale (years)
             */
            const double menv = newstar->mass;
            const double tth = kelvin_helmholtz_time_from_LMR(newstar->luminosity,
                                                              newstar->mass,
                                                              menv,
                                                              Reff);

            /*
             * Accretion timescale (years)
             */
            const double tacc = newstar->mass / mdot;
            printf("LAU2024: M %g R %g Mdot %g, Rmax1 %g, Rmax2 %g -> Rmax %g : tth = %g, tacc = %g\n",
                   newstar->mass,
                   newstar->radius,
                   mdot,
                   Rmax1,
                   Rmax2,
                   Rmax,
                   tth,
                   tacc);

            /* now what? */
            if(tacc < tth)
            {
                printf("FAST!\n");
            }
        }
    }
#endif // LAU_THERMAL_EXPANSION_2024

    return r;
}



static double PMS_bisect_func(const double lum,
                              void * p)
{
    /*
     * Function based on rgbf so we can bisect
     * to find the luminosity corresponding to the
     * current radius
     */
    Map_GSL_params(p,args);
    Map_varg(double,r,args);
    Map_varg(double,A,args);
    Map_varg(double,B,args);
    Map_varg(double,C,args);
    Map_varg(double,D,args);
    va_end(args);
    return A * (pow(lum,B) + C*pow(lum,D)) / r - 1.0;
}

#endif//BSE
