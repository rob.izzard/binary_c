#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef BSE

#include "stellar_structure_debug.h"
static Stellar_type stellar_structure_CHeB_implementation(struct star_t * const oldstar Maybe_unused,
                                                          struct star_t * const newstar,
                                                          double *const rg,
                                                          struct stardata_t * const stardata,
                                                          const Caller_id caller_id Maybe_unused,
                                                          const Boolean can_change_stellar_type);

Stellar_type stellar_structure_CHeB(struct star_t * const oldstar Maybe_unused,
                                    struct star_t * const newstar,
                                    double * const rg,
                                    struct stardata_t * const stardata,
                                    const Caller_id caller_id Maybe_unused)
{
    Stellar_type ret;

    if(stardata->preferences->require_drdm == TRUE)
    {
        const double dm = newstar->mass * 1e-7;
        update_mass(stardata,newstar,dm);
        stellar_structure_CHeB_implementation(oldstar,
                                              newstar,
                                              rg,
                                              stardata,
                                              caller_id,
                                              FALSE);
        const double rm = newstar->radius;
        update_mass(stardata,newstar,-dm);
        ret = stellar_structure_CHeB_implementation(oldstar,
                                                    newstar,
                                                    rg,
                                                    stardata,
                                                    caller_id,
                                                    TRUE);
        newstar->drdm = (rm - newstar->radius) / dm;
    }
    else
    {
        ret = stellar_structure_CHeB_implementation(oldstar,
                                                    newstar,
                                                    rg,
                                                    stardata,
                                                    caller_id,
                                                    TRUE);
    }

    return ret;
}


static Stellar_type stellar_structure_CHeB_implementation(struct star_t * const oldstar Maybe_unused,
                                                          struct star_t * const newstar,
                                                          double * const rg,
                                                          struct stardata_t * const stardata,
                                                          const Caller_id caller_id Maybe_unused,
                                                          const Boolean can_change_stellar_type)
{
    /*
     * Core helium burning star.
     */
#ifdef MINT
    /*
     * Check caller_id : if we're called from MINT, this is
     * because we are a backup call. Prevent recursion.
     */
    if(caller_id != STELLAR_STRUCTURE_CALLER_MINT
       &&
       stardata->preferences->stellar_structure_algorithm ==
       STELLAR_STRUCTURE_ALGORITHM_MINT)
    {
        /* let MINT handle this phase */
        return MINT_stellar_structure_CHeB(stardata,
                                           NULL,
                                           newstar,
                                           caller_id);
    }
#endif//MINT

    const double * metallicity_parameters=stardata->common.metallicity_parameters;
    const double * giant_branch_parameters=stardata->common.giant_branch_parameters;
    double tau,lx,ly,rx,ry,rmin,texp,taul,tauh,tau2,tloop;
    newstar->white_dwarf_atmosphere_type =
        WHITE_DWARF_ATMOSPHERE_HYDROGEN;

#if defined NUCSYN && defined NUCSYN_FIRST_DREDGE_UP

    /*
     * Check if we have done 1st dredge up
     */
    if(!RLOF_CALL &&
       newstar->first_dredge_up == FALSE &&
       newstar->stellar_type == GIANT_BRANCH)
    {
        Dprint("1DUP (CHeB) check mc=%12.12e vs mc1dup(evert)=%12.12e (newstar->stellar_type=%d)\n",
               newstar->core_mass[CORE_He],
               mc_1DUP(stardata,
                       newstar->phase_start_mass,
                       stardata->common.metallicity),
               newstar->stellar_type);

        /*
         * Enforce first dredge up if it has not yet happened
         */
        if(newstar->first_dredge_up==FALSE
           && newstar->stellar_type==FIRST_GIANT_BRANCH
           && newstar->effective_zams_mass<NUCSYN_WR_MASS_BREAK)
        {

            Dprint("NUCSYNs__ DUP1\n");
            newstar->first_dredge_up=TRUE;
            nucsyn_set_1st_dup_abunds(newstar->Xenv,
                                      newstar->mass,
                                      newstar->stellar_type,
                                      newstar,
                                      stardata,
                                      TRUE);
        }
#if (DEBUG==1)
        else
        {
            Dprint("(CHeB) Star already had first DUP?\n");
        }
#endif
    }
#ifdef NUCSYN_FIRST_DREDGE_UP_PHASE_IN
    newstar->mc_gb_was=0.0; // reset just in case
#endif
#endif /* NUCSYN_FIRST_DREDGE_UP and NUCSYN */

    const double accretion_dm = stardata->model.dt * Mdot_net(newstar);
    const Stellar_type donor_stellar_type = System_is_binary ? Other_star_struct(newstar)->stellar_type : -1;
    const double fractional_age_in = (newstar->age - newstar->bse->timescales[T_HE_IGNITION])/
        newstar->bse->timescales[T_HE_BURNING];
    Dprint("stellar structure CHEB in : fractional_age_in = %g, m0 %g\n",
           fractional_age_in,
           newstar->phase_start_mass);

    /*
     * Core mass evolution
     */
    {
        double mc0;
        const double mcbagb = mcagbf(newstar->phase_start_mass,
                                     giant_branch_parameters);

        if(Less_or_equal(newstar->phase_start_mass,
                         metallicity_parameters[ZPAR_MASS_HE_FLASH]))
        {
            mc0 = mcgbf(newstar->bse->luminosities[L_HE_IGNITION],
                        newstar->bse->GB,
                        newstar->bse->luminosities[L_LMX]);
        }
        else
        {
            mc0 = mcheif(newstar->phase_start_mass,
                         metallicity_parameters[ZPAR_MASS_HE_FLASH],
                         metallicity_parameters[10],
                         giant_branch_parameters);
        }

        tau = fractional_age_in;
        Clamp(tau,0.0,1.0);

        /*
         * Core growth : equivalent to (1-tau)*mc0 + tau*mcbagb
         * Hurley et al. 2000 Eq. 67.
         *
         * We do not let the core shrink.
         *
         * We leave a small envelope on the surface for numerical
         * stability when transferring mass (amount is in menv_min).
         */
        const double menv_min = (System_is_binary && Is_not_zero(accretion_dm)) ? 1e-3 : 0.0;
        const double mcwas = newstar->core_mass[CORE_He];
        set_no_core(newstar);
        Dprint("set Mc was %g\n",mcwas);
        newstar->core_mass[CORE_He] =
            Max(newstar->core_mass[CORE_He],
                Min(newstar->mass - menv_min,
                    mc0 + (mcbagb - mc0)*tau));
        Dprint("BSE wants the Mc=%g\n",newstar->core_mass[CORE_He]);

        if(System_is_binary)
        {
            Dprint("st=%d m=%g mcHe=%g\n",
                   newstar->stellar_type,
                   newstar->mass,
                   newstar->core_mass[CORE_He]);

            if(Is_zero(mcwas))
            {
                /*
                 * We've been converted from a HeMS or HeWD star
                 * with no helium core: first step is to enforce
                 * the accreted surface layer to be hydrogen
                 */
                Dprint("HeWD/HeMS -> CHeB conversion by accretion\n");
                newstar->core_mass[CORE_He] = newstar->mass - accretion_dm;
            }
            else if(accretion_dm > 0.0
                    &&
                    donor_stellar_type < HeMS
                    &&
                    newstar->mass - newstar->core_mass[CORE_He] < ACCRETION_LIMIT_H_ENV_CHeB)
            {
                /*
                 * Net-Accreting CHeB with a thin hydrogen shell
                 * which was probably a HeMS star that has accreted hydrogen:
                 * limit its core growth to the accretion rate
                 */
                Dprint("Accreting CHeB\n");
                Clamp(newstar->core_mass[CORE_He],
                      mcwas,
                      newstar->mass + accretion_dm);
            }
        }
        Dprint("CHEB mc=%g\n",newstar->core_mass[CORE_He]);

        Dprint("Core mass set %12.12e from mc0=%12.12e + bit=%12.12e (tau=%12.12e) : mass = %20.12e phase_start_mass = %20.12e\n",
               newstar->core_mass[CORE_He],
               mc0,
               (mcbagb-mc0)*(tau),
               tau,
               newstar->mass,
               newstar->phase_start_mass
            );
    }

    if(Less_or_equal(newstar->phase_start_mass,
                     metallicity_parameters[ZPAR_MASS_HE_FLASH]))
    {
        lx = newstar->bse->luminosities[L_HE_BURNING];
        ly = newstar->bse->luminosities[L_BAGB];
        rx = rzahbf(newstar->mass,
                    newstar->core_mass[CORE_He],
                    metallicity_parameters[ZPAR_MASS_HE_FLASH],
                    giant_branch_parameters);

        *rg = rgbf(newstar->mass,
                   lx,
                   giant_branch_parameters);
        rmin=(*rg)*pow(metallicity_parameters[13],newstar->phase_start_mass/metallicity_parameters[ZPAR_MASS_HE_FLASH]);


        texp = Limit_range(rmin/rx,0.4,2.5);
        ry = ragbf(newstar->mass,
                   ly,
                   metallicity_parameters[ZPAR_MASS_HE_FLASH],
                   giant_branch_parameters
#ifdef AXEL_RGBF_FIX
                   ,stardata->common.metallicity
#endif
            );

        if(rmin<rx)
        {
            taul = cbrt(log(rx/rmin));
        }
        else
        {
            rmin = rx;
            taul = 0.0;
        }

        tauh = cbrt(log(ry/rmin));
        tau2 = taul*(tau - 1.00) + tauh*tau;
        newstar->radius = rmin*exp(Pow3(fabs(tau2)));
        Dprint("r=%12.12e\n",newstar->radius);

        *rg += tau*(ry - *rg);
        newstar->luminosity = lx*pow((ly/lx),pow(tau,texp));
    }
    else if(newstar->phase_start_mass > metallicity_parameters[ZPAR_MASS_FGB])
    {
        /*
         * For HM stars He-ignition takes place at Rmin in the HG, and CHeB
         * consists of a blue phase (before tloop) and a RG phase (after tloop).
         */
        Dprint("Core Helium Burning High-mass Blue Phase and RG Phase\n");

        tau2 = tblf(newstar->phase_start_mass,
                    metallicity_parameters[ZPAR_MASS_HE_FLASH],
                    metallicity_parameters[ZPAR_MASS_FGB],
                    giant_branch_parameters,
                    stardata->common.metallicity);

        Dprint("tau2=%g phase_start_mass=%g ZPAR_MASS_HE_FLASH=%g ZPAR_MASS_FGB=%g HE_BURNING=%g\n",
               tau2,
               newstar->phase_start_mass,
               metallicity_parameters[ZPAR_MASS_HE_FLASH],
               metallicity_parameters[ZPAR_MASS_FGB],
               newstar->bse->timescales[T_HE_BURNING]);

        tloop = newstar->bse->timescales[T_HE_IGNITION] +
            tau2*newstar->bse->timescales[T_HE_BURNING];


        rmin = rminf(newstar->phase_start_mass,
                     giant_branch_parameters);
        *rg = rgbf(newstar->mass,
                   newstar->bse->luminosities[L_HE_IGNITION],
                   giant_branch_parameters);
        rx = ragbf(newstar->mass,
                   newstar->bse->luminosities[L_HE_IGNITION],
                   metallicity_parameters[ZPAR_MASS_HE_FLASH],
                   giant_branch_parameters
#ifdef AXEL_RGBF_FIX
                   ,stardata->common.metallicity
#endif
            );

        rmin = Min(rmin, rx);

        if(Less_or_equal(newstar->phase_start_mass,MLP))
        {
            texp = log(newstar->phase_start_mass/MLP)/log(metallicity_parameters[ZPAR_MASS_FGB]/MLP);
            rx = rmin*pow(*rg/rmin,texp);
        }
        else
        {
            rx = rmin;
        }

        Dprint("age = %g (helium ignites at %g) cf tloop = %g : %s CHeB phase\n",
               newstar->age,
               newstar->bse->timescales[T_HE_IGNITION],
               tloop,
               (newstar->age < tloop ? "Blue" : "Red"));

        texp = Limit_range(rmin/rx,0.4,2.5);
        newstar->luminosity = newstar->bse->luminosities[L_HE_IGNITION]*
            pow(newstar->bse->luminosities[L_BAGB]/newstar->bse->luminosities[L_HE_IGNITION],
                pow(tau,texp)) ;
        Dprint("CHEB L = %g from L_HE_IGNITION=%g L_BAGB=%g tau=%g texp=%g\n",
               newstar->luminosity,
               newstar->bse->luminosities[L_HE_IGNITION],
               newstar->bse->luminosities[L_BAGB],
               tau,
               texp);

        Dprint("newstar->age %g < tloop %g ? %s diff %g, tau2==0? %s\n",
               newstar->age,
               tloop,
               Yesno(newstar->age + TINY < tloop),
               newstar->age - tloop,
               Yesno(Is_zero(tau2)));
        if(newstar->age < tloop &&
           Is_not_zero(tau2))
        {
            /*
             * Blue loop : if tau2==0 this cannot happen
             */
            ly = newstar->bse->luminosities[L_HE_IGNITION]*
                pow(newstar->bse->luminosities[L_BAGB]/newstar->bse->luminosities[L_HE_IGNITION],pow(tau2,texp));
            ry = ragbf(newstar->mass,
                       ly,
                       metallicity_parameters[ZPAR_MASS_HE_FLASH],
                       giant_branch_parameters
#ifdef AXEL_RGBF_FIX
                       ,stardata->common.metallicity
#endif
                );

            taul = 0.0;
            if(Is_not_zero(rmin-rx)) taul = cbrt(log(rx/rmin));
            tauh = ry>rmin ? cbrt(log(ry/rmin)) : 0.0;
            tau = (newstar->age - newstar->bse->timescales[T_HE_IGNITION])/
                (tau2*newstar->bse->timescales[T_HE_BURNING]);
            Clamp(tau,0.0,1.0);
            Dprint("tau = %g from age=%g T_HE_IGNITION=%g tau2=%g T_HE_BURNING=%g\n",
                   tau,
                   newstar->age,
                   newstar->bse->timescales[T_HE_IGNITION],
                   tau2,
                   newstar->bse->timescales[T_HE_BURNING]);

             tau2 = taul*(tau - 1.00) + tauh*tau;

            /* ROB: this newstar->radius is not large enough to match Z=0.004 models? */
            newstar->radius = rmin*exp(Pow3(fabs(tau2)));
            Dprint("setting r (2) to %g from rmin %g, tau %g\n",
                   newstar->radius,
                   rmin,
                   tau
                );

            *rg += tau*(ry-*rg);
        }
        else
        {
            Dprint("call ragb M=%g L=%g Zpar=%g\n",
                   newstar->mass,
                   newstar->luminosity,
                   metallicity_parameters[ZPAR_MASS_HE_FLASH]);
            newstar->radius = ragbf(newstar->mass,
                                    newstar->luminosity,
                                    metallicity_parameters[ZPAR_MASS_HE_FLASH],
                                    giant_branch_parameters
#ifdef AXEL_RGBF_FIX
                                    ,stardata->common.metallicity
#endif
                );

            *rg = newstar->radius;
            Dprint("Set r=rg=%12.12e\n",newstar->radius);
        }
    }
    else
    {
        /*
         * For IM stars CHeB consists of a RG phase (before tloop) and a blue
         * loop (after tloop).
         */
        Dprint("Core Helium burning red giant phase then blue phase\n");
        tau2 = 1.0 - tblf(newstar->phase_start_mass,
                          metallicity_parameters[ZPAR_MASS_HE_FLASH],
                          metallicity_parameters[ZPAR_MASS_FGB],
                          giant_branch_parameters,
                          stardata->common.metallicity);
        tloop = newstar->bse->timescales[T_HE_IGNITION] + tau2*newstar->bse->timescales[T_HE_BURNING];

        if(newstar->age<tloop)
        {
            tau = (tloop - newstar->age)/(tau2*newstar->bse->timescales[T_HE_BURNING]);
            Clamp(tau,0.0,1.0);

            newstar->luminosity = newstar->bse->luminosities[L_HE_BURNING]*
                pow(newstar->bse->luminosities[L_HE_IGNITION]/newstar->bse->luminosities[L_HE_BURNING],Pow3(tau));

            newstar->radius = rgbf(newstar->mass,
                             newstar->luminosity,
                             giant_branch_parameters);
            Dprint("set r (3) from rgbf %12.12e\n",newstar->radius);

            *rg = newstar->radius;
        }
        else
        {
            lx = newstar->bse->luminosities[L_HE_BURNING];
            ly = newstar->bse->luminosities[L_BAGB];
            rx = rgbf(newstar->mass,lx,giant_branch_parameters);
            rmin = rminf(newstar->mass,giant_branch_parameters);

            texp = Limit_range(rmin/rx,0.4,2.5);
            ry = ragbf(newstar->mass,ly,metallicity_parameters[ZPAR_MASS_HE_FLASH],giant_branch_parameters
#ifdef AXEL_RGBF_FIX
                       ,stardata->common.metallicity
#endif
                );

            if(rmin<rx)
            {
                taul = cbrt(log(rx/rmin));
            }
            else
            {
                rmin = rx;
                taul = 0.0;
            }
            tauh = cbrt(log(ry/rmin));
            tau = (newstar->age - tloop)/(newstar->bse->timescales[T_HE_BURNING] - (tloop - newstar->bse->timescales[T_HE_IGNITION]));
            Clamp(tau,0.0,1.0);
            tau2 = taul*(tau - 1.0) + tauh*tau;

            newstar->radius = rmin*exp(Pow3(fabs(tau2)));
            Dprint("set r (4) %12.12e\n",newstar->radius);

            *rg = rx + tau*(ry - rx);
            newstar->luminosity = lx*pow(ly/lx,(pow(tau,texp)));
        }
    }

    Dprint("CHEB L=%g R=%g age=%g\n",
           newstar->luminosity,
           newstar->radius,
           newstar->age);
    Dprint("Test whether core mass (%12.12e) exceeds total mass (%12.12e)\n",
           newstar->core_mass[CORE_He],newstar->mass);

    /*
     * Test whether core mass exceeds total mass,
     * if so we form a HeMS star.
     * Note: if can_change_stellar_type is FALSE,
     * we cannot do this. This happens if we're just
     * after a radius for dR/dM calculation, for example.
     */
    if(can_change_stellar_type == TRUE)
    {
        if(More_or_equal(newstar->core_mass[CORE_He],
                         newstar->mass))
        {
            /*
             * Helium-burning core is exposed at the surface:
             * convert to an evolved MS naked helium star.
             */
            Dprint("Core mass (%g) exceeds total mass (%g) [accretion_dm=%g] -> Evolved MS naked He Star or HeWD\n",
                   newstar->core_mass[CORE_He],
                   newstar->mass,
                   accretion_dm);
            newstar->stellar_type = HeMS;
            newstar->core_stellar_type = MASSLESS_REMNANT;
        }
        else
        {
            Dprint("Setting newstar->stellar_type=CHeB here\n");
            newstar->stellar_type = CHeB;
            newstar->core_stellar_type = COWD;
        }

        if(newstar->stellar_type == HeMS)
        {
            Dprint("stellar structure CHeB : CHeB -> HeMS\n");

            /*
             * Make a HeMS star with the same amount of
             * helium burnt to carbon, i.e. preserve its
             * fractional age.
             */
            const double fractional_age = (newstar->age - newstar->bse->timescales[T_HE_IGNITION])/newstar->bse->timescales[T_HE_BURNING];
            Dprint("CHeB fractional age %g\n",
                   fractional_age);


            newstar->stellar_type = CHeB;
            Dprint("CHeB star: tscls %g %g %d\n",
                   newstar->phase_start_mass,
                   newstar->mass,
                   newstar->stellar_type);
            stellar_timescales(stardata,
                               newstar,
                               newstar->bse,
                               newstar->phase_start_mass,
                               newstar->mass,
                               newstar->stellar_type);

            Dprint("bse CHeB %g %g : %g\n",
                   newstar->bse->timescales[T_HE_IGNITION],
                   newstar->bse->timescales[T_HE_BURNING],
                   (newstar->age - newstar->bse->timescales[T_HE_IGNITION])/newstar->bse->timescales[T_HE_BURNING]);

            /*
             * now as a helium star
             */
            newstar->phase_start_mass = newstar->mass;
            newstar->stellar_type = HeMS;
            Dprint("He star: Set new phase start mass %g\n",
                   newstar->phase_start_mass);

            Dprint("tscls %g %g %d\n",
                   newstar->phase_start_mass,
                   newstar->mass,
                   newstar->stellar_type);
            stellar_timescales(stardata,
                               newstar,
                               newstar->bse,
                               newstar->phase_start_mass,
                               newstar->mass,
                               newstar->stellar_type);
            newstar->age = fractional_age * newstar->bse->tm;
            Dprint("bse HeMS %g : %g\n",
                   newstar->bse->tm,
                   newstar->age / newstar->bse->tm);
        }
    }

    Dprint("exit CHeB with stellar type %d age = %g \n",
           newstar->stellar_type,
           newstar->age);

    return newstar->stellar_type;
}
#endif // BSE
