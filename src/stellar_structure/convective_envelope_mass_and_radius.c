#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef BSE
/*
 * A function to estimate the mass and radius of the convective envelope,
 * as well as the gyration radius of the envelope.
 * N.B. Valid only for Z=0.02!
 *
 * The following stellar structure input is needed:
 *   star->stellar_type = stellar type
 *   star->phase_start_mass = zero-age stellar mass
 *   star->mass = actual mass
 *   Outermost_core_mass(star) = core mass
 *   star->luminosity = luminosity
 *   star->radius = radius
 *   star->core_radius = core radius
 *   star->age = age
 *   star->tm = main-sequence lifetime
 *   star->bse->luminosities[L_END_MS] = luminosity at TMS, lums(2)
 *   star->bse->luminosities[L_BGB] = luminosity at BGB, lums(3)
 *   star->bse->luminosities[L_HE_IGNITION] = luminosity at He ignition, lums(4)
 *   star->rzams = radius at ZAMS
 *   star->TAMS_radius = radius at TMS
 *   rg = giant branch or Hayashi track radius, approporaite for the type.
 *        For kw=1 or 2 this is radius at BGB, and for kw=4 either GB or
 *        AGB radius at present luminosity.
 *
 * RGI: I also limit the low-mass limit used below to calculate logm to
 *      0.1 Msun. Failing to do this results in negative k2bgb because the
 *      fit is (presumably) not valid for very low-mass stars/planets.
 *
 *      menv and renv used to be set to Max(whatever,1e-10) but there's
 *      no reason why this should be so. They could be zero, so we make
 *      their lower value be 0.0.
 *
 * This was the BSE function "mrenv".
 *
 * Note: BSE's "k2" is k^2 = I/(MR^2) where I is the moment of inertia,
 *       M,R are the mass and radius. This was renamed
 *       moment_of_inertia_factor in binary_c to avoid confusion with the
 *       gyration constant k_2. (argh!0
 */
#define MENV_MIN 0.0
#define RENV_MIN 0.0

void convective_envelope_mass_and_radius(struct stardata_t * const stardata,
                                         struct star_t * const star,
                                         const double rg,
                                         double z Maybe_unused) // z= metallicity
{
    const Boolean PMS =
#ifdef PRE_MAIN_SEQUENCE
        star->PMS
#else
        FALSE
#endif
        ;
    Dprint("Star %d : M=%g M0=%g Mc=%g Menv=%g st=%d L=%g R=%g Rc=%g J=%g omega=%g v_eq=%g v_crit_eq=%g PMS=%s : k2 was %g, called with rg = %g\n",
           star->starnum,
           star->mass,
           star->phase_start_mass,
           Outermost_core_mass(star),
           envelope_mass(star),
           star->stellar_type,
           star->luminosity,
           star->radius,
           star->core_radius,
           star->angular_momentum,
           star->omega,
           star->v_eq,
           star->v_crit_eq,
           Yesno(PMS),
           star->moment_of_inertia_factor,
           rg
        );

    /*
     * If the envelope has no radial extent, or mass,
     * renv and menv must be zero.
     *
     * We set the moment of inertia factor to be 0.21,
     * typical for a convective main-sequence star.
     */
    if(Fequal(star->radius,star->core_radius)||
       Fequal(star->mass,Outermost_core_mass(star)) ||
       star->mass < MAXIMUM_BROWN_DWARF_MASS)
    {
        star->menv = 0.0;
        star->renv = 0.0;
        star->moment_of_inertia_factor = 0.21;
        Dprint("No envelope %s%s%s : menv=renv=moment_of_inertia_factor=0\n",
               Fequal(star->radius,star->core_radius) ? "R==Rc " : "",
               Fequal(star->mass,Outermost_core_mass(star)) ? "M=Mc ": "",
               star->mass < MAXIMUM_BROWN_DWARF_MASS ? "M<MBD " : "");
    }
    else
    {
        double fmenv,frenv;
        double menvg,menvt,menvz,renvg,renvt,renvz,y,k2bgb,k2g,k2z;
        double tebgb=0.0,tauenv,tautms;
        const double ajtm = star->age/star->tm;
        Dprint("Star age = %g, tm = %g -> ajtm = %g\n",
               star->age,
               star->tm,
               ajtm);
        const double logm = log10(Max(0.1,star->phase_start_mass));
        const double A = Min(0.81,Max(0.68,0.68+0.4*logm));

        /*
         * Zero-age and BGB values of k^2 (== moment_of_inertia_factor = I/(MR^2))
         */
        k2z = Min(CORE_MOMENT_OF_INERTIA_FACTOR,
                  Max(0.09-0.27*logm,0.037+0.033*logm));
        if(logm>1.3) k2z -= 0.055*Pow2(logm-1.3);
        k2bgb = Min3(0.15,0.147+0.03*logm,0.162-0.04*logm);

        Dprint("k2bgb = %g\n",k2bgb);

        /*
         * Choose the luminosity we should use
         */

        const Stellar_type effective_stellar_type =
            (PMS == TRUE && star->luminosity>0.0)
            ? GIANT_BRANCH : star->stellar_type;

        switch(effective_stellar_type)
        {
        case GIANT_BRANCH:
        case CHeB:
        case EAGB:
        case TPAGB:
            /*
             * Envelope k^2 for giant-like stars; this will be modified for non-giant
             * CHeB stars or small envelope mass below.
             * Formula is fairly accurate for both FGB and AGB stars if M <= 10, and
             * gives reasonable values for higher masses. Mass dependence is on actual
             * rather than ZA mass, expected to work for mass-losing stars (but not
             * tested!). The slightly complex appearance is to insure continuity at
             * the BGB, which depends on the ZA mass.
             */
        {
            /*
             * Define L_BGB if required
             */
            if(Is_zero(star->bse->luminosities[L_BGB]))
            {
                star->bse->luminosities[L_BGB] = lbgbf(star->phase_start_mass,
                                                       stardata->common.giant_branch_parameters);
            }

            const double logmass = log10(star->mass);
            const double F = 0.208 + 0.125*logmass - 0.035*Pow2(logmass);
            double w = Pow1p5(star->mass);
            const double B = 1e-4*(1.0/w+0.1);
            w = (star->luminosity - star->bse->luminosities[L_BGB])*B;
            w *= w;
            y = (F - 0.033*log10(star->bse->luminosities[L_BGB]))/k2bgb - 1.0;
            y = Max(0.0,y);
            const double numerator = (F - 0.033*log10(star->luminosity) + 0.4*w);
            const double denominator = 1.0+y*(star->bse->luminosities[L_BGB]/star->luminosity)+w;
            k2g = numerator / denominator;

            Dprint("numerator = (F=%g) - (0.033 * log(L)=%g) + (0.4 * w=%g) = %g\n",
                   F,
                   log10(star->luminosity),
                   w,
                   numerator);
            Dprint("denominator 1.0+%g*(%g/%g)+%g = %g\n",
                   y,
                   star->bse->luminosities[L_BGB],
                   star->luminosity,
                   w,
                   denominator);
            Dprint("k2g = numerator/denominator = %g\n",k2g);
            Dprint("convective_envelope_mass_and_radius star->stellar_type=%d k2 mid star->moment_of_inertia_factor=%g k2g=%g from L=%g star->bse->luminosities[L_BGB]=%g star->mass=%g k2bgb=%g w=%g y=%g numerator=%g denominator=%g\n",
                   star->stellar_type,
                   star->moment_of_inertia_factor,
                   k2g,
                   star->luminosity,
                   star->bse->luminosities[L_BGB],star->mass,k2bgb,w,y,
                   (F - 0.033*log10(star->luminosity) + 0.4*w),
                   (1.0+y*(star->bse->luminosities[L_BGB]/star->luminosity)+w));
        }

        break;

        case HeGB:
            /*
             * Rough fit for for HeGB stars...
             */
        {
            const double B = 3e4 * Pow1p5(star->mass);
            const double w = Pow2(Max(0.0,star->luminosity/B-0.5));
            const double w04 = 0.4*w;
            k2g = (k2bgb + w04)/(1.0 + w04);
        }
        break;

        default:
            Dprint("Set k2g from k2bgb = %g\n",k2bgb);
            k2g = k2bgb;
        }

        if(effective_stellar_type < GIANT_BRANCH)
        {
            menvg = 0.5;
            renvg = 0.65;
        }
        else if(effective_stellar_type == GIANT_BRANCH &&
                star->luminosity < 3.0 * star->bse->luminosities[L_BGB])
        {
            /*
             * FGB stars still close to the BGB do not yet have a fully developed CE.
             */
            const double x = Min(3.0,star->bse->luminosities[L_HE_IGNITION] / star->bse->luminosities[L_BGB]);
            const double tau = Pow2(Max(0.0,Min(1.0,(x-star->luminosity / star->bse->luminosities[L_BGB])/(x-1.0))));
            menvg = 1.0 - 0.5*tau;
            renvg = 1.0 - 0.35*tau;
        }
        else
        {
            menvg = 1.0;
            renvg = 1.0;
        }


        double compact_k2 = 0.0;

        if(star->radius < rg || PMS == TRUE)
        {
            /*
             * Stars not on the Hayashi track: MS and HG stars, non-giant CHeB stars,
             * HeMS and HeHG stars, as well as giants with very small envelope mass.
             */
            if(effective_stellar_type < HeMS)
            {
                /*
                 * Envelope k^2 fitted for MS and HG stars.
                 * Again, pretty accurate for M <= 10 but less so for larger masses.
                 * [Note that this represents the whole star on the MS, so there is a
                 * discontinuity in stellar k^2 between MS and HG - okay for stars with a
                 * MS hook but low-mass stars should preferably be continous...]
                 *
                 * For other types of star not on the Hayashi track we use the same fit as
                 * for HG stars, this is not very accurate but has the correct qualitative
                 * behaviour. For CheB stars this is an overestimate because they appear
                 * to have a more centrally concentrated envelope than HG stars.
                 */
                const double tau=star->radius/star->rzams;
                Dprint("tau = %g from radius=%g / rzams=%g\n",
                       tau,star->radius,star->rzams);
                const double C = Max(-2.5,Min(-1.5,-2.5+5.0*logm));
                star->moment_of_inertia_factor = (k2z - 0.025) * pow(tau,C) + 0.025 * pow(tau,-0.1);
                Dprint("k2z = %g tau = %g C = %g \n",k2z,tau,C);
                Dprint("stellar type = %d < HeMS : star->moment_of_inertia_factor set to %g\n",
                       effective_stellar_type,star->moment_of_inertia_factor);
            }
            else if(effective_stellar_type==HeMS)
            {
                /*
                 * Rough fit for naked He MS stars.
                 */
                star->moment_of_inertia_factor = 0.08 - 0.03 * ajtm;
                Dprint("HeMS : star->moment_of_inertia_factor = %g from ajtm = %g\n",
                       star->moment_of_inertia_factor,
                       ajtm);
            }
            else if(effective_stellar_type<HeWD)
            {
                /*
                 * Rough fit for HeHG stars.
                 */
                const double rzams = rzhef(star->phase_start_mass);
                star->moment_of_inertia_factor = 0.08 * star->rzams / star->radius;
                Dprint("<HeWD : star->moment_of_inertia_factor = %g from RZAMS = %g / R=%g (recalc %g)\n",
                       star->moment_of_inertia_factor,
                       star->rzams,
                       star->radius,
                       rzams
                    );

                /*
                 * K2 can become very small when the rough fit
                 * breaks down: don't let this happen! Really
                 * we need a better solution to fix this ...
                 */
                star->moment_of_inertia_factor = Max(0.005,star->moment_of_inertia_factor);
            }

            compact_k2 = star->moment_of_inertia_factor;

            /*
             * tauenv measures proximity to the Hayashi track in terms of Teff.
             * If tauenv>0 then an appreciable convective envelope is present, and
             * k^2 needs to be modified.
             */
            const double iw=1.0/(1.0-A);
            if(effective_stellar_type<GIANT_BRANCH && !PMS)
            {
                tebgb = sqrt(sqrt(star->bse->luminosities[L_BGB])/rg);
                tauenv = tebgb/(sqrt(sqrt(star->luminosity)/star->radius));
            }
            else
            {
                tebgb = 0.0;
                tauenv = sqrt(star->radius/rg);
            }
            tauenv = Max(0.0,Min(1.0,iw*(tauenv-A)));
            Dprint("tauenv (proximity to Hayashi track) = %g\n",tauenv);

            /* NB iw=1/(1-A) */
            if(tauenv > 0.0)
            {
                fmenv = menvg*Pow5(tauenv);
                frenv = renvg*Pow5d4(tauenv);
                double xx = Pow3(tauenv) * (k2g - star->moment_of_inertia_factor);

                Dprint("Near Hayashi track: menv=%g renv=%g\n",
                       fmenv,
                       frenv);
                if(effective_stellar_type<HERTZSPRUNG_GAP)
                {
                    /*
                     * Zero-age values for CE mass and radius.
                     */
                    const double x = Max(0.0,Min(1.0,(0.1-logm)/0.55));
                    double u = Pow5(x);
                    menvz = 0.18*x + 0.82*u;
                    renvz = 0.4*Pow1d4(x) + 0.6*Pow2(u);
                    const double yy = 2.0 + 8.0*x;
                    /*
                     * Values for CE mass and radius at start of the HG.
                     */
                    tautms = Max(0.0,Min(1.0,(tebgb/(sqrt(sqrt(star->bse->luminosities[L_END_MS])/star->TAMS_radius))-A)*iw));
                    menvt = menvg*Pow5(tautms);
                    renvt = renvg*Pow5d4(tautms);

                    /*
                     * Modified expressions during MS evolution.
                     */
                    const double u2 = pow(ajtm,yy);
                    if(tautms>0.0)
                    {
                        fmenv = menvz + u2*(fmenv)*(1.0 - menvz/menvt);
                        frenv = renvz + u2*(frenv)*(1.0 - renvz/renvt);
                    }
                    else
                    {
                        fmenv = 0.0;
                        frenv = 0.0;
                    }
                    xx *= u;
                }//<HERTZSPRUNG_GAP

                star->moment_of_inertia_factor += xx;
                Dprint("Correction xxf = %g : star->moment_of_inertia_factor = %g\n",xx,star->moment_of_inertia_factor);
            }
            else
            {
                fmenv = 0.0;
                frenv = 0.0;
            }
        }
        else
        {
            /*
             * All other stars should be true giants.
             */
            fmenv = menvg;
            frenv = renvg;
            star->moment_of_inertia_factor = k2g;
            Dprint("Set k2 = k2g = %g\n",k2g);
        }

#ifdef PRE_MAIN_SEQUENCE
        if(PMS && Is_not_zero(compact_k2))
        {
            /*
             * Factor to smooth transition from the preMS
             * to the MS. This really needs to be properly
             * fitted...
             */
            const double tauPMS = 1e6 * star->age / preMS_lifetime(Max(1.0,Min(8.0,star->mass)));
            if(tauPMS < 1.0)
            {
                double f =
                    log10(Max(1.0,preMS_radius_factor(star->mass,1e6*star->age)));
                f = Max(0.0, Min(1.0, f));
                star->moment_of_inertia_factor = k2g * f + compact_k2 * (1.0 - f);
            }
        }
#endif

        star->menv = Max(fmenv * envelope_mass(star), MENV_MIN);
        star->renv = Max(frenv * (star->radius - star->core_radius) , RENV_MIN);

        if(star->moment_of_inertia_factor < 0.0)
        {
            /*
             * k2e should be positive : this is a serious error
             */
            Backtrace;
            Exit_binary_c(BINARY_C_OUT_OF_RANGE,
                          "star %d : k2e = %g <0 error at time %15.12e : INPUT : stellar_type=%d phase_start_mass=%g mass=%g core_mass[CORE_He]=%g (envelope mass %g, envelope radius %g) luminosity=%g radius=%g core_radius=%g age=%g tm=%g luminosities[L_END_MS]=%g luminosities[L_BGB]=%g luminosities[L_HE_IGNITION]=%g rzams=%g TAMS_radius=%g rg=%g menv=%g renv=%g k2e=%g k2g=%g\n",
                          star->starnum,
                          star->moment_of_inertia_factor,
                          stardata->model.time,
                          star->stellar_type,
                          star->phase_start_mass,
                          star->mass,
                          Outermost_core_mass(star),
                          envelope_mass(star),
                          star->radius - star->core_radius,
                          star->luminosity,
                          star->radius,
                          star->core_radius,
                          star->age,
                          star->tm,
                          star->bse->luminosities[L_END_MS],
                          star->bse->luminosities[L_BGB],
                          star->bse->luminosities[L_HE_IGNITION],
                          star->rzams,
                          star->TAMS_radius,
                          rg,
                          star->menv,
                          star->renv,
                          star->moment_of_inertia_factor,
                          k2g);
        }
    }
}

#endif//BSE
