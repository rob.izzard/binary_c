#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "stellar_structure_debug.h"

#ifdef NUCSYN
void stellar_structure_nucsyn(Stellar_type stellar_type,
                              Stellar_type stellar_typein,
                              struct star_t * const newstar,
                              struct stardata_t * const stardata)
{
    /*
     * A function to do some nucleosynthesis at the end of stellar_structure
     */
    Dprint("STELLAR_STRUCTURE nucleosynthesis : st in %d out %d\n",
           stellar_typein,
           stellar_type
        );
#ifdef MINT
    if(stardata->preferences->stellar_structure_algorithm == STELLAR_STRUCTURE_ALGORITHM_MINT) return;
#endif

    /*
     * If the star is not on the AGB set the spiky luminosity
     * to the actual luminosity (for logging purposes)
     */
    if(stellar_type!=TPAGB) newstar->spiky_luminosity = newstar->luminosity;

    /*
     * Check the new stellar type, if it's not the same as the stellar
     * type that was passed in, we might need to change the surface
     * abundance and the abundance of the accretion layer (which is mixed
     * in if the star is convective).
     *
     * We might also need to set the abundances on the first timestep,
     * or if the stellar age is zero (which implies it has recently formed
     * or merged or changed type).
     */
    if(stellar_type!=stellar_typein ||
       stardata->model.model_number==0 ||
       Is_zero(stardata->model.time) ||
       Is_zero(newstar->age))
    {
#ifdef NUCSYN_FIRST_DREDGE_UP_ACCRETION_CORRECTION_FROM_TAMS
        if(stellar_type==HG && stellar_typein<HG)
        {
            /* MS->HG conversion : copy the *envelope* abundances to XTAMS */
            Dprint("MEMCPY TAMS star %d (env C12=%g acc %g\n",newstar->starnum,
                   newstar->Xenv[XC12],newstar->Xacc[XC12]);
            /* save TAMS abundance for 1st DUP routine */
            Copy_abundances(newstar->Xenv,newstar->XTAMS);
        }
#endif // NUCSYN_FIRST_DREDGE_UP_ACCRETION_CORRECTION_FROM_TAMS

        if(stellar_type==EAGB && stellar_typein==TPAGB)
        {
            /* This should never happen! */
            Exit_binary_c(BINARY_C_ALGORITHM_BRANCH_FAILURE,
                          "AGB WENT THE WRONG WAY!\n");
        }

        if(stellar_type>=NS)
        {
            /* BH,NS or massless */
            nucsyn_set_remnant_abunds(&newstar->Xenv[0]);
        }
        else if(WHITE_DWARF(stellar_type))
        {
            /* all white dwarfs */
            nucsyn_set_WD_abunds(stardata,
                                 &newstar->Xenv[0],
                                 stellar_type);
        }
        else if(NAKED_HELIUM_STAR(stellar_type))
        {
            /* all helium stars */
            if(stellar_typein==HeMS &&
               stellar_type==HeHG)
            {
                Dprint("Convert HeMs -> HeHG (mix in dmacc)\n");

                /*
                 * Star has been converted from a radiative
                 * star into a convective giant star so
                 * mix the envelope (defined by the core mass)
                 * and the accretion layer, set
                 * the accretion layer size to zero
                 */
                if(can_use_strip_and_mix(stardata,newstar)==FALSE)
                {
                    if(newstar->dmacc>0.0)
                    {
                        nucsyn_mix_shells(newstar->mass - Outermost_core_mass(newstar),
                                          newstar->Xenv,
                                          newstar->dmacc,
                                          newstar->Xacc);
                        newstar->dmacc=0.0;
                    }
                }
            }
        }

        if(CONVECTIVE_ENVELOPE(stellar_type) &&
           newstar->dmacc > 0.0 &&
           can_use_strip_and_mix(stardata,newstar)==FALSE)
        {
            Dprint("CONVERT to convective star at t=%g kw=%d convective=%d dmacc=%g\n",
                   stardata->model.time,
                   newstar->stellar_type,
                   CONVECTIVE_ENVELOPE(newstar->stellar_type),
                   newstar->dmacc);
            /*
             * convective star : mix envelope and accretion layer
             */
            nucsyn_mix_shells(newstar->mass - Outermost_core_mass(newstar),
                              newstar->Xenv, // the envelope
                              newstar->dmacc,
                              newstar->Xacc);
            newstar->dmacc=0.0;
        }

        Dprint("Surface abunds now H1=%12.12e He4=%12.12e C12=%12.12e C13=%12.12e N14=%12.12e 1-Xsum = %g\n",
               newstar->Xenv[XH1],
               newstar->Xenv[XHe4],
               newstar->Xenv[XC12],
               newstar->Xenv[XC13],
               newstar->Xenv[XN14],
               1.0 - nucsyn_totalX(newstar->Xenv));
    }

#ifdef LITHIUM_TABLES
    if(NUCLEAR_BURNING(stellar_type))
    {
        /* prevent call in final thermal pulse */
        nucsyn_lithium(newstar,stardata);
    }
    else
    {
        newstar->Xenv[XLi7] = 0.0;
    }
    nucsyn_renormalize_to_max(newstar->Xenv);
#endif
    Dprint("post lithium tables now H1=%12.12e He4=%12.12e C12=%12.12e C13=%12.12e N14=%12.12e 1-Xsum = %g\n",
           newstar->Xenv[XH1],
           newstar->Xenv[XHe4],
           newstar->Xenv[XC12],
           newstar->Xenv[XC13],
           newstar->Xenv[XN14],
           1.0 - nucsyn_totalX(newstar->Xenv));

#ifdef NUCSYN_ANGELOU_LITHIUM
    nucsyn_angelou_lithium(stardata,star);
#endif // NUCSYN_ANGELOU_LITHIUM

    Dprint("post angelou lithium now H1=%12.12e He4=%12.12e C12=%12.12e C13=%12.12e N14=%12.12e 1-Xsum = %g\n",
           newstar->Xenv[XH1],
           newstar->Xenv[XHe4],
           newstar->Xenv[XC12],
           newstar->Xenv[XC13],
           newstar->Xenv[XN14],
           1.0 - nucsyn_totalX(newstar->Xenv));
#ifdef NUCSYN_STRIP_AND_MIX
    /*
     * First dredge up and/or envelope stripping
     * using TAMS abundances
     */
    nucsyn_strip_and_mix(stardata,star);
#endif // NUCSYN_STRIP_AND_MIX

    Dprint("post strip/mix now H1=%12.12e He4=%12.12e C12=%12.12e C13=%12.12e N14=%12.12e 1-Xsum = %g\n",
           newstar->Xenv[XH1],
           newstar->Xenv[XHe4],
           newstar->Xenv[XC12],
           newstar->Xenv[XC13],
           newstar->Xenv[XN14],
           1.0 - nucsyn_totalX(newstar->Xenv));

    /* enforce black hole/massless remnant accretion layer removal */
    if(stellar_type>=BH) newstar->dmacc=0.0;

    Dprint("pre-WR now H1=%12.12e He4=%12.12e C12=%12.12e C13=%12.12e N14=%12.12e 1-Xsum = %g\n",
           newstar->Xenv[XH1],
           newstar->Xenv[XHe4],
           newstar->Xenv[XC12],
           newstar->Xenv[XC13],
           newstar->Xenv[XN14],
           1.0 - nucsyn_totalX(newstar->Xenv));
#ifdef NUCSYN_WR
    if(stellar_type < HeWD &&
       stellar_type > LOW_MASS_MS &&
       stellar_type != TPAGB &&
       // applies to helium stars and stars with ZAMS mass > 8.0
       (stellar_type > TPAGB ||
        newstar->effective_zams_mass > 8.0))
    {
        nucsyn_WR(newstar,
                  stardata,newstar->mass);
    }
#endif // NUCSYN_WR
    Dprint("return now H1=%12.12e He4=%12.12e C12=%12.12e C13=%12.12e N14=%12.12e 1-Xsum = %g\n",
           newstar->Xenv[XH1],
           newstar->Xenv[XHe4],
           newstar->Xenv[XC12],
           newstar->Xenv[XC13],
           newstar->Xenv[XN14],
           1.0 - nucsyn_totalX(newstar->Xenv));
}
#endif // NUCSYN
