#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef BSE
#include "stellar_structure_debug.h"

void stellar_structure_WD(struct star_t * const newstar,
                          struct stardata_t * const stardata,
                          const Caller_id caller_id Maybe_unused)
{
    Dprint("White dwarf... (star %d type in %d) with core masses He=%g, CO=%g\n",
           newstar->starnum,
           newstar->stellar_type,
           newstar->core_mass[CORE_He],
           newstar->core_mass[CORE_CO]);

    /*
     * White dwarf structure
     */
    const Core_type core_type = ID_core(newstar->stellar_type);

    /*
     * We might need to set the core, although really
     * the calling function should do this.
     */
    if(Is_zero(newstar->core_mass[core_type]))
    {
        set_no_core(newstar);
        newstar->core_mass[core_type] = newstar->mass;
    }

    /*
     * But always limit cores to mass
     */
    trim_cores(newstar,newstar->mass);

    newstar->luminosity = lwd(stardata,
                              newstar,
                              newstar->mass,
                              newstar->age,
                              newstar->stellar_type,
                              TRUE);

    if(stardata->preferences->require_drdm == TRUE)
    {
        const double drdm_was = newstar->drdm; /* might need this */
        const double dm = newstar->mass * 1e-7;
        update_mass(stardata,newstar,dm);

        /*
         * Note we want the pure WD structure dR/dM, hence
         * thermal_transition = FALSE
         */
        const double rm = rwd(stardata,
                              newstar,
                              newstar->mass,
                              newstar->stellar_type,
                              FALSE);
        update_mass(stardata,newstar,-dm);
        newstar->radius = rwd(stardata,
                              newstar,
                              newstar->mass,
                              newstar->stellar_type,
                              FALSE);
        newstar->drdm = (rm - newstar->radius) / dm;

        /*
         * Final radius calculation requires the thermal
         * transition.
         */
        newstar->radius = rwd(stardata,
                              newstar,
                              newstar->mass,
                              newstar->stellar_type,TRUE);

#ifdef HeMS_HeWD_THERMAL_TRANSITION
        if(newstar->stellar_type == HeWD &&
           newstar->core_stellar_type == COWD)
        {
            const double t = Min(0.0,newstar->phase_end[HeMS].time - stardata->model.time);
            const double tkh = 1e-6 * kelvin_helmholtz_time_from_LMR(
                newstar->phase_end[HeMS].luminosity,
                newstar->phase_end[HeMS].mass,
                newstar->phase_end[HeMS].mass,
                newstar->phase_end[HeMS].radius);

            /* interpolate dRdM between the HeMS and HeWD */
            const double f = Min(t/tkh,1.0);
            const double drdm_HeMS = Is_zero(newstar->phase_end[HeMS].drdm) ? drdm_was : newstar->phase_end[HeMS].drdm;
            const double drdm =
                (1.0 - f) * drdm_HeMS +
                f * newstar->drdm;

            if(newstar->stellar_type == HeWD)
            {
                printf("Interpolate dRdM : HeMS = %g, HeWD = %g -> %g\n",
                       drdm_HeMS,
                       newstar->drdm,
                       drdm);
            }
            newstar->drdm = drdm;
        }
#endif
    }
    else
    {
        newstar->radius = rwd(stardata,
                              newstar,
                              newstar->mass,
                              newstar->stellar_type,
                              TRUE);
    }

    newstar->radius = Max(newstar->radius,
                          1e-3);

    if(newstar->mass < 5e-4) newstar->radius = (newstar->mass < 5e-6) ? 0.009 : 0.09;
    newstar->derivative[DERIVATIVE_STELLAR_GB_CORE_MASS] = 0.0;
    newstar->derivative[DERIVATIVE_STELLAR_He_CORE_MASS] = 0.0;
    newstar->derivative[DERIVATIVE_STELLAR_CO_CORE_MASS] = 0.0;
    newstar->derivative[DERIVATIVE_STELLAR_He_CORE_MASS_NO_TDUP] = 0.0;

#define MCh (stardata->preferences->chandrasekhar_mass)
    const Stellar_type companion_type = Other_star_struct(newstar)->stellar_type;

    /*
     * Check for supernova
     */
    if(More_or_equal(newstar->mass,MCh))
    {
        if(newstar->deny_SN==0)
        {
            /*
             * Accretion induced supernova with no remnant
             * unless WD is ONe in which case we assume a NS
             * is the remnant. Jarrod's 2007 code sets the
             * mass to 1.3Msun, but in this version we let the
             * mass be set by the ns_bh_mass function.
             */
            Dprint("above Chandrasekhar Mass, M = %g, Mch = %g\n",
                   newstar->mass,
                   MCh);

            Append_logstring(LOG_MCHAND,
                             "0WD MCh reached");

            struct star_t * news;

            if(newstar->stellar_type==ONeWD)
            {
                Dprint("accretion induced sn with core mass %g\n",newstar->mass);
                newstar->SN_type = SN_AIC;
                news = new_supernova(stardata,
                                     newstar,
                                     Other_star_struct(newstar),
                                     newstar);
                if(news)
                {
                    news->stellar_type = NEUTRON_STAR;
                    news->mass = ns_bh_mass(newstar->mass,
                                            newstar->mass,
                                            stardata,
                                            newstar,
                                            &news->baryonic_mass);
                }
            }
            else
            {
                Dprint("WD reaches the Chandrasekahar mass\n");

                /*
                 * When a (He/CO) WD reaches
                 * Mch by wind, blow it up in a Type Ia
                 * (JC: 13/12/2010)
                 *
                 * RGI: except when it is a hybrid He+COWD
                 *      merger. This should be a delayed
                 *      detonation of the helium during the merging
                 *      process;
                 */
                if(newstar->merge_state > 0 &&
                   newstar->hybrid_HeCOWD == TRUE)
                {
                    newstar->SN_type = SN_IA_Hybrid_HeCOWD;
                }
                else
                {
                    newstar->SN_type = SN_IA_CHAND;
                }
                news = new_supernova(stardata,
                                     newstar,
                                     Other_star_struct(newstar),
                                     newstar);


                if(news)
                {
                    stellar_structure_make_massless_remnant(stardata,
                                                            news);
                    Append_logstring(LOG_ROCHE,
                                     "SNIa [%d %d %p] M=%g>MCh=%g (%g %g) stellar type %d companion type %d",
                                     stardata->preferences->disable_events,
                                     stardata->preferences->handle_events,
                                     (void*)news,
                                     newstar->mass,
                                     stardata->preferences->chandrasekhar_mass,
                                     stardata->star[0].mass,
                                     stardata->star[1].mass,
                                     newstar->stellar_type,
                                     companion_type
                        );
                }
            }
        }
    }
    else
    {

        Dprint("below Chandrasekhar Mass M = %g, MCh = %g\n",
               newstar->mass,
               MCh);
        Dprint("r=%.12g\n",newstar->radius);

        /*
         * Check for Double Detonation SNIa
         * (in BSE, an edge-lit detonation) :
         *
         * This is a COWD or ONeWD that accretes some helium
         * and then blows up in a sub-Mch explosion.
         *
         * We require accretion at the appropriate rate for this,
         * or that the stars just merged.
         */
        const Boolean DDet =
            star_can_DDet(stardata,
                          newstar)
            &&
            (He_accretion_regime_WD(stardata,newstar) == DETONATION_He ||
             newstar->merge_state != STAR_NOT_MERGED);

#ifdef KEMP_SUPERNOVAE
        if(stardata->preferences->individual_novae == TRUE &&
           stardata->preferences->He_nova_DDets == TRUE)
        {
            /*
             * Alternative double detonation criterion
             * when we know the nova He-shell mass from
             * individual novae.
             */
            const double dMcrit = Hedet_dMHe(newstar);
            Dprint("star %d dmnovaHe %g dmcrit %g >crit? %d regime detotation He? %d st %d\n",
                   newstar->starnum,
                   newstar->dm_novaHe,
                   dMcrit,
                   newstar->dm_novaHe >= dMcrit,
                   newstar->WD_accretion_regime == DETONATION_He,
                   newstar->stellar_type);
            DDet =
                newstar->WD_accretion_regime == DETONATION_He
                &&
                (
                    newstar->stellar_type==COWD
                    ||
                    newstar->stellar_type==ONeWD
                    )
                &&
                newstar->dm_novaHe >= dMcrit;
        }
#endif // KEMP_SUPERNOVAE

        Dprint("new stellar type %d : mass %g cf. max %g\n",
               newstar->stellar_type,
               newstar->mass,
               stardata->preferences->max_HeWD_mass);

        if(newstar->deny_SN == 0 &&
           DDet == TRUE)
        {
            Dprint("COWD DDet");
            newstar->SN_type = newstar->stellar_type == COWD ? SN_IA_COWD_DDet : SN_IA_ONeWD_DDet;
            struct star_t * news = new_supernova(stardata,
                                                 newstar,
                                                 Other_star_struct(newstar),
                                                 newstar);

            if(news)
            {
                Append_logstring(LOG_ROCHE,
                                 "SNIa (Double detonation; DDet) M=%g; DM=%g > %g (companion type %d)",
                                 newstar->mass,
                                 newstar->core_mass[CORE_He] - newstar->core_mass[newstar->stellar_type == COWD ? CORE_CO : CORE_ONe],
                                 newstar->stellar_type == COWD ? stardata->preferences->mass_accretion_for_COWD_DDet : stardata->preferences->mass_accretion_for_ONeWD_DDet,
                                 companion_type);
                stellar_structure_make_massless_remnant(stardata,
                                                        news);
            }
        }
#ifdef ALLOW_HeWD_SUPERNOVAE
        else if(newstar->deny_SN == 0 &&
                newstar->stellar_type == HeWD &&
                companion_type <= HeWD &&
                More_or_equal(newstar->mass,
                              stardata->preferences->HeWD_HeWD_ignition_mass))
        {
            Dprint("HeWD SN");
            newstar->SN_type = SN_IA_He;
            struct star_t * news =
                new_supernova(stardata,
                              newstar,
                              Other_star_struct(newstar),
                              newstar);
            if(news)
            {
                Append_logstring(LOG_ROCHE,
                                 "SNI? (Helium) M=%g > %g (companion type %d)\n",
                                 newstar->mass,
                                 stardata->preferences->HeWD_HeWD_ignition_mass,
                                 companion_type);
                stellar_structure_make_massless_remnant(stardata,
                                                        news);
            }
        }
#endif // ALLOW_HeWD_SUPERNOVAE
        else if(newstar->stellar_type == HeWD &&
                newstar->mass > stardata->preferences->max_HeWD_mass &&
                Is_zero(newstar->age))
        {
            /*
             * Maximum mass for a young, hot HeWD :
             * ignite helium now
             */
            Dprint("mass exceeds max HeWD mass\n");
            newstar->stellar_type = HeMS;
            newstar->phase_start_mass = newstar->mass;
            set_no_core(newstar);
            call_stellar_timescales2(newstar->stellar_type,
                                     newstar->phase_start_mass,
                                     newstar->mass);
        }
    }

#if defined NUCSYN && defined NUCSYN_FIRST_DREDGE_UP_PHASE_IN
    newstar->mc_gb_was=0.0;
#endif//NUCSYN_FIRST_DREDGE_UP_PHASE_IN

    /*
     * WD atmosphere fallback: should have been set elsewhere!
     */
    if(newstar->white_dwarf_atmosphere_type == WHITE_DWARF_ATMOSPHERE_UNKNOWN)
    {
        newstar->white_dwarf_atmosphere_type = WHITE_DWARF_ATMOSPHERE_HYDROGEN;
    }

    /*
     * Disable any PN detection that might have previously happened
     */
    newstar->in_PN = FALSE;
}
#endif//BSE
