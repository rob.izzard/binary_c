#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef BSE
#include "stellar_structure_debug.h"
#define LOW -1
#define HIGH 1
#define p (newstar->bse->GB[GB_p])
#define q (newstar->bse->GB[GB_q])
#define B (newstar->bse->GB[GB_B])
#define D (newstar->bse->GB[GB_D])
#define A (newstar->bse->GB[GB_EFFECTIVE_AH])

Stellar_type stellar_structure_RG(struct star_t * Restrict const newstar,
                                  double * Restrict const rg,
                                  struct stardata_t * Restrict const stardata,
                                  const Caller_id caller_id Maybe_unused)
{
    /*
     * Hydrogen-enveloped Red Giant.
     */
    const double * const metallicity_parameters = stardata->common.metallicity_parameters;
    const double * const giant_branch_parameters = stardata->common.giant_branch_parameters;
    Stellar_type stellar_type = FIRST_GIANT_BRANCH;
    Stellar_type core_stellar_type = HELIUM_WHITE_DWARF;
    newstar->white_dwarf_atmosphere_type = WHITE_DWARF_ATMOSPHERE_HYDROGEN;

    /*
     * BSE works by using t as the proxy parameter
     * a) finding L(t+dt)
     * b) finding Mc(L)
     *
     * We want to use Mc as the parmeter instead.
     * a) find L(Mc)
     * b) hence dMc/dt and the new Mc(t+dt)
     *
     * Equation numbers are from Hurley+2000
     */

    newstar->luminosity = lgbtf(newstar->age,
                                newstar->bse->GB[GB_EFFECTIVE_AH],
                                newstar->bse->GB,
                                newstar->bse->timescales[T_GIANT_TINF_1],
                                newstar->bse->timescales[T_GIANT_TINF_2],
                                newstar->bse->timescales[T_GIANT_TX]);
    Dprint("L(GB,age=%g) = %g\n",
           newstar->age,
           newstar->luminosity);

    /*
     * Account for growth at the end of the HG.
     * This is often missed because of finite time resolution,
     * and while it is a small effect it can be annoying.
     */
    const double mcwas = newstar->core_mass[CORE_He];
    set_no_core(newstar);
    newstar->core_mass[CORE_He] = Max(mcbgb(newstar,stardata),
                                      mcwas);
    Dprint("T model %d MC BSE %g (mcwas %g, stwas %d, prev %d)\n",
           stardata->model.model_number,
           newstar->core_mass[CORE_He],
           mcwas,
           newstar->stellar_type,
           stardata->previous_stardata != NULL ? stardata->previous_stardata->star[newstar->starnum].stellar_type : -1);

    const Boolean degenerate_core = Less_or_equal(newstar->phase_start_mass,
                                                  metallicity_parameters[ZPAR_MASS_HE_FLASH]);


    /*
     * BSE algorithm for Mc(L(t))
     */
    if(degenerate_core)
    {
        /*
         * Star has a degenerate He core which grows on the GB
         */
        Dprint("Degenerate He Core with L=%g and core_mass[CORE_He] was %g\n",
               newstar->luminosity,
               newstar->core_mass[CORE_He]);
        newstar->core_mass[CORE_He] = mcgbf(newstar->luminosity,
                                            newstar->bse->GB,
                                            newstar->bse->luminosities[L_LMX]);
        Dprint("Hence core mass %g\n",newstar->core_mass[CORE_He]);
    }
    else
    {
        /*
         * Star has a non-degenerate He core which may grow, but
         * only slightly, on the GB
         */
        Dprint("Non-Degenerate He Core\n");
        const double tbranch = newstar->bse->timescales[T_HE_IGNITION] - newstar->bse->timescales[T_BGB];
        const double tau = tbranch>TINY ? (newstar->age - newstar->bse->timescales[T_BGB])/tbranch : 1.0;
        const double mcx = mcheif(newstar->phase_start_mass,
                                  metallicity_parameters[ZPAR_MASS_HE_FLASH],
                                  metallicity_parameters[9],
                                  giant_branch_parameters);
        const double mcy = mcheif(newstar->phase_start_mass,
                                  metallicity_parameters[ZPAR_MASS_HE_FLASH],
                                  metallicity_parameters[10],
                                  giant_branch_parameters);
        newstar->core_mass[CORE_He] = mcx + (mcy - mcx)*tau;
        Dprint("core_mass[CORE_He] = %g = %g+(%g-%g)*%g\n",newstar->core_mass[CORE_He],mcx,mcy,mcx,tau);
        Nancheck(newstar->core_mass[CORE_He]);
    }


    const double accretion_rate = Mdot_net(newstar);
    Boolean rate_limited;
    if(System_is_binary &&
       Is_not_zero(mcwas) &&
       accretion_rate > REALLY_TINY &&
       newstar->mass - newstar->core_mass[CORE_He] < ACCRETION_LIMIT_H_ENV_GB)
    {
        /*
         * If we're an accreting RGB star,
         * with a thin envelope,
         * don't grow the core faster than
         * the accretion rate, otherwise we'll
         * eat through the envelope to immediately
         * return to being a HeMS star or WD.
         */
        Clamp(newstar->core_mass[CORE_He],
              mcwas,
              stardata->model.dt * accretion_rate + mcwas);
        rate_limited = TRUE;
    }
    else
    {
        rate_limited = FALSE;
    }

    Dprint("setting r from rgbf to ");
    if(stardata->preferences->require_drdm == TRUE)
    {
        const double dm = 1e-7 * newstar->mass;
        update_mass(stardata,newstar,dm);
        const double rm = rgbf(newstar->mass,
                               newstar->luminosity,
                               giant_branch_parameters);
        update_mass(stardata,newstar,-dm);
        newstar->radius = rgbf(newstar->mass,
                               newstar->luminosity,
                               giant_branch_parameters);
        newstar->drdm = (rm - newstar->radius) / dm;
    }
    else
    {
        newstar->radius = rgbf(newstar->mass,
                               newstar->luminosity,
                               giant_branch_parameters);
    }

    Dprint("%12.12e\n",newstar->radius);

    *rg = newstar->radius;

    /*
     * Check if the star contains only the helium core
     */
    if(More_or_equal(newstar->core_mass[CORE_He],
                     newstar->mass))
    {
        /*
         * Not clear what we should use for age.
         * For a WD, 0 is probably fine, but this
         * might not be true for a HeMS star that
         * has already burned some of its helium...
         */
        newstar->age = 0.0;

        const Boolean ignite =
            Fequal(
                stardata->preferences->minimum_helium_ignition_core_mass,
                HELIUM_IGNITION_ALGORITHM_BSE
                ) ?
            /* use BSE algorithm based on the phase start mass */
            newstar->phase_start_mass > metallicity_parameters[ZPAR_MASS_HE_FLASH]
            :
            /* use helium core mass algorithm */
            newstar->core_mass[CORE_He] > min_He_ignition_core_mass(stardata,newstar);

        Dprint("CF%d phase start mass %g to zpar %g\n",
               newstar->starnum,
               newstar->phase_start_mass,
               metallicity_parameters[ZPAR_MASS_HE_FLASH]);

        Dprint("CF Mc = %g to He ig Mc %g -> ignite? %s\n",
               newstar->core_mass[CORE_He],
               min_He_ignition_core_mass(stardata,newstar),
               Yesno(ignite));

        if(rate_limited == FALSE)
        {
            if(ignite == TRUE)
            {
                /*
                 * Zero-age helium star
                 */
                Dprint("Zero-age helium star\n");
                newstar->core_mass[CORE_He]=0.0;
                newstar->phase_start_mass = newstar->mass;
                stellar_type = HeMS;
                core_stellar_type = MASSLESS_REMNANT;
            }
            else
            {
                /*
                 * Zero-age helium white dwarf.
                 */
                Dprint("Zero-age helium white dwarf\n");
                newstar->core_mass[CORE_He] = newstar->mass;
                newstar->phase_start_mass = newstar->mass;
                stellar_type = HeWD;
                core_stellar_type = MASSLESS_REMNANT;
            }
        }
    }
    else
    {
        /* star is a normal red giant : we can do first DUP etc. */
#if defined NUCSYN && defined NUCSYN_FIRST_DREDGE_UP
        if(!RLOF_CALL)
        {

            Dprint("1DUP star %d check mc=%12.12e vs mc1dup(evert)=%12.12e\n",
                   newstar->starnum,
                   newstar->core_mass[CORE_He],
                   mc_1DUP(stardata,newstar->phase_start_mass,stardata->common.metallicity));
#ifdef  NUCSYN_FIRST_DREDGE_UP_PHASE_IN

            /* 1st DUP is a process */
            if(newstar->first_dredge_up==FALSE &&
               newstar->stellar_type==FIRST_GIANT_BRANCH &&
               newstar->effective_zams_mass < NUCSYN_WR_MASS_BREAK)
            {
                nucsyn_set_1st_dup_abunds(newstar->Xenv,
                                          newstar->mass,
                                          FIRST_GIANT_BRANCH,
                                          newstar,
                                          stardata,
                                          FALSE);
            }

#else
            /* 1st DUP is an event */
            if(newstar->first_dredge_up==FALSE
               && stellar_type==FIRST_GIANT_BRANCH
               && newstar->effective_zams_mass < NUCSYN_WR_MASS_BREAK
               && newstar->core_mass[CORE_He] > mc_1DUP(newstar->phase_start_mass,
                                                        stardata->common.metallicity))
            {
                nucsyn_set_1st_dup_abunds(newstar->Xenv,
                                          newstar->mass,
                                          stardata->common.metallicity,
                                          FIRST_GIANT_BRANCH,
                                          newstar,
                                          stardata,
                                          TRUE);
                newstar->first_dredge_up = TRUE;
            }
#endif // NUCSYN_FIRST_DREDGE_UP_PHASE_IN
        }
#endif /* NUCSYN_FIRST_DREDGE_UP && NUCSYN */
    }

#ifdef NUCSYN
    if(!RLOF_CALL)
    {
        /*
         * Save the core mass for the first dredge up routine
         */
        if(newstar->mc_bgb<TINY) newstar->mc_bgb = newstar->core_mass[CORE_He];
    }
#endif

    newstar->stellar_type = stellar_type;
    newstar->core_stellar_type = core_stellar_type;

    if(newstar->stellar_type==HeMS)
    {
        call_stellar_timescales2(newstar->stellar_type,
                                 newstar->phase_start_mass,
                                 newstar->mass);
    }
    Dprint("end of RG stellar structure: stellar type now %d\n",
           newstar->stellar_type);

    return stellar_type;
}
#endif//BSE
