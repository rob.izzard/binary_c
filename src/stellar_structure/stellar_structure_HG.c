#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef BSE

#include "stellar_structure_debug.h"
static double BSE_HG_radius(struct stardata_t * const stardata,
                            struct star_t * const star,
                            const double tau);

/*
 * Evolve HG stars according to the BSE algorithm
 */

Stellar_type stellar_structure_HG(struct star_t * Restrict const newstar,
                                  struct stardata_t * Restrict const stardata,
                                  const Caller_id caller_id Maybe_unused)
{
    const double * metallicity_parameters=stardata->common.metallicity_parameters;
    double rho,tau;
    const double thg = newstar->bse->timescales[T_BGB] - newstar->tm;
    double mcbgbf;

#ifdef MINT
    if(stardata->preferences->stellar_structure_algorithm ==
       STELLAR_STRUCTURE_ALGORITHM_MINT)
    {
        /*
         * Interface from MINT's end of MS to BSE:
         * use MINT's shellular nuclear burning if
         * it is on
         */
        newstar->derivative[DERIVATIVE_STELLAR_CENTRAL_HYDROGEN]=0.0;
        newstar->derivative[DERIVATIVE_STELLAR_CENTRAL_HELIUM]=0.0;

#ifdef NUCSYN
        mcbgbf =
            (MINT_has_shells(newstar)
             &&
             stardata->preferences->MINT_nuclear_burning == TRUE)
            ? MINT_core(newstar,XHe4,0.9)
            : mcbgb(newstar,stardata);
#else
        mcbgbf = mcbgb(newstar,stardata);
#endif // NUCYSN
        Dprint("MINT He core %g vs BSE %g\n",mcbgbf,mcbgb(newstar,stardata));
    }
    else
#endif // MINT
    {
        mcbgbf = mcbgb(newstar,stardata);
    }


    Dprint("Hertzsprung Gap Star\n");

    /*
     * Use TAMS core mass if we know it,
     * otherwise Hurley+2000 Eq.30
     */
    rho = mctmsf(newstar->phase_start_mass);

    Dprint("age = %g, tm = %g, thg = %g, rho = %g, mcbgbf = %g\n",
           newstar->age,
           newstar->tm,
           thg,
           rho,
           mcbgbf);

    tau = (newstar->age - newstar->tm) / thg;

    Clamp(tau,0.0,1.0);

    set_no_core(newstar);
    newstar->core_mass[CORE_He] = mcbgbf * ((1.0-tau)*rho+tau);

#ifdef NUCSYN
    if(!RLOF_CALL)
    {
#ifdef NUCSYN_FIRST_DREDGE_UP_PHASE_IN
        newstar->mc_gb_was=0.0;
#endif//NUCSYN_FIRST_DREDGE_UP_PHASE_IN
    }
#endif//NUCSYN

    /*
     * Test whether core mass has reached total mass.
     */
    Dprint("coremass = %12.12g >=? mt= %12.12g; m0=%g zpar12=%g\n",
           newstar->core_mass[CORE_He],
           newstar->mass,
           newstar->phase_start_mass,
           metallicity_parameters[ZPAR_MASS_HE_FLASH]);

    if(unlikely(More_or_equal(newstar->core_mass[CORE_He],newstar->mass)))
    {
        newstar->age = 0.0;
        if(newstar->phase_start_mass > metallicity_parameters[ZPAR_MASS_HE_FLASH])
        {
            /*
             * Zero-age helium star
             */
            Dprint("Zero-age helium star\n");

            newstar->core_mass[CORE_He] = 0.0;
            newstar->phase_start_mass = newstar->mass;
            newstar->stellar_type = HeMS;
            newstar->core_stellar_type = MASSLESS_REMNANT;
            call_stellar_timescales2(newstar->stellar_type,
                          newstar->phase_start_mass,
                          newstar->mass);

            Dprint("\ntm is now %12.12g\n",newstar->tm);
        }
        else
        {
            /*
             * Zero-age helium white dwarf.
             */
            Dprint("Zero-age helium white dwarf\n");
            set_no_core(newstar);
            newstar->core_mass[CORE_He] = newstar->phase_start_mass = newstar->mass;
            newstar->stellar_type = HeWD;
            newstar->core_stellar_type = HeWD;
        }
    }
    else
    {
        newstar->luminosity = newstar->TAMS_luminosity * //newstar->bse->luminosities[L_END_MS]*
            pow(newstar->bse->luminosities[L_BGB]/newstar->bse->luminosities[L_END_MS], tau);

        if(stardata->preferences->require_drdm == TRUE)
        {
            const double dm = 1e-7 * newstar->mass;
            update_mass(stardata,newstar,dm);
            const double rm = BSE_HG_radius(stardata,newstar,tau);
            update_mass(stardata,newstar,-dm);
            newstar->radius = BSE_HG_radius(stardata,newstar,tau);
            newstar->drdm = (rm - newstar->radius)/dm;
        }
        else
        {
            newstar->radius = BSE_HG_radius(stardata,newstar,tau);
        }

#ifdef NUCSYN
        if(!RLOF_CALL)
        {
            /* save mass at the start of the HG */
            if(newstar->start_HG_mass < 0) newstar->start_HG_mass=newstar->mass;
        }
#endif

        newstar->stellar_type = HERTZSPRUNG_GAP;
        newstar->core_stellar_type = HeWD;

        Dprint("HG : l=%g r=%g (TAMS_radius=%g tau=%g)\n",newstar->luminosity,newstar->radius,newstar->TAMS_radius,tau);
    }
    newstar->white_dwarf_atmosphere_type =
        WHITE_DWARF_ATMOSPHERE_HYDROGEN;

    return newstar->stellar_type;
}

static double BSE_HG_radius(struct stardata_t * const stardata,
                            struct star_t * const newstar,
                            const double tau)
{
    const double rg = rgbf(newstar->mass,
                           newstar->bse->luminosities[L_BGB],
                           stardata->common.giant_branch_parameters);
    double rx;
    if(Less_or_equal(newstar->phase_start_mass,
                     stardata->common.metallicity_parameters[ZPAR_MASS_FGB]))
    {
        rx = rg;
    }
    else
    {
        /*
         * He-ignition and end of HG occur at Rmin
         */
        const double rmin = rminf(newstar->phase_start_mass,
                                  stardata->common.giant_branch_parameters);
        const double ry = ragbf(newstar->mass,
                                newstar->bse->luminosities[L_HE_IGNITION],
                                stardata->common.metallicity_parameters[ZPAR_MASS_HE_FLASH],
                                stardata->common.giant_branch_parameters
#ifdef AXEL_RGBF_FIX
                                ,stardata->common.metallicity
#endif
            );
        rx = Min(rmin,ry);
        if(Less_or_equal(newstar->phase_start_mass,MLP))
        {
            const double texp = log(newstar->phase_start_mass/MLP)/
                log(stardata->common.metallicity_parameters[ZPAR_MASS_FGB]/MLP);
            rx = rmin*pow(rg/rmin,texp);
        }
        const double tau2 = tblf(newstar->phase_start_mass,
                                 stardata->common.metallicity_parameters[ZPAR_MASS_HE_FLASH],
                                 stardata->common.metallicity_parameters[ZPAR_MASS_FGB],
                                 stardata->common.giant_branch_parameters,
                                 stardata->common.metallicity);
        if(tau2<TINY) rx = ry;

        Dprint("He-ignition at end of HG : tau2=%g\n",tau2);
    }
    return newstar->TAMS_radius * pow(rx/newstar->TAMS_radius,tau);
}

#endif//BSE
