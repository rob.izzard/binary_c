/*
 * Calculate the structure variables for a star, based on its
 * current evolutionary state and the (calculated elsewhere) timescales.
 *
 * Based on the original BSE hrdiag.f and binary_c V1's hrdiag.c:
 *
 *       Computes the new mass, luminosity, radius & stellar type.
 *       Input (MASS, AJ, TM, TN, LUMS & TSCLS) supplied by routine STAR.
 *       Ref: P.P. Eggleton, M.J. Fitchett & C.A. Tout (1989) Ap.J. 347, 998.
 *
 *       Revised 27th March 1995 by C. A. Tout;
 *       24th October 1995 to include metallicity;
 *       14th November 1996 to include naked helium stars;
 *       28th February 1997 to allow accretion induced supernovae.
 *
 *       Revised 5th April 1997 by J. R. Hurley
 *       to include Z=0.001 as well as Z=0.02, convective overshooting,
 *       MS hook and more elaborate CHeB
 *
 *       Updated to C version, 2001, Rob Izzard.
 *       2001-2003 added nucleosynthesis.
 *
 *       2008 Added 'caller_id' support to enable certain
 *       parts of the code depending on the calling function's
 *       requirements. Most useful is if RLOF_CALL is TRUE
 *       then much of the nucleosynthesis is not required.
 *
 *       2017 replaced many input parameters with star_t structures.
 *       "oldstar" is the star as it was, "newstar" is the star
 *       as it will be after the update.
 */
#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef BSE
#include "stellar_structure_debug.h"
#define NEGATIVE_AGE_CHECK_MAGNITUDE 1e-10

int stellar_structure_BSE_given_timescales(struct star_t *newstar,
                                           struct star_t *oldstar,
                                           struct stardata_t *stardata,
                                           const Caller_id caller_id)
{
    double remnant_luminosity=0.0,remnant_radius=0.0,mcx=0.0;
    double mtc=0.0,rg=0.0;

    /* time at which we hit the base of the AGB */
    const double tbagb = newstar->bse->timescales[T_HE_IGNITION] +
        newstar->bse->timescales[T_HE_BURNING];

    /*
     * force age to be >= first pulse if already TPAGB
     * or progeny (WD)
     */
    if(newstar->stellar_type == TPAGB ||
       newstar->stellar_type >= HeWD)
    {
        newstar->age = Max(newstar->age,
                           newstar->bse->timescales[T_TPAGB_FIRST_PULSE]);
    }

#ifdef MINIMUM_STELLAR_MASS
    double restore_m=0.0,restore_mt=0.0;
    Boolean restore_mass=FALSE;
#endif//MINIMUM_STELLAR_MASS

    /************************************************************/
    Dprint("STELLAR_STRUCTURE called by %s (%d) in star %d (RLOF call? %d) ",
           Stellar_structure_caller_string(caller_id),
           caller_id,
           newstar->starnum,
           RLOF_CALL);
#ifdef DEBUG
    newstar->moment_of_inertia_factor = 0.0;
#endif // DEBUG
    Dprint("phase_start_mass=%g age=%g mt=%g mc=%g st=%d mc=%g k2=%g star->phase_start_mass=%g ntp=%g angular_momentum=%g\n",
           newstar->phase_start_mass,
           newstar->age,
           newstar->mass,
           Outermost_core_mass(newstar),
           newstar->stellar_type,
           Outermost_core_mass(newstar),
           newstar->moment_of_inertia_factor,
           oldstar->phase_start_mass,
           newstar->num_thermal_pulses,
           newstar->angular_momentum);
    if(newstar->age < -NEGATIVE_AGE_CHECK_MAGNITUDE)
    {
        printf("warning : Negative age (=%g) at stellar_structure entrance for star %d (M=%g M0=%g Mc=%g stellar_type=%d)\n",
               newstar->age,
               newstar->starnum,
               newstar->mass,
               newstar->phase_start_mass,
               Outermost_core_mass(newstar),
               newstar->stellar_type
            );
        Exit_binary_c(BINARY_C_OUT_OF_RANGE,
                      "Negative age = %g at stellar_structure entrance!\n",
                      newstar->age);
        newstar->age = Max(0.0,newstar->age); // shouldn't really happen!
    }


#ifdef MINIMUM_STELLAR_MASS
    if(newstar->phase_start_mass < MINIMUM_STELLAR_MASS)
    {
        restore_mass = TRUE;
        restore_m = newstar->phase_start_mass;
        restore_mt = newstar->mass;
    }
    newstar->phase_start_mass = Max(MINIMUM_STELLAR_MASS,
                                    newstar->phase_start_mass);
    newstar->mass = Max(MINIMUM_STELLAR_MASS,newstar->mass);
#endif // MINIMUM_STELLAR_MASS

    /*
     * Check for evaporated star
     */
    if(Is_zero(newstar->mass))
    {
        newstar->stellar_type = MASSLESS_REMNANT;
    }

#ifdef NUCSYN
    /*
     * Convert AGB stars which HBB their envelopes to 99% helium into
     * HeHG stars. Core mass remains the same (the CO core)
     */
    if(newstar->dont_change_stellar_type==FALSE &&
       AGB(newstar->stellar_type) &&
       newstar->Xenv[XH1]<0.01 &&
       newstar->Xenv[XHe4]>0.9)
    {
        Dprint("Convert AGB star to He star because X=%12.12e Y=%12.12e stellar type =%d\n",
               newstar->Xenv[XH1],
               newstar->Xenv[XHe4],
               newstar->stellar_type);

        newstar->stellar_type=HeGB;
        call_stellar_timescales2(newstar->stellar_type,
                                 newstar->phase_start_mass,
                                 newstar->mass);
        if(Less_or_equal(newstar->core_mass[CORE_He],
                         newstar->bse->GB[GB_Mx]))
        {
            newstar->age = newstar->bse->timescales[T_GIANT_TINF_1] - (1.0/((newstar->bse->GB[GB_p]-1.0)*newstar->bse->GB[GB_A_HE]*newstar->bse->GB[GB_D]))*
                pow(newstar->core_mass[CORE_He],1.0-newstar->bse->GB[GB_p]);
        }
        else
        {
            newstar->age = newstar->bse->timescales[T_GIANT_TINF_2] - (1.0/((newstar->bse->GB[GB_q]-1.0)*newstar->bse->GB[GB_A_HE]*newstar->bse->GB[GB_B]))*
                pow(newstar->core_mass[CORE_He],1.0-newstar->bse->GB[GB_q]);
        }
        Dprint("Want age = Max(age=%g, tm=%g)\n",
               newstar->age,
               newstar->tm);
        /* force age to be just after the HeMS */
        newstar->age = Max(newstar->age,newstar->tm)+TINY;
        newstar->epoch = stardata->model.time - newstar->age;
        // just in case, reset AGB parameters
        newstar->num_thermal_pulses_since_mcmin=0.0;
        newstar->num_thermal_pulses=-1.0;
    }
#endif // NUCSYN

    /************************************************************/
    if(newstar->stellar_type < HeMS)
    {
        /*
         * Make evolutionary changes to stars that have not evolved beyond
         * the AGB
         */
        newstar->rzams = rzamsf(newstar->phase_start_mass,
                                stardata->common.main_sequence_parameters);

        if(stardata->preferences->stellar_structure_algorithm ==
           STELLAR_STRUCTURE_ALGORITHM_MODIFIED_BSE)
        {
            newstar->TAMS_radius  = rtmsf(newstar->phase_start_mass,
                                          stardata->common.main_sequence_parameters);
            Dprint("rzams = %g from M=%g MSP=%p\n",
                   newstar->rzams,
                   newstar->phase_start_mass,
                   (void*)stardata->common.main_sequence_parameters);
        }

        Dprint("pre-He star AJ =%12.12e c.f. T_BGB timescale %12.12e rzams=%g TAMS_radius=%g\n",
               newstar->age,
               newstar->bse->timescales[T_BGB],
               newstar->rzams,
               newstar->TAMS_radius);

        if(DEBUG)
        {
            char * cores = core_string(newstar,FALSE);
            Dprint("STELLAR_STRUCTURE star %d type %d age=%g mass=%g %s cf timescales MS=%g BGB=%g He-ignition=%g tbagb=%g (tin-f1=%g tin-f2=%g)\n",
                   newstar->starnum,
                   newstar->stellar_type,
                   newstar->age,
                   newstar->mass,
                   cores,
                   newstar->tm,
                   newstar->bse->timescales[T_BGB],
                   newstar->bse->timescales[T_HE_IGNITION],
                   tbagb,
                   newstar->bse->timescales[T_GIANT_TINF_1],
                   newstar->bse->timescales[T_GIANT_TINF_2]
                );
            Safe_free(cores);
        }

        /* do not allow CHeB stars to reverse their evolution */

        if(
            (newstar->age < newstar->bse->timescales[T_BGB] &&
             (newstar->stellar_type<CHeB ||
              (newstar->stellar_type==CHeB && stardata->model.intpol>0)))
            )
        {
            /* Either on MS or HG */
            Dprint("Pre-RGB : MS or HG\n");

#ifdef NUCSYN
#ifdef ADAPTIVE_RLOF
            if(!RLOF_CALL)
#endif // ADAPTIVE_RLOF
                /* guess the eventual start of HG mass */
                newstar->start_HG_mass =
                    Is_zero(newstar->age) ? (newstar->mass)
                    :
                    ((newstar->mass - newstar->effective_zams_mass)*(newstar->bse->timescales[T_BGB]/(newstar->age)) + newstar->effective_zams_mass);
#endif // NUCSYN

            /* save RGB radius */
            rg = rgbf(newstar->mass,
                      newstar->bse->luminosities[L_BGB],
                      stardata->common.giant_branch_parameters);

#ifdef MINT
            if(stardata->preferences->stellar_structure_algorithm ==
               STELLAR_STRUCTURE_ALGORITHM_MINT)
            {
                /*
                 * If we're coming from MINT, we just want to
                 * move to the HG
                 */
                stellar_structure_HG(newstar,stardata,caller_id);
            }
            else
#endif //MINT
            {

                /*
                 * We compare age and main sequence lifetime:
                 * note that we cannot just compare them because they
                 * are often large numbers, for which the error > TINY.
                 * Instead, use Signed_diff.
                 */
                if(Signed_diff(newstar->age, newstar->tm) < TINY)
                {
                    /************************/
                    /** Main sequence star **/
                    /************************/
                    Dprint("Call stellar_structure_MS\n");
                    stellar_structure_MS_BSE(newstar,stardata);
                }
                else
                {
                    /************************************/
                    /** Star is on the Hertzsprung Gap **/
                    /************************************/
                    Dprint("Call stellar_structure_HG age = %g\n",
                           newstar->age);
                    stellar_structure_HG(newstar,stardata,caller_id);
                }
            }
        }

        /*
         * Now the GB, CHeB and AGB evolution.
         *
         * The Signed_diff here must be <-TINY to prevent the
         * star having a timestep of length zero on the RGB : this
         * leads to jumps in stellar radius in massive stars which are
         * not realistic because such stars should not have a hydrogen
         * shell burning phase without central carbon ignition, so should be
         * type CHeB
         */

        else if(Signed_diff(newstar->age,
                            newstar->bse->timescales[T_HE_IGNITION]) < -TINY)
        {
            /*
             * Red Giant.
             */
            Dprint("Call stellar_structure_RG with age=%g mc=%g\n",
                   newstar->age,
                   Outermost_core_mass(newstar));
            stellar_structure_RG(newstar,
                                 &rg,
                                 stardata,
                                 caller_id);
        }
        else if(Signed_diff(newstar->age, tbagb)<TINY &&
                newstar->core_mass[CORE_CO] < TINY)
        {
            /*
             *  Core helium burning star.
             */
            Dprint("Call stellar_structure_CHeB with age = %g < tbagb = %g\n",
                   newstar->age,
                   tbagb);
            stellar_structure_CHeB(oldstar,
                                   newstar,
                                   &rg,
                                   stardata,
                                   caller_id);
        }
        else
        {
            /*
             * Asymptotic Red Giant.
             */
            if(newstar->TZO == TRUE)
            {
                Dprint("Call stellar_structure_TZO age = %g > tbagb = %g\n",
                       newstar->age,
                       tbagb);
                stellar_structure_TZO(newstar,
                                      stardata,
                                      caller_id);
                rg = newstar->radius;
            }
            else
            {
                Dprint("Call stellar_structure_AGB age = %g > tbagb = %g\n",
                       newstar->age,
                       tbagb);
                stellar_structure_AGB(oldstar,
                                      newstar,
                                      &rg,
                                      tbagb,
                                      stardata,
                                      caller_id);
            }
            mcx = newstar->core_mass[CORE_CO];
        }
    }

#ifdef NUCSYN
    if(newstar->stellar_type != TPAGB) newstar->mc_1tp=0.0;
#endif

    /*
     * Do not allow the core to go from ONe to CO, or CO to He
     */
    if(newstar->stellar_type>MAIN_SEQUENCE)
    {
        newstar->compact_core_type = Max(newstar->stellar_type,
                                         newstar->compact_core_type);
        if(newstar->stellar_type < newstar->compact_core_type)
        {
            if(WHITE_DWARF(newstar->stellar_type))
            {
                newstar->stellar_type = newstar->compact_core_type;
            }
        }
    }

    if(NAKED_HELIUM_STAR(newstar->stellar_type))
    {
        /*
         * Naked Helium Star
         */
        newstar->rzams = rzhef(newstar->mass);
        remnant_radius = newstar->rzams;
        Dprint("Call stellar_structure_HeStar\n");

        if(newstar->TZO == TRUE)
        {
            stellar_structure_TZO(newstar,
                                  stardata,
                                  caller_id);
        }
        else
        {
            stellar_structure_HeStar(newstar,
                                     &mtc,
                                     &rg,
                                     stardata,
                                     caller_id);
        }
        Dprint("stellar type now %d\n",newstar->stellar_type);
    }

    /* White dwarf. */
    if(WHITE_DWARF(newstar->stellar_type))
    {
        Dprint("Call stellar_structure_WD\n");
        stellar_structure_WD(newstar,
                             stardata,
                             caller_id);
    }
    else
    {
        /* cannot be a hybrid HeCOWD */
        newstar->hybrid_HeCOWD = FALSE;
    }

    /* Neutron Star. */
    if(newstar->stellar_type==NEUTRON_STAR)
    {
        Dprint("Call stellar_structure_NS\n");
        stellar_structure_NS(newstar,stardata);
    }

    /* Black hole */
    if(newstar->stellar_type==BLACK_HOLE)
    {
        Dprint("Call stellar_structure_BH\n");
        stellar_structure_BH(stardata,newstar);
    }

    /*
     * Sanity checks
     */
    stellar_structure_sanity_checks(stardata,
                                    newstar);

    /*
     * update phase_start_core_mass
     * if stellar type has change
     */
    if(newstar->stellar_type != oldstar->stellar_type)
    {
        newstar->phase_start_core_mass = Outermost_core_mass(newstar);
    }

    if(newstar->stellar_type <  HeWD &&
       newstar->TZO == FALSE)
    {
        /*
         * Calculate the core radius and the luminosity and radius of the
         * remnant that the star will become and perturb L,R if the
         * envelope mass is small.
         */
        Dprint("pre-perturb tm=%30.22e r=%g\n",
               newstar->tm,
               newstar->radius);
        stellar_structure_remnant_and_perturbations(newstar,
                                                    remnant_luminosity,
                                                    remnant_radius,
                                                    tbagb,
                                                    mtc,
                                                    mcx,
                                                    stardata);
        Dprint("post-perturb tm=%30.22e r=%g\n",
               newstar->tm,
               newstar->radius);
    }
    else if(newstar->stellar_type==MASSLESS_REMNANT)
    {
        /*
         * Massless remnant
         */
        stellar_structure_make_massless_remnant(stardata,
                                                newstar);
    }

#ifdef ADAPTIVE_RLOF
    /*
     * Other data are not required for the RLOF mass transfer rate
     * calculations
     */
    if(!RLOF_CALL)
    {
#endif // ADAPTIVE_RLOF
        /*
         * Calculate mass and radius of convective envelope, and envelope
         * gyration radius.
         */
        if(newstar->stellar_type<=HeGB)
        {
#ifdef DEBUG
            Dprint("<=HeGB convective_envelope_mass_and_radius call\n");
#endif
            /*
             * Test for pathological case where giant envelope
             * radius has not been previously calculated!
             */
            if(Is_zero(rg))
            {
                rg = rgbf(newstar->mass,
                          newstar->bse->luminosities[L_BGB],
                          stardata->common.giant_branch_parameters);
                Dprint("Pathological case: radius not set, compute rg = %g\n",
                       rg);
            }

            Dprint("convective_envelope_mass_and_radius pre k2=%g\n",
                   newstar->moment_of_inertia_factor);
            convective_envelope_mass_and_radius(stardata,
                                                newstar,
                                                rg,
                                                stardata->common.metallicity);

            Dprint("convective_envelope_mass_and_radius post k2=%g\n",newstar->moment_of_inertia_factor);
        }
        else
        {
            /* Remnants: WD, NS, BH, MASSLESS_REMNANT */

            /* presumably menv (convective envelope mass) should be zero ? */
            newstar->menv = 0.0;

            /* similarly renv should be zero */
            newstar->renv = 0.0;

            /* set core radius to actual radius */
            newstar->core_radius = newstar->radius;

            /* generic k2 */
            newstar->moment_of_inertia_factor = CORE_MOMENT_OF_INERTIA_FACTOR;
        }

#ifdef NUCSYN
        /* Do nucleosynthesis calculations */
        Dprint("Call stellar_structure_nucsyn\n");
        if(caller_id != STELLAR_STRUCTURE_CALLER_common_envelope_evolution)
            stellar_structure_nucsyn(
                newstar->stellar_type,
                oldstar->stellar_type,
                newstar,
                stardata);
#endif//NUCSYN
        if(newstar->moment_of_inertia_factor<0.0)
        {
            Exit_binary_c(BINARY_C_OUT_OF_RANGE,
                          "Negative k2 = %g at stellar_structure exit! (mass=%g mt=%g core_mass[CORE_He]=%g lum=%g) \n",
                          newstar->moment_of_inertia_factor,
                          newstar->phase_start_mass,
                          newstar->mass,
                          Outermost_core_mass(newstar),
                          newstar->luminosity);
        }
        else if(newstar->age < -NEGATIVE_AGE_CHECK_MAGNITUDE)
        {
            Exit_binary_c(BINARY_C_OUT_OF_RANGE,
                          "Negative age = %g at stellar_structure exit!\n",
                          newstar->age);
        }

#ifdef ADAPTIVE_RLOF
    }
#endif

    char * cores = core_string(newstar,FALSE);
    Dprint("STELLAR_STRUCTURE out: star %d phase_start_mass=%.12g, age=%.12g, mt=%.12g, tm=%30.22g, tn=%.12g r=%.12g, lum=%.12g, stellar_type=%d (In was %d), %s, rc=%.12g menv = %12.12g, renv=%12.12g, k2=%12.12g, omega=%g jspin=%g ntp=%g\n",
           newstar->starnum,
           newstar->phase_start_mass,
           newstar->age,
           newstar->mass,
           newstar->tm,
           newstar->tn,
           newstar->radius,
           newstar->luminosity,
           newstar->stellar_type,
           oldstar->stellar_type,
           cores,
           newstar->core_radius,
           newstar->menv,
           newstar->renv,
           newstar->moment_of_inertia_factor,
           newstar->omega,
           newstar->angular_momentum,
           newstar->num_thermal_pulses);
    Safe_free(cores);

    /* backward evolution bug checks */
    if(0 && stardata->model.intermediate_step == FALSE &&
       caller_id!=STELLAR_STRUCTURE_CALLER_common_envelope_evolution &&
       stardata->model.dtm > 0 &&
       stardata->model.intpol == 0 &&
       newstar->stellar_type == HERTZSPRUNG_GAP &&
       (oldstar->stellar_type == GIANT_BRANCH ||
        oldstar->stellar_type==CHeB))
    {
        Exit_binary_c(BINARY_C_ALGORITHM_BRANCH_FAILURE,
                      "OOPS GB/CHeB to HG in one step! oldstar->stellar_type=%d stellar_type=%d dtm=%g dt=%g intpol=%dx age : old = %g, new = %g\n",
                      oldstar->stellar_type,
                      newstar->stellar_type,
                      stardata->model.dtm,
                      stardata->model.dt,
                      stardata->model.intpol,
                      oldstar->age,
                      newstar->age
            );
    }

#ifdef MINIMUM_STELLAR_MASS
    if(restore_mass==TRUE)
    {
        newstar->phase_start_mass = restore_m;
        newstar->mass = restore_mt;
    }
#endif // MINIMUM_STELLAR_MASS

#ifdef NANCHECKS
    if(1)
    {
        if(isnan(newstar->mass) ||
           isnan(newstar->menv) ||
           isnan(newstar->core_mass[CORE_He]) ||
           isnan(newstar->core_mass[CORE_CO]) ||
           isnan(newstar->core_mass[CORE_ONe]) ||
           isnan(newstar->core_mass[CORE_Si]) ||
           isnan(newstar->core_mass[CORE_Fe]) ||
           isnan(newstar->core_mass[CORE_NEUTRON]) ||
           isnan(newstar->moment_of_inertia_factor) ||
           isnan(newstar->luminosity) ||
           isnan(newstar->renv) ||
           isnan(newstar->angular_momentum) ||
           isnan(newstar->omega))
        {
            Exit_binary_c(BINARY_C_EXIT_NAN,
                          "NAN in stellar_structure type=%d t=%g mt=%g menv=%g core_masses=%g,%g,%g,%g,%g,%g L=%g R=%g k2=%g renv=%g jspin=%g omega=%g\n",
                          newstar->stellar_type,
                          stardata->model.time,
                          newstar->mass,
                          newstar->menv,
                          newstar->core_mass[CORE_He],
                          newstar->core_mass[CORE_CO],
                          newstar->core_mass[CORE_ONe],
                          newstar->core_mass[CORE_Si],
                          newstar->core_mass[CORE_Fe],
                          newstar->core_mass[CORE_NEUTRON],
                          newstar->luminosity,
                          newstar->radius,
                          newstar->moment_of_inertia_factor,
                          newstar->renv,
                          newstar->angular_momentum,
                          newstar->omega);
        }
    }
#endif // NANCHECKS

    newstar->phase_start_mass = fabs(newstar->phase_start_mass);

    return 0;
}

#endif//BSE
