#ifndef STELLAR_STRUCTURE_MACROS_H
#define STELLAR_STRUCTURE_MACROS_H

#define call_stellar_timescales2(STELLAR_TYPE,      \
                                 PHASE_START_MASS,  \
                                 MASS)              \
    stellar_timescales(stardata,                    \
                       newstar,                     \
                       newstar->bse,                \
                       (PHASE_START_MASS),          \
                       (MASS),                      \
                       (STELLAR_TYPE));             \
    newstar->tm = newstar->bse->tm;                 \
    newstar->tn = newstar->bse->tn;

#define call_stellar_timescalesvar(S) stellar_timescales(stardata,      \
                                                         oldstar,       \
                                                         (S)->bse,      \
                                                         (S)->phase_start_mass, \
                                                         (S)->mass,     \
                                                         (S)->stellar_type); \
    (S)->tm = (S)->bse->tm;                                             \
    (S)->tn = (S)->bse->tn;





#define RLOF_CALL (caller_id == STELLAR_STRUCTURE_CALLER_RLOF_mass_transfer_rate)

/* third dredge up lambda_min prescriptions */
enum {
    THIRD_DREDGE_UP_LAMBDA_MIN_AUTO = -1
};
#endif
