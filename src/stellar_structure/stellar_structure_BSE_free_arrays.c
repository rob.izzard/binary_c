#include "../binary_c.h"
No_empty_translation_unit_warning;

void stellar_structure_BSE_free_arrays(struct star_t * const star,
                                       const Boolean alloc[3])
{
    /*
     * Free arrays used to calculate BSE stellar structure
     */
#undef _Check
#define _Check(VAR,N)                           \
    if(alloc[N] == TRUE)                        \
    {                                           \
        Safe_free(star->bse->VAR);              \
    }

    if(star->bse)
    {
        _Check(timescales,0);
        _Check(luminosities,1);
        _Check(GB,2);
    }
}
