#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef BSE
#include "stellar_structure_debug.h"


/*
 * Adjust the stellar radius and luminosity to
 * ensure a smooth transition between the tip of the AGB
 * and the white dwarf track.
 */
#define CGS_CONV_CONSTANT (R_SUN*R_SUN/L_SUN)
void stellar_structure_small_envelope_perturbations(struct stardata_t * const stardata,
                                                    struct star_t * const newstar,
                                                    const double mtc, // Mc,max in Jarrod's thesis
                                                    const double remnant_luminosity,
                                                    const double remnant_radius)
{
    Dprint("Perturbing lum and rad due to small envelope mass, stellar type =%d was l=%12.12e r=%12.12e\n",
           newstar->stellar_type,newstar->luminosity,newstar->radius);

    double mu = 0.0;

    /*
     * Decide which algorithm to use.
     *
     * If stellar_type is GB or AGB, we can use the
     * Miller Bertolami tables.
     *
     * Otherwise, use the Hurley et al. (2002) algorithm.
     */
    const int method =
        stardata->preferences->small_envelope_method ==

        /* if we want to use Hurley+ 2002, always use it */
        SMALL_ENVELOPE_METHOD_BSE ? SMALL_ENVELOPE_METHOD_BSE :

        /* if we're GB or AGB, we can use Miller Bertolami's tracks */
        (
            ON_GIANT_BRANCH(newstar->stellar_type)&&
            stardata->preferences->small_envelope_method == SMALL_ENVELOPE_METHOD_MILLER_BERTOLAMI
            )
        ?
        SMALL_ENVELOPE_METHOD_MILLER_BERTOLAMI :
        /* but fall back on Hurley+ 2002 with other stellar types */
        SMALL_ENVELOPE_METHOD_BSE;

    if(method == SMALL_ENVELOPE_METHOD_BSE)
    {
        if(newstar->stellar_type>MAIN_SEQUENCE &&
           newstar->stellar_type<HeWD &&
           newstar->stellar_type!=HeMS)
        {
            if(newstar->stellar_type>=HeHG)
            {
                mu = (1.0 - Outermost_core_mass(newstar)/mtc)*5.0;
                Dprint("mu = %g from mc=%g mcmax=%g : 'menv'=%g\n",
                       mu,
                       Outermost_core_mass(newstar),
                       mtc,
                       mtc-Outermost_core_mass(newstar));
            }
            else
            {
                mu = envelope_mass(newstar)/newstar->mass*
                    Min(5.0,Max(1.20, pow(newstar->luminosity/LUM0,KAP)));
                Dprint("mu = %g from mc=%g newstar->mass=%g multiplier=%g : menv=%g\n",
                       mu,
                       Outermost_core_mass(newstar),
                       newstar->mass,
                       Min(5.0,Max(1.20, pow(newstar->luminosity/LUM0,KAP))),
                       envelope_mass(newstar));
            }

            if(mu<1.0)
            {
                /*
                 * Perturbation coefficients
                 *
                 * If you look at the equations in Hurley's thesis, Eqs. 2.127 and 2.128, p.58,
                 * they look terribly complicated.
                 *
                 * However, if you just take logs and treat s,r (lpert and rpert here)
                 * as magical constants, it's just an interpolation formula. The
                 * effective temperature of the remnant is then:
                 *
                 * 4 log T' =    [ s log L + (1-s) log Lc ]
                 *            -2 [ r log R + (1-r) log Rc ] + log (4 pi sigma)
                 *
                 * As you can see, the "luminosity" and "radius" parts are simply
                 * replaced by interpolations between log L and Lc, or log R and Rc.
                 *
                 * The problem is that sometimes this implies the remnant
                 * of the star is >> the temperature of the remnant. To determine
                 * when, just set log T' > log Tc. Enable LIMIT_RPERT to prevent
                 * this from happening (by limiting rpert while keeping lpert fixed).
                 *
                 * This is, of course, probably not realistic.
                 *
                 * How are r and s calculated? sigh. Now *that* is complicated :(
                 */
                double lpert=lpertf(newstar->mass,mu);
                double rpert=rpertf(newstar->mass,mu,newstar->radius,remnant_radius);

#ifdef LIMIT_RPERT
                double rlim = lpert*0.5*log10(newstar->luminosity/remnant_luminosity)/log10(newstar->radius/remnant_radius);
                rpert = Max(rlim,rpert);
                Dprint("Limit rpert to rlim=%g\n",rlim);
#endif
                Dprint("exponents s=%g r=%g\n",
                       lpert,
                       rpert);
                Dprint("L=%g Lc=%g : L/Lc=%g\n",
                       newstar->luminosity,
                       remnant_luminosity,
                       newstar->luminosity/remnant_luminosity);
                Dprint("R=%g Rc=%g (%g km) : R/Rc=%g\n",
                       newstar->radius,
                       remnant_radius,
                       remnant_radius*R_SUN*1e-5,
                       newstar->radius/remnant_radius);
                Dprint("Teff star=%g remn_ant=%g\n",
                       Teff_from_luminosity_and_radius(newstar->luminosity,newstar->radius),
                       Teff_from_luminosity_and_radius(remnant_luminosity,remnant_radius));
                Dprint("Pre-perturb:\n");
                Dprint("4 logT = log L - 2 log R - log(4 pi sigma)\n");
                Dprint("4 logT = [ log %g ] - 2 [ log %g ] - log(4 pi sigma)\n",
                       newstar->luminosity,
                       newstar->radius);
                Dprint("4 logT = [ %g ] - 2 [ %g ] - log(4 pi sigma)\n",
                       log10(newstar->luminosity),
                       log10(newstar->radius));
                Dprint("4 logT = %g - %g - %g\n",
                       log10(newstar->luminosity),
                       2.0*log10(newstar->radius),
                       log10(4.0*PI*STEFAN_BOLTZMANN_CONSTANT*CGS_CONV_CONSTANT));
                Dprint("4 logT = %g\n",
                       log10(newstar->luminosity)-2.0*log10(newstar->radius)
                       -log10(4.0*PI*STEFAN_BOLTZMANN_CONSTANT*CGS_CONV_CONSTANT));
                Dprint("  logT = %g\n",
                       0.25*(log10(newstar->luminosity)-2.0*log10(newstar->radius)
                             -log10(4.0*PI*STEFAN_BOLTZMANN_CONSTANT*CGS_CONV_CONSTANT)));
                Dprint("     T = %g\n\n",
                       exp10(
                           0.25*(log10(newstar->luminosity)-2.0*log10(newstar->radius)
                                 -log10(4.0*PI*STEFAN_BOLTZMANN_CONSTANT*CGS_CONV_CONSTANT))));
                Dprint("Perturbation\n\n");
                Dprint("4 logT = [ (1-s) log Lc + s log L ] - 2 [ (1-r) log Rc + r log R ] - log(4 pi sigma)\n");
                Dprint("4 logT = [ %g log Lc + %g log L ] - 2 [ %g log Rc + %g log R ] - log(4 pi sigma)\n",
                       1.0-lpert,lpert,
                       1.0-rpert,rpert);
                Dprint("4 logT = [ %g log %g + %g log %g ] - 2 [ %g log %g + %g log %g ] - log(4 pi sigma)\n",
                       1.0-lpert,remnant_luminosity,lpert,newstar->luminosity,
                       1.0-rpert,remnant_radius,rpert,newstar->radius);
                Dprint("4 logT = [ %g * %g + %g * %g ] - 2 [ %g * %g + %g * %g ] - log(4 pi sigma)\n",
                       1.0-lpert,log10(remnant_luminosity),
                       lpert,log10(newstar->luminosity),
                       1.0-rpert,log10(remnant_radius),
                       rpert,log10(newstar->radius));
                Dprint("4 logT = [ %g + %g ] - 2 [ %g + %g ] - log(4 pi sigma)\n",
                       (1.0-lpert)*log10(remnant_luminosity),
                       lpert*log10(newstar->luminosity),
                       (1.0-rpert)*log10(remnant_radius),
                       rpert*log10(newstar->radius));
                Dprint("4 logT = [ %g ] - 2 [ %g ] - log(4 pi sigma)\n",
                       (1.0-lpert)*log10(remnant_luminosity)+
                       lpert*log10(newstar->luminosity),
                       (1.0-rpert)*log10(remnant_radius)+
                       rpert*log10(newstar->radius));
                Dprint("4 logT =  %g  - %g - %g\n",
                       (1.0-lpert)*log10(remnant_luminosity)+
                       lpert*log10(newstar->luminosity),
                       2.0*((1.0-rpert)*log10(remnant_radius)+
                            rpert*log10(newstar->radius)),
                       log10(4.0*PI*STEFAN_BOLTZMANN_CONSTANT*CGS_CONV_CONSTANT));
                Dprint("4 logT =  %g\n",
                       (1.0-lpert)*log10(remnant_luminosity)+
                       lpert*log10(newstar->luminosity)
                       -2.0*((1.0-rpert)*log10(remnant_radius)+
                             rpert*log10(newstar->radius))
                       -log10(4.0*PI*STEFAN_BOLTZMANN_CONSTANT*CGS_CONV_CONSTANT));
                Dprint("  logT =  %g\n",
                       0.25*((1.0-lpert)*log10(remnant_luminosity)+
                             lpert*log10(newstar->luminosity)
                             -2.0*((1.0-rpert)*log10(remnant_radius)+
                                   rpert*log10(newstar->radius))
                             -log10(4.0*PI*STEFAN_BOLTZMANN_CONSTANT*CGS_CONV_CONSTANT)));
                Dprint("     T =  %g\n\n",
                       exp10(0.25*((1.0-lpert)*log10(remnant_luminosity)+
                                   lpert*log10(newstar->luminosity)
                                   -2.0*((1.0-rpert)*log10(remnant_radius)+
                                         rpert*log10(newstar->radius))
                                   -log10(4.0*PI*STEFAN_BOLTZMANN_CONSTANT*CGS_CONV_CONSTANT))));
                Dprint("perturbed radius\n");
                Dprint("log R = %g log %g + %g log %g = %g * %g + %g * %g = %g + %g = %g > R=%g\n\n",
                       1.0-rpert,remnant_radius,rpert,newstar->radius,
                       (1.0-rpert),
                       log10(remnant_radius),
                       rpert,
                       log10(newstar->radius),
                       (1.0-rpert)*log10(remnant_radius),rpert*log10(newstar->radius),
                       (1.0-rpert)*log10(remnant_radius)+rpert*log10(newstar->radius),
                       exp10((1.0-rpert)*log10(remnant_radius)+
                           rpert*log10(newstar->radius)));
                Dprint("Error condition : %g > %g\n",
                       lpert * log10(newstar->luminosity/remnant_luminosity),
                       2.0*rpert *log10(newstar->radius/remnant_radius));

                newstar->luminosity = remnant_luminosity*
                    pow(newstar->luminosity/remnant_luminosity,lpert);

                if(Less_or_equal(newstar->radius,remnant_radius))
                {
                    newstar->radius = remnant_radius;
                }
                else
                {
                    newstar->radius = remnant_radius*
                        pow(newstar->radius/remnant_radius,rpert);
                }
                Dprint("real perturbed T=%g\n",
                       Teff_from_luminosity_and_radius(newstar->luminosity,
                                                       newstar->radius));
            }
            newstar->core_radius = Min(newstar->core_radius,newstar->radius);
        }

    }
    else if(method == SMALL_ENVELOPE_METHOD_MILLER_BERTOLAMI)
    {
        stellar_structure_small_envelope_miller_bertolami(stardata,
                                                          newstar,
                                                          remnant_radius,
                                                          remnant_luminosity);
    }
    else
    {
        Exit_binary_c(BINARY_C_ALGORITHM_OUT_OF_RANGE,
                      "Unknown method = %d for small envelope perturbations.",
                      method);
    }

    if(Is_not_zero(newstar->radius) && Is_not_zero(newstar->radius))
    {
        Dprint("Post-perturb: l=%12.12e r=%12.12e teff=%g\n",
               newstar->luminosity,
               newstar->radius,
               Teff_from_luminosity_and_radius(newstar->luminosity,
                                               newstar->radius));
    }
}
#endif//BSE
