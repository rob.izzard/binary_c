#include "../binary_c.h"
No_empty_translation_unit_warning;


double lambda_3dup_stancliffe(struct star_t * const newstar,
                              const double n,
                              struct stardata_t * const stardata,
                              Boolean * const above_mcmin)
{
    /*
     * Third dredge up efficiency based on Richard Stancliffe's models
     * of SMC and LMC metallicity stars. Valid for 1.5<M<6 only.
     * Fit form is like Karakas et al. except with a drop after about 15 pulses
     */
    double l=0.0;
    double m = Limit_range(newstar->phase_start_mass,1.5,6.0);
    double mcmin =
        stardata->common.metallicity > 0.006 ?
        // high metallicity : use LMC
        (9.65900e-01+
         2.63090e-01*exp(-Pow2(m-1.07240)/1.26290)+
         -7.37630e-01/(1+pow(3.34400e-01,2.86280-m)))
        :
        // low metallicity : use SMC
        (5.80420e-01+
         6.53760e-02*m/(1+pow(9.80490e-02,m-2.66910)));

    mcmin += stardata->preferences->delta_mcmin;
    mcmin = Max(0.0,mcmin);

    if(newstar->core_mass[CORE_He] < mcmin)
    {
        l = 0.0;
        * above_mcmin = FALSE;
    }
    else
    {
        double z = stardata->common.metallicity;
        double c1=15.0*z +
            Quartic(m,2.03690,-2.87750,1.62060,-3.35270e-01,2.32110e-02);
        double c2 =
            6.83500-4.44870/(1.0+pow(2.0e-03,m-2.5));
        l = Max(stardata->preferences->lambda_min, c1)
            * (1.0 - exp(-n/c2) * (1.0 - 0.42*n));
        Clamp(l,0.0,1.0);
        * above_mcmin = TRUE;
    }

    return(l);
}
