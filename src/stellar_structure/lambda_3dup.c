#include "../binary_c.h"
No_empty_translation_unit_warning;


double lambda_3dup(struct stardata_t * Restrict const stardata,
                   struct star_t * Restrict const newstar,
                   Boolean * const above_mcmin)
{
    double lambda;

    /*
     * Lambda = 0 if third dup is deliberately switched off
     * or if menv < threshold (if NOT using the Hurley formalism).
     */
    Dprint("Calc lambda : tdup on? %d : menv = %g > menv,min = %g ?\n",
           stardata->preferences->third_dup,
           newstar->mass - newstar->core_mass[CORE_He],
           Minimum_envelope_mass_for_3dup);


    if(stardata->preferences->third_dup == FALSE ||
       newstar->mass - newstar->core_mass[CORE_He] < Minimum_envelope_mass_for_3dup)
    {
        *above_mcmin = FALSE;
        lambda = 0.0;
    }
    else
    {
        /*
         * Determine algorithm to use
         */
        int third_dup_algorithm = AGB_Third_Dredge_Up_Algorithm;

        if(third_dup_algorithm == AGB_THIRD_DREDGE_UP_ALGORITHM_KARAKAS)
        {
            lambda = lambda_3dup_Karakas(newstar,stardata,above_mcmin);
        }
        else if(third_dup_algorithm == AGB_THIRD_DREDGE_UP_ALGORITHM_STANCLIFFE)
        {
            /*
             * Richard Stancliffe's fits for LMC/SMC
             */
            lambda = lambda_3dup_stancliffe(newstar,
                                            newstar->num_thermal_pulses_since_mcmin,
                                            stardata,
                                            above_mcmin);
        }
        else if(third_dup_algorithm == AGB_THIRD_DREDGE_UP_ALGORITHM_HURLEY)
        {
            *above_mcmin = TRUE;
            lambda = lambda_3dup_Hurley(newstar->phase_start_mass);
        }
        else
        {
            lambda = 0.0;
            Exit_binary_c(BINARY_C_ALGORITHM_BRANCH_FAILURE,
                          "AGB 3dup algorithm %d is unknown",
                          third_dup_algorithm);

        }

        /*
         * Artificial modulation
         */
        lambda *= stardata->preferences->lambda_multiplier;

        if(Fequal(stardata->preferences->lambda_min,
                  THIRD_DREDGE_UP_LAMBDA_MIN_AUTO))
        {
            /*
             * Function of metallicity as in Izzard
             * and Tout (2004)
             */
            lambda = Max3(0.0,
                          0.80316 - 37.973 *stardata->common.metallicity,
                          lambda);
        }
        else
        {
            /*
             * Use value passed in (might be zero)
             */
            lambda = Max(stardata->preferences->lambda_min,
                         lambda);
        }
    }

    Dprint("Calc lambda : Boolean %d : menv %g > %g ? -> above mcmin %d, lambda = %g\n",
           stardata->preferences->third_dup,
           Max(0.0,newstar->mass - newstar->core_mass[CORE_He]),
           Minimum_envelope_mass_for_3dup,
           * above_mcmin,
           lambda);
    return lambda;
}
