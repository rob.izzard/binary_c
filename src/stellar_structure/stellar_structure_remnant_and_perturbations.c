#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef BSE

#include "stellar_structure_debug.h"

/*
 * Calculate the core radius and the luminosity and radius of the
 * remnant that the star will become.
 *
 * Also calculates k2 (radius of gyration) in some cases.
 *
 * If SAVE_MASS_HISTORY is defined and
 * stardata->preferences->adjust_structure_from_mass_changes == TRUE
 * we adjust the stellar structure based on mass-loss histrory.
 */


void stellar_structure_remnant_and_perturbations(struct star_t * const newstar,
                                                 double remnant_luminosity,
                                                 double remnant_radius,
                                                 double tbagb,
                                                 double mtc,
                                                 double mcx,
                                                 struct stardata_t * const stardata)
{
    const double * metallicity_parameters =
        stardata->common.metallicity_parameters;
    Dprint("RC %g\n",newstar->core_radius);
    Dprint("Calculating core radius, lum and rad of remn a nt star, stellar type %d.\n",newstar->stellar_type);

    double tau = 0.0;

#ifdef SAVE_MASS_HISTORY
    if(stardata->preferences->adjust_structure_from_mass_changes == TRUE)
    {
        stellar_structure_adjust_from_mass_history(stardata,
                                                   newstar);
    }
#endif//SAVE_MASS_HISTORY

    /*
     * Main sequence
     */
    if(ON_EITHER_MAIN_SEQUENCE(newstar->stellar_type))
    {
        newstar->core_radius = 0.0;
        remnant_luminosity = 0.0;
        remnant_radius = 0.0;
    }
    /*
     * HG and RGB
     */
    else if(newstar->stellar_type==HERTZSPRUNG_GAP
            ||
            newstar->stellar_type==FIRST_GIANT_BRANCH)
    {
        const Boolean degenerate_core =
            Less_or_equal(newstar->phase_start_mass,
                          metallicity_parameters[ZPAR_MASS_HE_FLASH]);
        const double LWD = lwd(stardata,
                               newstar,
                               newstar->core_mass[CORE_He],
                               0.0,
                               HeWD,
                               FALSE);
        if(degenerate_core == TRUE)
        {
            remnant_luminosity = LWD;
            remnant_radius = rwd(stardata,
                                 newstar,
                                 newstar->core_mass[CORE_He],
                                 newstar->core_stellar_type,
                                 TRUE);
#ifdef HALL_TOUT_2014_RADII
            newstar->core_radius =
                Hall_Tout_2014_low_mass_HG_RGB_radius(stardata,
                                                      newstar,
                                                      newstar->core_mass[CORE_He]);
#else
            newstar->core_radius = 5.0*remnant_radius;
#endif // HALL_TOUT_2014_RADII
        }
        else
        {
            remnant_luminosity = Max(LWD,lzhef(newstar->core_mass[CORE_He]));
            remnant_radius = rzhef(newstar->core_mass[CORE_He]);
            newstar->core_radius = remnant_radius;
        }
    }
    /*
     * CHeB
     */
    else if(newstar->stellar_type==CORE_HELIUM_BURNING)
    {
        tau = (newstar->age - newstar->bse->timescales[T_HE_IGNITION])/newstar->bse->timescales[T_HE_BURNING];
        Dprint("Call calc_lum as HeMS although the star is really CHeB (tau=%g core_mass[CORE_He]=%g)\n",
               tau,
               newstar->core_mass[CORE_He]);
        call_stellar_timescales2(HeMS,
                                 newstar->core_mass[CORE_He],
                                 newstar->core_mass[CORE_He]);

        double am = Max(0.0, 0.850 - 0.080*newstar->core_mass[CORE_He]);
        remnant_luminosity = newstar->bse->luminosities[L_ZAMS]*
            (1.0+0.450*tau+am*Pow2(tau));
        remnant_radius = rzhef(newstar->core_mass[CORE_He]);
        am = Max(0.0, 0.40 - 0.220*log10(newstar->core_mass[CORE_He]));
        remnant_radius *= (1.0 + am * (tau-Pow6(tau)));
        Dprint("Call lt2 for core radius with stellar_type=%d phase_start_mass=%g mass=%g\n",
               newstar->stellar_type,
               newstar->phase_start_mass,
               newstar->mass);
        call_stellar_timescales2(newstar->stellar_type,
                                 newstar->phase_start_mass,
                                 newstar->mass);
        newstar->core_radius = remnant_radius;
    }
    /*
     * EAGB
     */
    else if(newstar->stellar_type==EARLY_ASYMPTOTIC_GIANT_BRANCH)
    {
        if(newstar->tn > tbagb)
        {
            tau = 3.0 * (newstar->age - tbagb) / (newstar->tn - tbagb);
        }

        Dprint("EAGB tau = %12.12e from age=%12.12e tbagb=%12.12e tn=%12.12e m=%g mc=%g\n",
               tau,
               newstar->age,
               tbagb,
               newstar->tn,
               newstar->mass,
               newstar->core_mass[CORE_He]
            );

        call_stellar_timescales2(HeGB,
                                 newstar->core_mass[CORE_He],
                                 newstar->core_mass[CORE_He]);
        if(Is_zero(newstar->age))
        {
            Exit_binary_c(2,"Age should not be zero");
        }
        remnant_luminosity = lmcgbf(mcx,newstar->bse->GB);

        Dprint("LMCGBF gave %g, tau = %g, endMS=%g\n",
               remnant_luminosity,
               tau,
               newstar->bse->luminosities[L_END_MS]);

        if(tau<1.0)
        {
            remnant_luminosity = newstar->bse->luminosities[L_END_MS]*
                pow(remnant_luminosity/newstar->bse->luminosities[L_END_MS] ,tau);
        }

        Dprint("REMNA NT_LUMINOSITY remna nt_luminosity = %12.12e\n",
               remnant_luminosity);


        remnant_radius = rzhef(newstar->core_mass[CORE_He]);
        Dprint("rzhef(%g) gave %g\n",newstar->core_mass[CORE_He],remnant_radius)

            double r1 = rhehgf(newstar->core_mass[CORE_He],
                               remnant_luminosity,
                               remnant_radius,
                               newstar->bse->luminosities[L_END_MS]);

        Dprint("rhehgf(%g,%g,%g,%g) = %g\n",
               newstar->core_mass[CORE_He],
               remnant_luminosity,
               remnant_radius,
               newstar->bse->luminosities[L_END_MS],
               r1);

        double r2 = rhegbf(remnant_luminosity);

        Dprint("EAGB set remna nt_radius=%12.12e from Min(%12.12e,%12.12e)\n",
               Min(r1,r2),r1,r2);

        /*
         * Fudge here to catch negative radii, which are
         * sometimes given by rhehgf.
         * If both radii are positive, use the minimum,
         * but ensure it's > 1e-3 Rsun.
         * If either is negative, use the other.
         * Otherwise, just use 1e-3 Rsun so that
         * there are no nans.
         *
         * This solution is not ideal.
         */
        if(r1>0.0 && r2>0.0)
        {
            /* the original BSE algorithm */
            remnant_radius = Max(1e-3,Min(r1,r2));
        }
        else if(r1>0.0)
        {
            remnant_radius = r1;
        }
        else if(r2>0.0)
        {
            remnant_radius = r2;
        }
        else
        {
            /* at least something that will not cause a crash */
            remnant_radius = 1e-3;
        }

        call_stellar_timescales2(newstar->stellar_type,
                                 newstar->phase_start_mass,
                                 newstar->mass);
        newstar->core_radius = remnant_radius;
        Dprint("EAGB Rc = %12.12e (remn ant_radius=%12.12e)\n",
               newstar->core_radius,
               remnant_radius);
    }
    else if(newstar->stellar_type==TPAGB)
    {

        remnant_luminosity = lwd(stardata,
                                 newstar,
                                 newstar->core_mass[CORE_He],
                                 0.0,
                                 COWD,
                                 FALSE);
        remnant_radius = Max(rwd(stardata,
                                 newstar,
                                 newstar->core_mass[CORE_He],
                                 newstar->stellar_type,
                                 FALSE),
                             rns(newstar->core_mass[CORE_He]));

#ifdef HALL_TOUT_2014_RADII
        newstar->core_radius = Hall_Tout_2014_AGB_radius(stardata,
                                                         newstar,
                                                         newstar->core_mass[CORE_He],
                                                         newstar->core_stellar_type);
#else
        newstar->core_radius = 5.0*remnant_radius;
#endif //HALL_TOUT_2014_RADII

        newstar->core_radius = Max(newstar->core_radius,
                                   rns(newstar->core_mass[CORE_He]));

    }
    else if(newstar->stellar_type<=NAKED_HELIUM_STAR_GIANT_BRANCH)
    {
        remnant_luminosity = lwd(stardata,
                                 newstar,
                                 newstar->core_mass[CORE_CO],
                                 0.0,
                                 COWD,
                                 FALSE);
        remnant_radius = rwd(stardata,
                             newstar,
                             newstar->core_mass[CORE_CO],
                             newstar->core_stellar_type,
                             FALSE);
#ifdef HALL_TOUT_2014_RADII
        if(newstar->stellar_type==HeHG)
        {
            newstar->core_radius =
                Hall_Tout_2014_HeHG_radius(newstar->core_mass[CORE_CO]);
        }
        else if(newstar->stellar_type==HeGB)
        {
            newstar->core_radius =
                Hall_Tout_2014_HeGB_radius(stardata,
                                           newstar,
                                           newstar->core_mass[CORE_CO]);
        }
#else
        newstar->core_radius = 5.0*remnant_radius;
#endif
        newstar->core_radius = Max(newstar->core_radius,
                                   rns(newstar->core_mass[CORE_He]));

        Dprint("Helium giant remn ant: L=%g R=%g -> Rc=%g\n",
               remnant_luminosity,
               remnant_radius,
               newstar->core_radius);
    }
    else
    {
        newstar->core_radius = Max(newstar->radius,
                                   rns(newstar->core_mass[CORE_He]));
        newstar->menv = 1e-10;
        newstar->renv = 1e-10;
        newstar->moment_of_inertia_factor = CORE_MOMENT_OF_INERTIA_FACTOR;
    }

    /*
     * Perturb the luminosity and radius due to small envelope mass.
     */
    stellar_structure_small_envelope_perturbations(
        stardata,
        newstar,
        mtc,
        remnant_luminosity,
        remnant_radius);
}

#endif//BSE
