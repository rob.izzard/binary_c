#include "../binary_c.h"
No_empty_translation_unit_warning;


/*
 * Remnant radius
 */
#ifdef BSE
double Pure_function remnant_radius(struct stardata_t * const stardata,
                                    struct star_t * const star,
                                    double * const metallicity_parameters,
                                    const Stellar_type stellar_type,
                                    const double mass,
                                    const double mc,
                                    const double r)
{
    double rr;
    if(stellar_type <= FIRST_GIANT_BRANCH)
    {
        rr = mass > metallicity_parameters[ZPAR_MASS_HE_FLASH] ?
            rzhef(mc) :
            rwd(stardata,star,mc,stellar_type,FALSE);
    }
    else if(stellar_type==CORE_HELIUM_BURNING ||
            stellar_type==EARLY_ASYMPTOTIC_GIANT_BRANCH)
    {
        rr = rzhef(mc);
    }
    else if(stellar_type < COWD)
    {
        rr = rwd(stardata,star,mc,stellar_type,TRUE);
    }
    else
    {
        rr = r;
    }
    return rr;
}
#endif //BSE
