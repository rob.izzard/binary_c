#ifndef HRDIAG_STRUCTURES_H
#define HRDIAG_STRUCTURES_H

#include "../binary_c_structures.h"

/*
 * This structure holds most of the information used in the hrdiag
 * routines.
 */


struct hrdiag_t {
  
  double mass;
  double age;
  double mt;
  double core_mass[CORE_He];
  double tm;
  double tn;
  double r;
  double rc;
  double lum;
  double menv;
  double renv;
  double k2;

  double mc_CO_core;
  double remnant_radius;
  double remnant_luminosity;

  double rzams;
  double TAMS_radius;
  double rg;

  double mtc;

  double tbagb;

  double *timescales;
  double *luminosities;
  double *GB;

  double *metallicity_parameters;
  double *main_sequence_parameters;
  double *giant_branch_parameters;

  Stellar_type stellar_type;
  Stellar_type stellar_typein;

  struct star_t * star;
  struct stardata_t * stardata;
};

#endif // HRDIAG_STRUCTURES_H
