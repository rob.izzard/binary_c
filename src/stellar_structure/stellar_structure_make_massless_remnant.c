#include "../binary_c.h"
No_empty_translation_unit_warning;


void stellar_structure_make_massless_remnant(struct stardata_t * const stardata,
                                             struct star_t * const newstar)
{
    /*
     * Turn a star into a massless remnant: the only
     * quantity that survives is the star number.
     */
    Dprint("make star %d (pointer %p) a massless remna-nt\n",
           newstar->starnum,
           (void*)newstar);
    newstar->baryonic_mass = 0.0;
    newstar->mass = 0.0;
    newstar->luminosity = REMNANT_LUMINOSITY;
    newstar->radius = REMNANT_RADIUS;
    newstar->effective_radius = REMNANT_RADIUS;
    newstar->rzams = REMNANT_RADIUS;
    newstar->TAMS_radius = REMNANT_RADIUS;
    newstar->core_radius = REMNANT_RADIUS;
    newstar->renv = REMNANT_RADIUS;
    newstar->stellar_type = MASSLESS_REMNANT;
    newstar->hybrid_HeCOWD = FALSE;
    newstar->age = 0.0;
    newstar->vwind = 0.0;
    set_no_core(newstar);
    newstar->core_stellar_type = MASSLESS_REMNANT;
    newstar->angular_momentum = 0.0;
    newstar->hybrid_HeCOWD = FALSE;
    newstar->drdm = 0.0;
    newstar->omega = 0.0;
    newstar->angular_momentum = 0.0;
    newstar->moment_of_inertia_factor = 0.0;
}
