#include "../binary_c.h"
No_empty_translation_unit_warning;

#undef Cprint
#define Cprint(...)
//#define Cprint(...) printf("DEEPCOPY: ");printf(__VA_ARGS__);fflush(NULL);

void stellar_structure_BSE_alloc_arrays(struct star_t * const star,
                                        struct BSE_data_t * const bse,
                                        Boolean alloc[3])
{
    /*
     * Allocate arrays for BSE stellar structure call
     *
     * We use bse if it is non-NULL, and set the star to it.
     *
     * set the alloc[3] array Booleans so the above arrays
     * can be freed later if required
     */
    if(star != NULL)
    {
        Cprint("star is non-null\n");
        if(bse != NULL)
        {
            /*
             * Use bse passed in
             */
            if(star->bse == bse)
            {
                Cprint("use bse passed in %p %p %p : cpy bse=%p to star->bse=%p\n",
                       (void*)bse->timescales,
                       (void*)bse->luminosities,
                       (void*)bse->GB,
                       (void*)bse,
                       (void*)star->bse);
            }
            if(star->bse != bse)
            {
                memcpy(star->bse,
                       bse,
                       sizeof(struct BSE_data_t));
            }
        }
        else if(star->bse == NULL)
        {
            /*
             * allocate bse in the star if it
             * doesn't already exist
             */
            star->bse = Calloc(1,sizeof(struct BSE_data_t));
        }

#define Check(VAR,N,SIZE)                                   \
        if(star->bse->VAR == NULL)                          \
        {                                                   \
            star->bse->VAR = Calloc(1,sizeof(double)*SIZE); \
            alloc[N] = TRUE;                                \
        }                                                   \
        else                                                \
        {                                                   \
            alloc[N] = FALSE;                               \
        }

        Check(timescales,0,TSCLS_ARRAY_SIZE);
        Check(luminosities,1,LUMS_ARRAY_SIZE);
        Check(GB,2,GB_ARRAY_SIZE);
    }
    else
    {
        /*
         * Should not be required
         */
        for(unsigned int i=0;i<3;i++)
        {
            alloc[i] = FALSE;
        }
    }

}
