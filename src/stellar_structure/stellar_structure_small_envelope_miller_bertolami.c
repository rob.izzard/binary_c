#include "../binary_c.h"
No_empty_translation_unit_warning;


/*
 * Choose one or the other
 */
#define USE_DIRECT
//#define USE_FITS


#ifdef USE_DIRECT
#include "miller_bertolami_postagb.h"
#include "miller_bertolami_postagb_n.h"
#endif

#ifdef USE_FITS
#include "miller_bertolami_postagb_coeffs_n.h"
#endif

void stellar_structure_small_envelope_miller_bertolami(struct stardata_t * const stardata,
                                                       struct star_t * const star,
                                                       const double remnant_radius Maybe_unused,
                                                       const double remnant_luminosity Maybe_unused
    )
{

    double menv = star->mass - star->core_mass[CORE_He];
#ifdef USE_DIRECT

    /*
     * Use tracks for Miller Bertolami to calculate the
     * decline in L and R.
     *
     * These are valid when menv/M < 0.01 and L > 1.0 Lsun
     */
    if(menv/star->mass < 0.01 && star->luminosity > 1.0)
    {
        double param[TABLE_MILLER_BERTOLAMI_N_PARAMS] =
            {
                log10(stardata->common.metallicity),
                log10(star->core_mass[CORE_He]), // assume mass is ~ core_mass[CORE_He] ~ constant
                log10(menv/star->mass)
            };
        double data[TABLE_MILLER_BERTOLAMI_N_DATA];

        //test_table(stardata,stardata->store->miller_bertolami);

        Interpolate(stardata->store->miller_bertolami,
                    param,
                    data,
                    FALSE);

        /*
         * Data give
         *
         * 0 : log10(L/Lsun)
         * 1 : log10(L/Linit) = log10(fL)
         * 2 : log10(R/Rsun)
         * 3 : log10(R/Rinit) = log10(fR)
         *
         * as a function of M,Menv,Z
         *
         * When the multipliers fL, fR are > 1, cap them at 1.
         */
        double fL = Min(1.0,exp10(data[1]));
        double fR = Min(1.0,exp10(data[3]));
        star->luminosity *= fL;
        star->radius *= fR;
        printf("FACS kw=%d t=%20.12e age=%20.12e m=%g mc=%g menv=%g (param: %g, %g, %g) -> (data: %g %g %g %g): fL=%g fR=%g -> L=%g R=%g (MB L=%g R=%g)\n",
               star->stellar_type,
               stardata->model.time,
               star->age,
               star->mass,
               star->core_mass[CORE_He],
               menv,
               param[0],
               param[1],
               param[2],
               data[0],
               data[1],
               data[2],
               data[3],
               fL,
               fR,
               star->luminosity,
               star->radius,
               exp10(data[0]),
               exp10(data[2])
            );
    }

#endif

#ifdef USE_FITS
    double x = log10(menv / star->mass);

    {
        /*
         * Interpolate to get coefficients for R
         */
        double param[TABLE_MILLER_BERTOLAMI_COEFFS_R_N_PARAMS] =
            {
                stardata->common.metallicity,
                star->core_mass[CORE_He]
            };
        double data[TABLE_MILLER_BERTOLAMI_COEFFS_R_N_DATA];

        Interpolate(stardata->store->miller_bertolami_coeffs_R,
                    param,
                    data,
                    FALSE);

        double fR = Min3(0,
                         data[0] / (1.0  + exp(data[1] * x + data[2])) - data[3],
                         data[4] + data[5] * x);
        fR = exp10(fR);
        star->radius *= fR;

        printf("Interpolate R %g %g -> %g %g %g %g %g %g -> fR(%g) = %g\n",
               param[0],
               param[1],
               data[0],
               data[1],
               data[2],
               data[3],
               data[4],
               data[5],
               x,
               fR);
    }

    {
        /*
         * Interpolate to get coefficients for L
         */
        double param[TABLE_MILLER_BERTOLAMI_COEFFS_L_N_PARAMS] =
            {
                stardata->common.metallicity,
                star->core_mass[CORE_He]
            };
        double data[TABLE_MILLER_BERTOLAMI_COEFFS_L_N_DATA];

        Interpolate(stardata->store->miller_bertolami_coeffs_L,
                    param,
                    data,
                    FALSE);


        double fL = Min3(0,
                         data[0] / (1.0  + exp(data[1] * x + data[2])) - data[3],
                         data[4] + data[5] * x);
        fL = exp10(fL);
        star->luminosity *= fL;

        printf("Interpolate L %g %g -> %g %g %g %g %g %g -> fL(%g) = %g\n",
               param[0],
               param[1],
               data[0],
               data[1],
               data[2],
               data[3],
               data[4],
               data[5],
               x,
               fL
            );
    }

#endif
}
