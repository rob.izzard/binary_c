#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "stellar_structure_debug.h"

void stellar_structure_BH(struct stardata_t * const stardata,
                          struct star_t * const newstar)
{
    Dprint("Black Hole\n");

    newstar->phase_start_mass = newstar->mass;
    set_no_core(newstar);
    newstar->core_mass[CORE_BLACK_HOLE] = newstar->mass;
    newstar->luminosity = REMNANT_LUMINOSITY;
    if(stardata->preferences->require_drdm == TRUE)
    {
        const double dm = newstar->mass * 1e-7;
        update_mass(stardata,newstar,dm);
        const double rm = rbh(newstar->mass);
        update_mass(stardata,newstar,-dm);
        newstar->radius = rbh(newstar->mass);
        newstar->drdm = (rm - newstar->radius) / dm;
    }
    else
    {
        newstar->radius = rbh(newstar->mass);
    }
    newstar->menv = 0.0;
    newstar->renv = 0.0;
    newstar->TZO = TZO_NONE;
}
