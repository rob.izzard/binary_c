#include "../binary_c.h"
No_empty_translation_unit_warning;


double Pure_function lambda_3dup_Hurley(const double m)
{
    /*
     * Third dredge up efficiency based on Hurley et al. (2002)
     */
    return Min(0.90,(0.30+0.0010*Pow5(m)));
}
