#include "../binary_c.h"
No_empty_translation_unit_warning;


void set_stellar_structure_struct_from_star(struct star_t * const newstar,
                                            struct star_t * const oldstar)
{
    /*
     * Given a star_t struct, set the new_stellar_structure struct.
     * Does the opposite of set_star_struct_from_stellar_structure.
     */
#define set(x) newstar->x = oldstar->x
    set(age);
    set(stellar_type);
    set(radius);
    set(mass);
    set(core_radius);
    set(luminosity);
    set(tm);
    set(tn);
    set(renv);
    set(moment_of_inertia_factor);
    set(phase_start_mass);
    set(phase_start_core_mass);
    set(core_mass[CORE_CO]);
    set(core_mass[CORE_He]);
    set(core_mass[CORE_ONe]);
    set(core_mass[CORE_Si]);
    set(core_mass[CORE_Fe]);
    set(core_mass[CORE_NEUTRON]);
    set(max_MS_core_mass);
    set(time_first_pulse);
    set(num_thermal_pulses);
    set(time_prev_pulse);
    set(prev_tagb);
    set(menv_1tp);
    set(mc_1tp);
    set(core_mass_no_3dup);
    set(interpulse_period);
    set(time_next_pulse);
    set(lambda_3dup);
    set(num_thermal_pulses_since_mcmin);
    set(spiky_luminosity);
    memcpy(newstar->derivative,
           oldstar->derivative,
           DERIVATIVE_STELLAR_NUMBER*sizeof(double));
    set(stellar_type);
    set(SN_type);
}
