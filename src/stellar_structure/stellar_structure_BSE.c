/*
 * stellar_structure_BSE :
 *
 * Updates the star struct for its age with the new stellar structure,
 * according to binary_c's modified BSE algorithm.
 *
 * NB this function creates a "newstar" struct (in tmpstore), which is copied
 * from the "star" struct passed in. Changes are made to "newstar" and then,
 * if all is ok, this is copied over "star".
 */
#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "stellar_structure_debug.h"

#ifdef BSE
static void _checks(struct stardata_t * const stardata,
                    struct star_t * const star);

int stellar_structure_BSE(struct stardata_t * const stardata,
                          const Caller_id caller_id,
                          struct star_t * const star,
                          struct BSE_data_t * bse)
{

    /*
     * Make the "newstar" struct in the tmpstore if required.
     * Copy the old star on top of it.
     */
    if(stardata->tmpstore->stellar_structure_newstar == NULL)
    {
        stardata->tmpstore->stellar_structure_newstar = New_star;
    }
    struct star_t * const newstar = stardata->tmpstore->stellar_structure_newstar;
    Dprint("stellar structure called from %s with age %g (star %d, type %d, bse %p)\n",
           Stellar_structure_caller_string(caller_id),
           star->age,
           star->starnum,
           star->stellar_type,
           (void*)bse);
    Dprint("Made stellar_structure_newstar at %p\n",
           (void*)stardata->tmpstore->stellar_structure_newstar);
    copy_star(stardata,
              star,
              newstar);

    Dprint("newstar = %p\n",(void*)newstar);

    const int ret = stellar_structure_BSE_with_newstar(stardata,
                                                       caller_id,
                                                       star,
                                                       newstar,
                                                       bse);

    _checks(stardata,star);
    return ret;
}


int stellar_structure_BSE_with_newstar(struct stardata_t * const stardata,
                                       const Caller_id caller_id,
                                       struct star_t * const oldstar,
                                       struct star_t * const newstar,
                                       struct BSE_data_t * bse)
{
    Dprint("oldstar %p newstar %p bse %p bse->timescales %p : oldstar Mgrav %g Mbary %g; newstar Mgrav %g Mbary %g \n",
           (void*)oldstar,
           (void*)newstar,
           (void*)bse,
           (void*)(bse?bse->timescales:NULL),
           oldstar->mass,
           oldstar->baryonic_mass,
           newstar->mass,
           newstar->baryonic_mass);

    Supernova_type new_SN_type = SN_NONE;

    Boolean alloc[3] = {FALSE};
    Dprint("pre-alloc\n");
    stellar_structure_BSE_alloc_arrays(newstar,
                                       bse,
                                       alloc);
    Dprint("done alloc\n");

    /*
     * Except for compact remnants, ignore binding
     * energy.
     */
    if(newstar->stellar_type < NEUTRON_STAR &&
       newstar->TZO == FALSE)
    {
        newstar->baryonic_mass = newstar->mass;
    }

    Dprint("timescales = %p (alloced? %s) luminosities = %p (alloced? %s) GB = %p (alloced? %s)\n",
           (void*)(newstar->bse?newstar->bse->timescales:NULL),
           Yesno(alloc[0]),
           (void*)(newstar->bse?newstar->bse->luminosities:NULL),
           Yesno(alloc[1]),
           (void*)(newstar->bse?newstar->bse->GB:NULL),
           Yesno(alloc[2]));
    Dprint("newstar %p\n",(void*)newstar);
    Dprint("bse %p\n",newstar ? (void*)newstar->bse : NULL);
    Dprint("timescales %p\n",(newstar && newstar->bse) ? (void*)newstar->bse->timescales : NULL);
    Dprint("tscl 0 %g\n",(newstar && newstar->bse) ? newstar->bse->timescales[0] : -1);
    /*
     * If any of the above is newly allocated, or if the first element
     * of the timescales is FORCE_TIMESCALES_CALCULATION, recalculate
     * the star's timescales and luminosities
     */
    if(alloc[0] == TRUE ||
       alloc[1] == TRUE ||
       alloc[2] == TRUE ||
       (newstar && newstar->bse && Fequal(newstar->bse->timescales[0],
                                          FORCE_TIMESCALES_CALCULATION)) )
    {
        Dprint("Calculate timescales\n");
        stellar_timescales(stardata,
                           newstar,
                           newstar->bse,
                           newstar->phase_start_mass,
                           newstar->mass,
                           newstar->stellar_type
                           );
    }
    else
    {
        Dprint("Use existing timescales\n");
        stellar_timescales(stardata,
                           newstar,
                           newstar->bse,
                           newstar->phase_start_mass,
                           newstar->mass,
                           newstar->stellar_type
                           );
    }

    /*
     * Calculate new stellar structure
     */
    Dprint("Calculate stellar structure (BSE) phase start masses %g %g this star Mgrav=%g Mbary=%g (allocs %s %s %s)\n",
           stardata->star[0].phase_start_mass,
           stardata->star[1].phase_start_mass,
           newstar->mass,
           newstar->baryonic_mass,
           Yesno(alloc[0]),
           Yesno(alloc[1]),
           Yesno(alloc[2]));

    stellar_structure_BSE_given_timescales(newstar,
                                           oldstar,
                                           stardata,
                                           caller_id);

    newstar->tbgb = newstar->bse->timescales[T_BGB];
    newstar->tms = newstar->tm;
    new_SN_type = newstar->SN_type;

    stellar_structure_BSE_free_arrays(newstar,
                                      alloc);

    /*
     * Propagate SN type
     */
    newstar->SN_type = new_SN_type;

    /*
     * Derived variables
     */
    newstar->effective_radius =
        (newstar->roche_radius > TINY &&
         newstar->radius > newstar->roche_radius) ?
        Max(newstar->roche_radius,
            newstar->core_radius) : newstar->radius;

#ifdef XRAY_LUMINOSITY
    newstar->Xray_luminosity = Xray_luminosity(newstar);
#endif

#ifdef GIANT_CRICTICAL_Q_CHEN_HAN
    // save base of the giant branch luminosity and radius
    // as well as the A0 parameter
    if(Is_zero(newstar->A0) && (newstar->stellar_type>HERTZSPRUNG_GAP))
    {
        newstar->A0 = log10(newstar->radius);
    }
#endif // GIANT_CRICTICAL_Q_CHEN_HAN

    /*
     * Set the new structure in place : this also copies
     * event structures that have been allocated.
     */
    copy_star(stardata,
              newstar,
              oldstar);
    Dprint("return M = %30.20e, age = %g, stellar_type %d, phase_start_mass = %g, R = %30.20e : intermediate %d\n",
           oldstar->mass,
           oldstar->age,
           oldstar->stellar_type,
           oldstar->phase_start_mass,
           oldstar->radius,
           stardata->model.intermediate_step
        );

    _checks(stardata,oldstar);
    return 0;
}

static void _checks(struct stardata_t * const stardata,
                    struct star_t * const star)
{   /*
     * Physics checks
     */
    if(star->radius < 0.0 ||
       star->core_radius < 0.0 ||
       star->luminosity < 0.0 ||
       star->mass < 0.0 ||
       Outermost_core_mass(star) < 0.0)
    {
        Exit_binary_c(BINARY_C_ALGORITHM_OUT_OF_RANGE,
                      "Star %d, type %d at %p, has an unphysical property: M=%g Mc(outer)=%g R=%g Rc=%g L=%g\n",
                      star->starnum,
                      star->stellar_type,
                      (void*)star,
                      star->mass,
                      Outermost_core_mass(star),
                      star->radius,
                      star->core_radius,
                      star->luminosity);
    }
}

#endif//BSE
