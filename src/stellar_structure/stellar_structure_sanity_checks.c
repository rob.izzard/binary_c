#include "../binary_c.h"
No_empty_translation_unit_warning;

/*
 * Perform sanity checks on newstar to make sure
 * it is physical.
 *
 * You define the sanity checks in _SANITY_CHECKS.
 *
 * The error string is constructed for you automatically.
 */
void stellar_structure_sanity_checks(struct stardata_t * const stardata,
                                     struct star_t * const newstar)
{
    {
#define _SANITY_CHECKS                          \
        X(     radius,< 0.0)                    \
            X( luminosity,< 0.0)                \
            X(       mass,< 0.0)
#undef X
#define X(VAR,CHECK) _count += newstar->VAR CHECK;
        unsigned int _count = 0;
        _SANITY_CHECKS
            if(_count > 0)
            {
                char * _error = "error after computing structure: ";
#undef X
#define X(VAR,CHECK)                                \
                if(newstar->VAR CHECK)              \
                {                                   \
                    if(asprintf(&_error,            \
                                "%s%s = %g %s : ",  \
                                _error,             \
                                #VAR,               \
                                newstar->VAR,       \
                                #CHECK)>0){};       \
                }
                _SANITY_CHECKS;
#undef X
#define X(VAR,CHECK) newstar->VAR CHECK ? _errors[_i++] : "",
                Exit_binary_c(BINARY_C_UNPHYSICAL,
                              "%s",
                              _error);
#undef X
            }
    }
}
