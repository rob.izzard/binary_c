#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "wind.h"
/*
 * Reimers' mass loss rate, Msun / yr
 */
double Pure_function wind_Reimers(struct star_t * const star,
                                  const double eta)
{
    return
        eta * 4.0e-13 * star->radius * star->luminosity / star->mass;
}
