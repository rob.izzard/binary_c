#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "wind.h"

/*
 * Magnetic dynamo-driven wind from Tout and Pringle (1992)
 */

double Constant_function_if_no_debug wind_Tout_Pringle_1992(WIND_PROTOTYPE_ARGS)
{
    double mdot;
    if(Is_not_zero(stardata->preferences->Tout_Pringle_1992_multiplier))
    {
        /*
         * Tout's R* should be Renv, the convective envelope
         */
        const double Rstar = star->renv;

        if(Is_not_zero(Rstar))
        {
            /*
             * Convective turnover length (Hurley PhD Eq. B.21)
             */
            const double l = Rstar / 3.0;

            /*
             * eta = 3.0 * radius / convective turnover length
             * (end section 3)
             */
            const double eta = 3.0 * (Rstar / l);

            /*
             * f is defined just after Eq. 5.6
             */
            const double f =
                Is_zero(star->omega_crit) ? 0.0 :
                (star->omega / star->omega_crit);

            /*
             * Eq. 5.12
             */
            mdot =
                1.4e-6 *
                f *
                pow(star->luminosity/4.0, 2.0/3.0) *
                pow(eta/30.0, -2.0/3.0) *
                pow(star->mass, -1.0/6.0) *
                pow(Rstar/3.0, 1.0/6.0)
                *
                stardata->preferences->Tout_Pringle_1992_multiplier;
        }
        else
        {
            mdot = 0.0;
        }
    }
    else
    {
        mdot = 0.0;
    }
    return mdot;
}
