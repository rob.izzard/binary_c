#pragma once
#ifndef WIND_SCHNEIDER_H
#define WIND_SCHNEIDER_H

#define SCHNEIDER_CALL_ARGS                     \
    WIND_CALL_ARGS,                             \
        Zbase,                                  \
        debug,                                  \
        Tsurf

#define SCHNEIDER_PROTOTYPE_ARGS                \
    WIND_PROTOTYPE_ARGS,                        \
        const double Maybe_unused Zbase,        \
        const Boolean Maybe_unused debug,       \
        const double Maybe_unused Tsurf

static double eval_lowT_Schneider(SCHNEIDER_PROTOTYPE_ARGS);
static double eval_highT_Schneider(SCHNEIDER_PROTOTYPE_ARGS);
static double eval_Schneider_WR_wind(SCHNEIDER_PROTOTYPE_ARGS);
static double eval_Vink_wind(SCHNEIDER_PROTOTYPE_ARGS);
static double eval_Nieuwenhuijzen_wind(SCHNEIDER_PROTOTYPE_ARGS);
static double eval_LBV(SCHNEIDER_PROTOTYPE_ARGS);

#endif // WIND_SCHNEIDER_H
