#include "../binary_c.h"
No_empty_translation_unit_warning;
#include "wind.h"


double wind_postAGB(WIND_PROTOTYPE_ARGS)
{
    /*
     * Post-(A)GB mass loss rate
     */
    double mdot=0.0;

    if(star_is_postAGB(stardata, star) == TRUE)
    {
        if(stardata->preferences->postagbwind == POSTAGB_WIND_GIANT)
        {
            Exit_binary_c(BINARY_C_ALGORITHM_BRANCH_FAILURE,
                          "Despite wanting to use the giant-branch wind, for some reason wind_postAGB has been called. This is a bug.");
        }
        else if(stardata->preferences->postagbwind == POSTAGB_WIND_NONE)
        {
            /*
             * No wind
             */
            mdot = 0.0;
        }
        else if(stardata->preferences->postagbwind == POSTAGB_WIND_KRTICKA2020)
        {
            /*
             * Krticka, Kubát and Krticková (2020, A&A 635, A173)
             */
            mdot = wind_Krticka2020(WIND_CALL_ARGS);
        }
        else
        {
            Exit_binary_c(BINARY_C_ALGORITHM_BRANCH_FAILURE,
                          "Unknown post-AGB wind %d attempted.",
                          stardata->preferences->postagbwind);
        }
    }

    return mdot;
}
