#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "wind.h"

/*
 * Wind mass-loss rate for helium-rich WR stars
 * https://arxiv.org/abs/2009.01849
 */

double wind_Sander_and_Vink_2020(struct stardata_t * const stardata Maybe_unused,
                                 struct star_t * const star,
                                 const double Z,
                                 int method)
{
    double log_mdot = 1e-100;
    const double Zsolar = 0.014; /* "defined" in Table 1 */

    /* Z/Zsolar is limited by the range modelled */
    const double z = log10(Limit_range(Z/Zsolar,0.02,2.0));

    if(method == WIND_SANDER_VINK_2020_LUMINOSITY)
    {
        /* Eq. 18 */
        const double alpha = 0.32 * z + 1.40;

        /* Eq. 19 */
        const double logL0 = -0.87 * z + 5.06;
        const double L0 = exp10(logL0);

        /* Eq. 20 */
        const double logM10 = -0.75 * z - 4.06;
        const double M10 = exp10(logM10);

        /* Eq. 14 */
        log_mdot =
            M10 *
            pow(log10(star->luminosity) - logL0, alpha) *
            pow(star->luminosity/(10.0*L0),0.75);

    }
    else if(method == WIND_SANDER_VINK_2020_GAMMA)
    {
        /*
         * Use formulae as a function of Eddington factors, Gamma
         */
        //const double eta = mdot * vinf / (L/c);

        /* Eq. 4 */
        const double q_ion = 0.5;
        const double Gamma_e = pow(10.0,-4.51) * q_ion * star->luminosity/star->mass;

        /*
         * Eq. 5 (eta=1, at the onset of multiple scattering)
         * This is only for the start of WR winds, and is not general.
         * Thanks to Avishai Gilkis for pointing this out!
         */
        //const double Gamma_e = -0.300 * z + 0.236;

        /* Eq. 29 */
        const double a = 2.932;

        /* Eq. 30 */
        const double Gamma_e_b = -0.324 * z + 0.244;

        /* Eq. 31 d = log_Mdot_off */
        const double d = 0.23 * z - 2.61;

        /* Eq. 32 (note cbd == c) */
        const double c = -0.44 * z + 9.15;

        /* Eq. 28 */
        log_mdot =
            a * log10(-log10(1.0 - Gamma_e)) - log10(2.0) * pow(Gamma_e_b / Gamma_e, c) + d;

    }

    /*
     * Vink (2022) recommends using his 2017 rate as a lower limit
     *
     * https://arxiv.org/abs/2109.08164
     *
     * Although because we don't use this mass loss rate
     * below log(L/Lsun)=4.85, when we use Vink anyway, this
     * should not be a problem for us.
     *
     * Note that the calculation of z uses 0.02, while Vink's
     * prescription uses 0.019. This is a small difference, however,
     * and if it's your biggest uncertainty you can fix the code
     * to be more correct (good luck finding a problem in which this
     * is the case).
     */
    const double log_mdot_Vink_2017 =
        -13.3 + 1.36 * log10(star->luminosity) + 0.61 * z;

    log_mdot = Max(log_mdot, log_mdot_Vink_2017);

    /* hence the mass-loss rate */
    const double mdot = exp10(log_mdot);

    Nancheck(mdot);

    return mdot;
}
