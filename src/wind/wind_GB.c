#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "wind.h"

double Pure_function wind_GB(WIND_PROTOTYPE_ARGS)
{
    double mdot_GB=0.0;

    /*
     * "first giant branch" winds: AGB winds are dealt with in the AGB
     * function: all other potentially giant stars have this wind applied
     */
    const Boolean post_AGB =
        star_is_postAGB(stardata,star);

    if(post_AGB == TRUE &&
       stardata->preferences->postagbwind != POSTAGB_WIND_GIANT)
    {
        /*
         * Use post-AGB mass loss rate
         */
        mdot_GB = wind_postAGB(WIND_CALL_ARGS);
    }
    else if((stellar_type==HERTZSPRUNG_GAP)||
            (stellar_type==GIANT_BRANCH)||
            (stellar_type==CHeB)||
            (stellar_type>=HeMS))
    {

        if(stardata->preferences->PN_fast_wind &&
           stellar_type == GIANT_BRANCH &&
           mass - star->core_mass[CORE_He] < stardata->preferences->PN_fast_wind_dm_GB)
        {
            mdot_GB = stardata->preferences->PN_fast_wind_mdot_GB;
        }
        else
        {
            const double reimers=radius*luminosity/mass;
            if(stardata->preferences->gbwind==GB_WIND_REIMERS)
            {
                /* 'Reimers' mass loss */
                mdot_GB = stardata->preferences->gb_reimers_eta * 4.0e-13 * reimers;
            }
            else if(stardata->preferences->gbwind==GB_WIND_SCHROEDER_CUNTZ_2005)
            {
                /*
                 * Use Schroeder and Cuntz 2005 variant of Reimers
                 */

                /* gratio = g_sun / g_star */
                const double gratio = radius/sqrt(mass);
                const double teff = 1000.0*pow((1130.0*luminosity/Pow2(radius)),0.25);
                mdot_GB = stardata->preferences->gb_reimers_eta * 8e-14 * reimers * Pow3p5(teff/4000) * (1.0+gratio/4300.0);
            }
            else if(stardata->preferences->gbwind==GB_WIND_BEASOR_ETAL_2020)
            {
                /*
                 * Beasor et al. (2020) Eq.4
                 * https://arxiv.org/pdf/2001.07222.pdf
                 * https://ui.adsabs.harvard.edu/abs/arXiv:2001.07222
                 *
                 * Really only suitable for RSGs
                 */
                const double b = 4.8; /* 4.8 +- 0.6 */
                mdot_GB = exp10(-26.4 - 0.23 * star->phase_start_mass + b * log10(star->luminosity));
            }
            else if(stardata->preferences->gbwind==GB_WIND_ANTONIADIS_ETAL_2024)
            {
                const double logL = log10(luminosity);
                if(logL > 3.7)
                {
                    /*
                     * Antoniadis et al. (2024) Eq. 7
                     * https://arxiv.org/pdf/2401.15163.pdf
                     * note that 2.512e4 ~ 10^4.4
                     *
                     * We assume, by comparing to Fig.4,
                     * that this relation is only valid if
                     * logL > 3.7, i.e. L > 5e3 Lsun
                     */
                    double c1,c2,c3;
                    if(logL < 4.4)
                    {
                        c1 = 0.26; // +- 0.2
                        c2 = -14.19; // +- 4.48
                        c3 = -9.17; // +- 0.86
                    }
                    else
                    {
                        c1 = 2.5; // +- 0.12
                        c2 = -31.78; // +- 1.62
                        c3 = -17.47; // +- 0.57
                    }
                    const double _teff = Teff_from_luminosity_and_radius(luminosity, radius);
                    mdot_GB = exp10(c1 * logL * c2 * log10(_teff/4000.0) + c3);
                }
                else
                {
                    mdot_GB = stardata->preferences->gb_reimers_eta * 4.0e-13 * reimers;
                }
            }
            else if(stardata->preferences->gbwind==GB_WIND_GOLDMAN_ETAL_2017)
            {
                /*
                 * Goldman et al. (2017)
                 * https://ui.adsabs.harvard.edu/abs/2017MNRAS.465..403G/abstract
                 *
                 * Valid for "highly evolved AGB and red supergiant stars".
                 */

                /* gas to dust ratio "typically 200" */
                const double rgd = stardata->preferences->wind_gas_to_dust_ratio;
                /* pulsation period */
                const double P = Mira_pulsation_period(star->mass,
                                                       star->radius,
                                                       stardata);
                mdot_GB = 1.06e-5 * /* +3.5 -0.8 */
                    pow(star->luminosity * 1e-4, 0.9 /* +-0.1 */) *
                    pow(P/500.0, 0.75 /* +-0.3 */) *
                    pow(rgd/200.0, -0.03 /*+-0.07 */);
            }
        }
    }


    /*
     * do not allow a proto-WD core to be eaten into by the wind
     */
    mdot_GB = Max(0.0,
                  Min(mdot_GB,
                      ((star->mass - star->core_mass[CORE_He]) / stardata->model.dt)));

    mdot_GB *= stardata->preferences->wind_type_multiplier[WIND_TYPE_GB];

    return mdot_GB;
}
