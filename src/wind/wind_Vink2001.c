#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "wind.h"

/*
 * Vink 2001 mass-loss rate
 */

double wind_Vink2001(
    const struct stardata_t * const stardata,
    const struct star_t * const star
    )
{
    const double zz = stardata->common.effective_metallicity/VINK_SOLAR_METALLICITY;
    const double Teff = Teff_from_star_struct(star);
    const double vinf_div_vesc =
        2.6 * /* this is the hot side Galactic value */
        pow(zz,0.13); /* corrected for Z */
    return
        exp10(
            - 6.697
            + 2.194 * log10(star->luminosity/1e5)
            - 1.313 * log10(star->mass/30.0)
            - 1.226 * log10(vinf_div_vesc/2.0) // check this
            + 0.933 * log10(Teff/4.0e4) // check this
            - 10.92 * Pow2(log10(Teff/4.0e4)) // check this
            +  0.85 * log10(zz)
            );
}
