#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "wind.h"

void wind_multiplicative_factors(double *mdot,
                                 WIND_PROTOTYPE_ARGS)
{
    /* GB wind factor */
    mdot[MDOT_GB] *= stardata->preferences->gbwindfac;

    /* AGB wind factor */
    mdot[MDOT_AGB] *= star->stellar_type == EAGB ?
        stardata->preferences->eagbwindfac :
        stardata->preferences->tpagbwindfac;

    /* WR wind factor */
    mdot[MDOT_WR] *= stardata->preferences->wr_wind_fac;

}
