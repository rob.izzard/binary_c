#include "../binary_c.h"
No_empty_translation_unit_warning;
#include "wind.h"

/*
 * Wind velocity, returned in cm/s
 */
static double escape_wind_velocity(struct stardata_t * const stardata,
                                   struct star_t * const star);

double wind_velocity(struct stardata_t * const stardata,
                     struct star_t * const star)
{

    /*
     * Default to the escape velocity multiplied by vwind_beta
     * for stars that are not remnants
     */
    double vwind =
        POST_NUCLEAR_BURNING(star->stellar_type)
        ? 0.0
        : escape_wind_velocity(stardata,star);

    /*
     * Special cases
     */
    if((star->stellar_type == EAGB &&
        stardata->preferences->VW93_EAGB_wind_speed == TRUE)
       ||
       (star->stellar_type == TPAGB &&
        stardata->preferences->VW93_TPAGB_wind_speed == TRUE))
    {
        /*
         * CAB:
         *      Use the wind velocity calculated with fitting formulae (e.g.: VW93-K02 on the TPAGB)
         *      at the beginning of the TPAGB vwind might be 0, which can give problems in M_acc;
         *      If vwind=0, then put vwind = 5km/s, apprximately the minimum observed
         *      (see e.g. Vassiliadis&Wood, 1993)
         *      If you don't define this function, when you calculate the mass accretion rate
         *      you will use for vwind2 a fraction of escape velocity
         */
        const double p0 = Mira_pulsation_period(star->mass,
                                                star->radius,
                                                stardata);
        if(p0 > stardata->preferences->tpagb_superwind_mira_switchon)
        {
            /*
             * Switch on superwind and
             * limit wind velocity (cgs)
             */
            vwind = 1.0e5 * (-13.5 + 0.056 * stardata->preferences->tpagb_superwind_mira_switchon);
            Clamp(vwind,0.0,1.5e6);
        }
        else
        {
            vwind = Mira_wind_velocity(p0);
        }
        vwind = Max(5e5,vwind);
    }

    return vwind;
}

static double escape_wind_velocity(struct stardata_t * const stardata,
                                   struct star_t * const star)
{
    /*
     * Wind velocity ~ escape velocity
     *
     * beta = 0.125 by default
     */
    return
        sqrt(stardata->preferences->vwind_beta) *
        escape_velocity(star);
}
