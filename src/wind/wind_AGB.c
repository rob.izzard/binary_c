#include "../binary_c.h"
No_empty_translation_unit_warning;
#include "wind.h"

/*
 * Mass-loss rate for AGB stars
 * (EAGB and TPAGB)
 */

static double wind_EAGB(WIND_PROTOTYPE_ARGS);
static double wind_EAGB_BSE(WIND_PROTOTYPE_ARGS);
static double wind_TPAGB(WIND_PROTOTYPE_ARGS);

double wind_AGB(WIND_PROTOTYPE_ARGS)
{
    double mdot_AGB;
    const Boolean post_AGB =
        star_is_postAGB(stardata, star);

    if(post_AGB == TRUE &&
            stardata->preferences->postagbwind != POSTAGB_WIND_GIANT)
    {
        mdot_AGB = wind_postAGB(WIND_CALL_ARGS);
    }
    else if(stellar_type == EAGB)
    {
        mdot_AGB = wind_EAGB(WIND_CALL_ARGS);
    }
    else if(stellar_type == TPAGB)
    {
        mdot_AGB = wind_TPAGB(WIND_CALL_ARGS);
    }
    else
    {
        mdot_AGB = 0.0;
    }

    /*
     * Do not allow a proto-WD core to be eaten into by the
     * wind.
     *
     * We *do* allow the helium intershell layer to be
     * lost.
     */
    const double mdot_env = (star->mass - star->core_mass[CORE_CO]) / stardata->model.dt;

    return Limit_range(mdot_AGB,0.0,mdot_env);
}

static double wind_EAGB_BSE(WIND_PROTOTYPE_ARGS)
{
    /*
     * Vassiliadis and Wood, 1993, with Max(mass-2.5,0) term
     * as in VW93 and Hurley et al 2002
     */

    /* calculate Mira pulsation period */
    double p0 = -2.07 - 0.90 * log10(mass) + 1.940 * log10(radius);
    p0 = exp10(p0);
    p0 = Min(p0, 2000.0);

    /* hence mass loss rate */
    double mdot = -11.4 + 0.0125 * (p0 - 100.0 * Max(mass - 2.50, 0.0));
    mdot = exp10(mdot);
    mdot = Min(mdot, 1.36e-9 * luminosity); // steady superwind

    return mdot;
}

static double wind_EAGB(WIND_PROTOTYPE_ARGS)
{
    double mdot_EAGB;

    /* wind loss on the Early AGB */

    if(stardata->preferences->PN_fast_wind == TRUE
            &&
            mass - star->core_mass[CORE_He] < stardata->preferences->PN_fast_wind_dm_AGB)
    {
        mdot_EAGB = stardata->preferences->PN_fast_wind_mdot_AGB;
    }
    else
    {
        if(stardata->preferences->eagbwind == EAGB_WIND_BSE)
        {

            mdot_EAGB = wind_EAGB_BSE(WIND_CALL_ARGS);
        }
        else if(stardata->preferences->eagbwind == EAGB_WIND_BEASOR_ETAL_2020)
        {
            /*
             * Beasor et al. (2020) Eq.4
             * https://arxiv.org/pdf/2001.07222.pdf
             * https://ui.adsabs.harvard.edu/abs/arXiv:2001.07222
             */
            const double b = 4.8; /* 4.8 +- 0.6 */
            mdot_EAGB = exp10(-26.4 - 0.23 * star->phase_start_mass + b * log10(star->luminosity));
        }
        else if(stardata->preferences->eagbwind == EAGB_WIND_ANTONIADIS_ETAL_2024)
        {
            const double logL = log10(luminosity);
            if(logL > 3.7)
            {
                /*
                 * Antoniadis et al. (2024) Eq. 7
                 * https://arxiv.org/pdf/2401.15163.pdf
                 * note that 2.512e4 ~ 10^4.4
                 *
                 * We assume, by comparing to Fig.4,
                 * that this relation is only valid if
                 * logL > 3.7, i.e. L > 5e3 Lsun
                 */
                double c1,c2,c3;
                if(logL < 4.4)
                {
                    c1 = 0.26; // +- 0.2
                    c2 = -14.19; // +- 4.48
                    c3 = -9.17; // +- 0.86
                }
                else
                {
                    c1 = 2.5; // +- 0.12
                    c2 = -31.78; // +- 1.62
                    c3 = -17.47; // +- 0.57
                }
                const double _teff = Teff_from_luminosity_and_radius(luminosity, radius);
                mdot_EAGB = exp10(c1 * logL * c2 * log10(_teff/4000.0) + c3);
            }
            else
            {
                mdot_EAGB = wind_EAGB_BSE(WIND_CALL_ARGS);
            }
        }
        else if(stardata->preferences->eagbwind == EAGB_WIND_GOLDMAN_ETAL_2017)
        {
            /*
             * Goldman et al. (2017)
             * https://ui.adsabs.harvard.edu/abs/2017MNRAS.465..403G/abstract
             *
             * Valid for "highly evolved AGB and red supergiant stars".
             */

            /* gas to dust ratio "typically 200" */
            const double rgd = stardata->preferences->wind_gas_to_dust_ratio;
            /* pulsation period */
            const double P = Mira_pulsation_period(star->mass,
                                                   star->radius,
                                                   stardata);
            mdot_EAGB = 1.06e-5 * /* +3.5 -0.8 */
                        pow(star->luminosity * 1e-4, 0.9 /* +-0.1 */) *
                        pow(P / 500.0, 0.75 /* +-0.3 */) *
                        pow(rgd / 200.0, -0.03 /*+-0.07 */);
        }
        else
        {
            mdot_EAGB = 0.0;
        }
    }

    mdot_EAGB *= stardata->preferences->wind_type_multiplier[WIND_TYPE_AGB];

    return mdot_EAGB;
}

static double wind_TPAGB(WIND_PROTOTYPE_ARGS)
{
    double mdot_TPAGB;

    if(stardata->preferences->PN_fast_wind == TRUE &&
            mass - star->core_mass[CORE_He] < stardata->preferences->PN_fast_wind_dm_AGB)
    {
        mdot_TPAGB = stardata->preferences->PN_fast_wind_mdot_AGB;
    }
    else
    {
        if(stardata->preferences->tpagbwind < 2)
        {
            /*
             * Vassiliadis and Wood 1993
             *
             * type 0 is as Karakas et al. 2002
             * type 1 is as Hurley et al. 2002
             */

            /* calculate mira pulsation period */
            double p0 = Mira_pulsation_period(mass, radius, stardata);

            /* wind velocity in cgs FAR FROM THE STAR! */
            double vw = Mira_wind_velocity(p0);
            Dprint("Mira vw = %g from p0 = %g\n", vw, p0);

            if(stardata->preferences->tpagbwind == TPAGB_WIND_VW93_KARAKAS)
            {
                /*
                 * Karakas et al. 2002
                 * calculate mdot_TPAGB without limit on p0
                 * and without the mass correction
                 */
                mdot_TPAGB = -11.4 + 0.0125 * (p0); // without -100.0*Max((mt-2.50),0.0));
                mdot_TPAGB = exp10(mdot_TPAGB);

                if(p0 > stardata->preferences->tpagb_superwind_mira_switchon)
                {
                    /* Switch on superwind */
                    p0 = stardata->preferences->tpagb_superwind_mira_switchon;

                    /* limit wind velocity (cgs) */
                    vw = 1.0e5 * (-13.5 + 0.056 * p0);
                    Clamp(vw, 0.0, 1.5e6);

                    /* hence mass loss rate */
                    mdot_TPAGB = Min(mdot_TPAGB,
                                     (L_SUN / M_SUN) * (YEAR_LENGTH_IN_SECONDS / SPEED_OF_LIGHT)
                                     * (luminosity / vw));
                }
            }
            else
            {
                /* tpagbwind==1==TPAGB_WIND_VW93_ORIG
                 *
                 * : Original Vassiliadis and Wood 1993 */

                /* calculate mdot_TPAGB *with* mass correction */
                mdot_TPAGB = -11.4 + 0.0125 *
                             (p0 - 100.0 * Max(mass - 2.50, 0.0));
                mdot_TPAGB = exp10(mdot_TPAGB);
                /* superwind */
                mdot_TPAGB = Min(mdot_TPAGB, 1.36e-9 * luminosity);
            }

            star->vwind = vw;

            Dprint("WIND pmira=%12.12e l=%12.12e r=%12.12e L/vw=%12.12e m=%12.12e menv=%g Teff=%g -> mdot_TPAGB=%g\n",
                   p0,
                   luminosity,
                   radius,
                   (L_SUN / M_SUN) *
                   (YEAR_LENGTH_IN_SECONDS / SPEED_OF_LIGHT) *
                   (luminosity / vw),
                   mass,
                   mass - Outermost_core_mass(star),
                   Teff(star->starnum),
                   mdot_TPAGB);


#ifdef VW93_MULTIPLIER
            /* Allow VW93 rate to be modulated */
            mdot_TPAGB *= stardata->preferences->vw93_multiplier;
#endif

            Dprint("hence mdot_TPAGB=%g\n", mdot_TPAGB);

        }

        else if(stardata->preferences->tpagbwind == TPAGB_WIND_REIMERS)
        {
            /* Reimers mass loss on the TPAGB */
            mdot_TPAGB = stardata->preferences->tpagb_reimers_eta * 4.0E-13 * radius * luminosity / mass;
            Dprint("Reimers TPAGB %g\n", mdot_TPAGB);
        }
        else if(stardata->preferences->tpagbwind == TPAGB_WIND_BLOECKER)
        {
            /* Bloecker 1995 */
            mdot_TPAGB = 4.83e-9 * stardata->preferences->tpagb_reimers_eta *
                         pow(luminosity, 2.7) * pow(mass, -2.1) *
                         (4.0E-13 * radius * luminosity / mass);
        }
        else if(stardata->preferences->tpagbwind == TPAGB_WIND_VAN_LOON)
        {
            /* Van Loon 2005 */

            double l4 = log10(luminosity * 1e-4);
            double teff = 1000.0 * pow((1130.0 * luminosity / Pow2(radius)), 0.25);

            /* general formula from the abstract of his paper */
            //mdot_TPAGB=-5.65+1.05*l4-6.3*log10(teff/3500);

            /*
             * instead, use the red supergiant formula if we're
             * bright enough, otherwise use the AGB formula, neither
             * may be correct at low luminosity
             */
            if(l4 < 0.9)
            {
                /* AGB formula */
                mdot_TPAGB = -5.6 + 1.10 * l4 - 5.2 * log10(teff / 3500);
            }
            else
            {
                /* Red supergiant formula */
                mdot_TPAGB = -5.3 + 0.82 * l4 - 10.8 * log10(teff / 3500);
            }

            mdot_TPAGB = exp10(mdot_TPAGB);

            /*
             * Force small envelope to be lost when the AGB star
             * gets hot (i.e. approaches the WD track)
             *
             */
            if(teff > 4000) mdot_TPAGB = 1e-4;
        }
#ifdef NUCSYN
        else if(stardata->preferences->tpagbwind == TPAGB_WIND_ROB_CWIND)
        {
            /*
             * Strong wind for C-rich stars only
             */
            double r = (star->Xenv[XC12] / 12.0 +
                        star->Xenv[XC13] / 13.0) /
                       (star->Xenv[XO16] / 16.0 +
                        star->Xenv[XO17] / 17.0 +
                        star->Xenv[XO18] / 18.0);
            /*
              double rn=(star->Xenv[XC12]/12.0+
              star->Xenv[XC13]/13.0)/
              (star->Xenv[XN14]/14.0+
              star->Xenv[XN14]/15.0);
            */
            printf("M=%g Mc=%g C/O=%g\n", star->mass, star->core_mass[CORE_He], r);

            /*
             * We need more C than O, otherwise CO forms
             * and more C than N, otherwise CN forms
             * ASSUME THESE HAVE LOW OPACITY compared to C-C CCC etc.
             */
            if((r > 0.99)) //&&(rn>0.99)&&(star->num_thermal_pulses>10))
            {
                // nC/nO > 0.99
                mdot_TPAGB = 1e-4;
            }
            else
            {
                mdot_TPAGB = 1e-8;
            }
        }

        else if((stardata->preferences->tpagbwind == TPAGB_WIND_VW93_KARAKAS_CARBON_STARS) ||
                 (stardata->preferences->tpagbwind == TPAGB_WIND_VW93_ORIG_CARBON_STARS))
        {
            // deprecated!
            mdot_TPAGB = 0.0;
        }

#ifdef MATTSSON_MASS_LOSS
        else if(stardata->preferences->tpagbwind == TPAGB_WIND_MATTSSON)
        {
            // Lars Mattsson's wind
            Boolean fail = FALSE;
            double teff = 1000.0 * pow((1130.0 * luminosity / Pow2(radius)), 0.25);
            mdot_TPAGB = mattsson_massloss(mass,
                                           star->phase_start_mass,
                                           luminosity,
                                           teff,
                                           star->Xenv[XC12],
                                           star->Xenv[XC13],
                                           star->Xenv[XO16],
                                           stardata->preferences->zero_age.XZAMS[XH1],
                                           &fail);

        }
#endif // MATTSSON_MASS_LOSS
#endif // NUCSYN
        else if(stardata->preferences->tpagbwind == TPAGB_WIND_BEASOR_ETAL_2020)
        {
            /*
             * Beasor et al. (2020) Eq.4
             * https://arxiv.org/pdf/2001.07222.pdf
             * https://ui.adsabs.harvard.edu/abs/arXiv:2001.07222
             */
            const double b = 4.8; /* 4.8 +- 0.6 */
            mdot_TPAGB = exp10(-26.4 - 0.23 * star->phase_start_mass + b * log10(star->luminosity));
        }
        else if(stardata->preferences->tpagbwind == TPAGB_WIND_GOLDMAN_ETAL_2017)
        {
            /*
             * Goldman et al. (2017)
             * https://ui.adsabs.harvard.edu/abs/2017MNRAS.465..403G/abstract
             *
             * Valid for "highly evolved AGB and red supergiant stars".
             */

            /* gas to dust ratio "typically 200" */
            const double rgd = stardata->preferences->wind_gas_to_dust_ratio;
            /* pulsation period */
            const double P = Mira_pulsation_period(star->mass,
                                                   star->radius,
                                                   stardata);
            mdot_TPAGB = 1.06e-5 * /* +3.5 -0.8 */
                         pow(star->luminosity * 1e-4, 0.9 /* +-0.1 */) *
                         pow(P / 500.0, 0.75 /* +-0.3 */) *
                         pow(rgd / 200.0, -0.03 /*+-0.07 */);
        }
        else
        {
            mdot_TPAGB = 0.0;
        }
    }

    mdot_TPAGB *= stardata->preferences->wind_type_multiplier[WIND_TYPE_AGB];

    return mdot_TPAGB;
}
