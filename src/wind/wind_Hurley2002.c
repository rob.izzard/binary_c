#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "wind.h"
//#undef DEBUG
//#define DEBUG 1
double wind_Hurley2002(WIND_PROTOTYPE_ARGS)
{
    double mdot_wind;
    double mdot[MDOT_COUNT]; // different wind mass losses
    if(stellar_type<HeWD)
    {
        /* calculate contributions from different types of mass loss */
        mdot[MDOT_MS]  = 0.0;
        mdot[MDOT_GB] = wind_GB(WIND_CALL_ARGS);
        mdot[MDOT_AGB] = wind_AGB(WIND_CALL_ARGS);
        mdot[MDOT_WR] = wind_WR(WIND_CALL_ARGS);
        mdot[MDOT_OTHER]  = wind_other(WIND_CALL_ARGS);
        mdot[MDOT_LBV] = wind_LBV(WIND_CALL_ARGS);

        Dprint("mdot%d (premod): M=%g Mc=%g st=%s : MS=%d=%g GB=%d=%g AGB=%d=%g WR=%d=%g OTHER=%d=%g LBV=%d=%g : ",
               star->starnum,
               mass,
               Outermost_core_mass(star),
               Stellar_type_string(star->stellar_type),
               MDOT_MS,mdot[MDOT_MS],
               MDOT_GB,mdot[MDOT_GB],
               MDOT_AGB,mdot[MDOT_AGB],
               MDOT_WR,mdot[MDOT_WR],
               MDOT_OTHER,mdot[MDOT_OTHER],
               MDOT_LBV,mdot[MDOT_LBV]
            );

        /* apply aritificial multiplicative factors */
        wind_multiplicative_factors(mdot,WIND_CALL_ARGS);

        /* enhancement factors (CRAP, rotation, etc) */
        const double mult = wind_enhancement_factors(mdot,WIND_CALL_ARGS);

        /* choose the mass loss rate to use */
        mdot_wind = max_from_list(5,
                                  mdot[MDOT_MS],
                                  mdot[MDOT_GB],
                                  mdot[MDOT_AGB],
                                  mdot[MDOT_WR],
                                  mdot[MDOT_OTHER])
            + mdot[MDOT_LBV];
        Dprint("max from list %g\n",mdot_wind);

        /* use global multiplication factor (see above) */
        mdot_wind *= mult;
        Dprint("multiplied %g\n",mdot_wind);

        /* limit to the dynamical rate of the envelope */
        const double menv = envelope_mass(star);
        if(menv > TINY)
        {
            const double mdot_dyn =
                Max(0.1*star->mass, menv) /
                Max(1e-20,dynamical_timescale_envelope(star));
            mdot_wind = Min(mdot_wind,
                            mdot_dyn);
            Dprint("dynamical limit for Menv = %g - %g = %g : %g\n",
                   star->mass,
                   Outermost_core_mass(star),
                   menv,
                   mdot_wind);
        }
    }
    else
    {
        /*
         * no mass loss for remnants, except post-AGB stars
         */
        if(WHITE_DWARF(stellar_type))
        {
            mdot_wind = wind_WD(WIND_CALL_ARGS);
        }
        else
        {
            mdot_wind = 0.0;
        }
    }

    if(0 && star->starnum==0 && stardata->common.ndiscs > 0)
    {
        printf("DISC %30.20e %g %g %g %g %g %g\n",
               stardata->model.time,
               mdot_wind,
               mdot[MDOT_GB],
               mdot[MDOT_AGB],
               mdot[MDOT_WR],
               mdot[MDOT_OTHER],
               mdot[MDOT_LBV]
            );
    }
    return mdot_wind;
}
