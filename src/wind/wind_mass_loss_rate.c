#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "wind.h"

double wind_mass_loss_rate(WIND_PROTOTYPE_ARGS)
{
    /*
     * Calculate the wind mass loss rate from a star.
     *
     * mdot_wind contains the result, in Msun/year, which is
     * a positive number (and is returned).
     */
    double mdot_wind = 0.0; // result : returned

    if(no_wind(stardata) == FALSE)
    {
        switch(stardata->preferences->wind_mass_loss)
        {

        case WIND_ALGORITHM_NONE:
            /*
             * No wind - do nothing
             */
            mdot_wind = 0.0;
            break;

        case WIND_ALGORITHM_HURLEY2002:
            /*
             * Hurley et al (2002) prescription
             */
            mdot_wind = wind_Hurley2002(WIND_CALL_ARGS);
            break;

        case WIND_ALGORITHM_SCHNEIDER2018:

            /*
             * Fabian Schneider's 2018 routine
             * or the binary_c 2020/2022 update
             */
            mdot_wind = wind_Schneider(WIND_CALL_ARGS);
            break;

        case WIND_ALGORITHM_BINARY_C_2020:
            mdot_wind = wind_binary_c_2020(WIND_CALL_ARGS);
            break;

        case WIND_ALGORITHM_BINARY_C_2022:
            mdot_wind = wind_binary_c_2022(WIND_CALL_ARGS);
            break;

        default:
            Exit_binary_c(BINARY_C_ALGORITHM_OUT_OF_RANGE,
                          "Wind mass loss prescription %u is unknown.\n",
                          stardata->preferences->wind_mass_loss);
        }

        Dprint("mdot_wind = %g from prescription %u %s\n",
               mdot_wind,
               stardata->preferences->wind_mass_loss,
               Wind_algorithm_string(stardata->preferences->wind_mass_loss)
            );
    }


    /*
     * Apply Tout and Pringle (1992) magnetic wind
     */
    const double mdot_TP92 =  wind_Tout_Pringle_1992(WIND_CALL_ARGS);
    mdot_wind = Max(mdot_wind,mdot_TP92);

    /*
     * modulate by wind_multiplier_%d where %d is the stellar type
     */
    mdot_wind *= stardata->preferences->wind_multiplier[star->stellar_type];

    /*
     * GB and TPAGB stars should not strip into their helium
     */
    if(star->stellar_type == GIANT_BRANCH || star->stellar_type == TPAGB)
    {
        const double mc = star->core_mass[CORE_He] + stardata->model.dt*star->derivative[DERIVATIVE_STELLAR_He_CORE_MASS];
        const double menv = star->mass - mc;
        const double dm = -mdot_wind * stardata->model.dt;
        if(dm > menv)
        {
            const double f = menv/dm;
            mdot_wind *= f;
            Dprint("Reduce wind by %g to avoid core stripping\n",f);
        }
    }
    Dprint("Set mdot wind %g from mult %g M0 %g M %20.15e Mc %20.15e Menv %20.15e : max should be %g (equal? %s, dt = %g)\n",
           mdot_wind,
           stardata->preferences->wind_multiplier[star->stellar_type],
           star->phase_start_mass,
           star->mass,
           Outermost_core_mass(star),
           envelope_mass(star),
           envelope_mass(star)/stardata->model.dt,
           Yesno(Fequal(envelope_mass(star),star->mass)),
           stardata->model.dt);

    return mdot_wind;
}
