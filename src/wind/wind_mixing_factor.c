#include "../binary_c.h"
No_empty_translation_unit_warning;



/*
 * Function to choose the wind mixing factor f for the binary as a function of
 * the DONOR and ACCRETOR's wind speeds and mass loss rates.
 *
 *
 * Typical values:
 * 0 : RLOF, direct stream accretion, accretor has zero wind
 * 1 : All accreted from accretor's wind, donor has no effect
 */

/* Define donor and accretor */
#define ACCRETOR (k)
#define DONOR (Other_star(k))

double Pure_function wind_mixing_factor(const struct stardata_t * const stardata,
                                        const Star_number k)
{
    double f=0; /* the mixing factor */
    double v[NUMBER_OF_STARS]; /* Wind speeds */
    double vm[NUMBER_OF_STARS]; /* Wind momenta fluxes (speed * mass loss rates) */
    double fv; /* wind balance factor */
    double r; /* wind shock location */
    Star_number i; /* counter */

#ifdef MIXDEBUG
    printf("MIXING (F) in nucsyn_choose_wind_mixing_factor (donor=%d accretor=%d)\n",DONOR,ACCRETOR);
    printf("MIXING (F) MIXFAC star %d\n",ACCRETOR);
#endif

    Starloop(i)
    {
        /*
         * calculate wind velocities. these are actually the sqrt of half the
         * velocity but I Have left out a LOAD of constants that don't actually
         * matter
         */
//#warning Should use vwind here!
        //v[i]=sqrt(stardata->star[i].mass/stardata->star[i].radius);
        v[i]=stardata->star[i].vwind;

        /* Calculate mdot * wind velocity */
        vm[i]=v[i]*-stardata->star[i].derivative[DERIVATIVE_STELLAR_MASS_WIND_LOSS];
#ifdef MIXDEBUG
        printf("MIXING (F) MIXFAC Star %d : v=%g v*mdot=%g\n",i,v[i],vm[i]);
#endif
    }

    /* Apply momentum balance argument */
    if(vm[DONOR]>TINY)
    {
        /*
         * Both stars have a wind velocity (although this also deals with
         * vm[ACCRETOR]==0), we need to calculate the shock position and
         * compare it to the stellar radius of the accretor to see whether the
         * shock is inside or outside the star.
         */

        /* calculate fv */
        fv=sqrt(vm[ACCRETOR]/vm[DONOR]);

        /* calculate shock radius */
        r=stardata->common.orbit.separation*fv/(1+fv);

#ifdef MIXDEBUG
        printf("MIXING (F) MIXFAC fv=%g sep=%g shock radius r=%g c.f. stellar radius %g\n",fv,stardata->common.orbit.separation,r,stardata->star[ACCRETOR].radius);
#endif
        /*
         * If r<radius of accretor then set all accreted material from the
         * donor i.e. f=0 otherwise f=1
         */
        if(r<stardata->star[ACCRETOR].radius)
        {
#ifdef MIXDEBUG
            printf("MIXING (F) r<stellar radius so shock is inside star ->  f=0\n");
#endif
            f=0.0;
        }
        else
        {
#ifdef MIXDEBUG
            printf("MIXING (F) r>stellar radius so shock is outside star ->  f=1\n");
#endif
            f=1.0;
        }
    }
    else if(vm[ACCRETOR]>TINY) /* this implies vm[DONOR]<TINY */
    {
        /* Accretor's wind is > 0 but donor's wind is = 0 */
        /* So all accreted material must be from accretor */
        f=0.0;

#ifdef MIXDEBUG
        printf("MIXING (F) MIXFAC donor has no wind ??? wtf? \n");
#endif
    }
    else
    {
        /* No wind from either */
#ifdef MIXDEBUG
        printf("MIXING (F) MIXFAC neither star has a wind (ACCRETOR=%d, vm[ACCRETOR]=%g vm[DONOR]=%g)\n",ACCRETOR,vm[ACCRETOR],vm[DONOR]);
#endif
        /* don't care, set to zero to be er... well, at least a value that's not NaN or Inf */
        f=0;
    }



#ifdef MIXDEBUG
    /* debugging */
    printf("MIXING (F) MIXFAC f=%g -> ",f);
    if(Is_zero(f-1.0))
    {
        printf("all accretion onto star %d is infall \n",ACCRETOR);
    }
    else if(f<TINY)
    {
        printf("all accretion onto star %d is from donor (%d) wind\n", ACCRETOR,DONOR);
    }
    else
    {
        printf("an intermediate situation\n");
    }
#endif


    return f;
}
