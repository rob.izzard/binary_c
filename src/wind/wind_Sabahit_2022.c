#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "wind.h"

/*
 * Sabahit 2022 mass-loss rate
 */
double wind_Sabahit_2022(const struct stardata_t * const stardata,
                         const struct star_t * const star)
{
    const double zz = stardata->common.effective_metallicity/VINK_SOLAR_METALLICITY;
    const double Teff = Teff_from_star_struct(star);
    const double vinf_div_vesc =
        2.6 * /* this is the hot side Galactic value */
        pow(zz,0.13); /* corrected for Z */
    return
        exp10(
            -8.492
            + 4.77 * log10(star->luminosity/1e5)
            - 3.99 * log10(star->mass/30.0)
            - 1.226 * 2.0 * log10(vinf_div_vesc) * // CHECK THIS
            - 10.92 * 2.0 * log10(Teff/4.0e4) // CHECK THIS
            + 0.85*log10(zz)
            );
}
