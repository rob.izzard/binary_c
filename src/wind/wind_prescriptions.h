#pragma once
#ifndef WIND_PRESCRIPTIONS_H
#define WIND_PRESCRIPTIONS_H


#include "wind/wind_prescriptions.def"

/*
 * Different wind algorithms
 */
#undef X
#define X(CODE,STRING) WIND_ALGORITHM_##CODE ,
enum { WIND_ALGORITHMS_LIST };
#undef X
#define X(CODE,STRING) STRING ,
static const char * const wind_algorithm_strings[] =  { WIND_ALGORITHMS_LIST };

/*
 * Sander and Vink 2020 provide two
 * methods
 */
#undef X
#define X(CODE) WIND_SANDER_VINK_2020_##CODE,
enum { WIND_SANDER_VINK_2020_LIST };

/*
 * Wind type lists
 */
#undef X
#define X(CODE) MDOT_##CODE,
enum { WIND_MDOTS_LIST };

#undef X
#define X(CODE) WIND_TYPE_##CODE,
enum { WIND_MDOTS_LIST };

#undef X
#define X(CODE) #CODE,
static const char * const wind_mdots_strings[] = { WIND_MDOTS_LIST };

/*
 * Factors
 */
#undef X
#define X(CODE) MDOT_FAC_##CODE,
enum { WIND_FACTORS_LIST };


/*
 * Different Wolf-Rayet type winds.
 */
#undef X
#define X(CODE) WR_WIND_##CODE ,
enum { WR_WIND_PRESCRIPTIONS_LIST };

/*
 * Different first giant-branch winds.
 */
#undef X
#define X(CODE) GB_WIND_##CODE ,
enum { GB_WIND_PRESCRIPTIONS_LIST };

/* different EAGB winds */
#undef X
#define X(CODE) EAGB_WIND_##CODE ,
enum { EAGB_WIND_PRESCRIPTIONS_LIST };


/* different TPAGB winds */
#undef X
#define X(CODE) TPAGB_WIND_##CODE ,
enum { TPAGB_WIND_PRESCRIPTIONS_LIST };

/* different post-(A)GB winds */
#undef X
#define X(CODE) POSTAGB_WIND_##CODE ,
enum { POSTAGB_WIND_PRESCRIPTIONS_LIST };



/* rotationally enhanced mass-loss prescriptions */
#undef X
#define X(CODE) ROTATIONALLY_ENHANCED_MASSLOSS_##CODE ,
enum { ROTATIONALLY_ENHANCED_MASSLOSS_LIST };

/* wind angmom prescriptions */
#undef X
#define X(CODE) WIND_ANGMOM_LOSS_##CODE ,
enum { WIND_ANGMOM_LIST };

/* wind-RLOF methods */
#undef X
#define X(CODE) WRLOF_##CODE ,
enum { WRLOF_LIST };


#undef X

#endif // WIND_PRESCRIPTIONS_H
