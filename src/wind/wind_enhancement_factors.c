#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "wind.h"

static double rotational_factor(struct star_t *star,
                                unsigned int useme);

double wind_enhancement_factors(double *mdot,
                                WIND_PROTOTYPE_ARGS)
{
    /*
     * Wind enhancement factors:
     * 1) multiplies certain wind loss rates directly
     * 2) returns the global wind multiplier (which is applied later)]
     */

    double factor[MDOT_FAC_COUNT];

    /*
     * CRAP : Companion Reinforced Attrition Process
     *
     * Chris Tout's extra mass loss due to tidal effects in RS CVn binaries
     * Tout & Eggleton (1988)
     */
    factor[MDOT_FAC_CRAP] = 1.0;

    if(stardata->preferences->CRAP_parameter>TINY &&
       roche_lobe_radius>TINY)
    {
        factor[MDOT_FAC_CRAP] +=
            stardata->preferences->CRAP_parameter
            *Pow6(Min(0.50, radius/roche_lobe_radius));

        /* apply CRAP to giant winds only */
        Dprint("pre -CRAP1 GB=%g AGB=%g\n",mdot[MDOT_GB],mdot[MDOT_AGB]);
        mdot[MDOT_GB] *= factor[MDOT_FAC_CRAP];
        mdot[MDOT_AGB] *= factor[MDOT_FAC_CRAP];
        Dprint("post-CRAP1 (r=%g rl=%g CRAP_parameter=%g) GB=%g AGB=%g\n",radius,roche_lobe_radius,stardata->preferences->CRAP_parameter,mdot[MDOT_GB],mdot[MDOT_AGB]);
    }

    /* rotational enhancement */
    factor[MDOT_FAC_ROTATION] = rotational_factor(star,
                                                  stardata->preferences->rotationally_enhanced_mass_loss);

    Dprint("factors: CRAP=%g ROTATION=%g\n",
           factor[MDOT_FAC_CRAP],
           factor[MDOT_FAC_ROTATION]);

    /* calculate global wind-loss factor */
    double global_factor=1.0;

    global_factor *= factor[MDOT_FAC_ROTATION];

    return(global_factor);
}


static double rotational_factor(struct star_t *star,
                                unsigned int useme)
{
    /*
     * Rotationally enhanced mass loss factor
     */
    if(!((useme==ROTATIONALLY_ENHANCED_MASSLOSS_LANGER_FORMULA)||
         (useme==ROTATIONALLY_ENHANCED_MASSLOSS_LANGER_FORMULA_AND_ANGMOM))) return(1.0);
    double f;

    if(star->stellar_type<HeWD)
    {
        // Calculate wind factor
        const double xi=0.43;
        calculate_rotation_variables(star,star->radius);
        double vratio=Min(1.0-1e-8,star->v_eq_ratio);
        f=pow(1.0/(1.0-vratio),xi);
    }
    else
    {
        f=1.0; // WDs, NS, BH
    }
    return f;
}
