#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "wind.h"

/*
 * LBV-like mass loss beyond the Humphreys-Davidson limit.
 */

double Constant_function_if_no_debug wind_LBV(WIND_PROTOTYPE_ARGS)
{
    double mdot_LBV = 0.0;
    const double x = 1.0e-5 * radius * sqrt(luminosity) - 1.0;

    if(stellar_type >= HERTZSPRUNG_GAP &&
       luminosity > stardata->preferences->wind_LBV_luminosity_lower_limit &&
       x > 0.0)
    {
        mdot_LBV = 0.10 * Pow3(x) * (luminosity/stardata->preferences->wind_LBV_luminosity_lower_limit - 1.0);
    }
    else
    {
        mdot_LBV = 0.0;
    }

    Dprint("mdotLBV check stellar_type=%d radius=%g lum=%g hence x=(10^-5*R*sqrt(L)-1.0)=%g -> mdot_LBV=%g\n",
           stellar_type,radius,luminosity,x,mdot_LBV);

    mdot_LBV *= stardata->preferences->wind_type_multiplier[WIND_TYPE_LBV];

    /*
     * In some cases, the above will give crazy numbers
     * that evaporate the star: so don't lose > 10% of
     * total stellar mass per timestep.
     */
    mdot_LBV = Min(0.1*mass/stardata->model.dt,
                   mdot_LBV);

    return mdot_LBV;
}
