#pragma once
#ifndef BINARY_C_SOURCES_H
#define BINARY_C_SOURCES_H
/*
 * The binary_c stellar population nucleosynthesis framework.
 *
 * Copyright Robert Izzard 2000-2025
 *
 * Contact rob.izzard@gmail.com
 *
 * Code repository:
 * https://gitlab.com/binary_c
 *
 * Documentation is in doc/
 * Code is in src/
 *
 * To build, run
 *
 * ---
 *
 * meson setup builddir
 * cd builddir
 * ninja binary_c_install
 *
 * ---
 *
 * Please see the files README, LICENCE and CHANGES,
 * and the doc/ directory for documentation which is mirrored
 * at https://binary_c.gitlab.io
 *
 **********************
 *
 * Purpose:
 *
 * Header file to define nucleosynthesis sources in binary_c.
 *
 **********************
 */
/* source loop */
#define Sourceloop(N) for(Yield_source (N)=0;(N)<SOURCE_NUMBER;(N)++)

#include "binary_c_sources.def"

#undef X
#define X(CODE,STRING) SOURCE_##CODE,
enum { SOURCES_LIST() };
#undef X
#define X(CODE,STRING) STRING,
static const char * const source_strings[] Maybe_unused = { SOURCES_LIST() };
#undef X
#define Source_string(N) ((char*)source_strings[(N)])


#endif // BINARY_C_SOURCES_H
