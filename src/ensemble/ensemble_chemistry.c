#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef STELLAR_POPULATIONS_ENSEMBLE

#ifdef NUCSYN
void ensemble_chemistry(struct stardata_t * const stardata,
                        const double dtp,
                        const double T Maybe_unused,
                        const Stellar_type iprimary Maybe_unused,
                        const Boolean system_is_binary Maybe_unused)

{

    /*
     * Set stellar surface chemical abundances in the
     * ensemble
     *
     * To make this work, set the following arguments
     * to binary_c:
     *
     * ensemble True
     * ensemble_filter_CHEMISTRY True
     */

    /*
     * A list of element pairs to turn into square brackets
     */
    static const char * const bracket_pairs[][2] = {
        {"Fe", "H"},
        {"Ag", "Fe"},
        {"Al", "Fe"},
        {"Au", "Fe"},
        {"Ba", "Fe"},
        {"C", "Fe"},
        {"Ca", "Fe"},
        {"Cr", "Fe"},
        {"Cu", "Fe"},
        {"F", "Fe"},
        {"Li", "Fe"},
        {"K", "Fe"},
        {"Mg", "Fe"},
        {"N", "Fe"},
        {"Na", "Fe"},
        {"Ne", "Fe"},
        {"O", "Fe"},
        {"Pt", "Fe"},
        {"Sc", "Fe"},
        {"Si", "Fe"},
        {"Ti", "Fe"},
        {"V", "Fe"},
        {"Y", "Fe"},
        {"Zn", "Fe"}
    };

    /*
     * Values of the square brackets must be binned:
     * choose the bin width
     */
    static const double square_bracket_bin_width = 0.1;

    /*
     * Pointer to the Solar abundances
     */
    const double * const Xsolar = stardata->preferences->zero_age.Xsolar;

    /*
     * Loop over stars to make sure we output
     * the chemistry for each
     */
    Foreach_star(star)
    {
        /*
         * Here you choose stars based on some selection criteria,
         * whatever these may be.
         *
         * I have chosen stars that are up to and including
         * the (TP)AGB phase, so this should include all
         * hydrogen-burning stars.
         *
         * You may want to choose only main-sequence stars or
         * only red giants, and you can also choose stars based on
         * star->luminosity, star->mass, star->radius, etc.
         */
        if(star->stellar_type <= TPAGB)
        {
            /*
             * Get the surface abundances in the Xobs array
             */
            const double * const Xobs = nucsyn_observed_surface_abundances(star);

            for(size_t i=0; i<Array_size(bracket_pairs); i++)
            {
                /*
                 * Grab the numerator and denominator elements
                 * to go in the square bracket
                 */
                const char * const numerator_element = bracket_pairs[i][0];
                const char * const denominator_element = bracket_pairs[i][1];

                /*
                 * Compute the square bracket
                 */
                const double square_bracket_value = nucsyn_elemental_square_bracket(numerator_element,
                                                                                    denominator_element,
                                                                                    Xobs,
                                                                                    Xsolar,
                                                                                    stardata);

                /*
                 * Bin the square bracket value
                 */
                const double binned_square_bracket_value = Bin_data(square_bracket_value,
                                                                    square_bracket_bin_width);

                /*
                 * Make the square bracket string with asprintf
                 * (remember to free it if allocation works)
                 */
                char * square_bracket;
                if(asprintf(&square_bracket,
                            "[%s/%s]",
                            numerator_element,
                            denominator_element) > 0)
                {
                    /*
                     * Set the number of stars with this square bracket
                     * value in the stellar population ensemble which
                     * is output as JSON at the end.
                     */
                    Set_ensemble_count(
                        "chemistry distributions",

                        /* star number */
                        "star", star->starnum,

                        /* square bracket string and value */
                        square_bracket,binned_square_bracket_value
                        );

                    /*
                     * Free the square bracket string
                     */
                    Safe_free(square_bracket);
                }
            }
        }
    }

}
#endif // NUCSYN
#endif // STELLAR_POPULATIONS_ENSEMBLE
