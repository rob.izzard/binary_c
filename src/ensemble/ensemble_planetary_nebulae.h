#pragma once
#ifndef ENSEMBLE_PLANETARY_NEBULAE_H
#define ENSEMBLE_PLANETARY_NEBULAE_H

#define PN_NONE 0
#define PN_SINGLE_WIND 1
#define PN_BINARY_WIND 2
#define PN_MERGED 3
#define PN_COMENV_EJECTION 4

#define PN_string(N) (                                          \
        (N) == PN_NONE ? "None" :                               \
        (N) == PN_SINGLE_WIND ? "Single, wind"  :               \
        (N) == PN_BINARY_WIND ? "Binary, wind"  :               \
        (N) == PN_MERGED ? "Merger -> wind"  :    \
        (N) == PN_COMENV_EJECTION ? "Comenv ejection"           \
        : "Unknown" )

/*
 * Stars that can be the central stars of a PN
 */
#define Possibly_hot_star(K)                    \
    (ON_GIANT_BRANCH(K) || WHITE_DWARF(K))

#define Menv(N) (                                   \
        stardata->star[(N)].mass -                  \
        Outermost_core_mass(&stardata->star[(N)])   \
        )

#define Binary_PN(N)                            \
    (                                           \
        (N)==PN_BINARY_WIND ||                  \
        (N)==PN_COMENV_EJECTION                 \
        )
#define Single_PN(N) (!Binary_PN(N))

#define Wind_PN(N)                              \
    (                                           \
        (N) == PN_SINGLE_WIND ||                \
        (N) == PN_BINARY_WIND ||                \
        (N) == PN_MERGED                 \
        )
#define Comenv_PN(N)                            \
    (                                           \
        (N) == PN_MERGED ||              \
        (N) == PN_COMENV_EJECTION               \
        )


#endif // ENSEMBLE_PLANETARY_NEBULAE_H
