#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef STELLAR_POPULATIONS_ENSEMBLE

/*
 * Hertzsprung-Russell, logg-logL/logTeff , diagrams
 * counting pulsating stars.
 *
 * Use Set_ensemble_HRD() to add data to the ensemble in the
 * appropriate format. This adds to both HRD and HRD(t),
 * i.e. a time-integrated version (HRD) and a set of time
 * slices (HRD(t)).
 *
 * A similar macro, Set_ensemble_CMD(), adds colour-magnitude
 * a diagram.
 *
 * It also checks the ensemble filters, and adds the period-distributions
 * for each point, if required.
 */

#include "ensemble_HRD_macros.h"
#include "../pulsation/pulsation_macros.h"
static void ensemble_pulsator_generic(struct stardata_t * const stardata,
                                      struct star_t * star,
                                      const double dtp,
                                      const double T,
                                      const double log10_Porb_days_binned,
                                      const double a_binned,
                                      const Pulsator_ID type,
                                      const Boolean system_is_binary);

void ensemble_pulsators(struct stardata_t * const stardata,
                        const double dtp,
                        const double T,
                        const double log10_Porb_days_binned,
                        const double a_binned,
                        const Star_number iprimary,
                        const Boolean system_is_binary
#ifdef STELLAR_COLOURS
                        ,const Boolean CMD
#endif // STELLAR_COLOURS
    )
{

    /*
     * First, convert to either a single or binary pulsating system
     */
    Pulsator_ID ** ids_array = Calloc(stardata->preferences->zero_age.multiplicity,
                                      sizeof(Pulsator_ID *));

    Pulsator_ID n[NUMBER_OF_STARS] = {0};
    Foreach_evolving_star(star)
    {
        /*
         * HRD regions from Simon Jeffery
         */
        Pulsator_ID * ids;
        const double logTeff = log10(Teff(star->starnum));
        const double logL = log10(star->luminosity);
        id_pulsator(stardata,
                    logTeff,
                    logL,
                    &(n[star->starnum]),
                    &ids);
        ids_array[star->starnum] = ids;
    }

    for(Pulsator_ID k=0; k<stardata->store->num_pulsator_ellipses; k++)
    {
        /*
         * Which stars in this system are pulsator_ellipses of type k?
         */
        unsigned int count = 0;
        Boolean pulsator_of_this_type[NUMBER_OF_STARS] = {FALSE};
        Foreach_evolving_star(star)
        {
            const unsigned int N = n[star->starnum];
            const Pulsator_ID * const ids = ids_array[star->starnum];
            if(N > 0)
            {
                /*
                 * Star is a pulsator
                 */
                for(Pulsator_ID j=0; j<N; j++)
                {
                    if(ids[j] == k)
                    {
                        count++;
                        pulsator_of_this_type[star->starnum] = TRUE;
                        ensemble_pulsator_generic(stardata,
                                                  star,
                                                  dtp,
                                                  T,
                                                  log10_Porb_days_binned,
                                                  a_binned,
                                                  k,
                                                  system_is_binary);
                    }
                }
            }
        }

        if(count>0)
        {
            /*
             * Detect RR Lyrae stars
             */
            if(Use_ensemble(STELLAR_POPULATIONS_ENSEMBLE_FILTER_SCALARS) == TRUE &&
               k == PULSATOR_RR_LYRAE)
            {
                /*
                 * Count RR Lyrae
                 */
                Set_ensemble_count(
                    "scalars",
                    "ENSEMBLE_RRLyrae",
                    (double)T);
            }


            /*
             * Pulsator HRD
             */
            {
                char * prekey;
                if(asprintf(&prekey,
                            "pulsator %s",
                            stardata->store->pulsator_ellipses[k].name)>0)
                {
                    /*
                     * Find pulsating star or, if both are pulsating,
                     * use the primary as the trigger.
                     */
                    Star_number itrigger = -1;
                    if(count == 1)
                    {
                        struct star_t * star;
                        if(system_is_binary)
                        {
                            const Star_number j =
                                pulsator_of_this_type[0] == TRUE ? 0 : 1;
                            star = &(stardata->star[j]);
                        }
                        else
                        {
                            star = &(stardata->star[0]);
                        }
                        itrigger = star->starnum;
                    }
                    else if(count == 2)
                    {
                        itrigger = iprimary;
                    }
                    Boolean toggles[HRD_DATA_N_TOGGLES] = { FALSE };
                    toggles[HRD_DATA_TOGGLE_CORE_MASS_RATIO] = TRUE;
                    toggles[HRD_DATA_TOGGLE_FUNDAMENTAL_FREQUENCY] = TRUE;

                    ensemble_HRD(stardata,
                                 dtp,
                                 T,
                                 iprimary,
                                 itrigger,
                                 count == 2 ? TRUE : FALSE,
                                 system_is_binary,
#ifdef STELLAR_COLOURS
                                 CMD,
#endif // STELLAR_COLOURS
                                 prekey,
                                 toggles
                        );
                    Safe_free(prekey);
                }
            }


#ifdef __HRD_HERE
            /*
             * This code is deprecated: we should
             * now call the HRD code directly
             */
            const double logTeff_bin_width = 0.05;
            const double logL_bin_width = 0.05;
            const double logg_bin_width = 0.05;
            if(count==1)
            {
                /*
                 * One star is a pulsator
                 */
                struct star_t * star;
                if(system_is_binary)
                {
                    const Star_number j =
                        pulsator_of_this_type[0] == TRUE ? 0 : 1;
                    star = &(stardata->star[j]);
                }
                else
                {
                    star = &(stardata->star[0]);
                }
                const double logTeff = log10(Teff(star->starnum));
                const double logL = log10(star->luminosity);
                const double lg = logg(star);

                const double logL_binned = Bin_data(logL,
                                                    logL_bin_width);
                const double logTeff_binned = Bin_data(logTeff,
                                                       logTeff_bin_width);
                const double logg_binned = Bin_data(lg,
                                                    logg_bin_width);



                if(system_is_binary)
                {
                    Set_ensemble_count(
                        "pulsator HRD",(char*)(stardata->store->pulsator_ellipses[k].name),
                        "one in binary",
                        "time",(double)T,
                        "logTeff",logTeff_binned,
                        "logL",logL_binned
                        );
                    Set_ensemble_count(
                        "pulsator HRD",(char*)(stardata->store->pulsator_ellipses[k].name),
                        "one in binary",
                        "time",(double)T,
                        "logTeff",logTeff_binned,
                        "logg",logg_binned
                        );
                }
                else
                {
                    Set_ensemble_count(
                        "pulsator HRD",(char*)(stardata->store->pulsator_ellipses[k].name),
                        "one in single",
                        "time",(double)T,
                        "logTeff",logTeff_binned,
                        "logL",logL_binned
                        );
                    Set_ensemble_count(
                        "pulsator HRD",(char*)(stardata->store->pulsator_ellipses[k].name),
                        "one in single",
                        "time",(double)T,
                        "logTeff",logTeff_binned,
                        "logg",logg_binned
                        );
                }
            }
            else if(count==2)
            {
                /*
                 * Both stars are pulsator_ellipses
                 *
                 * first, log the unresolved binary
                 */
                {
                    double logTeff;
                    double lg;
                    double logL;
                    mean_HRD(stardata,&logTeff,&lg,&logL);
                    if(logTeff > 0.0)
                    {
                        const double logL_binned = Bin_data(logL,
                                                            logL_bin_width);
                        const double logTeff_binned = Bin_data(logTeff,
                                                               logTeff_bin_width);
                        const double logg_binned = Bin_data(lg,
                                                            logg_bin_width);

                        Set_ensemble_count(
                            "pulsator HRD",(char*)(stardata->store->pulsator_ellipses[k].name),
                            "two in binary unresolved",
                            "time",(double)T,
                            "logTeff",logTeff_binned,
                            "logL",logL_binned
                            );
                        Set_ensemble_count(
                            "pulsator HRD",(char*)(stardata->store->pulsator_ellipses[k].name),
                            "two in binary unresolved",
                            "time",(double)T,
                            "logTeff",logTeff_binned,
                            "logg",logg_binned
                            );
                    }
                }

                /*
                 * Then log the resolved stars
                 */
                Foreach_evolving_star(star)
                {
                    const double logTeff = log10(Teff(star->starnum));
                    const double logL = log10(star->luminosity);
                    const double lg = logg(star);
                    const double logL_binned = Bin_data(logL,
                                                        logL_bin_width);
                    const double logTeff_binned = Bin_data(logTeff,
                                                           logTeff_bin_width);
                    const double logg_binned = Bin_data(lg,
                                                        logg_bin_width);
                    Set_ensemble_count(
                        "pulsator HRD",(char*)(stardata->store->pulsator_ellipses[k].name),
                        "two in binary resolved",
                        "time",(double)T,
                        "logTeff",logTeff_binned,
                        "logL",logL_binned
                        );
                    Set_ensemble_count(
                        "pulsator HRD",(char*)(stardata->store->pulsator_ellipses[k].name),
                        "two in binary resolved",
                        "time",(double)T,
                        "logTeff",logTeff_binned,
                        "logg",logg_binned
                        );
                }
            }
#endif // __HRD_HERE
        }
    }
    Foreach_star(star)
    {
        Safe_free(ids_array[star->starnum]);
    }
    Safe_free(ids_array);
}

static void ensemble_pulsator_generic(struct stardata_t * const stardata,
                                      struct star_t * const star,
                                      const double dtp,
                                      const double T,
                                      const double log10_Porb_days_binned,
                                      const double a_binned,
                                      const Pulsator_ID type,
                                      const Boolean system_is_binary)
{

#undef X
#define X(MACRO,STRING,LOGTEFF,LOGL,A,B,P)      \
    STRING ,
    static char* pulsator_strings[] = { CSJ_PULSATOR_LIST };
#undef X
    /*
     * generic pulsator distributions
     */
    char * const pulsator = pulsator_strings[type];
    if(stardata->preferences->ensemble_alltimes == TRUE)
    {
        Set_ensemble_count(
            "distributions",
            "all times",
            pulsator,
            "log mass",Bin_data(Safelog10(star->mass),0.1)
            );
        Set_ensemble_count(
            "distributions",
            "all times",
            pulsator,
            "log luminosity",Bin_data(Safelog10(star->luminosity),0.1)
            );
        if(system_is_binary)
        {
            Set_ensemble_count(
                "distributions",
                "all times",
                pulsator,
                "log orbital period / days : label->dist",
                "all",
                "log orbital period / days",(double)log10_Porb_days_binned
                );
            Set_ensemble_count(
                "distributions",
                "all times",
                pulsator,
                "log orbital separation / Rsun : label->dist",
                "all",
                "log orbital period / Rsun",(double)a_binned
                );
        }
    }
    if(stardata->preferences->ensemble_notimebins == FALSE)
    {
        Set_ensemble_count(
            "distributions",
            T,
            pulsator,
            "log mass",Bin_data(Safelog10(star->mass),0.1)
            );
        Set_ensemble_count(
            "distributions",
            T,
            pulsator,
            "log luminosity",Bin_data(Safelog10(star->luminosity),0.1)
            );
        if(system_is_binary)
        {
            Set_ensemble_count(
                "distributions",
                T,
                pulsator,
                "log orbital period / days : label->dist",
                "all",
                "log orbital period / days",(double)log10_Porb_days_binned
                );
            Set_ensemble_count(
                "distributions",
                T,
                pulsator,
                "log orbital separation / Rsun : label->dist",
                "all",
                "log orbital period / Rsun",(double)a_binned
                );
        }
    }

}


#endif // STELLAR_POPULATIONS_ENSEMBLE
