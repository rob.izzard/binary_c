#pragma once
#ifndef ENSEMBLE_MACROS_H
#define ENSEMBLE_MACROS_H

/*
 * Macros for population ensemble output.
 */

#include "ensemble.def"
#undef X
#define X(CODE) STELLAR_POPULATIONS_ENSEMBLE_FILTER_##CODE,
enum { STELLAR_POPULATIONS_ENSEMBLE_FILTER_LIST };
#undef X
#define X(CODE) #CODE,
static char * ensemble_filters[] Maybe_unused = { STELLAR_POPULATIONS_ENSEMBLE_FILTER_LIST };
#undef X
#define X(CODE) ENSEMBLE_TYPE_##CODE,
enum { ENSEMBLE_TYPES_LIST };
#undef X


/*
 * Compress output, e.g. "0 0 0 0" becomes "4x0".
 */
#define COMPRESS_ENSEMBLE_OUTPUT

/*
 * Perhaps number ensemble items?  Useful for debugging.
 */
//#define NUMBER_ENSEMBLE_ITEMS

/* Maximum error in mass conservation */
#define MAXPCERR 10

/*
 * Significant digits used to round output
 * in the ensemble (e.g. output time, logTeff,
 * logg, logL etc.).
 */
#define ENSEMBLE_SIGNIFICANT_DIGITS 6

#define Ensemble_zeros(N)                       \
    {                                           \
        if(zerocount<3)                         \
        {                                       \
            if(zerocount==1)                    \
            {                                   \
                Ensemble_printf("0 ");          \
            }                                   \
            else if(zerocount==2)               \
            {                                   \
                Ensemble_printf("0 0 ");        \
            }                                   \
        }                                       \
        else                                    \
        {                                       \
            Ensemble_printf("%ux0 ",zerocount); \
        }                                       \
    }

/*
 * Given a linear time T, calculate the equvialent
 * ensemble time (which can be log or linear)
 */
#define Ensemble_time(T)                                \
    (stardata->preferences->ensemble_logtimes == TRUE ? \
     log10(Max(1e-50,(T))) :                            \
     (T))

/*
 * Given an ensemble time Y calculate a linear time
 */
#define Time_from_ensemble_time(Y)                      \
    (stardata->preferences->ensemble_logtimes == TRUE ? \
     exp10(Y) :                                         \
     (Y))

/*
 * What is the ensemble timestep? If we're using log times,
 * return the ensemble_logdt, if we're using linear times, return
 * the ensemble_dt
 */
#define Ensemble_timestep                               \
    (stardata->preferences->ensemble_logtimes == TRUE ? \
     stardata->preferences->ensemble_logdt :            \
     stardata->preferences->ensemble_dt)

/*
 * The maximum ensemble time. Usually this is the
 * actual maximum time, but it may be longer if you
 * want the final log timestep to be fully resolved.
 */
#define Ensemble_max_linear_time                                \
    (stardata->preferences->ensemble_enforce_maxtime ?          \
     stardata->preferences->max_evolution_time :                \
     exp10(                                                     \
         stardata->preferences->ensemble_logdt *                \
         (1.0+(int)(log10(stardata->model.max_evolution_time)/  \
                    stardata->preferences->ensemble_logdt))     \
         )                                                      \
        )

/*
 * Return the ensemble timestep in linear time.
 *
 * When converting from log to linear, we do this the
 * (very) slow but accurate way.
 */
#define Ensemble_timestep_linear                                        \
    (stardata->preferences->ensemble_logtimes == FALSE ?                \
     /* linear anyway, just use ensemble_dt */                          \
         stardata->preferences->ensemble_dt :                           \
                                                                        \
     /* convert log to linear */                                        \
         (                                                              \
             exp10(stardata->model.fixed_timesteps[FIXED_TIMESTEP_ENSEMBLE].previous_trigger + stardata->preferences->ensemble_logdt) \
             -                                                          \
             exp10(stardata->model.fixed_timesteps[FIXED_TIMESTEP_ENSEMBLE].previous_trigger) \
             )                                                          \
        )

#define Ensemble_start_time_linear                      \
    (stardata->preferences->ensemble_logtimes == TRUE ? \
     Time_from_ensemble_time(stardata->preferences->    \
                             ensemble_startlogtime) :   \
     0.0)

/*
 * Start of yielding in ensemble time units
 */
#define Ensemble_start_time                                 \
    (stardata->preferences->ensemble_logtimes == TRUE ?     \
     log10(stardata->preferences->ensemble_startlogtime) :  \
     0.0)

/*
 * Round the number X to calculate the next ensemble time.
 * NB we have to take care with negative numbers, hence the TINY.
 */
#define Round_ensemble_t(X)                     \
    (                                           \
        (X) > 0.0 ?                             \
        ((long int) (X)) :                      \
        (-(long int) (- (X) + TINY))            \
        )

/*
 * Given a linear time T, what is the next trigger time?
 *
 * To calculate this, we need the Ensemble_timestep_number, then add one
 * timestep to it. The timestep number starts at the yields start time.
 */

#define Ensemble_timestep_number(T)                 \
    Round_ensemble_t(                               \
        (Ensemble_time(T) - Ensemble_start_time)/   \
        Ensemble_timestep)

/* hence the next ensemble time (in ensemble time units) */
#define Next_ensemble_t(T)                      \
    (                                           \
        (                                       \
            1.0 +                               \
            1.0 * Ensemble_timestep_number(T)   \
            ) *                                 \
        Ensemble_timestep                       \
        +                                       \
        Ensemble_start_time                     \
        )

/*
 * And a generic version for stardata->model.time
 */
#define Next_ensemble_time Next_ensemble_t(stardata->model.time)

/*
 * Macro to determine if times are equal: the usual
 * threshold for equality is not quite lenient enough,
 * particularly when calculating to/from a logarithm.
 *
 * 1e-8 is usually ok.
 */
#define Ensemble_dt_threshold (1e-8)
#define Ensemble_times_equal(T0,T1)                 \
    (Float_same_within_eps((T0),                    \
                           (T1),                    \
                           Ensemble_dt_threshold))


#define ENSEMBLE_START_TIME 1e-14

#define ENSEMBLE_TEST_STAR 0

#define ENSEMBLE_MINIMUM_PMS_MASS 0.02




/*
 * Output macros for the ensemble
 */

/*
 * Macro to convert ensemble data to the appropriate
 * output format.
 */
#define Ensemble_data(K,X)                              \
    (                                                   \
        (double)                                        \
        (ensemble_type[(K)]==ENSEMBLE_TYPE_MAGNITUDE ?  \
         (Is_not_zero(X) ?                              \
          magnitude_from_luminosity(X) :                \
          DARK_STAR_MAGNITUDE) :                        \
         (X)                                            \
            )                                           \
        )



#ifdef NUMBER_ENSEMBLE_ITEMS
/*
 * Output but prepend ensemble item number
 */
#define __FORMAT__ "%u=%g "
#define Ensemble_print(K,X)                     \
    Printf(                                     \
        __FORMAT__,                             \
        (K),(X)                                 \
        );
#else
/*
 * The usual output: no prepend, just the number
 */
#define __FORMAT__ "%g "
#define Ensemble_print(K,X)                     \
    Printf(                                     \
        __FORMAT__,                             \
        (X)                                     \
        );
#endif // NUMBER_ENSEMBLE_ITEMS

#define Ensemble_printf(...) Printf(__VA_ARGS__)

/*
 * Ensemble cdict macros
 */
#include "ensemble_cdict_macros.h"


/*
 * Nester() and Nester_set() are wrappers for the
 * equivalent CDict_nest() and CDict_nest_set()
 * commands.
 */
#define Nester(...)     CDict_nest    (__VA_ARGS__)
#define Nester_set(...) CDict_nest_set(__VA_ARGS__)

#define Set_ensemble(DATA,X,...)                \
    if(Is_not_zero(X))                          \
    {                                           \
        Nester(                                 \
            stardata->model.ensemble_cdict,     \
            (DATA),                             \
            ((double)(X)),                      \
            __VA_ARGS__);                       \
    }


#define Set_ensemble(DATA,X,...)                \
    if(Is_not_zero(X))                          \
    {                                           \
        Nester(                                 \
            stardata->model.ensemble_cdict,     \
            (DATA),                             \
            ((double)(X)),                      \
            __VA_ARGS__);                       \
    }

/*
 * API for appending counts and rates : use these in ensemble_log
 */
#define Set_ensemble_count(...)                 \
    Nester(                                     \
        stardata->model.ensemble_cdict,         \
        (LAST(__VA_ARGS__)),                    \
        (dtp),                                  \
        EXCEPT_LAST(__VA_ARGS__))

#define Set_ensemble_rate(...)                  \
    Nester(                                     \
        stardata->model.ensemble_cdict,         \
        (LAST(__VA_ARGS__)),                    \
        (p),                                    \
        EXCEPT_LAST(__VA_ARGS__)                \
        )


/*
 * Set an ensemble value, without any
 * time or probability.
 */
#define Set_ensemble_value(...)                 \
    Nester_set(                                 \
        stardata->model.ensemble_cdict,         \
        (LAST_BUT1(__VA_ARGS__)),               \
        (LAST(__VA_ARGS__)),                    \
        EXCEPT_LAST2(__VA_ARGS__)               \
        )
#define Set_ensemble_append(...)                \
    Nester(                                     \
        stardata->model.ensemble_cdict,         \
        (LAST_BUT1(__VA_ARGS__)),               \
        (LAST(__VA_ARGS__)),                    \
        EXCEPT_LAST2(__VA_ARGS__)               \
        )


/*
 * To save mean values, you want to add up x*dtp
 *
 * The first argument is an cdict_entry_t *
 * which contain the denominator.
 */
#define Set_ensemble_mean_IMPL(COUNTER,                             \
                               DENOMINATOR,                         \
                               ...)                                 \
    {                                                               \
        struct cdict_entry_t * Concat(__entry,                      \
                                      COUNTER) =                    \
            Nester(                                                 \
                stardata->model.ensemble_cdict,                     \
                (LAST_BUT1(__VA_ARGS__)),                           \
                ((dtp) * (LAST(__VA_ARGS__))),                      \
                EXCEPT_LAST2(__VA_ARGS__)                           \
                );                                                  \
        Concat(__entry,COUNTER)->pre_output->data = (DENOMINATOR);  \
        Concat(__entry,COUNTER)->pre_output->function =             \
            ensemble_apply_denominator;                             \
    }
#define Set_ensemble_mean(DENOMINATOR,...)      \
    Set_ensemble_mean_IMPL(__COUNTER__,         \
                           DENOMINATOR,         \
                           __VA_ARGS__)

#define Set_ensemble_denominator(...)           \
    Nester(                                     \
        stardata->model.ensemble_cdict,         \
        (LAST(__VA_ARGS__)),                    \
        (dtp),                                  \
        EXCEPT_LAST(__VA_ARGS__)                \
        )


/*
 * A constant test rate of 1/Myr
 * (lineardt is measured in Myr)
 */
#define Set_ensemble_test_rate(...)             \
    Nester(                                     \
        stardata->model.ensemble_cdict,          \
        (LAST(__VA_ARGS__)),                    \
        (lineardt*p),                           \
        EXCEPT_LAST(__VA_ARGS__)                \
        );

/*
 * Dummy rates and counts: these just set 0.0
 * in a bin so the plotter knows the binwidth
 */
#define Set_dummy_count(...)                    \
    Nester(                                     \
        stardata->model.ensemble_cdict,          \
        (LAST(__VA_ARGS__)),                    \
        0.0,                                    \
        EXCEPT_LAST(__VA_ARGS__)                \
        );

#define Set_dummy_rate(...)                     \
    Nester(                                     \
        stardata->model.ensemble_cdict,          \
        (LAST(__VA_ARGS__)),                    \
        0.0,                                    \
        EXCEPT_LAST(__VA_ARGS__)                \
        );



#define Set_SN_stats_ensemble(SN_TYPE)                                  \
    if(system_is_binary == TRUE)                                        \
    {                                                                   \
        Set_ensemble_rate(                                              \
            "distributions",                                            \
            "supernova rotation rates",                                 \
            "SN type",(SN_TYPE),                                        \
            "star","primary",                                           \
            "stellar type","all",                                       \
            "v_eq",bin_data(star->v_eq,10.0)                            \
            );                                                          \
        Set_ensemble_rate(                                              \
            "distributions",                                            \
            "supernova rotation rates",                                 \
            "SN type",(SN_TYPE),                                        \
            "star","secondary",                                         \
            "stellar type","all",                                       \
            "v_eq",bin_data(other_star->v_eq,10.0)                      \
            );                                                          \
        Set_ensemble_rate(                                              \
            "distributions",                                            \
            "supernova rotation rates",                                 \
            "SN type",(SN_TYPE),                                        \
            "star","primary",                                           \
            "stellar type",                                             \
            (char*)Short_stellar_type_string(star->stellar_type),       \
            "v_eq",bin_data(star->v_eq,10.0)                            \
            );                                                          \
        Set_ensemble_rate(                                              \
            "distributions",                                            \
            "supernova rotation rates",                                 \
            "SN type",(SN_TYPE),                                        \
            "star","secondary",                                         \
            "stellar type",                                             \
            (char*)Short_stellar_type_string(other_star->stellar_type), \
            "v_eq",bin_data(other_star->v_eq,10.0)                      \
            );                                                          \
    }                                                                   \
    else                                                                \
    {                                                                   \
        Set_ensemble_rate(                                              \
            "distributions",                                            \
            "supernova rotation rates",                                 \
            "SN type",(SN_TYPE),                                        \
            "star","single",                                            \
            "stellar type","all"                                        \
            "v_eq",bin_data(star->v_eq,10.0)                            \
            );                                                          \
        Set_ensemble_rate(                                              \
            "distributions",                                            \
            "supernova rotation rates",                                 \
            "SN type",(SN_TYPE),                                        \
            "star","single",                                            \
            "stellar type",                                             \
            Short_stellar_type_string(star->stellar_type),              \
            "v_eq",bin_data(star->v_eq,10.0)                            \
            );                                                          \
    }

#define System_summed_property(X)               \
    (                                           \
        born_binary == FALSE ?                  \
        stardata->star[0].X :                   \
        (stardata->star[0].X +                  \
         stardata->star[1].X)                   \
        )

#define Luminosity_weighted_stellar_property(X)                         \
    (                                                                   \
    born_binary == FALSE ?                                              \
        (                                                               \
            stardata->star[0].stellar_type != MASSLESS_REMNANT ?        \
            stardata->star[0].X :                                       \
            stardata->star[1].X                                         \
            ) :                                                         \
        ((stardata->star[0].X * stardata->star[0].luminosity            \
          +                                                             \
          system_is_binary == FALSE ? 0.0 :                             \
          (stardata->star[1].X * stardata->star[1].luminosity))/        \
         system_luminosity)                                             \
        )



/*
 * Data should be binned to whatever binwidth
 * you want, but limit the number of significant digits
 * to prevent a large number of cdict keys.
 *
 * Note this is cast to a double: it's hard to see
 * why you'd want to bin any other data type. (If you
 * want ints, you can convert them to doubles, but I have
 * never had to bin an int...)
 */
#define Bin_data(value,binwidth)                    \
    (double)bin_data_sigfigs((value),               \
                             (binwidth),            \
                             ENSEMBLE_SIGNIFICANT_DIGITS)



/*
 * Use_ensemble checks:
 * 1) stardata->model.ensemble_cdict to make sure it exists
 * 2) stardata->preferences->ensemble to make sure it is TRUE
 * 3) then returns filter N.
 */
#define Use_ensemble(N) (                                       \
        stardata->model.ensemble_cdict == NULL ? FALSE :         \
        stardata->preferences->ensemble == FALSE ? FALSE :      \
        stardata->preferences->ensemble_filter[(N)]             \
        )

#define Set_spectral_type_ensemble_weight(TYPE)                         \
    {                                                                   \
        if(star->stellar_type>HERTZSPRUNG_GAP)                          \
        {                                                               \
            if(l>4.9)                                                   \
            {                                                           \
                ensemble_weight[ENSEMBLE_SPECTRAL_TYPE_##TYPE##_SUPERGIANT] = 1.0; \
            }                                                           \
            else                                                        \
            {                                                           \
                ensemble_weight[ENSEMBLE_SPECTRAL_TYPE_##TYPE##_GIANT] = 1.0; \
            }                                                           \
        }                                                               \
        else                                                            \
        {                                                               \
            ensemble_weight[ENSEMBLE_SPECTRAL_TYPE_##TYPE##_DWARF] = 1.0; \
        }                                                               \
        ensemble_weight[ENSEMBLE_SPECTRAL_TYPE_##TYPE] = 1.0;           \
        ensemble_weight[ENSEMBLE_SPECTRAL_TYPE_##TYPE##_LUM] =          \
            star->luminosity;                                           \
    }
#endif // ENSEMBLE_MACROS_H
