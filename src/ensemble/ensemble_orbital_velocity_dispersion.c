#include "../binary_c.h"
No_empty_translation_unit_warning;


/*
 * Log periods and eccentricities
 */

#ifdef STELLAR_POPULATIONS_ENSEMBLE

#include "ensemble.h"

void ensemble_orbital_velocity_dispersion(struct stardata_t * const stardata,
                                          const double dtp,
                                          const double T,
                                          const Boolean system_is_binary)
{
    /*
     * Orbital velocity dispersion computations
     */


    /*
     * log between 9 and 10 Gyr
     */
    const double Tmin = 9e3;
    const double Tmax = 10e3;

    if(In_range(T,Tmin,Tmax))
    {
        /*
         * bin widths
         */
        const double logvorb_binwidth = 0.1; // dex
        const double mass_binwidth = 0.05; // Msun
        const double q_binwidth = 0.05; // no unit
        const double logL_binwidth = 0.1; // dex

        /*
         * log10(orbital velocity)
         */
        const double logvorb = system_is_binary ? Safelog10(orbital_velocity(stardata)) : -10.0;

        Foreach_star(star,companion)
        {
            if(star->stellar_type != MASSLESS_REMNANT &&
               star->luminosity > 1e-3)
            {
                Set_ensemble_count(
                    "distributions",
                    "orbital velocity dispersion",
                    "age/Myr",(double)T,
                    "stellar type",Short_stellar_type_string(star->stellar_type),
                    "RLOF",Truefalse(system_is_binary && star->RLOFing),
                    "mass/Msun",Bin_data(star->mass,mass_binwidth),
                    "q",system_is_binary ? Bin_data(companion->mass/star->mass,q_binwidth) : -1.0,
                    "log10(vorb/km/s)",Bin_data(logvorb,logvorb_binwidth), // km/s
                    "log10(L/Lsun)",Bin_data(Safelog10(star->luminosity),logL_binwidth)
                    );
            }
        }
    }
}
#endif // STELLAR_POPULATIONS_ENSEMBLE
