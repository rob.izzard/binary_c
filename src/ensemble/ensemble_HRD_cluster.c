#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef STELLAR_POPULATIONS_ENSEMBLE

/*
 * 3D HRD hypercube for work with
 * Ted von Hippel
 */

#include "ensemble_HRD_macros.h"

void ensemble_HRD_cluster(struct stardata_t * const stardata,
                          const double p,
                          const double dtp,
                          const double T Maybe_unused,
                          const Star_number iprimary Maybe_unused,
                          const Star_number itrigger Maybe_unused,
                          const Boolean both_triggered Maybe_unused,
                          const Boolean system_is_binary,
#ifdef STELLAR_COLOURS
                          const Boolean CMD Maybe_unused,
#endif // STELLAR_COLOURS
                          char * prekey Maybe_unused,
                          const Boolean * const toggles Maybe_unused
    )
{
    /*
     * Compute logTeff-logg-logL hypercube location
     */
    const double logTeff_bin_width = 0.05;
    const double logL_bin_width = 0.05;
    const double logg_bin_width = 0.05;
    double mean_Teff = 0.0;
    double mean_g = 0.0;
    double Ltot = 0.0;

    Foreach_star(star)
    {
        if(star->stellar_type < NEUTRON_STAR)
        {
            const double Teff = Teff_from_star_struct(star);
            const double g = exp10(logg(star));
            Ltot += star->luminosity;
            mean_Teff += star->luminosity * Teff;
            mean_g += star->luminosity * g;
        }
    }

    Ltot = Max(Ltot,1e-20);
    mean_Teff /= Ltot;
    mean_g /= Ltot;

    const double mean_logTeff_binned = Bin_data(Safelog10(mean_Teff),
                                                logTeff_bin_width);
    const double mean_logL_binned = Bin_data(Safelog10(Ltot),
                                             logL_bin_width);
    const double mean_logg_binned = Bin_data(Safelog10(mean_g),
                                             logg_bin_width);

    /*
     * Initial mass distribution
     */
    if(stardata->model.model_number == 0)
    {
        Set_ensemble_rate(
            mean_logTeff_binned,
            mean_logg_binned,
            mean_logL_binned,
            "M0i",
            stardata->preferences->zero_age.mass[0]
            );
    }

    /*
     * Current primary mass distribution
     */
    Set_ensemble_count(
        mean_logTeff_binned,
        mean_logg_binned,
        mean_logL_binned,
        "M0",
        stardata->star[iprimary].mass
        );

    {
        /*
         * binary statistics
         */
        const double q_binwidth = 0.05;
        const double q_binned =
            system_is_binary ?
            Bin_data(Min(stardata->star[0].q,
                         stardata->star[1].q),
                     q_binwidth) :
            -1.0;
        Set_ensemble_count(
            mean_logTeff_binned,
            mean_logg_binned,
            mean_logL_binned,
            "q",
            q_binned
            );

        const double Porb_binwidth = 0.1;
        const double Porb_binned =
            system_is_binary ?
            Bin_data(Safelog10(Max(1e-6,Min(1e10,stardata->common.orbit.period))),
                     Porb_binwidth) :
            -50.0;
        Set_ensemble_count(
            mean_logTeff_binned,
            mean_logg_binned,
            mean_logL_binned,
            "P",
            Porb_binned
            );
    }

    /*
     * Stellar-type matrix
     */
    Set_ensemble_count(
        mean_logTeff_binned,
        mean_logg_binned,
        mean_logL_binned,
        "types",
        stardata->star[0].stellar_type,
        stardata->star[1].stellar_type
        );

}
#endif // STELLAR_POPULATIONS_ENSEMBLE
