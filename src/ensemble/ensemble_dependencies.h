#pragma once
#ifndef ENSEMBLE_DEPENDENCIES_H
#define ENSEMBLE_DEPENDENCIES_H

/*
 * Features "required" for the population ensemble
 */

#ifndef XRAY_BINARIES
#define XRAY_BINARIES
#endif
#ifndef XRAY_LUMINOSITY
#define XRAY_LUMINOSITY
#endif

#endif // ENSEMBLE_DEPENDENCIES_H
