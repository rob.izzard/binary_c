#include "../binary_c.h"
No_empty_translation_unit_warning;

/*
 * Log Be-XRBs to the ensemble according to Boyuan Liu's
 * prescriptions.
 */

#ifdef STELLAR_POPULATIONS_ENSEMBLE

#include "ensemble.h"

void ensemble_BeXRBs(struct stardata_t * const stardata,
                     const double dtp Maybe_unused,
                     const Boolean system_is_binary)
{
#undef X
#define X(TYPE) #TYPE,
    static const char * type_strings[] = {
        Be_XRAY_TYPES_LIST
    };
#undef X
    if(system_is_binary == TRUE)
    {
        const double logP = Bin_data(log10(stardata->common.orbit.period * YEAR_LENGTH_IN_DAYS),0.1);
        const double ee = Bin_data(stardata->common.orbit.eccentricity, 0.05);

        Foreach_star(donor,accretor)
        {
            if(cs_Be_criterion(stardata,
                               donor,
                               accretor) == TRUE
               &&
               cs_XRB_duty_cycle_criterion(stardata,
                                           donor,
                                           accretor) == TRUE)
            {
                const int Xtype = cs_Be_XRB_type(stardata, donor, accretor);
                const char * const type = type_strings[Xtype];
                const double log_Mdonor = (double)Bin_data(log10(donor->mass),0.1);
                const double log_Maccretor = (double)Bin_data(log10(accretor->mass),0.1);
                const double logLX = Bin_data(log10(cs_LX_Be(stardata,accretor)),0.1);
                const char * output_types[2] = {
                    type,
                    "all"
                };

                for(int j=0; j<2; j++)
                {
                    const char * output_type = output_types[j];
                    Set_ensemble_count(
                        "distributions",
                        "BeXRB",
                        "type",(char*)output_type,
                        "count",1.0
                        );
                    Set_ensemble_count(
                        "distributions",
                        "BeXRB",
                        "type",(char*)output_type,
                        "log10(Mdonor/Msun)",log_Mdonor
                        );
                    Set_ensemble_count(
                        "distributions",
                        "BeXRB",
                        "type",(char*)output_type,
                        "log10(Maccretor/Msun)",log_Maccretor
                        );
                    Set_ensemble_count(
                        "distributions",
                        "BeXRB",
                        "type",(char*)output_type,
                        "log10(Orbital Period/d)",logP
                        );
                    Set_ensemble_count(
                        "distributions",
                        "BeXRB",
                        "type",(char*)output_type,
                        "eccentricity",ee
                        );
                    Set_ensemble_count(
                        "distributions",
                        "BeXRB",
                        "type",(char*)output_type,
                        "log10(Orbital Period/d) - eccentricity",
                        logP,
                        ee
                        );
                    Set_ensemble_count(
                        "distributions",
                        "BeXRB",
                        "type",(char*)output_type,
                        "log10(mdonor) - log10(maccetor) - log10(Orbital Period/d) - eccentricity - log10(LX)",
                        log_Mdonor,
                        log_Maccretor,
                        logP,
                        ee,
                        logLX
                        );
                    Set_ensemble_count(
                        "distributions",
                        "BeXRB",
                        "type",(char*)output_type,
                        "log10(LX)",logLX
                        );
                    Set_ensemble_count(
                        "distributions",
                        "BeXRB",
                        "type",(char*)output_type,
                        "logP - log10(LX)",
                        logP,
                        logLX
                        );

                }
            }
        }
    }
}
#endif // STELLAR_POPULATIONS_ENSEMBLE
