#include "../binary_c.h"
No_empty_translation_unit_warning;

void ensemble_comenv(struct stardata_t * const stardata)
{
    /*
     * Detect common-envelope outcomes and log them
     * in the ensemble
     */
    static char * header = "common envelopes";

    /*
     * Bin widths
     */
    static const double dm = 0.05; // M
    static const double dmc = 0.05; // Mc
    static const double dq = 0.05; // q = Mdonor/Maccretor
    static const double dqc = 0.05; // qc = Mc/M
    static const double dlog10P = 0.1; // log10(orbital period)
    static const double dlogTeff = 0.1; // effective temperature
    static const double dlogL = 0.1; // luminosity

    if(stardata->model.comenv_count >
       stardata->previous_stardata->model.comenv_count)
    {

        /*
         * New common-envelope event
         */
        const double p = stardata->model.probability;
        const Time T = ensemble_output_time(stardata);
        const Star_number donor_n = stardata->model.comenv_overflower;
        const Star_number accretor_n = Other_star(donor_n);
        struct star_t * const donor = &stardata->star[donor_n];
        struct star_t * const prev_donor = &stardata->previous_stardata->star[donor_n];
        struct star_t * const accretor = &stardata->star[accretor_n];
        struct star_t * const prev_accretor = &stardata->previous_stardata->star[accretor_n];
        const Boolean post_binary = _System_is_binary(stardata);
        const Boolean merged = ! post_binary;

        /*
         * Binned data
         */
        const double M_pre_donor = Bin_data(prev_donor->mass,dm);
        const double Mc_pre_donor = Bin_data(Outermost_core_mass(prev_donor),dmc);
        const double M_pre_accretor = Bin_data(prev_accretor->mass,dm);
        const double Mc_pre_accretor = Bin_data(Outermost_core_mass(prev_accretor),dmc);
        const double q_pre = Bin_data(prev_donor->mass / prev_accretor->mass,dqc);
        const double log10P_pre = Bin_data(Safelog10(stardata->previous_stardata->common.orbit.period),dlog10P);
        const double logTeff_pre_donor = Bin_data(Safelog10(Teff_from_star_struct(prev_donor)),dlogTeff);
        const double logL_pre_donor = Bin_data(Safelog10(prev_donor->luminosity),dlogL);

        /*
         * 1D histograms of masses, core masses, mass ratios,
         * stellar types of donor and accretor
         */
        Set_ensemble_rate(
            header,
            "pre",
            "log10(orbital period)",
            log10P_pre
            );
        Set_ensemble_rate(
            header,
            "pre",
            "T",
            T
            );
        Set_ensemble_rate(
            header,
            "pre",
            "donor mass",
            M_pre_donor
            );
        Set_ensemble_rate(
            header,
            "pre",
            "donor core mass",
            Mc_pre_donor
            );
        Set_ensemble_rate(
            header,
            "pre",
            "accretor mass",
            M_pre_accretor
            );
        Set_ensemble_rate(
            header,
            "pre",
            "accretor core mass",
            Mc_pre_accretor
            );
        Set_ensemble_rate(
            header,
            "pre",
            "mass ratio",
            q_pre
            );
        Set_ensemble_rate(
            header,
            "pre",
            "stellar type donor",
            prev_donor->stellar_type
            );
        Set_ensemble_rate(
            header,
            "pre",
            "stellar type accretor",
            prev_accretor->stellar_type
            );

        /*
         * 2D Maps
         */
        Set_ensemble_rate(
            header,
            "pre",
            "M vs Mc donor : label->map",
            "all",
            "M",M_pre_donor,
            "Mc",Mc_pre_donor
            );
        Set_ensemble_rate(
            header,
            "pre",
            "M vs Mc accretor : label->map",
            "all",
            "M",M_pre_accretor,
            "Mc",Mc_pre_accretor
            );
        Set_ensemble_rate(
            header,
            "pre",
            "HRD : label->map",
            "all",
            "logTeff",logTeff_pre_donor,
            "logL",logL_pre_donor
            );

        if(merged)
        {
            /*
             * Merged binaries
             */
            struct star_t * const star =
                stardata->star[0].stellar_type!=MASSLESS_REMNANT ?
                &stardata->star[0] :
                &stardata->star[1];
            const double M = Bin_data(star->mass,dm);
            const double Mc = Bin_data(Outermost_core_mass(star),dmc);
            const double core_mass_ratio = Bin_data(Outermost_core_mass(star)/star->mass,dqc);
            Set_ensemble_rate(
                header,
                "post, single",
                "M vs Mc : label->map",
                "all",
                "M",M,
                "Mc",Mc);
            Set_ensemble_rate(
                header,
                "post, single",
                "stellar type",
                star->stellar_type
                );
            Set_ensemble_rate(
                header,
                "post, single",
                "core mass ratio",
                core_mass_ratio
                );
            Set_ensemble_rate(
                header,
                "post, single",
                "mass",
                M
                );
            Set_ensemble_rate(
                header,
                "post, single",
                "core mass",
                Mc
                );
        }
        else
        {
            /*
             * Surviving binaries
             */
            const double log10P_post = Bin_data(Safelog10(stardata->common.orbit.period),dlog10P);
            const double q_post = Bin_data(donor->mass / accretor->mass,dq);
            const double M_post_donor = Bin_data(donor->mass,dm);
            const double Mc_post_donor = Bin_data(Outermost_core_mass(donor),dmc);
            const double core_mass_ratio_post_donor = Bin_data(Outermost_core_mass(donor)/donor->mass,dqc);
            const double M_post_accretor = Bin_data(accretor->mass,dm);
            const double Mc_post_accretor = Bin_data(Outermost_core_mass(accretor),dmc);
            const double core_mass_ratio_post_accretor = Bin_data(Outermost_core_mass(accretor)/accretor->mass,dqc);

            Set_ensemble_rate(
                header,
                "post, binary",
                "donor mass",
                M_post_donor
                );
            Set_ensemble_rate(
                header,
                "post, binary",
                "donor core mass",
                Mc_post_donor
                );
            Set_ensemble_rate(
                header,
                "post, binary",
                "donor core mass ratio",
                core_mass_ratio_post_donor
                );
            Set_ensemble_rate(
                header,
                "post, binary",
                "donor core mass ratio",
                core_mass_ratio_post_accretor
                );
            Set_ensemble_rate(
                header,
                "post, binary",
                "accretor mass",
                M_post_accretor
                );
            Set_ensemble_rate(
                header,
                "post, binary",
                "accretor core mass",
                Mc_post_accretor
                );
            Set_ensemble_rate(
                header,
                "post, binary",
                "mass ratio",
                q_post
                );
            Set_ensemble_rate(
                header,
                "post, binary",
                "log10P",
                log10P_post
                );
            Set_ensemble_rate(
                header,
                "post, binary",
                "donor stellar type",
                donor->stellar_type
                );
            Set_ensemble_rate(
                header,
                "post, binary",
                "accretor stellar type",
                accretor->stellar_type
                );
        }
    }
}
