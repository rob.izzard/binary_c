#include "../binary_c.h"
No_empty_translation_unit_warning;

/*
 * Log (C,N)EMP stars in the population ensemble
 */

#ifdef STELLAR_POPULATIONS_ENSEMBLE

#include "ensemble.h"

void ensemble_EMP(struct stardata_t * const stardata Maybe_unused,
                  const Boolean is_binary Maybe_unused,
                  const double dtp Maybe_unused,
                  const struct star_t * const star Maybe_unused,
                  const Abundance * const Xsurf Maybe_unused,
                  const double T Maybe_unused,
                  const double Porb_binned Maybe_unused)
{
    /*
     * EMP stars : must be "giants" (logg < EMP_logg_maximum)
     *             sufficiently old (age > EMP_minimum_age)
     *             sufficiently massive (mass > 0.07Msun)
     *
     * We distinguish between EMP and its subsets:
     * CEMP, CNEMP, NEMP and XEMP
     */
#ifdef NUCSYN
    struct empdata_t {
        char * label; /* "EMP", "CEMP", "NEMP" etc. */
        char * element_top; /* e.g. "C" or "N" */
        char * element_bottom; /* usually "Fe" */
        char * scalar_label; /* ENSEMBLE_EMP etc. */
        Abundance ratio; /* e.g. [C/Fe] or [N/Fe] */
        Abundance binned; /* ratio binned to nearest 0.1 dex */
        Boolean is_type; /* TRUE if this type */
        Boolean use_ratio; /* if TRUE we use the ratio to make plots */
    };

    const char * multiplicity_string = is_binary ? "binary" : "single";

    const double lg = logg(star);

    const Boolean can_be_EMP =
        star->stellar_type < HeWD &&
        star->mass > MAXIMUM_BROWN_DWARF_MASS &&
        stardata->model.time > stardata->preferences->EMP_minimum_age &&
        lg < stardata->preferences->EMP_logg_maximum;

    if(can_be_EMP == TRUE)
    {

        /* shortcut for square brackets */
#define Sq(TOP,BOTTOM)                              \
        nucsyn_elemental_square_bracket(            \
            TOP,                                    \
            BOTTOM,                                 \
            Xsurf,                                  \
            stardata->preferences->zero_age.Xsolar, \
            stardata)

#define EMPDATA_LIST                                                    \
        /* EMP */                                                       \
            X(EMP,                                                      \
              Fe, H,                                                    \
              < -2.0,                                                   \
              TRUE)                                                     \
                                                                        \
                /* CNEMP */                                             \
                X(CNEMP,                                                \
                  N, Fe,                                                \
                  > stardata->preferences->NEMP_nfe_minimum &&          \
                  Sq("C","Fe") > stardata->preferences->CEMP_cfe_minimum, \
                  TRUE)                                                 \
                                                                        \
                /* CEMP */                                              \
                X(CEMP,                                                 \
                  C, Fe,                                                \
                  > stardata->preferences->CEMP_cfe_minimum &&          \
                  empdata[CNEMP].is_type == FALSE,                      \
                  TRUE)                                                 \
                                                                        \
                /* NEMP */                                              \
                X(NEMP,                                                 \
                  N, Fe,                                                \
                  > stardata->preferences->NEMP_nfe_minimum &&          \
                  empdata[CNEMP].is_type == FALSE &&                    \
                  empdata[CEMP].is_type == FALSE,                       \
                  TRUE)                                                 \
                                                                        \
                /* exclusive EMP */                                     \
                X(XEMP,                                                 \
                  0, 0,                                                 \
                  == 0.0 &&                                             \
                  empdata[CEMP].is_type == FALSE &&                     \
                  empdata[CNEMP].is_type == FALSE &&                    \
                  empdata[NEMP].is_type == FALSE,                       \
                  TRUE)

#undef X
        /*
         * enumerated type list to give us EMP, CEMP, NEMP
         * which can be used as indices
         */
#define X(LABEL,TOP,BOTTOM,CONDITION,USE)       \
        LABEL,
        enum { EMPDATA_LIST EMPDATA_NUMBER };
#undef X
        /*
         * fill the empdata :
         * we calculate the appropriate square bracket only
         * once (it's stored in sq)
         */
#define X(LABEL,TOP,BOTTOM,CONDITION,USE)           \
        if(Strings_equal(#TOP, "0"))                \
        {                                           \
            const double sq = 0.0;                  \
            empdata[LABEL] =                        \
                (struct empdata_t){                 \
                #LABEL,                             \
                    "",                             \
                    "",                             \
                    Stringize(ENSEMBLE_ ## LABEL),  \
                    0.0,                            \
                    0.0,                            \
                    sq CONDITION,                   \
                    USE                             \
                    };                              \
        }                                           \
        else                                        \
        {                                           \
            const double sq = Sq(#TOP,#BOTTOM);     \
            empdata[LABEL] =                        \
                (struct empdata_t){                 \
                #LABEL,                             \
                #TOP,                               \
                #BOTTOM,                            \
                Stringize(ENSEMBLE_ ## LABEL),      \
                USE ? sq : 0.0,                     \
                USE ? Bin_data(sq,0.1) : 0.0,       \
                (can_be_EMP && sq CONDITION),       \
                USE                                 \
            };                                      \
        }

        struct empdata_t empdata[EMPDATA_NUMBER];
        EMPDATA_LIST;
#undef X

        /*
         * Check that we are an EMP star
         */
        if(empdata[EMP].is_type == TRUE)
        {
            /* logg,logTeff,logL,logM  binned */
            const double logg_binned = Bin_data(lg,0.1);
            const double logTeff_binned = Bin_data(Safelog10(Teff_from_star_struct(star)),0.1);
            const double logL_binned = Bin_data(Safelog10(star->luminosity),0.1);
            const double logM_binned = Bin_data(Safelog10(star->mass),0.05);

            /*
             * EMP, CEMP and NEMP counts, both alone
             * and as functions of the [C/Fe] and [N/Fe]
             * for CEMPs and NEMPs respectively.
             */
#define X(LABEL,TOP,BOTTOM,CONDITION,USE)                               \
            if(empdata[i].is_type == TRUE && empdata[i].use_ratio)      \
            {                                                           \
                /* scalar number count vs time */                       \
                Set_ensemble_count(                                     \
                    "scalars",                                          \
                    "all stars",                                        \
                    (char*)#LABEL,                                      \
                    (double)T);                                         \
                Set_ensemble_count(                                     \
                    "scalars",                                          \
                    "all stars",                                        \
                    "stellar type",(int)star->stellar_type,             \
                    (char*)#LABEL,                                      \
                    (double)T);                                         \
                Set_ensemble_count(                                     \
                    "scalars",                                          \
                    (char*)multiplicity_string,                         \
                    (char*)#LABEL,                                      \
                    (double)T);                                         \
                Set_ensemble_count(                                     \
                    "scalars",                                          \
                    (char*)multiplicity_string,                         \
                    "stellar type",(int)star->stellar_type,             \
                    (char*)#LABEL,                                      \
                    (double)T);                                         \
                                                                        \
                /* number vs [X/Fe] or [Fe/H] vs time */                \
                Set_ensemble_count(                                     \
                    "distributions",                                    \
                    #LABEL" number vs ["#TOP"/"#BOTTOM"] : label->t->dist", \
                    "all",                                              \
                    "time",(double)T,                                   \
                    (char*)#LABEL,(double)empdata[i].binned);           \
                Set_ensemble_count(                                     \
                    "distributions",                                    \
                    #LABEL" number vs ["#TOP"/"#BOTTOM"] : label->t->dist", \
                    "stellar type",(int)star->stellar_type,             \
                    "time",(double)T,                                   \
                    (char*)#LABEL,(double)empdata[i].binned);           \
            }                                                           \
            i++;

            size_t i = 0;
            EMPDATA_LIST;

            if(is_binary)
            {
                /*
                 * Orbital period distributions as functions of
                 * the EMP type
                 */
#undef X
#define X(LABEL,TOP,BOTTOM,CONDITION,USE)                               \
                if(USE && empdata[i].is_type && empdata[i].use_ratio)   \
                {                                                       \
                    Set_ensemble_count(                                 \
                        "distributions",                                \
                        #LABEL" period distribution : label->dist",     \
                        "all",                                          \
                        "log orbital period",(double)Porb_binned);      \
                    Set_ensemble_count(                                 \
                        "distributions",                                \
                        #LABEL" period distribution : label->dist",     \
                        "stellar type",(int)star->stellar_type,         \
                        "log orbital period",(double)Porb_binned);      \
                    Set_ensemble_count(                                 \
                        "distributions",                                \
                        #LABEL" period distribution : label->t->dist",  \
                        "all",                                          \
                        "time",(double)T,                               \
                        "log orbital period",(double)Porb_binned);      \
                    Set_ensemble_count(                                 \
                        "distributions",                                \
                        #LABEL" period distribution : label->t->dist",  \
                        "stellar type",(int)star->stellar_type,         \
                        "time",(double)T,                               \
                        "log orbital period",(double)Porb_binned);      \
                }                                                       \
                i++;

                i = 0;
                EMPDATA_LIST;
            }

            /*
             * initial masses and periods
             * as functions of the EMP type
             */
            const double init_logM1_binned = Bin_data(Safelog10(stardata->preferences->zero_age.mass[0]),0.05);
            const double init_logM2_binned = is_binary ? Bin_data(Safelog10(stardata->preferences->zero_age.mass[1]),0.05) : -1.0;
            const double init_logP_binned = is_binary ? Bin_data(Safelog10(stardata->preferences->zero_age.orbital_period[0]),0.05) : -1.0;
#undef X
#undef Y
#define Y(LABEL,TOP,BOTTOM,XDATA,YAXIS,YDATA)                   \
            Set_ensemble_count(                                 \
                "initial distributions",                        \
                #LABEL" "YAXIS" distribution : label->dist",    \
                "all",                                          \
                YAXIS,YDATA);                                   \
            Set_ensemble_count(                                 \
                "initial distributions",                        \
                #LABEL" "YAXIS" distribution : label->dist",    \
                "stellar type",(int)star->stellar_type,         \
                YAXIS,YDATA);

#define X(LABEL,TOP,BOTTOM,CONDITION,USE)                               \
            if(USE && empdata[i].is_type && empdata[i].use_ratio)       \
            {                                                           \
                if(is_binary)                                           \
                {                                                       \
                    Y(LABEL,TOP,BOTTOM,empdata[i].binned,"log initial primary mass",(double)init_logM1_binned); \
                    Y(LABEL,TOP,BOTTOM,empdata[i].binned,"log initial secondary mass",(double)init_logM2_binned); \
                    Y(LABEL,TOP,BOTTOM,empdata[i].binned,"log initial period",(double)init_logP_binned); \
                }                                                       \
                else                                                    \
                {                                                       \
                    Y(LABEL,TOP,BOTTOM,empdata[i].binned,"log initial mass",(double)init_logM1_binned); \
                }                                                       \
            }                                                           \
            i++;

            i = 0;
            EMPDATA_LIST;



            /*
             * logL, logM, logg, stellar type
             * as functions of the EMP type
             */
#undef X
#undef Y
#define Y(LABEL,TOP,BOTTOM,XDATA,YAXIS,YDATA)                   \
            Set_ensemble_count(                                 \
                "distributions",                                \
                #LABEL" "YAXIS" distribution : label->dist",    \
                "all",                                          \
                YAXIS,YDATA);                                   \
            Set_ensemble_count(                                 \
                "distributions",                                \
                #LABEL" "YAXIS" distribution : label->dist",    \
                "stellar type",(int)star->stellar_type,         \
                YAXIS,YDATA);                                   \
            Set_ensemble_count(                                 \
                "distributions",                                \
                #LABEL" "YAXIS" distribution : label->t->dist", \
                "all",                                          \
                "time",(double)T,                               \
                YAXIS,YDATA);                                   \
            Set_ensemble_count(                                 \
                "distributions",                                \
                #LABEL" "YAXIS" distribution : label->t->dist", \
                "time",(double)T,                               \
                "stellar type",(int)star->stellar_type,         \
                YAXIS,YDATA);                                   \


#define X(LABEL,TOP,BOTTOM,CONDITION,USE)                               \
            if(USE && empdata[i].is_type && empdata[i].use_ratio)       \
            {                                                           \
                Y(LABEL,TOP,BOTTOM,empdata[i].binned,"log luminosity",(double)logL_binned); \
                Y(LABEL,TOP,BOTTOM,empdata[i].binned,"log mass",(double)logM_binned); \
                Y(LABEL,TOP,BOTTOM,empdata[i].binned,"logg",(double)logg_binned); \
                Y(LABEL,TOP,BOTTOM,empdata[i].binned,"stellar type",(int)star->stellar_type); \
            }                                                           \
            i++;

            i = 0;
            EMPDATA_LIST;

            if(is_binary)
            {
                /*
                 * Orbital period distributions as functions of
                 * the square-bracket condition
                 */
#undef X
#define X(LABEL,TOP,BOTTOM,CONDITION,USE)                               \
                if(USE && empdata[i].is_type && empdata[i].use_ratio)   \
                {                                                       \
                    Set_ensemble_count(                                 \
                        "distributions",                                \
                        #LABEL" period vs ["#TOP"/"#BOTTOM"] : label->map", \
                        "all",                                          \
                        "["#TOP"/"#BOTTOM"]",(double)empdata[i].binned, \
                        "log orbital period",(double)Porb_binned);      \
                    Set_ensemble_count(                                 \
                        "distributions",                                \
                        #LABEL" period vs ["#TOP"/"#BOTTOM"] : label->map", \
                        "stellar type",(int)star->stellar_type,         \
                        "["#TOP"/"#BOTTOM"]",(double)empdata[i].binned, \
                        "log orbital period",(double)Porb_binned);      \
                    Set_ensemble_count(                                 \
                        "distributions",                                \
                        #LABEL" period vs ["#TOP"/"#BOTTOM"] : label->t->map", \
                        "all",                                          \
                        "time",(double)T,                               \
                        "["#TOP"/"#BOTTOM"]",(double)empdata[i].binned, \
                        "log orbital period",(double)Porb_binned);      \
                    Set_ensemble_count(                                 \
                        "distributions",                                \
                        #LABEL" period vs ["#TOP"/"#BOTTOM"] : label->t->map", \
                        "time",(double)T,                               \
                        "stellar type",(int)star->stellar_type,         \
                        "["#TOP"/"#BOTTOM"]",(double)empdata[i].binned, \
                        "log orbital period",(double)Porb_binned);      \
                }                                                       \
                i++;

                i = 0;
                EMPDATA_LIST;
            }

            /*
             * logL, logM, logg, stellar type
             * as functions of the square bracket condition
             */
#undef X
#undef Y
#define Y(LABEL,TOP,BOTTOM,XDATA,YAXIS,YDATA)                           \
            Set_ensemble_count(                                         \
                "distributions",                                        \
                #LABEL" "YAXIS" vs ["#TOP"/"#BOTTOM"] : label->map",    \
                "all",                                                  \
                "["#TOP"/"#BOTTOM"]",(double)empdata[i].binned,         \
                YAXIS,YDATA);                                           \
            Set_ensemble_count(                                         \
                "distributions",                                        \
                #LABEL" "YAXIS" vs ["#TOP"/"#BOTTOM"] : label->map",    \
                "stellar type",(int)star->stellar_type,                 \
                "["#TOP"/"#BOTTOM"]",(double)empdata[i].binned,         \
                YAXIS,YDATA);                                           \
            Set_ensemble_count(                                         \
                "distributions",                                        \
                #LABEL" "YAXIS" vs ["#TOP"/"#BOTTOM"] : label->t->map", \
                "all",                                                  \
                "time",(double)T,                                       \
                "["#TOP"/"#BOTTOM"]",(double)XDATA,                     \
                YAXIS,YDATA);                                           \
            Set_ensemble_count(                                         \
                "distributions",                                        \
                #LABEL" "YAXIS" vs ["#TOP"/"#BOTTOM"] : label->t->map", \
                "stellar type",(int)star->stellar_type,                 \
                "time",(double)T,                                       \
                "["#TOP"/"#BOTTOM"]",(double)XDATA,                     \
                YAXIS,YDATA);                                           \


#define X(LABEL,TOP,BOTTOM,CONDITION,USE)                               \
            if(USE && empdata[i].is_type && empdata[i].use_ratio)       \
            {                                                           \
                Y(LABEL,TOP,BOTTOM,empdata[i].binned,"log luminosity",(double)logL_binned); \
                Y(LABEL,TOP,BOTTOM,empdata[i].binned,"log mass",(double)logM_binned); \
                Y(LABEL,TOP,BOTTOM,empdata[i].binned,"logg",(double)logg_binned); \
                Y(LABEL,TOP,BOTTOM,empdata[i].binned,"stellar type",(int)star->stellar_type); \
            }                                                           \
            i++;

            i = 0;
            EMPDATA_LIST;

            /*
             * HRD of XEMP stars, by type
             */
#undef X
#undef Y
#define Y(LABEL,YAXIS,YDATA)                            \
            Set_ensemble_count(                         \
                "HRD",                                  \
                #LABEL,(double)empdata[i].binned,       \
                "all",                                  \
                "logTeff",logTeff_binned,               \
                YAXIS,YDATA);                           \
            Set_ensemble_count(                         \
                "HRD",                                  \
                #LABEL,(double)empdata[i].binned,       \
                "stellar type",(int)star->stellar_type, \
                "logTeff",logTeff_binned,               \
                YAXIS,YDATA);                           \
            Set_ensemble_count(                         \
                "HRD(t)",                               \
                "time",(double)T,                       \
                #LABEL,(double)empdata[i].binned,       \
                "all",                                  \
                "logTeff",logTeff_binned,               \
                YAXIS,YDATA);                           \
            Set_ensemble_count(                         \
                "HRD(t)",                               \
                "time",(double)T,                       \
                #LABEL,(double)empdata[i].binned,       \
                "stellar type",(int)star->stellar_type, \
                "logTeff",logTeff_binned,               \
                YAXIS,YDATA);                           \

#define X(LABEL,TOP,BOTTOM,CONDITION,USE)                           \
            if(USE && empdata[i].is_type && empdata[i].use_ratio)   \
            {                                                       \
                Y(LABEL,"logL",logL_binned);                        \
                Y(LABEL,"logg",logg_binned);                        \
            }                                                       \
            i++;

            i = 0;
            EMPDATA_LIST;

        }

    }
#endif // NUCSYN
}

#endif // STELLAR_POPULATIONS_ENSEMBLE
