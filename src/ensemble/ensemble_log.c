#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef STELLAR_POPULATIONS_ENSEMBLE

#include "ensemble.h"
#include "../pulsation/pulsation_macros.h"

static void make_ensemble_type_array(struct stardata_t * const stardata,
                                     Ensemble_type ** ensemble_type);
static void _set_previous_stars(const struct star_t * const previous_star,
                                const struct star_t * const previous_other_star,
                                struct star_t ** previous_primary_star,
                                struct star_t ** previous_secondary_star);

#define estore (stardata->model.ensemble_store)
#define lastensembleouttime (stardata->model.lastensembleouttime)

void ensemble_log(struct stardata_t * Restrict const stardata,
                  const Boolean flush_deferred)
{
    /*
     * Ensemble stellar population data output.
     * activated by STELLAR_POPULATIONS_ENSEMBLE
     *
     * This function is called from log_every_timestep only
     * if stardata->preferences->ensemble is TRUE.
     *
     * You should consult the binary_c documentation
     * regarding how ensembles are constructed and function,
     * because they can generate a lot of data if not
     * used carefully. They are, however, the most powerful
     * way to log stellar population data, so you should use them.
     */
    struct stardata_t * const previous_stardata = stardata->previous_stardata;
    Ensemble_type * ensemble_type;
    double ensemble_weight[ENSEMBLE_NUMBER] = {0};
    double teff,l,linear_dt_since_last;
#ifdef STELLAR_POPULATIONS_ENSEMBLE_SPARSE
    double dt_since_last Maybe_unused;
#endif
    Ensemble_index k;

    Boolean have_logged_transient = FALSE;
    Boolean reset_time = FALSE;
    struct preferences_t * const preferences = stardata->preferences;

    /*
     * If final == TRUE, we are on the last timestep
     */
    const Boolean final =
        (Ensemble_times_equal(stardata->model.time,
                              stardata->model.max_evolution_time) ||
         stardata->model.evolution_stop) &&
        stardata->preferences->disable_end_logging == FALSE;

    /*
     * If json_output == TRUE, we should flush the ensemble
     * cdict.
     */
    const Boolean json_output =
        (preferences->ensemble_defer == FALSE &&
         final==TRUE) ||
        unlikely(flush_deferred == TRUE);

    Dprint("Final %d defer %d flush_deferred %d json_output %d\n",
           final,
           preferences->ensemble_defer,
           flush_deferred,
           json_output);

    /*
     * T is the time of the next trigger : when triggered, T == current time,
     * so you can use T to output. Also, when setting the ensemble cdict
     * you can use T because it will be the correct time for the next flush.
     *
     * Because this is the next trigger time, it is the time that
     * should be used when making multidimensional distributions
     * in the ensemble_cdict.
     *
     * Note that T is rounded and truncated to avoid floating-point errors.
     * See ENSEMBLE_SIGNIFICANT_DIGITS and the limit_sigfigs
     * functions for details.
     */
    const Time T = ensemble_output_time(stardata);

    /*
     * dt_since_last is the time between this output and the
     * previous : it is usually the yield_dt but may be less
     * if we have reached the maximum evolution time (in which
     * case final==TRUE and we're being called to flush all the
     * arrays).
     *
     * NB this is the difference between logarithmic times if
     * ensemble_logtimes is TRUE
     *
     * If you want the linear time difference, even when using
     * logarithmic times for logging, use linear_dt_since_last.
     */
    if(preferences->ensemble_logtimes == TRUE)
    {
        dt_since_last = log10(stardata->model.time) - stardata->model.fixed_timesteps[FIXED_TIMESTEP_ENSEMBLE].previous_trigger;
        linear_dt_since_last = stardata->model.time - exp10(stardata->model.fixed_timesteps[FIXED_TIMESTEP_ENSEMBLE].previous_trigger);
    }
    else
    {
        dt_since_last = stardata->model.time - stardata->model.fixed_timesteps[FIXED_TIMESTEP_ENSEMBLE].previous_trigger;
        linear_dt_since_last = dt_since_last;
    }

    /*
     * Detect if the time has hit T, the trigger time.
     */
    const Boolean trigger = timestep_fixed_trigger(stardata,
                                                   FIXED_TIMESTEP_ENSEMBLE);

    /*
     * Make the ensemble type array, or get it
     * from the tmpstore if it is already made.
     */
    make_ensemble_type_array(stardata,
                             &ensemble_type);

    /*
     * Make the ensemble cdict, if required
     */
    make_ensemble_cdict(stardata);

    /*
     * Calculate time since last call that is after the start time
     */
    const double prevtime =
        preferences->ensemble_logtimes == TRUE ?
        Max(preferences->ensemble_startlogtime,
            exp10(stardata->model.fixed_timesteps[FIXED_TIMESTEP_ENSEMBLE].previous_test)) :
        stardata->model.fixed_timesteps[FIXED_TIMESTEP_ENSEMBLE].previous_test;



    const double lineardt = Max(0.0,
                                stardata->model.time - prevtime);


    /*
    static double lineart = 0.0;
    lineart += lineardt;
    printf("at t=%g %d lineardt=%g lineart=%g (ensemble dt %g) max_evolution_time=%g : prev trigger %g + logdt %g : linear %g - %g = %g vs %g : next %g\n",
           stardata->model.time,
           stardata->preferences->ensemble_enforce_maxtime,
           lineardt,
           lineart,
           Ensemble_timestep_linear,
           stardata->preferences->max_evolution_time,
           stardata->model.fixed_timesteps[FIXED_TIMESTEP_ENSEMBLE].previous_trigger,
           stardata->model.fixed_timesteps[FIXED_TIMESTEP_ENSEMBLE].previous_trigger+stardata->preferences->ensemble_logdt,
           exp10(stardata->model.fixed_timesteps[FIXED_TIMESTEP_ENSEMBLE].previous_trigger+stardata->preferences->ensemble_logdt),
           exp10(stardata->model.fixed_timesteps[FIXED_TIMESTEP_ENSEMBLE].previous_trigger),
           exp10(stardata->model.fixed_timesteps[FIXED_TIMESTEP_ENSEMBLE].previous_trigger+stardata->preferences->ensemble_logdt)-
           exp10(stardata->model.fixed_timesteps[FIXED_TIMESTEP_ENSEMBLE].previous_trigger),
           log(10.0)*stardata->model.time*stardata->preferences->ensemble_logdt,
           exp10(Next_ensemble_time));
    */

    /*
     * dt<0 can happen with the old BSE algorithm
     * but shouldn't with modern binary_c. If it happens,
     * we hope that it's not for long and ignore it. If you
     * really care, use the binary_c evolution algorithm instead
     * (which is now the default).
     *
     * Note that dt == 0 is perfectly fine. This happens when there
     * is an event (e.g. a common envelope merging), hence we
     * check that lineardt > -TINY, not >0.
     */
    if(lineardt > -TINY &&
       flush_deferred == FALSE)
    {
        /*
         * dtp is the linear timestep * probability
         */
        const double p = stardata->model.probability;
        const double dtp = lineardt * p;

        /*
         * Detect a massless system
         */
        const Boolean both_massless =
            stardata->star[0].stellar_type == MASSLESS_REMNANT &&
            stardata->star[1].stellar_type == MASSLESS_REMNANT;

        /*
         * Star numbers and pointers for the primary and secondary,
         * defined by the brightest and dimmest star in the system
         *
         * Note: these can be -1 if the star is massless
         */
        const Star_number iprimary Maybe_unused =
            both_massless == TRUE ?
            /* both massless : neither can be the primary */
            -1 :
            /* 0 massless : 1 must be primary */
            stardata->star[0].stellar_type == MASSLESS_REMNANT ? 1 :
            /* 1 massless : 0 must be primary */
            stardata->star[1].stellar_type == MASSLESS_REMNANT ? 0 :
            /* neither massless : choose the brighter of the two */
            stardata->star[0].luminosity > stardata->star[1].luminosity ? 0 : 1;

        struct star_t * const primary Maybe_unused =
            iprimary == -1 ? NULL : &stardata->star[iprimary];
        struct star_t * const prevprimary Maybe_unused =
            iprimary == -1 ? NULL : &stardata->previous_stardata->star[iprimary];

        const Star_number isecondary Maybe_unused =
            iprimary == -1 ? -1 :
            Other_star_struct(primary)->stellar_type == MASSLESS_REMNANT ? -1 :
            Other_star(iprimary);

        struct star_t * const secondary Maybe_unused =
            isecondary == -1 ? NULL : &stardata->star[isecondary];
        struct star_t * const prevsecondary Maybe_unused =
            isecondary == -1 ? NULL : &stardata->previous_stardata->star[isecondary];

        /*
         * Star numbers and pointers defined by the most_massive
         * and least_massive star in the system
         */
        const Star_number imost_massive Maybe_unused =
            stardata->star[0].mass > stardata->star[1].mass ? 0 : 1;
        const Star_number ileast_massive Maybe_unused =
            Other_star(imost_massive);
        struct star_t * const most_massive Maybe_unused =
            &stardata->star[imost_massive];
        struct star_t * const least_massive Maybe_unused =
            &stardata->star[ileast_massive];
        struct star_t * const prev_most_massive Maybe_unused =
            &stardata->previous_stardata->star[imost_massive];
        struct star_t * const prev_least_massive Maybe_unused =
            &stardata->previous_stardata->star[ileast_massive];

        /*
         * Binary system detection
         */
        const Boolean born_binary =
            WAS_A_STAR(0) &&
            WAS_A_STAR(1);

        const Boolean Maybe_unused system_is_binary =
            Boolean_(born_binary==TRUE &&
                     NEITHER_STAR_MASSLESS &&
                     stardata->common.orbit.separation > TINY);

        const Boolean Maybe_unused system_is_observable_binary =
            Boolean_(Observable_binary);

        const Boolean Maybe_unused system_was_binary =

            Boolean_(born_binary==TRUE &&
                     both_massless==FALSE &&
                     stardata->previous_stardata->common.orbit.separation > TINY);

        /*
         * Whole-system properties
         */
        const double system_luminosity =
            System_summed_property(luminosity);

        const double system_gravitational_mass =
            System_summed_property(mass);

        const double log10_Porb_binwidth = 0.1;
        const double log10_Porb_years_binned =
            Bin_data(Safelog10(stardata->common.orbit.period),
                     log10_Porb_binwidth);
        const double log10_Porb_days_binned =
            Bin_data(Safelog10(stardata->common.orbit.period * YEAR_LENGTH_IN_DAYS),
                     log10_Porb_binwidth);

        const double e_binwidth = 0.02;
        const double e_binned =
            Bin_data(stardata->common.orbit.eccentricity,
                     e_binwidth);

        const double a_binwidth = 0.1;
        const double a_binned =
            Bin_data(Safelog10(stardata->common.orbit.separation),
                     a_binwidth);

        const double q_binwidth = 0.02;
        const double q_binned =
            Bin_data(least_massive->mass / most_massive->mass,
                     q_binwidth);


#ifdef STELLAR_COLOURS
        const Boolean CMD =
            preferences->ensemble_filter[STELLAR_POPULATIONS_ENSEMBLE_FILTER_CMD_BOKEH] == TRUE ||
            preferences->ensemble_filter[STELLAR_POPULATIONS_ENSEMBLE_FILTER_GAIA_CMD] == TRUE ||
            preferences->ensemble_filter[STELLAR_POPULATIONS_ENSEMBLE_FILTER_GAIA_CMD_TIME_SLICES] == TRUE ||
            preferences->ensemble_filter[STELLAR_POPULATIONS_ENSEMBLE_FILTER_GAIA_CMD_PERIOD_DISTRIBUTIONS] == TRUE ||
            preferences->ensemble_filter[STELLAR_POPULATIONS_ENSEMBLE_FILTER_GAIA_CMD_PERIOD_DISTRIBUTIONS_TIME_SLICES] == TRUE;
#endif // STELLAR_COLOURS

#ifdef STELLAR_POPULATIONS_ENSEMBLE
        if(stardata->model.model_number == 0 &&
           Use_ensemble(STELLAR_POPULATIONS_ENSEMBLE_FILTER_INITIAL_DISTRIBUTIONS) == TRUE)
        {
            /*
             * initial functions:
             * note these are set with Set_ensemble_rate
             * as they are (effectively) instantaneous
             * events, not number counts because they are
             * only set at t==0 exactly.
             */
            Foreach_star(star)
            {
                if(born_binary == FALSE)
                {
                    Set_ensemble_rate(
                        "initial distributions",
                        "log mass : label->dist",
                        "single",
                        "log mass",Bin_data(Safelog10(star->mass),0.1)
                        );
                    Set_ensemble_rate(
                        "initial distributions",
                        "log luminosity : label->dist",
                        "single",
                        "log luminosity",Bin_data(Safelog10(star->luminosity),0.1)
                        );
                    Set_ensemble_rate(
                        "initial distributions",
                        "log v_eq : label->dist",
                        "single",
                        "log v_eq",Bin_data(Safelog10(Max(1.0,star->v_eq)),0.1)
                        );
                }

                /*
                 * Multiplicity statistics
                 */
                Set_ensemble_rate("initial distributions",
                                  "multiplicity count",
                                  stardata->preferences->zero_age.multiplicity);

                if(born_binary == TRUE &&
                   star->starnum == 0)
                {
                    Set_ensemble_rate(
                        "initial distributions",
                        "log orbital period / years : label->dist",
                        "all",
                        "log orbital period / years",(double)log10_Porb_years_binned
                        );
                    Set_ensemble_rate(
                        "initial distributions",
                        "log orbital period / years vs mass 0 : label->map",
                        "all",
                        "log mass 0",Bin_data(Safelog10(star->mass),0.1),
                        "log orbital period / years",(double)log10_Porb_years_binned
                        );
                    Set_ensemble_rate(
                        "initial distributions",
                        "log orbital period / days : label->dist",
                        "all",
                        "log orbital period / days",(double)log10_Porb_days_binned
                        );
                    Set_ensemble_rate(
                        "initial distributions",
                        "log orbital period /days vs mass 0 : label->map",
                        "all",
                        "log mass 0",Bin_data(Safelog10(star->mass),0.1),
                        "log orbital period / days",(double)log10_Porb_days_binned
                        );
                    Set_ensemble_rate(
                        "initial distributions",
                        "log orbital separation / Rsun : label->dist",
                        "all",
                        "log orbital separation",(double)a_binned
                        );

                    Set_ensemble_rate(
                        "initial distributions",
                        "orbital eccentricity : label->dist",
                        "all",
                        "orbital eccentricity",(double)e_binned
                        );

                    Set_ensemble_rate(
                        "initial distributions",
                        "mass ratio : label->dist",
                        "all",
                        "mass ratio",(double)q_binned
                        );

                    Set_ensemble_rate(
                        "initial distributions",
                        "mass ratio vs mass 0 : label->map",
                        "all",
                        "log mass 0",Bin_data(Safelog10(star->mass),0.1),
                        "mass ratio",(double)q_binned
                        );
                    Set_ensemble_rate(
                        "initial distributions",
                        "mass ratio vs mass 1 : label->map",
                        "all",
                        "log mass 1",Bin_data(Safelog10(stardata->star[1].mass),0.1),
                        "mass ratio",(double)q_binned
                        );

                    Set_ensemble_rate(
                        "initial distributions",
                        "log mass : labelint->dist",
                        "star",(int)star->starnum,
                        "log mass",Bin_data(Safelog10(star->mass),0.1)
                        );
                    Set_ensemble_rate(
                        "initial distributions",
                        "log luminosity : labelint->dist",
                        "star",(int)star->starnum,
                        "log luminosity",Bin_data(Safelog10(star->luminosity),0.1)
                        );
                    Set_ensemble_rate(
                        "initial distributions",
                        "log v_eq : labelint->dist",
                        "star",(int)star->starnum,
                        "log v_eq",Bin_data(Safelog10(Max(1.0,star->v_eq)),0.1)
                        );
                    Set_ensemble_rate(
                        "initial distributions",
                        "log mass : label->dist",
                        "all resolved",
                        "log mass",Bin_data(Safelog10(star->mass),0.1)
                        );
                    Set_ensemble_rate(
                        "initial distributions",
                        "log luminosity : label->dist",
                        "all resolved",
                        "log luminosity",Bin_data(Safelog10(star->luminosity),0.1)
                        );
                    Set_ensemble_rate(
                        "initial distributions",
                        "log v_eq : label->dist",
                        "all resolved",
                        "log v_eq",Bin_data(Safelog10(Max(1.0,star->v_eq)),0.1)
                        );
                }
            }
            Set_ensemble_rate(
                "initial distributions",
                "log mass : label->dist",
                "all unresolved",
                "log mass",Bin_data(
                    Safelog10(stardata->star[0].mass +
                              born_binary == TRUE ? stardata->star[1].mass : 0.0),0.1)
                );
            Set_ensemble_rate(
                "initial distributions",
                "log luminosity : label->dist",
                "all unresolved",
                "log luminosity",Bin_data(Safelog10(stardata->star[0].luminosity +
                                                    born_binary == TRUE ? stardata->star[1].luminosity : 0.0),0.1)
                );
            Set_ensemble_rate(
                "initial distributions",
                "log v_eq : label->dist",
                "all unresolved",
                "log v_eq",Bin_data(Safelog10(Max(1.0,Luminosity_weighted_stellar_property(v_eq))),0.1)
                );
            if(primary)
            {
                Set_ensemble_rate(
                    "initial distributions",
                    "log mass : label->dist",
                    "primary",
                    "log mass",Bin_data(Safelog10(primary->mass),0.1)
                    );
                Set_ensemble_rate(
                    "initial distributions",
                    "log luminosity : label->dist",
                    "primary",
                    "log luminosity",Bin_data(Safelog10(primary->luminosity),0.1)
                    );
                Set_ensemble_rate(
                    "initial distributions",
                    "log v_eq : label->dist",
                    "primary",
                    "log v_eq",Bin_data(Safelog10(Max(1.0,primary->v_eq)),0.1)
                    );
            }
            if(born_binary == TRUE)
            {
                if(secondary)
                {
                    Set_ensemble_rate(
                        "initial distributions",
                        "log mass : label->dist",
                        "secondary",
                        "log mass",Bin_data(Safelog10(secondary->mass),0.1)
                        );
                    Set_ensemble_rate(
                        "initial distributions",
                        "log luminosity : label->dist",
                        "secondary",
                        "log luminosity",Bin_data(Safelog10(secondary->luminosity),0.1)
                        );
                    Set_ensemble_rate(
                        "initial distributions",
                        "log v_eq : label->dist",
                        "secondary",
                        "log v_eq",Bin_data(Safelog10(Max(1.0,secondary->v_eq)),0.1)
                        );
                }
            }
        }

        if((preferences->ensemble_logtimes == TRUE &&
            stardata->model.time  < preferences->ensemble_startlogtime + TINY) ||
           (preferences->ensemble_logtimes == FALSE &&
            stardata->model.time  < preferences->ensemble_starttime + TINY))
        {
            /*
             * We cannot log before a certain time.
             */
            lastensembleouttime =
                preferences->ensemble_logtimes==TRUE ?
                log10(preferences->ensemble_startlogtime) :
                0.0;
            for(Yield_source s = 0;
                s < SOURCE_NUMBER;
                s++)
            {
                stardata->model.ensemble_yield[s] = 0.0;
            }
        }
        else
        {
            Dprint("Grand ensemble\n");

            if(flush_deferred == FALSE)
            {
                /*
                 * A grand ensemble of things we can observe, all nicely pre-binned.
                 */

                /*
                 * Require spectral types in a few places
                 */
                Long_spectral_type spectypef[NUMBER_OF_STARS];
                Long_spectral_type prevspectypef[NUMBER_OF_STARS];

                Foreach_star(star)
                {
                    if(star->stellar_type == MASSLESS_REMNANT)
                    {
                        spectypef[star->starnum] = 8.0;
                    }
                    else
                    {
                        spectypef[star->starnum] = long_spectral_type(stardata,
                                                                      star);
                    }
                    if(stardata->previous_stardata->star[star->starnum].stellar_type == MASSLESS_REMNANT)
                    {
                        prevspectypef[star->starnum] = 8.0;
                    }
                    else
                    {
                        prevspectypef[star->starnum] = long_spectral_type(stardata,
                                                                          star);
                    }
                }


                Foreach_star(star,other_star)
                {
                    const char * const system_label Maybe_unused =
                        stardata->model.sgl == TRUE ?
                        "single" :
                        (star->starnum == iprimary ? "primary" :
                         star->starnum == isecondary ? "secondary" :
                         "neither");
                    const struct star_t * const previous_star = &previous_stardata->star[star->starnum];
                    const struct star_t * const previous_other_star = &previous_stardata->star[other_star->starnum];
                    struct star_t * previous_primary_star = NULL;
                    struct star_t * previous_secondary_star = NULL;
                    _set_previous_stars(previous_star,
                                        previous_other_star,
                                        &previous_primary_star,
                                        &previous_secondary_star);
#ifdef NUCSYN
                    Abundance * const Xsurf = nucsyn_observed_surface_abundances(star);
#endif // NUCSYN

                    /*
                     * First set all detections to false.
                     */
                    memset(ensemble_weight,0,sizeof(double)*ENSEMBLE_NUMBER);

                    /*
                     * Test variables
                     */
                    if(star->starnum == ENSEMBLE_TEST_STAR &&
                       Use_ensemble(STELLAR_POPULATIONS_ENSEMBLE_FILTER_TEST) == TRUE)
                    {
                        /*
                         * There is one "star" at any time
                         */
                        ensemble_weight[ENSEMBLE_TEST_COUNT] = 1.0;

                        /*
                         * The test rate is 1/Myr, so in one timestep
                         * we expect 1.0 * (linear timestep/Myr) events,
                         * i.e. dt * 1e-6.
                         */
                        ensemble_weight[ENSEMBLE_TEST_RATE] =
                            lineardt * 1e-6;

                        Set_ensemble_test_rate(
                            "distributions",
                            "test","rate(t) : label->t->dist",
                            "test",
                            "time",(double)T,
                            "dummy",(double)1.0
                            );
                        Set_ensemble_test_rate(
                            "distributions",
                            "test","rate(t) : label->t->dist",
                            "test",
                            "time",(double)T,
                            "dummy",(double)2.0
                            );
                        Set_ensemble_count(
                            "distributions",
                            "test","count(t) : label->t->dist",
                            "test",
                            "time",(double)T,
                            "dummy",(double)1.0
                            );
                        Set_ensemble_count(
                            "distributions",
                            "test","count(t) : label->t->dist",
                            "test",
                            "time",(double)T,
                            "dummy",(double)2.0
                            );
                    }

                    /*
                     * Stars which form with M>ENSEMBLE_MINIMUM_PMS_MASS are logged: everything
                     * with a lower mass is a planet and we do nothing
                     */

                    if(stardata->common.zero_age.mass[star->starnum] > ENSEMBLE_MINIMUM_PMS_MASS)
                    {
                        Dprint("HRD");

                        /*
                         * calculate effective temperature, log L
                         */
                        teff = log10(Teff_from_star_struct(star));
                        l = log10(star->luminosity);

                        /*
                         * Save the total number of stars and the total luminosity in stars
                         */
                        if(IS_A_STAR(star->starnum))
                        {
                            ensemble_weight[ENSEMBLE_NUMBER_OF_STARS]=1.0;
                            ensemble_weight[ENSEMBLE_LUMINOSITY_OF_STARS]=star->luminosity;
                            ensemble_weight[ENSEMBLE_MASS_IN_STARS]=star->mass;
                        }

                        /*
                         * Binary/single statistics : only do this for index==0
                         * to avoid double-counting. We assume that for single
                         * stars, the star is in star 0 and that star 1 is a 'planet'
                         * that can be ignored.
                         */
                        if(star->starnum == 0)
                        {
                            Dprint("Binary statistics");

                            if(NEITHER_STAR_MASSLESS)
                            {
                                const int __key = system_is_observable_binary == TRUE ?
                                    ENSEMBLE_NUMBER_OF_OBSERVABLE_BINARY_STARS :
                                    ENSEMBLE_NUMBER_OF_OBSERVABLE_SINGLE_STARS;
                                ensemble_weight[__key] = 1.0;
                            }

                            if(born_binary==TRUE)
                            {
                                /*
                                 * Only stars born binary can still be binary stars,
                                 * mergers or disrupted binaries.
                                 */


                                /*
                                 * Pre- and post-common envelope evolution statistics
                                 */
                                if(Use_ensemble(STELLAR_POPULATIONS_ENSEMBLE_FILTER_COMENV) == TRUE)
                                {
                                    ensemble_comenv(stardata);
                                }

                                if(IS_A_STAR(0) && IS_A_STAR(1))
                                {
                                    /* both stars are observable as stars :
                                     * check the orbit to see if they are bound.
                                     * NB this cannot be a merged star because
                                     * were it, the companion would be a MASSLESS_REMNANT
                                     * and IS_A_STAR would fail.
                                     */
                                    if(stardata->common.orbit.separation > TINY)
                                    {
                                        /* binary system : still bound */
                                        ensemble_weight[ENSEMBLE_NUMBER_OF_BINARY_STARS]=1.0;
                                        ensemble_weight[ENSEMBLE_LUMINOSITY_OF_BINARY_STARS]=system_luminosity;
                                        ensemble_weight[ENSEMBLE_MASS_IN_BINARY_STARS]=system_gravitational_mass;
                                    }
                                    else
                                    {
                                        /* binary system but unbound : each star would look single */
                                        ensemble_weight[ENSEMBLE_NUMBER_OF_DISRUPTED_BINARIES]=1.0;
                                        ensemble_weight[ENSEMBLE_LUMINOSITY_OF_DISRUPTED_BINARIES]=system_luminosity;
                                        ensemble_weight[ENSEMBLE_MASS_IN_DISRUPTED_BINARIES]=system_gravitational_mass;

                                        ensemble_weight[ENSEMBLE_NUMBER_OF_SINGLE_STARS]=2;
                                        ensemble_weight[ENSEMBLE_LUMINOSITY_OF_SINGLE_STARS]=system_luminosity;
                                        ensemble_weight[ENSEMBLE_MASS_IN_SINGLE_STARS]=system_gravitational_mass;

                                        /* both stars would be runaways */
                                        ensemble_weight[ENSEMBLE_NUMBER_OF_RUNAWAY_STARS]=2;
                                        ensemble_weight[ENSEMBLE_LUMINOSITY_OF_RUNAWAY_STARS]=system_luminosity;
                                        ensemble_weight[ENSEMBLE_MASS_IN_RUNAWAY_STARS]=system_gravitational_mass;
                                    }
                                }
                                /* one of the stars is a planet or massless remnant: find out which */
                                else if(IS_A_STAR(0))
                                {
                                    /* star 1 is observable as a single star */
                                    ensemble_weight[ENSEMBLE_NUMBER_OF_SINGLE_STARS]=1.0;
                                    ensemble_weight[ENSEMBLE_LUMINOSITY_OF_SINGLE_STARS]=stardata->star[0].luminosity;
                                    ensemble_weight[ENSEMBLE_MASS_IN_SINGLE_STARS]=stardata->star[0].mass;

                                    if(stardata->model.merged == TRUE || stardata->model.coalesce == TRUE)
                                    {
                                        /* it is a merged star */
                                        ensemble_weight[ENSEMBLE_NUMBER_OF_MERGED_BINARIES]=1.0;
                                        ensemble_weight[ENSEMBLE_LUMINOSITY_OF_MERGED_BINARIES]=stardata->star[0].luminosity;
                                        ensemble_weight[ENSEMBLE_MASS_IN_MERGED_BINARIES]=stardata->star[0].mass;
                                    }
                                }
                                else if(IS_A_STAR(1))
                                {
                                    /* star 2 is observable as a single star */
                                    ensemble_weight[ENSEMBLE_NUMBER_OF_SINGLE_STARS]=1.0;
                                    ensemble_weight[ENSEMBLE_LUMINOSITY_OF_SINGLE_STARS]=stardata->star[1].luminosity;
                                    ensemble_weight[ENSEMBLE_MASS_IN_SINGLE_STARS]=stardata->star[1].mass;

                                    if(stardata->model.merged == TRUE)
                                    {
                                        /* it is a merged star */
                                        ensemble_weight[ENSEMBLE_NUMBER_OF_MERGED_BINARIES]=1.0;
                                        ensemble_weight[ENSEMBLE_LUMINOSITY_OF_MERGED_BINARIES]=stardata->star[1].luminosity;
                                        ensemble_weight[ENSEMBLE_MASS_IN_MERGED_BINARIES]=stardata->star[1].mass;
                                    }
                                }
                            }
                            else
                            {
                                /* system was born single in star 1 : check if it's still a star */
                                if(IS_A_STAR(0))
                                {
                                    ensemble_weight[ENSEMBLE_NUMBER_OF_SINGLE_STARS]=1.0;
                                    ensemble_weight[ENSEMBLE_LUMINOSITY_OF_SINGLE_STARS]=stardata->star[0].luminosity;
                                    ensemble_weight[ENSEMBLE_MASS_IN_SINGLE_STARS]=stardata->star[0].mass;
                                }
                            }

                            if(born_binary &&
                               Use_ensemble(STELLAR_POPULATIONS_ENSEMBLE_FILTER_TIDES) == TRUE)
                            {
                                /*
                                 * Data that is useful for testing tides
                                 */
                                ensemble_tides(stardata,dtp,T);
                            }

                            /*
                             * Orbital velocity dispersion data
                             */
                            if(Use_ensemble(STELLAR_POPULATIONS_ENSEMBLE_FILTER_VELOCITY_DISPERSION))
                            {
                                ensemble_orbital_velocity_dispersion(stardata,
                                                                     dtp,
                                                                     T,
                                                                     system_is_binary);
                            }

                            if(born_binary &&
                               Use_ensemble(STELLAR_POPULATIONS_ENSEMBLE_FILTER_ORBIT) == TRUE)
                            {
                                /*
                                 * grid orbital properties by stellar type,
                                 * so we have to be born binary (might not
                                 * be binary now)
                                 */
                                Set_ensemble_count(
                                    "distributions",
                                    "log orbital period : map->dist",
                                    "stellar type 0",(int)stardata->star[0].stellar_type,
                                    "stellar type 1",(int)stardata->star[1].stellar_type,
                                    "log orbital period / years",(double)log10_Porb_years_binned
                                    );
                                Set_ensemble_count(
                                    "distributions",
                                    "log orbital period(t) : map->t->dist",
                                    "stellar type 0",(int)stardata->star[0].stellar_type,
                                    "stellar type 1",(int)stardata->star[1].stellar_type,
                                    "time",(double)T,
                                    "log orbital period / years",(double)log10_Porb_years_binned
                                    );
                                Set_ensemble_count(
                                    "distributions",
                                    "log orbital separation : map->dist",
                                    "stellar type 0",(int)stardata->star[0].stellar_type,
                                    "stellar type 1",(int)stardata->star[1].stellar_type,
                                    "log separation",(double)a_binned
                                    );
                                Set_ensemble_count(
                                    "distributions",
                                    "log orbital separation(t) : map->t->dist",
                                    "stellar type 0",(int)stardata->star[0].stellar_type,
                                    "stellar type 1",(int)stardata->star[1].stellar_type,
                                    "time",(double)T,
                                    "log separation",(double)a_binned
                                    );
                                Set_ensemble_count(
                                    "distributions",
                                    "orbital eccentricity : map->dist",
                                    "stellar type 0",(int)stardata->star[0].stellar_type,
                                    "stellar type 1",(int)stardata->star[1].stellar_type,
                                    "eccentricity",(double)e_binned
                                    );
                                Set_ensemble_count(
                                    "distributions",
                                    "orbital eccentricity(t) : map->t->dist",
                                    "stellar type 0",(int)stardata->star[0].stellar_type,
                                    "stellar type 1",(int)stardata->star[1].stellar_type,
                                    "time",(double)T,
                                    "eccentricity",(double)e_binned
                                    );
                            } // binary now check
                        } // star->starnum == 0 check

                        if(star->stellar_type != MASSLESS_REMNANT)
                        {
                            /*
                             * Mass, luminosity, rotation functions for all
                             * non-remannts, both integrated and as a function
                             * of time
                             */
                            const double luminosity_binned = Bin_data(Safelog10(star->luminosity),0.1);
                            const double v_eq_binned = Bin_data(Safelog10(Max(1.0,star->v_eq)),0.1);
                            const double system_luminosity_binned =
                                Bin_data(Safelog10(system_luminosity),0.1);
                            const double mass_binned = Bin_data(Safelog10(star->mass),0.1);

                            /* luminosity functions */
                            if(Use_ensemble(STELLAR_POPULATIONS_ENSEMBLE_FILTER_LUMINOSITY_FUNCTIONS) == TRUE)
                            {
                                /* all resolved stars */
                                Set_ensemble_count(
                                    "distributions",
                                    "log luminosity : label->dist",
                                    "all resolved",
                                    "log luminosity",(double)luminosity_binned
                                    );
                                Set_ensemble_count(
                                    "distributions",
                                    "log luminosity(t) : label->t->dist",
                                    "all resolved",
                                    "time",(double)T,
                                    "log luminosity",(double)luminosity_binned
                                    );
                                /* by star number */
                                Set_ensemble_count(
                                    "distributions",
                                    "log luminosity : labelint->dist",
                                    "star",(int)star->starnum,
                                    "log luminosity",(double)luminosity_binned
                                    );
                                Set_ensemble_count(
                                    "distributions",
                                    "log luminosity(t) : labelint->t->dist",
                                    "star",(int)star->starnum,
                                    "time",(double)T,
                                    "log luminosity",(double)luminosity_binned
                                    );
                                /* by system label */
                                Set_ensemble_count(
                                    "distributions",
                                    "log luminosity : label->dist",
                                    (char*)system_label,
                                    "log luminosity",(double)luminosity_binned
                                    );
                                Set_ensemble_count(
                                    "distributions",
                                    "log luminosity(t) : label->t->dist",
                                    (char*)system_label,
                                    "time",(double)T,
                                    "log luminosity",(double)luminosity_binned
                                    );

                                /* unresolved */
                                Set_ensemble_count(
                                    "distributions",
                                    "log luminosity : label->dist",
                                    "all unresolved",
                                    "log luminosity",(double)system_luminosity_binned
                                    );
                                Set_ensemble_count(
                                    "distributions",
                                    "log luminosity(t) : label->t->dist",
                                    "all unresolved",
                                    "time",(double)T,
                                    "log luminosity",(double)system_luminosity_binned
                                    );
                            }

                            /* mass functions */
                            if(Use_ensemble(STELLAR_POPULATIONS_ENSEMBLE_FILTER_MASS_FUNCTIONS) == TRUE)
                            {
                                /* resolved */
                                Set_ensemble_count(
                                    "distributions",
                                    "log mass : label->dist",
                                    "all resolved",
                                    "log mass",(double)mass_binned
                                    );
                                Set_ensemble_count(
                                    "distributions",
                                    "log mass(t) : label->t->dist",
                                    "all resolved",
                                    "time",(double)T,
                                    "log mass",(double)mass_binned
                                    );
                                /* by star number */
                                Set_ensemble_count(
                                    "distributions",
                                    "log mass : labelint->dist",
                                    "star",(int)star->starnum,
                                    "log mass",(double)mass_binned
                                    );
                                Set_ensemble_count(
                                    "distributions",
                                    "log mass(t) : labelint->t->dist",
                                    "star",(int)star->starnum,
                                    "time",(double)T,
                                    "log mass",(double)mass_binned
                                    );
                                /* by system label */
                                Set_ensemble_count(
                                    "distributions",
                                    "log mass : label->dist",
                                    (char*)system_label,
                                    "log mass",(double)mass_binned
                                    );
                                Set_ensemble_count(
                                    "distributions",
                                    "log mass(t) : label->t->dist",
                                    (char*)system_label,
                                    "time",(double)T,
                                    "log mass",(double)mass_binned
                                    );

                                /* unresolved */
                                const double system_mass_binned =
                                    Bin_data(Safelog10(system_gravitational_mass),0.1);
                                Set_ensemble_count(
                                    "distributions",
                                    "log mass : label->dist",
                                    "all unresolved",
                                    "log mass",(double)system_mass_binned
                                    );
                                Set_ensemble_count(
                                    "distributions",
                                    "log mass(t) : label->t->dist",
                                    "all unresolved",
                                    "time",(double)T,
                                    "log mass",(double)system_mass_binned
                                    );
                            }

                            if(Use_ensemble(STELLAR_POPULATIONS_ENSEMBLE_FILTER_VEQ_FUNCTIONS) == TRUE)
                            {
                                /* resolved */
                                Set_ensemble_count(
                                    "distributions",
                                    "log v_eq : label->dist",
                                    "all resolved",
                                    "log v_eq",(double)v_eq_binned
                                    );
                                Set_ensemble_count(
                                    "distributions",
                                    "log v_eq(t) : label->t->dist",
                                    "all resolved",
                                    "time",(double)T,
                                    "log v_eq",(double)v_eq_binned
                                    );
                                /* by star number */
                                Set_ensemble_rate(
                                    "distributions",
                                    "log v_eq : labelint->dist",
                                    "star",(int)star->starnum,
                                    "log v_eq",(double)v_eq_binned
                                    );
                                Set_ensemble_rate(
                                    "distributions",
                                    "log v_eq(t) : labelint->t->dist",
                                    "star",(int)star->starnum,
                                    "time",(double)T,
                                    "log v_eq",(double)v_eq_binned
                                    );
                                /* by system label */
                                Set_ensemble_rate(
                                    "distributions",
                                    "log v_eq : label->dist",
                                    (char*)system_label,
                                    "log v_eq",(double)v_eq_binned
                                    );
                                Set_ensemble_rate(
                                    "distributions",
                                    "log v_eq(t) : label->t->dist",
                                    (char*)system_label,
                                    "time",(double)T,
                                    "log v_eq",(double)v_eq_binned
                                    );
                            }


                            if(stardata->model.merged == TRUE &&
                               Use_ensemble(STELLAR_POPULATIONS_ENSEMBLE_FILTER_MERGED) == TRUE)
                            {
                                /*
                                 * merged stars
                                 */
                                const double mass_binned_hires =
                                    Bin_data(Safelog10(star->mass),0.02); // NB high resolution
                                Set_ensemble_count(
                                    "distributions",
                                    "merged log mass : label->dist",
                                    "all resolved",
                                    "log mass",(double)mass_binned_hires
                                    );
                                Set_ensemble_count(
                                    "distributions",
                                    "merged log mass(t) : label->t->dist",
                                    "all resolved",
                                    "time",(double)T,
                                    "log mass",(double)mass_binned_hires
                                    );
                                Set_ensemble_count(
                                    "distributions",
                                    "merged log luminosity : label->dist",
                                    "all resolved",
                                    "log luminosity",(double)luminosity_binned
                                    );
                                Set_ensemble_count(
                                    "distributions",
                                    "merged log luminosity(t) : label->t->dist",
                                    "all resolved",
                                    "time",(double)T,
                                    "log luminosity",(double)luminosity_binned
                                    );
                                Set_ensemble_rate(
                                    "distributions",
                                    "log merged v_eq : label->dist",
                                    "all resolved",
                                    "log v_eq",(double)v_eq_binned
                                    );
                                Set_ensemble_rate(
                                    "distributions",
                                    "log merged v_eq(t) : label->t->dist",
                                    "all resolved",
                                    "time",(double)T,
                                    "log v_eq",(double)v_eq_binned
                                    );
                            } // merged check

                            if(star->starnum == 0 &&
                               Use_ensemble(STELLAR_POPULATIONS_ENSEMBLE_FILTER_ORBIT) == TRUE)
                            {
                                /*
                                 * orbital-period, separation and eccentricity distributions
                                 */
                                if(system_is_binary)
                                {
                                    Set_ensemble_count(
                                        "distributions",
                                        "mass ratio : label->dist",
                                        "all",
                                        "mass ratio",(double)q_binned
                                        );
                                    Set_ensemble_count(
                                        "distributions",
                                        "mass ratio(t) : label->t->dist",
                                        "all",
                                        "time",(double)T,
                                        "mass ratio",(double)q_binned
                                        );
                                    Set_ensemble_count(
                                        "distributions",
                                        "log orbital period(t) : label->t->dist",
                                        "all",
                                        "time",(double)T,
                                        "log orbital period / years",(double)log10_Porb_years_binned
                                        );
                                    Set_ensemble_count(
                                        "distributions",
                                        "log orbital period : label->dist",
                                        "all",
                                        "log orbital period / years",(double)log10_Porb_years_binned
                                        );
                                    Set_ensemble_count(
                                        "distributions",
                                        "log orbital period(t) : label->t->dist",
                                        "all",
                                        "time",(double)T,
                                        "log orbital period / years",(double)log10_Porb_years_binned
                                        );
                                    Set_ensemble_count(
                                        "distributions",
                                        "log orbital separation : label->dist",
                                        "all",
                                        "log orbital separation",(double)a_binned
                                        );
                                    Set_ensemble_count(
                                        "distributions",
                                        "log orbital separation(t) : label->t->dist",
                                        "all",
                                        "time",(double)T,
                                        "log orbital separation",(double)a_binned
                                        );
                                    Set_ensemble_count(
                                        "distributions",
                                        "orbital eccentricity : label->dist",
                                        "all",
                                        "orbital eccentricity",(double)e_binned
                                        );
                                    Set_ensemble_count(
                                        "distributions",
                                        "orbital eccentricity(t) : label->t->dist",
                                        "all",
                                        "time",(double)T,
                                        "orbital eccentricity",(double)e_binned
                                        );

                                }// binary now check
                            } // i==0 check

                            if(Use_ensemble(STELLAR_POPULATIONS_ENSEMBLE_FILTER_SPECTRAL_TYPES) == TRUE)
                            {
                                /*
                                 * spectral-type distributions
                                 */
                                const double spectype_binned = Bin_data(spectypef[star->starnum],0.1);

                                /* all stars individually */
                                Set_ensemble_count(
                                    "distributions",
                                    "spectral types : label->dist",
                                    "all",
                                    "spectral type",(double)spectype_binned
                                    );
                                Set_ensemble_count(
                                    "distributions",
                                    "spectral types(t) : label->t->dist",
                                    "all",
                                    "time",(double)T,
                                    "spectral type",(double)spectype_binned
                                    );

                                /* by star number */
                                Set_ensemble_count(
                                    "distributions",
                                    "spectral types : labelint->dist",
                                    "star",(int)star->starnum,
                                    "spectral type",(double)spectype_binned
                                    );
                                /* by star number */
                                Set_ensemble_count(
                                    "distributions",
                                    "spectral types(t) : labelint->t->dist",
                                    "star",(int)star->starnum,
                                    "time",(double)T,
                                    "spectral type",(double)spectype_binned
                                    );

                                if(stardata->model.sgl == TRUE)
                                {
                                    /* single stars */
                                    Set_ensemble_count(
                                        "distributions",
                                        "spectral types : label->dist",
                                        "single",
                                        "spectral type",(double)spectype_binned
                                        );
                                    Set_ensemble_count(
                                        "distributions",
                                        "spectral types(t) : label->t->dist",
                                        "single",
                                        "time",(double)T,
                                        "spectral type",(double)spectype_binned
                                        );

                                    if(stardata->model.merged == TRUE)
                                    {
                                        /*
                                         * merged single stars
                                         */
                                        Set_ensemble_count(
                                            "distributions",
                                            "spectral types : label->dist",
                                            "merged",
                                            "spectral type",(double)spectype_binned
                                            );
                                        Set_ensemble_count(
                                            "distributions",
                                            "spectral types(t) : label->t->dist",
                                            "time",(double)T,
                                            "merged",
                                            "spectral type",(double)spectype_binned
                                            );
                                    }
                                }
                                else if(born_binary == TRUE)
                                {
                                    /*
                                     * primary or secondary in a binary
                                     */
                                    Set_ensemble_count(
                                        "distributions",
                                        "spectral types : label->dist",
                                        (char*)system_label,
                                        "spectral type",(double)spectype_binned
                                        );
                                    Set_ensemble_count(
                                        "distributions",
                                        "spectral types(t) : label->t->dist",
                                        (char*)system_label,
                                        "time",(double)T,
                                        "spectral type",(double)spectype_binned
                                        );
                                }
                            }
                        }

                        if(system_is_binary &&
                           Use_ensemble(STELLAR_POPULATIONS_ENSEMBLE_FILTER_SPECTRAL_TYPES) == TRUE)
                        {
                            /*
                             * 2D spectral type distribution :
                             * primary vs secondary selected
                             * by spectral type
                             */
                            const double earliest_spectype_float_binned =
                                Bin_data(Min(spectypef[0], spectypef[1]), 0.1);
                            const double latest_spectype_float_binned =
                                Bin_data(Max(spectypef[0], spectypef[1]), 0.1);
                            Set_ensemble_count(
                                "distributions",
                                "spectral type floats : map",
                                "Earliest",
                                (double)earliest_spectype_float_binned,
                                "Latest",
                                (double)latest_spectype_float_binned);
                            Set_ensemble_count(
                                "distributions",
                                "spectral type floats(t) : t->map",
                                "time",(double)T,
                                "Earliest",
                                (double)earliest_spectype_float_binned,
                                "Latest",
                                (double)latest_spectype_float_binned);
                        }

                        /*
                         * Save the number of each stellar type and the luminosity in each stellar type
                         */
                        ensemble_weight[ENSEMBLE_LMMS+2*star->stellar_type]=1.0;
                        ensemble_weight[ENSEMBLE_LMMS_LUM+2*star->stellar_type]=star->luminosity;

                        /*
                         * Hybrid He-COWDs are a special case
                         */
                        if(star->stellar_type == COWD && star->hybrid_HeCOWD == TRUE)
                        {
                            ensemble_weight[ENSEMBLE_Hybrid_HeCOWD] = 1.0;
                            ensemble_weight[ENSEMBLE_Hybrid_HeCOWD_LUM] = star->luminosity;
                        }

                        Dprint("WR checks");
                        /*
                         * Check for WR stars
                         */
#ifdef NUCSYN
                        if(NUCLEAR_BURNING(star->stellar_type))
                        {
                            // avoid WDs, which would qualify as WO stars!

                            switch(nucsyn_WR_type(star))
                            {
                            case WR_WNL:
                                // WNL star
                                ensemble_weight[ENSEMBLE_WNL]=1.0;
                                ensemble_weight[ENSEMBLE_WNL_LUM]=star->luminosity;
                                break;
                            case WR_WNE:
                                // WNE star
                                ensemble_weight[ENSEMBLE_WNE]=1.0;
                                ensemble_weight[ENSEMBLE_WNE_LUM]=star->luminosity;
                                break;
                            case WR_WC:
                                // WC star
                                ensemble_weight[ENSEMBLE_WC]=1.0;
                                ensemble_weight[ENSEMBLE_WC_LUM]=star->luminosity;
                                break;
                            case WR_WO:
                                // WO star
                                ensemble_weight[ENSEMBLE_WO]=1.0;
                                ensemble_weight[ENSEMBLE_WO_LUM]=star->luminosity;
                                break;
                            }
                        }
#endif // NUCSYN

                        Dprint("B/RSG checks");
                        /*
                         * Check for BSG or RSG
                         */
#ifdef NUCSYN
                        if(Xsurf[XH1]>0.4)
#else
                        if(star->stellar_type <= TPAGB)
#endif //NUCSYN
                        {
                            if(l>4.9)
                            {
                                if(teff<3.66)
                                {
                                    ensemble_weight[ENSEMBLE_RSG]=1.0;
                                    ensemble_weight[ENSEMBLE_RSG_LUM]=star->luminosity;
                                }
                                else if(teff>3.9)
                                {
                                    ensemble_weight[ENSEMBLE_BSG]=1.0;
                                    ensemble_weight[ENSEMBLE_BSG_LUM]=star->luminosity;
                                }
                                else
                                {
                                    ensemble_weight[ENSEMBLE_ISG]=1.0;
                                    ensemble_weight[ENSEMBLE_ISG_LUM]=star->luminosity;
                                }
                            }

                            /*
                             * RSGs for Ben Davies (Liverpool, 2016)
                             * L>10^3 and Teff<5000 (log10Teff < 3.69897)
                             */
                            if(l>3.0 && teff<3.69897)
                            {
                                ensemble_weight[ENSEMBLE_DAVIES_RSG] = 1.0;
                                ensemble_weight[ENSEMBLE_DAVIES_RSG_LUM] = star->luminosity;
                            }
                        }

                        Dprint("SN checks");

                        /*
                         * Check for SNe: NB set to -1 so we do NOT multiply by dt
                         * when calculating populations
                         */
                        if(star->SN_type != -1 &&
                           star->SN_type != SN_NONE)
                        {
                            /*
                             * Find corresponding event log item corresponding to
                             * a SN in either star
                             */
                            Dprint("SN at t=%g star %d\n",
                                   stardata->model.time,star->starnum);
                            double KE_SN = 0.0;
                            double L_SN = 0.0;
                            double p_SN = 0.0;
                            Event_type etype = star->starnum == 0
                                ? BINARY_C_EVENT_SUPERNOVA0
                                : BINARY_C_EVENT_SUPERNOVA1;
                            struct binary_c_event_log_t * const elog =
                                Next_event_log(etype);
                            if(elog)
                            {
                                const Star_number exploder_number =
                                    elog->type == BINARY_C_EVENT_SUPERNOVA0 ? 0 :
                                    elog->type == BINARY_C_EVENT_SUPERNOVA1 ? 1 :
                                    -1;
                                const Boolean this_star = elog->label == star->starnum;

                                if(exploder_number >= 0 &&
                                   this_star &&
                                   elog->data != NULL)
                                {
                                    struct binary_c_new_supernova_event_t * const edata =
                                        (struct binary_c_new_supernova_event_t * const) elog->data;

                                    KE_SN = supernova_kinetic_energy(stardata,
                                                                     edata->pre_explosion_star);
                                    p_SN = supernova_momentum(stardata,
                                                              edata->pre_explosion_star);
                                    L_SN = supernova_luminous_energy(stardata,
                                                                     edata->pre_explosion_star);
                                }
                            }

                            /*
                             * Set supernova statistics:
                             * note that GRB_COLLAPSAR also implies SN_IBC
                             */
                            if(Use_ensemble(STELLAR_POPULATIONS_ENSEMBLE_FILTER_SUPERNOVAE) == TRUE)
                            {
                                Set_SN_stats_ensemble(SN_String(star->SN_type));
                            }

                            /*
                             * X macro to log all SN types quickly
                             *
                             * Note that the INDEX already has SN_ prefixed
                             */
#undef X
#define X(INDEX,STRING,CODE)                                            \
                            case INDEX :                                \
                                /* global count */                      \
                                ensemble_weight[ENSEMBLE_##INDEX] = 1.0; \
                                                                        \
                                /* dark/light counts */                 \
                                ensemble_weight[Is_zero(L_SN) ? ENSEMBLE_##INDEX##_DARK : ENSEMBLE_##INDEX##_BRIGHT] = 1.0; \
                                                                        \
                                /* kinetic power */                     \
                                ensemble_weight[ENSEMBLE_SUPERNOVA_KINETIC_POWER] = \
                                    ensemble_weight[ENSEMBLE_##INDEX##_KINETIC_POWER] = \
                                    KE_SN;                              \
                                                                        \
                                /* momentum */                          \
                                ensemble_weight[ENSEMBLE_SUPERNOVA_MOMENTUM] = \
                                    ensemble_weight[ENSEMBLE_##INDEX##_MOMENTUM]= \
                                    p_SN;                               \
                                                                        \
                                /* luminosity */                        \
                                ensemble_weight[ENSEMBLE_SUPERNOVA_LUMINOSITY] = \
                                    ensemble_weight[ENSEMBLE_##INDEX##_LUMINOSITY]= \
                                    L_SN;                               \
                                                                        \
                                break;

                            switch(star->SN_type)
                            {
                                _SN_TYPES_LIST(SN_,);
                            }

                            /*
                             * Special cases
                             */
                            if(star->SN_type == SN_GRB_COLLAPSAR)
                            {
                                /*
                                 * A collapsar is also a SN Ib/c, so set SN_IBC as well
                                 */
                                ensemble_weight[ENSEMBLE_SN_IBC]=1.0;
                                Set_SN_stats_ensemble(SN_String(SN_IBC));

#ifdef __DEPRECATED
                            case SN_TZ:
                                /*
                                 * Thorne-Zytkow object : deprecated!
                                 */
                                ensemble_weight[ENSEMBLE_SN_TZ]=1.0;
                                /*
                                 * TZ mass distribution
                                 *
                                 * We use the previous timestep's mass as the TZO mass.
                                 * This neglects mass that is lost in the common envelope
                                 * merger, so is likely only accurate to ~10%
                                 */
                                const double MTZ = previous_star->mass + previous_other_star->mass;
                                const Ensemble_index tzi =
                                    MTZ < 8.0 ? ENSEMBLE_TZ_M1_LT8 :
                                    MTZ > 30.0 ? ENSEMBLE_TZ_M1_GT30 :
                                    (ENSEMBLE_TZ_M1_8_9 +
                                     (Ensemble_index)( Max(0.0, Min(30.0 - 8.0,MTZ - 8.0 ))));
                                Dprint("TZ MASSES : PREV %g %g : NOW %g %g : MTZ = %g -> tz index %u\n",
                                       previous_star->mass,
                                       previous_other_star->mass,
                                       star->mass,
                                       other_star->mass,
                                       MTZ,
                                       tzi);
                                ensemble_weight[tzi] = 1.0;
                                break;
#endif // __DEPRECATED
                            }

                            // reset sn_last_time
                            star->sn_last_time=-1;
                        }

#ifdef XRAY_LUMINOSITY
                        Dprint("Xray binary checks");
                        /*
                         * X-ray binaries: luminosity is calculated in log_xray_binary
                         */
                        if(star->Xray_luminosity > TINY)
                        {
                            if(ON_MAIN_SEQUENCE(other_star->stellar_type)&&
                               (other_star->mass<=2.0))
                            {
                                /*
                                 * LMXB
                                 */
                                ensemble_weight[ENSEMBLE_XRAY_LMXB]=1.0;
                                ensemble_weight[ENSEMBLE_XRAY_LMXB_LUM]=star->Xray_luminosity;
                            }
                            else if(WHITE_DWARF(other_star->stellar_type))
                            {
                                /*
                                 * WDXB
                                 */
                                ensemble_weight[ENSEMBLE_XRAY_WDXB]=1.0;
                                ensemble_weight[ENSEMBLE_XRAY_WDXB_LUM]=star->Xray_luminosity;
                            }
                            else
                            {
                                /*
                                 * HMXB
                                 */
                                ensemble_weight[ENSEMBLE_XRAY_HMXB]=1.0;
                                ensemble_weight[ENSEMBLE_XRAY_HMXB_LUM]=star->Xray_luminosity;
                            }
                        }
#endif // XRAY_LUMINOSITY

#ifdef STELLAR_COLOURS
                        Dprint("Stellar colours");
                        if(star->stellar_type < NEUTRON_STAR)
                        {
                            /*
                             * The eldridge2015_magnitudes and gaia_magnitudes functions
                             * expect a pointer to an array of size STELLAR_MAGNITUDE_NUMBER.
                             * We can set these magnitudes directly into the ensemble array, which
                             * is set up to handle the magnitudes by making sure that
                             * ENSEMBLE_MAGNITUDE_U to ENSEMBLE_MAGNITUDE_GAIA_GRVS
                             * match (with an offset) the macros STELLAR_MAGNITUDE_* in
                             * src/stellar_magnitudes/stellar_colour_macros.h
                             *
                             * We can use the stellar_magnitudes wrapper function to
                             * call the subordinate magnitude functions.
                             */
                            double ** stellar_mags =
                                stardata->tmpstore->stellar_magnitudes;

                            Dprint("call magnitudes...\n");
                            if(stellar_mags[star->starnum] == NULL)
                            {
                                stellar_magnitudes(stardata,
                                                   star,
                                                   &stellar_mags[star->starnum]);
                            }
                            Dprint("called magnitudes\n");

                            Dprint("Set magnitudes' ensemble weights\n");
                            Stellar_magnitude_index m;
                            for(m=0; m<STELLAR_MAGNITUDE_NUMBER; m++)
                            {
                                /*
                                 * The ensemble weight should be a luminosity, not a magnitude,
                                 * because we can weight and add luminosities, but not magnitudes.
                                 * Conversion is done on output.
                                 */
                                const double magnitude = stellar_mags[star->starnum][m];
                                if(!Fequal(magnitude,STELLAR_MAGNITUDE_UNPHYSICAL))
                                {
                                    const Ensemble_index n = ENSEMBLE_MAGNITUDE_U + m;
                                    const double lum = luminosity_from_magnitude(magnitude);
                                    ensemble_weight[n] = lum;
                                }
                            }
                            Dprint("Have set magnitudes' ensemble weights\n");
                        }
#endif // STELLAR_COLOURS

                        Dprint("Spectral types");
                        /*
                         * Spectral types from Jaschek and Jaschek based on
                         * Lang (1992) "Astrophysical Data: Planets and Stars (Springer)
                         */
                        if(star->stellar_type > HERTZSPRUNG_GAP &&
                           star->stellar_type < HeWD)
                        {
                            if(l>4.9)
                            {
                                // supergiant
                                if(teff>log10(26000.0))
                                {
                                    // O
                                    ensemble_weight[ENSEMBLE_SPECTRAL_TYPE_O]=1.0;
                                    ensemble_weight[ENSEMBLE_SPECTRAL_TYPE_O_SUPERGIANT]=1.0;
                                    ensemble_weight[ENSEMBLE_SPECTRAL_TYPE_O_LUM]=star->luminosity;
                                }
                                else if(teff>log10(9730.0))
                                {
                                    // B
                                    ensemble_weight[ENSEMBLE_SPECTRAL_TYPE_B]=1.0;
                                    ensemble_weight[ENSEMBLE_SPECTRAL_TYPE_B_SUPERGIANT]=1.0;
                                    ensemble_weight[ENSEMBLE_SPECTRAL_TYPE_B_LUM]=star->luminosity;
                                }
                                else if(teff>log10(7700.0))
                                {
                                    // A
                                    ensemble_weight[ENSEMBLE_SPECTRAL_TYPE_A]=1.0;
                                    ensemble_weight[ENSEMBLE_SPECTRAL_TYPE_A_SUPERGIANT]=1.0;
                                    ensemble_weight[ENSEMBLE_SPECTRAL_TYPE_A_LUM]=star->luminosity;
                                }
                                else if(teff>log10(6550.0))
                                {
                                    // F
                                    ensemble_weight[ENSEMBLE_SPECTRAL_TYPE_F]=1.0;
                                    ensemble_weight[ENSEMBLE_SPECTRAL_TYPE_F_SUPERGIANT]=1.0;
                                    ensemble_weight[ENSEMBLE_SPECTRAL_TYPE_F_LUM]=star->luminosity;
                                }
                                else if(teff>log10(4420.0))
                                {
                                    // G
                                    ensemble_weight[ENSEMBLE_SPECTRAL_TYPE_G]=1.0;
                                    ensemble_weight[ENSEMBLE_SPECTRAL_TYPE_G_SUPERGIANT]=1.0;
                                    ensemble_weight[ENSEMBLE_SPECTRAL_TYPE_G_LUM]=star->luminosity;
                                }
                                else if(teff>log10(3650.0))
                                {
                                    // K
                                    ensemble_weight[ENSEMBLE_SPECTRAL_TYPE_K]=1.0;
                                    ensemble_weight[ENSEMBLE_SPECTRAL_TYPE_K_SUPERGIANT]=1.0;
                                    ensemble_weight[ENSEMBLE_SPECTRAL_TYPE_K_LUM]=star->luminosity;
                                }
                                else
                                {
                                    // M
                                    ensemble_weight[ENSEMBLE_SPECTRAL_TYPE_M]=1.0;
                                    ensemble_weight[ENSEMBLE_SPECTRAL_TYPE_M_SUPERGIANT]=1.0;
                                    ensemble_weight[ENSEMBLE_SPECTRAL_TYPE_M_LUM]=star->luminosity;
                                }
                            }
                            else
                            {
                                // giant
                                if(teff>log10(29000.0))
                                {
                                    // O
                                    ensemble_weight[ENSEMBLE_SPECTRAL_TYPE_O]=1.0;
                                    ensemble_weight[ENSEMBLE_SPECTRAL_TYPE_O_GIANT]=1.0;
                                    ensemble_weight[ENSEMBLE_SPECTRAL_TYPE_O_LUM]=star->luminosity;
                                }
                                else if(teff>log10(10100.0))
                                {
                                    // B
                                    ensemble_weight[ENSEMBLE_SPECTRAL_TYPE_B]=1.0;
                                    ensemble_weight[ENSEMBLE_SPECTRAL_TYPE_B_GIANT]=1.0;
                                    ensemble_weight[ENSEMBLE_SPECTRAL_TYPE_B_LUM]=star->luminosity;
                                }
                                else if(teff>log10(7150.0))
                                {
                                    // A
                                    ensemble_weight[ENSEMBLE_SPECTRAL_TYPE_A]=1.0;
                                    ensemble_weight[ENSEMBLE_SPECTRAL_TYPE_A_GIANT]=1.0;
                                    ensemble_weight[ENSEMBLE_SPECTRAL_TYPE_A_LUM]=star->luminosity;
                                }
                                else if(teff>log10(5850.0))
                                {
                                    // F
                                    ensemble_weight[ENSEMBLE_SPECTRAL_TYPE_F]=1.0;
                                    ensemble_weight[ENSEMBLE_SPECTRAL_TYPE_F_GIANT]=1.0;
                                    ensemble_weight[ENSEMBLE_SPECTRAL_TYPE_F_LUM]=star->luminosity;
                                }
                                else if(teff>log10(4750.0))
                                {
                                    // G
                                    ensemble_weight[ENSEMBLE_SPECTRAL_TYPE_G]=1.0;
                                    ensemble_weight[ENSEMBLE_SPECTRAL_TYPE_G_GIANT]=1.0;
                                    ensemble_weight[ENSEMBLE_SPECTRAL_TYPE_G_LUM]=star->luminosity;
                                }
                                else if(teff>log10(3800.0))
                                {
                                    // K
                                    ensemble_weight[ENSEMBLE_SPECTRAL_TYPE_K]=1.0;
                                    ensemble_weight[ENSEMBLE_SPECTRAL_TYPE_K_GIANT]=1.0;
                                    ensemble_weight[ENSEMBLE_SPECTRAL_TYPE_K_LUM]=star->luminosity;
                                }
                                else
                                {
                                    // M
                                    ensemble_weight[ENSEMBLE_SPECTRAL_TYPE_M]=1.0;
                                    ensemble_weight[ENSEMBLE_SPECTRAL_TYPE_M_GIANT]=1.0;
                                    ensemble_weight[ENSEMBLE_SPECTRAL_TYPE_M_LUM]=star->luminosity;
                                }
                            }
                        }
                        else if(star->stellar_type < BLACK_HOLE)
                        {
                            // everything else (not a BH or massless remnant) is a dwarf
                            if(teff>log10(30000.0))
                            {
                                // O
                                ensemble_weight[ENSEMBLE_SPECTRAL_TYPE_O]=1.0;
                                ensemble_weight[ENSEMBLE_SPECTRAL_TYPE_O_DWARF]=1.0;
                                ensemble_weight[ENSEMBLE_SPECTRAL_TYPE_O_LUM]=star->luminosity;
                            }
                            else if(teff>log10(9520.0))
                            {
                                // B
                                ensemble_weight[ENSEMBLE_SPECTRAL_TYPE_B]=1.0;
                                ensemble_weight[ENSEMBLE_SPECTRAL_TYPE_B_DWARF]=1.0;
                                ensemble_weight[ENSEMBLE_SPECTRAL_TYPE_B_LUM]=star->luminosity;
                            }
                            else if(teff>log10(7200.0))
                            {
                                // A
                                ensemble_weight[ENSEMBLE_SPECTRAL_TYPE_A]=1.0;
                                ensemble_weight[ENSEMBLE_SPECTRAL_TYPE_A_DWARF]=1.0;
                                ensemble_weight[ENSEMBLE_SPECTRAL_TYPE_A_LUM]=star->luminosity;
                            }
                            else if(teff>log10(6030.0))
                            {
                                // F
                                ensemble_weight[ENSEMBLE_SPECTRAL_TYPE_F]=1.0;
                                ensemble_weight[ENSEMBLE_SPECTRAL_TYPE_F_DWARF]=1.0;
                                ensemble_weight[ENSEMBLE_SPECTRAL_TYPE_F_LUM]=star->luminosity;
                            }
                            else if(teff>log10(5250.0))
                            {
                                // G
                                ensemble_weight[ENSEMBLE_SPECTRAL_TYPE_G]=1.0;
                                ensemble_weight[ENSEMBLE_SPECTRAL_TYPE_G_DWARF]=1.0;
                                ensemble_weight[ENSEMBLE_SPECTRAL_TYPE_G_LUM]=star->luminosity;
                            }
                            else if(teff>log10(3850.0))
                            {
                                // K
                                ensemble_weight[ENSEMBLE_SPECTRAL_TYPE_K]=1.0;
                                ensemble_weight[ENSEMBLE_SPECTRAL_TYPE_K_DWARF]=1.0;
                                ensemble_weight[ENSEMBLE_SPECTRAL_TYPE_K_LUM]=star->luminosity;
                            }
                            else
                            {
                                // M
                                ensemble_weight[ENSEMBLE_SPECTRAL_TYPE_M]=1.0;
                                ensemble_weight[ENSEMBLE_SPECTRAL_TYPE_M_DWARF]=1.0;
                                ensemble_weight[ENSEMBLE_SPECTRAL_TYPE_M_LUM]=star->luminosity;
                            }
                        }

#ifdef NUCSYN

                        /* C/O by number */
                        const Abundance C_over_O =
                            nucsyn_elemental_abundance("C",Xsurf,stardata,stardata->store)/
                            nucsyn_elemental_abundance("O",Xsurf,stardata,stardata->store);

                        if(star->stellar_type < HeMS)
                        {
                            if(Fequal(ensemble_weight[ENSEMBLE_SPECTRAL_TYPE_M],1.0))
                            {
                                /*
                                 * M-type stars can be C stars
                                 */
                                if(C_over_O > 1.05)
                                {
                                    Set_spectral_type_ensemble_weight(C);

                                    /*
                                     * Which might also be J-stars is C12/C13 < 12
                                     */
                                    if(((Xsurf[XC12]/12.0)/
                                        (Xsurf[XC13]/13.0))<12.0)
                                    {
                                        Set_spectral_type_ensemble_weight(J);

                                        if(Use_ensemble(STELLAR_POPULATIONS_ENSEMBLE_FILTER_CHEMICALLY_PECULIAR) == TRUE)
                                        {
                                            Set_ensemble_count(
                                                "distributions",
                                                "log J-star luminosity : label->dist",
                                                "all resolved",
                                                "log luminosity",Bin_data(log10(star->luminosity +
                                                                                TINY),0.1)
                                                );
                                            Set_ensemble_count(
                                                "distributions",
                                                "log J-star luminosity(t) : label->t->dist",
                                                "all resolved",
                                                "time",(double)T,
                                                "log luminosity",Bin_data(log10(star->luminosity +
                                                                                TINY),0.1)
                                                );
                                        }
                                    }

                                    if(Use_ensemble(STELLAR_POPULATIONS_ENSEMBLE_FILTER_CHEMICALLY_PECULIAR) == TRUE)
                                    {
                                        Set_ensemble_count(
                                            "distributions",
                                            "log C-star luminosity : label->dist",
                                            "all resolved",
                                            "log luminosity",Bin_data(log10(star->luminosity +
                                                                            TINY),0.1)
                                            );
                                        Set_ensemble_count(
                                            "distributions",
                                            "log C-star luminosity(t) : label->t->dist",
                                            "all resolved",
                                            "time",(double)T,
                                            "log luminosity",Bin_data(log10(star->luminosity +
                                                                            TINY),0.1)
                                            );
                                    }
                                }
                                else if(C_over_O > 0.95)
                                {
                                    Set_spectral_type_ensemble_weight(S);

                                    if(Use_ensemble(STELLAR_POPULATIONS_ENSEMBLE_FILTER_CHEMICALLY_PECULIAR) == TRUE)
                                    {
                                        Set_ensemble_count(
                                            "distributions",
                                            "log S-star luminosity : label->dist",
                                            "all resolved",
                                            "log luminosity",Bin_data(log10(star->luminosity +
                                                                            TINY),0.1)
                                            );
                                        Set_ensemble_count(
                                            "distributions",
                                            "log S-star luminosity(t) : label->t->dist",
                                            "all resolved",
                                            "time",(double)T,
                                            "log luminosity",Bin_data(log10(star->luminosity +
                                                                            TINY),0.1)
                                            );
                                    }
                                }
                            }
                            else if(Fequal(ensemble_weight[ENSEMBLE_SPECTRAL_TYPE_K],1.0)&&
                                    ((star->stellar_type==GIANT_BRANCH) ||
                                     (star->stellar_type==CHeB))
                                )
                            {
                                /*
                                 * K-type giants which are C-rich are assumed to be R-stars
                                 */
                                if(C_over_O > 1.0)
                                {
                                    Set_spectral_type_ensemble_weight(R);

                                    if(Use_ensemble(STELLAR_POPULATIONS_ENSEMBLE_FILTER_CHEMICALLY_PECULIAR) == TRUE)
                                    {
                                        Set_ensemble_count(
                                            "distributions",
                                            "log R-star luminosity : label->dist",
                                            "all resolved",
                                            "log luminosity",Bin_data(log10(star->luminosity +
                                                                            TINY),0.1)
                                            );
                                        Set_ensemble_count(
                                            "distributions",
                                            "log R-star luminosity(t) : label->t->dist",
                                            "all resolved",
                                            "time",(double)T,
                                            "log luminosity",Bin_data(log10(star->luminosity +
                                                                            TINY),0.1)
                                            );
                                    }
                                }
                            }

                            if((nucsyn_elemental_abundance("Ba",Xsurf,stardata,stardata->store)/
                                nucsyn_elemental_abundance("Ba",stardata->preferences->zero_age.XZAMS,stardata,stardata->store)>2.0))
                            {
                                /*
                                 * Red G/K giants are classical barium stars
                                 */
                                if((Fequal(ensemble_weight[ENSEMBLE_SPECTRAL_TYPE_G],1.0) ||
                                    Fequal(ensemble_weight[ENSEMBLE_SPECTRAL_TYPE_K],1.0)) &&
                                   (star->stellar_type>=GIANT_BRANCH)&&
                                   (star->stellar_type<HeMS))
                                {
                                    Set_spectral_type_ensemble_weight(Ba);

                                    if(Use_ensemble(STELLAR_POPULATIONS_ENSEMBLE_FILTER_CHEMICALLY_PECULIAR) == TRUE)
                                    {
                                        Set_ensemble_count(
                                            "distributions",
                                            "log Ba-giant luminosity : label->dist",
                                            "all resolved",
                                            "log luminosity",Bin_data(log10(star->luminosity +
                                                                            TINY),0.1)
                                            );
                                        Set_ensemble_count(
                                            "distributions",
                                            "log Ba-giant luminosity(t) : label->t->dist",
                                            "all resolved",
                                            "time",(double)T,
                                            "log luminosity",Bin_data(log10(star->luminosity +
                                                                            TINY),0.1)
                                            );
                                    }
                                }
                                else if(star->stellar_type <= MAIN_SEQUENCE &&
                                        Use_ensemble(STELLAR_POPULATIONS_ENSEMBLE_FILTER_CHEMICALLY_PECULIAR) == TRUE)
                                {
                                    /*
                                     * Main-sequence barium dwarfs
                                     */
                                    Set_ensemble_count(
                                        "distributions",
                                        "log Ba-dwarf luminosity : label->dist",
                                        "all resolved",
                                        "log luminosity",Bin_data(log10(star->luminosity +
                                                                        TINY),0.1)
                                        );
                                    Set_ensemble_count(
                                        "distributions",
                                        "log Ba-dwarf luminosity(t) : label->t->dist",
                                        "all resolved",
                                        "time",(double)T,
                                        "log luminosity",Bin_data(log10(star->luminosity +
                                                                        TINY),0.1)
                                        );
                                }
                            }

                            /*
                             * Look for Tc-rich stars which are GB/CHeB
                             */
                            if((star->stellar_type==GIANT_BRANCH || star->stellar_type==CHeB) &&
                               ((nucsyn_elemental_abundance("Tc",Xsurf,stardata,stardata->store)/
                                 nucsyn_elemental_abundance("Tc",stardata->preferences->zero_age.XZAMS,stardata,stardata->store)>2.0)))
                            {
                                Set_spectral_type_ensemble_weight(Tc);

                                if(star->stellar_type <= MAIN_SEQUENCE &&
                                   Use_ensemble(STELLAR_POPULATIONS_ENSEMBLE_FILTER_CHEMICALLY_PECULIAR) == TRUE)
                                {
                                    Set_ensemble_count(
                                        "distributions",
                                        "log Tc-giant luminosity : label->dist",
                                        "all resolved",
                                        "log luminosity",Bin_data(log10(star->luminosity +
                                                                        TINY),0.1)
                                        );
                                    Set_ensemble_count(
                                        "distributions",
                                        "log Tc-giant luminosity(t) : label->t->dist",
                                        "all resolved",
                                        "time",(double)T,
                                        "log luminosity",Bin_data(log10(star->luminosity +
                                                                        TINY),0.1)
                                        );
                                }
                            }
                        }

                        /*
                         * Look for extrinsic carbon stars:
                         * Must be pre-1TP, C/O>1 and have WD companion so we can
                         * definitely associate them with AGB mass transfer
                         */
                        if((star->stellar_type<=TPAGB) &&
                           (C_over_O>1.0) &&
                           (WHITE_DWARF(other_star->stellar_type)))
                        {
                            if(ON_MAIN_SEQUENCE(star->stellar_type))
                            {
                                ensemble_weight[ENSEMBLE_SPECTRAL_TYPE_EXTRINSIC_DWARF_C]=1.0;
                                ensemble_weight[ENSEMBLE_SPECTRAL_TYPE_EXTRINSIC_DWARF_C_LUM]=star->luminosity;

                                if(Use_ensemble(STELLAR_POPULATIONS_ENSEMBLE_FILTER_CHEMICALLY_PECULIAR) == TRUE)
                                {
                                    Set_ensemble_count(
                                        "distributions",
                                        "log C-dwarf luminosity : label->dist",
                                        "all resolved",
                                        "log luminosity",Bin_data(log10(star->luminosity +
                                                                        TINY),0.1)
                                        );
                                    Set_ensemble_count(
                                        "distributions",
                                        "log C-dwarf luminosity(t) : label->t->dist",
                                        "all resolved",
                                        "time",(double)T,
                                        "log luminosity",Bin_data(log10(star->luminosity +
                                                                        TINY),0.1)
                                        );
                                }
                            }
                            else
                            {
                                /*
                                 * C-rich pre-TPAGB stars are "extrinsic"
                                 */
                                if(star->num_thermal_pulses_since_mcmin<0.5)
                                {
                                    ensemble_weight[ENSEMBLE_SPECTRAL_TYPE_EXTRINSIC_GIANT_C]=1.0;
                                    ensemble_weight[ENSEMBLE_SPECTRAL_TYPE_EXTRINSIC_GIANT_C_LUM]=star->luminosity;

                                    if(Use_ensemble(STELLAR_POPULATIONS_ENSEMBLE_FILTER_CHEMICALLY_PECULIAR) == TRUE)
                                    {
                                        Set_ensemble_count(
                                            "distributions",
                                            "log extrinsic C-star luminosity : label->dist",
                                            "all resolved",
                                            "log luminosity",Bin_data(log10(star->luminosity +
                                                                            TINY),0.1)
                                            );
                                        Set_ensemble_count(
                                            "distributions",
                                            "log extrinsic C-star luminosity(t) : label->t->dist",
                                            "all resolved",
                                            "time",(double)T,
                                            "log luminosity",Bin_data(log10(star->luminosity +
                                                                            TINY),0.1)
                                            );
                                    }
                                }
                            }
                        }

                        if(Use_ensemble(STELLAR_POPULATIONS_ENSEMBLE_FILTER_EMP) == TRUE)
                        {
                            ensemble_EMP(stardata,
                                         system_is_binary,
                                         dtp,
                                         star,
                                         Xsurf,
                                         T,
                                         log10_Porb_years_binned);
                        }
#endif // NUCSYN


                        /*
                         * Classify WR stars by Hurley Pols Tout 2002 prescription:
                         * Assume WDs cannot be WR stars (dubious?)
                         */
                        if(star_is_WR(star))
                        {
                            ensemble_weight[ENSEMBLE_SPECTRAL_TYPE_WR]=1.0;
                            ensemble_weight[ENSEMBLE_SPECTRAL_TYPE_WR_LUM]=star->luminosity;
                        }

                        /*
                         * Luminous blue variable
                         */
                        if(star_is_LBV(stardata,star))
                        {
                            ensemble_weight[ENSEMBLE_LBV] = 1.0;
                            ensemble_weight[ENSEMBLE_LBV_LUM] = star->luminosity;
                        }

                        /*
                         * FK Comae stars :
                         *
                         * These are G and K giants that are rapidly rotating.
                         * What does this mean? For some reason I had v>20km/s in here,
                         * but perhaps > 50% critical rotation is more useful?
                         *
                         * Korhonen et al. 1999 have v sin i = 155km/s and G5 III type
                         * for FK Com. They also define FK Com stars as single systems
                         * rather than spun-up binaries (radial velocity variations are
                         * < 3km/s [Heuenemoerder+ 1993] or 5km/s [McCarthy and Ramsey 1984].
                         * Korhonen provide a nice review of the class definition.
                         *
                         * The class includes FK Com, HD 199178, possibly HD 32918, I-1 in NGC188,
                         * and 1E 1751+7046.
                         */
                        if(
                            (Fequal(ensemble_weight[ENSEMBLE_SPECTRAL_TYPE_G],1.0) ||
                             Fequal(ensemble_weight[ENSEMBLE_SPECTRAL_TYPE_K],1.0)) &&
                            GIANT_LIKE_STAR(star->stellar_type) &&
                            star->v_eq_ratio > 0.5 &&
                            stardata->model.sgl == TRUE
                            )
                        {
                            ensemble_weight[ENSEMBLE_FK_COMAE] = 1.0;
                        }


                        /*
                         * Be-star X-ray binaries according to Boyuan Liu's prescription
                         */
                        if(cs_Be_criterion(stardata,
                                           star,
                                           other_star) == TRUE)
                        {
                            ensemble_weight[ENSEMBLE_Be] = 1.0;

                            if(cs_XRB_duty_cycle_criterion(stardata,star,other_star) == TRUE)
                            {
                                ensemble_weight[ENSEMBLE_Be_XRB] = 1.0;
                            }
                        }

                        Dprint("Special binaries");
                        /*
                         * Special binary stellar systems
                         */
                        if(star->starnum == 0) // only check when starnum==0 to avoid double counting
                        {
                            /*
                             * Check for double degenerates
                             */
                            if(WHITE_DWARF(star->stellar_type))
                            {
                                /*
                                 * Primary is a white dwarf
                                 */
                                if(WHITE_DWARF(other_star->stellar_type))
                                {
                                    /*
                                     * WD WD system
                                     */
                                    ensemble_weight[ENSEMBLE_WDWD_DD]=1.0;
                                }
                                else if(other_star->stellar_type==NS)
                                {
                                    ensemble_weight[ENSEMBLE_WDNS_DD]=1.0;
                                }
                                else if(other_star->stellar_type==BH)
                                {
                                    ensemble_weight[ENSEMBLE_WDBH_DD]=1.0;
                                }
                            }
                            else if(star->stellar_type==NS)
                            {
                                if(other_star->stellar_type==NS)
                                {
                                    ensemble_weight[ENSEMBLE_NSNS_DD]=1.0;
                                }
                                else if(other_star->stellar_type==BH)
                                {
                                    ensemble_weight[ENSEMBLE_NSBH_DD]=1.0;
                                }
                            }
                            else if(star->stellar_type==BH)
                            {
                                if(other_star->stellar_type==BH)
                                {
                                    ensemble_weight[ENSEMBLE_BHBH_DD]=1.0;
                                }
                            }
                        }

                        /*
                         * Cataclysmic variables
                         */
                        Dprint("Cataclysmic variables");
                        if(WHITE_DWARF(star->stellar_type)&&
                           (other_star->radius > other_star->roche_radius)&&
                           (other_star->stellar_type<COWD))
                        {
                            /*
                             * CV
                             */
                            if(other_star->stellar_type<=MAIN_SEQUENCE)
                            {
                                /*
                                 * Donor on MS: classical CV
                                 */
                                ensemble_weight[ENSEMBLE_CLASSICAL_CV]=1.0;
                            }
                            else if(other_star->stellar_type==HERTZSPRUNG_GAP)
                            {
                                /*
                                 * Donor is HG star: GK Per CV
                                 */
                                ensemble_weight[ENSEMBLE_GK_PER_CV]=1.0;
                            }
                            else if((other_star->stellar_type>=GIANT_BRANCH)&&
                                    (other_star->stellar_type<=TPAGB))
                            {
                                /*
                                 * Donor is helium-burning: Symbiotic star
                                 */
                                ensemble_weight[ENSEMBLE_SYMBIOTIC_CV]=1.0;
                            }
                            else if(NAKED_HELIUM_STAR(other_star->stellar_type))
                            {
                                /*
                                 * AM CVn systems
                                 */
                                ensemble_weight[ENSEMBLE_AMCVN]=1.0;
                            }
                        }

                        /*
                         * Hot subdwarfs:
                         * these are either low-mass helium stars
                         * or
                         * very stripped CHeB stars
                         */
                        if(
                            (NAKED_HELIUM_STAR(star->stellar_type) ||
                             (star->stellar_type==CHeB &&
                              envelope_mass(star) < 0.01))
                            &&
                            star->mass < 1.4
                            )
                        {
                            /*
                             * sdB : 20-40kK
                             * sdO : 40-100kK
                             */
                            if(teff > log10(20e3) && teff < log10(40e3))
                            {
                                ensemble_weight[ENSEMBLE_SDB]=1.0;
                            }
                            else if(teff > log10(100e3))
                            {
                                ensemble_weight[ENSEMBLE_SDO]=1.0;
                            }
                        }


                        /*
                         * The various types of Algol systems
                         */
                        Dprint("Algols");
                        if(ON_MAIN_SEQUENCE(star->stellar_type)&&
                           (other_star->radius > other_star->roche_radius))
                        {
                            if(other_star->mass/star->mass > 1.0)
                            {
                                ensemble_weight[ENSEMBLE_PRE_ALGOL]=1.0;
                            }
                            else
                            {
                                if(ON_MAIN_SEQUENCE(other_star->stellar_type))
                                {
                                    ensemble_weight[ENSEMBLE_MS_ALGOL]=1.0;
                                }
                                else
                                {
                                    if(star->mass<=1.25)
                                    {
                                        ensemble_weight[ENSEMBLE_COLD_ALGOL]=1.0;
                                    }
                                    else
                                    {
                                        ensemble_weight[ENSEMBLE_HOT_ALGOL]=1.0;
                                    }
                                }
                            }
                        }

                        Dprint("Blue stragglers");
                        /*
                         * Blue straggler systems (set in acquire_stellar_parameters...)
                         */
                        if(star->starnum == 0 &&
                           (stardata->star[0].blue_straggler==TRUE ||
                            stardata->star[1].blue_straggler==TRUE))
                        {
                            ensemble_weight[ENSEMBLE_BLUE_STRAGGLER]=1.0;

                            if(system_is_observable_binary == TRUE)
                            {
                                ensemble_weight[ENSEMBLE_BINARY_BLUE_STRAGGLER]=1.0;
                            }
                            else
                            {
                                ensemble_weight[ENSEMBLE_SINGLE_BLUE_STRAGGLER]=1.0;
                            }
                        }

                        Dprint("Low-mass WDs");
                        /*
                         * Low-mass white dwarfs
                         */
                        if(star->mass<0.5)
                        {
                            if(star->stellar_type==HeWD)
                            {
                                ensemble_weight[ENSEMBLE_LOW_MASS_He_WD]=1.0;
                            }
                            else if(star->stellar_type==COWD)
                            {
                                ensemble_weight[ENSEMBLE_LOW_MASS_CO_WD]=1.0;
                            }
                        }

                        Dprint("Symbiotic stars");
                        /*
                         * Symbiotic stars
                         */

                        if(star->stellar_type<=ONeWD)
                        {

                            if((star->accretion_luminosity>10.0)||
                               (star->accretion_luminosity>0.01*other_star->luminosity))
                            {
                                if(other_star->stellar_type==TPAGB)
                                {
                                    /*
                                     * D-type symbiotic
                                     */
                                    ensemble_weight[ENSEMBLE_D_TYPE_SYMBIOTIC]=1.0;
                                }
                                else if(other_star->stellar_type<TPAGB)
                                {
                                    /*
                                     * S-type symbiotic
                                     */
                                    ensemble_weight[ENSEMBLE_S_TYPE_SYMBIOTIC]=1.0;
                                }
                            }
                        }

                        if(star->starnum == 0)
                        {
                            /*
                             * Mergers and post-common envelope systems
                             */
                            if(stardata->model.comenv_type != 0)
                            {
                                /*
                                 * If stardata->model.comenv_type < 0, then the
                                 * type is negative, meaning there was a merger.
                                 * Otherwise, the post-CE system is detached.
                                 */

                                int type = stardata->model.comenv_type;
                                if(type<0)
                                {
                                    /* merger */
                                    type = - stardata->model.comenv_type;
                                    ensemble_weight[ENSEMBLE_COMENV_MERGER] = 1.0;
                                }
                                else
                                {
                                    /* detached */
                                    type = stardata->model.comenv_type;
                                    ensemble_weight[ENSEMBLE_COMENV_DETACHED] = 1.0;
                                }
                                ensemble_weight[type]=1.0;
                            }
                        }

                        /* merger rates */
                        const Boolean newly_merged =
                            /* one of the flags should be set */
                            (
                                stardata->model.merged == TRUE ||
                                stardata->model.coalesce == TRUE ||
                                stardata->model.comenv_type !=0
                                ) &&

                            /* must change from binary to single */
                            stardata->model.sgl==TRUE &&
                            previous_stardata->model.sgl==FALSE &&

                            /* companion must be a massless remnant and wasn't last time */
                            other_star->stellar_type==MASSLESS_REMNANT &&
                            previous_other_star->stellar_type!=MASSLESS_REMNANT &&
                            Is_zero(other_star->mass) &&
                            Is_not_zero(previous_other_star->mass);

                        const Boolean new_transient =
                            /* haven't already logged */
                            have_logged_transient == FALSE &&
                            (
                                /* comenv and this is the donor */
                                (stardata->model.comenv_type != 0 &&
                                 stardata->model.comenv_overflower == star->starnum)
                                ||
                                /* or a straightforward merger */
                                newly_merged
                                );

                        /*
                         * detect new transients: we check transient hasn't
                         * been set already
                         */
                        if(newly_merged)
                        {
                            ensemble_merger(stardata,
                                            star,
                                            previous_star,
                                            previous_other_star,
                                            ensemble_weight,
                                            prevspectypef);
                        }

                        if(new_transient)
                        {
                            /*
                            printf("-> TRANSIENT %d %d %d %d\n",
                                     have_logged_transient == FALSE,
                                     stardata->model.comenv_type != 0,
                                     stardata->model.comenv_overflower == star->starnum,
                                     newly_merged);
                            */

                            if(Use_ensemble(STELLAR_POPULATIONS_ENSEMBLE_FILTER_TRANSIENTS) == TRUE ||
                               Use_ensemble(STELLAR_POPULATIONS_ENSEMBLE_FILTER_TRANSIENTS_BOKEH) == TRUE)
                            {
                                /*
                                 * The primary star, as far as the transient is concerned,
                                 * is the star that transfers mass.
                                 *
                                 * Usually, this is the star that was overflowing its Roche
                                 * lobe on the previous step. However, there might not be
                                 * a previous step of RLOF, in which case choose the star
                                 * that was closest to its Roche lobe (by R/RL).
                                 *
                                 */

                                /*
                                 * To call the ensemble_transient function,
                                 * star should be the primary, i.e. the donor.
                                 */
                                ensemble_transient(stardata,
                                                   T,
                                                   p,
                                                   star,
                                                   previous_primary_star,
                                                   previous_secondary_star,
                                                   iprimary,
                                                   system_was_binary,
                                                   system_is_binary);
                            }
                            have_logged_transient = TRUE;
                        }


                        if(star->starnum == iprimary)
                        {
#ifdef NUCSYN
                            if(preferences->ensemble_filter[STELLAR_POPULATIONS_ENSEMBLE_FILTER_CHEMISTRY])
                            {
                                ensemble_chemistry(stardata,
                                                   dtp,
                                                   T,
                                                   iprimary,
                                                   system_is_binary);
                            }
#endif//NUCSYN

                            if(Use_ensemble(STELLAR_POPULATIONS_ENSEMBLE_FILTER_HRD) == TRUE ||
                               Use_ensemble(STELLAR_POPULATIONS_ENSEMBLE_FILTER_HRD_BOKEH) == TRUE ||
                               Use_ensemble(STELLAR_POPULATIONS_ENSEMBLE_FILTER_HRD_PERIOD_DISTRIBUTIONS) == TRUE ||
                               Use_ensemble(STELLAR_POPULATIONS_ENSEMBLE_FILTER_HRD_TIME_SLICES) == TRUE ||
                               Use_ensemble(STELLAR_POPULATIONS_ENSEMBLE_FILTER_HRD_PERIOD_DISTRIBUTIONS_TIME_SLICES) == TRUE ||
                               Use_ensemble(STELLAR_POPULATIONS_ENSEMBLE_FILTER_HRD_MASS_DISTRIBUTIONS) == TRUE
#ifdef STELLAR_COLOURS
                               || CMD == TRUE
#endif // STELLAR_COLOURS
                                )
                            {
                                /*
                                 * Hertzsprung-Russell and logg-logTeff diagrammes
                                 */
                                ensemble_HRD(stardata,
                                             dtp,
                                             T,
                                             iprimary,
                                             -1,
                                             FALSE,
                                             system_is_binary,
#ifdef STELLAR_COLOURS
                                             CMD,
#endif // STELLAR_COLOURS
                                             NULL,
                                             NULL
                                    );
                            }

                            if(Use_ensemble(STELLAR_POPULATIONS_ENSEMBLE_FILTER_HRD_CLUSTER) == TRUE)
                            {
                                /*
                                 * Stellar-cluster ensemble for work with
                                 * Ted von Hippel (2024)
                                 */
                                ensemble_HRD_cluster(stardata,
                                                     p,
                                                     dtp,
                                                     T,
                                                     iprimary,
                                                     -1,
                                                     FALSE,
                                                     system_is_binary,
#ifdef STELLAR_COLOURS
                                                     CMD,
#endif // STELLAR_COLOURS
                                                     NULL,
                                                     NULL
                                    );
                            }
                        }

                        /*
                         * Sources of ejected/gained mass
                         *
                         * ENSEMBLE_MDOT + 0     is the total, while
                         * ENSEMBLE_MDOT + 1 + s is each individual source
                         * (the sources start at 0 hence we need the +1)
                         */
                        {

                            Yield_source s;
                            for(s = 0;
                                s < SOURCE_NUMBER;
                                s++)
                            {
                                ensemble_weight[ENSEMBLE_MDOT + 0] += stardata->model.ensemble_yield[s];
                                ensemble_weight[ENSEMBLE_MDOT + 1 + (Ensemble_index)s] += stardata->model.ensemble_yield[s] ;
                                stardata->model.ensemble_yield[s] = 0.0;
                            }
                        }

                        /*
                         * Summation and output
                         */
                        Dprint("Summation and output");

#ifdef STELLAR_POPULATIONS_ENSEMBLE_SPARSE
                        if(Is_zero(stardata->model.time))
                        {
                            /*
                             * First timestep
                             */
                            lastensembleouttime =
                                preferences->ensemble_logtimes==TRUE ?
                                log10(preferences->ensemble_startlogtime) :
                                0.0;
                        }

                        /*
                         * add up the ensemble store for this star
                         */

                        double * const es = estore[star->starnum];

                        for(k=0;k<ENSEMBLE_NUMBER;k++)
                        {
                            es[k] +=

                                ensemble_weight[k] *

                                (
                                    /* this is a rate: just count probabilities */
                                    unlikely(ensemble_type[k] == ENSEMBLE_TYPE_RATE) ? p :

                                    /* raw data: do not multiply by timestep (for testing only!) */
                                    unlikely(ensemble_type[k] == ENSEMBLE_TYPE_RAW)  ? ensemble_weight[k] :

                                    /* everything else: weight by timestep and probability */
                                    dtp
                                    );
                        }

                        /*************************************
                         *
                         * Ensemble output if trigger == TRUE.
                         *
                         *************************************/
                        if(trigger == TRUE)
                        {
                            if(preferences->ensemble_legacy_ensemble == TRUE)
                            {
                                Printf("STELLARPOPENSEMBLE%d__ %g ",
                                       star->starnum,
                                       T);
                            }

                            /*
                             * We want to output the value of x taken as a
                             * time-averaged mean over the time bin.
                             *
                             * The values which are in ensemble_store are
                             *
                             *  sum(x * delta t)
                             *
                             * where delta t is the each *code* timestep.
                             *
                             * x is the variable in question (=1 for simple counts, but
                             * possibly a true stellar variable)
                             *
                             * We must now modulate by the probability to get the true
                             * 'dtp' contribution to each time *bin* (given by
                             * Ensemble_timestep), then divide by the
                             * time since the last log to get the time-average dp.
                             *
                             * Rates are treated identically except that they are counted as
                             *
                             * sum = # of events
                             *
                             * i.e. there is no 'delta t' term, hence the rate, or amount in the
                             * "timestep", is output directly.
                             */
                            const double fac = 1.0 / linear_dt_since_last;

#ifdef COMPRESS_ENSEMBLE_OUTPUT
                            unsigned int zerocount = 0;
#endif // COMPRESS_ENSEMBLE_OUTPUT
                            for(k=0;k<ENSEMBLE_NUMBER;k++)
                            {
                                /*
                                 * value to add to the ensemble
                                 */
                                const double x = Ensemble_data(k, fac * estore[star->starnum][k]);

                                if(Use_ensemble(STELLAR_POPULATIONS_ENSEMBLE_FILTER_SCALARS) == TRUE)
                                {
                                    /*
                                     * Set ND ensemble
                                     *
                                     * First we set the scalars.
                                     */
                                    Set_ensemble(T,
                                                 x,
                                                 (char*)"scalars",
                                                 (char*)ensemble_1D_strings[k]);
                                }

                                if(preferences->ensemble_legacy_ensemble == TRUE)
                                {
#ifdef COMPRESS_ENSEMBLE_OUTPUT
                                    if(Is_zero(x))
                                    {
                                        // found a zero, do not output yet
                                        zerocount++;
                                    }
                                    else
                                    {
                                        if(zerocount != 0)
                                        {
                                            Ensemble_zeros(zerocount);
                                            zerocount = 0;
                                        }

                                        Ensemble_print(k,x);
                                    }
#else //COMPRESS_ENSEMBLE_OUTPUT
                                    Ensemble_print(k,x);
#endif //COMPRESS_ENSEMBLE_OUTPUT
                                }

                                /* reset */
                                estore[star->starnum][k] = 0.0;
                            }

                            if(preferences->ensemble_legacy_ensemble == TRUE)
                            {
#ifdef COMPRESS_ENSEMBLE_OUTPUT
                                if(zerocount!=0)
                                {
                                    Ensemble_zeros(zerocount);
                                }
#endif // COMPRESS_ENSEMBLE_OUTPUT

                                Printf("\n");
                            }
                            reset_time=TRUE;
                        }
#else

                        if(preferences->ensemble_legacy_ensemble == TRUE)
                        {
                            printf("This is deprecated!\n");exit(0);

                            Printf("STELLARPOPENSEMBLE%d__ %6.6e %3.3e ",
                                   i,
                                   Ensemble_time(stardata->model.time),
                                   stardata->model.probability);

                            for(k=0;k<ENSEMBLE_NUMBER;k++)
                            {
                                Ensemble_print(k,ensemble_weight[k]);
                            }
                            Printf("\n");
                        }
#endif // STELLAR_POPULATIONS_ENSEMBLE_SPARSE

                    }
                } // main Foreach_star loop


                /************************************************************
                 *
                 * The following calls are per system, not per star.
                 *
                 ************************************************************/
                if(Use_ensemble(STELLAR_POPULATIONS_ENSEMBLE_FILTER_BeXRB))
                {
                    ensemble_BeXRBs(stardata,
                                    dtp,
                                    system_is_binary);
                }

#ifdef DISCS
                /*
                 * Circumbinary discs
                 */
                if(Use_ensemble(STELLAR_POPULATIONS_ENSEMBLE_FILTER_CBDISCS) == TRUE)
                {
                    ensemble_disc_log(stardata,
                                      p,
                                      dtp,
                                      T,
                                      system_luminosity,
                                      system_is_binary);
                }
#endif // DISCS

                if(Use_ensemble(STELLAR_POPULATIONS_ENSEMBLE_FILTER_PNE) == TRUE)
                {
                    ensemble_planetary_nebulae(stardata,dtp);
                }

                if(Use_ensemble(STELLAR_POPULATIONS_ENSEMBLE_FILTER_PULSATORS) == TRUE)
                {
                    ensemble_pulsators(stardata,
                                       dtp,
                                       T,
                                       log10_Porb_days_binned,
                                       a_binned,
                                       iprimary,
                                       system_is_binary
#ifdef STELLAR_COLOURS
                                       ,CMD
#endif // STELLAR_COLOURS
                        );
                }

//#define _PULSATION_TEST
#ifdef _PULSATION_TEST
                if(1)
                {
                    /*
                     * Pulsating stars test code
                     */
                    int nn = 0;
                    while(nn++<100000)
                    {
                        Pulsator_ID * ids;
                        Pulsator_ID n = 0;
                        SETstar(0);
                        const double logL = -2.0 + 5.0 * random_number(stardata,NULL);
                        const double logTeff = 3.3 + 1.0 * random_number(stardata,NULL);
                        const double R = sqrt(pow(exp10(logTeff)*1e-3,-4.0)*1130.0*(exp10(logL)));
                        star->luminosity = exp10(logL);
                        star->radius = R;

                        id_pulsator(stardata,
                                    star,
                                    &n,
                                    &ids);

                        if(n>0)
                        {
                            Pulsator_ID j;
                            for(j=0; j<n; j++)
                            {
                                Pulsator_ID type = ids[j];
                                printf("PULSE%u %s %g %g\n",
                                       type,
                                       stardata->store->pulsator_ellipses[type].name,
                                       log10(Teff_from_star_struct(star)),
                                       log10(star->luminosity)
                                    );
                            }
                            Safe_free(ids);
                        }
                    }
                    Flexit;
                }
#endif // _PULSATION_TEST

                {
                    /*
                     * Count stellar types
                     */
                    Dprint("Set ensemble\n");

                    if(Use_ensemble(STELLAR_POPULATIONS_ENSEMBLE_FILTER_STELLAR_TYPE_COUNTS) == TRUE)
                    {
                        Foreach_star(star)
                        {
                            Set_ensemble_count(
                                "number_counts",
                                "stellar_type",
                                star->starnum,
                                (char*)Short_stellar_type_string(star->stellar_type)
                                );
                        }
                    }

                    /*
                     * Initial final mass relation
                     */
                    if(Use_ensemble(STELLAR_POPULATIONS_ENSEMBLE_FILTER_IFMR) == TRUE)
                    {
                        Foreach_star(star)
                        {
                            if(WHITE_DWARF(star->stellar_type))
                            {
                                Set_ensemble_rate(
                                    "IFMR : map",
                                    "max MS mass",
                                    Bin_data(star->phase[MAIN_SEQUENCE].maximum_mass,0.01),
                                    "initial WD mass",
                                    Bin_data(star->mass,0.01)
                                    );
                            }
                        }
                    }
                }

                if(reset_time==TRUE)
                {
                    lastensembleouttime = Ensemble_time(stardata->model.time);
                }
            }
#endif // ENSEMBLE
        }
    }

    Call_function_hook(extra_ensemble);

    /*
     * Debugging output JSON to a file?
     */
    if(0 && final)
    {
        static int counter = 0;
        char * file;
        if(asprintf(&file,"/tmp/json.%d",counter++)){};
        ensemble_output_to_json(stardata,file);
        Safe_free(file);
    }

    if(json_output == TRUE)
    {
        ensemble_output_to_json(stardata,NULL);
    }

    /*
     * Output cdict stats? Useful to test memory use
     */
    if(0 && stardata->model.ensemble_cdict->stats != NULL && final)
    {
        CDict_stats(stardata->model.ensemble_cdict);
    }
}


static void make_ensemble_type_array(struct stardata_t * const stardata,
                                     Ensemble_type ** ensemble_type_p)
{
    Ensemble_type * ensemble_type;
    if(stardata->tmpstore->ensemble_type==NULL)
    {
        ensemble_type = stardata->tmpstore->ensemble_type
            = Calloc(ENSEMBLE_NUMBER,sizeof(Ensemble_type));
        *ensemble_type_p = ensemble_type;

        /* default all to type "normal" */
        for(unsigned int i=0;i<ENSEMBLE_NUMBER;i++)
        {
            ensemble_type[i] = ENSEMBLE_TYPE_NORMAL;
        }

        /* some ensemble variables are rates : treat these differently */

        /* supernovae */
#undef X
#define X(TYPE,string,STRING) ensemble_type[TYPE] = ENSEMBLE_TYPE_RATE;
        _SN_TYPES_LIST(ENSEMBLE_SN_,);
        _SN_TYPES_LIST(ENSEMBLE_SN_,_DARK);
        _SN_TYPES_LIST(ENSEMBLE_SN_,_BRIGHT);

        /* test rate */
        ensemble_type[ENSEMBLE_TEST_RATE] = ENSEMBLE_TYPE_RATE;

        /* common envelope rates */
        for(unsigned int i=ENSEMBLE_COMENV_HG_MERGER;
            i<ENSEMBLE_COMENV_DETACHED+1;
            i++)
        {
            ensemble_type[i] = ENSEMBLE_TYPE_RATE;
        }

        /* merger rates */
        for(unsigned int i=ENSEMBLE_MERGER_INTO_LMMS;
            i<ENSEMBLE_MERGER_BH_BH+1;
            i++)
        {
            ensemble_type[i] = ENSEMBLE_TYPE_RATE;
        }

        /* special case : hybrid COWD mergers */
        for(unsigned int i=ENSEMBLE_MERGER_LMMS_Hybrid_HeCOWD;
            i<ENSEMBLE_MERGER_Hybrid_HeCOWD_Hybrid_HeCOWD+1;
            i++)
        {
            ensemble_type[i] = ENSEMBLE_TYPE_RATE;
        }

        for(unsigned int i=ENSEMBLE_PERETS_HeWD_COWD_LT0p8;
            i<=ENSEMBLE_PERETS_HeWD_LT0p25_ONeWD;
            i++)
        {
            ensemble_type[i] = ENSEMBLE_TYPE_RATE;
        }
        ensemble_type[ENSEMBLE_MERGER_HeWD_gt_COWD] = ENSEMBLE_TYPE_RATE;

        /*
         * Various luminosities and momentum fluxes should
         * be considered as rates
         */
        ensemble_type[ENSEMBLE_WIND_KINETIC_POWER] = ENSEMBLE_TYPE_RATE;
        ensemble_type[ENSEMBLE_WIND_RSG_KINETIC_POWER] = ENSEMBLE_TYPE_RATE;
        ensemble_type[ENSEMBLE_WIND_ISG_KINETIC_POWER] = ENSEMBLE_TYPE_RATE;
        ensemble_type[ENSEMBLE_WIND_BSG_KINETIC_POWER] = ENSEMBLE_TYPE_RATE;
        ensemble_type[ENSEMBLE_WIND_WOLF_RAYET_KINETIC_POWER] = ENSEMBLE_TYPE_RATE;
        ensemble_type[ENSEMBLE_WIND_AGB_KINETIC_POWER] = ENSEMBLE_TYPE_RATE;
        ensemble_type[ENSEMBLE_SUPERNOVA_KINETIC_POWER] = ENSEMBLE_TYPE_RATE;
        ensemble_type[ENSEMBLE_SUPERNOVA_CORE_COLLAPSE_KINETIC_POWER] = ENSEMBLE_TYPE_RATE;
        ensemble_type[ENSEMBLE_SUPERNOVA_IA_KINETIC_POWER] = ENSEMBLE_TYPE_RATE;
        ensemble_type[ENSEMBLE_NOVA_KINETIC_POWER] = ENSEMBLE_TYPE_RATE;
        ensemble_type[ENSEMBLE_SUPERNOVA_LUMINOSITY] = ENSEMBLE_TYPE_RATE;
        ensemble_type[ENSEMBLE_SUPERNOVA_CORE_COLLAPSE_LUMINOSITY] = ENSEMBLE_TYPE_RATE;
        ensemble_type[ENSEMBLE_SUPERNOVA_IA_LUMINOSITY] = ENSEMBLE_TYPE_RATE;
        ensemble_type[ENSEMBLE_NOVA_LUMINOSITY] = ENSEMBLE_TYPE_RATE;
        ensemble_type[ENSEMBLE_WIND_MOMENTUM] = ENSEMBLE_TYPE_RATE;
        ensemble_type[ENSEMBLE_WIND_RSG_MOMENTUM] = ENSEMBLE_TYPE_RATE;
        ensemble_type[ENSEMBLE_WIND_ISG_MOMENTUM] = ENSEMBLE_TYPE_RATE;
        ensemble_type[ENSEMBLE_WIND_BSG_MOMENTUM] = ENSEMBLE_TYPE_RATE;
        ensemble_type[ENSEMBLE_WIND_WOLF_RAYET_MOMENTUM] = ENSEMBLE_TYPE_RATE;
        ensemble_type[ENSEMBLE_WIND_AGB_MOMENTUM] = ENSEMBLE_TYPE_RATE;
        ensemble_type[ENSEMBLE_SUPERNOVA_MOMENTUM] = ENSEMBLE_TYPE_RATE;
        ensemble_type[ENSEMBLE_SUPERNOVA_CORE_COLLAPSE_MOMENTUM] = ENSEMBLE_TYPE_RATE;
        ensemble_type[ENSEMBLE_SUPERNOVA_IA_MOMENTUM] = ENSEMBLE_TYPE_RATE;
        ensemble_type[ENSEMBLE_NOVA_MOMENTUM] = ENSEMBLE_TYPE_RATE;

        /* mass loss rates */
        for(unsigned int i = ENSEMBLE_MDOT;
            i < ENSEMBLE_MDOT + SOURCE_NUMBER;
            i++)
        {
            ensemble_type[i] = ENSEMBLE_TYPE_RATE;
        }

        /* Thorne-Zytkow formation rates as a function of binary mass */
        for(unsigned int i = ENSEMBLE_TZ_M1_LT8;
            i < ENSEMBLE_TZ_M1_GT30+1;
            i++)
        {
            ensemble_type[i] = ENSEMBLE_TYPE_RATE;
        }

        /* magnitudes */
        for(Stellar_magnitude_index m=0; m<STELLAR_MAGNITUDE_NUMBER; m++)
        {
            const Ensemble_index n = ENSEMBLE_MAGNITUDE_U + (Ensemble_index)m;
            ensemble_type[n] = ENSEMBLE_TYPE_MAGNITUDE;
        }
    }
    else
    {
        *ensemble_type_p = stardata->tmpstore->ensemble_type;
    }
}


static void _set_previous_stars(const struct star_t * const previous_star,
                                const struct star_t * const previous_other_star,
                                struct star_t ** previous_primary_star,
                                struct star_t ** previous_secondary_star)
{
    if(previous_star->stellar_type != MASSLESS_REMNANT &&
       previous_star->RLOFing)
    {
        *previous_primary_star = (struct star_t*)previous_star;
        *previous_secondary_star = (struct star_t*)previous_other_star;
    }
    else if(previous_other_star->stellar_type != MASSLESS_REMNANT &&
            previous_other_star->RLOFing)
    {
        *previous_primary_star = (struct star_t*)previous_other_star;
        *previous_secondary_star = (struct star_t*)previous_star;
    }
    else
    {
        /*
         * Choose the star nearest to filling its Roche lobe
         */
        const double fr0 = previous_star->radius/previous_star->roche_radius;
        const double fr1 = previous_other_star->radius/previous_other_star->roche_radius;
        *previous_primary_star = fr0 > fr1 ? (struct star_t*)previous_star : (struct star_t*) previous_other_star;
        *previous_secondary_star = fr0 > fr1 ? (struct star_t*)previous_other_star : (struct star_t*) previous_star;
    }
}

#endif // STELLAR_POPULATIONS_ENSEMBLE
