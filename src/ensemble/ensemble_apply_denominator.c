#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef STELLAR_POPULATIONS_ENSEMBLE

void ensemble_apply_denominator(
    struct cdict_t * cdict CDict_maybe_unused,
    struct cdict_entry_t * entry CDict_maybe_unused,
    void * data)
{
    /*
     * Apply the value in void * data as a denominator
     * on the passed in entry's double-precision value.
     */
    if(data != NULL)
    {
        /* extract denominator data */
        const struct cdict_entry_t * const denominator =
            (struct cdict_entry_t const *) data;

        /* use the denominator to normalize */
        entry->value.value.double_data /=
            denominator->value.value.double_data;

        /*
         * make sure metadata is set
         */
        CDict_set_metadata(cdict,
                           entry,
                           NULL,
                           NULL);

        /* do not apply the factor twice */
        entry->pre_output->function = NULL;
    }
}

#endif // STELLAR_POPULATIONS_ENSEMBLE
