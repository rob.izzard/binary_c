#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef STELLAR_POPULATIONS_ENSEMBLE

/*
 * Hertzsprung-Russell, logg-logTeff and colour-magnitude diagrammes
 *
 * Use Set_ensemble_HRD() to add data to the ensemble in the
 * appropriate format. This adds to both HRD and HRD(t),
 * i.e. a time-integrated version (HRD) and a set of time
 * slices (HRD(t)).
 *
 * A similar macro, Set_ensemble_CMD(), adds colour-magnitude
 * a diagram (e.g. for Gaia, JWST).
 *
 * It also checks the ensemble filters, and adds the period-distributions
 * for each point, if required.
 */

#include "ensemble_HRD_macros.h"

void ensemble_HRD(struct stardata_t * const stardata,
                  const double dtp,
                  const double T,
                  const Star_number iprimary,
                  const Star_number itrigger,
                  const Boolean both_triggered,
                  const Boolean system_is_binary,
#ifdef STELLAR_COLOURS
                  const Boolean CMD,
#endif // STELLAR_COLOURS
                  char * prekey,
                  const Boolean * const toggles
    )
{
    Dprint("Set HRD\n");
    if(iprimary==-1) return; /* both massless */
    struct star_t * const primary_star = &stardata->star[iprimary];
    struct star_t * const secondary_star = &stardata->star[Other_star(iprimary)];
    const struct preferences_t * const p = stardata->preferences;
    double mean_Teff = 0.0;
    double mean_logg = 0.0;
    double Ltot = 1e-20;
    const double logTeff_bin_width = 0.05;
    const double logL_bin_width = 0.05;
    const double logg_bin_width = 0.05;
    const double Porb_binwidth = 0.1;
    const double q_binwidth = 0.05;
    const double ecc_binwidth = 0.05;
    const double mass_binwidth = 0.1;
#ifdef STELLAR_COLOURS
    const double G_bin_width = 0.05;
    const double GBP_GRP_bin_width = 0.05;
#endif// STELLAR_COLOURS

    Star_number isecondary Maybe_unused = Other_star(iprimary);

    /*
     * Log10 Orbital period: single-stars are
     * given -50.0 (i.e. implausibly short).
     *
     * Normal periods are limited to the range 10^-6 to 10^10 years.
     */
    const double Porb_binned =
        system_is_binary ?
        Bin_data(Safelog10(Max(1e-6,Min(1e10,stardata->common.orbit.period))),
                 Porb_binwidth) :
        -50.0;
    const double ecc_binned =
        system_is_binary ?
        Bin_data(stardata->common.orbit.eccentricity,
                 ecc_binwidth) :
        -1.0;
    const double q_binned =
        system_is_binary ?
        Bin_data(Min(stardata->star[0].q,
                     stardata->star[1].q),
                 q_binwidth) :
        -1.0;

    unsigned int n_included = 0;
    Foreach_star(star)
    {
        const int i = (int) star->starnum;

        /*
         * HRD for all nuclear-burning stars and white dwarfs
         */
        if(star->stellar_type < NEUTRON_STAR)
        {
            n_included++;
            const double logL_binned = Bin_data(Safelog10(star->luminosity),
                                                logL_bin_width);
            const double logTeff_binned = Bin_data(Safelog10(Teff(i)),
                                                   logTeff_bin_width);
            const double lg = logg(star);
            const double logg_binned = Bin_data(lg,
                                                logg_bin_width);
            Set_ensemble_HRD(
                "star",i,
                "logTeff",logTeff_binned,
                "logL",logL_binned
                );
            Set_ensemble_HRD(
                "star",(char*)(i == iprimary ? "primary" : "secondary"),
                "logTeff",logTeff_binned,
                "logL",logL_binned
                );

            Set_ensemble_HRD(
                "star",i,
                "logTeff",logTeff_binned,
                "logg",logg_binned
                );
            Set_ensemble_HRD(
                "star",(char*)(i == iprimary ? "primary" : "secondary"),
                "logTeff",logTeff_binned,
                "logg",logg_binned
                );
            if(itrigger!=-1)
            {
                if(both_triggered == TRUE)
                {
                   Set_ensemble_HRD(
                        "star",(char*)(i == iprimary ? "triggered primary" : "triggered secondary"),
                        "logTeff",logTeff_binned,
                        "logL",logL_binned
                        );
                    Set_ensemble_HRD(
                        "star",(char*)(i == iprimary ? "triggered primary" : "triggered secondary"),
                        "logTeff",logTeff_binned,
                        "logg",logg_binned
                        );
                }
                else
                {
                    Set_ensemble_HRD(
                        "star",(char*)(i == itrigger ? "triggered star" : "triggered companion"),
                        "logTeff",logTeff_binned,
                        "logL",logL_binned
                        );
                    Set_ensemble_HRD(
                        "star",(char*)(i == itrigger ? "triggered star" : "triggered companion"),
                        "logTeff",logTeff_binned,
                        "logg",logg_binned
                        );
                }
            }
            mean_Teff += star->luminosity * Teff(i);
            mean_logg += star->luminosity * lg;
            Ltot += star->luminosity;

#ifdef STELLAR_COLOURS
            if(CMD)
            {
                if(stardata->tmpstore->stellar_magnitudes[i] == NULL)
                {
                    stellar_magnitudes(stardata,
                                       star,
                                       &stardata->tmpstore->stellar_magnitudes[i]);
                }

                if(stardata->tmpstore->stellar_magnitudes[i] != NULL)
                {
                    /*
                     * Resolved colour-magnitude diagram:
                     * G vs GBP-GRP
                     */

                    const double * mag = stardata->tmpstore->stellar_magnitudes[i];

                    if(Magnitude_is_physical(mag[STELLAR_MAGNITUDE_GAIA_GBP]))
                    {
                        const double GBP_GRP_binned = Bin_data(mag[STELLAR_MAGNITUDE_GAIA_GBP]-mag[STELLAR_MAGNITUDE_GAIA_GRP],
                                                               GBP_GRP_bin_width);
                        const double G_binned = Bin_data(mag[STELLAR_MAGNITUDE_GAIA_G],
                                                         G_bin_width);

                        Set_ensemble_CMD(
                            "star",i,
                            "GBP-GRP",GBP_GRP_binned,
                            "G",G_binned
                            );
                        Set_ensemble_CMD(
                            "star",(char*)(i == iprimary ? "primary" : "secondary"),
                            "GBP-GRP",GBP_GRP_binned,
                            "G",G_binned
                            );
                        if(itrigger!=-1)
                        {
                            if(both_triggered == TRUE)
                            {
                                Set_ensemble_CMD(
                                    "star",(char*)(i == iprimary ? "triggered primary" : "triggered secondary"),
                                    "GBP-GRP",GBP_GRP_binned,
                                    "G",G_binned
                                    );
                            }
                            else
                            {
                                Set_ensemble_CMD(
                                    "star",(char*)(i == itrigger ? "triggered star" : "triggered companion"),
                                    "GBP-GRP",GBP_GRP_binned,
                                    "G",G_binned
                                    );
                            }
                        }
                        Set_ensemble_CMD(
                            "star",i,
                            "stellar_type",star->stellar_type,
                            "GBP-GRP",GBP_GRP_binned,
                            "G",G_binned
                            );
                    }
                }
            }
#endif // STELLAR_COLOURS
        }
    }

    mean_Teff /= Max(Ltot,1e-20);
    mean_logg /= Max(Ltot,1e-20);

    const double mean_logTeff_binned = Bin_data(Safelog10(mean_Teff),
                                                logTeff_bin_width);
    const double mean_logL_binned = Bin_data(Safelog10(Ltot),
                                             logL_bin_width);
    const double mean_logg_binned = Bin_data(mean_logg, /* already logged */
                                             logg_bin_width);

    if(n_included > 0 &&
       In_range(mean_logTeff_binned,2.5,7.0))
    {
        {
            /*
             * Standard HRD
             */
            struct star_t * const star = NULL;
            if(In_range(mean_logL_binned,-4.0,+8.0))
            {
                Set_ensemble_HRD(
                    (char*)"unresolved",
                    "logTeff",mean_logTeff_binned,
                    "logL",mean_logL_binned
                    );
            }
            if(In_range(mean_logg_binned,-5.0,+5.0))
            {
                Set_ensemble_HRD(
                    (char*)"unresolved",
                    "logTeff",mean_logTeff_binned,
                    "logg",mean_logg_binned
                    );
            }
        }

        {
            /*
             * HRD for Bokeh plots
             */


            if(In_range(mean_logg_binned,-5.0,+5.0))
            {
                /*
                 * Unresolved logg vs logTeff
                 */
                Set_ensemble_HRD_Bokeh(
                    "unresolved",
                    "logTeff",mean_logTeff_binned,
                    "logg",mean_logg_binned
                    );
                /*
                 * Resolved logg vs logTeff
                 */
                if(itrigger!=-1)
                {
                    Foreach_star(object)
                    {
                        const double lg = logg(object);
                        const double logg_binned = Bin_data(lg,
                                                            logg_bin_width);
                        const double logTeff_binned = Bin_data(Safelog10(Teff_from_star_struct(object)),
                                                               logTeff_bin_width);
                        Set_ensemble_HRD_Bokeh(
                            object->starnum == itrigger ? "resolved triggered star" : "resolved triggered companion",
                            "logTeff",logTeff_binned,
                            "logg",logg_binned
                            );
                    }
                }
            }
            if(In_range(mean_logL_binned,-4.0,+8.0))
            {
                /*
                 * Unresolved logL vs logTeff
                 */
                Set_ensemble_HRD_Bokeh(
                    "unresolved",
                    "logTeff",mean_logTeff_binned,
                    "logL",mean_logL_binned
                    );

                /*
                 * Resolved logL vs logTeff
                 */
                if(itrigger!=-1)
                {
                    /* Resolved HRDs for the triggered star and its companion */
                    Foreach_star(object)
                    {
                        const double logL_binned = Bin_data(Safelog10(object->luminosity),
                                                            logL_bin_width);
                        const double logTeff_binned = Bin_data(Safelog10(Teff_from_star_struct(object)),
                                                               logTeff_bin_width);
                        Set_ensemble_HRD_Bokeh(
                            object->starnum == itrigger ? "resolved triggered star" : "resolved triggered companion",
                            "logTeff",logTeff_binned,
                            "logL",logL_binned
                            );
                    }
                }
            }
        }
    }

#ifdef STELLAR_COLOURS
    /*
     * Unresolved colour-magnitude diagram
     */
    if(CMD)
    {
        if(stardata->tmpstore->unresolved_magnitudes == NULL)
        {
            unresolved_stellar_magnitudes(stardata,
                                          &stardata->tmpstore->unresolved_magnitudes);
        }
        if(stardata->tmpstore->unresolved_magnitudes != NULL)
        {
            const double * const mag = stardata->tmpstore->unresolved_magnitudes;

            if(Magnitude_is_physical(mag[STELLAR_MAGNITUDE_GAIA_GBP]))
            {
                const double GBP_GRP_binned = Bin_data(mag[STELLAR_MAGNITUDE_GAIA_GBP]-mag[STELLAR_MAGNITUDE_GAIA_GRP],
                                                       GBP_GRP_bin_width);
                const double G_binned = Bin_data(mag[STELLAR_MAGNITUDE_GAIA_G],
                                                 G_bin_width);
                /*
                 * Log by stellar type but reduce data grid
                 * by putting the largest stellar type first
                 * (i.e. avoid counting x-y the same as y-x,
                 *  where y>x)
                 */
                const Star_number first =
                    stardata->star[0].stellar_type >= stardata->star[1].stellar_type ? 0 : 1;

                {
                    /*
                     * this is a dummy star struct that, being NULL,
                     * means we work on the unresolved binary
                     */
                    struct star_t * star = NULL;
                    Set_ensemble_CMD(
                        (char*)"unresolved",
                        "stellar_type1",stardata->star[first].stellar_type,
                        "stellar_type2",stardata->star[Other_star(first)].stellar_type,
                        "GBP-GRP",GBP_GRP_binned,
                        "G",G_binned
                        );
                }
                if(In_range(GBP_GRP_binned,-2.0,7.0) &&
                   In_range(G_binned,-5.0,17.0))
                {
                    Set_ensemble_CMD_Bokeh(
                        "GBP-GRP",GBP_GRP_binned,
                        "G",G_binned
                        );
                }
            }
#ifdef YBC
            /*
             * Gaia CMDs
             */
            if(Magnitude_is_physical(mag[STELLAR_MAGNITUDE_YBC_GAIA_GBP]))
            {
                const double YBC_GBP_GRP_binned = Bin_data(mag[STELLAR_MAGNITUDE_YBC_GAIA_GBP] - mag[STELLAR_MAGNITUDE_YBC_GAIA_GRP],
                                                           GBP_GRP_bin_width);
                if(In_range(YBC_GBP_GRP_binned,-2.0,7.0))
                {
                    const double YBC_G_binned = Bin_data(mag[STELLAR_MAGNITUDE_YBC_GAIA_G],
                                                         G_bin_width);
                    if(In_range(YBC_G_binned,-5.0,17.0))
                    {
                        Set_ensemble_CMD_Bokeh(
                            "YBC GBP-GRP",YBC_GBP_GRP_binned,
                            "YBC G",YBC_G_binned
                            );
                    }
                }
            }


            /*
             * JWST CMDs
             */
            if(Magnitude_is_physical(mag[STELLAR_MAGNITUDE_YBC_JWST_F070W]))
            {
                /*
                 * These macros are (A,B,u,C,D,w) where
                 * C-D (in bins of width w) is plotted vs
                 * A-B (in bins of width u).
                 */

#define JWST_CMD_PAIRS                         \
    X( F090W, F150W, 0.05, F090W,      , 0.05) \
    X( F090W, F360M, 0.05, F090W, F480M, 0.05) \
    X( F090W, F250M, 0.05, F090W, F430M, 0.05) \
    X( F090W, F277W, 0.05, F090W, F444W, 0.05)

#define Colour_or_magnitude(PREFIX,A,...)                   \
                mag[PREFIX##A]                              \
                    __VA_OPT__(- mag[PREFIX##__VA_ARGS__])
#define Colour_or_magnitude_string(PREFIX,A,...)        \
                PREFIX #A __VA_OPT__("-" #__VA_ARGS__)

#undef X
#define X(A,B,u,C,D,w)                                                  \
                Set_ensemble_CMD_Bokeh(                                 \
                    Colour_or_magnitude_string("JWST ",A,B),            \
                    Bin_data(Colour_or_magnitude(STELLAR_MAGNITUDE_YBC_JWST_,A,B),u), \
                    Colour_or_magnitude_string("JWST ",C,D),            \
                    Bin_data(Colour_or_magnitude(STELLAR_MAGNITUDE_YBC_JWST_,C,D),w) \
                    );

                JWST_CMD_PAIRS;
            }
#endif // YBC
        }
    }
#endif // STELLAR_COLOURS
}

#endif // STELLAR_POPULATIONS_ENSEMBLE
