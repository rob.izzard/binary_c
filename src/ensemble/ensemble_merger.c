#include "../binary_c.h"
No_empty_translation_unit_warning;


void ensemble_merger(struct stardata_t * const stardata,
                     const struct star_t * const star,
                     const struct star_t * const previous_star,
                     const struct star_t * const previous_other_star,
                     double * const ensemble_weight,
                     const Long_spectral_type * const prevspectypef)
{
    const double p = stardata->model.probability;
    static const Ensemble_index merger_matrix[NUMBER_OF_STELLAR_TYPES-1][NUMBER_OF_STELLAR_TYPES-1] =
        ENSEMBLE_MERGER_MATRIX;
    /*
     * The stars merged into star i and
     * the code in here is called only once per
     * timestep, so if you want to check
     * symmetric mergers, use a and b below.
     */
    const Ensemble_index merge_index = ENSEMBLE_MERGER_INTO_LMMS +
        (Ensemble_index)star->stellar_type;

    Dprint("merge index %u from %d,%d -> matrix %u\n",
           merge_index,
           previous_star->stellar_type,
           previous_other_star->stellar_type,
           merger_matrix[previous_star->stellar_type][previous_other_star->stellar_type]
        );

    /* log the merged star's stellar type */
    ensemble_weight[merge_index] = 1.0;

    /* log the merging stars' types */
    ensemble_weight[merger_matrix[previous_star->stellar_type][previous_other_star->stellar_type]] = 1.0;


    if(Use_ensemble(STELLAR_POPULATIONS_ENSEMBLE_FILTER_MERGED) == TRUE)
    {
        /*
         * Stellar merger matrices. Remember to use the previous_stardata
         * because right now one of the stars is a MASSLESS_REMNANT.
         */

        /*
         * merger matrix by mass
         */
        Set_ensemble_rate(
            "distributions",
            "merged log masses : map",
            "primary",
            Bin_data(
                log10(Max(stardata->previous_stardata->star[0].mass,
                          stardata->previous_stardata->star[1].mass)), 0.1),
            "secondary",
            Bin_data(
                log10(Min(stardata->previous_stardata->star[0].mass,
                          stardata->previous_stardata->star[1].mass)), 0.1)
            );

        /*
         * Merger matrix by stellar type, with the
         * most evolved first
         */
        Set_ensemble_rate(
            "distributions",
            "merged stellar types : map",
            "primary",(short int)(Max(previous_star->stellar_type,
                                      previous_other_star->stellar_type)),
            "secondary",(short int)(Min(previous_star->stellar_type,
                                        previous_other_star->stellar_type))
            );

        /*
         * merger matrix by spectral type
         */
        char * prevspectype[NUMBER_OF_STARS];
        {
            Star_number j;
            Starloop(j)
            {
                prevspectype[j] = spectral_type_static_string(
                    stardata,
                    &stardata->previous_stardata->star[j]);
            }
        }


        Set_ensemble_rate(
            "distributions",
            "merged spectral type strings : map",
            "primary",(char*)(prevspectypef[0] < prevspectypef[1] ? prevspectype[0] : prevspectype[1]),
            "secondary",(char*)(prevspectypef[0] < prevspectypef[1] ? prevspectype[1] : prevspectype[0])
            );

        Set_ensemble_rate(
            "distributions",
            "merged spectral type floats : map",
            "primary",Bin_data(prevspectypef[0] < prevspectypef[1] ? prevspectypef[0] : prevspectypef[1], 0.1),
            "secondary",Bin_data(prevspectypef[0] < prevspectypef[1] ? prevspectypef[1] : prevspectypef[0], 0.1)
            );
    }

    /*
     * Special case : Hybrid COWD mergers
     */
    if(previous_star->hybrid_HeCOWD == TRUE ||
       previous_other_star->hybrid_HeCOWD == TRUE)
    {
        Ensemble_index hybrid_merge_index;

        if(previous_star->hybrid_HeCOWD == TRUE &&
           previous_other_star->hybrid_HeCOWD == TRUE)
        {
            /*
             * Both stars are hybrids
             */
            hybrid_merge_index = ENSEMBLE_MERGER_Hybrid_HeCOWD_Hybrid_HeCOWD;
        }
        else
        {
            /*
             * Identify the non-hybrid
             */
            const struct star_t * const non_hybrid =
                previous_star->hybrid_HeCOWD == FALSE ? previous_star : previous_other_star;

            /*
             * Hence the merge index
             */
            hybrid_merge_index = ENSEMBLE_MERGER_LMMS_Hybrid_HeCOWD +
                non_hybrid->stellar_type;
        }

        ensemble_weight[hybrid_merge_index] = 1.0;
    }


    /*
     * Now do logging for stars a and b, where
     * the two are previous stars 0,1 and then 1,0.
     */
    for(unsigned int kk=0; kk<2; kk++)
    {
        /*
         * Set stars a and b, note that kk is 0 or 1
         */
        const struct star_t * const a = kk == 0 ? previous_star : previous_other_star;
        const struct star_t * const b = kk == 0 ? previous_other_star : previous_star;

        /*
         * Special case for Simon Jeffery
         * HeWD + COWD mergers in which the HeWD is more
         * massive than the COWD
         */
        if((a->stellar_type == HeWD &&
            b->stellar_type == COWD &&
            a->mass > b->mass))
        {
            ensemble_weight[ENSEMBLE_MERGER_HeWD_gt_COWD] = 1.0;
        }

        /*
         * Hagai Perets' HeWD + CO/ONeWD mergers
         */
        if(a->stellar_type==HeWD)
        {
            if(b->stellar_type==COWD)
            {
                /* cases with no HeWD mass cuts */
                if(b->mass < 0.8)
                {
                    ensemble_weight[ENSEMBLE_PERETS_HeWD_COWD_LT0p8] = 1.0;
                }
                else if(b->mass < 0.7)
                {
                    ensemble_weight[ENSEMBLE_PERETS_HeWD_COWD_LT0p7] = 1.0;
                }
                else if(b->mass < 0.6)
                {
                    ensemble_weight[ENSEMBLE_PERETS_HeWD_COWD_LT0p6] = 1.0;
                }
                else if(b->mass < 0.5)
                {
                    ensemble_weight[ENSEMBLE_PERETS_HeWD_COWD_LT0p5] = 1.0;
                }

                /* as above with mass cuts */
                if(a->mass>0.3)
                {
                    if(b->mass < 0.8)
                    {
                        ensemble_weight[ENSEMBLE_PERETS_HeWD_GT0p3_COWD_LT0p8] = 1.0;
                    }
                    else if(b->mass < 0.7)
                    {
                        ensemble_weight[ENSEMBLE_PERETS_HeWD_GT0p3_COWD_LT0p7] = 1.0;
                    }
                    else if(b->mass < 0.6)
                    {
                        ensemble_weight[ENSEMBLE_PERETS_HeWD_GT0p3_COWD_LT0p6] = 1.0;
                    }
                    else if(b->mass < 0.5)
                    {
                        ensemble_weight[ENSEMBLE_PERETS_HeWD_GT0p3_COWD_LT0p5] = 1.0;
                    }
                }

                if(a->mass<0.25)
                {
                    if(b->mass < 0.8)
                    {
                        ensemble_weight[ENSEMBLE_PERETS_HeWD_LT0p25_COWD_LT0p8] = 1.0;
                    }
                    else if(b->mass < 0.7)
                    {
                        ensemble_weight[ENSEMBLE_PERETS_HeWD_LT0p25_COWD_LT0p7] = 1.0;
                    }
                    else if(b->mass < 0.6)
                    {
                        ensemble_weight[ENSEMBLE_PERETS_HeWD_LT0p25_COWD_LT0p6] = 1.0;
                    }
                    else if(b->mass < 0.5)
                    {
                        ensemble_weight[ENSEMBLE_PERETS_HeWD_LT0p25_COWD_LT0p5] = 1.0;
                    }
                }
            }
            else if(b->stellar_type==ONeWD)
            {
                ensemble_weight[ENSEMBLE_PERETS_HeWD_ONeWD] = 1.0;

                if(a->mass>0.3)
                {
                    ensemble_weight[ENSEMBLE_PERETS_HeWD_GT0p3_ONeWD] = 1.0;
                }
                else if(a->mass<0.25)
                {
                    ensemble_weight[ENSEMBLE_PERETS_HeWD_LT0p25_ONeWD] = 1.0;
                }
            }
        }
    } // end loop kk
}
