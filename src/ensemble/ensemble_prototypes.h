#pragma once
#ifndef ENSEMBLE_PROTOTYPES_H
#define ENSEMBLE_PROTOTYPES_H

/* ensemble_output_time is also required by legacy yields */
Time ensemble_output_time(const struct stardata_t * const stardata);

#ifdef STELLAR_POPULATIONS_ENSEMBLE
void ensemble_log(struct stardata_t * Restrict const stardata,
                  const Boolean flush_deferred);

int ensemble_cdict_error_handler(void * s,
                                 const int error_number,
                                 const char * const format,
                                 va_list args);


void ensemble_disc_log(struct stardata_t * const stardata,
                       const double p,
                       const double dtp,
                       const double T,
                       const double system_luminosity,
                       const Boolean system_is_binary);
void ensemble_HRD(struct stardata_t * const stardata,
                  const double dtp,
                  const double T,
                  const Star_number iprimary,
                  const Star_number itrigger,
                  const Boolean both_triggered,
                  Boolean system_is_binary,
#ifdef STELLAR_COLOURS
                  const Boolean gaiaCMD,
#endif // STELLAR_COLOURS
                  char * prekey,
                  const Boolean * const toggles);
void ensemble_EMP(struct stardata_t * const stardata,
                  const Boolean is_binary,
                  const double dtp Maybe_unused,
                  const struct star_t * const star,
                  const Abundance * const Xsurf,
                  const double T,
                  const double Porb_binned);

void ensemble_planetary_nebulae(struct stardata_t * const stardata Maybe_unused,
                                const double real_dtp Maybe_unused);
PN_type ensemble_detect_new_PN(const struct stardata_t * const stardata,
                               Star_number * const PN_star_number,
                               const Boolean vb);


void ensemble_apply_denominator(
    struct cdict_t * cdict CDict_maybe_unused,
    struct cdict_entry_t * entry CDict_maybe_unused,
    void * data);

void ensemble_output_to_json(struct stardata_t * const stardata,
                             const char * const filename);

Boolean PN_converged_on_envelope_mass(const struct stardata_t * const stardata,
                                      const struct star_t * const star);

void ensemble_tides(struct stardata_t * const stardata,
                    const double dtp,
                    const double T);

void ensemble_pulsators(struct stardata_t * const stardata,
                        const double dtp,
                        const double T,
                        const double log10_Porb_days_binned,
                        const double a_binned,
                        const Star_number iprimary,
                        const Boolean system_is_binary
#ifdef STELLAR_COLOURS
                        ,const Boolean CMD
#endif // STELLAR_COLOURS
    );

void ensemble_merger(struct stardata_t * const stardata,
                     const struct star_t * const star,
                     const struct star_t * const previous_star,
                     const struct star_t * const previous_other_star,
                     double * const ensemble_weight,
                     const Long_spectral_type * const prevspectypef);

void ensemble_transient(struct stardata_t * const stardata,
                        const double T,
                        const double p,
                        const struct star_t * const primary, // primary star
                        const struct star_t * const previous_primary_star, // primary star
                        const struct star_t * const previous_secondary_star, // secondary star
                        const Stellar_type iprimary,
                        const Boolean system_was_binary,
                        const Boolean system_is_binary
    );
void ensemble_comenv(struct stardata_t * const stardata);

void ensemble_BeXRBs(struct stardata_t * const stardata,
                     const double dtp Maybe_unused,
                     const Boolean system_is_binary);

void ensemble_orbital_velocity_dispersion(struct stardata_t * const stardata,
                                          const double dtp,
                                          const double T,
                                          const Boolean system_is_binary);
#ifdef NUCSYN
void ensemble_chemistry(struct stardata_t * const stardata,
                        const double dtp,
                        const double T,
                        const Stellar_type iprimary Maybe_unused,
                        const Boolean system_is_binary Maybe_unused);
#endif//NUCSYN
void ensemble_HRD_cluster(struct stardata_t * const stardata,
                          const double p,
                          const double dtp,
                          const double T,
                          const Star_number iprimary,
                          const Star_number itrigger,
                          const Boolean both_triggered,
                          const Boolean system_is_binary,
#ifdef STELLAR_COLOURS
                          const Boolean CMD,
#endif // STELLAR_COLOURS
                          char * prekey,
                          const Boolean * const toggles
    );
#endif // STELLAR_POPULATIONS_ENSEMBLE
#endif // ENSEMBLE_PROTOTYPES_H
