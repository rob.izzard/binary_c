#include "../binary_c.h"
No_empty_translation_unit_warning;
#ifdef STELLAR_POPULATIONS_ENSEMBLE
#ifdef DISCS

/*
 * Log discs to the ensemble
 */


/*
 * We do most of the logging through these macros. They
 * allow us to have one macro call which then logs distributions
 * to "all", which is all stellar types, and by stellar type,
 * so we can differentiate post-TPAGB, post-EAGB, post-GB, etc.
 * discs.
 */
#define Disc_(TYPE,LABEL,DIST_TYPE,...)                     \
    Set_ensemble_##TYPE("distributions",                    \
                        LABEL" : label->"DIST_TYPE,         \
                        "all",                              \
                        __VA_ARGS__);                       \
    Set_ensemble_##TYPE("distributions",                    \
                        LABEL" : labelint->"DIST_TYPE,      \
                        "stellar type",                     \
                        (int)disc->overflower_stellar_type, \
                        __VA_ARGS__);

#define Disc_rate(...) Disc_(rate,__VA_ARGS__)
#define Disc_count(...) Disc_(count,__VA_ARGS__)

void ensemble_disc_log(struct stardata_t * const stardata,
                       const double p,
                       const double dtp,
                       const double T,
                       const double system_luminosity,
                       const Boolean system_is_binary)
{

    int i;

    if(stardata->previous_stardata->common.ndiscs > 0 &&
       stardata->common.ndiscs == 0)
    {
        /*
         * Circumbinary disc has evaporated
         */
        struct disc_t * const disc = & stardata->previous_stardata->common.discs[0];

        Disc_rate("cbdisc end log lifetime",
                  "dist",
                  "time",Bin_data(Safelog10(disc->lifetime/YEAR_LENGTH_IN_SECONDS),0.1)
            );
        if(system_is_binary == TRUE)
        {
            Disc_rate(
                "cbdisc end eccentricity",
                "dist",
                "eccentricity",Bin_data(stardata->common.orbit.eccentricity,0.02)
                );
            Disc_rate(
                "cbdisc end orbital log period",
                "dist",
                "log period",Bin_data(Safelog10(stardata->common.orbit.period),0.05)
                );
            Disc_rate(
                "cbdisc end orbital log separation",
                "dist",
                "log separation",Bin_data(Safelog10(stardata->common.orbit.separation),0.05)
                );
        }
        if(system_is_binary == TRUE)
        {
            Set_ensemble_rate(
                "scalars",
                "Disc dissolution rate (binary)",
                (double)T);
        }
        else
        {
            Set_ensemble_rate(
                "scalars",
                "Disc dissolution rate (single)",
                (double)T);
        }
    }

    for(i=0; i<stardata->common.ndiscs; i++)
    {
        struct disc_t * const disc = & stardata->common.discs[i];

        /*
         * If disc is converged and has finite mass and
         * angular momentum, log it
         */
        if(disc->converged == TRUE &&
           disc->M > 0.0 &&
           disc->J > 0.0)
        {
            /*
             * Allocate memory for the binary struct and
             * initialize it if required
             */
            struct binary_system_t * b;
            b = Calloc(1,sizeof(struct binary_system_t));
            disc_init_binary_structure(stardata,b,disc);
            const double Ldisc = disc_total_luminosity(disc);
            const double Tin = disc_inner_edge_temperature(disc);
            const double Tout = disc_outer_edge_temperature(disc);
            const double Idisc = disc_total_moment_of_inertia(disc);
            const double RMhalf = disc_half_mass_radius(stardata,disc,b);
            const double RJhalf = disc_half_angular_momentum_radius(stardata,disc,b);
            const Star_number overflower = stardata->model.comenv_overflower;
            const double Mdotwind =
                Disc_Mdot(DISC_LOSS_GLOBAL)+
                Disc_Mdot(DISC_LOSS_ISM)+
                Disc_Mdot(DISC_LOSS_FUV)+
                Disc_Mdot(DISC_LOSS_XRAY)+
                Disc_Mdot(DISC_LOSS_INNER_EDGE_STRIP)+
                Disc_Mdot(DISC_LOSS_OUTER_EDGE_STRIP);
            const double Jdotwind =
                Disc_Jdot(DISC_LOSS_GLOBAL)+
                Disc_Jdot(DISC_LOSS_ISM)+
                Disc_Jdot(DISC_LOSS_FUV)+
                Disc_Jdot(DISC_LOSS_XRAY)+
                Disc_Jdot(DISC_LOSS_INNER_EDGE_STRIP)+
                Disc_Jdot(DISC_LOSS_OUTER_EDGE_STRIP);
            const double hwind = Is_really_not_zero(Mdotwind) ?
                (Jdotwind / Mdotwind) : 0.0;
            const double min_Toomre_Q = disc_minimum_Toomre_Q(stardata,disc,b);
            const double HRin = Disc_scale_height_ratio(in);
            const double HRout = Disc_scale_height_ratio(out);
            if(disc->firstlog == TRUE)
            {
                /*
                 * Start of the disc
                 */
                disc->firstlog = FALSE;
                Set_ensemble_rate(
                    "scalars",
                    "cbdisc formation rate",
                    (double)T);

                Set_ensemble_rate(
                    "distributions",
                    "cbdisc formation rate : label->dist",
                    "all",
                    "stellar type",(int)disc->overflower_stellar_type
                    );

                /* ZAMS of disc progenitors */
                Disc_rate("cbdisc initial disc log ZAMS stellar mass",
                          "map",
                          "log mass overflower",Bin_data(Safelog10(stardata->common.zero_age.mass[overflower]),0.1),
                          "log mass companion",Bin_data(Safelog10(stardata->common.zero_age.mass[Other_star(overflower)]),0.1));


                Disc_rate(
                    "cbdisc initial disc log ZAMS stellar mass",
                    "dist",
                    "log mass overflower",Bin_data(Safelog10(stardata->common.zero_age.mass[overflower]),0.1)
                    );
                Disc_rate(
                    "cbdisc initial disc log ZAMS orbital period",
                    "dist",
                    "log period",Bin_data(Safelog10(stardata->common.zero_age.orbital_period[0]),0.1)
                    );
                Disc_rate(
                    "cbdisc initial disc log ZAMS separation",
                    "dist",
                    "log separation",Bin_data(Safelog10(stardata->common.zero_age.separation[0]),0.1)
                    );
                Disc_rate(
                    "cbdisc initial disc log ZAMS eccentricity",
                    "dist",
                    "eccentricity",Bin_data(stardata->common.zero_age.eccentricity[0],0.02)
                    );

                /* pre-comenv system information */
                Disc_rate(
                    "cbdisc initial disc log stellar mass pre-comenv",
                    "dist",
                    "log mass",Bin_data(Safelog10(stardata->previous_stardata->star[overflower].mass),0.1)
                    );
                Disc_rate(
                    "cbdisc initial disc log orbital period pre-comenv",
                    "dist",
                    "log period",Bin_data(Safelog10(stardata->previous_stardata->common.orbit.period),0.1)
                    );
                Disc_rate(
                    "cbdisc initial disc log separation pre-comenv",
                    "dist",
                    "log separation",Bin_data(Safelog10(stardata->previous_stardata->common.orbit.separation),0.1)
                    );

                Disc_rate(
                    "cbdisc initial disc log stellar mass pre-comenv",
                    "map",
                    "log mass overflower",Bin_data(Safelog10(stardata->previous_stardata->star[overflower].mass),0.1),
                    "log mass companion",Bin_data(Safelog10(stardata->previous_stardata->star[Other_star(overflower)].mass),0.1)
                    );

                if(system_is_binary == TRUE)
                {
                    Disc_rate(
                        "cbdisc comenv log orbital period",
                        "map",
                        "before",Bin_data(Safelog10(stardata->previous_stardata->common.orbit.period),0.1),
                        "after",Bin_data(Safelog10(stardata->common.orbit.period),0.1)
                        );
                    Disc_rate(
                        "cbdisc comenv log orbital separation",
                        "map",
                        "before",Bin_data(Safelog10(stardata->previous_stardata->common.orbit.separation),0.1),
                        "after",Bin_data(Safelog10(stardata->common.orbit.separation),0.1)
                        );
                    Disc_rate(
                        "cbdisc comenv eccentricity",
                        "map",
                        "before",Bin_data(stardata->previous_stardata->common.orbit.eccentricity,0.02),
                        "after",Bin_data(stardata->common.orbit.eccentricity,0.02)
                        );
                    Disc_rate(
                        "cbdisc comenv angular momentum",
                        "map",
                        "before",Bin_data(Safelog10(Angular_momentum_code_to_cgs(stardata->previous_stardata->common.orbit.angular_momentum)),0.1),
                        "after",Bin_data(Safelog10(Angular_momentum_code_to_cgs(stardata->common.orbit.angular_momentum)),0.1)
                        );

                    Disc_rate(
                        "cbdisc initial disc log eccentricity",
                        "dist",
                        "eccentricity",Bin_data(stardata->common.orbit.eccentricity,0.02)
                        );
                    /* post-comenv system information */
                    Disc_rate(
                        "cbdisc initial log stellar mass post-comenv",
                        "dist",
                        "log mass",Bin_data(Safelog10(stardata->star[overflower].mass),0.1)
                        );
                    Disc_rate(
                        "cbdisc initial log orbital period post-comenv",
                        "dist",
                        "log orbital period",Bin_data(Safelog10(stardata->common.orbit.period),0.1)
                        );
                    Disc_rate(
                        "cbdisc initial log separation post-comenv",
                        "dist",
                        "log separation",Bin_data(Safelog10(stardata->common.orbit.separation),0.1)
                        );
                    Disc_rate(
                        "cbdisc initial log orbital angular momentum post-comenv",
                        "dist",
                        "log separation",Bin_data(Safelog10(stardata->common.orbit.angular_momentum),0.1)
                        );
                    Disc_rate(
                        "cbdisc initial log (disc angular momentum/orbital angular momentum)",
                        "dist",
                        "log angular momentum",Bin_data(Safelog10(Angular_momentum_cgs_to_code(disc->J)/stardata->common.orbit.angular_momentum),0.1)
                        );
                }
                Disc_rate(
                    "cbdisc initial disc log stellar mass post-comenv",
                    "map",
                    "log mass overflower",Bin_data(Safelog10(stardata->star[overflower].mass),0.1),
                    "log mass companion",Bin_data(Safelog10(stardata->star[Other_star(overflower)].mass),0.1)
                    );

                /* disc at birth */
                Disc_rate(
                    "cbdisc initial disc log disc mass",
                    "dist",
                    "log mass",Bin_data(Safelog10(disc->M/M_SUN),0.1)
                    );
                Disc_rate(
                    "cbdisc initial disc log disc angular momentum",
                    "dist",
                    "log angular momentum",Bin_data(Safelog10(disc->J),0.1)
                    );
                Disc_rate(
                    "cbdisc initial disc log disc luminosity",
                    "dist",
                    "log luminosity",Bin_data(Safelog10(Ldisc/L_SUN),0.1)
                    );
                Disc_rate(
                    "cbdisc initial disc log disc Rin",
                    "dist",
                    "log Rin",Bin_data(Safelog10(disc->Rin/R_SUN),0.1)
                    );
                Disc_rate(
                    "cbdisc initial disc log disc Rout",
                    "dist",
                    "log Rout",Bin_data(Safelog10(disc->Rout/R_SUN),0.1)
                    );
                if(system_is_binary == TRUE)
                {
                    Disc_rate(
                        "cbdisc initial disc log Rin/a",
                        "dist",
                        "log Rin/a",Bin_data(Safelog10(disc->Rin/R_SUN/stardata->common.orbit.separation),0.1)
                        );
                    Disc_rate(
                        "cbdisc initial disc log Rout/a",
                        "dist",
                        "log Rout/a",Bin_data(Safelog10(disc->Rout/R_SUN/stardata->common.orbit.separation),0.1)
                        );
                }
                Disc_rate(
                    "cbdisc initial disc overflower stellar type",
                    "dist",
                    "stellar type",(int)stardata->previous_stardata->star[overflower].stellar_type
                    );
                Disc_rate(
                    "cbdisc initial disc overflower stellar types",
                    "map",
                    "stellar type overflower",(int)stardata->previous_stardata->star[overflower].stellar_type,
                    "stellar type companion",(int)stardata->previous_stardata->star[Other_star(overflower)].stellar_type
                    );
            } /* new disc */

            /*
             * Generic disc
             */

            /* counts */
            Set_ensemble_count(
                "scalars",
                "Disc count",
                (double)T);


            /* stellar properties */
            Disc_count(
                "cbdisc log stellar mass",
                "map",
                "log mass overflower",Bin_data(Safelog10(stardata->star[overflower].mass),0.1),
                "log mass companion",Bin_data(Safelog10(stardata->star[Other_star(overflower)].mass),0.1)
                );
            Set_ensemble_count(
                "distributions",
                "cbdisc log Teff : label->dist",
                "overflower",
                "log Teff overflower",Bin_data(Safelog10(Teff(overflower)),0.1)
                );
            Set_ensemble_count(
                "distributions",
                "cbdisc log Teff : label->dist",
                "companion",
                "log Teff companion",Bin_data(Safelog10(Teff(Other_star(overflower))),0.1)
                );
            if(system_is_binary == TRUE)
            {
                Disc_count(
                    "cbdisc log Teff",
                    "map",
                    "log Teff overflower",Bin_data(Safelog10(Teff(overflower)),0.1),
                    "log Teff companion",Bin_data(Safelog10(Teff(Other_star(overflower))),0.1)
                    );
                Disc_count(
                    "cbdisc log stellar luminosity",
                    "map",
                    "log luminosity overflower",Bin_data(Safelog10(stardata->star[overflower].luminosity),0.1),
                    "log luminosity companion",Bin_data(Safelog10(stardata->star[Other_star(overflower)].luminosity),0.1)
                    );
            }
            Disc_count(
                "cbdisc log stellar X-ray luminosity",
                "dist",
                "log luminosity",Bin_data(Safelog10(b->LX/L_SUN),0.1)
                );
            Set_ensemble_count(
                "distributions",
                "cbdisc stellar type : label->dist",
                "overflower",
                "stellar type",(int)stardata->star[overflower].stellar_type
                );
            Set_ensemble_count(
                "distributions",
                "cbdisc stellar type : label->dist",
                "companion",
                "stellar type",(int)stardata->star[Other_star(overflower)].stellar_type
                );
            Disc_count(
                "cbdisc stellar types",
                "map",
                "stellar type overflower",(int)stardata->star[overflower].stellar_type,
                "stellar type companion",(int)stardata->star[Other_star(overflower)].stellar_type
                );

            /* binary-property distributions */
            if(system_is_binary == TRUE)
            {
                Disc_count(
                    "cbdisc log orbital angular momentum",
                    "dist",
                    "log angular momentum",Bin_data(Safelog10(Angular_momentum_code_to_cgs(stardata->common.orbit.angular_momentum)),0.1)
                    );
                Disc_count(
                    "cbdisc log orbital period",
                    "dist",
                    "log period",Bin_data(Safelog10(stardata->common.orbit.period),0.1)
                    );
                Disc_count(
                    "cbdisc log separation",
                    "dist",
                    "log separation",Bin_data(Safelog10(stardata->common.orbit.separation),0.1)
                    );
                Disc_count(
                    "cbdisc eccentricity",
                    "dist",
                    "log eccentricity",Bin_data(stardata->common.orbit.eccentricity,0.02)
                    );
                Disc_count(
                    "cbdisc log resonant edot",
                    "dist",
                    "log edot",Bin_data(Safelog10(disc->loss[DISC_LOSS_RESONANCES].edot),0.1)
                    );

                /* binary-property maps */
                Disc_count(
                    "cbdisc log orbital period-eccentricity",
                    "map",
                    "log period",Bin_data(Safelog10(stardata->common.orbit.period),0.1),
                    "log eccentricity",Bin_data(stardata->common.orbit.eccentricity,0.02)
                    );
                Disc_count(
                    "cbdisc log separation-eccentricity",
                    "map",
                    "log separation",Bin_data(Safelog10(stardata->common.orbit.separation),0.1),
                    "log eccentricity",Bin_data(stardata->common.orbit.eccentricity,0.02)
                    );
                Disc_count(
                    "cbdisc log Teff-eccentricity",
                    "map",
                    "log Teff",Bin_data(Safelog10(Teff(overflower)),0.1),
                    "log eccentricity",Bin_data(stardata->common.orbit.eccentricity,0.02)
                    );
                Disc_count(
                    "cbdisc log Teff-log orbital period",
                    "map",
                    "log Teff",Bin_data(Safelog10(Teff(overflower)),0.1),
                    "log period",Bin_data(Safelog10(stardata->common.orbit.period),0.1)
                    );
                Disc_count(
                    "cbdisc log Teff-log separation","map",
                    "log Teff",Bin_data(Safelog10(Teff(overflower)),0.1),
                    "log separation",Bin_data(Safelog10(stardata->common.orbit.separation),0.1)
                    );
            }

            /* disc properties */

            /* mass vs ... */
            Disc_count(
                "cbdisc log disc mass","dist",

                "log mass",Bin_data(Safelog10(disc->M/M_SUN),0.1)
                );

            if(system_is_binary == TRUE)
            {
                Disc_count(
                    "cbdisc log disc mass-eccentricity",
                     "map",
                    "log mass",Bin_data(Safelog10(disc->M/M_SUN),0.1),
                    "log eccentricity",Bin_data(stardata->common.orbit.eccentricity,0.02)
                    );
                Disc_count(
                    "cbdisc log disc mass-orbital period",
                    "map",
                    "log mass",Bin_data(Safelog10(disc->M/M_SUN),0.1),
                    "log period",Bin_data(Safelog10(stardata->common.orbit.period),0.1)
                    );
                Disc_count(
                    "cbdisc log disc mass-separation",
                    "map",
                    "log mass",Bin_data(Safelog10(disc->M/M_SUN),0.1),
                    "log separation",Bin_data(Safelog10(stardata->common.orbit.separation),0.1)
                    );
            }

            /* angular momentum vs ... */
            Disc_count(
                "cbdisc log disc angular momentum",
                "dist",
                "log angular momentum",Bin_data(Safelog10(disc->J),0.1)
                );

            if(system_is_binary == TRUE)
            {
                Disc_count(
                    "cbdisc log (disc angular momentum/orbital angular momentum)",
                    "dist",
                    "log angular momentum",Bin_data(Safelog10(Angular_momentum_cgs_to_code(disc->J)/stardata->common.orbit.angular_momentum),0.1)
                    );
                Disc_count(
                    "cbdisc log disc angular momentum-eccentricity",
                    "map",
                    "log angular momentum",Bin_data(Safelog10(disc->J),0.1),
                    "log eccentricity",Bin_data(stardata->common.orbit.eccentricity,0.02)
                    );
                Disc_count(
                    "cbdisc log disc angular momentum-orbital period",
                    "map",
                    "log angular momentum",Bin_data(Safelog10(disc->J),0.1),
                    "log period",Bin_data(Safelog10(stardata->common.orbit.period),0.1)
                    );
                Disc_count(
                    "cbdisc log disc angular momentum-separation",
                    "map",
                    "log angular momentum",Bin_data(Safelog10(disc->J),0.1),
                    "log separation",Bin_data(Safelog10(stardata->common.orbit.separation),0.1)
                    );
                Disc_count(
                    "cbdisc log disc Rin/a",
                    "dist",
                    "log Rin/a",Bin_data(Safelog10(disc->Rin/R_SUN/stardata->common.orbit.separation),0.1)
                    );
                Disc_count(
                    "cbdisc log disc Rout/a",
                    "dist",
                    "log Rout/a",Bin_data(Safelog10(disc->Rout/R_SUN/stardata->common.orbit.separation),0.1)
                    );
            }

            /* other disc properties */
            Disc_count(
                "cbdisc log disc moment of inertia",
                "dist",
                "log moment of inertia",Bin_data(Safelog10(Idisc),0.1)
                );
            Disc_count(
                "cbdisc log disc specific angular momentum",
                "dist",
                "log specific angular momentum",Bin_data(Safelog10(disc->J/disc->M),0.1)
                );
            Disc_count(
                "cbdisc log disc luminosity",
                "dist",
                "log luminosity",Bin_data(Safelog10(Ldisc/L_SUN),0.1)
                );
            Disc_count(
                "cbdisc log disc luminosity ratio",
                "dist",
                "log (Ldisc/Lstars)",Bin_data(Safelog10((Ldisc/L_SUN)/system_luminosity),0.1)
                );
            Disc_count(
                "cbdisc log disc Rin",
                "dist",
                "log Rin",Bin_data(Safelog10(disc->Rin/R_SUN),0.1)
                );
            Disc_count(
                "cbdisc log disc Rout",
                "dist",
                "log Rout",Bin_data(Safelog10(disc->Rout/R_SUN),0.1)
                );
            Disc_count(
                "cbdisc log disc half mass radius",
                "dist",
                "log RMhalf",Bin_data(Safelog10(RMhalf/R_SUN),0.1)
                );
            Disc_count(
                "cbdisc log disc half angular momentum radius",
                "dist",
                "log RJhalf",Bin_data(Safelog10(RJhalf/R_SUN),0.1)
                );
            Disc_count(
                "cbdisc log disc Tin",
                "dist",
                "log Tin",Bin_data(Safelog10(Tin),0.1)
                );
            Disc_count(
                "cbdisc log disc Tout",
                "dist",
                "log Tout",Bin_data(Safelog10(Tout),0.1)
                );
            Disc_count(
                "cbdisc log disc tvisc in",
                "dist",
                "log tvisc",Bin_data(Safelog10(disc_viscous_timescale(disc->Rin, disc,b) / YEAR_LENGTH_IN_SECONDS),0.1)
                );
            Disc_count(
                "cbdisc log disc tvisc out",
                "dist",
                "log tvisc",Bin_data(Safelog10(disc_viscous_timescale(disc->Rout,disc,b) / YEAR_LENGTH_IN_SECONDS),0.1)
                );
            Disc_count(
                "cbdisc log disc density_in",
                "dist",
                "log rhoin",Bin_data(Safelog10(disc_density(disc->Rin,disc,b)),0.1)
                );
            Disc_count(
                "cbdisc log disc density_out",
                "dist",
                "log rhoout",Bin_data(Saferlog10(disc_density(disc->Rout,disc,b)),0.1)
                );
            Disc_count(
                "cbdisc log disc column density_in",
                "dist",
                "log rhoin",Bin_data(Safelog10(disc_column_density(disc->Rin,disc)),0.1)
                );
            Disc_count(
                "cbdisc log disc column density_out",
                "dist",
                "log rhoout",Bin_data(Safelog10(disc_column_density(disc->Rout,disc)),0.1)
                );
            Disc_count(
                "cbdisc log disc H/R inner",
                "dist",
                "log H/R",Bin_data(Limit_range(HRin,0.0,10.0),0.1)
                );
            Disc_count(
                "cbdisc log disc H/R outer",
                "dist",
                "log H/R",Bin_data(Limit_range(HRout,0.0,10.0),0.1)
                );
            Disc_count(
                "cbdisc num thermal zones",
                "dist",
                "num thermal zones",(int)disc->n_thermal_zones
                );

            /* disc mass loss */
            Disc_count(
                "cbdisc log Mdot viscous",
                "dist",
                "log Mdot",Bin_data(Safelog10(Disc_Mdot(DISC_LOSS_INNER_VISCOUS)),0.1)
                );
            Disc_count(
                "cbdisc log Mdot ISM",
                "dist",
                "log Mdot",Bin_data(Safelog10(Disc_Mdot(DISC_LOSS_ISM)),0.1)
                );
            Disc_count(
                "cbdisc log Mdot inner L2",
                "dist",
                "log Mdot",Bin_data(Safelog10(Disc_Mdot(DISC_LOSS_INNER_L2_CROSSING)),0.1)
                );
            Disc_count(
                "cbdisc log Mdot FUV",
                "dist",
                "log Mdot",Bin_data(Safelog10(Disc_Mdot(DISC_LOSS_FUV)),0.1)
                );
            Disc_count(
                "cbdisc log Mdot X-ray",
                "dist",
                "log Mdot",Bin_data(Safelog10(Disc_Mdot(DISC_LOSS_XRAY)),0.1)
                );
            Disc_count(
                "cbdisc log Mdot inner edge",
                "dist",
                "log Mdot",Bin_data(Safelog10(Disc_Mdot(DISC_LOSS_INNER_EDGE_STRIP)),0.1)
                );
            Disc_count(
                "cbdisc log Mdot outer edge",
                "dist",
                "log Mdot",Bin_data(Safelog10(Disc_Mdot(DISC_LOSS_OUTER_EDGE_STRIP)),0.1)
                );
            /* disc angular momentum loss */
            Disc_count(
                "cbdisc log Jdot viscous",
                "dist",
                "log Jdot",Bin_data(Safelog10(Disc_Jdot(DISC_LOSS_INNER_VISCOUS)),0.1)
                );
            Disc_count(
                "cbdisc log Jdot ISM",
                "dist",
                "log Jdot",Bin_data(Safelog10(Disc_Jdot(DISC_LOSS_ISM)),0.1)
                );
            Disc_count(
                "cbdisc log Jdot inner L2",
                "dist",
                "log Jdot",Bin_data(Safelog10(Disc_Jdot(DISC_LOSS_INNER_L2_CROSSING)),0.1)
                );
            Disc_count(
                "cbdisc log Jdot FUV",
                "dist",
                "log Jdot",Bin_data(Safelog10(Disc_Jdot(DISC_LOSS_FUV)),0.1)
                );
            Disc_count(
                "cbdisc log Jdot X-ray",
                "dist",
                "log Jdot",Bin_data(Safelog10(Disc_Jdot(DISC_LOSS_XRAY)),0.1)
                );
            Disc_count(
                "cbdisc log Jdot inner edge",
                "dist",
                "log Jdot",Bin_data(Safelog10(Disc_Jdot(DISC_LOSS_INNER_EDGE_STRIP)),0.1)
                );
            Disc_count(
                "cbdisc log Jdot outer edge",
                "dist",
                "log Jdot",Bin_data(Safelog10(Disc_Jdot(DISC_LOSS_OUTER_EDGE_STRIP)),0.1)
                );
            Disc_count(
                "cbdisc log Jdot binary torque",
                "dist",
                "log Jdot",Bin_data(Safelog10(Disc_Jdot(DISC_LOSS_BINARY_TORQUE)),0.1)
                );
            /* misc. other disc properties */
            Disc_count(
                "log wind specific angular momentum",
                "dist",
                "log hwind",Bin_data(Safelog10(hwind),0.1)
                );
            Disc_count(
                "log minimum Toomre Q",
                "dist",
                "log Q",Bin_data(Safelog10(min_Toomre_Q),0.1)
                );
            Disc_count(
                "cbdisc log absolute potential energy",
                "dist",
                "log potential energy",Bin_data(Safelog10(-disc_total_gravitational_potential_energy(disc,b)),0.1)
                );
            Disc_count(
                "cbdisc log kinetic energy",
                "dist",
                "log kinetic energy",Bin_data(Safelog10(disc_total_kinetic_energy(disc,b)),0.1)
                );

            /*
             * Clean up allocated structure
             */
            Safe_free(b);
        }
    }

}
#endif // DISCS
#endif // STELLAR_POPULATIONS_ENSEMBLE
