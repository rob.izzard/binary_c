#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "RLOF_prototypes.h"
#include "RLOF_mass_transfer_rate.h"
#include "RLOF.h"

/*
 * Function which returns the instantaneous mass transfer
 * rate during RLOF.
 *
 * There are multiple prescriptions for this, and really this
 * code should be less complicated :/ sorry...
 */

//#define RETURN_BSE_IF_NEGATIVE_DRDM

/* verbose warnings */
#define SHOW_WARNINGS

/*
 * Skip dmdr convergence if the star is "convective" :
 * sometimes this fails because e.g. early-HG stars may
 * find a solution with dmdr>0 but a simple check on the
 * stellar type will not.
 */
//#define ADAPTIVE_RLOF_CONVECTIVE_CHECK

/* PRETTYLOG is not thread safe */
//#define PRETTYLOG

double Pure_function RLOF_instantaneous_mass_transfer_rate(
    const double r,
    const double roche_radius,
    const double mass,
    struct stardata_t * const stardata,
    struct star_t * const donor /* "primary" */)
{
    /*
     * Mass transfer rate becaues of Roche lobe overflow
     */
    double mdot = 0.0; // calculate (and returned) mass transfer rate
    double mdotBSE = 0.0;// Hurley rate
    const double menv Maybe_unused = donor->mass - donor->core_mass[CORE_He]; // envelope mass
    const double dt = 1e6 * stardata->model.dtm; // timestep (years)

    /*
     * Options: can we apply the various limits?
     */
    Boolean can_apply_limits = TRUE;
    Boolean apply_dynamical_limit Maybe_unused = Boolean_(donor->stellar_type < HeWD);
    Boolean apply_thermal_limit Maybe_unused = Boolean_(donor->stellar_type < HeWD);
    Boolean apply_envelope_limit Maybe_unused = Boolean_(donor->mass - donor->core_mass[CORE_He] > TINY);

    /* strict BSE options  (Hurley+ 2002 Sections 2.6.2, 2.6.3) */
    apply_thermal_limit = Boolean_(GIANT_LIKE_STAR(donor->stellar_type));
    apply_dynamical_limit = Boolean_(!GIANT_LIKE_STAR(donor->stellar_type));

    /* thermal mass loss rate */
    const double thermal_rate_limit = RLOF_donor_thermal_rate_limit(stardata,donor);

    /* dynamical mass loss rate */
    const double dynamical_rate_limit = RLOF_donor_dynamical_rate_limit(stardata,donor);

    /* envelope rate */
    const double envelope_rate_limit = RLOF_donor_envelope_rate_limit(stardata,donor);

#ifdef ADAPTIVE_RLOF
    /*
     * Multipler for the KH (thermal) timescale limit
     *
     * 20 matches ~ Wellstein
     */
    const double tkh_multiplier = 1.0;
    if(unlikely(Is_zero(donor->RLOF_starttime)))
    {
        donor->RLOF_starttime=stardata->model.time;
        donor->RLOF_tkh = tkh_multiplier * 1e-6 * donor->tkh; // KH timescale in Myr
    }
#ifdef ADAPTIVE_RLOF_LOG
    /* logging */
    stardata->common.thermal_cap = FALSE;
    stardata->common.runaway_rlof = FALSE;
    stardata->common.used_rate = RLOF_RATE_UNDEF;
#endif // ADAPTIVE_RLOF_LOG
#endif // ADAPTIVE_RLOF

#ifdef PRETTYLOG
    int status=STATUS_NONE;
#endif //PRETTYLOG

    Dprint("timestep=%g(y) star %d st=%d r=%g roche_radius=%g (r/roche_radius=%3.2f %%) mass=%g sep=%g age 1=%g 2=%g\n",
           dt,
           donor->starnum,
           donor->stellar_type,r,roche_radius,100.0*r/roche_radius,mass,stardata->common.orbit.separation,
           stardata->star[0].age,stardata->star[1].age);

    if(donor->stellar_type == MASSLESS_REMNANT)
    {
        mdot = 0.0;
    }
    else
    {
        /*
         * Hurley et al 2002 (BSE) rate formula, perhaps modified
         * by the Claeys factor
         */
        mdotBSE = hurley_or_claeys_rate(r,roche_radius,mass,donor,stardata);




        /*
         * Calculate sign of the timestep:
         * It can be negative during the interpolation phase,
         * in this case just return the Hurley 02 rate
         */
        if(stardata->preferences->RLOF_method==RLOF_METHOD_BSE ||
           stardata->preferences->RLOF_method==RLOF_METHOD_CLAEYS ||
           dt < TINY)
        {
#ifdef DEBUG
            if(dt<TINY)
            {
                Dprint("dt < 0 so just use Hurley rate");
            }
            else
            {
                Dprint("use Hurley rate = %g",mdotBSE);
            }
#endif//DEBUG
            mdot = mdotBSE;
        }

        else if(stardata->preferences->RLOF_method == RLOF_METHOD_ADAPTIVE2)
        {
#ifdef ADAPTIVE_RLOF2
            /*
             * We try to converge slightly outside the Roche
             * Lobe for numerical stability purposes
             */
            mdot = RLOF_adaptive_mass_transfer_rate2(r,
                                                     roche_radius*(1.0+1e-5),
                                                     mass,
                                                     stardata,
                                                     donor,
                                                     dt,
                                                     &can_apply_limits);
            mdot *= stardata->preferences->RLOF_mdot_factor;
#else
            Exit_binary_c(BINARY_C_ALGORITHM_OUT_OF_RANGE,
                          "Adaptive RLOF 2 is not available : you must turn on ADAPTIVE_RLOF2 in binary_c_parameters.h and recompile\n");
#endif // ADAPTIVE_RLOF2
        }

        else if(stardata->preferences->RLOF_method == RLOF_METHOD_ADAPTIVE)
        {
            // default to BSE rate
            mdot = mdotBSE;

#ifdef ADAPTIVE_RLOF
            mdot = RLOF_adaptive_mass_transfer_rate(r,
                                                    roche_radius,
                                                    mass,
                                                    stardata,
                                                    donor,
                                                    mdotBSE,
                                                    dt);

            stardata->common.mdot_RLOF_BSE = mdotBSE; // save for logging

#ifdef PRETTYLOG
            double mdotwant=mdot;
            int cap=CAP_NONE;
#endif//PRETTYLOG

            /* rates of change of r, rL (for logging) */
            if(Is_zero(stardata->common.RLOF_rwas))
            {
                stardata->common.RLOF_rwas = r;
                stardata->common.RLOF_rolwas = roche_radius;
            }

#ifdef PRETTYLOG
            double rdot=(r-stardata->common.RLOF_rwas)/dt;
            double roldot=(roche_radius-stardata->common.RLOF_rolwas)/dt;
#endif//PRETTYLOG
            stardata->common.RLOF_rwas=r;
            stardata->common.RLOF_rolwas=roche_radius;

            /** calculate mdot limits **/

            /* envelope removal limit */
            double envelope_limit = Is_zero(ADAPTIVE_RLOF_MAX_ENV_FRAC) ?
                (menv/dt) :
                (More_or_equal(menv, ADAPTIVE_RLOF_MIN_ENV) ?
                 (ADAPTIVE_RLOF_MAX_ENV_FRAC*menv/dt) : 0.0);

            Dprint("Limits: want mdot=%g but dynamical_limit=%g thermal_limit=%g env(%3.2f%%)=%g\n",
                   mdot,
                   dynamical_rate_limit,
                   thermal_rate_limit,
                   ADAPTIVE_RLOF_MAX_ENV_FRAC,
                   envelope_limit);

            if(apply_dynamical_limit==TRUE &&
               More_or_equal(mdot ,dynamical_rate_limit))
            {
                /* limit mdot to the dynamical rate */
                mdot = Min(mdot,dynamical_rate_limit);

#ifdef ADPATIVE_RLOF_LOG
                cap = CAP_DYNAMICAL;
#endif//ADAPTIVE_RLOF_LOG
                donor->prev_dm=0.0;
            }

            if(apply_thermal_limit==TRUE &&
               More_or_equal(mdot ,thermal_rate_limit))
            {
                /* Limit mdot to the thermal rate */
                mdot = Min(mdot,thermal_rate_limit);

#ifdef ADAPTIVE_RLOF_LOG
                stardata->common.thermal_cap = TRUE;
#endif//ADAPTIVE_RLOF_LOG
#ifdef PRETTYLOG
                cap=CAP_THERMAL;
#endif//PRETTYLOG
                donor->prev_dm=0.0;
            }


            /* limit to a fraction of the (envelope) mass in any one timestep */
            if(apply_envelope_limit==TRUE &&
               More_or_equal(mdot, envelope_limit))
            {
                Dprint("t=%g %sLimit mass-transfer to %g x Menv%s (menv=%g, mdot was %g now %g) dt=%g\n",
                       stardata->model.time,
                       stardata->store->colours[YELLOW],
                       ADAPTIVE_RLOF_MAX_ENV_FRAC,
                       stardata->store->colours[COLOUR_RESET],menv,
                       mdot,
                       envelope_limit,
                       dt);

#ifdef PRETTYLOG
                cap=CAP_ENVELOPE;
#endif//PRETTY_LOG
                mdot = envelope_limit;

                donor->prev_dm=0.0;
            }

#ifdef RLOF_MDOT_MODULATION
            /*
             * Global modulation factor
             */
            mdot *= stardata->preferences->RLOF_mdot_factor;
            Dprint("global mdot modulation by factor %g to %g\n",
                   stardata->preferences->RLOF_mdot_factor,
                   mdot);
#endif//RLOF_MDOT_MODULATION

#ifdef VISCOUS_RLOF
            /*
              printf("VISCOUS Clamp st=%d want mdot=%g (was %g) but should be in range %g to %g: %d %d %d %d\n",
              donor->stellar_type,
              mdot,stardata->common.prevt_mdot_RLOF,
              stardata->common.prevt_mdot_RLOF*VISCOUS_RLOF_MIN_FAC,
              stardata->common.prevt_mdot_RLOF*VISCOUS_RLOF_MAX_FAC,
              donor->stellar_type>1,
              Is_not_zero(stardata->common.prevt_mdot_RLOF),
              mdot > stardata->common.prevt_mdot_RLOF*VISCOUS_RLOF_MAX_FAC,
              mdot < stardata->common.prevt_mdot_RLOF*VISCOUS_RLOF_MIN_FAC
              );
            */

            /* artificial viscosity for mdot */
            if(donor->stellar_type>1)// && Is_not_zero(stardata->common.prevt_mdot_RLOF))
            {
                if((mdot > stardata->common.prevt_mdot_RLOF*VISCOUS_RLOF_MAX_FAC)||
                   (mdot < stardata->common.prevt_mdot_RLOF*VISCOUS_RLOF_MIN_FAC))
                {
#ifdef VISCOUS_RLOF_TRIGGER_SMALLER_TIMESTEP
                    stardata->common.viscous_RLOF_wants_smaller_timestep=TRUE;
#endif//VISCOUS_RLOF_TRIGGER_SMALL_TIMESTEP
                    //double unclamped_mdot = mdot;
                    const double ref_mdot = Max(stardata->common.prevt_mdot_RLOF,
                                                1e-14);

                    Clamp(mdot,
                          ref_mdot*VISCOUS_RLOF_MIN_FAC,
                          ref_mdot*VISCOUS_RLOF_MAX_FAC
                        );

                    /*
                      printf("VISCOUS Clamp t=%g mdot was %g want %g clamped %g\n",
                      stardata->model.time,
                      stardata->common.prevt_mdot_RLOF,unclamped_mdot,mdot);
                    */

                }
            }
#endif//VISCOUS_RLOF

            /* adapt timestep for MS stars */
            if(donor->stellar_type<2)
            {
                /*
                 * We have to adapt to something relatively stable, otherwise the
                 * explicit scheme just goes nuts. The radius excess dR = R - R_L is
                 * relatively smoothly behaved (and Mdot ~ dR^3 is less so).
                 */
                const double fff=Max(1.0,donor->radius/(donor->radius - roche_radius));
                const double fmsm = donor->stellar_type==1 ? 0.01 : 1.0;
                stardata->common.suggested_timestep = 0.05 * fff;

                stardata->common.suggested_timestep =
                    Max(fmsm*donor->tkh,
                        stardata->common.suggested_timestep);
            }
            else
            {
                /*
                 * For post-MS stars just make sure we resolve the thermal timescale
                 * because that is the timescale of the mass transfer: again, this will
                 * be smoothly behaved.
                 */
                stardata->common.suggested_timestep = 1e6*(donor->tkh * 1e-3);
            }

#ifdef PRETTYLOG
            {
                /*
                 * Deprecated! the static int will break everything
                 */
#warning This static int should not be here!
                static int zerocount=0;
                zerocount = (Is_zero(mdot)) ? zerocount+1 : 0;
                const char *colours[] = {stardata->store->colours[MAGENTA],stardata->store->colours[GREEN],stardata->store->colours[RED],stardata->store->colours[RED],stardata->store->colours[YELLOW],stardata->store->colours[RED]};
                const char *statstrings[] = {"","Cnvrgd","dmdr<0","dr=0","WDenv","mt<0"};
                const char *capcolours[] = {stardata->store->colours[GREEN],stardata->store->colours[BRIGHT_YELLOW],stardata->store->colours[BRIGHT_RED]};
                const char *capstrings[] = {"   ","(T)","(E)"};
                const char *rolmov[] = {"(Detaching)","(Attaching)"};
                const double tdyn_local = dynamical_timescale(star);

                fprintf(stderr,
                        "%d st=%d t=% 20.10e P=% 20.10e dt=% 12g tdyn=% 12g tkh=% 12g m=% 12g r=% 12g roche_radius=% 12g (r/roche_radius=%5.4f) rdot=% 12g roldot=% 12g %s %s %6s mdot=% 12g wanted=% 12g %scap=%d %s %ssuggested dt=%g tkh=%g zero=%d dmdr=%g %s\n",
                        donor->starnum,
                        donor->stellar_type,
                        stardata->model.time,
                        stardata->common.orbit.period*YEAR_LENGTH_IN_DAYS,
                        dt,
                        tdyn_local,
                        donor->tkh,
                        donor->mass,
                        r,roche_radius,r/roche_radius,
                        rdot,roldot,
                        rolmov[rdot>roldot],
                        colours[status],
                        statstrings[status],
                        mdot,
                        mdotwant,
                        capcolours[cap],
                        cap,
                        capstrings[cap],
                        stardata->store->colours[CYAN],
                        stardata->common.suggested_timestep,
                        donor->tkh,
                        zerocount,
                        radius_mass_derivative(stardata,donor,donor->mass),
                        stardata->store->colours[COLOUR_RESET]);
            }
#endif // PRETTYLOG


#else
            Exit_binary_c(BINARY_C_ALGORITHM_OUT_OF_RANGE,
                          "Adaptive RLOF is not available : you must turn on ADAPTIVE_RLOF in binary_c_parameters.h and recompile\n");
#endif //ADAPTIVE_RLOF

            Nancheck(mdot);
            Nancheck(roche_radius);

            /* never allow mdot to be negative */
            if(unlikely(mdot<0.0)) mdot=0.0;

            Dprint("%sFINAL MDOT RLOF = %g (final R=%g RL=%g)%s\n",
                   stardata->store->colours[YELLOW],mdot,
                   donor->radius,roche_radius,
                   stardata->store->colours[COLOUR_RESET]);
        }


#ifdef ADAPTIVE_RLOF
        stardata->common.mdot_RLOF=mdot;
        stardata->common.prevt_mdot_RLOF=mdot;
#endif // ADAPTIVE_RLOF
    }

    /*
     * Apply limits to the mass transfer rate
     */
#ifdef DEBUG
    const double mdotwas = mdot;
#endif

    if(can_apply_limits == TRUE)
    {
        if(apply_thermal_limit==TRUE &&
           unlikely(More_or_equal(mdot, thermal_rate_limit)))
        {
            /* Limit mdot to the thermal rate */
            mdot = Min(mdot,thermal_rate_limit);
#ifdef DEBUG
            if(!Fequal(mdotwas,mdot))
            {
                Dprint("Mass transfer rate limited by thermal limit\n");
            }
#endif
        }

        if(apply_dynamical_limit==TRUE &&
           unlikely(More_or_equal(mdot,dynamical_rate_limit)))
        {
            /* limit mdot to the dynamical rate */
            mdot = Min(mdot,dynamical_rate_limit);
#ifdef DEBUG
            if(!Fequal(mdotwas,mdot))
            {
                Dprint("Mass transfer rate limited by dynamical limit\n");
            }
#endif
        }

        if(apply_envelope_limit==TRUE &&
           unlikely(More_or_equal(mdot,envelope_rate_limit)))
        {
            /* limit mdot to the envelope rate */
            mdot = Min(mdot,envelope_rate_limit);
#ifdef DEBUG
            if(!Fequal(mdotwas,mdot))
            {
                Dprint("Mass transfer rate limited by envelope limit\n");
            }
#endif
        }
    }

    return mdot;
}

void No_return RLOF_exit(struct stardata_t * stardata)
{
    Dprint("Initial conditions:\nM1=%g; M2=%g; ",
           stardata->common.zero_age.mass[0],
           stardata->common.zero_age.mass[1]);
    Dprint("PER=%g; ECC=%g; ",
           stardata->common.zero_age.orbital_period[0],
           stardata->common.zero_age.eccentricity[0]);
    Dprint("Z=%g\n",
           stardata->common.metallicity);
    Exit_binary_c(BINARY_C_RLOF_EXIT,"RLOF voluntary exit");
}

#ifdef ADAPTIVE_RLOF

double Constant_function calc_target_radius(const double r,
                                            const double roche_radius,
                                            struct stardata_t * const stardata)
{
    /*
     * Calculate target radius with numerical fudge factor
     *  to target just outside the roche lobe
     * for stability and speed
     */
    const double target_radius =  roche_radius * ADAPTIVE_RLOF_TARGET_FACTOR;

    Dprint("Target radius = %g (r=%g roche_radius=%g roche_radius*factor=%g)\n",
           target_radius,r,roche_radius,roche_radius*ADAPTIVE_RLOF_TARGET_FACTOR);

    return target_radius;
}



#endif // ADAPTIVE_RLOF


double Pure_function hurley_or_claeys_rate(const double r,
                                           const double roche_radius,
                                           const double mass,
                                           const struct star_t * const star,
                                           const struct stardata_t * const stardata)
{
    /*
     * Hurley et al 2002 (BSE) rate formula
     * perhaps modulated by the Claeys factor.
     */
    double mdot;

    /*
     * R<RL is unlikely because we're called from
     * a function which is called when r>RL... but check
     * just in case.
     */
    if(unlikely(r < roche_radius))
    {
        mdot = 0.0;
    }
    else
    {
        mdot = 3.0e-06*Pow3(log(r/roche_radius))*Pow2(Min(mass,5.0));

        if(stardata->preferences->RLOF_method == RLOF_METHOD_CLAEYS)
        {
            /*
             * Apply Joke's correction function to the mass-transfer
             * rate (Claeys et al. 2013)
             */
            mdot *= Claeys_factor(star);
        }
        mdot = Max(0.0,mdot);
#ifdef RLOF_MDOT_MODULATION
        /* modulate with user-specified factor */
        mdot *=stardata->preferences->RLOF_mdot_factor;
#endif
    }
    return(mdot);
}


static double Pure_function Claeys_factor(const struct star_t * const loser)
{
    double f;
    const double Q = 1.0 / loser->q;
    if(Q < 1.0+TINY)
    {
        f = 1000.0;
    }
    else
    {
        /*
         * RGI : limited q to 100 to prevent
         * numerical overflow. This should be
         * very rare.
         */
        f = Max(1.0,
                1000.0/Q *
                exp(-0.5*Pow2(log(Min(100.0,Q))/0.15))
            );
    }
    return f;
}

double Pure_function ritter_rate(const double r,
                                 const double roche_radius,
                                 struct star_t * const donor,
                                 struct stardata_t * const stardata)
{
    /*
     * Requires implementation
     */

    /* photospheric density ?
     * this is a GUESS to match ~ Ritter's table A1
     */
    const double rho = 2e-7 *
        (donor->mass * M_SUN)/(4.0/3.0*PI*Pow3(donor->radius*R_SUN));
    const double Q=roche_stream_area(donor,stardata);
    const double Hp=surface_pressure_scale_height(donor,stardata)/R_SUN;
    const double vs=sound_speed(donor,stardata);
    const double mdot0 = rho * vs * Q / sqrt(exp(1.0));
    const double Dr=r-roche_radius;
    const double mdot = mdot0 * exp(Dr/Hp);

    Dprint("Ritter mdot=%g (mdot0=%g r=%g Rsun roche_radius=%g Rsun (Dr=%g) vs=%g cm/s Q=%g cm^2 rho=%g (g/cm^3) Hp=%g Rsun ; Hp/R=%g ; exp(%g)=%g)\n",
           mdot,mdot0,r,roche_radius,
           Dr,
           vs,Q,rho,Hp,Hp/r,
           Dr/Hp,
           exp(Dr/Hp)
        );

    return (mdot);
}

double Constant_function_if_no_debug surface_pressure_scale_height(struct star_t * const donor,
                                                                   struct stardata_t * const stardata)
{
    double Constant_function gamma0(double q);
    const double g0 = gamma0(donor->q);
    const double mu = molecular_weight(donor,stardata);
    const double H0 = GAS_CONSTANT * Teff_from_star_struct(donor) * Pow2(R_SUN*donor->radius)
        / (mu * GRAVITATIONAL_CONSTANT * donor->mass*M_SUN);
    const double Hp = H0/g0;

    Dprint("gamma0=%g H0=%g Rsun : Hp=%g Rsun\n",g0,H0/R_SUN,Hp/R_SUN);
    return Hp;
}

double Constant_function_if_no_debug sound_speed(struct star_t * const donor,
                                                 struct stardata_t * const stardata)
{
    /* surface sound speed (cm/s) */
    const double mu = molecular_weight(donor,stardata);
    const double vs = sqrt(GAS_CONSTANT*Teff_from_star_struct(donor)/mu);
    Dprint("vs=%g cm/s (teff=%g K ; mu=%g)\n",vs,Teff_from_star_struct(donor),mu);
    return vs;
}

double Constant_function gamma0(const double q)
{
    const double lq=log10(q);
    return q<1.0 ?
        (0.025*lq -0.038 * Pow2(lq)) :
        (0.039*lq -0.114 * Pow2(lq))
        ;
}

double Pure_function roche_stream_area(const struct star_t * const donor,
                                       const struct stardata_t * const stardata)
{
    /*
     * Meyer and Meyer-Hofmeister 1983
     * eqs 17 and 18
     */
    double Q; // stream area (cm^2)
    const double mu = molecular_weight(donor,stardata);
    const double Rg = GAS_CONSTANT/mu;
    const double m1 = M_SUN * stardata->star[0].mass;
    const double m2 = M_SUN * stardata->star[1].mass;
    const double a =  stardata->common.orbit.separation*R_SUN;
    const double xL = effective_Roche_radius(stardata,donor)/stardata->common.orbit.separation;
    const double k0 = m1/Pow3(xL)+m2/Pow3(1.0-xL);
    const double k = sqrt((k0-1.0)*k0); // eq 18

    Q = 2.0*PI * Rg * Teff_from_star_struct(donor) * Pow3(a) /
        (GRAVITATIONAL_CONSTANT * (m1+m2) * k); // eq 17

    return Q;
}
