#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "RLOF.h"

void RLOF_black_hole_merger(struct stardata_t * Restrict const stardata,
                            struct RLOF_orbit_t * const RLOF_orbit)
{
    RLOF_stars;
    Dprint("RLOF black hole merger stellar types %d %d\n",
           stardata->star[0].stellar_type,
           stardata->star[1].stellar_type
        );

    /*
     * Primary is a black hole, secondary can be anything.
     *
     * In normal mass transfer, one would expect the secondary
     * to also be a black hole. However, it's possible that the
     * primary is kicked into an orbit causing sudden mass transfer.
     */
    RLOF_orbit->dM_RLOF_lose = -donor->mass;
    RLOF_orbit->dM_RLOF_transfer = -RLOF_orbit->dM_RLOF_lose;

    stellar_structure_make_massless_remnant(stardata,
                                            donor);

    /* No nucsyn yields : impossible to lose mass */
#ifdef NUCSYN
    // again, no accretion layers for black holes
    donor->dmacc=0.0;
    accretor->dmacc=0.0;
#endif
}
