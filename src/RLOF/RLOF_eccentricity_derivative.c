#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "RLOF.h"

void RLOF_eccentricity_derivative(struct stardata_t * Restrict const stardata)
{
    RLOF_stars;

    /*
     * Calculate stellar angular momentum derivatives because of
     * RLOF mass transfer.
     *
     * we assume:
     *
     * de/dt = de/dm * dm/dt(RLOF loss)
     *
     * where de/dm is set in preferences
     */
    stardata->model.derivative[DERIVATIVE_ORBIT_ECCENTRICITY_RLOF] =
        stardata->preferences->dedmRLOF * donor->derivative[DERIVATIVE_STELLAR_MASS_RLOF_LOSS];

}
