#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "RLOF.h"

void RLOF_dM_orbit_to_time_derivatives(struct stardata_t * Restrict const stardata,
                                       struct RLOF_orbit_t * Restrict const RLOF_orbit)
{ 
    RLOF_stars;
    /*
     * Convert per-orbit mass changes into true time derivatives.
     *
     * NB if the period is zero, set the orbits_per_timestep
     * also to zero to prevent NaNs
     */  
    const double orbits_per_timestep = 
        unlikely(Is_zero(stardata->common.orbit.period)) ? 0.0 : 
        stardata->model.dt/stardata->common.orbit.period;

    Dprint("Modulate (per orbit) dM_RLOF_lose(=%g),dM_RLOF_transfer(=%g),dM_RLOF_accrete(=%g) by orbits_per_timestep=%g=%g/%g\n",
           RLOF_orbit->dM_RLOF_lose,
           RLOF_orbit->dM_RLOF_transfer,
           RLOF_orbit->dM_RLOF_accrete,
           orbits_per_timestep,
           stardata->model.dt,
           stardata->common.orbit.period);
        
    RLOF_orbit->dM_RLOF_lose *= orbits_per_timestep;
    RLOF_orbit->dM_RLOF_transfer *= orbits_per_timestep;
    RLOF_orbit->dM_RLOF_accrete *= orbits_per_timestep;
    
    Dprint("post speedup (dM_RLOF_lose=) %12.12e dms1,2=%g,%g\n",
           RLOF_orbit->dM_RLOF_lose,
           RLOF_orbit->dM_other[0],
           RLOF_orbit->dM_other[1]);

    /*
     * Set RLOF mass derivatives
     */
    donor->derivative[DERIVATIVE_STELLAR_MASS_RLOF_GAIN] = 0.0;
    donor->derivative[DERIVATIVE_STELLAR_MASS_RLOF_TRANSFER] = RLOF_orbit->dM_RLOF_transfer/stardata->model.dt;
    donor->derivative[DERIVATIVE_STELLAR_MASS_RLOF_LOSS] = RLOF_orbit->dM_RLOF_lose/stardata->model.dt;
        
    accretor->derivative[DERIVATIVE_STELLAR_MASS_RLOF_GAIN] = RLOF_orbit->dM_RLOF_accrete/stardata->model.dt;
    accretor->derivative[DERIVATIVE_STELLAR_MASS_RLOF_TRANSFER] = 0.0;
    accretor->derivative[DERIVATIVE_STELLAR_MASS_RLOF_LOSS] = 0.0; 
}
