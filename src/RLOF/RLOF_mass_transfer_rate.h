/*
 * header file for macros used in RLOF_mass_transfer_rate
 * and associated functions.
 *
 * These are *only* to make the code clearer, not provide options/parameters.
 * Also included are function prototypes, enums, etc..
 */
enum { CAP_NONE, CAP_THERMAL, CAP_ENVELOPE, CAP_DYNAMICAL };
enum { STATUS_NONE,
    STATUS_CONVERGED,
    STATUS_DMDR_NEGATIVE,
    STATUS_DR_ZERO,
    STATUS_WD_ENVELOPE,
    STATUS_MT_LT_ZERO
};

double Pure_function hurley_or_claeys_rate(const double r,
                                           const double roche_radius,
                                           const double mass,
                                           const struct star_t * const star,
                                           const struct stardata_t * const stardata);

double Pure_function ritter_rate(const double r,
                                 const double roche_radius,
                                 struct star_t * const star,
                                 struct stardata_t * const stardata);

static double Pure_function Claeys_factor(const struct star_t * const loser);

double radius_mass_derivative(struct stardata_t * const stardata,
                              struct star_t * const star,
                              const double mass);

double Pure_function roche_stream_area(const struct star_t * const star,
                                       const struct stardata_t * const stardata);
double Constant_function_if_no_debug sound_speed(struct star_t * const star,
                                                 struct stardata_t * const stardata);
double Constant_function_if_no_debug surface_pressure_scale_height(struct star_t * const star,
                                                                   struct stardata_t * const stardata);
