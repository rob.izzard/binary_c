
#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "RLOF.h"

double RLOF_speed_up_factor(struct stardata_t * Restrict const stardata,
                            const struct RLOF_orbit_t * Restrict const RLOF_orbit)
{
    RLOF_stars;
    /*
     * calculate "speed up factor":
     *
     * This is a factor relating the desired timestep during RLOF to
     * the orbital period.
     */
    double f1 =
        Is_not_zero(donor->mass) ? 
        (fabs(RLOF_orbit->dM_RLOF_lose)+
               fabs(RLOF_orbit->dM_other[naccretor]))/
        donor->mass : 0.0;

    double f2 = Is_not_zero(accretor->mass) ? fabs(RLOF_orbit->dM_other[naccretor])/accretor->mass : 0.0;
    
    /*
     * If RLOF_speed_up_factor is zero (e.g. because the previous timestep
     * was zero, such as after common envelope evolution) then
     * we are in trouble, so reset the timescale in this case.
     */
    if(Is_zero(stardata->common.RLOF_speed_up_factor))
    {
        set_nuclear_timescale_and_slowdown_factor(stardata);
        stardata->common.RLOF_speed_up_factor = Max(1e-2, stardata->common.RLOF_speed_up_factor);
    }

    double speed_up_factor =
        Min(2.0*stardata->common.RLOF_speed_up_factor,5.0E-03/Max3(f1,f2,1e-14));

    Dprint("calc speed_up_factor=%g (RLOF_speed_up_factor=%g f1(lose=%g other=%g)=%g f2=%g)\n",
           speed_up_factor,
           stardata->common.RLOF_speed_up_factor,
           RLOF_orbit->dM_RLOF_lose,
           RLOF_orbit->dM_other[naccretor],
           f1,
           f2);
  
#ifdef RLOF_MINIMUM_SPEEDUP_FACTOR
    speed_up_factor = Max(RLOF_MINIMUM_SPEEDUP_FACTOR,speed_up_factor);
#endif

    /* set for next time */
    stardata->common.RLOF_speed_up_factor = speed_up_factor;

    return speed_up_factor;
}
