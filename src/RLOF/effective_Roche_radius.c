#include "../binary_c.h"
No_empty_translation_unit_warning;


/*
 * Return the effective Roche radius
 *
 * BSE uses RL(q,a) (star->roche_radius) while you might prefer to use
 * RL(q,a(1-e)) (star->roche_radius_at_periastron) by setting the option use_periastron_Roche_radius to TRUE
 */

double Pure_function effective_Roche_radius(const struct stardata_t * const stardata,
                                            const struct star_t * const star)
{
    return
        stardata->preferences->use_periastron_Roche_radius==TRUE
        ? star->roche_radius_at_periastron
        : star->roche_radius;
}
