#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "RLOF.h"

/*
 * RLOF is unstable: choose which algorithm to apply.
 * Use the events system if we can.
 */

int RLOF_unstable_mass_transfer(struct stardata_t * Restrict const stardata,
                                const int instability,
                                const Boolean do_event)
{
    /*
     * Check that we're still overflowing: we're probably called
     * from an event and previous events may have changed the
     * system. Also, we do not want to check for overflow
     * on the previous timestep, only this one.
     */
    if(test_for_roche_lobe_overflow(stardata,
                                    TEST_RLOF_ONLY_THIS_TIMESTEP)
       != EVOLUTION_ROCHE_OVERFLOW_NONE)
    {
        if(stardata->preferences->disable_events == FALSE &&
           do_event == FALSE)
        {
            /*
             * Set up unstable RLOF event
             */
            struct binary_c_unstable_RLOF_event_t * event_data =
                Malloc(sizeof(struct binary_c_unstable_RLOF_event_t));

            if(Add_new_event(stardata,
                             BINARY_C_EVENT_UNSTABLE_RLOF,
                             &RLOF_unstable_mass_transfer_event_handler,
                             NULL,
                             event_data,
                             UNIQUE_EVENT) == BINARY_C_EVENT_DENIED)
            {
                Dprint("not allowed a dynamical low-mass MS RLOF event : disable? %d deny? %d do_event? %d\n",
                       stardata->preferences->disable_events,
                       stardata->model.deny_new_events,
                       do_event
                    );
                Safe_free(event_data);
            }
            else
            {
                /*
                 * Save the state of the system for later event handling
                 */
                RLOF_stars;
                event_data->donor = donor;
                event_data->accretor = accretor;
                event_data->instability = instability;
            }
        }
        else
        {
            /*
             * Perform unstable RLOF
             */
            {
                RLOF_stars;
                Dprint("RLOF is unstable (0: m=%g m0=%g st=%d R/RL=%g, 1: m=%g m0=%g st=%d R/RL=%g) donor=%d\n",
                       stardata->star[0].mass,
                       stardata->star[0].phase_start_mass,
                       stardata->star[0].stellar_type,
                       stardata->star[0].radius / stardata->star[0].roche_radius,
                       stardata->star[1].mass,
                       stardata->star[1].phase_start_mass,
                       stardata->star[1].stellar_type,
                       stardata->star[1].radius / stardata->star[1].roche_radius,
                       donor->starnum
                    );
            }
            /* action : returned */
            struct RLOF_orbit_t RLOF_orbit;
            RLOF_init_dM_orbit(stardata,&RLOF_orbit);
            switch(instability)
            {
            case RLOF_UNSTABLE_MS_MERGER:
                Dprint("RLOF -> main sequence merger\n");
                RLOF_merger(stardata,&RLOF_orbit);
                break;

            case RLOF_UNSTABLE_VERY_LARGE_DONOR:
                Dprint("R>10RL i.e. very large donor star\n");
                RLOF_merger(stardata,&RLOF_orbit);
                break;

            case RLOF_UNSTABLE_LOW_MASS_MS_COMENV:
                Dprint("Dynamical mass transfer from low mass MS star\n");
                RLOF_dynamical_transfer_from_low_mass_MS_star(stardata,
                                                              &RLOF_orbit);
                break;

            case RLOF_UNSTABLE_GIANT_COMENV:
                Dprint("Common envelope evolution (q>qc)\n");
                const int action = RLOF_dynamical_comenv(stardata,&RLOF_orbit);
                Dprint("dynamical comenv ret %d (SYSTEM_ACTION_CONTINUE_RLOF=%d SYSTEM_ACTION_END_RLOF=%d)\n",
                       action,SYSTEM_ACTION_CONTINUE_RLOF,SYSTEM_ACTION_END_RLOF);
                break;

            case RLOF_UNSTABLE_WD_COMENV:
                Dprint("Dynamical transfer from a white dwarf\n");

                RLOF_dynamical_transfer_from_WD_to_remnant(stardata,&RLOF_orbit);
                break;

            case RLOF_UNSTABLE_NS:
                Dprint("NS-NS/BH merger -> GRB?\n");
                RLOF_NSNS_NSBH_merger(stardata,&RLOF_orbit);
                break;

            case RLOF_UNSTABLE_BH:
                Dprint("BH BH merger\n");
                RLOF_black_hole_merger(stardata,&RLOF_orbit);
                break;

            default:
                Exit_binary_c(BINARY_C_WRONG_ARGUMENT,"Unknown unstable RLOF case %d",instability);
            }


            if(EITHER_STAR_MASSLESS)
            {
                stardata->common.orbit.separation = 0.0;
                stardata->common.orbit.period = 0.0;
                stardata->common.orbit.angular_momentum = 0.0;
                stardata->common.orbit.eccentricity = 0.0;
                stardata->star[0].derivative[DERIVATIVE_STELLAR_MASS_RLOF_GAIN] = 0.0;
                stardata->star[0].derivative[DERIVATIVE_STELLAR_ANGMOM_RLOF_GAIN] = 0.0;
                stardata->star[0].derivative[DERIVATIVE_STELLAR_MASS_RLOF_LOSS] = 0.0;
                stardata->star[0].derivative[DERIVATIVE_STELLAR_ANGMOM_RLOF_LOSS] = 0.0;

                update_orbital_variables(stardata,
                                         &stardata->common.orbit,
                                         &stardata->star[0],
                                         &stardata->star[1]);

                Dprint("merger %g %g\n",
                       stardata->star[0].mass,
                       stardata->star[1].mass);
                stardata->model.sgl = TRUE;
            }
        }

    }
    return EVOLUTION_LOOP;
}
