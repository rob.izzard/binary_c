#pragma once
#ifndef RLOF_CRITICAL_Q_H
#define RLOF_CRITICAL_Q_H

static Pure_function double qc_HG_BSE(const struct stardata_t * const stardata,
                                      const struct star_t * const donor);
static Pure_function double chen_han_qc(struct stardata_t * const stardata);
static double Ge_et_al_2015_qc(struct stardata_t * stardata,
                               double m,
                               double r);
static Pure_function double Temmink_2022_qc(struct stardata_t * const stardata Maybe_unused,
                                            struct star_t * const donor Maybe_unused,
                                            struct star_t * const accretor Maybe_unused);

#include "RLOF_Ge2015_qcrit.h"
#include "RLOF_stability_macros.h"

#endif // RLOF_CRITICAL_Q_H
