#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "RLOF.h"

void RLOF_NSNS_NSBH_merger(struct stardata_t * const stardata,
                           struct RLOF_orbit_t * const RLOF_orbit)
{
    RLOF_stars;

    /*
     * NS donor: NS-NS/BH merger: Gamma ray burst ... ?
     */

#ifdef NS_BH_AIC_LOG
    if((donor->stellar_type==NEUTRON_STAR)&&
       (accretor->stellar_type==NEUTRON_STAR))
    {
        printf("NSNS p=%12.12e t=%12.12e in special cases\n",stardata->model.probability,
               stardata->model.time);
        //printf("sgl = %d\n",stardata->model.sgl);
#ifdef FILE_LOG
        char c[STRING_LENGTH];
        sprintf(c,"NSNS at time t=%12.12e\n",stardata->model.time);
        output_string_to_log(stardata,c);
#endif
    }
#endif

    /*
     * The neutron star must be the donor
     */
    if(accretor->stellar_type == NEUTRON_STAR &&
       donor->stellar_type == BLACK_HOLE)
    {
        Swap_star_pointers(donor,accretor);
    }

    /*
     * Set mass
     */
    RLOF_orbit->dM_RLOF_lose = -donor->mass;
    RLOF_orbit->dM_RLOF_transfer = -RLOF_orbit->dM_RLOF_lose;

    /*
     * donor is evaporated
     */
    stellar_structure_make_massless_remnant(stardata,
                                            donor);

    /*
     * Set merge flag and update the orbit
     */
    stardata->model.coalesce = TRUE;
    stardata->common.orbit.separation = 0.0;
    stardata->common.orbit.period = 0.0;
    stardata->common.orbit.angular_momentum = 0.0;
    stardata->common.orbit.eccentricity = 0.0;

    update_orbital_variables(stardata,
                             &stardata->common.orbit,
                             donor,
                             accretor);
#ifdef NUCSYN
    // remove accretion layers for BH and MASSLESS_REMNANT
    donor->dmacc=0.0;
    accretor->dmacc=0.0;
    nucsyn_merge_NSs(stardata);
    donor->sn_last_time = accretor->SN_type;
#endif

}
