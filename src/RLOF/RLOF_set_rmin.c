#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "RLOF.h"

/*
 * Determine whether the transferred material forms an accretion 
 * disc around the secondary or hits the secondary in a direct 
 * stream, by using eq.(1) of Ulrich & Burger (1976, ApJ, 206, 509) 
 * fitted to the calculations of Lubow & Shu (1975, ApJ, 198, 383).
 *
 * In this function, we set rmin, the radius of nearest approach.
 */
void RLOF_set_rmin(struct stardata_t * const stardata)
{
    struct star_t * const accretor = &(stardata->star[stardata->model.naccretor]);
    accretor->rmin = radius_closest_approach(stardata->common.orbit.separation,
                                             accretor->q);
}
