#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "RLOF.h"
#include "RLOF_stability_macros.h"

/*
 * This function checks if RLOF is stable, and acts
 * to call the appropriate algorithm. In the case
 * of instability, RLOF is usually via common envelope
 * evolution, but may be a merger or (say) Ia supernova.
 *
 * Returns RLOF_IMPOSSIBLE (e.g. if the system is single),
 *         RLOF_STABLE if RLOF is possible and stable,
 *         RLOF_UNSTABLE if RLOF is possible and unstable or
 *         RLOF_BLOCKED if RLOF is possible but is not allowed to happen.
 */

int RLOF_stability_tests(struct stardata_t * Restrict const stardata)
{
    RLOF_stars;

    /******************************************************************/
    Dprint("t=%g intermediate step? %s kstar(ndonor=%d)=%d, kstar(naccretor=%d)=%d sep=%g t=%g dtm=%g dt=%g (0: R=%g RL=%g SN=%d, 1: R=%g RL=%g SN=%d)\n",
           stardata->model.time,
           Yesno(stardata->model.intermediate_step),
           ndonor,
           donor->stellar_type,
           naccretor,
           accretor->stellar_type,
           stardata->common.orbit.separation,
           stardata->model.time,
           stardata->model.dtm,
           stardata->model.dt,
           stardata->star[0].radius,
           stardata->star[0].roche_radius,
           stardata->star[0].SN_type,
           stardata->star[1].radius,
           stardata->star[1].roche_radius,
           stardata->star[1].SN_type
        );

    /*
     * No RLOF is possible if the system is single or
     * we are on an intermediate solution step
     */
    RLOF_stability stability;

    if(stardata->model.intermediate_step == TRUE)
    {
        stability = RLOF_INSTABILITY_BLOCKED;
    }
    else if(System_is_single)
    {
        stability = RLOF_IMPOSSIBLE;
    }
    else
    {
        /*
         * To start with, assume no novae, accretion disk, super-Eddington
         * or Hachisu-style disk-wind accretion
         */
        stardata->model.supedd  = FALSE;
        stardata->model.hachisu = FALSE;

        /*
         * determine critical q for dynamical accretion
         * (unstable if q > qc)
         */
        const double qc = RLOF_critical_q(stardata);

        Dprint("comenv tests COMENV_GIANT_STAR=%d DYNAMICAL_TESTQ=%d COMENV_TESTR=%d COMENV_SUBGIANT_STAR=%d COMENV_TEST_MENV=%d donor->q=%g qcrit=%g (->%sstable)\n",
               COMENV_GIANT_STAR,
               DYNAMICAL_TESTQ,
               COMENV_TESTR,
               COMENV_SUBGIANT_STAR,
               COMENV_TEST_MENV,
               donor->q,
               qc,
               (donor->q>qc?"un":""));

#ifdef RLOF_ABC
        const int rlof_type = RLOF_type(donor);
        if(rlof_type != stardata->model.rlof_type)
        {
            stardata->model.rlof_type   = rlof_type;
            stardata->model.do_rlof_log = TRUE;
        }
        else
        {
            stardata->model.do_rlof_log = FALSE;
        }
#endif

        /*
         * Test for unstable RLOF : e.g. common envelope evolution/merger
         *
         * First, assume stable RLOF
         */
         stability = RLOF_STABLE;

         if(!GIANT_LIKE_STAR(donor->stellar_type) &&
            donor->radius>10.0*donor->roche_radius)
        {
            stability = RLOF_UNSTABLE_VERY_LARGE_DONOR;
            Append_logstring(LOG_ROCHE,
                             "0Unstable RLOF: R > 10RL");

        }
        else if(donor->stellar_type<=MAIN_SEQUENCE &&
                accretor->stellar_type<=MAIN_SEQUENCE &&
                donor->q > qc)
        {
            /*
             * Allow the stars to merge with the product in *1.
             */
            stability = RLOF_UNSTABLE_MS_MERGER;
            Append_logstring(LOG_ROCHE,
                             "0Unstable RLOF: MS+MS with qdonor=Mdonor/Maccretor=%g>qcrit=%g",donor->q,qc);

        }
        else if(LOW_MASS_MS_COMENV_TEST)
        {
            /*
             * Dynamical mass transfer from convective,
             * low mass MS star
             */
            stability = RLOF_UNSTABLE_LOW_MASS_MS_COMENV;
            Append_logstring(LOG_ROCHE,
                             "0Unstable RLOF : low mass MS star");
        }
        else if(GIANT_BRANCH_COMENV_TEST)
        {
            /*
             * Common-envelope evolution
             * with q>qc instability
             */
            stability = RLOF_UNSTABLE_GIANT_COMENV;

#undef X
#define X(CODE,NUM,STRING) STRING,

            static const char* qcrit_strings[] = { "None", RLOF_QCRIT_LIST };
#undef X
            const double qc_prescription = Qcrit_prescription;
            Append_logstring(LOG_ROCHE,
                             "0Unstable RLOF : q=Mdonor/Maccretor=%g > qc=%g (prescription=%s, donor [st=%d M=%g Mc=%g R=%g Rc=%g], accretor [st=%d M=%g Mc=%g R=%g Rc=%g] sep=%g e=%g )",
                             donor->q,
                             qc,
                             qc_prescription<-TINY ?
                             qcrit_strings[-Map_float_algorithm(Qcrit)] : "float",
                             donor->stellar_type,
                             donor->mass,
                             Outermost_core_mass(donor),
                             donor->radius,
                             donor->core_radius,
                             accretor->stellar_type,
                             accretor->mass,
                             Outermost_core_mass(accretor),
                             accretor->radius,
                             accretor->core_radius,
                             stardata->common.orbit.separation,
                             stardata->common.orbit.eccentricity
                );
        }
        else if(WHITE_DWARF_DYNAMICAL_TEST)
        {
            /*
             * Dynamical transfer from a white dwarf
             * onto a compact remnant.
             */
            stability = RLOF_UNSTABLE_WD_COMENV;
            Append_logstring(LOG_ROCHE,
                             "0Unstable RLOF : WD onto compact remNant");
        }
        else if(donor->stellar_type == NEUTRON_STAR)
        {
            /*
             * NS mass transfer (must be onto another
             * compact object) : gamma ray burst?
             */
            stability = RLOF_UNSTABLE_NS;
            Dprint("Unstable NS %d -> %d \n",
                   donor->stellar_type,
                   accretor->stellar_type
                );
            Append_logstring(LOG_ROCHE,
                             "0Unstable RLOF : NS-NS/BH");
        }
        else if(donor->stellar_type == BLACK_HOLE)
        {
            /* BH-BH merger */
            Dprint("DONOR %d R=%g RL=%g ACCRETOR %d R=%g RL=%g\n",
                   donor->stellar_type,
                   donor->radius,
                   donor->roche_radius,
                   accretor->stellar_type,
                   accretor->radius,
                   accretor->roche_radius
                );
            stability = RLOF_UNSTABLE_BH;
            Append_logstring(LOG_ROCHE,
                             "0Unstable RLOF : BH-BH");
        }


#ifdef RLOF_ABC
        stardata->model.rlof_stability =
            stability==RLOF_STABLE ? RLOF_STABLE : RLOF_UNSTABLE;
#endif

        if(DEBUG)
        {
            Dprint("Stability : %d : %s (intermediate? %s)\n",
                   stability,
                   RLOF_stability_string(stability),
                   Yesno(stardata->model.intermediate_step)
                );
        }
    }
    return stability;
}
