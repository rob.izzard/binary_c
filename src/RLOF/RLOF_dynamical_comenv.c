#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "RLOF.h"

Boolean RLOF_dynamical_comenv(struct stardata_t * const Restrict stardata,
                              struct RLOF_orbit_t * const RLOF_orbit)
{
    /*
     * Common-envelope evolution (q>qc) during RLOF.
     *
     * Returns SYSTEM_ACTION_CONTINUE_RLOF or SYSTEM_ACTION_END_RLOF
     */
    RLOF_stars;
    int action;
    const double m1ce = donor->mass;
    const double m2ce = accretor->mass;
    Dprint("call comenv sep=%12.12e\n",stardata->common.orbit.separation);

    common_envelope_evolution(
        donor,
        accretor,
        stardata
        );

    donor->epoch = stardata->model.time - donor->age;

    /* check for merger */
    if(stardata->model.coalesce==TRUE)
    {
        stardata->model.com = TRUE;
        action = SYSTEM_ACTION_END_RLOF;
    }
    else
    {
        /* reset epoch */
        accretor->epoch = stardata->model.time - accretor->age;
        Dprint("Set epoch %g\n",accretor->epoch);

        /* check for break up */
        if(stardata->common.orbit.eccentricity>1.0)
        {
#ifdef BSE
            Foreach_star(star)
            {
                if(star->stellar_type>=NEUTRON_STAR)
                {
                    double rc = core_radius(stardata,
                                            star,
                                            star->stellar_type,
                                            star->core_stellar_type,
                                            star->mass,
                                            star->mass,
                                            star->phase_start_core_mass,
                                            stardata->common.metallicity_parameters[ZPAR_MASS_HE_FLASH]);

                    if(Is_zero(rc)) rc = star->radius;

                    star->omega = star->angular_momentum/(CORE_MOMENT_OF_INERTIA_FACTOR*Pow2(rc)*star->mass);
                }
            }
#endif//BSE
            action = SYSTEM_ACTION_END_RLOF;
        }
        else
        {
            /*
             * Next step should be made without changing the time.
             *
             * Note that these dM_* are not used for anything except
             * logging: they're not realisitic given that we know little
             * about the timescale of common-envelope evolution.
             */
            RLOF_orbit->dM_RLOF_lose = -(m1ce - donor->mass);
            RLOF_orbit->dM_RLOF_transfer = accretor->mass - m2ce;
            RLOF_orbit->dM_RLOF_accrete = RLOF_orbit->dM_RLOF_transfer;
            stardata->model.dtm = 0.0;

            /*
             * Reset orbital parameters as separation may have changed.
             */

            stardata->common.orbit.period = (stardata->common.orbit.separation/AU_IN_SOLAR_RADII)*sqrt(stardata->common.orbit.separation/(AU_IN_SOLAR_RADII*Total_mass));
            stardata->common.orbit.angular_frequency = TWOPI/stardata->common.orbit.period;

            action = SYSTEM_ACTION_CONTINUE_RLOF;
        }
    }

    return action;
}
