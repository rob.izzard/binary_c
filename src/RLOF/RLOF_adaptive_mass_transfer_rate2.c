#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef ADAPTIVE_RLOF2


/*
 * TODO:
 *
 * We should return the previously calculated mdot, then calculate
 * a more accurate one for the next step (if required) thus taking
 * into account changes in the Roche lobe size.
 *
 * At the moment, we calculate this and then just reject, which is
 * no good.
 *
 */

/*
 * threshold (epsilon): we force R=RL to this accuracy.
 * Normally 1e-6 is ok.
 */
#define THRESHOLD 0

/*
 * Max number of iterations
 */
#define MAX_ITERATIONS 2

/*
 * over-relax if we can : but this is very slow
 */
//#define OVERRELAX
#define OVERRELAX_THRESHOLD 1e-11

/*
 * Initial guess
 */
#define MDOT_INITIAL_GUESS 1e-14

/*
 * factors to modulate in second step (count==1)
 */
#define INCREASE_FACTOR 1.02
#define DECREASE_FACTOR 0.98

//#undef rprintf
#define rprintf(...) if(vb==TRUE){printf("ADAP2:");printf(__VA_ARGS__);}

/*
 * Adapative mass transfer rate (algorithm version 2)
 *
 * The idea is the adapt the mass loss rate so that
 * the stellar radius == target_radius (which is
 * normally the Roche-lobe radius).
 *
 * We do this by rejecting timesteps, without changing
 * the timestep, if the above condition is not satisfied
 * within THRESHOLD.
 */
#define VB_MACRO stardata->model.model_number == 3

double RLOF_adaptive_mass_transfer_rate2(const double r Maybe_unused,
                                         const double roche_radius Maybe_unused,
                                         const double mass Maybe_unused,
                                         struct stardata_t * const stardata Maybe_unused,
                                         struct star_t * const star  Maybe_unused/* primary (donor) */,
                                         const double dt Maybe_unused,
                                         Boolean * const can_apply_limits Maybe_unused
    )
{

#ifdef NEW_ADAPTIVE_RLOF_CODE
    const double target_radius = effective_Roche_radius(stardata,star);
    const double Rexcess = r - target_radius;
    const Boolean vb Maybe_unused = VB_MACRO;
    double mdot = star->adaptive2_mdot;

    stardata->model.adapting_RLOF2 = TRUE;

    if(vb)printf("\n\n%s** %u  **********************************************************%s\n\nfirst a = %30.20e, r = %30.20e, star->radius = %30.20e, target_radius = %30.20e Rexcess %g\n",
           stardata->store->colours[RED],
           stardata->model.adaptive_RLOF2_adapt_count,
           stardata->store->colours[COLOUR_RESET],
           stardata->common.orbit.separation,
           r,
           star->radius,
           target_radius,
           Rexcess);

    if(stardata->model.adaptive_RLOF2_adapt_count == 0)
    {
        /*
         * First adaptative: base guess on Rexcess
         */
        mdot = 0.0;//Rexcess / star->drdm;

        if(vb)printf("\nRLOF rate update: model %d star %d iteration first mdot was %g with Rexcess %g, a = %30.20e : Mdot [ %g, %g ], Rexcess [ %g, %g ]  -> %sreject adapt2%s : -> converged? %s\n",
                 stardata->model.model_number,
                 star->starnum,
                 mdot,
                 Rexcess,
                 stardata->common.orbit.separation,
                 star->adaptive2_Mdot_low,
                 star->adaptive2_Mdot_high,
                 star->adaptive2_Rexcess_low,
                 star->adaptive2_Rexcess_high,
                 stardata->store->colours[GREEN],
                 stardata->store->colours[COLOUR_RESET],
                 Yesno(star->adaptive2_converged)
        );


        printf("RLOF rate 0 : Rexcess = %g with rate %g (r=%30.20e rl=%30.20e), drdm = %g -> %g\n",
               Rexcess,
               star->derivative[DERIVATIVE_STELLAR_MASS_RLOF_LOSS],
               r,
               target_radius,
               star->drdm,
               mdot);

        /*
         * Initial setup
         */
        stardata->model.adaptive_RLOF2_overflower = star->starnum;
        stardata->model.adaptive_RLOF2_drdm = star->drdm;
        star->prev_radius_excess = Rexcess;
        star->adaptive2_converged = FALSE;
        stardata->model.adaptive_RLOF2_reject_count = 0;
        star->adaptive2_mdot = mdot;
        star->adaptive2_Mdot_low = 0.0;
        star->adaptive2_Mdot_high = 0.0;
        star->adaptive2_Rexcess_low = 0.0;
        star->adaptive2_Rexcess_high = 0.0;
    }
    else
    {
        /*
         * All other times use guess calculated below
         */
        mdot = star->adaptive2_mdot;
        printf("RLOF rate from previous %u = %g\n",
               stardata->model.adaptive_RLOF2_adapt_count,
               star->adaptive2_mdot);

        if(stardata->model.model_number==663)
        {
            stardata->model.adaptive_RLOF2_overflower = star->starnum;
            stardata->model.adaptive_RLOF2_drdm = star->drdm;
            star->prev_radius_excess = Rexcess;
            star->adaptive2_converged = FALSE;
            stardata->model.adaptive_RLOF2_reject_count = 0;
            star->adaptive2_mdot = mdot;
            star->adaptive2_Mdot_low = 0.0;
            star->adaptive2_Mdot_high = 0.0;
            star->adaptive2_Rexcess_low = 0.0;
            star->adaptive2_Rexcess_high = 0.0;
        }
    }

    printf("RLOF STATE R = %g, RL = %g, R-RL = %g, M = %g, dMdt = %g\n",
           star->radius,
           target_radius,
           star->radius - target_radius,
           star->mass,
           mdot);
    return mdot;
#else // NEW_ADAPTIVE_RLOF_CODE
    return 0.0;
#endif // NEW_ADAPTIVE_RLOF_CODE
}

#ifdef NEW_ADAPTIVE_RLOF_CODE

double RLOF_adaptive_mass_transfer_rate2_update(struct stardata_t * const stardata)
{
    Dprint("PRE RLOF %p %p MDOT 0=%g 1=%g\n",
           (void*)stardata->model.restore_mask,
           (void*)stardata->model.restore_mask_contents,
           stardata->star[0].adaptive2_mdot,
           stardata->star[1].adaptive2_mdot);

    const Star_number k = stardata->model.adaptive_RLOF2_overflower;
    struct star_t * const star = &stardata->star[k]; /* donor */
    const double target_radius = effective_Roche_radius(stardata,
                                                        star);
    const double r = star->radius;
    double mdot = star->adaptive2_mdot; // returned
    const Boolean vb = VB_MACRO;

    star->reject_same_timestep = REJECT_ADAPTIVE_RLOF2;
    stardata->model.reject_same_timestep =  REJECT_ADAPTIVE_RLOF2;

    /*
     * Calculate next mdot
     */
#define THRESHOLD 0
    const double Rexcess = r - target_radius;
    const double Rexcess0 = star->prev_radius_excess;
    const double epsR = (r/target_radius - 1.0);
    const double epstarget_radius = (target_radius/star->prev_target_radius - 1.0);
    const double eps = Max(fabs(epsR),fabs(epstarget_radius));

    if(vb)printf("\nRLOF rate update: model %d star %d iteration %u mdot was %g with Rexcess %g, a = %30.20e : Mdot [ %g, %g ], Rexcess [ %g, %g ]  -> %sreject adapt2%s : eps = Max(%g, %g) = %g -> converged? %s\n",
                 stardata->model.model_number,
                 k,
                 stardata->model.adaptive_RLOF2_adapt_count,
                 mdot,
                 Rexcess,
                 stardata->common.orbit.separation,
                 star->adaptive2_Mdot_low,
                 star->adaptive2_Mdot_high,
                 star->adaptive2_Rexcess_low,
                 star->adaptive2_Rexcess_high,
                 stardata->store->colours[GREEN],
                 stardata->store->colours[COLOUR_RESET],
                 epsR,
                 epstarget_radius,
                 eps,
                 Yesno(star->adaptive2_converged)
        );

    /*
     * save the previous dM/dt
     */
    star->prevMdot1 = star->prevMdot0;
    star->prevMdot0 = star->adaptive2_mdot;

    double e1=0.0, e2=0.0;


    if(1)
    {
        mdot = 1e-5 * stardata->model.adaptive_RLOF2_adapt_count;
    }
    else if(stardata->model.adaptive_RLOF2_adapt_count == 1)
    {
        mdot = Rexcess / star->drdm;
    }
    else if(stardata->model.adaptive_RLOF2_adapt_count == 2)
    {
        e1 = e2 = 0.0;

        /*
         * Second adaptation: we now have two points
         * and can start to build the bisection span
         */
        if(Rexcess > 0.0)
        {
            /*
             * mdot was not high enough
             */
            star->adaptive2_Mdot_low = mdot;
            star->adaptive2_Rexcess_low = Rexcess;
            mdot *= 2.0;
            if(vb)printf("Step 0->1 : mdot was too low -> new %g\n",mdot);
        }
        else
        {
            /*
             * mdot was too high
             */
            star->adaptive2_Mdot_high = mdot;
            star->adaptive2_Rexcess_high = Rexcess;
            mdot *= 0.99;
            if(vb)printf("Step 0->1 : mdot was too high -> new %g\n",mdot);
        }
    }
    else if(stardata->model.adaptive_RLOF2_adapt_count == 30)
    {
        /*
         * SPECIAL CASE : Exit after n steps
         */
        if(vb)printf("exit at %u\n",stardata->model.adaptive_RLOF2_adapt_count);
        Flexit;
    }
    else if(vb && stardata->model.adaptive_RLOF2_adapt_count == 3)
    {
        mdot = 0;
    }
    else if(vb && stardata->model.adaptive_RLOF2_adapt_count == 4)
    {
        Flexit;
    }
    else if(stardata->model.adaptive_RLOF2_adapt_count == 40 &&
            star->adaptive2_mdot > 0.0)
    {
        /*
         * SPECIAL CASE : Assume converged after 40 steps
         */
        mdot = star->adaptive2_mdot;
        star->adaptive2_converged = TRUE;
    }

    /*
     * star->adaptive2_mdot gave the current Rexcess
     */
    else if(Is_not_zero(star->adaptive2_Mdot_high) &&
            Is_not_zero(star->adaptive2_Mdot_low))
    {
        /*
         * We have previous guesses: where should this one go?
         */
        if(Rexcess < 0.0)
        {
            /*
             * overshot mass loss : this should be the new Mdot_high
             */
            star->adaptive2_Mdot_high = star->adaptive2_mdot;
            star->adaptive2_Rexcess_high = Rexcess;
        }
        else
        {
            /*
             * undershot mass loss : this should be the new Mdot_low
             */
            star->adaptive2_Mdot_low = star->adaptive2_mdot;
            star->adaptive2_Rexcess_low = Rexcess;
        }
        const double dR = star->adaptive2_Rexcess_high - star->adaptive2_Rexcess_low;
        if(1)
        {
            /* pure bisection */
            mdot = 0.5 * (star->adaptive2_Mdot_low + star->adaptive2_Mdot_high);
        }
        else
        {
            /* better guess */
            const double slope = (star->adaptive2_Mdot_high - star->adaptive2_Mdot_low) / dR;
            const double intercept = star->adaptive2_Mdot_low;
            mdot = -intercept/slope;
        }
    }
    /*
     * If we're searching for an Mdot_low that gives us a positive Rexcess,
     * or we're searching for an Mdot_high that gives us a negative Rexcess,
     * we've found our span.
     */
    else if((Is_not_zero(star->adaptive2_Mdot_high) &&
             Rexcess > 0.0)
            ||
            (Is_not_zero(star->adaptive2_Mdot_low) &&
             Rexcess < 0.0))
    {
        /*
         * Our two mdots bisect what we want: update the excess radii
         */
        if(Is_zero(star->adaptive2_Mdot_low))
        {
            star->adaptive2_Mdot_low = star->prevMdot0;
            star->adaptive2_Rexcess_low = Rexcess;
        }
        else
        {
            star->adaptive2_Mdot_high = star->prevMdot0;
            star->adaptive2_Rexcess_high = Rexcess;
        }

        mdot = 0.5 * (star->adaptive2_Mdot_low + star->adaptive2_Mdot_high);
    }

    /*
     * Not spanning : keep adapting whichever
     *                bound is incorrect
     */
    else if(Is_zero(star->adaptive2_Mdot_high))
    {
        /*
         * No upper limit : keep increasing mdot
         */
        mdot *= 2.0;
    }
    else if(Is_zero(star->adaptive2_Mdot_low))
    {
        /*
         * No lower limit : keep decreasing mdot
         */
        mdot *= 0.5;
    }
    else
    {
        if(vb)printf("both\n");
        Flexit;
    }

    /*
     * Test for convergence
     */
    if(eps < 1e-6 &&
       mdot > 0.0)
    {
        star->adaptive2_converged = TRUE;
        if(vb)printf("CONVERGED\n");
    }

    if(0)
    {
        if(vb)printf("ADAPDATA%d %u %30.20e %30.20e %30.20e %30.20e %30.20e %30.20e %30.20e %30.20e %30.20e %30.20e %30.20e %30.20e %30.20e %30.20e %30.20e %30.20e %30.20e %30.20e %30.20e %30.20e\n",
                     stardata->model.model_number,
                     stardata->model.adaptive_RLOF2_adapt_count, // 1
                     stardata->common.orbit.separation,
                     stardata->common.orbit.angular_momentum,
                     star->prevMdot0,
                     r,//5
                     target_radius,
                     Rexcess,
                     epsR,
                     epstarget_radius,
                     eps,//10
                     stardata->star[0].mass, //11
                     stardata->star[1].mass,
                     stardata->star[0].drdm, //13
                     stardata->star[1].drdm,
                     e1,//15
                     e2,
                     stardata->model.derivative[DERIVATIVE_ORBIT_ANGMOM_GRAVITATIONAL_RADIATION], //17
                     stardata->model.derivative[DERIVATIVE_ORBIT_ANGMOM_RLOF_LOSS], //18
                     stardata->model.derivative[DERIVATIVE_ORBIT_ANGMOM_RLOF_GAIN], //19
                     stardata->model.derivative[DERIVATIVE_ORBIT_SEMI_MAJOR_AXIS], //20
                     stardata->model.derivative[DERIVATIVE_ORBIT_ANGMOM_NONCONSERVATIVE_LOSS] //21
            );
    }

    if(stardata->model.adaptive_RLOF2_adapt_count > 10000)
    {
        if(vb)printf("too many RLOF2 iterations\n");
        Flexit;
    }

    star->prevR0 = r;
    star->prevRL0 = target_radius;
    star->prev_radius_excess = Rexcess;
    star->prev_target_radius = target_radius;
    Dprint("ADAPTIVE RLOF REJECT %u (set star %d reject_same_timestep %u) adapt_count %u reject_count %u mdot_mask %g\n",
           stardata->model.adaptive_RLOF2_reject_count,
           k,
           star->reject_same_timestep,
           stardata->model.restore_mask_contents->model.adaptive_RLOF2_adapt_count,
           stardata->model.restore_mask_contents->model.adaptive_RLOF2_reject_count,
           stardata->model.restore_mask_contents->star[k].adaptive2_mdot
        );

    stardata->model.adaptive_RLOF2_reject_count++;
    stardata->model.adaptive_RLOF2_adapt_count++;

    if(vb)printf("RETURN MDOT %g\n",mdot);
    return mdot;
}
#endif


#ifdef __DEPRECATED
double _RLOF_adaptive_mass_transfer_rate2(const double r,
                                          const double roche_radius,
                                          const double mass,
                                          struct stardata_t * const stardata,
                                          struct star_t * const star /* primary (donor) */,
                                          const double dt,
                                          Boolean * const can_apply_limits
    )
{
    const Boolean vb = TRUE;

    /*
     * Target radius is actually 1+THRESHOLD * the roche_radius
     * so we stay "in RLOF" for logging purposes.
     */
    const double target_radius = roche_radius * (1.0 + 1.0 * THRESHOLD);
    if(vb)printf("ADAPTIVE%d sep=%20.10e J=%20.12e\n",
                 stardata->star[1].stellar_type,
                 stardata->common.orbit.separation,
                 stardata->common.orbit.angular_momentum);

    /*
     * Initial guess is from the previous timestep
     */
    double mdot = star->adaptive2_mdot;


    /*
     * Dubious assumption that we shouldn't lose
     * more than 0.1*M in a timestep.
     */
    const double maxmdot = 1e6 * 0.1 * star->mass / dt;

    /*
     * Set the flag so algorithms know we are adapting the RLOF rate
     */
    stardata->model.adapting_RLOF2 = TRUE;

    rif(vb)printf("Top of adaptive RLOF : count = %u\n",
                  stardata->model.adaptive_RLOF2_adapt_count);
    if(stardata->model.adaptive_RLOF2_adapt_count == 0)
    {
        /*
         * First time
         */
        if(Is_not_zero(star->adaptive2_mdot))
        {
            /*
             * We have Mdot0 from the previous (converged) RLOF call
             */
            const double prev = -stardata->previous_stardata->star[star->starnum].derivative[DERIVATIVE_STELLAR_MASS_RLOF_LOSS];
            star->prevMdot0 =
                Is_really_zero(prev) ? star->prevMdot0 : prev;
        }
        star->prevMdot1 = 0.0;
        star->prevR0 = r;
        star->prevR1 = 0.0;
    }
    else
    {
        /*
         * shift the previously stored values
         * and set the current step's
         */
        star->prevR1 = star->prevR0;
        star->prevR0 = r;
        star->prevMdot1 = star->prevMdot0;
        star->prevMdot0 = mdot;
    }

    star->prevM0 = stardata->star[0].mass;
    star->prevst0 = stardata->star[0].stellar_type;
    star->prevM1 = stardata->star[1].mass;
    star->prevst1 = stardata->star[1].stellar_type;


    rif(vb)printf("DATA %u prev R: %g %g (delta %g) Mdot: %g %g (delta %g) : M %g %g (delta %g) : st %d %d\n",
                  stardata->model.adaptive_RLOF2_adapt_count,
                  star->prevR0,
                  star->prevR1,
                  star->prevR0 - star->prevR1,
                  star->prevMdot0,
                  star->prevMdot1,
                  star->prevMdot0 - star->prevMdot1,
                  star->prevM0,
                  star->prevM1,
                  star->prevM0 - star->prevM1,
                  star->prevst0,
                  star->prevst1
        );

    rif(vb)printf("DATAX %d %u [ R=%20.15g targetR=%20.15g diff=%20.15g absdiff=%20.15g ] M=%20.15g star->adaptive2_mdot=%20.15g\n",
                  stardata->model.model_number,
                  stardata->model.adaptive_RLOF2_adapt_count,
                  r,
                  target_radius,
                  r - target_radius,
                  Abs_diff(r,target_radius),
                  star->mass,
                  star->adaptive2_mdot
        );
    rif(vb)printf("ADAP %s%d:%u%s M = %15.12g %15.12g ; mdot was %g : maxmdot = %g, dt = %g : R=%15.12g RL=%15.12g eps=%g ",
                  stardata->store->colours[RED],
                  stardata->model.model_number,
                  stardata->model.adaptive_RLOF2_adapt_count,
                  stardata->store->colours[COLOUR_RESET],
                  mass,
                  star->mass,
                  mdot,
                  maxmdot,
                  dt,
                  r,
                  target_radius,
                  Abs_diff(r,target_radius)
        );
    rif(vb)printf("\n");

    double newMdot;
    Boolean use_thermal_rate = FALSE;
    if(stardata->model.adaptive_RLOF2_adapt_count == 0)
    {
        /*
         * First time estimate
         */
        newMdot  =

            Is_not_zero(stardata->previous_stardata->star[star->starnum].derivative[DERIVATIVE_STELLAR_MASS_RLOF_LOSS]) ?
            (-stardata->previous_stardata->star[star->starnum].derivative[DERIVATIVE_STELLAR_MASS_RLOF_LOSS]) :

            Is_not_zero(stardata->previous_stardata->star[star->starnum].adaptive2_mdot) ?
            stardata->previous_stardata->star[star->starnum].adaptive2_mdot :

            (MDOT_INITIAL_GUESS);

        newMdot = Min(maxmdot,newMdot);

        rif(vb)printf("first time (%s) dt = %g, M will be %g -> mdot %g\n",
                      Is_not_zero(stardata->previous_stardata->star[star->starnum].adaptive2_mdot) ?
                      "use previous" :
                      "use MDOT_INITIAL_GUESS",
                      stardata->model.dt,
                      mass - mdot * dt,
                      newMdot);
        rif(vb)printf("DATA STEP 0 newMdot %g\n",newMdot);
    }
    else if(stardata->model.adaptive_RLOF2_adapt_count == 1)
    {
        /*
         * Shift a little before we can do anything better
         */
        newMdot = mdot * (r > target_radius ? (INCREASE_FACTOR) : (DECREASE_FACTOR));
        //newMdot = mdot * r/target_radius;
        //newMdot = Min(maxmdot,newMdot); // will cause failures!
        rif(vb)printf("DATA STEP 1 mdot %g\n",newMdot);
    }
    else
    {
        const double alpha = 0.3; /* convergencefactor */
        const double maxdeltalog = 1;
        double deltalogR = Saferlog10(r) - Saferlog10(target_radius);
        double dlogMdot = Saferlog10(star->prevMdot0) - Saferlog10(star->prevMdot1);
        double dlogR = Saferlog10(star->prevR0) - Saferlog10(star->prevR1);
        double deltalogMdot = Max(-maxdeltalog,
                                  Min(maxdeltalog,
                                      -deltalogR * dlogMdot / dlogR));
        rif(vb)printf("delta log Mdot = %g from dlogR = %g\n",
                      deltalogMdot, dlogR);
        double logMdot = log10(mdot);
        double newlogMdot = logMdot + alpha * deltalogMdot;
        newMdot = exp10(newlogMdot);
        Boolean fail;
        if(Is_really_zero(dlogR) || Is_really_zero(dlogMdot))
        {
            rif(vb)printf("problem: dlogMdot %g or dlogR %g is zero :(\n",
                          dlogMdot,
                          dlogR
                );
            fail = TRUE;
        }
        else if(0 && dlogMdot/dlogR > 0.0)
        {
            /*
             * We require dlogMdot/dlogR < 0 for the adaptive scheme
             * to work: this cannot happen. Set fail=TRUE because the
             * scheme failed, but set can_apply_limits and converged
             * TRUE as well so we don't repeat. Set Mdot = Mdot_thermal.
             */
            rif(vb)printf("problem: dlogMdot %g / dlogR %g = %g < 0.0 ... this means there is no solution\n",
                          dlogMdot,
                          dlogR,
                          dlogMdot/dlogR
                );
            use_thermal_rate = TRUE;
            *can_apply_limits = TRUE;
            newMdot =  RLOF_donor_thermal_rate_limit(stardata,star);
            fail = TRUE;
        }
        else
        {
            fail = FALSE;
        }

        rif(vb)printf("make newMdot from min maxmdot=%g, newmdot=%g\n",
                      maxmdot,
                      newMdot);
        newMdot = Min(maxmdot,newMdot);

        rif(vb)printf("DLOGMDOT %g - %g = %g\n",
                      log10(Max(1e-100,star->prevMdot0)),
                      log10(Max(1e-100,star->prevMdot1)),
                      log10(Max(1e-100,star->prevMdot0)) - log10(Max(1e-100,star->prevMdot1)));
        rif(vb)printf("DATA STEP n %u prevR0 = %15.12g, prevR1 = %15.12g, prevMdot0 = %g, prevMdot1 = %g -> dlogMdot = %g / dlogR = %g * deltalogR = %g -> deltalogMdot = %g -> logMdot %g -> newlogMdot %g -> want newMdot %g (newM %g)\n",
                      stardata->model.adaptive_RLOF2_adapt_count,
                      star->prevR0,
                      star->prevR1,
                      star->prevMdot0,
                      star->prevMdot1,
                      dlogMdot,
                      dlogR,
                      deltalogR,
                      deltalogMdot,
                      logMdot,
                      newlogMdot,
                      newMdot,
                      star->mass - dt * newMdot
            );

        if(use_thermal_rate == TRUE)
        {
            rif(vb)printf("Using thermal rate\n");
        }
        else if(isnan(newMdot) || fail==TRUE)
        {
            rif(vb)printf("newMdot = %g, dlogR = %g, dlogMdot = %g\n",newMdot,dlogR,dlogMdot);
            if(Is_really_zero(dlogR) && Is_not_zero(dlogMdot))
            {
                rif(vb)printf("no change in radius when we changed Mdot, this is step %u\n",
                              stardata->model.adaptive_RLOF2_adapt_count);
                rif(vb)printf("Prev step gave: Mdot=%30.20e R=%30.20e dR=%g\n",
                              star->prevMdot0,
                              r,
                              r - target_radius);
                rif(vb)printf("Prevprev step: Mdot=%30.20e R=%30.20e dR=%g\n",
                              star->prevMdot1,
                              star->prevR0,
                              star->prevR0 - target_radius);
            }
        }
    }

    if(Fequal(mdot,newMdot) &&
       stardata->model.adaptive_RLOF2_adapt_count >1)
    {
        /*
         * If the mass transfer rates are identical
         * then convergence is stuck
         */
        rif(vb)printf("%sMdot now == mdot previous : reduce from %g to %g!%s\n",
                      stardata->store->colours[YELLOW],
                      newMdot,
                      newMdot*0.9,
                      stardata->store->colours[COLOUR_RESET]);

        newMdot *= r < target_radius ? 1.1 : 0.9;
    }

    mdot = newMdot;

    /*
     * Check for convergence
     */
    star->adaptive2_converged =
        use_thermal_rate == TRUE ? TRUE :
        Float_same_within_eps(r,
                              target_radius,
                              THRESHOLD);
#ifdef OVERRELAX
    if(star->adaptive2_converged == TRUE &&
       stardata->model.adaptive_RLOF2_adapt_count < MAX_ITERATIONS &&
       !Float_same_within_eps(r,target_radius,OVERRELAX_THRESHOLD))
    {
        star->adaptive2_converged = FALSE;
    }
#endif//OVERRELAX

    /*
     * We can apply limits, e.g. thermal and dynamical,
     * to the mass-transfer rate only if we're converged.
     */
    *can_apply_limits = star->adaptive2_converged;

    if(star->adaptive2_converged == TRUE)
    {
        //mdot  = Min(maxmdot,mdot);
    }

    star->adaptive2_mdot = mdot;

    rif(vb)printf("%smdot = %g (change of %g) : converged? %s (dt = %g, zoomfac %g, M will be %g, R/RL-1 = %g < %g) %s\n",
                  star->adaptive2_converged == TRUE ? stardata->store->colours[CYAN] : stardata->store->colours[COLOUR_RESET],
                  star->adaptive2_mdot,
                  star->adaptive2_mdot/stardata->previous_stardata->star[star->starnum].derivative[DERIVATIVE_STELLAR_MASS_RLOF_LOSS] - 1.0,
                  Yesno(star->adaptive2_converged),
                  dt,
                  stardata->model.dt_zoomfac,
                  star->mass - mdot * dt,
                  r/target_radius-1.0,
                  THRESHOLD,
                  stardata->store->colours[COLOUR_RESET]
        );


    if(isnan(mdot))
    {
        Exit_binary_c(2,"NAN in adapt2\n");
    }
    else if(star->mass - mdot * dt < 0.0)
    {
        /*
         * Removing all the mass should be impossible
         */
        Exit_binary_c(2,"M<0 in adapt2\n");
    }

    return mdot;
}


if(0){
    /*
     * Rexcess = slope * Mdot + intercept
     *
     * we want Rexcess = 0
     *
     * hence
     *
     * Mdot = -intercept/slope
     */

    /*
     * R = slope * log10(-Mdot) + intercept
     * and we want R = target_radius so
     *
     * Mdot = (target_radius - intercept)/slope
     */
    double slope2 =
        (r - star->prevR0) /
        (star->prevMdot0 - star->prevMdot1);
    double intercept2 = star->prevR0;

    e1 = Is_really_zero(slope) ? 0.0 : (-intercept / slope);
    e2 = Is_really_zero(slope2) ? 0.0 : ((target_radius - intercept2) / slope2);

    if(vb)printf("slope = (%g - %g) / (%g - %g) = %g ... intercept %g -> %g\n",
                 Rexcess,
                 Rexcess0,
                 star->prevMdot0,
                 star->prevMdot1,
                 slope,
                 intercept,
                 e1);
    if(vb)printf("slope2 = (%g - %g) / (%g - %g) = %g ... intercept2 %g want %g -> %g\n",
                 r,
                 star->prevR0,
                 star->prevMdot0,
                 star->prevMdot1,
                 slope2,
                 intercept2,
                 target_radius,
                 e2);

    if(vb)printf("Rexcess = %g * Mdot + %g\n",
                 slope,
                 intercept);
    if(vb)printf("Radius - Roche_radius = %g * Mdot + %g\n",
                 slope2,
                 intercept2);

    /*
     * 0 = slope * Mdot + intercept
     *
     * ->
     *
     * -intercept/slope = -Mdot
     */
    const double f = 0.5;
    mdot = f * mdot + (1.0-f) * e1;

    if(vb)printf("new estimate %d = %g with solution %g %g\n",
                 stardata->model.adaptive_RLOF2_adapt_count,
                 mdot,
                 slope * mdot + intercept,
                 slope2 * mdot + intercept2 - target_radius);
}
#endif // __DEPRECATED
#endif // ADAPTIVE_RLOF2
