#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "RLOF.h"

int Pure_function RLOF_type(const struct star_t * Restrict const donor)
{
    int rlof_type;

    /*
     * identify RLOF type (A,B,C) based on evolutionary 
     * state of the donor star
     */
    if(donor->stellar_type <= MAIN_SEQUENCE)
    {
        /* main sequence */
        rlof_type = RLOF_A;
    }
    else if(donor->stellar_type < HeWD)
    {
	/* H-shell or He burning */
	rlof_type = donor->stellar_type>=CHeB ? RLOF_C : RLOF_B;
    }
    else
    {
        /* presumably RLOF from a compact object */
        rlof_type = RLOF_OTHER;
    }

    return rlof_type;
}
