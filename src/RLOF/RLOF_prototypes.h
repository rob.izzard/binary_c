#pragma once
#ifndef RLOF_PROTOTYPES_H
#define RLOF_PROTOTYPES_H


#include "../binary_c_parameters.h"
#include "../binary_c_macros.h"

#include "../binary_c_structures.h"
#include "../binary_c_prototypes.h"

#include "../evolution/evolution_macros.h"
#include "../nucsyn/nucsyn.h"


int RLOF_stability_tests(struct stardata_t * Restrict stardata);

void RLOF_set_rmin(struct stardata_t * const stardata);
Boolean Pure_function RLOF_test_for_accretion_disc(struct stardata_t * Restrict const stardata);


double RLOF_critical_q(struct stardata_t * Restrict const stardata);
void RLOF_dynamical_transfer_from_low_mass_MS_star(struct stardata_t * const stardata,
                                                   struct RLOF_orbit_t * const RLOF_orbit);

Boolean RLOF_dynamical_comenv(struct stardata_t * Restrict const stardata,
                              struct RLOF_orbit_t * const RLOF_orbit);
void RLOF_dynamical_transfer_from_WD_to_remnant(struct stardata_t * Restrict const stardata,
                                                struct RLOF_orbit_t * const RLOF_orbit);
void RLOF_NSNS_NSBH_merger(struct stardata_t * Restrict const stardata,
                           struct RLOF_orbit_t * const RLOF_orbit);
void RLOF_black_hole_merger(struct stardata_t * Restrict const stardata,
                            struct RLOF_orbit_t * const RLOF_orbit);

double Pure_function RLOF_mass_transfer_rate(struct stardata_t * const stardata,
                                             struct star_t * const donor);

double Pure_function RLOF_instantaneous_mass_transfer_rate(
    const double r,
    const double roche_radius,
    const double mass,
    struct stardata_t * const stardata,
    struct star_t * const donor);

Boolean RLOF_mass_transferred_in_one_orbit(struct stardata_t * Restrict const stardata,
                                           const double mass_transfer_rate,
                                           struct RLOF_orbit_t * Restrict const RLOF_orbit);
double RLOF_speed_up_factor(struct stardata_t * Restrict const stardata,
                            const struct RLOF_orbit_t * Restrict const RLOF_orbit);

void RLOF_orbital_angular_momentum_loss(struct stardata_t * Restrict const stardata);

void RLOF_exit(struct stardata_t * const stardata) No_return;

double Constant_function calc_target_radius(const double r,
                                            const double roche_radius,
                                            struct stardata_t * Restrict const stardata);



double Pure_function effective_Roche_radius(const struct stardata_t * const stardata,
                                            const struct star_t * const star);
double Pure_function Hachisu_max_rate(struct stardata_t * const stardata,
                                      const struct star_t * donor,
                                      const struct star_t * accretor);

int Pure_function RLOF_type(const struct star_t * Restrict const donor);

void RLOF_dM_orbit_to_time_derivatives(struct stardata_t * Restrict const stardata,
                                       struct RLOF_orbit_t * Restrict const RLOF_orbit);

void RLOF_stellar_angmom_derivative(struct stardata_t * Restrict const stardata);

int RLOF_unstable_mass_transfer(struct stardata_t * Restrict const stardata,
                                const int instability,
                                const Boolean do_event);

Boolean Pure_function RLOF_overflowing(struct stardata_t * Restrict const stardata,
                                       const double fac);

void RLOF_init_dM_orbit(struct stardata_t * Restrict const stardata,
                        struct RLOF_orbit_t * Restrict const RLOF_orbit);

void RLOF_merger(struct stardata_t * Restrict const stardata,
                 struct RLOF_orbit_t * const RLOF_orbit);

double RLOF_adaptive_mass_transfer_rate(const double r,
                                        const double roche_radius,
                                        const double mass,
                                        struct stardata_t * const stardata,
                                        struct star_t * const star /* primary (donor) */,
                                        const double mdotBSE,
                                        const double dt
    );

double RLOF_adaptive_mass_transfer_rate2_update(struct stardata_t * const stardata);

Event_handler_function RLOF_unstable_mass_transfer_event_handler(void * eventp,
                                                                 struct stardata_t * const stardata,
                                                                 void * data);


double Pure_function RLOF_donor_thermal_rate_limit(const struct stardata_t * const stardata,
                                                   const struct star_t * const donor);
double Pure_function RLOF_donor_dynamical_rate_limit(const struct stardata_t * const stardata,
                                                     const struct star_t * const donor);
double Pure_function RLOF_donor_envelope_rate_limit(const struct stardata_t * const stardata,
                                                    const struct star_t * const donor);

#ifdef ADAPTIVE_RLOF2
double RLOF_adaptive_mass_transfer_rate2(const double r,
                                         const double roche_radius,
                                         const double mass,
                                         struct stardata_t * const stardata,
                                         struct star_t * const star /* primary (donor) */,
                                         const double dt,
                                         Boolean * const can_apply_limits
    );
#endif//ADAPTIVE_RLOF2

double RLOF_phase_dependent(struct stardata_t * const stardata,
                            struct star_t * const donor);

void RLOF_stellar_angmom_derivative(struct stardata_t * Restrict const stardata);
void RLOF_eccentricity_derivative(struct stardata_t * Restrict const stardata);

#endif // RLOF_PROTOYPES.H
