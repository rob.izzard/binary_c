#include "binary_c.h"


#ifdef ADAPTIVE_RLOF

#define CHANGE_INITIAL_MASS2 (ON_EITHER_MAIN_SEQUENCE(star->stellar_type)|| \
                              (star->stellar_type==HeHG)                \
                              ||(star->stellar_type==HeGB))


double RLOF_adaptive_mass_transfer_rate(const double r,
                                        const double roche_radius,
                                        const double mass,
                                        struct stardata_t * const stardata,
                                        struct star_t * const star /* primary (donor) */,
                                        const double mdotBSE,
                                        const double dt
    )
{
    /*
     * Calculate and return the mass transfer rate
     * which keeps the star *just* on its Roche lobe
     */
    double mdot = mdotBSE; // returned
    unsigned int drzerocount = 0;
    Boolean use_BSE=FALSE; // logic flow control

    /*
     * Perturbed star struct
     */
    struct star_t * pstar = New_star_from(star);

    Dprint("Adaptive RLOF star %d t=%g dt=%g kw=%d BSE=%g dt=%g roche_radius=%g mass=%g (star mass=%g mt=%g r=%g roche_radius=%g roche_radius_at_periastron=%g) convenv=%d : pstar = %p\n",
           star->starnum,
           stardata->model.time,
           dt,
           star->stellar_type,
           mdot,
           dt,
           roche_radius,mass,
           star->phase_start_mass,
           star->mass,
           star->radius,
           star->roche_radius,
           star->roche_radius_at_periastron,
           CONVECTIVE_ENVELOPE(star->stellar_type),
           (void*)pstar);

    /* target radius : ~ the Roche lobe radius */
    double target_radius = calc_target_radius(r,roche_radius,stardata);

#ifdef ADAPTIVE_RLOF_CONVECTIVE_CHECK
    /*
     * In convective stars, simply try to eject the envelope,
     * at the dynamical rate.
     */
    if(CONVECTIVE_ENVELOPE(star->stellar_type) || star->stellar_type == 0)
    {
        mdot = menv / dynamical_timescale(star);
        stardata->common.mdot_RLOF_adaptive=mdot;
        fprintf(stderr,"%s conv check : eject all env%s\n",stardata->store->colours[YELLOW],stardata->store->colours[COLOUR_RESET]);
    }
    else
#endif // ADAPTIVE_RLOF_CONVECTIVE_CHECK


        /*
         * Adapt the star's mass such that its radius fits inside the
         * Roche lobe (just) if we can
         * (NB you could use the target_radius as this is slightly outside the
         * Roche lobe: this is "good enough" (i.e. 1% outside)
         */
        if(More_or_equal(r,target_radius))
    {
#ifdef ADAPTIVE_RLOF_LOG
        stardata->common.used_rate = RLOF_RATE_ADAPTIVE;
#endif
        double dm; /* mass increment */

        /* determine if we want to change the initial mass
         * before stellar_structure calls */
        const Boolean change_phase_start_mass = (CHANGE_INITIAL_MASS2);

#ifdef __DEPRECATED

        if(star->prev_dm > TINY
#ifdef ADAPTIVE_RLOF_CONVECTIVE_CHECK
           && !CONVECTIVE_ENVELOPE(star->stellar_type)
#endif
            )
        {
            /* mass increment, first guess : something small */
            dm = - star->prev_dm;
        }
        else
        {
            /*
             * If we have done this before, use the previous guess
             * (which is *much* faster as it provides us with a
             * solution after just a few iterations!)
             */
            dm = - mass * 1e-4 ;
        }
#endif // __DEPRECATED
        dm = - mass * 1e-7;
        //if(star->stellar_type==HG) dm=-0.1;

        Dprint("First estimate of dm = %g\n",dm);

        /*
         * Revert the star to its starting conditions
         */
        copy_star(stardata,
                  star,
                  pstar);

        /* iterative loop to force r=target_radius */
        unsigned int i = 0;
        while(i < ADAPTIVE_RLOF_IMAX)
        {
            /*
             * Calculate structure at the new, pertubed mass
             * at the next timestep
             */
            copy_star(stardata,
                      star,
                      pstar);
            pstar->mass = star->mass;
            update_mass(stardata,pstar,dm);
            pstar->age += dt*1e-6;
            if(change_phase_start_mass==TRUE)
                pstar->phase_start_mass = star->phase_start_mass + dm;

            pstar->deny_SN++;
            stellar_structure(stardata,
                              STELLAR_STRUCTURE_CALLER_RLOF_mass_transfer_rate,
                              pstar,
                              NULL);
            pstar->deny_SN--;

            Dprint("RRR %g orig: M=%g R=%g ROL=%g perturbed: PM=%g R=%g ST=%d\n",
                   stardata->model.time,
                   star->mass,
                   star->radius,
                   star->roche_radius,
                   pstar->mass,
                   pstar->radius,
                   pstar->stellar_type
                );

            /*
             * Check for stellar type change, don't allow it!
             */
            if(pstar->stellar_type != star->stellar_type)
            {
#ifdef SHOW_WARNINGS
                fprintf(stderr,"t=%20.12g Stellar type change! was %d now %d (m was %g now %g)\n",
                        stardata->model.time,
                        star->stellar_type,
                        pstar->stellar_type,
                        star->mass,
                        pstar->mass);
#endif
                {
                    /* reset stellar variables except the mass */
                    const double m = pstar->mass;
                    const double m0 = pstar->phase_start_mass;
                    copy_star(stardata,
                              star,
                              pstar);
                    pstar->mass = m;
                    pstar->phase_start_mass = m0;
                }

                /* in some cases (convective, WD with an envelope)
                 * attempt to throw off the whole envelope */
                if(pstar->stellar_type>=HeWD
#ifdef ADAPTIVE_RLOF_CONVECTIVE_CHECK
                   ||(CONVECTIVE_ENVELOPE(star->stellar_type))||(star->stellar_type ==0)
#endif
                    )
                {
                    const Core_type core_id = ID_core(pstar->stellar_type);
                    if(core_id != CORE_NONE)
                    {
                        pstar->mass = star->core_mass[core_id];
                    }
#ifdef PRETTYLOG
                    status=STATUS_WD_ENVELOPE;
#endif
                    break;
                }
            }

            /*
             * If the radius increases as we pull off mass
             * then the star has a convective
             * envelope and we really don't know what the mass transfer
             * rate should be.
             */
            double dr = pstar->radius - star->radius;

            /*
             * Check that the radius has changed: note that if it
             * has not changed (dr<TINY) then just leave dm and
             * keep trying.
             */
            Dprint("DR %g\n",dr);
            if(Is_not_zero(dr))
            {
                double dmdr = dm/dr ;
                Dprint("DMDR (M=%g M0=%g Mc=%g st=%d : R=%g target=%g) = %g / %g = %g\n",
                       pstar->mass,
                       pstar->phase_start_mass,
                       Outermost_core_mass(pstar),
                       pstar->stellar_type,
                       pstar->radius,
                       target_radius,
                       dm,
                       dr,
                       dmdr);
                star->dmdr = dmdr; // save for thermal rate correction

                if(dmdr < 0)
                {
                    /*
                     * What to do here? dR/dM < 0 so the star expands
                     * when we remove mass!
                     *
                     * The star wants to eject everything:
                     * We can either use the BSE formula (if RETURN_BSE_IF_NEGATIVE_DRDM
                     * is defined) or try to dump the whole star and have it limited
                     * but the various limits (e.g. thermal) later on
                     */
#ifdef ADAPTIVE_RLOF_LOG
                    stardata->common.runaway_rlof = TRUE;
#endif//ADAPTIVE_RLOF_LOG

#ifdef RETURN_BSE_IF_NEGATIVE_DRDM
                    /* alternatively, use the BSE formula */
                    Dprint("DR/DM < 0 : use BSE formula\n");
                    mdot = mdotBSE;
                    use_BSE = TRUE;
                    fprintf(stderr,"%sdr/dm<0 use BSE%s\n",stardata->store->colours[RED],stardata->store->colours[COLOUR_RESET]);
#ifdef ADAPTIVE_RLOF_LOG
                    stardata->common.used_rate = RLOF_RATE_BSE;
#endif // ADAPTIVE_RLOF_LOG
#else  //  RETURN_BSE_IF_NEGATIVE_DRDM
                    /*
                     * no matter what pstar->mass is set to,
                     * mass loss is still osciallatory
                     */
                    const Core_type core_id = ID_core(pstar->stellar_type);
                    if(core_id != CORE_NONE)
                    {
                        pstar->mass = star->core_mass[core_id];
                    }

                    Dprint("DR/DM = %g < 0 : %seject all the envelope%s menv=%g dt=%g dr=%g dm=%g\n",
                           dr/dm,stardata->store->colours[MAGENTA],stardata->store->colours[COLOUR_RESET],
                           envelope_mass(star),
                           dt,
                           dr,
                           dm);
#endif //  RETURN_BSE_IF_NEGATIVE_DRDM

                    star->prev_dm = 0.0;
#ifdef PRETTYLOG
                    status = STATUS_DMDR_NEGATIVE;
#endif//PRETTYLOG

                    /* break the loop to impose limits */

                    break;
                }
#ifdef DEBUG
                /* dR/dM > 0 : is good, output if debugging */
                else
                {
                    Dprint("DR/DM > 0 = %g/%g=%g from rwas=%g pstar->radius=%g (want roche_radius=%g) dm=%g kw=%d\n",
                           -star->radius+pstar->radius,
                           dm,
                           1.0/dmdr,
                           star->radius,
                           pstar->radius,
                           roche_radius,
                           dm,
                           star->stellar_type);
                }
#endif//DEBUG

                /*
                 * Use dmdr to calculate new dm based on Delta R, the
                 * ADAPTIVE_RLOF_ALPHA factor slows down the convergence
                 * to make sure we don't overshoot.
                 * factor retains some resolution in the approach to R=R_L
                 * 0.5 seems stable. Note this is the *linear* approximation,
                 * and something else may be more appropriate.
                 */
                double dr_rol = target_radius - pstar->radius;
                dm = ADAPTIVE_RLOF_ALPHA * dr_rol * dmdr;
                Dprint("Estimate dm=%g from dm/dr=%g, deltaR=%g\n",dm,dmdr,dr_rol);

                /* limit rate of change of mass (helps stabilise convergence) */
                double fdm=fabs(dm);
                dm = dm / fdm  * Min(ADAPTIVE_RLOF_DM_MAX,fdm);

                double radius_error = fabs(dr_rol);

                Dprint("Post HRD DR i=%u m=%8.8e r=%8.8e = %2.2f %% of Rol (=%8.8e), radius_error=%g, dm now %g\n",
                       i,pstar->mass,pstar->radius,100.0*pstar->radius/roche_radius,roche_radius,radius_error,dm);

                if(i>=ADAPTIVE_RLOF_IMIN)
                {
                    /*
                     * Check if we have converged the radius to the Roche
                     * radius to within ADAPTIVE_RLOF_THRESHOLD Rsun (and
                     * have had a minimum number of iterations)
                     */
                    Boolean check1 = (fabs(1.0-fabs(pstar->radius-target_radius)/target_radius)<1e-6);
                    Boolean check2 = (radius_error < ADAPTIVE_RLOF_MINIMUM_RADIUS_ERROR);
                    if(check1 || check2)
                    {
                        Dprint("Break : better than threshold (check1=%d check2=%d)\n\n",check1,check2);
                        /*fprintf(stderr,"%st=%g Converged m=%g (was %g) r=%g roche_radius=%g dm/dr=%g%s (%d iterations)\n",

                          stardata->store->colours[GREEN],
                          stardata->model.time,
                          pstar->mass,mass,
                          pstar->radius,target_radius,dmdr,
                          stardata->store->colours[COLOUR_RESET],i);
                        */
#ifdef PRETTYLOG
                        status=STATUS_CONVERGED;
#endif
                        break;
                    }
                }

                /* save the radius in rwas (for dr calculation next time) */
                star->radius = pstar->radius;
            }
            else
            {
                /*
                 * dr=0 : radius isn't changing with mass:
                 * usually only true if there is a stellar type change
                 * or we reach the stellar core.
                 */
                Dprint("dr=0 error\n");

#ifdef SHOW_WARNINGS
                fprintf(stderr,"t=%g dr=0 error (but dm=%g) stellar type in %d now %d\n",
                        stardata->model.time,
                        dm,
                        star->stellar_type,
                        pstar->stellar_type
                    );
#endif
                drzerocount++;

                /* fail if we have too many dr=0 errors */
                if(drzerocount>ADAPTIVE_RLOF_MAX_DRZERO_COUNT)
                {
                    use_BSE=TRUE;
                    mdot = mdotBSE;
#ifdef SHOW_WARNINGS
                    fprintf(stderr,"%sFailed dr=0%s (%d iterations)\n",stardata->store->colours[RED],stardata->store->colours[COLOUR_RESET],i);
#endif
#ifdef PRETTYLOG
                    status=STATUS_DR_ZERO;
#endif
                    break;
                }
            }

            i++; /* raise iteration counter */

            update_mass(stardata,pstar,dm);
            if(change_phase_start_mass==TRUE) pstar->phase_start_mass += dm;

            /* this should never happen, but just in case */
            if(pstar->mass < 0.0)
            {
                pstar->mass=0.0;
#ifdef SHOW_WARNINGS
                fprintf(stderr,"%sFailed dm<0%s (%d iterations)\n",stardata->store->colours[RED],stardata->store->colours[COLOUR_RESET],i);
#endif
                Dprint("pstar->mass < 0 error in RLOF_mass_transfer rate : continuing...\n");
#ifdef PRETTYLOG
                status=STATUS_MT_LT_ZERO;
#endif
                break;
            }
        }

        /* calculate mass stripped */
        dm = mass - pstar->mass;

        dm = Max(0.0,dm);

        /* check for failed convergence */
        if(i >= ADAPTIVE_RLOF_IMAX)
        {
            fprintf(stderr,"RLOF_mass_transfer_rate: failed to converge in imax=%d iterations\nM0=%g Mt=%g st=%d r=%g roche_radius=%g\n",
                    ADAPTIVE_RLOF_IMAX,pstar->phase_start_mass,pstar->mass,pstar->stellar_type,pstar->radius,roche_radius);
            RLOF_exit(stardata);
        }

        /* if we're allow to use it, set mdot */
        if(use_BSE==FALSE)
        {
            star->prev_dm=dm;
            mdot = dm/dt;
        }

        stardata->common.mdot_RLOF_adaptive=mdot; // save for logging

        Dprint("Final mass in Roche lobe = %g (dm %g) mdot = %g after %u iterations (dt=%g)\n\n",
               pstar->mass,
               dm,
               mdot,
               i,
               dt);

    }
    else
    {
        /*
         * r<roche_radius : no mass transfer ...
         * probably. assuming numerical stability
         */
        mdot = 0.0;
        stardata->common.mdot_RLOF_adaptive=mdot;
    }

    free_star(&pstar);
    return mdot;
}
#endif // ADAPTIVE_RLOF
