#include "../binary_c.h"
No_empty_translation_unit_warning;


/*
 * Donor star's maximum rate limit : envelope
 */

double Pure_function RLOF_donor_envelope_rate_limit(const struct stardata_t * const stardata,
                                                    const struct star_t * const donor)
{
    return
        /*
         * If the star has no defined core, or the multiplier < 0,
         * don't set a limit (i.e. use a very large rate)
         */
        (!CORED_STAR(donor->stellar_type) || stardata->preferences->donor_limit_envelope_multiplier < -TINY) ?
        VERY_LARGE_MASS_TRANSFER_RATE :
        /*
         * Otherwise use the envelope mass / timestep as the limit
         */
        (envelope_mass(donor) / (1e6 * stardata->model.dtm));
}
