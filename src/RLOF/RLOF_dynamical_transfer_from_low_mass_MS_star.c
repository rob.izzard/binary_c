#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "RLOF.h"

#undef naccretor
#undef ndonor

void RLOF_dynamical_transfer_from_low_mass_MS_star(struct stardata_t * const stardata,
                                                   struct RLOF_orbit_t * const RLOF_orbit)
{
    Dprint("RLOF caught dynamical low mass MS donor event\n");
    RLOF_stars;

    double taum;
#ifdef NUCSYN
    double am,bm;

#ifdef NUCSYN_STRIP_AND_MIX
    accretor->strip_and_mix_disabled=TRUE;
    donor->strip_and_mix_disabled=TRUE;
#endif // NUCSYN_STRIP_AND_MIX
#endif // NUCSYN

    /*
     * This will be dynamical mass transfer of a similar nature to
     * common-envelope evolution.  The result is always a single
     * merged star.
     */


    /*
     * Eddington limit for accretion on to the secondary in one orbit.
     */
    const double Eddington_rate =
        eddington_limit_for_accretion(accretor->radius,
                                      stardata->preferences->accretion_limit_eddington_LMMS_multiplier,
                                      Hydrogen_mass_fraction(donor));

    /*
     * Dynamical timescale for the primary.
     */
    const double tdyn = dynamical_timescale(donor);

    taum = sqrt(donor->tkh*tdyn);
    RLOF_orbit->dM_RLOF_lose = -donor->mass;

    if(ON_MAIN_SEQUENCE(accretor->stellar_type))
    {
        /*
         * Restrict accretion to thermal timescale of secondary.
         */
        Dprint("Restrict accretion to thermal timescale of secondary.");
        RLOF_orbit->dM_RLOF_transfer = -taum/accretor->tkh*RLOF_orbit->dM_RLOF_lose;
        update_mass(stardata,accretor,
                    RLOF_orbit->dM_RLOF_transfer);

#ifdef NUCSYN
        /* Mix abundances of star 1 through star 2 */
        bm = RLOF_orbit->dM_RLOF_transfer/accretor->mass;
        am=1.0-bm;
        Nucsyn_isotope_stride_loop(
                accretor->Xenv[i] = am * accretor->Xenv[i] + bm * donor->Xenv[i];
            );
#endif

#ifdef BSE
        /*
         * Rejuvenate if the star is still on the main sequence.
         */
        accretor->phase_start_mass = accretor->mass;

        struct BSE_data_t * bse = new_BSE_data();
        stellar_timescales(stardata,
                           accretor,
                           bse,
                           accretor->phase_start_mass,
                           accretor->mass,
                           accretor->stellar_type);
        /*
         * If the star has no convective core then the effective age decreases,
         * otherwise it will become younger still.
         */
        accretor->age = bse->tm/accretor->tms*accretor->age;
        if(!Convective_star(naccretor))
        {
            accretor->age *= (1.0 - RLOF_orbit->dM_RLOF_transfer/accretor->mass);
        }
        accretor->epoch = stardata->model.time - accretor->age;
        Dprint("set epoch 1\n");
        free_BSE_data(&bse);
#endif // BSE
    }
    else if(accretor->stellar_type<=TPAGB)
    {
        /*
         * Add all the material to the giant's envelope.
         */
        Dprint("Add all the material to the giant's envelope.");
        RLOF_orbit->dM_RLOF_transfer = -RLOF_orbit->dM_RLOF_lose;
        update_mass(stardata,accretor,
                    RLOF_orbit->dM_RLOF_transfer);

#ifdef NUCSYN
        bm=RLOF_orbit->dM_RLOF_transfer/(envelope_mass(accretor)+TINY);
        am=1.0-bm;
        Nucsyn_isotope_stride_loop(
            accretor->Xenv[i] = am * accretor->Xenv[i] + bm * donor->Xenv[i];
            );
#endif // NUCSYN
#ifdef BSE
        if(accretor->stellar_type==HERTZSPRUNG_GAP)
        {
            accretor->phase_start_mass = accretor->mass;
            struct BSE_data_t * bse = new_BSE_data();
            stellar_timescales(stardata,
                               accretor,
                               bse,
                               accretor->phase_start_mass,
                               accretor->mass,
                               accretor->stellar_type);

            accretor->age = bse->tm + bse->timescales[T_BGB]*
                (accretor->age - accretor->tms)/accretor->tbgb;
            accretor->epoch = stardata->model.time - accretor->age;
            Dprint("set epoch 2\n");
            free_BSE_data(&bse);
        }
#endif//BSE

    }
    else if(accretor->stellar_type<=ONeWD)
    {
        /*
         * Form a new giant envelope.
         */
        Dprint("Form a new giant envelope.");
        RLOF_orbit->dM_RLOF_transfer = -RLOF_orbit->dM_RLOF_lose;

        Stellar_type stellar_type = merger_stellar_type(stardata,
                                                        donor,
                                                        accretor);

        Dprint("Set from collision matrix[%d][%d] = %d\n",
               donor->stellar_type,
               accretor->stellar_type,
               stellar_type);

        if(stellar_type==CHeB)
        {
            set_no_core(accretor);
            const Core_type core_id = ID_core(stellar_type);
            if(core_id != CORE_NONE)
            {
                accretor->core_mass[core_id] = accretor->mass;
            }
        }

#ifdef NUCSYN
        /* New abundances are those of star 1 */
        Dprint("Copy abundances\n");
        Copy_abundances(donor->Xenv,accretor->Xenv);
#endif//NUCSYN

        /*
         * Check for planets or low-mass WDs.
         */
        Dprint("Check for planets or low-mass WDs\n");
        if((accretor->stellar_type==HeWD && accretor->mass<0.05)||

           /* in BSE the 0.05 was 0.5, which was presumably a typo */
           (accretor->stellar_type>=COWD && accretor->mass<0.05))
        {
            stellar_type = donor->stellar_type;
            donor->mass = accretor->mass + RLOF_orbit->dM_RLOF_transfer;
            accretor->mass=0.0;
        }
        else
        {
            Dprint("update accretor mass\n");
            update_mass(stardata,accretor,
                        RLOF_orbit->dM_RLOF_transfer);
#ifdef BSE
            {

                double _mc = Outermost_core_mass(accretor);
                Dprint("Call giant age with _mc = %g\n",_mc);
                giant_age(&_mc,
                          accretor->mass,
                          &stellar_type,
                          &accretor->phase_start_mass,
                          &accretor->age,
                          stardata,
                          accretor);
                Dprint("set no core\n");
                set_no_core(accretor);
                const Core_type core_id = ID_core(accretor->stellar_type);
                if(core_id != CORE_NONE)
                {
                    Dprint("set new core with ID %d from stellar type %d\n",
                           core_id,
                           accretor->stellar_type);
                    accretor->core_mass[core_id] = _mc;
                }
            }
            Dprint("giant age gave stellar type %d\n",stellar_type);
#endif//BSE
            accretor->epoch = stardata->model.time - accretor->age;
            Dprint("set epoch 3 = %g from time=%g - age=%g\n",
                   accretor->epoch,
                   stardata->model.time,
                   accretor->age
                );
        }
        /*
         * NB any existing accretion layer will be mixed into the new envelope
         * in the update_masses() function
         */
        accretor->stellar_type = stellar_type;
        Dprint("set ac st %d\n",stellar_type);
    }
    else
    {
        /*
         * The neutron star or black hole simply accretes at the Eddington rate.
         */
        Dprint("The neutron star or black hole simply accretes at the Eddington rate.");
        RLOF_orbit->dM_RLOF_transfer = Min(Eddington_rate*taum,
                                           -RLOF_orbit->dM_RLOF_lose);
        if(RLOF_orbit->dM_RLOF_transfer < -RLOF_orbit->dM_RLOF_lose)
            stardata->model.supedd = TRUE;
        update_mass(stardata,accretor,
                    RLOF_orbit->dM_RLOF_transfer);
    }

    stardata->model.coalesce = TRUE;

    /*
     * Case where the other star cannot accept all the mass being dumped on
     * it e.g. low mass MS dumping all of its mass onto a low mass MS
     */
    if(accretor->mass > TINY)
    {
        /* Lose mass of star ndonor to environment */
        calc_yields(stardata,
                    donor,
                    -RLOF_orbit->dM_RLOF_lose,
#ifdef NUCSYN
                    donor->Xenv,
#endif//NUCSYN
                    RLOF_orbit->dM_RLOF_transfer,
#ifdef NUCSYN
                    accretor->Xenv,
#endif//NUCSYN
                    ndonor,
                    YIELD_NOT_FINAL,
                    SOURCE_RLOF);

        /*
         * Update (massless) structures
         */
        stellar_structure_make_massless_remnant(stardata,
                                                donor);
        stellar_structure_BSE(stardata,
                              0,
                              accretor,
                              NULL);
    }
    else
    {
        /* Lose star naccretor */
        calc_yields(stardata,
                    accretor,
                    -RLOF_orbit->dM_RLOF_lose,
#ifdef NUCSYN
                    accretor->Xenv,
#endif
                    RLOF_orbit->dM_RLOF_transfer,
#ifdef NUCSYN
                    donor->Xenv,
#endif//NUCSYN
                    naccretor,
                    YIELD_NOT_FINAL,
                    SOURCE_RLOF);

        donor->stellar_type = accretor->stellar_type;
        stellar_structure_make_massless_remnant(stardata,
                                                accretor);
    }

    stardata->common.orbit.separation = 0.0;
    stardata->common.orbit.period = 0.0;
    stardata->common.orbit.angular_momentum = 0.0;
    stardata->common.orbit.eccentricity = 0.0;

    update_orbital_variables(stardata,
                             &stardata->common.orbit,
                             donor,
                             accretor);

    Dprint("post RLOF dyn st %d %d\n",
           donor->stellar_type,
           accretor->stellar_type
        );
}
