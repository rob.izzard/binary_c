#pragma once
#ifndef RLOF_UPDATE_MASSES_H
#define RLOF_UPDATE_MASSES_H

#include "../evolution/evolution_macros.h"
#include "../binary_c.h"
No_empty_translation_unit_warning;


#if (DEBUG==1)
#define MIXDEBUG
#define Mprint(...) if((DEBUG)&&(Debug_expression))debug_fprintf(__FILE__,__LINE__,__VA_ARGS__);
#else
#define Mprint(...) /**/;
#endif

#define DONOR (Other_star(naccretor))
#define ACCRETOR (naccretor)

//#define MIXDEBUG

#endif // RLOF_UPDATE_MASSES_H
