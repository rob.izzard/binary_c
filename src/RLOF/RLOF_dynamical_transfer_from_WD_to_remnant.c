#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "RLOF.h"


void RLOF_dynamical_transfer_from_WD_to_remnant(struct stardata_t * Restrict const stardata,
                                                struct RLOF_orbit_t * const RLOF_orbit)
{
    /*
     * Dynamical transfer from a white dwarf to a
     * remnant of type WD, NS or BH.
     *
     * The result is a merged object or a destroyed
     * object, depending on the stellar type and
     * composition.
     *
     * Note: this is different from a "merged" star.
     * Merged stars happen when both stars exceed their
     * Roche radius. In a dynamical transfer, only
     * one star overflows its Roche lobe.
     */
    RLOF_stars;
    set_kelvin_helmholtz_times(stardata);

    /*
     * Dynamical transfer: lose all the mass
     */
    RLOF_orbit->dM_RLOF_lose = -donor->mass;

    if(stardata->preferences->accretion_limit_eddington_WD_to_remnant_multiplier < -TINY)
    {
        /*
         * No Eddington limit
         */
        RLOF_orbit->dM_RLOF_transfer = -RLOF_orbit->dM_RLOF_lose;
        stardata->model.supedd = FALSE;
    }
    else
    {
        /*
         * Eddington limit for accretion on to the secondary in one orbit
         */
        const double Eddington_rate =
            eddington_limit_for_accretion(accretor->radius,
                                          stardata->preferences->accretion_limit_eddington_WD_to_remnant_multiplier,
                                          Hydrogen_mass_fraction(donor));

        /*
         * Dynamical timescale of the primary
         */
        const double tdyn = dynamical_timescale(donor);

        /*
         * Mass transfer timescale is the geometric mean of the
         * thermal (Kelvin-Helmholtz) and dynamical rate.
         */
        const double taum = sqrt(donor->tkh * tdyn);

        /*
         * Hence the limiting rate
         */
        const double limiting_rate = Eddington_rate * taum;

        /*
         * Limit what can be transferred
         */
        RLOF_orbit->dM_RLOF_transfer =
            Min(limiting_rate,
                -RLOF_orbit->dM_RLOF_lose);

        /*
         * If we exceed the Eddington limit,
         * set the logging flag.
         */
        stardata->model.supedd =
            Boolean_(RLOF_orbit->dM_RLOF_transfer > limiting_rate);
    }



    /*
     * Merge stars into the accretor without ejecting material
     * during the merging process (hence the FALSE) - this is
     * done by us in this subroutine.
     */
    donor->mass = RLOF_orbit->dM_RLOF_transfer;
    struct star_t * merged_star = merge_stars(stardata,
                                              accretor->starnum,
                                              FALSE
#ifdef NUCSYN
                                              ,NULL
#endif // NUCSYN
        );
#ifdef NUCSYN
    /*
     * donor's yields are merged with accretor
     */
    nucsyn_zero_yields(donor);
#endif
    copy_star(stardata,
              merged_star,
              accretor);
    free_star(&merged_star);
    /*
     * Only allow SNIa from RLOFing system if we're told
     */
    if(stardata->preferences->unstable_RLOF_can_trigger_SNIa == FALSE)
    {
        accretor->SN_type = SN_NONE;
    }

    /*
     * Evaporate the donor and clean up the orbit
     */
    stellar_structure_make_massless_remnant(stardata,
                                            donor);


    stardata->model.coalesce = TRUE;
    memset(&stardata->common.orbit,0,sizeof(struct orbit_t));
    stardata->common.orbit.separation = 0.0;
    stardata->common.orbit.period = 0.0;
    stardata->common.orbit.angular_momentum = 0.0;
    stardata->common.orbit.eccentricity = 0.0;

    update_orbital_variables(stardata,
                             &stardata->common.orbit,
                             donor,
                             accretor);

    Dprint("force ndonor (star %d), star ndonor (%d) has mass %12.12e\n",
           donor->starnum,accretor->starnum,accretor->mass);
    Dprint("put nucsyn yields here? 4\n");

}
