#pragma once
#ifndef RLOF_PARAMETERS_H
#define RLOF_PARAMETERS_H

#include "RLOF_parameters.def"

#undef X
#define X(CODE) RLOF_METHOD_##CODE,
enum { RLOF_METHODS_LIST };
#undef X

#define X(CODE) RLOF_ECCENTRIC_##CODE,
enum { RLOF_ECCENTRIC_LIST };
#undef X

#define X(CODE) RLOF_RATE_##CODE,
enum { RLOF_RATE_METHODS_LIST };
#undef X

#define X(CODE) RLOF_INTERPOLATION_##CODE,
enum { RLOF_INTERPOLATION_METHODS_LIST };
#undef X

#define X(CODE,NUM,STRING) DONOR_RATE_ALGORITHM_##CODE = (NUM),
enum { RLOF_DONOR_RATE_ALGORITHMS_LIST };
#undef X

/*
 * QCRIT_ codes
 */
#define X(CODE,NUM,STRING) QCRIT_##CODE = (NUM),
enum { RLOF_QCRIT_LIST };
#undef X

/*
 * for backwards compatibility, also define QCRIT_GB_*
 */
#define X(CODE,NUM,STRING) QCRIT_GB_##CODE = (NUM),
enum { RLOF_QCRIT_LIST };
#undef X


/* special case */
#define QCRIT_GB_DEFAULT QCRIT_GB_BSE

#define X(CODE,STRING) RLOF_##CODE ,
enum { RLOF_CASES_LIST };
#undef X

#define X(CODE,STRING) STRING,
static char * RLOF_case_strings[] Maybe_unused = { RLOF_CASES_LIST };
#undef X

#define X(CODE,NUM) RLOF_##CODE = (NUM) ,
enum { RLOF_STABILITY_CASES_LIST };
#undef X

#define X(CODE,NUM) RLOF_NONCONSERVATIVE_GAMMA_##CODE = (NUM) ,
enum { RLOF_NONCONSERVATIVE_GAMMA_LIST };
#undef X

#define X(CODE) RLOF_ANGULAR_MOMENTUM_TRANSFER_MODEL_##CODE,
enum { RLOF_ANGULAR_MOMENTUM_TRANSFER_MODEL_LIST };
#undef X

enum { HACHISU_IGNORE_QCRIT = -1 };

#define X(CODE) RLOF_##CODE,
enum { RLOF_CONTROL_FLAGS_LIST };
#undef X

#define X(CODE) NONCONSERVATIVE_TYPE_##CODE,
enum { NONCONSERVATIVE_TYPES_LIST };
#undef X

#endif // RLOF_PARAMETERS_H
