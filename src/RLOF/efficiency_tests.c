#include "../binary_c.h"
No_empty_translation_unit_warning;

//#define EFFIENCY_TESTS

#ifdef EFFICIENCY_TESTS

void efficiency_test(struct stardata_t * stardata)
{
    /*
     * test the white dwarf accretion efficiency functions
     */
    double m2,lograte;

    for(m2 = 0.7; m2<1.4; m2+=0.1)
    {
        stardata->star[1].mass = m2;
        stardata->star[0].Xenv[XH1] = 0.7;
        for(lograte = -10; lograte < -5; lograte += 0.1)
        {
            double rate = exp10(lograte);
            double eff=white_dwarf_H_accretion_efficiency(rate,
                                                          &(stardata->star[0]),
                                                          &(stardata->star[1]),stardata);
            printf("WDRATE %g %g %g\n",lograte,m2,eff);
        }
        printf("WDRATE\nWDRATE\n");
    }
    Exit_binary_c(BINARY_C_NORMAL_EXIT,"");
}
#endif // EFFICIENCY_TESTS
