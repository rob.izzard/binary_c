#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "RLOF.h"
#include "RLOF_critical_q.h"

/*
 * Determine critical q (qc) for dynamical timescale RLOF.
 *
 * This is compared to q = mass(donor)/mass(accretor).
 *
 * If q > qc, mass transfer is unstable.
 *
 * Note that qc is set as a function of stellar type if it is zero or positive,
 * and by a given algorithm if negative (where the qc given defines
 * the algorithm to be used).
 */

double RLOF_critical_q(struct stardata_t * Restrict const stardata)
{
    /*
     * choose qc based on whether the accretor is degenerate or not
     */
    RLOF_stars;

    double qc = Qcrit;

    if(qc < -TINY)
    {
        /* use an algorithm for qcrit */

        const int algorithm = Map_float_algorithm(qc);

#undef X
#define X(CODE,NUM,STRING) STRING,
        static const char* qcrit_strings[] = { "None", RLOF_QCRIT_LIST };
#undef X
        Dprint("Mapped qcrit %g to algorithm %d \"%s\"\n",
               qc,
               algorithm,
               qcrit_strings[-algorithm]);

        switch(algorithm)
        {
        case QCRIT_BSE:
            /* Hurley et al 2002 */
            qc = qc_HG_BSE(stardata,donor);
            break;
        case QCRIT_HJELLMING_WEBBINK:
            /* Hjellming & Webbink, 1987, ApJ, 318, 794. */
            qc = 0.362 + 1.0/(3.0*envelope_mass(donor)/donor->mass);
            break;
        case QCRIT_Q_NO_COMENV:
            /* something huge to prevent any common envelope evolution */
            qc = 1e12;
            break;
        case QCRIT_CHEN_HAN_TABLE:
        case QCRIT_CHEN_HAN_FORMULA:
            /* Chen & Han 2008 */
            qc = chen_han_qc(stardata);
            break;
        case QCRIT_GE2015:
            /* Ge et al 2015 */
            qc = Ge_et_al_2015_qc(stardata,donor->mass, donor->radius);
            break;
        case QCRIT_VOS2018:
            /* Vos 2018 for RGB tip */
            qc = 1.0/Pow2(Outermost_core_mass(donor))-0.25*Outermost_core_mass(donor)-2.55;
            break;
        case QCRIT_TEMMINK2022:
            /* Temmink et al. 2022 */
            qc = Temmink_2022_qc(stardata,
                                 donor,
                                 accretor);
            break;
        default:
            Exit_binary_c(BINARY_C_SETUP_UNKNOWN_ARGUMENT,
                          "Unknown qcrit method %d\n",
                          stardata->preferences->qcrit_giant_branch_method);
        }
        Dprint("QCRIT method = %d : qc = %g\n",
               stardata->preferences->qcrit_giant_branch_method,
               qc);
    }

    /*
     * Special case:
     * Apply the Hachisu, Kato and Nomoto qcrit, if qcrit>0 and
     * if the accreting star is a white dwarf
     */
    if(stardata->preferences->hachisu_disk_wind==TRUE &&
       WHITE_DWARF(accretor->stellar_type) &&
       More_or_equal(stardata->preferences->hachisu_qcrit,0.0))
    {
        /*
         * Hachisu, Kato and Nomoto 1999 suggest qc=1.15, even if
         * the donor is a red giant. This is set by default
         * into stardata->preferences->hachisu_qcrit, but of
         * course you can change it.
         *
         * Their later paper suggests that perhaps this limit is too
         * low, and that the disk wind strips the giant: this leads
         * to a higher qc. Whatever... you choose.
         */
        qc = stardata->preferences->hachisu_qcrit;
        Dprint("Set qc = %g from Hachisu",qc);
    }
    else
    {
        Dprint("Set qc = %g from prefs->qc=%g prefs->qc_degenerate=%g, prescription %g, donor stellar_type=%d, degenerate accretor? %d\n",
               qc,

               stardata->preferences->qcrit[donor->stellar_type],

               stardata->preferences->qcrit_degenerate[donor->stellar_type],
               Qcrit_prescription,
               donor->stellar_type,
               COMPACT_OBJECT(accretor->stellar_type));
    }


    return qc;
}

/**********************************/
/* Critical q algorithm functions */
/**********************************/

static Pure_function double chen_han_qc(struct stardata_t * const stardata)
{

    /*
     * What to adopt for beta (mass transfer efficiency) on the first
     * timestep?
     *
     * If we have already had mass transfer, adopt its efficiency.
     * But if we haven't, use the value in CHEN_HAN_FIRST_TIMESTEP_BETA.
     *
     * If this is 1.0, we assume mass transfer start conservatively.
     * This is the most unstable, so you might want to start with
     * zero (Carlo suggests that we do this).
     */
    RLOF_stars;

    double beta = Is_not_zero(donor->derivative[DERIVATIVE_STELLAR_MASS_RLOF_LOSS]) ?
        -accretor->derivative[DERIVATIVE_STELLAR_MASS_RLOF_GAIN]/donor->derivative[DERIVATIVE_STELLAR_MASS_RLOF_LOSS] :
        CHEN_HAN_FIRST_TIMESTEP_BETA;
    double qc,qc0;

    if(stardata->preferences->qcrit_giant_branch_method==QCRIT_CHEN_HAN_TABLE)
    {
        double r=log10(donor->radius)-donor->A0;
        double M1=donor->phase_start_mass;
        double c0,c1,c2,c3,c11,c12,c13,c21,c22,c23,c31,c32,c33;
        double c4,c5,c41,c42,c43,c51,c52,c53;

        if(donor->stellar_type==GIANT_BRANCH)
        {
            /* Eqs. 3,4,5 */
            if(M1<2.0 && r>0.2)
            {
                c0=0.37;
                c11=1.4418;
                c12=-0.3477;
                c13=0.1180;
                c21=-0.08252;
                c22=0.4180;
                c23=-0.1873;
                c31=0.08967;
                c32=-0.3129;
                c33=0.1079;

                c1 = c11+c12*M1+c13*M1*M1; // Eq. 6
                c2 = c21+c22*M1+c23*M1*M1;
                c3 = c31+c32*M1+c33*M1*M1;

                qc0=c1+c2*r+c3*r*r; // Eq. 4
            }
            else
            {
                if(M1<2.0) // r<0.2 by definition
                {
                    c0=0.35;
                    c41=1.7141;
                    c42=-0.4919;
                    c43=0.1695;
                    c51=-0.3978;
                    c52=0.2620;
                    c53=-0.1101;
                }
                else if(M1<3.98)
                {
                    c0=0.35;
                    c41=0.9989;
                    c42=0.1755;
                    c43=0.003139;
                    c51=0.2740;
                    c52=-0.2353;
                    c53=-0.007348;
                }
                else
                {
                    c0=0.37;
                    c41=1.3840;
                    c42=0.1389;
                    c43=-0.0124;
                    c51=0.005596;
                    c52=-0.2602;
                    c53=0.02045;
                }

                c4 = c41+c42*M1+c43*M1*M1;
                c5 = c51+c52*M1+c53*M1*M1;

                qc0=c4+c5*cbrt(r); // Eq. 5
            }
        }
        else// if(donor->stellar_type==EAGB || donor->stellar_type==TPAGB)
        {
            if(M1<2.0)
            {
                c0=0.39;
                c11=0.02442;
                c12=7.0840;
                c13=-3.6961;
                c21=9.6761;
                c22=-19.0892;
                c23=7.9462;
                c31=-4.5199;
                c32=7.8189;
                c33=-3.1087;
            }
            else if(M1<2.51)
            {
                c0=0.34;
                c11=1.0462;
                c12=0.3115;
                c13=-0.03702;
                c21=-0.6179;
                c22=0.5193;
                c23=-0.2027;
                c31=0.7346;
                c32=-0.7568;
                c33=0.2074;
            }
            else if(M1<3.98)
            {
                c0=0.36;
                c11=-0.5270;
                c12=1.4289;
                c13=-0.2325;
                c21=7.1432;
                c22=-4.9954;
                c23=0.7625;
                c31=-4.9258;
                c32=3.2653;
                c33=-0.4966;
            }
            else
            {
                c0=0.36;
                c11=1.4681;
                c12=0.01922;
                c13=-0.004501;
                c21=0.1092;
                c22=-0.2844;
                c23=0.02368;
                c31=-0.1971;
                c32=0.1226;
                c33=-0.006051;
            }


            c1 = c11+c12*M1+c13*M1*M1;
            c2 = c21+c22*M1+c23*M1*M1;
            c3 = c31+c32*M1+c33*M1*M1;

            qc0 = c1 + c2*r + c3*r*r; /* Eq. 7 */
        }

        qc = qc0 - c0*beta; // Eq. 3

    }
    else
    {
        /* simpler fit: Eq. 9 */
        qc0 = 1.142+1.081*Outermost_core_mass(donor) -2.852*Pow2(Outermost_core_mass(donor));
        qc = qc0 - 0.35*beta;
    }
    return qc;
}


static double Ge_et_al_2015_qc(struct stardata_t * const stardata,
                               const double m,
                               const double r)
{

    Const_data_table table[TABLE_GE2015_LINES*3] = { TABLE_GE2015_DATA };
    double x[2]={log10(m),log10(r)};
    double y[1];
    rinterpolate(table,
                 stardata->persistent_data->rinterpolate_data,
                 2,
                 1,
                 TABLE_GE2015_LINES,
                 x,
                 y,
                 FALSE);
    return y[0];
}



static Pure_function double qc_HG_BSE(const struct stardata_t * const stardata,
                                      const struct star_t * const donor)
{
    double qc;
#ifdef BSE
    qc = (1.670-stardata->common.metallicity_parameters[7]+2.0*
          Pow5((donor->core_mass[CORE_He]/donor->mass)))/2.130;
#else
#define C0 3.040581e-01
#define C1 8.049509e-02
#define C2 8.967485e-02
#define C3 8.780198e-02
#define C4 2.219170e-02
    double lzs = log10(stardata->common.metallicity) - log10(0.02);
    double mp7 = C0 + lzs*(C1 + lzs*(C2 + lzs*(C3 + lzs*C4)));
    qc = (1.670 - mp7 + 2.0 *
          Pow5((donor->core_mass[CORE_He]/donor->mass)))/2.130;
#endif//BSE
    return qc;
}

static Pure_function double Temmink_2022_qc(struct stardata_t * const stardata Maybe_unused,
                                            struct star_t * const donor Maybe_unused,
                                            struct star_t * const accretor Maybe_unused)
{
    if(stardata->persistent_data->Temmink2022_RLOF_mapped == NULL)
    {
        Interpolate(
            stardata->store->Temmink2022_RLOF,
            NULL,
            NULL,
            FALSE
            );
        struct rinterpolate_table_t * const unmapped =
            rinterpolate_table_struct(
                stardata->persistent_data->rinterpolate_data,
                stardata->store->Temmink2022_RLOF->data,
                NULL
                );
        struct rinterpolate_table_t * const mapped =
            rinterpolate_map_right_columns(
                stardata->persistent_data->rinterpolate_data,
                unmapped,
                (rinterpolate_Boolean_t []) {FALSE,TRUE},
                (rinterpolate_float_t []) {0,100},
                NULL
                );
        mapped->auto_free_data = TRUE;

        stardata->persistent_data->Temmink2022_RLOF_mapped = NULL;
        NewDataTable_from_Pointer(mapped->data,
                                  stardata->persistent_data->Temmink2022_RLOF_mapped,
                                  mapped->n,
                                  mapped->d,
                                  mapped->l);

    }


    const double x[2] = {
        donor->mass,
        log10(donor->radius),
    };
    double r[6];
    Interpolate(
        stardata->persistent_data->Temmink2022_RLOF_mapped,
        x,
        r,
        FALSE);


    /*
     * Make Karel's Fig. 8
     */
    /*
    for(double m=1.0; m<=8.0; m+=0.1)
    {
        x[0] = m;
        for(double logR = 0.25; logR < 3.0; logR += 0.02)
        {
            x[1] = logR;
            Interpolate(
                stardata->persistent_data->Temmink2022_RLOF_mapped,
                x,
                r,
                FALSE
                );
            printf("XXX %g %g %g\n",
                   m,
                   logR,
                   r[0]);
        }
        printf("XXX\n\n");
    }
    Flexit;
    */

    /*
     * r = [q_crit, dqcrit/dq, q_RLOF, q_dyn, logMdot_crit]
     *
     * Karel gives q_crit where q = Maccretor/Mdonor
     *
     * binary_c wants Mdonor/Maccretor, so invert and return
     */
    return 1.0/r[0];
}
