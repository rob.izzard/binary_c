#include "../binary_c.h"
No_empty_translation_unit_warning;


double Pure_function RLOF_mass_transfer_rate(struct stardata_t * const stardata,
                                             struct star_t * const donor)
{
    double mdot = 0.0;
    if(stardata->preferences->eccentric_RLOF_model == RLOF_ECCENTRIC_AS_CIRCULAR)
    {
        /*
         * The system is treated as ciruclar, whether it is or not.
         * This is the same as the original BSE algorithm.
         */
        mdot = RLOF_instantaneous_mass_transfer_rate(
            donor->radius,
            effective_Roche_radius(stardata,donor),
            donor->mass,
            stardata,
            donor
            );
    }
    else if(stardata->preferences->eccentric_RLOF_model == RLOF_ECCENTRIC_NUMERICAL)
    {
        /*
         * Apply phase dependent mass transfer rate
         */
        mdot = RLOF_phase_dependent(stardata,
                                    donor);
    }

    return mdot;
}
