#include "../binary_c.h"
No_empty_translation_unit_warning;


double RLOF_phase_dependent(struct stardata_t * const stardata,
                            struct star_t * const donor)
{
    double mdot;

    /*
     * Todo!
     */

    mdot = RLOF_instantaneous_mass_transfer_rate(donor->radius,
                                                 effective_Roche_radius(stardata,donor),
                                                 donor->mass,
                                                 stardata,
                                                 donor);

    return mdot;
}
