#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "RLOF.h"

void RLOF_merger(struct stardata_t * Restrict stardata,
                 struct RLOF_orbit_t * RLOF_orbit)
{
    /*
     * Two stars (dynamically) merge during RLOF, either
     * because R>10RL or because they are two
     * main sequence stars with q>qc
     *
     * This is handled as an event when events are available.
     */
    RLOF_stars;

    const double donor_mass_was = donor->mass;
    const double accretor_mass_was = accretor->mass;
    mix_stars(stardata,
              TRUE,
              SOURCE_RLOF);
    RLOF_orbit->dM_RLOF_lose = -(donor_mass_was - donor->mass);
    RLOF_orbit->dM_RLOF_transfer = accretor->mass - accretor_mass_was;
    stardata->star[0].epoch = stardata->model.time - stardata->star[0].age;
    Dprint("Set epoch 0 %g\n",stardata->star[0].epoch);
    stardata->model.coalesce = TRUE;
}
