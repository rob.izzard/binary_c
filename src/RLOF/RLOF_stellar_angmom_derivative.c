#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "RLOF.h"

void RLOF_stellar_angmom_derivative(struct stardata_t * Restrict const stardata)
{
    RLOF_stars;

    /*
     * Calculate stellar angular momentum derivatives because of
     * RLOF mass transfer.
     */

    /*
     * Remove angular momentum from the donor and add it
     * to the orbit.
     */
    double dj_dt_from_donor =
        donor->derivative[DERIVATIVE_STELLAR_MASS_RLOF_LOSS] *
        Pow2(donor->effective_radius)*donor->omega;

    /*
     * Prevent the star spinning down below 0 : this can
     * happen during RLOF and would be dealt with by shrinking
     * the timestep, but it's usually not a problem to simply
     * cap the rate here.
     */
    dj_dt_from_donor = Min(donor->angular_momentum/stardata->model.dt,
                           dj_dt_from_donor);

    donor->derivative[DERIVATIVE_STELLAR_ANGMOM_RLOF_LOSS] += dj_dt_from_donor;
    stardata->model.derivative[DERIVATIVE_ORBIT_ANGMOM_RLOF_LOSS] -= dj_dt_from_donor;
    Dprint("jdot (RLOF stream) from donor -= %30.20e (RLOF, Jdonor = %g, would be %g)\n",
           dj_dt_from_donor,
           donor->angular_momentum,
           donor->angular_momentum + stardata->model.dt * dj_dt_from_donor
        );

    /*
     * Determine maximum accreted angular momentum depending on whether
     * there is a disk or not and whether there are novae or not.
     * This quantity is positive.
     */
    double dj_dt_onto_accretor;

    if(stardata->preferences->RLOF_angular_momentum_transfer_model==
       RLOF_ANGULAR_MOMENTUM_TRANSFER_MODEL_BSE)
    {
        const double r_acc = accretion_radius(stardata,accretor);
        dj_dt_onto_accretor =
            donor->derivative[DERIVATIVE_STELLAR_MASS_RLOF_TRANSFER]
            * TWOPI * AU_IN_SOLAR_RADII * sqrt(AU_IN_SOLAR_RADII * r_acc * accretor->mass);
    }
    else if(stardata->preferences->RLOF_angular_momentum_transfer_model==
            RLOF_ANGULAR_MOMENTUM_TRANSFER_MODEL_CONSERVATIVE)
    {
        /*
         * Simple conservative model of angular momentum transfer
         */
        dj_dt_onto_accretor = -dj_dt_from_donor;
    }
    else
    {
        /* fail */
        Exit_binary_c(BINARY_C_WRONG_ARGUMENT,"Unknown RLOF angular momentum transfer model\n");
    }

    Dprint("RLOF ang mom ratio : donated/accreted = %g/%g = %g\n",
           dj_dt_from_donor,
           dj_dt_onto_accretor,
           Is_zero(dj_dt_onto_accretor) ? 0.0 :
           (dj_dt_from_donor/dj_dt_onto_accretor));

    /*
     * Save the specific angular momentum in case we later
     * wish to remove mass
     */
    accretor->l_acc =
        Is_zero(donor->derivative[DERIVATIVE_STELLAR_MASS_RLOF_TRANSFER]) ? 0.0 :
        dj_dt_onto_accretor/ donor->derivative[DERIVATIVE_STELLAR_MASS_RLOF_TRANSFER];

    Dprint("DJT Accrete : (accretor=%d) djt2=%30.20e (donor->derivative[DERIVATIVE_STELLAR_MASS_RLOF_TRANSFER]=%30.20e) l_acc=%30.20e\n",
           accretor->starnum,
           dj_dt_onto_accretor,
           donor->derivative[DERIVATIVE_STELLAR_MASS_RLOF_TRANSFER],
           accretor->l_acc
        );

    /*
     * Take angular momentum from the orbit and accrete it
     * onto the accretor
     */
    stardata->model.derivative[DERIVATIVE_ORBIT_ANGMOM_RLOF_GAIN] -= dj_dt_onto_accretor;
    accretor->derivative[DERIVATIVE_STELLAR_ANGMOM_RLOF_GAIN] += dj_dt_onto_accretor;

    Dprint("jdot (RLOF onto accretor) += %30.20e (RLOF)\n",dj_dt_onto_accretor);

}
