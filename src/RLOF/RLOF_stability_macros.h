#pragma once
#ifndef RLOF_STABILITY_MACROS_H
#define RLOF_STABILITY_MACROS_H



/*
 * Qcrit : there are two sets of values, choose which
 * to access depending on whether the accretor is degenerate or not.
 * Then, choose which to use based on the stellar type of the donor.
 */
#define Qcrit                                           \
    __extension__                                       \
    ({                                                  \
        double _qc = Qcrit_prescription;                \
        if(_qc < -TINY &&                               \
           Map_float_algorithm(_qc) == QCRIT_DEFAULT)   \
        {                                               \
            _qc = Qcrit_default;                        \
        }                                               \
        _qc;                                            \
    })

#define Qcrit_prescription                                          \
    (COMPACT_OBJECT(accretor->stellar_type) ?                       \
     stardata->preferences->qcrit_degenerate[donor->stellar_type] : \
     stardata->preferences->qcrit[donor->stellar_type])

#define Qcrit_default                                                   \
    (COMPACT_OBJECT(accretor->stellar_type) ?                           \
     stardata->tmpstore->default_preferences->qcrit_degenerate[donor->stellar_type] : \
     stardata->tmpstore->default_preferences->qcrit[donor->stellar_type])

/*
 * Tests for common envelope and/or dynamical mass transfer
 */
#define COMENV_GIANT_STAR (ON_EITHER_GIANT_BRANCH(donor->stellar_type) || \
                           donor->stellar_type==HeHG)

#define COMENV_SUBGIANT_STAR (donor->stellar_type==HERTZSPRUNG_GAP ||   \
                              donor->stellar_type==CHeB)

#define DYNAMICAL_TESTQ (donor->q > qc)

#define COMENV_TESTR (Less_or_equal(donor->effective_radius,    \
                                    donor->core_radius))

#ifdef DISCS
#define COMENV_TEST_DISC (Is_zero(stardata->common.discs[0].M))
#else
#define COMENV_TEST_DISC 1
#endif

#ifdef POST_CEE_WIND_ONLY
#define COMENV_TEST_MENV                                        \
    (envelope_mass(donor) >                                     \
     Max(MIN_MENV_FOR_CEE,                                      \
         stardata->preferences->minimum_donor_menv_for_comenv))
#else
#define COMENV_TEST_MENV                                    \
    (envelope_mass(donor) >                                 \
     stardata->preferences->minimum_donor_menv_for_comenv)
#endif // POST_CEE_WIND_ONLY

#define GIANT_BRANCH_COMENV_TEST                                \
    (((COMENV_GIANT_STAR && (DYNAMICAL_TESTQ || COMENV_TESTR))  \
      || (COMENV_SUBGIANT_STAR && DYNAMICAL_TESTQ))             \
     && COMENV_TEST_MENV &&                                     \
     COMENV_TEST_DISC)

/*

  #warning This 0.628 should not be hard coded
  #define WHITE_DWARF_COMENV_TEST (WHITE_DWARF(donor->stellar_type) &&    \
  donor->q > 0.628 &&                                                   \
  stardata->star[naccretor].stellar_type>HeGB)

  #warning This 0.695 should not be hard coded
  #define LOW_MASS_MS_COMENV_TEST (donor->stellar_type==LOW_MASS_MS &&    \
  donor->q > 0.695)
*/

#define WHITE_DWARF_DYNAMICAL_TEST                  \
    (WHITE_DWARF(donor->stellar_type) &&            \
     stardata->star[naccretor].stellar_type>HeGB && \
     DYNAMICAL_TESTQ)

#define LOW_MASS_MS_COMENV_TEST                 \
    (donor->stellar_type==LOW_MASS_MS &&        \
     DYNAMICAL_TESTQ)



#include "RLOF_stability_macros.def"
#undef X
#define X(REASON,STRING) REASON,
enum{
    CONTACT_REASONS_LIST
};
#undef X

#endif //RLOF_STABILITY_MACROS_H
