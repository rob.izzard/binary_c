#include "../binary_c.h"
No_empty_translation_unit_warning;

struct data_table_analysis_t *
analyse_data_table(struct stardata_t * const stardata Maybe_unused,
                   struct data_table_t * const table)
{
    /*
     * Given a data table, analyse it
     */
    struct data_table_analysis_t * analysis =
        Calloc(1,sizeof(struct data_table_analysis_t));
    analysis->skip = Calloc(table->nparam,sizeof(int));

    /*
     * Detect parameter skip counts
     */

    /* first copy in first line of parameters */
    double * current = Malloc(sizeof(double)*table->nparam);
    memcpy(current,table->data,sizeof(double)*table->nparam);
    double * linedata = table->data;

    for(size_t l=0; l<table->nlines; l++)
    {
        for(size_t c=0;c<table->nparam;c++)
        {
            if(analysis->skip[c]==0 &&
               !Fequal(linedata[c],current[c]))
            {
                /* changed */
                analysis->skip[c] = l;
            }
        }
        table->data += table->nparam + table->ndata;
    }

    return analysis;
}
