#include "../binary_c.h"
No_empty_translation_unit_warning;

/*
 * Set all values in a data table column
 */

void set_data_table_column(struct data_table_t * const table,
                           const int ncol,
                           const double value)
{
    const size_t width = table->nparam + table->ndata;
    for(size_t i=0;i<table->nlines;i++)
    {
        *(table->data + i*width + ncol) = value;
    }
}
