#include "../binary_c.h"
No_empty_translation_unit_warning;
struct pair_t {
    struct data_table_t * table;
    double * values;
    size_t index;
};


#define _CHECK                                                          \
    if(0)                                                               \
    {                                                                   \
        printf("combine line %d column_is_mapped : ",__LINE__);         \
        for(unsigned int _i=0; _i<stardata->persistent_data->rinterpolate_data->number_of_interpolation_tables; _i++) \
        {                                                               \
            printf("%u %p = %p, ",                                      \
                   _i,                                                  \
                   (void*)stardata->persistent_data->rinterpolate_data->tables[_i], \
                   (void*)stardata->persistent_data->rinterpolate_data->tables[_i]->column_is_mapped); \
        }                                                               \
        printf("\n");                                                   \
    }

struct data_table_t * combine_data_tables(struct stardata_t * const stardata,
                                          struct data_table_t ** const tables,
                                          double ** const leftparams,
                                          const size_t ntables,
                                          const size_t n_leftparams)
{
    /*
     * Combine a list of data tables, each with a
     * list of parameters to label them in the new leftmost
     * column.
     *
     * The new table looks like:
     *
     * left_params[0][0] leftparams[0][1] ... leftparams[0][n_leftparams -1] <data line 1>
     * left_params[1][0] leftparams[1][1] ... leftparams[2][n_leftparams -1] <data line 1>
     * ...
     *
     * The table is sorted by its parameters on exit.
     */
    _CHECK;
    if(ntables == 0 || n_leftparams == 0 || tables == NULL || leftparams == NULL)
    {
        return NULL;
    }
    _CHECK;

    /*
     * Check all tables have the same number of parameters and
     * data items.
     */
    size_t nparam = 0;
    size_t ndata = 0;
    size_t nlines = 0;
    char * combined_label = NULL;
    for(size_t i=0; i<ntables; i++)
    {
        struct data_table_t * const table = tables[i];
        if(ndata==0)
        {
            /*
             * First time: save ndata and nparam
             */
            ndata = table->ndata;
            nparam = table->nparam;
        }
        else
        {
            /*
             * Check this table has the same ndata and
             * nparam as previous tables. If not, we cannot
             * combine the data.
             */
            if(table->ndata != ndata ||
               table->nparam != nparam)
            {
                Exit_binary_c(BINARY_C_ALGORITHM_BRANCH_FAILURE,
                              "cannot combine tables with differeing nparam,ndata : nparam=%zu ndata=%zu but these do not match previous table which had nparam=%zu ndata=%zu\n",
                              table->nparam,
                              table->ndata,
                              nparam,
                              ndata);
            }
        }

        if(DATA_TABLE_LABELS)
        {
            if(combined_label != NULL)
            {
                char * newlabel = NULL;
                if(asprintf(&newlabel,
                            "%s + %s",
                            combined_label,
                            table->label)>0)
                {
                    Safe_free(combined_label);
                    combined_label = newlabel;
                }
            }
            else
            {
                combined_label = strdup(table->label);
            }
        }
        else
        {
            combined_label = NULL;
        }

        nlines += table->nlines;
    }
    _CHECK;

    /*
     * New data space
     */
    double * const newdata = Calloc(sizeof(double) * nlines, (nparam + n_leftparams + ndata));
    size_t newline = 0;
    const size_t old_size = nparam + ndata;
    const size_t new_size = old_size + n_leftparams;

    for(size_t i=0; i<ntables; i++)
    {
        double * const olddata = tables[i]->data;
        for(size_t l=0; l<tables[i]->nlines; l++)
        {
            double * const p = newdata + newline * new_size;
            memcpy(p,
                   leftparams[i],
                   sizeof(double) * n_leftparams);
            memcpy(p + n_leftparams,
                   olddata + l * old_size,
                   sizeof(double) * old_size);
            newline++;
        }
    }
    _CHECK;

    /*
     * Hence new table
     */
    struct data_table_t * newtable = NULL;
    NewDataTable_from_Pointer(newdata, newtable, nparam + n_leftparams, ndata, nlines);
    Safe_free(newtable->label);
    newtable->label = combined_label;
    _CHECK;

    /*
     * Sort and return
     */
    struct data_table_t * const sorted_table = sort_data_table(newtable);
    Delete_data_table(newtable);
    _CHECK;
    return sorted_table;
}
