#include "../binary_c.h"
No_empty_translation_unit_warning;

struct data_table_t * join_data_tables(struct stardata_t * const stardata,
                                       struct data_table_t * const table1,
                                       struct data_table_t * const table2,
                                       const size_t offset1,
                                       const size_t step1,
                                       const size_t offset2,
                                       const size_t step2)
{
    /*
     * Join table1 and table2, both should have the
     * same nparam and ndata, return the new table.
     *
     * Start copying at given offset lines in each table,
     * with a given step.
     */
    const size_t nparam = Max(table1->nparam, table2->nparam);
    const size_t ndata = Max(table1->ndata, table2->ndata);
    const size_t width1 = table1->ndata + table1->nparam;
    const size_t width2 = table2->ndata + table2->nparam;
    const size_t width = Max(width1,width2);
    const size_t swidth = width * sizeof(double);
    const size_t size = swidth * (table1->nlines + table2->nlines);
    if(unlikely(size) == 0)
    {
        return NULL;
    }
    else
    {
        struct data_table_t * newtable;
        New_data_table(newtable);
        double * const newdata = Malloc(size);
        if(newdata == NULL)
        {
            Exit_binary_c(BINARY_C_ALLOC_FAILED,
                          "Could not malloc data in join_data_tables : wanted size %zu\n",
                          size);
        }

        if(step1 == 1 &&
           step2 == 1 &&
           table1->nparam == table2->nparam &&
           table1->ndata == table2->ndata)
        {
            /*
             * Want all data lines, with some offset.
             * Tables are identical in number of parameters and data.
             */
            memcpy(newdata,
                   table1->data + offset1 * width,
                   (table1->nlines - offset1) * width * sizeof(double));
            memcpy(newdata + (table1->nlines - offset1) * width,
                   table2->data + offset2  *width,
                   (table2->nlines - offset2) * width * sizeof(double));
            newtable->nlines = table1->nlines + table2->nlines;
        }
        else
        {
            newtable->nlines = 0;
            const size_t sparam1 = table1->nparam * sizeof(double);
            const size_t sdata1 = table1->ndata * sizeof(double);
            for(size_t i = offset1; i<table1->nlines; i+=step1)
            {
                double * const dst = newdata + newtable->nlines * width;
                double * const src = table1->data + i * width1;
                memset(dst,0,width);
                memcpy(dst,
                       src,
                       sparam1);
                memcpy(dst + nparam,
                       src + table1->nparam,
                       sdata1);
                newtable->nlines++;
            }
            const size_t sparam2 = table2->nparam * sizeof(double);
            const size_t sdata2 = table2->ndata * sizeof(double);
            for(size_t i = offset2; i<table2->nlines; i+=step2)
            {
                double * const dst = newdata + newtable->nlines * width;
                double * const src = table2->data + i * width2;
                memset(dst,0,width);
                memcpy(dst,
                       src,
                       sparam2);
                memcpy(dst + nparam,
                       src + table2->nparam,
                       sdata2);
                newtable->nlines++;
            }
        }

        newtable->data = newdata;
        newtable->nparam = nparam;
        newtable->ndata = ndata;
        return newtable;
    }
}
