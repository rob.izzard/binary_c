#include "../binary_c.h"
No_empty_translation_unit_warning;

/*
 * Free a data table, also freeing its metadata
 * using that metadata's free_function if this
 * is defined.
 *
 * If delete_contents is TRUE, delete the data in the table.
 *
 * If delete_metadata is TRUE, delete the table's metadata.
 */

void free_data_table(struct data_table_t ** data_table_p,
                     const Boolean delete_contents,
                     const Boolean delete_metadata,
                     const Boolean delete_analysis)
{
    if(data_table_p != NULL)
    {
        struct data_table_t * const data_table = * data_table_p;
        if(data_table != NULL)
        {
            if(delete_analysis == TRUE &&
               data_table->analysis != NULL)
            {
                if(data_table->analysis != NULL &&
                   data_table->analysis->values != NULL)
                {
                    for(size_t i=0; i<data_table->analysis->nparameters; i++)
                    {
                        Safe_free(data_table->analysis->values[i]);
                    }
                    Safe_free(data_table->analysis->values);
                }
                Safe_free(data_table->analysis->nvalues);
                Safe_free(data_table->analysis->data_line_offset_bytes);
                Safe_free(data_table->analysis->skip);
                Safe_free(data_table->analysis);
            }
            if(delete_contents == TRUE)
            {
                Safe_free(data_table->data);
            }
            if(delete_metadata == TRUE &&
               data_table->metadata != NULL)
            {
                if(data_table->metadata_free_function != NULL)
                {
                    data_table->metadata_free_function(&data_table->metadata);
                }
                Safe_free(data_table->metadata);
            }
            Safe_free(data_table->label);
        }
        Safe_free_nocheck(*data_table_p);
    }
}
