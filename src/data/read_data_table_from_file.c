#include "../binary_c.h"
No_empty_translation_unit_warning;

#define _fread(...)                                                     \
    if(fread(__VA_ARGS__)==0 && ferror(fp))                             \
    {                                                                   \
        Exit_binary_c(BINARY_C_FILE_READ_ERROR,                         \
                      "Failed to read any data in read_data_table_from_file, trying to read from \"%s\"\n", \
                      bfp->path);                                       \
    }

struct data_table_t * read_data_table_from_file(struct stardata_t * const stardata,
                                                struct binary_c_file_t * bfp)
{
    /*
     * Read from file pointer fp into a table
     * which we return.
     */
    FILE * fp = bfp->fp;
    struct data_table_t * table;
    size_t nparam;
    _fread(&nparam,sizeof(size_t),1,fp);
    if(nparam > 0)
    {
        New_data_table(table);
        table->nparam = nparam;
        _fread(&table->ndata,sizeof(size_t),1,fp);
        _fread(&table->nlines,sizeof(size_t),1,fp);
        size_t label_length;
        _fread(&label_length,sizeof(size_t),1,fp);
        fflush(NULL);
        if(label_length > 0)
        {
           table->label = Malloc(sizeof(char)*label_length);
            _fread(table->label,sizeof(char),label_length,fp);
        }
        const size_t data_size = (table->nparam + table->ndata) * table->nlines;
        table->data = Malloc(sizeof(double)*data_size);
        _fread(table->data,sizeof(double),data_size,fp);
    }
    else
    {
        table = NULL;
    }
    return table;
}
