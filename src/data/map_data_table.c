#include "../binary_c.h"
No_empty_translation_unit_warning;
#undef DEBUG
#define DEBUG 1
struct data_table_t * map_data_table(struct stardata_t * const stardata,
                                     struct data_table_t ** unmapped_table_p,
                                     int * wanted_columns,
                                     int * const new_resolutions,
                                     double ** const resolution_list,
                                     int n_wanted_columns)
{
    /*
     * Given a data_table_t remap it using rinterpolate's
     * functionality.
     *
     * unmapped_table : a pointer to a data_table_t struct
     *                  pointer containing the unmapped data.
     *
     * wanted_columns : an array of integers which are the
     *                  columns to be selected from the data.
     *                  If NULL or first column is -1, use all
     *                  columns.
     *
     * new_resolutions : an array of integers specifying
     *                   the new resolution of tabular parameters, or NULL.
     *
     * resolution_list : an array of arrays of points (between 0 and 1) to
     *                   act as mapping locations, or NULL.
     *
     * Returns a pointer to the mapped table struct.
     */

    /*
     * Reduce to columns we want
     */
    struct data_table_t * reduced_table = reduce_data_table_columns(stardata,
                                                                    *unmapped_table_p,
                                                                    wanted_columns,
                                                                    n_wanted_columns);

    /*
     * Make rinterpolate_table version
     */
    printf("reduced version %p %p %p %p\n",
           (void*)stardata->persistent_data,
           stardata->persistent_data ? (void*)stardata->persistent_data->rinterpolate_data : NULL,
           (void*)reduced_table,
           reduced_table ? (void*)reduced_table->data : NULL);
    struct rinterpolate_table_t * reduced_rinterpolate_table =
        rinterpolate_table_struct(stardata->persistent_data->rinterpolate_data,
                                  reduced_table->data,
                                  NULL);
    Boolean made_new_rinterpolate_table;
    if(reduced_rinterpolate_table == NULL)
    {
        /*
         * make rinterpolate version of the table
         */
        Dprint("call Interpolate to set up the table\n");
        Interpolate(reduced_table,
                    NULL,
                    NULL,
                    FALSE);

        /*
         * extract correct rinterpolate table
         */
        Dprint("get rinterpolate table struct pointer\n");
        reduced_rinterpolate_table =
            rinterpolate_table_struct(stardata->persistent_data->rinterpolate_data,
                                      reduced_table->data,
                                      NULL);
        made_new_rinterpolate_table = TRUE;
    }
    else
    {
        made_new_rinterpolate_table = FALSE;
    }

    /*
     * construct new resolution array in appropriate way
     * for librinterpolate
     */
    Dprint("make new resolution array\n");
    rinterpolate_Boolean_t * change_resolution = Malloc(sizeof(rinterpolate_Boolean_t)*reduced_table->nparam);
    rinterpolate_float_t * new_resolutions_f =
        new_resolutions != NULL ?
        Malloc(sizeof(rinterpolate_float_t)*reduced_table->nparam) :
        NULL;
    for(size_t j=0; j<reduced_table->nparam; j++)
    {
        if(new_resolutions != NULL)
        {
            new_resolutions_f[j] = (rinterpolate_float_t) new_resolutions[j];
        }
        change_resolution[j] =
            (new_resolutions != NULL && new_resolutions[j]) > 0 ? TRUE :
            (resolution_list != NULL && resolution_list[j] != NULL) ? TRUE :
            FALSE;
        Dprint("col %zu : change resolution %s to %g, or use list? %s\n",
               j,
               Yesno(change_resolution[j]),
               new_resolutions != NULL ? new_resolutions_f[j] : -1,
               Yesno(resolution_list != NULL && resolution_list[j] != NULL));
    }


    /*
     * map to orthogonal table
     */
    Dprint("map to orthogonal table\n");
    struct rinterpolate_table_t * const orthogonal_table =
        rinterpolate_map_right_columns(stardata->persistent_data->rinterpolate_data,
                                       reduced_rinterpolate_table,
                                       change_resolution,
                                       new_resolutions_f,
                                       resolution_list);

    /*
     * set orthogonal table and its parameters
     */
    Dprint("set table and its parameters\n");
    struct data_table_t * mapped_table;
    New_data_table(mapped_table);

    if(mapped_table)
    {
        NewDataTable_from_Pointer(orthogonal_table->data,
                                  mapped_table,
                                  orthogonal_table->n,
                                  orthogonal_table->d,
                                  orthogonal_table->l);


        if(made_new_rinterpolate_table == TRUE)
        {
            /*
             * clean original non-orthogonal table (no longer required)
             */
            Dprint("delete unmapped table\n");
            rinterpolate_delete_table(stardata->persistent_data->rinterpolate_data,
                                      reduced_rinterpolate_table,
                                      NULL);
            Safe_free(reduced_rinterpolate_table->label);
            Safe_free(reduced_rinterpolate_table);
        }

        Dprint("free locally allocated memory\n");
        Safe_free(change_resolution);
        Safe_free(new_resolutions_f);
        Dprint("loaded and remapped\n");
    }

    Delete_data_table(reduced_table);

    return mapped_table;
}
