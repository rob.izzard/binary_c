#include "../binary_c.h"
No_empty_translation_unit_warning;

struct data_table_t * prepend_left_value(struct stardata_t * const stardata Maybe_unused,
                                         struct data_table_t * const table_in,
                                         const double left_value)
{
    /*
     * Given a table passed in, make a new table that is
     * identical but with left_value prepended to each line
     */

    const size_t old_size = table_in->nparam + table_in->ndata;
    const size_t new_size = old_size + 1;
    double * const olddata = table_in->data;
    double * const newdata = Malloc(sizeof(double) * new_size * table_in->nlines);

    for(size_t l=0; l<table_in->nlines; l++)
    {
        const size_t offset = l * new_size;
        *(newdata + offset + 0) = left_value;
        memcpy(newdata + offset + 1,
               olddata + l * old_size,
               sizeof(double) * old_size);
    }

    struct data_table_t * table = NULL;
    NewDataTable_from_Pointer(newdata,
                              table,
                              table_in->nparam + 1,
                              table_in->ndata,
                              table_in->nlines);
    return table;
}
