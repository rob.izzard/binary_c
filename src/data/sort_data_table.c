#include "../binary_c.h"
No_empty_translation_unit_warning;
#include <stdlib.h>

#ifdef __HAVE_GNU_QSORT_R
static int __cmpfunc(const void * const a,
                     const void * const b,
                     void * const arg);
#endif
#ifdef __HAVE_BSD_QSORT_R
static int __cmpfunc(void * const arg,
                     const void * const a,
                     const void * const b);
#endif

struct data_table_t * sort_data_table(struct data_table_t * const in)
{
    /*
     * Function to sort the table (in) and return a pointer
     * to the sorted table (out).
     *
     * The parameters in the table are sorted from
     * low to high value.
     */

    const size_t nd = in->ndata + in->nparam;
    const size_t size = (size_t)nd * sizeof(double);

    /*
     * Make a new table which is a copy of in
     */
    struct data_table_t * out = NULL;
    Copy_data_table(in, out);

    /*
     * Sort using qsort
     */
    qsort_r(out->data,
            out->nlines,
            size,
#ifdef __HAVE_GNU_QSORT_R
            (comparison_fn_r)__cmpfunc,
            (void*)in
#endif
#ifdef  __HAVE_BSD_QSORT_R
            (void*)in,
            (comparison_fn_r)__cmpfunc
#endif
        );

    return out;
}


/*
 * Line comparison function for qsort_r
 */
#ifdef __HAVE_GNU_QSORT_R
static int __cmpfunc(const void * const a,
                     const void * const b,
                     void * const arg)
#endif
#ifdef  __HAVE_BSD_QSORT_R
static int __cmpfunc(void * const arg,
                     const void * const a,
                     const void * const b)
#endif
{
    const struct data_table_t * const table =
        (const struct data_table_t * const) arg;
    const rinterpolate_float_t * const xa = (double const *)a;
    const rinterpolate_float_t * const xb = (double const *)b;

    /* xa and xb are two lines in the table, with n parameters to compare */

    /* prefetch is marginally faster */
    prefetch(xa);
    prefetch(xb);

    size_t i = 0;
    while(i<table->nparam)
    {
        const double x = xa[i];
        const double y = xb[i];
        if(unlikely(Fequal(x,y)))
        {
            ++i;
        }
        else
        {
            return x > y ? 1 : -1;
        }
    }
    return 0; /* fallback */
}
