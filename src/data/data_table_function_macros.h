#pragma once
#ifndef DATA_TABLE_FUNCTION_MACROS_H
#define DATA_TABLE_FUNCTION_MACROS_H


/************************************************************
 * Data table function macros
 ************************************************************/

/* verbosity flag */
#define DTVB 0

/* turn on table labels? */
#define DATA_TABLE_LABELS 0

/*
 * Functions to count the number of items in, and
 * define a new, data table.
 * TARGET is an (empty) pointer which is Malloced to the
 * appropriate size.
 */
#define DataTableN(T) (sizeof(T)/sizeof(double))
#define DataTableNLines(T,P,D) (sizeof(T)/(sizeof(double)*((P)+(D))))

/*
 * Data table line length
 * (in number of doubles)
 */
#define Data_table_line_length(T) ((T)->nlines*((T)->ndata+(T)->nparam))

/*
 * Size of the data footprint in a data table
 */
#define Data_table_data_size(T) ((size_t)(sizeof(double)*Data_table_line_length(T)))

/*
 * Pointers to the nth line's data and parameters
 */
#define Data_table_pointer_to_line_n_parameters(T,N)    \
    ((T)->data + (N) * Data_table_line_length(T))
#define Data_table_pointer_to_line_n_data(T,N)                  \
    ((T)->data + (N) * Data_table_line_length(T) + (T)->nparam)

/*
 * Pointers to the first and last lines of a data table
 */
#define Data_table_first_line_parameters_pointer(T) \
    Data_table_pointer_to_line_n_parameters((T),0)
#define Data_table_first_line_data_pointer(T)   \
    Data_table_pointer_to_line_n_data((T),0)
#define Data_table_last_line_parameters_pointer(T) \
    Data_table_pointer_to_line_n_parameters((T),(T)->nlines-1)
#define Data_table_last_line_data_pointer(T)   \
    Data_table_pointer_to_line_n_data((T),(T)->nlines-1)


/*
 * Make a new data table at TARGET with original data at either
 *
 * POINTER (for NewDataTable_from_Pointer which sets from a pre-allocated memory location) or
 * or
 * ARRAY (for NewDataTable_from_Array, which sets from a static array)
 * new table is an empty struct data_table_t pointer at TARGET,
 * with NDATA data items and NPARAM parameters on each
 * of NLINES lines of table.
 *
 * Usually the TARGET is a NULL pointer, but could (in principle)
 * already exist, in which case the Malloc is skipped.
 */

#define NewDataTable_from_Pointer(POINTER,                              \
                                  TARGET,                               \
                                  NPARAM,                               \
                                  NDATA,                                \
                                  NLINES)                               \
    {                                                                   \
        (TARGET) = (TARGET==NULL) ?                                     \
            ((struct data_table_t *)                                    \
             Calloc(1,sizeof(struct data_table_t))) : (TARGET);         \
        (TARGET)->data = (POINTER);                                     \
        (TARGET)->nlines = (size_t) (NLINES);                           \
        (TARGET)->ndata = (size_t) (NDATA);                             \
        (TARGET)->nparam = (size_t) (NPARAM);                           \
        (TARGET)->metadata = NULL;                                      \
        (TARGET)->metadata_free_function = NULL;                        \
        if(DATA_TABLE_LABELS)                                           \
        {                                                               \
            if(asprintf(&(TARGET)->label,                               \
                        "%s():%d",                                      \
                        __func__,                                       \
                        __LINE__)>0){};                                 \
        }                                                               \
        else                                                            \
        {                                                               \
            (TARGET)->label = NULL;                                     \
        }                                                               \
        if(DTVB)                                                        \
            fprintf(stderr,                                             \
                    "Alloc table at %p size %ld nparam %zu ndata %zu nlines %zu\n", \
                    (void*)(TARGET),                                    \
                    (long int)sizeof((TARGET)->data),                   \
                    (size_t)(TARGET)->nparam,                           \
                    (size_t)(TARGET)->ndata,                            \
                    (size_t)(TARGET)->nlines                            \
                );                                                      \
    }


/*
 * This version requires _C99.
 * Do not try to shrink this to a one-line version that calls the pointer
 * macro. This is technically possible, but the ARRAY_DATA is expanded in the process
 * which is not what you want.
 *
 * Note: beware shadowing of the variable __p so
 *       don't nest this macro.
 */
#define NewDataTable_from_Array_C99(ARRAY_DATA, \
                                    TARGET,     \
                                    NPARAM,     \
                                    NDATA,      \
                                    NLINES)     \
    {                                                                   \
        double * __p = (double[]){ ARRAY_DATA };    \
        if(DTVB)fprintf(stderr,"__p = %p allocd %ld\n",                 \
                        (void*)(Concat3(__p,                            \
                                        LABEL,                          \
                                        LINE)),                         \
                        (long int)Malloc_usable_size(Concat3(__p,       \
                                                             LABEL,     \
                                                             LINE)));   \
        NewDataTable_from_Pointer(__p,                                  \
                                  (TARGET),                             \
                                  (NPARAM),                             \
                                  (NDATA),                              \
                                  (NLINES));                            \
    }


/*
 * This version does not require _C99, but does require an
 * extra memcpy and hence probably more RAM
 *
 * Note: beware shadowing of the variable __p so
 *       don't nest this macro.
 */
#define NewDataTable_from_Array(ARRAY_DATA,                         \
                                TARGET,                             \
                                NPARAM,                             \
                                NDATA,                              \
                                NLINES)                             \
    {                                                               \
        if(DTVB)fprintf(stderr,"cast [%d]\n",                       \
                        ((NPARAM)+(NDATA))*(NLINES));               \
        const double __newdata[((NPARAM)+(NDATA))*(NLINES)] =       \
            { ARRAY_DATA };                                         \
        if(DTVB)fprintf(stderr,"alloc size %ld\n",                  \
                        (long int)sizeof(__newdata));               \
        double * __p =                                              \
            (double*) Calloc(1,sizeof(__newdata));                  \
        if(DTVB)fprintf(stderr,"__p = %p allocd %ld\n",             \
                        (void*)__p,                                 \
                        (long int)Malloc_usable_size(__p));         \
        if(DTVB)fprintf(stderr,"copy %p to %p, size %ld\n",         \
                        (void*)__newdata,                           \
                        (void*)__p,                                 \
                        (long int)sizeof(__newdata));               \
        memcpy(__p,                                                 \
               __newdata,                                           \
               sizeof(__newdata));                                  \
        if(DTVB)fprintf(stderr,"Allocate from pointer %d %d %d\n",  \
                        NPARAM,NDATA,NLINES);                       \
        NewDataTable_from_Pointer(__p,                              \
                                  (TARGET),                         \
                                  (NPARAM),                         \
                                  (NDATA),                          \
                                  (NLINES)                          \
            );                                                      \
    }


#define ShowDataTable(TABLE)                    \
    ShowDataTable_implementation(TABLE,         \
                                 ShowDataTable, \
                                 __COUNTER__)
#define ShowDataTable_implementation(TABLE,LABEL,LINE)                  \
    {                                                                   \
        size_t Concat3(_i,LABEL,LINE);                                  \
        for(Concat3(_i,LABEL,LINE)=0;                                   \
            Concat3(_i,LABEL,LINE)<(TABLE)->nlines;                     \
            Concat3(_i,LABEL,LINE)++)                                   \
        {                                                               \
            size_t Concat3(_j,LABEL,LINE);                              \
            for(Concat3(_j,LABEL,LINE)=0;                               \
                Concat3(_j,LABEL,LINE)<(TABLE)->nparam;                 \
                Concat3(_j,LABEL,LINE)++)                               \
            {                                                           \
                _printf("p(%zu)=%g ",                                   \
                        Concat3(_j,LABEL,LINE),                         \
                        *((TABLE)->data+Concat3(_i,LABEL,LINE)*         \
                          ((TABLE)->nparam+(TABLE)->ndata)+             \
                          Concat3(_j,LABEL,LINE)));                     \
            }                                                           \
            for(Concat3(_j,LABEL,LINE)=(TABLE)->nparam;                 \
                Concat3(_j,LABEL,LINE)<((TABLE)->nparam+(TABLE)->ndata); \
                Concat3(_j,LABEL,LINE)++)                               \
            {                                                           \
                _printf("d(%zu)=%g ",                                   \
                        Concat3(_j,LABEL,LINE)-(TABLE)->nparam,         \
                        *((TABLE)->data+Concat3(_i,LABEL,LINE)*         \
                          ((TABLE)->nparam+(TABLE)->ndata)+             \
                          Concat3(_j,LABEL,LINE)));                     \
            }                                                           \
            _printf("\n");                                              \
        }                                                               \
    }


/*
 * Wrapper to rinterpolate() to use the data_table_t structs.
 */

#define Interpolate(DATA_TABLE,X,R,CACHE_N)                             \
    if(DTVB)fprintf(stderr,                                             \
                    "Interpolate %p data=%p nparam=%zu ndata=%zu nlines=%zu ncache=%zu stardata=%p stardata->persistent_data=%p stardata->persistent_data->rinterpolate_data=%p\n", \
                    (void*)(DATA_TABLE),                                \
                    (void*)(DATA_TABLE)->data,                          \
                    (size_t)(DATA_TABLE)->nparam,                       \
                    (size_t)(DATA_TABLE)->ndata,                        \
                    (size_t)(DATA_TABLE)->nlines,                       \
                    (size_t)(CACHE_N),                                  \
                    (void*)stardata,                                    \
                    (void*)(stardata ? stardata->persistent_data : NULL), \
                    (void*)((stardata && stardata->persistent_data) ? stardata->persistent_data->rinterpolate_data : NULL) \
        );                                                              \
    if(stardata->persistent_data->rinterpolate_data==NULL)              \
    {                                                                   \
        if(DTVB)fprintf(stderr,"Alloc rinterpolate dataspace\n");       \
        rinterpolate_alloc_dataspace(&stardata->persistent_data->       \
                                     rinterpolate_data);                \
        if(DTVB)fprintf(stderr,"Allocd rinterpolate dataspace\n");      \
    }                                                                   \
    if(DTVB)fprintf(stderr,"Call rinterpolate data=%p rinterpolate_data=%p nparam=%zu ndata=%zu nlines=%zu x=%p r=%p cache_n=%d\n", \
                    (void*)(DATA_TABLE)->data,                          \
                    (void*)stardata->persistent_data->rinterpolate_data, \
                    (size_t)(DATA_TABLE)->nparam,                       \
                    (size_t)(DATA_TABLE)->ndata,                        \
                    (size_t)(DATA_TABLE)->nlines,                       \
                    (void*)(X),                                         \
                    (void*)(R),                                         \
                    (int)(CACHE_N));                                    \
                                                                        \
    rinterpolate((const rinterpolate_float_t * const)(DATA_TABLE)->data, \
                 stardata->persistent_data->rinterpolate_data, \
                 (const rinterpolate_counter_t)(DATA_TABLE)->nparam,    \
                 (const rinterpolate_counter_t)(DATA_TABLE)->ndata,     \
                 (const rinterpolate_counter_t)(DATA_TABLE)->nlines,    \
                 (X),                                                   \
                 (R),                                                   \
                 (CACHE_N)                                              \
        );                                                              \
    if(stardata->persistent_data->rinterpolate_data)                    \
    {                                                                   \
        struct rinterpolate_table_t * const _rtable =                   \
            rinterpolate_table_struct(stardata->persistent_data->rinterpolate_data, \
                                      (DATA_TABLE)->data,               \
                                      NULL);                            \
        if(_rtable && _rtable->label == NULL)                           \
        {                                                               \
            if(DATA_TABLE_LABELS)                                       \
            {                                                           \
                if((DATA_TABLE)->label != NULL)                         \
                {                                                       \
                    if(0)_rtable->label = strdup((DATA_TABLE)->label);  \
                }                                                       \
                else                                                    \
                {                                                       \
                    if(asprintf(&_rtable->label,                        \
                                "%s() %d",                              \
                                __func__,                               \
                                __LINE__)>0){};                         \
                }                                                       \
            }                                                           \
        }                                                               \
    }


#define Scatterterpolate(DATA_TABLE,                                    \
                         X,                                             \
                         R,                                             \
                         CACHE_N,                                       \
                         NRANDOM,                                       \
                         RANDOM_SCALE)                                  \
    {                                                                   \
    rinterpolate_float_t * _x = Malloc(sizeof(rinterpolate_float_t) *   \
                                       (DATA_TABLE)->nparam);           \
    rinterpolate_float_t * _r = Calloc((DATA_TABLE)->ndata,             \
                                       sizeof(rinterpolate_float_t));   \
    for(rinterpolate_counter_t _i=0;_i<(NRANDOM);_i++)                  \
    {                                                                   \
        /* set randomized parameters */                                 \
        for(rinterpolate_counter_t _j=0;_j<(DATA_TABLE)->nparam;_j++)   \
        {                                                               \
            _x[_j] = (X)[_j] *                                          \
                (1.0 + (RANDOM_SCALE)*(random_number(stardata,          \
                                                     NULL)-0.5));       \
        }                                                               \
        /* interpolate these */                                         \
        Interpolate((DATA_TABLE),_x,(R),(CACHE_N));                     \
        /* save the mean result */                                      \
        for(rinterpolate_counter_t _j=0;_j<(DATA_TABLE)->ndata;_j++)    \
        {                                                               \
            _r[_j] += (R)[_j];                                          \
        }                                                               \
    }                                                                   \
    for(rinterpolate_counter_t _j=0;_j<(DATA_TABLE)->ndata;_j++)        \
    {                                                                   \
        (R)[_j] = _r[_j]/(rinterpolate_float_t)(NRANDOM);               \
    }                                                                   \
    Safe_free(_x);                                                      \
    Safe_free(_r);                                                      \
    }



/*
 * Copy data table from source S to destination D. Note this
 * does NOT copy the analysis or metadata.
 */
#define Copy_data_table(S,D)                                            \
    {                                                                   \
        if(DTVB)                                                        \
        {                                                               \
            fprintf(stderr,"copy table %p to %p, dest data = %p ; source data=%p nparam=%zu ndata=%zu nlines=%zu\n", \
                    (void*)(S),                                         \
                    (void*)(D),                                         \
                    (void*)((D)==NULL?NULL:(D)->data),                  \
                    (void*)(S)->data,                                   \
                    (size_t)(S)->nparam,                                \
                    (size_t)(S)->ndata,                                 \
                    (size_t)(S)->nlines);                               \
        }                                                               \
        if((D) == NULL)                                                 \
        {                                                               \
            if(DTVB)fprintf(stderr,"calloc new table\n");               \
            (D) = Calloc(1,sizeof(struct data_table_t));                \
        }                                                               \
        if((D)->data == NULL)                                           \
        {                                                               \
            if(DTVB)                                                    \
            {                                                           \
                fprintf(stderr,                                         \
                        "malloc %zu bytes\n",                           \
                        (size_t)Data_table_data_size(S));               \
            }                                                           \
            (D)->data = Malloc(Data_table_data_size(S));                \
        }                                                               \
        (D)->nlines = (S)->nlines;                                      \
        (D)->ndata  = (S)->ndata;                                       \
        (D)->nparam = (S)->nparam;                                      \
        if(DATA_TABLE_LABELS)                                           \
        {                                                               \
            if(asprintf(&(D)->label,                                    \
                        "copy of %s",                                   \
                        (S)->label)>0){};                               \
        }                                                               \
        memcpy((D)->data,                                               \
               (S)->data,                                               \
               Data_table_data_size(S));                                \
        if(DTVB)                                                        \
        {                                                               \
            fprintf(stderr,                                             \
                    "Copied data table %p to %p data=%p nparam=%zu ndata=%zu nlines=%zu\n", \
                    (void*)(S),                                         \
                    (void*)(D),                                         \
                    (void*)(D)->data,                                   \
                    (size_t)(D)->nparam,                                \
                    (size_t)(D)->ndata,                                 \
                    (size_t)(D)->nlines);                               \
        }                                                               \
    }

/*
 * Wrapper to make interpolate() parameter lists
 */
#define Parameters(...) ((double[]){__VA_ARGS__})

/*
 * Malloc a new data table in empty pointer P
 * and return it.
 *
 * The label is the function():line combination
 * made into a string.
 */
#define New_data_table(P)                                           \
    __extension__                                                   \
    ({                                                              \
        (P) = Calloc(1,sizeof(struct data_table_t));                \
        if((P)==NULL)                                               \
        {                                                           \
            Exit_binary_c(BINARY_C_MALLOC_FAILED,                   \
                          "Failed to allocate new data table.\n");  \
        }                                                           \
        if(DATA_TABLE_LABELS)                                       \
        {                                                           \
            if(asprintf(&(P)->label,                                \
                        "%s():%d",                                  \
                        __func__,                                   \
                        __LINE__)==-1)                              \
            {                                                       \
                Exit_binary_c(BINARY_C_ALLOC_FAILED,                \
                              "asprintf failed on pointer %p\n",    \
                              (void*)(P));                          \
            }                                                       \
        }                                                           \
        else                                                        \
        {                                                           \
            (P)->label = NULL;                                      \
        }                                                           \
        (P);                                                        \
    })

/*
 * Use Delete_data_table to clear up a table
 * including its contents.
 *
 * Should crash if P is NULL, this is deliberate.
 */
#define Delete_data_table(P)                    \
    free_data_table(&P,TRUE,TRUE,TRUE)

/*
 * Use Delete_data_table_not_contents to clear up a table
 * without touching its contents: handy if they are
 * a compiled-in constant that cannot be freed.
 * Note that we still free the table's metadata and
 * analysis, if set.
 */
#define Delete_data_table_not_contents(P)       \
    free_data_table(&P,FALSE,TRUE,TRUE);


/*
 * Wrapper around load_and_remap_table so it can be called
 * with only four arguments
 *
 * WANTED_COLS should be a list of integers inside a New_res(...)
 * corresponding to the columns required from the original table,
 * or All_cols for all of them.
 *
 * REMAP_RES should be a list of integers corresponding to the
 * new resolution of each parameter inside a Wanted_cols(...).
 *
 * The UNMAPPED_TABLE will be modified and freed by map_data_table,
 * so if you want to keep it, first copy it.
 */
#define Map_data_table(UNMAPPED_TABLE,          \
                       WANTED_COLS,             \
                       REMAP_RES,\
                       RESOLUTION_LIST)         \
    __extension__                               \
    ({                                          \
        map_data_table(                         \
            stardata,                           \
            &(UNMAPPED_TABLE),                  \
            WANTED_COLS,                        \
            REMAP_RES,                          \
            RESOLUTION_LIST,                    \
            Array_size(WANTED_COLS));           \
    })


#define _Protect(...) ((int[]){__VA_ARGS__})
#define New_res(...) _Protect(__VA_ARGS__)
#define Wanted_cols(...) _Protect(__VA_ARGS__)
#define All_cols _Protect(-1)

/*
 * Function macro to return a new table with selected
 * columns only.
 */
#define Reduce_data_table_columns(TABLE,                    \
                                  WANTED_COLS)              \
    __extension__                                           \
    ({                                                      \
        reduce_data_table_columns(stardata,                 \
                                  (TABLE),                  \
                                  WANTED_COLS,              \
                                  Array_size(WANTED_COLS)); \
    })

#endif // DATA_TABLE_FUNCTION_MACROS_H
