#include "../binary_c.h"
No_empty_translation_unit_warning;

void show_data_table(const struct data_table_t * const table)
{
    /*
     * Show a data table on stdout
     */

#undef item
#define item(I,J)                               \
    *(table->data +                             \
      (I)*(table->nparam + table->ndata) +      \
      (J))

    for(size_t i=0; i<table->nlines; i++)
    {
        for(size_t j=0; j<table->nparam; j++)
        {
            printf("%g ",item(i,j));
        }
        for(size_t j=0; j<table->ndata; j++)
        {
            printf("%g ",item(i,j+table->nparam));
        }
        printf("\n");
    }
}
