#include "../binary_c.h"
No_empty_translation_unit_warning;

#define _fwrite(...)                                                    \
    __extension__                                                       \
    ({                                                                  \
        const size_t s = fwrite(__VA_ARGS__);                           \
        if(s == 0 && ferror(fp))                                        \
        {                                                               \
            Exit_binary_c(BINARY_C_WRITE_FAILED,                        \
                          "Failed to write any data in write_data_table_to_file, trying to write to \"%s\"\n", \
                          bfp->path);                                   \
        }                                                               \
        s;                                                              \
    })

size_t write_data_table_to_file(struct stardata_t * const stardata,
                                struct binary_c_file_t * bfp,
                                struct data_table_t * table)
{
    /*
     * Write a data table to a file pointer
     */
    FILE * fp = bfp->fp;
    size_t n = 0;
    if(table)
    {
        n += _fwrite(&table->nparam,sizeof(size_t),1,fp);
        n += _fwrite(&table->ndata,sizeof(size_t),1,fp);
        n += _fwrite(&table->nlines,sizeof(size_t),1,fp);
        const size_t label_length = table->label == NULL ? 0 : strlen(table->label);
        n += _fwrite(&label_length,sizeof(size_t),1,fp);
        if(label_length > 0)
        {
            n += _fwrite(table->label,sizeof(char),label_length,fp);
        }
        n += _fwrite(table->data,
                    sizeof(double),
                    (table->nparam + table->ndata) * table->nlines,fp);
    }
    else
    {
        /*
         * table is NULL : just write a single zero,
         * a table with nparam=0 cannot ever work
         */
        const size_t nparam = 0;
        n += _fwrite(&nparam,sizeof(size_t),1,fp);
    }
    return n;
}
