#pragma once
#ifndef DATA_PROTOTYPES_H
#define DATA_PROTOTYPES_H
#include "../binary_c_code_options.h"

void show_data_table(const struct data_table_t * const table);
struct data_table_t * combine_data_tables(struct stardata_t * const stardata,
                                          struct data_table_t ** const tables,
                                          double ** const leftparams,
                                          const size_t ntables,
                                          const size_t n_leftparams);
struct data_table_t * prepend_left_value(struct stardata_t * const stardata,
                                         struct data_table_t * const table_in,
                                         const double left_value);
struct data_table_t * sort_data_table(struct data_table_t * const in);
struct data_table_t * copy_data_table(struct data_table_t * const table);
struct data_table_t * map_data_table(struct stardata_t * const stardata,
                                     struct data_table_t ** unmapped_table,
                                     int * wanted_columns,
                                     int * const new_resolutions,
                                     double ** const resolution_list,
                                     int n_wanted_columns);

struct data_table_t * reduce_data_table_columns(struct stardata_t * const stardata,
                                                struct data_table_t * const table,
                                                int * wanted_columns,
                                                int n_wanted_columns);

struct data_table_t * read_data_table_from_file(struct stardata_t * const stardata,
                                                struct binary_c_file_t * bfp);
size_t write_data_table_to_file(struct stardata_t * const stardata,
                                struct binary_c_file_t * bfp,
                                struct data_table_t * table);
Boolean check_for_repeated_parameters(struct stardata_t * const stardata,
                                      struct data_table_t * const table,
                                      size_t * const firstmatch);
Boolean check_data_table_for_nans(const struct data_table_t * const table);
struct data_table_t * join_data_tables(struct stardata_t * const stardata,
                                       struct data_table_t * const table1,
                                       struct data_table_t * const table2,
                                       const size_t offset1,
                                       const size_t step1,
                                       const size_t offset2,
                                       const size_t step2);

void remove_data_table_leading_rows(struct data_table_t * const table,
                                    const size_t n);
void add_leading_columns_to_data_table(struct data_table_t * const table,
                                       const size_t n_new_columns);
void transpose_data_table(struct data_table_t * const table,
                          const size_t new_nparam);
void set_data_table_column(struct data_table_t * const table,
                           const int ncol,
                           const double value);
void join_data_tables_in_place(struct stardata_t * const stardata,
                               struct data_table_t * const table1,
                               struct data_table_t * const table2,
                               const size_t offset1,
                               const size_t step1,
                               const size_t offset2,
                               const size_t step2);
struct data_table_analysis_t *
analyse_data_table(struct stardata_t * stardata,
                   struct data_table_t * table);
void free_data_table(struct data_table_t ** data_table_p,
                     const Boolean delete_contents,
                     const Boolean delete_metadata,
                     const Boolean delete_analysis);

#endif // DATA_PROTOTYPES_H
