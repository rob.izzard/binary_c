
    {
        struct cdict_entry_t * entry = CDict_nest_get_entry(hash,"mint_t","XHc");
        if(entry != NULL)
        {
            double data = entry->value.value.double_data;
            mint->XHc = data;
        }
    }

    {
        struct cdict_entry_t * entry = CDict_nest_get_entry(hash,"mint_t","XHec");
        if(entry != NULL)
        {
            double data = entry->value.value.double_data;
            mint->XHec = data;
        }
    }

    {
        struct cdict_entry_t * entry = CDict_nest_get_entry(hash,"mint_t","XCc");
        if(entry != NULL)
        {
            double data = entry->value.value.double_data;
            mint->XCc = data;
        }
    }

    {
        struct cdict_entry_t * entry = CDict_nest_get_entry(hash,"mint_t","XOc");
        if(entry != NULL)
        {
            double data = entry->value.value.double_data;
            mint->XOc = data;
        }
    }

    {
        struct cdict_entry_t * entry = CDict_nest_get_entry(hash,"mint_t","nshells");
        if(entry != NULL)
        {
            int data = entry->value.value.int_data;
            mint->nshells = data;
        }
    }

    {
        struct cdict_entry_t * entry = CDict_nest_get_entry(hash,"mint_t","_shell_t shells");
        if(entry != NULL)
        {
            int * data = entry->value.value.int_array_data;
            for(int i=0; i<1000; i++)
            {
                mint->_shell_t shells[i] = data[i];
            }
        }
