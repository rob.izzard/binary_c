#include "../binary_c.h"
No_empty_translation_unit_warning;

struct data_table_t * copy_data_table(struct data_table_t * const table)
{
    /*
     * Make and return a pointer to a copy of a
     * data table. The new table also has a copy
     * of the data, not the original data, i.e.
     * it is a shallow copy.
     */
    struct data_table_t * newtable = NULL;
    //Copy_data_table(table,newtable);

    if(newtable == NULL)
    {
        newtable = Calloc(1,sizeof(struct data_table_t));
    }
    if(newtable->data == NULL)
    {
        newtable->data = Malloc(Data_table_data_size(table));
    }
    newtable->nlines = table->nlines;
    newtable->ndata  = table->ndata;
    newtable->nparam = table->nparam;
    if(DATA_TABLE_LABELS)
    {
        if(asprintf(&newtable->label,
                    "copy of %s",
                    table->label)>0){};
    }
    memcpy(newtable->data,
           table->data,
           Data_table_data_size(table));

    return newtable;
}
