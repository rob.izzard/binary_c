
    {

        double data = mint->XHc;

        CDict_nest(hash,
                    "XHc",(double) data,
                    "mint_t");
    }

    {

        double data = mint->XHec;

        CDict_nest(hash,
                    "XHec",(double) data,
                    "mint_t");
    }

    {

        double data = mint->XCc;

        CDict_nest(hash,
                    "XCc",(double) data,
                    "mint_t");
    }

    {

        double data = mint->XOc;

        CDict_nest(hash,
                    "XOc",(double) data,
                    "mint_t");
    }

    {

        int data = mint->nshells;

        CDict_nest(hash,
                    "nshells",(int) data,
                    "mint_t");
    }

    {

        int data[1000];
        for(int i=0; i<1000; i++)
        {
            data[i] = mint->_shell_t shells[i];
        }

        CDict_nest(hash,
                    "_shell_t shells",(int[]) data,
                    "mint_t");
    }
