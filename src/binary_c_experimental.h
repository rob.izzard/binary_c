#pragma once
#ifndef BINARY_C_EXPERIMENTAL_H
#define BINARY_C_EXPERIMENTAL_H
/*
 * The binary_c stellar population nucleosynthesis framework.
 *
 * Copyright Robert Izzard 2000-2025
 *
 * Contact rob.izzard@gmail.com
 *
 * Code repository:
 * https://gitlab.com/binary_c
 *
 * Documentation is in doc/
 * Code is in src/
 *
 * To build, run
 *
 * ---
 *
 * meson setup builddir
 * cd builddir
 * ninja binary_c_install
 *
 * ---
 *
 * Please see the files README, LICENCE and CHANGES,
 * and the doc/ directory for documentation which is mirrored
 * at https://binary_c.gitlab.io
 *
 **********************
 *
 * Purpose:
 *
 * Experimental options for binary_c. Usually, this file does
 * nothing, but if you're trying something new and want to wrap
 * it in
 *     #ifdef XYZ ... #endif // XYZ
 * this file is the place to
 *     #define XYZ
 *
 **********************
 */
#ifndef MAKE_BSE_TABLES
/*
 * Activate table replacements for parts of the BSE
 * algorithm.
 *
 * NB if MAKE_BSE_TABLES is defined, you do NOT want
 * to do this.
 */
//#define USE_BSE_TIMESCALES_H
#endif


#ifdef USE_BSE_TIMESCALES_H
#define USE_BSE_TABLES
#endif // USE_BSE_TABLES

//#define NEW_ADAPTIVE_RLOF_CODE

#endif // BINARY_C_EXPERIMENTAL_H
