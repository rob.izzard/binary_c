#pragma once

/*
 * The binary_c stellar population nucleosynthesis framework.
 *
 * Copyright Robert Izzard 2000-2025
 *
 * Contact rob.izzard@gmail.com
 *
 * Code repository:
 * https://gitlab.com/binary_c
 *
 * Documentation is in doc/
 * Code is in src/
 *
 * To build, run
 *
 * ---
 *
 * meson setup builddir
 * cd builddir
 * ninja binary_c_install
 *
 * ---
 *
 * Please see the files README, LICENCE and CHANGES,
 * and the doc/ directory for documentation which is mirrored
 * at https://binary_c.gitlab.io
 *
 **********************
 *
 * Purpose:
 *
 * This file contains debugging information and macros.
 *
 * TLDR: if you want debugging...
 * 1) set DEBUG to 1 (this is usually enough)
 *
 * #define DEBUG 1
 *
 * 2) make sure Debug_expression is TRUE
 *
 * #define Debug_expression (TRUE)
 *
 * 3) make sure Debug_stop_expression is not defined
 *
 * #undef Debug_stop_expression
 *
 * If after this you have too much output, you can tune Debug_expression
 * appropriately, e.g.
 *
 * #define Debug_expression(stardata->model.time>10.0)
 *
 * would make sure there's debugging output only after 10Myr
 * of evolution. You can assume you always have stardata available
 * for debugging.
 **********************
 */
#ifndef BINARY_C_DEBUG_H
#define BINARY_C_DEBUG_H

#include "binary_c_string_macros.h"
#include "logging/ansi_colours.h"

/*
 * Do you want debugging output?
 *
 * DEBUG = 1 : debugging output
 * DEBUG = 0 : debug functions are 'built' but not called
 * DISABLE_DPRINT defined : debug functions are disabled
 *
 * NB If DEBUG is zero (0) then usually the functions are not built
 * because the compiler is clever enough to recognise that if(0)
 * is always false. This is the default.
 *
 * If DEBUG is 1 and preferences->disable_debug is TRUE
 * then debugging output will not happen and NaN/InF checks
 * will be disabled. The code will run slower than usual
 * because of all the DEBUG checking that goes on.
 *
 * See below for further options.
 */
#define DEBUG 0

/*
 * To remove debugging possibility from the code, globally, hence
 * speed it up (and reduce the executable size) enable this. Note, this
 * will be undefined if DEBUG is set above, but if DEBUG is set on a per-file
 * basis, you have to enable DISABLE_DPRINT here.
 *
 * Normally, you want to enable DISABLE_DPRINT (and disable DEBUG) for
 * production code. For ANY testing, do not DISABLE_DPRINT (although DEBUG
 * is optional).
 *
 * NOTE: with gcc (4.1.2 and later have been tested) because the
 * compiler is clever, and the debug statements are prepended with
 * "if(DEBUG)", the print statements are not built into the code.
 * (if(DEBUG) is if(0) which is clearly never true) For this reason,
 * you should not have to define DISABLE_DPRINT unless you have an
 * ancient compiler ...
 * (Intel's compiler is similarly clever although its executables
 *  seem to be larger, and faster)
 */
//#define DISABLE_DPRINT


/* an expression to dictate when we use debugging :
 * usually (TRUE) but you can use an expression which
 * must be TRUE for Dprint to do anything.
 *
 * e.g. the following:12.3482943844
 * #define Debug_expression (stardata->model.time>=259.0)
 * will only output when the time is >= 259 Myr.
 *
 * Use stardata instead of stardata: usually these are
 * the same, but stardata is defined everywhere as a global
 * variable.
 */
#define Debug_expression (TRUE)
//#define Debug_expression (stardata->model.model_number >= 806-1)

/*
 * If you define Debug_stop_expression, and it is at any time TRUE,
 * then binary_c will stop.
 *
 * This expression is checked every time Dprint is used (i.e. every
 * call to debug_fprintf).
 *
 * Note that Debug_expression is not required to be TRUE.
 * If you require Debug_expression to be TRUE, then you can
 * set this as part of the Debug_stop_expression.
 *
 * Beware: the Debug_stop_expression should not trigger
 *         before version output, i.e. during the memeory-allocation
 *         stage of binary_c startup. To avoid this, use
 *
 *   stardata->model.model_number > 0
 *
 *         as one of your criteria.
 */


#define Debug_stop_expression (                                         \
        stardata!=NULL &&                                               \
        stardata->model.model_number > 1 &&                             \
        stardata->star != NULL &&                                       \
        (                                                               \
            (stardata->star[0].stellar_type != MASSLESS_REMNANT &&      \
             (                                                          \
                 !Fequal(nucsyn_totalX(stardata->star[0].Xenv),1.0)     \
                 ||                                                     \
                 Is_really_zero(nucsyn_totalX(stardata->star[0].Xenv))  \
                 )                                                      \
                )                                                       \
            ||                                                          \
            (stardata->star[1].stellar_type != MASSLESS_REMNANT &&      \
             (                                                          \
                 !Fequal(nucsyn_totalX(stardata->star[1].Xenv),1.0)     \
                 ||                                                     \
                 Is_really_zero(nucsyn_totalX(stardata->star[1].Xenv))  \
                 )                                                      \
                )                                                       \
            ||                                                          \
            (stardata->star[0].stellar_type < NEUTRON_STAR              \
             &&                                                         \
             stardata->star[0].TZO == FALSE                             \
             &&                                                         \
             !Fequal(stardata->star[0].mass,                            \
                     stardata->star[0].baryonic_mass))                  \
            ||                                                          \
            (stardata->star[1].stellar_type < NEUTRON_STAR              \
             &&                                                         \
             stardata->star[1].TZO == FALSE                             \
             &&                                                         \
             !Fequal(stardata->star[1].mass,                            \
                     stardata->star[1].baryonic_mass))                  \
                                                                        \
            )                                                           \
        )
#undef Debug_stop_expression

/*
 * Debug_show_expression, if defined, is shown at the beginning
 * of every Dprint. It should take the form of a format statement
 * followed by variables (or expressions) just as you would
 * pass as arguments to printf.
 *
 * If Debug_expression is 0, or stardata is NULL in debug_fprintf(),
 * this is not shown.
 */
#ifdef NUCSYN
#define Debug_show_expression "%d [0 Mgrav=%g Mbary=%g %s M+Y=%g Xtot 1-env=%g 1-acc=%g ] [1 Mgrav=%g Mbary=%g %s M+Y=%g Xtot 1-env=%g 1-acc=%g] TOT %g ", \
        stardata->model.model_number,                                   \
        stardata->star[0].mass,                                         \
        stardata->star[0].baryonic_mass,                                \
        nucsyn_yield_string(&stardata->star[0]),                        \
        stardata->star[0].mass + sum_C_double_array(stardata->star[0].Xyield, ISOTOPE_ARRAY_SIZE), \
        1.0-nucsyn_totalX(stardata->star[0].Xenv),                      \
        1.0-nucsyn_totalX(stardata->star[0].Xacc),                      \
        stardata->star[1].mass,                                         \
        stardata->star[1].baryonic_mass,                                \
        nucsyn_yield_string(&stardata->star[1]),                        \
        stardata->star[1].mass + sum_C_double_array(stardata->star[1].Xyield, ISOTOPE_ARRAY_SIZE), \
        1.0-nucsyn_totalX(stardata->star[1].Xenv),                      \
        1.0-nucsyn_totalX(stardata->star[1].Xacc),nucsyn_total_yield(stardata)
#endif//NUCSYN

#undef Debug_show_expression

/*
 * DEBUG_STREAM is either a stream (stdout, stderr).
 *
 * If you want Dprint to output to Printf and hence be captured
 * in the output buffer, define DEBUG_STREAM_BUFFER (and DEBUG_STREAM
 * is ignored by Dprint, although possibly is used elsewhere).
 *
 * If you are using binary_c-python you want to set
 * DEBUG_STREAM to stderr and turn off DEBUG_STREAM_BUFFER.
 */
#ifndef DEBUG_STREAM
#define DEBUG_STREAM stdout
//#define DEBUG_STREAM_BUFFER
#define MAX_DEBUG_PRINT_SIZE 2048
#endif // !DEBUG_STREAM


/* enable line number output from Dprint */
#define DEBUG_LINENUMBERS

/* enable filenames */
#define DEBUG_SHOW_FILENAMES


/*
 * If enabled, DEBUG_FAIL_ON_NAN bails out binary_c when a
 * nan is printed. Note that this can be overridden by setting
 * preferences->allow_debug_nan to TRUE (usually temporarily).
 */
#define DEBUG_FAIL_ON_NAN

/*
 * If enabled, DEBUG_FAIL_ON_INF bails out binary_c when an
 * inf is printed. Note that this can be overridden by setting
 * preferences->allow_debug_inf to TRUE (usually temporarily).
 */
#define DEBUG_FAIL_ON_INF

/*
 * Sometimes you'll be searching for a nan (not a number) in the debug
 * output. Enable DEBUG_REMOVE_NAN_FROM_FILENAMES to remove the nan from
 * filenames (e.g. remnant -> remn_nt). This adds a little overhead, but
 * who cares? You're in DEBUG mode anyway, you can't expect it to be fast.
 */
#define DEBUG_REMOVE_NAN_FROM_FILENAMES

/*
 * Enable caller information, if this is possible (requires BACKTRACE
 * and building with -rdynamic, -g etc. (use ./configure debug)
 */
//#define DEBUG_CALLER
#define DEBUG_REMOVE_NAN_FROM_CALLER
#define DEBUG_CALLER_LINE

#ifdef DEBUG_CALLER
/*
 * DEBUG_CALLER_DEPTH is the depth in the backtrace that one must
 * go
 */
#define DEBUG_CALLER_DEPTH 3
#endif // DEBUG_CALLER

#if (defined DEBUG_CALLER) && (defined DEBUG_CALLER_LINE)
#define ADDR2LINE
#endif // DEBUG_CALLER && DEBUG_CALLER_LINE

/*
 * Action for the Debug_stop_expression
 */
#ifdef Debug_stop_expression
#define _Debug_stop                                                     \
    if(stardata != NULL &&                                              \
       Debug_stop_expression &&                                         \
       (stardata->store == NULL ||                                      \
        stardata->store->debug_stopping == 0))                          \
    {                                                                   \
        fflush(NULL);                                                   \
        if(stardata->store != NULL)                                     \
        {                                                               \
            stardata->store->debug_stopping = TRUE;                     \
        }                                                               \
        Exit_binary_c(BINARY_C_DEBUG_STOP,                              \
                      "Stopping binary_c because Debug_stop_expression (%s) is TRUE.\n", \
                      Stringify_macro(Debug_stop_expression));          \
    }
#else
#define _Debug_stop /* */
#endif // Debug_stop_expression


/*
 * Macro that is TRUE if we should do debugging,
 * false otherwise.
 */
#define DO_DEBUG (                                          \
        ( DEBUG ) &&                                        \
        (stardata != NULL) &&                               \
        (stardata->preferences == NULL ||                   \
         stardata->preferences->disable_debug==FALSE) &&    \
        (Debug_expression)                                  \
        )

/*
 * The all-important Dprint macro.
 *
 * If preferences is NULL, we output.
 */
#ifndef DISABLE_DPRINT

#ifdef Debug_show_expression
#define Dprint_to_pointer(P,NEWLINE,...)                \
    if(DO_DEBUG)                                        \
    {                                                   \
        char * __c;                                     \
        int __n = asprintf(&__c,Debug_show_expression); \
        if(__n>0)                                       \
        {                                               \
            fprintf(DEBUG_STREAM,                       \
                    "%s ",                              \
                    __c);                               \
        }                                               \
        Safe_free(__c);                                 \
        debug_fprintf((struct stardata_t * const)(P),   \
                      strchr(__FILE__,'/')+1,           \
                      __func__,                         \
                      __LINE__,                         \
                      (NEWLINE),                        \
                      __VA_ARGS__);                     \
    }
#else
#define Dprint_to_pointer(P,NEWLINE,...)                \
    if(DO_DEBUG)                                        \
    {                                                   \
        debug_fprintf((struct stardata_t * const)(P),   \
                      strchr(__FILE__,'/')+1,           \
                      __func__,                         \
                      __LINE__,                         \
                      (NEWLINE),                        \
                      __VA_ARGS__);                     \
    }
#endif

#define Dprint(...)                                             \
    {                                                           \
        Dprint_to_pointer((struct stardata_t * const)stardata,  \
                          TRUE,                                 \
                          __VA_ARGS__);                         \
        _Debug_stop;                                            \
    }

#define Dprint_no_newline(...)                                  \
    {                                                           \
        Dprint_to_pointer((struct stardata_t * const)stardata,  \
                          FALSE,                                \
                          __VA_ARGS__);                         \
        _Debug_stop;                                            \
    }

#else
#define Dprint(...) _Debug_stop;
#endif // !DISABLE_PRINT

#ifdef NANCHECKS
/* nan check */
#define Nancheck(A)                                                     \
    {                                                                   \
        if(isnan(A) || isinf(A))                                        \
        {                                                               \
            fflush(stdout);                                             \
            fflush(stderr);                                             \
            printf("NaN detected in %s (file %s, line %d)\n",           \
                   Stringof(A),__FILE__,__LINE__);                      \
            fflush(stdout);                                             \
            fprintf(stderr,"NaN detected in %s (file %s, line %d)\n",   \
                    Stringof(A),__FILE__,__LINE__);                     \
            fflush(stderr);                                             \
            kill(0,SIGSEGV);                                            \
            Exit_binary_c(BINARY_C_EXIT_NAN,                            \
                          "NaN detected in %s (file %s, line %d)\n",    \
                          Stringof(A),__FILE__,__LINE__);               \
        }                                                               \
    }
#else
/* do nothing if no Nancheck is defined, but also do not fail to compile */
#define Nancheck(A) /* do nothing */
#endif

/*
 * Define SHOW_STARDATA to use the contents of stardata.h to show
 * the contents of the stardata struct.
 */
//#define SHOW_STARDATA

#endif // BINARY_C_DEBUG_H
