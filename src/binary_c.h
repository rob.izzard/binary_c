#ifndef BINARY_C_H
#define BINARY_C_H

/*
 * The binary_c stellar population nucleosynthesis framework.
 *
 * Copyright Robert Izzard 2000-2025
 *
 * Contact rob.izzard@gmail.com
 *
 * Code repository:
 * https://gitlab.com/binary_c
 *
 * Documentation is in doc/
 * Code is in src/
 *
 * To build, run
 *
 * ---
 *
 * meson setup builddir
 * cd builddir
 * ninja binary_c_install
 *
 * ---
 *
 * Please see the files README, LICENCE and CHANGES,
 * and the doc/ directory for documentation which is mirrored
 * at https://binary_c.gitlab.io
 *
 **********************
 *
 * Purpose:
 *
 * This header file loads in all the macros, prototypes, etc.
 * for the binary_c framework.
 *
 * This is the header file to be loaded if you are using the
 * libbinary_c API.
 *
 **********************
 */

/* first : system headers */
#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif
#include <stdlib.h>
#include <errno.h>
#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <strings.h>
#include <setjmp.h>
#include <stdint.h>
#include <stdalign.h>
#include <ctype.h>
#include <limits.h>
#include <float.h>
#include <sys/timeb.h>
#include <sys/time.h>
#include <time.h>
#include <stdarg.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <signal.h>
#include <fenv.h>
#include <uuid/uuid.h>
#ifdef __HAVE_LIBBSD__
#include <bsd/string.h>
#endif
#ifdef __HAVE_LIBPTHREAD__
#include <pthread.h>
#endif

/* binary_c's headers */
#include "binary_c_version_macros.h"
#include "binary_c_code_options.h"
#include "binary_c_debug.h"
#include "binary_c_error_codes.h"
#include "misc/misc.h"
#include "binary_c_parameters.h"
#include "binary_c_types.h"
#include "binary_c_macros.h"
#include "binary_c_structures.h"
#include "binary_c_cdict.h"
#include "binary_c_prototypes.h"
#include "timestep/timestep.h"
#include "nucsyn/nucsyn.h"
#include "supernovae/sn.h"
#include "disc/disc.h"
#include "memory/memory_function_macros.h"
#include "tides/tides.h"
#include "API/binary_c_API.h"
#ifdef __HAVE_LIBSUNDIALS_CVODE__
#include "maths/cvode.h"
#endif
#ifdef MEMOIZE
#ifdef __HAVE_LIBMEMOIZE__
#  include <memoize.h>
#else
#  include "libmemoize/memoize.h"
#endif //__HAVE_LIBMEMOIZE__
#endif //MEMOIZE
#include "binary_c_objects.h"
#ifdef __HAVE_LIBRINTERPOLATE__
#  include <rinterpolate.h>
#else
#  include "librinterpolate/rinterpolate.h"
#endif//__HAVE_LIBRINTERPOLATE__
#include "MINT/MINT.h"
#ifdef YBC
#include "stellar_colours/ybc.h"
#endif //YBC

#endif //BINARY_C_H
