#include "../binary_c.h"
No_empty_translation_unit_warning;


/*
 * Checks on various mathematical operations
 * to make sure we are compliant.
 */
void maths_checks(struct stardata_t * const stardata)
{
    check_nans_are_signalled();

    /*
     * Check that memset to 0 of a double gives 0.0
     */
    {
        double * x = Malloc(sizeof(double));
        const double y = 0.0;
        memset(x,0,sizeof(double));
        if(y != x[0])
        {
            Exit_binary_c(BINARY_C_MEMSET_FAILURE,
                          "Memset(*,0,...) does not give a double-precision zero.");
        }
        Safe_free(x);
    }

    /*
     * Check NARGS() counts macro arguments correctly.
     *
     * On some compilers, e.g. Clang 11, it fails, but
     * meson should spot this and error.
     */
    if(NARGS() != 0 ||
       NARGS(1) != 1 ||
       NARGS(1,2) != 2 ||
       NARGS(1,2,3) != 3 ||
       NARGS(1,2,3,4) != 4)
    {
        Exit_binary_c(BINARY_C_MACRO_COUNT_FAILURE,
                      "NARG() macro failed to count properly. Please check whether __VA_OPT__ works correctly with this compiler.");
    }

    {
        /*
         * Check that cdict returns appropriate data
         */
        CDict_new(cdict);
        CDict_nest_set(cdict,
                       "Robert Pires",7,
                       "Best player ever");

        struct cdict_entry_t * ee =
            CDict_nest_get_entry(
                cdict,
                "Best player ever",
                "Robert Pires"
                );
        if(ee->value.value.int_data != 7)
        {
            Exit_binary_c(BINARY_C_CDICT_ERROR,
                          "Cdict failed to return correct data. Please check that you have not compiled with -fpack-struct=1.");
        }
        CDict_free(cdict);
    }

    for(unsigned char _i=0; _i<255; _i++)
    {
        const Boolean truth = Boolean_(isdigit(_i));
        if(is_integer1(_i) != truth ||
           is_integer2(_i) != truth ||
           is_integer3(_i) != truth ||
           is_integer_or(_i) != truth ||
           is_integer_table(_i) != truth)
        {
            Exit_binary_c(BINARY_C_ASSERT_FAILED,
                          "One of the is_integer functions failed: char %u = %c isdigit? %s : is_integer %s, isinteger2 %s, is_integer3 %s, is_integer_or %s, is_integer_table %s\n",
                          _i,
                          _i,
                          Yesno(truth),
                          Yesno(is_integer1(_i)),
                          Yesno(is_integer2(_i)),
                          Yesno(is_integer3(_i)),
                          Yesno(is_integer_or(_i)),
                          Yesno(is_integer_table(_i)));
        }
    }
}
