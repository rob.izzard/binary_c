#pragma once
#ifndef LSODA_H
#define LSODA_H
#include <stdio.h>
#include <math.h>
#include <stdlib.h>

/*
 * This is an edited copy of the C LSODA library.
 *
 * RGI has copied it under the MIT licence (below)
 * for use in binary_c, and made many changes to clean
 * the code for binary_c's style and to (hopefully!) make
 * it thread safe (e.g. so there are no static variables).
 */


/*
  This is a C version of the LSODA library. I acquired the original
  source code from this web page:

    http://www.ccl.net/cca/software/SOURCES/C/kinetics2/index.shtml

  I merged several C files into one and added a simpler interface. I
  also made the array start from zero in functions called by lsoda(),
  and fixed two minor bugs: a) small memory leak in freevectors(); and
  b) misuse of lsoda() in the example.

  The original source code came with no license or copyright
  information. I now release this file under the MIT/X11 license. All
  authors' notes are kept in this file.

  - Heng Li <lh3lh3@gmail.com>

  *
  * This version is from
  * http://lh3lh3.users.sourceforge.net/download/lsoda.c
  */

/* The MIT License

   Copyright (c) 2009 Genome Research Ltd (GRL).

   Permission is hereby granted, free of charge, to any person obtaining
   a copy of this software and associated documentation files (the
   "Software"), to deal in the Software without restriction, including
   without limitation the rights to use, copy, modify, merge, publish,
   distribute, sublicense, and/or sell copies of the Software, and to
   permit persons to whom the Software is furnished to do so, subject to
   the following conditions:

   The above copyright notice and this permission notice shall be
   included in all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
   EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
   MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
   NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
   BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
   ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
   SOFTWARE.
*/
/* Contact: Heng Li <lh3@sanger.ac.uk> */






#ifdef __LSODA_REQUIRE_STATIC_PROTOTYPES

static void     stoda(struct lsoda_t * Restrict const l,int neq, double *y, _lsoda_f f, void *_data);
static void     correction(struct lsoda_t * Restrict const l,int neq, double *y, _lsoda_f f, int *corflag, double pnorm, double *del, double *delp, double *told,
                           int *ncf, double *rh, int *m, void *_data);
static void     prja(struct lsoda_t * Restrict const l,
                     const int neq,
                     double * const Restrict y,
                     _lsoda_f f,
                     void * _data);
static void     lsoda_terminate(struct lsoda_t * Restrict const l,int *const istate);
static void     lsoda_terminate2(struct lsoda_t * Restrict const l,double * consty, double * const t);
static void successreturn(struct lsoda_t * Restrict const l,
                          double * const y,
                          double * const t,
                          const int itask,
                          const int ihit,
                          const double tcrit,
                          int * const istate);
static void     freevectors(struct lsoda_t * Restrict const l); /* this function does nothing */
static void     _freevectors(struct lsoda_t * Restrict const l);
static void     ewset(struct lsoda_t * Restrict const l,int itol, double *rtol, double *atol, double *ycur);
static void     resetcoeff(struct lsoda_t * Restrict const l);
static void     solsy(struct lsoda_t * Restrict const l,double *y);
static void     endstoda(struct lsoda_t * Restrict const l);
static void     orderswitch(struct lsoda_t * Restrict const l,double *rhup, double dsm, double *pdh, double *rh, int *orderflag);
static void     intdy(struct lsoda_t * Restrict const l,double t, int k, double *dky, int *iflag);
static void     corfailure(struct lsoda_t * Restrict const l,double *told, double *rh, int *ncf, int *corflag);
static void     methodswitch(struct lsoda_t * Restrict const l,double dsm, double pnorm, double *pdh, double *rh);
static void     cfode(struct lsoda_t * Restrict const l,int meth);
static void     scaleh(struct lsoda_t * Restrict const l,double *rh, double *pdh);
static double fnorm(int n, const double ** Restrict a, const double *const w);
static double vmnorm(const int n, const double * const Restrict v, const double * const Restrict w);


// RGI extra prototypes
static void dgesl(double ** const Restrict a,
                  const int n,
                  int * const ipvt,
                  double * const Restrict b,
                  const int job);
void dgefa(double ** const Restrict a,
           const int n,
           int * const Restrict ipvt,
           int * const Restrict info);
static int idamax(const int n,
                  const double * const Restrict dx,
                  const int incx);
void dscal(const int n,
           const double da,
           double * const Restrict dx,
           const int incx);
static double ddot(const int n,
                   double * const Restrict dx,
                   const int incx,
                   double * const Restrict dy,
                   const int incy);
static void daxpy(const int n,
                  const double da,
                  double * const Restrict dx,
                  const int incx,
                  double * const Restrict dy,
                  const int incy);

#endif//__LSODA_REQUIRE_STATIC_PROTOTYPES

int lsoda_example(struct lsoda_t * Restrict const l);

// RGI : replaced max,min with Max,Min
//#define max( a , b )  ( (a) > (b) ? (a) : (b) )
//#define min( a , b )  ( (a) < (b) ? (a) : (b) )

#define max( a , b )  Max((a),(b))
#define min( a , b )  Min((a),(b))

#define ETA 2.2204460492503131e-16


#ifndef Swap
#define Swap(A,B) {const Autotype(A) temp=(A);(A)=(B);(B)=temp;}
#endif

/*
   The following are useful statistics.

   hu,
   h,
   tn,
   tolsf,
   tsw,
   nst,
   nfe,
   nje,
   nqu,
   nq,
   imxer,
   mused,
   meth
*/


#endif // LSODA_H
