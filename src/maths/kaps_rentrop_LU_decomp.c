#include "../binary_c.h"
No_empty_translation_unit_warning;

#include <gsl/gsl_linalg.h>

void kaps_rentrop_LU_decomp(double *const * const a,
                            const unsigned int n2,
                            gsl_matrix * Restrict m,
                            gsl_permutation * const Restrict p)
{
    /*
     * Perform LU decomposition using GSL
     */
    for(unsigned int i=1;i<n2;i++)
    {
        for(unsigned int j=1;j<n2;j++)
        {
            gsl_matrix_set(m,i-1,j-1,a[i][j]);
        }
    }

    int ss;
    gsl_linalg_LU_decomp(m, p, &ss);

    for(unsigned int i=1;i<n2;i++)
    {
        for(unsigned int j=1;j<n2;j++)
        {
            a[i][j] = gsl_matrix_get(m,i-1,j-1);
        }
    }
}
