#include "../binary_c.h"
No_empty_translation_unit_warning;

/*
 * Check array for nan, return the index of the first
 * found nan, or n+1 if not found.
 */
size_t first_nan_in_array(const double * const array,
                          const size_t n)
{
    /*
     * Check array, of size n, for nans
     */
    for(size_t i=0; i<n; i++)
    {
        if(isnan(array[i]))
        {
            return i;
        }
    }
    return n+1;
}
