#include "../binary_c.h"
No_empty_translation_unit_warning;


#define __LSODA_REQUIRE_STATIC_PROTOTYPES
#include "lsoda.h"
#define Lprintf(...) /* do nothing */
//#define Lprintf(...) printf(__VA_ARGS__);

/*
  This is a C version of the LSODA library. I acquired the original
  soul->rce code from this web page:

  http://www.ccl.net/cca/software/SOUL->RCES/C/kinetics2/index.shtml

  I merged several C files into one and added a simpler interface. I
  also made the array start from zero in functions called by lsoda(),
  and fixed two minor bugs: a) small memory leak in freevectors(); and
  b) misuse of lsoda() in the example.

  The original soul->rce code came with no license or copyright
  information. I now release this file under the MIT/X11 license. All
  authors' notes are kept in this file.

  - Heng Li <lh3lh3@gmail.com>

  *
  * This version is from
  * http://lh3lh3.users.soul->rceforge.net/download/lsoda.c
  */

/* The MIT License

   Copyright (c) 2009 Genome Reseal->rch Ltd (GRL).

   Permission is hereby granted, free of charge, to any person obtaining
   a copy of this software and associated documentation files (the
   "Software"), to deal in the Software without restriction, including
   without limitation the rights to use, copy, modify, merge, publish,
   distribute, sublicense, and/or sell copies of the Software, and to
   permit persons to whom the Software is furnished to do so, subject to
   the following conditions:

   The above copyright notice and this permission notice shall be
   included in all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
   EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
   MEL->RCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
   NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT L->HOLDERS
   BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
   ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
   SOFTWARE.
*/

/*
 * Todo: enum the istates
 *
 * enum itask:
 *
 */

#define ITASKS_LIST                                     \
    X(ITASK_SUCCESS_REACHED_TOUT, 1, "reached tout")    \
        X(ITASK_SUCCESS, 2, "success, not yet at tout") \
        X(ITASK_3, 3, "3")                              \
        X(ITASK_4, 4, "4")                              \
        X(ITASK_5, 5, "5")
#undef X
#define X(ITASK,NUM,STRING) ITASK = (NUM),
enum { ITASKS_LIST };
#undef X



enum {
    INITIAL_VALUE = 1,
    DERIVATIVES = 2
};

enum {
    METHOD_NONSTIFF = 1,
    METHOD_STIFF = 2
};

/* Contact: Heng Li <lh3@sanger.ac.uk> */

/************
 * idamax.c *
 ************/

/* Purpose : Find largest component of double vector dx


   --- Input ---

   n    : number of elements in input vector
   dx   : double vector with n+1 elements, dx[0] is not used
   incx : storage spacing between elements of dx


   --- Output ---

   idamax : smallest index, 0 if n <= 0


   Find smallest index of maximum magnitude of dx.
   idamax = first i, i=1 to n, to minimize fabs( dx[1-incx+i*incx] ).

*/
static int idamax(const int n,
                  const double * const Restrict dx,
                  const int incx)
{
    int xindex = 0; /* returned */
    if(n>0)
    {
        xindex = 1;
        if(!(n <= 1 || incx <= 0))
        {
            /*
             * Code for increments not equal to 1.
             */
            double dmax = fabs(dx[1]);
            if(incx != 1)
            {
                int ii = 2;
                for(int i = 1 + incx; i <= n * incx; i = i + incx)
                {
                    const double xmag = fabs(dx[i]);
                    if(xmag > dmax)
                    {
                        xindex = ii;
                        dmax = xmag;
                    }
                    ii++;
                }
            }
            else
            {
                /*
                 * Code for increments equal to 1.
                 */
                for(int i = 2; i <= n; i++)
                {
                    const double xmag = fabs(dx[i]);
                    if(xmag > dmax)
                    {
                        xindex = i;
                        dmax = xmag;
                    }
                }
            }
        }
    }
    return xindex;
}

/***********
 * dscal.c *
 ***********/

/* Purpose : scalar vector multiplication

   dx = da * dx


   --- Input ---

   n    : number of elements in input vector
   da   : double scale factor
   dx   : double vector with n+1 elements, dx[0] is not used
   incx : storage spacing between elements of dx


   --- Output ---

   dx = da * dx, unchanged if n <= 0


   For i = 0 to n-1, replace dx[1+i*incx] with
   da * dx[1+i*incx].

*/
void dscal(const int n,
           const double da,
           double * const Restrict dx,
           const int incx)
{
    if(n>0)
    {
        /*
         * Code for increments not equal to 1.
         */
        if(incx != 1)
        {
            for(int i = 1; i <= n * incx; i = i + incx)
            {
                dx[i] *= da;
            }
        }
        else
        {
            /*
             * Code for increments equal to 1.
             */

            /*
             * Clean-up loop so remaining vector length is a multiple of 5.
             */
            const int m = n % 5;
            if(m != 0)
            {
                for(int i = 1; i <= m; i++)
                {
                    dx[i] *= da;
                }
                if(n < 5)
                {
                    return;
                }
            }
            for(int i = m + 1; i <= n; i += 5)
            {
                for(int j=0; j<5; j++)
                {
                    dx[i+j] *= da;
                }
            }
        }
    }
}

/*
 * ddot.c *
 *
 *
 * Purpose : Inner product dx . dy
 *
 *
 * --- Input ---
 *
 * n    : number of elements in input vector(s)
 * dx   : double vector with n+1 elements, dx[0] is not used
 * incx : storage spacing between elements of dx
 * dy   : double vector with n+1 elements, dy[0] is not used
 * incy : storage spacing between elements of dy
 *
 *
 * --- Output ---
 *
 * ddot : dot product dx . dy, 0 if not <= 0
 *
 * ddot = sum for i = 0 to n-1 of
 * dx[lx+i*incx] * dy[ly+i*incy] where lx = 1 if
 * incx >= 0, else lx = (-incx)*(n-1)+1, and ly
 * is defined in a similar way using incy.
 *
 */
static double ddot(const int n,
                   double * const Restrict dx,
                   const int incx,
                   double * const Restrict dy,
                   const int incy)
{
    double dotprod = 0.0;

    if(n>0)
    {
        /*
         * Code for unequal or nonpositive increments.
         */
        if(incx != incy || incx < 1)
        {
            int ix = incx < 0 ? ((-n + 1) * incx + 1) : 1;
            int iy = incy < 0 ? ((-n + 1) * incy + 1) : 1;
            for(int i = 1; i <= n; i++)
            {
                dotprod += dx[ix] * dy[iy];
                ix += incx;
                iy += incy;
            }
            return dotprod;
        }
        /*
         * Code for both increments equal to 1.
         */

        /*
         *Clean-up loop so remaining vector length is a multiple of 5.
         */
        if(incx == 1)
        {
            const int m = n % 5;
            if(m != 0)
            {
                for(int i = 1; i <= m; i++)
                {
                    dotprod += dx[i] * dy[i];
                }
                if(n < 5)
                {
                    return dotprod;
                }
            }
            for(int i = m + 1; i <= n; i = i + 5)
            {
                for(int j = 0; j < 5; j++)
                {
                    dotprod += dx[i+j] * dx[i+j];
                }
            }
        }
        else
        {
            /*
             * Code for positive equal nonunit increments.
             */
            for(int i = 1; i <= n * incx; i = i + incx)
            {
                dotprod += dx[i] * dy[i];
            }
        }
    }
    return dotprod;
}

/***********
 * daxpy.c
 *
 *
 * From tam@dragonfly.wri.com Wed Apr 24 15:48:31 1991
 * Return-Path: <tam>
 * Date: Wed, 24 Apr 91 17:48:43 CDT
 * From: tam@dragonfly.wri.com
 * To: whitbeck@sanjuan.wl->rc.unr.edu
 *
 *
 *
 *    Purpose : To compute
 *
 *    dy = da * dx + dy
 *
 *
 *   --- Input ---
 *
 *   n    : number of elements in input vector(s)
 *   da   : double scalar multiplier
 *   dx   : double vector with n+1 elements, dx[0] is not used
 *   incx : storage spacing between elements of dx
 *   dy   : double vector with n+1 elements, dy[0] is not used
 *   incy : storage spacing between elements of dy
 *
 *
 *   --- Output ---
 *
 *   dy = da * dx + dy, unchanged if n <= 0
 *
 *
 *   For i = 0 to n-1, replace dy[ly+i*incy] with
 *   da*dx[lx+i*incx] + dy[ly+i*incy], where lx = 1
 *   if  incx >= 0, else lx = (-incx)*(n-1)+1 and ly is
 *   defined in a similar way using incy.
 *
 */
static void daxpy(const int n,
                  const double da,
                  double * const Restrict dx,
                  const int incx,
                  double * const Restrict dy,
                  const int incy)
{
    if(n>=0 && da!=0.0)
    {
        /*
         * Code for nonequal or nonpositive increments.
         */
        if(incx != incy || incx < 1)
        {
            int ix = incx < 0 ? ((-n + 1) * incx + 1) : 1;
            int iy = incy < 0 ? ((-n + 1) * incy + 1) : 1;
            for(int i = 1; i <= n; i++)
            {
                dy[iy] += da * dx[ix];
                ix += incx;
                iy += incy;
            }
        }
        else
        {
            /*
             * Code for both increments equal to 1.
             */

            /*
             * Clean-up loop so remaining vector length is a multiple of 4.
             */
            if(incx == 1)
            {
                const int m = n % 4;
                if(m != 0)
                {
                    for(int i = 1; i <= m; i++)
                    {
                        dy[i] += da * dx[i];
                    }
                    if(n < 4)
                    {
                        return;
                    }
                }
                for(int i = m + 1; i <= n; i = i + 4)
                {
                    for(int j=0; j<4; j++)
                    {
                        dy[i+j] += da * dx[i+j];
                    }
                }
            }
            else
            {
                /*
                 * Code for equal, positive, nonunit increments.
                 */
                for(int i = 1; i <= n * incx; i = i + incx)
                {
                    dy[i] += da * dx[i];
                }
            }
        }
    }
}

/***********
 * dgesl.c *
 *
 *   Purpose : dgesl solves the linear system
 *   a * x = b or Transpose(a) * x = b
 *   using the factors computed by dgeco or degfa.
 *
 *
 *    On Entry :
 *
 *       a    : double matrix of dimension ( n+1, n+1 ),
 *              the output from dgeco or dgefa.
 *              The 0-th row and column are not used.
 *       n    : the row dimension of a.
 *       l->ipvt : the pivot vector from degco or dgefa.
 *       b    : the right hand side vector.
 *       job  : = 0       to solve a * x = b,
 *              = nonzero to solve Transpose(a) * x = b.
 *
 *
 *    On Return :
 *
 *       b : the solution vector x.
 *
 *
 *    Error Condition :
 *
 *       A division by zero will occur if the input factor contains
 *       a zero on the diagonal.  Technically this indicates
 *       singularity but it is often caused by improper argments or
 *       improper setting of the pointers of a.  It will not occur
 *       if the subroutines are called correctly and if dgeco has
 *       set l->rcond > 0 or dgefa has set info = 0.
 *
 *
 *    BLAS : daxpy, ddot
 */
static void dgesl(double ** const Restrict a,
                  const int n,
                  int * const ipvt,
                  double * const Restrict b,
                  const int job)
{
    /*
     * Job = 0, solve a * x = b.
     */
    if(job == 0)
    {
        /*
         * First solve L * y = b.
         */
        for(int k = 1; k <= n; k++)
        {
            const double t = ddot(k - 1, a[k], 1, b, 1);
            b[k] = (b[k] - t) / a[k][k];
        }
        /*
         *  Now solve U * x = y.
         */
        for(int k = n - 1; k >= 1; k--)
        {
            b[k] += ddot(n - k, a[k] + k, 1, b + k, 1);
            const int j = ipvt[k];
            if(j != k)
            {
                Swap(b[j],b[k]);
            }
        }
    }
    else
    {
        /*
         * Job = nonzero, solve Transpose(a) * x = b.
         *
         * First solve Transpose(U) * y = b.
         */
        for(int k = 1; k <= n - 1; k++)
        {
            const int j = ipvt[k];
            const double t = b[j];
            if(j != k)
            {
                b[j] = b[k];
                b[k] = t;
            }
            daxpy(n - k, t, a[k] + k, 1, b + k, 1);
        }
        /*
         *   Now solve Transpose(L) * x = y.
         */
        for(int k = n; k >= 1; k--)
        {
            b[k] /= a[k][k];
            daxpy(k - 1, -b[k], a[k], 1, b, 1);
        }
    }
}

/***********
 * dgefa.c *
 ***********/

/*
  Purpose : dgefa factors a double matrix by Gaussian elimination.

  dgefa is usually called by dgeco, but it can be called directly
  with a saving in time if l->rcond is not needed.
  (Time for dgeco) = (1+9/n)*(time for dgefa).

  This c version uses algorithm kji rather than the kij in dgefa.f.
  Note that the fortran version input variable lda is not needed.


  On Entry :

  a   : double matrix of dimension ( n+1, n+1 ),
  the 0-th row and column are not used.
  a is created using NewDoubleMatrix, hence
  lda is unnecessary.
  n   : the row dimension of a.

  On Return :

  a     : a lower triangular matrix and the multipliers
  which were used to obtain it.  The factorization
  can be written a = L * U where U is a product of
  permutation and unit upper triangular matrices
  and L is lower triangular.
  ipvt  : an n+1 integer vector of pivot indices.
  *info : = 0 normal value,
  = k if U[k][k] == 0.  This is not an error
  condition for this subroutine, but it does
  indicate that dgesl or dgedi will divide by
  zero if called.  Use l->rcond in dgeco for
  a reliable indication of singularity.

  Notice that the calling program must use &info.

  BLAS : daxpy, dscal, idamax
*/

void dgefa(double ** const Restrict a,
           const int n,
           int * const Restrict ipvt,
           int * const Restrict info)
{
    /*
     * Gaussian elimination with partial pivoting.
     */
    *info = 0;
    for(int k = 1; k <= n - 1; k++)
    {
        /*
         * Find j = pivot index.  Note that a[k]+k-1 is the address of
         * the 0-th element of the row vector whose 1st element is a[k][k].
         */
        const int j = idamax(n - k + 1, a[k] + k - 1, 1) + k - 1;
        ipvt[k] = j;
        /*
         * Zero pivot implies this row already triangularized.
         */
        if(a[k][j] == 0.0)
        {
            *info = k;
            continue;
        }
        /*
         * Interchange if necessary.
         */
        else if(j != k)
        {
            Swap(a[k][j], a[k][k]);
        }
        /*
         * Compute multipliers.
         */
        double t = -1.0 / a[k][k];
        dscal(n - k, t, a[k] + k, 1);
        /*
         * Column elimination with row indexing.
         */
        for(int i = k + 1; i <= n; i++)
        {
            t = a[i][j];
            if(j != k)
            {
                a[i][j] = a[i][k];
                a[i][k] = t;
            }
            daxpy(n - k, t, a[k] + k, 1, a[i] + k, 1);
        }
    }			/* end k-loop  */

    ipvt[n] = n;
    if(a[n][n] == 0.0)
    {
        *info = n;
    }
}

/***********
 * lsoda.c
 *
 *
 * From tam@dragonfly.wri.com Wed Apr 24 01:35:52 1991
 * Return-Path: <tam>
 * Date: Wed, 24 Apr 91 03:35:24 CDT
 * From: tam@dragonfly.wri.com
 * To: whitbeck@wheeler.wl->rc.unr.edu
 * Subject: lsoda.c
 * Cc: augenbau@spal->rc0.bl->rc.uconn.edu
 *
 *
 * I'm told by Steve Nichols at Georgia Tech that you are interested in
 * a stiff integrator.  Here's a translation of the fortran code LSODA.
 *
 * Please note
 * that there is no comment.  The interface is the same as the FORTRAN
 * code and I believe the documentation in LSODA will suffice.
 * As usual, a free software comes with no guarantee.
 *
 * Hon Wah Tam
 * Wolfram Reseal->rch, Inc.
 * tam@wri.com
 */


/*
 * Terminate lsoda due to illegal input.
 */
static void lsoda_terminate(struct lsoda_t * Restrict const l,
                            int * const istate)
{
    if(l->illin == 5)
    {
        fprintf(stderr, "[lsoda] repeated occurrence of illegal input. run aborted.. apparent infinite loop\n");
    }
    else
    {
        l->illin++;
        *istate = -3;
    }
}


/*
 * Terminate lsoda due to various error conditions.
 */
static void lsoda_terminate2(struct lsoda_t * Restrict const l,
                             double * const y,
                             double * const t)
{
    l->yp1 = l->yh[INITIAL_VALUE];
    for(int i = 1; i <= l->n; i++)
    {
        y[i] = l->yp1[i];
    }
    *t = l->tn;
    l->illin = 0;
    freevectors(l);
}

/*
  The following block handles all successful returns from lsoda.
  If itask != 1, y is loaded from yh and t is set accordingly.
  *Istate is set to 2, the illegal input counter is zeroed, and the
  optional outputs are loaded into the work arrays before returning.
*/

static void successreturn(struct lsoda_t * Restrict const l,
                          double * const y,
                          double * const t,
                          const int itask,
                          const int ihit,
                          const double tcrit,
                          int * const istate)
{
    Lprintf("successreturn itask=%d ihit=%d istate=%d\n",
           itask,ihit,*istate);
    l->yp1 = l->yh[INITIAL_VALUE];
    for(int i = 1; i <= l->n; i++)
    {
        y[i] = l->yp1[i];
    }
    *t = l->tn;
    if(ihit && (itask == 4 || itask == 5))
    {
        *t = tcrit;
    }
    *istate = 2;
    l->illin = 0;
    freevectors(l);
}

/*
  c-----------------------------------------------------------------------
  c this is the mal->rch 30, 1987 version of
  c lsoda.. livermore solver for ordinary differential equations, with
  c         automatic method switching for stiff and nonstiff problems.
  c
  c this version is in double precision.
  c
  c lsoda solves the initial value problem for stiff or nonstiff
  c systems of first order ode-s,
  c     dy/dt = f(t,y) ,  or, in component form,
  c     dy(i)/dt = f(i) = f(i,t,y(1),y(2),...,y(neq)) (i = 1,...,neq).
  c
  c this a variant version of the lsode package.
  c it switches automatically between stiff and nonstiff methods.
  c this means that the user does not have to determine whether the
  c problem is stiff or not, and the solver will automatically choose the
  c appropriate method.  it always starts with the nonstiff method.
  c
  c authors..
  c                linda r. petzold  and  alan c. hindmarsh,
  c                computing and mathematics reseal->rch division, l-316
  c                lawrence livermore national laboratory
  c                livermore, ca 94550.
  c
  c references..
  c 1.  alan c. hindmarsh,  odepack, a systematized collection of ode
  c     solvers, in scientific computing, r. s. stepleman et al. (eds.),
  c     north-holland, amsterdam, 1983, pp. 55-64.
  c 2.  linda r. petzold, automatic selection of methods for solving
  c     stiff and nonstiff systems of ordinary differential equations,
  c     siam j. sci. stat. comput. 4 (1983), pp. 136-148.
  c-----------------------------------------------------------------------
  c summary of usage.
  c
  c communication between the user and the lsoda package, for normal
  c situations, is summarized here.  this summary describes only a subset
  c of the full set of options available.  see the full description for
  c details, including alternative treatment of the jacobian matrix,
  c optional inputs and outputs, nonstandard options, and
  c instructions for special situations.  see also the example
  c problem (with program and output) following this summary.
  c
  c a. first provide a subroutine of the form..
  c               subroutine f (neq, t, y, ydot)
  c               dimension y(neq), ydot(neq)
  c which supplies the vector function f by loading ydot(i) with f(i).
  c
  c b. write a main program which calls subroutine lsoda once for
  c each point at which answers are desired.  this should also provide
  c for possible use of logical unit 6 for output of error messages
  c by lsoda.  on the first call to lsoda, supply arguments as follows..
  c f      = name of subroutine for right-hand side vector f.
  c          this name must be declared external in calling program.
  c neq    = number of first order ode-s.
  c y      = array of initial values, of length neq.
  c t      = the initial value of the independent variable.
  c tout   = first point where output is desired (.ne. t).
  c itol   = 1 or 2 according as atol (below) is a scalar or array.
  c rtol   = relative tolerance parameter (scalar).
  c atol   = absolute tolerance parameter (scalar or array).
  c          the estimated local error in y(i) will be controlled so as
  c          to be less than
  c             l->ewt(i) = rtol*abs(y(i)) + atol     if itol = 1, or
  c             l->ewt(i) = rtol*abs(y(i)) + atol(i)  if itol = 2.
  c          thus the local error test passes if, in each component,
  c          either the absolute error is less than atol (or atol(i)),
  c          or the relative error is less than rtol.
  c          use rtol = 0.0 for pure absolute error control, and
  c          use atol = 0.0 (or atol(i) = 0.0) for pure relative error
  c          control.  caution.. actual (global) errors may exceed these
  c          local tolerances, so choose them conservatively.
  c itask  = 1 for normal computation of output values of y at t = tout.
  c istate = integer flag (input and output).  set istate = 1.
  c iopt   = 0 to indicate no optional inputs used.
  c rwork  = real work array of length at least..
  c             22 + neq * Max(16, neq + 9).
  c          see also paragraph e below.
  c lrw    = declared length of rwork (in user-s dimension).
  c iwork  = integer work array of length at least  20 + neq.
  c liw    = declared length of iwork (in user-s dimension).
  c jac    = name of subroutine for jacobian matrix.
  c          use a dummy name.  see also paragraph e below.
  c jt     = jacobian type indicator.  set jt = 2.
  c          see also paragraph e below.
  c note that the main program must declare arrays y, rwork, iwork,
  c and possibly atol.
  c
  c c. the output from the first call (or any call) is..
  c      y = array of computed values of y(t) vector.
  c      t = corresponding value of independent variable (normally tout).
  c istate = 2  if lsoda was successful, negative otherwise.
  c          -1 means excess work done on this call (perhaps wrong jt).
  c          -2 means excess accuracy requested (tolerances too small).
  c          -3 means illegal input detected (see printed message).
  c          -4 means repeated error test failures (check all inputs).
  c          -5 means repeated convergence failures (perhaps bad jacobian
  c             supplied or wrong choice of jt or tolerances).
  c          -6 means error weight became zero during problem. (solution
  c             component i vanished, and atol or atol(i) = 0.0)
  c          -7 means work space insufficient to finish (see messages).
  c
  c d. to continue the integration after a successful return, simply
  c reset tout and call lsoda again.  no other parameters need be reset.
  c
  c e. note.. if and when lsoda regards the problem as stiff, and
  c switches methods accordingly, it must make use of the neq by neq
  c jacobian matrix, j = df/dy.  for the sake of simplicity, the
  c inputs to lsoda recommended in paragraph b above cause lsoda to
  c treat j as a full matrix, and to approximate it internally by
  c difference quotients.  alternatively, j can be treated as a band
  c matrix (with great potential reduction in the size of the rwork
  c array).  also, in either the full or banded case, the user can supply
  c j in closed form, with a routine whose name is passed as the jac
  c argument.  these alternatives are described in the paragraphs on
  c rwork, jac, and jt in the full description of the call sequence below.
  c
  c-----------------------------------------------------------------------
*/

void lsoda_setup(struct lsoda_t * Restrict const l)
{
    /*
     * RGI new function
     *
     * Set up lsoda struct variables.
     */
    l->g_nyh = 0;
    l->g_lenyh = 0;
    l->mord[0] = 0;
    l->mord[1] = 12;
    l->mord[2] = 5;
    l->illin = 0;
    l->init = 0;
    l->ixpr = 0;
    l->ntrep = 0;

    const double sm1[13] = {0.0,
                            0.5, 0.575, 0.55, 0.45, 0.35, 0.25, 0.2, 0.15, 0.1, 0.075, 0.05, 0.025};
    memcpy(l->sm1,sm1,sizeof(double)*13);

    /* and force pointers to be NULL */
    l->Jacobian = NULL;
    l->yh = NULL;
    l->wm = NULL;
    l->ewt = NULL;
    l->savf = NULL;
    l->acor = NULL;
    l->ipvt = NULL;
    l->yp1 = NULL;
    l->yp2 = NULL;
}


void lsoda(struct lsoda_t * Restrict const l,
           _lsoda_f f,
           int neq,
           double *y,
           double *t,
           double tout,
           int itol,
           double *rtol,
           double *atol,
           int itask,
           int *istate,
           int iopt,
           int jt,
           int iwork1,
           int iwork2,
           int iwork5,
           int iwork6,
           int iwork7,
           int iwork8,
           int iwork9,
           double rwork1,
           double rwork5,
           double rwork6,
           double rwork7,
           void *_data)
/*
  If the user does not supply any of these values, the calling program
  should initialize those untouched working variables to zero.

  ml = iwork1
  mu = iwork2
  l->ixpr = iwork5
  l->mxstep = iwork6
  l->mxhnil = iwork7
  l->mxordn = iwork8
  l->mxords = iwork9

  tcrit = rwork1
  h0 = rwork5
  hmax = rwork6
  l->hmin = rwork7
*/
{
    const int mxstp0 = l->max_steps;
    const int mxhnl0 = 10;
    int iflag, lenyh, ihit = 0;
    double atoli, ayi, big, h0 = 0.0, hmax,
        hmx, rh, rtoli, tcrit = 0.0, tdist, tnext, tol,
        tolsf, tp, size, sum, w0;

    if(*istate == 1)
    {
        _freevectors(l);
    }

    /*
      Block a.
      This code block is executed on every call.
      It tests *istate and itask for legality and branches appropriately.
      If *istate > 1 but the flag init shows that initialization has not
      yet been done, an error return occurs.
      If *istate = 1 and tout = t, return immediately.
    */
    if(*istate < 1 || *istate > 3)
    {
        fprintf(stderr, "[lsoda] illegal istate = %d\n", *istate);
        lsoda_terminate(l,istate);
        return;
    }
    else if(itask < 1 || itask > 5)
    {
        fprintf(stderr, "[lsoda] illegal itask = %d\n", itask);
        lsoda_terminate(l,istate);
        return;
    }
    else if(l->init == 0 && (*istate == 2 || *istate == 3))
    {
        fprintf(stderr, "[lsoda] istate > 1 but lsoda not initialized\n");
        lsoda_terminate(l,istate);
        return;
    }

    if(*istate == 1)
    {
        l->init = 0;
        if(tout == *t)
        {
            l->ntrep++;
            if(l->ntrep >= 5)
            {
                fprintf(stderr,
                        "[lsoda] repeated calls with istate = 1 and tout = t. run aborted.. apparent infinite loop\n");
            }
            return;
        }
    }

    /*
      Block b.
      The next code block is executed for the initial call ( *istate = 1 ),
      or for a continuation call with parameter changes ( *istate = 3 ).
      It contains checking of all inputs and various initializations.

      First check legality of the non-optional inputs neq, itol, iopt,
      jt, ml, and mu.
    */
    if(*istate == 1 || *istate == 3)
    {
        l->ntrep = 0;
        if(neq <= 0)
        {
            fprintf(stderr, "[lsoda] neq = %d is less than 1\n", neq);
            lsoda_terminate(l,istate);
            return;
        }
        else if(*istate == 3 && neq > l->n)
        {
            fprintf(stderr, "[lsoda] istate = 3 and neq increased\n");
            lsoda_terminate(l,istate);
            return;
        }
        l->n = neq;
        if(itol < 1 || itol > 4)
        {
            fprintf(stderr, "[lsoda] itol = %d illegal\n", itol);
            lsoda_terminate(l,istate);
            return;
        }
        else if(iopt < 0 || iopt > 1)
        {
            fprintf(stderr, "[lsoda] iopt = %d illegal\n", iopt);
            lsoda_terminate(l,istate);
            return;
        }
        else if(jt == 3 || jt < 1 || jt > 5)
        {
            fprintf(stderr, "[lsoda] jt = %d illegal\n", jt);
            lsoda_terminate(l,istate);
            return;
        }
        l->jtyp = jt;
        if(jt > 2)
        {
            l->ml = iwork1;
            l->mu = iwork2;
            if(l->ml < 0 || l->ml >= l->n)
            {
                fprintf(stderr, "[lsoda] l->ml = %d not between 1 and neq\n", l->ml);
                lsoda_terminate(l,istate);
                return;
            }
            else if(l->mu < 0 || l->mu >= l->n)
            {
                fprintf(stderr, "[lsoda] l->mu = %d not between 1 and neq\n", l->mu);
                lsoda_terminate(l,istate);
                return;
            }
        }

        /* Next process and check the optional inputs.   */

        /* Default options.   */
        if(iopt == 0)
        {
            l->ixpr = 0;
            l->mxstep = mxstp0;
            l->mxhnil = mxhnl0;
            l->hmxi = 0.0;
            l->hmin = 0.0;
            if(*istate == 1)
            {
                h0 = 0.0;
                l->mxordn = l->mord[1];
                l->mxords = l->mord[2];
            }
        }
        /* end if( iopt == 0 )   */
        /* Optional inputs.   */
        else
        {
            /* if( iopt = 1 )  */
            l->ixpr = iwork5;
            if(l->ixpr < 0 || l->ixpr > 1)
            {
                fprintf(stderr, "[lsoda] l->ixpr = %d is illegal\n", l->ixpr);
                lsoda_terminate(l,istate);
                return;
            }
            l->mxstep = iwork6;
            if(l->mxstep < 0)
            {
                fprintf(stderr, "[lsoda] l->mxstep < 0\n");
                lsoda_terminate(l,istate);
                return;
            }
            else if(l->mxstep == 0)
            {
                l->mxstep = mxstp0;
            }
            l->mxhnil = iwork7;
            if(l->mxhnil < 0)
            {
                fprintf(stderr, "[lsoda] l->mxhnil < 0\n");
                lsoda_terminate(l,istate);
                return;
            }
            else if(*istate == 1)
            {
                h0 = rwork5;
                l->mxordn = iwork8;
                if(l->mxordn < 0)
                {
                    fprintf(stderr, "[lsoda] l->mxordn = %d is less than 0\n", l->mxordn);
                    lsoda_terminate(l,istate);
                    return;
                }
                else if(l->mxordn == 0)
                {
                    l->mxordn = 100;
                }
                l->mxordn = Min(l->mxordn, l->mord[1]);
                l->mxords = iwork9;
                if(l->mxords < 0)
                {
                    fprintf(stderr, "[lsoda] l->mxords = %d is less than 0\n", l->mxords);
                    lsoda_terminate(l,istate);
                    return;
                }
                else if(l->mxords == 0)
                {
                    l->mxords = 100;
                }
                l->mxords = Min(l->mxords, l->mord[2]);
                if((tout - *t) * h0 < 0.0)
                {
                    fprintf(stderr, "[lsoda] tout = %g behind t = %g. integration direction is given by %g\n",
                            tout, *t, h0);
                    lsoda_terminate(l,istate);
                    return;
                }
            } /* end if( *istate == 1 )  */

            hmax = rwork6;
            if(hmax < 0.0)
            {
                fprintf(stderr, "[lsoda] hmax < 0.0\n");
                lsoda_terminate(l,istate);
                return;
            }
            l->hmxi = 0.0;
            if(hmax > 0)
            {
                l->hmxi = 1.0 / hmax;
            }
            l->hmin = rwork7;
            if(l->hmin < 0.0)
            {
                fprintf(stderr, "[lsoda] l->hmin < 0.0\n");
                lsoda_terminate(l,istate);
                return;
            }
        }		/* end else   *//* end iopt = 1   */
    }			/* end if( *istate == 1 || *istate == 3 )   */
    /*
      If *istate = 1, l->meth is initialized to 1.

      Also allocate memory for yh, wm, l->ewt, l->savf, acor, l->ipvt.
    */
    if(*istate == 1)
    {
        /*
         * If memory were not freed, *istate = 3 need not reallocate memory.
         * Hence this section is not executed by *istate = 3.
         */
        l->sqrteta = sqrt(ETA);
        l->meth = METHOD_NONSTIFF;

        l->nyh = l->n;
        l->g_nyh = l->n;
        lenyh = 1 + Max(l->mxordn,
                        l->mxords);
        l->g_lenyh = lenyh;

        l->yh = (double **) Calloc(1 + lenyh,
                                   sizeof(*(l->yh)));
        if(l->yh == NULL)
        {
            printf("lsoda -- insufficient memory for your problem\n");
            lsoda_terminate(l,istate);
            return;
        }
        for(int i = 1; i <= lenyh; i++)
        {
            l->yh[i] = (double *) Calloc(1 + l->nyh, sizeof(double));
        }
        l->wm = (double **) Calloc(1 + l->nyh, sizeof(*(l->wm)));
        if(l->wm == NULL)
        {
            Safe_free(l->yh);
            printf("lsoda -- insufficient memory for your problem\n");
            lsoda_terminate(l,istate);
            return;
        }
        for(int i = 1; i <= l->nyh; i++)
        {
            l->wm[i] = (double *) Calloc(1 + l->nyh, sizeof(double));
        }
        l->ewt = (double *) Calloc(1 + l->nyh, sizeof(double));
        if(l->ewt == NULL)
        {
            Safe_free(l->yh);
            Safe_free(l->wm);
            printf("lsoda -- insufficient memory for your problem\n");
            lsoda_terminate(l,istate);
            return;
        }
        l->savf = (double *) Calloc(1 + l->nyh, sizeof(double));
        if(l->savf == NULL)
        {
            Safe_free(l->yh);
            Safe_free(l->wm);
            Safe_free(l->ewt);
            printf("lsoda -- insufficient memory for your problem\n");
            lsoda_terminate(l,istate);
            return;
        }
        l->acor = (double *) Calloc(1 + l->nyh, sizeof(double));
        if(l->acor == NULL)
        {
            Safe_free(l->yh);
            Safe_free(l->wm);
            Safe_free(l->ewt);
            Safe_free(l->savf);
            printf("lsoda -- insufficient memory for your problem\n");
            lsoda_terminate(l,istate);
            return;
        }
        l->ipvt = (int *) Calloc(1 + l->nyh, sizeof(int));
        if(l->ipvt == NULL)
        {
            Safe_free(l->yh);
            Safe_free(l->wm);
            Safe_free(l->ewt);
            Safe_free(l->savf);
            Safe_free(l->acor);
            printf("lsoda -- insufficient memory for your problem\n");
            lsoda_terminate(l,istate);
            return;
        }
    }
    /*
     * Check rtol and atol for legality.
     */
    if(*istate == 1 || *istate == 3)
    {
        rtoli = rtol[1];
        atoli = atol[1];
        for(int i = 1; i <= l->n; i++)
        {
            if(itol >= 3)
            {
                rtoli = rtol[i];
            }
            if(itol == 2 || itol == 4)
            {
                atoli = atol[i];
            }
            if(rtoli < 0.0)
            {
                fprintf(stderr, "[lsoda] rtol = %g is less than 0.0\n", rtoli);
                lsoda_terminate(l,istate);
                freevectors(l);
                return;
            }
            if(atoli < 0.0)
            {
                fprintf(stderr, "[lsoda] atol = %g is less than 0.0\n", atoli);
                lsoda_terminate(l,istate);
                freevectors(l);
                return;
            }
        }		/* end for   */
    }			/* end if( *istate == 1 || *istate == 3 )   */
    /*
      If *istate = 3, set flag to signal parameter changes to stoda.
    */
    if(*istate == 3)
    {
        l->jstart = -1;
    }
/*
  Block c.
  The next block is for the initial call only ( *istate = 1 ).
  It contains all remaining initializations, the initial call to f,
  and the calculation of the initial step size.
  The error weights in l->ewt are inverted after being loaded.
*/
    if(*istate == 1)
    {
        l->tn = *t;
        l->tsw = *t;
        l->maxord = l->mxordn;
        if(itask == 4 || itask == 5)
        {
            tcrit = rwork1;
            if((tcrit - tout) * (tout - *t) < 0.0)
            {
                fprintf(stderr, "[lsoda] itask = 4 or 5 and tcrit behind tout\n");
                lsoda_terminate(l,istate);
                freevectors(l);
                return;
            }
            if(h0 != 0.0 && (*t + h0 - tcrit) * h0 > 0.0)
            {
                h0 = tcrit - *t;
            }
        }
        l->jstart = 0;
        l->nhnil = 0;
        l->nst = 0;
        l->nje = 0;
        l->nslast = 0;
        l->hu = 0.0;
        l->nqu = 0;
        l->mused = 0;
        l->miter = 0;
        l->ccmax = 0.3;
        l->maxcor = 3;
        l->msbp = 20;
        l->mxncf = 10;
        /*
         * Initial call to f.
         */

        if(0)
        {
            {
                double * yy = l->yh[DERIVATIVES] + 1;
                for(int i=0;i<7;i++)
                {
                    printf("in lsoda derivative [%d] = %g\n",i,yy[i]);
                }
            }
            {
                for(int i=1;i<=7;i++)
                {
                    printf("in lsoda value [%d] = %g\n",i,y[i]);
                }
            }

            printf("call derivs function with t=%g y=%p dydt=%p data=%p\n",
                   *t, (void*)(y + 1), (void*)(l->yh[DERIVATIVES] + 1), (void*)_data);
        }
        (*f) (*t, y + 1, l->yh[DERIVATIVES] + 1, _data);

        l->nfe = 1;

        /*
         * Load the initial value vector in yh.
         */
        l->yp1 = l->yh[INITIAL_VALUE];
        for(int i = 1; i <= l->n; i++)
        {
            l->yp1[i] = y[i];
        }

        /*
         * Load and invert the l->ewt array.  ( h is temporarily set to 1.0 )
         */
        l->nq = 1;
        l->h = 1.0;
        ewset(l,itol, rtol, atol, y);
        for(int i = 1; i <= l->n; i++)
        {
            if(l->ewt[i] <= 0.0)
            {
                fprintf(stderr, "[lsoda] l->ewt[%d] = %g <= 0.0\n", i, l->ewt[i]);
                lsoda_terminate2(l,y, t);
                return;
            }
            l->ewt[i] = 1.0 / l->ewt[i];
        }

        /*
          The code below computes the step size, h0, to be attempted on the
          first step, unless the user has supplied a value for this.
          First check that tout - *t differs significantly from zero.
          A scalar tolerance quantity tol is computed, as Max(rtol[i])
          if this is positive, or Max(atol[i]/fabs(y[i])) otherwise, adjusted
          so as to be between 100*ETA and 0.001.
          Then the computed value h0 is given by

          h0^(-2) = 1.0 / ( tol * w0^2 ) + tol * ( norm(f) )^2

          where   w0     = Max( fabs(*t), fabs(tout) ),
          f      = the initial value of the vector f(t,y), and
          norm() = the weighted vector norm used throughout, given by
          the vmnorm function routine, and weighted by the
          tolerances initially loaded into the l->ewt array.

          The sign of h0 is il->nferred from the initial values of tout and *t.
          fabs(h0) is made < fabs(tout-*t) in any case.
        */
        if(h0 == 0.0)
        {
            tdist = fabs(tout - *t);
            w0 = Max(fabs(*t),
                     fabs(tout));

            if(tdist < 2.0 * ETA * w0)
            {
                fprintf(stderr, "[lsoda] tout too close to t to start integration\n ");
                lsoda_terminate(l,istate);
                freevectors(l);
                return;
            }
            tol = rtol[1];
            if(itol > 2)
            {
                for(int i = 2; i <= l->n; i++)
                {
                    tol = Max(tol, rtol[i]);
                }
            }
            if(tol <= 0.0)
            {
                atoli = atol[1];
                for(int i = 1; i <= l->n; i++)
                {
                    if(itol == 2 || itol == 4)
                        atoli = atol[i];
                    ayi = fabs(y[i]);
                    if(ayi != 0.0)
                        tol = Max(tol, atoli / ayi);
                }
            }
            tol = Min(0.001,Max(tol, 100.0 * ETA));

            sum = vmnorm(l->n, l->yh[DERIVATIVES], l->ewt);
            sum = 1.0 / (tol * w0 * w0) + tol * sum * sum;
            h0 = Min(1.0 / sqrt(sum), tdist);
            h0 = h0 * ((tout - *t >= 0.0) ? 1.0 : -1.0);
        }		/* end if( h0 == 0. )   */

        /*
         * Adjust h0 if necessary to meet hmax bound.
         */
        rh = fabs(h0) * l->hmxi;
        if(rh > 1.0)
        {
            h0 /= rh;
        }

        /*
         * Load h with h0 and scale yh[DERIVATIVES] by h0.
         */
        l->h = h0;
        Lprintf("H5 %g\n",l->h);
        l->yp1 = l->yh[DERIVATIVES];
        for(int i = 1; i <= l->n; i++)
        {
            l->yp1[i] *= h0;
        }
    }			/* if( *istate == 1 )   */
    /*
      Block d.
      The next code block is for continuation calls only ( *istate = 2 or 3 )
      and is to check stop conditions before taking a step.
    */
    if(*istate == 2 || *istate == 3)
    {
        l->nslast = l->nst;
        switch (itask)
        {
        case 1:
            if((l->tn - tout) * l->h >= 0.0)
            {
                intdy(l,tout, 0, y, &iflag);
                if(iflag != 0)
                {
                    fprintf(stderr, "[lsoda] trouble from intdy, itask = %d, tout = %g\n", itask, tout);
                    lsoda_terminate(l,istate);
                    freevectors(l);
                    return;
                }
                *t = tout;
                *istate = 2;
                l->illin = 0;
                freevectors(l);
                return;
            }
            break;
        case 2:
            break;
        case 3:
            tp = l->tn - l->hu * (1.0 + 100.0 * ETA);
            if((tp - tout) * l->h > 0.0)
            {
                fprintf(stderr, "[lsoda] itask = %d and tout behind tcur - hu\n", itask);
                lsoda_terminate(l,istate);
                freevectors(l);
                return;
            }
            if((l->tn - tout) * l->h < 0.0)
            {
                break;
            }
            successreturn(l,y, t, itask, ihit, tcrit, istate);
            return;
        case 4:
            tcrit = rwork1;
            if((l->tn - tcrit) * l->h > 0.0)
            {
                fprintf(stderr, "[lsoda] itask = 4 or 5 and tcrit behind tcur\n");
                lsoda_terminate(l,istate);
                freevectors(l);
                return;
            }
            if((tcrit - tout) * l->h < 0.0)
            {
                fprintf(stderr, "[lsoda] itask = 4 or 5 and tcrit behind tout\n");
                lsoda_terminate(l,istate);
                freevectors(l);
                return;
            }
            if((l->tn - tout) * l->h >= 0.0)
            {
                intdy(l,tout, 0, y, &iflag);
                if(iflag != 0)
                {
                    fprintf(stderr, "[lsoda] trouble from intdy, itask = %d, tout = %g\n", itask, tout);
                    lsoda_terminate(l,istate);
                }
                else
                {
                    *t = tout;
                    *istate = 2;
                    l->illin = 0;
                }
                freevectors(l);
                return;
            }
            Fallthrough; /* FALLTHROUGH */
        case 5:
            if(itask == 5)
            {
                tcrit = rwork1;
                if((l->tn - tcrit) * l->h > 0.0)
                {
                    fprintf(stderr, "[lsoda] itask = 4 or 5 and tcrit behind tcur\n");
                    lsoda_terminate(l,istate);
                    freevectors(l);
                    return;
                }
            }
            hmx = fabs(l->tn) + fabs(l->h);
            ihit = fabs(l->tn - tcrit) <= (100.0 * ETA * hmx);
            if(ihit)
            {
                *t = tcrit;
                successreturn(l,y, t, itask, ihit, tcrit, istate);
                return;
            }
            tnext = l->tn + l->h * (1.0 + 4.0 * ETA);
            if((tnext - tcrit) * l->h <= 0.0)
            {
                break;
            }
            l->h = (tcrit - l->tn) * (1.0 - 4.0 * ETA);
            Lprintf("H1 %g\n",l->h);
            if(*istate == 2)
            {
                l->jstart = -2;
            }
            break;
        }		/* end switch   */
    }			/* end if( *istate == 2 || *istate == 3 )   */
    /*
      Block e.
      The next block is normally executed for all calls and contains
      the call to the one-step core integrator stoda.

      This is a looping point for the integration steps.

      First check for too many steps being taken, update l->ewt ( if not at
      start of problem).  Check for too much accuracy being requested, and
      check for h below the roundoff level in *t.
    */
    Loop_forever
    {
        if(*istate != 1 || l->nst != 0)
        {
            if((l->nst - l->nslast) >= l->mxstep)
            {
                fprintf(stderr, "[lsoda] %d steps taken before reaching tout = %g\n", l->mxstep, tout);
                *istate = -1;
                lsoda_terminate2(l,y, t);
                return;
            }
            ewset(l,itol, rtol, atol, l->yh[INITIAL_VALUE]);
            for(int i = 1; i <= l->n; i++)
            {
                if(l->ewt[i] <= 0.0)
                {
                    fprintf(stderr, "[lsoda] l->ewt[%d] = %g <= 0.0\n", i, l->ewt[i]);
                    *istate = -6;
                    lsoda_terminate2(l,y, t);
                    return;
                }
                l->ewt[i] = 1.0 / l->ewt[i];
            }
        }
        tolsf = ETA * vmnorm(l->n, l->yh[INITIAL_VALUE], l->ewt);
        if(tolsf > 0.01)
        {
            tolsf *= 200.0;
            if(l->nst == 0)
            {
                fprintf(stderr, "lsoda -- at start of problem, too much accuracy\n");
                fprintf(stderr, "         requested for precision of machine,\n");
                fprintf(stderr, "         suggested scaling factor = %g\n", tolsf);
                lsoda_terminate(l,istate);
                freevectors(l);
                return;
            }
            fprintf(stderr, "lsoda -- at t = %g, too much accuracy requested\n", *t);
            fprintf(stderr, "         for precision of machine, suggested\n");
            fprintf(stderr, "         scaling factor = %g\n", tolsf);
            *istate = -2;
            lsoda_terminate2(l,y, t);
            return;
        }
        if((l->tn + l->h) == l->tn)
        {
            l->nhnil++;
            if(l->nhnil <= l->mxhnil)
            {
                fprintf(stderr, "lsoda -- warning..internal t = %g and h = %g are\n", l->tn, l->h);
                fprintf(stderr, "         such that in the machine, t + h = t on the next step\n");
                fprintf(stderr, "         solver will continue anyway.\n");
                if(l->nhnil == l->mxhnil)
                {
                    fprintf(stderr, "lsoda -- above warning has been issued %d times,\n", l->nhnil);
                    fprintf(stderr, "         it will not be issued again for this problem\n");
                }
            }
        }

        /*
         * Call stoda
         */
        Lprintf("call stoda\n");
        stoda(l,neq, y, f, _data);
        Lprintf("back from stoda\n");

        Lprintf( "l->meth= %d,   order= %d,   l->nfe= %d,   l->nje= %d\n",
                l->meth,
                l->nq,
                l->nfe,
                l->nje );
        Lprintf( "t= %20.15e,   h= %20.15e,   nst=%d\n", l->tn, l->h, l->nst );
        Lprintf( "STODA at %g y= %20.15e,   %20.15e,   %20.15e\n",
                l->tn,
                l->yh[INITIAL_VALUE][1],
                l->yh[INITIAL_VALUE][2],
                l->yh[INITIAL_VALUE][3] );
        Lprintf("kflag = %d : success ? %s\n",
               l->kflag,
               Yesno(l->kflag == 0));

        if(l->kflag == 0)
        {
            /*
              Block f.
              The following block handles the case of a successful return from the
              core integrator ( l->kflag = 0 ).
              If a method switch was just made, record l->tsw, reset l->maxord,
              set l->jstart to -1 to signal stoda to complete the switch,
              and do extra printing of data if l->ixpr = 1.
              Then, in any case, check for stop conditions.
            */
            l->init = 1;
            if(l->meth != l->mused)
            {
                l->tsw = l->tn;
                l->maxord = l->mxordn;
                if(l->meth == METHOD_STIFF)
                {
                    l->maxord = l->mxords;
                }

                l->jstart = -1;

                if(l->ixpr)
                {
                    if(l->meth == METHOD_STIFF)
                    {
                        fprintf(stderr, "[lsoda] a switch to the stiff method has occurred ");
                    }
                    else if(l->meth == METHOD_NONSTIFF)
                    {
                        fprintf(stderr, "[lsoda] a switch to the nonstiff method has occurred");
                    }
                    fprintf(stderr, "at t = %g, tentative step size h = %g, step nst = %d\n", l->tn, l->h, l->nst);
                }
            }	/* end if( l->meth != l->mused )   */
            /*
             * itask = 1.
             * If tout has been reached, interpolate.
             */
            Lprintf("itask %d\n",itask);
            if(itask == 1)
            {
                if((l->tn - tout) * l->h < 0.0)
                {
                    continue;
                }
                intdy(l,tout, 0, y, &iflag);
                *t = tout;
                *istate = 2;
                l->illin = 0;
                freevectors(l);
                return;
            }
            /*
             * itask = 2.
             * Success.
             */
            else if(itask == 2)
            {
                printf("success return at t = %g (cf. tout %g)\n",*t,tout);
                successreturn(l,y, t, itask, ihit, tcrit, istate);
                return;
            }
            /*
             * itask = 3.
             * Jump to exit if tout was reached.
             */
            else if(itask == 3)
            {
                if((l->tn - tout) * l->h >= 0.0)
                {
                    successreturn(l,y, t, itask, ihit, tcrit, istate);
                    return;
                }
                continue;
            }

            /*
             * itask = 4.
             * See if tout or tcrit was reached.  Adjust h if necessary.
             */
            else if(itask == 4)
            {
                if((l->tn - tout) * l->h >= 0.0)
                {
                    intdy(l,tout, 0, y, &iflag);
                    *t = tout;
                    *istate = 2;
                    l->illin = 0;
                    freevectors(l);
                    return;
                }
                else
                {
                    hmx = fabs(l->tn) + fabs(l->h);
                    ihit = fabs(l->tn - tcrit) <= (100.0 * ETA * hmx);
                    if(ihit)
                    {
                        successreturn(l,y, t, itask, ihit, tcrit, istate);
                        return;
                    }
                    tnext = l->tn + l->h * (1.0 + 4.0 * ETA);
                    if((tnext - tcrit) * l->h > 0.0)
                    {
                        l->h = (tcrit - l->tn) * (1.0 - 4.0 * ETA);
                        Lprintf("H2 %g\n",l->h);
                        l->jstart = -2;
                    }
                    continue;
                }
            }	/* end if( itask == 4 )   */
            /*
              itask = 5.
              See if tcrit was reached and jump to exit.
            */
            else if(itask == 5)
            {
                hmx = fabs(l->tn) + fabs(l->h);
                ihit = fabs(l->tn - tcrit) <= (100.0 * ETA * hmx);
                successreturn(l,y, t, itask, ihit, tcrit, istate);
                return;
            }
        }		/* end if( l->kflag == 0 )   */
        /*
          l->kflag = -1, error test failed repeatedly or with fabs(h) = l->hmin.
          l->kflag = -2, convergence failed repeatedly or with fabs(h) = l->hmin.
        */
        if(l->kflag == -1 || l->kflag == -2)
        {
            fprintf(stderr, "lsoda -- at t = %g and step size h = %g, the\n", l->tn, l->h);
            if(l->kflag == -1)
            {
                fprintf(stderr, "         error test failed repeatedly or\n");
                fprintf(stderr, "         with fabs(h) = l->hmin\n");
                *istate = -4;
            }
            if(l->kflag == -2)
            {
                fprintf(stderr, "         corrector convergence failed repeatedly or\n");
                fprintf(stderr, "         with fabs(h) = l->hmin\n");
                *istate = -5;
            }
            big = 0.0;
            l->imxer = 1;
            for(int i = 1; i <= l->n; i++)
            {
                size = fabs(l->acor[i]) * l->ewt[i];
                if(big < size)
                {
                    big = size;
                    l->imxer = i;
                }
            }
            lsoda_terminate2(l,y, t);
            return;
        }		/* end if( l->kflag == -1 || l->kflag == -2 )   */
    }			/* end while   */
}				/* end lsoda   */


static void stoda(struct lsoda_t * Restrict const l,
                  int neq,
                  double *y,
                  _lsoda_f f,
                  void *_data)
{
    int corflag, orderflag;
    int m, ncf;
    double del, delp, dsm=0.0, dup, exup, r, rh, rhup, told;
    double pdh, pnorm;

/*
  stoda performs one step of the integration of an initial value
  problem for a system of ordinary differential equations.
  Note.. stoda is independent of the value of the iteration method
  indicator l->miter, when this is != 0, and hence is independent
  of the type of chord method used, or the Jacobian structure.
  Communication with stoda is done with the following variables:

  l->jstart = an integer used for input only, with the following
  values and meanings:

  0  perform the first step,
  > 0  take a new step continuing from the last,
  -1  take the next step with a new value of h,
  n, l->meth, l->miter, and/or matrix parameters.
  -2  take the next step with a new value of h,
  but with other inputs unchanged.

  l->kflag = a completion code with the following meanings:

  0  the step was successful,
  -1  the requested error could not be achieved,
  -2  corrector convergence could not be achieved,
  -3  fatal error in prja or solsy.

  l->miter = corrector iteration method:

  0  functional iteration,
  >0  a chord method corresponding to jacobian type jt.

*/
    Lprintf("STODA l->jstart %d \n",
            l->jstart);

    l->kflag = 0;
    told = l->tn;
    ncf = 0;
    l->ierpj = 0;
    l->iersl = 0;
    l->jcur = 0;
    delp = 0.0;

    /*
      On the first call, the order is set to 1, and other variables are
      initialized.  l->rmax is the maximum l->ratio by which h can be increased
      in a single step.  It is initially 1.0e4 to compensate for the small
      initial h, but then is normally equal to 10.  If a failure occurs
      (in corrector convergence or error test), l->rmax is set at 2 for
      the next increase.
      cfode is called to get the needed coefficients for both methods.
    */
    if(l->jstart == 0)
    {
        l->lmax = l->maxord + 1;
        l->nq = 1;
        l->l = 2;
        l->ialth = 2;
        l->rmax = 10000.0;
        l->rc = 0.0;
        l->el0 = 1.0;
        l->crate = 0.7;
        l->hold = l->h;
        l->nslp = 0;
        l->ipup = l->miter;
        /*
         * Initialize switching parameters.
         * l->meth = METHOD_NONSTIFF is assumed initially.
         */
        l->icount = 20;
        l->irflag = 0;
        l->pdest = 0.0;
        l->pdlast = 0.0;
        l->ratio = 5.0;
        cfode(l,2);
        for(int i = 1; i <= 5; i++)
        {
            l->cm2[i] = l->tesco[i][2] * l->elco[i][i + 1];
        }
        cfode(l,1);
        for(int i = 1; i <= 12; i++)
        {
            l->cm1[i] = l->tesco[i][2] * l->elco[i][i + 1];
        }
        resetcoeff(l);
    }			/* end if( l->jstart == 0 )   */
    /*
      The following block handles preliminaries needed when l->jstart = -1.
      l->ipup is set to l->miter to force a matrix update.
      If an order increase is about to be considered ( l->ialth = 1 ),
      l->ialth is reset to 2 to postpone considel->ration one more step.
      If the caller has changed l->meth, cfode is called to reset
      the coefficients of the method.
      If h is to be changed, yh must be rescaled.
      If h or l->meth is being changed, l->ialth is reset to l = nq + 1
      to prevent further changes in h for that many steps.
    */
    if(l->jstart == -1)
    {
        l->ipup = l->miter;
        l->lmax = l->maxord + 1;
        if(l->ialth == 1)
        {
            l->ialth = 2;
        }
        if(l->meth != l->mused)
        {
            cfode(l,l->meth);
            l->ialth = l->l;
            resetcoeff(l);
        }
        if(l->h != l->hold)
        {
            rh = l->h / l->hold;
            l->h = l->hold;
            Lprintf("H3 %g\n",l->h);
            scaleh(l,&rh, &pdh);
        }
    }			/* if( l->jstart == -1 )   */
    if(l->jstart == -2)
    {
        if(l->h != l->hold)
        {
            rh = l->h / l->hold;
            l->h = l->hold;
            Lprintf("H4 %g\n",l->h);
            scaleh(l,&rh, &pdh);
        }
    }			/* if( l->jstart == -2 )   */
    /*
      Prediction.
      This section computes the predicted values by effectively
      multiplying the yh array by the pascal triangle matrix.
      l->rc is the l->ratio of new to old values of the coefficient h * el[1].
      When l->rc differs from 1 by more than ccmax, l->ipup is set to l->miter
      to force pjac to be called, if a jacobian is involved.
      In any case, prja is called at least every l->msbp steps.
    */
    Loop_forever
    {
        Loop_forever
        {
            if((fabs(l->rc - 1.0) > l->ccmax)||
               (l->nst >= l->nslp + l->msbp))
            {
                l->ipup = l->miter;
            }
            l->tn += l->h;
            for(int j = l->nq; j >= 1; j--)
            {
                for(int i1 = j; i1 <= l->nq; i1++)
                {
                    l->yp1 = l->yh[i1];
                    l->yp2 = l->yh[i1 + 1];
                    for(int i = 1; i <= l->n; i++)
                    {
                        l->yp1[i] += l->yp2[i];
                    }
                }
            }
            pnorm = vmnorm(l->n, l->yh[INITIAL_VALUE], l->ewt);

            correction(l,neq, y, f, &corflag, pnorm, &del, &delp, &told, &ncf, &rh, &m, _data);
            if(corflag == 0)
            {
                break;
            }
            else if(corflag == 1)
            {
                rh = Max(rh, l->hmin / fabs(l->h));
                scaleh(l,&rh, &pdh);
                continue;
            }
            else if(corflag == 2)
            {
                l->kflag = -2;
                l->hold = l->h;
                l->jstart = 1;
                return;
            }
        }		/* end inner while ( corrector loop )   */


        /*
         * The corrector has converged.  l->jcur is set to 0
         * to signal that the Jacobian involved may need updating later.
         * The local error test is done now.
         */
        l->jcur = 0;
        if(m == 0)
        {
            dsm = del / l->tesco[l->nq][2];
        }
        else if(m > 0)
        {
            dsm = vmnorm(l->n, l->acor, l->ewt) / l->tesco[l->nq][2];
        }
        if(dsm <= 1.0)
        {
            /*
              After a successful step, update the yh array.
              Decrease l->icount by 1, and if it is -1, consider switching methods.
              If a method switch is made, reset various parameters,
              rescale the yh array, and exit.  If there is no switch,
              consider changing h if l->ialth = 1.0  Otherwise decrease l->ialth by 1.
              If l->ialth is then 1 and nq < l->maxord, then l->acor is saved for
              use in a possible order increase on the next step.
              If a change in h is considered, an increase or decrease in order
              by one is considered also.  A change in h is made only if it is by
              a factor of at least 1.1.0  If not, l->ialth is set to 3 to prevent
              testing for that many steps.
            */
            Lprintf("update yh\n");
            l->kflag = 0;
            l->nst++;
            l->hu = l->h;
            l->nqu = l->nq;
            l->mused = l->meth;
            for(int j = 1; j <= l->l; j++)
            {
                l->yp1 = l->yh[j];
                r = l->el[j];
                for(int i = 1; i <= l->n; i++)
                {
                    l->yp1[i] += r * l->acor[i];
                }
            }
            l->icount--;
            if(l->icount < 0)
            {
                methodswitch(l,dsm, pnorm, &pdh, &rh);
                if(l->meth != l->mused)
                {
                    rh = Max(rh, l->hmin / fabs(l->h));
                    scaleh(l,&rh, &pdh);
                    l->rmax = 10.0;
                    endstoda(l);
                    break;
                }
            }
            /*
             * No method switch is being made.
             * Do the usual step/order selection.
             */
            l->ialth--;
            Lprintf("ialth %d\n",l->ialth);
            if(l->ialth == 0)
            {
                rhup = 0.0;
                if(l->l != l->lmax)
                {
                    l->yp1 = l->yh[l->lmax];
                    for(int i = 1; i <= l->n; i++)
                    {
                        l->savf[i] = l->acor[i] - l->yp1[i];
                    }
                    dup = vmnorm(l->n, l->savf, l->ewt) / l->tesco[l->nq][3];
                    exup = 1.0 / (double) (l->l + 1);
                    rhup = 1.0 / (1.4 * pow(dup, exup) + 0.0000014);
                }
                orderswitch(l,&rhup, dsm, &pdh, &rh, &orderflag);

                /*
                 * No change in h or nq.
                 */
                if(orderflag == 0)
                {
                    endstoda(l);
                    break;
                }
                /*
                 * h is changed, but not nq.
                 */
                else if(orderflag == 1)
                {
                    rh = Max(rh, l->hmin / fabs(l->h));
                    scaleh(l,&rh, &pdh);
                    l->rmax = 10.0;
                    endstoda(l);
                    break;
                }
                /*
                 * both nq and h are changed.
                 */
                else if(orderflag == 2)
                {
                    resetcoeff(l);
                    rh = Max(rh, l->hmin / fabs(l->h));
                    scaleh(l,&rh, &pdh);
                    l->rmax = 10.0;
                    endstoda(l);
                    break;
                }
            }	/* end if( l->ialth == 0 )   */

            if(l->ialth > 1 || l->l == l->lmax)
            {
                endstoda(l);
                break;
            }

            l->yp1 = l->yh[l->lmax];
            for(int i = 1; i <= l->n; i++)
            {
                l->yp1[i] = l->acor[i];
            }
            endstoda(l);
            break;
        }
        /* end if( dsm <= 1.0 )   */
        /*
          The error test failed.  l->kflag keeps track of multiple failures.
          Restore tn and the yh array to their previous values, and prepare
          to try the step again.  Compute the optimum step size for this or
          one lower.  After 2 or more failures, h is forced to decrease
          by a factor of 0.2 or less.
        */
        else
        {
            l->kflag--;
            l->tn = told;
            for(int j = l->nq; j >= 1; j--)
            {
                for(int i1 = j; i1 <= l->nq; i1++)
                {
                    l->yp1 = l->yh[i1];
                    l->yp2 = l->yh[i1 + 1];
                    for(int i = 1; i <= l->n; i++)
                    {
                        l->yp1[i] -= l->yp2[i];
                    }
                }
            }
            l->rmax = 2.0;
            if(fabs(l->h) <= l->hmin * 1.00001)
            {
                l->kflag = -1;
                l->hold = l->h;
                l->jstart = 1;
                break;
            }
            if(l->kflag > -3)
            {
                rhup = 0.0;
                orderswitch(l,&rhup, dsm, &pdh, &rh, &orderflag);
                if(orderflag == 1 || orderflag == 0)
                {
                    if(orderflag == 0)
                    {
                        rh = Min(rh, 0.2);
                    }
                    rh = Max(rh, l->hmin / fabs(l->h));
                    scaleh(l,&rh, &pdh);
                }
                if(orderflag == 2)
                {
                    resetcoeff(l);
                    rh = Max(rh, l->hmin / fabs(l->h));
                    scaleh(l,&rh, &pdh);
                }
                continue;
            }
            /* if( l->kflag > -3 )   */
            /*
              Control reaches this section if 3 or more failures have occurred.
              If 10 failures have occurred, exit with l->kflag = -1.
              It is assumed that the derivatives that have accumulated in the
              yh array have errors of the wrong order.  Hence the first
              derivative is recomputed, and the order is set to 1.0  Then
              h is reduced by a factor of 10, and the step is retried,
              until it succeeds or h reaches l->hmin.
            */
            else
            {
                if(l->kflag == -10)
                {
                    l->kflag = -1;
                    l->hold = l->h;
                    l->jstart = 1;
                    break;
                }
                else
                {
                    rh = 0.1;
                    rh = Max(l->hmin / fabs(l->h), rh);
                    l->h *= rh;
                    l->yp1 = l->yh[INITIAL_VALUE];
                    for(int i = 1; i <= l->n; i++)
                    {
                        y[i] = l->yp1[i];
                    }
                    (*f) (l->tn, y + 1, l->savf + 1, _data);
                    l->nfe++;
                    l->yp1 = l->yh[DERIVATIVES];
                    for(int i = 1; i <= l->n; i++)
                    {
                        l->yp1[i] = l->h * l->savf[i];
                    }
                    l->ipup = l->miter;
                    l->ialth = 5;
                    if(l->nq == 1)
                    {
                        continue;
                    }
                    l->nq = 1;
                    l->l = 2;
                    resetcoeff(l);
                    continue;
                }
            }	/* end else -- l->kflag <= -3 */
        }		/* end error failure handling   */
    }			/* end outer while   */

}				/* end stoda   */

static void ewset(struct lsoda_t * Restrict const l,
                  int itol, double *rtol, double *atol, double *ycur)
{
    switch (itol)
    {
    case 1:
        for(int i = 1; i <= l->n; i++)
        {
            l->ewt[i] = rtol[1] * fabs(ycur[i]) + atol[1];
        }
        break;
    case 2:
        for(int i = 1; i <= l->n; i++)
        {
            l->ewt[i] = rtol[1] * fabs(ycur[i]) + atol[i];
        }
        break;
    case 3:
        for(int i = 1; i <= l->n; i++)
        {
            l->ewt[i] = rtol[i] * fabs(ycur[i]) + atol[1];
        }
        break;
    case 4:
        for(int i = 1; i <= l->n; i++)
        {
            l->ewt[i] = rtol[i] * fabs(ycur[i]) + atol[i];
        }
        break;
    }

}				/* end ewset   */

static void intdy(struct lsoda_t * Restrict const l,
                  double t, int k, double *dky, int *iflag)

/*
  Intdy computes interpolated values of the k-th derivative of the
  dependent variable vector y, and stores it in dky.  This routine
  is called within the package with k = 0 and *t = tout, but may
  also be called by the user for any k up to the current order.
  ( See detailed instructions in the usage documentation. )

  The computed values in dky are gotten by interpolation using the
  Nordsieck history array yh.  This array corresponds uniquely to a
  vector-valued polynomial of degree nqcur or less, and dky is set
  to the k-th derivative of this polynomial at t.
  The formula for dky is

  q
  dky[i] = sum c[k][j] * ( t - tn )^(j-k) * h^(-j) * yh[j+1][i]
  j=k

  where c[k][j] = j*(j-1)*...*(j-k+1), q = nqcur, tn = tcur, h = hcur.
  The quantities nq = nqcur, l = nq+1, n = neq, tn, and h are declared
  static globally.  The above sum is done in reverse order.
  *iflag is returned negative if either k or t is out of bounds.
  */

{
    int             ic, jj, jp1;
    double          c, r, s, tp;

    *iflag = 0;
    if(k < 0 || k > l->nq)
    {
        fprintf(stderr, "[intdy] k = %d illegal\n", k);
        *iflag = -1;
        return;
    }
    tp = l->tn - l->hu - 100.0 * ETA * (l->tn + l->hu);
    if((t - tp) * (t - l->tn) > 0.0)
    {
        fprintf(stderr, "intdy -- t = %g illegal. t not in interval tcur - hu to tcur\n", t);
        *iflag = -2;
        return;
    }
    s = (t - l->tn) / l->h;
    ic = 1;
    for(jj = l->l - k; jj <= l->nq; jj++)
    {
        ic *= jj;
    }
    c = (double) ic;
    l->yp1 = l->yh[l->l];
    for(int i = 1; i <= l->n; i++)
    {
        dky[i] = c * l->yp1[i];
    }
    for(int j = l->nq - 1; j >= k; j--)
    {
        jp1 = j + 1;
        ic = 1;
        for(jj = jp1 - k; jj <= j; jj++)
        {
            ic *= jj;
        }
        c = (double) ic;
        l->yp1 = l->yh[jp1];
        for(int i = 1; i <= l->n; i++)
        {
            dky[i] = c * l->yp1[i] + s * dky[i];
        }
    }
    if(k == 0)
    {
        return;
    }
    r = pow(l->h, (double) (-k));
    for(int i = 1; i <= l->n; i++)
    {
        dky[i] *= r;
    }
}				/* end intdy   */

static void cfode(struct lsoda_t * Restrict const l,
                  int meth)
{
    int             nq /* NOT l->nq */;
    double          agamq, pc[13], pint, ragq, rq1fac, tsign, xpin;
    /*
      cfode is called by the integrator routine to set coefficients
      needed there.  The coefficients for the current method, as
      given by the value of meth, are set for all orders and saved.
      The maximum order assumed here is 12 if meth = 1 and 5 if meth = 2.
      ( A smaller value of the maximum order is also allowed. )
      cfode is called once at the beginning of the problem, and
      is not called again unless and until meth is changed.

      The l->elco array contains the basic method coefficients.
      The coefficients el[i], 1 < i < nq+1, for the method of
      order nq are stored in l->elco[nq][i].  They are given by a generating
      polynomial, i.e.,

      l(x) = el[1] + el[2]*x + ... + el[nq+1]*x^nq.

      For the implicit Adams method, l(x) is given by

      dl/dx = (x+1)*(x+2)*...*(x+nq-1)/factorial(nq-1),   l(-1) = 0.

      For the bdf methods, l(x) is given by

      l(x) = (x+1)*(x+2)*...*(x+nq)/k,

      where   k = factorial(nq)*(1+1/2+...+1/nq).

      The l->tesco array contains test constants used for the
      local error test and the selection of step size and/or order.
      At order nq, l->tesco[nq][k] is used for the selection of step
      size at order nq-1 if k = 1, at order nq if k = 2, and at order
      nq+1 if k = 3.
    */
    double rqfac;

    if(meth == METHOD_NONSTIFF)
    {
        l->elco[1][1] = 1.0;
        l->elco[1][2] = 1.0;
        l->tesco[1][1] = 0.0;
        l->tesco[1][2] = 2.0;
        l->tesco[2][1] = 1.0;
        l->tesco[12][3] = 0.0;
        pc[1] = 1.0;
        rqfac = 1.0;
        for(nq = 2; nq <= 12; nq++)
        {
            /*
              The pc array will contain the coefficients of the polynomial

              p(x) = (x+1)*(x+2)*...*(x+nq-1).

              Initially, p(x) = 1.
            */
            rq1fac = rqfac;
            rqfac = rqfac / (double) nq;
            const int nqm1 = nq - 1;
            const double fnqm1 = (double) nqm1;
            const int nqp1 = nq + 1;
            /*
              Form coefficients of p(x)*(x+nq-1).
            */
            pc[nq] = 0.0;
            for(int i = nq; i >= 2; i--)
            {
                pc[i] = pc[i - 1] + fnqm1 * pc[i];
            }
            pc[1] *= fnqm1;
            /*
              Compute integral, -1 to 0, of p(x) and x*p(x).
            */
            pint = pc[1];
            xpin = pint / 2.0;
            tsign = 1.0;
            for(int i = 2; i <= nq; i++)
            {
                tsign = -tsign;
                const double xx = tsign * pc[i];
                pint += xx / (double) i;
                xpin += xx / (double) (i + 1);
            }
            /*
              Store coefficients in l->elco and l->tesco.
            */
            l->elco[nq][1] = pint * rq1fac;
            l->elco[nq][2] = 1.0;
            for(int i = 2; i <= nq; i++)
            {
                l->elco[nq][i + 1] = rq1fac * pc[i] / (double) i;
            }
            agamq = rqfac * xpin;
            ragq = 1.0 / agamq;
            l->tesco[nq][2] = ragq;
            if(nq < 12)
            {
                l->tesco[nqp1][1] = ragq * rqfac / (double) nqp1;
            }
            l->tesco[nqm1][3] = ragq;
        }		/* end for   */
        return;
    }			/* end if( meth == 1 == METHOD_NONSTIFF )   */

    /*
      meth = METHOD_STIFF = 2.
    */
    pc[1] = 1.0;
    rq1fac = 1.0;
    /*
      The pc array will contain the coefficients of the polynomial

      p(x) = (x+1)*(x+2)*...*(x+nq).

      Initially, p(x) = 1.
    */
    for(nq = 1; nq <= 5; nq++)
    {
        const double fnq = (double) nq;
        const int nqp1 = nq + 1;
        /*
          Form coefficients of p(x)*(x+nq).
        */
        pc[nqp1] = 0.0;
        for(int i = nq + 1; i >= 2; i--)
        {
            pc[i] = pc[i - 1] + fnq * pc[i];
        }
        pc[1] *= fnq;
        /*
          Store coefficients in l->elco and l->tesco.
        */
        for(int i = 1; i <= nqp1; i++)
        {
            l->elco[nq][i] = pc[i] / pc[2];
        }
        l->elco[nq][2] = 1.0;
        l->tesco[nq][1] = rq1fac;
        l->tesco[nq][2] = ((double) nqp1) / l->elco[nq][1];
        l->tesco[nq][3] = ((double) (nq + 2)) / l->elco[nq][1];
        rq1fac /= fnq;
    }
    return;

}				/* end cfode   */

static void scaleh(struct lsoda_t * Restrict const l,
                   double *rh,
                   double *pdh)
{
    double          r;

    /*
      If h is being changed, the h l->ratio rh is checked against l->rmax, l->hmin,
      and l->hmxi, and the yh array is rescaled.  l->ialth is set to l = nq + 1
      to prevent a change of h for that many steps, unless forced by a
      convergence or error test failure.
    */
    *rh = Min(*rh, l->rmax);
    *rh = *rh / Max(1.0, fabs(l->h) * l->hmxi * *rh);
    /*
      If l->meth = METHOD_NONSTIFF, also restrict the new step size by the stability region.
      If this reduces h, set l->irflag to 1 so that if there are roundoff
      problems later, we can assume that is the cause of the trouble.
    */
    if(l->meth == METHOD_NONSTIFF)
    {
        l->irflag = 0;
        *pdh = Max(fabs(l->h) * l->pdlast, 0.000001);
        if((*rh * *pdh * 1.00001) >= l->sm1[l->nq])
        {
            *rh = l->sm1[l->nq] / *pdh;
            l->irflag = 1;
        }
    }
    r = 1.0;
    for(int j = 2; j <= l->l; j++)
    {
        r *= *rh;
        l->yp1 = l->yh[j];
        for(int i = 1; i <= l->n; i++)
        {
            l->yp1[i] *= r;
        }
    }
    l->h *= *rh;
    l->rc *= *rh;
    l->ialth = l->l;

}				/* end scaleh   */


static void prja(struct lsoda_t * Restrict const l,
                 const int neq Maybe_unused,
                 double * const Restrict y,
                 _lsoda_f f,
                 void * _data)
{
    int             ier;
    double          fac, hl0, r, r0, yj;
    /*
      prja is called by stoda to compute and process the matrix
      P = I - h * el[1] * J, where J is an approximation to the Jacobian.
      Here J is computed by finite differencing.
      J, scaled by -h * el[1], is stored in wm.  Then the norm of J ( the
      matrix norm consistent with the weighted max-norm on vectors given
      by vmnorm ) is computed, and J is overwritten by P.  P is then
      subjected to LU decomposition in prepal->ration for later solution
      of linear systems with p as coefficient matrix.  This is done
      by dgefa if l->miter = 2, and by dgbfa if l->miter = 5.
    */
    l->nje++;
    l->ierpj = 0;
    l->jcur = 1;
    hl0 = l->h * l->el0;
    /*
      If l->miter = 2, make n calls to f to approximate J.
    */
    if(l->miter != 2)
    {
        fprintf(stderr, "[prja] l->miter != 2\n");
        return;
    }
    else if(l->miter == 2)
    {
        fac = vmnorm(l->n, l->savf, l->ewt);
        r0 = 1000. * fabs(l->h) * ETA * ((double) l->n) * fac;
        if(r0 == 0.0)
        {
            r0 = 1.0;
        }
        if(l->Jacobian != NULL)
        {
            // todo put in Boolean to determine when we have to recalc

            /* use Jacobian function */
            l->Jacobian(l->tn,
                        y,
                        l->wm,
                        _data);

            for(int i = 1; i <= l->n; i++)
            {
                for(int j = 1; j <= l->n; j++)
                {
                    l->wm[i][j] *= -hl0;
                }
            }
        }
        else
        {
            /* finite difference Jacobian */
            for(int j = 1; j <= l->n; j++)
            {
                yj = y[j];
                r = Max(l->sqrteta * fabs(yj), r0 / l->ewt[j]);
                y[j] += r;
                fac = -hl0 / r;
                (*f) (l->tn, y + 1, l->acor + 1, _data);
                for(int i = 1; i <= l->n; i++)
                {
                    l->wm[i][j] = (l->acor[i] - l->savf[i]) * fac;
                }
                y[j] = yj;
            }
        }
        l->nfe += l->n;

        /*
          Compute norm of Jacobian.
        */
        l->pdnorm = fnorm(l->n, (const double **)l->wm, l->ewt) / fabs(hl0);

        /*
          Add identity matrix.
        */
        for(int i = 1; i <= l->n; i++)
        {
            l->wm[i][i] += 1.0;
        }

        /*
         * Do LU decomposition on P.
         */
        dgefa(l->wm, l->n, l->ipvt, &ier);
        if(ier != 0)
        {
            l->ierpj = 1;
        }
        return;
    }
}				/* end prja   */


/*
  This function routine computes the weighted max-norm
  of the vector of length n contained in the array v, with weights
  contained in the array w of length n.

  vmnorm = Max( i = 1, ..., n ) fabs( v[i] ) * w[i].
*/
static double vmnorm(const int n,
                     const double * const Restrict v,
                     const double * const Restrict w)
{
    double vm = 0.0;
    for(int i = 1; i <= n; i++)
    {
        vm = Max(vm,
                 fabs(v[i]) * w[i]);
    }
    return vm;
}



/*
  This subroutine computes the norm of a full n by n matrix,
  stored in the array a, that is consistent with the weighted max-norm
  on vectors, with weights stored in the array w.

  fnorm = Max(i=1,...,n) ( w[i] * sum(j=1,...,n) fabs( a[i][j] ) / w[j] )
*/
static double fnorm(int n, const double ** Restrict a, const double *const Restrict w)
{
    double an = 0.0;
    for(int i = 1; i <= n; i++)
    {
        double sum = 0.0;
        const double * ap1 = a[i];
        for(int j = 1; j <= n; j++)
        {
            sum += fabs(ap1[j]) / w[j];
        }
        an = Max(an, sum * w[i]);
    }
    return an;

}

static void correction(struct lsoda_t * Restrict const l,
                       int neq,
                       double *y,
                       _lsoda_f f,
                       int *corflag,
                       double pnorm,
                       double *del,
                       double *delp,
                       double *told,
                       int *ncf,
                       double *rh,
                       int *m,
                       void *_data)
{
    /*
     * *corflag = 0 : corrector converged,
     * 1 : step size to be reduced, redo prediction,
     * 2 : corrector cannot converge, failure flag.
     */
    double rate, dcon;

    /*
      Up to l->maxcor corrector iterations are taken.  A convergence test is
      made on the r.m.s. norm of each correction, weighted by the error
      weight vector l->ewt.  The sum of the corrections is accumulated in the
      vector l->acor[i].  The yh array is not altered in the corrector loop.
    */
    *m = 0;
    *corflag = 0;
    rate = 0.0;
    *del = 0.0;
    l->yp1 = l->yh[INITIAL_VALUE];
    for(int i = 1; i <= l->n; i++)
    {
        y[i] = l->yp1[i];
    }
    (*f) (l->tn, y + 1, l->savf + 1, _data);
    l->nfe++;

    /*
      If indicated, the matrix P = I - h * el[1] * J is reevaluated and
      preprocessed before starting the corrector iteration.  l->ipup is set
      to 0 as an indicator that this has been done.
    */
    Loop_forever
    {
        if(*m == 0)
        {
            if(l->ipup > 0)
            {
                prja(l,neq, y, f, _data);
                l->ipup = 0;
                l->rc = 1.0;
                l->nslp = l->nst;
                l->crate = 0.7;
                if(l->ierpj != 0)
                {
                    corfailure(l,told, rh, ncf, corflag);
                    return;
                }
            }
            for(int i = 1; i <= l->n; i++)
            {
                l->acor[i] = 0.0;
            }
        }/* end if( *m == 0 )   */
        if(l->miter == 0)
        {
            /*
              In case of functional iteration, update y directly from
              the result of the last function evaluation.
            */
            l->yp1 = l->yh[DERIVATIVES];
            for(int i = 1; i <= l->n; i++)
            {
                l->savf[i] = l->h * l->savf[i] - l->yp1[i];
                y[i] = l->savf[i] - l->acor[i];
            }
            *del = vmnorm(l->n, y, l->ewt);
            l->yp1 = l->yh[INITIAL_VALUE];
            for(int i = 1; i <= l->n; i++)
            {
                y[i] = l->yp1[i] + l->el[1] * l->savf[i];
                l->acor[i] = l->savf[i];
            }
        }
        /* end functional iteration   */
        /*
          In the case of the chord method, compute the corrector error,
          and solve the linear system with that as right-hand side and
          P as coefficient matrix.
        */
        else
        {
            l->yp1 = l->yh[DERIVATIVES];
            for(int i = 1; i <= l->n; i++)
            {
                y[i] = l->h * l->savf[i] - (l->yp1[i] + l->acor[i]);
            }
            solsy(l,y);
            *del = vmnorm(l->n, y, l->ewt);
            l->yp1 = l->yh[INITIAL_VALUE];
            for(int i = 1; i <= l->n; i++)
            {
                l->acor[i] += y[i];
                y[i] = l->yp1[i] + l->el[1] * l->acor[i];
            }
        }		/* end chord method   */

        /*
         * Test for convergence.  If *m > 0, an estimate of the convergence
         * rate constant is stored in l->crate, and this is used in the test.
         *
         *  We first check for a change of iterates that is the size of
         * roundoff error.  If this occurs, the iteration has converged, and a
         * new rate estimate is not formed.
         * In all other cases, force at least two iterations to estimate a
         * local Lipschitz constant estimate for Adams method.
         * On convergence, form l->pdest = local maximum Lipschitz constant
         * estimate.  l->pdlast is the most recent nonzero estimate.
         */
        if(*del <= 100.0 * pnorm * ETA)
        {
            break;
        }

        if(*m != 0 || l->meth != METHOD_NONSTIFF)
        {
            if(*m != 0)
            {
                double rm = 1024.0;
                if(*del <= (1024.0 * *delp))
                {
                    rm = *del / *delp;
                }
                rate = Max(rate, rm);
                l->crate = Max(0.2 * l->crate, rm);
            }
            dcon = *del * Min(1., 1.5 * l->crate) / (l->tesco[l->nq][2] * l->conit);
            if(dcon <= 1.0)
            {
                l->pdest = Max(l->pdest, rate / fabs(l->h * l->el[1]));
                if(l->pdest != 0.0)
                {
                    l->pdlast = l->pdest;
                }
                break;
            }
        }
        /*
         * The corrector iteration failed to converge.
         * If l->miter != 0 and the Jacobian is out of date, prja is called for
         * the next try.   Otherwise the yh array is retracted to its values
         * before prediction, and h is reduced, if possible.  If h cannot be
         * reduced or l->mxncf failures have occured, exit with corflag = 2.
         */
        (*m)++;
        if(*m == l->maxcor || (*m >= 2 && *del > 2.0 * *delp))
        {
            if(l->miter == 0 || l->jcur == 1)
            {
                corfailure(l,told, rh, ncf, corflag);
                return;
            }
            l->ipup = l->miter;
/*
  Restart corrector if Jacobian is recomputed.
*/
            *m = 0;
            rate = 0.0;
            *del = 0.0;
            l->yp1 = l->yh[INITIAL_VALUE];
            for(int i = 1; i <= l->n; i++)
            {
                y[i] = l->yp1[i];
            }
            (*f) (l->tn, y + 1, l->savf + 1, _data);
            l->nfe++;
        }
        /*
          Iterate corrector.
        */
        else
        {
            *delp = *del;
            (*f) (l->tn, y + 1, l->savf + 1, _data);
            l->nfe++;
        }
    }			/* end while   */
}				/* end correction   */

static void corfailure(struct lsoda_t * Restrict const l,
                       double *told,
                       double *rh,
                       int *ncf,
                       int *corflag)
{
    /*
     * RGI : presumably this should be the value, not the pointer,
     *       that is incremented? The original code was just
     *       ncf++ but that caused reading of uninitialized
     *       memory... if ncf = "number of cor failures" then
     *       my change makes sense.
     */
    (*ncf)++;

    l->rmax = 2.0;
    l->tn = *told;
    for(int j = l->nq; j >= 1; j--)
    {
        for(int i1 = j; i1 <= l->nq; i1++)
        {
            l->yp1 = l->yh[i1];
            l->yp2 = l->yh[i1 + 1];
            for(int i = 1; i <= l->n; i++)
            {
                l->yp1[i] -= l->yp2[i];
            }
        }
    }
    if(fabs(l->h) <= l->hmin * 1.00001 ||
       *ncf == l->mxncf)
    {
        *corflag = 2;
        return;
    }
    *corflag = 1;
    *rh = 0.25;
    l->ipup = l->miter;

}

static void solsy(struct lsoda_t * Restrict const l,
                  double *y)

/*
  This routine manages the solution of the linear system arising from
  a chord iteration.  It is called if l->miter != 0.
  If l->miter is 2, it calls dgesl to accomplish this.
  If l->miter is 5, it calls dgbsl.

  y = the right-hand side vector on input, and the solution vector
  on output.
*/

{
    l->iersl = 0;
    if(l->miter != 2)
    {
        printf("solsy -- l->miter != 2\n");
    }
    else if(l->miter == 2)
    {
        dgesl(l->wm, l->n, l->ipvt, y, 0);
    }
    return;
}

static void methodswitch(struct lsoda_t * Restrict const l,
                         double dsm,
                         double pnorm,
                         double *pdh,
                         double *rh)
{
    int lm1, lm1p1, lm2, lm2p1, nqm1, nqm2;
    double rh1, rh2, rh1it, exm2, dm2, exm1, dm1, alpha, exsm;

/*
  We are currently using an Adams method.  Consider switching to bdf.
  If the current order is greater than 5, assume the problem is
  not stiff, and skip this section.
  If the Lipschitz constant and error estimate are not polluted
  by roundoff, perform the usual test.
  Otherwise, switch to the bdf methods if the last step was
  restricted to insure stability ( l->irflag = 1 ), and stay with Adams
  method if not.  When switching to bdf with polluted error estimates,
  in the absence of other information, double the step size.

  When the estimates are ok, we make the usual test by computing
  the step size we could have (ideally) used on this step,
  with the current (Adams) method, and also that for the bdf.
  If nq > l->mxords, we consider changing to order l->mxords on switching.
  Compare the two step sizes to decide whether to switch.
  The step size advantage must be at least l->ratio = 5 to switch.
*/
    if(l->meth == METHOD_NONSTIFF)
    {
        if(l->nq > 5)
        {
            return;
        }
        if(dsm <= (100.0 * pnorm * ETA) || l->pdest == 0.0)
        {
            if(l->irflag == 0)
            {
                return;
            }
            rh2 = 2.0;
            nqm2 = Min(l->nq, l->mxords);
        }
        else
        {
            exsm = 1.0 / (double) l->l;
            rh1 = 1.0 / (1.2 * pow(dsm, exsm) + 0.0000012);
            rh1it = 2.0 * rh1;
            *pdh = l->pdlast * fabs(l->h);
            if((*pdh * rh1) > 0.00001)
            {
                rh1it = l->sm1[l->nq] / *pdh;
            }
            rh1 = Min(rh1, rh1it);
            if(l->nq > l->mxords)
            {
                nqm2 = l->mxords;
                lm2 = nqm2 + 1;
                exm2 = 1.0 / (double) lm2;
                lm2p1 = lm2 + 1;
                dm2 = vmnorm(l->n, l->yh[lm2p1], l->ewt) / l->cm2[l->mxords];
                rh2 = 1.0 / (1.2 * pow(dm2, exm2) + 0.0000012);
            }
            else
            {
                dm2 = dsm * (l->cm1[l->nq] / l->cm2[l->nq]);
                rh2 = 1.0 / (1.2 * pow(dm2, exsm) + 0.0000012);
                nqm2 = l->nq;
            }
            if(rh2 < l->ratio * rh1)
            {
                return;
            }
        }
        /*
         * The method switch test passed.
         * Reset relevant quantities for bdf.
         */
        *rh = rh2;
        l->icount = 20;
        l->meth = METHOD_STIFF;
        l->miter = l->jtyp;
        l->pdlast = 0.0;
        l->nq = nqm2;
        l->l = l->nq + 1;
        return;
    }			/* end if( l->meth == METHOD_NONSTIFF )   */
    /*
      We are currently using a bdf method, considering switching to Adams.
      Compute the step size we could have (ideally) used on this step,
      with the current (bdf) method, and also that for the Adams.
      If nq > l->mxordn, we consider changing to order l->mxordn on switching.
      Compare the two step sizes to decide whether to switch.
      The step size advantage must be at least 5/l->ratio = 1 to switch.
      If the step size for Adams would be so small as to cause
      roundoff pollution, we stay with bdf.
    */
    exsm = 1.0 / (double) l->l;
    if(l->mxordn < l->nq)
    {
        nqm1 = l->mxordn;
        lm1 = l->mxordn + 1;
        exm1 = 1.0 / (double) lm1;
        lm1p1 = lm1 + 1;
        dm1 = vmnorm(l->n, l->yh[lm1p1], l->ewt) / l->cm1[l->mxordn];
        rh1 = 1.0 / (1.2 * pow(dm1, exm1) + 0.0000012);
    }
    else
    {
        dm1 = dsm * (l->cm2[l->nq] / l->cm1[l->nq]);
        rh1 = 1.0 / (1.2 * pow(dm1, exsm) + 0.0000012);
        nqm1 = l->nq;
        exm1 = exsm;
    }
    rh1it = 2.0 * rh1;
    *pdh = l->pdnorm * fabs(l->h);
    if((*pdh * rh1) > 0.00001)
    {
        rh1it = l->sm1[nqm1] / *pdh;
    }
    rh1 = Min(rh1, rh1it);
    rh2 = 1.0 / (1.2 * pow(dsm, exsm) + 0.0000012);
    if((rh1 * l->ratio) < (5.0 * rh2))
    {
        return;
    }
    alpha = Max(0.001, rh1);
    dm1 *= pow(alpha, exm1);
    if(dm1 > 1000. * ETA * pnorm)
    {
        /*
         * The switch test passed.  Reset relevant quantities for Adams.
         */
        *rh = rh1;
        l->icount = 20;
        l->meth = METHOD_NONSTIFF;
        l->miter = 0;
        l->pdlast = 0.0;
        l->nq = nqm1;
        l->l = l->nq + 1;
    }
}				/* end methodswitch   */


/*
  This routine returns from stoda to lsoda.  Hence freevectors(l) is
  not executed.
*/
static void endstoda(struct lsoda_t * Restrict const l)
{
    const double r = 1.0 / l->tesco[l->nqu][2];
    for(int i = 1; i <= l->n; i++)
    {
        l->acor[i] *= r;
    }
    l->hold = l->h;
    l->jstart = 1;
}

static void orderswitch(struct lsoda_t * Restrict const l,
                        double *rhup,
                        double dsm,
                        double *pdh,
                        double *rh,
                        int *orderflag)

/*
  Regardless of the success or failure of the step, factors
  rhdn, rhsm, and rhup are computed, by which h could be multiplied
  at order nq - 1, order nq, or order nq + 1, respectively.
  In the case of a failure, rhup = 0. to avoid an order increase.
  The largest of these is determined and the new order chosen
  accordingly.  If the order is to be increased, we compute one
  additional scaled derivative.

  orderflag = 0  : no change in h or nq,
  1  : change in h but not nq,
  2  : change in both h and nq.
*/

{
    double exsm = 1.0 / (double) l->l;
    double rhsm = 1.0 / (1.2 * pow(dsm, exsm) + 0.0000012);
    double rhdn = 0.0;
    int newq;
    *orderflag = 0;

    if(l->nq != 1)
    {
        const double ddn = vmnorm(l->n, l->yh[l->l], l->ewt) / l->tesco[l->nq][1];
        const double exdn = 1.0 / (double) l->nq;
        rhdn = 1.0 / (1.3 * pow(ddn, exdn) + 0.0000013);
    }
    /*
      If l->meth = METHOD_NONSTIFF, limit rh accordinfg to the stability region also.
    */
    if(l->meth == METHOD_NONSTIFF)
    {
        *pdh = Max(fabs(l->h) * l->pdlast, 0.000001);
        if(l->l < l->lmax)
        {
            *rhup = Min(*rhup, l->sm1[l->l] / *pdh);
        }
        rhsm = Min(rhsm, l->sm1[l->nq] / *pdh);
        if(l->nq > 1)
        {
            rhdn = Min(rhdn, l->sm1[l->nq - 1] / *pdh);
        }
        l->pdest = 0.0;
    }
    if(rhsm >= *rhup)
    {
        if(rhsm >= rhdn)
        {
            newq = l->nq;
            *rh = rhsm;
        }
        else
        {
            newq = l->nq - 1;
            *rh = rhdn;
            if(l->kflag < 0 && *rh > 1.0)
            {
                *rh = 1.0;
            }
        }
    }
    else
    {
        if(*rhup <= rhdn)
        {
            newq = l->nq - 1;
            *rh = rhdn;
            if(l->kflag < 0 && *rh > 1.0)
            {
                *rh = 1.0;
            }
        }
        else
        {
            *rh = *rhup;
            if(*rh >= 1.1)
            {
                const double r = l->el[l->l] / (double) l->l;
                l->nq = l->l;
                l->l = l->nq + 1;
                l->yp1 = l->yh[l->l];
                for(int i = 1; i <= l->n; i++)
                {
                    l->yp1[i] = l->acor[i] * r;
                }
                *orderflag = 2;
                return;
            }
            else
            {
                l->ialth = 3;
                return;
            }
        }
    }
    /*
     * If l->meth = METHOD_NONSTIFF and h is restricted by
     * stability, bypass 10 pel->rcent test.
     */
    if(l->meth == METHOD_NONSTIFF)
    {
        if((*rh * *pdh * 1.00001) < l->sm1[newq])
        {
            if(l->kflag == 0 && *rh < 1.1) {
                l->ialth = 3;
                return;
            }
        }
    }
    else
    {
        if(l->kflag == 0 && *rh < 1.1)
        {
            l->ialth = 3;
            return;
        }
    }
    if(l->kflag <= -2)
    {
        *rh = Min(*rh, 0.2);
    }
    /*
     * If there is a change of order, reset nq, l, and the coefficients.
     * In any case h is reset according to rh and the yh array is rescaled.
     * Then exit or redo the step.
     */
    if(newq == l->nq)
    {
        *orderflag = 1;
    }
    else
    {
        l->nq = newq;
        l->l = newq + 1;
        *orderflag = 2;
    }
}				/* end orderswitch   */


static void resetcoeff(struct lsoda_t * Restrict const l)
/*
  The el vector and related constants are reset
  whenever the order nq is changed, or at the start of the problem.
*/
{
    double *ep1 = l->elco[l->nq];
    for(int i = 1; i <= l->l; i++)
    {
        l->el[i] = ep1[i];
    }
    l->rc = l->rc * l->el[1] / l->el0;
    l->el0 = l->el[1];
    l->conit = 0.5 / (double) (l->nq + 2);
}

/* this function does nothing. */
static void freevectors(struct lsoda_t * Restrict const l Maybe_unused)
{
}

static void _freevectors(struct lsoda_t * Restrict const l)
{
    if(l->wm)
    {
        for(int i = 1; i <= l->g_nyh; ++i)
        {
            Safe_free(l->wm[i]);
        }
    }
    if(l->yh)
    {
        for(int i = 1; i <= l->g_lenyh; ++i)
        {
            Safe_free(l->yh[i]);
        }
    }
    Safe_free(l->yh);
    Safe_free(l->wm);
    Safe_free(l->ewt);
    Safe_free(l->savf);
    Safe_free(l->acor);
    Safe_free(l->ipvt);
    l->g_nyh = l->g_lenyh = 0;
}

/*****************************
 * more convenient interface *
 *****************************/

int n_lsoda(struct lsoda_t * Restrict const l,
            double y[],
            int n,
            double *x,
            double xout,
            double eps,
            _lsoda_f devis,
            struct lsoda_data_t *data)
{
    int istate, itask;
    double         *_y, *atol, *rtol;
    _y = (double *) Calloc(3 * (n + 1), sizeof(double));
    atol = _y + n + 1;
    rtol = atol + n + 1;
    rtol[0] = atol[0] = 0.0; //probably not required
    for(int i = 1; i <= n; ++i)
    {
        _y[i] = y[i - 1];
        atol[i] = 0.0;// RGI : 0
        rtol[i] = eps; // RGI : use rtol
    }
    /* atol should be tiny if zero */
    for(int i = 1; i <= n; ++i)
    {
        atol[i] = Max(atol[i],
                      VERY_TINY);
    }
    istate = 1;//l->init ? 2 : 1; // RGI : force this to be 1
    itask = 1;
    lsoda(l,devis, n, _y, x, xout, 2, rtol, atol, itask, &istate, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0., 0., 0., 0., data);
    for(int i = 1; i <= n; ++i)
    {
        y[i - 1] = _y[i];
    }
    Safe_free(_y);
    return istate < 0 ? istate : 0;
}

void n_lsoda_terminate(struct lsoda_t * Restrict const l)
{
    if(l->init)
    {
        _freevectors(l);
    }
    l->init = 0;
}

#ifdef __BUILD_LSODA_EXAMPLE

/*************
 * example.c *
 *************/

static void fex(double t Maybe_unused,
                double *y Maybe_unused,
                double *ydot Maybe_unused,
                void *data Maybe_unused)
{
    ydot[0] = 1.0E4 * y[1] * y[2] - .04E0 * y[0];
    ydot[2] = 3.0E7 * y[1] * y[1];
    ydot[1] = -1.0 * (ydot[0] + ydot[2]);
}

int lsoda_example(struct lsoda_t * Restrict const l)
{
    double          rwork1, rwork5, rwork6, rwork7;
    double          atol[4], rtol[4], t, tout, y[4];
    int             iwork1, iwork2, iwork5, iwork6, iwork7, iwork8, iwork9;
    int             neq = 3;
    int             itol, itask, istate, iopt, jt;

    iwork1 = iwork2 = iwork5 = iwork6 = iwork7 = iwork8 = iwork9 = 0;
    rwork1 = rwork5 = rwork6 = rwork7 = 0.0;
    y[1] = 1.0E0;
    y[2] = 0.0E0;
    y[3] = 0.0E0;
    t = 0.0E0;
    tout = 0.4E0;
    itol = 2;
    rtol[0] = 0.0;
    atol[0] = 0.0;
    rtol[1] = 1.0E-4;
    rtol[2] = 1.0E-8;
    rtol[3] = 1.0E-4;
    atol[1] = 1.0E-6;
    atol[2] = 1.0E-10;
    atol[3] = 1.0E-6;
    itask = 1;
    istate = 1;
    iopt = 0;
    jt = 2;

    lsoda_setup(l);
    l->max_steps = 500;
    for(int iout = 1; iout <= 12; iout++)
    {
        lsoda(l,
              fex, neq, y, &t, tout, itol, rtol, atol, itask, &istate, iopt, jt,
              iwork1, iwork2, iwork5, iwork6, iwork7, iwork8, iwork9,
              rwork1, rwork5, rwork6, rwork7, 0);
        printf(" at t= %12.4e y= %14.6e %14.6e %14.6e\n", t, y[1], y[2], y[3]);
        fflush(NULL);
        if(istate <= 0)
        {
            printf("error istate = %d\n", istate);
            exit(0);
        }
        tout = tout * 10.0E0;
    }
    n_lsoda_terminate(l);
    printf("\n\n");
    fflush(NULL);
    return 0;
}
/*
  The correct answer (up to certain precision):

  at t=   4.0000e-01 y=   9.851712e-01   3.386380e-05   1.479493e-02
  at t=   4.0000e+00 y=   9.055333e-01   2.240655e-05   9.444430e-02
  at t=   4.0000e+01 y=   7.158403e-01   9.186334e-06   2.841505e-01
  at t=   4.0000e+02 y=   4.505250e-01   3.222964e-06   5.494717e-01
  at t=   4.0000e+03 y=   1.831976e-01   8.941773e-07   8.168015e-01
  at t=   4.0000e+04 y=   3.898729e-02   1.621940e-07   9.610125e-01
  at t=   4.0000e+05 y=   4.936362e-03   1.984221e-08   9.950636e-01
  at t=   4.0000e+06 y=   5.161833e-04   2.065787e-09   9.994838e-01
  at t=   4.0000e+07 y=   5.179804e-05   2.072027e-10   9.999482e-01
  at t=   4.0000e+08 y=   5.283675e-06   2.113481e-11   9.999947e-01
  at t=   4.0000e+09 y=   4.658667e-07   1.863468e-12   9.999995e-01
  at t=   4.0000e+10 y=   1.431100e-08   5.724404e-14   1.000000e+00
*/

#endif // __BUILD_LSODA_EXAMPLE
