#include "../binary_c.h"
No_empty_translation_unit_warning;

/*
 * Free an unsigned int array
 */
void free_unsigned_int_array(struct unsigned_int_array_t ** Restrict unsigned_int_array)
{
    if(unsigned_int_array != NULL &&
       *unsigned_int_array != NULL)
    {
        Safe_free((*unsigned_int_array)->unsigned_ints);
        Safe_free_nocheck(*unsigned_int_array);
    }
}
