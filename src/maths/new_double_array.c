#include "../binary_c.h"
No_empty_translation_unit_warning;

struct double_array_t * new_double_array(const ssize_t n)
{
    /*
     * Make and return a new double array
     */
    struct double_array_t * s = Malloc(sizeof(struct double_array_t));
    s->n = n;
    if(n<=0)
    {
        s->doubles = NULL;
    }
    else
    {
        s->doubles = Malloc(sizeof(double) * n);
    }
    return s;
}
