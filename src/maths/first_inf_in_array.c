#include "../binary_c.h"
No_empty_translation_unit_warning;

/*
 * Check array for inf, return the index of the first
 * found inf, or n+1 if not found.
 */
size_t first_inf_in_array(const double * const array,
                          const size_t n)
{
    /*
     * Check array, of size n, for infs
     */
    for(size_t i=0; i<n; i++)
    {
        if(isinf(array[i]))
        {
            return i;
        }
    }
    return n+1;
}
