#include "../binary_c.h"
No_empty_translation_unit_warning;


/*
 * Function inverse3 to invert a 3x3 matrix
 */

static double determinantOfMinor(int theRowHeightY,
                                 int          theColumnWidthX,
                                 const double theMatrix [/*Y=*/3] [/*X=*/3] );
static double determinant( const double theMatrix [/*Y=*/3] [/*X=*/3] );

static double determinantOfMinor( int          theRowHeightY,
                                  int          theColumnWidthX,
                                  const double theMatrix [/*Y=*/3] [/*X=*/3] )
{
    int x1 = theColumnWidthX == 0 ? 1 : 0;  /* always either 0 or 1 */
    int x2 = theColumnWidthX == 2 ? 1 : 2;  /* always either 1 or 2 */
    int y1 = theRowHeightY   == 0 ? 1 : 0;  /* always either 0 or 1 */
    int y2 = theRowHeightY   == 2 ? 1 : 2;  /* always either 1 or 2 */

    return ( theMatrix [y1] [x1]  *  theMatrix [y2] [x2] )
        -  ( theMatrix [y1] [x2]  *  theMatrix [y2] [x1] );
}

static double determinant( const double theMatrix [/*Y=*/3] [/*X=*/3] )
{
    return ( theMatrix [0] [0]  *  determinantOfMinor( 0, 0, theMatrix ) )
        -  ( theMatrix [0] [1]  *  determinantOfMinor( 0, 1, theMatrix ) )
        +  ( theMatrix [0] [2]  *  determinantOfMinor( 0, 2, theMatrix ) );
}

Boolean inverse3( const double theMatrix [/*Y=*/3] [/*X=*/3],
                  double theOutput [/*Y=*/3] [/*X=*/3] )
{
    double det = determinant( theMatrix );
    int x,y;
    Boolean ret;
    if (Is_zero(det))
    {
        for (y = 0;  y < 3;  y ++ )
        {
    
            for (x = 0;  x < 3;  x ++   )
            {
                theOutput[y][x] = 0.0;
            }
        }
        ret = FALSE;
    }
    else
    {
        double oneOverDeterminant = 1.0 / det;
        for (y = 0;  y < 3;  y ++ )
        {
            for (x = 0;  x < 3;  x ++   )
            {
/* Rule is inverse = 1/det * minor of the TRANSPOSE matrix.  *
 * Note (y,x) becomes (x,y) INTENTIONALLY here!              */
                theOutput [y] [x]
                    = determinantOfMinor( x, y, theMatrix ) * oneOverDeterminant;

                /* (y0,x1)  (y1,x0)  (y1,x2)  and (y2,x1)  all need to be negated. */
                if( 1 == ((x + y) % 2) )
                    theOutput [y] [x] = - theOutput [y] [x];
            }
        }
    
        ret = TRUE;
    }
    return ret;
}
