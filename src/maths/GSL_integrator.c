#include "../binary_c.h"
No_empty_translation_unit_warning;

#include <gsl/gsl_integration.h>

double GSL_integrator(const double lower,
                      const double upper,
                      const double relative_tolerance,
                      double * const error,
                      const int integrator,
                      double func(double x,void *params),
                      ...)
{
    /*
     * Wrapper for GSL integration functions.
     *
     * Uses the specified integrator between lower
     * and upper bounds.
     *
     * Integrates to within "relative_tolerance" relative tolerance.
     *
     * Does not use an absolute tolerance.
     *
     * Returns the result.
     *
     * Sets the (relative) error in *error.
     *
     * Function F takes (double x, void *params) as its arguments
     * (as specified in the GSL documentation).
     *
     * The first line of the function should be :
     *
     * Map_GSL_params(params,args);
     *
     * Which maps the void * pointer to a va_list called "args" which
     * can contains the va_list which is passed into this function.
     *
     * args can then be accessed in the usual way with va_arg, e.g.,
     *
     * type * whatever = va_arg(args,type *);
     *
     * to retrieve the contents of the args and hence calculate
     * the function at x.
     *
     * Remember to call
     *
     * va_end(args);
     *
     * once you are done with the args.
     */
    double result = 0.0;
    va_list args;
    va_start(args,func);
    gsl_function F;
    F.function = func;
    F.params = &args;
    int status = -1; // default to failure
    *error = 0.0;
    if(integrator == GSL_INTEGRATOR_QAG)
    {
        gsl_integration_workspace * w
            = gsl_integration_workspace_alloc (GSL_INTEGRATOR_WORKSPACE_SIZE);
        if(w)
        {
            status = gsl_integration_qag (&F,
                                          lower,
                                          upper,
                                          GSL_INTEGRATOR_ABSOLUTE_TOLERANCE,
                                          relative_tolerance,
                                          GSL_INTEGRATOR_WORKSPACE_SIZE,
                                          GSL_INTEG_GAUSS61,
                                          w,
                                          &result,
                                          error);
            gsl_integration_workspace_free (w);
        }
    }
    else if(integrator == GSL_INTEGRATOR_QNG)
    {
        size_t neval;
        status = gsl_integration_qng (&F,
                                      lower,
                                      upper,
                                      GSL_INTEGRATOR_ABSOLUTE_TOLERANCE,
                                      relative_tolerance,
                                      &result,
                                      error,
                                      &neval);
    }
    else if(integrator == GSL_INTEGRATOR_QAGS)
    {
        gsl_integration_workspace * w
            = gsl_integration_workspace_alloc (GSL_INTEGRATOR_WORKSPACE_SIZE);
        if(w)
        {
            status = gsl_integration_qags (&F,
                                           lower,
                                           upper,
                                           GSL_INTEGRATOR_ABSOLUTE_TOLERANCE,
                                           relative_tolerance,
                                           GSL_INTEGRATOR_WORKSPACE_SIZE,
                                           w,
                                           &result,
                                           error);
            gsl_integration_workspace_free (w);
        }
    }
    else if(integrator == GSL_INTEGRATOR_CQUAD)
    {
        gsl_integration_cquad_workspace * w_cquad
            = gsl_integration_cquad_workspace_alloc (1000);
        if(w_cquad)
        {
            size_t neval;
            double abserror;
            status = gsl_integration_cquad (&F,
                                            lower,
                                            upper,
                                            GSL_INTEGRATOR_ABSOLUTE_TOLERANCE,
                                            relative_tolerance,
                                            w_cquad,
                                            &result,
                                            &abserror,
                                            &neval);
            gsl_integration_cquad_workspace_free(w_cquad);
        }
    }
    va_end(args);

    /*
     * Exit on error
     */
    if(status)
    {
        Backtrace;
        Exit_binary_c_no_stardata(BINARY_C_GSL_ERROR,
                                  "GSL Integration failed: \"%s\" (lower=%g upper=%g relative_tolerance=%g error=%p=%g integrator=%d=%s func=%p)",
                                  gsl_strerror(status),
                                  lower,
                                  upper,
                                  relative_tolerance,
                                  (void*)error,
                                  *error,
                                  integrator,
                                  GSL_Integrator_String(integrator),
                                  Cast_function_pointer(func)
            );
    }

    return result;
}
