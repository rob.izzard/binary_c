#pragma once
#ifndef BINARY_C_MATHS_CVODE_H
#define BINARY_C_MATHS_CVODE_H
#define SUNDIALS_STATIC_DEFINE

/*
 * Define these macros to prevent compiler warnings
 * about functions that have just () in the prototype
 * when they should have (void)
 */
#define N_VNewEmpty(...) N_VNewEmpty(void)
#define SUNMatNewEmpty(...) SUNMatNewEmpty(void)
#define SUNLinSolNewEmpty(...) SUNLinSolNewEmpty(void)
#define SUNNonlinSolNewEmpty(...) SUNNonlinSolNewEmpty(void)

/*
 * Get sundials / CVODE headers
 */
#include <sundials/sundials_export.h>   /* RGI need this! */
#include <cvode/cvode.h>               /* prototypes for CVODE fcts., consts.  */
#include <nvector/nvector_serial.h>    /* access to serial N_Vector            */
#include <sunmatrix/sunmatrix_dense.h> /* access to dense SUNMatrix            */
#include <sunlinsol/sunlinsol_dense.h> /* access to dense SUNLinearSolver      */
#include <sundials/sundials_types.h>   /* defs. of realtype, sunindextype      */

/* User-defined vector and matrix accessor macros: Ith, IJth */

/* These macros are defined in order to write code which exactly matches
   the mathematical problem description given above.

   Ith(v,i) references the ith component of the vector v, where i is in
   the range [1..NEQ] and NEQ is defined below. The Ith macro is defined
   using the N_VIth macro in nvector.h. N_VIth numbers the components of
   a vector starting from 0.

   IJth(A,i,j) references the (i,j)th element of the dense matrix A, where
   i and j are in the range [1..NEQ]. The IJth macro is defined using the
   SM_ELEMENT_D macro in dense.h. SM_ELEMENT_D numbers rows and columns of
   a dense matrix starting from 0. */

#define Ith(v,i)    NV_Ith_S(v,i-1)         /* Ith numbers components 1..NEQ */
#define IJth(A,i,j) SM_ELEMENT_D(A,i-1,j-1) /* IJth numbers rows,cols 1..NEQ */


#endif // BINARY_C_MATHS_CVODE_H
