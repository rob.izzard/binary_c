#include "../binary_c.h"
No_empty_translation_unit_warning;

struct int_array_t * new_int_array(const ssize_t n)
{
    /*
     * Make and return a new int array
     */
    struct int_array_t * s = Malloc(sizeof(struct int_array_t));
    s->n = n;
    if(n<=0)
    {
        s->ints = NULL;
    }
    else
    {
        s->ints = Malloc(sizeof(int) * n);
    }
    return s;
}
