#include "../binary_c.h"
No_empty_translation_unit_warning;

/*
 * Check array for nan content.
 * Return TRUE if nan found, FALSE otherwise.
 */
Boolean check_double_array_for_nan(struct double_array_t * Restrict const d)
{
    /*
     * Check array, of size n, for nans
     */
    for(ssize_t i=0; i<d->n; i++)
    {
        if(isnan(d->doubles[i]))
        {
            return TRUE;
        }
    }
    return FALSE;
}
