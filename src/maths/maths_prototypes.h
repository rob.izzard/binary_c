#pragma once
#ifndef MATHS_PROTOTYPES_H
#define MATHS_PROTOTYPES_H

#include "../binary_c_code_options.h"
#include "../binary_c_parameters.h"
#include "../binary_c_macros.h"

#ifdef BINARY_C_USE_LOCAL_RAND48
#include "binary_c_drandr.h"
#endif // BINARY_C_USE_LOCAL_RAND48

/* Mathematical functions, generally stolen from numerical recipes */
#ifndef USE_NATIVE_ISNAN
int my_isnan(double val);
#endif // USE_NATIVE_ISNAN

/** Random number generator function **/
double random_number(struct stardata_t * const stardata,
                     Random_seed * random_seed);

void set_random_buffer(Random_seed seed,
                       Random_buffer * buffer);
double random_number_buffer(const struct stardata_t * const stardata Maybe_unused,
                            Random_buffer *buffer);


double qsimp(double (*func)(double), double a, double b);
double trapzd(double (*func)(double),double a,double b,int n);

double qsimp_array(double (*func)(double,double*,double*), double a, double b,
                   double *xarray, double *yarray);


double trapzd_array(double (*func)(double,double*,double*),
                    double a,
                    double b,
                    int n,
                    double *xarray,
                    double *yarray);

double kaps_rentrop_GSL(struct stardata_t * Restrict const stardata,
                        const double h, // timestep
                        double * Restrict const y, // number densities
                        double * Restrict const y2, // dummy array
                        const unsigned int n, // number of isotopes
                        double * const * const Restrict J, // Jacobian (modified)
                        const double * const Restrict  sigmav, // reaction rates
                        const double * const N, // initial number densities of all isotopes
                        void (*jacobian)(double *const*const J,
                                         const double * const N,
                                         const double * const Restrict sigmav),
                        void (*derivatives)(const double * const y,
                                            double * const dydt,
                                            const double * const Restrict sigmav,
                                            const double * const N),
                        double * const Restrict f1,
                        double * const Restrict f2,
                        double * const Restrict f3,
                        double * const Restrict f4,
                        const double error_threshold,
                        const Boolean solution_must_be_positive,
                        int * const Restrict indx Maybe_unused
    );
void SolveCubic(const double a,
                const double b,
                const double c,
                const double d,
                unsigned int *nsolutions, /* number of REAL solutions */
                double * Restrict x,
                double * Restrict y);
void SolveCubic2(const double b,
                 const double c,
                 const double d,
                 unsigned int * nsolutions, /* number of REAL solutions */
                 double * Restrict x,
                 double * Restrict y);
#ifdef NANCHECKS
void _nancheck(struct stardata_t * Restrict const stardata,
               const char * Restrict const label,
               const double * Restrict const x,
               const unsigned int s);
#endif


void kaps_rentrop_LU_decomp(double *const*const a,
                            const unsigned int n,
                            gsl_matrix * m,
                            gsl_permutation * p
    );
void kaps_rentrop_LU_backsub(struct stardata_t * Restrict const stardata,
                             double *const * const a Maybe_unused,
                             double * const b,
                             const unsigned int n,
                             gsl_matrix  * const m,
                             gsl_permutation * const p
    );





struct probability_distribution_t * new_pdist(struct stardata_t * Restrict const stardata Maybe_unused,
                                              double (*func)(double*),
                                              double xmin,
                                              double xmax,
                                              int nlines,
                                              int maxcount);

double select_from_pdist(struct stardata_t * const stardata,
                         struct probability_distribution_t * const p);


void free_pdist(struct probability_distribution_t * Restrict const p);

double Constant_function fastPow(const double a, const double b) ;

double brent_valist(double (*func)(double,va_list args),
                    const int itmax,
                    const double xmin,
                    const double xmax,
                    const double tol,
                    va_list args
    );

double brent(double (*func)(double,va_list args),
             const double xmin,
             const double xmax,
             const double tol,
             ...
    );




double generic_bisect(int * Restrict error,
                      const int monochecks,
                      int n,
                      brent_function func,
                      double guess,
                      double min,
                      double max,
                      const double tol,
                      const int itmax,
                      const Boolean uselog,
                      const double alpha,
                      ...);
void timestamp (void);

double Li(const int s,
          const double z) Constant_function;
double polyalgorithm(const int s,
                     const double z) Constant_function;



Boolean inverse3( const double theMatrix [/*Y=*/3] [/*X=*/3],
                  double theOutput [/*Y=*/3] [/*X=*/3] );

double GSL_integrator(const double lower,
                      const double upper,
                      const double tolerance,
                      double * const error,
                      const int integrator,
                      double func(double x,void *params),
                      ...);
void setup_GSL_handlers(struct stardata_t * Restrict const stardata Maybe_unused);


void check_nans_are_signalled(void);

#ifdef USE_MERSENNE_TWISTER
#include "mersenne_twister_prototypes.h"
#endif // USE_MERSENNE_TWISTER

double max_from_list(int n,...);


Constant_function double sin2(double x);
Constant_function double cos2(double x);

double generic_minimizer(int * status,
                         const int n,
                         const Boolean logscale,
                         const double accuracy,
                         double *minimumx,
                         const brent_function func,
                         const double min,
                         const double max,
                         ...);


int apply_derivative(struct stardata_t * const stardata,
                     double * const value, /* pointer to the value, in stardata, to which we add the derivative */
                     double * const wanted, /* value wanted: only set when checkfunc fails */
                     double * const d, /* array of derivatives in stardata */
                     const double dt, /* timestep */
                     const Derivative nd, /* derivative number */
                     const Derivative_group derivative_group,
                     void * data,
                     derivative_check_function checkfunc
    );

void test_integrators(struct stardata_t * stardata) No_return;

Constant_function double fermi(const double x,
                               const double a,
                               const double b,
                               const double c,
                               const double d);
Constant_function double dfermi(const double x,
                                const double a,
                                const double b,
                                const double c,
                                const double d);

double Pure_function bin_data(const double x,
                              const double bin_width);
void maths_checks(struct stardata_t * const stardata);


Pure_function double limit_sigfigs(const double f,
                                   const int digits);

Pure_function double bin_data_sigfigs(const double x,
                                      const double bin_width,
                                      const int sigfigs);

/*
 * Vector API
 */
union vector_t vector_add(const union vector_t v1,
                          const union vector_t v2);
union vector_t vector_subtract(const union vector_t v1,
                               const union vector_t v2);
double vector_magnitude(const union vector_t v1);
double vector_dot_product(const union vector_t v1,
                          const union vector_t v2);
union vector_t vector_cross_product(const union vector_t v1,
                                    const union vector_t v2);
union vector_t vector_unit(const union vector_t v);
union vector_t vector_multiply_scalar(const union vector_t v,
                                      const double s);

union vector_t axis_vector(unsigned int axis_number);
union vector_t rotate_vector(union vector_t v,
                             union vector_t axis,
                             const double angle);
double vector_angle(union vector_t v1,
                    union vector_t v2);
union vector_t clean_vector(union vector_t v,
                            const double threshold);
union vector_t vector_plane_projection(union vector_t v,
                                       union vector_t n);
void show_vector(union vector_t v,
                 char * format,
                 ...);
union vector_t vector_multiplet(union vector_t * const X,
                                const double * const multipliers,
                                const Boolean norm,
                                const size_t n);
union vector_t vector_triplet(union vector_t * const X,
                              const double * const multipliers,
                              const Boolean norm);
union vector_t vector_doublet(union vector_t * const X,
                              const double * const multipliers,
                              const Boolean norm);


/*
 * lsoda functions
 */
void lsoda_setup(struct lsoda_t * Restrict const l);

void lsoda(struct lsoda_t * Restrict const l,
           _lsoda_f f, int neq, double *y, double *t, double tout, int itol, double *rtol, double *atol,
           int itask, int *istate, int iopt, int jt,
           int iwork1, int iwork2, int iwork5, int iwork6, int iwork7, int iwork8, int iwork9,
           double rwork1, double rwork5, double rwork6, double rwork7, void *_data);
int n_lsoda(struct lsoda_t * Restrict const l,
            double y[],
            int n,
            double *x,
            double xout,
            double eps,
            _lsoda_f devis,
            struct lsoda_data_t *data);

Boolean check_array_for_nan(const double * const array,
                            const size_t n);
Boolean check_array_for_inf(const double * const array,
                            const size_t n);

Boolean check_double_array_for_inf(struct double_array_t * const Restrict d);
Boolean check_double_array_for_nan(struct double_array_t * const Restrict d);

size_t first_inf_in_array(const double * const array,
                          const size_t n);
size_t first_nan_in_array(const double * const array,
                          const size_t n);
long strtol10(char * ptr,
              char ** endptr);

#ifndef __HAVE_NATIVE_EXP10
double exp10(double x);
#endif // __HAVE_NATIVE_EXP10


void floating_point_exception_checks(struct stardata_t * const stardata);
void floating_point_clear_flags(struct stardata_t * const stardata);
char * floating_point_exception_string(void);

#ifdef KEMP_NOVAE
double interp_lin(const double x,
                  const double xl,
                  const double xh,
                  const double yl,
                  const double yh);
#endif // KEMP_NOVAE
long long int checksum(void * const pointer,
                       const size_t size);
long long int checksum_stardata(struct stardata_t * const stardata);


void free_double_array(struct double_array_t ** Restrict double_array);
void free_int_array(struct int_array_t ** Restrict int_array);
void free_long_int_array(struct long_int_array_t ** Restrict long_int_array);
void free_unsigned_int_array(struct unsigned_int_array_t ** Restrict unsigned_int_array);
void free_Boolean_array(struct Boolean_array_t ** Restrict Boolean_array);

struct double_array_t * new_double_array(const ssize_t n);
struct int_array_t * new_int_array(const ssize_t n);
struct long_int_array_t * new_long_int_array(const ssize_t n);
struct unsigned_int_array_t * new_unsigned_int_array(const ssize_t n);
struct Boolean_array_t * new_Boolean_array(const ssize_t n);

ssize_t first_nan_in_double_array(struct double_array_t * const Restrict double_array);
ssize_t first_inf_in_double_array(struct double_array_t * const Restrict double_array);

double dot_product(const double * const a,
                   const double * const b,
                   const int N);
double multiply_vector(const double * const a,
                       const double b,
                       const int N);
void append_double_array(struct stardata_t * const stardata,
                         struct double_array_t * const a,
                         struct double_array_t * const b);

double sum_C_double_array(const double * const x,
                          const size_t size);

#endif // MATHS_PROTOTYPES
