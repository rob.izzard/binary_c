#include "../binary_c.h"
No_empty_translation_unit_warning;

/*
 * provide exp10() for OSX etc.
 */

#ifndef __HAVE_NATIVE_EXP10

double exp10(double x)
{
    return pow(10.0,x);
}

#endif // __HAVE_NATIVE_EXP10
