#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifndef USE_NATIVE_ISNAN
int my_isnan(const double val)
{
    /*
     * This may be faster than libm's isnan()
     * but may not work.
     */

    /* this works only on 64-bit architectures */
//    union { double f; uint64_t x; } u = { val };
//    return (u.x << 1) > 0x7ff0000000000000u;

    /* this should always work */
    return val != val;
}
#endif // ! USE_NATIVE_ISNAN

#ifdef NANCHECKS
#include <stdint.h>
//9.05
/*
 * Code to check an array for NaNs: if we find one then
 * exit binary_c and give an error
 *
 * x points to the array, which is of size s
 *
 * nancheck_oops is called if a nan is found
 */
static void nancheck_oops(struct stardata_t * const stardata,
                          const char * Restrict const label,
                          const double * Restrict const x,
                          const unsigned int i,
                          const unsigned int s);

#define OOPS nancheck_oops(stardata,label,x,y-x,s)


void _nancheck(struct stardata_t * Restrict const stardata,
               const char * Restrict const label,
               const double * Restrict const x,
               const unsigned int s)
{
    if(stardata->preferences->nanchecks == TRUE &&
       likely(x!=NULL) &&
       likely(s!=0))
    {
        double * y = (double*) x + s;
        while(1)
        {
            y--;
            if(unlikely(_Nanchecker(*y)!=0)) OOPS;
            if(y==x) break;
        }
    }
}


static void nancheck_oops(struct stardata_t * const stardata,
                          const char * Restrict const label,
                          const double * Restrict const x Maybe_unused,
                          const unsigned int i,
                          const unsigned int s
    )

{
    // oops
    printf("NaN detected in %s at %u (max %d)\n",label,i,Max(0,(int)(s-1)));
    fflush(stdout);
    fprintf(stderr,"NaN detected in %s at %u (max %d)\n",label,i,Max(0,(int)(s-1)));
    fflush(stderr);
#ifdef BACKTRACE
    Backtrace;
#endif
    fflush(NULL);
    // segfault for gdb
    //kill(0,SIGSEGV);
    Exit_binary_c(BINARY_C_EXIT_NAN,"NaN detected in %s at %u (max %d)",label,i,Max(0,(int)(s-1)));
    free(NULL);
}
#endif // NANCHECKS
