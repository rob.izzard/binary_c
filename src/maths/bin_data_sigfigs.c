#include "../binary_c.h"
No_empty_translation_unit_warning;


Pure_function double bin_data_sigfigs(const double x,
                                      const double bin_width,
                                      const int sigfigs)
{
    /*
     * bin_data_sigfigs : as bin_data but
     * limits the number of significant figures.
     */
    return limit_sigfigs(bin_data(x,bin_width),sigfigs);
}
