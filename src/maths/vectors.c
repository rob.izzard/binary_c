#include "../binary_c.h"
No_empty_translation_unit_warning;


/*
 * 3D vector operations
 */

union vector_t vector_add(const union vector_t v1,
                          const union vector_t v2)
{
    union vector_t result;
    result.x = v1.x + v2.x;
    result.y = v1.y + v2.y;
    result.z = v1.z + v2.z;
    return result;
}

union vector_t vector_subtract(const union vector_t v1,
                               const union vector_t v2)
{
    union vector_t result;
    result.x = v1.x - v2.x;
    result.y = v1.y - v2.y;
    result.z = v1.z - v2.z;
    return result;
}

double vector_magnitude(const union vector_t v1)
{
    return sqrt(Pow2(v1.x) + Pow2(v1.y) + Pow2(v1.z));
}

union vector_t vector_unit(const union vector_t v)
{
    return vector_multiply_scalar(v,1.0/vector_magnitude(v));
}

double vector_dot_product(const union vector_t v1,
                          const union vector_t v2)
{
    return v1.x * v2.x + v1.y * v2.y + v1.z * v2.z;
}

union vector_t vector_cross_product(const union vector_t v1,
                                    const union vector_t v2)
{
    union vector_t result;
    result.x = v1.y * v2.z - v1.z * v2.y;
    result.y = v1.z * v2.x - v1.x * v2.z;
    result.z = v1.x * v2.y - v1.y * v2.x;
    return result;
}

union vector_t vector_multiply_scalar(const union vector_t v,
                                      const double s)
{
    union vector_t result;
    result.x = s * v.x;
    result.y = s * v.y;
    result.z = s * v.z;
    return result;
}


/*
 * Return the axis vector corresponding to axis_number
 * (defined in binary_c_maths.h)
 */
union vector_t axis_vector(unsigned int axis_number)
{
    union vector_t result;
    if(axis_number == AXIS_X)
    {
        result.x = 1.0;
        result.y = 0.0;
        result.z = 0.0;
    }
    else if(axis_number == AXIS_Y)
    {
        result.x = 0.0;
        result.y = 1.0;
        result.z = 0.0;
    }
    else
    {
        result.x = 0.0;
        result.y = 0.0;
        result.z = 1.0;
    }
    return result;
}

double vector_angle(union vector_t v1,
                    union vector_t v2)
{
    /*
     * Return the angle between two vectors
     */
    return acos(vector_dot_product(v1,v2) /
                (vector_magnitude(v1) * vector_magnitude(v2)));
}

/*
 * "Clean" a vector by setting components with absolute
 * values < threshold to zero.
 */
union vector_t clean_vector(union vector_t v,
                            const double threshold)
{
    const double mag = vector_magnitude(v);
    if(fabs(v.x)/mag < threshold)
    {
        v.x = 0.0;
    }
    if(fabs(v.y)/mag < threshold)
    {
        v.y = 0.0;
    }
    if(fabs(v.z)/mag < threshold)
    {
        v.z = 0.0;
    }
    return v;
}

/*
 * Return the projection of v into the plane normal to n
 */
union vector_t vector_plane_projection(union vector_t v,
                                       union vector_t n)
{
    return vector_subtract(v,
                           vector_multiply_scalar(n,
                                                  vector_dot_product(v,
                                                                     n)));
}

/*
 * Rotate vector about given axis vector, k, by given angle.
 *
 * We normalize and then denormalize in the probably unfounded
 * hope that this improves accuracy.
 */
union vector_t rotate_vector(union vector_t v,
                             union vector_t k,
                             const double angle)
{
    union vector_t result;
    const double mag = vector_magnitude(v);
    const double c = cos(angle);
    const double s = sin(angle);

    /* normalize v */
    v = vector_multiply_scalar(v,1.0/mag);

    /*
     * Rodrigues' rotation formula for rotation
     * about a generic axis
     */
    result =
        vector_add(vector_multiply_scalar(v,c),
                   vector_multiply_scalar(vector_cross_product(k,v),s));
    result =
        vector_add(result,
                   vector_multiply_scalar(k,vector_dot_product(k,v)*(1.0 - c)));

    /* denormalize v */
    result = vector_multiply_scalar(result,mag);

    return result;

    /*
     * Same thing about a particular axis : less flexible,
     * if faster
     */
    /*
      if(axis == AXIS_X)
      {
      result.x = v.x;
      result.y = v.y * c - v.z * s;
      result.z = v.y * s + v.z * c;

      }
      else if(axis == AXIS_Y)
      {
      result.x = v.x * c + v.z * s;
      result.y = v.y;
      result.z = - v.x * s + v.z * c;
      }
      else if(axis == AXIS_Z)
      {
      result.x = v.x * c - v.y * s;
      result.y = v.x * s + v.y * c;
      result.z = v.z;
      }
      v = vector_multiply_scalar(v,mag);
      return result;
    */
}

void show_vector(union vector_t v,
                 char * format,
                 ...)
{
    va_list args;
    va_start(args,format);
    vprintf(format,
            args);
    printf(" : [%g, %g, %g] (%g)\n",
           v.x,
           v.y,
           v.z,
           vector_magnitude(v));
    va_end(args);
}



/*
 * Construct a vector multiplet (array of vectors) from
 * X[i] where each is multiplied by multipliers[i].
 *
 * n is the number of elements in the multiplet
 *
 * if norm is TRUE then the final vector is divided
 * by the sum of the multipliers (e.g. to find
 * a centre of mass vector).
 */
union vector_t vector_multiplet(union vector_t * const X,
                                const double * const multipliers,
                                const Boolean norm,
                                const size_t n)
{
    union vector_t result = Zero_vector;
    size_t i;
    for(i=0;i<n;i++)
    {
        result = vector_add(result,
                            vector_multiply_scalar(X[i],multipliers[i]));
    }
    if(norm == TRUE)
    {
        double normval = 0.0;
        for(i=0;i<n;i++)
        {
            normval += multipliers[0];
        }
        result = vector_multiply_scalar(result,
                                        1.0/normval);
    }
    return result;
}

/*
 * Wrapper to make a vector triplet
 */
union vector_t vector_triplet(union vector_t * const X,
                              const double * const multipliers,
                              const Boolean norm)
{
    return vector_multiplet(X,multipliers,norm,3);
}

/*
 * Wrapper to make a vector doublet
 */
union vector_t vector_doublet(union vector_t * const X,
                              const double * const multipliers,
                              const Boolean norm)
{
    return vector_multiplet(X,multipliers,norm,2);
}
