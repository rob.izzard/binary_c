
#ifndef MERSENNE_TWISTER_PROTOTYPES_H
#define MERSENNE_TWISTER_PROTOTYPES_H

unsigned long long genrand64_int64(Random_buffer * buffer);
void init_genrand64(unsigned long long seed,
                    Random_buffer * buffer);
//void init_by_array64(unsigned long long init_key[],
//		     unsigned long long key_length);

long long genrand64_int63(Random_buffer * buffer);
double genrand64_real1(Random_buffer * buffer);
double genrand64_real2(Random_buffer * buffer);
double genrand64_real3(Random_buffer * buffer);


#endif // MERSENNE_TWISTER_PROTOTYPES_H
