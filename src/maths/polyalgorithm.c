#include "../binary_c.h"
No_empty_translation_unit_warning;



double Constant_function polyalgorithm(const int s,
                                       const double z)
{
    /* wrapper */
    return Li(s,z);
}

double Constant_function Li(const int s,
                            const double z)
{
    /*
     * Polylogarithm
     * https://en.wikipedia.org/wiki/Polylogarithm
     *
     * Proceed up to 100 terms or when the terms change 
     * by < LI_THRESH, whichever is first.
     */
    int k;

#define LI_THRESH 1e-8
    double zz = z;
    double x = z;
    for(k=2;k<100;k++)
    {
        zz *= z;
        double dx = zz * pow((double)k,-s);
        x += dx;
        
        /* break out if accurate enough */
        if(fabs(dx/x) < LI_THRESH)
        {
            break;
        }
    }
    return x;
}
