#include "../binary_c.h"
No_empty_translation_unit_warning;

/*
 * Check array for nan, return the index of the first
 * found nan, or -1 if not found.
 */
ssize_t first_nan_in_double_array(struct double_array_t * const Restrict double_array)
{
    /*
     * Check array, of size n, for nans
     */
    for(ssize_t i=0; i<double_array->n; i++)
    {
        if(isnan(double_array->doubles[i]))
        {
            return i;
        }
    }
    return -1;
}
