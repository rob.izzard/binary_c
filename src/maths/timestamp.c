
/*
  Purpose:

    TIMESTAMP prints the current YMDHMS date as a time stamp.

  Example:

    31 May 2001 09:45:54 AM

  Licensing:

    This code is distributed under the GNU LGPL license. 

  Modified:

    24 September 2003

  Author:

    John Burkardt

  Parameters:

    None
*/
#include "../binary_c.h"
No_empty_translation_unit_warning;

void timestamp (void)
{
#define TIME_SIZE 40

    static char time_buffer[TIME_SIZE];
    const struct tm *tm;
    size_t len Maybe_unused;
    time_t now;
    now = time ( NULL );
    tm = localtime ( &now );
    len = strftime ( time_buffer, TIME_SIZE, "%d %B %Y %I:%M:%S %p", tm );
    printf ( "%s\n", time_buffer );
    return;
# undef TIME_SIZE
}
