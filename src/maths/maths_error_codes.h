#ifndef MATHS_ERROR_CODES_H
#define MATHS_ERROR_CODES_H

#include "bisect_error_codes.def"

/*
 * Construct generic_bisect errors
 */
#undef X
#define X(CODE,TEXT) CODE,
enum { BINARY_C_BISECT_ERROR_CODES };

#undef X
#define X(CODE,TEXT) #CODE,
static char * binary_c_bisect_error_code_macros[] Maybe_unused = { BINARY_C_BISECT_ERROR_CODES };

#undef X
#define X(CODE,TEXT) TEXT,
static char * binary_c_bisect_error_code_strings[] Maybe_unused = { BINARY_C_BISECT_ERROR_CODES };

#define Bisect_error_string(N) Array_string(binary_c_bisect_error_code_strings,(N))

#undef X

#endif // MATHS_ERROR_CODES_H
