#include "../binary_c.h"
No_empty_translation_unit_warning;

#include <gsl/gsl_errno.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_roots.h>
/*
 * Brent interpolator using the (free) GSL library.
 *
 * Based on the example at
 *
 * https://www.gnu.org/software/gsl/manual/html_node/Root-Finding-Examples.html
 *
 * Input parameters:
 *
 * func is the function that does the computation: this should
 *      return (calulated / constraint - 1.0)
 *
 * wrapper_func calls func after testing its args for things
 *      such as whether log spacing should be used. If this is NULL
 *      it is ignored, and func is called directly.
 *
 *
 * [If you seek constraint = 0, you'll have to rewrite your equations.]
 *
 * itmax is the maximum number of iterations
 *
 * xmin and xmax are the range over which we interpolate
 *
 * tol is the *relative* tolerance required for convergence, i.e.
 * for convergence : fabs(calculated/constraint - 1.0) < tol
 */


double brent_GSL_valist(brent_function wrapper_func,
                        brent_function func,
                        const int itmax,
                        const double xmin,
                        const double xmax,
                        const double tol,
                        const Boolean uselog,
                        va_list args)
{
    double ret = -1.0;
    const int vb = 0;
    int status, iter = 0;

    /*
     * the parameters sent to the function should
     * be inside a GSL_args struct
     */
    struct GSL_args_t * GSL_args = Malloc(sizeof(struct GSL_args_t));
    GSL_args->func = func;
    GSL_args->uselog = uselog;

    if(vb)
    {
        printf("setup brent : itmax=%d xmin=%g xmax=%g tol=%g\n",
               itmax,xmin,xmax,tol
            );
        fflush(NULL);
    }

    const gsl_root_fsolver_type * T =
        GSL_ROOT_FSOLVER;

    if(vb)
    {
        printf("alloc solver\n");fflush(NULL);
    }

    gsl_root_fsolver * s =
        gsl_root_fsolver_alloc (T);

#ifdef ALLOC_CHECKS
    if(s==NULL)
    {
        Backtrace;
        Exit_binary_c_no_stardata(
            BINARY_C_ALLOC_FAILED,
            "Failed to allocate memory for GSL root fsolver\n");
    }
#endif

    gsl_function * F = Malloc(sizeof(gsl_function));

    if(vb)
    {
        printf("set F\n");fflush(NULL);
    }
    F->function = wrapper_func ? wrapper_func : func;


    /*
     * Calling gsl_root_fsolver_set requires args to
     * be accessed, which messes up the va_list, so use
     * a copy (args_copy) instead
     */
    F->params = GSL_args;
    va_copy(GSL_args->args,args);

    if(vb)
    {
        printf("calling gsl_root_fsolver_set with range xmin=%g xmax=%g\n",
               xmin,
               xmax
            );
        fflush(NULL);
    }
    gsl_set_error_handler_off();
    status = gsl_root_fsolver_set(s,F,xmin,xmax);

    if(status)
    {
        Backtrace;
        printf("F = %p\n",(void*)F);
        printf("xmin=%g xmax=%g\n",xmin,xmax);
        printf("s=%p\n",(void*)s);
        printf("status = %d\n",status);
        printf("error string = %s\n",gsl_strerror(status));
        Exit_binary_c_no_stardata(
            BINARY_C_GSL_ERROR,
            "GSL failed to set root fsolver function F=%p with bounds min %g, max %g. GSL s=%p, status = %d\n",
            (void*)F,
            xmin,
            xmax,
            (void*)s,
            status
            );
    }

    if(vb){
        printf("using method %s\n",
               gsl_root_fsolver_name(s));

        printf ("%5s [%9s, %9s] %9s %10s %9s\n",
                "iter", "lower", "upper", "root",
                "err", "err(est)");
    }

    double x_lo = xmin;
    double x_hi = xmax;
    double r = 0.0;
    do
    {
        iter++;
        if(vb) printf("iter %d\n",iter);
        status = gsl_root_fsolver_iterate(s);

        if(status == GSL_SUCCESS)
        {
            r = gsl_root_fsolver_root(s);
            x_lo = gsl_root_fsolver_x_lower(s);
            x_hi = gsl_root_fsolver_x_upper(s);
            status = gsl_root_test_interval(x_lo,
                                            x_hi,
                                            0,
                                            tol);

            if(vb)
            {
                if (status == GSL_SUCCESS)
                    printf ("Converged:\n");

                printf ("%5d [%g, %g] %g %g\n",
                        iter, x_lo, x_hi,
                        r,
                        x_hi - x_lo);
            }
        }
    }
    while (status == GSL_CONTINUE && iter < itmax);

    /*
     * Free and return
     */
    gsl_root_fsolver_free(s);
    Safe_free(F);
    Safe_free(GSL_args);

    ret = r;

    return ret;
}
