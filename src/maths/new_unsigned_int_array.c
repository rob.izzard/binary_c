#include "../binary_c.h"
No_empty_translation_unit_warning;

struct unsigned_int_array_t * new_unsigned_int_array(const ssize_t n)
{
    /*
     * Make and return a new unsigned_int array
     */
    struct unsigned_int_array_t * s = Malloc(sizeof(struct unsigned_int_array_t));
    s->n = n;
    if(n<=0)
    {
        s->unsigned_ints = NULL;
    }
    else
    {
        s->unsigned_ints = Malloc(sizeof(unsigned int) * n);
    }
    return s;
}
