#include "../binary_c.h"
No_empty_translation_unit_warning;

/*
 * Check for floating-point exceptions after each timestep
 */

void floating_point_exception_checks(struct stardata_t * const stardata)
{
    if(fetestexcept(FE_ALL_EXCEPT))
    {
        /*
         * Exception found!
         */
        if(stardata->preferences->float_overflow_checks == 1)
        {
            fprintf(stderr,"Floating point exception detected\n");
        }
        else
        {
            Exit_binary_c(BINARY_C_FLOATING_POINT_ERROR,
                          "Floating point exception detected\n");
        }
    }
}
