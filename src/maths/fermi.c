#include "../binary_c.h"
No_empty_translation_unit_warning;


Constant_function double fermi(const double x,
                        const double a, 
                        const double b,
                        const double c,
                        const double d)
{
    /*
     * generalized fermi function
     * 0 for x<<d
     * a for x>>d
     * smooth in between according to b,c
     */
    return a*(1.0-1.0/(b+pow(c,d-x)));
}

Constant_function double dfermi(const double x,
                         const double a, 
                         const double b,
                         const double c,
                         const double d) 
{
    /*
     * derivative of generalized fermi function
     */
    return -a*log10(c)*pow(c,x-d) /
        Pow2(b+pow(c,x-d)) ;

}
