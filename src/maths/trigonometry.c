
#include "binary_c.h"

Constant_function double sin2(double x)
{
    /* sin squared */
    const double y = sin(x);
    return Pow2(y);
}

Constant_function double cos2(double x)
{
    /* cos squared */
    const double y = cos(x);
    return Pow2(y);
}
