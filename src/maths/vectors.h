#pragma once
#ifndef BINARY_C_VECTORS_H
#define BINARY_C_VECTORS_H

/*
 * Header file for the vector functions in maths/vectors.c
*/

/*
 * A zero-vector
 */
#define Zero_vector {.component = {0.0, 0.0, 0.0} }

/*
 * Numbers for axes
 */
#define AXIS_X 0
#define AXIS_Y 1
#define AXIS_Z 2


#endif // BINARY_C_VECTORS_H
