#include "../binary_c.h"
No_empty_translation_unit_warning;
static uint16_t fletcher16(const uint8_t * data,
                           size_t length);

long long int checksum(void * const pointer,
                       const size_t size)
{
    /*
     * Simple checksum function
     */
    /*
    long long int sum = 255;
    unsigned char * p = (unsigned char *)pointer;
    unsigned char * max = p + size;
    printf("p = %p, max = %p\n",(void*)p,(void*)max);
    while(p < max)
    {
        printf("or %lld\n",(long long int)*p);
        sum ^= (long long int)*p;
        p++;
    }
    */

    /*
     * XOR
     */
    /*
    uint8_t checksum = 0;
    const char * const max = (char*)pointer + size;
    char * data = (char*) pointer;
    while(data < max)
    {
        char d = *data++;
        printf("check \"%c\" %u\n",d,(unsigned int)d);
        checksum ^= d;
    }
    return checksum;
    */

    return (long long int)fletcher16(pointer,
                                     size);
}

static uint16_t fletcher16(const uint8_t * data,
                           const size_t size)
{
    size_t length = size / sizeof(uint16_t);
    uint16_t sum1 = 0xff, sum2 = 0xff;

    while(length)
    {
        size_t tlen = length > 20 ? 20 : length;
        length -= tlen;
        do
        {
            sum1 += *data++;
            sum2 += sum1;
        }while (--tlen);

        sum1 = (sum1 & 0xff) + (sum1 >> 8);
        sum2 = (sum2 & 0xff) + (sum2 >> 8);
    }

    sum1 = (sum1 & 0xff) + (sum1 >> 8);
    sum2 = (sum2 & 0xff) + (sum2 >> 8);
    return (sum2 << 8) | sum1;
}
