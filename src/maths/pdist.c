#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "nintlib.h"

/*
 * Monte-Carlo selection from a given distribution
 */

double select_from_pdist(struct stardata_t * const stardata,
                         struct probability_distribution_t * const p)
{
    /*
     * Given a 1D probability function depending
     * select a point from that distribution.
     */
    double result[1] = { 0.0 };
    double rand[1];
    Boolean brk = FALSE;
    int count=0;
    p->error=0;

    while(brk == FALSE &&
          (p->maxcount==0 || count < p->maxcount))
    {
        // choose random number
        rand[0] = random_number(stardata,&stardata->common.random_seed);

        // hence result
        rinterpolate(p->table,
                     stardata->persistent_data->rinterpolate_data,
                     1,
                     1,
                     p->nlines,
                     rand,
                     result,
                     0);

        /* check probability density is non-zero */
        if(Is_not_zero(p->func(result))) brk = TRUE;
    }

    /* too many iterations is an error */
    if(count == p->maxcount) p->error = 1;

    /* return the result (even in the error case) */
    return result[0];
}


struct probability_distribution_t * new_pdist(struct stardata_t * Restrict const stardata Maybe_unused,
                                              double (*func)(double*),
                                              double xmin,
                                              double xmax,
                                              int nlines,
                                              int maxcount)
{
    /*
     * Set up a new probability distribution struct,
     * with n variables, and nlines in the interpolation table
     */

    struct probability_distribution_t * p =
        Malloc(sizeof(struct probability_distribution_t));
    p->nlines = nlines;
    p->func = func;
    p->xmin = xmin;
    p->xmax = xmax;
    p->maxcount=maxcount;
    int i;

    /*
     * Integrate over the space in each dimension
     */
    double * inttable = NULL;

    /*
     * perform the integration,
     * save the results in inttable
     */
    int flag,neval;
    int sub_num[1];
    sub_num[0] = p->nlines*2; // Shannon sampling
    int it_max = 4;
    double tol = 1e-6;
    double max[1],min[1];
    min[0]=xmin;
    max[0]=xmax;

    /*
     * Use a Romberg integrator
     */
    double norm = romberg_nd_RGI(
        p->func,
        min,
        max,
        1,
        sub_num,
        it_max,
        tol,
        &flag,
        &neval,
        &inttable
        );

    /* save number of lines */
    p->nlines = sub_num[0];

    /* copy to struct memory */
    size_t copy_size = (sizeof(double) * p->nlines * 2+4);
    p->table = Malloc(copy_size);

    //printf("table returned %d lines\n",p->nlines);

    /*
     * swap columns, and renormalize the table
     * NB ignore the first line at 0,0 : this may not be the
     * true zero.
     */
    norm = 1.0/norm;
    double prev=0.0;
    nlines=0;
    for(i=1;i<p->nlines;i++)
    {
        //printf("Check %g %g\n",inttable[i*2],inttable[i*2+1]);
        if(i==0 || !Fequal(inttable[i*2+1],prev))
        {
            p->table[nlines*2+1] = inttable[i*2];
            p->table[nlines*2] = norm * inttable[i*2+1];
            //printf("# TABLE %d : %g %g\n",nlines,p->table[nlines*2],p->table[nlines*2+1]);
            nlines++;
        }
        prev = inttable[i*2+1];
    }
    p->nlines = nlines;

    /* free the inttable */
    Safe_free(inttable);

    /* return pointer to the struct */
    return p;
}

void free_pdist(struct probability_distribution_t * Restrict const p)
{
    Safe_free(p->table);
    Unsafe_free(p);
}

/*
 * test code
 */

/*

double testfunc(double *x)
{
    // Density function
    double f;
    //f = (x<0.2 || (x>0.3 && x<0.4) || x>0.8) ? 1.0 : 0.0;
    f=sin(x[0]);

    return f;
}


 struct probability_distribution_t * pdist;
    stardata->common.random_seed = random_seed();
    stardata->common.init_random_seed = stardata->common.random_seed;

    pdist = new_pdist(&testfunc,
                      0.0,
                      PI,
                      1000,
                      100);

    int count=0;
    while(count++<100000)
    {
        double x = select_from_pdist(stardata,pdist);
        if(!pdist->error)
        {
            printf("%g\n",x);
        }
    }
    free_pdist(pdist);

    Exit_binary_c(0,"pnats");

*/
