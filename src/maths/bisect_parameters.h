#pragma once
#ifndef BISECT_PARAMETERS_H
#define BISECT_PARAMETERS_H

#include "bisect_parameters.def"

#undef X
#define X(CODE,STRING) BISECTOR_##CODE ,
enum { BISECTOR_USER_LIST };
#undef X

#define X(CODE,STRING) STRING,
static const char * const bisect_users[] Maybe_unused = { BISECTOR_USER_LIST };
#undef X

#define X(CODE) BISECT_##CODE ,
enum { BISECTOR_MONOCHECKS_LIST };
#undef X

#endif // BISECT_PARAMETERS_H
