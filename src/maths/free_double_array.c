#include "../binary_c.h"
No_empty_translation_unit_warning;

/*
 * Free a double array
 */
void free_double_array(struct double_array_t ** Restrict double_array)
{
    if(double_array != NULL &&
       *double_array != NULL)
    {
        Safe_free((*double_array)->doubles);
        Safe_free_nocheck(*double_array);
    }
}
