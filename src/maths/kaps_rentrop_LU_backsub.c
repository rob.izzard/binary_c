#include "../binary_c.h"
No_empty_translation_unit_warning;

#include <gsl/gsl_linalg.h>
#define gsl_b stardata->tmpstore->kaps_rentrop_LU_backsub_b
#define gsl_x stardata->tmpstore->kaps_rentrop_LU_backsub_x

void kaps_rentrop_LU_backsub(struct stardata_t * Restrict const stardata,
                             double *const * const a Maybe_unused,
                             double * const b,
                             const unsigned int n2,
                             gsl_matrix * const m,
                             gsl_permutation * const p)
{
    /*
     * LU substitution using GSL.
     *
     * Note: the gsl_b and gsl_x should be pre-allocated.
     */
    for(unsigned int i=1;i<n2;i++)
    {
        gsl_vector_set(gsl_b,i-1,b[i]);
    }
    gsl_linalg_LU_solve(m,
                        p,
                        gsl_b,
                        gsl_x);
    for(unsigned int i=1;i<n2;i++)
    {
        b[i] = gsl_vector_get(gsl_x,i-1);
    }
}
