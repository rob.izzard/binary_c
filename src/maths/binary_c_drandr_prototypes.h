#pragma once
#ifndef BINARY_C_DRANDR_PROTOTYPES_H
#define BINARY_C_DRANDR_PROTOTYPES_H

#ifdef BINARY_C_USE_LOCAL_RAND48

/*
 * An implementation of srand48_r and drand48_r
 * because these are not available on this operating
 * system (which is probably some version of OSX). 
 *
 * These functions were taken from glibc-2.27 on 15/06/2018. 
 */

#include "binary_c_drandr_types.h"

/* Copyright (C) 1995-2018 Free Software Foundation, Inc.
   This file is part of the GNU C Library.
   Contributed by Ulrich Drepper <drepper@gnu.ai.mit.edu>, August 1995.

   The GNU C Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.

   The GNU C Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with the GNU C Library; if not, see
   <http://www.gnu.org/licenses/>.  */


/* Return non-negative, double-precision floating-point value in [0.0,1.0).  */
int binary_c_drand48_r (struct binary_c_drand48_data * Restrict __buffer,
                        double * Restrict __result);
int binary_c_erand48_r (unsigned short int __xsubi[3],
                        struct binary_c_drand48_data * Restrict __buffer,
                        double *Restrict __result);

/* Seed random number generator.  */
int binary_c_srand48_r (long int __seedval,
                        struct binary_c_drand48_data *__buffer);

#endif // BINARY_C_USE_LOCAL_RAND48

#endif // BINARY_C_DRANDR_PROTOTYPES_H
