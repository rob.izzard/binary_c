#include "../binary_c.h"
No_empty_translation_unit_warning;

/*
 * Check array for inf content.
 * Return TRUE if inf found, FALSE otherwise.
 */
Boolean check_array_for_inf(const double * const array,
                            const size_t n)
{
    /*
     * Check array, of size n, for infs
     */
    for(size_t i=0; i<n; i++)
    {
        if(isinf(array[i]))
        {
            return TRUE;
        }
    }
    return FALSE;
}
