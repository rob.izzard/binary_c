#include "../binary_c.h"
No_empty_translation_unit_warning;


#include <gsl/gsl_errno.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_min.h>

double generic_minimizer(int * status,
                         const int n,
                         const Boolean logscale,
                         const double accuracy,
                         double *minimumx,
                         const brent_function func,
                         const double min,
                         const double max,
                         ...)
{
    /*
     * Find the minimum of func, f(x), over the range x=min to x=max.
     *
     * To do this we first find f(min) and f(max). Then we take
     * n trial points inside the range min to max, and if one is
     * less than f(min) and less than f(max), we call the Brent
     * minimizer of GSL (which can deal with a bracketed minimum)
     * which finds the minimum to within a relative accuracy given
     * as the "accuracy" argument.
     *
     * Otherwise, we return the minimum value found, and set
     * *minimumx to the corresponding x if minimumx is non-NULL.
     *
     * Set status=0 on success, otherwise status is an error
     * code:
     * -1 : min==max, no bracket is possible, return f(x=min==max)
     * 0  : no error
     * >0 : GSL error code
     */


    /*
     * we have no stardata, so Dprint will not work.
     * instead use a private vb Boolean
     */
#undef Dprint
    const int vb = 0; /* set to >=1 for verbose output */
#define Dprint if(vb)printf

    Dprint("generic minimizer: status=%p, n=%d, logscale=%d, func=%p, min=%g, max=%g\n",
           (void*)status,n,logscale,

           Cast_function_pointer(func),
           min,max);

    *status = 0;

    /* returned value(s) */
    double miny,minx;

    /*
     * Put everything in a GSL_args struct
     */
    struct GSL_args_t * GSL_args = Malloc(sizeof(struct GSL_args_t));
    GSL_args->func = func;
    GSL_args->uselog = logscale;
    va_start(GSL_args->args,max);

    /*
     * check min != max, if min == max then
     * just evaluate at min (==max) and return
     */
    if(Fequal(min,max))
    {
        *status = -1;
        minx = min;
        miny = func(min,GSL_args);
    }
    else
    {
        /*
         * First guess is the minimum of the function
         * at the end points
         */
        const double f_min = func(min,GSL_args);
        const double f_max = func(max,GSL_args);
        miny = MinX(min,f_min,max,f_max,&minx);

        Dprint("first guess %g (fmin = %g, fmax = %g)\n",
               miny,
               f_min,
               f_max);

        /*
         * then make sure there is a minimum somewhere between
         * min and max
         */
        double loopmin,loopmax,dloop;
        double (*loopfunc)(double);

        if(logscale == TRUE)
        {
            loopmin = log(min);
            loopmax = log(max);
            loopfunc = &exp;
        }
        else
        {
            loopmin = min;
            loopmax = max;
            loopfunc = NULL;
        }
        dloop = (loopmax - loopmin) / ((double)n - 1.0);

        /*
         * loop to find a minimum
         */
        double x;
        Boolean bracketed = FALSE;
        for(x = loopmin + dloop; x < loopmax + dloop*0.5; x+=dloop)
        {
            const double y = func((loopfunc!=NULL ? loopfunc(x) : x),GSL_args);
            Dprint("GENERIC MINIMIZER (linear) f(%g) = %g cf. miny %g : less? %d (diff %g)\n",
                   x,y,miny,y<miny,y-miny);

            if(Less_than(y,miny) &&
               Is_not_zero(miny) &&
               fabs(y/miny-1.0) < TINY)
            {
                /* found new minimum : store and exit loop */
                minx = x;
                miny = y;
                bracketed = TRUE;
                x = loopmax + dloop;
            }
        }

        Dprint("bracketed? %d\n",bracketed);

        if(bracketed == TRUE)
        {
            /*
             * We're bracketed : use GSL's Brent minimizer
             */
            gsl_function * F = Malloc(sizeof(gsl_function));
            F->function = func;
            F->params = GSL_args;
            const gsl_min_fminimizer_type * T = gsl_min_fminimizer_brent;
            gsl_min_fminimizer * s = gsl_min_fminimizer_alloc(T);
            /*
              printf("min %g\n",min);
              printf("max %g\n",max);
            */
            *status = gsl_min_fminimizer_set(s,
                                             F,
                                             0.5*(min + max),
                                             min,
                                             max);

            /*
            printf("TEST state = %p\n",(void*)s->state);
            printf("TEST function = %p\n",(void*)s->function);
            printf("TEST f min %g\n",s->f_minimum);
            printf("TEST f min %g\n",s->f_lower);
            printf("TEST f min %g\n",s->f_upper);
            printf("TEST x min %g\n",s->x_minimum);
            printf("TEST x min %g\n",s->x_lower);
            printf("TEST x min %g\n",s->x_upper);
            */


            int iter = 0,max_iter = 100;
            do
            {
                iter++;
                *status = gsl_min_fminimizer_iterate (s);
                const double lower = gsl_min_fminimizer_x_lower (s);
                const double upper = gsl_min_fminimizer_x_upper (s);
                Dprint("iter %d : %g < minQ = %g < %g\n",iter,min,lower,upper);

                /* converge once min==max within 1% */
                *status = gsl_min_test_interval(lower,
                                                upper,
                                                0.0,
                                                accuracy);
            } while (*status == GSL_CONTINUE && iter < max_iter);

            /* best estimate of radius at which we have a minimum */
            miny = gsl_min_fminimizer_f_minimum (s);
            minx = gsl_min_fminimizer_x_minimum (s);

            /* free memory */
            gsl_min_fminimizer_free (s);
            Safe_free(F);
        }
    }

    va_end(GSL_args->args);
    Safe_free(GSL_args);

    /* store x-value if non-null pointer passed in */
    if(minimumx != NULL)
    {
        *minimumx = minx;
    }

    Dprint("Generic minimizer return x=%g, y=%g\n",minx,miny);
    return miny;
}
