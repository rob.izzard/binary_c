#include "../binary_c.h"
No_empty_translation_unit_warning;


void SolveCubic(const double a,
                const double b,
                const double c,
                const double d,
                unsigned int *nsolutions, /* number of REAL solutions */
                double * Restrict x,
                double * Restrict y)
{
    /*
     * Wrapper for SolveCubic2 to perform the divisions
     * first.
     */
    SolveCubic2(b/a,c/a,d/a,nsolutions,x,y);
}
