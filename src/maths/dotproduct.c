#include "../binary_c.h"
No_empty_translation_unit_warning;


#include "memory/memory_alignment_checks.h"

#ifdef AVX2_EXTENSIONS
#include <x86intrin.h>
//static double reduce_vector1(const __m256d input);
static double reduce_vector2(const __m256d input);
static double dot_product_avx2(const double * const a,
                               const double * const b,
                               const int N);
#endif // AVX2_EXTENSIONS

static double Maybe_unused dot_product_slow(const double * const a,
                                            const double * const b,
                                            const int N);
static double Maybe_unused dot_product_fast(const double * const a,
                                            const double * const b,
                                            const int N);

double dot_product(const double * const a,
                   const double * const b,
                   const int N)
{
#ifdef AVX2_EXTENSIONS
    return dot_product_avx2(a,b,N);
#else
    return N>15 ? dot_product_fast(a,b,N) : dot_product_slow(a,b,N);
#endif
}

/************************************************************
 * Local (static) functions
 ************************************************************/

static double Maybe_unused dot_product_slow(const double * const a,
                                            const double * const b,
                                            const int N)
{
    double answer = 0.0;
    for(int ii = 0; ii < N; ++ii)
    {
        answer += a[ii]*b[ii];
    }
    return answer;
}

static double Maybe_unused dot_product_fast(const double * const a,
                                            const double * const b,
                                            const int N)
{
    const int _max = (N/16)*16;
    double answer[16] = { 0.0 };
    PRAGMA_GCC_IVDEP
    for(int i = 0; i < _max; )
    {
        answer[0] += a[i]*b[i]; i++;
        answer[1] += a[i]*b[i]; i++;
        answer[2] += a[i]*b[i]; i++;
        answer[3] += a[i]*b[i]; i++;
        answer[4] += a[i]*b[i]; i++;
        answer[5] += a[i]*b[i]; i++;
        answer[6] += a[i]*b[i]; i++;
        answer[7] += a[i]*b[i]; i++;
        answer[8] += a[i]*b[i]; i++;
        answer[9] += a[i]*b[i]; i++;
        answer[10] += a[i]*b[i]; i++;
        answer[11] += a[i]*b[i]; i++;
        answer[12] += a[i]*b[i]; i++;
        answer[13] += a[i]*b[i]; i++;
        answer[14] += a[i]*b[i]; i++;
        answer[15] += a[i]*b[i]; i++;
    }
    for(int i = _max; i < N; i++)
    {
        answer[0] += a[i]*b[i];
    }
    return answer[0] + answer[1] + answer[2] + answer[3] + answer[4] + answer[5] + answer[6] + answer[7] + answer[8] + answer[9] + answer[10] + answer[11] + answer[12] + answer[13] + answer[14] + answer[15];
}

// was 0.56s


#ifdef AVX2_EXTENSIONS
/*
 * Code based on
 * https://github.com/kshitijl/avx2-examples/blob/master/examples/04-dot-product.c
 */
static double reduce_vector2(const __m256d input)
{
    const __m256d temp = _mm256_hadd_pd(input, input);
    const __m128d sum_high = _mm256_extractf128_pd(temp, 1);
    const __m128d result = _mm_add_pd(sum_high, _mm256_castpd256_pd128(temp));
    return ((double*)&result)[0];
}

double dot_product_avx2(const double * const a,
                        const double * const b,
                        const int n)
{
    __m256d sum_vec = _mm256_set_pd(0.0, 0.0, 0.0, 0.0);

    /*
    printf("avx dot product %p %p %d\naligned %zu %zu : %d %d\n",
           (void*)a,
           (void*)b,
           n,
           alignof(a),
           alignof(b),

        );
    fflush(stdout);
    */

    /*
     * Load in each block of four doubles and
     * do the dot product on them.
     *
     * Note that we use load_pd on the assumption
     * that the arrays are 16-byte (256-bit) aligned.
     *
     * If they are not, use _mm256_loadu_pd.
     */
    for(int ii = 0; ii < n; ii+=4)
    {
#ifdef AVX2_ASSUME_ALIGNED
#define __load_func _mm256_load_pd
#else
#define __load_func _mm256_loadu_pd
#endif //  AVX2_ASSUME_ALIGNED

        const __m256d x = __load_func (a + ii);
        const __m256d y = __load_func (b + ii);
        const __m256d z = _mm256_mul_pd(x,y);
        sum_vec = _mm256_add_pd(sum_vec, z);
    }

    /*
     * Remaining elements
     */
    double final = 0.0;
    for(int ii = n - n%4; ii < n; ii++)
    {
        final += a[ii] * b[ii];
    }
    return reduce_vector2(sum_vec) + final;
}

#endif // AVX2_EXTENSIONS
