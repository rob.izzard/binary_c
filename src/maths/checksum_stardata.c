#include "../binary_c.h"
No_empty_translation_unit_warning;


long long int checksum_stardata(struct stardata_t * const stardata)
{
    /*
     * Simple checksum function of a stardata struct
     *
     * We can only checksum data we think should not change.
     */
#define Sum(STRUCT)                             \
    sum += checksum((STRUCT),sizeof(STRUCT));

#define Sum_data_table(STRUCT)                              \
    if((STRUCT) != NULL)                                    \
    {                                                       \
        sum += (long long int)(STRUCT)->nparam;             \
        sum += (long long int)(STRUCT)->ndata;              \
        sum += (long long int)(STRUCT)->nlines;             \
        sum += checksum((STRUCT)->data,sizeof(STRUCT));     \
    }

    long long int sum = 0;
    Sum(stardata->store->fallback_preferences);
    Sum_data_table(stardata->store->jaschek_jaschek_dwarf);
    Sum_data_table(stardata->store->jaschek_jaschek_giant);
    Sum_data_table(stardata->store->jaschek_jaschek_supergiant);
    Sum_data_table(stardata->store->miller_bertolami);
    Sum_data_table(stardata->store->miller_bertolami_coeffs_L);
    Sum_data_table(stardata->store->miller_bertolami_coeffs_R);
    Sum_data_table(stardata->store->Temmink2022_RLOF);
    Sum_data_table(stardata->store->massive_MS_lifetimes);
#ifdef NUCSYN
    Sum(stardata->store->mnuc);
    Sum(stardata->store->imnuc);
    Sum(stardata->store->mnuc_amu);
    Sum(stardata->store->imnuc_amu);
    Sum(stardata->store->molweight);
    Sum(stardata->store->ZonA);
    Sum(stardata->store->atomic_number);
    Sum(stardata->store->nucleon_number);
    Sum(stardata->store->ionised_molecular_weight_multiplier);
    Sum(stardata->store->nisotopes);
    Sum(stardata->store->element_info);
    Sum_data_table(stardata->store->sigmav);
    Sum_data_table(stardata->store->novae_JH98_CO);
    Sum_data_table(stardata->store->novae_JH98_ONe);
    Sum_data_table(stardata->store->novae_Jose2022_ONe);
    Sum_data_table(stardata->persistent_data->Limongi_Chieffi_2018_mapped);
#endif // NUCSYN
#ifdef STELLAR_COLOURS
    Sum_data_table(stardata->store->Eldridge2012_colours);
#endif//STELLAR_COLOURS
    Sum_data_table(stardata->store->carrasco2014_table3);
    Sum_data_table(stardata->store->carrasco2014_table4);
    Sum_data_table(stardata->store->carrasco2014_table5);
    Sum_data_table(stardata->store->carrasco2014_cooling_table3);
    Sum_data_table(stardata->store->carrasco2014_cooling_table4);
    Sum_data_table(stardata->store->carrasco2014_cooling_table5);

    Sum_data_table(stardata->persistent_data->Temmink2022_RLOF_mapped);
    Sum_data_table(stardata->persistent_data->Peters_grav_wave_merger_time_table);
#ifdef COMENV_WANG2016
    Sum_data_table(stardata->persistent_data->comenv_lambda_table);
#endif // COMENV_WANG2016
    return sum;
}
