#pragma once
#ifndef MATHS_FUNCTION_MACROS_H
#define MATHS_FUNCTION_MACROS_H

/*
 * Mathematics function macros for binary_c, including
 * GSL macros.
 */


/*
 * Logical not
 */
#define Not(A) ((A)==TRUE ? FALSE : TRUE)

/*
 * Floating point precision :
 * http://christian-seiler.de/projekte/fpmath/
 */
#if defined FPU_PRECISION &&                    \
    defined FPU_CONTROL &&                      \
    defined _FPU_EXTENDED

#define Set_FPU_precision fpu_control_t fpu_oldcw,fpu_cw;   \
    _FPU_GETCW(fpu_oldcw);                                  \
    fpu_cw = (fpu_oldcw &                                   \
              ~ _FPU_EXTENDED &                             \
              ~ _FPU_DOUBLE &                               \
              ~ _FPU_SINGLE) |                              \
        _FPU_DOUBLE;                                        \
    _FPU_SETCW(fpu_cw);
pats
#else
#define Set_FPU_precision /* */
#endif // FPU_PRECISION

/*
 * Floating point policy : this is determined
 * by the FPU_CAPTURE_* macros, defined in binary_c_code_options.h
 */

#ifdef FPU_CAPTURE_INVALID
#define FPU_POLICY_INVALID | FE_INVALID
#else
#define FPU_POLICY_INVALID
#endif
#ifdef FPU_CAPTURE_INEXACT
#define FPU_POLICY_INEXACT | FE_INEXACT
#else
#define FPU_POLICY_INEXACT
#endif

#ifdef FPU_CAPTURE_OVERFLOWS
#define FPU_POLICY_OVERFLOWS | FE_OVERFLOW
#else
#define FPU_POLICY_OVERFLOWS
#endif

#ifdef FPU_CAPTURE_UNDERFLOWS
#define FPU_POLICY_UNDERFLOWS | FE_UNDERFLOW
#else
#define FPU_POLICY_UNDERFLOWS
#endif
#ifdef FPU_CAPTURE_DIVIDE_BY_ZERO
#define FPU_POLICY_DIVIDE_BY_ZERO | FE_DIVBYZERO
#else
#define FPU_POLICY_DIVIDE_BY_ZERO
#endif

#define FPU_POLICY                              \
    FPU_POLICY_INVALID                          \
    FPU_POLICY_OVERFLOWS                        \
    FPU_POLICY_UNDERFLOWS                       \
    FPU_POLICY_DIVIDE_BY_ZERO                   \
    FPU_POLICY_INEXACT

#define Set_FPU_policy /**/
//feenableexcept(0 FPU_POLICY)

#define Fpstring(X)                                 \
    (                                               \
        fpclassify(X)==FP_NAN ?       "NaN" :       \
        fpclassify(X)==FP_INFINITE ?  "Inf" :       \
        fpclassify(X)==FP_ZERO ?      "Zero" :      \
        fpclassify(X)==FP_SUBNORMAL ? "Subnormal" : \
        fpclassify(X)==FP_NORMAL ?    "Normal" :    \
        "Unknown"                                   \
        )

/*
 * GSL convenience macros.
 *
 * NB vectors are assumed to be 3D.
 */
#define Set_GSL_vector(V,X,Y,Z)                 \
    {                                           \
        gsl_vector_set((V),0,(X));              \
        gsl_vector_set((V),1,(Y));              \
        gsl_vector_set((V),2,(Z));              \
    }
#define Set_GSL_4vector(V,W,X,Y,Z)              \
    {                                           \
        gsl_vector_set((V),0,(W));              \
        gsl_vector_set((V),1,(X));              \
        gsl_vector_set((V),2,(Y));              \
        gsl_vector_set((V),3,(Z));              \
    }
#define Set_GSL_vector_array(V,X)               \
    {                                           \
        gsl_vector_set((V),0,((X)[0]));         \
        gsl_vector_set((V),1,((X)[1]));         \
        gsl_vector_set((V),2,((X)[2]));         \
    }
#define Set_GSL_4vector_array(V,X)              \
    {                                           \
        gsl_vector_set((V),0,((X)[0]));         \
        gsl_vector_set((V),1,((X)[1]));         \
        gsl_vector_set((V),2,((X)[2]));         \
        gsl_vector_set((V),3,((X)[3]));         \
    }
#define Get_GSL_vector(V,X,Y,Z)                 \
    {                                           \
        (X)=gsl_vector_get((V),0);              \
        (Y)=gsl_vector_get((V),1);              \
        (Z)=gsl_vector_get((V),2);              \
    }
#define Get_GSL_4vector(V,W,X,Y,Z)              \
    {                                           \
        (W)=gsl_vector_get((V),0);              \
        (X)=gsl_vector_get((V),1);              \
        (Y)=gsl_vector_get((V),2);              \
        (Z)=gsl_vector_get((V),3);              \
    }
#define Get_GSL_vector_array(V,X)               \
    {                                           \
        (X)[0] = gsl_vector_get((V),0);         \
        (X)[1] = gsl_vector_get((V),1);         \
        (X)[2] = gsl_vector_get((V),2);         \
    }
#define Get_GSL_4vector_array(V,X)              \
    {                                           \
        (X)[0] = gsl_vector_get((V),0);         \
        (X)[1] = gsl_vector_get((V),1);         \
        (X)[2] = gsl_vector_get((V),2);         \
        (X)[3] = gsl_vector_get((V),3);         \
    }

/*
 * Here, ARGS are a va_list, but PARAMS are
 * a (void * p) which is a pointer to a GSL_args struct.
 *
 * This in turn contains the pointer to the args list.
 *
 * You can access the error value through the integer pointer
 * int * GSL_error : the VA_ARGS should be used to set this
 * in a variable which is returned.
 */
#define Map_GSL_params(PARAMS,VA_ARGS)          \
    va_list (VA_ARGS);                          \
    va_copy(                                    \
        (VA_ARGS),                              \
        ((struct GSL_args_t *)(PARAMS))->args   \
        );

/*
 * Copy a set of GSL_args while also copying the va_list
 * properly with va_copy.
 *
 * DEST and SRC are cast to a GSL_args_t * (so can be void *).
 */
#define Copy_GSL_args(SRC,DEST)                     \
                                                    \
    memcpy(((struct GSL_args_t *)(DEST)),           \
           ((struct GSL_args_t *)(SRC)),            \
           sizeof(struct GSL_args_t));              \
                                                    \
    va_copy(((struct GSL_args_t *)(DEST))->args,    \
            ((struct GSL_args_t *)(SRC))->args));   \



/*
 * Bisector function result
 *
 * Given a CONSTRAINT, we want VARIABLE to equal it,
 * at which point the bisection function should give zero.
 *
 * The function must be monotonically increasing with
 * VARIABLE, and preferably not be infinite.
 *
 * Thus, if CONSTRAINT is zero we just
 * compare VARIABLE to CONSTRAINT directly.
 *
 * If the function is monotonically decreasing,
 * use Bisection_result_inverse, in which we switch
 * CONSTRAINT and VARIABLE, while this time checking
 * that VARIABLE is non-zero.
 */

#define Bisection_result(CONSTRAINT,VARIABLE)   \
    Bisection_result_implementation(            \
        (CONSTRAINT),                           \
        (VARIABLE),                             \
        Bisection_result,                       \
        __COUNTER__)

#define Bisection_result_implementation(                                \
    CONSTRAINT,                                                         \
    VARIABLE,                                                           \
    LABEL,                                                              \
    LINE)                                                               \
    __extension__                                                       \
    ({                                                                  \
        Autotype(VARIABLE) Concat3(_v,LABEL,LINE) =                     \
            (VARIABLE);                                                 \
        Autotype(CONSTRAINT) Concat3(_c,LABEL,LINE) =                   \
            (CONSTRAINT);                                               \
        Is_really_not_zero(Concat3(_c,LABEL,LINE)) ?                    \
            (Concat3(_v,LABEL,LINE) / Concat3(_c,LABEL,LINE) - 1.0) :   \
            (Concat3(_v,LABEL,LINE) - Concat3(_c,LABEL,LINE));          \
    })

#define Bisection_result_inverse(CONSTRAINT,    \
                                 VARIABLE)      \
    (Bisection_result(                          \
        (VARIABLE),                             \
        (CONSTRAINT)                            \
        ))


/*
 * NaN checking
 */

/*
 * Wrapper to perform soft checks of the nanchecks boolean
 */
#ifdef SOFT_NANCHECK
#define _Nanwrap(X) (stardata->preferences->nanchecks==TRUE ? X : FALSE)
#else
#define _Nanwrap(X) (X)
#endif

/*
 * perhaps use custom isnan function?
 */
#ifdef USE_NATIVE_ISNAN
/*
 * Use a native isnan: either gcc's builtin
 * version or the C library isnan() macro
 */
#ifdef __HAVE_ATTRIBUTE_BUILTIN_ISNAN__
#define _Nanchecker(X) _Nanwrap(__builtin_isnan(X))
//#undef isnan
#else
#define _Nanchecker(X) _Nanwrap(isnan(X))
#endif
#else
/*
 * Use custom my_isnan() function
 *
 * also remove C library's isnan
 */
#undef isnan
#define isnan(X) my_isnan((X))
#define _Nanchecker(X) _Nanwrap(my_isnan(X))
#endif // USE_NATIVE_ISNAN


/*
 * Wrapper for nancheck
 */
#ifdef NANCHECKS
#define nancheck(...)                               \
    if(stardata->preferences->nanchecks == TRUE)    \
    {                                               \
        _nancheck(stardata,__VA_ARGS__);            \
    }
#else
#define nancheck(...) /* NANCHECKS not defined: nancheck is disabled */
#endif


#define Bisector_string(N) (                                            \
        (abs(N)) == BISECTOR_DISC_TVISC ? "Tvisc0" :                    \
        (abs(N)) == BISECTOR_DISC_J ? "J" :                             \
        (abs(N)) == BISECTOR_DISC_F ? "F" :                             \
        (abs(N)) == BISECTOR_DISC_RHALFJ ? "RhalfJ" :                   \
        (abs(N)) == BISECTOR_DISC_PRESSURE_RADIUS ? "RfromP" :          \
        (abs(N)) == BISECTOR_COMENV_DM ? "ComenvDM" :                   \
        (abs(N)) == BISECTOR_DISC_OWEN_RADIUS ? "OwenRadius" :          \
        (abs(N)) == BISECTOR_DISC_VISCOUS ? "Viscous" :                 \
        (abs(N)) == BISECTOR_DISC_MASS_RADIUS ? "RfromM" :              \
        (abs(N)) == BISECTOR_DISC_RIN_MIN ? "RinMin" :                  \
        (abs(N)) == BISECTOR_DISC_BISECTION_ROOTER ? "Disc_bi_rooter" : \
        (abs(N)) == BISECTOR_TIDES_XA ? "tides_xa" :                    \
        "Unknown")


#define Bisector_error_string(N) (                                      \
        (N) == BINARY_C_BISECT_ERROR_NONE ? "None" :                    \
        (N) == BINARY_C_BISECT_ERROR_MAXED_OUT ? "Maxed out" :          \
        (N) == BINARY_C_BISECT_ERROR_BRACKET_FAILED ? "Bracket failed" : \
        "Unknown")

/*
 * long int converters
 */
#define Long_int_to_int(N)                      \
    ((int)(                                     \
        (N) > INT_MAX ? INT_MAX :               \
        (N) < INT_MIN ? INT_MIN :               \
        (N)                                     \
        ))
#define Long_int_to_short_int(N)                \
    ((short int)(                               \
        (N) > SHRT_MAX ? SHRT_MAX :             \
        (N) < SHRT_MIN ? SHRT_MIN :             \
        (N)                                     \
        ))
#define Long_int_to_unsigned_int(N)             \
        ((unsigned int)(                        \
            (N) > UINT_MAX ? UINT_MAX :         \
            (N) < 0 ? 0 :                       \
            (N)                                 \
            ))
#define Long_int_to_unsigned_short_int(N)       \
    ((unsigned short int)(                      \
        (N) > USHRT_MAX ? USHRT_MAX :           \
        (N) < 0 ? 0 :                           \
        (N)                                     \
        ))
#define Long_int_to_boolean(N)                  \
        ((Boolean)(                             \
            (N) == 0 ? FALSE : TRUE             \
            ))

#endif // MATHS_FUNCTION_MACROS_H
