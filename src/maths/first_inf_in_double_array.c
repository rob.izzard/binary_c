#include "../binary_c.h"
No_empty_translation_unit_warning;

/*
 * Check array for inf, return the index of the first
 * found inf, or -1 if not found.
 */
ssize_t first_inf_in_double_array(struct double_array_t * const Restrict double_array)
{
    /*
     * Check array, of size n, for infs
     */
    for(ssize_t i=0; i<double_array->n; i++)
    {
        if(isinf(double_array->doubles[i]))
        {
            return i;
        }
    }
    return -1;
}
