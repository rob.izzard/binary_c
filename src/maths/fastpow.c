#include "../binary_c.h"
No_empty_translation_unit_warning;



double Constant_function fastPow(const double a,
                              const double b)
{
/*
 * From https://martin.ankerl.com/2007/10/04/optimized-pow-approximation-for-java-and-c-c/
 *
 * An approximate pow(a,b) function
 */

    if(Fequal(b,0.0))
    {
        return 1.0;
    }
    else if(Fequal(a,0.0))
    {
        return 0.0;
    }
    else
    {
        union {
            double d;
            int x[2];
        } u = { a };

        if(b < 0.0)
        {
            u.x[1] = (int)(-b * (u.x[1] - 1072632447) + 1072632447);
            u.x[0] = 0;
            return - (u.d);
        }
        else
        {
            u.x[1] = (int)(b * (u.x[1] - 1072632447) + 1072632447);
            u.x[0] = 0;
            return u.d;
        }
    }
}

