#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef MINT

/*
 * Function to estimate the binding energy of the
 * whole star (erg).
 */

double MINT_binding_energy(struct stardata_t * const stardata,
                           struct star_t * const star)
{
    return MINT_partial_binding_energy(stardata,
                                       star,
                                       0.0,
                                       star->mass,
                                       NULL,
                                       NULL);
}
#endif//MINT
