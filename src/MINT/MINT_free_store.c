#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef MINT

void MINT_free_store(struct store_t * const store)
{
    /*
     * Free memory allocated for MINT in the
     * binary_c store.
     */
    for(unsigned int i=0; i<MINT_TABLE_NUMBER; i++)
    {
        struct data_table_t * table = store->MINT_tables[i];
        struct mint_table_metadata_t * metadata =
            table != NULL ?
            table->metadata : NULL;
        if(metadata != NULL)
        {
            MINT_free_table_metadata(&metadata);
        }
        Delete_data_table(store->MINT_tables[i]);
        for(Stellar_type s=0; s<NUMBER_OF_STELLAR_TYPES; s++)
        {
            Safe_free(store->MINT_generic_map[s]);
        }
    }
}

#endif//MINT
