#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef MINT

/*
 * Load MINT data
 */
void MINT_load_data(struct stardata_t * stardata)
{
    Dprint("Inited MINT already? %s (stardata=%p store=%p)\n",
           (stardata != NULL && stardata->store != NULL) ? Yesno(stardata->store->MINT_loaded) : "NULL",
           (void*)stardata,
           stardata!=NULL ? (void*)stardata->store : NULL
        );

    if(stardata != NULL &&
       stardata->store->MINT_loaded == FALSE &&
       stardata->preferences != NULL &&
       stardata->preferences->stellar_structure_algorithm == STELLAR_STRUCTURE_ALGORITHM_MINT)
    {
#ifdef __EXPERIMENTAL
        if(stardata->preferences->MINT_load_state_file[0] != '\0')
        {
            /*
             * Load MINT data from state file
             */
            if(Pthread_lock(BINARY_C_MUTEX_MINT_LOAD) == 0)
            {
                MINT_load_state(stardata);
                stardata->store->MINT_loaded = TRUE;
                Pthread_unlock(BINARY_C_MUTEX_MINT_LOAD);
            }
        }
        else
#endif // _EXPERIMENTAL
        {
            /*
             * Initialize MINT data from data directory
             */
            if(stardata->store->MINT_loaded == FALSE)
            {
                if(Pthread_lock(BINARY_C_MUTEX_MINT_LOAD) == 0)
                {
                    if(stardata->store->MINT_loaded == FALSE)
                    {
                        MINT_load_grid(stardata);
                        stardata->store->MINT_loaded = TRUE;
                    }
                    Pthread_unlock(BINARY_C_MUTEX_MINT_LOAD);
                }
            }
        }
    }

#ifdef __EXPERIMENTAL
    if(stardata->preferences->MINT_save_state_file[0] != '\0')
    {
        MINT_save_state(stardata);
    }
#endif // _EXPERIMENTAL
}
#endif // MINT
