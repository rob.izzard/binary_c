#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef MINT
#include "MINT.h"
#include "MINT_load_grid.h"

void MINT_map_COWD_table(struct stardata_t * const stardata)
{
    /*
     * Remap the final column of the COWD table
     */


    /*
     * Find the original COWD table in the rinterpolate_data
     */
    struct rinterpolate_table_t * disordered_COWD_table =
        rinterpolate_table_struct(stardata->persistent_data->rinterpolate_data,
                                  stardata->store->MINT_tables[MINT_TABLE_COWD]->data,
                                  NULL);

    if(disordered_COWD_table == NULL)
    {
        /*
         * Call interpolate with NULL to make sure the
         * table is set up in stardata->persistent_data_t->rinterpolate_data
         */
        Interpolate(stardata->store->MINT_tables[MINT_TABLE_COWD],
                    NULL,
                    NULL,
                    FALSE);
        disordered_COWD_table =
            rinterpolate_table_struct(stardata->persistent_data->rinterpolate_data,
                                      stardata->store->MINT_tables[MINT_TABLE_COWD]->data,
                                      NULL);
    }
    /*
      rinterpolate_show_table(stardata->persistent_data_t->rinterpolate_data,
      disordered_COWD_table,
      stdout,
      -1);
    */
    /*
      rinterpolate_analyse_table(stardata->persistent_data_t->rinterpolate_data,
      disordered_COWD_table,
      "map COWD table");
    */
    /*
     * Map the final table parameter column
     * with 100 points
     */
    struct rinterpolate_table_t * const ordered_COWD_table =
        rinterpolate_map_right_columns(stardata->persistent_data->rinterpolate_data,
                                       disordered_COWD_table,
                                       (rinterpolate_Boolean_t []){FALSE,FALSE,TRUE},
                                       (rinterpolate_float_t []){0,0,100},
                                       NULL);

    /*
     * Set the mapped MINT table in persistent_data_t->MINT_mapped_tables
     */
    stardata->persistent_data->MINT_mapped_tables[MINT_TABLE_COWD] =
        Malloc(sizeof(struct data_table_t));
    stardata->persistent_data->MINT_mapped_tables[MINT_TABLE_COWD]->data =
        ordered_COWD_table->data;
    stardata->persistent_data->MINT_mapped_tables[MINT_TABLE_COWD]->nlines =
        ordered_COWD_table->l;
    stardata->persistent_data->MINT_mapped_tables[MINT_TABLE_COWD]->nparam =
        ordered_COWD_table->n;
    stardata->persistent_data->MINT_mapped_tables[MINT_TABLE_COWD]->ndata =
        ordered_COWD_table->d;

    //show_data_table(stardata->persistent_data->MINT_mapped_tables[MINT_TABLE_COWD]);
}
#endif // MINT
