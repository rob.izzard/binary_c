#include "../binary_c.h"

No_empty_translation_unit_warning;

#if defined MINT && defined NUCSYN

void MINT_nucsyn_MS_post(struct stardata_t * Restrict const stardata Maybe_unused,
                         struct star_t * Restrict const star)
{
#ifdef BSE
    /*
     * Tie in BSE algorithm to MINT
     */
    if(star->mint->shells!=NULL)
    {
        memcpy(star->Xenv,
               star->mint->shells[star->mint->nshells-1].X,
               ISOTOPE_MEMSIZE);
    }
#endif // BSE
}
#endif // MINT && NUCSYN
