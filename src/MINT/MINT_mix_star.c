#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef MINT

/*
 * Function to mix a star from mass coordinate
 * lower to coordinate upper in MINT
 */

void MINT_mix_star(struct stardata_t * const stardata Maybe_unused,
                   struct star_t * const star,
                   const double lower,
                   const double upper)
{
#ifdef NUCSYN
    struct mint_shell_t * mixed_region = MINT_new_clean_shell;
    Shell_index start = -1, end = -1, i;

    for(i=0; i<star->mint->nshells; i++)
    {
        struct mint_shell_t * const shell = &star->mint->shells[i];
        if(In_range(shell->m,lower,upper))
        {
            mixed_region->dm += shell->dm;
            for(Isotope j=0; j<ISOTOPE_ARRAY_SIZE; j++)
            {
                mixed_region->X[j] += shell->dm * shell->X[j];
            }
            if(start == -1)
            {
                start = i;
            }
        }
    }
    end = i - 1;

    for(Isotope k=0; k<ISOTOPE_ARRAY_SIZE; k++)
    {
        mixed_region->X[k] /= mixed_region->dm;
    }
    for(Shell_index j=start; j<=end; j++)
    {
        memcpy(star->mint->shells[j].X,
               mixed_region->X,
               ISOTOPE_MEMSIZE);
    }
    Safe_free(mixed_region);

#endif
}
#endif // MINT
