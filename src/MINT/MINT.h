#pragma once
#ifndef MINT_H
#define MINT_H

/*
 * Main header file for the MINT library
 *
 * This should just suck in other header files...
 */


/*
 * Non-MINT code can require the data tables
 * and data columns to access the MINT_data_available
 * array
 */
#include "MINT_actions.h"
#include "MINT_data_tables.h"
#include "MINT_data_columns.h"

/*
 * Everything else should be included only if
 * MINT is built
 */
#ifdef MINT
#include "MINT_macros.h"
#include "MINT_function_macros.h"
#include "MINT_prototypes.h"
#include "MINT_nucsyn_functions.h"
#endif//MINT



#endif // MINT_H
