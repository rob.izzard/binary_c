#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef MINT

/*
 * MINT function to calculate stellar structure
 * during the Hertzsprung Gap.
 *
 * Note that prevstar can be NULL, in which case
 * we cannot compare to the previous star to do (say)
 * rejuvenation.
 */

Stellar_type MINT_stellar_structure_HG(struct stardata_t * Restrict const stardata Maybe_unused,
                                       struct star_t * const prevstar Maybe_unused,
                                       struct star_t * const newstar,
                                       const Caller_id caller_id Maybe_unused)
{
    Stellar_type st = MINT_stellar_structure_GB(stardata,
                                                prevstar,
                                                newstar,
                                                caller_id);

    newstar->stellar_type = st;

    if(newstar->core_mass[CORE_He] < 0.0)
    {
        Exit_binary_c(BINARY_C_MINT_ERROR,
                      "OOPS MINT HG Mc<0\n");

    }
    return newstar->stellar_type;
}

#endif // MINT
