#pragma once
#ifndef MINT_CHeB_DATA_COLUMNS_H
#define MINT_CHeB_DATA_COLUMNS_H

/*
 * Define the MINT data columns.
 */

#include "MINT_data_columns_CHeB_list.def"
#undef X

/* loading actions */
#define X(NAME) MINT_CHeB_ACTION_##NAME,
enum { MINT_ACTIONS_LIST };
#undef X

/* MS parameter items list */
#define X(NAME,ACTION) MINT_CHeB_##NAME,
enum { MINT_CHeB_PARAMETER_ITEMS_LIST };
#undef X

/* parameter items actions */
#define X(NAME,ACTION) MINT_CHeB_ACTION_##ACTION,
static int MINT_CHeB_parameter_actions[] Maybe_unused = { MINT_CHeB_PARAMETER_ITEMS_LIST };
#undef X

/* data items list */
#define X(NAME,ACTION) MINT_CHeB_##NAME,
enum { MINT_CHeB_DATA_ITEMS_LIST };
#undef X

/* data items actions */
#define X(NAME,ACTION) MINT_CHeB_ACTION_##ACTION,
static int MINT_CHeB_data_actions[] Maybe_unused = { MINT_CHeB_DATA_ITEMS_LIST };
#undef X


#endif // MINT_CHeB_DATA_COLUMMNS_H
