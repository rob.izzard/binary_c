#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef MINT

/*
 * MINT function to calculate stellar structure
 * of a neutron star.
 *
 * Note that prevstar can be NULL, in which case
 * we cannot compare to the previous star to do (say)
 * rejuvenation.
 */

Stellar_type MINT_stellar_structure_NS(struct stardata_t * Restrict const stardata Maybe_unused,
                                       struct star_t * const prevstar Maybe_unused,
                                       struct star_t * const newstar,
                                       const Caller_id caller_id Maybe_unused)
{
    if(MINT_has_shells(newstar))
    {
        /*
         * Make the envelope cool
         */
        Foreach_shell(newstar,shell)
        {
            shell->T = 0.0;
            shell->convective = FALSE;
        }
    }

    return newstar->stellar_type;
}

#endif // MINT
