#include "../binary_c.h"
No_empty_translation_unit_warning;

/*
 * Rejuvenate MS star : return TRUE if there is a change
 * in central hydrogen abundance which requires the stellar
 * structure to be updated.
 */

#ifdef MINT

static double MINT_Xfresh(struct stardata_t * const stardata,
                          struct star_t * const star,
                          const double dm) Maybe_unused;

Boolean MINT_rejuvenate_MS(struct stardata_t * const stardata,
                           struct star_t * prevstar,
                           struct star_t * newstar)
{
    return FALSE;    /*
     * If the star is unevolved, there can be no rejuvenation
     *
     * We return retval.
     */
    Boolean retval;

    if(Fequal(newstar->mint->XHc,
              MINT_initial_XHc(stardata)))
    {
        /*
         * We haven't evolved: cannot rejuvenate.
         */
        retval = FALSE;
    }
    else
    {
        Dprint("Rejuvenate star %d ?\n",newstar->starnum);

        /*
         * Convective core should have grown by dmc_natural in the
         * previous timestep during normal evolution
         */
        const double dmc_natural =
            stardata->model.dt * prevstar->convective_core_mass_time_derivative;

        /*
         * Convective core has actually grown by dmc_actual
         */
        const double dmc_actual =
            newstar->convective_core_mass - prevstar->convective_core_mass;

        /*
         * Non-equilibrium core mass, caused by accretion luminosity
         */
        const double mc_non_eq =
            MINT_MS_non_eq_conv_coremass(stardata,
                                         newstar);

        /*
         * Excess material mixed into the core should
         * dilute its hydrogen content
         */
        const double dmc_excess = Max3(0.0,
                                       Min(newstar->mass,
                                           mc_non_eq - prevstar->convective_core_mass),
                                       dmc_actual - dmc_natural);

        Dprint("at t=%g dt=%g : dMconv/dt natural=%g actual=%g : L_acc=%g (%5.2f%%) -> mc_non_eq = %g : dMconv excess %g -> rejuvenate? %s\n",
               stardata->model.time,
               stardata->model.dt,
               dmc_natural,
               dmc_actual,
               newstar->accretion_luminosity,
               newstar->accretion_luminosity * 100.0 / newstar->luminosity,
               mc_non_eq,
               dmc_excess,
               Yesno(dmc_actual > 0.0));

        if(dmc_actual > 0.0)
        {
            if(dmc_excess > TINY)
            {
                /*
                 * Our core has excess mass compared to normal evolution
                 */

#ifdef NUCSYN
                /*
                 * Mix the shells in the centre of the star that
                 * correspond to this extended (possibly non-equilibrium)
                 * convection zone.
                 */
                if(stardata->preferences->MINT_nuclear_burning == TRUE)
                {
                    const double Mc_conv_was = newstar->convective_core_mass;

                    /*
                     * Set the non-equilibrium core mass and
                     * use this to mix the central convection zone
                     */
                    newstar->convective_core_mass = mc_non_eq;

                    MINT_convection(stardata,newstar);

                    newstar->mint->XHc = newstar->mint->shells[0].X[XH1];
                    newstar->mint->XHec = newstar->mint->shells[0].X[XHe4];

                    /*
                     * Restore the equilibrium convective core mass
                     */
                    newstar->convective_core_mass = Mc_conv_was;
                }
                else
#endif // NUCSYN
                {
                    /*
                     * No shell burning, so use MINT_Xfresh to
                     * calculate the new hydrogen mass fraction
                     */
                    const double Xfresh = MINT_Xfresh(stardata,
                                                      prevstar,
                                                      dmc_excess);
                    const double Xc =
                        prevstar->mint->XHc * prevstar->convective_core_mass
                        +
                        Xfresh * dmc_excess;

                    const double dXc =
                        Xc / (prevstar->convective_core_mass + dmc_excess)
                        - prevstar->mint->XHc;

                    newstar->mint->XHc  += dXc;
                    newstar->mint->XHec -= dXc;

                    Dprint("Xfresh = %g -> Xc = %g -> dXc = %g -> XHC now %g\n",
                           Xfresh,
                           Xc,
                           dXc,
                           newstar->mint->XHc);
                }


                /*
                 * return TRUE because we require the stellar structure
                 * to be recomputed
                 */
                retval = TRUE;
            }
            else
            {
                retval = FALSE;
            }
        }
        else
        {
            retval = FALSE;
        }
    }
    Dprint("Return %s\n",Truefalse(retval));
    return retval;
}


static double MINT_Xfresh(struct stardata_t * const stardata,
                          struct star_t * const star,
                          const double dmcore)
{
    /*
     * We should construct a table containing columns
     * M and X where M>Mconv and X is the convective
     * core hydrogen abundance at the point in the
     * history of the star when its core equalled the mass
     */
    const double X0 = MINT_initial_XHc(stardata);
    double X = 0.0;
    if(More_or_equal(X0, star->mint->XHc))
    {
        X = X0;
    }
    else
    {
        const size_t nl = 1000;
        const double dX = (X0 - star->mint->XHc) / ((double)nl);

        Dprint("Rejuv star %d : X0 = %g, XHc = %g, dX = %g\n",
               star->starnum,
               X0,
               star->mint->XHc,
               dX);

        double * table = Malloc(sizeof(double)*2*(nl+1));
        const double dm = Min(dmcore/10.0,1e-3);

        /*
         * Construct the X(Mconv) lookup table
         * for this total stellar mass
         */
        const double log_star_mass = log10(star->mass);
        double * result = Malloc(MINT_result_size(MINT_TABLE_MS));
        size_t i;
        for(i = 0 , X = star->mint->XHc;
            X < X0;
            X += dX)
        {
            Interpolate(stardata->store->MINT_tables[MINT_TABLE_MS],
                        Parameters(log_star_mass,X),
                        result,
                        FALSE);
            table[i*2+0] = star->mass * result[MINT_MS_CONVECTIVE_CORE_MASS_FRACTION];
            table[i*2+1] = X;
            Dprint("At mass %g =-> X = %g\n",
                   table[i*2+0],
                   table[i*2+1]);
            i++;
        }
        Safe_free(result);
        /*
         * Hence look up the abundances
         */
        struct data_table_t * t = NULL;
        NewDataTable_from_Pointer(table,
                                  t,
                                  1,
                                  1,
                                  nl);


        X = 0.0;
        for(double m = 0.0;
            m < dmcore;
            m += dm)
        {
            double r[1];
            Interpolate(t,
                        Parameters(star->convective_core_mass + m),
                        r,
                        FALSE);
            X += r[0] * dm;
            Dprint("At m = %g + %g = %g interpolated %g * %g -> Xbar = %g\n",
                   m,
                   star->convective_core_mass,
                   m + star->convective_core_mass,
                   r[0],
                   dm,
                   X/dmcore);
        }
        X /= dmcore;

        /*
         * Done with the temporary table
         */
        Delete_data_table(t);

        /*
         * return mass fraction
         */
        Dprint("-> X = %g\n", X);

    }

    if(X<-TINY || X-1.0 > TINY)
    {
        Exit_binary_c(BINARY_C_OUT_OF_RANGE,
                      "Xfresh in MINT_rejuvenate_MS : XHc = %g is not valid",
                      X);
    }

    return X;
}
#endif // MINT
