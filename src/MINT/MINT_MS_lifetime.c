#include "../binary_c.h"

No_empty_translation_unit_warning;

#ifdef MINT

double MINT_MS_lifetime(struct stardata_t * const stardata,
                        struct star_t * const star)
{
    /*
     * Estimate the main sequence lifetime of star
     * based on its metallicity, mass, etc.
     *
     * Unit: Myr
     */
    double params[1] = {
        log10(star->phase_start_mass),
    };
    double result[2];
    /*
     * Interpolate to find stellar structure:
     * params column is: log10(mass)
     * result columns are : log10(age(Myr)), log10(luminosity)
     */
    Interpolate(stardata->store->MINT_tables[MINT_TABLE_TAMS],
                params,
                result,
                FALSE);
    Dprint("MS lifetime(M=%g) = t=%g L=%g\n",
           exp10(params[0]),
           exp10(result[0]),
           exp10(result[1]));

    return exp10(result[0]);
}
#endif // MINT
