#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef MINT

/*
 * Subroutine to take a band (in mass) in the star
 * and resolve it well, while keeping to a default
 * shell thickness outside it.
 *
 * band_centre_mass is the band's central mass co-ordinate,
 *     e.g. the convective core boundary
 *
 * band_thickness_mass is the thickness of the band
 *     that requires the extra resolution
 *
 * thinnest_dm is how thin the shells in the band should be
 *
 * default_dm is the thickness of the shells that do
 *     not have extra resolution.
 *
 * bail_above_core means once we've looped above the band,
 * we return immediately. Usually you want this to be TRUE
 * unless you (for some reason) want to keep scanning the rest
 * of the star.
 */

void MINT_resolve_core_boundary(struct stardata_t * Restrict const stardata Maybe_unused,
                                struct star_t * const star,
                                const double band_centre_mass,
                                const double band_thickness_mass,
                                const double thinnest_dm,
                                const double default_dm,
                                const Boolean bail_above_core)
{
    int k = 0;
    const double half_thickness = 0.5 * band_thickness_mass;
    const double band_low = band_centre_mass - half_thickness;
    const double band_high = band_centre_mass + half_thickness;

    while(k < star->mint->nshells)
    {
        struct mint_shell_t * const shell =
            &star->mint->shells[k];

        if(k>0 &&
           shell->m < band_low &&
           (shell-1)->dm + shell->dm <= default_dm)
        {
            /*
             * Inside the core band, if the shell
             * is thin, merge shell into shell-1
             */
            MINT_merge_shells(stardata,
                              star,
                              shell,
                              shell-1);

            /*
             * Move all the shells at shell (and above)
             * down one
             */
            memmove(shell,
                    shell+1,
                    sizeof(struct mint_shell_t) * (star->mint->nshells - k - 1));
        }
        else if(shell->dm > thinnest_dm &&
                fabs(shell->m - band_centre_mass) < half_thickness)
        {
            /*
             * Split the shell in the band that's not thin enough
             */
            MINT_split_shell(stardata,
                             star,
                             k);
        }
        else
        {
            /*
             * Do nothing: move to next shell
             */
            if(bail_above_core == TRUE &&
               shell->m > band_high)
            {
                /*
                 * Bail out now we're above the band
                 */
                return;
            }
            k++;
        }
    }
}

#endif//MINT
