#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef MINT
#include "MINT.h"

#define vprint(...) printf(__VA_ARGS__);fflush(stdout);

Boolean MINT_make_CHeB_ZA_table(struct stardata_t * const stardata,
                                const int when Maybe_unused)
{
    /*
     * Zero-age CHeB (CHeB_ZA) data
     *
     * Return TRUE on success, FALSE on failure.
     */
    const Boolean vb = FALSE;
    if(MINT_has_table(MINT_TABLE_CHeB))
    {
        double logM;
        const int N = 1000;
        const double min = log10(0.01);
        const double max = log10(1000.0);
        const double dlogM = (max - min) / (N - 1);

        /* one parameter : logM */
        const int nparams = 1;
        /* one data item: luminosity at the CHeB ZA */
        const int ndata = 1;

        double * table_CHeB_ZA = Malloc(sizeof(double) * N * (nparams + ndata));

        if(table_CHeB_ZA == NULL)
        {
            Exit_binary_c(BINARY_C_MALLOC_FAILED,
                          "Failed to malloc for table_liftimes\n");
        }

        double * const table_CHeB_ZA_start = table_CHeB_ZA;
        if(vb)
        {
            vprint("PRE %p : nparams %zu ndata %zu nlines %zu : table_CHeB_ZA is at %p\n",
                   (void*)stardata->store->MINT_tables[MINT_TABLE_CHeB_ZA],
                   stardata->store->MINT_tables[MINT_TABLE_CHeB]->nparam,
                   stardata->store->MINT_tables[MINT_TABLE_CHeB]->ndata,
                   stardata->store->MINT_tables[MINT_TABLE_CHeB]->nlines,
                   (void*)table_CHeB_ZA);
        }

        double * result = Malloc(MINT_result_size(MINT_TABLE_CHeB));

        for(logM = min; logM < max+TINY; logM += dlogM)
        {
            Interpolate(stardata->store->MINT_tables[MINT_TABLE_CHeB],
                        ((double[]){logM,1.0}), /* log10(mass), Yc=1.0 (or nearest) */
                        result,
                        FALSE);

            /*
             * columns are:
             * 1 log10(M/Msun)
             * 2 log10(CHeB ZA luminosity)
             */
            *(table_CHeB_ZA++) = logM;
            *(table_CHeB_ZA++) = log10(result[MINT_CHeB_LUMINOSITY]);
        }

        NewDataTable_from_Pointer(table_CHeB_ZA_start,
                                  stardata->store->MINT_tables[MINT_TABLE_CHeB_ZA],
                                  nparams,
                                  ndata,
                                  N);
        Safe_free(result);

        return TRUE;
    }
    else
    {
        return FALSE;
    }

}

#endif // MINT
