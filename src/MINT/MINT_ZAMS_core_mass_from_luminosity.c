#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef MINT

double MINT_ZAMS_core_mass_from_luminosity(struct stardata_t * const stardata,
                                           struct star_t * star Maybe_unused,
                                           const double L)
{
    /*
     * Find a star on the grid with the same
     * central hydrogen abundance and luminosity
     * as passed in, and return its core mass.
     *
     * The MS table has two parameters:
     *
     * M,X
     *
     * and ndata lookup data.
     *
     * Our core has luminosity given by L, which we assume
     * is the same as the total stellar luminosity or some
     * accretion luminosity.
     *
     * We don't know which phase of evolution this corresponds
     * to, because we don't know how much of the core is mixed,
     * so we assume we're on the ZAMS.
     */

    const int N = 1000;
    const double min = log10(0.01);
    const double max = log10(1000.0);
    const double dlogM = (max - min) / (N - 1);
    double * table_data = Malloc(N * 2 * sizeof(double));

    if(table_data == NULL)
    {
        Exit_binary_c(BINARY_C_MALLOC_FAILED,
                      "Failed to malloc size %zu for data table\n",
                      (size_t)(N*2*sizeof(double)));
    }

    double * p = table_data;
    double * result = Malloc(MINT_result_size(MINT_TABLE_MS));

    for(double logM = min;
        logM < max+TINY;
        logM += dlogM)
    {
        Interpolate(stardata->store->MINT_tables[MINT_TABLE_MS],
                    ((double[]){
                        logM,
                        star->mint->XHc
                    }), /* log10(mass), Xc=Xhc */
                    result,
                    FALSE);

        /*
         * columns are: log10(L/Msun), M_conv
         */
        *(p++) = result[MINT_MS_LUMINOSITY];
        *(p++) = result[MINT_MS_CONVECTIVE_CORE_MASS_FRACTION] * exp10(logM);

        /*
          printf("M= %g -> logL = %g : Mconv %g\n",
                 exp10(logM),
                 result[MINT_MS_LUMINOSITY],
                 result[MINT_MS_CONVECTIVE_CORE_MASS] * exp10(logM));
        */
    }

    /*
     * Make data table of L at XHc
     */
    struct data_table_t * table = NULL;
    NewDataTable_from_Pointer(table_data,
                              table,
                              1,
                              1,
                              N);

    /*
     * And interpolate it
     */
    double r[1];
    Interpolate(table,
                Parameters(log10(L)), /* log10(luminosity) */
                r,
                FALSE);
/*
    printf("Inteprolate at XHc = %g, logL = %g -> Mc = %g\n",
           star->mint->XHc,
           log10(L),
           r[0]);
*/
    Delete_data_table(table);
    Safe_free(result);

    return r[0];
}

#endif//MINT
