#pragma once
#ifndef MINT_ACTIONS_H
#define MINT_ACTIONS_H

/*
 * Actions to be taken upon loading of
 * a data column. These are prefixed by
 * MINT_MS_ACTION_
 *
 * NOTHING just loads the data in the table.
 * LOG10 puts log10(data) in the table.
 * LOG10ABS puts log10(fabs(data)) in the table.
 * LOG10NEGATIVE puts log10(-(data)) in the table.
 * ABS puts fabs(data) in the table.
 *
 * See also MINT_data_filter() which implements
 * these.
 */
#define MINT_ACTIONS_LIST \
    X(       NOTHING)     \
    X(         LOG10)     \
    X(      LOG10ABS)     \
    X( LOG10NEGATIVE)     \
    X(           ABS)

#undef X
#define X(ACTION) MINT_ACTION_##ACTION,
enum { MINT_ACTIONS_LIST };
#undef X
#define X(ACTION) #ACTION,
static const char * const MINT_action_strings[] Maybe_unused = { MINT_ACTIONS_LIST };
#undef X

#endif // MINT_ACTIONS_H
