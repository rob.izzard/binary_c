#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef MINT

int MINT_call_BSE_stellar_structure(struct stardata_t * const stardata,
                                    struct star_t * const oldstar,
                                    struct star_t * const newstar,
                                    const Caller_id caller_id Maybe_unused)
{
    /*
     * Call BSE stellar structure routine from MINT
     */
    Boolean vb = FALSE;

    if(vb)
        printf("MINT_call_BSE_stellar_structure: model %d time %30.20e : star %d (%d %d) -> call BSE\n",
               stardata->model.model_number,
               stardata->model.time,
               newstar ? newstar->starnum : oldstar ? oldstar->starnum : -1,
               oldstar ? oldstar->stellar_type : -1,
               newstar ? newstar->stellar_type : -1
            );
    int ret = newstar->stellar_type;
    if(oldstar != NULL)
    {
        if(oldstar->stellar_type <= MAIN_SEQUENCE)
        {
            /*
             * Link MS MINT -> MS BSE
             */
            newstar->stellar_type = MAIN_SEQUENCE;
#ifdef NUCSYN
            Copy_abundances(oldstar->Xenv,newstar->Xenv);
            Copy_abundances(oldstar->Xacc,newstar->Xacc);
#endif // NUCSYN
            newstar->derivative[DERIVATIVE_STELLAR_He_CORE_MASS] = 0.0;
            newstar->derivative[DERIVATIVE_STELLAR_GB_CORE_MASS] = 0.0;
            newstar->derivative[DERIVATIVE_STELLAR_CO_CORE_MASS] = 0.0;
            newstar->derivative[DERIVATIVE_STELLAR_CENTRAL_HYDROGEN] = 0.0;
            newstar->derivative[DERIVATIVE_STELLAR_CENTRAL_HELIUM] = 0.0;
            newstar->derivative[DERIVATIVE_STELLAR_CENTRAL_CARBON] = 0.0;
        }

        /*
         * Allocate array space that MINT doesn't need
         * but BSE does
         */
        Boolean alloc[3];
        stellar_structure_BSE_alloc_arrays(newstar,
                                           newstar->bse,
                                           alloc);

        if(vb)
            printf("MINT call BSE stellar structure\n");

        stellar_timescales(stardata,
                           newstar,
                           newstar->bse,
                           newstar->phase_start_mass,
                           newstar->mass,
                           newstar->stellar_type);
        ret = stellar_structure_BSE_with_newstar(stardata,
                                                 STELLAR_STRUCTURE_CALLER_MINT,
                                                 oldstar,
                                                 newstar,
                                                 newstar->bse);
        stellar_structure_BSE_free_arrays(newstar, alloc);
    }
    return ret;
}
#endif //MINT
