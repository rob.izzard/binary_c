#include "../binary_c.h"
No_empty_translation_unit_warning;



#ifdef MINT
#include "MINT_data_columns.h"
void MINT_make_generic_map(struct stardata_t * const stardata)
{
    /*
     * We want to map each MINT variable type in the generic list to
     * the variable type in the stellar type's list.
     *
     * When first called, the store should all be memset(0)
     * so all the MINT_generic_map pointers are NULL.
     */

    /*
     * Make list of generic items as strings
     */
#undef X
#define X(NAME) #NAME,
    static const char * generic_strings[] = { MINT_DATA_TYPES_LIST };
    static const size_t n_generic_strings = Array_size(generic_strings);
#undef X


    /*
     * If the map is already set, just return.
     */
    if(stardata->store->MINT_generic_map[0]) return;

    /*
     * Main sequence
     */
    {
        for(Stellar_type s=0; s<=MAIN_SEQUENCE; s++)
        {
            stardata->store->MINT_generic_map[s] =
                Malloc(n_generic_strings * sizeof(int));

            /*
             * Set the map to -1 (no map) by default
             */
            for(size_t i=0; i<n_generic_strings; i++)
            {
                stardata->store->MINT_generic_map[s][i] = -1;
            }

            /*
             * Check each generic type with each MS type:
             * if they match, the type is in both lists, so
             * set the map
             */
#define X(NAME,ACTION)                                                  \
            for(size_t i=0; i<n_generic_strings; i++)                   \
            {                                                           \
                if(Strings_equal(#NAME,                                 \
                                 generic_strings[i]))                   \
                {                                                       \
                    stardata->store->MINT_generic_map[s][MINT_##NAME] = \
                        MINT_MS_##NAME;                                 \
                    Dprint(                                             \
                        "map generic stellar_type %d : %s = %d to MINT_MS = %d\n", \
                           s,                                           \
                           #NAME,                                       \
                           MINT_##NAME,                                 \
                           MINT_MS_##NAME);                             \
                }                                                       \
            }

            MINT_MS_DATA_ITEMS_LIST;
        }
#undef X
    }


    /*
     * Core-helium burning
     */
    {
        const Stellar_type s = CHeB;
        {
            stardata->store->MINT_generic_map[s] =
                Malloc(n_generic_strings * sizeof(int));

            /*
             * Set the map to -1 (no map) by default
             */
            for(size_t i=0; i<n_generic_strings; i++)
            {
                stardata->store->MINT_generic_map[s][i] = -1;
            }

            /*
             * Check each generic type with each CHeB type:
             * if they match, the type is in both lists, so
             * set the map
             */
#define X(NAME,ACTION)                                                  \
            for(size_t i=0; i<n_generic_strings; i++)                   \
            {                                                           \
                if(Strings_equal(#NAME,                                 \
                                 generic_strings[i]))                   \
                {                                                       \
                    stardata->store->MINT_generic_map[s][MINT_##NAME] = \
                        MINT_CHeB_##NAME;                               \
                    Dprint(                                             \
                        "map generic stellar_type %d : %s = %d to MINT_MS = %d\n", \
                        s,                                              \
                        #NAME,                                          \
                        MINT_##NAME,                                    \
                        MINT_CHeB_##NAME);                              \
                }                                                       \
            }

            MINT_CHeB_DATA_ITEMS_LIST;
        }
#undef X
    }
}
#endif//MINT
