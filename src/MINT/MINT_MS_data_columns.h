#pragma once
#ifndef MINT_MS_DATA_COLUMNS_H
#define MINT_MS_DATA_COLUMNS_H

/*
 * Define the MINT data columns.
 */

#include "MINT_data_columns_MS_list.def"
#undef X

/* loading actions */
#define X(NAME) MINT_MS_ACTION_##NAME,
enum { MINT_ACTIONS_LIST };
#undef X

/* MS parameter items list */
#define X(NAME,ACTION) MINT_MS_##NAME,
enum { MINT_MS_PARAMETER_ITEMS_LIST };
#undef X

/* parameter items actions */
#define X(NAME,ACTION) MINT_MS_ACTION_##ACTION,
static int MINT_MS_parameter_actions[] Maybe_unused = { MINT_MS_PARAMETER_ITEMS_LIST };
#undef X

/* data items list */
#define X(NAME,ACTION) MINT_MS_##NAME,
enum { MINT_MS_DATA_ITEMS_LIST };
#undef X

/* data items actions */
#define X(NAME,ACTION) MINT_MS_ACTION_##ACTION,
static int MINT_MS_data_actions[] Maybe_unused = { MINT_MS_DATA_ITEMS_LIST };
#undef X


#endif // MINT_MS_DATA_COLUMMNS_H
