#pragma once
#ifndef MINT_LOAD_GRID_H
#define MINT_LOAD_GRID_H

/*
 * Macros to extract column numbers from the
 * header cdict
 */

/*
 * Offset : the data files start at
 * column 1, we want to return starting at 0
 */
#undef OFFSET
#define OFFSET -1

#define MINT_Column(STRING)                         \
    MINT_Column_implementation(MINT_Column_single,  \
                               __COUNTER__,         \
                               #STRING)

#define MINT_Column_implementation(LABEL,                               \
                                   LINE,                                \
                                   STRING)                              \
    __extension__                                                       \
    ({                                                                  \
        int Concat3(LABEL,LINE,range)[2] =                              \
            MINT_Column_range_with_string(STRING);                      \
                                                                        \
        if(Concat3(LABEL,LINE,range)[0] !=                              \
           Concat3(LABEL,LINE,range)[1])                                \
        {                                                               \
            Exit_binary_c(BINARY_C_OUT_OF_RANGE,                        \
                          "MINT Column range of %s is from %d to %d but these should be identical\n", \
                          STRING,                                       \
                          Concat3(LABEL,LINE,range)[0],                 \
                          Concat3(LABEL,LINE,range)[1]);                \
        }                                                               \
        if(Concat3(LABEL,LINE,range)[0] == -1 ||                        \
           Concat3(LABEL,LINE,range)[1] == -1)                          \
        {                                                               \
            Exit_binary_c(BINARY_C_OUT_OF_RANGE,                        \
                          "MINT Column range of %s is from %d to %d but these should be >=0\n", \
                          STRING,                                       \
                          Concat3(LABEL,LINE,range)[0],                 \
                          Concat3(LABEL,LINE,range)[1]);                \
        }                                                               \
                                                                        \
        (int)Concat3(LABEL,LINE,range)[0];                              \
    })

/*
 * MINT_Column_range should be called with the macro
 * directly
 */
#define MINT_Column_range(STRING)               \
    {                                           \
        MINT_Column_range_implementation(       \
            low,                                \
            Column_range,                       \
            __COUNTER__,                        \
            #STRING                             \
            ),                                  \
        MINT_Column_range_implementation(       \
            high,                               \
            Column_range,                       \
            __COUNTER__,                        \
            #STRING                             \
            )                                   \
    }
#define MINT_Column_range_with_string(STRING)   \
    {                                           \
        MINT_Column_range_implementation(       \
            low,                                \
            Column_range,                       \
            __COUNTER__,                        \
            STRING                              \
            ),                                  \
        MINT_Column_range_implementation(       \
            high,                               \
            Column_range,                       \
            __COUNTER__,                        \
            STRING                              \
            )                                   \
    }


#define MINT_Column_range_implementation(POS,                   \
                                         LABEL,                 \
                                         LINE,                  \
                                         STRING)                \
    __extension__                                               \
    ({                                                          \
        struct cdict_entry_t * Concat3(e,LABEL,LINE) =          \
            CDict_nest_get_entry(                               \
                metadata->header->cdict,                        \
                "columns",                                      \
                (char*)(STRING),                                \
                (char*)Stringify(POS)                           \
                );                                              \
        Concat3(e,LABEL,LINE) != NULL                           \
            ? (Concat3(e,LABEL,LINE)->value.value.int_data +    \
               OFFSET)                                          \
            : -1;                                               \
    })


#define MINT_Load_Table(                        \
    STARDATA,                                   \
    TABLE_ID,                                   \
    DATA_TABLE,                                 \
    FILENAME,                                   \
    HEADER_STRING,                              \
    PARAMETER_NAMES,                            \
    DATA_NAMES,                                 \
    PARAMETER_ACTIONS,                          \
    DATA_ACTIONS,                               \
    WHEN,                                       \
    WHEN_DATA,                                  \
    VB)                                         \
                                                \
    MINT_table_loader(                          \
        (STARDATA),                             \
        (TABLE_ID),                             \
        (DATA_TABLE),                           \
        (FILENAME),                             \
        (HEADER_STRING),                        \
        (PARAMETER_NAMES),                      \
        Array_size(PARAMETER_NAMES),            \
        (DATA_NAMES),                           \
        Array_size(DATA_NAMES),                 \
        (PARAMETER_ACTIONS),                    \
        (DATA_ACTIONS),                         \
        (WHEN),                                 \
        (WHEN_DATA),                            \
        (VB)                                    \
        )


#define _push_string(STACK,STRING)                      \
    (STACK##_n)++;                                      \
    (STACK) = Realloc((STACK),                          \
                      sizeof(char*)*(STACK##_n));       \
    (STACK)[(STACK##_n)-1] = strdup((char*)(STRING));

#define _free_string_stack                                          \
    for(size_t k=0; k<metadata->missing_MINT_types_strings_n; k++)  \
    {                                                               \
        Safe_free(metadata->missing_MINT_types_strings[k]);         \
    }                                                               \
    for(size_t k=0; k<metadata->missing_from_file_strings_n; k++)   \
    {                                                               \
        Safe_free(metadata->missing_from_file_strings[k]);          \
    }                                                               \
    Safe_free(metadata->missing_from_file_strings);                 \
    Safe_free(metadata->missing_MINT_types_strings);                \
    metadata->missing_MINT_types_strings_n = 0;                     \
    metadata->missing_from_file_strings_n = 0;

#define _show_string_stack                                              \
    for(size_t k=0; k<metadata->missing_MINT_types_strings_n; k++)      \
    {                                                                   \
        Printf("WARNING : %s Data type %s is in the file but is not a MINT type\n", \
               metadata->header_string,                                 \
               metadata->missing_MINT_types_strings[k]);                \
    }                                                                   \
    for(size_t k=0; k<metadata->missing_from_file_strings_n; k++)       \
    {                                                                   \
        Printf("WARNING : %s Data type %s is a MINT type but is not in the file\n", \
               metadata->header_string,                                 \
               metadata->missing_from_file_strings[k]);                 \
    }

#endif // MINT_LOAD_GRID_H
