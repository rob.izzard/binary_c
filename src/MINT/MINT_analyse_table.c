#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef MINT

/*
 * Analyse a table pointed to by the file stream
 * in a metadata.
 */
#define vbprintf(...)                           \
    if(vb ||                                    \
       stardata->preferences->MINT_filename_vb) \
    {                                           \
        printf(__VA_ARGS__);                    \
    }

#define METADATA_LIST                                \
    X( data_line_offsets,  "data line offset bytes") \
    X( data_start_offset, "data start offset bytes") \
    X(       ndata_lines,    "number of data lines") \
    X(    parameter_skip,          "parameter skip") \
    X( parameter_nvalues,       "parameter nvalues") \
    X(  parameter_values,        "parameter values")

struct data_table_analysis_t * MINT_analyse_table(struct stardata_t * const stardata,
                                                  struct mint_table_metadata_t * const metadata,
                                                  const size_t n_parameters,
                                                  Boolean vb)
{
    /*
     * Set up analysis memory, this is returned.
     */
    struct data_table_analysis_t * const analysis = Calloc(1,sizeof(struct data_table_analysis_t));
    vb = TRUE;

    /*
     * Structure declarations
     */
#undef X
#define X(VAR,STR) struct cdict_entry_t * VAR;
    struct metadata_cdicts_t {
        METADATA_LIST
    };
#undef X
#define X(VAR,STR) Boolean VAR;
    struct metadata_state_t {
        METADATA_LIST
    };

    /*
     * Pointers to file's metadata cdicts
     */
    struct metadata_cdicts_t metadata_cdicts = {
#undef X
#define X(VAR,STR)                              \
        .VAR = CDict_nest_get_entry(            \
            metadata->header->cdict,            \
            "metadata",                         \
            STR                                 \
            ),
        METADATA_LIST
    };

    /*
     * Booleans to make sure we have the metadata
     */
    struct metadata_state_t metadata_state = {
#undef X
#define X(VAR,STR) .VAR = FALSE,
        METADATA_LIST
    };
#undef X

    if(analysis == NULL)
    {
        Exit_binary_c(BINARY_C_MALLOC_FAILED,
                      "Could not allocate memory for MINT table analysis.");
    }

    /*
     * Restart the stream from the beginning
     */
    restart_stream(stardata,
                   metadata->stream);
    vbprintf("fseeked stream %p ->fp = %p to 0\n",
             (void*)metadata->stream,
             (void*)metadata->stream->fp);

    double ** values = Calloc(n_parameters,sizeof(double*));
    struct string_array_t * string_array = new_string_array(0);
    double * current = NULL;
    size_t * data_line_offset_bytes = Calloc(metadata->nl_full_table+1,sizeof(size_t));
    size_t * nvalues = Calloc(n_parameters,sizeof(size_t));
    size_t * const skip = Calloc(n_parameters,sizeof(size_t));
    char * line = NULL;
    size_t byte_offset = 0;
    size_t data_start_offset_bytes = 0;
    size_t data_line_number = 0;
    size_t prealloc = 0;
    Boolean defervb = FALSE;

    if(values == NULL ||
       string_array == NULL ||
       data_line_offset_bytes == NULL ||
       nvalues == NULL ||
       skip == NULL)
    {
        Exit_binary_c(BINARY_C_MALLOC_FAILED,
                      "Failed to allocate memory in for data in a MINT analysis. values = %p, string_array = %p, data_line_offset_bytes = %p, nvalues = %p, skip = %p\n",
                      (void*)values,
                      (void*)string_array,
                      (void*)data_line_offset_bytes,
                      (void*)nvalues,
                      (void*)skip);
    }

    /*
     * Check whether metadata are available in the metadata cdicts
     */
#undef X
#define X(VAR,STR) if(metadata_cdicts.VAR != NULL)  \
    {                                               \
        metadata_state.VAR = TRUE;                  \
    }
    METADATA_LIST;

    /*
     * Check we have values for each parameter
     */
    if(metadata_state.parameter_nvalues == TRUE &&
       metadata_state.parameter_values == TRUE)
    {
        metadata_state.parameter_values = TRUE;
        struct cdict_t * const c = metadata_cdicts.parameter_values->value.value.cdict_pointer_data;
        for(size_t i=0; i<n_parameters; i++)
        {
            if(CDict_nest_get_entry(c,(int)i) == NULL)
            {
                metadata_state.parameter_values = FALSE;
                break;
            }
        }
    }

    /*
     * Get data from the metadata cdicts and put them
     * in local variables.
     */
    if(metadata_state.ndata_lines == TRUE)
    {
        data_line_number = (size_t) metadata_cdicts.ndata_lines->value.value.long_int_data;
    }

    if(metadata_state.data_start_offset == TRUE)
    {
        data_start_offset_bytes = (size_t) metadata_cdicts.data_start_offset->value.value.long_int_data;
    }

    if(metadata_state.data_line_offsets == TRUE)
    {
        long int * const long_int_array =  metadata_cdicts.data_line_offsets->value.value.long_int_array_data;
        for(size_t i=0; i<metadata->nl_full_table; i++)
        {
            data_line_offset_bytes[i] = (size_t) long_int_array[i];
        }
    }

    if(metadata_state.parameter_nvalues == TRUE)
    {
        long int * const long_int_array = metadata_cdicts.parameter_nvalues->value.value.long_int_array_data;
        for(size_t i=0; i<n_parameters; i++)
        {
            nvalues[i] = (size_t) long_int_array[i];
        }
    }

    if(metadata_state.parameter_nvalues == TRUE &&
       metadata_state.parameter_values == TRUE)
    {
        struct cdict_t * const c = metadata_cdicts.parameter_values->value.value.cdict_pointer_data;
        for(size_t i=0; i<n_parameters; i++)
        {
            /* apologies for the lack of apostrophe! */
            struct cdict_entry_t * this_values_entry = CDict_nest_get_entry(c,(int)i);
            double * const double_array = this_values_entry->value.value.double_array_data;
            values[i] = Calloc(1 + nvalues[i], sizeof(double));
            memcpy(values[i],double_array,sizeof(double)*nvalues[i]);
        }
    }

    if(metadata_state.parameter_skip == TRUE)
    {
        long int * const long_int_array =  metadata_cdicts.parameter_skip->value.value.long_int_array_data;
        for(size_t i=0; i<n_parameters; i++)
        {
            skip[i] = (size_t) long_int_array[i];
        }
    }

#undef X
#define X(VAR,STR)                                      \
    vbprintf("Analyse %s? %s\n",                        \
             #VAR,                                      \
             Yesno(metadata_state.VAR));


    /*
     * Now, as a fallback if the metadata are not provided,
     * analyse the data in the file.
     */

    const Boolean have_set3 =
        metadata_state.parameter_nvalues == FALSE ||
        metadata_state.parameter_values == FALSE ||
        metadata_state.parameter_skip == FALSE;

    if(metadata_state.data_line_offsets == FALSE ||
       have_set3 == FALSE)
    {
        const size_t maxsplit = n_parameters;
        data_line_number = 0;
        while(data_line_number < metadata->nl_full_table)
        {
            /*
             * Load line
             */
            line = file_splitline(stardata,
                                  metadata->stream->fp,
                                  &line,
                                  string_array,
                                  0,
                                  ' ',
                                  prealloc,
                                  maxsplit);

            if(defervb)
            {
                printf("Line %zu (%zd) : offset %zu\n",
                       data_line_number,
                       string_array->n,
                       byte_offset);
            }

            if(string_array->n > 0 &&
               string_array->strings[0][0] != MINT_HEADER_CHAR)
            {
                /*
                 * Is a data line
                 */
                if(metadata_state.data_line_offsets == FALSE)
                {
                    if(data_start_offset_bytes == 0)
                    {
                        /*
                         * First data line, save byte offset of
                         * data start
                         */
                        data_start_offset_bytes = byte_offset;
                    }
                    data_line_offset_bytes[data_line_number] = byte_offset - data_start_offset_bytes;
                }

                if(have_set3 == FALSE)
                {
                    /*
                     * Convert string array to double array
                     */
                    struct double_array_t * double_array =
                        string_array_to_double_array(string_array,NAN);

                    if(current == NULL)
                    {
                        /* first time setup */
                        current = Calloc(n_parameters,sizeof(double));
                        if(current == NULL)
                        {
                            Exit_binary_c(BINARY_C_MALLOC_FAILED,
                                          "Failed to allocate memory for current in MINT table analysis.");
                        }

                        memcpy(current,double_array->doubles,sizeof(double)*n_parameters);

                        if(metadata_state.parameter_nvalues == FALSE ||
                           metadata_state.parameter_values == FALSE)
                        {
                            for(size_t i=0; i<n_parameters; i++)
                            {
                                nvalues[i] = 0;
                                Safe_free(values[i]);
                                values[i] = Calloc(1,sizeof(double));
                                if(values[i] == NULL)
                                {
                                    Exit_binary_c(BINARY_C_MALLOC_FAILED,
                                                  "Failed to allocate memory for values[%zu] in MINT table analysis.",
                                                  i);
                                }
                                values[i][0] = current[i];
                            }
                        }
                    }
                    else
                    {
                        for(size_t i=0; i<n_parameters; i++)
                        {
                            if(defervb)
                                printf("line %zu param %zu = %g current %g skip %lu\n",
                                       data_line_number,
                                       i,
                                       double_array->doubles[i],
                                       current[i],
                                       skip[i]);

                            const double x = double_array->doubles[i];
                            if(!Fequal(x,current[i]))
                            {
                                if(metadata_state.parameter_skip == FALSE)
                                {
                                    /*
                                     * Value changed
                                     */
                                    if(skip[i] == 0)
                                    {
                                        skip[i] = data_line_number;
                                    }
                                }

                                /*
                                 * If it increases, store it
                                 */
                                if(x > current[i] &&
                                   !Fequal(x,current[i]))
                                {
                                    /*
                                     * Expand the array values[i] : we could
                                     * do this more efficiently, saving on a
                                     * few reallocs, but this routine is never
                                     * MINT's bottleneck.
                                     */
                                    if(metadata_state.parameter_values == FALSE)
                                    {
                                        double * const _new = Realloc(values[i],
                                                                      sizeof(double) * (nvalues[i]+1));

                                        if(_new == NULL)
                                        {
                                            Exit_binary_c(BINARY_C_MALLOC_FAILED,
                                                          "Failed to allocate memory for values[%zu] in MINT table analysis.\n",
                                                          i);
                                        }

                                        values[i] = _new;
                                        values[i][nvalues[i]] = x;
                                        nvalues[i]++;
                                    }
                                    current[i] = x;
                                }
                            }
                        }
                    }

                    free_double_array(&double_array);
                }
                data_line_number++;
            }
            byte_offset += (strlen(line)+1) * sizeof(char);
            Safe_free(line);
        }
    }

    /*
     * Skip[i] that are not set should equal the data_line_number
     */
    if(metadata_state.parameter_skip == FALSE)
    {
        for(size_t i=0; i<n_parameters; i++)
        {
            if(skip[i]==0)
            {
                skip[i] = data_line_number;
            }
        }
    }

    if(defervb)
    {
        for(size_t i=0; i<n_parameters; i++)
        {
            printf("Skip %zu = %zu, nvalues = %zu\n ... ",
                   i,
                   skip[i],
                   nvalues[i]);

            for(size_t j=0; j<Min(nvalues[i],(size_t)10); j++)
            {
                printf("%g ",values[i][j]);
            }
            printf("\n");
        }
    }

    /*
     * Save data in the analysis struct
     */
    if(analysis != NULL)
    {
        analysis->values = values;
        analysis->nvalues = nvalues;
        analysis->nparameters = n_parameters;
        analysis->data_line_offset_bytes = data_line_offset_bytes;
        analysis->data_start_offset_bytes = data_start_offset_bytes;
        analysis->skip = skip;
    }

    /*
     * Free memory
     */
    free_string_array(&string_array);
    Safe_free(current);

    /*
     * Show metadata if asked to
     */
    if(stardata->preferences->MINT_show_metadata == TRUE)
    {
        printf("# metadata for file: %s\n",
               metadata->stream->name);
        printf("# METADATA_NUMBER_OF_LINES  %zu\n",
               metadata->nl_full_table);
        printf("# METADATA_START_DATA_OFFSET_BYTES %zu\n",
               data_start_offset_bytes);
        printf("# METADATA_LINE_OFFSET_DIFF_BYTES 0 %zu ",
               data_line_offset_bytes[1]-data_line_offset_bytes[0]);
        const size_t last_i = metadata->nl_full_table - 1;
        for(size_t i=2; i<metadata->nl_full_table; i++)
        {
            const size_t line_len = data_line_offset_bytes[i] - data_line_offset_bytes[i-1];
            const size_t prev_line_len = data_line_offset_bytes[i-1] - data_line_offset_bytes[i-2];
            const ssize_t diff = line_len - prev_line_len;
            printf("%zd%c",
                   diff,
                   i == last_i ? '\n' : ' ');
        }
        for(size_t i=0; i<n_parameters; i++)
        {
            printf("# METADATA_PARAMETER_VALUES %zu",i);
            for(size_t j=0; j<nvalues[i]; j++)
            {
                printf(" %g",values[i][j]);
            }
            printf("\n");
        }
        printf("# METADATA_PARAMETER_NVALUES");
        for(size_t i=0; i<n_parameters; i++)
        {
            printf(" %zu",nvalues[i]);
        }
        printf("\n");
        printf("# METADATA_PARAMETER_SKIP");
        for(size_t i=0; i<n_parameters; i++)
        {
            printf(" %zu",skip[i]);
        }
        printf("\n");
    }

    return analysis;
}

#endif // MINT
