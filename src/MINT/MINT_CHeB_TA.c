#include "../binary_c.h"

No_empty_translation_unit_warning;

#ifdef MINT

void MINT_CHeB_TA(struct stardata_t * const stardata,
                  const double mass,
                  double * const CHeB_lifetime,
                  double * const CHeB_TA_luminosity)
{
    /*
     * Estimate CHeB terminal-age data:
     * 1) the CHeB lifetime (Myr)
     * 2) the CHeB TA luminosity (Lsun)
     *
     * Unit: Myr
     *
     * If *luminosity is non-NULL, set it also
     *
     * Note: this requires the MINT_TABLE_CHeB_TA
     * table to be available. You should check this
     * before calling!
     */
    const double params[1] = {
        log10(mass),
    };
    double result[2];

    /*
     * Interpolate
     * params column is: log10(mass)
     * result columns are : log10(age(Myr)), log10(luminosity)
     */
    Interpolate(stardata->store->MINT_tables[MINT_TABLE_CHeB_TA],
                params,
                result,
                FALSE);

    if(CHeB_lifetime != NULL)
    {
        *CHeB_lifetime = exp10(result[0]);
    }
    if(CHeB_TA_luminosity != NULL)
    {
        *CHeB_TA_luminosity = exp10(result[1]);
    }

    return;
}
#endif // MINT
