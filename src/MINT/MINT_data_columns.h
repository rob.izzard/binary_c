#pragma once
#ifndef MINT_DATA_COLUMNS_H
#define MINT_DATA_COLUMNS_H

/*
 * Define the MINT data columns.
 */


/*
 * Load the generic (all stellar types)
 * data type list
 */
#include "MINT_generic_data_types.h"

/*
 * There should be a file for each stellar type.
 */
#include "MINT_MS_data_columns.h"
#include "MINT_GB_data_columns.h"
#include "MINT_CHeB_data_columns.h"
#include "MINT_COWD_data_columns.h"

/* ... todo: more stellar types */


/*
 * Other tables
 */
#include "MINT_ZAMS_composition_data_columns.h"


#endif // MINT_DATA_COLUMMNS_H
