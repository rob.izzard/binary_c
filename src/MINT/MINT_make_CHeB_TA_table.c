#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef MINT
#include "MINT.h"

#define vprint(...) printf(__VA_ARGS__);fflush(stdout);

Boolean MINT_make_CHeB_TA_table(struct stardata_t * const stardata,
                                const int when Maybe_unused)
{
    /*
     * Terminal-age CHeB (CHeB_TA) data
     *
     * Return TRUE on success, FALSE on failure.
     */
    const Boolean vb = FALSE;

    if(MINT_has_table(MINT_TABLE_CHeB))
    {
        double logM;
        const int N = 1000;
        const double min = log10(0.01);
        const double max = log10(1000.0);
        const double dlogM = (max - min) / (N - 1);

        /* one parameter : logM */
        const int nparams = 1;
        /* two data items: age and luminosity at the end of CHeB */
        const int ndata = 2;

        double * table_lifetimes = Malloc(sizeof(double) * N * (nparams + ndata));

        if(table_lifetimes == NULL)
        {
            Exit_binary_c(BINARY_C_MALLOC_FAILED,
                          "Failed to malloc for table_liftimes\n");
        }

        double * const table_lifetimes_start = table_lifetimes;
        if(vb)
        {
            vprint("PRE %p : nparams %zu ndata %zu nlines %zu : table_lifetimes is at %p\n",
                   (void*)stardata->store->MINT_tables[MINT_TABLE_CHeB_TA],
                   stardata->store->MINT_tables[MINT_TABLE_CHeB]->nparam,
                   stardata->store->MINT_tables[MINT_TABLE_CHeB]->ndata,
                   stardata->store->MINT_tables[MINT_TABLE_CHeB]->nlines,
                   (void*)table_lifetimes);
        }

        double * result = Malloc(MINT_result_size(MINT_TABLE_CHeB));

        for(logM = min; logM < max+TINY; logM += dlogM)
        {
            Interpolate(stardata->store->MINT_tables[MINT_TABLE_CHeB],
                        ((double[]){logM,1.0}), /* log10(mass), Yc=1.0 (or nearest) */
                        result,
                        FALSE);
            const double start_age = result[MINT_CHeB_AGE];

            Interpolate(stardata->store->MINT_tables[MINT_TABLE_CHeB],
                        ((double[]){logM,0.0}), /* log10(mass), Yc=0.0 (or nearest) */
                        result,
                        FALSE);

            const double lifetime = result[MINT_CHeB_AGE] - start_age; // yr

            /*
             * columns are:
             * log10(M/Msun),
             * log10(TAMS age (Myr)), log10(TAMS luminosity)
             */
            *(table_lifetimes++) = logM;
            *(table_lifetimes++) = log10(lifetime) - 6.0; // log10(Myr)
            *(table_lifetimes++) = log10(result[MINT_CHeB_LUMINOSITY]);

            if(vb)vprint("Lifetimes table M=%g lifetime=%g-%g=%g Myr L(TAMS)=%g %s\n",
                         exp10(logM),
                         result[MINT_CHeB_AGE],
                         start_age,
                         result[MINT_CHeB_AGE] - start_age,
                         exp10(result[MINT_MS_LUMINOSITY]),
                         L_SOLAR
                );

        }

        NewDataTable_from_Pointer(table_lifetimes_start,
                                  stardata->store->MINT_tables[MINT_TABLE_CHeB_TA],
                                  nparams,
                                  ndata,
                                  N);
        Safe_free(result);

        return TRUE;
    }
    else
    {
        return FALSE;
    }
}

#endif // MINT
