#include "../binary_c.h"
No_empty_translation_unit_warning;

/*
 * Thermohalin mixing in a star.
 * Note: you should call MINT_convection first.
 */

#if defined MINT &&                             \
    defined NUCSYN


#define MIX_DMU 1e-10

static Boolean MINT_do_thermohaline(struct stardata_t * const stardata,
                                    struct star_t * const star,
                                    const Boolean vb);
void MINT_thermohaline(struct stardata_t * const stardata,
                       struct star_t * const star)
{
    const Boolean vb = FALSE;
    if(stardata->preferences->no_thermohaline_mixing==FALSE)
    {
        /*
         * Do thermohaline mixing
         */
        int n = 0;
        const int nmax = 5;
        if(vb)printf("Thermohaline loop start model = %d\n",stardata->model.model_number);
        while(n++<nmax &&
              MINT_do_thermohaline(stardata,
                                   star,
                                   vb)==TRUE)
        {
            if(vb)printf("mixn %d\n",n);
        };
        if(vb)printf("done thermohaline loop : %d (model %d)\n",n,stardata->model.model_number);
    }
}




static Boolean MINT_do_thermohaline(struct stardata_t * const stardata,
                                    struct star_t * const star,
                                    const Boolean vb)
{
    Shell_index i;
    Boolean ret = FALSE, mix_state = FALSE;
    struct mint_shell_t * mixed_region =
        MINT_new_clean_shell;

    Shell_index start = -1, end = -1;
    Dprint("Thermohaline loop over %d shells\n",
           star->mint->nshells);

    if(vb)
    {
        for(i=star->mint->nshells-1; i>=star->mint->nshells-6; i--)
        {
            printf("shell %d : dm %g : mu = %g : XH1 = %g\n",
                   i,
                   star->mint->shells[i].dm,
                   star->mint->shells[i].X[XH1],
                   nucsyn_molecular_weight(star->mint->shells[i].X,
                                           stardata,
                                           1.0));
        }
    }

    for(i=star->mint->nshells-2; i>=0; i--)
    {
        Boolean vvb = vb && i > star->mint->nshells-6;
        const struct mint_shell_t * const above_shell = &star->mint->shells[i+1];
        const struct mint_shell_t * const this_shell = &star->mint->shells[i];

        /* determine already-mixed region, or shell above's, molecular weight */
        const double mu_above =
            nucsyn_molecular_weight(Is_not_zero(mixed_region->dm) ? mixed_region->X : above_shell->X,
                                    stardata,
                                    1.0);

        /* determine molecular weight of this shell */
        const double mu_this =
            nucsyn_molecular_weight(this_shell->X,
                                    stardata,
                                    1.0);

        if(vvb)printf("at %d (mixed region %d to %d dm = %g) mu = %g [above_shell = %d, mu = %g]\n",
                     i,
                     start,
                     end,
                     mixed_region->dm,
                     mu_this,
                     i+1,
                     mu_above);

        /*
         * Check mu is in range
         */
        if(mu_this < 0.0 || mu_above < 0.0 ||
           mu_this > 100.0 || mu_above > 100.0)
        {
            Exit_binary_c(BINARY_C_OUT_OF_RANGE,
                          "mu out of range error : mu_this = %g, mu_above = %g\n",
                          mu_this,
                          mu_above);
        }

        /* if dmu >= 0, mix more */
        const double dmu = mu_above - mu_this;

        /* hence should we mix? */
        Boolean new_mix_state = (dmu >= MIX_DMU && i > 0) ? TRUE : FALSE;

        if(vvb)
            printf("Star %d Shell %d mu this %g above %g : dmu = %g >0 ? %s : mix? %s\n",
                   star->starnum,
                   i,
                   mu_this,
                   mu_above,
                   dmu,
                   Yesno(dmu > MIX_DMU),
                   Yesno(new_mix_state));

        if(new_mix_state == TRUE)
        {
            ret = TRUE;
            if(mix_state == FALSE)
            {
                /*
                 * start of mixing region: has abundances
                 * of the shell above and this shell
                 */
                start = i + 1;
                nucsyn_dilute_shell(mixed_region->dm, mixed_region->X,
                                    above_shell->dm,  above_shell->X);
                mixed_region->dm = above_shell->dm;
            }

            /*
             * Mix this shell into the existing
             * mixing region and set the end shell index
             */
            nucsyn_dilute_shell(mixed_region->dm, mixed_region->X,
                                this_shell->dm,   this_shell->X);
            mixed_region->dm += this_shell->dm;
            if(vvb)printf("MIXED in %d to %d : XH1 = %g, mu = %g\n",
                          start,
                          end,
                          mixed_region->X[XH1],
                          nucsyn_molecular_weight(mixed_region->X,
                                                  stardata,
                                                  1.0));
            end = i;
        }
        /* mix_state == FALSE */
        else if(mix_state == TRUE)
        {
            /*
             * No longer mixing, but we *were* mixing
             * at the shell above:
             * set abundances in all the mixed shells
             */
            if(vvb) printf("Mix region from %d to %d\n",start,end);
            for(Shell_index j=end; j<=start; j++)
            {

                Copy_abundances(mixed_region->X,
                                star->mint->shells[j].X);
                const double mu = nucsyn_molecular_weight(star->mint->shells[j].X,
                                                          stardata,
                                                          1.0);
                if(vvb) printf("SET IN %d mu = %g\n",j,mu);
            }

            /*
             * Clear the mixed_region
             */
            MINT_clean_shell(mixed_region);

            /*
             * Go again
             */
            i = Min(star->mint->nshells - 1,
                    start );
            start = -1;
            end = -1;
        }

        mix_state = new_mix_state;
    }

    if(vb)
    {
        printf("thermohaline mix ret %d\n",ret);
        for(i=star->mint->nshells-1; i>=star->mint->nshells-6; i--)
        {
            printf("shell %d/%d : mu = %g\n",
                   i,
                   star->mint->nshells,
                   nucsyn_molecular_weight(star->mint->shells[i].X,
                                           stardata,
                                           1.0));
        }
    }

    Safe_free(mixed_region);

    return ret;
}

#endif // MINT && NUCSYN
