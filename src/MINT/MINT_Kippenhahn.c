#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef MINT
static void _outkipp(struct stardata_t * const stardata,
                     struct star_t * const star,
                     char * const header);

/*
 * MINT function to output a Kippenhahn diagram of
 * various elements.
 */

void MINT_Kippenhahn(struct stardata_t * Restrict const stardata,
                     struct star_t * const star Maybe_unused)
{
    //printf("KIPPY %p %p\n",(void*)star,(void*)companion);
#ifdef NUCSYN
#define _KIPPENHAHN_ISOTOPE_LIST \
    X(   H1)                     /* 5 */\
    X(  He4)                     \
    X(  Li7)                     \
    X(  C12)                     \
    X(  C13)                     \
    X(  N14)                     /* 10 */\
    X(  N15)                     \
    X(  O16)                     \
    X(  O17)                     \
    X(  O18)                     \
    X(  F17)                     /* 15 */\
    X(  F19)                     \
    X( Ne20)                     \
    X( Ne21)                     \
    X( Ne22)                     \
    X( Na23)                     /* 20 */\
    X( Mg24)                     \
    X( Mg25)                     \
    X( Mg26)                     \
    X( Al26)                     \
    X( Al27)                     /* 25 */\
    X( Si28)

#endif //NUCSYN

    if(star != NULL &&
       star->stellar_type != MASSLESS_REMNANT)
    {
        _outkipp(stardata,star,"KIPP");
    }
}

static void _outkipp(struct stardata_t * const stardata,
                     struct star_t * const star Maybe_unused,
                     char * const header Maybe_unused)
{
    if(MINT_has_shells(star))
    {
        Foreach_shell(shell)
        {
            Printf("%s %g %g %g %g %g ",
                   header,
                   stardata->model.time,
                   shell->m,
                   shell->dm,
                   shell->T,
                   shell->rho);
#ifdef NUCSYN
#undef X
            /* use this to show, e.g., C12=<number> */
//#define X(ISO) Printf("%s=%g ",#ISO,shell->X[X##ISO]);
//int n=0;

            /* use this for just numbers */
#define X(ISO) Printf("%g ",shell->X[X##ISO]);
            _KIPPENHAHN_ISOTOPE_LIST;
#undef X
#endif // NUCSYN

            Printf("\n");
        }
        Printf("%s\n",header);
    }
}

#endif // MINT
