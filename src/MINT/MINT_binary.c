#include "../binary_c.h"

No_empty_translation_unit_warning;

#if defined MINT

/*
 * Mass in this timestep from derivative D of star S
 */
#define Dm(DERIV) (star->derivative[DERIV] * stardata->model.dt)

/*
 * Subroutine to update the abundances and yields of each star
 * due to mass mass transfer, gain and loss.
 *
 * Returns TRUE if masses change, which requires a recalculation
 * of stellar structure.
 */

void MINT_binary(struct stardata_t * const Restrict stardata)
{

    const Boolean vb = FALSE;

    /*
     * These shells contain the accretion abundances
     */
    struct mint_shell_t * wind[NUMBER_OF_STARS] = { NULL };
    struct mint_shell_t * stream[NUMBER_OF_STARS] = { NULL };

    /*
     * Abundances in stellar wind mass loss
     */
    Foreach_star(star)
    {
        if(MINT_has_shells(star))
        {
            const double dm = -Dm(DERIVATIVE_STELLAR_MASS_WIND_LOSS);
            wind[star->starnum] = MINT_new_clean_shell;
            if(Is_really_not_zero(dm))
            {
                if(vb) printf("remove dm = %g in wind from star %d (nshells %d)\n",
                              dm,
                              star->starnum,
                              star->mint->nshells);
#ifdef NUCSYN
                MINT_X_from_surface(
                    stardata,
                    star,
                    0.0,
                    dm,
                    wind[star->starnum]->X
                    );
#endif//NUCSYN
                wind[star->starnum]->dm = -dm;
            }
        }
    }

    /*
     * Collisional wind mass transfer alters wind[k].
     */
    Foreach_star(star)
    {
        if(MINT_has_shells(star))
        {
            const double dm_wind_gain = +Dm(DERIVATIVE_STELLAR_MASS_WIND_GAIN);
            const int k = star->starnum;

            if(Is_really_not_zero(dm_wind_gain))
            {
                const double dm_wind_loss = -Dm(DERIVATIVE_STELLAR_MASS_WIND_LOSS);

                /*
                 * wind[k]->dm should be the net mass change of the star
                 * due to wind
                 */
                wind[k]->dm = dm_wind_gain + dm_wind_loss;

#ifdef NUCSYN
                /*
                 * Choose f, the fraction of the accreting wind which gets mixed
                 * with the star's wind prior to accretion.
                 *
                 * f=0 is like having all the mass from the companion's wind
                 *      hit the star, as in normal wind accretion.
                 *
                 * f=1 is like having a shock so all this star's wind
                 *     hits the star, as in complete reaccretion.
                 *
                 * if wind[Other_star(k)] == NULL, there is not wind
                 * from the other star, so f = 1
                 *
                 * f is also limited by the accretion rate
                 */
                const double f =
                    wind[Other_star(k)] == NULL
                    ? 1.0
                    : Min(wind_mixing_factor(stardata,k),
                          dm_wind_loss/(dm_wind_gain+VERY_TINY));

                /* mass from the companion */
                const double Dm_companion_wind = dm_wind_gain * (1.0 - f);

                /* mass from the star that is reaccreted */
                const double Dm_star_wind = dm_wind_gain * f;

                /* mix to new shell */
                nucsyn_dilute_shell(
                    Dm_star_wind,
                    wind[k]->X,
                    Dm_companion_wind,
                    wind[Other_star(k)]->X
                    );
#endif//NUCSYN
            }
        }
    }

    /*
     * Abundances in RLOF and other "streams" of mass loss.
     *
     * We remove this from the surface after the wind mass loss
     * has already removed its material.
     */
    Foreach_star(star)
    {
        if(MINT_has_shells(star))
        {
            stream[star->starnum] = MINT_new_clean_shell;
        }
    }

    Foreach_star(star,companion)
    {
        if(MINT_has_shells(star))
        {

            const double dm_RLOF_loss = Dm(DERIVATIVE_STELLAR_MASS_RLOF_LOSS);
            const double dm_RLOF_gain = Dm(DERIVATIVE_STELLAR_MASS_RLOF_GAIN);
            const double dm_excretion_loss = Dm(DERIVATIVE_STELLAR_MASS_DECRETION_DISC);
            const double Maybe_unused dm_disc_loss = Dm(DERIVATIVE_STELLAR_MASS_DISC_LOSS);
            const double dm_nonconservative_loss = Dm(DERIVATIVE_STELLAR_MASS_DISC_LOSS);

            /*
             * dm is the stream change in mass of star k
             */
            const double dm =
                dm_RLOF_loss +
                dm_RLOF_gain +
                dm_excretion_loss +
                dm_disc_loss +
                dm_nonconservative_loss;

            if(Is_not_zero(dm))
            {
                if(dm > 0.0)
                {
                    /*
                     * Accrete on star k from star r,
                     * skipping material already removed by winds
                     * from star r
                     */
                    const double dm_skip = wind[companion->starnum] != NULL ? (-wind[companion->starnum]->dm) : 0.0;
                    if(vb)
                        printf("star %d accreting from star %d : remove dm = %g, starting at depth %g, in RLOF \n",
                               star->starnum,
                               companion->starnum,
                               dm,
                               dm_skip);
#ifdef NUCSYN
                    MINT_X_from_surface(
                        stardata,
                        &stardata->star[companion->starnum],
                        dm_skip,
                        dm,
                        stream[star->starnum]->X
                        );
                    if(vb)printf("X from surf XH1 %g\n",
                                 stream[star->starnum]->X[XH1]);
#endif//NUCSYN
                }
                stream[star->starnum]->dm = dm;
            }
        }
    }

    /*
     * Hence the abundance of accreted material
     */
    Boolean accreted[NUMBER_OF_STARS] = { FALSE };
    Foreach_star(star)
    {
        const int k = star->starnum;
        /*
         * Add wind accretion shell
         */
        if(wind[k] != NULL &&
           wind[k]->dm > 0.0)
        {
            accreted[k] = TRUE;
            if(MINT_has_shells(star))
            {
                MINT_add_shell(stardata,
                               star,
                               wind[k]);
#ifdef NUCSYN
                //printf("ADDED shell on star %d mass %g XH1 %g\n",k,wind[k]->dm,wind[k]->X[XH1]);
#endif//NUCSYN
            }

            /*
             * Add RLOF accretion shell
             */
            if(stream[k] != NULL &&
               stream[k]->dm > 0.0)
            {
                accreted[k] = TRUE;
                MINT_add_shell(stardata,
                               star,
                               stream[k]);
                if(vb)printf("add stream to star %d dm %g : outer shell mass %g\n",
                             star->starnum,
                             stream[k]->dm,
                             star->mint->shells[star->mint->nshells-1].dm);
            }
        }
    }

    /*
     * If we accreted, recalculate stellar structure.
     */
    Foreach_evolving_star(star)
    {
        if(accreted[star->starnum])
        {
            MINT_stellar_structure(stardata,
                                   NULL,
                                   star,
                                   STELLAR_STRUCTURE_CALLER_MINT_BINARY);
        }
    }

    /*
     * Remesh and mix if required
     */
    Foreach_evolving_star(star)
    {
        if(MINT_has_shells(star))
        {
            Star_number k = star->starnum;
            if(vb)
            {
                for(Shell_index j=star->mint->nshells-1; j>star->mint->nshells-10; j--)
                {
                    struct mint_shell_t * const shell = &star->mint->shells[j];
                    printf("remesh Shell %d : m = %g, dm = %g\n",
                           j,
                           shell->m,
                           shell->dm);
                }
            }

            if(accreted[k] == TRUE)
            {
                if(vb)
                    printf("remesh %d from %d to ...\n",k,star->mint->nshells-1);

                MINT_remesh(stardata,
                            star);

                if(vb)printf("%d\n",star->mint->nshells);
#ifdef NUCSYN
                if(vb)printf("thermohaline %d\n",k);
                MINT_thermohaline(stardata,
                                  star);
#endif // NUCSYN
            }
        }
    }

    {
        Star_number k;
        Starloop(k)
        {
            Safe_free(wind[k]);
            Safe_free(stream[k]);
        }
    }
}

#endif // MINT && NUCSYN
