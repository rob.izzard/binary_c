#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef MINT

/*
 * MINT function to calculate stellar structure
 * of a massless remnant.
 */

Stellar_type MINT_stellar_structure_massless_remnant(struct stardata_t * Restrict const stardata Maybe_unused,
                                                     struct star_t * const prevstar Maybe_unused,
                                                     struct star_t * const newstar,
                                                     const Caller_id caller_id Maybe_unused)
{
    /*
     * Do nothing
     */


    return newstar->stellar_type;
}

#endif // MINT
