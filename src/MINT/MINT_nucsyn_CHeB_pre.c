#include "../binary_c.h"

No_empty_translation_unit_warning;

#if defined MINT && defined NUCSYN

void MINT_nucsyn_CHeB_pre(struct stardata_t * Restrict const stardata,
                          struct star_t * Restrict const star)
{
    /*
     * Enable shellular nuclear burning
     */
    star->do_burn = TRUE;

    if(star->do_burn == FALSE)
    {
        /*
         * Set the (convective) helium-burning core mass.
         *
         * BSE does not give us this, so assume it is half
         * the core mass (~true for M=2.5 Z=0.02)
         */
        const double mc_He = star->convective_core_mass;

        /* maximum possible helium content */
        const double max_Y = 1.0 - stardata->common.metallicity;

        /*
         * He-burning lifetime from BSE
         */
        struct BSE_data_t * bse = new_BSE_data();
        stellar_timescales(stardata,
                           star,
                           bse,
                           star->phase_start_mass,
                           star->mass,
                           star->stellar_type);
        const double dY_dt = -max_Y / (bse->timescales[T_HE_BURNING]*1e6);
        const double dY = stardata->model.dt * dY_dt;

        free_BSE_data(&bse);

        Foreach_shell(shell)
        {
            if(shell->m < mc_He)
            {
                /*
                 * Evolve helium in the core.
                 *
                 * We assume all the core is burning helium,
                 * such that at the beginning of CHeB Y=1-Z,
                 * and at the end Y=0.
                 *
                 * Shells which are newly ingested in the core
                 * have X>1, so in those we immediately
                 * convert H to He, and CNO to N, prior to
                 * helium burning.
                 */
                if(shell->X[XH1] > 0.0)
                {
                    shell->X[XHe4] += shell->X[XH1];
                    shell->X[XH1] = 0.0;
                    shell->X[XN14] += shell->X[XC12] + shell->X[XC13] + shell->X[XN15] + shell->X[XO16] + shell->X[XO17] + shell->X[XO18];
                    shell->X[XC12] = 0.0;
                    shell->X[XC13] = 0.0;
                    shell->X[XN15] = 0.0;
                    shell->X[XO16] = 0.0;
                    shell->X[XO17] = 0.0;
                    shell->X[XO18] = 0.0;
                }
                shell->X[XHe4] += dY;
                shell->X[XHe4] = Max(0.0,shell->X[XHe4]);
                shell->X[XC12] -= 0.5 * dY;
                shell->X[XO16] -= 0.5 * dY;
                shell->X[XNe22] += shell->X[XN14];
                shell->X[XN14] = 0.0;
            }
        }
    }
}
#endif // MINT && NUCSYN
