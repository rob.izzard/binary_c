#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef MINT

void MINT_free(struct stardata_t * const Restrict stardata Maybe_unused)
{
    /*
     * Free memory allocated for MINT in the
     * binary_c stardata but that's not the store
     * or persistent_data
     * (see MINT_free_store and MINT_free_persistent_data
     * for that).
     */
    Foreach_star(star)
    {
        free_data_table(&star->mint->GB_table,
                        TRUE,
                        TRUE,
                        TRUE);
        MINT_free_star(star);
    }
}

#endif//MINT
