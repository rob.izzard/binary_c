#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef MINT

/*
 * Copy the MINT data in star in src to dest, which
 * should already be allocated memory.
 *
 * To be called from copy_star()
 */
void MINT_copy_star(struct stardata_t * const stardata,
                    const struct star_t * const Restrict src,
                    struct star_t * const Restrict dest,
                    struct mint_shell_t * const mint_shells_p,
                    const Shell_index mint_nshells)
{
    /*
     * Copy MINT structures
     */
    if(src->mint != dest->mint &&
       src->mint != NULL)
    {
        if(dest->mint == NULL)
        {
            dest->mint = Malloc(sizeof(struct mint_t));
            Cprint("dest->mint is NULL, alloced to %p\n",
                   (void*)dest->mint);
        }

        Cprint("memcpy mint %p %p\n",
               (void*)dest->mint,
               (void*)src->mint);

        /*
         * Copy main MINT pointer
         */
        memcpy(dest->mint,
               src->mint,
               sizeof(struct mint_t));

        /*
         * Restore shells
         */
        Cprint("restore mint->shells from %p to %p\n",
               (void*)dest->mint->shells,
               (void*)mint_shells_p);
        dest->mint->shells = mint_shells_p;

        if(src->mint->shells == NULL)
        {
            /*
             * No shells to copy
             */
            Safe_free(dest->mint->shells);
        }
        else
        {
            /*
             * Copy shells
             */
            const size_t shellssize = sizeof(struct mint_shell_t)*
                src->mint->nshells;

            if(dest->mint->shells == NULL &&
               dest->mint->nshells > 0)
            {
                /*
                 * Require new memory for shells
                 */
                dest->mint->shells = Malloc(shellssize);
                Cprint("MINT shells Malloc at %p\n",
                       (void*)dest->mint->shells);
            }
            else if(dest->mint->shells != NULL &&
                    mint_nshells != dest->mint->nshells)
            {
                /*
                 * Shell number has changed, realloc
                 * or free (if new shell number is zero).
                 */
                if(shellssize == 0)
                {
                    Safe_free(dest->mint->shells);
                    Cprint("MINT shells set to %p\n",
                           (void*)dest->mint->shells);
                }
                else
                {
                    Cprint("realloc MINT shells to size %zu, was %p\n",
                           shellssize,
                           (void*)dest->mint->shells);
                    dest->mint->shells = Realloc(dest->mint->shells,
                                                 shellssize);
                }
            }

            Cprint("copy MINT shells %p to %p size %zu\n",
                   (void*)src->mint->shells,
                   (void*)dest->mint->shells,
                   shellssize);

            /*
             * Copy shells' contents if required
             */
            if(shellssize > 0 &&
               src->mint->shells != NULL &&
               dest->mint->shells != NULL)
            {
                memcpy(dest->mint->shells,
                       src->mint->shells,
                       shellssize);
            }
        }
    }
}

#endif//MINT
