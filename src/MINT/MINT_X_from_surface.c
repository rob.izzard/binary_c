#include "../binary_c.h"
No_empty_translation_unit_warning;

#if defined MINT && defined NUCSYN

/*
 * Mass in this timestep from derivative D of star S
 */
#define Dm(DERIV) (star->derivative[DERIV] * stardata->model.dt)

/*
 * Subroutine to update the abundances and yields of each star
 * due to mass mass transfer, gain and loss.
 */

void MINT_X_from_surface(const struct stardata_t * const Restrict stardata Maybe_unused,
                         const struct star_t * const star,
                         const double dm_skip,
                         const double dm,
                         Abundance * const X)
{
    /*
     * Obtain the abundances in the mass dm lost from
     * the star, starting at a depth dm_skip.
     *
     * X should be set with New_clear_isotope_array()
     * prior to this function being called.
     */
    Shell_index n = star->mint->nshells - 1;
    double remaining = dm;
    double depth = 0.0;
    struct mint_shell_t * mixed_region =
        MINT_new_clean_shell;
    const Boolean vb = FALSE;

    if(vb)printf("remove mixed_region = %p (%p) : dm = %g from surface, shell starts with X=%g dm=%g\n",
                 (void*)mixed_region,
                 (void*)(mixed_region->X),
                 dm,
                 mixed_region->X[XH1],
                 mixed_region->dm);

    while(n >= 0 &&
          remaining > 0.0)
    {
        const struct mint_shell_t * const shell = &star->mint->shells[n];
        if(vb)printf("At shell %d\n",n);

        /*
         * Current depth
         */
        depth += shell->dm;
        if(vb)printf("Depth at base of shell = %g\n",depth);

        /*
         * How much mass do we take from this shell?
         */
        const double ddm =
            depth > dm_skip ?
            Min3(dm,
                 shell->dm,
                 depth - dm_skip) :
            0.0;

        if(vb)printf("take ddm = %g from this shell\n",ddm);

        if(Is_really_not_zero(ddm))
        {
            /*
             * Mass ddm from this shell contributes
             * to the abundances
             */
            nucsyn_dilute_shell(mixed_region->dm, mixed_region->X,
                                ddm,              shell->X);
            mixed_region->dm += ddm;
            remaining -= ddm;
            if(vb)printf("Mixed region : dM = %g, X H1 = %g\n",
                         mixed_region->dm,
                         mixed_region->X[XH1]);
        }

        /*
         * Move to the next shell
         */
        n--;
    }

    if(vb)printf("Copy abundances from %p to %p\n",
                 (void*)mixed_region->X,
                 (void*)X);
    Copy_abundances(mixed_region->X,X);

    Safe_free(mixed_region);
}
#endif // MINT && NUCSYN
