#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef MINT
#include "MINT.h"
#include "MINT_load_grid.h"

/*
 * Subroutine to load zero-age main-sequence
 * composition data for MINT.
 *
 * Return TRUE on success, FALSE on failure.
 */

Boolean MINT_load_ZAMS_composition_grid(struct stardata_t * const stardata,
                                        const int when)
{
    const int vb = 0; /* set vb >=1 for verbose output */

    if(vb)printf("MINT load ZAMS composition? (MINT_use_ZAMS_profiles %s, MINT_MS == %p, MINT_dir \"%s\")\n",
                 Yesno(stardata->preferences->MINT_use_ZAMS_profiles),
                 (void*)stardata->store->MINT_tables[MINT_TABLE_ZAMS_COMPOSITION],
                 stardata->preferences->MINT_dir);

    if(stardata->preferences->MINT_use_ZAMS_profiles == TRUE)
    {

#undef X
#define X(NAME,ACTION) Stringify_expanded(NAME),
        static const char * parameter_names[] = { MINT_ZAMS_PARAMETER_ITEMS_LIST };
        static const char * data_names[] = { MINT_ZAMS_COMPOSITION_DATA_ITEMS_LIST };
#undef X

        const Boolean x = MINT_Load_Table(
            stardata,
            MINT_TABLE_ZAMS_COMPOSITION,
            NULL,
            NULL,
            "MINT_ZAMS_COMPOSITION",
            parameter_names,
            data_names,
            MINT_ZAMS_composition_parameter_actions,
            MINT_ZAMS_composition_data_actions,
            when,
            NULL,
            vb
            );

        if(vb)
        {
            printf("ZAMS composition table %p\n",
                   (void*)stardata->store->MINT_tables[MINT_TABLE_ZAMS_COMPOSITION]);
        }

        Dprint("Loaded grid\n");
        return x;
    }
    else
    {
        return TRUE;
    }
}

#endif // MINT
