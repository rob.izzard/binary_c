#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef MINT

/*
 * Function to estimate the binding energy of the star,
 * between m_low and m_high, in cgs.
 */

double MINT_partial_binding_energy(struct stardata_t * const stardata,
                                   struct star_t * const star,
                                   const double m_low,
                                   const double m_high,
                                   struct mint_shell_t ** const low_shell,
                                   struct mint_shell_t ** const high_shell)
{
    double Ebind;
    if(MINT_has_shells(star))
    {
        Ebind = 0.0;
        if(low_shell != NULL)
        {
            *low_shell = NULL;
        }
        if(high_shell != NULL)
        {
            *high_shell = NULL;
        }
        Dprint("Loop from shell %d to %d\n",star->mint->nshells-1,0);
        const struct mint_shell_t * const surface = &star->mint->shells[star->mint->nshells-1];
        struct mint_shell_t * shell = &star->mint->shells[0];
        while(shell<surface)
        {
            if(shell->m > m_low && shell->m < m_high)
            {
                if(low_shell != NULL &&
                   *low_shell == NULL)
                {
                    *low_shell = shell;
                }
                if(high_shell != NULL)
                {
                    *high_shell = shell;
                }
                Ebind += shell->m * shell->dm / shell->radius;
            }
            shell++;
        }
        Ebind *= - GRAVITATIONAL_CONSTANT *
            Pow2(M_SUN) / R_SUN;
    }
    else
    {
        /*
         * Fallbacks for when we have no shell structure
         */
        if(star->stellar_type <= MAIN_SEQUENCE)
        {
            /* TODO : integrate structure */

            Ebind =
                -0.5 *
                GRAVITATIONAL_CONSTANT *
                star->mass * star->mass / star->radius *
                M_SUN * M_SUN / R_SUN;
        }
        else
        {
            /*
             * For giants, use algorithm of choice.
             *
             * convfrac is the fraction of the envelope outside
             * the core that is convective.
             */
            const double convfrac = 1.0;
            const double lambda = common_envelope_dewi_tauris(convfrac,
                                                              star->luminosity,
                                                              star->mass,
                                                              star->rzams,
                                                              star->radius,
                                                              star->stellar_type,
                                                              stardata);
            const double menv = envelope_mass(star);
            Ebind =
                - GRAVITATIONAL_CONSTANT *
                star->mass * menv / (star->radius * lambda) *
                M_SUN * M_SUN / R_SUN;
        }
    }
    return Ebind;
}
#endif //MINT
