#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef MINT

void MINT_split_shell(struct stardata_t * const stardata Maybe_unused,
                      struct star_t * const star Maybe_unused,
                      const int k)
{
    /*
     * Split shell k into two identical shells
     * and increase the number of shells
     */
    if(star->mint->nshells < stardata->preferences->MINT_maximum_nshells)
    {
        const Shell_index new_nshells = star->mint->nshells + 1;
        star->mint->shells =
            Realloc(star->mint->shells,
                    new_nshells * sizeof(struct mint_shell_t));

        struct mint_shell_t * const this = &star->mint->shells[k];
        memmove(this+1,
                this,
                sizeof(struct mint_shell_t) * (star->mint->nshells - k));
        this->dm *= 0.5;
        (this+1)->m += this->dm;
        (this+1)->dm *= 0.5;
        star->mint->nshells  = new_nshells;
    }
}


#endif // MINT
