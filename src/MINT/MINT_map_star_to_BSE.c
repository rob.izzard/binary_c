#include "../binary_c.h"

No_empty_translation_unit_warning;

#ifdef MINT

void MINT_map_star_to_BSE(struct stardata_t * stardata Maybe_unused,
                          struct star_t * oldstar Maybe_unused,
                          struct star_t * MINT_star)
{
    /*
     * Function to map a MINT star to an equivalent in BSE
     */
    struct star_t * BSE_star = new_star(stardata,MINT_star);

    if(MINT_star->stellar_type <= MAIN_SEQUENCE)
    {
        Boolean alloc[3] = { FALSE };
        stellar_structure_BSE_alloc_arrays(BSE_star, BSE_star->bse, alloc);
        stellar_timescales(stardata,
                           BSE_star,
                           BSE_star->bse,
                           BSE_star->phase_start_mass,
                           BSE_star->mass,
                           BSE_star->stellar_type);

        const double Xc0 = MINT_initial_XHc(stardata);
        BSE_star->age = BSE_star->bse->timescales[T_MS] * (1.0 - BSE_star->mint->XHc / Xc0);
        BSE_star->epoch = stardata->model.time - BSE_star->age;

        Dprint("SETAGE %g vs MS %g\n",
               BSE_star->age,
               BSE_star->bse->timescales[T_MS]);
        stellar_structure_BSE_free_arrays(BSE_star, alloc);
    }
    else if(MINT_star->stellar_type == HERTZSPRUNG_GAP)
    {
        Boolean alloc[3] = { FALSE };
        stellar_structure_BSE_alloc_arrays(BSE_star, BSE_star->bse, alloc);
        stellar_timescales(stardata,
                           BSE_star,
                           BSE_star->bse,
                           BSE_star->phase_start_mass,
                           BSE_star->mass,
                           BSE_star->stellar_type);
        BSE_star->age = BSE_star->bse->timescales[T_MS];
        BSE_star->epoch = stardata->model.time - BSE_star->age;

        Dprint("SETAGE %g vs MS %g\n",
               BSE_star->age,
               BSE_star->bse->timescales[T_MS]);
        stellar_structure_BSE_free_arrays(BSE_star, alloc);
    }
    else if(MINT_star->stellar_type == GIANT_BRANCH)
    {
        Boolean alloc[3] = { FALSE };
        stellar_structure_BSE_alloc_arrays(BSE_star, BSE_star->bse, alloc);
        stellar_timescales(stardata,
                           BSE_star,
                           BSE_star->bse,
                           BSE_star->phase_start_mass,
                           BSE_star->mass,
                           BSE_star->stellar_type);
        BSE_star->age = BSE_star->bse->timescales[T_BGB];
        BSE_star->epoch = stardata->model.time - BSE_star->age;

        Dprint("SETAGE %g vs BGB %g\n",
               BSE_star->age,
               BSE_star->bse->timescales[T_BGB]);
        stellar_structure_BSE_free_arrays(BSE_star, alloc);
    }
    else if(MINT_star->stellar_type == CHeB)
    {
        Boolean alloc[3] = { FALSE };
        stellar_structure_BSE_alloc_arrays(BSE_star, BSE_star->bse, alloc);
        stellar_timescales(stardata,
                           BSE_star,
                           BSE_star->bse,
                           BSE_star->phase_start_mass,
                           BSE_star->mass,
                           BSE_star->stellar_type);
        /*
         * force the age and epoch such that BSE thinks
         * we are a CHeB star
         */
        BSE_star->age = BSE_star->bse->timescales[T_HE_IGNITION];
        BSE_star->epoch = stardata->model.time - BSE_star->age;

        Dprint("SETAGE %g vs Heig %g\n",
               BSE_star->age,
               BSE_star->bse->timescales[T_HE_IGNITION]);
        stellar_structure_BSE_free_arrays(BSE_star, alloc);
    }

    copy_star(stardata,
              BSE_star,
              MINT_star);
    free_star(&BSE_star);
}

#endif // MINT
