#pragma once
#ifndef MINT_DATA_COLUMNS_GB_LIST_DEF
#define MINT_DATA_COLUMNS_GB_LIST_DEF

/*
 * Define the MINT first red giant branch (GB) table.
 *
 * These are the order in which columns are
 * set in the MINT interpolation table.
 *
 * Each column name should be in CAPTIALS with
 * underscores (_) as word separators,
 * and wrapped in an X(...), e.g.,
 *
 * #define MINT_GB_PARAMETER_ITEMS             \
 *         X(MASS, NOTHING),                   \
 *         X(CENTRAL_HELIUM, NOTHING)
 *
 * And note that in the code, these are prefixed
 * automatically with MINT_GB_, e.g. to make
 * MINT_GB_MASS, MINT_GB_CENTRAL_HELIUM, etc.
 *
 * Scalar items should be listed first,
 * with items on Chebyshev lists coming last
 * with CHEBYSHEV in their names.
 *
 * The column names listed here should each
 * match a column of the input data file, as
 * specified in the header of that file. If they
 * do not, you probably will not be able to
 * get things to run as you'd like... so you'll
 * have to take some kind of executive action
 * to fix this, or fill the data from some other
 * source (e.g. BSE's fitting functions).
 *
 * The ..._PARAMETER_ITEMS must be scalars only.
 *
 * The lengths of the Chebyshev lists are automatically
 * calculated based on the data file contents.
 */


/*
 * The MINT_GB_PARAMETER_ITEMS are prefixed by MINT_GB_
 *
 * The table has two columns: first the identifier
 * which is used in the code and in the data file.
 * Second is the "action" which is one of the
 * MINT_ACTIONS_LIST
 */
#define MINT_GB_PARAMETER_ITEMS_LIST                    \
    X(                      MASS, NOTHING/* was log */) \
    X(        CENTRAL_DEGENERACY,              NOTHING) \
    X( HELIUM_CORE_MASS_FRACTION,              NOTHING)

/*
 * The MINT_GB_DATA_ITEMS are prefixed by MINT_GB_
 *
 * The table has two columns: first the identifier
 * which is used in the code and in the data file.
 * Second is the "action" which is one of the
 * MINT_ACTIONS_LIST
 */
#define MINT_GB_DATA_ITEMS_LIST                                        \
    X(                                      RADIUS,  NOTHING/*LOG10*/) \
    X(                                  LUMINOSITY,  NOTHING/*LOG10*/) \
    X(                                         AGE,  NOTHING/*LOG10*/) \
    X(                                INITIAL_MASS, NOTHING/* LOG10*/) \
    X(               CONVECTIVE_CORE_MASS_FRACTION,           NOTHING) \
    X(             CONVECTIVE_CORE_RADIUS_FRACTION,           NOTHING) \
    X(     CONVECTIVE_CORE_MASS_OVERSHOOT_FRACTION,           NOTHING) \
    X(   CONVECTIVE_CORE_RADIUS_OVERSHOOT_FRACTION,           NOTHING) \
    X(           CONVECTIVE_ENVELOPE_MASS_FRACTION,           NOTHING) \
    X(         CONVECTIVE_ENVELOPE_RADIUS_FRACTION,           NOTHING) \
    X(                 HELIUM_CORE_RADIUS_FRACTION,           NOTHING) \
    X(         LUMINOSITY_DIV_EDDINGTON_LUMINOSITY,           NOTHING) \
    X(                         NEUTRINO_LUMINOSITY,           NOTHING) \
    X(                                          K2,           NOTHING) \
    X(                                    TIDAL_E2,           NOTHING) \
    X(                          TIDAL_E_FOR_LAMBDA,           NOTHING) \
    X(                    MOMENT_OF_INERTIA_FACTOR,           NOTHING) \
    X(        HELIUM_CORE_MOMENT_OF_INERTIA_FACTOR,           NOTHING) \
    X(                  TIMESCALE_KELVIN_HELMHOLTZ,           NOTHING) \
    X(                         TIMESCALE_DYNAMICAL,           NOTHING) \
    X(                           TIMESCALE_NUCLEAR,           NOTHING) \
    X(                  MEAN_MOLECULAR_WEIGHT_CORE,           NOTHING) \
    X(               MEAN_MOLECULAR_WEIGHT_AVERAGE,           NOTHING) \
    X(         FIRST_DERIVATIVE_CENTRAL_DEGENERACY,           NOTHING) \
    X(        SECOND_DERIVATIVE_CENTRAL_DEGENERACY,           NOTHING) \
    X(  FIRST_DERIVATIVE_HELIUM_CORE_MASS_FRACTION,           NOTHING) \
    X( SECOND_DERIVATIVE_HELIUM_CORE_MASS_FRACTION,           NOTHING) \
    X(                                WARNING_FLAG,           NOTHING) \
    X(                         HELIUM_IGNITED_FLAG,           NOTHING) \
    X(              LOG_HELIUM_LUMINOSITY_FRACTION,           NOTHING) \
    X(                           HELIUM_LUMINOSITY,           NOTHING) \
    X(                       CHEBYSHEV_TEMPERATURE,           NOTHING) \
    X(                           CHEBYSHEV_DENSITY,           NOTHING) \
    X(                    CHEBYSHEV_TOTAL_PRESSURE,           NOTHING) \
    X(                      CHEBYSHEV_GAS_PRESSURE,           NOTHING) \
    X(                            CHEBYSHEV_RADIUS,           NOTHING) \
    X(                            CHEBYSHEV_GAMMA1,           NOTHING) \
    X(             CHEBYSHEV_PRESSURE_SCALE_HEIGHT,           NOTHING) \
    X(             CHEBYSHEV_DIFFUSION_COEFFICIENT,           NOTHING) \

#endif // MINT_DATA_COLUMNS_GB_LIST_DEF


/*
    X(   CONVECTIVE_ENVELOPE_MASS_SIMPLIFIED, NOTHING) \
    X( CONVECTIVE_ENVELOPE_RADIUS_SIMPLIFIED, NOTHING) \
*/
