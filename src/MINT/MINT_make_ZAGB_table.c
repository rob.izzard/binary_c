#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef MINT
#include "MINT.h"
#include "MINT_load_grid.h"

/*
 * Subroutine to construct zero-age GB data
 * for MINT.
 *
 * Note : we work with the unmapped version of the GB data,
 * so this should be called from the appropriate place in
 * MINT_load_GB_grid.
 *
 * Return TRUE on success, FALSE on failure.
 */

Boolean MINT_make_ZAGB_table(struct stardata_t * const stardata Maybe_unused,
                             const int when Maybe_unused)
{
    return TRUE;
}


#endif // MINT
