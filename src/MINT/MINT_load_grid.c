#include "../binary_c.h"
No_empty_translation_unit_warning;

//#undef DEBUG
//#define DEBUG 1

#ifdef MINT

/* disable, during testing, deprecated declarations */
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"

#define __TEST


int MINT_load_grid(struct stardata_t * const stardata)
{
    /*
     * Load MINT stellar evolution data.
     *
     * The data is in stardata->preferences->MINT_dir
     */

    /*
     * Check the MINT directory has been set
     */
    const Boolean vb = FALSE;

    Dprint("MINT_dir = %s\n",
           stardata->preferences->MINT_dir);
    if(stardata->preferences->MINT_dir[0] == '\0')
    {
        if(vb)printf("call exit\n");
        Exit_binary_c(BINARY_C_ENVVAR_EMPTY,
                      "MINT_dir is empty : probably you need to set the MINT_dir environment variable\n");
    }

    /*
     * Strip trailing slashes from the MINT directory name
     */
    strip_trailing(stardata->preferences->MINT_dir,'/');

    /*
     * Load data tables from the functions
     * set in MINT_data_tables.def, subject to
     * those set in the cmd-line option MINT_data_tables
     * (if set).
     */
#undef X
#define X(ID,FILE,FUNCTION,FAILACTION,WHEN) &FUNCTION,
    static Boolean (*functions[])(struct stardata_t * const,const int) =
        { MINT_TABLES_LIST };
#undef X
#define X(ID,FILE,FUNCTION,FAILACTION,WHEN) TRUE,
    static Boolean table_bool[] = /* load all tables by default */
        { MINT_TABLES_LIST };
#undef X
#define X(ID,FILE,FUNCTION,FAILACTION,WHEN) FAILACTION,
    static const unsigned int fail_on_error[] =
        { MINT_TABLES_LIST };
#undef X
#define X(ID,FILE,FUNCTION,FAILACTION,WHEN) WHEN,
    static const int when[] =
        { MINT_TABLES_LIST };
#undef X
#define X(ID,FILE,FUNCTION,FAILACTION,WHEN) #ID,
    static const char * idstrings[] =
        { MINT_TABLES_LIST };
#undef X
#define X(ID,FILE,FUNCTION,FAILACTION,WHEN) #FUNCTION,
    static const char * funcstrings[] =
        { MINT_TABLES_LIST };
#undef X
#define X(ID,FILE,FUNCTION,FAILACTION,WHEN) FILE,
    static const char * filenames[] =
        { MINT_TABLES_LIST };
#undef X


    if(stardata->preferences->MINT_data_tables[0] != '\0')
    {
        for(unsigned int i=0; i<MINT_TABLE_NUMBER; i++)
        {
            table_bool[i] = FALSE;
        }
        struct string_array_t * table_strings =
            new_string_array(0);
        string_split(stardata,
                     stardata->preferences->MINT_data_tables,
                     table_strings,
                     ',',
                     0,
                     0,
                     FALSE);
#undef X
#define X(ID,FILE,FUNCTION,FAILACTION,WHEN)                 \
        if(string_in_string_array(#ID,table_strings))       \
        {                                                   \
            table_bool[MINT_TABLE_##ID] = TRUE;             \
            j++;                                            \
        }
        unsigned int Maybe_unused j=0;
        if(0){}MINT_TABLES_LIST;
        free_string_array(&table_strings);
    }

    if(vb)
    {
        for(unsigned int i=0; i<MINT_TABLE_NUMBER; i++)
        {
            printf("Load table %u %s ? %s\n",
                   i,
                   idstrings[i],
                   Yesno(table_bool[i]));
        }
    }

    for(unsigned int i=0; i<MINT_TABLE_NUMBER; i++)
    {
        if(table_bool[i] == TRUE)
        {
            if(vb)
            {
                printf(
                    "Mint load-table function %u is %s() at %p, raw filename %s\n",
                    i,
                    funcstrings[i],
                    Cast_function_pointer(functions[i]),
                    filenames[i]
                    );
            }

            if(functions[i] != &MINT_load_no_grid)
            {
                if(functions[i](stardata,
                                when[i]) == FALSE)
                {
                    if(fail_on_error[i] == MINT_LOAD_FAIL_EXIT)
                    {
                        Exit_binary_c(BINARY_C_DATA_FAILED_TO_LOAD,
                                      "Failed to load MINT table %u \"%s\" %s%s\n",
                                      i,
                                      idstrings[i],
                                      filenames[i]!=NULL ? "in file " : "",
                                      filenames[i]!=NULL ? filenames[i] : "");
                    }
                    else if(fail_on_error[i] == MINT_LOAD_FAIL_WARN)
                    {
                        Printf("Warning : failed to load MINT table %u \"%s\" %s%s\n",
                               i,
                               idstrings[i],
                               filenames[i]!=NULL ? "in file " : "",
                               filenames[i]!=NULL ? filenames[i] : "");
                    }
                }
            }
            Dprint("Returned from mint load function %u\n",i);
        }
        else
        {
            if(vb)
            {
                printf(
                    "Mint skip loading of table %u %s\n",
                    i,
                    idstrings[i]
                    );
            }
        }
    }

    Dprint("loaded grid functions\n");
    return 0;
}
#endif // MINT
