#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef MINT

/*
 * Remesh MINT shells : warning, experimental!
 */

void MINT_remesh(struct stardata_t * const stardata Maybe_unused,
                 struct star_t * const star Maybe_unused)
{
    if(star->mint->shells == NULL || star->mint->nshells==0) return;

    const Boolean vb = FALSE;
    const Shell_index nshells_in = star->mint->nshells;

    if(vb)
    {
        printf("MINT remesh star %d, %d shells\n",
               star->starnum,
               nshells_in);

        double m = 0.0;
        Foreach_shell(shell)
        {
            m += shell->dm;
        }
        printf("Mass integral = %g should be %g\n",
               m,
               star->mass);
    }

    if(stardata->preferences->MINT_remesh == TRUE &&
       star->stellar_type != MASSLESS_REMNANT &&
       star->mint->nshells > stardata->preferences->MINT_minimum_nshells)
    {
        struct mint_shell_t * surface = &star->mint->shells[star->mint->nshells-1];
        struct mint_shell_t * centre = &star->mint->shells[0];

        Foreach_shell(star,shell,inner,outer,k)
        {
            if(vb)
                printf("remesh shell %d = %p (radius %g) : inner = %p, outer = %p\n",
                       k,
                       (void*)shell,
                       shell ? shell->radius : -1.0,
                       (void*)inner,
                       (void*)outer);

            if(shell != NULL &&
               shell->dm > -TINY)
            {
                if(outer != NULL)
                {
                    /*
                     * Interior (non-surface) shell : we
                     * can merge with a companion shell if
                     * this shell's mass is too low.
                     *
                     * We should merge with whichever neighbouring shell
                     * with least mass, to maintain maximum resolution,
                     * or - if both shells are the same - merge with
                     * the inner shell.
                     */
                    if(shell->dm < stardata->preferences->MINT_minimum_shell_mass
                       //&& outer != surface // deny merging with surface shell?
                        )
                    {
                        struct mint_shell_t * const with =
                            Fequal(star->mint->shells[k-1].dm,
                                   star->mint->shells[k+1].dm) ?
                            inner :
                            star->mint->shells[k-1].dm < star->mint->shells[k+1].dm ?
                            inner :
                            outer;

                        MINT_merge_shells(stardata,
                                          star,
                                          with,
                                          shell);

                        /*
                         * We merge with the inner or outer shell
                         */
                        if(with == outer)
                        {
                            if(vb)printf("remove outer : is surface? %s\n",Yesno(with==surface));

                            /*
                             * Outer shell removed
                             */
                            if(with == surface)
                            {
                                /* there is no outer shell */
                            }
                            else
                            {
                                memmove(outer,
                                        outer + 1,
                                        sizeof(struct mint_shell_t) * (star->mint->nshells - k - 1));
                            }
                        }
                        else
                        {
                            /*
                             * Inner shell removed
                             */
                            if(vb)printf("remove inner, is centre? %s\n",Yesno(with==centre));
                            memmove(inner,
                                    shell,
                                    sizeof(struct mint_shell_t) * (star->mint->nshells - k));
                        }
                        if(vb)printf("now have %d shells\n",star->mint->nshells);
                    }
                    surface = &star->mint->shells[star->mint->nshells-1];
                    centre = &star->mint->shells[0];
                }
                else if(inner == NULL)
                {
                    /*
                     * Non-centre shell.
                     */
                }
            }
        }
    }

    /*
     * Shrink shells array if necessary
     */
    if(star->mint->shells==0 || star->mint->shells == NULL)
    {
        Safe_free(star->mint->shells);
    }
    else
    {
        if(star->mint->nshells < nshells_in)
        {
            star->mint->shells =
                Realloc(star->mint->shells,
                        star->mint->nshells * sizeof(struct mint_shell_t));
        }
    }

    if(vb)printf("done remesh\n");
}


#endif // MINT
