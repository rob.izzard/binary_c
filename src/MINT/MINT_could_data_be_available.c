#include "../binary_c.h"
No_empty_translation_unit_warning;

Boolean MINT_could_data_be_available(struct stardata_t * const stardata,
                               struct star_t * const star,
                               const int variable_type)
{
    /*
     * Check if a variable type is available from MINT for
     * this star's stellar_type. If so, return TRUE, otherwise FALSE.
     */
    if(stardata->preferences->stellar_structure_algorithm == STELLAR_STRUCTURE_ALGORITHM_MINT &&
       stardata->store->MINT_generic_map[star->stellar_type] != NULL &&
       stardata->store->MINT_generic_map[star->stellar_type][variable_type] != -1)
    {
        return TRUE;
    }
    else
    {
        return FALSE;
    }
}
