#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef MINT

/*
 * Get the data on the Chebyshev coordinates for a CHeB star of
 * known mass and central hydrogen abundance.
 */

void MINT_CHeB_get_chebyshev_data(struct stardata_t * Restrict const stardata Maybe_unused,
                                  struct star_t * const star,
                                  double * const result_cheb)
{
    Interpolate(stardata->store->MINT_tables[MINT_TABLE_CHeB],
                ((double[]){
                    log10(star->mass),
                    star->mint->XHec
                }), /* log(mass), Xc */
                result_cheb,
                FALSE);
}

#endif // MINT
