#include "../binary_c.h"

No_empty_translation_unit_warning;

#if defined MINT && defined NUCSYN

void MINT_nucsyn_HeGB_pre(struct stardata_t * Restrict const stardata Maybe_unused,
                          struct star_t * Restrict const star)
{
    /*
     * Disable shellular nuclear burning
     */
    star->do_burn = FALSE;

  /*
     * Set the core abundances
     */
    const double mc_CO = star->core_mass[CORE_CO];
    if(Is_not_zero(star->menv))
    {
        Foreach_shell(shell)
        {
            if(shell->m < mc_CO)
            {
                shell->X[XH1] = 0.0;
                shell->X[XH2] = 0.0;
                shell->X[XHe3] = 0.0;
                shell->X[XHe4] = 0.0;
                shell->X[XO16] = 0.2;
                shell->X[XNe22] += shell->X[XN14];
                shell->X[XN14] = 0.0;
                shell->X[XC12] = 1.0 - nucsyn_totalX(shell->X);
            }
        }
    }
}
#endif // MINT && NUCSYN
