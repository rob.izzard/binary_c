#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef MINT

void MINT_clear_evolutionary_derivatives(struct stardata_t * const stardata Maybe_unused,
                                         struct star_t * const star)
{
    /*
     * Clear derivatives that will be used by MINT
     * as evolutionary parameters.
     */
#undef X
#define X(CODE,STRING,VAR,FUNC,TYPE,MINTCLEAR,ADDITION_METHOD)  \
    if(MINTCLEAR == TRUE)                                       \
    {                                                           \
        star->derivative[DERIVATIVE_STELLAR_##CODE] = 0.0;      \
    }
    STELLAR_DERIVATIVES_LIST;
#undef X
}

#endif // MINT
