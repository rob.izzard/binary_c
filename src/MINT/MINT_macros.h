#pragma once
#ifndef MINT_MACROS_H
#define MINT_MACROS_H
#include "../binary_c_cdict.h"

/*
 * Character for comments/headers
 * in data files
 */
#define MINT_HEADER_CHAR '#'



/*
 * Data table nan and inf checks?
 */
#define MINT_NAN_CHECKS
#define MINT_INF_CHECKS


/*
 * Import the MINT data tables
 */

/*
 * Import the MINT stellar structure functions
 */
#include "MINT_stellar_structure_functions.def"
#include "MINT_actions.h"

/*
 * Stellar structure parameters
 */

/*
 * MINT_MS_MINIMUM_HYDROGEN defined the terminal-age
 * main sequence (TAMS) when the star becomes a Hertzsprung
 * gap star.
 */
//#define MINT_MS_MINIMUM_HYDROGEN 1e-6
#define MINT_MS_MINIMUM_HYDROGEN 1e-12

/*
 * Hard limits on the number of shells.
 *
 * Note that nshells can be zero, in which case
 * no shells are allocated.
 */
#define MINT_HARD_MAX_NSHELLS 10000
#define MINT_HARD_MIN_NSHELLS 0

/*
 * Min and max shell mass
 */
#define MINT_DEFAULT_MINIMUM_SHELL_MASS 1e-6
#define MINT_DEFAULT_MAXIMUM_SHELL_MASS 1e-1

/*
 * MINT table
 */
enum {
    MINT_CACHE_TABLES_NO = 0,
    MINT_CACHE_TABLES_YES = 1,
    MINT_CACHE_TABLES_FORCE = 2
};


#endif // MINT_MACROS_H
