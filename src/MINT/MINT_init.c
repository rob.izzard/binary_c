#include "../binary_c.h"
No_empty_translation_unit_warning;

/*
 * Initialize MINT
 *
 * Here we
 * 1) Allocate memory
 * 2) Load data tables
 * 3) Initialize the stars
 */

#ifdef MINT

void MINT_init(struct stardata_t * const stardata)
{
    /*
     * Make check and make variable map
     */
    MINT_check_variables(stardata);
    MINT_make_generic_map(stardata);

    if(stardata->preferences->stellar_structure_algorithm ==
       STELLAR_STRUCTURE_ALGORITHM_MINT)
    {
        /*
         * Allocate MINT memory
         */
        MINT_alloc(stardata);

        /*
         * Initialize the MINT data if required
         */
        MINT_load_data(stardata);

        /*
         * Set up stars : we always need to do this
         */
        MINT_init_stars(stardata);
    }
}
#endif // MINT

/*
 * This is a dummy function that does nothing to prevent
 * ranlib errors about empty files on
 */
void Maybe_unused MINT_dummy_function(void){return;}
