#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef MINT
#include "MINT.h"

double MINT_initial_XHc(struct stardata_t * const stardata)
{
    /*
     * Determine the initial central hydrogen abundance
     * in the MINT models
     */
    if(stardata->store->MINT_initial_coordinate[MINT_TABLE_MS] < -TINY)
    {
        const size_t nperline =
            stardata->store->MINT_tables[MINT_TABLE_MS]->ndata +
            stardata->store->MINT_tables[MINT_TABLE_MS]->nparam;
        double * const pmin = stardata->store->MINT_tables[MINT_TABLE_MS]->data + MINT_MS_CENTRAL_HYDROGEN;
        double * const pmax = pmin + nperline * (1 + stardata->store->MINT_tables[MINT_TABLE_MS]->nlines);

        /*
         * While the abundance is increasing, keep looping through
         * lines of data. XHc will increase, but then when you
         * get to the next star (or the end of the table) it will decrease.
         *
         * The pointer p points to the hydrogen abundance at each line.
         * You should increment this by nperline.
         *
         * Note that pmax is the last value +1 line because p<pmax (not <=).
         */
        double * p = pmin;
        double prev_XHC0 = *p;
        while(p < pmax && *p >= prev_XHC0)
        {
            prev_XHC0 = *p;
            p += nperline;
        }

        /* we've overshot */
        p -= nperline;

        /* make sure we haven't wrapped back around the first value */
        p = Max(p, pmin);

        Dprint("Initial hydrogen abundance %g\n",*p);
        stardata->store->MINT_initial_coordinate[MINT_TABLE_MS] = *p;
    }

    /* hence return the central hydrogen */
    return stardata->store->MINT_initial_coordinate[MINT_TABLE_MS];
}
#endif //MINT
