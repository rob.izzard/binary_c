#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef MINT

/*
 * MINT function to calculate stellar structure
 * on the hydrogen-burning main sequence.
 *
 * Note that prevstar can be NULL, in which case
 * we cannot compare to the previous star to do (say)
 * rejuvenation.
 */

Stellar_type MINT_stellar_structure_MS(struct stardata_t * Restrict const stardata,
                                       struct star_t * const prevstar,
                                       struct star_t * const newstar,
                                       const Caller_id caller_id Maybe_unused)
{
    const Boolean vb = FALSE;
    struct mint_table_metadata_t * const metadata =
        stardata->store->MINT_tables[MINT_TABLE_MS]->metadata;

    /************************/
    /** Main sequence star **/
    /************************/
    Dprint("MS star : MINT algorithm\n");

    /* MS stars have no He, CO or GB core */
    newstar->core_mass[CORE_He] = 0.0;
    newstar->core_mass[CORE_CO] = 0.0;

    newstar->derivative[DERIVATIVE_STELLAR_He_CORE_MASS] = 0.0;
    newstar->derivative[DERIVATIVE_STELLAR_GB_CORE_MASS] = 0.0;
    newstar->derivative[DERIVATIVE_STELLAR_CO_CORE_MASS] = 0.0;

    /*
     * Calculate the maximum convective core
     * mass on the MS.
     * In low-mass stars this is zero.
     */
    const double fconvcore =
        effective_core_mass_fraction_of_MS_stars(newstar->mass,
                                                 stardata->common.metallicity);

    newstar->max_MS_core_mass = Max(newstar->max_MS_core_mass,
                                    newstar->mass * fconvcore);

    newstar->phase_start_mass = fabs(newstar->phase_start_mass);

    /*
     * Set stellar type
     */
    newstar->stellar_type = MAIN_SEQUENCE;

    /*
     * Booleans to test for data availability
     */
    const Boolean * const available = metadata->data_available;

    if(available == NULL)
    {
        Exit_binary_c(BINARY_C_ALLOC_FAILED,
                      "stardata->store->MINT_data_available[MINT_TABLE_MS] is NULL. This should not happen.");
    }

    /*
     * Map between MINT's wanted columns and columns available
     * in the data table
     */
    const unsigned int * const map = metadata->data_map;

    if(map == NULL)
    {
        Exit_binary_c(BINARY_C_ALLOC_FAILED,
                      "stardata->store->MINT_data_map[MINT_TABLE_MS] is NULL. This should not happen.");
    }

    /*
     * Space for interpolations
     */
    double * result, * result_chebyshev, * result_conv;
    if(stardata->persistent_data->MINT_MS_result == NULL)
    {
        result =
            stardata->persistent_data->MINT_MS_result =
            Malloc(MINT_result_size(MINT_TABLE_MS));
        result_chebyshev =
            stardata->persistent_data->MINT_MS_result_chebyshev =
            Malloc(MINT_result_size(MINT_TABLE_MS));
        result_conv =
            stardata->persistent_data->MINT_MS_result_conv =
            Malloc(MINT_result_size(MINT_TABLE_MS));

        if(result == NULL ||
           result_chebyshev == NULL ||
           result_conv == NULL)
        {
            Exit_binary_c(BINARY_C_ALLOC_FAILED,
                          "Failed to allocate memory for MINT MS interpolation tables");
        }
    }
    else
    {
        result =
            stardata->persistent_data->MINT_MS_result;
        result_chebyshev =
            stardata->persistent_data->MINT_MS_result_chebyshev;
        result_conv =
            stardata->persistent_data->MINT_MS_result_conv;
    }

    /*
     * Macro to get data from the interpolation, if it is
     * available, otherwise return 0.0. You can check
     * available[VARTYPE] yourself if you want to apply
     * a more complicated algorithm.
     */
#undef get_data
#define get_data(VARTYPE)                                           \
    ( available[(VARTYPE)]==TRUE ? result[map[(VARTYPE)]] : 0.0 )

    /*
     * Use table to look up stellar structure, e.g. dXH/dt,L,R,
     * as a function of phase start mass and central hydrogen abundance
     */
    Boolean rejuvenated = FALSE;
    Loop_forever
    {
        /*
         * Interpolate to find stellar structure
         */
        Interpolate(stardata->store->MINT_tables[MINT_TABLE_MS],
                    ((double[]){
                        log10(newstar->mass),
                        newstar->mint->XHc
                    }),
                    result,
                    FALSE);

#if defined DEBUG && DEBUG == 1
        {
            char * s = NULL;
            for(size_t i=0;
                i<MINT_result_size(MINT_TABLE_MS)/sizeof(double);
                i++)
            {
                reasprintf(&s,
                           "%s %zu=%g",
                           s == NULL ? "" : s,
                           i,
                           result[i]);
            }
            Dprint("Interpolate:%s\n",
                   s);
            Safe_free(s);
        }
#endif

        /*
         * Hence set the stellar properties
         */
        newstar->luminosity = exp10(get_data(MINT_MS_LUMINOSITY));
        newstar->radius = exp10(get_data(MINT_MS_RADIUS));
        newstar->mint->central_degeneracy = get_data(MINT_MS_CENTRAL_DEGENERACY);
        printf("MS degen %g\n",newstar->mint->central_degeneracy);
        newstar->mint->warning = get_data(MINT_MS_WARNING_FLAG);

        if(Is_zero(newstar->age))
        {
            newstar->rzams = newstar->radius;
        }

        /*
         * Zero burning derivatives
         */
        newstar->derivative[DERIVATIVE_STELLAR_CENTRAL_HYDROGEN] = 0.0;
        newstar->derivative[DERIVATIVE_STELLAR_CENTRAL_HELIUM] = 0.0;
        newstar->derivative[DERIVATIVE_STELLAR_CENTRAL_CARBON] = 0.0;

        /*
         * burn H to He
         */
        const double dxcdt = -exp10(get_data(MINT_MS_FIRST_DERIVATIVE_CENTRAL_HYDROGEN));
        newstar->derivative[DERIVATIVE_STELLAR_CENTRAL_HYDROGEN] = dxcdt;
        newstar->derivative[DERIVATIVE_STELLAR_CENTRAL_HELIUM] = -dxcdt;

        /*
         * Rate of change of convective core mass. We calculate this
         * by calculating the core mass at a slightly higher Xc, hence
         * we have dMc/dXc, then multiply by dXc/dt to calculate
         * dMc/dt.
         */
        {
            const double dXc = 1e-7;
            Interpolate(stardata->store->MINT_tables[MINT_TABLE_MS],
                        ((const double[]){
                            log10(newstar->mass),
                            newstar->mint->XHc + dXc
                        }),
                        result_conv,
                        FALSE);
            const double dmc = newstar->mass * result_conv[MINT_MS_CONVECTIVE_CORE_MASS_FRACTION] -
                newstar->convective_core_mass;
            newstar->convective_core_mass_time_derivative =
                dmc/dXc * dxcdt;
        }

        newstar->core_mass[CORE_He] = newstar->mass * get_data(MINT_MS_HELIUM_CORE_MASS_FRACTION);
        newstar->core_radius = 0.0;
        newstar->convective_core_mass = newstar->mass * get_data(MINT_MS_CONVECTIVE_CORE_MASS_OVERSHOOT_FRACTION);
        newstar->convective_core_radius = newstar->radius * get_data(MINT_MS_CONVECTIVE_CORE_RADIUS_OVERSHOOT_FRACTION);
        newstar->menv = newstar->mass * get_data(MINT_MS_CONVECTIVE_ENVELOPE_MASS_FRACTION);
        newstar->renv = newstar->radius * (1.0 - get_data(MINT_MS_CONVECTIVE_ENVELOPE_RADIUS_FRACTION));

        newstar->mint->mean_molecular_weight_star = get_data(MINT_MS_MEAN_MOLECULAR_WEIGHT_AVERAGE);
        newstar->mint->mean_molecular_weight_core = get_data(MINT_MS_MEAN_MOLECULAR_WEIGHT_CORE);

        /*
         * Check for core growth in hydrogen-burning stars:
         * if there is some, mix fresh hydrogen into the core
         * and restart calculation of the stellar structure.
         *
         * Note:
         *
         * We can only rejuvenate once and only if we have the
         * prevstar set.
         *
         * We must restart the stellar structure loop if we do rejuvenate,
         * because the central hydrogen abundance will have changed.
         */
        if(rejuvenated == FALSE &&
           prevstar != NULL &&
           stardata->preferences->MINT_MS_rejuvenation == TRUE &&
           MINT_rejuvenate_MS(stardata,
                              prevstar,
                              newstar) == TRUE)
        {
            Dprint("Rejuvenated : restart stellar evolution \n");
            rejuvenated = TRUE;
            continue;
        }

        newstar->max_MS_core_mass = Max(newstar->max_MS_core_mass,
                                        newstar->core_mass[CORE_He]);

        /*
         * To calculate the "age" we use Xc as a (linear) proxy
         */
        newstar->tms = MINT_MS_lifetime(stardata,newstar);
        const double Xc0 = MINT_initial_XHc(stardata);
        newstar->age = newstar->tms * (1.0 - newstar->mint->XHc / Xc0);
        newstar->epoch = stardata->model.time - newstar->age;

        newstar->moment_of_inertia_factor = get_data(MINT_MS_MOMENT_OF_INERTIA_FACTOR);

        /*
         * Compute E2
         * Mirouh et al. (2023)
         */
        newstar->E2 =
            available[MINT_MS_TIDAL_E2] ?
            get_data(MINT_MS_TIDAL_E2) :
            E2(stardata,newstar);
        newstar->E_Zahn = get_data(MINT_MS_TIDAL_E_FOR_LAMBDA);

        if(MINT_has_shells(newstar))
        {
            /*
             * Interpolate to get Chebyshev data for shells
             */
            MINT_MS_get_chebyshev_data(stardata,
                                       newstar,
                                       result_chebyshev);

            /*
             * Chebyshev coordinates
             */

            int n_cheb = 0;

            Foreach_shell(newstar,shell)
            {
                /*
                 * Set stellar shell data from Chebyshev data
                 */
                n_cheb = MINT_MS_set_chebyshev_data(stardata,
                                                    newstar,
                                                    shell,
                                                    result_chebyshev,
                                                    n_cheb,
                                                    map,
                                                    available);

                /*
                 * Identify instantly-mixed region from the
                 * convective core mass : note this needs
                 * updating to include the overshooting region
                 * and the surface convection zone
                 */
                shell->convective = (shell->m < newstar->convective_core_mass) ? TRUE : FALSE;

            } /* loop over shells */
        }
        break;
    }

    if(vb)
    {

        if(0)printf("MINT MS %d(%g,%g,%g) at t=%g = L=%g R=%g dXHc/dt=%g\n",
                    newstar->starnum,
                    log10(stardata->common.metallicity),
                    newstar->phase_start_mass,
                    newstar->mint->XHc,
                    stardata->model.time,
                    newstar->luminosity,
                    newstar->radius,
                    newstar->derivative[DERIVATIVE_STELLAR_CENTRAL_HYDROGEN]);

        printf("MINT h-burning time remaining %g Myr\n",
               -1e-6 * newstar->mint->XHc /newstar->derivative[DERIVATIVE_STELLAR_CENTRAL_HYDROGEN]
            );

        printf("AGE %g vs lifetime %g, XHc = %g\n",
               newstar->age,
               newstar->tms,
               newstar->mint->XHc
            );
        printf("Moment of inertia factor moment_of_inertia_factor=I/(MR^2) : star = %d, moment_of_inertia_factor2 = %g\n",
               newstar->starnum,
               newstar->moment_of_inertia_factor);
    }

    /*
    printf("XHC MINT t %20.12e Myr dt %20.12e Myr : H %g : tH = %g Myr\n",
           stardata->model.time,
           stardata->model.dt,
           newstar->mint->XHc,
           -1e-6 * newstar->mint->XHc /newstar->derivative[DERIVATIVE_STELLAR_CENTRAL_HYDROGEN]
        );
    */
#ifdef PRE_MAIN_SEQUENCE
    if(stardata->preferences->pre_main_sequence == TRUE)
    {
        const double f = preMS_radius_factor(newstar->mass, 1e6*newstar->age);
        newstar->radius *= f;
        newstar->PMS = Boolean_(f>1.00001);
        Dprint("preMS f=%g r=%g roche_radius=%g roche_radius_at_periastron=%g PMS? %d\n",
               f,
               newstar->radius,
               newstar->roche_radius,
               newstar->roche_radius_at_periastron,
               newstar->PMS);
    }
#endif // PRE_MAIN_SEQUENCE

    Dprint("MS radius %g\n",newstar->radius);

#ifdef BSE
    /*
     * BSE-MINT compatibility
     */
    newstar->TAMS_radius = newstar->radius;
    newstar->TAMS_luminosity = newstar->luminosity;
    newstar->tm = newstar->tms;
    Dprint("Set tm %g\n",newstar->tm);
    newstar->phase_start_mass = newstar->mass;
    newstar->core_stellar_type = MASSLESS_REMNANT;
#endif // BSE

    Dprint("return stellar type %d, XHC %g\n",
           newstar->stellar_type,
           newstar->mint->XHc);

    return newstar->stellar_type;
}


#endif // MINT
