#include "../binary_c.h"

No_empty_translation_unit_warning;

#if defined MINT && defined NUCSYN

void MINT_nucsyn_NS_pre(struct stardata_t * Restrict const stardata Maybe_unused,
                        struct star_t * Restrict const star)
{
    /*
     * Disable shellular nuclear burning
     */
    star->do_burn = FALSE;

    /*
     * Turn to neutrons
     */
    Foreach_shell(shell)
    {
        Clear_isotope_array(shell->X);
        shell->X[Xn] = 1.0;
    }
}
#endif // MINT && NUCSYN
