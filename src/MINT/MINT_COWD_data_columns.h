#pragma once
#ifndef MINT_COWD_DATA_COLUMNS_H
#define MINT_COWD_DATA_COLUMNS_H

/*
 * Define the MINT data columns.
 */

#include "MINT_data_columns_COWD_list.def"
#undef X

/* loading actions */
#define X(NAME) MINT_COWD_ACTION_##NAME,
enum { MINT_ACTIONS_LIST };
#undef X

/* MS parameter items list */
#define X(NAME,ACTION) MINT_COWD_##NAME,
enum { MINT_COWD_PARAMETER_ITEMS_LIST };
#undef X

/* parameter items actions */
#define X(NAME,ACTION) MINT_COWD_ACTION_##ACTION,
static int MINT_COWD_parameter_actions[] Maybe_unused = { MINT_COWD_PARAMETER_ITEMS_LIST };
#undef X

/* data items list */
#define X(NAME,ACTION) MINT_COWD_##NAME,
enum { MINT_COWD_DATA_ITEMS_LIST };
#undef X

/* data items actions */
#define X(NAME,ACTION) MINT_COWD_ACTION_##ACTION,
static int MINT_COWD_data_actions[] Maybe_unused = { MINT_COWD_DATA_ITEMS_LIST };
#undef X


#endif // MINT_COWD_DATA_COLUMMNS_H
