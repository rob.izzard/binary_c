#include "../binary_c.h"
No_empty_translation_unit_warning;

/*
 * MINT derivative check functions
 *
 * Return TRUE if check is ok, FALSE if it fails.
 */

#ifdef MINT
Pure_function
Boolean MINT_derivative_check_stellar_central_hydrogen(struct stardata_t * const stardata Maybe_unused,
                                                       void * const data Maybe_unused,
                                                       const double previous_value Maybe_unused,
                                                       const double value,
                                                       const double derivative Maybe_unused,
                                                       const double dt Maybe_unused,
                                                       const Derivative nd Maybe_unused,
                                                       const Derivative_group derivative_group Maybe_unused)
{
    /*
     * central hydrogen abundance cannot be outside the
     * range zero to one
     */
    const struct star_t * const star = (struct star_t *)data;
    const Boolean x = (star->mint->XHc < -TINY || star->mint->XHc > 1.0 + TINY ) ? FALSE : TRUE;
    Dprint("central H checkfunc : return %s%s%s -> %g\n",
           Colourtruefalse(x),
           value
        );
    return x;
}

Pure_function
Boolean MINT_derivative_check_stellar_central_helium(struct stardata_t * const stardata Maybe_unused,
                                                     void * const data Maybe_unused,
                                                     const double previous_value Maybe_unused,
                                                     const double value,
                                                     const double derivative Maybe_unused,
                                                     const double dt Maybe_unused,
                                                     const Derivative nd Maybe_unused,
                                                     const Derivative_group derivative_group Maybe_unused)
{
    /*
     * central helium abundance cannot be outside the
     * range zero to one
     */
    const struct star_t * const star = (struct star_t *)data
        ;
    const Boolean x = (star->mint->XHec < -TINY || star->mint->XHec > 1.0 + TINY ) ? FALSE : TRUE;
    Dprint("REJECT central He checkfunc : return %s%s%s -> %g (<0 ? %s : >1 ? %s)\n",
           Colourtruefalse(x),
           value,
           Yesno(star->mint->XHec < -TINY),
           Yesno(star->mint->XHec > 1.0 + TINY)
        );
    return x;
}

Pure_function
Boolean MINT_derivative_check_stellar_central_carbon(struct stardata_t * const stardata Maybe_unused,
                                                     void * const data Maybe_unused,
                                                     const double previous_value Maybe_unused,
                                                     const double value,
                                                     const double derivative Maybe_unused,
                                                     const double dt Maybe_unused,
                                                     const Derivative nd Maybe_unused,
                                                     const Derivative_group derivative_group Maybe_unused)
{
    /*
     * central carbon abundance cannot be outside the
     * range zero to one
     */
    const struct star_t * const star = (struct star_t *)data
        ;
    const Boolean x = (star->mint->XCc < -TINY || star->mint->XCc > 1.0 + TINY ) ? FALSE : TRUE;
    Dprint("REJECT central C checkfunc : return %s%s%s -> %g (<0 ? %s : >1 ? %s)\n",
           Colourtruefalse(x),
           value,
           Yesno(star->mint->XCc < -TINY),
           Yesno(star->mint->XCc > 1.0 + TINY)
        );
    return x;
}

Pure_function
Boolean MINT_derivative_check_stellar_central_oxygen(struct stardata_t * const stardata Maybe_unused,
                                                     void * const data Maybe_unused,
                                                     const double previous_value Maybe_unused,
                                                     const double value,
                                                     const double derivative Maybe_unused,
                                                     const double dt Maybe_unused,
                                                     const Derivative nd Maybe_unused,
                                                     const Derivative_group derivative_group Maybe_unused)
{
    /*
     * central oxygen abundance cannot be outside the
     * range zero to one
     */
    const struct star_t * const star = (struct star_t const *)data
        ;
    const Boolean x = (star->mint->XOc < -TINY || star->mint->XOc > 1.0 + TINY ) ? FALSE : TRUE;
    Dprint("REJECT central O checkfunc : return %s%s%s -> %g (<0 ? %s : >1 ? %s)\n",
           Colourtruefalse(x),
           value,
           Yesno(star->mint->XOc < -TINY),
           Yesno(star->mint->XOc > 1.0 + TINY)
        );
    return x;
}


Pure_function
Boolean MINT_derivative_check_stellar_central_temperature(struct stardata_t * const stardata Maybe_unused,
                                                          void * const data Maybe_unused,
                                                          const double previous_value Maybe_unused,
                                                          const double value Maybe_unused,
                                                          const double derivative Maybe_unused,
                                                          const double dt Maybe_unused,
                                                          const Derivative nd Maybe_unused,
                                                          const Derivative_group derivative_group Maybe_unused)
{
    /*
     * central temperature must exceed 1e5 K
     * (we have no data below this temperature)
     */
    const struct star_t * const star = (struct star_t *)data;
    star->mint->central_temperature = Max(star->mint->central_temperature,1e5);
    return TRUE;
}

Pure_function
Boolean MINT_derivative_check_stellar_central_degeneracy(struct stardata_t * const stardata Maybe_unused,
                                                         void * const data Maybe_unused,
                                                         const double previous_value Maybe_unused,
                                                         const double value Maybe_unused,
                                                         const double derivative Maybe_unused,
                                                         const double dt Maybe_unused,
                                                         const Derivative nd Maybe_unused,
                                                         const Derivative_group derivative_group Maybe_unused)
{
    /*
     * central degeneracy must not change too much
     * in a timestep, called only for HG and GB stars
     * at present.
     *
     * Note: degeneracy moves through zero, so check
     * that the change is small and that the value > some minimum)
     */
    const double delta_abs = Abs_diff(value, previous_value);
    const double delta_pc = Abs_percent_diff(value, previous_value);
    const Boolean ret = fabs(previous_value) < 0.01 || delta_pc < 1.0; /* limit to 1% */

    if(0)fprintf(stderr,"Check degen was %g want %g : delta %% %g, dt = %g, good? %s\n",
            previous_value,
            value,
            delta_pc,
            dt,
            Yesno(ret));

    if(ret == FALSE && dt < 1.0) Flexit;
    return ret;
}

#endif //MINT
