#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef MINT

#define Tmp_free(P) Safe_free(tmpstore->P)

void MINT_free_tmpstore(struct tmpstore_t * const tmpstore Maybe_unused)
{
    /*
     * Free memory allocated for MINT in the
     * binary_c store.
     */
}

#endif//MINT
