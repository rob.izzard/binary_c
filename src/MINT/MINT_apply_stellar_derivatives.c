#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef MINT

Boolean MINT_apply_stellar_derivatives(struct stardata_t * const stardata,
                                       const double dt,
                                       const Boolean can_reject Maybe_unused,
                                       const Boolean can_reject_and_shorten Maybe_unused)
{
    Foreach_evolving_star(star)
    {
        if(star->mint != NULL)
        {
            Dprint("MINT derivs apply star %d\n",star->starnum);
            /*
             * Burn central hydrogen to helium
             */
            if(ON_MAIN_SEQUENCE(star->stellar_type) &&
               apply_derivative(stardata,
                                &star->mint->XHc,
                                NULL,
                                star->derivative,
                                dt,
                                DERIVATIVE_STELLAR_CENTRAL_HYDROGEN,
                                DERIVATIVES_GROUP_STELLAR,
                                star,
                                &MINT_derivative_check_stellar_central_hydrogen) == FALSE)
            {
                Dprint("MS H burn derivative fail\n");
                if(can_reject && can_reject_and_shorten)
                {
                    stardata->model.reject_shorten_timestep = REJECT_STELLAR_CENTRAL_HYDROGEN;
                    return FALSE;
                }
                else
                {
                    /*
                     * We're running at the minimum timestep, so just
                     * assume all hydrogen is burnt if it's negative.
                     */
                    star->mint->XHc = 0.0;
                }
            }

            if(star->stellar_type > MAIN_SEQUENCE &&
               star->stellar_type < HeWD &&
               apply_derivative(stardata,
                                &star->mint->XHec,
                                NULL,
                                star->derivative,
                                dt,
                                DERIVATIVE_STELLAR_CENTRAL_HELIUM,
                                DERIVATIVES_GROUP_STELLAR,
                                star,
                                &MINT_derivative_check_stellar_central_helium) == FALSE)
            {
                Dprint("MS He burn derivative fail\n");
                if(can_reject && can_reject_and_shorten)
                {
                    stardata->model.reject_shorten_timestep = REJECT_STELLAR_CENTRAL_HELIUM;
                    return FALSE;
                }
                else
                {
                    /*
                     * We're running at the minimum timestep, so just
                     * assume all helium is burnt if it's negative.
                     */
                    star->mint->XHec = 0.0;
                }
            }

            if(apply_derivative(stardata,
                                &star->mint->XCc,
                                NULL,
                                star->derivative,
                                dt,
                                DERIVATIVE_STELLAR_CENTRAL_CARBON,
                                DERIVATIVES_GROUP_STELLAR,
                                star,
                                &MINT_derivative_check_stellar_central_carbon) == FALSE)
            {
                Dprint("MS C burn derivative fail\n");
                if(can_reject && can_reject_and_shorten)
                {
                    stardata->model.reject_shorten_timestep = REJECT_STELLAR_CENTRAL_CARBON;
                    return FALSE;
                }
                else
                {
                    /*
                     * We're running at the minimum timestep, so just
                     * assume all carbon is burnt if it's negative.
                     */
                    star->mint->XCc = 0.0;
                }
            }

            if(apply_derivative(stardata,
                                &star->mint->XOc,
                                NULL,
                                star->derivative,
                                dt,
                                DERIVATIVE_STELLAR_CENTRAL_OXYGEN,
                                DERIVATIVES_GROUP_STELLAR,
                                star,
                                &MINT_derivative_check_stellar_central_oxygen) == FALSE)
            {
                Dprint("MS O burn derivative fail\n");
                if(can_reject && can_reject_and_shorten)
                {
                    stardata->model.reject_shorten_timestep = REJECT_STELLAR_CENTRAL_OXYGEN;
                }
                else
                {
                    /*
                     * We're running at the minimum timestep, so just
                     * assume all oxygen is burnt if it's negative.
                     */
                    star->mint->XOc = 0.0;
                }
            }


            if((star->stellar_type == HG ||
                star->stellar_type == GIANT_BRANCH))
            {
                Dprint("Apply deriv eta %g was %g should be %g\n",
                       star->derivative[DERIVATIVE_STELLAR_CENTRAL_DEGENERACY],
                       star->mint->central_degeneracy,
                       star->mint->central_degeneracy +
                       star->derivative[DERIVATIVE_STELLAR_CENTRAL_DEGENERACY] * dt);
            }

            if((star->stellar_type == HG ||
                star->stellar_type == GIANT_BRANCH) &&
               apply_derivative(stardata,
                                &star->mint->central_degeneracy,
                                NULL,
                                star->derivative,
                                dt,
                                DERIVATIVE_STELLAR_CENTRAL_DEGENERACY,
                                DERIVATIVES_GROUP_STELLAR,
                                star,
                                MINT_derivative_check_stellar_central_degeneracy) == FALSE)
            {
                Dprint("Central degeneracy failed\n");
                stardata->model.reject_shorten_timestep = REJECT_STELLAR_CENTRAL_DEGENERACY;
                return FALSE;
            }

            if(star->stellar_type == COWD &&
               apply_derivative(stardata,
                                &star->mint->central_temperature,
                                NULL,
                                star->derivative,
                                dt,
                                DERIVATIVE_STELLAR_CENTRAL_TEMPERATURE,
                                DERIVATIVES_GROUP_STELLAR,
                                star,
                                NULL) == FALSE)
            {
                Dprint("COWD dT/dt integration failed\n");
                stardata->model.reject_shorten_timestep = REJECT_STELLAR_CENTRAL_TEMPERATURE;
                return FALSE;
            }
            else
            {
                star->mint->central_temperature = Max(1e5,
                                                      star->mint->central_temperature);
            }
        }
    }
    return TRUE;
}
#endif // MINT
