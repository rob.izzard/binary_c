#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef MINT
#include "MINT.h"
#include "MINT_load_grid.h"

#define vbprintf(...)                           \
    if(vb ||                                    \
       stardata->preferences->MINT_filename_vb) \
    {                                           \
        printf(__VA_ARGS__);                    \
    }

void MINT_update_table(struct stardata_t * const stardata,
                       const struct data_table_t * const data_table,
                       const struct data_table_defer_instructions_t * const instructions,
                       const struct data_table_analysis_t * const analysis,
                       struct mint_table_metadata_t * metadata,
                       const int vb)
{
    struct string_array_t * string_array = new_string_array(0);
    double * const table = data_table->data;
    Boolean * set = Malloc(sizeof(Boolean) * metadata->nperline);
    const Boolean cleanup = stardata->preferences->MINT_data_cleanup;
    size_t relative_data_line_number = 0;
    size_t prealloc = 0;
    size_t data_line_number;

    if(set == NULL)
    {
        Exit_binary_c(BINARY_C_MALLOC_FAILED,
                      "Could not allocate memory for set in MINT table updater.");
    }

    /*
     * If we are given instructions, seek to the lower
     * line number. Note, this requires that the stream
     * is a FILE type, which in MINT it is.
     */
    if(instructions)
    {
        const size_t offset =
            analysis->data_start_offset_bytes +
            analysis->data_line_offset_bytes[instructions->lower_line_number];
        fseek(metadata->stream->fp,
              offset,
              SEEK_SET);
        data_line_number = instructions->lower_line_number;
    }
    else
    {
        restart_stream(stardata,
                       metadata->stream);
        data_line_number = 0;
    }
    metadata->lower_line_number = data_line_number;

    /*
     * Preset column ranges for speed
     */
    int ** parameter_ranges = Malloc(sizeof(int*) * metadata->n_MINT_parameter_names);
    if(parameter_ranges == NULL)
    {
        Exit_binary_c(BINARY_C_MALLOC_FAILED,
                      "Could not allocate memory for parameter_ranges in MINT table updater.");
    }
    for(size_t i=0; i<metadata->n_MINT_parameter_names; i++)
    {
        const int range[2] = MINT_Column_range_with_string(metadata->MINT_parameter_names[i]);
        parameter_ranges[i] = Malloc(sizeof(int)*2);
        if(parameter_ranges[i] == NULL)
        {
            Exit_binary_c(BINARY_C_MALLOC_FAILED,
                          "Could not allocate memory for parameter_ranges[%zu] in MINT table updater.",
                          i);
        }
        memcpy(parameter_ranges[i],range,sizeof(int)*2);
    }

    int ** datatype_ranges = Malloc(sizeof(int*) * metadata->n_MINT_datatype_names);
    if(datatype_ranges == NULL)
    {
        Exit_binary_c(BINARY_C_MALLOC_FAILED,
                      "Could not allocate memory for datatype_ranges in MINT table updater.");
    }
    for(size_t i=0; i<metadata->n_MINT_datatype_names; i++)
    {
        const int range[2] = MINT_Column_range_with_string(metadata->MINT_datatype_names[i]);
        datatype_ranges[i] = Malloc(sizeof(int)*2);
        if(datatype_ranges[i] == NULL)
        {
            Exit_binary_c(BINARY_C_MALLOC_FAILED,
                          "Could not allocate memory for datatype_ranges[%zu] in MINT table updater.",
                          i);
        }
        memcpy(datatype_ranges[i],range,sizeof(int)*2);
    }


    /*
     * Check if we can use cached data: we only do this
     * if we're loading the entire table, if we have instructions
     * to load only a few lines it's not worth the effort.
     */
    Boolean found_data_cache = FALSE;
    Boolean output_data_cache = FALSE;

    printf("MINT cache tables %d (file %s)\n",
           stardata->preferences->MINT_cache_tables,
           metadata->filename);
    if(!instructions &&
       (stardata->preferences->MINT_cache_tables == MINT_CACHE_TABLES_YES ||
        stardata->preferences->MINT_cache_tables == MINT_CACHE_TABLES_FORCE))
    {
        /*
         * We should cache table data
         */
        if(metadata->data_cache_filename == NULL)
        {
            if(asprintf(&metadata->data_cache_filename,
                        "%s.cache",
                        metadata->filename)==0)
            {
                Exit_binary_c(BINARY_C_ALLOC_FAILED,
                              "Failed to asprintf data_cache_filename in MINT\n");
            }
        }

        if(stardata->preferences->MINT_cache_tables == MINT_CACHE_TABLES_YES &&
           check_file_exists(metadata->data_cache_filename))
        {
            /*
             * Found cached data file: use it.
             */
            found_data_cache = TRUE;
        }
        else
        {
            /*
             * We should output the data cache or,
             * if it exists, rebuild it.
             */
            output_data_cache = TRUE;
        }
    }

    if(output_data_cache == TRUE)
    {
        /*
         * Open data cache for output
         */
        metadata->data_cache = fopen(metadata->data_cache_filename,"w");
        found_data_cache = FALSE;
    }
    else if(found_data_cache == TRUE)
    {
        /*
         * Read data from the cache
         */
        metadata->data_cache = fopen(metadata->data_cache_filename,"r");
        const size_t n_read = fread(table,
                                    sizeof(double),
                                    metadata->nperline*metadata->nl,
                                    metadata->data_cache);
        if(n_read != metadata->nperline*metadata->nl)
        {
            Exit_binary_c(BINARY_C_FILE_READ_ERROR,
                          "Failed to read cached data from %s (read %zu, should be %zu)\n",
                          metadata->data_cache_filename,
                          n_read,
                          metadata->nperline * metadata->nl);
        }
    }

    if(found_data_cache == FALSE)
    {
        /*
         * We must load in the data for the first time or to
         * refresh the cache
         */
        char * line = NULL;
        Boolean brk = FALSE;

        while(brk == FALSE &&
              relative_data_line_number < metadata->nl)
        {
            /*
             * Decide here whether we want to skip to the
             * next newline, or read in the line
             */
            line = file_splitline(stardata,
                                  metadata->stream->fp,
                                  &line,
                                  string_array,
                                  0,
                                  ' ',
                                  prealloc,
                                  0);

            prealloc = Max((size_t)string_array->n,
                           string_array->nalloc);

            if(string_array->n <= -1)
            {
                Exit_binary_c(BINARY_C_DATA_FAILED_TO_LOAD,
                              "Found string_array->n < 0 when trying to load data from %s line %zu (table %s)\n",
                              metadata->filename,
                              data_line_number,
                              metadata->header_string);
            }
            else if(string_array->strings[0][0] != MINT_HEADER_CHAR)
            {
                /*
                 * Grid data line
                 */
                if(vb>1)
                {
                    vbprintf("line %zu/%zu got string_array->n = %zd : 0=%s, 1=%s, 2=%s, 3=%s ...\n",
                             data_line_number+1,
                             metadata->nl,
                             string_array->n,
                             string_array->n >= 1 ? string_array->strings[0] : "",
                             string_array->n >= 2 ? string_array->strings[1] : "",
                             string_array->n >= 3 ? string_array->strings[2] : "",
                             string_array->n >= 4 ? string_array->strings[3] : ""
                        );
                }

                if(string_array->n >= 0)
                {
                    struct double_array_t * double_array =
                        string_array_to_double_array(string_array,NAN);

#ifdef MINT_NAN_CHECKS
                    if(check_double_array_for_nan(double_array) == TRUE)
                    {
                        if(cleanup == TRUE)
                        {
                            for(ssize_t i=0; i<string_array->n; i++)
                            {
                                if(isnan(double_array->doubles[i]))
                                {
                                    double_array->doubles[i] = 0.0;
                                }
                            }
                        }
                        else
                        {
                            Exit_binary_c(BINARY_C_EXIT_NAN,
                                          "Found NaN when loading line %zu from %s, table %s, first nan at index %zd\n",
                                          data_line_number,
                                          metadata->filename,
                                          metadata->header_string,
                                          first_nan_in_double_array(double_array)
                                );
                        }
                    }
#endif // MINT_NAN_CHECKS
#ifdef MINT_INF_CHECKS
                    if(check_double_array_for_inf(double_array) == TRUE)
                    {
                        if(cleanup == TRUE)
                        {
                            for(ssize_t i=0; i<string_array->n; i++)
                            {
                                if(isnan(double_array->doubles[i]))
                                {
                                    double_array->doubles[i] = 0.0;
                                }
                            }
                        }
                        else
                        {
                            Exit_binary_c(BINARY_C_EXIT_INF,
                                          "Found inf when loading line %zu from %s, table %s, first inf at index %zd\n",
                                          data_line_number,
                                          metadata->filename,
                                          metadata->header_string,
                                          first_inf_in_double_array(double_array));
                        }
                    }
#endif // MINT_INF_CHECKS

                    /*
                     * Pointer to the start of this data line
                     *
                     * Note that this is offset by the relative_data_line_number
                     * because we may be saving a chunk of the table. If we're
                     * saving the whole table then relative_data_line_number ==
                     * data_line_number anyway.
                     */
                    double * const line_start = table + relative_data_line_number * metadata->nperline;
                    int offset = 0;

                    /*
                     * Make sure set[] is all FALSE
                     */
                    for(size_t i=0; i<metadata->nperline; i++)
                    {
                        set[i] = FALSE;
                    }

                    /*
                     * Copy in parameter values
                     */
                    for(size_t i=0; i<metadata->n_MINT_parameter_names; i++)
                    {
                        int * const range = parameter_ranges[i];

                        vbprintf("parameter %s : range %d to %d\n",
                                 metadata->MINT_parameter_names[i],
                                 range[0],
                                 range[1]);
                        for(int j=range[0]; j<=range[1]; j++)
                        {
                            const int c = offset + j - range[0];
                            const double x = MINT_data_filter(double_array->doubles[j],
                                                              metadata->parameter_actions[i]);


                            vbprintf("Read param %zu at file column %d, interpolation table column %d (action %s on %g) : %g\n",
                                     i,
                                     j,
                                     c,
                                     MINT_action_strings[metadata->parameter_actions[i]],
                                     double_array->doubles[j],
                                     x);

                            set[c] = TRUE;

                            /*
                             * Set the data in the big interpolation table,
                             * using the MINT_data_filter to apply functions
                             * to the data, if required.
                             */
                            *(line_start + i + j - range[0]) = x;

                            /*
                             * Save the column mapping if first time
                             */
                            if(relative_data_line_number == 0)
                            {
                                vbprintf("Set (param) MINT to file map [%d] = %d (max %d)\n",
                                         j,
                                         c,
                                         (int)(metadata->nparam + metadata->ndata));

                                metadata->MINT_to_file_map[c] = j;
                                metadata->file_to_MINT_map[j] = c;
                            }
                        }
                        offset += range[1] - range[0] + 1;
                    }

                    /*
                     * Save the start of data offset
                     */
                    const int data_start_offset = offset;

                    /*
                     * Copy in data values from MINT's table to
                     * our interpolation table
                     */
                    for(size_t i=0; i<metadata->n_MINT_datatype_names; i++)
                    {
                        /*
                         * Get the column range.
                         * If the first column is -1, we have no data.
                         */
                        int * const range = datatype_ranges[i];
                        const Boolean have_data = Boolean_(range[0] != -1);

                        vbprintf("data item %s : have data? %s : range in file %d to %d : map to %u\n",
                                 metadata->MINT_datatype_names[i],
                                 Yesno(have_data),
                                 range[0],
                                 range[1],
                                 metadata->data_map[i]);

                        /*
                         * Check for missing data and add to the
                         * stack of strings if we do (to warn the user)
                         */
                        if(relative_data_line_number == 0 &&
                           have_data == FALSE)
                        {
                            /*
                             * Realloc for each string is slow,
                             * but hopefully this will not be required often.
                             *
                             * Also note, we only perform this check for line 0.
                             */
                            _push_string(metadata->missing_from_file_strings,
                                         metadata->MINT_datatype_names[i]);
                        }

                        /*
                         * If we have data in the file,
                         * set it in the interpolation table
                         */
                        offset = data_start_offset + metadata->data_map[i];
                        if(have_data == TRUE)
                        {
                            for(int j=range[0]; j<=range[1]; j++)
                            {
                                /*
                                 * Calculate the column at which we
                                 * should set data in the interpolation table
                                 */
                                const int c = offset + j - range[0];

                                /*
                                 * Calculate the value that should go
                                 * into the interpolation table
                                 */
                                const double x =
                                    have_data == FALSE
                                    ? 0.0
                                    : MINT_data_filter(double_array->doubles[j],
                                                       metadata->data_actions[i]);

                                vbprintf("Read data from %s relative line %zu/%zu (% 5.1f%%), file column %d, interpolation table column (%d+%u+%d = %d)/%u (action %s on %g) : %g\n",
                                         metadata->filename,
                                         relative_data_line_number,
                                         metadata->nl,
                                         (100.0*relative_data_line_number)/(1.0*metadata->nl),
                                         j,
                                         data_start_offset,
                                         metadata->data_map[i],
                                         j-range[0],
                                         c,
                                         metadata->nperline,
                                         MINT_action_strings[metadata->data_actions[i]],
                                         double_array->doubles[j],
                                         x);

                                /*
                                 * Save the column mapping
                                 */
                                if(relative_data_line_number == 0)
                                {
                                    if(vb)
                                    {
                                        printf("Set (data) MINT to file map [%d] = %d (max %d)\n",j,c,(int)(metadata->nparam + metadata->ndata));
                                        fflush(NULL);
                                    }
                                    metadata->MINT_to_file_map[c] = j;
                                    metadata->file_to_MINT_map[j] = c;
                                }

                                set[c] = TRUE;

                                /*
                                 * Set the data in the big interpolation table,
                                 * using the MINT_data_filter to apply functions
                                 * to the data, if required.
                                 */
                                *(line_start + c) = x;

#ifdef MINT_NAN_CHECKS
                                if(isnan(x))
                                {
                                    Exit_binary_c(BINARY_C_EXIT_NAN,
                                                  "Found NaN when loading line %zu of %s column %d, to go in interpolation table column %d : raw data %g, columns %d-%d, MINT action \"%s\"\n",
                                                  data_line_number,
                                                  metadata->filename,
                                                  j+1,
                                                  c,
                                                  double_array->doubles[j],
                                                  range[0],
                                                  range[1],
                                                  MINT_action_strings[metadata->data_actions[i]]
                                        );
                                }
#endif // MINT_NAN_CHECKS
#ifdef MINT_INF_CHECKS
                                if(isinf(x))
                                {
                                    Exit_binary_c(BINARY_C_EXIT_NAN,
                                                  "Found inf when loading line %zu of %s column %d, to go in interpolation table column %d : raw data %g, columns %d-%d, MINT action \"%s\"\n",
                                                  data_line_number,
                                                  metadata->filename,
                                                  j+1,
                                                  c,
                                                  double_array->doubles[j],
                                                  range[0],
                                                  range[1],
                                                  MINT_action_strings[metadata->data_actions[i]]
                                        );
                                }
#endif // MINT_INF_CHECKS
                            }
                        }
                    }

                    /*
                     * Check all data has been set
                     */
                    for(size_t i=0; i<metadata->nperline; i++)
                    {
                        if(set[i] == FALSE)
                        {
                            Exit_binary_c(BINARY_C_DATA_FAILED_TO_LOAD,
                                          "Data load fail : item %zu on line %zu is not set\n",
                                          i,
                                          data_line_number);
                        }
                    }

                    /* free memory */
                    free_double_array(&double_array);
                }
                relative_data_line_number++;
                data_line_number++;
            }

            /* line is no longer required: free the data */
            Safe_free(line);

            _show_string_stack;
            _free_string_stack;
        } /* loop over lines */


        metadata->upper_line_number = data_line_number-1;

        /*
         * free memory
         */
        Dprint("free strings %p -> %p\n",(void*)string_array,(void*)string_array->strings);
        free_string_array(&string_array);
    }

    /*
     * Output to data cache?
     */
    if(output_data_cache == TRUE)
    {
        printf("WRITE TABLE\n");
        if(fwrite(table,
                  sizeof(double),
                  metadata->nperline * metadata->nl,
                  metadata->data_cache) != metadata->nperline * metadata->nl)
        {
            Exit_binary_c(BINARY_C_WRITE_FAILED,
                          "Failed to write %zu doubles to %s\n",
                          metadata->nperline * metadata->nl,
                          metadata->data_cache_filename);
        }
    }

    /*
     * close data cache file
     */
    if(output_data_cache == TRUE ||
       found_data_cache == TRUE)
    {
        fclose(metadata->data_cache);
    }

    for(size_t i=0; i<metadata->n_MINT_parameter_names; i++)
    {
        Safe_free(parameter_ranges[i]);
    }
    for(size_t i=0; i<metadata->n_MINT_datatype_names; i++)
    {
        Safe_free(datatype_ranges[i]);
    }
    Safe_free(datatype_ranges);
    Safe_free(parameter_ranges);
    Safe_free(set);

    vbprintf("done MINT_update_table\n");
}


#endif // MINT
