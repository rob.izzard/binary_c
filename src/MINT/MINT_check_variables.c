#include "../binary_c.h"
No_empty_translation_unit_warning;



#ifdef MINT
void MINT_check_variables(struct stardata_t * const stardata)
{

#undef X
#define X(NAME) #NAME,
    const char * generic_strings[] = { MINT_DATA_TYPES_LIST };
#undef X
    const size_t n_generic_strings = Array_size(generic_strings);

    /*
     * Check that the various generic data type lists
     * are complete list and self-consistent.
     */
    {
        /*
         * make a list of all names in the stellar type files
         * including parameters and data items
         */
#define X(NAME,ACTION) #NAME,
        const char * _stellar_type_strings[] = {
            MINT_MS_PARAMETER_ITEMS_LIST
            MINT_GB_PARAMETER_ITEMS_LIST
            MINT_CHeB_PARAMETER_ITEMS_LIST
            MINT_COWD_PARAMETER_ITEMS_LIST
            MINT_MS_DATA_ITEMS_LIST
            MINT_GB_DATA_ITEMS_LIST
            MINT_CHeB_DATA_ITEMS_LIST
            MINT_COWD_DATA_ITEMS_LIST
        };
#undef X
        const size_t n_stellar_type_strings = Array_size(_stellar_type_strings);

        for(size_t i=0; i<n_stellar_type_strings; i++)
        {
            Boolean found = FALSE;
            for(size_t j=0; found==FALSE && j<n_generic_strings; j++)
            {
                found = Strings_equal(_stellar_type_strings[i],
                                      generic_strings[j]);
            }
            if(found == FALSE)
            {
                Exit_binary_c(BINARY_C_MINT_ERROR,
                              "Data type %s is defined as a stellar data type but not a generic data type: please add it to MINT_generic_data_types.def.",
                              _stellar_type_strings[i]);
            }
        }

        for(size_t j=0; j<n_generic_strings; j++)
        {
            Boolean found = FALSE;
            for(size_t i=0; found==FALSE && i<n_stellar_type_strings; i++)
            {
                found = Strings_equal(_stellar_type_strings[i],
                                      generic_strings[j]);
            }
            if(found == FALSE)
            {
                Exit_binary_c(BINARY_C_MINT_ERROR,
                              "Data type %s is defined as a generic type but not a stellar data type: please add it to MINT_data_columns_<stellar_type>_list.def.",
                              generic_strings[j]);
            }
        }
    }
}
#endif // MINT
