#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef MINT

/*
 * Stellar evolution based on the MINT algorithm.
 *
 * Usually, we are evolving from oldstar to newstar,
 * so we can check oldstar for the previous structure.
 *
 * However, oldstar *can* be NULL, in which case
 * we simply update the structure in newstar.
 *
 * While we are developing MINT, you can choose
 * whether to call the MINT function before or
 * after BSE, and whether to use the BSE function
 * for each stellar type.
 *
 * Please see
 * MINT_stellar_structure_functions.def
 * for instructions.
 *
 */

int MINT_stellar_structure(struct stardata_t * const stardata,
                           struct star_t * const oldstar,
                           struct star_t * const newstar,
                           const Caller_id caller_id Maybe_unused)
{
    const Boolean vb = FALSE;

    /*
     * Trim shells, e.g. because of mass loss
     */
    if(MINT_has_shells(newstar))
    {
        MINT_trim_shells(stardata,
                         newstar);
    }

    if(vb)
        printf("MINT_stellar_structure: model %d time %30.20e (stellar types %d %d)\n",
               stardata->model.model_number,
               stardata->model.time,
               oldstar ? oldstar->stellar_type : -1,
               newstar ? newstar->stellar_type : -1);

    if(oldstar != NULL)
    {
        /*
         * Check for fuel exhaustion at each evolutionary phase
         */
        if(oldstar->stellar_type < HERTZSPRUNG_GAP)
        {
            if(vb)
                printf("AGE %g %g \n",
                         oldstar->mint->XHc,
                         MINT_MS_MINIMUM_HYDROGEN);


            if(oldstar->mint->XHc < MINT_MS_MINIMUM_HYDROGEN)
            {
                if(vb)
                    printf("MS end with XHc = %g\n",oldstar->mint->XHc);
                /*
                 * MS has ended
                 */
                newstar->stellar_type = HERTZSPRUNG_GAP;
                newstar->tms = stardata->model.time;
                newstar->age = newstar->tms;
                newstar->TAMS_luminosity = newstar->luminosity;
                newstar->TAMS_radius = newstar->radius;


                //// FOR TESTING M=2.09 star ////
                //newstar->mint->central_degeneracy = -2.15;
                //newstar->core_mass[CORE_He] = 0.065 * newstar->mass;

#ifdef NUCSYN
                newstar->TAMS_core_mass =
                    MINT_has_shells(newstar) ?
                    MINT_core(newstar,XHe4,0.99) :
                    0.0;
#else
                newstar->TAMS_core_mass = 0.0;
#endif// NUCSYN
                newstar->tm = newstar->age;

                if(vb)
                    printf("MS ended : L=%g R=%g Mc=%g tm=%g\n",
                           newstar->TAMS_luminosity,
                           newstar->TAMS_radius,
                           newstar->TAMS_core_mass,
                           newstar->tm);
            }
        }
        else if(oldstar->stellar_type == CHeB)
        {
            if(vb)
            {
                printf("New CHEB? prev algs ");
                for(int i=0; i<STELLAR_STRUCTURE_ALGORITHM_NUMBER; i++)
                {
                    if(oldstar->stellar_structure_algorithm[i] == TRUE)
                    {
                        printf("%s ",
                               stellar_structure_algorithm_strings[i]);
                    }
                }
            }

            if(oldstar->mint->XHec <= 0.0)
            {
                /*
                 * Finished CHeB
                 */
                newstar->stellar_type = EAGB;
                newstar->mint->XHec = 0.0;
            }
        }
    }

    /*
     * Clear derivatives used by MINT
     */
    MINT_clear_evolutionary_derivatives(stardata,
                                        newstar);

    Stellar_type ret = newstar->stellar_type;
#undef X
#define X(STELLAR_TYPE,FUNCTION,CALL_BSE,MINT_TABLE) FUNCTION,
    static Stellar_type (*functions[])(struct stardata_t * stardata,
                                       struct star_t * const star,
                                       struct star_t * const newstar,
                                       const Caller_id caller_id) =
        { MINT_STELLAR_STRUCTURE_FUNCTIONS_LIST };
#undef X
#define X(STELLAR_TYPE,FUNCTION,CALL_BSE,MINT_TABLE) MINT_CALL_BSE_##CALL_BSE,
    static const unsigned int call_bse_action[] =
        { MINT_STELLAR_STRUCTURE_FUNCTIONS_LIST };
#undef X
#define X(STELLAR_TYPE,FUNCTION,CALL_BSE,T) MINT_TABLE_##T,
    static const unsigned int checktables[] =
        { MINT_STELLAR_STRUCTURE_FUNCTIONS_LIST };
#undef X
#define X(ARG) #ARG,
    static const char * const call_bse_action_strings[] =
        { MINT_CALL_BSE_ACTIONS };
#undef X

    for(int i=0; i<STELLAR_STRUCTURE_ALGORITHM_NUMBER; i++)
    {
        newstar->stellar_structure_algorithm[i] = FALSE;
    }

    newstar->stellar_structure_algorithm[STELLAR_STRUCTURE_ALGORITHM_MINT] = TRUE;

    /*
     * Call BSE before MINT?
     */
    const unsigned int action = call_bse_action[newstar->stellar_type];

    if(vb)printf("MINT star %d : t=%30.20e st = %d action = %u %s X=%g Y=%g\n",
                 newstar->starnum,
                 stardata->model.time,
                 newstar->stellar_type,
                 call_bse_action[newstar->stellar_type],
                 call_bse_action_strings[action],
                 newstar->mint->XHc,
                 newstar->mint->XHec
        );
    if(stellar_type_exceeded(stardata,newstar) == TRUE)
    {
        return ret;
    }

    Boolean BSE_mapped = FALSE;
    if(action == MINT_CALL_BSE_BEFORE ||
       (action == MINT_CALL_BSE_BEFORE_IF_NO_TABLE &&
        checktables[newstar->stellar_type] != MINT_TABLE_NONE &&
        MINT_has_table(checktables[newstar->stellar_type]) == FALSE) ||
       action == MINT_CALL_BSE_BOTH)
    {
        if(vb) printf("MINT_stellar_structure : call BSE before MINT\n");
        if(oldstar &&
           oldstar->stellar_structure_algorithm[STELLAR_STRUCTURE_ALGORITHM_MINT] == TRUE &&
           oldstar->stellar_structure_algorithm[STELLAR_STRUCTURE_ALGORITHM_MODIFIED_BSE] == FALSE)
        {
            if(vb)printf("MAP STAR (before)\n");
            MINT_map_star_to_BSE(stardata,oldstar,newstar);
            BSE_mapped = TRUE;
        }
        ret = MINT_call_BSE_stellar_structure(stardata,
                                              oldstar,
                                              newstar,
                                              caller_id);
        newstar->stellar_structure_algorithm[STELLAR_STRUCTURE_ALGORITHM_MODIFIED_BSE] = TRUE;
        if(stellar_type_exceeded(stardata,newstar) == TRUE)
        {
            return ret;
        }
    }

    /*
     * Call MINT?
     */
    if(functions[newstar->stellar_type] != NULL &&
       stellar_type_exceeded(stardata,newstar) == FALSE)
    {
        if(vb)
        {
            printf("MINT_stellar_structure : call MINT structure function (star type %d)\n",
                   newstar->stellar_type);
            fflush(NULL);
        }
        ret = functions[newstar->stellar_type](stardata,
                                               oldstar,
                                               newstar,
                                               caller_id);
        newstar->stellar_structure_algorithm[STELLAR_STRUCTURE_ALGORITHM_MINT] = TRUE;
        if(stellar_type_exceeded(stardata,newstar) == TRUE)
        {
            return ret;
        }
    }

    /*
     * Check if MINT no longer controls this star: if so,
     * call BSE.
     */
    if(oldstar != NULL &&
       BSE_mapped == FALSE &&
       oldstar->stellar_type != newstar->stellar_type &&
       oldstar->stellar_structure_algorithm[STELLAR_STRUCTURE_ALGORITHM_MINT] == TRUE &&
       oldstar->stellar_structure_algorithm[STELLAR_STRUCTURE_ALGORITHM_MODIFIED_BSE] == FALSE &&
       newstar->stellar_structure_algorithm[STELLAR_STRUCTURE_ALGORITHM_MINT] == FALSE &&
       newstar->stellar_structure_algorithm[STELLAR_STRUCTURE_ALGORITHM_MODIFIED_BSE] == TRUE)
    {
        if(vb) printf("MAP STAR (after)\n");
        MINT_map_star_to_BSE(stardata,oldstar,newstar);
        BSE_mapped = TRUE;
    }


    /*
     * Call BSE after MINT?
     */
    if(action == MINT_CALL_BSE_AFTER ||
       (action == MINT_CALL_BSE_AFTER_IF_NO_TABLE &&
        checktables[newstar->stellar_type] != MINT_TABLE_NONE &&
        MINT_has_table(checktables[newstar->stellar_type]) == FALSE) ||
       action == MINT_CALL_BSE_BOTH)
    {
        if(vb) printf("MINT_stellar_structure : call BSE after MINT\n");
        ret = MINT_call_BSE_stellar_structure(stardata,
                                              oldstar,
                                              newstar,
                                              caller_id);
        newstar->stellar_structure_algorithm[STELLAR_STRUCTURE_ALGORITHM_MODIFIED_BSE] = TRUE;
        if(stellar_type_exceeded(stardata,newstar) == TRUE)
        {
            return ret;
        }
    }

    if(vb)
    {
        printf("MINT_stellar_structure return %d (stellar types %d %d)\n",
               ret,
               oldstar ? oldstar->stellar_type : -1,
               newstar ? newstar->stellar_type : -1);
    }
    return ret;
}


#endif // MINT
