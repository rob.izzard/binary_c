#include "../binary_c.h"

No_empty_translation_unit_warning;

#if defined MINT && defined NUCSYN


/*
 * Wrapper for MINT general nucleosynthesis.
 */

void MINT_nucsyn(struct stardata_t * const Restrict stardata)
{
#ifdef NUCSYN
    /*
     * Shellular nuclear burning
     */

#undef X
#define X(STELLAR_TYPE,BEFORE,BURN,AFTER) BURN,
    static Boolean do_nuclear_burning[] = { MINT_NUCSYN_FUNCTIONS_LIST };
#undef X
#define X(STELLAR_TYPE,BEFORE,BURN,AFTER) BEFORE,
    static void (*before_functions[])(struct stardata_t * stardata,
                                      struct star_t * const star) =
        { MINT_NUCSYN_FUNCTIONS_LIST };
#undef X
#define X(STELLAR_TYPE,BEFORE,BURN,AFTER) AFTER,
    static void (*after_functions[])(struct stardata_t * stardata,
                                     struct star_t * const star) =
        { MINT_NUCSYN_FUNCTIONS_LIST };
#undef X

    if(stardata->preferences->MINT_nuclear_burning == TRUE)
    {
        Foreach_evolving_star(star)
        {
            /*
             * Burning function before shellular burning
             */
            if(before_functions[star->stellar_type] != NULL)
            {
                before_functions[star->stellar_type](stardata,star);
            }

            if(MINT_has_shells(star))
            {
                /*
                 * nucleosynthesis that requires shells to be defined
                 */

                /*
                 * Do shellular burning?
                 */
                const Boolean do_burn =
                    do_nuclear_burning[star->stellar_type] &&
                    star->do_burn == TRUE &&
                    stardata->preferences->MINT_nuclear_burning == TRUE;

                if(do_burn)
                {
                    Reaction_rate convective_sigmav[SIGMAV_SIZE] = { 0.0 };
                    Abundance Xconv[ISOTOPE_ARRAY_SIZE] = { 0.0 };
                    double mconv = 0.0;
                    for(Shell_index k=0; k<star->mint->nshells; k++)
                    {
                        struct mint_shell_t * const shell = &star->mint->shells[k];
                        struct mint_shell_t * const prevshell = &star->mint->shells[k==0 ? 0 : (k-1)];

                        /*
                         * Nuclear burning
                         */
                        MINT_shellular_burning(stardata,
                                               star,
                                               shell,
                                               prevshell,
                                               k,
                                               convective_sigmav,
                                               Xconv,
                                               &mconv);
                    }


                    /*
                     * Correct for slight mass changes because
                     * of burning
                     */
                    MINT_renormalize_shells(star);
                }

                /*
                 * Mix convective zones
                 */
                MINT_convection(stardata,
                                star);

                /*
                 * Do thermohaline mixing
                 */
                MINT_thermohaline(stardata,
                                  star);

                for(Shell_index k=0; k<star->mint->nshells; k++)
                {
                    nancheck("MINT burn",
                             star->mint->shells[k].X,
                             ISOTOPE_ARRAY_SIZE);
                }
            }

            /*
             * Function to be called after shellular burning and mixing.
             */
            if(after_functions[star->stellar_type] != NULL)
            {
                after_functions[star->stellar_type](stardata,star);
            }

        }
    }
#endif//NUCSYN

    /*
     * Binary mass transfer
     */
    MINT_binary(stardata);

    /*
     * Radioactive decays
     */
    MINT_radioactive_decay(stardata);

    /*
     * Kippenhahn diagram?
     */
    Foreach_star(star,companion)
    {
        const Boolean do_kipp =
            stardata->preferences->MINT_Kippenhahn == star->starnum + 1;
        const Boolean this =
            (stardata->preferences->MINT_Kippenhahn_stellar_type == -1 ||
             stardata->preferences->MINT_Kippenhahn_stellar_type == star->stellar_type);
        const Boolean other =
            (stardata->preferences->MINT_Kippenhahn_companion_stellar_type == -1 ||
             stardata->preferences->MINT_Kippenhahn_companion_stellar_type == companion->stellar_type);
        const Boolean call = do_kipp && (this || other);

/*
        printf("ifs %d : %d %d %d %d %d -> %d : %d %d %d: %p %p -> %d && (%d || %d) -> %d && %d -> %s\n",
               star->starnum,
               star->starnum == stardata->preferences->MINT_Kippenhahn - 1,
               stardata->preferences->MINT_Kippenhahn_stellar_type == -1,
               stardata->preferences->MINT_Kippenhahn_stellar_type == star->stellar_type,
               stardata->preferences->MINT_Kippenhahn_companion_stellar_type == -1,
               stardata->preferences->MINT_Kippenhahn_companion_stellar_type == companion->stellar_type,
               star->starnum == stardata->preferences->MINT_Kippenhahn - 1 &&
               (
                   (stardata->preferences->MINT_Kippenhahn_stellar_type == -1 ||
                    stardata->preferences->MINT_Kippenhahn_stellar_type == star->stellar_type)
                   &&
                   (stardata->preferences->MINT_Kippenhahn_companion_stellar_type == -1 ||
                    stardata->preferences->MINT_Kippenhahn_companion_stellar_type == companion->stellar_type)
                   ),
               do_kipp,
               this,
               other,
               (void*)star,
               (void*)companion,
               do_kipp,
               this,
               other,
               do_kipp,
               this||other,
               Yesno(call));
*/

        if( call )
        {
            MINT_Kippenhahn(stardata,
                            star);
        }
    }
}

#endif // MINT && NUCSYN
