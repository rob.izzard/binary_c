#pragma once
#ifndef MINT_FUNCTION_MACROS_H
#define MINT_FUNCTION_MACROS_H


/*
 * Pointer offset for VAR in each shell,
 * used in conjunction with MINT_sort_shells().
 *
 * Returns in units of sizeof(double), but can be
 * positive or negative, so long int.
 */
#define MINT_shell_offset(STAR,VAR)                             \
    ((long int)((                                               \
                    (long int)(                                 \
                        (char*)&((STAR)->mint->shells[0].VAR) - \
                        (char*)&((STAR)->mint->shells[0]))      \
                    )/sizeof(double)))

#define MINT_shell_var_at_offset(STAR,                  \
                                 NSHELL,                \
                                 OFFSET)                \
    (                                                   \
        *(double*)(                                     \
            (char*)&((STAR)->mint->shells[(NSHELL)])    \
            +                                           \
            sizeof(double)*(OFFSET)                     \
            )                                           \
        )

#define MINT_var_at_offset(POINTER,             \
                           NSHELL,              \
                           OFFSET)              \
    (                                           \
        *(double*)(                             \
            (char*)(POINTER)                    \
            +                                   \
            sizeof(double)*(OFFSET)             \
            )                                   \
        )

/*
 * Copy shell from S1 to S2
 */
#define MINT_copy_shell(S1,S2)                  \
    memcpy(S2,                                  \
           S1,                                  \
           sizeof(struct mint_shell_t));

/*
 * Clean shell at pointer S
 */
#define MINT_clean_shell(S)                     \
    memset(S,0,sizeof(struct mint_shell_t));

#define MINT_new_clean_shell                        \
    __extension__                                   \
    ({                                              \
        struct mint_shell_t * const s =             \
            Malloc(sizeof(struct mint_shell_t));    \
        MINT_clean_shell(s);                        \
        s;                                          \
    })


/*
 * Table result size of table N
 */
#define MINT_result_size(N) (sizeof(double)*stardata->store->MINT_tables[(N)]->ndata)
#define MINT_mapped_result_size(N) (sizeof(double)*stardata->persistent_data->MINT_mapped_tables[(N)]->ndata)

/*
 * MINT data action filter.
 * If ACTION is not one of the MINT_ACTIONS_LIST,
 * we just return the data.
 *
 * Put the most commonly used actions first, so the
 * code is slightly faster.
 *
 * The Boolean "cleanup", if TRUE, activates checks
 * on the data and fills it with dummy values that at
 * least will not give inf or nan errors.
 */
#define MINT_data_filter(DATA,ACTION) (                                 \
        (ACTION) == MINT_ACTION_NOTHING ? (DATA) :                      \
        (ACTION) == MINT_ACTION_LOG10 ? (cleanup==TRUE ? Saferlog10(DATA) : log10(DATA)) : \
        (ACTION) == MINT_ACTION_LOG10ABS ? (cleanup==TRUE ? Saferlog10(fabs(DATA)) : log10(fabs(DATA))) : \
        (ACTION) == MINT_ACTION_LOG10NEGATIVE ? (cleanup==TRUE ? Saferlog10(-(DATA)) : log10(-(DATA))) : \
        (ACTION) == MINT_ACTION_ABS ? (fabs(DATA)) :                    \
        (DATA)                                                          \
        )


/*
 * You can call Foreach_shell with a variable number of arguments
 *
 * 1 argument: shell
 *    Takes the name of the shell struct.
 *
 * 2 arguments: star,shell
 *    Takes the star and shell struct names.
 *
 * 3 arguments: star,shell,prevshell
 *    Takes the star, shell and previous shell struct names.
 *    If there is no previous shell, it is set to NULL.
 *
 * 4 arguments: star,shell,prevshell,nextshell
 *    Takes the star, shell, previous and next shell struct names,
 *    as well as a Shell_index variable that stores the shell
 *    number.
 *   When shells prevshell and nextshell do not exist, they are NULL.
 *
 * 5 arguments: star,shell,prevshell,nextshell,index
 *    Takes the star, shell, previous and next shell struct names (as above),
 *    as well as a Shell_index variable that stores the shell
 *    number.
 */


#undef ConcatC
#define ConcatC(A,B) Concat(A,B)

#define Foreach_shell(...)                      \
    ConcatC(                                    \
        Foreach_shell_implementation,           \
        NARGS(__VA_ARGS__)                      \
        )(Foreachshell,                         \
          __COUNTER__,                          \
          __VA_ARGS__)
/*
 * given star, shell, prevshell, nextshell,index:
 * the outer for defines INDEX, which is then used in
 * the inner for and has its scope limited to the double for loop
 */
#define Foreach_shell_implementation5(LABEL,                            \
                                      LINE,                             \
                                      STAR,                             \
                                      SHELL,                            \
                                      PREVSHELL,                        \
                                      NEXTSHELL,                        \
                                      INDEX)                            \
    Boolean Concat3(_i,LABEL,LINE) = TRUE;                              \
    for(Shell_index INDEX = 0;                                          \
        Concat3(_i,LABEL,LINE)==TRUE ;                                  \
        Concat3(_i,LABEL,LINE)=FALSE)                                   \
        for(struct mint_shell_t * SHELL, *PREVSHELL, *NEXTSHELL;        \
            (                                                           \
                (INDEX) < (STAR)->mint->nshells)                        \
                &&                                                      \
                (SHELL = &(STAR)->mint->shells[(INDEX)])                \
                &&                                                      \
                ( ( PREVSHELL = (                                       \
                        (INDEX)==0                                      \
                        ? NULL                                          \
                        : &(STAR)->mint->shells[(INDEX)-1]) )           \
                  || 1)                                                 \
                &&                                                      \
                ( ( NEXTSHELL = (                                       \
                        (INDEX)==((STAR)->mint->nshells-1)              \
                        ? NULL                                          \
                        : &(STAR)->mint->shells[(INDEX)+1]) )           \
                  || 1 )                                                \
                ;                                                       \
            (INDEX)++                                                   \
            )



/*
 * given star, shell, prevshell, nextshell
 */
#define Foreach_shell_implementation4(LABEL,                            \
                                      LINE,                             \
                                      STAR,                             \
                                      SHELL,                            \
                                      PREVSHELL,                        \
                                      NEXTSHELL)                        \
    Shell_index Concat3(k,LABEL,LINE) = 0;                              \
    for(struct mint_shell_t * SHELL, *PREVSHELL, *NEXTSHELL;            \
        (Concat3(k,LABEL,LINE) < (STAR)->mint->nshells)                 \
            &&                                                          \
            (SHELL = &(STAR)->mint->shells[Concat3(k,LABEL,LINE)])      \
            &&                                                          \
            ( ( (PREVSHELL) = k==0                                      \
                ? NULL :                                                \
                &(STAR)->mint->shells[Concat3(k,LABEL,LINE)-1] )        \
              || 1)                                                     \
            &&                                                          \
            ( ( (NEXTSHELL) = k==star->mint->shells-1                   \
                ? NULL                                                  \
                : &(STAR)->mint->shells[Concat3(k,LABEL,LINE)+1] )      \
              || 1);                                                    \
        Concat3(k,LABEL,LINE)++)

/* given star, shell, prevshell */
#define Foreach_shell_implementation3(LABEL,                    \
                                      LINE,                     \
                                      STAR,                     \
                                      SHELL,                    \
                                      PREVSHELL)                \
    Shell_index Concat3(k,LABEL,LINE) = 0;                      \
    for(struct mint_shell_t * SHELL, * PREVSHELL;               \
    (Concat3(k,LABEL,LINE) < (STAR)->mint->nshells)             \
    && (SHELL = &(STAR)->mint->shells[Concat3(k,LABEL,LINE)])   \
        && ( (PREVSHELL = k==0                                  \
              ? NULL                                            \
              : &                                               \
              (STAR)->mint->shells[Concat3(k,LABEL,LINE)-1])    \
             || 1);                                             \
    Concat3(k,LABEL,LINE)++)


/* given star and shell */
#define Foreach_shell_implementation2(LABEL,LINE,STAR,SHELL)    \
    Shell_index Concat3(k,LABEL,LINE) = 0;                      \
    for(struct mint_shell_t * SHELL;                            \
        (Concat3(k,LABEL,LINE) < (STAR)->mint->nshells)         \
            && (SHELL = &(STAR)->mint->shells[                  \
                    Concat3(k,LABEL,LINE)]);                    \
        Concat3(k,LABEL,LINE)++)

/* given only shell */
#define Foreach_shell_implementation1(LABEL,LINE,SHELL) \
    Shell_index Concat3(k,LABEL,LINE) = 0;              \
    for(struct mint_shell_t * SHELL;                    \
        (Concat3(k,LABEL,LINE) < star->mint->nshells)   \
            && (SHELL = &star->mint->shells[            \
                    Concat3(k,LABEL,LINE)]);            \
        Concat3(k,LABEL,LINE)++)

/* neither star not shell : do not nest! */
#define Foreach_shell_implementation0(LABEL,LINE)       \
    Shell_index Concat3(k,LABEL,LINE) = 0;              \
    for(struct mint_shell_t * shell;                    \
        (Concat3(k,LABEL,LINE) < star->mint->nshells)   \
            && (shell = &star->mint->shells[            \
                    Concat3(k,LABEL,LINE)]);            \
        Concat3(k,LABEL,LINE)++)


#define MINT_has_shells(STAR) (                 \
        (STAR) != NULL &&                       \
        (STAR)->mint != NULL &&                 \
        (STAR)->mint->shells != NULL &&         \
        (STAR)->mint->nshells > 0               \
    )

#define MINT_has_table(TABLE) (                     \
        stardata != NULL &&                         \
        stardata->store != NULL &&                  \
        stardata->store->MINT_tables[(TABLE)] != NULL   \
        )


/*
 * MINT warnings
 */
#define MINT_warning(...)                                       \
    if(stardata->preferences->MINT_disable_warnings == FALSE)   \
    {                                                           \
        fprintf(stderr,"Warning: ");                            \
        fprintf(stderr,__VA_ARGS__);                            \
    }

#endif // MINT_FUNCTION_MACROS_H
