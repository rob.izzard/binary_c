#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef MINT

/*
 * MINT function to calculate stellar structure
 * during core helium burning.
 */
#undef DEBUG
#define DEBUG 1
Stellar_type MINT_stellar_structure_CHeB(struct stardata_t * Restrict const stardata Maybe_unused,
                                         struct star_t * const prevstar Maybe_unused,
                                         struct star_t * const newstar,
                                         const Caller_id caller_id Maybe_unused)
{
    Boolean vb = FALSE;

    /*
     * Boolean to test for data availability
     */
    struct mint_table_metadata_t * const metadata =
        stardata->store->MINT_tables[MINT_TABLE_CHeB]->metadata;
    const Boolean * const available = metadata->data_available;
    if(vb) printf("MINT_stellar_structure_CHeB : MINT data available? %s\n",
                  Yesno(available!=NULL));

    if(available == NULL)
    {
        /*
         * MINT table unavailable, call BSE
         */
        newstar->stellar_type = CHeB;
        if(vb) printf("MINT_stellar_structure_CHeB : data not available, call BSE\n");
        return MINT_call_BSE_stellar_structure(stardata,
                                               prevstar,
                                               newstar,
                                               caller_id);
    }
    else
    {
#ifdef __DEPRECATED
        if(MINT_has_shells(newstar))
        {
            /*
             * Make the envelope cool
             */
            Foreach_shell(newstar,shell)
            {
                shell->T = 0.0;
            }

            /*
             * Set the envelope convection zone
             */
            const double m_inner_edge = newstar->mass - newstar->menv;

            /*
             * Set the (convective) helium-burning core mass.
             *
             * BSE does not give us this, so assume it is half
             * the core mass (~true for M=2.5 Z=0.02)
             */
            const double mc_He = newstar->core_mass[CORE_He] * 0.5;

            Foreach_shell(newstar,shell)
            {
                shell->convective =
                    (shell->m > m_inner_edge) ? TRUE :
                    (shell->m < mc_He) ? TRUE :
                    FALSE;
            }
        }
#endif // __DEPRECATED

        /******************************/
        /** Core-helium burning star **/
        /******************************/
        Dprint("CHeB star : MINT algorithm\n");

        newstar->derivative[DERIVATIVE_STELLAR_He_CORE_MASS] = 0.0;
        newstar->derivative[DERIVATIVE_STELLAR_GB_CORE_MASS] = 0.0;
        newstar->derivative[DERIVATIVE_STELLAR_CO_CORE_MASS] = 0.0;

        /*
         * Set stellar type
         */
        newstar->stellar_type = CHeB;

        if(available == NULL)
        {
            Exit_binary_c(BINARY_C_ALLOC_FAILED,
                          "stardata->store->MINT_data_available[MINT_TABLE_CHeB] is NULL. This should not happen.");
        }

        /*
         * Map between MINT's wanted columns and columns available
         * in the data table
         */
        const unsigned int * const map = metadata->data_map;

        if(map == NULL)
        {
            Exit_binary_c(BINARY_C_ALLOC_FAILED,
                          "stardata->store->MINT_data_map[MINT_TABLE_CHeB] is NULL. This should not happen.");
        }

        /*
         * Space for interpolations
         */
        double * result, * result_chebyshev, * result_conv;
        if(stardata->persistent_data->MINT_CHeB_result == NULL)
        {
            result =
                stardata->persistent_data->MINT_CHeB_result =
                Malloc(MINT_result_size(MINT_TABLE_CHeB));
            result_chebyshev =
                stardata->persistent_data->MINT_CHeB_result_chebyshev =
                Malloc(MINT_result_size(MINT_TABLE_CHeB));
            result_conv =
                stardata->persistent_data->MINT_CHeB_result_conv =
                Malloc(MINT_result_size(MINT_TABLE_CHeB));

            if(result == NULL ||
               result_chebyshev == NULL ||
               result_conv == NULL)
            {
                Exit_binary_c(BINARY_C_ALLOC_FAILED,
                              "Failed to allocate memory for MINT MS interpolation tables");
            }
        }
        else
        {
            result =
                stardata->persistent_data->MINT_CHeB_result;
            result_chebyshev =
                stardata->persistent_data->MINT_CHeB_result_chebyshev;
            result_conv =
                stardata->persistent_data->MINT_CHeB_result_conv;
        }

        /*
         * Macro to get data from the interpolation, if it is
         * available, otherwise return 0.0. You can check
         * available[VARTYPE] yourself if you want to apply
         * a more complicated algorithm.
         */
#undef get_data
#define get_data(VARTYPE)                                               \
        ( available[(VARTYPE)]==TRUE ? result[map[(VARTYPE)]] : 0.0 )

        /*
         * Use table to look up stellar structure, e.g. dY/dt,L,R,
         * as a function of phase start mass and central hydrogen abundance
         */
        Loop_forever
        {
            /*
             * Interpolate to find stellar structure
             */
            Interpolate(stardata->store->MINT_tables[MINT_TABLE_CHeB],
                        ((double[]){
                            log10(newstar->mass),
                            newstar->mint->XHec
                        }),
                        result,
                        FALSE);

#if defined DEBUG && DEBUG == 1
            {
                char * s = NULL;
                for(size_t i=0;
                    i<MINT_result_size(MINT_TABLE_CHeB)/sizeof(double);
                    i++)
                {
                    reasprintf(&s,
                               "%s %zu=%g",
                               s == NULL ? "" : s,
                               i,
                               result[i]);
                }
                Dprint("Interpolate:%s\n",
                       s);
                Safe_free(s);
            }
#endif

            /*
             * Hence set the stellar properties
             */
            newstar->luminosity = exp10(get_data(MINT_CHeB_LUMINOSITY));
            newstar->radius = exp10(get_data(MINT_CHeB_RADIUS));
            newstar->mint->warning = get_data(MINT_MS_WARNING_FLAG);

            if(Is_zero(newstar->age))
            {
                newstar->rzams = newstar->radius;
            }

            /*
             * Zero burning derivatives
             */
            newstar->derivative[DERIVATIVE_STELLAR_CENTRAL_HYDROGEN] = 0.0;
            newstar->derivative[DERIVATIVE_STELLAR_CENTRAL_HELIUM] = 0.0;
            newstar->derivative[DERIVATIVE_STELLAR_CENTRAL_CARBON] = 0.0;

            /*
             * burn He to C
             */
            const double dycdt = -exp10(get_data(MINT_CHeB_FIRST_DERIVATIVE_CENTRAL_HELIUM));

            newstar->derivative[DERIVATIVE_STELLAR_CENTRAL_HELIUM] = dycdt;
            newstar->derivative[DERIVATIVE_STELLAR_CENTRAL_CARBON] = -0.8*dycdt;
            newstar->derivative[DERIVATIVE_STELLAR_CENTRAL_OXYGEN] = -0.2*dycdt;
            // TODO: C12 + alpha -> O16 + gamma properly

            /*
             * Rate of change of convective core mass. We calculate this
             * by calculating the core mass at a slightly higher Xc, hence
             * we have dMc/dYc, then multiply by dYc/dt to calculate
             * dMc/dt.
             */
            {
                const double dYc = 1e-7;
                Interpolate(stardata->store->MINT_tables[MINT_TABLE_CHeB],
                            ((const double[]){
                                log10(newstar->mass),
                                newstar->mint->XHec + dYc
                            }),
                            result_conv,
                            FALSE);
                const double dmc =
                    newstar->mass * result_conv[MINT_CHeB_CONVECTIVE_CORE_MASS_OVERSHOOT_FRACTION] -
                    newstar->convective_core_mass;
                newstar->convective_core_mass_time_derivative =
                    dmc/dYc * dycdt;
            }

            newstar->core_radius = 0.0;
            newstar->convective_core_mass = newstar->mass * get_data(MINT_CHeB_CONVECTIVE_CORE_MASS_OVERSHOOT_FRACTION);
            newstar->convective_core_radius = newstar->radius * get_data(MINT_CHeB_CONVECTIVE_CORE_RADIUS_OVERSHOOT_FRACTION);

            // TODO : update

            // helium core growth
            // newstar->core_mass[CORE_He] = // assumed constant in CHeB

            // assume CO core never shrinks
            newstar->core_mass[CORE_CO] = Max(newstar->convective_core_mass,
                                              newstar->core_mass[CORE_CO]);

            newstar->menv = newstar->mass * get_data(MINT_CHeB_CONVECTIVE_ENVELOPE_MASS_FRACTION);
            newstar->renv = newstar->radius * (1.0 - get_data(MINT_CHeB_CONVECTIVE_ENVELOPE_RADIUS_FRACTION));

            newstar->mint->mean_molecular_weight_star = get_data(MINT_CHeB_MEAN_MOLECULAR_WEIGHT_AVERAGE);
            newstar->mint->mean_molecular_weight_core = get_data(MINT_CHeB_MEAN_MOLECULAR_WEIGHT_CORE);

            /*
             * Set the phase start time if it hasn't already been set
             */
            if(newstar->phase_start[CHeB].time < 0.0)
            {
                newstar->phase_start[CHeB].time = stardata->model.time;
            }

            /*
             * To calculate the "age" we use XHec as a (linear) proxy
             */
            double phase_lifetime;
            MINT_CHeB_TA(stardata,
                         newstar->phase_start_mass,
                         &phase_lifetime,
                         NULL);

            const double helium_available = 1.0 - stardata->common.metallicity;
            newstar->age =
                newstar->phase_start[CHeB].time +
                phase_lifetime * (1.0 - newstar->mint->XHec / helium_available );
            newstar->epoch = stardata->model.time - newstar->age;
            if(vb)printf("CHeB = %g + %g * (1 - %g / %g) = %g : EPOCH %g\n",
                         newstar->phase_start[CHeB].time,
                         phase_lifetime,
                         newstar->mint->XHec,
                         helium_available,
                         newstar->age,
                         newstar->epoch);

            //newstar->moment_of_inertia_factor = get_data(MINT_CHeB_MOMENT_OF_INERTIA_FACTOR);
            /*
             * The line above should be replaced with
             * newstar->moment_of_inertia_factor = get_data(MINT_CHeB_MOMENT_OF_INERTIA_FACTOR);
             * once that quantity is available in CHeB grids
             * The MOMENT_OF_INERTIA_FACTOR above is MESA's and not I/(MR^2)
             */
            newstar->E2 =
                available[MINT_CHeB_TIDAL_E2] ?
                get_data(MINT_CHeB_TIDAL_E2) :
                E2(stardata,newstar);

            newstar->E_Zahn = get_data(MINT_CHeB_TIDAL_E_FOR_LAMBDA);

            if(MINT_has_shells(newstar))
            {
                if(newstar->mint->XHec > 0.1)
                {
                    /*
                     * increase shell resolution around the burning core
                     */
                    MINT_resolve_core_boundary(stardata,
                                               newstar,
                                               newstar->convective_core_mass, // band centre
                                               newstar->phase_start_mass * 0.005, // band thickness
                                               1e-3, // thinnest shell dm
                                               newstar->phase_start_mass / stardata->preferences->MINT_nshells, // default shell dm
                                               TRUE);
                }

                /*
                 * Interpolate to get Chebyshev data for shells
                 */
                MINT_CHeB_get_chebyshev_data(stardata,
                                             newstar,
                                             result_chebyshev);

                /*
                 * Chebyshev coordinates
                 */
                const double * const coords = CDict_nest_get_entry(
                    metadata->header->cdict,
                    "Chebyshev grid",
                    "masses"
                    )->value.value.double_array_data;

                int n_cheb = 0;

                Foreach_shell(newstar,shell)
                {
                    /*
                     * Set stellar shell data from Chebyshev data
                     */
                    n_cheb = MINT_CHeB_set_chebyshev_data(stardata,
                                                          newstar,
                                                          shell,
                                                          coords,
                                                          result_chebyshev,
                                                          n_cheb,
                                                          map,
                                                          available);

                    /*
                     * Identify instantly-mixed region from the
                     * convective core mass : note this needs
                     * updating to include the overshooting region
                     * and the surface convection zone
                     */
                    shell->convective = (shell->m < newstar->convective_core_mass) ? TRUE : FALSE;
                } /* loop over shells */
            } /* shells != NULL test */

            break;
        }


        if(vb)
        {
            printf("MINT CHeB star %d: stellar_type=%d (Z=%g, M=%g, XHec=%g) at t=%g = L=%g R=%g dYHec/dt=%g\n",
                   newstar->starnum,
                   newstar->stellar_type,
                   stardata->common.metallicity,
                   newstar->mass,
                   newstar->mint->XHec,
                   stardata->model.time,
                   newstar->luminosity,
                   newstar->radius,
                   newstar->derivative[DERIVATIVE_STELLAR_CENTRAL_HELIUM]);

            printf("MINT CHeB time remaining %g Myr\n",
                   - 1e-6 * newstar->mint->XHec /newstar->derivative[DERIVATIVE_STELLAR_CENTRAL_HELIUM]
                );

        }

#ifdef PRE_MAIN_SEQUENCE
        if(stardata->preferences->pre_main_sequence == TRUE)
        {
            double f = preMS_radius_factor(newstar->mass, 1e6*newstar->age);
            newstar->radius *= f;
            newstar->PMS = Boolean_(f>1.00001);
            Dprint("preMS f=%g r=%g roche_radius=%g roche_radius_at_periastron=%g PMS? %d\n",
                   f,
                   newstar->radius,
                   newstar->roche_radius,
                   newstar->roche_radius_at_periastron,
                   newstar->PMS);
        }
#endif

        Dprint("MS radius %g\n",newstar->radius);

#ifdef BSE
        /*
         * BSE-MINT compatibility
         */
#endif // BSE

        Dprint("return stellar type %d, XHe C %g\n",
               newstar->stellar_type,
               newstar->mint->XHec);


    }

    if(vb) printf("CHeB return stellar type %d\n",newstar->stellar_type);
    return newstar->stellar_type;
}

#endif // MINT
