#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef MINT
#include "MINT.h"

/*
 * Placeholder function to load no grid
 */

Boolean MINT_load_no_grid(struct stardata_t * const stardata Maybe_unused,
                          const int when Maybe_unused)
{
    return FALSE;
}

#endif // MINT
