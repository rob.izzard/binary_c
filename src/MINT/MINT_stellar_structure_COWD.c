#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef MINT

/*
 * MINT function to calculate stellar structure
 * of a carbon-oxygen white dwarf.
 *
 * Note that prevstar can be NULL, in which case
 * we cannot compare to the previous star to do (say)
 * rejuvenation.
 */

Stellar_type MINT_stellar_structure_COWD(struct stardata_t * Restrict const stardata Maybe_unused,
                                         struct star_t * const prevstar Maybe_unused,
                                         struct star_t * const newstar,
                                         const Caller_id caller_id Maybe_unused)
{
    struct mint_table_metadata_t * const metadata =
        stardata->store->MINT_tables[MINT_TABLE_COWD]->metadata;

    /* COWD stars have no He core */
    newstar->core_mass[CORE_He] = 0.0;

    /* but are all CO */
    newstar->core_mass[CORE_CO] = newstar->mass;

    /*
     * If central temperature is zero, we are just
     * starting the WD track and must set the central
     * temperature here.
     */
    if(Is_zero(newstar->mint->central_temperature))
    {
        // TODO make new table and interpolate it
        double ZAresult[1];
        Interpolate(stardata->store->MINT_tables[MINT_TABLE_ZACOWD],
                ((double[]){
                    stardata->common.metallicity,
                    newstar->mass
                }),
                ZAresult,
                FALSE);
        newstar->mint->central_temperature = exp10(ZAresult[0]);
    }

    /*
     * Set stellar type
     */
    newstar->stellar_type = COWD;

    /*
     * Booleans to test for data availability
     */
    const Boolean * const available = metadata->data_available;

    if(available == NULL)
    {
        Exit_binary_c(BINARY_C_ALLOC_FAILED,
                      "stardata->store->MINT_data_available[MINT_TABLE_COWD] is NULL. This should not happen.");
    }

    /*
     * Map between MINT's wanted columns and columns available
     * in the data table
     */
    const unsigned int * const map = metadata->data_map;

    if(map == NULL)
    {
        Exit_binary_c(BINARY_C_ALLOC_FAILED,
                      "stardata->store->MINT_data_map[MINT_TABLE_COWD] is NULL. This should not happen.");
    }

    /*
     * Space for interpolations
     */
    double * result, * result_chebyshev, * result_conv;
    if(stardata->persistent_data->MINT_COWD_result == NULL)
    {
        result =
            stardata->persistent_data->MINT_COWD_result =
            Malloc(MINT_result_size(MINT_TABLE_MS));
        result_chebyshev =
            stardata->persistent_data->MINT_COWD_result_chebyshev =
            Malloc(MINT_result_size(MINT_TABLE_MS));
        result_conv =
            stardata->persistent_data->MINT_COWD_result_conv =
            Malloc(MINT_result_size(MINT_TABLE_MS));

        if(result == NULL ||
           result_chebyshev == NULL ||
           result_conv == NULL)
        {
            Exit_binary_c(BINARY_C_ALLOC_FAILED,
                          "Failed to allocate memory for MINT COWD interpolation tables");
        }
    }
    else
    {
        result =
            stardata->persistent_data->MINT_COWD_result;
        result_chebyshev =
            stardata->persistent_data->MINT_COWD_result_chebyshev;
        result_conv =
            stardata->persistent_data->MINT_COWD_result_conv;
    }

    /*
     * Map the table's parameter column
     */
    if(stardata->persistent_data->MINT_mapped_tables[MINT_TABLE_COWD] == NULL)
    {
        Dprint("map COWD table\n");
        MINT_map_COWD_table(stardata);
    }

    /*
     * Macro to get data from the interpolation, if it is
     * available, otherwise return 0.0. You can check
     * available[VARTYPE] yourself if you want to apply
     * a more complicated algorithm.
     */
#undef get_data
#define get_data(VARTYPE)                                           \
    ( available[(VARTYPE)]==TRUE ? result[map[(VARTYPE)]] : 0.0 )

    /*
     * Interpolate to find stellar structure
     */
    Interpolate(stardata->persistent_data->MINT_mapped_tables[MINT_TABLE_COWD],
                ((double[]){
                    stardata->common.metallicity,
                    newstar->mass,
                    log10(newstar->mint->central_temperature)
                    }),
                result,
                FALSE);

    /*
     * Hence set the stellar properties
     */
    newstar->luminosity = exp10(get_data(MINT_COWD_LOG_LUMINOSITY));
    newstar->radius = get_data(MINT_COWD_RADIUS);
    newstar->mint->warning = get_data(MINT_MS_WARNING_FLAG);
    newstar->mint->central_density = exp10(get_data(MINT_COWD_LOG_CENTRAL_DENSITY));
    newstar->mint->central_pressure = exp10(get_data(MINT_COWD_LOG_CENTRAL_PRESSURE));

    /*
     * And the rate of change of central temperature, convert
     * to K/Myr
     */
    newstar->derivative[DERIVATIVE_STELLAR_CENTRAL_TEMPERATURE] =
        get_data(MINT_COWD_CENTRAL_TEMPERATURE_TIME_DERIVATIVE);

    /*
     * Do not allow central temperatures to dip below 1e5K
     * (this is also checked when the derivatives are applied)
     */
    if(newstar->mint->central_temperature < 1e5)
    {
        newstar->derivative[DERIVATIVE_STELLAR_CENTRAL_TEMPERATURE] = 0.0;
        newstar->mint->central_temperature = 1e5;
    }

    /*
     * debugging
     */
    if(FALSE)
    {
        Printf("COWD %g %g %g %g %g %g %g\n",
               stardata->common.metallicity,
               newstar->mass,
               newstar->age,
               // Tc and dTc/dt
               log10(newstar->mint->central_temperature),
               newstar->derivative[DERIVATIVE_STELLAR_CENTRAL_TEMPERATURE],
               // HRD
               log10(Teff_from_star_struct(newstar)),
               log10(newstar->luminosity)
            );
    }

    if(MINT_has_shells(newstar))
    {
        /*
         * Make the envelope cool
         */
        Foreach_shell(newstar,shell)
        {
            shell->T = 0.0;
            shell->convective = FALSE;
        }
    }
    return newstar->stellar_type;
}

#endif // MINT
