#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef MINT

/*
 * Function to merge the shells of two stars in MINT
 *
 * Merges star1 and star2 to make newstar, and frees
 * the shells in both stars
 */

void MINT_merge_stars_shells_MS(struct stardata_t * const stardata,
                                struct star_t * const star1,
                                struct star_t * const star2,
                                struct star_t * const newstar)
{
    /*
     * New shell count
     */
    newstar->mint->nshells = Sum_of_stars(mint->nshells);
    Dprint("new nshells = %d + %d = %d\n",
           star1->mint->nshells,
           star2->mint->nshells,
           newstar->mint->nshells);

    if(newstar->mint->nshells > stardata->preferences->MINT_maximum_nshells)
    {
        /* oops : > shells than we are allowed */
        Exit_binary_c(BINARY_C_OUT_OF_RANGE,
                      "more shells required than allowed on stellar merger");
    }

    /*
     * Expand newstar appropriately
     */
    newstar->mint->shells =
        Realloc(newstar->mint->shells,
                sizeof(struct mint_shell_t) *
                newstar->mint->nshells);

    /*
     * Copy the shells from star1 and star2 to newstar
     */
    if(star1->mint->nshells > 0)
    {
        memcpy(&newstar->mint->shells[0] + 0,
               &star1->mint->shells[0],
               sizeof(struct mint_shell_t) * star1->mint->nshells);
    }
    if(star2->mint->nshells > 0)
    {
        memcpy(&newstar->mint->shells[0] + star1->mint->nshells,
               &star2->mint->shells[0],
               sizeof(struct mint_shell_t) * star2->mint->nshells);
    }

    /*
     * Free star1 and star2's shells
     */
    Safe_free(star1->mint->shells);
    Safe_free(star2->mint->shells);
    star1->mint->nshells = 0;
    star2->mint->nshells = 0;
}

#endif // MINT
