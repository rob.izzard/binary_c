#include "../binary_c.h"
No_empty_translation_unit_warning;


#if defined MINT && defined NUCSYN

double MINT_core(struct star_t * star,
                 Isotope n,
                 Abundance threshold)
{
    /*
     * Return the mass of the first shell that has
     * isotope n's abundance > threshold, or 0.0
     * if none is found.
     */

    if(star->mint == NULL || star->mint->shells == NULL) return 0.0;
    Shell_index i = MINT_core_index(star,n,threshold);
    return i == -1 ? 0.0 : star->mint->shells[i].m;
}

Shell_index MINT_core_index(struct star_t * star,
                            Isotope n,
                            Abundance threshold)
{
    /*
     * Locate the first shell from the surface with
     * isotope n > the threshold abundance
     */
    int ret = -1;
    for(Shell_index i=star->mint->nshells-1;
        i>=0;
        i--)
    {
        /*
        printf("check isotope n=%u at i=%d/%d (mint = %p shells = %p)\n",
               n,
               i,
               star->mint ? star->mint->nshells : -1,
               (void*)(star->mint ? star->mint : NULL),
               (void*)(star->mint ? star->mint->shells : NULL)
            );
        */
        if(star->mint->shells[i].X[n] > threshold)
        {
            return i;
        }
    }
    return ret;
}

#endif // MINT && NUCSYN
