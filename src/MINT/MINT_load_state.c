#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef MINT
#ifdef __EXPERIMENTAL

#define _fread(...)                                                     \
    if(fread(__VA_ARGS__)==0 && ferror(fp))                             \
    {                                                                   \
        Exit_binary_c(BINARY_C_FILE_READ_ERROR,                         \
                      "Failed to read any data in read_data_table_from_file, trying to read from \"%s\"\n", \
                      bfp->path);                                       \
    }

void MINT_load_state(struct stardata_t * const stardata Maybe_unused)
{


    /*
     * Load the state of MINT from a file
     */
    struct binary_c_file_t * bfp = binary_c_fopen(stardata,
                                                  stardata->preferences->MINT_load_state_file,
                                                  "r",
                                                  0);
    FILE * fp = bfp->fp;

    /* data tables */
    for(int i=0; i<MINT_TABLE_NUMBER; i++)
    {
        stardata->store->MINT_tables[i] = read_data_table_from_file(stardata,
                                                                    bfp);
        printf("Loaded unmapped table %d %p\n",
               i,
               (void*)stardata->store->MINT_tables[i]);
    }
    for(int i=0; i<MINT_TABLE_NUMBER; i++)
    {
        stardata->persistent_data->MINT_mapped_tables[i] = read_data_table_from_file(stardata,
                                                                                     bfp);
        printf("Loaded mapped table %d %p\n",
               i,
               (void*)stardata->persistent_data->MINT_mapped_tables[i]);
    }

    /* headers */
    for(int i=0; i<MINT_TABLE_NUMBER; i++)
    {
        Boolean have_h;
        _fread(&have_h,sizeof(Boolean),1,fp);

        if(have_h == TRUE)
        {
            struct mint_header_t * const h =
                stardata->store->MINT_metadata[i]->header =
                Malloc(sizeof(struct mint_header_t));
            size_t buffer_size = 0;
            _fread(&buffer_size,sizeof(size_t),1,fp);
            char * buffer = Malloc((buffer_size + 1) * sizeof(char));
            _fread(buffer,sizeof(char),buffer_size,fp);
            CDict_new(cdict);
            h->cdict=cdict;
            CDict_JSON_buffer_to_CDict(buffer,h->cdict);
            Safe_free(buffer);
            size_t filename_len;
            _fread(&filename_len,sizeof(size_t),1,fp);
            _fread(h->filename,filename_len,1,fp);
        }
    }

    /* data maps */
    _fread(stardata->store->MINT_metadata[i]->data_map_size,
           sizeof(size_t),
           MINT_TABLE_NUMBER,
           fp);
    for(int i=0; i<MINT_TABLE_NUMBER; i++)
    {
        Boolean have_map; // = stardata->store->MINT_data_map[i] == NULL ? FALSE : TRUE;
        _fread(&have_map,sizeof(Boolean),1,fp);
        if(have_map == TRUE)
        {
            stardata->store->MINT_data_map[i] =
                Malloc(stardata->store->MINT_metadata[i]->data_map_size*sizeof(unsigned int));
            _fread(stardata->store->MINT_data_map[i],
                   sizeof(unsigned int),
                   stardata->store->MINT_data_map_size[i],
                   fp);
        }
    }

    _fread(stardata->store->MINT_tables_nscalars,
           sizeof(unsigned int),
           MINT_TABLE_NUMBER,
           fp);
    _fread(stardata->store->MINT_initial_coordinate,
           sizeof(Abundance),
           MINT_TABLE_NUMBER,
           fp);

    /* generic map */
    size_t n_generic_strings;
    _fread(&n_generic_strings,
           sizeof(size_t),
           1,
           fp);
    for(int i=0; i<NUMBER_OF_STELLAR_TYPES; i++)
    {
        Boolean have_map;
        _fread(&have_map,sizeof(Boolean),1,fp);
        if(have_map == TRUE)
        {
            _fread(stardata->store->MINT_generic_map[i],
                   sizeof(int),
                   n_generic_strings,
                   fp);
        }
    }

    /* data available */
    _fread(stardata->store->MINT_data_available_size,
           sizeof(size_t),
           MINT_TABLE_NUMBER,
           fp);
    for(int i=0; i<MINT_TABLE_NUMBER; i++)
    {
        Boolean have_available;
        _fread(&have_available,sizeof(Boolean),1,fp);
        if(have_available == TRUE)
        {
            stardata->store->MINT_data_available[i] = Malloc(sizeof(unsigned int) * stardata->store->MINT_data_available_size[i]);
            _fread(stardata->store->MINT_data_available[i],
                   sizeof(unsigned int),
                   stardata->store->MINT_data_available_size[i],
                   fp);
        }
    }

    binary_c_fclose(&bfp);

}
#endif // __EXPERIMENTAL
#endif
