#include "../binary_c.h"

No_empty_translation_unit_warning;

#ifdef MINT

void MINT_CHeB_ZA(struct stardata_t * const stardata,
                  const double mass,
                  double * const CHeB_ZA_luminosity)
{
    /*
     * Estimate CHeB zero-age data:
     * 1) the CHeB ZA luminosity (Lsun)
     *
     * Unit: Myr
     *
     * If *luminosity is non-NULL, set it also
     */
    double params[1] = {
        log10(mass),
    };
    double result[1];
    /*
     * Interpolate
     * params column is: log10(mass)
     * result columns are : log10(luminosity)
     */
    Interpolate(stardata->store->MINT_tables[MINT_TABLE_CHeB_ZA],
                params,
                result,
                FALSE);

    if(CHeB_ZA_luminosity != NULL)
    {
        *CHeB_ZA_luminosity = exp10(result[0]);
    }

    return;
}
#endif // MINT
