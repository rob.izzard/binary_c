#include "../binary_c.h"

No_empty_translation_unit_warning;

#if defined MINT && defined NUCSYN

void MINT_nucsyn_MS_pre(struct stardata_t * Restrict const stardata Maybe_unused,
                        struct star_t * Restrict const star)
{
#ifdef NUCSYN_FIRST_DREDGE_UP_ACCRETION_CORRECTION
    /* save the main sequence mass, which will remain at the terminal MS value */
    star->MS_mass = star->phase_start_mass;
#endif
    star->do_burn = TRUE;
}
#endif // MINT && NUCSYN
