#pragma once
#ifndef MINT_DATA_COLUMNS_MS_H
#define MINT_DATA_COLUMNS_MS_H

/*
 * Columns should be listed in
 * MINT_data_columns_MS_list.h
 */

#include "MINT_data_columns_MS_list.def"

/*
 * Make parameter indices
 */
#undef LOCAL_COUNTER
#undef X
#define LOCAL_COUNTER (__COUNTER__ - COUNTER_BASE_MS_PARAMETER_ITEMS - 1)
#define X(NAME) NAME = LOCAL_COUNTER
enum { COUNTER_BASE_MS_PARAMETER_ITEMS = __COUNTER__ };
enum {MINT_MS_PARAMETER_ITEMS};

/*
 * Make data item indices
 */
#undef LOCAL_COUNTER
#undef X
#define LOCAL_COUNTER (__COUNTER__ - COUNTER_BASE_MS_DATA_ITEMS - 1)
#define X(NAME) NAME = LOCAL_COUNTER
enum { COUNTER_BASE_MS_DATA_ITEMS = __COUNTER__ };
enum {MINT_MS_DATA_ITEMS};

/*
 * Clear X macro and LOCAL_COUNTER
 */
#undef LOCAL_COUNTER
#undef X


#endif // MINT_DATA_COLUMMNS_MS_H
