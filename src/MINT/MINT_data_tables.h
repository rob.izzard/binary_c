#pragma once
#ifndef MINT_DATA_TABLES_H
#define MINT_DATA_TABLES_H

/*
 * Construct MINT data tables
 */

#include "MINT_data_tables.def"
#undef X
#define X(ID,FILENAME,FUNCTION,FAILACTION,WHEN) MINT_TABLE_##ID,
enum {
    MINT_TABLES_LIST
    MINT_TABLE_NONE
};
#undef X

/*
 * What to do on data table load failure?
 */
enum {
    MINT_LOAD_FAIL_DO_NOTHING,
    MINT_LOAD_FAIL_EXIT,
    MINT_LOAD_FAIL_WARN,
    MINT_LOAD_FAIL_IGNORED
};

/*
 * When to load the table?
 */

enum {
    MINT_LOAD_TABLE_NOW,
    MINT_LOAD_TABLE_DEFER
};

#endif // MINT_DATA_TABLES_H
