#include "../binary_c.h"
No_empty_translation_unit_warning;

/*
 * Return the convective core mass of the star,
 * taking into account non-equilibrium processes
 * such as accretion luminosity.
 */

#ifdef MINT
#include "MINT.h"

double MINT_MS_non_eq_conv_coremass(struct stardata_t * const stardata,
                                    struct star_t * star)
{
    const double L_excess =
        Max(0.0,
            star->accretion_luminosity - star->luminosity);

    const double Mc_lum =
        Is_zero(L_excess) ? 0.0 :
        MINT_ZAMS_core_mass_from_luminosity(stardata,
                                            star,
                                            L_excess);

    //if(Mc_lum > TINY) printf("mc lum %g\n",Mc_lum);

    return Min(star->mass,
               Max(star->convective_core_mass,
                   Mc_lum));
}
#endif // MINT
