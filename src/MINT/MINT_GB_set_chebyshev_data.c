#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef MINT
#include "MINT.h"

/*
 * Set T, rho, etc. which are interpolated
 * on the Chebyshev mass grid
 */

int MINT_GB_set_chebyshev_data(struct stardata_t * Restrict const stardata Maybe_unused,
                               struct star_t * const newstar,
                               struct data_table_t * const data_table,
                               struct mint_shell_t * const shell,
                               const double * const coords,
                               const double * const result_cheb,
                               const unsigned int nin,
                               const unsigned int * const map,
                               const Boolean * const available)
{
    /*
     * Mass coordinate :
     * core is from 0 to 1
     * envelope is from 1 to 2
     */
    const double Mc = newstar->core_mass[CORE_He];
    const double Menv = newstar->mass - newstar->core_mass[CORE_He];
    const double qcoord =
        shell->m < Mc ? (shell->m/Mc) : (1.0 + (shell->m - Mc)/Menv);
    struct mint_table_metadata_t * const metadata =
        data_table->metadata;
    const unsigned int ncheb = CDict_nest_get_entry(
        metadata->header->cdict,
        "Chebyshev grid",
        "number of masses"
        )->value.value.int_data;

    /*
     * find the next Chebyshev coordinate just outside this shell
     */
    unsigned int n = nin>0 ? nin-1 : 0;
    while(coords[n] < qcoord &&
          n < ncheb - 1)
    {
        n++;
    }
    if(unlikely(n>=ncheb)) n = ncheb - 1; /* should not happen */

    /*
     * Interpolation in the Chebyshev grid:
     * either linear or power law
     */
#define _interpolate_linear(F,INDEX)            \
    ((1.0-(F))*result_cheb[(INDEX)+n-1] +       \
     (F)*result_cheb[(INDEX)+n])
#define _interpolate_powerlaw(F,INDEX)                  \
    (exp10(                                             \
        ((1.0-(F))*log10(result_cheb[(INDEX)+n-1]) +    \
         (F)*log10(result_cheb[(INDEX)+n]))             \
        ))
#define _interpolate(F,INDEX)                   \
    (_interpolate_powerlaw((F),(INDEX)))

/*
 * __interpolate returns the data or 0.0 if not available
 */
#define __interpolate(INDEX)                    \
    (available[(INDEX)] == TRUE                 \
     ? (_interpolate(f,map[(INDEX)]))           \
     : 0.0)

    /* interpolate between Chebyshev coordinate n-1 and n */
    const double f = Limit_range((qcoord - coords[n-1]) / (coords[n] - coords[n-1]),
                                 0.0,1.0);
    shell->T = __interpolate(MINT_GB_CHEBYSHEV_TEMPERATURE);
    shell->rho = __interpolate(MINT_GB_CHEBYSHEV_DENSITY);
    shell->total_pressure = __interpolate(MINT_GB_CHEBYSHEV_TOTAL_PRESSURE);
    shell->gas_pressure = __interpolate(MINT_GB_CHEBYSHEV_GAS_PRESSURE);
    shell->radius = __interpolate(MINT_GB_CHEBYSHEV_RADIUS);
    shell->radiation_pressure = shell->total_pressure - shell->gas_pressure;
    shell->gamma1 = __interpolate(MINT_GB_CHEBYSHEV_GAMMA1);
    shell->pressure_scale_height = __interpolate(MINT_GB_CHEBYSHEV_PRESSURE_SCALE_HEIGHT);

    /*
     * Prevent table problems
     */
    shell->rho = Max(1e-20,shell->rho);
    shell->T = Max(300.0,shell->T);

    return n;
}

#endif // MINT
