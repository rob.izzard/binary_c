#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef MINT
void MINT_clear(struct stardata_t * const stardata)
{
    /*
     * Clear MINT's memory
     */
    Foreach_star(star)
    {
        if(star->mint != NULL)
        {
            memset(star->mint,0,sizeof(struct mint_t));
        }
    }
}
#endif//MINT
