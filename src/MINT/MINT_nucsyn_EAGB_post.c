#include "../binary_c.h"

No_empty_translation_unit_warning;

#if defined MINT && defined NUCSYN

void MINT_nucsyn_EAGB_post(struct stardata_t * Restrict const stardata Maybe_unused,
                          struct star_t * Restrict const star Maybe_unused)
{

}
#endif // MINT && NUCSYN
