#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef MINT

/*
 * Instantaenous convective mixing
 */

void MINT_convection(struct stardata_t * const stardata Maybe_unused,
                     struct star_t * const star Maybe_unused)
{
#ifdef NUCSYN
    struct mint_shell_t * mixed_region = MINT_new_clean_shell;
    const Boolean vb = FALSE;

    if(vb)
    {
        printf("pre conv %d shells\n",star->mint->nshells);
        for(Shell_index i=star->mint->nshells-1; i>=star->mint->nshells-6; i--)
        {
            printf("shell %d : mu = %g\n",
                   i,
                   nucsyn_molecular_weight(star->mint->shells[i].X,
                                           stardata,
                                           1.0));
        }
    }
#endif // NUCSYN


#ifdef NUCSYN
    /*
     * Do the mixing of convective zones
     */
    Shell_index start = -1, end = -1;
    for(Shell_index i=0; i<star->mint->nshells; i++)
    {
        struct mint_shell_t * const shell = &star->mint->shells[i];
        if(shell->convective == TRUE)
        {
            mixed_region->dm += shell->dm;
            for(Isotope j=0; j<ISOTOPE_ARRAY_SIZE; j++)
            {
                mixed_region->X[j] += shell->dm * shell->X[j];
            }
            if(start == -1)
            {
                start = i;
            }
        }


        if(start != -1 &&
           (shell->convective == FALSE ||
            i == star->mint->nshells - 1))
        {
            /*
             * We were convective, now radiative or at
             * the surface. Mix from start to end inclusive.
             */
            end = i-1;
            for(Isotope k=0; k<ISOTOPE_ARRAY_SIZE; k++)
            {
                mixed_region->X[k] /= mixed_region->dm;
            }
            for(Shell_index j=start; j<=end; j++)
            {
                memcpy(star->mint->shells[j].X,
                       mixed_region->X,
                       ISOTOPE_MEMSIZE);
            }
            start = -1;
            end = -1;
            if(i != star->mint->nshells - 1)
            {
                mixed_region->dm = 0.0;
                memset(mixed_region->X,0,ISOTOPE_MEMSIZE);
            }
        }
    }

    if(vb)
    {
        printf("post conv %d shells\n",star->mint->nshells);
        for(Shell_index i=star->mint->nshells-1; i>=star->mint->nshells-6; i--)
        {
            printf("shell %d : mu = %g\n",
                   i,
                   nucsyn_molecular_weight(star->mint->shells[i].X,
                                           stardata,
                                           1.0));
        }
    }

    Safe_free(mixed_region);
#endif // NUCSYN
}
#endif // MINT
