#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef MINT
#ifdef __EXPERIMENTAL

void MINT_save_state(struct stardata_t * const stardata Maybe_unused)
{


    /*
     * Save the state of MINT to a file
     */
    struct binary_c_file_t * bfp = binary_c_fopen(stardata,
                                                  stardata->preferences->MINT_save_state_file,
                                                  "w",
                                                  0);
    FILE * fp = bfp->fp;

    /* data tables */
    for(int i=0; i<MINT_TABLE_NUMBER; i++)
    {
        write_data_table_to_file(stardata,
                                 bfp,
                                 stardata->store->MINT_tables[i]);
        printf("Saved unmapped table %d %p\n",
               i,
               (void*)stardata->store->MINT_tables[i]);
    }
    for(int i=0; i<MINT_TABLE_NUMBER; i++)
    {
        write_data_table_to_file(stardata,
                                 bfp,
                                 stardata->persistent_data->MINT_mapped_tables[i]);
        printf("Saved mapped table %d %p\n",
               i,
               (void*)stardata->persistent_data->MINT_mapped_tables[i]);
    }

    /* headers */
    for(int i=0; i<MINT_TABLE_NUMBER; i++)
    {
        struct mint_header_t * const h = stardata->store->MINT_metadata[i]->header;
        const Boolean have_h = h == NULL ? FALSE : TRUE;
        fwrite(&have_h,sizeof(Boolean),1,fp);

        if(have_h == TRUE)
        {
            char * buffer = NULL;
            size_t buffer_size = 0;
            CDict_to_JSON(h->cdict,
                          buffer,
                          buffer_size,
                          ",",
                          ":",
                          CDICT_JSON_NO_INDENT,
                          CDICT_JSON_NO_NEWLINES,
                          FALSE);
            fwrite(&buffer_size,sizeof(size_t),1,fp);
            fwrite(buffer,sizeof(char),buffer_size,fp);
            Safe_free(buffer);
            const size_t filename_len = h->filename == NULL ? 0 : strlen(h->filename);
            fwrite(&filename_len,sizeof(size_t),1,fp);
            fwrite(h->filename,filename_len,1,fp);
        }
    }

    /* data maps */
    fwrite(stardata->store->MINT_data_map_size,
           sizeof(size_t),
           MINT_TABLE_NUMBER,
           fp);
    for(int i=0; i<MINT_TABLE_NUMBER; i++)
    {
        const Boolean have_map = stardata->store->MINT_data_map[i] == NULL ? FALSE : TRUE;
        fwrite(&have_map,sizeof(Boolean),1,fp);
        if(have_map == TRUE)
        {
            fwrite(stardata->store->MINT_data_map[i],
                   sizeof(unsigned int),
                   stardata->store->MINT_data_map_size[i],
                   fp);
        }
    }

    fwrite(stardata->store->MINT_tables_nscalars,
           sizeof(unsigned int),
           MINT_TABLE_NUMBER,
           fp);
    fwrite(stardata->store->MINT_initial_coordinate,
           sizeof(Abundance),
           MINT_TABLE_NUMBER,
           fp);

    /* generic map */
#undef X
#define X(NAME) #NAME,
    static const char * generic_strings[] = { MINT_DATA_TYPES_LIST };
    static const size_t n_generic_strings = Array_size(generic_strings);
#undef X
    fwrite(&n_generic_strings,
           sizeof(size_t),
           1,
           fp);
    for(int i=0; i<NUMBER_OF_STELLAR_TYPES; i++)
    {
        const Boolean have_map = stardata->store->MINT_generic_map[i] == NULL ? FALSE : TRUE;
        fwrite(&have_map,sizeof(Boolean),1,fp);
        if(have_map == TRUE)
        {
            fwrite(stardata->store->MINT_generic_map[i],
                   sizeof(int),
                   n_generic_strings,
                   fp);
        }
    }

    /* data available */
    fwrite(stardata->store->MINT_data_available_size,
           sizeof(size_t),
           MINT_TABLE_NUMBER,
           fp);
    for(int i=0; i<MINT_TABLE_NUMBER; i++)
    {
        const Boolean have_available = stardata->store->MINT_data_available[i] == NULL ? FALSE : TRUE;
        fwrite(&have_available,sizeof(Boolean),1,fp);
        if(have_available == TRUE)
        {
            fwrite(stardata->store->MINT_data_available[i],
                   sizeof(unsigned int),
                   stardata->store->MINT_data_available_size[i],
                   fp);
        }
    }

    binary_c_fclose(&bfp);

}
#endif // __EXPERIMENTAL
#endif // MINT
