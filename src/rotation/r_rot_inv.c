#include "../binary_c.h"
No_empty_translation_unit_warning;


Constant_function double r_rot_inv(double f)
{
    /*
     * inverse of the r_rot function : given a bulge
     * fraction (f), calculate the spin rate
     */
    f=Max(0.0,Min(log10(1.5),f));

    /*
     * fit good to RMS 0.000115
     */
    const double rinv = 9.71220e-05+1.32160e+00*pow(1.0-Pow2(f/0.2-1.0),5.10440e-01)+-1.92740e+00*f+-4.14510e+00*Pow2(f)+2.89750e+01*Pow3(f);

    return (rinv);
}
