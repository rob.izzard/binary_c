#pragma once
#ifndef ROTATION_PROTOTYPES_H
#define ROTATION_PROTOTYPES_H

double Pure_function omegaspin(struct stardata_t * const stardata,
                               const struct star_t * Restrict const star);

Constant_function double Lang_initial_rotation(const double m);

Constant_function double r_rot(double f);

Constant_function double r_rot_inv(double f);

void calculate_rotation_variables(struct star_t * Restrict const star,
                                  const double radius);

double moment_of_inertia(struct stardata_t * const stardata,
                         const struct star_t * Restrict const star,
                         const double r);

double Pure_function core_moment_of_inertia(const struct star_t * Restrict const star,
                                            const double core_radius,
                                            Core_type core_type);

double envelope_moment_of_inertia(const struct star_t * Restrict const star,
                                  const double r);

double Pure_function Kerr_max_angular_momentum(const double mass);

double Pure_function Kerr_max_angular_momentum_cgs(const double mass);

double Constant_function bh_angular_velocity(const double M,
                                             const double J);

Boolean Pure_function stellar_structure_giant_branch_helium_ignition(struct stardata_t * const stardata,
                                                                     struct star_t * const star);

double breakup_angular_momentum(struct stardata_t * const stardata,
                                struct star_t * Restrict const star);

double Pure_function stellar_angular_momentum(const struct star_t * Restrict const star,
                                              const int of_what,
                                              const double radius,
                                              const double core_radius);

double Rossby_number(struct stardata_t * stardata Maybe_unused,
                     struct star_t * const star);

#endif // ROTATION_PROTOTYPES_H
