#include "../binary_c.h"
No_empty_translation_unit_warning;


/*
 * Calculate the moment of inertia of a star's envelope
 *
 * This assumes the core contributes little to the moment of inertia.
 */

double envelope_moment_of_inertia(const struct star_t * Restrict const star,
                                  const double r)
{
    return star->moment_of_inertia_factor * envelope_mass(star) * Pow2(r);
}
