#include "../binary_c.h"
No_empty_translation_unit_warning;


/*
 * Calculate the moment of inertia of a star
 */

static double BH_MOI(const struct star_t * Restrict const star);
static double neutron_star_MOI(const struct star_t * Restrict const star,
                               const double r);
static double normal_star_MOI(const struct star_t * Restrict const star,
                              const double r);


double moment_of_inertia(struct stardata_t * const stardata,
                         const struct star_t * Restrict const star,
                         const double r)
{
    double momI =
        star->stellar_type == MASSLESS_REMNANT ? TINY :
        star->stellar_type == BLACK_HOLE ? BH_MOI(star) :
        star->stellar_type == NEUTRON_STAR ? neutron_star_MOI(star,r) :
        normal_star_MOI(star,r);

    if(stardata->preferences->apply_Darwin_Radau_correction == TRUE)
    {
        /*
         * EXPERIMENTAL! (maybe even wrong!)
         *
         * Darwin-Radau corretion todata into account that
         * the star is rotating.
         *
         * References at
         * https://en.wikipedia.org/wiki/Darwin%E2%80%93Radau_equation
         */

        if(Is_not_zero(star->omega_crit))
        {
            /* Re/Rp */
            const double Re_Rp_ratio = r_rot(star->omega/star->omega_crit);

            if(!Fequal(Re_Rp_ratio,1.0))
            {
                /* epsilon = 1.0 - Rp/Re */
                double epsilon = 1.0 - 1.0/Re_Rp_ratio;

                /* q = geodynamical constant */
                double q = Pow2(star->omega / YEAR_LENGTH_IN_SECONDS) *
                    Pow3(star->radius * R_SUN) /
                    (GRAVITATIONAL_CONSTANT * star->mass * M_SUN);

                /* Radau parameter */
                const double eta = 5.0/2.0 * q / epsilon - 2.0;

                const double f = (1.0 - 2.0/5.0 * sqrt(Max(0.0,1.0 + eta)));

                printf("Re_Rp_ratio %30.20e \n",Re_Rp_ratio);
                printf("epsilon %g\n",epsilon);
                printf("q %g\n",q);
                printf("5/2 q/epsilon %g\n",5.0/2.0*q/epsilon);
                printf("eta %g\n",eta);

                printf("multiplier %g\n",
                       f);
                momI *= f;
            }
        }
    }

    return momI;
}

static double neutron_star_MOI(const struct star_t * Restrict const star,
                               const double r)
{    /*
     * Based on Ravenhall and Pethick, 1994
     * ApJ 484, 846, Eq. 6.
     *
     * NB factor x is dimensionless.
     */
    const double x = GRAVITATIONAL_CONSTANT * star->mass * M_SUN /
        (r * R_SUN * Pow2(SPEED_OF_LIGHT));
    return CORE_MOMENT_OF_INERTIA_FACTOR * star->mass * Pow2(star->radius) / Max(1e-20,1.0 - 2.0 * x);
}

static double BH_MOI(const struct star_t * Restrict const star)
{
    /*
     * Hard to define a moment of inertia for a BH,
     * but we know its angular momentum (code units) and spin rate (/y).
     *
     * Using https://arxiv.org/ftp/physics/papers/0608/0608080.pdf
     */
    const double Rg = GRAVITATIONAL_CONSTANT * star->mass * M_SUN / Pow2(SPEED_OF_LIGHT); // Eq. 3
    const double J = Angular_momentum_code_to_cgs(star->angular_momentum);
    const double M = star->mass * M_SUN;
    const double a = J / (M * SPEED_OF_LIGHT); // Eq. 4
    const double cosphi = Limit_range(a / Rg,-1.0,1.0); // Eq.12 //star->angular_momentum / Kerr_max_angular_momentum(star->mass);
    const double phi = acos(cosphi);

    /*
     * Use outer event horizon hence Rz+
     */
    const double Rz_plus = Rg * sqrt(2.0 * (1.0 + sin(phi))); // Eq. 15
    const double I = M * Pow2(Rz_plus) / (M_SUN * Pow2(R_SUN)); // Eq. 17
    return I;
}

static double normal_star_MOI(const struct star_t * Restrict const star,
                              const double r)
{
    /*
     * Moment of inertia of a "normal" star
     */
    const double Ienv = envelope_moment_of_inertia(star,r);
    const double Icore = core_moment_of_inertia(star,star->core_radius,-1);
    return Ienv + Icore;
}
