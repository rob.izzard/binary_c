#include "../binary_c.h"
No_empty_translation_unit_warning;


#define EPSILON1 1e-10

void calculate_rotation_variables(struct star_t * Restrict const star,
                                  const double radius)
{
    /*
     * Function to calculate a star's:
     *
     * Breakup spin rate (omega_crit)
     *
     * Rotational velocity and breakup rotational velocity both
     * at the equator (calculating the deformation) and assuming
     * the star is a sphere
     *
     * The radius parameter allows calculation at an
     * alternative radius: usually you just want the radius
     * of the star, but if the star is confined to its Roche lobe,
     * perhaps you want to use the Roche radius instead.
     */

    /*
     * Calculate the critical spin rate (/yr)
     * Note that the code stellar spin rates are stored PER YEAR,
     * so we convert the stored value (star->omega_crit) for use elsewhere.
     * omega(/yr)/YEAR_LENGTH_IN_SECONDS = omega(/s)
     *
     * Note factor 1.5 due to equatorial bulge.
     *
     * ir means "Inverse radius"
     */
    if(likely(Is_not_zero(radius)))
    {
        const double G_m_cgs = GRAVITATIONAL_CONSTANT*star->mass*M_SUN;
        const double ir_cgs_sph  = 1.0 / (radius*R_SUN);
        const double ir_cgs_deformed = ir_cgs_sph / 1.5;
        star->omega_crit = YEAR_LENGTH_IN_SECONDS * sqrt(G_m_cgs*Pow3(ir_cgs_deformed));
        star->omega_crit_sph = YEAR_LENGTH_IN_SECONDS * sqrt(G_m_cgs*Pow3(ir_cgs_sph));

#ifdef OMEGA_CORE
        const double G_mc_cgs = GRAVITATIONAL_CONSTANT*star->mass*M_SUN;
        const double ir_core_cgs_sph  = 1.0 / (star->core_radius*R_SUN);
        const double ir_core_cgs_deformed = ir_core_cgs_sph / 1.5;
        star->omega_core_crit = YEAR_LENGTH_IN_SECONDS * sqrt(G_mc_cgs*Pow3(ir_core_deformed));
        star->omega_core_crit_sph = YEAR_LENGTH_IN_SECONDS * sqrt(G_mc_cgs*Pow3(ir_core_cgs_sph));
#endif //OMEGA_CORE

        /*
         * From this calculate the critical velocity as a sphere and
         * at the equator, noting the luminosity Gamma factor
         *
         * Normally we assume stars are spherical, so f=1,
         * but in the case of stars rotating at critical, f=1.5
         */
        const double Gamma = Min(1.0 - EPSILON1,
                                 gamma_rot(star->mass,
                                           star->luminosity));

        /* critical velocities (km/s) */
        star->v_crit_sph = sqrt(G_m_cgs*(1.0-Gamma)*ir_cgs_sph)*1e-5;
        star->v_crit_eq  = star->v_crit_sph / sqrt(1.5);

        /*
         * calculate the spin velocity of the star assuming it is spherical (km/s)
         *
         * NB OMEGA_FROM_VKM = YEAR_LENGTH_IN_SECONDS*1e5/R_SUN ~ 45.35
         */
        star->v_sph = radius * star->omega / OMEGA_FROM_VKM;

        /* calculate the spin velocity of the star at the equator */
        star->v_eq = Is_zero(star->omega_crit) ? 0.0 : // should not happen!
            (r_rot(star->omega/star->omega_crit) * star->v_sph);

        star->v_eq  = Max(1e-10, star->v_eq);
        star->v_sph = Max(1e-10, star->v_sph);

        /* ratios, used everywhere */
        star->v_sph_ratio = Is_zero(star->v_crit_sph) ? 0.0:
            (star->v_sph / star->v_crit_sph);
        star->v_eq_ratio  = Is_zero(star->v_crit_eq) ? 0.0 :
            (star->v_eq / star->v_crit_eq);
        star->omega_ratio = Is_zero(star->omega_crit) ? 0.0 :
            (star->omega / star->omega_crit);

        /*
          fprintf(stderr,
          "ROTV %d st=%d : J=%g M=%g R=%g omega=%g omega_crit=%g (f=%g) v_sph=%g v_eq=%g (fac=%g) v_crit_sph=%g v_crit_eq=%g, v_sph_ratio=%g v_eq_ratio=%g\n",
          star->starnum,
          star->stellar_type,
          star->angular_momentum,
          star->mass,
          radius,
          star->omega,
          star->omega_crit,
          star->omega_ratio,
          star->v_sph,
          star->v_eq,
          r_rot(star->omega/star->omega_crit),
          star->v_crit_sph,
          star->v_crit_eq,
          star->v_sph_ratio,
          star->v_eq_ratio
          );
        */

        star->omega_crit = Max(star->omega_crit, 1e-14);
#ifdef OMEGA_CORE
        star->omega_core_crit = Max(star->omega_core_crit,1e-14);
#endif//OMEGA_CORE
    }
}
