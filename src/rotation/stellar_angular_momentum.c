#include "../binary_c.h"
No_empty_translation_unit_warning;


/*
 * Function to calculate the angular momentum of the envelope,
 * core or the whole star, assuming star->omega or star->omega_core
 * is correct.
 *
 * When there is no envelope, e.g. white dwarfs, neutron stars,
 * black holes, main sequence, we follow the BSE convention and
 */

#ifdef CORE_OMEGA
#define omega_core star->omega_core
#else
#define omega_core star->omega
#endif
#define omega_env star->omega

double Pure_function stellar_angular_momentum(const struct star_t * star,
                                              const int of_what,
                                              const double radius,
                                              const double core_radius)
{
    double J;
    if(of_what == ANGULAR_MOMENTUM_ENVELOPE)
    {
        /*
         * Return the angular momentum of the envelope
         */
        J = envelope_moment_of_inertia(star,radius) * omega_env;
    }
    else if(of_what == ANGULAR_MOMENTUM_CORE)
    {
        /*
         * Return the angular momentum of the core
         */
        if(HAS_BSE_CORE(star->stellar_type))
        {
            J = core_moment_of_inertia(star,
                                       core_radius,
                                       Outermost_core_type(star)) * omega_core;
        }
        else
        {
            J = 0.0;
        }
    }
    else
    {
        /*
         * Assume we want the whole star
         */
        J = envelope_moment_of_inertia(star,radius) *
            omega_env
            +
            core_moment_of_inertia(star,
                                   core_radius,
                                   Outermost_core_type(star)) *
            omega_core;
    }
    return J;
}
