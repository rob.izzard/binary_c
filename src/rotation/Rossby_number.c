#include "../binary_c.h"
No_empty_translation_unit_warning;

double Rossby_number(struct stardata_t * stardata Maybe_unused,
                     struct star_t * const star)
{
    const double tc = convective_turnover_time(star->radius,
                                               star,
                                               stardata);

    /*
     * Rossby number
     *
     * Conveniently, star->omega has units y^-1
     * and tc is in years too.
     */
    return 2.0 * PI / (star->omega * tc);
}
