#include "../binary_c.h"
No_empty_translation_unit_warning;


Constant_function double r_rot(double f)
{
    /*
     * Given the ratio of spin rates f=omega/omega_crit
     * this is the ratio of equatorial to polar radius.
     *
     * The polar radius is the same as the non-rotating radius
     * to within 2%. (See Maeder's book chapter 2)
     *
     * Data from Selma de Mink (thanks!)
     */
    f = Max(0.0, Min(1.0, f) );
    const double rr =
        exp10(
            (
                1.75390e-02 * tan(1.42200e+00 * f) +
                4.51100e-02 * sin(f)
                )
            * f);
    return rr;
}
