#include "../binary_c.h"
No_empty_translation_unit_warning;


//#define BDEBUG

#ifdef BDEBUG
#define Bprint(...) printf(__VA_ARGS__)
#else
#define Bprint(...)
#endif

double Constant_function bh_angular_velocity(const double M,
                                             const double J)
{
    /*
     * Given a "star", which is actually a black hole,
     * we knows its mass (M/Msun) and angular momentum
     * (J/code units).
     *  
     * Calculate and return its angular velocity, omega
     * (y^-1, the code unit).
     *
     * Assume the black hole has no charge, M>0 and J>=0.
     *
     * If J>Jmax, the angular momentum is truncated to Jmax.
     */

    /*
     * Convert angular momentum and mass to cgs
     */
    const double Jcgs = Angular_momentum_code_to_cgs(J);
    const double Mcgs = M * M_SUN;
    Bprint("cgs : J = %g M = %g\n",Jcgs,Mcgs);

    /*
     * convert to Planck units (c=hbar=G=1)
     */
    double j = Jcgs / PLANCK_CONSTANT_BAR;
    double m = Mcgs / PLANCK_MASS;
    Bprint("planck : j = %g m = %g\n",j,m);
    
    /*
     * Maximum angular momentum the black hole can have
     */
    const double jmax = Pow2(m);
    Bprint("jmax = %g -> j/jmax = %g\n",jmax,j/jmax);

    if(j>jmax)
    {
        Bprint("limiting j to jmax\n");
        j = jmax;
    }

    /* 
     * Calculate parameter a = j / m
     */ 
    const double a = j / m;
    Bprint("hence a = %g\n",a);
    
    /*
     * Take the outer radius to be the observable horizon.
     * See e.g. https://arxiv.org/pdf/0706.0622.pdf Eq. 104 
     */
    const double r =  m + sqrt(Max(0.0,Pow2(m) - Pow2(a)));
#ifdef BDEBUG
    const double rm =  m + sqrt(Max(0.0,Pow2(m) - Pow2(a)));
    const double rSchwarzschild = 2.0 * m;
#endif
    Bprint("radius r = %g = %g Rs\n",
           r,
           r / rSchwarzschild);

    /*
     * Hence omega, the spin angular velocity per Planck time.
     * See e.g. https://arxiv.org/pdf/0706.0622.pdf Eq. 112
     */
    double omega = a/(2.0 * m * r); // = a/(Pow2(a) + Pow2(r));
#ifdef BDEBUG
    double omegam = a/(2.0 * m * rm);
#endif
    Bprint("omega+ = %g, omega- = %g (vrot = %g km/s cf c = %g km/s)\n",
           omega,
           omegam,
           r*PLANCK_LENGTH * omega/PLANCK_TIME * 1e-5,
           SPEED_OF_LIGHT*1e-5
        );
    
    /*
     * Convert to per year 
     */
    omega *= YEAR_LENGTH_IN_SECONDS / PLANCK_TIME;
    Bprint("omega = %g y^-1\n",omega);

    return omega;
}
