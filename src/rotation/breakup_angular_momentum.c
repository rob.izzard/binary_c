#include "../binary_c.h"
No_empty_translation_unit_warning;


double breakup_angular_momentum(struct stardata_t * const stardata,
                                struct star_t * Restrict const star)
{
    /*
     * Return the maximum angular momentum this star can hold
     * (without a change in its structure)
     *
     * Returns in code units for angular momentum
     */
    double J_breakup;
    if(star->stellar_type ==  BLACK_HOLE)
    {
        J_breakup = Kerr_max_angular_momentum(star->mass);
        Dprint("st = %d -> Kerr %g\n",
               star->stellar_type,
               J_breakup
            );
    }
    else
    {
        const double r = star->radius > star->roche_radius ?
            star->effective_radius : star->radius;
        calculate_rotation_variables(star,r);
        const double I = moment_of_inertia(stardata,star,r);
        J_breakup = I * star->omega_crit;
        Dprint("st = %d, r = %g, I = %g, omega_crit = %g -> %g\n",
               star->stellar_type,
               r,
               I,
               star->omega_crit,
               J_breakup
            );
    }
    return J_breakup;
}

#ifdef __EXPERIMENTAL_CODE
double core_breakup_angular_momentum(struct star_t * Restrict const star)
{
    /*
     * Return the maximum angular momentum the core can hold
     */
#ifdef OMEGA_CORE
#define omega_core star->omega_core
#else
#define omega_core star->omega
#endif // OMEGA_CORE

    if(star->stellar_type == BLACK_HOLE)
    {
        return Kerr_max_angular_momentum(star->mass);
    }
    else
    {
        return core_moment_of_inertia(star,star->core_radius) *
            omega_core;
    }
}
#endif
