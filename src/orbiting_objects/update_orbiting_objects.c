#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef ORBITING_OBJECTS

#define Objlog(...)                             \
    Printf("Objlog: ");                         \
    Printf(__VA_ARGS__);                        \
    Printf("\n");                               \
    Append_logstring(LOG_ORBITING_OBJECTS,      \
                     __VA_ARGS__);              \


void update_orbiting_objects(struct stardata_t * Restrict const stardata)
{
    /*
     * Update the orbits of orbiting objects
     */
    const Boolean is_binary = System_is_binary;

    /*
     * Save previous state
     */
    struct orbiting_object_t * prev_objects =
        copy_orbiting_objects(stardata->common.orbiting_object,
                              stardata->common.n_orbiting_objects);
    const size_t n_prev_objects = stardata->common.n_orbiting_objects;

    Foreach_bound_orbiting_object(o,i)
    {
        /*
         * First, convert to cgs
         */
        o->mass *= M_SUN;
        o->orbit.separation *= R_SUN;
        o->orbit.period *= YEAR_LENGTH_IN_SECONDS;
        o->orbit.angular_frequency /= YEAR_LENGTH_IN_SECONDS;

        o->orbit.angular_momentum = Angular_momentum_code_to_cgs(o->orbit.angular_momentum);

        /*
         * Central mass and angular momentum in cgs
         */
        const double M_c_cgs = M_SUN *
            Central_object_property(o,mass);

        if(o->type == ORBITING_OBJECT_TYPE_CIRCUMBINARY)
        {
            if(is_binary)
            {
                /*
                 * Check for hierarchical orbit stability
                 */
                const double min_stable_sep =
                    minimum_stable_orbit_separation(stardata,
                                                    o,
                                                    M_c_cgs,
                                                    R_SUN * stardata->common.orbit.separation);

                const Boolean stable =
                    o->orbit.separation > min_stable_sep;

                if(stable == FALSE)
                {
                    /*
                     * Eject object
                     */
                    o->type = ORBITING_OBJECT_TYPE_UNBOUND;
                    o->central_object = NULL;
                    Objlog("eject %s orbiting %s because its orbit is no longer hierarchically stable (a=%g amin=%g)",
                           o->name,
                           orbiting_object_central_object_string(stardata,o),
                           o->orbit.separation,
                           min_stable_sep);
                }
                else
                {
                    /*
                     * Stable orbit: update orbital properties
                     */
                    o->orbit.separation =
                        Pow2(o->orbit.angular_momentum / o->mass) /
                        (GRAVITATIONAL_CONSTANT * M_c_cgs * (1.0 - Pow2(o->orbit.eccentricity)));

                    o->orbit.period =
                        2.0 * PI * Pow1p5(o->orbit.separation) /
                        sqrt(GRAVITATIONAL_CONSTANT * M_c_cgs);
                }
            }
            else
            {
                /*
                 * system is no longer binary : object
                 * is ejected or becomes circumstellar
                 */
                o->central_object =
                    stardata->star[0].stellar_type != MASSLESS_REMNANT ? &stardata->star[0] :
                    stardata->star[1].stellar_type != MASSLESS_REMNANT ? &stardata->star[1] :
                    NULL;
                if(o->central_object)
                {
                    o->type = ORBITING_OBJECT_TYPE_CIRCUMSTELLAR;
                }
                Objlog("system is no longer binary, object %s becomes type %u orbiting %s",
                       o->name,
                       o->type,
                       orbiting_object_central_object_string(stardata,o)
                    );
            }
        }
        else if(o->type == ORBITING_OBJECT_TYPE_CIRCUMSTELLAR)
        {
            /*
             * We're orbiting a star
             */
            o->orbit.separation =
                Pow2(o->orbit.angular_momentum / o->mass) /
                (GRAVITATIONAL_CONSTANT * M_c_cgs * (1.0 - Pow2(o->orbit.eccentricity)));

            o->orbit.period =
                2.0 * PI * Pow1p5(o->orbit.separation) /
                sqrt(GRAVITATIONAL_CONSTANT * M_c_cgs);
        }

        /*
         * If the central mass has changed, while angular
         * momentum of the object stays the same, its
         * total energy has changed so we must recalculate its
         * eccentricity.
         *
         * E = mu^2 / (2.0 * h^2) * (e^2 - 1)
         */
        //o->orbit.eccentricity =

        /*
         * Check for transition outside the Roche lobe
         */
        if(is_binary &&
           o->type == ORBITING_OBJECT_TYPE_CIRCUMSTELLAR)
        {
            const double RL = Central_object_property(o,roche_radius);
            if(o->orbit.separation > RL)
            {
                if(stardata->preferences->RLOF_transition_objects_escape == TRUE)
                {
                    /*
                     * Object escapes the system
                     */
                    o->type = ORBITING_OBJECT_TYPE_UNBOUND;
                    o->central_object = NULL;
                    Objlog("circumstellar object %s has escaped its Roche lobe and been ejected",
                           o->name);
                }
                else
                {
                    /*
                     * Object is placed on innermost
                     * stable orbit (+ 5%)
                     */
                    const double Mbin_cgs = Central_object_property(o,mass) * M_SUN;
                    const double min_stable_sep =
                        minimum_stable_orbit_separation(stardata,
                                                        o,
                                                        Mbin_cgs,
                                                        R_SUN * stardata->common.orbit.separation);

                    o->type = ORBITING_OBJECT_TYPE_CIRCUMBINARY;
                    o->orbit.separation = 1.05 * min_stable_sep;
                    o->orbit.eccentricity = 0.0;
                    o->orbit.period =
                        2.0 * PI * Pow1p5(o->orbit.separation) /
                        sqrt(GRAVITATIONAL_CONSTANT * Mbin_cgs);
                    o->orbit.angular_frequency = 2.0  * PI / o->orbit.period;
                    o->orbit.angular_momentum = orbiting_system_angular_momentum_cgs(stardata, o);
                    o->central_object = stardata;
                    Objlog("circumstellar object %s has escaped its Roche lobe and placed on the innermost orbit",
                           o->name);
                }
            }
        }

        /*
         * convert to code units
         */
        o->mass /= M_SUN;
        o->orbit.separation /= R_SUN;
        o->orbit.period /= YEAR_LENGTH_IN_SECONDS;
        o->orbit.angular_momentum = Angular_momentum_cgs_to_code(o->orbit.angular_momentum);
        o->orbit.angular_frequency = 2.0 * PI / o->orbit.period;
    }

    /*
     * Check for destroyed ("evaporated") objects.
     *
     * Note that when we evaporate an object, we should break
     * the loop immediately and restart, because the pointers
     * may have changed.
     */
    Boolean restart = TRUE;
    while(restart == TRUE)
    {
        restart = FALSE;
        Foreach_orbiting_object(o,i)
        {
            if(o->central_object == NULL &&
               stardata->preferences->evaporate_escaped_orbiting_objects)
            {
                Objlog("object %s orbiting %s has been destroyed",
                       o->name,
                       orbiting_object_central_object_string(stardata,o));

                evaporate_orbiting_object(stardata,i);
                restart = TRUE;
                break;
            }
        }
    }

    /*
     * Check for collision at periastron
     */
    Foreach_bound_orbiting_object(o,i)
    {
        const double a_c = Central_object_size(o);

        if(o->orbit.separation * (1.0 - o->orbit.eccentricity) <= a_c)
        {
            Objlog("object %s orbiting %s has collided with its central object at periastron",
                   o->name,
                   orbiting_object_central_object_string(stardata,o));
            evaporate_orbiting_object(stardata,i);
            i--;
        }
    }

    /*
     * Check for orbit crossing
     */
    if(prev_objects != NULL)
    {
        /*
         * Filter objects that are orbiting the same
         * central object
         */
        Foreach_bound_orbiting_object(this,i)
        {
            Foreach_bound_orbiting_object(that,j)
            {
                if(this != that &&
                   this->central_object == that->central_object)
                {
                    /*
                     * Get the previous version of this and that
                     */
                    struct orbiting_object_t * const prev_this =
                        locate_orbiting_object(stardata,
                                               this->name,
                                               this->central_object,
                                               prev_objects);
                    if(prev_this != NULL)
                    {
                        struct orbiting_object_t * const prev_that =
                            locate_orbiting_object(stardata,
                                                   that->name,
                                                   that->central_object,
                                                   prev_objects);

                        if(prev_that != NULL)
                        {
                            /*
                             * See if the orbit of that crossed the orbit
                             * this.
                             */
                            if((this->orbit.separation > that->orbit.separation &&
                                prev_this->orbit.separation < prev_that->orbit.separation) ||
                               (this->orbit.separation < that->orbit.separation &&
                                prev_this->orbit.separation > prev_that->orbit.separation))
                            {
                                Objlog("Orbits of %s and %s cross : was %g, %g (%s %s, %u %u) now %g, %g (%s %s, %u %u)",
                                       this->name,
                                       that->name,
                                       prev_this->orbit.separation,
                                       prev_that->orbit.separation,
                                       orbiting_object_central_object_string(stardata,prev_this),
                                       orbiting_object_central_object_string(stardata,prev_that),
                                       prev_this->type,
                                       prev_that->type,
                                       this->orbit.separation,
                                       that->orbit.separation,
                                       orbiting_object_central_object_string(stardata,this),
                                       orbiting_object_central_object_string(stardata,that),
                                       this->type,
                                       that->type
                                    );

                                /*
                                 * Eject the least massive
                                 */
                                struct orbiting_object_t * const least_massive_object =
                                    this->mass < that->mass ? this : that;
                                least_massive_object->type = ORBITING_OBJECT_TYPE_UNBOUND;
                                least_massive_object->central_object = NULL;
                            }
                        }
                    }
                }
            }
        }
    }

    /*
     * Check for orbits that are too close to be stable
     * and eject the least massive
     */
    const double threshold = stardata->preferences->orbiting_objects_close_pc_threshold; /* per cent */
    Foreach_bound_orbiting_object(this,i)
    {
        Foreach_bound_orbiting_object(that,j)
        {
            const double pc_diff = Abs_percent_diff(this->orbit.separation,
                                                    that->orbit.separation);
            if(this != that &&
               this->central_object == that->central_object &&
               pc_diff < threshold)
            {
                Objlog("objects %s and %s are too close : separations %g, %g diff %g %% (central objects %s, %s; types %u, %u)",
                       this->name,
                       that->name,
                       this->orbit.separation,
                       that->orbit.separation,
                       pc_diff,
                       orbiting_object_central_object_string(stardata,this),
                       orbiting_object_central_object_string(stardata,that),
                       this->type,
                       that->type
                    );
                struct orbiting_object_t * const least_massive_object =
                    this->mass < that->mass ? this : that;
                least_massive_object->type = ORBITING_OBJECT_TYPE_UNBOUND;
                least_massive_object->central_object = NULL;
            }
        }
    }

    /*
     * Check for unbound ojects
     */
    Foreach_bound_orbiting_object(o)
    {
        /*
         * Central object mass (g)
         */
        const double M_c = M_SUN * Central_object_property(o,mass);

        /*
         * Object mass (g)
         */
        const double m = M_SUN * o->mass;

        /*
         * Reduced mass (g)
         */
        const double mu = m * M_c / (m + M_c);

        /*
         * Angle at which we calculate: 0.0 = periastron
         * (although this should not really matter)
         */
        //const double theta = 0.0;

        /*
         * Distance at periastron (theta=0) (cgs)
         */
        //const double r_peri = orbiting_object_distance_cgs(o,theta);

        /*
         * speed at periastron (cgs)
         */
        //const double v_peri = orbiting_object_velocity_cgs(stardata,o,theta);

        /*
         * Hence total energy of the system
         */
        const double E = orbiting_system_energy_cgs(stardata,o,0.0);

        /*
         * Check for escape.
         */
        if(E > 0.0)
        {
            Objlog("object %s is unbound because it has positive energy %g",
                   o->name,
                   E);
            unbind_orbiting_object(o);
        }
        else
        {
            /*
             * Combined orbiting object + central object angular momentum
             * (cgs)
             */
            const double J = orbiting_system_angular_momentum_cgs(stardata,o);

            /*
             * Specific angular momentum (per unit reduced mass)
             */
            const double h = J / mu;

            /*
             * Hence new eccentricity
             */
            const double e_squared =
                2.0 * (E/mu) * Pow2(h)
                /
                Pow2(GRAVITATIONAL_CONSTANT * (M_c + m))
                + 1.0;

            if(e_squared >= 0.0)
            {
                o->orbit.eccentricity = sqrt(e_squared);
            }
        }
    }

    /*
     * Unbound objects should have their orbit struct
     * values set to -1.0
     */
    Foreach_unbound_orbiting_object(o)
    {
        if(o->type == ORBITING_OBJECT_TYPE_UNBOUND)
        {
            o->central_object = NULL;
            o->orbit.separation = -1.0;
            o->orbit.period = -1.0;
            o->orbit.angular_frequency = -1.0;
            o->orbit.angular_momentum = -1.0;
            o->orbit.eccentricity = -1.0;
            o->orbit.inclination = 0.0;
        }
    }

    /*
     * Free previous objects copy
     */
    free_orbiting_objects(&prev_objects,
                          n_prev_objects);

    /*
     * Check for errors
     */
    Foreach_orbiting_object(o)
    {
        if(isnan(o->orbit.separation) ||
           isnan(o->orbit.angular_frequency) ||
           isnan(o->orbit.angular_momentum))
        {
            Exit_binary_c(BINARY_C_EXIT_NAN,
                          "NAN in sep %g, ang freq %g or ang mom %g\n",
                          o->orbit.separation,
                          o->orbit.angular_frequency,
                          o->orbit.angular_momentum);
        }
        else if((o->type == ORBITING_OBJECT_TYPE_CIRCUMSTELLAR ||
                 o->type == ORBITING_OBJECT_TYPE_CIRCUMBINARY)
                &&
                (Is_zero(o->orbit.angular_frequency) ||
                 Is_zero(o->orbit.angular_momentum)))
        {
            Exit_binary_c(BINARY_C_EXIT_NAN,
                          "Orbiting central object %s but ang freq is %g and ang mom is %g\n",
                          orbiting_object_central_object_string(stardata,o),
                          o->orbit.angular_frequency,
                          o->orbit.angular_momentum);
        }
    }
}

#endif//ORBITING_OBJECTS
