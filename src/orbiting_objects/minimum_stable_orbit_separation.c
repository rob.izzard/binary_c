#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef ORBITING_OBJECTS

double minimum_stable_orbit_separation(struct stardata_t * Restrict const stardata Maybe_unused ,
                                       struct orbiting_object_t * const o,
                                       const double central_object_mass,
                                       const double semi_major_axis)
{
    /*
     * Mardling and Aarseth (2001)
     * MNRAS 321, 398
     *
     * Minimum stable semi-major axis
     * Their Eq. 90
     */
    return
        semi_major_axis *
        minimum_stable_orbit_Mardling_Aarseth_2001(o->orbit.eccentricity,
                                                   o->orbit.inclination,
                                                   o->mass / central_object_mass);
}
#endif
