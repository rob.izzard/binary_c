#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef ORBITING_OBJECTS

/*
 * Add an orbiting object from a command-line
 * argument.
 */


struct orbiting_object_t * add_orbiting_object_from_arg(
    struct stardata_t * Restrict const stardata,
    char * const Restrict argstring)
{
    /*
     * Copy the argstring
     */
    char * argstringwas;
    if(asprintf(&argstringwas,"%s",argstring)>0)
    {
        /*
         * Make the new object
         */
        struct orbiting_object_t * const o = new_orbiting_object(stardata);

        /*
         * Split the arg to give the parameters of the object
         */
        struct string_array_t * string_array = new_string_array(0);
        string_split(stardata,
                     argstring,
                     string_array,
                     ',',
                     0,
                     0,
                     FALSE);

        for(ssize_t i=0; i<string_array->n; i++)
        {
            struct string_array_t * sub_string_array = new_string_array(0);
            string_split(stardata,
                         string_array->strings[i],
                         sub_string_array,
                         '=',
                         0,
                         0,
                         FALSE);
            if(sub_string_array->n == 2)
            {
                /*
                 * Found x=y argument
                 */
                if(sub_string_array->strings[0] != NULL &&
                   sub_string_array->strings[1] != NULL)
                {
#undef _set
#define _set(FUNC,STRING,VAR)                                   \
                    if(Strings_equal(sub_string_array->strings[0],(STRING)))     \
                    {                                           \
                        double value;                           \
                        int errnum;                             \
                        struct unit_t u;                        \
                        string_to_code_units(                   \
                            stardata,                           \
                            sub_string_array->strings[1],                        \
                            &value,                             \
                            &u,                                 \
                            &errnum);                           \
                                                                \
                        o->VAR = value; /* auto cast */         \
                                                                \
                        break;                                  \
                    }

#undef _setf
#undef _seti
#undef _setstring
#define _setf(STRING,VAR) _set(fast_strtod,(STRING),VAR)
#define _seti(STRING,VAR) _set((int)strtol10,(STRING),VAR)

                    do{
                        /*
                         * Floats
                         */
                        _setf("M",mass);
                        _setf("Jspin",spin_angular_momentum);
                        _setf("Jorb",orbit.angular_momentum);
                        _setf("orbital_period",orbit.period); /* years */
                        _setf("orbital_angular_momentum",orbit.angular_momentum);
                        _setf("orbital_eccentricity",orbit.eccentricity);

                        /*
                         * Integers
                         */
                        _seti("type",type);

                        /*
                         * Special cases
                         */
                        if(Strings_equal(sub_string_array->strings[0],"orbital_separation"))
                        {
                            /*
                             * You can set the separation to "innermost_stable"
                             * if the central object is a binary. This is now a
                             * unit but we have to set it manually.
                             */
                            double value = 1.0;
                            struct unit_t unit;
                            int errnum;
                            string_to_code_units(
                                stardata,
                                sub_string_array->strings[1],
                                &value,
                                &unit,
                                &errnum);

                            if(Strings_equal(unit.string,"innermost_stable"))
                            {
                                if(o->central_object == NULL)
                                {
                                    Exit_binary_c(BINARY_C_SETUP_UNKNOWN_ARGUMENT,
                                                  "You want to set up an innermost stable orbit, but need to specify a central object first. Please check your arguments.");
                                }
                                else if(o->central_object != stardata)
                                {
                                    Exit_binary_c(BINARY_C_SETUP_UNKNOWN_ARGUMENT,
                                                  "You want to set up an innermost stable orbit, but the central object is not the binary. Please check your arguments.");
                                }
                                else
                                {
                                    /*
                                     * Central object is the required binary
                                     */
                                    const double M_c_cgs = M_SUN *
                                        Central_object_property(o,mass);

                                    o->orbit.separation =
                                        value * minimum_stable_orbit_separation(stardata,
                                                                              o,
                                                                              M_c_cgs,
                                                                              R_SUN * stardata->common.orbit.separation) / R_SUN;

                                    break;
                                }
                            }
                            else
                            {
                                /*
                                 * Nothing special matched
                                 */
                                o->orbit.separation = value;
                                break;
                            }
                        }
                        else if(Strings_equal(sub_string_array->strings[0],"orbits"))
                        {
                            int starnum = -1;
                            if(Strings_equal(sub_string_array->strings[1],"binary"))
                            {
                                o->central_object = stardata;
                                o->type = ORBITING_OBJECT_TYPE_CIRCUMBINARY;
                                break;
                            }
                            else if(sscanf(sub_string_array->strings[1],"star%d",&starnum))
                            {
                                starnum--;
                                if(In_range(starnum,0,NUMBER_OF_STARS))
                                {
                                    o->central_object = &stardata->star[starnum];
                                    o->type = ORBITING_OBJECT_TYPE_CIRCUMSTELLAR;
                                    break;
                                }
                            }
                            else
                            {
                                Exit_binary_c(BINARY_C_SETUP_UNKNOWN_ARGUMENT,
                                              "%s failed to match a central body for your orbiting object.",
                                              sub_string_array->strings[1]);
                            }
                        }
                        else if(Strings_equal(sub_string_array->strings[0],"name"))
                        {
                            if(asprintf(&(o->name),"%s",sub_string_array->strings[1])==0)
                            {
                                Exit_binary_c(BINARY_C_ALLOC_FAILED,
                                              "asprintf failed.");
                            }
                            break;
                        }

                        Exit_binary_c(BINARY_C_SETUP_UNKNOWN_ARGUMENT,
                                      "Tried to parse orbiting object argument \"%s\" from \"%s\", but failed. Please check it.\n",
                                      string_array->strings[i],
                                      argstringwas);
                    } while(0);
                }
                else
                {
                    Exit_binary_c(BINARY_C_SETUP_UNKNOWN_ARGUMENT,
                                  "Tried to parse orbiting object argument \"%s\" from \"%s\", but failed. Please check it.\n",
                                  string_array->strings[i],
                                  argstringwas);
                }
            }
            else
            {
                Exit_binary_c(BINARY_C_SETUP_UNKNOWN_ARGUMENT,
                              "Tried to parse orbiting object argument \"%s\" from \"%s\", but failed. Please check it.\n",
                              string_array->strings[i],
                              argstringwas);
            }

            free_string_array(&sub_string_array);
        }

        free_string_array(&string_array);
        Safe_free(argstringwas);

        return o;
    }
    else
    {
        return NULL;
    }
}



#endif//ORBITING_OBJECTS
