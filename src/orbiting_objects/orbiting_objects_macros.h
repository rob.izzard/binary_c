#pragma once
#ifndef ORBITING_OBJECTS_MACROS_H
#define ORBITING_OBJECTS_MACROS_H

/*
 * Macros for binary_c's orbiting objects
 */

/*
 * Orbiting object types
 */
enum {
    ORBITING_OBJECT_TYPE_CIRCUMSTELLAR,
    ORBITING_OBJECT_TYPE_CIRCUMBINARY,
    ORBITING_OBJECT_TYPE_UNBOUND,
    ORBITING_OBJECT_TYPE_NUMBER
};


#include "orbiting_objects_function_macros.h"

#endif // ORBITING_OBJECTS_MACROS_H
