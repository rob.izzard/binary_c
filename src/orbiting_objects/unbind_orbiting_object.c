#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef ORBITING_OBJECTS

/*
 * Make object o unbound, but do not evaporate it
 */
void unbind_orbiting_object(struct orbiting_object_t * const Restrict o)
{
    o->type = ORBITING_OBJECT_TYPE_UNBOUND;
    o->orbit.separation = -1.0;
    o->orbit.period = -1.0;
    o->orbit.angular_frequency = -1.0;
    o->orbit.angular_momentum = -1.0;
    o->orbit.eccentricity = -1.0;
    o->central_object = NULL;
}

#endif // ORBITING_OBJECTS
