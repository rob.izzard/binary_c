#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef ORBITING_OBJECTS

/*
 * Make a new orbiting object and return a
 * pointer to it
 */
struct orbiting_object_t * new_orbiting_object(struct stardata_t * const stardata)
{
    stardata->common.orbiting_object =
        Realloc(
            stardata->common.orbiting_object,
            sizeof(struct orbiting_object_t) *
            (stardata->common.n_orbiting_objects + 1)
            );
    memset(stardata->common.orbiting_object + stardata->common.n_orbiting_objects,
           0,
           sizeof(struct orbiting_object_t));
    if(stardata->common.orbiting_object == NULL)
    {
        Exit_binary_c(BINARY_C_MALLOC_FAILED,
                      "Could not allocate memory for new orbiting object\n");
    }
    stardata->common.orbiting_object[stardata->common.n_orbiting_objects].index =
        stardata->common.n_orbiting_objects;
    stardata->common.n_orbiting_objects++;
    return stardata->common.orbiting_object + (stardata->common.n_orbiting_objects-1);
}

#endif//ORBITING_OBJECTS
