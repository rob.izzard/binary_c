#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef ORBITING_OBJECTS

/*
 * Add an orbiting object string to the zero-age list
 * from a command-line argument.
 *
 * This means the object is left in the string stack,
 * but the actual objects are not set up until the
 * binary system's orbit (in stardata->common.orbit) is
 * set up (done later).
 */
void add_zero_age_orbiting_object_from_arg(
    struct stardata_t * Restrict const stardata,
    char * const Restrict argstring)
{
    struct zero_age_system_t * const z =
        &stardata->preferences->zero_age;

    /*
     * Increase the orbiting_object stack's size by 1
     */
    z->n_orbiting_objects++;
    z->orbiting_object =
        Realloc(z->orbiting_object,
                z->n_orbiting_objects * sizeof(char**));

    /*
     * And set the argstring to be processed later
     */
    z->orbiting_object[z->n_orbiting_objects-1]
        = argstring;
}

#endif // ORBITING_OBJECTS
