#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef ORBITING_OBJECTS

double orbiting_object_distance(struct orbiting_object_t * const o,
                                const double theta)
{
    /*
     * Calculate the orbital distance as a function
     * of angle theta (radians), in code units (R_SUN).
     */
    return
        o->orbit.separation *
        (1.0 - Pow2(o->orbit.eccentricity)) /
        (1.0 + o->orbit.eccentricity * cos(theta));

}
#endif // ORBITING_OBJECTS
