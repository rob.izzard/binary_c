#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef ORBITING_OBJECTS

/*
 * Return a (long) string describing the central object
 * of an orbiting object. Note, you shouldn't malloc the
 * string, it's just a constant (compile-time) string literal.
 */
const char * orbiting_object_central_object_string(
    struct stardata_t * Restrict const stardata,
    struct orbiting_object_t * Restrict const o
    )
{
    return
        o->type == ORBITING_OBJECT_TYPE_UNBOUND ? "Unbound" :
        o->type == ORBITING_OBJECT_TYPE_CIRCUMBINARY ? "the binary" :
        o->type == ORBITING_OBJECT_TYPE_CIRCUMSTELLAR ?
        (
            o->central_object == &stardata->star[0] ? "star 1" :
            o->central_object == &stardata->star[1] ? "star 2" :
            (NUMBER_OF_STARS>=3 &&
             o->central_object == &stardata->star[2]) ? "star 3" :
            (NUMBER_OF_STARS>=4 &&
             o->central_object == &stardata->star[3]) ? "star 4" :
            "CS?") :
        "?";
}

#endif // ORBITING_OBJECTS
