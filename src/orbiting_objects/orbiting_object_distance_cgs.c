#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef ORBITING_OBJECTS

double orbiting_object_distance_cgs(struct orbiting_object_t * const o,
                                    const double theta)
{
    /*
     * Wrapper for orbiting_object_distance to return the result
     * in cgs
     */
    return R_SUN *
        orbiting_object_distance(o,theta);

}
#endif // ORBITING_OBJECTS
