#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef ORBITING_OBJECTS
void free_orbiting_objects(struct orbiting_object_t ** objects,
                           const size_t n)
{
    /*
     * Free a set of orbiting objects
     */
    if(objects != NULL && *objects != NULL)
    {
        for(size_t i=0; i < n; i++)
        {
            Safe_free((*objects)[i].name);
        }
        Safe_free(*objects);
    }
}
#endif//ORBITING_OBJECTS
