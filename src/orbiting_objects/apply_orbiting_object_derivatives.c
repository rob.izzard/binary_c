#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef ORBITING_OBJECTS

void apply_orbiting_object_derivatives(
    struct stardata_t * Restrict const stardata,
    const double dt
    )
{
    /*
     * Apply orbiting object derivatives
     */
    Foreach_orbiting_object(o)
    {
        double wanted;
        apply_derivative(stardata,
                         &o->mass,
                         &wanted,
                         o->derivative,
                         dt,
                         DERIVATIVE_ORBITING_OBJECT_MASS,
                         DERIVATIVES_GROUP_ORBITING_OBJECT,
                         NULL,
                         NULL);
        apply_derivative(stardata,
                         &o->orbit.angular_momentum,
                         &wanted,
                         o->derivative,
                         dt,
                         DERIVATIVE_ORBITING_OBJECT_ORBITAL_ANGULAR_MOMENTUM,
                         DERIVATIVES_GROUP_ORBITING_OBJECT,
                         NULL,
                         NULL);
        apply_derivative(stardata,
                         &o->orbit.eccentricity,
                         &wanted,
                         o->derivative,
                         dt,
                         DERIVATIVE_ORBITING_OBJECT_ORBITAL_ECCENTRICITY,
                         DERIVATIVES_GROUP_ORBITING_OBJECT,
                         NULL,
                         NULL);
        apply_derivative(stardata,
                         &o->spin_angular_momentum,
                         &wanted,
                         o->derivative,
                         dt,
                         DERIVATIVE_ORBITING_OBJECT_SPIN_ANGULAR_MOMENTUM,
                         DERIVATIVES_GROUP_ORBITING_OBJECT,
                         NULL,
                         NULL);
    }

    /*
     * Update orbits
     */
    update_orbiting_objects(stardata);
}


#endif//ORBITING_OBJECTS
