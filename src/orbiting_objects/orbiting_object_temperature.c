#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef ORBITING_OBJECTS

double orbiting_object_temperature(struct stardata_t * const stardata,
                                   struct orbiting_object_t * const o)
{
    /*
     * Calculate the average temperature of an orbiting object
     *
     * The factor 0.25 is because pi R^2 / (4.0 * pi * R^2) = 1/4
     */
    const double L_c =
        Central_object_property(o,luminosity);

    return Max(CMB_TEMPERATURE,
               Teff_from_luminosity_and_radius(L_c * 0.25,o->orbit.separation));
}

#endif//ORBITING_OBJECTS
