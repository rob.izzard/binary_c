#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef ORBITING_OBJECTS

void orbiting_objects_derivatives(
    struct stardata_t * Restrict const stardata
    )
{
    /*
     * Calculate orbiting object derivatives
     */
    const Boolean is_binary = System_is_binary;


    Foreach_orbiting_object(o)
    {
        /*
         * Clear derivatives
         */
        o->derivative[DERIVATIVE_ORBITING_OBJECT_MASS] = 0.0;
        o->derivative[DERIVATIVE_ORBITING_OBJECT_ORBITAL_ANGULAR_MOMENTUM] = 0.0;
        o->derivative[DERIVATIVE_ORBITING_OBJECT_ORBITAL_ECCENTRICITY] = 0.0;
        o->derivative[DERIVATIVE_ORBITING_OBJECT_SPIN_ANGULAR_MOMENTUM] = 0.0;

        /*
         * Clear flags
         */
        o->in_disc = FALSE;

        /*
         * Mass derivative
         */
        const double vwind =
            o->type == ORBITING_OBJECT_TYPE_CIRCUMBINARY ?
            Max(stardata->star[0].vwind,
                stardata->star[1].vwind) :
            o->type == ORBITING_OBJECT_TYPE_CIRCUMSTELLAR ?
            ((struct star_t const *)o->central_object)->vwind :
            0.0; // cm/s

        if(is_binary == TRUE)
        {
            const double mdot = Central_object_property(o,derivative[DERIVATIVE_STELLAR_MASS_WIND_LOSS]);
            if(Is_really_not_zero(mdot) &&
               vwind > TINY)
            {
                /*
                 * Bondi-Hoyle-Littleton accretion
                 */
               if(mdot < -REALLY_TINY)
                {
                    const double vorb = orbiting_system_velocity_cgs(stardata,o,0.0); // cm/s
                    o->derivative[DERIVATIVE_ORBITING_OBJECT_MASS] +=
                        Bondi_Hoyle_accretion_rate(stardata,
                                                   M_SUN * o->mass,
                                                   vorb,
                                                   M_SUN / YEAR_LENGTH_IN_SECONDS * mdot,
                                                   vwind,
                                                   R_SUN * o->orbit.separation,
                                                   o->orbit.eccentricity)
                        / (M_SUN / YEAR_LENGTH_IN_SECONDS);
                }
            }

#ifdef DISCS
            /*
             * TODO: disc accretion
             */
            if(stardata->common.ndiscs > 0)
            {
                for(int i=0; i<stardata->common.ndiscs; i++)
                {
                    struct disc_t * d = & stardata->common.discs[i];

                    /*
                     * Check if we orbit inside the disc
                     * (assumes a circular orbit: you'll need
                     *  a more complicated algorithm for the general case)
                     */
                    const double a = o->orbit.separation * R_SUN;
                    if(a > d->Rin &&
                       a < d->Rout)
                    {
                        o->in_disc = TRUE;
                        printf("DISC ACCRETION onto %zu \"%s\" : %g < a=%g < %g\n",
                               o->index,
                               o->name,
                               d->Rin/ASTRONOMICAL_UNIT,
                               a/ASTRONOMICAL_UNIT,
                               d->Rout/ASTRONOMICAL_UNIT
                            );
                    }
                }
            }

#endif
        }


        /*
         * The angular momentum is the sum of that
         * belonging to the orbiting object and the central
         * object. This should decrease as the central object
         * loses mass, for example, although quite how we
         * need to work out
         */
        if(o->type == ORBITING_OBJECT_TYPE_CIRCUMBINARY)
        {
            o->derivative[DERIVATIVE_ORBITING_OBJECT_ORBITAL_ANGULAR_MOMENTUM] +=
                wind_orbital_angular_momentum_derivative(
                    stardata,
                    &o->orbit,
                    (const double[]){Sum_of_stars(mass),o->mass},
                    (const double[]){Sum_of_stars(derivative[DERIVATIVE_STELLAR_MASS_WIND_LOSS]),0.0},
                    (const double[]){Sum_of_stars(derivative[DERIVATIVE_STELLAR_MASS_WIND_GAIN]),0.0},
                    (const double[]){Max_of_stars(vwind),0.0}
                    );
        }

        /*
         * Tidal torques: this function sets the
         * derivatives appropriately
         */
        orbiting_object_tidal_torque(stardata,o);
    }
}


#endif//ORBITING_OBJECTS
