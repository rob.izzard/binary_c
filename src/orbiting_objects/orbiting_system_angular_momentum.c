#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef ORBITING_OBJECTS

double orbiting_system_angular_momentum(struct stardata_t * const stardata,
                                        struct orbiting_object_t * const o)
{
    /*
     * Calculate the orbital angular momentum of the
     * object, o, + central object system, in code units
     */
    const double m = o->mass;
    const double M_c = Central_object_property(o,mass);
    const double mu = m * M_c / (m + M_c);
    return
        mu *
        sqrt(1.0 - Pow2(o->orbit.eccentricity)) *
        Pow2(o->orbit.separation) *
        o->orbit.angular_frequency;
}

#endif // ORBITING_OBJECTS
