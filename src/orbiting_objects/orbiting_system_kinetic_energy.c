#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef ORBITING_OBJECTS

double orbiting_system_kinetic_energy(struct stardata_t * const stardata,
                                      struct orbiting_object_t * const o,
                                      const double theta)
{
    /*
     * Calculate the orbital energy
     * as a function of angle theta, in code units.
     */
    const double vorb = orbiting_system_velocity(stardata,o,theta);
    const double M1 = Central_object_property(o,mass);
    const double M2 = o->mass;
    return 0.5 * M1 * M2 / (M1 + M2) * Pow2(vorb);
}
#endif // ORBITING_OBJECTS
