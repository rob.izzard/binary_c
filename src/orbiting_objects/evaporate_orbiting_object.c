#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef ORBITING_OBJECTS

#define Oprint(...) /* do nothing */
//#define Oprint(...) printf(__VA_ARGS__);

void evaporate_orbiting_object(struct stardata_t * const stardata,
                               size_t i)
{
    /*
     * Evaporate orbiting object i
     */

    /*
     * Erase the name
     */
    Safe_free(stardata->common.orbiting_object[i].name);

    if(stardata->common.n_orbiting_objects == 1)
    {
        /*
         * Only one left : just free
         */
        Safe_free(stardata->common.orbiting_object);
        stardata->common.n_orbiting_objects = 0;
    }
    else
    {
        for(size_t j =0; j<stardata->common.n_orbiting_objects;j++)
        {
            Oprint("s %zu %p %p %s\n",
                   j,
                   (void*)&stardata->common.orbiting_object[j],
                   (void*)stardata->common.orbiting_object[j].name,
                   stardata->common.orbiting_object[j].name
                );
        }

        if(i < stardata->common.n_orbiting_objects - 1)
        {
            /*
             * Shift the stack down one if the one we're
             * removing is not the final object
             */
            const size_t n = stardata->common.n_orbiting_objects - i - 1;
            const size_t s = sizeof(struct orbiting_object_t) * n;
            struct orbiting_object_t * const p = stardata->common.orbiting_object + i;
            Oprint("Shift %p (%p) to %p (%p)\n",
                   (void*)(p+1),
                   (void*)((p+1)->name),
                   (void*)p,
                   (void*)(p->name));
            memmove(p,p+1,s);
        }

        for(size_t j =0; j<stardata->common.n_orbiting_objects-1;j++)
        {
            Oprint("s %zu %p name=%p=%s\n",
                   j,
                   (void*)&stardata->common.orbiting_object[j],
                   (void*)stardata->common.orbiting_object[j].name,
                   stardata->common.orbiting_object[j].name
                );
        }

        /*
         * Realloc the orbiting object stack to
         * remove the final object (which is defunct)
         */
        stardata->common.n_orbiting_objects--;
        void * const was = stardata->common.orbiting_object;
        stardata->common.orbiting_object =
            Realloc(
                stardata->common.orbiting_object,
                sizeof(struct orbiting_object_t) *
                stardata->common.n_orbiting_objects);
        Oprint("realloc %zu\n",stardata->common.n_orbiting_objects);
        if(was != stardata->common.orbiting_object)
        {
            for(size_t j =0; j<stardata->common.n_orbiting_objects;j++)
            {
                Oprint("realloced to %zu %p name=%p=%s\n",
                       j,
                       (void*)&stardata->common.orbiting_object[j],
                       (void*)stardata->common.orbiting_object[j].name,
                       stardata->common.orbiting_object[j].name
                    );
            }
        }
    }

}

#endif // ORBITING_OBJECTS
