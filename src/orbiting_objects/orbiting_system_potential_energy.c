#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef ORBITING_OBJECTS

double orbiting_system_potential_energy(struct stardata_t * const stardata,
                                        struct orbiting_object_t * const o,
                                        const double theta)
{
    /*
     * Calculate the orbital potential energy
     * as a function of angle theta, in code units.
     */
    const double r = orbiting_object_distance(o,theta);
    const double M1 = Central_object_property(o,mass);
    const double M2 = o->mass;
    return - M1 * M2 / r;
}
#endif // ORBITING_OBJECTS
