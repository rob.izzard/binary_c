#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef ORBITING_OBJECTS

double orbiting_system_potential_energy_cgs(struct stardata_t * const stardata,
                                            struct orbiting_object_t * const o,
                                            const double theta)
{
    /*
     * Calculate the orbital energy
     * as a function of angle theta, in cgs units.
     */
    const double r = orbiting_object_distance_cgs(o,theta);
    const double M1 = M_SUN * Central_object_property(o,mass);
    const double M2 = M_SUN * o->mass;
    return - GRAVITATIONAL_CONSTANT * M1 * M2 / r;
}
#endif // ORBITING_OBJECTS
