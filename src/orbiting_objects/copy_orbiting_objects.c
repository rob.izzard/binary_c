#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef ORBITING_OBJECTS
struct orbiting_object_t * copy_orbiting_objects(struct orbiting_object_t * const objects,
                                                 const size_t n)
{
    /*
     * Make a copy of and oribiting objects struct array
     */
    if(objects != NULL)
    {
        const size_t s = sizeof(struct orbiting_object_t) * n;
        struct orbiting_object_t * copy = Malloc(s);
        memcpy(copy,
               objects,
               s);
        for(size_t i=0; i<n; i++)
        {
            const size_t s2 = 1 + sizeof(char)*strlen(objects[i].name);
            copy[i].name = Malloc(s2);
            memcpy(copy[i].name,
                   objects[i].name,
                   s2);
        }
        return copy;
    }
    else
    {
        return NULL;
    }
}

#endif//ORBITING_OBJECTS
