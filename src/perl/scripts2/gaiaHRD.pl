#!/usr/bin/env perl
use strict; # recommended
use 5.16.0; # recommended
use binary_grid2; # required
use binary_grid::C; # backend : C or Perl
use rob_misc qw/ncpus mkdirhier/;
use Sort::Key qw/nsort/;
use List::MoreUtils qw(uniq);

############################################################
#
# Script to make Gaia-like HRDs
#
# For full documentation, please see binary_grid2.pdf
# in the doc/ directory of binary_c
#
############################################################

# number of computational threads to launch, usually one
# per CPU
my $nthreads = rob_misc::ncpus();

# make a new stellar population
my $population = binary_grid2->new(
    # options can be given now ...
    metallicity => 0.02, # mass fraction of "metals"
    max_evolution_time => 15000, # Myr
    nthreads=>$nthreads, # number of threads
    );

# widths of HRD bins
my $binwidth = {
    Teff => 0.01,
    L => 0.01,
    gaia_G => 0.025,
    gaia_BP_RP => 0.025,
};

# ... or options can be set manually later.
$population->set(
    vb=>1, # turn on verbose logging (can be 0,1,2...)
    return_array_refs=>1, # quicker data parsing mode
    log_args=>0,
    sort_args=>0,
    save_args=>0,
    log_args_dir=>'/tmp',
    gaia_Teff_binwidth => $binwidth->{Teff},
    gaia_L_binwidth => $binwidth->{L},
    );


############################################################
# scan command line arguments for extra options
$population->parse_args();

############################################################
#
# Now we set up the physics of the stellar evolution and
# population runs
#
#
# population_duplicity is
# 0 for single stars
# 1 for binary stars
# 2 for a mixed population
# 3 for a mixed population sampled at equal times
my $population_duplicity = 1;

if($population_duplicity == 0)
{
    # make a grid of $nstars single binary stars, log-spaced,
    # with masses between $mmin and $mmax
    my $nstars = 400;
    my $mmin = 0.1;
    my $mmax = 80.0;
    $population->add_grid_variable(
        'name'       => 'lnm1',
        'longname'   =>'Primary mass',
        'range'      =>[log($mmin),log($mmax)],
        'resolution' => $nstars, # just a counter for the grid
        'spacingfunc'=>"const(log($mmin),log($mmax),$nstars)",
        'precode'    =>'$m1=exp($lnm1);',
        'probdist'   =>'Kroupa2001($m1)*$m1',
        'dphasevol'  =>'$dlnm1',
        );
}
elsif($population_duplicity == 1)
{
    # make a population of binary stars
    my $nres = 40;
    my $resolution = {
        m1 => $nres,
        q => $nres,
        P => $nres
    };
    my $mmin = 1.0;
    my $mmax = 80.0;
    $population->{_grid_options}{binary} = 1;

    $population->add_grid_variable
        (
        'name'       => 'lnm1',
        'longname'   =>'Primary mass',
        'range'      =>[log($mmin),log($mmax)],
        'resolution' => $resolution->{m1},
        'spacingfunc'=>"const(log($mmin),log($mmax),$resolution->{m1})",
        'precode'    =>'$m1=exp($lnm1);',
        'probdist'   =>'Kroupa2001($m1)*$m1',
        'dphasevol'  =>'$dlnm1',
        );
    # q=M1/M2 distribution flat in q between 0.1/M1 and 1.0
    $population->add_grid_variable
        (
        'condition'  =>'$self->{_grid_options}{binary}==1',
        'name'       =>'q',
        'longname'   =>'Mass ratio',
        'range'      =>['0.1/$m1',1.0],
         'resolution'=>$resolution->{q},
        'spacingfunc'=>"const(0.1/\$m1,1.0,$resolution->{q})",
        'probdist'   =>"flatsections\(\$q,\[
\{min=>0.1/\$m1,max=>0.8,height=>1.0\},
\{min=>0.8,max=>1.0,height=>1.0\},
\]\)",
        precode     =>'$m2=$q*$m1;',
        dphasevol   =>'$dq',
        );
     # orbital period Duquennoy and Mayor 1991 distribution
    my $Prange = [-2.0,12.0];
    $population->add_grid_variable
         (
          'name'       =>'logper',
          'longname'   =>'log(Orbital_Period)',
          'range'      =>$Prange,
          'resolution' =>$resolution->{P},
          'spacingfunc'=>"const($Prange->[0],$Prange->[1],$resolution->{P})",
          'precode'=>"\$per = 10.0 ** \$logper;
my \$eccentricity = 0.0;
\$sep=calc_sep_from_period(\$m1,\$m2,\$per) if(defined \$m1 && defined \$m2);
",
          'probdist'=>"gaussian(\$logper,4.8,2.3,$Prange->[0],$Prange->[1])",
          'dphasevol'=>'$dln10per'
         );
}
elsif($population_duplicity == 2)
{
    # Make a mixed population of single and binary stars
    my $res = 100;
    distribution_functions::bastard_distribution(
        $population, {
            mmin         =>0.1,
            mmax         =>80.0,
            nm1          => $res,
            m2min        =>0.1,
            nm2          => $res,
            nper         => $res,
            qmin         =>0.0,
            qmax         =>1.0,
            necc         =>undef,
            useecc       =>undef,
            agbzoom      =>0,
            time_adaptive=>undef,
        });
}
else
{
    # Make a mixed population of single and binary stars
    # with equal sampling in equal times.
    my $dt = 100.0;
    $population->set(
        time_adaptive_mass_grid_step => $dt,
        );
    my $sampling_factor = 0.5; # deliberate oversampling (<0.5)
    my $time_adaptive_options = {
        max_evolution_time =>
            $population->{_bse_options}{max_evolution_time},
        stellar_lifetime_table_nm=>1000,
        time_adaptive_mass_grid_log10_time=>0,
        time_adaptive_mass_grid_step=>$dt,
        time_adaptive_mass_grid_nlow_mass_stars=>10,
        nthreads           =>$nthreads,
        thread_sleep       =>1,
        mmin               =>0.1,
        mmax               =>80.0,
        mass_grid_log10_time=>0,
        mass_grid_step      =>$dt*$sampling_factor,
        extra_flash_resolution=>0, # 1 = broken?
        mass_grid_nlow_mass_stars=>10,
        debugging_output_directory=>'/tmp',
        max_delta_m         =>1.0,
        savegrid            =>1,
        vb                  =>0,
        metallicity=>$population->{_bse_options}{metallicity},
        agbzoom             =>0,
    };
    distribution_functions::bastard_distribution(
        $population, {
            mmin         =>0.1,
            mmax         =>80.0,
            m2min        =>0.1,
            nm2          =>10,
            nper         =>10,
            qmin         =>0.0,
            qmax         =>1.0,
            necc         =>undef,
            useecc       =>undef,
            agbzoom      =>0,
            time_adaptive=>$time_adaptive_options,
        });
}

############################################################
# link population to custom data parser function
$population->set(
    parse_bse_function_pointer => \&main::parse_data
    );

# evolution the stellar population (this takes some time)
$population->evolve();

# output the data
output($population);

# done : exit
exit;

############################################################
# subroutines
############################################################

sub parse_data
{
    my ($population, $results) = @_;

    while(1)
    {
        # subsequent calls to tbse_line contain
        # (references to) arrays of data
        my $la = $population->tbse_line();

        # first element is the "header" line
        my $header = shift @$la;

        # break out of the look if this is 'fin'
        last if ($header eq 'fin');

        # check if $header matches one of your
        # expected data lines, if so, act
        if($header eq 'GAIAHRD')
        {
            # matched header : get this binary system's data
            my ($time,$Teff,$L,$dtp,$system_duplicity,$gaia_BP_RP,$gaia_G) = @$la;

            # construct the theoretical HRD
            $results->{HRD}->{$system_duplicity}->{$Teff}->{$L} += $dtp;

            # construct the Gaia HRD
            $results->{CMD}->{$system_duplicity}->{$gaia_BP_RP}->{$gaia_G} +=  $dtp;
        }
    }
}

############################################################

sub output
{
    my ($population) = @_;

    # $results is a hash reference containing
    # the results that were added up in parse_data()
    my $results = $population->results;

    # make dir
    my $outdir = '/tmp/gaiahrd3';
    mkdirhier $outdir;

    {
        open(my $fp,'>',"$outdir/about")||die("cannot open $outdir/about for writing");
        # show information about the run, binary_c, etc.
        print {$fp} $population->about();
        close $fp;
    }

    # loop over the two types of HRD
    foreach my $plottype ('HRD',
                          'CMD')
    {
        my $HRD = $results->{$plottype};
        my $header;
        my $xcol;
        my $ycol;

        # choose columns based on plottype
        if($plottype eq 'CMD')
        {
            # Gaia CMD : x axis is BP-RP, y axis is G
            $header = "gaiacmd";
            $xcol = 'gaia_BP_RP';
            $ycol = 'gaia_G';
        }
        else
        {
            # Gaia HRD : x axis is Teff, y axis is L
            $header = "gaiahrd";
            $xcol = 'Teff';
            $ycol = 'L';
        }

        # one plot for each duplicity (0=single, 1=binary)
        foreach my $duplicity (0,1)
        {
            my $f = "$outdir/$header.$duplicity.dat";
            open(my $fp, '>', $f)||die("cannot write to $f");
            print {$fp} "# $header for duplicity $duplicity\n# Columns are : $xcol, $ycol, dtp\n";
            foreach my $x (nsort keys %{$HRD->{$duplicity}})
            {
                foreach my $y (nsort keys %{$HRD->{$duplicity}->{$x}})
                {
                    outbox($xcol,
                           $ycol,
                           $fp,
                           $x,
                           $y,
                           $HRD->{$duplicity}->{$x}->{$y});
                }
                print {$fp} "\n";
            }
            close $fp;
        }

        # output the with the binary fraction
        {

            my $f = "$outdir/$header.mix.dat";
            open(my $fp, '>', $f)||die("cannot write to $f");
            print {$fp} "# $header for binary fractions\n# Columns are : $xcol, $ycol, binary fraction\n";
            my @xs = uniq (keys %{$HRD->{0}},
                              keys %{$HRD->{1}});

            foreach my $x (nsort @xs)
            {
                my @ys = uniq (keys %{$HRD->{0}->{$x}},
                               keys %{$HRD->{1}->{$x}});

                foreach my $y (nsort @ys)
                {
                    my $dtp_both =
                        ($HRD->{0}->{$x}->{$y}//0.0) +
                        ($HRD->{1}->{$x}->{$y}//0.0);

                    if($dtp_both > 0.0)
                    {
                        my $binfrac = $HRD->{1}->{$x}->{$y}/$dtp_both;

                        if($binfrac > 1)
                        {
                            printf "Binfrac = %g > 1 from %g %g sum %g\n",
                                $binfrac,
                                $HRD->{0}->{$x}->{$y},
                                $HRD->{1}->{$x}->{$y},
                                $dtp_both;
                            exit;
                        }
                        outbox($xcol,
                               $ycol,
                               $fp,
                               $x,
                               $y,
                               $binfrac);
                    }
                }
            }
            close $fp;
        }
    }
}


sub outbox
{
    # output a box suitable for pm3d gnuplotting
    my ($xcol,$ycol,$fp,$x,$y,$z) = @_;

    $x -= $binwidth->{$x}*0.5;
    $y -= $binwidth->{$y}*0.5;

    my @x;
    my @y = (0,$binwidth->{$ycol});
    foreach my $dx (0,$binwidth->{$xcol})
    {
        foreach my $dy (@y)
        {
            my $x = sprintf('%g %g %g',
                            $x+$dx,
                            $y+$dy,
                            $z);
            push(@x, $x);
        }
        @y = reverse @y;
    }
    print {$fp} join("\n",@x), "\n\n", join("\n",reverse @x), "\n\n\n";
}
