#!/usr/bin/env perl
use strict;
use 5.16.0;
use Carp qw(confess);
use File::Path qw(make_path);
use File::Basename;
use rob_misc;
use Data::Dumper;
use subs qw(vprint vsay);
use List::MoreUtils qw(uniq);
use File::Temp qw(tempdir);
use Maths_Double;
use threads;
use threads::shared;
use robqueue;
use warnings;
$|=1;

$Carp::Verbose=1;

#
# script to plot one or more yield sets for comparison
#
# NB requires symbolic links version of yields_vs_time (as of 23/12/2013)
#
# Also assumes differential yields at fixed times (this is now the standard!)
#

my $help="
    Usage :

    plot_yieldsets.pl <datadirs> filter=... filter=... <options>

    where the <datadirs> are made by yields_vs_time.pl

  Filters:

    filter=xyz where xyz is

    * sourceXYZ
    XYZ is the source label, e.g. GB, AGB
    You can combine them e.g. filter=sourceGB+AGB

    xyz can be an element or isotope label e.g. filter=Li7
    If no elements or isotopes are specified, we try to plot them all,
    you can then choose isotopes to be omitted with e.g. filter=\!Li7

    * rebin:<opts>
    This pipes data through yieldset_rebin.pl to merge time bins.
    Typical usage: filter=rebin:modulo=10:tstart=10000
    where modulo is the number of old bins to make a new bin, and this
    is applied after tstart Myr.

    * cumulate
    with filter=cumulate you integrate the yields instead of plotting
    the time derivative.

  Options:
    You can change any of the options given in the options hash
    on the command line. See %options for details.

    ";

# options (can be overridden by x=y on command line)
my %options=(vb=>1, # verbosity

             # filters
             filters=>[],

             # (gnu)plot options
             run_gnuplot=>1,
             #gp_terminal=>'postscript enhanced landscape size 29.7cm,21.0cm',
             #gp_terminal=>'pdfcairo enhanced size 29.7cm,21.0cm',
             gp_terminal=>'pdfcairo enhanced size 35cm,21.0cm',
             gp_colour=>'colour',
             gp_fortalk=>0,
             gp_font=>'Helvetica',
             gp_fontsize=>22,
             gp_fontscale=>1.0,
             gp_solid=>'',
             gp_linewidth=>6,
             gp_dashlength=>4,
             gp_dashes=>0,
             gp_ptcount=>0,
             gp_pltfile=>'yieldsets.plt',
             gp_outfile=>'yieldsets.pdf',
             gp_with=>'lp ps 0.86 lw 1',
             gp_key=>1,
             gp_key_location=>'top right outside samplen 0.8 opaque font "Helvetica,14"',
             gp_linetitle_fontsize=>14,
             gp_logscale=>{'x'=>1,'y'=>1,'x2'=>0,'y2'=>0},
             gp_colours=>[
                 '0000ff', # 0
                 '007f00',
                 'ff0000',
                 '00bfbf',
                 'bf00bf',
                 'bfbf00', # 5
                 '3f3f3f',
                 'bf3f3f',
                 'bb9900',
                 '3f3fbf',
                 'bfbfbf', # 10
                 '00ff00',
                 'c1912b',
                 '89a038',
                 '5691ea',
                 'ff1999', # 15
                 'e0bfba',
                 '197c77',
                 'a856a5',
                 'fc683a',
                 'ff5555', # 20
                 '55ff55',
                 '5555ff',
                 'ff7733',
                 '7733ff', # 24
                 '00ffff',
                 'ff00ff',
             ],
             gp_ps=>undef,
             gp_colour_by=>'source', # can be : source, order, dir, isotope
             gp_points_by=>'dir', # can be : source, order, dir, isotope
             gp_title=>1, # 1 = title, 0 = no title
             gp_extra_title=>'',
             gp_key_width=>0,
             gp_offsets=>'0.1,0.1,0,0',

             line_end_labels=>0, # put data at the end of the lines, instead of in the gnuplot line titles

             yield_sigfigs=>2,
             yield_totals=>1, # plot yield totals
             yield_percents=>1, # plot yield %ages

             skip_zerocurves=>1, # if 1 skip curves which are just zero

             xlow=>'*',
             xhigh=>'*',

             ylow=>'*',
             yhigh=>'*',

             overplot=>0,

             # data directories
             datadirs=>[],

             # other
             tinyyield=>1e-50,

             # threshold : if yield < this fraction, ignore
             threshold=>0.0001,

             # if mintime/maxtime are undef, they should be ignored
             mintime=>undef,
             maxtime=>undef,

             # are times logged?
             yields_logtimes=>undef,

             # exit_on_nan or exit_on_inf exit when finding a nan or inf
             exit_on_nan=>1,
             exit_on_inf=>1,

             # separate plots for each source?
             separate=>0,

             # cleanup tmp?
             cleanup=>1,
    );

# tmp dir
$options{tmpdir} //= tempdir(CLEANUP=>0,#$options{cleanup},
                             DIR=>(-d '/var/tmp' ? '/var/tmp':'/tmp'));

my @infotable;

parse_args();

do_work();

output_infotable();

exit;

############################################################

sub parse_args
{
    # parse cmd line args
    while(my $arg=shift @ARGV)
    {
        if($arg=~/^-?h(?:elp)?/)
        {
            print $help;
            exit;
        }
        if($arg eq 'vb' || $arg eq 'verbose')
        {
            # set verbosity on
            $options{vb}=1;
        }
        elsif($arg=~/(\S+)=(.+)/ && exists $options{$1})
        {
            # override one of the default options
            $options{$1}=$2;
        }
        elsif(-d $arg)
        {
            # arg is a directory presumably containing yieldsets
            push(@{$options{datadirs}},$arg);
        }
        elsif($arg=~/(?:filter=)?(\S+)/)
        {
            # arg is a filter, save the details
            my $filter=$1;
            push(@{$options{filters}},$1);
        }
        else
        {
            # er? unknown!
            confess "What is arg $arg? It's not a yield directory".-d $arg." or recongised as a filter.";
        }
    }
}

sub do_work
{
    # do the work to make the plots
    make_path($options{tmpdir});

    # loop over yield dirs to discover how many isotopes to plot
    my %info;
    foreach my $datadir (@{$options{datadirs}})
    {
        $info{$datadir} = analyse_yielddir($datadir);
    }

    # find number of isotopes to plot and get a list of the isotope names :
    # assume that the longest list is the most complete
    my $binary_c_nisotopes = -1;
    my @isotopes;
    foreach my $datadir (@{$options{datadirs}})
    {
        my $n = $info{$datadir}{binary_c_nisotopes};
        if($n>$binary_c_nisotopes)
        {
            @isotopes=@{$info{$datadir}{isotopes}};
            $binary_c_nisotopes = $n;
        }
    }

    # find maximum number of sources to plot
    my $nsources=-1;
    foreach my $datadir (@{$options{datadirs}})
    {
        $nsources = MAX($nsources,$info{$datadir}{nsources});
    }
    my $n_native_sources = $nsources;

    vsay "nsources = $nsources";

    # open gnuplot file
    my $gpfile = $options{run_gnuplot} ? $options{tmpdir}.'/'.$options{gp_pltfile} : '/dev/null';
    open(my $gp,'>',$gpfile)||confess("cannot open $gpfile for output");
    print {$gp} "set terminal $options{gp_terminal} $options{gp_colour} ";

    # gnuplot labels file
    my $labelscount=0;
    my $labelsfile = $options{run_gnuplot} ? $options{tmpdir}.'/'.$options{gp_pltfile}.'labels'.$labelscount : '/dev/null';
    open(my $labels_fp,'>',$labelsfile)||confess("cannot open labels file");

    my $postscript = $options{gp_terminal}=~/(postscript|eps)/;

    print {$gp} $postscript ? " \"$options{gp_font}\" $options{gp_fontsize} "
        : " font \"$options{gp_font}\" fontscale $options{gp_fontscale} ";

    print {$gp} " $options{gp_solid} linewidth $options{gp_linewidth} dashlength $options{gp_dashlength}
    set output \"$options{gp_outfile}\"
set auto fix
set offsets $options{gp_offsets}
set zero 0.0
set xlabel \"Time / Myr\"
set format y ",'"%g"',"
set xrange\[$options{xlow}:$options{xhigh}\]
set yrange\[$options{ylow}:$options{yhigh}\]
load \"$labelsfile\"
\n\n";


    # override key width if required
    if(defined $options{gp_key_width} &&
       $options{gp_key_location}!~s/width -?\d+/width $options{gp_key_width}/)
    {
    $options{gp_key_location}.=" width $options{gp_key_width} ";
    }

    # key location
    say {$gp} $options{gp_key} ? "set key $options{gp_key_location}" : 'unset key';

    # logscales
    foreach my $axis (keys %{$options{gp_logscale}})
    {
    say {$gp} ($options{gp_logscale}{$axis}==0 ? 'un' : '') . 'set logscale '.$axis;
        if($axis=~/y/)
        {
            say {$gp} 'set format '.$axis.' '.
                ($options{gp_logscale}{$axis}==0 ? '"%g"' : '"10^{%L}"');
        }
    }

    # make a list of isotope names to plot
    my %isotopes_toplot;
    my %isotopes_tonotplot;
    foreach my $filter (@{$options{filters}})
    {
    if($filter=~/^([A-Z][a-z]*)(\d+)$/)
    {
        # isotope
        $isotopes_toplot{$1.$2}=1;
    }
    elsif($filter=~/^([A-Z][a-z]*)$/)
    {
        # element : choose matching isotopes
        my $element = $1;
        foreach my $isotope (@isotopes)
        {
        if($isotope=~/^$element/)
        {
            $isotopes_toplot{$isotope}=1;
        }
        }
    }
    elsif($filter=~/^\!([A-Z][a-z]*)(\d+)$/)
    {
        # isotope to not plot
        $isotopes_tonotplot{$1.$2}=1;
    }
    elsif($filter=~/^\!([A-Z][a-z]*)$/)
    {
        # element to not plot : choose matching isotopes
        my $element = $1;
        foreach my $isotope (@isotopes)
        {
        if($isotope=~/^$element/)
        {
            $isotopes_tonotplot{$isotope}=1;
        }
        }
    }
    }

    # if we're not asked to plot any isotopes, plot all isotopes
    if(! scalar keys %isotopes_toplot)
    {
    foreach my $isotope (@isotopes)
    {
        $isotopes_toplot{$isotope} = 1;
    }
    }

    # make sure isotopes chosen for non-plotting are not plotted
    foreach my $isotope (keys %isotopes_tonotplot)
    {
    delete $isotopes_toplot{$isotope};
    }

    # hence the final list of isotopes (TODO needs better sorting)
    my @isotopes_toplot = sort keys %isotopes_toplot;

    # make plots : loop over all binary_c isotopes ... not all will be available
    # so we have to find those that are, and also those with > 0 yield
    my $plotcomma = $options{separate} ? "\nplot" : 'plot';
    my $colour_count=-1;
    my $points_count=-1;

    foreach my $isotope (@isotopes_toplot) # isotope strings
    {
    my $gpisotope = flip_isotope($isotope);

    # make new plot, or append if overplot is set
    if(!$options{overplot})
    {
        $plotcomma=$options{separate} ? "\nplot" : 'plot';

        if($options{gp_title})
        {
        if(grep {/cumulate/} @{$options{filters}})
        {
            printf {$gp} "set title \"(1/M_\{*\}) * M_\{ej\} as $gpisotope (\{/Italic t\}) $options{gp_extra_title}\"\n";
        }
        else
        {
            printf {$gp} "set title \"(1/M_\{*\}) * d(M_\{ej\} as $gpisotope)/\{/Italic dt\} $options{gp_extra_title}\"\n";
        }
        }

        # reset counts
        $colour_count=-1;
        $points_count=-1;

            # reset labels
            close $labels_fp;
            $labelscount++;
            $labelsfile = $options{run_gnuplot} ? $options{tmpdir}.'/'.$options{gp_pltfile}.'labels'.$labelscount : '/dev/null';
            open($labels_fp,'>',$labelsfile)||confess("cannot open labels file");

            print {$gp} "unset label\nload \"$labelsfile\"\n";

    }

    if($options{gp_colour_by} eq 'isotope')
    {
        $colour_count++;
    }
    elsif($options{gp_colour_by} eq 'source')
    {
        $colour_count=-1
    }
    if($options{gp_points_by} eq 'isotope')
    {
        $points_count++;
    }
    elsif($options{gp_points_by} eq 'source')
    {
        $points_count=-1;
    }

    state $datadircount;
    $datadircount=0;

    foreach my $datadir (@{$options{datadirs}})
    {
        my @datadir_infotable;
        $datadircount++;

        # get isotope number and column number (used lots)
        my $nisotope = $info{$datadir}{isotopes_hash}{$isotope};
        my $ncol = $info{$datadir}{column_hash}{$isotope};

        # check we have this isotope
        next if (!defined $info{$datadir}{isotopes}[$nisotope]);
        next if ($nisotope >= $info{$datadir}{nisotopes});

        $colour_count++ if($options{gp_colour_by} eq 'dir');
        $colour_count=-1 if($options{gp_colour_by} eq 'source');

        $points_count++ if($options{gp_points_by} eq 'dir');
        $points_count=-1 if($options{gp_points_by} eq 'source');

        # make a local list of filters
        my @filters = @{$options{filters}};

        # convert source names to numbers
        my $allsources=join('+',0..$n_native_sources);
        map
        {
        print "CONV \"$_\" to ";
        s/sourceSUM/source$allsources/g;
        s/(\!?source)([a-zA-Z]+)/$1$info{$datadir}{source_strings}{$2}/g;
        s/\+([a-zA-Z]+)/\+$info{$datadir}{source_strings}{$1}/g;
        say "$_\n";
        }@filters;

        # make a list of sources to plot
        my %sources_toplot;
        my %dummy_source;
            foreach my $filter (@filters)
        {
        if($filter=~/^(?:dummy)?source(\d+.*)/)
        {
            $sources_toplot{$1}=1;
        }
                if($filter =~/^dummysource(\d+.*)/)
                {
                    $dummy_source{$1}=1;
                }
        }

        # sorter for the yields
        my @sources_toplot = sort {
        my @a = map{$_=$info{$datadir}{sources}[$_] } split(/\+/,$a);
        my @b = map{$_=$info{$datadir}{sources}[$_] } split(/\+/,$b);
        my $x;
        if($#a==$#b)
        {
            for(my $i=0;$i<=$#a;$i++)
            {
            $x =($a[$i] cmp $b[$i] ||
                 $a[$i] <=> $b[$i]);
            last if($x);
            }
        }
        else
        {
            $x = $#a<$#b ? -1 : 1;
        }
        return $x;
        }keys %sources_toplot;

        if($#sources_toplot==-1)
        {
        # plot all if none are selected by filters
        @sources_toplot = (0..$nsources);
        }

        # TODO add negative source filters?
        print "FINAL plot isotopes @isotopes_toplot, sources @sources_toplot\n";

        # if isotope changes, up count
        state $prev_isotope;
        state $isotope_count=-1;

        if(!defined $prev_isotope || $isotope ne $prev_isotope)
        {
        $prev_isotope=$isotope;
        $isotope_count++;
        }

        foreach my $source (@sources_toplot)
        {
        $colour_count++ if($options{gp_colour_by} eq 'source');
        $points_count++ if($options{gp_points_by} eq 'source');

        # split source into subsources
        my @subsources = split(/\+/,$source);

        # get yield as a function of source and isotope
        my $ytot = 0.0;
        foreach my $subsource (@subsources)
        {
            $ytot += $info{$datadir}{yieldtot}{source}[$subsource][$nisotope];
        }

        # construct line titles
        my $t;
        $t = flip_isotope($isotope).' ' if($options{overplot});
        $t .= gptitle($info{$datadir},
                              \@subsources,
                              $allsources);

        my $yfrac = $info{$datadir}{yieldtot}{isotope}[$nisotope]>0.0 ?
            $ytot/$info{$datadir}{yieldtot}{isotope}[$nisotope] : -0.01;

        vsay "PLOT source=$source, subsources=@subsources, ytot=$ytot, yfrac=$yfrac\n";

        # add total yield into this source
                if($options{yield_totals})
                {
                    $t .= sprintf ' %.'.$options{yield_sigfigs}.'g',$ytot;
                }
                if($options{yield_percents})
                {
                    $t .= sprintf ' %.1f%%',100.0*$yfrac;
                }

                # reformat exponential notation
                $t =~ s/e([\+-])0*(\d+)/\{\/Symbol \\264\}10^\{\/*0.8 $1$2\}/g;

        $colour_count++ if($options{gp_colour_by} eq 'order');
        $points_count++ if($options{gp_points_by} eq 'order');

        my $lineopts;

        if($options{gp_dashes})
        {
            # force dashed lines
            state $dashcount = 0;
            $dashcount ++;
            $lineopts .= ' dt '.($dashcount).' ';
            print "SET DASH $dashcount\n";
        }

        if($options{gp_with}=~/^l/)
        {
            # lines options
            $lineopts .= ' lt '.($isotope_count+1).' ';
        }

        if($options{gp_ptcount})
        {
            state $ptcount = 0;
            $ptcount++;
            $lineopts .= ' pt '.$ptcount.' ';
        }
        elsif($options{gp_with}=~/^(lp|points|linespoints)/)
        {
            # lines-points options
            $lineopts .= ' pt '.$datadircount.' ';
        }
        if(defined $options{gp_ps})
        {
            $lineopts .= ' ps '.$options{gp_ps}.' ';
        }
        $lineopts.= ' ';

        # line colour
        my $colour =
            $options{gp_colours}[($colour_count)%(1+$#{$options{gp_colours}})];
        $lineopts .= " linecolor rgbcolor \"\#".$colour.'" ';

        # choose the files to be plotted, pipe them through add_yieldsets.pl
        my $maxtimefilter = defined $options{maxtime} ? " maxtime=$options{maxtime} " : '';
        my $logyields = $options{yields_logtimes} ?
            'yields_logtimes=1 yields_startlogtime='.$options{yields_startlogtime} :
            'yields_logtimes=0';
        my @pipe_filters = ("< add_yieldsets.pl $logyields $maxtimefilter ".join(' ',(map {$_=$info{$datadir}{sourcedatafilepaths}[$_]} @subsources)));

        # check for inline pipe filters
        foreach my $filter (@{$options{filters}})
        {
            if($filter=~/rebin(\S+)$/)
            {
                my $opts=$1;
                print "Rebin filter $opts\n";
                push(@pipe_filters,"yieldset_rebin.pl $opts");
            }
            elsif($filter=~/cumulate/)
            {
                push(@pipe_filters,"cumulate_yields.pl $logyields");
            }
        }

        # construct the plotfile string
        my $plotfile = join(' | ',@pipe_filters);

        vsay "Plotfile is now : $plotfile";

        if(!$options{skip_zerocurves} ||
           $yfrac > $options{threshold})
        {
            state $prevt;

            # remove repeated information
            if(defined $prevt)
            {
                my @y;
                my $prevt_strip = $prevt;
                my $t_strip = $t;

                map
                {
                    s/\{([^\}]+)\s+/{$1+++/g;
                }($prevt_strip,$t_strip);

                my @x=split(/\s+/,$prevt_strip);
                @y=split(/\s+/,$t_strip);

                if(0){
                    map
                    {
                        foreach my $x (@x)
                        {
                            if($x eq $_)
                            {
                                # match! delete!
                                $_='';
                            }
                        }
                    }@y;
                }

                $prevt = $t;
                $t = join(' ',@y);
                $t =~ s/\+\+\+/ /g;
            }
            else
            {
                $prevt = $t;
            }

            # choose xcol for log vs linear time : output should always
            # be linera
            my $xcol = $options{yields_logtimes} ? '(10.0**$1)' : '1';

            if(!$dummy_source{$source})
            {
                # output plot string
                print {$gp} "$plotcomma '$plotfile' using $xcol:$ncol with $options{gp_with} $lineopts ";

                if($options{line_end_labels})
                {
                    my $x = "set label \"\{/=$options{gp_linetitle_fontsize} $t\}\" at $info{$datadir}{maxtime},$ytot\n";
                    print "$x\n";
                    print {$gp} 'notitle ';
                    print {$labels_fp} $x;
                }
                else
                {
                    $t=~s/^\s+//;
                    $t=~s/\s+$//;
                    print {$gp} "title \"$t\" "
                }

                if(!$options{separate})
                {
                    $plotcomma =',';
                }
            }
            push(@datadir_infotable,
                 sprintf "Element %s : Source %s : Total %g : Frac %5.3f %%",
                 $isotope,
                 $source,
                 $ytot,
                 $yfrac*100.0);
        }
        else
        {
            print "Skip because ";
            print "\$t(=$t) =~/0.0\%/ "if($t=~/0.0\%/);
            print "yfrac ($yfrac) < thresh ($options{threshold}) "if($yfrac <= $options{threshold});
            say '';
        }
        } # source loop
        print "Done source loop\n";

        push(@datadir_infotable,
             sprintf "Element %s : %s : Total %g : Frac %5.3f %%",
             $isotope,
             'All',
             $info{$datadir}{yieldtot}{isotope}[$nisotope],
             100);

        map
        {
            s/Source (\d+)/$info{$datadir}{sources}[$1]/g;
        }@datadir_infotable;

        push(@infotable,@datadir_infotable);
    } # datadir loop
    say {$gp} '' if(!$options{overplot});
    } # isotope loop


    if($options{run_gnuplot})
    {
        my $cmd = "gnuplot $options{tmpdir}/$options{gp_pltfile}";

        print "Running gnuplot : \"$cmd\"\n";
        say {$gp} '';
        close $gp;
        close $labels_fp;

        say `$cmd`;

        if(-s $options{gp_outfile})
        {
            say "See $options{gp_outfile} (size ",-s $options{gp_outfile},' bytes)';
        }
        else
        {
            say "No output file generated: check that the data is ok, the directories are correct, and that your filters are not too restrictive.";
        }
    }

}

sub analyse_yielddir
{
    my $datadir=$_[0];
    # analyse given data directory and return a hash (pointer) of information
    opendir(my $dirfp, $datadir) || confess ("cannot opendir $datadir");
    say "\nProcess $datadir";

    my %info : shared;
    my $yieldtot = {
        all=>[],
        source=>[]
    };
    %info =
        (
         atomic_number => &share([]),
         atomic_number_hash => &share({}),
         atomic_mass => &share([]),
         atomic_mass_hash => &share({}),
         element_number => &share([]),
         grid_option => &share({}),
         bse_option => &share({}),
         columns => &share([]),
         column_hash => &share({}),
         sources => &share([]),
         source_strings => &share({}),
         datafiles => &share([]),
         sourcedatafiles => &share([]),
         sourcedatafilepaths => &share([]),
         yieldtot => &share({
             all => &share([]),
             source => &share([]),
                            }),
         isotopes => &share([]),
         isotopes_hash => &share({}),
         datadir => $datadir,
         maxtime => undef,
         mintime => undef,
         nsources => undef,
         nisotopes => undef, # number of isotopes for which data is given
         binary_c_nisotopes => undef, # number of isotopes in binary_c (usually > nisotopes)
         mass_into_stars => undef,
         mass_out_of_stars => undef,
        );

    my @all =
        grep
    {
        !/^\./ && !/ensemble/ && -f "$datadir/$_" && /\.dat$/
    }
    readdir($dirfp);

    my @files = grep { -f $datadir.'/'.$_ && ! -l $datadir.'/'.$_ } @all;
    my @links = grep { -l $datadir.'/'.$_ } @all;

    # links
    no warnings;
    foreach my $file (sort
                      {
                          $a<=>$b || $a cmp $b
                      }
                      @links)
    {
        # link source name to source number
        my $f=$datadir.'/'.$file;
        my $realf = readlink $f;
        my $source_number = ($realf=~/source(\d+)/)[0];
        my $source_string = ($f=~/source([^\.]+)/)[0];
        $info{sources}[$source_number] = $source_string;
        $info{source_strings}{$source_string} = $source_number; # reverse lookup
        print "Add source string $source_string\n";
    }
    use warnings;

    # metadata parse
    parsefile([$files[0],\%info,-1]);
    $options{yields_logtimes} = rob_misc::truefalse_to_boolean($info{bse_option}{yields_logtimes}) // 0;
    $options{yields_logtimes} = $info{bse_option}{yields_logtimes}//0;
    $options{yields_startlogtime} = $info{bse_option}{yields_startlogtime}//undef;

    # make a queue
    my $q = robqueue->new(
        vb=>0,
        nthreads=>1,
        subr => \&parsefile,
        sumhash => $yieldtot
        );

    # data files
    foreach my $file (sort
                      {
                          ($a=~/(\d+)\.dat/)[0]<=>($b=~/(\d+)\.dat/)[0]
                              ||
                              $a<=>$b
                              ||
                              $a cmp $b
                      }
                      @files)
    {
        my $f=$datadir.'/'.$file;
        # source datafile
        push(@{$info{datafiles}},$file);
        say "Found data file $file";

        # source number
        my $source = ($file=~/source(\d+)/)[0];
        $info{nsources} = MAX($info{nsources}, $source);

        $info{sourcedatafiles}[$source] = $file;
        $info{sourcedatafilepaths}[$source] = $f;

        # information on the data file
        #parsefile($f,\%info,$source);
        $q->q([$f,\%info,$source]);
    }

    closedir $dirfp;

    # join the queue
    $q->end;

    # copy the data into %info
    $info{yieldtot} = shared_clone($yieldtot);

    $yieldtot = undef; # clear memory

    if($options{vb})
    {
        map
        {
            vsay "$_ = $info{$_}";
        }sort keys %info;
    }

    # show statistics table
    printf "Mass   into stars %12.5g\n",$info{mass_into_stars};
    printf "Mass out of stars %12.5g\n",$info{mass_out_of_stars};

    foreach my $source (0..$info{nsources})
    {
        no warnings;
        printf "Mass into source % 3d (% 20s) = %12.5g =% 5.1f%% --- ",
            $source,
            $info{sources}[$source],
            $info{yieldtot}{all}[$source],
            (defined $info{mass_out_of_stars} && $info{mass_out_of_stars} > 0.0) ?
            100.0 * $info{yieldtot}{all}[$source] / $info{mass_out_of_stars} :
            -1.0;
        use warnings;

        if($info{yieldtot}{all}[$source]>$options{tinyyield})
        {
            # mostly, what is this? (by isotope)

            # first, make reverse hash sorted by yield amount
            my %revy;
            foreach my $isotope (0..$info{nisotopes})
            {
                # revy{yield} = isotope
                $revy{$info{yieldtot}{source}[$source][$isotope]}=$info{isotopes}[$isotope];
            }

            # find top five (has to be done with a temporary array or splice fails)
            my @sorted_by_yield = sort {$b<=>$a} keys %revy;
            @sorted_by_yield = splice(@sorted_by_yield, 0,5);

            no warnings;
            foreach my $y (@sorted_by_yield)
            {
                printf "% 3.1f%% % 3s ",
                    $info{yieldtot}{all}[$source]>0.0 ? $y/$info{yieldtot}{all}[$source]*100.0 : -1.0,
                    $revy{$y};
            }
            use warnings;
        }
        else
        {
            print " no yield of this source";
        }
        print " (Colour $options{gp_colours}[$source]) \n";
    }

    return \%info;
}

sub gptitle
{
    # clean up title for gnuplotting
    my $info=$_[0];
    my @subsources=@{$_[1]};
    my $allsources=$_[2];

    my $t; 
    
    # add yield source strings
    if(join('+',@subsources) eq $allsources)
    {
        $ t.= 'All sources';
    }
    else
    {
        no warnings;
        $t .= ' '.join('+',map {$_=$$info{sources}[$_]} @subsources);
        use warnings;
    }

    my $single = $t=~s/binary=0/single/;
    $t=~s/binary=1/binary/;

    if($single)
    {
        # single stars : remove binary-specific stuff
        $t=~s/lambda\S+//;
        $t=~s/alpha\S+//;
        $t=~s/epsilon\S+//;
    }

    my %greek=('alpha'=>'a',
               'omega'=>'o',
               'Omega'=>'O',
               'lambda'=>'l',
               'Lambda'=>'L',
        );
    foreach my $greek (keys %greek)
    {
        $t=~s/$greek(_?)([a-zA-Z0-9]+)?/\{\/Symbol $greek{$greek}\}UNDER\{$2\}/g;
    }
    $t=~s/yields_vs_time\.//;
    $t=~s/binary=\d//g;
    $t=~s/z=/{\/Italic Z}=/;
    $t=~s/\.dat$//;
    $t=~s/_/ /g;

    if($options{gp_fortalk})
    {
        # remove details
        $t=~s/dt=\S+//;
        $t=~s/Z=\S+//;
        #$t=~s/single//;
        #$t=~s/binary//;
        $t=~s/THORN ZYTKOW/TZ/;
        $t=~s/CHAND/M_{Ch}/g;
        $t=~s/Coel/Merge/g;
    }
    else
    {
        $t=~s/dt/\{\/Symbol d\}t/g;
    }

    $t=~s/\.([a-zA-Z])/ $1/g;
    $t=~s/source(\d+)/$$info{sources}[$1]/g;
    $t=~s/UNDER/__/g;
    $t=~s/epsnov/\{\/Symbol e\}_\{nov\}/g;
    $t=~s/ionisation/ion/g;

    $t=~s/\s+/ /g;

    return $t;
}


sub vprint
{
    if($options{vb})
    {
        print @_;
    }
}

sub vsay
{
    if($options{vb})
    {
        say @_;
    }
}

sub flip_isotope
{
    my $x=$_[0];
    $x=~s/(\D+)(\d+)/^{$2}{$1}/;
    return $x;
}

sub output_infotable
{
    print "INFORMATION TABLE\n";
    map
    {
        say $_;
    }@infotable;
}

sub get_info
{
    my $file = shift;
    my $info = {};
    $info->{filename} = $file;
    open(FP,'<:perlio',$file)||confess("cannot open $file for reading");
    while(my $l=<FP>)
    {
        next if($l=~/^#/);
        my @x = split(' ',$l);
        for(my $i=0;$i<=$#x;$i++)
        {
            $info->{totals}->[$i] += $x[$i];
            $info->{max}->[$i] = max($info->{max}->[$i],$x[$i])
                // $x[$i];
            $info->{min}->[$i] = min($info->{min}->[$i],$x[$i])
                // $x[$i];
            $info->{range}->[$i] =
                $info->{max}->[$i] - $info->{min}->[$i];

            $info->{n}->[$i] ++;
            $info->{n_non_zero}->[$i] ++ if(abs($x[$i]) > 0.0);
            if($x[$i]!=0.0 &&
               (!defined $info->{'last'}->[$i] ||
                $x[$i] != $info->{'last'}->[$i]))
            {
                $info->{n_distinct_non_zero}->[$i] ++;
                $info->{'last'}->[$i] = $x[$i];
            }
            if(!defined $info->{'last'}->[$i] ||
               $x[$i] != $info->{'last'}->[$i])
            {
                $info->{n_distinct}->[$i] ++;
                $info->{'last'}->[$i] = $x[$i];
            }
        }
    }
    close FP;
    return $info;
}

sub parsefile
{
    my ($f,$info,$source) = @{$_[0]};
    my $h = {}; # returned

    # function to parse a source data file
    #
    # if source == -1, scans the metadata in the file only

    my $file = $info->{sourcedatafiles}[$source == -1 ? 0 : $source];
    open(my $fp,'<:perlio',$f)||confess "cannot open $f for analysis";
    my $lc=1;
    my $l;
    my $prevt = $info->{bse_option}{yields_startlogtime};
    while($l = readline($fp))
    {
        chomp $l;
        if($l=~/^\#/)
        {
            # only set metadata for source -1
            if($source==-1)
            {
                # comment line : could be useful
                if($l=~/_?((?:grid|bse)_option)s? ([^=]+) = (.*)/)
                {
                    # grid_option or bse_option
                    $info->{$1}{$2} //= $3;
                }
                elsif($l=~/^\# (?:log)?time /)
                {
                    # column information
                    $l=~s/^\# //;
                    @{$info->{columns}} = split(' ',$l);
                    my $i=1;
                    %{$info->{column_hash}} = map{ $_=>$i++ } @{$info->{columns}};
                }
                elsif($l=~/^\# Isotope (\d+) is (\S+)/)
                {
                    my $index = $1;
                    my $isotope = $2;
                    my $element =
                        $isotope eq 'n1' ? 'n' :
                        $isotope eq 'e0' ? 'e' :
                        ($isotope=~/^([A-Z][a-z]*)/)[0];

                    #print "ELEMENT $element from $isotope\n";

                    $info->{isotopes}[$index] = $isotope;
                    $info->{isotopes_hash}{$isotope} = $index;
                    $info->{binary_c_nisotopes} = MAX($index,$info->{binary_c_nisotopes});

                    # extra information in newer binary_c
                    if($l=~/Z=(\d+)/)
                    {
                        $info->{atomic_number}[$index] = $1;
                        $info->{element_number}[$1] = $element;
                        $info->{atomic_number_hash}{$isotope} = $1;
                        $info->{atomic_number_hash}{$element} = $1;
                    }
                    if($l=~/A=(\d+)/)
                    {
                        $info->{atomic_mass}[$index] = $1;
                        $info->{atomic_mass_hash}{$isotope} = $1;
                    }
                }
                elsif($l=~/mass input to stars = (\S+)/)
                {
                    $info->{mass_into_stars} = $1;
                }
            }
        }
        else
        {
            # data line
            last if ($source==-1);

            # check for errors!
            if($l=~/nan/io || $l=~/inf/o)
            {
                # NAN or INF error!
                if($l=~/nan/i)
                {
                    say "NAN at line \"$l\" in $file";
                    exit if($options{exit_on_nan});
                }
                if($l=~/inf/i)
                {
                    say "INF at line \"$l\" in $file";
                    exit if($options{exit_on_inf});
                }
            }
            elsif(!$l=~/^\s*$/)
            {
                # save mass ejected as each isotope:
                # requires bse_option yields_dt to be set (should be)
                $l=~s/^(\S+) //o; # remove the time (quicker than a shift!)
                my $t = $1;

                # calculate timestep
                my $dt;
                if($options{yields_logtimes})
                {
                    $t = 10.0**$t ;
                    $dt = $t - $prevt;
                }
                else
                {
                    $dt = $info->{bse_option}{yields_dt};
                }
                $prevt = $t;

                $info->{maxtime} = MAX($t, $info->{maxtime});
                $info->{mintime} = MAX($t, $info->{mintime});

                # if we haven't reached mintime, keep going
                if(defined $options{mintime} && $t < $options{mintime})
                {
                    next;
                }
                # if we exceed the max time, close file to stop parsing
                if(defined $options{maxtime} && $t > $options{maxtime})
                {
                    close $fp;
                    last;
                }
                else
                {
                    my @x = split(' ',$l);
                    my $n = $#x;
                    $info->{nisotopes} = MAX($info->{nisotopes}, $n);

                    # variables to point to the increment points
                    # (saves time in the loop)
                    if(0)
                    {
                        # pure perl : 18.77s
                        $n++;
                        my $h2 = \$h->{source}[$source]; # array ref
                        my $h3 = \$h->{all}[$source]; # scalar ref
                        my $h4 = \$h->{isotope}; # array ref
                        for(my $i=0; $i<$n; $i++)
                        {
                            my $y = $x[$i]*$dt;
                            # by isotope (all sources)
                            $$h4->[$i] += $y;
                            # by source, isotope
                            $$h2->[$i] += $y;
                            # by source (all isotopes)
                            $$h3 += $y;
                        }
                    }
                    else
                    {
                        # Maths_Double version : 16.82s
                        # but requires prebuilt array references
                        $h->{source}[$source] //= [];
                        $h->{isotope} //= [];
                        $h->{all}[$source] += $dt * sum_array_of_doubles(\@x);

                        increment_two_arrays(
                            $dt,
                            $h->{isotope},
                            $h->{source}[$source],
                            \@x);
                    }
                }
            }
        }
        $lc++; # up line counter
    }

    if($source>=0)
    {
        # modulate yields with timestep
        if(0)
        {
            my $dt =
                $info->{bse_option}{yields_dt};
            map
            {
                $_ *= $dt;
            }
            (
             @{$h->{source}[$source]},
             $h->{all}[$source]
            );
        }

        # up mass_out_of_stars
        $info->{mass_out_of_stars} += $h->{all}[$source];
    }

    close $fp;

    return $h;
}
