#!/usr/bin/env perl

############################################################
#
# Plot JSON ensemble data from binary_c
#
############################################################

use strict;
use 5.16.0;
use Carp qw/confess/;
use Data::Dumper;
use Data::Dump::Streamer;
use Hash::Diff;
use IPC::Open2;
use IPC::Open3;
use JSON::Parse qw/json_file_to_perl/;
use List::Gen;
use List::Util qw/uniq/;
use open ':encoding(UTF-8)';
use POSIX;
use rob_misc;
use threads;
use threads::shared;
use threadsx::shared;
use robqueue;
use RobHashTools;
use Sort::Key qw/nsort/;
use Sort::Key::Natural qw/natsort/;
use Term::ANSIScreen qw/:cursor/;
use utf8;

############################################################
# find the ensemble file and load it
my $opts = parse_options();
my @data_sources = load_data(); # array of hash references

my @option_hashes = (
    'grid_options',
    'bse_options',
    );
open_gnuplot();
my $titlekeys = make_titlekeys();
plot_scalars() if($opts->{scalars});
plot_yields() if($opts->{yields});
plot_distributions() if($opts->{distributions});
plot_maps() if($opts->{maps});
plot_multimaps() if($opts->{multimaps});
close_gnuplot();
print "Done\n";
exit;

############################################################

sub vprint
{
    print @_ if($opts->{vb});
}
sub vvprint
{
    print @_ if($opts->{vb}>=2);
}
sub gprint
{
    vvprint $opts->{'gprint_header'}, @_;
    print {$opts->{'gnuplot_in'}} @_;
}
sub gsay
{
    vvprint $opts->{'gprint_header'}, @_,"\n";
    say {$opts->{'gnuplot_in'}} @_;
}

sub open_gnuplot
{
    # open a persistent gnuplot
    my $cmd = "env GNUPLOT_CAIRO_OPERATOR=$opts->{blend_mode} $opts->{gnuplot}";
    print "CMD $cmd\n";
    open($opts->{'gnuplot_in'},
         '|-',
         $cmd);
    $opts->{'gnuplot_in'}->autoflush(1);
    $opts->{'gnuplot_in'}->binmode(':utf8');

    # set up gnuplot terminal
    gprint  '',
        "set encoding utf8\nset print \"-\"\n",
        'set terminal ',
        $opts->{'gnuplot_terminal'},' ',
        " font \"",$opts->{'gnuplot_font'},"\" fontscale ",$opts->{'gnuplot_fontscale'},' ',
        $opts->{'gnuplot_solid'},
        ' linewidth ',$opts->{'gnuplot_linewidth'},
        ' dashlength ',$opts->{'gnuplot_dashlength'},
        $opts->{'gnuplot_colour'},"\n",
        "\n",
        # output file''
        "set output \"",$opts->{'gnuplot_outfile'},"\"",
        "\n";

    gprint "
    set output \"$opts->{gnuplot_outfile}\"
    set auto fix
set offsets $opts->{gnuplot_offsets}
    set zero 0.0
set xlabel \"Time / Myr\"
set format y ",'"%g"',"
set xrange\[$opts->{xlow}:$opts->{xhigh}\]
set yrange\[$opts->{ylow}:$opts->{yhigh}\]
\n\n";

    # margins
    foreach my $margin (keys %{$opts->{gnuplot_margins}})
    {
        if(defined $opts->{gnuplot_margins}{$margin})
        {
            gprint 'set '.$margin."margin $opts->{gnuplot_margins}{$margin}\n";
        }
    }

    # override key width if required
    if(defined $opts->{gnuplot_key_width} &&
       $opts->{gnuplot_key_location}!~s/width -?\d+/width $opts->{gnuplot_key_width}/)
    {
        $opts->{gnuplot_key_location}.=" width $opts->{gnuplot_key_width} ";
    }

    # key location
    gsay $opts->{gnuplot_key} ? "set key $opts->{gnuplot_key_location}" : 'unset key';

    # logscales
    foreach my $axis (keys %{$opts->{'gnuplot_logscale'}})
    {
        gprint sprintf(
            "%s%s%s\n",
            $opts->{'gnuplot_logscale'}{$axis}==0 ? 'un' : '',
            'set logscale ',
            $axis
            );
        if($axis=~/y/)
        {
            gprint 'set format '.$axis.' '.($opts->{'gnuplot_logscale'}{$axis}==0 ? '"%g"' : '"10^{%L}"')."\n";
        }
    }
}

    sub close_gnuplot
{
    # shut down gnuplot
    gprint "exit\n";
    waitpid $opts->{'gnuplot_pid'},0;
}

sub plot_scalars
{
    # make master scalar type list
    my $data_types = {};
    my @timerange = ();
    foreach my $data_source (@data_sources)
    {
        my $json = $data_source->{'json'};
        foreach my $scalar_type (keys %{$json->{'ensemble'}->{'scalars'}})
        {
            $data_types->{$scalar_type}++;

            # send to gnuplot as two-column data
            my $logtimes = rob_misc::truefalse_to_boolean($data_source->{'json'}->{'ensemble'}->{'info'}->{'bse_options'}->{'ensemble_logtimes'});
            my $data = $data_source->{'json'}->{'ensemble'}->{'scalars'}->{$scalar_type};

            if(defined $data)
            {
                my @times = nsort keys %$data;
                if($#times > -1)
                {
                    my $t = $logtimes ? (10.0**$times[0]) : $times[0];
                    $timerange[0] //= $t;
                    $timerange[0] = rob_misc::MIN($t,$timerange[0]);
                    $t = $logtimes ? (10.0**$times[$#times]) : $times[$#times];
                    $timerange[1] //= $t;
                    $timerange[1] = rob_misc::MAX($t,$timerange[1]);
                }
            }
        }
    }

    my @data_types = natsort keys %$data_types;
    $timerange[0] //= '*';
    $timerange[1] //= '*';

    foreach my $scalar_type (@data_types)
    {
        my $colour_count=-1;
        my $points_count=-1;
        $colour_count++ if($opts->{gnuplot_colour_by} eq 'datatype');
        $points_count++ if($opts->{gnuplot_points_by} eq 'datatype');
        my $data_source_count = 0;
        my $plotcomma = 'plot';

        gprint "
reset
set logscale x
set xlabel \"Time / Myr\"
set xrange[$timerange[0]:$timerange[1]]
            ";
        if($opts->{'gnuplot_title'})
        {
            gprint "set title \"",format_title($scalar_type),"\"\n";
        }

        # loop to construct plot instructions
        foreach my $data_source (@data_sources)
        {
            # up counters
            $data_source_count++;
            $colour_count++ if($opts->{'gnuplot_colour_by'} eq 'dir');
            $points_count++ if($opts->{'gnuplot_points_by'} eq 'dir');

            # set plotting options
            my $lineopts;
            if($opts->{'gnuplot_with'}=~/^l/)
            {
                # lines options
                $lineopts = ' lt '.($data_source_count+1);
            }
            if($opts->{'gnuplot_with'}=~/^(lp|points|linespoints)/)
            {
                # lines-points options
                $lineopts .= ' pt '.$data_source_count++;
            }
            $lineopts .= ' ';

            # line colour
            $lineopts .= " linecolor rgbcolor \"\#".
                $opts->{'gnuplot_colours'}[($colour_count)%$#{$opts->{'gnuplot_colours'}}].'" ';

            # per-line title
            my $title = '';
            foreach my $x (@option_hashes)
            {
                my $keys = $titlekeys->{$x};
                if(scalar @$keys)
                {
                    foreach my $k (@$keys)
                    {
                        $title .= $k.'='.$data_source->{'json'}->{'ensemble'}->{'info'}->{$x}->{$k}.' ';
                    }
                    $title = format_title($title);
                }
            }

            # send gnuplot plotting instructions
            # if we have data
            my $data = $data_source->{'json'}->{'ensemble'}->{'scalars'}->{$scalar_type};   if(keys %$data)
                                                                                            {
                                                                                                gprint "$plotcomma \"-\" title \"$title\" with $opts->{gnuplot_with} $lineopts ";
                                                                                                $plotcomma = ', ';
                                                                                            }
        }

        # end plotting instructions
        gprint "\n";

        foreach my $data_source (@data_sources)
        {
            # send to gnuplot as two-column data
            my $logtimes = rob_misc::truefalse_to_boolean($data_source->{'json'}->{'ensemble'}->{'info'}->{'bse_options'}->{'ensemble_logtimes'});
            my $data = $data_source->{'json'}->{'ensemble'}->{'scalars'}->{$scalar_type};
            my @ks = nsort keys %$data;
            if(scalar @ks)
            {
                foreach my $k (@ks)
                {
                    gprint ($logtimes ? (10.0**$k) : $k) ,' ',$data->{$k},"\n";
                }
                gprint "e\n"; # end data entry
            }
        }
    }
    vprint "\n";
}

sub plot_maps
{
    if(1)
    {
        # integrated theoretical HRDs
        gprint "\n";
        my @systems = ('0','1','primary','secondary');
        foreach my $star (@systems)
        {
            my $location = ['json','ensemble','HRD','star',$star];
            my $name = 'Time-integrated theoretical HRD '.(is_numeric($star) ? ('star '.($star+1)) : $star);
            print "Make map $name\n";
            gprint "\nreset\n";
            plot_map($name, $location, {
                ygrep => 'logL'
                     });
        }

        # integrated observational HRDs
        foreach my $star (@systems)
        {
            my $location = ['json','ensemble','HRD','star',$star];
            my $name = 'Time-integrated observational HRD '.(is_numeric($star) ? ('star '.($star+1)) : $star);
            print "Make map $name\n";
            gprint "\nreset\n";
            plot_map($name, $location, {
                ygrep => 'logg'
                     });
        }
    }


    # plot stellar merger statistics
    {
        my $name = 'Stellar mergers';
        foreach my $type ('log10masses',
                          'stellar types',
                          'spectral type floats')
        {
            my $location = ['json','ensemble','mergers',$type];
            foreach my $extraopts (
                {},{'zlogscale'=>1}
                )
            {
                my $title =
                    $name.' '.
                    $type.' '.
                    ($extraopts->{'zlogscale'}==1 ? '(log)' : '(linear)');
                plot_map($title,
                         $location,
                         $extraopts);
            }
        }

        if(0)
        {
            # special case: normalized spectral type floats
            #
            # ### THIS IS BROKEN ###
            # we cannot normalize a count to a rate: it's just wrong
            #
            foreach my $extraopts (
                {},{'zlogscale'=>1}
                )
            {
                my $title =
                    $name.
                    ' spectral type floats (normalized '.($extraopts->{'zlogscale'}==1 ? 'log' : 'linear').')';
                plot_map($title,
                         ['json','ensemble','mergers','spectral type floats'],
                         {
                             %$extraopts,
                                 'normalizer map' => ['json','ensemble','spectral types','spectral type floats']
                         });
            }
        }
    }

}

sub plot_multimaps
{
    # time-dependent HRDs
    foreach my $star ('0','1','primary','secondary')
    {
        my $name = 'theoretical HRD(t) '.(is_numeric($star) ? ($star+1) : $star);
        plot_multimap(
            $name,
            ['json','ensemble','HRD(t)','time'],
            ['json','ensemble','HRD(t)','time',undef,'star',$star],
            {
                subs => {
                    #xtics => sub {' '} ,
                    label => sub {
                        $_[0] = 10.0**$_[0];
                        # format time label
                        $_[0] < 10.0 ? (sprintf "%.2f Myr", $_[0]) : # 0-10 Myr
                            $_[0] < 1000.0 ? (sprintf "%.0f Myr", $_[0]) : # 10-1000Myr
                            (sprintf "%.1f Gyr", $_[0]/1000.0); # >1Gyr
                    }
                },
                ygrep => 'logL'
            });
    }

    foreach my $star ('0','1','primary','secondary')
    {
        my $name = 'observational HRD(t) '.(is_numeric($star) ? ($star+1) : $star);
        plot_multimap(
            $name,
            ['json','ensemble','HRD(t)','time'],
            ['json','ensemble','HRD(t)','time',undef,'star',$star],
            {
                subs => {
                    #xtics => sub {' '} ,
                    label => sub {
                        $_[0] = 10.0**$_[0];
                        # format time label
                        $_[0] < 10.0 ? (sprintf "%.2f Myr", $_[0]) : # 0-10 Myr
                            $_[0] < 1000.0 ? (sprintf "%.0f Myr", $_[0]) : # 10-1000Myr
                            (sprintf "%.1f Gyr", $_[0]/1000.0); # >1Gyr
                    }
                },
                ygrep => 'logg'
            });
    }

    gprint "\n";
}

sub plot_map
{
    my ($name,$location,$extraopts) = @_;
    print "Plot map: $name\n";

    ############################################################
    # Plot map $name which is at hash location $location
    my $xdatatypes = {};
    my $ydatatypes = {};

    my @deferred_data;
    my @deferred_gnuplot_lines;

    # default options
    $extraopts->{'xgrep'} //= '.';
    $extraopts->{'ygrep'} //= '.';
    $extraopts->{'logscale_cb'} //= 0;

    vprint "Plot map $name at @$location\n";

    # title and map options for gnuplot
    my $mapopts = map_gnuplot_options($name,$extraopts);

    if($extraopts->{gnuplot_pre} !~/set title/)
    {
        push(@deferred_gnuplot_lines,
             "\nset title \"$name\"\n");
    }

    my $logscale = ($mapopts=~/set logscale cb/ ||
                    $extraopts->{'zlogscale'} == 1) ? 1 : 0;

    if(defined $extraopts)
    {
        if(exists $extraopts->{'gnuplot'})
        {
            push(@deferred_gnuplot_lines,
                 $extraopts->{'gnuplot'}."\n");
        }
        if(exists $extraopts->{'gnuplot_sub'})
        {
            push(@deferred_gnuplot_lines,
                 $extraopts->{'gnuplot_sub'}($extraopts)."\n");
        }
    }

    # get list of y datatypes
    my %all_ydatatypes;
    foreach my $data_source (@data_sources)
    {
        my $data = hash_lookup($data_source,$location,{nofail=>1});
        my ($xdt,$ydt) = map_datatypes($data,$extraopts);
        map
        {
            $all_ydatatypes{$_}=1;
        }@{$ydt};
    }
    my @all_ydatatypes = sort keys %all_ydatatypes;


    foreach my $ydatatype (@all_ydatatypes)
    {
        print "Plot y datatype $ydatatype\n";

        my $xlabel;
        my $ylabel;
        my $xrange = {};
        my $yrange = {};
        my $zrange = {};
        my $mapdata = {};
        my $dx = {};
        my $dy = {};
        my $nx = {};
        my $ny = {};

        # clear the deferred data
        @deferred_data=();
        @deferred_gnuplot_lines=();

        push(@deferred_gnuplot_lines,
             "
             $mapopts
             set rgbmax 1.0
             ");
        # get plot ranges, dx,dy, and maximum Z value
        # and get y datatypes
        foreach my $data_source (@data_sources)
        {
            #print "Data source $data_source at $data_source->{file}\n";
            my $logtimes = rob_misc::truefalse_to_boolean($data_source->{'json'}->{'ensemble'}->{'info'}->{'bse_options'}->{'ensemble_logtimes'});
            my $data = hash_lookup($data_source,$location,{nofail=>1});

            if(defined $data)
            {
                # find dx,dy
                ($dx->{$data_source},
                 $dy->{$data_source}) =
                    map_dx_dy($data,
                              {
                                  ydatatype=>$ydatatype
                              });
                vprint "DX $dx->{$data_source}, DY $dy->{$data_source}\n";

                if($dx->{$data_source} < 1e-10)
                {
                    print Data::Dumper::Dumper($data);
                    print "DX 0 or very small in data source $data_source : not enough data? skipping\n";
                    next;
                }
                if($dy->{$data_source} < 1e-10)
                {
                    print Data::Dumper::Dumper($data);
                    print "DY 0 or very small in data source $data_source : not enough data? skipping\n";
                    next;
                }

                if(defined $dx->{$data_source} &&
                   defined $dy->{$data_source})
                {
                    # depth is ok, have dx and dy

                    # find data ranges
                    ($xrange->{$data_source},
                     $yrange->{$data_source}) =
                        map_ranges($data,
                                   $ydatatype,
                                   $extraopts);

                    if($name =~/log v_eq/)
                    {
                        # bug in log v_eq data
                        $yrange->{$data_source}->[0] = MAX(0.05,
                                                           $yrange->{$data_source});
                    }

                    vprint "RANGE X @{$xrange->{$data_source}} : Y @{$yrange->{$data_source}}\n";

                    if($#{$xrange->{$data_source}} != 1 ||
                       $#{$yrange->{$data_source}} != 1)
                    {
                        vprint "RANGE failed @$location : no data? $#{$xrange->{$data_source}} $#{$yrange->{$data_source}}\n";
                    }
                    else
                    {
                        # x,y data types
                        ($xdatatypes->{$data_source},
                         $ydatatypes->{$data_source}) =
                            map_datatypes($data,$extraopts,{ydatatype=>$ydatatype});

                        vprint "DATATYPES X @{$xdatatypes->{$data_source}}, Y @{$ydatatypes->{$data_source}}\n";

                        # if there are > 1 or no data types, fail
                        if($#{$xdatatypes->{$data_source}}!=0)
                        {
                            print "xdata type fail ($#{$xdatatypes->{$data_source}} should be 0) in map $name of $data_source\n";
                            confess;
                        }

                        if(0 && $#{$ydatatypes->{$data_source}}!=0)
                        {
                            print "ydata type fail ($#{$ydatatypes->{$data_source}} should be 0) in map $name of $data_source\n";
                            print Data::Dumper::Dumper($data);
                            confess;
                        }
                    }
                }
            }
        }
        state $required_depth = 4;

        foreach my $data_source (@data_sources)
        {
            my $data = hash_lookup($data_source,$location,{nofail=>1});
            my $xdatatype = $xdatatypes->{$data_source}->[0];
            $xlabel //= format_title($xdatatype);

            my $normalizer_data;
            if(defined $extraopts->{'normalizer map'})
            {
                # normalize $z to the given map
                $normalizer_data = hash_lookup($data_source,
                                               $extraopts->{'normalizer map'});
                vprint "Normalizer map is $normalizer_data\n";
            }

            if(defined $data)
            {
                # the map should have a depth of 4
                my $depth = hash_max_depth($data);
                vprint "Data source $data_source, data $data, depth = $depth\n";

                # fail if the depth is wrong
                if($depth != $required_depth)
                {
                    print "error: plot_map expects to be given a location which has a hash depth of $required_depth : got $depth instead\n";
                    print join("\n",(split(/\n/,Data::Dumper::Dumper($data)))[0..10]);
                    confess;
                    return;
                }

                # make ylabel
                $ylabel = format_title($ydatatype);

                my $havedata = $xdatatype ? scalar keys %{hash_lookup($data_source,[@$location,$xdatatype])} : 0;
                vprint "Have $xdatatype, $ydatatype data? $havedata\n";

                if($havedata &&
                   $xrange->{$data_source}->[1] > $xrange->{$data_source}->[0] &&
                   $yrange->{$data_source}->[1] > $yrange->{$data_source}->[0])
                {
                    my $functions = map_functions($name);

                    # loop over the x,y ranges with the appropriate step
                    # and output either the data we have or zero
                    vprint sprintf("X range %g to %g, d %g\n",
                                   $xrange->{$data_source}->[0],
                                   $xrange->{$data_source}->[1],
                                   $dx->{$data_source});
                    vprint sprintf("Y range %g to %g, d %g\n",
                                   $yrange->{$data_source}->[0],
                                   $yrange->{$data_source}->[1],
                                   $dy->{$data_source});

                    my $xs = range($xrange->{$data_source}->[0],
                                   $xrange->{$data_source}->[1],
                                   $dx->{$data_source});
                    my $ys = range($yrange->{$data_source}->[0],
                                   $yrange->{$data_source}->[1],
                                   $dy->{$data_source});

                    $nx->{$data_source} = scalar @$xs;
                    $ny->{$data_source} = scalar @$ys;

                    my $h = $data->{$xdatatype};
                    my $n = 0;
                    my $nhave = 0;

                    printf "Analyse %d x %d = %d points\n",
                        scalar @$xs,
                        scalar @$ys,
                        scalar @$xs * scalar @$ys;

                    #print scalar @$xs," XS @$xs\n";
                    #print scalar @$ys," YS @$xs\n";

                    foreach my $x (@$xs)
                    {
                        my $xx = sprintf $opts->{'key_float_format'},$x;
                        my $h1 = $h->{$xx}->{$ydatatype};
                        foreach my $y (@$ys)
                        {
                            my $yy = sprintf '%g',$y;
                            my $X = $functions->[0]($xx);
                            my $Y = $functions->[1]($yy);
                            my $Z = $h1->{$yy};

                            # count number of potential data items
                            $n++;

                            if(defined $Z)
                            {
                                vvprint "Data $X,$Y,$Z -> ";

                                # if X is log(time), compensate to make
                                # the Z linear rates
                                if($extraopts->{x_logged} == 1 &&
                                   $ylabel =~/time/i &&
                                   $X != 0)
                                {
                                    $Z /= 10.0**$X;
                                }

                                # count number of data items we actually have
                                $nhave++;

                                # hence Zmin, Zmax, Zmin (nonzero)
                                $zrange->{$data_source} //= [];
                                my $zh = $zrange->{$data_source};
                                $zh->[0] = defined $zh->[0] ? MIN($zh->[0],$Z) : $Z;
                                $zh->[1] = defined $zh->[1] ? MAX($zh->[1],$Z) : $Z;
                                if($Z > 1e-100)
                                {
                                    $zh->[2] = defined $zh->[2] ? MIN($zh->[2],$Z) : $Z;
                                }
                                vvprint "$Z ",($Z==$zh->[1] ? " *** maximum ***": $Z==$zh->[0] ? " *** minimum *** " : ''),"\n";
                            }
                            else
                            {
                                # if no data, set to 0
                                $Z = 0.0;
                            }

                            my $f = 1.0; # multiplier

                            # perhaps normalize?
                            if(defined $normalizer_data &&
                               $Z != 0.0)
                            {
                                my $znorm = $normalizer_data->{$xdatatype}->{$x}->{$ydatatype}->{$y};
                                if(defined $znorm)
                                {
                                    $f = 1.0 / $znorm;
                                }
                                else
                                {
                                    print "Tried to normalize map '$name' at $xdatatype => $x => $ydatatype => $y : z = $Z, znorm = undefined : this is an error because the normalization data must be present if we are to divide by it\n";
                                    confess;
                                }
                            }

                            # place mapdata
                            $mapdata->{$data_source}->{$X}->{$Y} = $f * $Z;
                        }
                    }
                    vprint "$n points ($nhave from JSON)\n";
                }
            }
            else
            {
                warning("Map of $name from data source $data_source at @$location requires dx,dy but the automatic algorithm failed to find these\n");
                return;
            }
        }

        # strip data_sources without this map
        my @this_data_sources = grep {exists $mapdata->{$_}} @data_sources;

        # normalize the map, convert to RGB, output
        my $colours = $opts->{map_colours};
        my $n = 0;
        my $alpha = $opts->{alpha}; # 1 is darkest, 0 is transparent
        my $plotcomma = 'set rgbmax 1.0; plot';
        push(@deferred_gnuplot_lines,"\n");
        my $gpl;
        my $zranges=[];
        foreach my $data_source (@this_data_sources)
        {
            # gnuplot this map
            $gpl .= "$plotcomma dx=$dx->{$data_source} dy=$dy->{$data_source} \"-\" u 1:2:3:4:5:6 w rgbalpha title \"\"  ";
            $plotcomma=',';
        }
        push(@deferred_gnuplot_lines,"$gpl\n");

        foreach my $data_source (@this_data_sources)
        {
            my $h1 = $mapdata->{$data_source};
            my $nout = 0;
            my $deltaz = $logscale ?
                (safelog10($zrange->{$data_source}->[1]) - safelog10($zrange->{$data_source}->[2])) :
                $zrange->{$data_source}->[1];
            vprint "DELTA Z $deltaz from $zrange->{$data_source}->[0] and $zrange->{$data_source}->[1] (logscale? $logscale)\n";

            foreach my $x (nsort keys %$h1)
            {
                my $h2 = $h1->{$x};
                foreach my $y (nsort keys %$h2)
                {
                    my $l;
                    my $z = $h2->{$y};

                    if($z == 0.0)
                    {
                        # no data : white if first plot, otherwise transparent
                        $l = sprintf "%g %g 1 1 1 %d\n",
                            $x,
                            $y,
                            $n==0 ? 1 : 0;
                    }
                    else
                    {
                        # map $z to the range 0 to 1
                        if($logscale)
                        {
                            # use a logarithmic scale
                            $z =
                                $deltaz == 0.0
                                ?
                                safelog10(MAX($zrange->{$data_source}->[2],
                                              $zrange->{$data_source}->[1]))
                                :
                                MAX(1e-100,
                                    (safelog10($z) - safelog10($zrange->{$data_source}->[2]))
                                    / $deltaz);
                        }
                        else
                        {
                            # linear scale
                            $z = $z / $zrange->{$data_source}->[1];
                        }

                        # convert to RGBA
                        $l = sprintf "%g %g %g %g %g %g\n",
                            $x,
                            $y,
                            @{z_to_colour($colours,$n,$z)},
                            $alpha;
                    }
                    push(@deferred_data,$l) if(defined $l);
                    $nout++;
                }
            }

            vprint "found $nout points in the map\n";
            push(@deferred_data,"e\n");
            $zranges->[$n] = $zrange->{$data_source};
            $n++;
        }


        if($n == 0)
        {
            # nothing plotted
            @deferred_gnuplot_lines = ();
            @deferred_data = ();
            vprint "n=0 : nothing plotted\n";
        }
        else
        {
            # if not deferred, output now
            if(!$extraopts->{defer})
            {
                vprint "Not deferred\n";
                gprint "\n
                reset
                set xlabel \"$xlabel\"
                set ylabel \"$ylabel\"
                set rmargin screen $opts->{map_rmargin}
                ".fake_colourbox(1,$zranges)."\n";

                my $was = $opts->{'gprint_header'};
                map
                {
                    gprint $_;
                }@deferred_gnuplot_lines;
                $opts->{'gprint_header'} = '>gdata> ';
                map
                {
                    gprint $_;
                }@deferred_data;
                $opts->{'gprint_header'} = $was;
            }
        }
#            close_gnuplot();exit;
    } # loop over y datatypes

#    close_gnuplot();exit;
    return (\@deferred_gnuplot_lines,
            \@deferred_data);
}

sub plot_yields
{
    # plot yields
    # loop through data sources, find available times, sources, isotopes
    my %times;
    my %sources;
    my %isotopes;
    my @timerange = (undef,undef);
    my $timestep = {};
    my $yrange = {};
    foreach my $data_source (@data_sources)
    {
        my $logtimes = rob_misc::truefalse_to_boolean($data_source->{'json'}->{'ensemble'}->{'info'}->{'bse_options'}->{'ensemble_logtimes'});
        my $yielddata = $data_source->{'json'}->{'ensemble'}->{'Xyield'};
        my $h0 = $yielddata->{'time'};
        my @times = nsort keys %$h0;

        $timestep->{$data_source} = $times[1] - $times[0];
        foreach my $t (@times)
        {
            $times{$t}=1;
            my $realt = $logtimes ? 10.0**$t : $t;
            $timerange[0] //= $realt;
            $timerange[1] //= $realt;

            $timerange[0] = rob_misc::MIN($timerange[0],$realt);
            $timerange[1] = rob_misc::MAX($timerange[1],$realt);

            my $h1 = $h0->{$t}->{'source'};
            foreach my $source (keys %$h1)
            {
                $sources{$source}=1;
                my $h2 = $h1->{$source}->{'isotope'};
                foreach my $isotope (#grep {/Fe/}
                                     keys %$h2)
                {
                    $isotopes{$isotope}=1;

                    my $x = hash_lookup($yielddata,
                                        ['time',$t,'source',$source,'isotope',$isotope],
                                        {nofail=>1});

                    # save yield range
                    $yrange->{$isotope}->[0] //= $x;
                    $yrange->{$isotope}->[1] //= $x;
                    $yrange->{$isotope}->[0] =
                        MIN($yrange->{$isotope}->[0],$x);
                    $yrange->{$isotope}->[1] =
                        MAX($yrange->{$isotope}->[1],$x);

                }
            }
        }
    }

    my @times = nsort keys %times;
    my @sources = natsort keys %sources;
    my @isotopes = natsort keys %isotopes;
    gprint "\nreset
set logscale y
set logscale x
set xlabel \"Time / Myr\"
set xrange[$timerange[0]:$timerange[1]]
";
    print "Times : @times\n";
    print "Sources : @sources\n";
    print "Isotopes : @isotopes\n";

    foreach my $data_source (@data_sources)
    {
        my $yielddata = $data_source->{'json'}->{'ensemble'}->{'Xyield'};
        print "YIELDDATA $yielddata\n";

        if(defined $yielddata)
        {
            my $logtimes = rob_misc::truefalse_to_boolean($data_source->{'json'}->{'ensemble'}->{'info'}->{'bse_options'}->{'ensemble_logtimes'});
            foreach my $isotope (@isotopes)
            {
                my $plotcomma = 'plot';

                # set yrange to view the first 6 orders of magnitude
                # only, otherwise the very small yields will get in the
                # way
                my @plots = (
                    sprintf("set yrange[%g:%g]\n",
                            $yrange->{$isotope}->[1]*1e-6,
                            $yrange->{$isotope}->[1],
                    )
                    );
                my @data;
                my $ytot = 0.0;

                my $isotope_info = $data_source->{'json'}->{'ensemble'}->{'info'}->{'isotopes'}->{$isotope};
                foreach my $source (@sources)
                {
                    my $newdata = 0;
                    my $thisy = 0.0;
                    my @thisdata;
                    foreach my $time (@times)
                    {
                        # $x is the rate of loss of yields per unit time per unit mass into stars
                        my $x = hash_lookup($yielddata,
                                            ['time',$time,'source',$source,'isotope',$isotope],
                                            {nofail=>1});




                        # linear time
                        my $T = $logtimes ? (10.0**$time) : $time;
                        my $dt;

                        # linear timestep
                        #                        $dt =
 #                           $logtimes ?
  #                          ($T - 10.0**($time - $timestep->{$data_source})) :
   #                         $timestep->{$data_source};

                        # yield should be dy/dt, but is actually just dy
                        $dt = $logtimes ?
                            ($T*log(10.0)*$timestep->{$data_source}) :
                            $timestep->{$data_source};
                        $x /= $dt;

                        if(defined $x && $x > 0.0)
                        {
                            # found data : add it
                            push(@thisdata,$T.' '.$x);

                            #print "TIME (log? $logtimes) $time dt $timestep->{$data_source} -> $T, $dt\n";
                            my $dy = $x * $dt;
                            $ytot += $dy;
                            $thisy += $dy;
                            $newdata = 1;
                        }
                        else
                        {
                            push(@thisdata,
                                 $T.' 0');
                        }
                    }

                    # have data: add plot
                    if($newdata)
                    {
                        push(@data,@thisdata,'e');
                        if($plotcomma eq 'plot')
                        {
                            gprint "set title \"".format_isotope($isotope)." : {/:Italic Z}=".$isotope_info->{'atomic number'}." {/:Italic m}_{u}=".sprintf('%.1f',$isotope_info->{'mass/AMU'})." Da\"\n";
                        }
                        $thisy = sprintf '%.3g',$thisy;
                        push(@plots,
                             "$plotcomma \"-\" u 1:2 w $opts->{gnuplot_with} title \"".format_title($source)." $thisy __FRAC__\" ");
                        $plotcomma = ',';
                    }
                } # source loop

                if(scalar @data)
                {
                    # fix plots
                    map
                    {
                        s!(\S+) __FRAC__!'{/\"Lucida Sans Typewriter\"'.sprintf("%9.3g",$1).' '.sprintf('% 6.2f%%',100.0*$1/$ytot).'}'!ge;
                    }@plots;

                    # send plots
                    gprint "\n",join('',@plots,'');

                    # send data
                    gprint "\n",join("\n",@data,'');
                }
            } # isotope loop
        }
    }
}

sub plot_distributions
{
    my @prefix = ('json','ensemble','distributions');

    ############################################################
    # mass and luminosity distributions
    #
    my $distributions = [natsort @{keys_at_depth(\@data_sources,['distributions'],1)}];
    my @prefix = ('json','ensemble','distributions');

    print "Distributions\n";
    print join("\n",@$distributions);
    print "\n\n\n";

    foreach my $distribution (@$distributions)
    {
        print "\n\nMake plot $distribution\n";

        # simple distributions
        if($distribution =~ /(.*)? \: label(int)?(->t)?->(dist|map)/)
        {

            my ($name, $label_int, $time_slices, $plot_type)
                = ($1,
                   defined $2 ? 1 : 0,
                   defined $3 ? 1 : 0,
                   $4);
            print "Is a labelled 1D distribution of type $plot_type (integer label? $label_int, time slices? $time_slices)\n";


            # in the following set x to be the variable
            # and y to be the distribution: this will be
            # altered as necessary for maps

            # get list of available labels
            my $labels;

            if($label_int == 0)
            {
                # simple list of scalar keys as labels
                $labels = [[natsort @{keys_at_depth(\@data_sources,
                                                    ['distributions',$distribution],1)
                            }]];
            }
            else
            {
                # key <int> pairs, e.g. to represent
                # "star 0" or "star 1"
                #
                # these are stored as
                # "key" :
                #    { # nested hash
                #        "0" : { ... data ... },
                #        "1" : { ... data ... }
                #        ...
                #    }
                #
                # so we must loop to recover the keys of
                # the nested hash, then construct labels from
                # pairs of "key","0" and "key","1" etc.
                #
                # This is each to do with keys_at_depth()
                #
                my @keylist =
                    natsort @{keys_at_depth(\@data_sources,
                                            ['distributions',$distribution],1)};

                print "Subkeylist @keylist\n";

                foreach my $key (@keylist)
                {
                    my ($nested_key_list) =
                        keys_at_depth(\@data_sources,
                                      ['distributions',$distribution,$key],1);
                    foreach my $nested_key (natsort @$nested_key_list)
                    {
                        push(@$labels,
                             [$key,$nested_key]);
                    }
                }
            }

            foreach my $label (@$labels)
            {
                my $title = $name.' '.join(' ',@$label);

                print "Plot title $title\n";
                my $location = [@prefix,$distribution,@$label];

                if($plot_type eq 'map' && $time_slices == 0)
                {
                    print "Make map at @$location\n";
                    my $data = hash_lookup($data_sources[0],
                                           $location,
                                           {nofail=>1});

                    # map y->z, x->y in gnuplot_pre options
                    # because time is now the x-axis
                    my $pre = gnuplot_pre_map($name);

                    plot_map(
                        $name,
                        $location,
                        {
                            'gnuplot_pre' => $pre.'
                                set title "'.format_title($title).'"
                                ',
                                'x_logged'=>1,
                        });
                }
                elsif($time_slices == 1)
                {
                    # how do we make a plot of multiple mapped time slices?
                    # like the HRD? tricky...

                    print "Make time-slice map at @$location\n";
                    next;

                    my $data = hash_lookup($data_sources[0],$location,{nofail=>1});

                    # map y->z, x->y in gnuplot_pre options
                    # because time is now the x-axis
                    my $pre = gnuplot_pre_map($name);
                    plot_map(
                        $name,
                        $location,
                        {
                            'gnuplot_pre' => $pre.'
                                set title "'.format_title($title).'"
                                ',
                                'x_logged'=>1,
                        });
                }
                else
                {
                    my $pre = gnuplot_pre($name);
                    plot_distribution(
                        $name,
                        $location,
                        {
                            'gnuplot_pre' => $pre.'
                                set title "'.format_title($title).'"
                                '
                        });
                }
            }
        } # label is simple or int

        # maps
        #
        # todo: support map->dist and map->t->dist
        elsif($distribution =~ /(.*)? \: map$/)
        {
            # simple map
            my ($name, $have_label, $time_slices) = ($1,
                                                     defined $2 ? 1 : 0,
                                                     defined $3 ? 1 : 0);
            print "Make simple map $distribution\n";
            my $pre = gnuplot_pre($distribution,'map');
            my $title = $name;
            my $location = [@prefix,$distribution];

            print "Plot title $title\n";
            if($time_slices == 1)
            {
                print "Make map at @$location\n";
                my $data = hash_lookup($data_sources[0],$location,{nofail=>1});

                # map y->z, x->y in gnuplot_pre options
                # because time is now the x-axis
                my $pre = gnuplot_pre_map($name);

                plot_map(
                    $name,
                    $location,
                    {
                        'gnuplot_pre' => $pre.'
                            set title "'.format_title($title).'"
                            ',
                            'x_logged'=>1,
                    });
            }
        }
        else
        {
            print "Unknown distribution\n";
            confess;
        }
    }
}

sub plot_distribution
{
    my ($name,$location,$extraopts) = @_;
    my @deferred_data;
    print "Plot distribution $name in @data_sources\n";
    my $xlabel;

    # get all x
    my %xvalues;
    foreach my $data_source (@data_sources)
    {
        my $data = hash_lookup($data_source,$location,{nofail=>1});
        my @keys = keys %$data;


        if(!defined $data)
        {
            print "I tried to look in data_source=$data_source for a hash at ",join('->',map{'"'.$_.'"'}@$location)," but found nothing\n";
        }
        if(scalar @keys > 1)
        {
            print "Error: trying to plot a distribution with more than one x-key (found ",scalar @keys,"): keys are @keys\n";
            confess;
        }
        $xlabel //= format_title($keys[0]);
        print "xlabel $xlabel\n";
        foreach my $x (keys %{$data->{$keys[0]}})
        {
            $xvalues{$x} = 1;
        }
    }

    my @xvalues = nsort keys %xvalues;
    my $normalizer;
    print "Found ",scalar @xvalues," raw x value".($#xvalues == 1 ? '' : 's')." at location = @$location\n";

    if($#xvalues==0)
    {
        # we have only one data item:
        # insert a ficticious bin width
        # which will give a 0
        push(@xvalues,$xvalues[0]+0.1);
    }

    # limit xrange if required for the plot
    if(defined $extraopts->{'gnuplot_pre'} &&
       $extraopts->{'gnuplot_pre'}=~/set\s+xrange\[(\S+)\:(\S+)\]/)
    {
        my @range = ($1,$2);
        $range[0] = -1e100 if($range[0] eq '*');
        $range[1] = +1e100 if($range[1] eq '*');
        @xvalues = grep {
            $_ > $1 && $_ < $2
        }@xvalues;
    }
    #print "PostPRE @xvalues\n";

    if(scalar @xvalues)
    {
        # find dx
        #my $dx = distribution_dx(\@xvalues);
        my $dx = minimum_dx(\@xvalues);
        #print "XVALS @xvalues\nDX $dx\n";

        my @xvalues = @{range(
                            $xvalues[0],
                            $xvalues[$#xvalues],
                            $dx
                            )};
        @xvalues = map{
            $_ = sprintf '%g', $_;
        } @xvalues;
        #print "XVALS2 ",join(' ',@xvalues),"\n";

        # make distributions
        my $gpl;
        my $plotcomma = "reset\nset xlabel \"".$xlabel."\"\n".
            ($extraopts->{'gnuplot_pre'}//'')."\n plot ";

        foreach my $data_source (@data_sources)
        {
            my $data = hash_lookup($data_source,$location,{nofail=>1});

            my @keys = keys %$data;
            #print "Use key 0 $keys[0]\n";
            $data = $data->{$keys[0]};
            #print Data::Dumper::Dumper($data);

            my $normalizer = defined $extraopts->{'normalize to'} ?
                hash_lookup($data_source,$extraopts->{'normalize to'}) : undef;

            foreach my $x (@xvalues)
            {
                my $y = $data->{$x} // 0.0;
               # print "$x -> $y\n";

                if($y != 0.0 &&
                   defined $normalizer)
                {
                    if(!defined $normalizer->{$x})
                    {
                        print "Tried to normalize plot $name at $data_source but at x=$x there is no normalizing data\n";
                    }
                    $y /= $normalizer->{$x};
                }

                push(@deferred_data,
                     $x.' '.$y."\n");
            }


            push(@deferred_data,"e\n");
            $gpl .= $plotcomma.' "-" u 1:2 w lp title "'.$name.'" ';
            $plotcomma = ',';
        }
        $gpl .= "\n".$extraopts->{'gnuplot_post'}//'';

        # make plots
        gprint $gpl;
        map
        {
            gprint $_;
        }@deferred_data;
    }
}

sub load_data
{
    # load in ensemble data from directories given on the command line

    # default to current directory
    my @locations = @ARGV;
    @locations = ('.') if($#locations == -1);
    my @data_sources; # returned
    foreach my $location (@locations)
    {
        my $file = choose_file($location);
        if(defined $file)
        {
            # load JSON data from file, put hash
            # reference to the data in small info hash

            print "Loading JSON from $file\n";
            my $json = json_file_to_perl($file);

            # you should convert numerical keys
            # using %g to make sure they match
            if($opts->{convert_numerics})
            {
                print "Converting numeric elements\n";
                force_numeric_elements($json,{
                    format => $opts->{'key_float_format'},
                    vb => 0,
                    modulator => 1.0
                                       });
            }
            print "Done loading\n";
            push(@data_sources,
                 {
                     location => $location,
                     file => $file,
                     json => $json,
                 });
        }
    }
    return @data_sources;
}

sub choose_file
{
    # find file location, or undef on failure
    my ($location) = @_;
    my $file =
        -f $location ? $location :
        (-d $location && -f ($location.'/ensemble.json')) ? ($location.'/ensemble.json') :
        undef;
    return $file;
}

sub parse_options
{
    my $opts = {
        gnuplot => '/home/rgi/bin/gnuplot',
        gnuplot_terminal => 'pdfcairo color enhanced size 35cm,21.0cm',
        gnuplot_colour => 'colour',
        gnuplot_font => 'Helvetica',
        gnuplot_fontsize=>22,
        gnuplot_fontscale=>1.0,
        gnuplot_solid=>'',
        gnuplot_linewidth=>6,
        gnuplot_dashlength=>4,
        gnuplot_outfile=>'ensemble.pdf',
        gnuplot_with=>'lp ps 0.86 lw 1',
        gnuplot_key=>1,
        gnuplot_key_location=>'top right outside samplen 0.8 opaque font "Helvetica,14"',
        gnuplot_linetitle_fontsize=>14,
        gnuplot_logscale=>{
            'x'=>1,
                'y'=>1,
                'x2'=>0,
                'y2'=>0
        },
        gnuplot_colours=>[
            '0000ff','007f00','ff0000','00bfbf','bf00bf','bfbf00','3f3f3f','bf3f3f','bb9900','3f3fbf','bfbfbf','00ff00','c1912b','89a038','5691ea','ff1999','e0bfba','197c77','a856a5','fc683a'
            ],
        gnuplot_colour_by=>'dir', # can be : order, dir, datatype
        gnuplot_points_by=>'dir', # can be : order, dir, datatype
        gnuplot_title=>1, # 1 = title, 0 = no title
        gnuplot_key_width=>0,
        gnuplot_offsets=>'0.1,0.1,0,0',
        gnuplot_margins=>{
            l=>'screen 0.1',
            b=>undef,
            t=>undef,
            r=>undef
        },
        blend_mode=>'DARKEN', # cairo blend mode, see https://cairographics.org/operators/ : DARKEN is good for us
        alpha=>0.8, # transparency for maps
        skip_zerocurves=>1, # if 1 skip curves which are just zero
        xlow=>'*',
        xhigh=>'*'
            ,
        ylow=>'*',
        yhigh=>'*',
        vb=>0,
        warnings=>0,
        scalars=>1,
        yields=>1,
        distributions=>1,
        maps=>1,
        multimaps=>1,
        key_float_format => '%g',
        map_rmargin => 0.8,
        map_cbox_rstart => 0.85,
        map_cbox_rend => 0.95,
        map_cbox_bstart => 0.1,
        map_cbox_bend => 0.83,
        map_colours => [
            [0,0,1], # blue
            [1,0,0], # red
            [0.7,0.7,0], # dark yellow
            [0.7,0,0.7], # magenta
            [0,1,1], # cyan
            ],
        convert_numerics => 1,
        gprint_header => '>gplot> '
    };

    foreach my $arg (@ARGV)
    {
        print "$arg\n";
        if($arg eq 'noplots')
        {
            foreach my $x ('scalars',
                           'yields',
                           'distributions',
                           'maps',
                           'multimaps')
            {
                $opts->{$x} = 0;
            }
        }
        elsif($arg=~/(\S+)=(.+)/ && exists $opts->{$1})
        {
            # override one of the default options
            $opts->{$1} = $2;
        }
        elsif(exists $opts->{$arg})
        {
            # set to 1
            $opts->{$arg} = 1;
        }
    }

    return $opts;
}
sub format_title
{
    # title for each gnuplot line
    my $dir=$_[0];
    my $fontsize=$_[1];
    my $t=$dir;
    if(defined $fontsize)
    {
        $t="{/*$fontsize $t}";
    }

    # most words > 3 chars are not acronyms
    $t=~s/([A-Z])([A-Z]{3,})/$1\L$2/g;

    # but some are....
    foreach my $string ('Cowd','Hewd','Tpagb','Eagb','Bhbh','Lmms','Nsbh','Nsns',
                        'Wdns','Wdwd','Wdbh','SN','SNII','SAGB','RLOF')
    {
        $t=~s/$string/\U$string/gi;
    }

    # and some are not
    $t=~s/PRE/pre/g;
    $t=~s/GK PER/GK Per/g;
    $t=~s/HOT/Hot/g;
    $t=~s/LUM/Luminosity/g;
    $t=~s/_OF_/_of_/g;
    $t=~s/_IN_/_in_/g;

    # common changes
    $t=~s!Z=!{/Italic Z}=!g;
    $t=~s!binary=0!single!g;
    $t=~s!binary=1!binary!g;

    # subscripts
    my $sf = 0.8;
    $t=~s!_(ce)!__{/*$sf CE}!g;
    $t=~s!_(ionisation)!__{/*$sf ion}!g;
    $t=~s!_(eq)!__{eq}!g;

    # greek
    $t=~s!dt!{/Symbol d}t!g;
    $t=~s!alpha!{/Symbol a}!g;
    $t=~s!lambda!{/Symbol l}!g;
    $t=~s!epsnov!{/Symbol e}__{/*$sf nov}!g;

    # underscores > spaces
    $t=~s/__/UNDERSCORE/g;
    $t=~s/_/ /g;
    $t=~s/UNDERSCORE/_/g;

    # reformat exponential notation
    $t =~ s/e([\+-])0*(\d+)/\{\/Symbol \\264\}10^\{\/*0.8 $1$2\}/g;

    $t =~ s!metallicity=!{/Italic Z}=!g;

    $t =~ s/\s+$//;
    $t =~ s/^\s+//;
    return $t;
}

sub make_titlekeys
{
    # make a diff of the options hashes for each file
    # and return it
    my $titlekeys = {};

    foreach my $x (@option_hashes)
    {
        my $prekeys = {};
        if(scalar @data_sources > 1)
        {
            my $main = $data_sources[0]->{'json'}->{'ensemble'}->{'info'}->{$x};
            foreach my $data_source (grep {$_ ne $data_sources[0]} @data_sources)
            {
                my $o = $data_source->{'json'}->{'ensemble'}->{'info'}->{$x};
                my ($diff1,$diff2,$why) = rob_misc::nested_hashdiff($main,$o);

                if(defined $diff1 && keys %$diff1)
                {
                    map{
                        $prekeys->{$_}++;
                    }grep {
                        ref $_ eq '' &&
                            $_ ne '' &&
                            $_ ne 'z' &&
                            $_ ne 'ensemble' &&
                            $_ ne 'command_line' &&
                            $_ ne 'flexigrid' &&
                            $_ ne 'perlscript_arguments' &&
                            $_ ne 'process_ID'
                    }keys %$diff1;
                }
            }
        }
        $titlekeys->{$x} = [natsort keys %$prekeys];
    }

    return $titlekeys;
}

# https://stackoverflow.com/questions/41089311/how-to-find-the-depth-of-a-nested-hash-of-hashes
{
    my $max;
    sub hash_max_depth
    {
        my ($h, $n) = @_;
        $max //= 1;
        $max = $n if $max < $n;
        __SUB__->($_, $n+1) for grep {ref eq "HASH"} values %$h;
        return $max+1;
    }
}

sub hash_lookup
{
    # loop up $location, which is a list reference,
    # in hash $h, return the data there or undef
    # on failure
    my ($h,$location,$opts) = @_;

    # check args of the right type
    if(ref $h ne 'HASH')
    {
        print "Called hash_lookup with a non-hash $h\n";
        confess;
    }
    elsif(ref $location ne 'ARRAY')
    {
        print "The second argument to hash_lookup should be an array reference, it is $location\n";
        confess;
    }

    # traverse the hash
    my @current;
    foreach my $y (@$location)
    {
        if(ref $h eq 'HASH')
        {
            $h = $h->{$y};
            push(@current,$y);
        }
        else
        {
            if(defined $opts && $opts->{nofail})
            {
                return undef;
            }
            else
            {
                print "Tried to look up location \"$y\" in hash \"$h\", but this is not a hash (made it to @current)\n";
                confess;
            }
        }
    }
    return $h;
}

sub map_datatypes
{
    # given a map hash, return ([xdatatypes],[ydatatypes])
    my ($data,$extraopts,$localopts) = @_;
    $extraopts //= {};
    $localopts //= {};
    my %xtypes;
    my %ytypes;

    foreach my $xlabel (grep {/$extraopts->{xgrep}/} keys %$data)
    {
        $xtypes{$xlabel} = 1;
        my $h1 = $data->{$xlabel};
        foreach my $x (keys %$h1)
        {
            my $h2 = $h1->{$x};
            foreach my $ylabel (grep {
                defined $extraopts->{ygrep} ? /$extraopts->{ygrep}/ :
                    defined $localopts->{ydatatype} ? $_ eq $localopts->{ydatatype} : 1
                                } keys %$h2)
            {
                $ytypes{$ylabel} = 1;
            }
        }
    }
    return ([natsort keys %xtypes],
            [natsort keys %ytypes]);
}

sub map_ranges
{
    # given a map hash, and a selected y data type,
    # return ([xmin,xmax], [ymin,ymax])
    my ($data,$ydatatype,$extraopts) = @_;
    $extraopts//={};

    my @xrange;
    my @yrange;

    foreach my $xlabel (grep {/$extraopts->{xgrep}/} keys %$data)
    {
        my $h1 = $data->{$xlabel};
        my @xkeys = nsort keys %$h1;
        if($#xkeys >= 0)
        {
            $xrange[0] = rob_misc::MIN($xrange[0]//$xkeys[0],
                                       $xkeys[0]);
            $xrange[1] = rob_misc::MAX($xrange[1]//$xkeys[$#xkeys],
                                       $xkeys[$#xkeys]);
            foreach my $x (@xkeys)
            {
                my $h2 = $h1->{$x};
                foreach my $ylabel ($ydatatype)
                {
                    my @ykeys = nsort keys %{$h2->{$ylabel}};
                    if($#ykeys >= 0)
                    {
                        $yrange[0] = rob_misc::MIN($yrange[0]//$ykeys[0],
                                                   $ykeys[0]);
                        $yrange[1] = rob_misc::MAX($yrange[1]//$ykeys[$#ykeys],
                                                   $ykeys[$#ykeys]);
                    }
                }
            }
        }
    }
    return (\@xrange,\@yrange);
}

sub minimum_dx
{
    # given an array reference, return the most likely delta
    my ($values) = @_;
    my $float_tolerance = 1e-6; # bin widths should not be < this
    my $prevx;
    my $mindx = 1e100;
    foreach my $x (nsort @$values)
    {
        if(defined $prevx)
        {
            my $dx = $x - $prevx;
            if($dx > $float_tolerance)
            {
                $mindx = MIN($mindx,$dx);
            }
        }
        $prevx = $x;
    }
    return sprintf'%g',$mindx;
}

sub distribution_dx
{
    # given an array reference, return the most likely delta
    my ($values) = @_;
    my $prevx;
    my %dxs;
    my $firstx;
    foreach my $x (nsort @$values)
    {
        $firstx//=$x;
        if(defined $prevx)
        {
            my $dx = $x - $prevx;
            $dxs{$dx}++;
        }
        $prevx = $x;
    }
    my $maxx;
    my $maxn = -1;
    map
    {
        if($dxs{$_} > $maxn)
        {
            $maxx = $_;
            $maxn = $dxs{$_};
        }
    }keys %dxs;
    return sprintf'%g',$maxx;
}

sub map_dx_dy
{
    my ($data,$localopts) = @_;
    # given a map hash, return the dx,dy if possible, otherwise undef,undef
    my $dx;
    my $dy;

    foreach my $xlabel (nsort keys %$data)
    {
        my $h1 = $data->{$xlabel};
        my @xkeys = nsort keys %$h1;
        if($#xkeys >= 1)
        {
            $dx //= $xkeys[1] - $xkeys[0];
            for(my $i=1; $i<=$#xkeys; $i++)
            {
                my $d = $xkeys[$i] - $xkeys[$i-1];
                if($d > 1e-10)
                {
                    $dx = rob_misc::MIN($dx, $d);
                }
                else
                {
                    #  print Data::Dumper::Dumper($data);
                    print "Difference between two x keys ($xkeys[$i] and $xkeys[$i-1]) is tiny (<1e-10) suggesting a fault in the data\n";
                    print nsort keys %{$data_sources[0]->{json}->{ensemble}->{mergers}->{log10masses}->{primary}};
                    print "\n";
                    confess;
                }
            }
        }

        # put all y keys into a single hash
        my %ykeys;
        foreach my $x (@xkeys)
        {
            my $h2 = $h1->{$x};
            my @ytypes =
                defined $localopts->{ydatatype} ?
                ($localopts->{ydatatype}) :
                (nsort keys %$h2);

            foreach my $ylabel (@ytypes)
            {
                map{
                    $ykeys{$_}=1;
                }nsort keys %{$h2->{$ylabel}};
            }
        }

        # convert the ykeys hash to an array
        my @ykeys = nsort keys %ykeys;
        if($#ykeys >= 1)
        {
            $dy //= $ykeys[1] - $ykeys[0];
            for(my $i=1; $i<=$#ykeys; $i++)
            {
                my $d = $ykeys[$i] - $ykeys[$i-1];
                if($d > 1e-10)
                {
                    $dy = rob_misc::MIN($dy, $d);
                }
                else
                {
                    print "Difference between two y keys is tiny (<1e-10) suggesting a fault in the data\n";
                    confess;
                }
            }
        }
    }
    if(defined $dx)
    {
        $dx = sprintf '%g',$dx;
    }
    if(defined $dy)
    {
        $dy = sprintf '%g',$dy;
    }

    return ($dx,$dy); # maybe undef!
}


sub map_functions
{
    my ($m) = @_;
    # given a map name, $m, define functions to map
    # the x and y data and return them as
    #
    # [
    #   sub { ... }, # x
    #   sub { ... }  # y
    # ]
    if($m=~/HRD/)
    {
        return [sub{$_[0]} ,
                sub{$_[0]}];
    }
    else
    {
        return [sub{$_[0]},
                sub{$_[0]}];
    }
}

sub map_gnuplot_options
{
    my ($m,$extraopts) = @_;

    my $x = $extraopts->{'gnuplot_pre'} // '';
    if($m=~/mergers/)
    {
        $x = '
unset logscale x
unset logscale y
set colorbox vertical default
set rmargin 20
set cbrange[0:1]
set palette defined (0 "blue", 1 "white")
            ';
        if($m=~/log10masses/)
        {
            $x .= '
set xlabel "log_{10}(Most-massive star mass / Msun)"
set ylabel "log_{10}(Least-massive star mass / Msun)"
                ';
        }
        elsif($m=~/spectral type floats/)
        {
            $x .= '
set xlabel "Primary spectral type"
set ylabel "Secondary spectral type"
set xrange[7:0]
set yrange[7:0]
                ';
            my @types = split(//,"OBAFGKM");
            foreach my $axis ('x','y')
            {
                my $comma='';
                $x.='set '.$axis.'tics mirror (';
                for(my $k=0;$k<=$#types+1;$k++)
                {
                    $x .= $comma.'"" '.$k.' 0 ';
                    $comma=',';
                }
                $x.=")\n";
                $comma='';
                $x.='set '.$axis.'tics add mirror (';
                for(my $k=0;$k<=$#types;$k++)
                {
                    $x .= $comma.'"'.$types[$k].'" '.($k+0.5).' 0 ';
                    $comma=',';
                }
                $x.=")\n";
            }
        }
        elsif($m=~/stellar types/)
        {
            $x .= '
set xlabel "Primary stellar type"
set ylabel "Secondary stellar type"
                ';
            my @types = ('LMMS','MS','HG','GB','CHeB','EAGB','TPAGB','HeMS','HeHG','HeGB','HeWD','COWD','ONeWD','NS','BH');
            foreach my $axis ('x','y')
            {
                my $comma='';
                $x.='set '.$axis.'tics mirror (';
                for(my $k=0;$k<=$#types+1;$k++)
                {
                    $x .= $comma.'"" '.$k.' 0 ';
                    $comma=',';
                }
                $x.=")\n";
                $comma='';
                $x.='set '.$axis.'tics add mirror (';
                for(my $k=0;$k<=$#types;$k++)
                {
                    $x .= $comma.'"'.$types[$k].'" '.($k+0.5).' 0 ';
                    $comma=',';
                }
                $x.=")\n";
            }
        }
    }
    elsif($m=~/HRD/)
    {
        # Hertzsprung-Russell diagram
        my $ylabel;
        my $ytics;
        if($m=~/observational/)
        {
            $ylabel = 'log({/:Italic g})';
            $ytics = 'autofreq';
        }
        else
        {
            $ylabel = 'log_{10}({/:Italic L} / L_{☉})';
            $ytics = '('.join(',',map{(sprintf'"%g"',10**$_).' '.$_}@{range -3,+6.6,+1 }).')';
        }
        $x = "
set logscale cb
set xlabel \"log_{10}({/:Italic T}_{eff}/K)\"
set ylabel \"$ylabel\"
                ";

        if($m=~/observational/)
        {
            # observational HRD
            $x .= "
set xrange[5:3.3]
set yrange[6:-1]
                ";
            if($m !~ /HRD\(t\)/)
            {
                $x .= "
set xtics
set ytics $ytics
unset x2tics
unset y2tics
set format x \"\%g\"
                ";
            }
            else
            {
#set xtics (".join(',',map{(sprintf'"%g"',$_)    .' '.log10($_)}(4000,6000,10000) ).") nomirror
#set ytics (".join(',',map{(sprintf'"%g"',10**$_).' '.       $_}@{range -3,+6.6,+1 }).") nomirror
                $x .= "
unset x2tics
unset y2tics
set format xy \"\"
set format y \"\"
                ";
            }
        }
        else
        {
            # theoretical HRD
            if($m !~ /HRD\(t\)/)
            {
                # not time dependent: always show tics and labels
                $x .= "
set yrange[-3:+6.6]
set xrange[5:3.3]
set xtics
set ytics $ytics
unset x2tics
unset y2tics
set format x \"\%g\"
set format y \"\"
        ";
            }
            else
            {
                # time dependent : always show tics, but not labels
#set xtics (".join(',',map{(sprintf'"%g"',$_)    .' '.log10($_)}(4000,6000,10000) ).") nomirror
#set ytics (".join(',',map{(sprintf'"%g"',10**$_).' '.       $_}@{range -3,+6.6,+1 }).") nomirror
                $x .= "
set yrange[-3:+6.6]
set xrange[5:3.3]
unset x2tics
unset y2tics
set format xy \"\"
set format y \"\"
";
            }
        }

    }

    $x .= $extraopts->{'gnuplot_post'} // '';
    return $x;
}

sub warning
{
    print "Warning: @_\n"if($opts->{warnings});
    return;
}

sub plot_multimap
{
    my ($name,
        $multikey_location,
        $data_location,
        $extraopts) = @_;
    $extraopts//={};

    my @this_data_sources;
    my %multikeys;
    foreach my $data_source (@data_sources)
    {
        my $h = hash_lookup($data_source,
                            $multikey_location,
                            {
                                nofail=>1
                            });
        if(defined $h)
        {
            push(@this_data_sources,$data_source);

            # get a global list of keys
            foreach my $k (natsort keys %$h)
            {
                $multikeys{$k}=1;
            }
        }
    }

    # options for gnuplot
    my $eopts = {
                gnuplot => "
        unset title
        set margins 0,0,0,0
        set xlabel \"\"
        set ylabel \"\"
        unset cbtics
        unset colorbox
        set tics front
        ",
                'defer' => 1,
                %{$extraopts//{}}
            };

    # hence the keys for the plots
    my @plotkeys;
    my $nplots = 0;
    my $multimap = {};
    my $q = robqueue->new(
        vb=>0,
        subr => sub {
            my ($multikey, $name, $this_map_location, $eopts) = @{$_[0]};
            my ($plot_gnuplot_lines, $plot_data)
                = plot_map($name,
                           $this_map_location,
                           $eopts);
            return
                $#{$plot_gnuplot_lines} != -1 ?
            {
                $multikey => {
                    'gnuplot' => $plot_gnuplot_lines,
                        'data' => $plot_data,
                }
            } : undef;
        },
        sumhash => $multimap
        );

    my $n = 0;
    foreach my $multikey (nsort keys %multikeys)
    {
        my $this_map_location = [ map { $_ // $multikey } @$data_location ];
        $q->q([$multikey,$name,$this_map_location,$eopts]);
        push(@plotkeys,$multikey);
    }
    $q->end();

    # count the number of plots
    $nplots = scalar keys %$multimap;
    print "$name : Got $nplots plots\n";

    # if we found plots, send the data to gnuplot
    #
    # sadly, it's impossible to multithread this :(
    if($nplots>0)
    {
        # try to make a square grid
        my $ncols = int(sqrt($nplots))+1;
        my $nrows = int($nplots / $ncols)+1;
        print "Found data for $nplots (@plotkeys), hence $ncols columns, $nrows rows\n";

        # header for multiplot
        gprint "
        reset
        set multiplot title \"\" layout $nrows,$ncols
        #margins 0.1,0.9,0.1,0.9 # broken

        # title
        set label 2 \"$name\" at screen 0.5, screen 0.987 center front
        ";
        my $ncol = 0;
        my $nrow = 0;
        my $nplot = 0;

        printf "%5.2f %% : $ncol x $nrow\n",100.0*(1.0*$nplot)/(1.0*$nplots);

        for my $row (1..$nrows)
        {
            for my $col (1..$ncols)
            {
                print ".";
                $nplot++;
                last if($nplot >= $nplots);
            }
            print "\n";
        }
        print up($nrows);
        $nplot = 0;

        foreach my $multikey (@plotkeys)
        {
            my $this_map_location = [ map { $_ // $multikey } @$data_location ];
            vprint "This map: @$this_map_location\n";

            # set plot label
            my $label = $multikey;
            if(defined $extraopts->{'subs'}->{'label'})
            {
                $label = $extraopts->{'subs'}->{'label'}($label);
            }
            gprint "\nset label 1 \"$label\" at graph 0.1,0.2 front\n";

            # show tics?
            my $xticformat = ($nrow == $nrows-1 ||
                              $nplot + $ncols >= $nplots) ? '%g' : '';
            my $yticformat = $ncol==-1 ? '%g' : '';

            # output gnuplot instructions
            map
            {
                s/set format x .*/set format x \"$xticformat\"/;
                s/set format y .*/set format y \"$yticformat\"/;
                gprint $_;
            }@{$multimap->{$multikey}->{'gnuplot'}};

            # output data : this is the slow part
            # because it is limited by gnuplot's ability
            # to read in data
            map
            {
                gprint $_;
            }@{$multimap->{$multikey}->{'data'}};

            #print "PLOT MAP $nrow, $ncol which is key $multikey\n";
            print "#";
            $ncol++;
            $nplot++;
            if($ncol+1 > $ncols)
            {
                $nrow++;
                $ncol=0;
                print "\n";
            }
        }
        print "\n";

        # close multiplot
        gprint "\n
        unset multiplot\n";
    }
}


sub order_isotope
{
    my $n = ($_[0]=~/(\d+)/)[0];
    my $x = ($_[0]=~/(\D+)/)[0];
    return $n.$x;
}

sub format_isotope
{
    my $n = ($_[0]=~/(\d+)/)[0];
    my $x = ($_[0]=~/(\D+)/)[0];
    return '^{'.$n.'}'.$x;
}

sub safelog10
{
    return POSIX::log10(MAX(1e-100,$_[0]));
}

sub spectral_types_gnuplot_labels
{
    # return axis labels for floating-point spectral types
    my ($axis) = @_;
    my @types = split(//,"OBAFGKM");
    my $comma='';
    my $x;
    $x.='set '.$axis.'tics mirror (';
    for(my $k=0;$k<=$#types+1;$k++)
    {
        $x .= $comma.'"" '.$k.' 0 ';
        $comma=',';
    }
    $x.=")\n";
    $comma='';
    $x.='set '.$axis.'tics add mirror (';
    for(my $k=0;$k<=$#types;$k++)
    {
        $x .= $comma.'"'.$types[$k].'" '.($k+0.5).' 0 ';
        $comma=',';
    }
    $x.=")\n";
    return $x;
}



sub gnuplot_pre
{
    my ($name) = @_;

    return
        $name =~/cbdisc/ ?
        '' :

        $name =~/mass/ ?
# mass requires limited x range
'
        set logscale y
        set xrange[-2:2]
' :
# luminosity requires limited x range
        $name=~/luminosity/ ?
'
        set logscale y
        set xrange[-5:6.6]
' :
                # equatorial velocity
        $name=~/v_eq/ ?
'
        set xrange[*:3]
        set logscale y
' :
        $name =~ /spectral type floats.*map$/ ?
        (
         spectral_types_gnuplot_labels('x')."\n".
         spectral_types_gnuplot_labels('y')."\n"
        )
  :
        $name=~/spectral type floats/ ?
        spectral_types_gnuplot_labels('x').
'
        set logscale y
' :

        # default
        '';

}

sub gnuplot_pre_map
{
    # map y->z, x->y in gnuplot_pre options
    # because time is now the x-axis
    my ($name) = @_;
    my $p = gnuplot_pre($name);
    foreach ('logscale')
    {
        $p=~s/$_ y/$_ z/g;
        $p=~s/$_ x/$_ y/g;
    }
    foreach ('label','range','tics','mtics')
    {
        $p=~s/y$_/z$_/g;
        $p=~s/x$_/y$_/g;
    }
    return $p;
}

sub z_to_colour
{
    # map z value to colour
    my ($colours,$n,$z) = @_;
    return [1.0 - $z * (1.0 - $colours->[$n]->[0]),
            1.0 - $z * (1.0 - $colours->[$n]->[1]),
            1.0 - $z * (1.0 - $colours->[$n]->[2])];
}

sub hexify
{
    # numbers in range 0-1 converted to hex
    my @float_colours = @_;
    map
    {
        $_ = sprintf ('%02X',255 * $_);
    }@float_colours;
    return @float_colours;
}

sub gnuplot_palette
{
    # return a palette definition
    my ($colours,$n) = @_;

    my $palette = 'set palette model RGB
set palette defined ( ';
    my $comma = '';
    for(my $z = 0.0; $z <= 1.0; $z+=0.1)
    {
        $palette .=
            sprintf "%s %g %g %g %g ",
            $comma,
            $z,
            @{z_to_colour($colours,$n,$z)};
        $comma = ',';
    }
    $palette .= ' )
        ';
    return $palette;
}


sub fake_colourbox
{
    # make a fake gnuplot colourbox for up to $n colours
    my ($n,$zranges) = @_;

    my $box = '';
    my $nobj = 1;
    my $dx = ($opts->{map_cbox_rend} - $opts->{map_cbox_rstart})/($n+1);
    my $dy = ($opts->{map_cbox_bend} - $opts->{map_cbox_bstart});
    my $dz = 0.1; # colourbox resolution, 0.1 is usually ok
    my $tags = [];
    my $labeln = 0;

    for(my $i=0; $i<$n; $i++)
    {
        # z delta
        my $dZ = $zranges->[$i]->[1] - $zranges->[$i]->[0];


        for(my $z = 0.0; $z < 1.0; $z+=$dz)
        {
            my $Z = $zranges->[$i]->[0] + $z * $dZ;
            $box .= sprintf
                "
# z=$z, dz=$dz, dy=$dy, Z=$Z, dZ=$dZ
set obj %d rectangle from screen %g,%g to screen %g,%g
set obj %d fillstyle noborder solid %s fc rgbcolor \"\#%s%s%s\"
",
                $nobj,
                $opts->{map_cbox_rstart} + $dx * $i,
                $opts->{map_cbox_bstart} + $dy * $z,
                $opts->{map_cbox_rstart} + $dx * ($i+1),
                $opts->{map_cbox_bstart} + $dy * ($z+$dz),

                $nobj,
                $opts->{alpha},
                hexify(@{z_to_colour($opts->{map_colours},$i,$z)});
            $nobj++;
        }

        # low end label
        $box .= sprintf("set label \"%s\" at screen %g,%g center textcolor rgbcolor \"black\" front\n",
                        $zranges->[$i]->[0],
                        $opts->{map_cbox_rstart} + $dx * ($i+0.5),
                        $opts->{map_cbox_bstart} + $dy * (0.5*$dz));

        # high end label
        $box .= sprintf("set label \"%s\" at screen %g,%g center textcolor rgbcolor \"white\" front\n",
                        $zranges->[$i]->[1],
                        $opts->{map_cbox_rstart} + $dx * ($i+0.5),
                        $opts->{map_cbox_bstart} + $dy * 1.05
             );

        # surrounding box
        $box .= sprintf("set obj %d rectangle from screen %g,%g to screen %g,%g lw 0.7\nset obj %d fillstyle transparent border -1 lc black\n",
                        $nobj,
                        $opts->{map_cbox_rstart} + $dx * $i,
                        $opts->{map_cbox_bstart} + $dy * 0,
                        $opts->{map_cbox_rstart} + $dx * ($i+1),
                        $opts->{map_cbox_bstart} + $dy * 1.1,
                        $nobj);
        $nobj++;

    }

    #print "BOX $box\n";
    return $box;
}
