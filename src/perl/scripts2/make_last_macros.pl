#!/usr/bin/env perl
use strict;
my $maxi = 69;


print "/* This will let macros expand before concating them */
#define PRIMITIVE_CAT(x, y) x ## y
#define CAT(x, y) PRIMITIVE_CAT(x, y)

/* This removes the last argument and returns the rest */
#define EXCEPT_LAST(...) CAT(EXCEPT_LAST_, NARGS(__VA_ARGS__))(__VA_ARGS__)

/* This remove the last two arguments and returns the rest  */
#define EXCEPT_LAST2(...) CAT(EXCEPT_LAST2_, NARGS(__VA_ARGS__))(__VA_ARGS__)

/* This returns the last argument */
#define LAST(...) CAT(LAST_, NARGS(__VA_ARGS__))(__VA_ARGS__)

/* This returns the last but one argument */
#define LAST_BUT1(...) CAT(LAST_BUT1_, NARGS(__VA_ARGS__))(__VA_ARGS__)

";

print '#define NARGS_SEQ(';
for(my $i=1; $i<=$maxi; $i++)
{
    print '_'.$i.', ';
}
print "N, ...) N\n";

print '#define NARGS(...) NARGS_SEQ(__VA_ARGS__';
for(my $i=$maxi; $i>=1; $i--)
{
    print ', '.$i
}
print ")\n";



for(my $i=1; $i<=$maxi; $i++)
{
    print '#define EXCEPT_LAST_'.$i.'(';
    foreach my $j (1..$i)
    {
        printf "x%d%s",
            $j,
            $j == $i ? '' : ','
    }
    print ') ';
    if($i>1)
    {
        foreach my $j (1..$i-1)
        {
            printf "x%d%s",
                $j,
                $j == $i-1 ? '' : ','
        }
    }
    print "\n";
}
foreach my $k (1)
{
    for(my $i=1; $i<=$maxi; $i++)
    {
        print '#define EXCEPT_LAST'.($k+1).'_'.$i.'(';
        foreach my $j (1..$i)
        {
            printf "x%d%s",
                $j,
                $j == $i ? '' : ','
        }
        print ') ';
        if($i>1)
        {
            foreach my $j (1..$i-1-$k)
            {
                printf "x%d%s",
                    $j,
                    $j == $i-1-$k ? '' : ','
            }
        }
        print "\n";
    }
}

for(my $i=1;$i<=$maxi;$i++)
{
    print '#define LAST_'.$i.'(';
     foreach my $j (1..$i)
    {
        printf "x%d%s",
            $j,
            $j == $i ? '' : ','
    }
    print ') x'.$i."\n";
}
foreach my $k (1)
{
    for(my $i=1;$i<=$maxi;$i++)
    {
        print '#define LAST_BUT'.$k.'_'.$i.'(';
        foreach my $j (1..$i)
        {
            printf "x%d%s",
                $j,
                $j == $i ? '' : ','
        }
        print ') x'.($i-1)."\n";
    }
}
