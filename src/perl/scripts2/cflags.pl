#!/usr/bin/env perl
use strict;

my $gcc = $ENV{'CC'} // 'gcc';


my @cpus = split(/,?\s+/,(`$gcc --target-help`=~/generate code for CPU and EXTENSION, CPU is one of\:(.*)EXTENSION is/gs)[0]);
my %flags;
foreach my $cpu (@cpus,'native')
{
    my $cmd="gcc -march=$cpu -dM -E - < /dev/null 2>/dev/null | egrep \"SSE|AVX\" | sort";
    my $r = `$cmd 2>\&1`;
    while($r=~/\#define (\S+) 1/g)
    {
        $flags{$1}=1;
    }
}

foreach my $flag (sort keys %flags)
{
    print "Macrotest($flag);\n";
}
