#!/usr/bin/env perl
use strict;
use 5.16.0;
use Fcntl;
use IPC::Run qw(start pump);
use IO::Handle;
use List::Util qw(max min);
use Scalar::Util qw(openhandle);
use Symbol 'gensym';
$|=1;

############################################################
#
# Script to run binary_c in repeat and individual mode
# through tbse, compare the output, and error when they
# are different.
#
############################################################


# number of systems to be run
my $nrepeat = 900000000;

# basic tbse command
my $tbse = "tbse colour_log 0 log_arrows 0";

#my $testargs = "--metallicity 0.00615346 --effective_metallicity 0.0150989 --M_1 0.555659 --M_2 7.16508 --M_3 5.8415 --M_4 17.9217 --vrot1 288.957 --vrot2 155.166 --vrot3 192.378 --vrot4 165.525 --orbital_period 201412 --orbital_period_triple 5157.49 --orbital_period_quadruple 5641.58 --eccentricity 0.41338 --eccentricity_triple 0.418219 --eccentricity_quadruple 0.135081 --max_evolution_time 4121.49 --white_dwarf_radius_model 1 --post_ce_objects_have_envelopes False --multiplicity 1 --WD_accretion_rate_novae_upper_limit_hydrogen_donor 4.93001e-07 --WD_accretion_rate_novae_upper_limit_helium_donor 8.33046e-06 --WD_accretion_rate_novae_upper_limit_other_donor 3.9155e-08 --MINT_metallicity 0.0487324 --nucsyn_metallicity 0.0415736 --random_seed 83625";
my $testargs = "";

# command to launch one system
my $cmd_indiv = "$tbse log_filename /dev/stdout $testargs ";

# command to launch random system
$nrepeat++;
my $cmd_repeat = "env XARGS=xargs $cmd_indiv repeat $nrepeat random_systems 1 pause_after_repeat 1 $testargs";

# convert cmd_repeat to array reference
my $cmd_repeat = [ split (/\s+/,$cmd_repeat) ];

# open 3-way pipe
my $h = setup_handles();

# loop
my $n = 0;
my $args;
my $log_repeat;
while(my $line = <OUT>)
{
    #print "LINE $line\n";
    next if($line=~/Your binary_c process exited without error/ ||
            $line=~/^At/);

    chomp $line;

    if($line=~/^\s*--/)
    {
        # line starting with '--' contains binary_c args
        $args = $line;
    }
    elsif($line eq "Waiting for stdin")
    {
        # done checks : run the random system
        $n++;
        print "Rerun $n : $args\n";

        # run individually
        my $log_indiv = `$cmd_indiv $args`;

        # clean logs
        clean_log(\$log_repeat);
        clean_log(\$log_indiv);

        # if not equal output, do something
        if($log_repeat ne $log_indiv)
        {
            print "Not equal logs\n";
            my @log_repeat = split(/\n/,$log_repeat);
            my @log_indiv = split(/\n/,$log_indiv);
            my $max = ($#log_indiv,$#log_repeat);
            printf "NLog lines: %d %d : max %d\n",
                $#log_repeat,
                $#log_indiv,
                $max;

            print "REPEAT\n";
            print $log_repeat,"\n";
            print "INDIV\n";
            print $log_indiv,"\n";
            <STDIN>;
            for(my $i=0; $i<=$max; $i++)
            {
                if($log_repeat[$i] ne $log_indiv[$i])
                {
                   print "$i\nrepeat: $log_repeat[$i]\nindiv : $log_indiv[$i]\n";
                   exit;
                }
            }
        }
        else
        {
            print "Logs are identical\n";
        }
        print "\n";

        # reset
        $log_repeat = undef;
        $args = undef;
    }
    else
    {
        $log_repeat .= $line."\n";
    }

    # check for error
    flush_err();

    # check file handles are open
    check_handles();
}

flush_all();
finish $h;
print "Finished\n";
exit;

sub clean_log
{
    my ($logdata) = @_;

    # pointers
    $$logdata =~ s/0x[0-9a-f]+/<0xPOINTER>/g;

    # arguments are different : remove parse_arguments debugging
    $$logdata =~ s!.*src/setup/(parse|derived)_arguments(_from_string)?\.c.*\n!!g;

    # repeat vars are different
    $$logdata =~ s!repeat_reset = \d+!<repeat_reset>!g;

    # clear floating point information
    $$logdata =~ s/\)\S+[\.\!]+//g;

    # remove Tick count
    $$logdata =~ s/Tick.*\n//;

    # clean whitespace
    $$logdata =~ s/^\s+//;
    $$logdata =~ s/\s+$//;
}

sub flush_err
{
    # flush ERR and STDERR and, if error is found, exit
    #printf "Check ERR %s\n",eof ERR ? 'eof' : 'data';
    if(! eof ERR)
    {
        print "Flush ERR\n";
        while(my $l = <ERR>)
        {
            print "ERROR: $l\n";
        }
        exit;
    }
    #printf "Check STDERR %s\n",eof STDERR ? 'eof' : 'data';
    if(! eof STDERR)
    {
        print "Flush STDERR\n";
        while(my $l = <STDERR>)
        {
            print "ERROR: $l\n";
        }
        exit;
    }
}

sub flush_out
{
    # flush OUT and STDOUT and, if error is found, exit
    print "Check OUT\n";
    if(! eof OUT)
    {
        print "Flush OUT\n";
        while(my $l = <OUT>)
        {
            print "OUT: $l\n";
        }
        exit;
    }
    print "Check STDOUT\n";
    if(! eof STDOUT)
    {
        print "Flush STDOUT\n";
        while(my $l = <STDOUT>)
        {
            print "OUT: $l\n";
        }
        exit;
    }
}

sub flush_all
{
    flush_err();
    flush_out();
}

sub check_handles
{
    # check filehandles are open, if not this is
    # an error
    if(!openhandle(*IN))
    {
        print "Filehandle IN is closed\n";
        exit;
    }
    if(!openhandle(*OUT))
    {
        print "Filehandle OUT is closed\n";
        flush_err();
        exit;
    }
    if(!openhandle(*ERR))
    {
        print "Filehandle ERR is closed\n";
        exit;
    }
}

sub setup_handles
{
    # set up file handles to flush and non-block
    my $h =
        start $cmd_repeat,
        '<pipe',\*IN,
        '>pipe',\*OUT,
        '2>pipe',\*ERR,
        or die "$?";

    flush IN;
    flush OUT;
    flush ERR;

    # don't let ERR or STDERR block
    my $flags = '';
    fcntl(ERR, F_GETFL, $flags) or die;
    $flags |= O_NONBLOCK;
    fcntl(ERR, F_SETFL, $flags) or die;
    $flags = '';
    fcntl(STDERR, F_GETFL, $flags) or die;
    $flags |= O_NONBLOCK;
    fcntl(STDERR, F_SETFL, $flags) or die;

    # OUT should block
}
