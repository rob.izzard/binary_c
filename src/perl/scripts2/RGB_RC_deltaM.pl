#!/usr/bin/env perl
use strict; # recommended
use 5.16.0; # recommended
use binary_grid2; # required
use binary_grid::C; # backend : C 
use rob_misc qw/ncpus MAX/;
use POSIX qw/log10/;
use Sort::Key qw/nsort/;

############################################################
#
# Example script to demonstrate how to use the
# binary_grid2 module.
#
# For full documentation, please see binary_grid2.pdf
# in the doc/ directory of binary_c
#
############################################################

# number of computational threads to launch, usually one 
# per CPU
my $nthreads = rob_misc::ncpus();

############################################################
# Binary_c should output data that we can understand here. 
# There are two ways to do this:
#
# 1) Put output statements, using Printf, in binary_c's
#    log_every_timestep() function. This requires a rebuild
#    of libbinary_c.so and a reinstall of the binary_grid module
#    every time you change the Printf statement.
#
#
# 2) Put a list of hashes in the C_auto_logging grid option
#
#  $population->set(
#    C_auto_logging => {
#        'MY_STELLAR_DATA' =>
#            [
#             'model.time',
#             'star[0].mass',
#             'model.probability',
#             'model.dt'
#            ]
#    });
#
#  where MY_STELLAR_DATA is the key of the hash {...} and is also 
#  the header matched in the parse_data function (below). The list [...] 
#  contains the names of the variables to be output, which are all
#  assumed to be in stardata.
#
#  This option does not require a rebuild of libbinary_c.so or a
#  reinstall of binary_grid.
#
#
# 3) Put output statements, using Printf, into the C_logging_code
#    grid option
#
#  $population->set( C_logging_code => ' Printf("...\n"); ');
#
#    You have access to the stardata variable, so you can
#    output everything that is available to log_every_timestep();
#
#  This option does not require a rebuild of libbinary_c.so or a
#  reinstall of binary_grid.
#
############################################################


my $t = ("@ARGV"=~/t=(\S+)/)[0] // 10.0; # time required in Gyr
my $timewindow = [($t-0.5)*1e3,
                  ($t+0.5)*1e3];

# sample $nstars between $mmin and $mmax
my $nstars = 200; # 200
my $mmin = 0.8; # 0.8
my $mmax = 1.2; # 1.2

# wind can be
# 'Reimers' : runs with Reimers' massloss and variable eta
# 'Others' : runs other mass-loss rates
my $wind_prescription = 'Others';

# metallicity
my $Zsolar = 0.0189;
my $population = binary_grid2->new(
    # options can be given now ... 
    max_evolution_time => 15000, # Myr 
    nthreads=>$nthreads, # number of threads
    gb_reimers_eta=>0.2,
    vb=>1, # turn on verbose logging (can be 0,1,2...)
    return_array_refs=>1, # quicker data parsing mode
    log_args=>0,
    sort_args=>0,
    save_args=>1, # required
    full_args=>1, # required
    log_args_dir=>'/tmp',
    C_logging_code => '
    if(stardata->star[0].stellar_type == 3 ||
       stardata->star[0].stellar_type == 4)
    {
             Printf("MY_STELLAR_DATA %g %g %d %g\n",
                    stardata->model.time,
                    stardata->star[0].mass,
                    (int)stardata->star[0].stellar_type,
                    stardata->model.dt
                   );
    }
                   ',
    );

############################################################
# scan command line arguments for extra options
$population->parse_args();     

############################################################
#
# Now we set up the grid of stars.
#
# make a grid of $nstars single stars, log-spaced,
# with masses between $mmin and $mmax
$population->add_grid_variable(
    'name'       => 'lnm1', 
    'longname'   =>'Primary mass', 
    'range'      =>[log($mmin),log($mmax)],
    'spacingfunc'=>"const(log($mmin),log($mmax),$nstars)",
    'precode'    =>'$m1=exp($lnm1);',
    'method'     =>'grid',
    'gridtype'   => 'centred',
    'nstars' => $nstars,
    'noprobdist' => 1,
    );
$population->add_grid_variable(
    'name' => 'FeH',
    'longname' => 'FeH',
    'range' => [-0.3,-0.5,3],
    'nstars' => 3,
    'spacingfunc' => 'const(-0.3,-0.5,3)',
    'gridtype' => 'edge',
    'method' => 'grid',
    'precode' => '$self->set(metallicity=>'.$Zsolar.'*10.0**$FeH);', # set Z here
    'noprobdist' => 1,
    );

if($wind_prescription eq 'Reimers')
{
    # Only use Reimers, but vary eta
    $population->set(
        'gbwind' => 'GB_WIND_REIMERS'
        );
    $population->add_grid_variable(
        'name' => 'reimers_eta',
        'longname' => 'Reimers eta',
        'range' => [0,0.5],
        'nstars' => 1,
        'spacingfunc' => 'const(0,0.4,20)',
        'gridtype' => 'edge',
        'method' => 'grid',
        'precode' => '$self->set(gb_reimers_eta=>$reimers_eta);', # set eta here
        'noprobdist' => 1,
        );
}
else
{
    # Vary mass-loss prescription (non-Reimers)
    #
    # GB_WIND_SCHROEDER_CUNTZ_2005 = 1 : Schroeder and Cuntz 2005 (choose eta=1.0 for their Mdot)
    # GB_WIND_GOLDMAN_ETAL_2017 = 2 : Goldman et al. (2017, see also wind_gas_to_dust_ratio)
    # GB_WIND_BEASOR_ETAL_2020 = 3 : Beasor et al. (2020)
    $population->set(
        'gb_reimers_eta' => 1.0,
        'wind_gas_to_dust_ratio' => 200.0
        );
    $population->add_grid_variable(
        'name' => 'gbwind',
        'longname' => 'GB wind prescription',
        'range' => [1,3],
        'nstars' => 1,
        'spacingfunc' => 'const(1,3,2)',
        'gridtype' => 'edge',
        'method' => 'grid',
        'precode' => '$self->set(gbwind=>$gbwind);', # set eta here
        'noprobdist' => 1,
        );
}

############################################################
# link population to custom data parser function
$population->set(
    parse_bse_function_pointer => \&main::parse_data
    );

# evolution the stellar population (this takes some time)
$population->evolve();


# output the data
output($population);

# done : exit
exit;

############################################################
# subroutines 
############################################################

sub parse_data
{
    my ($population, $results) = @_;
    my $RGB = {
        Mtot => 0.0,
        dttot => 0.0,
    };
    my $done = 0;
    
    my ($reimers_eta,$gbwind,$metallicity) = 
        $population->get_from_arg('gb_reimers_eta',
                                  'gbwind',
                                  'metallicity');
    my $parameter = $wind_prescription eq 'Reimers' ? $reimers_eta : $gbwind;
    my $FeH = log10($metallicity/$Zsolar);
    my $status = 0;

    $results->{deltaMtot}->{$FeH}->{$parameter} //= 0.0;
    $results->{n}->{$FeH}->{$parameter} //= 1;
    
    while(1)
    {
        # subsequent calls to tbse_line contain
        # (references to) arrays of data 
        my $la = $population->tbse_line();
        
        # first element is the "header" line
        my $header = shift @$la;

        # break out of the loop if this is 'fin'
        last if ($header eq 'fin');

        # check if $header matches one of your
        # expected data lines, if so, act
        if($done == 0 && $header eq 'MY_STELLAR_DATA')
        {            
            # matched MY_STELLAR_DATA header
            #
            # get time, mass, probability etc. as specified above
            #
            # (note that $probability is possibly not the same as 
            #  the progenitor's probability!)
            my ($time,$mass,$stellar_type,$dt) = @$la;
            
            if($stellar_type == 3)
            {
                $RGB->{Mtot} += $mass * $dt;
                $RGB->{dttot} += $dt;
                if($status == 0)
                {
                    #printf "M = %g, t = %g at RGB base\n",$mass,$time;
                    $status = 1;
                }
            }
            elsif($stellar_type == 4)
            {
                if($status == 1)
                {
                    #printf "M = %g, t = %g at clump base\n",$mass,$time;
                    $status = 2;
                }
                if(!$done && $time > $timewindow->[0] && $time < $timewindow->[1])
                {
                    # mass is now the "red clump" mass
                    if($RGB->{dttot})
                    {
                        # calculate mean RGB mass
                        my $MRGB = $RGB->{Mtot} / $RGB->{dttot};
                        
                        my $deltaM = $MRGB - $mass;
                        #print "In range : DM = $deltaM\n";
                        
                        # add up the mass distribution
                        $results->{deltaMtot}->{$FeH}->{$parameter} += $deltaM;
                        $results->{n}->{$FeH}->{$parameter}++;
                    }
                }
                # set done flag
                $done = 1;
            }
        }
    }
}
############################################################

sub output
{
    my ($population) = @_;

    # $results is a hash reference containing
    # the results that were added up in parse_data()
    my $deltaMtot = $population->results->{deltaMtot};
    my $n = $population->results->{n};

    if(0)
    {
        # show results hash in full
        print "############################################################\n";
        print Data::Dumper::Dumper($deltaMtot);
        print "############################################################\n";
    }

    # output file
    my $outfile = sprintf (
        '/tmp/%s-%gGyr.dat',
        ($wind_prescription eq 'Reimers' ? 'reimers' : 'others'),
        $t);
    print "Outfile $outfile\n";
    open(my $out,'>',$outfile) ||die;
    foreach my $FeH (nsort keys %$n)
    {
        foreach my $parameter (nsort keys %{$n->{$FeH}})
        {
            my $dm = $deltaMtot->{$FeH}->{$parameter} / MAX(1,$n->{$FeH}->{$parameter}-1);
            printf {$out} "%g %g %g\n",
                $FeH,
                $parameter,
                $dm < 1e-10 ? 0.0 : $dm; 
        }
        print {$out} "\n";
    }
    close $out;
}
