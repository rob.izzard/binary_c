#!/usr/bin/env perl
use strict; # recommended
use 5.16.0; # recommended
use binary_grid2; # required
use binary_grid::C; # backend : C or Perl
use binary_grid::condor; # add condor support
use rob_misc qw/ncpus/;

############################################################
#
# Example script to demonstrate how to use the
# binary_grid2 module.
#
# For full documentation, please see binary_grid2.pdf
# in the doc/ directory of binary_c
#
############################################################

# number of computational threads to launch
my $nthreads = rob_misc::ncpus();

# make a new stellar population
my $population = binary_grid2->new(
    # options can be given now ... 
    metallicity => 0.02, # mass fraction of "metals"
    max_evolution_time => 15000, # Myr 
    nthreads=>$nthreads, # number of threads
    );

# ... or options can be set manually later.
$population->set(
    vb=>1, # turn on verbose logging (can be 0,1,2...)
    return_array_refs=>1, # quicker data parsing mode
    );

# scan command line arguments for extra options
$population->parse_args();     

# duplicity is 0 for single stars, 1 for binary stars
# and 2 for a mixed population sampled at equal times
my $duplicity = 1;

if($duplicity == 0)
{
    # make a grid of $nstars single binary stars, log-spaced,
    # with masses between $mmin and $mmax
    my $nstars = 100;
    my $mmin = 0.1;
    my $mmax = 80.0;
    $population->add_grid_variable(
        'name'       => 'lnm1', 
        'longname'   =>'Primary mass', 
        'range'      =>[log($mmin),log($mmax)],
        'resolution' => $nstars, # just a counter for the grid
        'spacingfunc'=>"const(log($mmin),log($mmax),$nstars)",
        'precode'    =>'$m1=exp($lnm1);',
        'probdist'   =>'Kroupa2001($m1)*$m1',
        'dphasevol'  =>'$dlnm1',
        );
}
elsif($duplicity == 1)
{
    # make a population of binary stars
    my $resolution = {
        m1 => 10,
        q => 10,
        P => 10
    };
    my $mmin = 0.1;
    my $mmax = 80.0;
    $population->{_grid_options}{binary} = 1;
    
    $population->add_grid_variable
        (
        'name'       => 'lnm1', 
        'longname'   =>'Primary mass', 
        'range'      =>[log($mmin),log($mmax)],
        'resolution' => $resolution->{m1},
        'spacingfunc'=>"const(log($mmin),log($mmax),$resolution->{m1})",
        'precode'    =>'$m1=exp($lnm1);',
        'probdist'   =>'Kroupa2001($m1)*$m1',
        'dphasevol'  =>'$dlnm1',
        );
    # q=M1/M2 distribution flat in q between 0.1/M1 and 1.0
    $population->add_grid_variable
        (
        'condition'  =>'$self->{_grid_options}{binary}==1',
        'name'       =>'q',
        'longname'   =>'Mass ratio',
        'range'      =>['0.1/$m1',1.0],
         'resolution'=>$resolution->{q},
        'spacingfunc'=>"const(0.1/\$m1,1.0,$resolution->{q})",
        'probdist'   =>"flatsections\(\$q,\[
\{min=>0.1/\$m1,max=>0.8,height=>1.0\},
\{min=>0.8,max=>1.0,height=>1.0\},
\]\)",
        precode     =>'$m2=$q*$m1;',
        dphasevol   =>'$dq',
        );
     # orbital period Duquennoy and Mayor 1991 distribution
     $population->add_grid_variable
         (
          'name'       =>'logper',
          'longname'   =>'log(Orbital_Period)',
          'range'      =>[-2.0,12.0],
          'resolution' =>$resolution->{P},
          'spacingfunc'=>"const(-1.0,10.0,$resolution->{P})",
          'precode'=>"\$per = 10.0 ** \$logper;
my \$eccentricity = 0.0;
\$sep=calc_sep_from_period(\$m1,\$m2,\$per) if(defined \$m1 && defined \$m2);
",
          'probdist'=>"gaussian(\$logper,4.8,2.3,-2.0,12.0)",
          'dphasevol'=>'$dln10per'
         );
}
else
{
    # Make a mixed population of single and binary stars
    # with equal sampling in equal times.
    my $dt = 100.0;
    $population->set(
        time_adaptive_mass_grid_step => $dt,
        );
    my $sampling_factor = 0.5; # deliberate oversampling (<0.5)
    my $time_adaptive_options = {
        max_evolution_time =>
            $population->{_bse_options}{max_evolution_time},
        stellar_lifetime_table_nm=>1000,
        time_adaptive_mass_grid_log10_time=>0,
        time_adaptive_mass_grid_step=>$dt,
        time_adaptive_mass_grid_nlow_mass_stars=>10,
        nthreads           =>$nthreads,
        thread_sleep       =>1,
        mmin               =>0.1,
        mmax               =>80.0,
        mass_grid_log10_time=>0,
        mass_grid_step      =>$dt*$sampling_factor,
        extra_flash_resolution=>0, # 1 = broken?
        mass_grid_nlow_mass_stars=>10,
        debugging_output_directory=>'/tmp',
        max_delta_m         =>1.0,
        savegrid            =>1,
        vb                  =>0,
        metallicity=>$population->{_bse_options}{metallicity},
        agbzoom             =>0,
    };
    distribution_functions::bastard_distribution(
        $population, {
            mmin         =>0.1,
            mmax         =>80.0,
            m2min        =>0.1,
            nm2          =>10,
            nper         =>10,
            qmin         =>0.0,
            qmax         =>1.0,
            necc         =>undef,
            useecc       =>undef,
            agbzoom      =>0,
            time_adaptive=>$time_adaptive_options,
        });
}

# link population to custom data parser function
$population->set(
    parse_bse_function_pointer => \&main::parse_data
    );

# set up condor
$population->set(
    condor=>1, # use condor
    condor_njobs=>4, # run 4 jobs
    condor_dir=>'/home/rgi/data/condor', # working directory for scripts etc.
    condor_streams=>0, # if 1 stream to stdout/stderr (warning: lots of network!)
    condor_memory=>1024, # RAM (MB) per job
    condor_universe=>'vanilla', # always vanilla
    condor_postpone_join=>1, # if 1 do not join on condor, join elsewhere
    condor_join_machine=>undef,
    condor_join_pwd=>undef,

    # resubmission options
    condor_resubmit_submitted=>0,
    condor_resubmit_running=>0,
    condor_resubmit_crashed=>0,
    condor_resubmit_finished=>0,
    );

# evolution the stellar population (this takes some time)
$population->evolve();

# output the data
output($population);

# done : exit
exit;

############################################################
# subroutines 
############################################################

sub parse_data
{
    my ($population, $results) = @_;

    while(1)
    {
        # subsequent calls to tbse_line contain
        # (references to) arrays of data 
        my $la = $population->tbse_line();
        
        # first element is the "header" line
        my $header = shift @$la;

        # break out of the look if this is 'fin'
        last if ($header eq 'fin');

        # check if $header matches one of your
        # expected data lines, if so, act
        if($header eq 'MY_STELLAR_DATA')
        {
            # matched MY_STELLAR_DATA header
            #
            # ... so do stuff with $la.You have to
            # enable a line in binary_c which outputs
            # the mass, the probability and the timestep
            # (see log_every_timestep.c for examples)
            # and starts with MY_STELLAR_DATA.
            #
            my $mass = $la->[0];
            my $probability = $la->[1];
            my $timestep = $la->[2];

            # bin mass to nearest 1.0 Msun
            $mass = binary_grid2::rebin($mass, 1.0);

            # add up the mass distribution in a histogram
            $results->{mass_distribution}->{$mass} += $probability * $timestep; 
        }
    }
}

############################################################

sub output
{
    my $population = shift;

    # $results is a hash reference containing
    # the results that were added up in parse_data()
    my $results = $population->results;

    # output the mass distribution
    foreach my $mass (sort {$a<=>$b} keys %{$results->{mass_distribution}})
    {
        printf "%g %g\n",$mass,$results->{mass_distribution}->{$mass};
    }
}
