#!/usr/bin/env perl
use strict; # recommended
use 5.16.0; # recommended
use binary_grid2; # required
use binary_grid::C; # backend : C or Perl
use rob_misc qw/ncpus/;
use POSIX qw/log10/;
use Sort::Key qw/nsort/;

############################################################
#
# Example script to demonstrate how to use the
# binary_grid2 module.
#
# For full documentation, please see binary_grid2.pdf
# in the doc/ directory of binary_c
#
############################################################

# number of computational threads to launch
my $nthreads = rob_misc::ncpus();

my $Zrange = [1e-4,0.03];
my $nZ = 20;
my $nM = 2000;
my $outfile = '/tmp/fabian_table.dat';
open(OUT,'>',$outfile)||die("cannot open output\n");

for(my $logZ = log10($Zrange->[0]); 
    $logZ <= log10($Zrange->[1]*(1.0+1e-3));
    $logZ += (log10($Zrange->[1]) - log10($Zrange->[0]))/(1.0*$nZ))
{
    my $Z = 10.0 ** $logZ;

    # make a new stellar population
    my $population = binary_grid2->new(
        # options can be given now ... 
        metallicity => $Z, # mass fraction of "metals"
        max_evolution_time => 14000, # Myr 
        nthreads=>$nthreads, # number of threads
        
        );

    # ... or options can be set manually later.
    $population->set(
        vb=>1, # turn on verbose logging (can be 0,1,2...)
        return_array_refs=>1, # quicker data parsing mode
        log_args=>0,
        sort_args=>0,
        save_args=>0,
        log_args_dir=>'/tmp',
        );

    $population->set(
        # physics
        BH_prescription => 2, # 2 = Spera, 1 = Belczynski
        );
    
    # scan command line arguments for extra options
    $population->parse_args();     

    # save version and info strings
    state $vdone = 0;
    if($vdone == 0)
    {
        $vdone = 1;
        my $version_string = $population->evcode_version_string();
        print OUT "
############################################################
# Version information
############################################################
";
        $version_string=~s/^/\#/g;
        $version_string=~s/\n/\n#/g;
        print OUT $version_string;
    }

    print OUT "
############################################################
# Population information
############################################################
";
    my $info_string = $population->infostring();
    $info_string=~s/^/\#/g;
    $info_string=~s/\n/\n#/g;
    print OUT $info_string;
    print OUT "
############################################################
";
    # make a grid of $nstars single binary stars, log-spaced,
    # with masses between $mmin and $mmax
    my $nstars = $nM;
    my $mmin = 0.1;
    my $mmax = 100.0;
    $population->add_grid_variable(
        'name'       => 'lnm1', 
        'longname'   =>'Primary mass', 
        'range'      =>[log($mmin),log($mmax)],
        'resolution' => $nstars, # just a counter for the grid
        'spacingfunc'=>"const(log($mmin),log($mmax),$nstars)",
        'precode'    =>'$m1=exp($lnm1);',
        'probdist'   =>'Kroupa2001($m1)*$m1',
        'dphasevol'  =>'$dlnm1',
        );
    

    # link population to custom data parser function
    $population->set(
        parse_bse_function_pointer => \&main::parse_data
        );

    # evolution the stellar population (this takes some time)
    $population->evolve();

    # output the data
    output($population);
}

# done : exit
exit;

############################################################
# subroutines 
############################################################

sub parse_data
{
    my ($population, $results) = @_;
    
    my $Z = $population->{_bse_options}->{metallicity};
    my $M = $population->{_grid_options}{progenitor_hash}{m};
    $results->{$Z}//={};
    
    while(1)
    {
        # subsequent calls to tbse_line contain
        # (references to) arrays of data 
        my $la = $population->tbse_line();
        
        # first element is the "header" line
        my $header = shift @$la;

        # break out of the look if this is 'fin'
        last if ($header eq 'fin');

        # check if $header matches one of your
        # expected data lines, if so, act
        if($header eq 'FABIAN_COSMOLOGY')
        {
            # matched MY_STELLAR_DATA header
            #
            # ... so do stuff with $la.You have to
            # enable a line in binary_c which outputs
            # the mass, the probability and the timestep
            # (see log_every_timestep.c for examples)
            # and starts with MY_STELLAR_DATA.
            #
            $results->{$Z}->{$M} = [@$la];
        }
    }
}

############################################################

sub output
{
    my $population = shift;

    # $results is a hash reference containing
    # the results that were added up in parse_data()
    my $results = $population->results;

    # output the mass distribution

    print OUT "# Z Minit McHe_final McCO_final Mcnuc_final M_final t_burn stellar_type\n";
    foreach my $Z (nsort grep {$_ != 'thread_number'} keys %{$results})
    {
        foreach my $M (nsort keys %{$results->{$Z}})
        {
            print OUT join(' ',@{$results->{$Z}->{$M}},"\n");
        }
    }
}
