#!/usr/bin/env perl

use strict; # recommended
use 5.16.0; # recommended
$|=1;
use IO::Handle;


############################################################
#
# Circumbinary discs with binary_grid2
#
# This version models discs around massive stars
#
############################################################


# before loading threads, open output file(s)
my $outdir = ("@ARGV"=~/outdir=(\S+)/)[0];
if(!$outdir)
{
    print STDERR "No outdir given : it is required\n"; 
    exit;
}
print "Output to $outdir\n";
mkdirhier($outdir);

# grid resolution (when used)
my $n = ("@ARGV"=~/\bn=(\d+)/)[0] // 40;

my $initparamspace_outfile = $outdir.'/paramspace.dat';
open(my $initparamspace_fp,'>',$initparamspace_outfile)||
    die("cannot open $initparamspace_outfile for writing");
binmode $initparamspace_fp;
$initparamspace_fp->autoflush;


# load binary grid and rob's modules
use binary_grid2; # required
use binary_grid::C; # backend : C or Perl
use rob_misc qw/ncpus log10 mkdirhier MAX MIN/;

# make lock var for atomic output 
use threads::shared;
my $lockvar : shared;

# number of computational threads to launch
my $nthreads = rob_misc::ncpus();

# make a new stellar population
my $population = binary_grid2->new(
    # options can be given now ... 
    metallicity => 0.02, # mass fraction of "metals"
    max_evolution_time => 15000, # Myr 
    nthreads=>$nthreads, # number of threads
    );

# ... or options can be set manually later.
$population->set(
    vb=>1, # turn on verbose logging (can be 0,1,2...)
    return_array_refs=>1, # quicker data parsing mode
    log_args=>0,
    sort_args=>0,
    save_args=>0,
    log_args_dir=>'/tmp',
    );


$population->set(
    {
        default_to => 'bse'
    },

    # comenv options
    minimum_donor_menv_for_comenv => 0.1,
    alpha_ce=>1.0,
    lambda_ce=>-1.0,
    lambda_ionisation=>0.1,
    lambda_enthalpy=>0.0,

    # AGB Mc,R,L evolution algorithm
    # 0 : compatibility mode : Hurley (if no NUCSYN), Karakas (if NUCSYN)
    # 1 : Hurley 2002 (overshooting)
    # 2 : Karakas 2002 (non-overshooting) 
    #
    # Third dredge up has the extra choice:
    # 3 : Stancliffe (LMC/SMC metallicity only)
    AGB_core_algorithm=>2,
    AGB_radius_algorithm=>2,
    AGB_luminosity_algorithm=>2,
    AGB_3dup_algorithm=>2,

    # CB disc options
    comenv_disc_mass_fraction => 1e-1,
    comenv_disc_angmom_fraction => -3,
    wind_disc_mass_fraction => 0.0,
    wind_disc_angmom_fraction => 0.0,
    cbdisc_gamma => 1.6666666666,
    cbdisc_alpha => 1e-3,
    cbdisc_kappa => 1e-2,
    cbdisc_torquef => 1e-4,
    cbdisc_mass_loss_constant_rate => 0.0,
    cbdisc_mass_loss_inner_viscous_accretion_method => 1,
    cbdisc_mass_loss_inner_viscous_multiplier => 1.0,
    cbdisc_mass_loss_inner_L2_cross_multiplier => 0.0,
    cbdisc_mass_loss_ISM_ram_pressure_multiplier => 1.0,
    cbdisc_mass_loss_ISM_pressure => 3000.0,
    cbdisc_mass_loss_FUV_multiplier => 0.0,
    cbdisc_mass_loss_Xray_multiplier => 1.0,
    cbdisc_viscous_photoevaporation_coupling => 1,
    cbdisc_minimum_luminosity => 1e-4,
    cbdisc_minimum_mass => 1e-6,
    cbdisc_minimum_fRing => 0.2,
    cbdisc_eccentricity_pumping_method => 1,
    cbdisc_resonance_multiplier => 1.0,
    cbdisc_resonance_damping => 1,
    comenv_post_eccentricity => 1e-5,
    minimum_timestep=>1e-8
    );

$population->set(
	# slurm
	slurm=>0, # use slurm if 1 (please set on command line)
	slurm_njobs=>40, # number of jobs
	slurm_dir=>'/users/ri0005/data/slurm', # working directory for scripts etc.
	slurm_memory=>512, # RAM (MB) per job
	slurm_postpone_join=>0, # if 1 do not join on slurm, join elsewhere
	slurm_partition=>'all', # MUST be defined
	slurm_jobname=>'binary_cbd', # not required but useful
	);

# scan command line arguments for extra options
$population->parse_args();  

# duplicity is 0 for single stars, 1 for binary stars
# and 2 for a mixed population sampled at equal times
my $duplicity = 1;

if($duplicity == 0)
{
    # make a grid of $n single stars, log-spaced,
    # with masses between $mmin and $mmax
    my $mmin = 0.1;
    my $mmax = 80.0;
    $population->add_grid_variable(
        'name'       => 'lnm1', 
        'longname'   =>'Primary mass', 
        'range'      =>[log($mmin),log($mmax)],
        'resolution' => $n, # just a counter for the grid
        'spacingfunc'=>"const(log($mmin),log($mmax),$n)",
        'precode'    =>'$m1=exp($lnm1);',
        'probdist'   =>'Kroupa2001($m1)*$m1',
        'dphasevol'  =>'$dlnm1',
        );
}
elsif($duplicity == 1)
{
    # make a population of binary stars
    my $resolution = {
        m1 => $n,
        q => $n,
        P => $n
    };
    my $mmin = 8;
    my $mmax = 20;
    $population->{_grid_options}{binary} = 1;
    
    $population->add_grid_variable
        (
        'name'       => 'lnm1', 
        'longname'   =>'Primary mass', 
        'range'      =>[log($mmin),log($mmax)],
        'resolution' => $resolution->{m1},
        'spacingfunc'=>"const(log($mmin),log($mmax),$resolution->{m1})",
        'precode'    =>'$m1=exp($lnm1);',
        'probdist'   =>'Kroupa2001($m1)*$m1',
        'dphasevol'  =>'$dlnm1',
        );
    # q=M1/M2 distribution flat in q between 0.1/M1 and 1.0
    $population->add_grid_variable
        (
        'condition'  =>'$self->{_grid_options}{binary}==1',
        'name'       =>'q',
        'longname'   =>'Mass ratio',
        'range'      =>['0.1/$m1',1.0],
         'resolution'=>$resolution->{q},
        'spacingfunc'=>"const(0.1/\$m1,1.0,$resolution->{q})",
        'probdist'   =>"flatsections\(\$q,\[
\{min=>0.1/\$m1,max=>0.8,height=>1.0\},
\{min=>0.8,max=>1.0,height=>1.0\},
\]\)",
        precode     =>'$m2=$q*$m1;',
        dphasevol   =>'$dq',
        );
     # orbital period Duquennoy and Mayor 1991 distribution
     $population->add_grid_variable
         (
          'name'       =>'logper',
          'longname'   =>'log(Orbital_Period)',
          'range'      =>[-2.0,12.0],
          'resolution' =>$resolution->{P},
          'spacingfunc'=>"const(-1.0,10.0,$resolution->{P})",
          'precode'=>"


\$per = 10.0 ** \$logper;
my \$eccentricity = 0.0;
\$sep=calc_sep_from_period(\$m1,\$m2,\$per) if(defined \$m1 && defined \$m2);

",

          'postcode' => "

",

          'probdist'=>"gaussian(\$logper,4.8,2.3,-2.0,12.0)",
          'dphasevol'=>'$dln10per'
         );
}
else
{
    # Make a mixed population of single and binary stars
    # with equal sampling in equal times.
    my $dt = 100.0;
    $population->set(
        time_adaptive_mass_grid_step => $dt,
        );
    my $sampling_factor = 0.5; # deliberate oversampling (<0.5)
    my $time_adaptive_options = {
        max_evolution_time =>
            $population->{_bse_options}{max_evolution_time},
        stellar_lifetime_table_nm=>1000,
        time_adaptive_mass_grid_log10_time=>0,
        time_adaptive_mass_grid_step=>$dt,
        time_adaptive_mass_grid_nlow_mass_stars=>10,
        nthreads           =>$nthreads,
        thread_sleep       =>1,
        mmin               =>0.1,
        mmax               =>80.0,
        mass_grid_log10_time=>0,
        mass_grid_step      =>$dt*$sampling_factor,
        extra_flash_resolution=>0, # 1 = broken?
        mass_grid_nlow_mass_stars=>10,
        debugging_output_directory=>'/tmp',
        max_delta_m         =>1.0,
        savegrid            =>1,
        vb                  =>0,
        metallicity=>$population->{_bse_options}{metallicity},
        agbzoom             =>0,
    };
    distribution_functions::bastard_distribution(
        $population, {
            mmin         =>0.1,
            mmax         =>80.0,
            m2min        =>0.1,
            nm2          =>10,
            nper         =>10,
            qmin         =>0.0,
            qmax         =>1.0,
            necc         =>undef,
            useecc       =>undef,
            agbzoom      =>0,
            time_adaptive=>$time_adaptive_options,
        });
}

# link population to custom data parser function
$population->set(
    parse_bse_function_pointer => \&main::parse_data
    );

my %init = $population->initial_abundance_hash(
    'Karakas2002',
    $population->{_bse_options}{metallicity});
my %isotope_hash = $population->isotope_hash();
my @isotope_list = $population->isotope_list();
my %nuclear_mass_hash = $population->nuclear_mass_hash();
my @nuclear_mass_list = $population->nuclear_mass_list();
my @sources = $population->source_list();
my @ensemble = $population->ensemble_list();

# check code features
$population->require_not(
    'DISC_LOG is on',
    'DISC_LOG_2D is on'
    );
$population->require(
    'DISC_DEBUG is 0'
    );

# evolution the stellar population (this takes some time)
$population->evolve();

# output the data
output($population);

# done : exit
exit;

############################################################
# subroutines 
############################################################

sub parse_data
{
    my ($population, $results) = @_;
    
    my $progenitor_stellar_type;
    my $disc_donor_stellar_type;
        
    while(1)
    {
        # subsequent calls to tbse_line contain
        # (references to) arrays of data 
        my $la = $population->tbse_line();

        # first element is the "header" line
        my $header = shift @$la;
        
        # break out of the look if this is 'fin'
        last if ($header eq 'fin');

        # check if $header matches one of your
        # expected data lines, if so, act
        if($header=~/DISC/)
        {
            #print "$header @$la\n", $population->{_grid_options}->{args},"\n";
            
            if($header eq 'DISC_EV')
            {
                my ($dtp,
                    $t,
                    $stellar_type0,
                    $stellar_type1,
                    $nzones,
                    $zone1_type,
                    $zone2_type,
                    $zone3_type,
                    $Rin,
                    $Rout,
                    $separation, # Rsun
                    $orbital_period, # years
                    $Rism,
                    $Xray_domination,
                    $R0,
                    $R1,
                    $RL0,
                    $RL1,
                    $Ldisc,
                    $Idisc,
                    $Jdisc,
                    $Jbinary,
                    $Mdisc,
                    $Mbinary,
                    $Mdot_viscous,
                    $Mdot_global,
                    $Mdot_inner_L2,
                    $Mdot_ISM,
                    $Mdot_FUV,
                    $Mdot_Xray,
                    $eccentricity,
                    $edot_resonance,
                    $Tin,
                    $Tout,
                    $density_in,
                    $density_out,
                    $Pgrav_in,
                    $Pgrav_out,
                    $PISM,
                    $HRin,
                    $HRout,
                    $Teffmax,
                    $Tin_over_Tstar,
                    $Lstar,
                    $LXstar,
                    $Ldisc_over_Lstar,
                    $tvisc_in,
                    $tvisc_out,
                    $PE,
                    $KE,
                    $sigma0,
                    $Tvisc0) = @$la;
                                
                my $r;
                foreach my $set ('all',
                                 $progenitor_stellar_type)
                {
                    $r = $results->{$set};

                    # prebin some quantities
                    
                    # we want orbital period in days
                    my $orbital_period_binned = $population->rebin(safelog10($orbital_period*365.25),0.1);
                    my $eccentricity_binned = $population->rebin($eccentricity,0.025);
                    my $logLstar_binned = $population->rebin(safelog10($Lstar),0.1);

                    # 1D distributions
                    $r->{Mdot_viscous}->{$population->rebin(safelog10($Mdot_viscous),0.1)} += $dtp;
                    $r->{Mdot_global}->{$population->rebin(safelog10($Mdot_global),0.1)} += $dtp;
                    $r->{Mdot_inner_L2}->{$population->rebin(safelog10($Mdot_inner_L2),0.1)} += $dtp;
                    $r->{Mdot_ISM}->{$population->rebin(safelog10($Mdot_ISM),0.1)} += $dtp;
                    $r->{Mdot_FUV}->{$population->rebin(safelog10($Mdot_FUV),0.1)} += $dtp;
                    $r->{Mdot_Xray}->{$population->rebin(safelog10($Mdot_Xray),0.1)} += $dtp;
                    $r->{logMdisc}->{$population->rebin(safelog10($Mdisc),0.1)} += $dtp;
                    $r->{logMbinary}->{$population->rebin(safelog10($Mbinary),0.1)} += $dtp;
                    $r->{logLdisc}->{$population->rebin(safelog10($Ldisc),0.1)} += $dtp;
                    $r->{logJdisc}->{$population->rebin(safelog10($Jdisc),0.1)} += $dtp;
                    $r->{logJbinary}->{$population->rebin(safelog10($Jbinary),0.1)} += $dtp;
                    $r->{age}->{$population->rebin(safelog10($t),0.1)} += $dtp;
                    $r->{separation}->{$population->rebin(safelog10($separation),0.1)} += $dtp;
                    $r->{orbital_period}->{$orbital_period_binned} += $dtp;
                    $r->{eccentricity}->{$eccentricity_binned} += $dtp;
                    $r->{edot_resonance}->{$population->rebin($edot_resonance,0.1)} += $dtp;
                    $r->{stellar_type0}->{$stellar_type0} += $dtp;
                    $r->{stellar_type1}->{$stellar_type1} += $dtp;
                    $r->{logRin}->{$population->rebin(safelog10($Rin),0.1)} += $dtp;
                    $r->{logRout}->{$population->rebin(safelog10($Rout),0.1)} += $dtp;
                    $r->{logRout_over_Rin}->{$population->rebin(safelog10($Rout/MAX(1e-50,$Rin)),0.1)} += $dtp;
                    $r->{logRin_over_separation}->{$population->rebin(safelog10($Rin/MAX(1e-50,$separation)),0.1)} += $dtp;
                    $r->{logRout_over_separation}->{$population->rebin(safelog10($Rout/MAX(1e-50,$separation)),0.1)} += $dtp;
                    $r->{logHRin}->{$population->rebin(safelog10($HRin),0.1)} += $dtp;
                    $r->{logHRout}->{$population->rebin(safelog10($HRout),0.1)} += $dtp;
                    $r->{logLdisc}->{$population->rebin(safelog10($Ldisc),0.1)} += $dtp;
                    $r->{logLstar}->{$logLstar_binned} += $dtp;
                    $r->{logLXstar}->{$population->rebin(safelog10($LXstar),0.1)} += $dtp;
                    $r->{logdensityin}->{$population->rebin(safelog10($density_in),0.1)} += $dtp;
                    $r->{logdensityout}->{$population->rebin(safelog10($density_out),0.1)} += $dtp;
                    $r->{logTin}->{$population->rebin(safelog10($Tin),0.1)} += $dtp;
                    $r->{logTout}->{$population->rebin(safelog10($Tout),0.1)} += $dtp;
                    $r->{logPgrav_in}->{$population->rebin(safelog10($Pgrav_in),0.1)} += $dtp;
                    $r->{logPgrav_out}->{$population->rebin(safelog10($Pgrav_out),0.1)} += $dtp;
                    $r->{logTeffmax}->{$population->rebin(safelog10($Teffmax),0.1)} += $dtp;
                    $r->{logtvisc_in}->{$population->rebin(safelog10($tvisc_in),0.1)} += $dtp;
                
                    # 2D plots
                    $r->{'2D_period_eccentricity'}->{$orbital_period_binned}->{$eccentricity_binned} += $dtp;
                    $r->{'2D_period_luminosity'}->{$orbital_period_binned}->{$logLstar_binned} += $dtp;

                    #print "2D $orbital_period_binned $eccentricity_binned $dtp\n";
                }
            }
            elsif($header eq 'DISC_END')
            {
                my ($endtime,
                    $p,
                    $lifetime,
                    $final_eccentricity,
                    $final_stellar_type0,
                    $final_stellar_type1,
                    $init_M0,
                    $init_M1,
                    $init_period,
                    $init_a,
                    $init_eccentricity
                    ) = @$la;
                foreach my $set ('all',
                                 $progenitor_stellar_type)
                {
                    my $r = $results->{$set};
                    $r->{logendtime}->{$population->rebin(safelog10($endtime),0.1)} += $p;
                    $r->{loglifetime}->{$population->rebin(safelog10($lifetime),0.1)} += $p;
                    $r->{final_eccentricity}->{$population->rebin($final_eccentricity,0.025)} += $p;
                }

                {
                    # output init param space
                    lock $lockvar;

                    my $s = sprintf "%g %g %g %g %d %g %g %g\n",
                        $init_M0,
                        $init_M1,
                        $init_a,
                        $init_period,
                        $disc_donor_stellar_type,
                        $lifetime,
                        $final_eccentricity,
                        $p;

                    
                    print {$initparamspace_fp} $s;
                    #print $s;
                }
            }
            elsif($header eq 'DISC_START')
            {
                my ($starttime,
                    $p,
                    $initial_stellar_type0,
                    $initial_stellar_type1,
                    $overflower, # the star number that gave the comenv
                    $donor_stellar_type,
                    $mdisc_msun,
                    $jdisc,
                    $init_M0,
                    $init_M1,
                    $init_P,
                    $init_a,
                    $init_e
                    ) = @$la;

               foreach my $set ('all',
                                 $progenitor_stellar_type)
               {
                   my $r = $results->{$set};
                   $r->{logstarttime}->{$population->rebin(safelog10($starttime),0.1)} += $p;
                   $r->{initial_stellar_type0}->{$initial_stellar_type0} += $p;
                   $r->{initial_stellar_type1}->{$initial_stellar_type1} += $p;
                   $r->{overflower_stellar_type}->{$donor_stellar_type} += $p;
               }

                # save for initial parameter space logging
                $disc_donor_stellar_type = $donor_stellar_type; 
                #print "Donor type $disc_donor_stellar_type, prob $p\n";
            }
            elsif($header eq 'DISC_COMENV')
            {
                ($progenitor_stellar_type) = @$la;
                foreach my $set ('all',
                                 $progenitor_stellar_type)
                {   
                    $results->{$set} //= {};
                }
            }
        }
        elsif($header eq 'POSTAGB')
        {
            # count the number of post-AGB stars with and without discs
            my ($t,
                $dtp,
                $stellar_type,
                $has_disc,
                $Teff, # K
                $orbital_period, # years
                $eccentricity) = @$la;


            # save results based on whether there
            # is a disc or not
            $has_disc = $has_disc ? 'has_disc' : 'no_disc';
            
            $results->{numbers}->{$stellar_type} //= {};
            $results->{numbers}->{'all'} //= {};


            my $r = $results->{numbers}->{$stellar_type};
            my $rall = $results->{numbers}->{'all'};

            foreach my $x ($r,$rall)
            {
                # save the raw numbers
                $x->{$has_disc}->{number} += $dtp;

                # save the period distribution
                my $orbital_period_binned = $population->rebin(safelog10($orbital_period*365.25),0.1);
                $x->{$has_disc}->{period}->{$orbital_period_binned} += $dtp;

                # save the eccentricity distribution
                my $eccentricity_binned = $population->rebin($eccentricity,0.025);
                $x->{$has_disc}->{eccentricity}->{$eccentricity_binned} += $dtp;
            }
        }
    }
}

############################################################



sub output
{

    my $population = shift;

    # $results is a hash reference containing
    # the results that were added up in parse_data()
    my $results = $population->results;
    printf "OUTPUT : hash size %d\n",rob_misc::hashcount($population->results);

    {
        # Output grid information
        my $outfile = $outdir.'/gridinfo';
        open(my $fp,'>',$outfile);
        print {$fp} $population->infostring();
        close $fp;
    }

    {
        # Output grid information
        my $outfile = $outdir.'/version';
        open(my $fp,'>',$outfile);
        print {$fp} $population->evcode_version_string();
        close $fp;
    }

    {
        # close initial parameter space
        close $initparamspace_fp;
    }


    # all other types of CB discs
    foreach my $progenitor_type (sort grep 
                                 {
                                     /^\d+$/ || $_ eq 'all'
                                 } keys %$results)
    {
        print "Progenitor type $progenitor_type\n";
        
        my $r = $results->{$progenitor_type};
        print "Found results for progenitor type $progenitor_type\n";
        
        foreach my $o (keys %$r)
        {
            my $outfile = $outdir.'/st_'.$progenitor_type.'_'.$o.'.dat';
            print "OUTPUT $progenitor_type,$o to $outfile\n";
            open(my $fp, '>', $outfile);

            my @k = sort {$a<=>$b} keys %{$r->{$o}};

            if($o =~ /^2D/)
            {
                # 2D surface plot
                foreach my $x (@k)
                {
                    foreach my $y (sort {$a<=>$b} keys %{$r->{$o}->{$x}})
                    {
                        printf {$fp} "%g %g %g\n",$x,$y,$r->{$o}->{$x}->{$y};
                    }
                    print {$fp} "\n";
                }
            }
            else
            {
                # 1D distribution
                foreach my $x (@k)
                {
                    printf {$fp} "%g %g\n",$x,$r->{$o}->{$x};
                }
            }
            close $fp;
        }
    }


    # post-AGB distributions
    my $r = $results->{numbers};
    open(my $fp, '>', $outdir.'/postAGB_numbers.dat');

    # fractions of each type with / without discs
    print {$fp} "\# fractions of each type with/without discs\n";
    printf {$fp} "\#% 20s : % 20s : % 20s\n",
    '#Type','% With disc','% Without disc';
    foreach my $stellar_type (sort {$a<=>$b} keys %$r)
    {
        my $norm =  $r->{$stellar_type}->{'has_disc'}->{number} + 
            $r->{$stellar_type}->{'no_disc'}->{number};
        
        $norm = MAX(1e-100,$norm);
        
        printf {$fp} "% 20s : % 20g : % 20g\n",
            ($stellar_type eq 'all' ? $stellar_type : $stellar_type),
            $r->{$stellar_type}->{'has_disc'}->{number} / $norm * 100.0,
            $r->{$stellar_type}->{'no_disc'}->{number} / $norm * 100.0;
    }

    # normalize to all post-TPAGB
    print {$fp} "\n\n\# number of each type normalized to all post-TPAGB\n";
     my $norm =  $r->{6}->{'has_disc'}->{number} + 
        $r->{6}->{'no_disc'}->{number};
    $norm = MAX(1e-100,$norm);
    foreach my $stellar_type (sort {$a<=>$b} keys %$r)
    {
        printf {$fp} "% 20s : % 20g : % 20g\n",
            ($stellar_type eq 'all' ? $stellar_type : $stellar_type),
            $r->{$stellar_type}->{'has_disc'}->{number} / $norm * 100.0,
            $r->{$stellar_type}->{'no_disc'}->{number} / $norm * 100.0;
    }

    print {$fp} "\n\n";
    print {$fp} "\n\n\# number of each type normalized to post-TPAGBs with discs\n";
    my $norm =  $r->{6}->{'has_disc'}->{number};
    $norm = MAX(1e-100,$norm);
    foreach my $stellar_type (sort {$a<=>$b} keys %$r)
    {
        printf {$fp} "% 20s : % 20g : % 20g\n",
            ($stellar_type eq 'all' ? $stellar_type : $stellar_type),
            $r->{$stellar_type}->{'has_disc'}->{number} / $norm * 100.0,
            $r->{$stellar_type}->{'no_disc'}->{number} / $norm * 100.0;
    }
    print {$fp} "\n\n";
    close $fp;

    # period and eccentricity distributions
    foreach my $stellar_type (sort {$a<=>$b} keys %$r)
    {
        foreach my $has_disc ('has_disc',
                              'no_disc')
        {
            foreach my $dist ('period','eccentricity')
            {
                my $h = $r->{$stellar_type}->{$has_disc}->{$dist};
                open(my $fp, '>', $outdir.'/dist_st_'.$stellar_type.'_'.$has_disc.'_'.$dist);
                foreach my $x (sort {$a<=>$b} keys %$h)
                { 
                    printf {$fp} "%g %g\n",$x,$h->{$x};
                }                    
                close $fp;
            }
        }
    }
}

sub safelog10
{
    # safe log10 function (prevents log(0))
    return log10(MAX(1e-30,$_[0]));
}
