#!/usr/bin/env perl
$|=1;
use strict; # recommended
use 5.16.0; # recommended
use IO::Handle;
use threads;
use threads::shared;
my %systems :shared;

############################################################
#
# Circumbinary discs with binary_grid2
#
############################################################


# before loading threads, open output file(s)
my $outdir = ("@ARGV"=~/outdir=(\S+)/)[0];
if(!$outdir)
{
    print STDERR "No outdir given : it is required\n"; 
    exit;
}
print "Output to $outdir\n";
mkdirhier($outdir);
my $log_args_dir = $outdir.'/args';
mkdirhier($log_args_dir);

# grid resolution (when used)
my $n = ("@ARGV"=~/\bn=(\d+)/)[0] // 10;

my $double_end_checks = 0;

# load binary grid and rob's modules
use binary_grid2; # required
use binary_grid::C; # backend : C or Perl
use binary_grid::slurm;
use rob_misc qw/ncpus log10 mkdirhier MAX MIN/;
use Data::Dumper;

# make lock var for atomic output 
use threads::shared;
my $lockvar : shared;

# number of computational threads to launch
my $nthreads = ("@ARGV"=~/nthreads=(\d+)/)[0] // rob_misc::ncpus();

# make a new stellar population
my $population = binary_grid2->new(
    # options can be given now ... 
    metallicity => 0.02, # mass fraction of "metals"
    max_evolution_time => 15000, # Myr 
    nthreads=>$nthreads, # number of threads
    );

# ... or options can be set manually later.
$population->set(
    vb=>1, # turn on verbose logging (can be 0,1,2...)
    return_array_refs=>1, # quicker data parsing mode
    log_args=>0,
    sort_args=>0,
    save_args=>0,
    log_fins=>0,
    log_args_dir=>$log_args_dir,
    );

$population->set(
    {
        default_to => 'bse'
    },

    minimum_timestep=>1e-8,
    
    # comenv options
    minimum_donor_menv_for_comenv => 0.1,
    alpha_ce=>1.0,
    lambda_ce=>-1.0,
    lambda_ionisation=>0.1,
    lambda_enthalpy=>0.0,
    comenv_post_eccentricity => 1e-5,

    # AGB Mc,R,L evolution algorithm
    # 0 : compatibility mode : Hurley (if no NUCSYN), Karakas (if NUCSYN)
    # 1 : Hurley 2002 (overshooting)
    # 2 : Karakas 2002 (non-overshooting) 
    #
    # Third dredge up has the extra choice:
    # 3 : Stancliffe (LMC/SMC metallicity only)
    AGB_core_algorithm=>2,
    AGB_radius_algorithm=>2,
    AGB_luminosity_algorithm=>2,
    AGB_3dup_algorithm=>2,

    # CB disc options
    comenv_disc_mass_fraction => 0.01,
    comenv_disc_angmom_fraction => 0.01,
    wind_disc_mass_fraction => 0.0,
    wind_disc_angmom_fraction => 0.0,
    
    disc_timestep_factor=>0.01,
    disc_log => 0,
    disc_n_monte_carlo_guesses => 0,
    
    cbdisc_gamma => 1.4, #1.6666666666,
    cbdisc_alpha => 1e-3,
    cbdisc_kappa => 1e-2,
    cbdisc_torquef => 1e-4, # 1e-4
    
    cbdisc_init_dM => 0.0, # 0 means do not adapt initial masses 
    cbdisc_init_dJdM => 0.5,

    cbdisc_minimum_luminosity => 1e-4,
    cbdisc_minimum_mass => 1e-6,
    cbdisc_minimum_fRing => 0.2,
    cbdisc_fail_ring_inside_separation=>0,
    cbdisc_max_lifetime => 1e6, # y

    cbdisc_minimum_evaporation_timescale => 1.0, # y
    cbdisc_mass_loss_constant_rate => 0.0,
    
    cbdisc_mass_loss_inner_viscous_multiplier => 1.0,
    cbdisc_mass_loss_inner_viscous_accretion_method => 1,
    cbdisc_mass_loss_inner_L2_cross_multiplier => 1.0,
    cbdisc_viscous_L2_coupling => 1,
    cbdisc_mass_loss_ISM_pressure => 3000.0,
    cbdisc_mass_loss_ISM_ram_pressure_multiplier => 1.0,
    cbdisc_mass_loss_FUV_multiplier => 0.0,
    cbdisc_mass_loss_Xray_multiplier => 1.0,
    cbdisc_viscous_photoevaporation_coupling => 1,
    cbdisc_inner_edge_stripping => 1,
    cbdisc_outer_edge_stripping => 1,
    cbdisc_inner_edge_stripping_timescale => 2, # 0=instant, 1=slow, 
    cbdisc_outer_edge_stripping_timescale => 2, # 2=viscous, 3=orbital
    cbdisc_eccentricity_pumping_method => 1,
    cbdisc_resonance_multiplier => 1.0,
    cbdisc_resonance_damping => 1,
    );

if($ARGV[0] eq 'show_opts')
{
    print "\nBSE_OPTIONS ";
    foreach my $opt (sort keys %{$population->{_bse_options}})
    {
        print '--'.$opt.' '.$population->{_bse_options}->{$opt},' ';
    }
    print "\n";
    exit;
}

$population->set(
    # slurm
    slurm=>0, # use slurm if 1 (please set on command line)
    slurm_njobs=>4, # number of jobs
    slurm_dir=>'/users/ri0005/data/slurm', # working directory for scripts etc.
    slurm_memory=>512, # RAM (MB) per job
    slurm_postpone_join=>0, # if 1 do not join on slurm, join elsewhere
    slurm_partition=>'all', # MUST be defined
    slurm_jobname=>'binary_cbd', # not required but useful
    slurm_use_all_node_CPUs=>1, # required on eureka
    );

# scan command line arguments for extra options
$population->parse_args();  

# detect slurm usage
my $slurm = defined $population->{_grid_options}{slurm_jobid} ? 1 : 0;

# make the parameter space output file(s), returns the
# file pointer which will be undef if no output is to be written
my $initparamspace_fp = make_paramspace_output();

# duplicity is 0 for single stars, 1 for binary stars
# and 2 for a mixed population sampled at equal times
my $duplicity = 1;

if($duplicity == 0)
{
    # make a grid of $n single stars, log-spaced,
    # with masses between $mmin and $mmax
    my $mmin = 0.1;
    my $mmax = 80.0;
    $population->add_grid_variable(
        'name'       => 'lnm1', 
        'longname'   =>'Primary mass', 
        'range'      =>[log($mmin),log($mmax)],
        'resolution' => $n, # just a counter for the grid
        'spacingfunc'=>"const(log($mmin),log($mmax),$n)",
        'precode'    =>'$m1=exp($lnm1);',
        'probdist'   =>'Kroupa2001($m1)*$m1',
        'dphasevol'  =>'$dlnm1',
        );
}
elsif($duplicity == 1)
{
    # make a population of binary stars
    my $resolution = {
        m1 => $n,
        q => $n,
        P => $n
    };
    
    my $mmin = 0.85;
    my $mmax = 6;
    $population->{_grid_options}{binary} = 1;
    
    $population->add_grid_variable
        (
         'name'       => 'lnm1', 
         'longname'   =>'Primary mass', 
         'range'      =>[log($mmin),log($mmax)],
         'resolution' => $resolution->{m1},
         'spacingfunc'=>"const(log($mmin),log($mmax),$resolution->{m1})",
         'precode'    =>'$m1=exp($lnm1);',
         'probdist'   =>'Kroupa2001($m1)*$m1',
         'dphasevol'  =>'$dlnm1',
        );
    # q=M1/M2 distribution flat in q between 0.1/M1 and 1.0
    $population->add_grid_variable
        (
         'condition'  =>'$self->{_grid_options}{binary}==1',
         'name'       =>'q',
         'longname'   =>'Mass ratio',
         'range'      =>['0.1/$m1',1.0],
         'resolution'=>$resolution->{q},
         'spacingfunc'=>"const(0.1/\$m1,1.0,$resolution->{q})",
         'probdist'   =>"flatsections\(\$q,\[
         \{min=>0.1/\$m1,max=>0.8,height=>1.0\},
         \{min=>0.8,max=>1.0,height=>1.0\},
         \]\)",
         precode     =>'$m2=$q*$m1;',
         dphasevol   =>'$dq',
        );
    # orbital period Duquennoy and Mayor 1991 distribution
    $population->add_grid_variable
        (
         'name'       =>'logper',
         'longname'   =>'log(Orbital_Period)',
         #          'range'      =>[-2.0,12.0],
         'range'      =>[1.0,4.0],
         'resolution' =>$resolution->{P},
         'spacingfunc'=>"const(-1.0,10.0,$resolution->{P})",
         'precode'=>"


         \$per = 10.0 ** \$logper;
         my \$eccentricity = 0.0;
         \$sep=calc_sep_from_period(\$m1,\$m2,\$per) if(defined \$m1 && defined \$m2);

         ",

         'postcode' => "

         ",

         'probdist'=>"gaussian(\$logper,4.8,2.3,-2.0,12.0)",
         'dphasevol'=>'$dln10per'
        );
}
else
{
    # Make a mixed population of single and binary stars
    # with equal sampling in equal times.
    my $dt = 100.0;
    $population->set(
        time_adaptive_mass_grid_step => $dt,
        );
    my $sampling_factor = 0.5; # deliberate oversampling (<0.5)
    my $time_adaptive_options = {
        max_evolution_time =>
            $population->{_bse_options}{max_evolution_time},
        stellar_lifetime_table_nm=>1000,
        time_adaptive_mass_grid_log10_time=>0,
        time_adaptive_mass_grid_step=>$dt,
        time_adaptive_mass_grid_nlow_mass_stars=>10,
        nthreads           =>$nthreads,
        thread_sleep       =>1,
        mmin               =>0.1,
        mmax               =>80.0,
        mass_grid_log10_time=>0,
        mass_grid_step      =>$dt*$sampling_factor,
        extra_flash_resolution=>0, # 1 = broken?
        mass_grid_nlow_mass_stars=>10,
        debugging_output_directory=>'/tmp',
        max_delta_m         =>1.0,
        savegrid            =>1,
        vb                  =>0,
        metallicity=>$population->{_bse_options}{metallicity},
        agbzoom             =>0,
    };
    distribution_functions::bastard_distribution(
        $population, {
            mmin         =>0.1,
            mmax         =>80.0,
            m2min        =>0.1,
            nm2          =>10,
            nper         =>10,
            qmin         =>0.0,
            qmax         =>1.0,
            necc         =>undef,
            useecc       =>undef,
            agbzoom      =>0,
            time_adaptive=>$time_adaptive_options,
        });
}

# link population to custom data parser function
$population->set(
    parse_bse_function_pointer => \&main::parse_data
    );

my %init = $population->initial_abundance_hash(
    'Karakas2002',
    $population->{_bse_options}{metallicity});
my %isotope_hash = $population->isotope_hash();
my @isotope_list = $population->isotope_list();
my %nuclear_mass_hash = $population->nuclear_mass_hash();
my @nuclear_mass_list = $population->nuclear_mass_list();
my @sources = $population->source_list();
my @ensemble = $population->ensemble_list();

# check code features
$population->require_not(
    #'DISC_LOG is on', # can be used now, with soft switch off
    'DISC_LOG_2D is on',
    );
$population->require(
    'DISC_DEBUG is 0',
    'BISECT_FAIL_EXIT is off',
    );


# list of variables in each data line, in order
# (note that dtp is the first data item, always)
#
# those that are associated with undef are not plotted
#
# those given an empty hash are plotted with no options
#
# otherwise a hash {...} can contain directives for each plot,
# e.g. whether data is binned, logged, multiplied or prepended
#      with 'log' 
my $vars = [
    age => { 
        bin => 0.1,
        log => 1
    },     
    stellar_type0 => {},
    stellar_type1 => {},
    nzones => undef,
    zone1_type => undef,
    zone2_type => undef,
    zone3_type => undef,
    Rin => {
        bin => 0.025,
        log => 1,
        prepend => 1,
    },
    Rout => {
        bin => 0.025,
        log => 1,
        prepend => 1,
    },
    separation => {
        bin => 0.025,
        log => 1,
    },
    orbital_period => {
        bin => 0.1, # years
        multiplier => 365.25,
        log => 1
    },
    Rism => undef,
    Xray_domination => undef,
    R0 => undef,
    R1 => undef,
    RL0 => undef,
    RL1 => undef,
    Ldisc =>
    {
        bin => 0.025,
        log => 1,
        prepend => 1,
    },
    Idisc => undef,
    Jdisc => {
        bin => 0.1,
        log => 1,
        prepend => 1,
    },
    Jbinary => {
        bin => 0.025,
        log => 1,
        prepend => 1,
    },
    Mdisc => {
        bin => 0.1,
        log => 1,
        prepend => 1,
    },
    Mbinary => {
        bin => 0.025,
        log => 1,
        prepend => 1,
    },
    Mdot_viscous => {
        bin => 0.1,
        log => 1,
    },
    Mdot_global => {
        bin => 0.1,
        log => 1,
    },
    Mdot_inner_L2 => {
        bin => 0.1,
        log => 1,
    },
    Mdot_ISM => {
        bin => 0.1,
        log => 1,
    },
    Mdot_FUV => {
        bin => 0.1,
        log => 1,
    },
    Mdot_Xray => {
        bin => 0.1,
        log => 1,
    },
    eccentricity => {
        bin => 0.025,
    },
    edot_resonance => {
        bin => 0.1,
    },
    Tin => {
        bin => 0.025,
        log => 1,
        prepend => 1,
    },
    Tout => {
        bin => 0.025,
        log => 1,
        prepend => 1,
    },
    densityin => {
        bin => 0.1,
        log => 1,
        prepend => 1,
    },
    densityout => {
        bin => 0.1,
        log => 1,
        prepend => 1,
    },
    Pgrav_in => {
        bin => 0.1,
        log => 1,
        prepend => 1,
    },
    Pgrav_out => {
        bin => 0.1,
        log => 1,
        prepend => 1,
    },
    PISM => undef,
    HRin => {
        bin => 0.025,
        log => 1,
        prepend => 1,
    },
    HRout => {
        bin => 0.025,
        log => 1,
        prepend => 1,
    },
    Teffmax => {
        bin => 0.025,
        log => 1,
        prepend => 1,
    },
    Tin_over_Tstar => {
        bin => 0.1,
    },
    Lstar => {
        bin => 0.025,
        prepend => 1,
        log => 1,
    },
    LXstar => {
        bin => 0.1,
        prepend => 1,
        log => 1,
    },
    Ldisc_over_Lstar => undef,
    tvisc_in => {
        bin => 0.1,
        prepend => 1,
        log => 1,
    },
    tvisc_out => {
        bin => 0.1,
        prepend => 1,
        log => 1,
    },
    PE => undef,
    KE => undef,
    sigma0 => undef,
    Tvisc0 => undef,
    Rout_over_Rin => {
        bin => 0.025,
        log => 1,              
    },
    Rin_over_separation => {
        bin => 0.025,
        log => 1,              
    },
    Rout_over_separation => {
        bin => 0.025,
        log => 1,               
    },
    ];

# 2D plots : binning, logging etc. is as in 1D
my $twoD = {
    'period_eccentricity' => 
    {
        x => 'orbital_period',
        y => 'eccentricity',
    },
        
        'period_luminosity' => 
    {
        x => 'orbital_period',
        y => 'logLstar', 
    },
};


# evolution the stellar population (this takes some time)
$population->evolve();

# output the data
output($population);

# done : exit
exit;

############################################################
# subroutines 
############################################################

sub parse_data
{
    my ($population, $results) = @_;
    
    my $progenitor_stellar_type;
    my $disc_donor_stellar_type;

    

    
    while(1)
    {
        # subsequent calls to tbse_line contain
        # (references to) arrays of data 
        my $la = $population->tbse_line();

        # first element is the "header" line
        my $header = shift @$la;
        
        # break out of the look if this is 'fin'
        last if ($header eq 'fin');

        # check if $header matches one of your
        # expected data lines, if so, act
        if($header=~/DISC/)
        {
            #print "$header\n";# @$la\n", $population->{_grid_options}->{args},"\n";
            if($header eq 'DISC_EV')
            {
                # first is always dtp : remove this
                my $dtp = shift @$la;
                #print "DISC EV : dtp = $dtp\n";
                #print "ARRAY @$la\n";

                # construct the $data hash
                my $data = {};
                my $c = 0;
                while(scalar @$la)
                {
                    my $y = shift @$la;
                    my $pre = '';
                    my $k = $vars->[$c++]; # variable key
                    my $h = $vars->[$c++]; # variable hash
                    
                    if(defined $h)
                    {
                        # extra things to do...

                        # multiplier
                        if(defined $h->{multiplier})
                        {
                            $y *= $h->{multiplier};
                        }
                        
                        # log10
                        if(defined $h->{log} && $h->{log})
                        {
                            $y = safelog10($y);
                            
                            # prepend 'log' to the data key
                            if($h->{prepend})
                            {
                                $pre = 'log';
                            }
                        }

                        # bin data
                        if(defined $h->{bin})
                        {
                            $y = $population->rebin($y,$h->{bin});
                        }
                        
                        # set the data hash : note that these data are unique
                        # there should be no existing data in $data->{$pre.$k}
                        $data->{$pre.$k}->{$y} = $dtp;
                    }
                }
                
                my $r;
                foreach my $set ('all',
                                 $progenitor_stellar_type)
                {
                    $r = $results->{$set};
                    
                    # set the data
                    foreach my $x (keys %$data)
                    {
                        foreach my $y (keys %{$data->{$x}})
                        {
                            $results->{$set}->{$x}->{$y} += $data->{$x}->{$y};
                        }
                    }

                    
                    # 2D plots
                    foreach my $plot (keys %$twoD)
                    {
                        my $x = (keys %{$data->{$twoD->{$plot}->{x}}})[0];
                        my $y = (keys %{$data->{$twoD->{$plot}->{y}}})[0];
                        $r->{'2D_'.$plot}->{$x}->{$y} += $dtp;
                    }
                }
            }
            elsif($header eq 'DISC_END')
            {
                my ($endtime,
                    $p,
                    $lifetime,
                    $final_eccentricity,
                    $final_stellar_type0,
                    $final_stellar_type1,
                    $init_M0,
                    $init_M1,
                    $init_period,
                    $init_a,
                    $init_eccentricity
                    ) = @$la;

                if($double_end_checks)
                {
                    my $x = "$init_M0 $init_M1 $init_period $endtime";
                    if(defined $systems{$x})
                    {
                        print "ERROR : double disc end ($systems{$x})\n$population->{_grid_options}->{args}\n";
                        
                    }
                    $systems{$x}++;
                }

                foreach my $set ('all',
                                 $progenitor_stellar_type)
                {
                    my $r = $results->{$set};
                    $r->{logendtime}->{$population->rebin(safelog10($endtime),0.025)} += $p;
                    $r->{loglifetime}->{$population->rebin(safelog10($lifetime),0.025)} += $p;
                    $r->{final_eccentricity}->{$population->rebin($final_eccentricity,0.025)} += $p;
                }
                
                if(defined $initparamspace_fp)
                    
                {
                    # output init param space
                    lock $lockvar;

                    my $s = sprintf "%g %g %g %g %d %g %g %g\n",
                        $init_M0,
                        $init_M1,
                        $init_a,
                        $init_period,
                        $disc_donor_stellar_type,
                        $lifetime,
                        $final_eccentricity,
                        $p;
                    
                    print {$initparamspace_fp} $s;
                }
            }
            elsif($header eq 'DISC_START')
            {
                my ($starttime,
                    $p,
                    $initial_stellar_type0,
                    $initial_stellar_type1,
                    $overflower, # the star number that gave the comenv
                    $donor_stellar_type,
                    $mdisc_msun,
                    $jdisc,
                    $init_M0,
                    $init_M1,
                    $init_P,
                    $init_a,
                    $init_e
                    ) = @$la;
                
                foreach my $set ('all',
                                 $progenitor_stellar_type)
                {
                    my $r = $results->{$set};
                    $r->{logstarttime}->{$population->rebin(safelog10($starttime),0.1)} += $p;
                    $r->{initial_stellar_type0}->{$initial_stellar_type0} += $p;
                    $r->{initial_stellar_type1}->{$initial_stellar_type1} += $p;
                    $r->{overflower_stellar_type}->{$donor_stellar_type} += $p;
                }

                # save for initial parameter space logging
                $disc_donor_stellar_type = $donor_stellar_type; 
            }
            elsif($header eq 'DISC_COMENV')
            {
                ($progenitor_stellar_type) = @$la;
                foreach my $set ('all',
                                 $progenitor_stellar_type)
                {   
                    $results->{$set} //= {};
                }
            }
        }
        elsif($header eq 'POSTAGB')
        {
            # count the number of post-AGB stars with and without discs
            my ($t,
                $dtp,
                $stellar_type,
                $has_disc,
                $Teff, # K
                $orbital_period, # years
                $eccentricity) = @$la;


            # save results based on whether there
            # is a disc or not
            $has_disc = $has_disc ? 'has_disc' : 'no_disc';
            
            $results->{numbers}->{$stellar_type} //= {};
            $results->{numbers}->{'all'} //= {};

            my $r = $results->{numbers}->{$stellar_type};
            my $rall = $results->{numbers}->{'all'};

            foreach my $x ($r,$rall)
            {
                # save the raw numbers
                $x->{$has_disc}->{number} += $dtp;

                # save the period distribution
                my $orbital_period_binned = $population->rebin(safelog10($orbital_period*365.25),0.1);
                $x->{$has_disc}->{period}->{$orbital_period_binned} += $dtp;

                # save the eccentricity distribution
                my $eccentricity_binned = $population->rebin($eccentricity,0.025);
                $x->{$has_disc}->{eccentricity}->{$eccentricity_binned} += $dtp;
            }
        }
    }
}

############################################################



sub output
{
    my $population = shift;
    # $results is a hash reference containing
    # the results that were added up in parse_data()
    my $results = $population->results;
    my $nhash = rob_misc::hashcount($population->results);
    
    if(
        (
         $population->{_grid_options}{slurm} == 0 ||
         $population->{_grid_options}{slurm_command} eq 'join' ||
         !defined $population->{_grid_options}{slurm_command}
        )
        &&
        $nhash)
    {
        printf "OUTPUT_ALL : hash size %d\n",$nhash;


        print "Dump to $outdir/dump\n";
        open(my $fp,'>',$outdir.'/dump');
        print Data::Dumper::Dumper($results);
        close $fp;
        
        {
            # Output grid information
            my $outfile = $outdir.'/gridinfo';
            open(my $fp,'>',$outfile);
            print {$fp} $population->infostring();
            close $fp;
        }

        {
            # Output grid information
            my $outfile = $outdir.'/version';
            open(my $fp,'>',$outfile);
            print {$fp} $population->evcode_version_string();
            close $fp;
        }

        if(defined $initparamspace_fp)
        {
            # close initial parameter space
            close $initparamspace_fp;
        }

        # all other types of CB discs
        foreach my $progenitor_type (
            sort grep 
            {
                /^\d+$/ || $_ eq 'all'
            } keys %$results)
        {
            print "Progenitor type $progenitor_type\n";
            
            my $r = $results->{$progenitor_type};
            print "Found results for progenitor type $progenitor_type\n";
            
            foreach my $o (keys %$r)
            {
                my $outfile = $outdir.'/st_'.$progenitor_type.'_'.$o.'.dat';
                print "OUTPUT $progenitor_type,$o to $outfile\n";
                open(my $fp, '>', $outfile);

                my @k = sort {$a<=>$b} keys %{$r->{$o}};

                if($o =~ /^2D/)
                {
                    # 2D surface plot
                    foreach my $x (@k)
                    {
                        foreach my $y (sort {$a<=>$b} keys %{$r->{$o}->{$x}})
                        {
                            printf {$fp} "%g %g %g\n",$x,$y,$r->{$o}->{$x}->{$y};
                        }
                        print {$fp} "\n";
                    }
                }
                else
                {
                    # 1D distribution
                    foreach my $x (@k)
                    {
                        printf {$fp} "%g %g\n",$x,$r->{$o}->{$x};
                    }
                }
                close $fp;
            }
        }


        # post-AGB distributions
        my $r = $results->{numbers};
        open(my $fp, '>', $outdir.'/postAGB_numbers.dat');

        # fractions of each type with / without discs
        print {$fp} "\# fractions of each type with/without discs\n";
        printf {$fp} "\#% 20s : % 20s : % 20s\n",
            '#Type','% With disc','% Without disc';
        foreach my $stellar_type (sort {$a<=>$b} keys %$r)
        {
            my $norm =  
                $r->{$stellar_type}->{'has_disc'}->{number} + 
                $r->{$stellar_type}->{'no_disc'}->{number};
            
            $norm = MAX(1e-100,$norm);
            
            printf {$fp} "% 20s : % 20g : % 20g\n",
                ($stellar_type eq 'all' ? $stellar_type : $stellar_type),
                $r->{$stellar_type}->{'has_disc'}->{number} / $norm * 100.0,
                $r->{$stellar_type}->{'no_disc'}->{number} / $norm * 100.0;
        }

        # normalize to all post-TPAGB
        print {$fp} "\n\n\# number of each type normalized to all post-TPAGB\n";
        my $norm =  $r->{6}->{'has_disc'}->{number} + 
            $r->{6}->{'no_disc'}->{number};
        $norm = MAX(1e-100,$norm);
        foreach my $stellar_type (sort {$a<=>$b} keys %$r)
        {
            printf {$fp} "% 20s : % 20g : % 20g\n",
                ($stellar_type eq 'all' ? $stellar_type : $stellar_type),
                $r->{$stellar_type}->{'has_disc'}->{number} / $norm * 100.0,
                $r->{$stellar_type}->{'no_disc'}->{number} / $norm * 100.0;
        }

        print {$fp} "\n\n";
        print {$fp} "\n\n\# number of each type normalized to post-TPAGBs with discs\n";
        my $norm =  $r->{6}->{'has_disc'}->{number};
        $norm = MAX(1e-100,$norm);
        foreach my $stellar_type (sort {$a<=>$b} keys %$r)
        {
            printf {$fp} "% 20s : % 20g : % 20g\n",
                ($stellar_type eq 'all' ? $stellar_type : $stellar_type),
                $r->{$stellar_type}->{'has_disc'}->{number} / $norm * 100.0,
                $r->{$stellar_type}->{'no_disc'}->{number} / $norm * 100.0;
        }
        print {$fp} "\n\n";
        close $fp;

        # period and eccentricity distributions
        foreach my $stellar_type (sort {$a<=>$b} keys %$r)
        {
            foreach my $has_disc ('has_disc',
                                  'no_disc')
            {
                foreach my $dist ('period','eccentricity')
                {
                    my $h = $r->{$stellar_type}->{$has_disc}->{$dist};
                    open(my $fp, '>', $outdir.'/dist_st_'.$stellar_type.'_'.$has_disc.'_'.$dist);
                    foreach my $x (sort {$a<=>$b} keys %$h)
                    { 
                        printf {$fp} "%g %g\n",$x,$h->{$x};
                    }                    
                    close $fp;
                }
            }
        }

    }
}

sub safelog10
{
    # safe log10 function (prevents log(0))
    return log10(MAX(1e-30,$_[0]));
}

sub make_paramspace_output
{
    if($population->{_grid_options}{slurm_command} ne 'join' &&
       $population->{_grid_options}{slurm_jobname} ne 'binary_cbd')
    {
        # open output parameter space file 
        # and return the file pointer
        my $initparamspace_outfile = 
            $slurm ? 
            $outdir.'/paramspace/'.$population->{_grid_options}{slurm_jobname}.'.dat' :
            $outdir.'/paramspace.dat';
        mkdir $outdir.'/paramspace' if($slurm);
        
        my $fp;
        if($outdir ne '/dev/null')
        {
            open($fp,'>',$initparamspace_outfile)||
                die("cannot open $initparamspace_outfile for writing");
            binmode $fp;
            $fp->autoflush;
        }
        return $fp;
    }
    else
    {
        return undef;
    } 
}
