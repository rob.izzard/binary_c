#!/usr/bin/env perl
use strict;
use Data::Dumper;
use Sort::Key qw/nsort/;
use rob_misc;

my $args = "@ARGV";
my $force = $args=~/force/ // 0;
my $agb = "KARAKAS"; # HURLEY or KARAKAS
my @pdfs;

# GAIA_CONVERSION_UBVRI_UNIVARIATE_JORDI2010 0
# GAIA_CONVERSION_UBVRI_BIVARIATE_JORDI2010 1
# GAIA_CONVERSION_ugriz_UNIVARIATE_JORDI2010 2
# GAIA_CONVERSION_ugriz_BIVARIATE_JORDI2010 3
# GAIA_CONVERSION_UBVRI_UNIVARIATE_EVANS2018 4
# GAIA_CONVERSION_ugriz_UNIVARIATE_EVANS2018 5
# GAIA_CONVERSION_UBVRI_RIELLO2020 6
# GAIA_CONVERSION_ugriz_RIELLO2020 7

foreach my $gaia_method (
    'GAIA_CONVERSION_UBVRI_UNIVARIATE_JORDI2010',
    'GAIA_CONVERSION_UBVRI_BIVARIATE_JORDI2010',
    'GAIA_CONVERSION_ugriz_UNIVARIATE_JORDI2010',
    'GAIA_CONVERSION_ugriz_BIVARIATE_JORDI2010',
    'GAIA_CONVERSION_UBVRI_UNIVARIATE_EVANS2018',
    'GAIA_CONVERSION_ugriz_UNIVARIATE_EVANS2018',
    'GAIA_CONVERSION_UBVRI_RIELLO2020',
    'GAIA_CONVERSION_ugriz_RIELLO2020',
   )
{
    my $plots={};
    my $labels = {};
    my $outdir = '/tmp/'.$gaia_method;
    mkdirhier($outdir);
    print "Gaia colour transform method $gaia_method\n";
    foreach my $m (0.5,0.7,0.8,1,1.5,2,5,10)
    {
        print "M = $m\n";
        my $file = sprintf "%s/%g.dat", $outdir, $m;
        my $first;
        if(!-f $file || $force)
        {
            my $data;
            my $out = `tbse M_1 $m M_2 0.1 orbital_period 0 separation 1e6  ensemble True ensemble_filters_off True ensemble_filter_HRD True  AGB_core_algorithm AGB_CORE_ALGORITHM_$agb AGB_radius_algorithm AGB_RADIUS_ALGORITHM_$agb AGB_luminosity_algorithm AGB_LUMINOSITY_ALGORITHM_$agb AGB_3dup_algorithm AGB_THIRD_DREDGE_UP_ALGORITHM_$agb gaia_colour_transform_method $gaia_method |grep -v WDCHECK`;
            $out=~s/SINGLE_STAR.*\n//;
            $out=~s/Tick.*\n//;
            $out = '$data = '.$out.';';
            $out =~s/\:/=>/g;
            eval $out;

            my $h = $data->{HRD}->{star}->{0}->{stellar_type};

            open(my $out,'>',$file)||die;
            foreach my $stellar_type (grep
                                      {
                                          $_ < 13
                                      } nsort keys %$h)
            {
                my $h2 = $data->{HRD}->{star}->{0}->{stellar_type}->{$stellar_type}->{'GBP-GRP'};
                foreach my $x (nsort keys %$h2)
                {
                    foreach my $y (nsort keys %{$h2->{$x}->{G}})
                    {
                        printf $out "%g %g %d\n",$x,$y,$stellar_type;
                        $first //= sprintf "%g,%g",$x,$y;
                    }
                }
            }
            close $out;
        }
        else
        {
            $first = `head -1 $file`;
            chomp $first;
            $first=~s/(\S+) (\S+).*/$1,$2/g;
        }
        $labels->{$m} = "set label \"$m\" at $first offset -3,0";
        $plots->{$m} = " \"$file\" u 1:2:3 w p ps 0.5 palette z notitle ";
    }

    my $title = $gaia_method;
    $title =~ s/_/ /g;

    my $pdf = "$outdir/gaiaHRD.pdf";
    open (my $gp,'>','/tmp/gaiaHRD.plt');
    print $gp "
set term pdf
set output \"$pdf\"
set xlabel \"GBP-GRP\"
set ylabel \"G\"
set xrange[-1:5]
set yrange[20:-8]
set title \"$title\"
    ";
    foreach my $m (nsort keys %$plots)
    {
        print $gp $labels->{$m},"\n";
    }

    my $plotcomma = 'plot';
    foreach my $m (nsort keys %$plots)
    {
        print $gp "$plotcomma $plots->{$m} ";
        $plotcomma = ',';
    }

    if(0)
    {
        print $gp ", \"/tmp/output966214316039.dat\" u (\$30-\$31):29 lt -1 notitle ";
    }
    print $gp "\n";
    close $gp;
    `gnuplot /tmp/gaiaHRD.plt`;

    push(@pdfs,$pdf);
}

`pdfjoin @pdfs --outfile /tmp/gaiaHRD.pdf`;

print "See /tmp/gaiaHRD.pdf\n";
