#!/usr/bin/env perl

use 5.16.0;
use feature qw(state);
use strict;

############################################################
#
# make_population_ensemble.pl
#
# A script to make population ensemble data using
# binary_grid2 and binary_c.
#
#
############################################################
# Rob's modules
use binary_grid2;
use binary_grid::C;
#use binary_grid::condor;
use binary_grid::slurm;
use distribution_functions;
use Hash::RobMerge;
use rob_misc qw/dumpfile force_numeric_elements MAX mkdirhier ncpus slurp/;
############################################################
# modules from CPAN (install with cpanm)
use Data::Dumper;
use Carp qw/confess/;
use Cwd;
use File::Path qw/make_path/;
use File::Basename;
use JSON::PP;
use JSON::Parse qw/parse_json json_file_to_perl/;
use POSIX qw/strftime log10/;
use Sort::Key qw/nsort/;
use Sort::Naturally qw/ncmp/;
use Sys::Hostname;
use Term::ANSIColor;
use Fcntl qw( :flock );
############################################################
$|=1; # enable this line for auto-flushed output

############################################################
# Version 2.1.7 for binary_c 2.1.7
############################################################

my $args = "@ARGV"; # used often
my $hostname = hostname();
my $date_today = strftime '%d%m%Y', localtime;

# output directory location
my $voldisk = $ENV{VOLDISK} // $ENV{HOME};
my $outdir = ($args=~/outdir=(\S+)/)[0] //
    $voldisk.'/data/pops/population_ensemble-PN-'.$date_today;
print "Yield vs time : outdir=$outdir : voldisk=$voldisk, outdir=$outdir\n";

use vars qw($dt $logdt $tstart $logtimes $condor_count $distribution $distribution_options %ensemble_opts  $exacttimes $force $gnuplot $gridstring $idt $nbins $nensemble $nisotopes $nisotopes1 $norm_output  $yieldoutfile );

# number of threads : usually this should match the number of CPUs
my $nthreads = ($args=~/nthreads=(\d+)/)[0] // MAX(1,ncpus());

# population object
print "New binary grid\n";
my $population = binary_grid2->new(defaults(),
                                   nthreads=>$nthreads);

print "Using binary_c version ",($population->evcode_version_string()=~/Version (\S+)/)[0],"\n";


# set up the population
local_setup($population);
#print Data::Dumper::Dumper($population->info_hash());
#exit;

# evolution the population
$population->evolve();

# output data
if(!$population->{_grid_options}{condor})
{
    all_output($population)
}

# done
exit(0);

############################################################
############################################################
############################################################

sub all_output
{
    my $population = shift;

    make_path($outdir); # nb we assume this works...

    # get initial abundances
    my %init_abunds = $population->initial_abundance_hash();

    print "\n\nOutputting to $outdir ...\n" if($population->{_grid_options}->{vb});
    printf "Total mass into stars: %g\n",$population->results->{mass_into_stars};

    # output ND JSON data
    output_json($population);

    print color('reset');
}

sub parser
{
    # parser function : takes output from binary_c and
    # converts it to useful statistics stored in $h.
    #
    # args (@_) are :
    # 0 : binary_grid2 population object
    # 1 : $h data hash to be populated
    my ($population,$h) = @_;

    if($population->{_tbse_callmode} ne 'flush persistent')
    {
        # add up mass into stars and notstars (planets/brown dwarfs)
        my $p = $population->{_grid_options}->{progenitor_hash}{prob};

        foreach my $x ('m1','m2')
        {
            my $M = $population->{_grid_options}->{progenitor_hash}{$x};
            $h->{$M > 0.07 ? 'mass_into_stars' : 'mass_into_notstars'} += $M * $p;
            printf "%s %d %s : this %g*%g = %g -> mass into stars %g\n\n",
                $x,
                $population->flexigrid_thread_number,
                $population->{_tbse_callmode},
                $M,
                $p,
                $M*$p,
                $h->{mass_into_stars}
            if(0 && $x eq 'm1');
        }
    }
    my $prevt;
    while(1)
    {
        # tbse_line is setup to get an array ref
        my $linearray = $population->tbse_line();
        #print "BINARY_C LINE @$linearray\n";

        # next action is based on the line's header
        my $header = shift @$linearray;
        last if ($header eq 'fin');

        if($header eq 'ENSEMBLE_JSON')
        {
            #print "GOT JSON $header @$linearray\n";
            $population->add_ensemble($h,$linearray);
            print "Added ensemble : pop=$population, h=$h\n";
        }
        # other output is ignored
    }
}

sub defaults
{
    my $testmachine = $hostname =~ /(capc85|grebe)/ ? 1 : 0;

    # local options
    my @opts = (

        # use the C backend : it's faster
        backend => 'binary_grid::C',

        # no verbose output
        vb => 1,

        # no timeouts, no logging
        timeout => 0,

        # no logging except on the capc85 test machine
        log_filename => $testmachine ?
        '/tmp/c_log_yvt__THREAD__.dat' : '/dev/null',

        # verbose output every 300s or every 1s on capc85
        log_dt_secs => $testmachine ? 1 : 300,

        nmod => 0,

        # use which parser function?
        parse_bse_function_pointer => \&main::parser,
        return_array_refs => 1,

        threads_stack_size => 50, # MBytes

        ############################################################
        # require ensemble data
        ensemble => 1,
        ensemble_defer => 1,

        # filter ensemble data

        # turn off all filters (they are all on by default)
        ensemble_filters_off => 1,

    # initial distribution filters
    ensemble_filter_INITIAL_DISTRIBUTIONS => 1,

        # turn on PN filter
        ensemble_filter_PNE => 1,


        # PN options
        PPN_envelope_mass => 0.01,
        PN_comenv_transition_time => 1e4,
        PN_Hall_fading_time_algorithm => 'PN_HALL_FADING_TIME_ALGORITHM_MINIMUM',

        # turn on gaia CMD and period distributions
        ensemble_filter_GAIA_CMD => 0,
        ensemble_filter_GAIA_CMD_PERIOD_DISTRIBUTIONS => 0,
        ensemble_filter_GAIA_CMD_TIME_SLICES => 0,
        ensemble_filter_GAIA_CMD_PERIOD_DISTRIBUTIONS_TIME_SLICES => 0,

        ############################################################
        # Gaia colour filter
        gaia_colour_transform_method => 'GAIA_CONVERSION_UBVRI_RIELLO2020',

        # tidal information
        ensemble_filter_TIDES => 0,

        ############################################################
        # ZAMS rotation rates (km/s)
        # VROT_NON_ROTATING for a star with no rotation (actually sets to 1e-10)
        # VROT_BSE = 0 = BSE (Hurley et al. 2002) formula for vrot (assuming the star is spherical)
        # VROT_BREAKUP = -1 to start at breakup (which assumes the star is equatorially deformed)
        # VROT_SYNC = -2 to sync the angular velocity with the orbit
        vrot1 => 'VROT_BSE',
        vrot2 => 'VROT_BSE',



        ############################################################
        # Stellar winds
        ################
        #
        # mass loss by stellar winds
        #
        # WIND_ALGORITHM_NONE = 0 : no wind
        # WIND_ALGORITHM_HURLEY2002 = 1 : default Hurley+ 2002 (options are below)
        # WIND_ALGORITHM_SCHNEIDER2018 = 2 : Schneider 2018
        # WIND_ALGORITHM_BINARY_C_2020 = 3 : binary_c 2020 (== Schneider 2018 + Sander and Vink 2020)
        wind_mass_loss => 'WIND_ALGORITHM_BINARY_C_2020',

        # Wind on Giant Branch:
        # GB_WIND_REIMERS = 0 : Reimers (original, choose eta~0.5),
        # GB_WIND_SCHROEDER_CUNTZ_2005 = 1 : Schroeder and Cuntz 2005 (choose eta=1.0 for their Mdot)
        # GB_WIND_GOLDMAN_ETAL_2017 = 2 : Goldman et al. (2017, see also wind_gas_to_dust_ratio)
        # GB_WIND_BEASOR_ETAL_2020 = 3 : Beasor et al. (2020)

        gbwind => 'GB_WIND_REIMERS',
        gb_reimers_eta => 0.5, # Reimers (BSE = Hurley et al. 2002, 0.5)

        # wind on EAGB
        # EAGB_WIND_BSE = 0 = BSE prescription (Hurley et al. 2002 version of Vassiliadis and Wood 1993)
        # EAGB_WIND_GOLDMAN_ETAL_2017 = 1 = Goldman et al. (2017, see also wind_gas_to_dust_ratio)
        # EAGB_WIND_BEASOR_ETAL_2020 = 2 = Beasor et al. (2020)
        eagbwind => 'EAGB_WIND_BSE',

        # Wind on TPAGB
        # TPAGB_WIND_VW93_KARAKAS = 0 = amanda (vw93)
        # TPAGB_WIND_VW93_ORIG = 1 = hurley (vw93)
        # TPAGB_WIND_REIMERS = 2 = Reimers,
        # TPAGB_WIND_BLOECKER = 3 = Bloecker
        # TPAGB_WIND_VAN_LOON = 4 = Van Loon,
        # TPAGB_WIND_ROB_CWIND = 5 = Rob's C-wind (buggy)
        # TPAGB_WIND_VW93_KARAKAS_CARBON_STARS = 6 = VW93 (karakas) when C/O>1
        # TPAGB_WIND_VW93_ORIG_CARBON_STARS = 7 = VW93 (hurley) when C/O>1
        # TPAGB_WIND_MATTSSON = 8 = Mattsson's
        tpagbwind => 'TPAGB_WIND_VW93_KARAKAS',

        # Mira period at which the VW93 superwind turns on
        superwind_mira_switchon => 500.0,

        # Wind during Wolf-Rayet phase:
        # WR_WIND_BSE = 0 = Hurley et al 2002
        # WR_WIND_MAEDER_MEYNET = 1 = Maeder & Meynet
        # WR_WIND_NUGIS_LAMERS = 2 = Nugis and Lamers
        # WR_WIND_ELDRIDGE = 3 = Eldridge
        # (See Lynnette Dray's thesis, or John Eldridge's thesis)
        # For OB supergiants, use the 2001 paper
        # For the WR stars, use the 2005 paper, scale with with [Fe/H] (not Z)
        # For very massive stars, use the 2001 paper but awaiting
        # updates.
        #
        # Note: ignored if wind is WIND_ALGORITHM_SCHNEIDER2018
        wr_wind => 'WR_WIND_BSE',
        wr_wind_fac => 1.0,

        # Enhanced mass loss because of rotation. If a star
        # overspins, material is removed with the orbital
        # angular momentum given by nonconservative_angmom_gamma (above)
        #
        # ROTATIONALLY_ENHANCED_MASSLOSS_NONE = 0 = none
        #
        # ROTATIONALLY_ENHANCED_MASSLOSS_LANGER_FORMULA = 1 =
        #                      Langer+ formula (in mass-loss rate calculation,
        #                      warning: can be unstable)
        #
        # ROTATIONALLY_ENHANCED_MASSLOSS_ANGMOM = 2 =
        #          remove material in a decretion disc until J<Jcrit
        #
        # ROTATIONALLY_ENHANCED_MASSLOSS_LANGER_FORMULA_AND_ANGMOM = 3 =
        #          Langer+ formula AND decretion disc.
        #
        # I recommend using ROTATIONALLY_ENHANCED_MASSLOSS_NONE or
        # ROTATIONALLY_ENHANCED_MASSLOSS_ANGMOM
        rotationally_enhanced_mass_loss => 'ROTATIONALLY_ENHANCED_MASSLOSS_ANGMOM',
        rotationally_enhanced_exponent => 1.0,

        # orb ang mom loss prescription
        # WIND_ANGMOM_LOSS_BSE = 0 = BSE (Hurley et al. 2002)
        # WIND_ANGMOM_LOSS_LW = 1 = lw * orbital angular momentum
        # WIND_ANGMOM_LOSS_LW_HYBRID = 2 = lw of 1, for fast winds
        # WIND_ANGMOM_LOSS_SPHERICALLY_SYMMETRIC = 3 = spherical wind
        wind_angular_momentum_loss => 'WIND_ANGMOM_LOSS_BSE',
        wind_djorb_fac => 1.0, # multiplies Tout's Jorbdot
        lw => 1.0,  # multiplies lw Jorbdot


        ############################################################
        # Wind accretion
        # bondi-hoyle mass accretion factor (1.5)
        Bondi_Hoyle_accretion_factor => 1.5,

        # Companion Reinforced Attrition Process factor
        CRAP_parameter => 0.0,

        # wind-Roche lobe overflow
        # WRLOF_NONE = 0 = none
        # WRLOF_Q_DEPENDENT = 1 = q-dependent
        # WRLOF_QUADRATIC = 2 = quadratic
        WRLOF_method => 'WRLOF_Q_DEPENDENT',

        ######################
        #
        # Critical mass ratios
        #
        ######################
        #
        # Critical q, qcrit, as a function of donor stellar type for dynamical
        # mass transfer onto either a degenerate or non-degenerate accretor.
        #
        # QCRIT_* is for a non-degenerate accretor.
        # QCRIT_DEGENERATE_* is for a degenerate accretor.
        # * is the stellar type of the donor.
        #
        # If q = mass(donor)/mass(accretor) exceeds qcrit, mass transfer is
        # dynamical (e.g. common-envelope evolution or merger), otherwise
        # the stable rate is used (see RLOF_METHOD below).
        #
        # method for calculating q_crit for comenv on giant branches:
        #
        # QCRIT_GB_BSE = -1 = BSE (Hurley et al. 2002),
        # QCRIT_GB_HJELLMING_WEBBINK = -2 = Hjellming+Webbink,
        # QCRIT_GB_Q_NO_COMENV = -3 = No comenv,
        # QCRIT_GB_CHEN_HAN_TABLE = -4 = Chen+Han(table),
        # QCRIT_GB_CHEN_HAN_FORMULA = -5 = Chen+Han(formula),
        # QCRIT_GB_GE2015 = -6 = Ge+2015,
        # QCRIT_GB_VOS2018 -7 = Vos2018
        #
        # See table 2 of Claeys et al 2014 (C14) although note that
        # they define Q = Ma/Md (hence instability if Q<Qcrit)
        # where we work in q=Md/Ma=M1/M2 instead.
        #
        # q_n = M_n / M_other is defined just after Eq.22 in H02 (BSE).
        # H02 compare q1 = M1/M2 = Md/Ma to qcrit, with unstable
        # mass transfer if q1>qcrit, just as in binary_c.
        #

        # LMMS : de Mink et al 2007 suggests 1.8, C14 suggest 1/1.44 = 0.694
        qcrit_LMMS => 0.6944,
        # MS : C14 suggest 1/0.625 = 1.6, based on de Mink et al 2007 (?)
        qcrit_MS => 1.6,
        # HG : see H02 sect 2.6.1
        qcrit_HG => 4.0,
        # GB : H02 prescription by default
        qcrit_GB => 'QCRIT_GB_BSE',
        # CHeB doesn't matter (screened by GB mass transfer)
        qcrit_CHeB => 3.0,
        # EAGB and TPAGB : see GB
        qcrit_EAGB => 'QCRIT_GB_BSE',
        qcrit_TPAGB => 'QCRIT_GB_BSE',

        # H02 suggest 3
        qcrit_HeMS => 3.0,
        # HeHG and HeGB : see H02 2.6.1
        # C14 suggests 1/0.25 = 4.0 for HeHG (Table 2), but surely this is wrong
        # as they claim in the text to follow H02? H02 gives 0.784, so use this.
        qcrit_HeHG => 0.784,
        # C14 suggests 0.78125=1/1.28 for HeGB (Table 2), H02 gives 0.784
        qcrit_HeGB => 0.784,
        # H02 values, although these should never undergo dynamical mass transfer
        # to a non-degenerate star
        qcrit_HeWD => 3.0,
        qcrit_COWD => 3.0,
        qcrit_ONeWD => 3.0,
        qcrit_NS => 3.0,
        qcrit_BH => 3.0,

        # as above for a degenerate accreting star, taken from Claeys et al 2014
        qcrit_degenerate_LMMS => 1.0,
        # this cannot happen
        qcrit_degenerate_MS => 1.0,
        #  C14 : 1/0.21 = 4.7619
        qcrit_degenerate_HG => 4.7619,
        # C14 : 1/0.87 = 1.15 (based on Hachisu)
        qcrit_degenerate_GB => 1.15,
        # not used
        qcrit_degenerate_CHeB => 3.0,
        # EAGB and TPAGB : see GB (based on Hachisu)
        qcrit_degenerate_EAGB => 1.15,
        qcrit_degenerate_TPAGB => 1.15,
        # ?
        qcrit_degenerate_HeMS => 3.0,
        # HeHG : C14 gives 1/0.21 = 4.7619
        qcrit_degenerate_HeHG => 4.7619,
        # HeGB : C14 gives 1/0.87 = 1.15
        qcrit_degenerate_HeGB => 1.15,
        # WDs : C14 gives 1/1.6 = 0.625
        qcrit_degenerate_HeWD => 0.625,
        qcrit_degenerate_COWD => 0.625,
        qcrit_degenerate_ONeWD => 0.625,
        qcrit_degenerate_NS => 0.625,
        qcrit_degenerate_BH => 0.625,


        ############################################################
        # Common envelopes
        #
        # COMENV_BSE = 0 = BSE (Hurley et al. 2002, based on Paczynski/Webbink-like energy prescription)
        # COMENV_NELEMANS_TOUT = 1 = Nelemans and Tout (angular momentum prescription)
        # COMENV_NANDEZ2016 = 2 = Nandez and Ivanova 2016
        comenv_prescription => 'COMENV_BSE',
        alpha_ce => 1.0, # 1

        # Lambda: structure parameter
        #   if >0 (e.g. 0.5) then use the given value
        #   if LAMBDA_CE_DEWI_TAURIS = -1 then uses functional fit to Dewi and Tauris
        #   if LAMBDA_CE_WANG_2016 = -2 then use the Wang, Jia and Li (2016) formalism (experimental)
        #   if LAMBDA_CE_POLYTROPE = -3 then uses polytropic structure with COMENV_SPLITMASS (experimental)

        lambda_ce => 'LAMBDA_CE_DEWI_TAURIS',
        lambda_ionisation => 0.1, # 0.0

        # spin treatments during common envelope evolution
        # Merger spins:
        # COMENV_MERGER_SPIN_METHOD_SPECIFIC = 0 = preserve specific angular momentum
        # COMENV_MERGER_SPIN_METHOD_CONSERVE_ANGMOM = 1 = preserve angular momentum
        # COMENV_MERGER_SPIN_METHOD_CONSERVE_OMEGA = 2 = preserve angular velocity
        # COMENV_MERGER_SPIN_METHOD_BREAKUP = 3 = leave the merged star at its breakup rotation rate
        comenv_merger_spin_method => 'COMENV_MERGER_SPIN_METHOD_BREAKUP',

        # Ejection spins:
        # COMENV_EJECTION_SPIN_METHOD_DO_NOTHING = 0 = leave spins as they were on comenv entry
        # COMENV_EJECTION_SPIN_METHOD_SYNCHRONIZE = 1 = synchronize to the orbit on comenv exit
        comenv_ejection_spin_method => 'COMENV_EJECTION_SPIN_METHOD_DO_NOTHING',

        # eccentricity after common envelope ejection
        # is at least this (1e-5 is a usual seed for circumbinary discs)
        comenv_post_eccentricity => 1e-5,


        ############################################################
        # circumbinary discs from common envelopes
        comenv_disc_mass_fraction => 1e-2,
        comenv_disc_angmom_fraction => 0.1,
        cbdisc_gamma => 1.4,
        cbdisc_alpha => 1e-3,
        cbdisc_kappa => 1e-2,
        cbdisc_torquef => 1e-3,
        cbdisc_init_dM => 0.0,
        cbdisc_init_dJdM => 0.0,
        cbdisc_minimum_evaporation_timescale => 0.0,
        cbdisc_mass_loss_constant_rate => 0.0,
        cbdisc_mass_loss_inner_viscous_multiplier => 1.0,
        cbdisc_mass_loss_inner_viscous_angular_momentum_multiplier => 1.0,
        cbdisc_mass_loss_inner_L2_cross_multiplier => 1.0,
        cbdisc_mass_loss_ISM_ram_pressure_multiplier => 1.0,
        cbdisc_mass_loss_ISM_pressure => 3000,
        cbdisc_mass_loss_FUV_multiplier => 1.0,
        cbdisc_mass_loss_Xray_multiplier => 1.0,
        cbdisc_minimum_luminosity => 0.0,
        cbdisc_minimum_mass => 1e-6,
        cbdisc_minimum_fRing => 0.2,
        cbdisc_no_wind_if_cbdisc => 'False',
        cbdisc_eccentricity_pumping_method => 'CBDISC_ECCENTRICITY_PUMPING_DERMINE',
        cbdisc_resonance_multiplier => 1.0,
        cbdisc_resonance_damping => 'True',
        cbdisc_mass_loss_inner_viscous_accretion_method => 'CBDISC_MASS_LOSS_INNER_VISCOUS_ACCRETION_METHOD_YOUNG_CLARKE_2015',
        cbdisc_viscous_photoevaporative_coupling => 'True',
        cbdisc_viscous_L2_coupling => 'True',
        cbdisc_fail_ring_inside_separation => 'False',
        cbdisc_inner_edge_stripping => 'True',
        cbdisc_outer_edge_stripping => 'True',
        cbdisc_inner_edge_stripping_timescale => 'DISC_STRIPPING_TIMESCALE_INSTANT',
        cbdisc_outer_edge_stripping_timescale => 'DISC_STRIPPING_TIMESCALE_INSTANT',
        cbdisc_max_lifetime => 1e6,
        disc_timestep_factor => 1.0,
        disc_log => 'DISC_LOG_LEVEL_NONE',
        disc_log2d => 'DISC_LOG_LEVEL_NONE',
        disc_log_dt => 0.0,
        disc_n_monte_carlo_guesses => 0,

        ############################################################
        # RLOF
        #
        # Method for calculting the Roche-lobe overflow rate:
        # RLOF_METHOD_BSE = 0 = BSE (Hurley et al. 2002)
        # RLOF_METHOD_ADAPTIVE = 1 = Adaptive (really only good for radiative stars)
        # RLOF_METHOD_RITTER = 2 = Ritter (probably broken),
        # RLOF_METHOD_CLAEYS = 3 = Claeys et al. 2014 variant on BSE
        RLOF_method => 'RLOF_METHOD_CLAEYS',

        # RLOF mass transfer rate multiplier
        RLOF_mdot_factor => 1.0,

        # RLOF interpolation : use binary_c
        RLOF_interpolation_method => 'RLOF_INTERPOLATION_BINARY_C',

        # Angular momentum in RLOF transfer model
        # RLOF_ANGULAR_MOMENTUM_TRANSFER_MODEL_BSE = 0 : H02 (including disk treatment)
        # RLOF_ANGULAR_MOMENTUM_TRANSFER_MODEL_CONSERVATIVE = 1 : Conservative
        RLOF_angular_momentum_transfer_model => 'RLOF_ANGULAR_MOMENTUM_TRANSFER_MODEL_BSE',

        # non-conservative RLOF angular momentum loss
        nonconservative_angmom_gamma => 'RLOF_NONCONSERVATIVE_GAMMA_DONOR',

        ############################################################
        #
        # White dwarfs
        #
        # Maximum mass of a HeWD when it accretes material
        max_HeWD_mass => 0.7,

        # white dwarf merger algorithm
        # WDWD_MERGER_ALGORITHM_BSE = 0 = BSE (Hurley+2002)
        # WDWD_MERGER_ALGORITHM_PERETS2019 = 1 = Perets 2019
        # WDWD_MERGER_ALGORITHM_CHEN2016 = 2 = Chen 2016
        WDWD_merger_algorithm => 'WDWD_MERGER_ALGORITHM_PERETS2019',

        # novae
        # how to calculate the amount of material accreted/lost during novae?
        # 0 = NOVA_RETENTION_ALGORITHM_CONSTANT = use NOVA_RETENTION_FRACTION (set below)
        # 1 = NOVA_RETENTION_ALGORITHM_CLAEYS2014
        nova_retention_method => 'NOVA_RETENTION_ALGORITHM_CONSTANT',

        # 1e-3 was used in Hurley et al. (2002)
        nova_retention_fraction=>1e-3,

        ############################################################
        # WD accretion
        #
        # Hachisu's disk wind limiter
        hachisu_disk_wind => 'False',
        hachisu_qcrit => 'HACHISU_IGNORE_QCRIT',

        # Lower limit of the mass transfer rate that leads
        # to a the formation of a new giant envelope when accreting
        # onto a white dwarf.
        # Below this mass transfer leads to stable burning.
        #
        # If the rate is a negative number, it is one of the following
        # algorithms:
        #
        # -1 DONOR_RATE_ALGORITHM_CLAEYS2014 : Claeys et al. (2014)
        # -2 DONOR_RATE_ALGORITHM_BSE : Hurley et al. (2002) the BSE algorithm
        #
        # There are three rates corresponding to a hydrogen,
        # helium and other donor.
        WD_accretion_rate_novae_upper_limit_hydrogen_donor => 'DONOR_RATE_ALGORITHM_BSE',
        WD_accretion_rate_novae_upper_limit_helium_donor => 'DONOR_RATE_ALGORITHM_BSE',
        WD_accretion_rate_novae_upper_limit_other_donor => 'DONOR_RATE_ALGORITHM_BSE',
        WD_accretion_rate_new_giant_envelope_lower_limit_hydrogen_donor => 'DONOR_RATE_ALGORITHM_BSE',
        WD_accretion_rate_new_giant_envelope_lower_limit_helium_donor => 'DONOR_RATE_ALGORITHM_BSE',
        WD_accretion_rate_new_giant_envelope_lower_limit_other_donor => 'DONOR_RATE_ALGORITHM_BSE',

        # white dwarf kicks
        wd_sigma => 0.0,
        wd_kick_when => 'WD_KICK_END_AGB',
        wd_kick_pulse_number => 0,
        # kick direction:
        # KICK_RANDOM = 0 = random
        # KICK_STRAIGHT_UP = 1 = up
        # KICK_FORWARD = 2 = forward
        # KICK_BACKWARD = 3 = backward
        # KICK_INWARD = 4 = inward
        # KICK_OUTWARD = 5 = outward
        wd_kick_direction => 'KICK_RANDOM',


        ############################################################
        #
        # Donor rate multipliers
        #
        # 1.0 means the standard rate is enforced.
        #
        # A large or negative number means the limit is
        # ignored.
        #
        # Thermal limit for giant-like stars (1.0)
        donor_limit_thermal_multiplier => 1.0,

        # Dynamical limit
        donor_limit_dynamical_multiplier => 1.0,

        # Envelope limit : don't transfer more than this * the envelope
        #                  mass in one timestep. This is for numerical stability
        #                  rather than to get the physics right.
        donor_limit_envelope_multiplier => 1.0,

        ############################################################
        #
        # Accretion rate multipliers
        #
        # 1.0 means the standard rate is enforced.
        #
        # A large or negative number means the limit is
        # ignored.
        #
        accretion_limit_eddington_steady_multiplier => -1.0, # 1.0
        accretion_limit_eddington_WD_to_remnant_multiplier => -1.0, #-1.0
        accretion_limit_eddington_LMMS_multiplier => -1.0, #1.0

        # Thermal limit for accretion onto a MS, HG or CHeB star
        # (sigma in Eq.14 of C14, in C14 this is 10)
        accretion_limit_thermal_multiplier => 10.0,

        # Dynamical limit for accretion
        accretion_limit_dynamical_multiplier => 1.0,


        ############################################################
        # supernovae
        #
        # yields from type Ia
        #
        # Chandrasekhar mass exploder yield algorithm
        # TYPE_IA_MCH_SUPERNOVA_ALGORITHM_DD2 = 0 = Iwamoto 1999's DD2 model
        # TYPE_IA_MCH_SUPERNOVA_ALGORITHM_SEITENZAHL2013 = 1 = Seitenzahl 2013 (you must set Seitenzahl2013_model)
        # TYPE_IA_MCH_SUPERNOVA_ALGORITHM_SEITENZAHL2013_AUTOMATIC = 2 = Seitenzahl 2013 automatic (uses N100 model with metallicity
        #                                interpolation)
        type_Ia_MCh_supernova_algorithm => 'TYPE_IA_MCH_SUPERNOVA_ALGORITHM_SEITENZAHL2013_AUTOMATIC',
        type_Ia_sub_MCh_supernova_algorithm => 'TYPE_IA_SUB_MCH_SUPERNOVA_ALGORITHM_LIVNE_ARNETT_1995',
        Seitenzahl2013_model => 'N100',

        # mass accretion required for an ELD supernova
        # (no ELDs if negative)
        mass_accretion_for_eld => -1.0,

        # method to calculate the post-SN orbit
        # POST_SN_ORBIT_BSE = 0 = original BSE (Hurley et al. 2002)
        # POST_SN_ORBIT_TT98 = 1 = Tauris and Takens 1998
        post_SN_orbit_method => 'POST_SN_ORBIT_TT98',


        # Supernova kick distribution types.
        #
        # KICK_VELOCITY_FIXED = 0 = fixed
        # KICK_VELOCITY_MAXWELLIAN = 1 = Maxwellian (cf. Hurley et al. 2002)
        # KICK_VELOCITY_CUSTOM = 2 = custom function (see monte_carlo_kicks.c to set this up)
        #
        # Core-collapse supernova kick distribution velocity dispersion
        # 190 km/s Maxwellian is the standard in Hurley et al. (2002)
        # based on the work of Hansen and Phinney (1997)
        #
        # Hobbs (2005) suggests Maxwellian with 265 km/s#
        #
        #
        # Now set the kick distributions per SN type, only defined for SNe
        # that leave a remnant that can be kicked (i.e. not WD kicks).
        #
        # For no kick, set to DISTRIBUTION_* to 0 and SIGMA_* to 0.0
        #
        # core collapse SNe:
        sn_kick_distribution_IBC  => 'KICK_VELOCITY_MAXWELLIAN',
        sn_kick_distribution_GRB_COLLAPSAR  => 'KICK_VELOCITY_FIXED',
        sn_kick_distribution_II  => 'KICK_VELOCITY_MAXWELLIAN',
        sn_kick_distribution_ECAP  => 'KICK_VELOCITY_FIXED',
        sn_kick_distribution_NS_NS  => 'KICK_VELOCITY_FIXED',
        sn_kick_distribution_TZ  => 'KICK_VELOCITY_FIXED',
        sn_kick_distribution_AIC_BH  => 'KICK_VELOCITY_FIXED',
        sn_kick_distribution_BH_BH  => 'KICK_VELOCITY_FIXED',
        sn_kick_distribution_BH_NS  => 'KICK_VELOCITY_FIXED',
        sn_kick_distribution_IA_Hybrid_HeCOWD_subluminous => 'KICK_VELOCITY_FIXED',
        sn_kick_distribution_IA_Hybrid_HeCOWD => 'KICK_VELOCITY_FIXED',

        sn_kick_dispersion_IBC  => 190.0,
        sn_kick_dispersion_GRB_COLLAPSAR  => 0.0,
        sn_kick_dispersion_II  => 190.0,
        sn_kick_dispersion_ECAP  => 0.0,
        sn_kick_dispersion_NS_NS  => 0.0,
        sn_kick_dispersion_TZ  => 0.0,
        sn_kick_dispersion_AIC_BH  => 0.0,
        sn_kick_dispersion_BH_BH  => 0.0,
        sn_kick_dispersion_BH_NS  => 0.0,
        sn_kick_dispersion_IA_Hybrid_HeCOWD => 0.0,
        sn_kick_dispersion_IA_Hybrid_HeCOWD_subluminous => 0.0,

        sn_kick_companion_IA_He  => 'SN_IMPULSE_NONE',
        sn_kick_companion_IA_ELD  => 'SN_IMPULSE_NONE',
        sn_kick_companion_IA_CHAND  => 'SN_IMPULSE_LIU2015',
        sn_kick_companion_AIC  => 'SN_IMPULSE_NONE',
        sn_kick_companion_ECAP  => 'SN_IMPULSE_NONE',
        sn_kick_companion_IA_He_Coal  => 'SN_IMPULSE_NONE',
        sn_kick_companion_IA_CHAND_Coal  => 'SN_IMPULSE_NONE',
        sn_kick_companion_NS_NS  => 'SN_IMPULSE_NONE',
        sn_kick_companion_GRB_COLLAPSAR  => 'SN_IMPULSE_LIU2015',
        sn_kick_companion_HeStarIa  => 'SN_IMPULSE_NONE',
        sn_kick_companion_IBC  => 'SN_IMPULSE_LIU2015',
        sn_kick_companion_II  => 'SN_IMPULSE_LIU2015',
        sn_kick_companion_IIa  => 'SN_IMPULSE_NONE',
        sn_kick_companion_WDKICK => 'SN_IMPULSE_NONE',
        sn_kick_companion_TZ  => 'SN_IMPULSE_NONE',
        sn_kick_companion_AIC_BH  => 'SN_IMPULSE_NONE',
        sn_kick_companion_BH_BH  => 'SN_IMPULSE_NONE',
        sn_kick_companion_BH_NS  => 'SN_IMPULSE_NONE',
        sn_kick_companion_IA_Hybrid_HeCOWD => 'SN_IMPULSE_NONE',
        sn_kick_companion_IA_Hybrid_HeCOWD_subluminous => 'SN_IMPULSE_NONE',

        ############################################################
        #
        # Black holes and neutron stars
        #
        # prescription for black hole masses
        # BH_HURLEY2002 = 0: Hurley et al. (2002)
        # BH_BELCZYNSKI = 1: Belczynski
        # BH_SPERA2015 = 2: Spera+ 2015
        # BH_FRYER12_DELAYED = 3: Fryer 2012 (delayed)
        # BH_FRYER12_RAPID = 4: Fryer 2012 (rapid)
        BH_prescription => 'BH_SPERA2015',

        ############################################################
        # Evolution splitting
        evolution_splitting => 0,
        evolution_splitting_sn_n => 10,
        evolution_splitting_maxdepth => 1,


        ############################################################
        #
        # Maximum time
        max_evolution_time => 15000, # 15000 => Hubble time

        # time(step) control
        minimum_timestep => 1e-6,
        maximum_timestep => 1e3,
        maximum_nuclear_burning_timestep => 1e2,
        maximum_timestep_factor => 0.0,
        dtfac => 1.0,
        timestep_solver_factor => 1.0,
        timestep_modulator => 1.0,
        reverse_time => 'False',
        start_time => 0.0,

        # core and 3dup algorithms:
        #
        # 0 : Hurley (if no NUCSYN), Karakas (if NUCSYN)
        # 1 : Hurley 2002 (overshooting)
        # 2 : Karakas 2002 (non-overshooting)
        # 3 : Stancliffe (3dup only)
        #
        # AGB_*_UP_ALGORITHM_DEFAULT = 0
        # AGB_*_UP_ALGORITHM_HURLEY = 1
        # AGB_*_UP_ALGORITHM_KARAKAS = 2
        # AGB_*_UP_ALGORITHM_STANCLIFFE = 3 (third dredge up and valid for LMC/SMC metallicity only)
        #
        # where * = CORE, RADIUS, LUMINOSITY or THIRD_DREDGE_UP
        AGB_core_algorithm => 'AGB_CORE_ALGORITHM_KARAKAS',
        AGB_3dup_algorithm => 'AGB_THIRD_DREDGE_UP_ALGORITHM_KARAKAS',
        AGB_luminosity_algorithm => 'AGB_LUMINOSITY_ALGORITHM_KARAKAS',
        AGB_radius_algorithm => 'AGB_RADIUS_ALGORITHM_KARAKAS',

        # solver
        # SOLVER_FORWARD_EULER = 0 = Forward Euler
        # SOLVER_RK2 = 1 = RK2
        # SOLVER_RK4 = 2 = RK4
        # SOLVER_PREDICTOR_CORRECTOR = 3 = Predictor-corrector
        solver => 'SOLVER_FORWARD_EULER',
        #solver => 'SOLVER_RK4',

        # Magnetic braking algorithm
        # MAGNETIC_BRAKING_ALGORITHM_HURLEY_2002 = 0 = Hurley et al. (2002)
        # MAGNETIC_BRAKING_ALGORITHM_ANDRONOV_2003 = 1 = Andronov, Pinnsoneault and Sills 2003
        # MAGNETIC_BRAKING_ALGORITHM_BARNES_2010 = 2 = Barnes and Kim 2010
        # MAGNETIC_BRAKING_ALGORITHM_RAPPAPORT_1983 = 3 = Rappaport et al. 1983 (see magnetic_braking_gamma)
        magnetic_braking_algorithm => 'MAGNETIC_BRAKING_ALGORITHM_ANDRONOV_2003',

        # Magnetic braking multipier
        magnetic_braking_factor => 1.0,

        # magnetic braking gamma factor
        # (only relevant to Rappaport et al. 1983)
        magnetic_braking_gamma => 3.0,

        # gravitational radiation
        gravitational_radiation_model => 'GRAVITATIONAL_RADIATION_BSE',
        gravitational_radiation_modulator_J => 1.0,
        gravitational_radiation_modulator_e => 1.0,

        ############################################################
        # small envelope behaviour
        small_envelope_method => 'SMALL_ENVELOPE_METHOD_BSE',

        ############################################################
        # Nucleosynthesis
        initial_abundance_mix => 'NUCSYN_INIT_ABUND_MIX_AG89',
        third_dup => 'True',
        delta_mcmin => 0.0,
        lambda_min => 0.0,
        no_thermohaline_mixing => 'False',
        lambda_multiplier => 1.0,
        hbbtfac => 1.0,
        lithium_hbb_multiplier => 10.0,
        lithium_GB_post_1DUP => 1.0,
        lithium_GB_post_Heflash => 4.0,
        c13_eff => 1.0,
        mc13_pocket_multiplier => 1.0,
        mass_of_pmz => 0.0,

        # code options
        nice => 'nice -n +19', # nice command e.g. 'nice -n +10' or ''

        timeout => 0, # seconds until timeout

        # save args for debugging
        log_args => 1,
        save_args => 1,
        sort_args => 1,
        #log_filename => "/tmp/c_log.__THREAD__.dat",

        tvb => 0, # thread logfile

        # fast as possible
        'no signals' => 1,
        timers => 0,
        );

    return @opts;
};

sub local_setup
{
    my $population = shift;

    # check for required features in binary_c
    # The following must be on:
    $population->require_on(
        #'STELLAR_POPULATIONS_ENSEMBLE',
        );

    # and the following must be off:
    $population->require_off(
        );
    # parse cmd line args
    $population->parse_grid_args();
    $tstart = ($args=~/tstart=(\S+)/)[0];


    $yieldoutfile=''; # if blank use default (compatible with gce.pl)

    # how to normalize output
    # can be 'mass_into_stars' (default), 'none'
    $norm_output='mass_into_stars';

    $gnuplot=0; # plot or not

    # set this to 1 if the stellar code outputs at exact intervals
    # (this is true for all modern binary_c)
    $exacttimes=1;


    if(defined $logdt)
    {
        $logtimes = 1;
        if(defined $dt)
        {
            print "\n>>> Please choose one of dt or logdt, but not both.\n";
            exit;
        }
    }

    if(!$logtimes && !$dt)
    {
        print "\n>>>No timestep is given. Please set this with 'dt=<whatever in Myr>' or logdt=<whatever(Myr)> and tstart=<whatever in Myr>.\n";
        exit;
    }

    if($logtimes && !defined $logdt && !defined $tstart)
    {
        print "If using logtimes, you must set logdt and $tstart\n";
        exit;
    }

    if(0 && $logtimes)
    {
        # adapt max time for log
        my $maxt = $tstart+
            10.0**($logdt*int(log10($population->{_bse_options}->{max_evolution_time} - $tstart)/$logdt)+1);
        print "\nMax time reset to log grid: $population->{_bse_options}->{max_evolution_time} -> $maxt\n";
        $population->{_bse_options}->{max_evolution_time} = $maxt;
    }

    if(!$logtimes)
    {
        # enforce max_evolution_time to be a multiple of dt
        # (otherwise we get a spike in the final timestep)
        my $maxt = $population->{_bse_options}->{max_evolution_time};
        if($maxt/$dt != int($maxt/$dt))
        {
            print "Please select max_evolution_time to be an integer multiple of dt\n";
            exit;
        }
    }

    # used lots, calculate once (* is faster than /, or at least used to be ...)
    $idt = 1.0 / ($logtimes ? $logdt : $dt);

    # calculate number of bins (+1, so we can use < instead of <= in fors)
    $nbins = ntimebins();

    # use time-adaptive mass grid to get yields smooth as f(time)
    $population->{_grid_options}{time_adaptive_mass_grid_step} = $dt;
    $population->{_grid_options}{time_adaptive_mass_grid_log10_step} = $logdt;

    # ensemble_dt controls how often yields are output from binary_c
    # set it : small for greater time accuracy
    #        : large for higher speed (fewer data lines)
    # really you want a happy medium, e.g. 0.1*dt
    $population->{_bse_options}{ensemble_dt} =
        ($exacttimes!=1 ? 0.1 * $dt : $dt) // 0.1;

    if($logtimes)
    {
        # same timestep for when we use log space
        $population->{_bse_options}{ensemble_logdt} = $logdt;

        # turn on log space?
        $population->{_bse_options}{ensemble_logtimes} =
            (defined $logtimes && $logtimes) ? 1 : 0;

        # when to start log times (Myr) NB this is NOT log time
        $population->{_bse_options}{ensemble_startlogtime} = $tstart;
    }

    # minimum and maximum masses
    my $mmin = 0.1; # 0.07 # 2.7 = 500Myr, 0.94 = 13.7Gyr (Z=0.02)
    my $mmax = 20.0;

    ############################################################
    #
    # set up the grid
    #
    ############################################################

    # grid resolution
    my $sampling_factor = 1.0/4.0; # over-resolution (Nyquist/Shannon)

    # M2 minimum mass
    my $m2min = 0.07;

    # fix masses to test period resolution
    my $fixm2 = ($args=~/fixm2=(\S+)/)[0] // 0.0; # set to non-zero mass to fix m2

    # HBB in this mass range (for possible period zoom)
    my $m1hbbmin = 3.0;
    my $m1hbbmax = 8.0;

    # period distribution D+M91 options
    my $DM91_logpermin = -2.0;
    my $DM91_logpermax = 12.0;
    my $DM91_gauss_logmean = 4.8;
    my $DM91_gauss_sigma = 2.3;
    my (@DM91) = ($DM91_logpermin,
                  $DM91_logpermax,
                  $DM91_gauss_logmean,
                  $DM91_gauss_sigma);

    # grid distribution options hash
    $distribution_options = {
        distribution =>
            (defined $distribution && $distribution ne '') ? $distribution :
            'bastard', # 'bastard', 'mixed', 'single', 'binary' or 'Moe'

        # if distribution is 'bastard', everything is handled automatically
        #
        # otherwise:
        # if distribution is "mixed", "single" or "binary"

        # mass 1 spacing : 'log' or 'fixed dt'
        m1spacing => 'log', # fixed dt

        ############################################################
        # options only for 'bastard' distribution

        # period distribution : either DM91 or 'bastard' from
        # Izzard+2018 (Appendix)
        period_distribution => 'bastard',

        # period spacing :  'const' or 'agbzoom'
        period_spacing => ($args=~/period_spacing=(\S+)/)[0] // 'const',
        ############################################################
    };

    my $agbzoom = $distribution_options->{period_spacing} eq 'const' ? 0 : 1;

    # disable agb zoom
    $agbzoom = 0;

    # options for the time-adaptive grid: declare them once
    # so that it's easy to run a bastard distribution or a
    # single/binary/mixed distribution with the same options
    my $time_adaptive_options = '{
    max_evolution_time=>'.$population->{_bse_options}{max_evolution_time}.',
    stellar_lifetime_table_nm=>100,
    time_adaptive_mass_grid_log10_time=>'.($logtimes ? 1 : 0).',
    time_adaptive_mass_grid_step=>'.$sampling_factor*$population->{_grid_options}{time_adaptive_mass_grid_step}.',
    time_adaptive_mass_grid_log10_step=>'.$sampling_factor*$population->{_grid_options}{time_adaptive_mass_grid_log10_step}.',
    time_adaptive_mass_grid_nlow_mass_stars=>100, # increased (default is 10)
    nthreads=>'.$nthreads.',
    thread_sleep=>1,
    mmin=>'.$mmin.',
    mmax=>'.$mmax.',
    extra_flash_resolution=>0, # 1 = broken?
    mass_grid_nlow_mass_stars=>100, # increased (default is 10)
    debugging_output_directory=>"/tmp",
    max_delta_m=>1.0,
    savegrid=>1,
    vb=>0,
    metallicity=>'.$population->{_bse_options}{metallicity}.',
    agbzoom=>'.$agbzoom.',
    ensemble_dt=>1e8,
    stellar_lifetime_cachedir=>"'.$ENV{HOME}.'/.binary_c/stellar_lifetime_cache",
    }';

    # $res is the resolution multiplier.
    # If using the time-adaptive mass grid, m and m1 are ignored.
    my $res = 10; # 40; # 100 for M2, Period
    my %resolution=(
        m=>40, # single stars
        m1=>40, # binary stars
        m2=>$res,
        per=>(($args=~/nper=(\d+)/)[0] // ($res)),
        perzoomf=>0.1, # zoom in ~ AGB RLOF region (0.1)
        perzoomd=>1.0, # delta log P for AGB RLOF (1.0)
        );

    my $nvar=0;

    if($distribution_options->{distribution} eq 'bastard')
    {
        # 'Bastard' distribution of single and binary stars,
        # handled automatically by the distribution_functions module.

        my $time_adaptive_opts = eval($time_adaptive_options);

        distribution_functions::bastard_distribution(
            $population,
            {
                mmin=>$mmin,
                mmax=>$mmax,
                m2min=>$m2min,
                nm=>$resolution{m},
                nm1=>$resolution{m1},
                nm2=>$resolution{m2},
                nper=>$resolution{per},
                qmin=>0.0,
                qmax=>1.0,
                necc=>undef,
                useecc=>undef,
                agbzoom=>$agbzoom,
                #time_adaptive=>$time_adaptive_opts
            }
            );
    }
    elsif($distribution_options->{distribution} eq 'Moe')
    {
        my $opts =
        {
            # input data file (Sophie Dykes' JSON version of Moe's data)
            file => $ENV{HOME}.'/MdS_data.json',

            # resolutions
            resolutions => {
                # masses
                M => [
                    2, # M1 (irrelevant if time adaptive)
                    2,  # M2 (i.e. q)
                    2,  # M3 currently unused
                    2   # M4 currently unused
                    ],
                # orbital periods
                logP => [
                    2, # P (binary period)
                    2,  # P2 (triple period) currently unused
                    2   # P3 (quadruple period) currently unused
                    ],
                # ecc2, ecc3, ecc4
                ecc => [
                    2, # e (binary eccentricity) currently unused
                    2, # e2 (triple eccentricity) currently unused
                    2  # e3 (quadruple eccentricity) currently unused
                    ],
            },

            # ranges employed in parameter space calculations
            ranges => {
                # stellar masses (Msun)
                M => [
                    $mmin, # maximum brown dwarf mass == minimum stellar mass
                    $mmax   # (rather arbitrary) upper mass cutoff
                    ],
                # mass ratio (no units)
                q => [
                    undef, # artificial qmin : set to undef to use default
                    undef  # artificial qmax : set to undef to use default
                    ],
                # log_10 orbital periods (days)
                logP => [
                    0, # 0 = log10(1 day)
                    8 # 8 = log10(10^8 days)
                    ],
                # eccentricity
                ecc => [
                    0.0,
                    1.0
                    ],
            },

            # minimum stellar mass used to calculate qmin
            Mmin => 0.07,

            # multiplicity model (as a function of log10M1)
            #
            # You can use 'Poisson' which uses the system multiplicty
            # given by Moe and maps this to single/binary/triple/quad
            # fractions.
            #
            # Alternatively, 'data' takes the fractions directly
            # from the data, but then triples and quadruples are
            # combined (and there are NO quadruples).
            multiplicity_model => 'Poisson',

            # multiplicity modulator:
            # [single, binary, triple, quadruple]
            #
            # e.g. [1,0,0,0] for single stars only
            #      [0,1,0,0] for binary stars only
            #
            # defaults to [1,1,1,1] i.e. all types
            #
            multiplicity_modulator => [
                1, # single
                1, # binary
                1, # triple
                1, # quadruple
                ],

            # given a mix of multiplities, you can either
            #
            # 'raw'   : stick to what is predicted, i.e.
            #           s=S/(S+B+T+Q), b=B/(S+B+T+Q), t=T/(S+B+T+Q),
            #           q=Q/(S+B+T+Q) without normalization
            #           (in which case s+b+t+q <= 1.0 unless
            #            all you use single, binary, triple and quadruple,
            #            and even then there will be numerical errors)
            #
            # 'norm'  : normalize so that s+b+t+q=1
            #           after calculating the appropriate fractions
            #           s=S/(S+B+T+Q), b=B/(S+B+T+Q), t=T/(S+B+T+Q),
            #           q=Q/(S+B+T+Q)
            #
            # 'merge' : e.g. if you only have single and binary,
            #           add the triples and quadruples to the binaries, so
            #           binaries represent all multiple systems. If you have
            #           only singles, binaries and triples, merges the quads
            #           into to the triples.
            #           After this, normalize such that s+t+b+q=1.
            #           ...
            #           *** this is canonical binary population synthesis ***
            #
            #
            #
            # note: if you only set one multiplicity_modulator
            # to 1, and all the others to 0, then normalizing
            # will mean that you effectively have the same number
            # of stars as single, binary, triple or quad (whichever
            # is non-zero) i.e. the multiplicity fraction is ignored.
            # This is probably not useful except for
            # testing purposes or comparing to old grids.
            normalize_multiplicities => 'merge',

            # mass ratio, q, extrapolation method:
            # none   (poorly tested)
            # flat   (poorly tested)
            # linear2
            # plaw2  (poorly tested)
            # nolowq (poorly tested)
            'q_low_extrapolation_method' => 'linear2',
            'q_high_extrapolation_method' => 'linear2',

            # If time_adaptive is set, use the time adaptive
            # options to calculate the primary mass (M1) spacing
            # such that equal times are sampled.
            #time_adaptive=>$time_adaptive_options,
        };

        distribution_functions::Moe_de_Stefano_2017(
            $population,
            $opts
        );
    }
    else
    {
        # 'mixed' -> mixed single/binary population (binary fraction=f(M1))
        # 'single -> single stars only
        # 'binary' -> binary stars only

        if($distribution_options->{distribution} eq 'mixed')
        {
            # mixture of single and binary stars, with binary fraction
            # as given by the distribution_functions modules as a function of m1
            $population->add_grid_variable(
                'name'=>'multiplicity',
                'longname'=>'Multiplicity',
                'range'=>[1,2],
                'resolution'=>1,
                'spacingfunc'=>'number(1.0)',
                'precode'=>'$self->{_grid_options}{multiplicity}=$multiplicity;$self->flexigrid_grid_resolution_shift();',
                'gridtype'=>'edge',
                'noprobdist'=>1,
                );
        }
        elsif($distribution_options->{distribution} eq 'single')
        {
            # only single stars
            $population->{_grid_options}{multiplicity} = 1;
        $population->add_grid_variable(
                'name'=>'multiplicity',
                'longname'=>'Multiplicity',
                'range'=>[1,1],
                'resolution'=>1,
                'spacingfunc'=>'number(1.0)',
                'precode'=>'$self->{_grid_options}{multiplicity}=$multiplicity;$self->flexigrid_grid_resolution_shift();my $q=0;my $per=0;',
                'gridtype'=>'edge',
                'noprobdist'=>1,
                );
        }
        elsif($distribution_options->{distribution} eq 'binary')
        {
            $population->{_grid_options}{multiplicity} = 2;
        $population->add_grid_variable(
                'name'=>'multiplicity',
                'longname'=>'Multiplicity',
                'range'=>[2,2],
                'resolution'=>1,
                'spacingfunc'=>'number(1.0)',
                'precode'=>'$self->{_grid_options}{multiplicity}=$multiplicity;$self->flexigrid_grid_resolution_shift();',
                'gridtype'=>'edge',
                'noprobdist'=>1,
                );
        }
        else
        {
            print STDERR "distribution_options->distribution (=$distribution_options->{distribution}) is unknown\n";
            exit;
        }

        print "Set multiplicity = $population->{_grid_options}{multiplicity}\n";

        # binary fraction depends on mass, or is 1.0 for
        # a uniquely single or binary distribution
        my $precode = ' $m1=exp($lnm1);  $eccentricity=0.0; ';

        $precode .=
            ($distribution_options->{distribution} eq 'mixed') ?

            # mixed single/binary : weighting is binary fraction
            'my $binary_fraction = main::binary_fraction($self,$m1);  $self->{_grid_options}{weight}= $self->{_grid_options}{multiplicity}==1 ? 1.0-$binary_fraction : $binary_fraction;   ' :

                                                                                                                                                                                         # single or binary stars : weighting is just 1.0
                                                                                                                                                                                         '  my $binary_fraction=($self->{_grid_options}{multiplicity}==1); $self->{_grid_options}{weight}=1.0; ';

        # Mass 1
        if($distribution_options->{m1spacing} eq 'log')
        {
            # log mass spacing
            $precode = '$m1=exp($lnm1); '.$precode;

            $population->add_grid_variable(
                'name'=> 'lnm1',
                'longname'=>'Primary mass',
                'range'=>["log($mmin)","log($mmax)"],
                'resolution'=> "$resolution{m1}", # ignore single stars in approximate resolution calculation
                'spacingfunc'=>"const(log($mmin),log($mmax),\$self->{_grid_options}{multiplicity}==1 ? $resolution{m1} : $resolution{m})",
                'precode'=>$precode,
                'probdist'=>"Kroupa2001(\$m1)*\$m1",
                'dphasevol'=>'$dlnm1',
                );

        }
        elsif($distribution_options->{m1spacing} eq 'fixed dt')
        {
            # fixed-dt mass spacing
            $population->add_grid_variable(
                'name'=> 'lnm1',
                'longname'=>'Primary mass',
                'range'=>["log($mmin)","log($mmax)"],

                # const_dt spacing function options
                'preloopcode'=>"
                my \$const_dt_opts=$time_adaptive_options;
                spacing_functions::const_dt(\$const_dt_opts,'reset');
                ",
                # use const_dt function
                'spacingfunc'=>"const_dt(\$const_dt_opts,'next')",

                # and its resolution
                'resolution'=> "spacing_functions::const_dt(\$const_dt_opts,'resolution');",

                'precode'=>$precode,

                # IMF : default to Kroupa 2001
                'probdist'=>'Kroupa2001($m1)*$m1',

                'dphasevol'=>'$dlnm1',
                );
        }
        else
        {
            print STDERR "What is distribution_options->m1spacing?\n";
            exit;
        }

        if($distribution_options->{distribution} ne 'single')
        {
            # Binary stars
            #
            # Mass 2
            $population->add_grid_variable(
                'condition'=>'$self->{_grid_options}{multiplicity}==2',
                'name'=>'q',
                'longname'=>'Mass ratio',
                'range'=>[($fixm2>0.0 ? $fixm2 : $m2min).'/$m1',1.0],
                'resolution'=>$resolution{m2},
                'spacingfunc'=>(
                    $fixm2!=0.0 ? 'number(0.0)' :
                    'const('.$m2min.'/$m1,1.0,'.$resolution{m2}.')'),
                'probdist'=>
                $fixm2!=0.0  ?
                'number(1.0)' :
                "flatsections\(\$q,\[
                \{min=>$m2min/\$m1,max=>0.8,height=>1.0\},
                \{min=>0.8,max=>1.0,height=>1.0\},
                \]\)",
                precode=>'$m2=$q*$m1;',
                dphasevol=>'$dq',
                );

            # period spacing
            my $perspacing;
            if($distribution_options->{period_spacing} eq 'const')
            {
                $perspacing = "const(-1.0,10.0,$resolution{per})";

            }
            elsif($distribution_options->{period_spacing} eq 'agbzoom')
            {
                $perspacing = 'agbzoom({
                vb=>0,
                logperiod=>$log10per//$log10permin,
                logpermin=>$log10permin, # set by RLOF minimum
                logpermax=>12.0, # DM91 log Period maximum
                dlogperiod=>1.0,
                m1=>$m1,
                m2=>$m2,
                metallicity=>$self->{_bse_options}{metallicity},
                resolution=>'.$resolution{per}.',
                m1hbbmin=>3.0,
                m1hbbmax=>8.0,
                zoomfactor=>0.1
                                   })';
            }
            else
            {
                print STDERR "Period spacing function unknown\n";
                exit;
            }

            # period distribution
            if($distribution_options->{period_distribution} eq 'DM91')
            {
                # old code
                $population->add_grid_variable
                    (
                     'name'=>'logper',
                     'longname'=>'log(Orbital_Period)',
                     'range'=>[$DM91_logpermin,$DM91_logpermax],
                     'resolution',$resolution{per},
                     'spacingfunc'=>$perspacing,
                     'precode'=>"\$per=10.0**\$logper;\$sep=calc_sep_from_period(\$m1,\$m2,\$per)if(defined(\$m1)&&defined(\$m2));",
                     'probdist'=>"gaussian(\$logper,@DM91)",
                     'dphasevol'=>'$dln10per'
                    );
            }
            elsif($distribution_options->{period_distribution} eq 'bastard')
            {
                # period distribution : rob's bastard
                $population->add_grid_variable(
                    'name'=>'log10per',
                    'longname'=>'log10(Orbital_Period)',
                    'range'=>['$log10permin','10.0'],
                    'resolution'=>int($resolution{per}+$resolution{per}/$resolution{perzoomf}),

                    # avoid instant RLOF
                    'preloopcode'=>'my $log10permin = POSIX::log10($self->minimum_period_for_RLOF($m1,$m2));',

                    # spacing function
                    'spacingfunc'=>$perspacing,

                    precode=>"my \$per=10.0**\$log10per;my\$sep=binary_stars::calc_sep_from_period(\$m1,\$m2,\$per);",
                    probdist=>'Izzard2012_period_distribution($per,$m1,$log10permin)',
                    dphasevol=>'$dlog10per'
                    );
            }
            else
            {
                print STDERR "Unknown period distribution\n";
                exit;
            }
        }
    }

    # unique string for this model grid, based on dt, Z, maxtime
    my $default_gridstring //=
        ($logtimes ? 'log' : '').'dt='.($logtimes ? $logdt : $dt).
        # single-star options
        '_Z='.$population->{_bse_options}{metallicity}.
        '_maxtime='.$population->{_bse_options}{max_evolution_time};

    # and input distribution
    $default_gridstring .= '_' .
        ($distribution_options->{distribution} eq 'bastard' ?
         'auto' :
         $distribution_options->{distribution});

    if($distribution_options->{period_spacing} eq 'agbzoom')
    {
        $default_gridstring .= '_agbzoom';
    }

    $gridstring //= $default_gridstring;

    print "Made gridstring : $gridstring\n";

    # output directory
    if(! $args=~/outdir=\S+/)
    {
        $outdir .= '/'.$gridstring;
    }
    make_outdir();

    # skip existing grids
    if(!$args=~/--force/)
    {
        my @testfiles = (
            $outdir.'/ensemble.json',
            );
        foreach my $testfile (@testfiles)
        {
            if(-e -f -s $testfile)
            {
                print "Grid already run (file found at $testfile)\n";
                exit if(!$force);
            }
        }
    }


    $ensemble_opts{tmp}="$outdir/burstensemble.binary".$population->{_grid_options}->{multiplicity}."/"
        if($ensemble_opts{tmp} eq '');
    print "Set output dir $outdir\n";
    my $working_dir;

    ############################################################
    # set up condor
    $working_dir = $ENV{HOME}.'/data/condor_control/yvt'.$condor_count;
    $population->set(
        condor=>0, # to use condor, set condor=1 on cmd line
        condor_njobs=>50, # run this many jobs
        condor_dir=>$working_dir, # working directory for scripts etc.
        condor_streams=>0, # if 1 stream to stdout/stderr (warning: lots of network!)
        condor_memory=>1024, # RAM (MB) per job
        condor_universe=>'vanilla', # always vanilla
        condor_resubmit_finished=>0, # do not resubmit finished jobs
        condor_resubmit_crashed=>1, # do not resubmit crashed jobs
        condor_resubmit_submitted=>0, # do not resubmit submitted jobs
        condor_resubmit_running=>0, # do not resubmit running jobs
        condor_postpone_join=>1, # if 1 do not join on condor, join elsewhere
        condor_join_machine=>undef,
        condor_join_pwd=>undef,
        condor_requirements=>'((Machine!="calx173.ast.cam.ac.uk") && (Machine!="calx141.ast.cam.ac.uk"))',
        );

    ############################################################
    # set up slurm
    $working_dir = $ENV{HOME}.'/data/slurm_control';
    $population->set(
        slurm=>0, # use slurm if 1
        slurm_njobs=>50, # number of jobs
        slurm_dir=>$working_dir, # working directory for scripts etc.
        slurm_memory=>512, # RAM (MB) per job (512MB)
        slurm_warn_max_memory=>1024, # warn at 1GB
        slurm_ntasks=>1, # 1 CPU required per job
        slurm_postpone_join=>0, # if 1 do not join on slurm, join elsewhere
        slurm_partition=>'shared', # MUST be defined
        slurm_jobname=>'binary_c', # not required but useful
        slurm_use_all_node_CPUs=>0, # if given nodes, set to 1
        # if given CPUs, set to 0
        slurm_time=>'48:00:00', # time limit
        );

    # make working dir and parse args
    mkdirhier($working_dir);
    $population->parse_args();

    if($population->{_grid_options}{condor})
    {
        print "Running on Condor : DIR $population->{_grid_options}{condor_dir} on $population->{_grid_options}{condor_njobs} jobs\n";
    }
    else
    {
        print "Running on local machine\n";
    }

    #$population->{_grid_options}->{flexigrid}->{'grid type'} = 'list';
    #$population->{_grid_options}->{flexigrid}->{'list filename'} = '/tmp/broken_args';
}

sub ntimebins
{
    # number of time bins
    print "start at log $tstart : logdt $logdt\n";

    if($logtimes)
    {
        if(!defined $tstart)
        {
            print "Please set the tstart variable, e.g. tstart=0.1\n";
            exit;
        }
        if(!defined $logdt)
        {
            print "Please set the logdt variable, e.g. logdt=0.1\n";
            exit;
        }
    }
    return
        $logtimes ?
        int((log10($population->{_bse_options}{max_evolution_time}) - log10($tstart))/$logdt) + 1 :
        int($population->{_bse_options}{max_evolution_time}/$dt) + 1;
}

sub process_json
{
    # process a chunk of JSON data
    my ($h,$linearray,$population) = @_;
    $h->{'ensemble'} //= {};

    # convert JSON to perl hash, and add to $h->{'ensemble'}
    my $data = parse_json_safe(join(' ',@$linearray));
    my $error = $@;

    # check for error
    if(!defined $data)
    {
        print "WARNING : JSON parsed incorrectly : \n$error\n";
    }

    Hash::RobMerge::arithmetically_add_hashes($h->{'ensemble'},$data);
}

sub get_colranges
{
    my $f=$_[0];
    my @colranges;
    my @datamax;
    my @datamin;

    my @x=`check_col_ranges.pl $f`;

    chomp @x;
    @colranges=split(/\s+/o,$x[0]);
    shift @colranges; # first is time

    @datamax=split(/\s+/o,$x[1]);
    shift @datamax; # MAX
    shift @datamax; # :
    shift @datamax; # the time
    #print "Datamax @datamax\n";

    @datamin=split(/\s+/o,$x[2]);
    shift @datamin; # MIN
    shift @datamin; # :
    shift @datamin; # the time
    #print "Datamin @datamin\n";

    return (\@colranges,\@datamax,\@datamin);
}





sub output_filename
{
    # standard output filename
    my ($stub,$source) = @_;

    my $f = $outdir.'/'.$stub.
        '.z='.$population->{_bse_options}->{metallicity};

    if($distribution_options->{distribution} eq 'single' ||
       $distribution_options->{distribution} eq 'binary')
    {
        $f .= '.'.$distribution_options->{distribution};
    }
    else
    {
        $f .= '.mixed';
    }

    if(defined $source)
    {
        $f .= '.'.$source;
    }

    $f .= '.dat';

    return $f;
}


sub binary_fraction
{
    my ($population,$m1) = @_;
    #return $population->{_grid_options}->{multiplicity}-1 ; # force single or binary stars only
    return distribution_functions::raghavan2010_binary_fraction($m1);
}


sub denominator
{
    # global divider for the yields, rates, etc.
    my ($population) = @_;
    return
        $norm_output eq 'mass_into_stars' ? $population->results->{'mass_into_stars'} :
        $norm_output eq 'none' ? 1.0 :
        0.0;
}

sub denominator_string
{
    # string to explain how the denominator is calculated
    return
        $norm_output eq 'mass_into_stars' ? 'mass into stars' :
        $norm_output eq 'none' ? 'none' :
        'unknown';
}

sub make_outdir
{
    # make output directory
    my $d = $outdir . (defined $_[0] ? ('/'.$_[0]) :'');
    if(! -d $d)
    {
        mkdirhier($d);
        print "Made outdir : $d\n";
    }
    return $d;
}


sub output_json
{
    my ($population) = @_;
    my $h = $population->results;
    print "Output json pop=$population, results=$h\n";

    # output formatted JSON to ensemble.json, and normalise by $idenom
    my $denom = denominator($population);
    if($denom == 0.0)
    {
        #  should be considered a bug
        $denom = 1.0;
        print "WARNING: denominator is 0 : did you run any stars?\n";
    }
    rob_misc::force_numeric_elements($h->{'ensemble'},
                                     {
                                         vb => 0,
                                         modulator => 1.0 / $denom,
                                         numeric_keyformat => '%g'
                                     }
        );

    # add info hash
    $h->{'ensemble'}->{'info'} = $population->info_hash($population);

    #open(FP,'>','/tmp/test.hash');
    #print FP Data::Dumper::Dumper($h);
    #close FP;

    # make text version of the data, sort numerically, pretty print etc.
    my $json = JSON::PP->new->utf8->pretty->allow_nonref->allow_blessed->allow_unknown->sort_by(
        sub
        {
            Sort::Naturally::ncmp($JSON::PP::a,
                                  $JSON::PP::b)
        })->encode($h);

    # and output
    my $file = "$outdir/ensemble.json";
    open(FP,'>',$file)||die("opening $outdir/ensemble.json failed");
    print FP $json,"\n";
    close FP;
    print "See $file\n";
}
