#!/usr/bin/env perl
$|=1;
use strict; # recommended
use 5.16.0; # recommended
use binary_grid2; # required
use binary_grid::C; # backend : C or Perl
use rob_misc qw/ncpus/;


############################################################
#
# Example script to demonstrate how to use the
# binary_grid2 module.
#
# For full documentation, please see binary_grid2.pdf
# in the doc/ directory of binary_c
#
############################################################

# number of computational threads to launch
my $nthreads = rob_misc::ncpus();


# set binary_c's output so we can scan it in parse_bse
# (you can also put output in log_every_timestep yourself)
#
# Note: you should set up this binding before defining
# your $population object, otherwise you will have to manually run
#
# $population->set(custom_output_C_function_pointer => binary_grid2::binary_c_function_bind());
#
Inline->bind(binary_grid2::binary_c_bindings(
                 '
              PRINTF("MASSFUNC %g %g %g %g\n",
                 stardata->model.time,
                 stardata->model.probability,
                 stardata->model.dt,
                 stardata->star[0].mass
                 );
                 '
             ));

# make a new stellar population
my $population = binary_grid2->new(
    # options can be given now ... 
    metallicity => 0.02, # mass fraction of "metals"
    max_evolution_time => 15000, # Myr 
    nthreads=>$nthreads, # number of threads
    );

# ... or options can be set manually later.
$population->set(
    vb=>1, # turn on verbose logging (can be 0,1,2...)
    return_array_refs=>1, # quicker data parsing mode
    log_args=>1,
    sort_args=>1,
    save_args=>1,
    log_args_dir=>'/tmp',
    );

# scan command line arguments for extra options
$population->parse_args();     

# duplicity is 0 for single stars, 1 for binary stars
# and 2 for a mixed population sampled at equal times
my $duplicity = 1;

if($duplicity == 0)
{
    # make a grid of $nstars single binary stars, log-spaced,
    # with masses between $mmin and $mmax
    my $nstars = 100;
    my $mmin = 0.1;
    my $mmax = 80.0;
    $population->add_grid_variable(
        'name'       => 'lnm1', 
        'longname'   =>'Primary mass', 
        'range'      =>[log($mmin),log($mmax)],
        'resolution' => $nstars, # just a counter for the grid
        'spacingfunc'=>"const(log($mmin),log($mmax),$nstars)",
        'precode'    =>'$m1=exp($lnm1);',
        'probdist'   =>'Kroupa2001($m1)*$m1',
        'dphasevol'  =>'$dlnm1',
        );
}
elsif($duplicity == 1)
{
    # make a population of binary stars
    my $resolution = {
        m1 => 10,
        q => 10,
        P => 10
    };
    my $mmin = 0.1;
    my $mmax = 80.0;
    $population->{_grid_options}{binary} = 1;
    
    $population->add_grid_variable
        (
        'name'       => 'lnm1', 
        'longname'   =>'Primary mass', 
        'range'      =>[log($mmin),log($mmax)],
        'resolution' => $resolution->{m1},
        'spacingfunc'=>"const(log($mmin),log($mmax),$resolution->{m1})",
        'precode'    =>'$m1=exp($lnm1);',
        'probdist'   =>'Kroupa2001($m1)*$m1',
        'dphasevol'  =>'$dlnm1',
        );
    # q=M1/M2 distribution flat in q between 0.1/M1 and 1.0
    $population->add_grid_variable
        (
        'condition'  =>'$self->{_grid_options}{binary}==1',
        'name'       =>'q',
        'longname'   =>'Mass ratio',
        'range'      =>['0.1/$m1',1.0],
         'resolution'=>$resolution->{q},
        'spacingfunc'=>"const(0.1/\$m1,1.0,$resolution->{q})",
        'probdist'   =>"flatsections\(\$q,\[
\{min=>0.1/\$m1,max=>0.8,height=>1.0\},
\{min=>0.8,max=>1.0,height=>1.0\},
\]\)",
        precode     =>'$m2=$q*$m1;',
        dphasevol   =>'$dq',
        );
     # orbital period Duquennoy and Mayor 1991 distribution
     $population->add_grid_variable
         (
          'name'       =>'logper',
          'longname'   =>'log(Orbital_Period)',
          'range'      =>[-2.0,12.0],
          'resolution' =>$resolution->{P},
          'spacingfunc'=>"const(-1.0,10.0,$resolution->{P})",
          'precode'=>"\$per = 10.0 ** \$logper;
my \$eccentricity = 0.0;
\$sep=calc_sep_from_period(\$m1,\$m2,\$per) if(defined \$m1 && defined \$m2);
",
          'probdist'=>"gaussian(\$logper,4.8,2.3,-2.0,12.0)",
          'dphasevol'=>'$dln10per'
         );
}
else
{
    # Make a mixed population of single and binary stars
    # with equal sampling in equal times.
    my $dt = 100.0;
    $population->set(
        time_adaptive_mass_grid_step => $dt,
        );
    my $sampling_factor = 0.5; # deliberate oversampling (<0.5)
    my $time_adaptive_options = {
        max_evolution_time =>
            $population->{_bse_options}{max_evolution_time},
        stellar_lifetime_table_nm=>1000,
        time_adaptive_mass_grid_log10_time=>0,
        time_adaptive_mass_grid_step=>$dt,
        time_adaptive_mass_grid_nlow_mass_stars=>10,
        nthreads           =>$nthreads,
        thread_sleep       =>1,
        mmin               =>0.1,
        mmax               =>80.0,
        mass_grid_log10_time=>0,
        mass_grid_step      =>$dt*$sampling_factor,
        extra_flash_resolution=>0, # 1 = broken?
        mass_grid_nlow_mass_stars=>10,
        debugging_output_directory=>'/tmp',
        max_delta_m         =>1.0,
        savegrid            =>1,
        vb                  =>0,
        metallicity=>$population->{_bse_options}{metallicity},
        agbzoom             =>0,
    };
    distribution_functions::bastard_distribution(
        $population, {
            mmin         =>0.1,
            mmax         =>80.0,
            m2min        =>0.1,
            nm2          =>10,
            nper         =>10,
            qmin         =>0.0,
            qmax         =>1.0,
            necc         =>undef,
            useecc       =>undef,
            agbzoom      =>0,
            time_adaptive=>$time_adaptive_options,
        });
}

# link population to custom data parser function
$population->set(
    parse_bse_function_pointer => \&main::parse_data
    );

my %init = $population->initial_abundance_hash('Karakas2002',0.02);
my %isotope_hash = $population->isotope_hash();
my @isotope_list = $population->isotope_list();
my %nuclear_mass_hash = $population->nuclear_mass_hash();
my @nuclear_mass_list = $population->nuclear_mass_list();
my @sources = $population->source_list();
my @ensemble = $population->ensemble_list();

if(0){
print Data::Dumper->Dump([
    #\%init,
    #\%isotope_hash,
    #\@isotope_list,
    #\%nuclear_mass_hash,
    \@nuclear_mass_list,
    #\@sources,
    #\@ensemble
                         ]);
}

# uncomment this to show version information
#print $population->evcode_version_string();

# uncomment this to show the evcode's args list
#print join("\n",@{$population->evcode_args_list()});

# evolution the stellar population (this takes some time)
$population->evolve();

# output the data
output($population);

# done : exit
exit;

############################################################
# subroutines 
############################################################

sub parse_data
{
    my ($population, $results) = @_;

    while(1)
    {
        # subsequent calls to tbse_line contain
        # (references to) arrays of data 
        my $la = $population->tbse_line();
        
        # first element is the "header" line
        my $header = shift @$la;

        # break out of the look if this is 'fin'
        last if ($header eq 'fin');

        # check if $header matches one of your
        # expected data lines, if so, act
        if($header eq 'MASSFUNC')
        {
            # matched MASSFUNC header
            #
            # ... so do stuff with $la.You have to
            # enable a line in binary_c which outputs
            # the mass, the probability and the timestep
            # (see log_every_timestep.c for examples)
            # and starts with MASSFUNC
            #
            my $time = $la->[0];
            my $probability = $la->[1];
            my $timestep = $la->[2];
            my $mass = $la->[3];
            
            # bin mass to nearest 1.0 Msun
            $mass = $population->rebin($mass, 1.0);

            # add up the mass distribution in a histogram
            $results->{mass_distribution}->{$mass} += $probability * $timestep; 
        }
    }
}

############################################################

sub output
{
    my $population = shift;

    # $results is a hash reference containing
    # the results that were added up in parse_data()
    my $results = $population->results;

    # output the mass distribution
    foreach my $mass (sort {$a<=>$b} keys %{$results->{mass_distribution}})
    {
        printf "%g %g\n",$mass,$results->{mass_distribution}->{$mass};
    }
}

__END__
__C__

