#!/usr/bin/env perl
use strict; # recommended
use 5.16.0; # recommended
$|=1;
use IO::Handle;
use Math::Trig;
use Sort::Key qw/nsort/;
use List::MoreUtils qw/uniq/;

############################################################
#
# CEMP stars with binary_grid2
#
############################################################

# before loading threads, open/setup output
my $outdir = "$ENV{HOME}/data/cemps2017";
mkdirhier($outdir);

# cosmology
my $OmegaM = 0.27;
my $OmegaLambda = 0.73; 

$OmegaM = 0.3;
$OmegaLambda = 0.7;

my $OmegaMLambda = $OmegaM / $OmegaLambda;
my $OmegaLambdaM = $OmegaLambda / $OmegaM;

my $sqrtOmegaMLambda = sqrt($OmegaMLambda);
my $sqrtOmegaLambdaM = sqrt($OmegaLambdaM);
my $asinhsqrtOmegaLambdaM = asinh(sqrt($OmegaLambdaM));


my $h = 68.0 * 1e5 / 3.08571e24; # H0 in cgs NB 1e5/3.086e24 = km/Mpc
my $agefac = 3.1557e7 * 1e9; # Gyr in s
my $agefach = 3.0/2.0 * $agefac * $h;
my $age_from_redshift_f0 = 2.0/3.0 * sqrt(1.0 + $OmegaMLambda) * (1.0/$h) / $agefac;


# load binary grid and rob's modules
use binary_grid2; # required
use binary_grid::C; # backend : C or Perl
use rob_misc qw/ncpus log10 mkdirhier MAX MIN/;
use IO::File;
my @types = ('CEMP','XEMP','CNEMP','NEMP');
my $progenitors_fp = {};
my $long_progenitors_fp = {};
foreach my $type (@types)
{
    open($progenitors_fp->{$type}, '>', $outdir.'/'.$type.'_progenitors')
        || die("cannot open $type progenitors' file");
    open($long_progenitors_fp->{$type}, '>', $outdir.'/'.$type.'_progenitors_long')
        || die("cannot open long $type progenitors' file");
    $progenitors_fp->{$type}->autoflush(1);
    $long_progenitors_fp->{$type}->autoflush(1);
}

# make lock var for atomic output 
use threads::shared;
my $lockvar : shared;
my $progenitor_fp_lock : shared;

# number of computational threads to launch
my $nthreads = rob_misc::ncpus();

# make a new stellar population
my $population = binary_grid2->new(
    # options can be given now ... 
    metallicity => 1e-4, # mass fraction of "metals"
    max_evolution_time => 13700, # Myr 
    nthreads=>$nthreads, # number of threads
    );

# ... or options can be set manually later.
$population->set(
    vb=>1, # turn on verbose logging (can be 0,1,2...)
    return_array_refs=>1, # quicker data parsing mode
    log_args=>1,
    sort_args=>1,
    save_args=>1,
    log_args_dir=>'/tmp',
    );

$population->set(
    {
        default_to => 'bse'
    },

    mass_accretion_for_eld => 1e6,

    # comenv options
    minimum_donor_menv_for_comenv => 0.1,
    alpha_ce=>1.0,
    lambda_ce=>-1.0,
    lambda_ionisation=>0.1,
    lambda_enthalpy=>0.0,

    # AGB Mc,R,L evolution algorithm
    # 0 : compatibility mode : Hurley (if no NUCSYN), Karakas (if NUCSYN)
    # 1 : Hurley 2002 (overshooting)
    # 2 : Karakas 2002 (non-overshooting) 
    #
    # Third dredge up has the extra choice:
    # 3 : Stancliffe (LMC/SMC metallicity only)
    AGB_core_algorithm=>0,
    AGB_radius_algorithm=>0,
    AGB_luminosity_algorithm=>0,
    AGB_3dup_algorithm=>0,

    # CEMP options
    CEMP_minimum_age => 10e3, # 10 Gyr 
    CEMP_logg_maximum => 4.0, # logg < 4.0 for CEMP stars 
    CEMP_cfe_minimum => 1.0, # [C/Fe] > 1 for CEMP stars

    # wind RLOF
    WRLOF_method => 0,

    # lose material on accretion
    rotationally_enhanced_mass_loss => 0,

    # fake metallicity
    nucsyn_metallicity => 1e-4,

    # max timestep 25 Myr
    maximum_timestep => 25,
    );

# scan command line arguments for extra options
$population->parse_args();  

# duplicity is 0 for single stars, 1 for binary stars
# and 2 for a mixed population sampled at equal times
my $duplicity = 1;
my $mmin = 0.08;

if($duplicity == 0)
{
    # make a grid of $nstars single binary stars, log-spaced,
    # with masses between $mmin and $mmax
    my $nstars = 40;
    
    my $mmax = 80.0;
    $population->add_grid_variable(
        'name'       => 'lnm1', 
        'longname'   =>'Primary mass', 
        'range'      =>[log($mmin),log($mmax)],
        'resolution' => $nstars, # just a counter for the grid
        'spacingfunc'=>"const(log($mmin),log($mmax),$nstars)",
        'precode'    =>'$m1=exp($lnm1);',
        'probdist'   =>'Kroupa2001($m1,{m0=>'.$mmin.'})*$m1',
        'dphasevol'  =>'$dlnm1',
        );
}
elsif($duplicity == 1)
{
    # make a population of binary stars
    my $n = 100; # n>20 gives reasonable CEMP/XEMP
    my $resolution = {
        m1 => $n,
        q => $n,
        P => $n
    };
    my $qzoom = 1;
    my $mmin = $mmin;
    my $mmax = 6.0;
    $population->{_grid_options}{binary} = 1;
    
    # M1 zoom expression
    # MIN(1.0, 1.0 - 0.8 * exp( - ($m - $mEMP)**2 / 0.01 ))

    # q zoom expression : 
    # MIN(1.0, 1.0 - 0.2 * exp( - ($m - $mEMP)**2 / 0.1 ))

    my $mEMP = 0.83; # mass that gives giants at 13Gyr with Z=1e-4

    # M1 has to be in the range of stars that can make carbon
    # e.g. 0.9Msun + and include those stars that can be XEMP
    # i.e. > 0.7Msun. Setting > 0.7Msun means the number of
    # XEMP "single" stars is wrong. Note that there is also
    # a small number of 0.4+0.4Msun mergers which contribute. These
    # are few in number because they are only very short period systems.
    my $m1min = 0.7;

    $population->add_grid_variable
        (
         'name'       => 'lnm1', 
         'longname'   =>'Primary mass', 
         'range'      =>[log($m1min),log($mmax)],
         'resolution' => $resolution->{m1},
         'spacingfunc'=>"const(log($m1min),log($mmax),$resolution->{m1})*MIN(1.0, 1.0 - 0.8 * exp( - (\$m1 - $mEMP)**2 / 0.01 ))",
         'precode'    =>'$m1=exp($lnm1);',
         'probdist'   =>'Kroupa2001($m1,{m0=>'.$mmin.'})*$m1', # 1.38%
         #'probdist'   =>'ktg93($m1,{m0=>'.$mmin.'})*$m1',
         'dphasevol'  =>'$dlnm1',
        );
    # q=M1/M2 distribution flat in q between $mmin/M1 and 1.0
    #
    # delta q = delta (M1 / M2) = M1 delta (1/M2) = - M1 * delta M2 / M2^2

    $population->add_grid_variable
        (
         'condition'  =>'$self->{_grid_options}{binary}==1',
         'name'       =>'q',
         'longname'   =>'Mass ratio',
         'range'      =>[$mmin.'/$m1',1.0],
         'resolution'=>$resolution->{q},
         'spacingfunc'=>
         ("const($mmin/\$m1,1.0,$resolution->{q})" . 
          ($qzoom ?
           "* (MIN(1.0, 1.0 - 0.8 * exp( - (\$q * \$m1 - 0.8)**2.0 / 0.1 ))) " 
          : '')),
         'probdist'   =>"flatsections\(\$q,\[
\{min=>$mmin/\$m1,max=>0.8,height=>1.0\},
\{min=>0.8,max=>1.0,height=>1.0\},
\]\)",
         precode     =>'$m2=$q*$m1;',
         dphasevol   =>'$dq',
        );

    # DQ -> 1%, flatlna -> 
    # persepdist should be DQ91 or flatlna
    my $persepdist = 'flatlna'; 

    if($persepdist eq 'DQ91')
    {
        # orbital period Duquennoy and Mayor 1991 distribution
        $population->add_grid_variable
            (
             'name'       =>'logper',
             'longname'   =>'log(Orbital_Period)',
             'range'      =>[-2.0,12.0],
             'resolution' =>$resolution->{P},
             'spacingfunc'=>"const(-1.0,10.0,$resolution->{P})",
             'precode'=>"\$per = 10.0 ** \$logper;
my \$eccentricity = 0.0;
\$sep=calc_sep_from_period(\$m1,\$m2,\$per) if(defined \$m1 && defined \$m2);
",
             'probdist'=>"gaussian(\$logper,4.8,2.3,-2.0,12.0)",
             'dphasevol'=>'$dln10per'
            );
    }
    elsif($persepdist eq 'flatlna')
    {
        $population->add_grid_variable(
            # name of the variable
            'name'=>'lnsep',
            'longname'=>'ln(Orbital_Separation)',
            'range'=>['log(3.0)','log(1e4)'],
            'resolution'=>$resolution->{P},
            'spacingfunc'=>"const(log(3.0),log(1e4),$resolution->{P})",
            # precode has to calculation the period (required for binary_c)
            'precode'=>"my \$eccentricity = 0.0;my\$sep=exp(\$lnsep);my\$per=calc_period_from_sep(\$m1,\$m2,\$sep);",
            # flat in log-separation (dN/da~1/a) distribution (Opik law)
            'probdist'=>'const(log(3.0),log(1e4))',
            # phase volume contribution
            'dphasevol'=>'$dlnsep'
            );
    }
    else
    {
        print STDERR "Unknown persepdist = $persepdist\n";
        die;
    }

}
else
{
    # Make a mixed population of single and binary stars
    # with equal sampling in equal times.
    my $dt = 100.0;
    $population->set(
        time_adaptive_mass_grid_step => $dt,
        );
    my $sampling_factor = 0.5; # deliberate oversampling (<0.5)
    my $time_adaptive_options = {
        max_evolution_time =>
            $population->{_bse_options}{max_evolution_time},
            stellar_lifetime_table_nm=>1000,
            time_adaptive_mass_grid_log10_time=>0,
            time_adaptive_mass_grid_step=>$dt,
            time_adaptive_mass_grid_nlow_mass_stars=>10,
            nthreads           =>$nthreads,
            thread_sleep       =>1,
            mmin               =>$mmin,
            mmax               =>80.0,
            mass_grid_log10_time=>0,
            mass_grid_step      =>$dt*$sampling_factor,
            extra_flash_resolution=>0, # 1 = broken?
            mass_grid_nlow_mass_stars=>10,
            debugging_output_directory=>'/tmp',
            max_delta_m         =>1.0,
            savegrid            =>1,
            vb                  =>0,
            metallicity=>$population->{_bse_options}{metallicity},
            agbzoom             =>0,
    };
    distribution_functions::bastard_distribution(
        $population, {
            mmin         =>$mmin,
            mmax         =>80.0,
            m2min        =>$mmin,
            nm2          =>10,
            nper         =>10,
            qmin         =>0.0,
            qmax         =>1.0,
            necc         =>undef,
            useecc       =>undef,
            agbzoom      =>0,
            time_adaptive=>$time_adaptive_options,
        });
}

# link population to custom data parser function
$population->set(
    parse_bse_function_pointer => \&main::parse_data
    );

my %init = $population->initial_abundance_hash('Karakas2002',0.02);
my %isotope_hash = $population->isotope_hash();
my @isotope_list = $population->isotope_list();
my %nuclear_mass_hash = $population->nuclear_mass_hash();
my @nuclear_mass_list = $population->nuclear_mass_list();
my @sources = $population->source_list();
my @ensemble = $population->ensemble_list();

# check code features
#$population->require_not();
$population->require(
    'NUCSYN is on',
    'NUCSYN_CEMP_LOGGING is on',
    'NUCSYN_THIRD_DREDGE_UP is on',
    'NUCSYN_TPAGB_HBB is on',
    );

# age sampling (Gyr)
my @ages = (0.0,1,10,11,12,13,13.3,13.4,13.5,13.6,13.7,13.8,13.9,14);

my $maxage = age_from_redshift(1e10);
print "Universe age $maxage\n";

@ages = grep { $_ < $maxage} @ages;

# make a set of redshifts corresponding to the ages
my @redshifts = make_redshifts(\@ages);

foreach my $redshift (@redshifts)
{
    printf "z = %g -> age = %g\n",
    $redshift,
    age_from_redshift($redshift);
}

if(0)
{
    # test cosmology functions
    foreach my $z (0,1,2,5,10,100,1e3,1e4)
    {
        my $age = age_from_redshift($z); 
        printf "Age at redshift $z = %g : inverse gives %g\n",
        $age,
        redshift_from_age($age);
    }
    exit;
}

# evolution the stellar population (this takes some time)
$population->evolve();


# output the data
output($population);

# done : exit
exit;

############################################################
# subroutines 
############################################################

sub parse_data
{
    my ($population, $results) = @_;
    
    # progenitor hash location
    my $phash = $population->{_grid_options}->{progenitor_hash};
    my $m1init = $phash->{m1}; 
    my $q = $phash->{m2} / $phash->{m1};
    my $p_Kroupa = distribution_functions::Kroupa2001($m1init,{m0=>$mmin});
    
    my $progenitor_string = sprintf '%g %g %g %g',
    $phash->{m1},
    $q,
    $phash->{per},
    $phash->{prob};
    my $stats = {};
    
    while(1)
    {
        # subsequent calls to tbse_line contain
        # (references to) arrays of data 
        my $la = $population->tbse_line();
        
        # first element is the "header" line
        my $header = shift @$la;
                
        # break out of the look if this is 'fin'
        last if ($header eq 'fin');

        # check if $header matches one of your
        # expected data lines, if so, act
        if($header=~/(?:CN?|X)EMP/)
        {
            # header is either CEMP, CNEMP or XEMP

            # logged data
            my (
                $time,
                $dt,
                $p,
                $stellar_type,
                $mass,
                $luminosity,
                $radius,
                $teff,
                $logg,
                $orbital_separation, # Rsun
                $orbital_period, # years
                $eccentricity,
                $CO_ratio,
                
                # abundances follow : we don't care about them
                # so ignore them
                ) = @$la;

#            printf "T=%g (sum %g) st=%d dt=%g p=%g M=%g L=%g R=%g\n", 
 #           $time,
  #          $stats->{$header}->{dt}*1e6,
   #         $stellar_type,$dt,$p,$mass,$luminosity,$radius;

            if($stats->{$header}->{dt}> 3.8e9)
            {
                print "$header for > 3.8 Gyr ! (mid)\n";
                exit(2);
            }
            
            my $dtp = $dt * $p;

            # now repeat for different redshifts
            foreach my $z (@redshifts)
            {
                my $p_Adam = adam_imf($m1init,$z);
                my $factor = $p_Adam / $p_Kroupa;

                if(0)
                {
                    printf "IMF(m=%g, z=%g, age=%g Gyr) = K %g A %g : f = %g\n",
                    $m1init,
                    $z,
                    age_from_redshift($z),
                    $p_Kroupa,
                    $p_Adam,
                    $factor;
                }
                
                $results->{count}->{$z}->{$header} += $dtp * $factor;
            }

            # CEMP/XEMP vs time to nearest 100 Myr
            my $t = $population->rebin($time,100.0);
            $results->{timecount}->{$header}->{$t} += $dtp; 

            # progenitor information
            $stats->{$header}->{dtp} += $dtp;
            $stats->{$header}->{dt} += $dt;
            #print "DT $dt at $time\n";
        }
    }

    # save progenitor information
    foreach my $type (@types)
    {
        if($stats->{$type}->{dtp} > 0.0)
        {
            lock $progenitor_fp_lock;
            printf {$progenitors_fp->{$type}} "%s %g %g\n",
            $progenitor_string,
            $stats->{$type}->{dtp},
            $stats->{$type}->{dt}*1e6;

            printf {$long_progenitors_fp->{$type}} "%s %g %g\n",
            $population->{_threadinfo}->{lastargs},
            $stats->{$type}->{dtp},
            $stats->{$type}->{dt}*1e6;
                        
            my $ddt = $stats->{$type}->{dt}*1e6;
            #printf "DDT %s %g\n",$type,$ddt;
            if($ddt > 3.8e9)
            {
                print "$type for $type : $ddt > 3.8 Gyr! (end)\n";
                exit(2);
            }
        }
    }
}


sub adam_imf
{
    my ($m,$z) = @_; # mass, redshift
    my $p;
    my $f = adam_f($z);
    return distribution_functions::three_part_power_law
        (
         $m,
         $mmin,
         0.08 * $f,
         0.5 * $f,
         80.0,
         -0.3,
         -1.3,
         -2.3);
}

sub adam_f
{
    my ($z) = @_;
    my $r = MAX(1.0, $z/6.3); 
    return $r * $r;
}

############################################################



sub output
{

    my $population = shift;

    # close open log files
    foreach my $type (@types)
    {
        close $progenitors_fp->{$type};
        close $long_progenitors_fp->{$type};
    }
    
    # $results is a hash reference containing
    # the results that were added up in parse_data()
    my $results = $population->results;
    printf "OUTPUT : hash size %d\n",rob_misc::hashcount($population->results);

    {
        # Output grid information
        my $outfile = $outdir.'/gridinfo';
        open(my $fp,'>',$outfile);
        print {$fp} $population->infostring();
        close $fp;
    }

    {
        # Output grid information
        my $outfile = $outdir.'/version';
        open(my $fp,'>',$outfile);
        print {$fp} $population->evcode_version_string();
        close $fp;
    }


    my $tot_EMP = {};
    
    foreach my $z (keys %{$results->{count}})
    {
        $tot_EMP->{$z} //= 0.0;
        my @counttypes = sort keys %{$results->{count}->{$z}};
        foreach my $counttype (@counttypes)
        {
            $tot_EMP->{$z} += $results->{count}->{$z}->{$counttype};
        }
        $tot_EMP->{$z} = MAX(1e-100,$tot_EMP->{$z}); # prevent 1/0        
    }

    foreach my $z (nsort keys %{$results->{count}})
    {
        print "----------\n";            
        printf "Redshift %g, age %g\n\n",$z,age_from_redshift($z);
        my @counttypes = sort keys %{$results->{count}->{$z}};
        foreach my $counttype (@counttypes)
        {
            printf "% 10s %5.2f %% %g\n",
            $counttype,
            $results->{count}->{$z}->{$counttype} * 100.0/ $tot_EMP->{$z},
            $results->{count}->{$z}->{$counttype} ;
        }
    }
    print "----------\n";

    # CEMP/EMP vs time

    open(my $fp,'>',$outdir.'/cemptimes.dat')||die("cannot open /tmp/cemptimes.dat");
    # make a sorted list of times
    my @times = nsort uniq (keys %{$results->{timecount}->{CEMP}},
                            keys %{$results->{timecount}->{XEMP}});
    my $streams = [ $fp, *STDOUT ]; # output streams
    mprintf($streams, "#%19s %20s %20s %20s\n",'Time','CEMP','XEMP','CEMP/EMP');
    foreach my $t (@times)
    {
        mprintf ($streams, 
        "% 20g % 20g % 20g % 20g\n",
        $t,
        $results->{timecount}->{CEMP}->{$t},
        $results->{timecount}->{XEMP}->{$t},
        $results->{timecount}->{CEMP}->{$t}/
            MAX(1e-50,
                $results->{timecount}->{CEMP}->{$t}+
                $results->{timecount}->{XEMP}->{$t}));
    }
    print "----------\n";
    close $fp;
}

sub mprintf
{
    # print to multiple streams
    my ($array) = shift @_;
    foreach my $stream (@$array)
    {
        printf {$stream} @_;
    }
}

sub safelog10
{
    # safe log10 function (prevents log(0))
    return log10(MAX(1e-30,$_[0]));
}

sub make_redshifts
{
    my ($ages) = @_;
    my @redshifts;
    foreach my $age (@$ages)
    {
        push(@redshifts, redshift_from_age($age));
    }
    return @redshifts;
}

############################################################
# cosmology
############################################################


sub redshift_from_age
{
    # Eq.13 : redshfift from age in Gyr
    my ($age) = @_;   # Gyr

    return 0.0 if ($age == 0.0);

    # Weinberg 1989 Eq. 5.4 inverted
    # Valid for a flat Universe

    my $z = (
        $sqrtOmegaMLambda 
        *
        sinh(
            $asinhsqrtOmegaLambdaM
            -
            $age * $agefach / 
            sqrt(1.0 + $OmegaMLambda)
        ))**(-2.0/3.0)
        - 
        1.0;
    
    return $z;
}

sub age_from_redshift
{
    # inverse of the above, return in Gyr
    my ($z) = @_; 

    return 0.0 if ($z == 0.0);

    # Weinberg 1989 Eq. 5.4
    # Valid for a flat Universe

    my $age = 
        $age_from_redshift_f0 * 
        (
         $asinhsqrtOmegaLambdaM
         -
         asinh(sqrt($OmegaLambdaM) * (1.0 + $z)**-1.5)
        );

    return $age;
}
