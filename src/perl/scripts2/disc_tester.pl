#!/usr/bin/env perl
use strict;
use rob_misc;
use Hash::Ordered;
use binary_grid::C;


#
# script to test discs' parameter space
#


my $opts = {
    nice => '/usr/bin/ionice -c 3 /usr/bin/nice -n +19',
    timeout => '/usr/bin/timeout 300',
    #timeout => '',
    grepv => '^DISC | grep -v ^XYIELD',
};

# binary_c_opts are either:
#
# scalar : fixed
# array of two elements : linear-spaced in range e.g. [MIN, MAX]
# array of two elements in double brackets : log-spaced in range [MIN, MAX]
# other array : choose one from the array (but ignore undefs)
#    i.e. to choose one of 0 or 1, set the array to [undef,0,1]

my $binary_c_opts = Hash::Ordered->new(
    # stellar system
    M_1 => [[1.0,6.0]],
    M_2 => [[1.0,6.0]],
    orbital_period => 0,
    separation => [[10.0, 1e4]],
    metallicity => 0.02,
    max_evolution_time => 15e3,
    minimum_timestep => 1e-6,

    # comenv -> disc initialisation
    comenv_disc_mass_fraction => 0.01,
    comenv_disc_angmom_fraction => 0.5,
    
    # disc physics
    cbdisc_gamma => 5.0/3.0,
    cbdisc_alpha => [[1e-6,0.1]],
    cbdisc_kappa => [[1e-4,1.0]],
    cbdisc_torquef => [[1e-10,1e-2]],
    cbdisc_mass_loss_constant_rate => 0.0,
    cbdisc_mass_loss_inner_viscous_multiplier => [0.0,1e2],
    cbdisc_mass_loss_inner_viscous_accretion_method => [0,1,2],
    cbdisc_mass_loss_inner_L2_cross_multiplier => [0.0,1.0],
    cbdisc_mass_loss_ISM_pressure => [[1e2,1e5]],
    cbdisc_mass_loss_ISM_ram_pressure_multiplier => [0.0,1e2],
    cbdisc_mass_loss_FUV_multiplier => 0.0,
    cbdisc_mass_loss_Xray_multiplier => [0.0,1e2],
    cbdisc_viscous_photoevaporation_coupling => [undef,0,1],
    cbdisc_inner_edge_stripping => [undef,0,1],
    cbdisc_outer_edge_stripping => [undef,0,1],
    cbdisc_eccentricity_pumping_method => [undef,0,1],
    cbdisc_resonance_multiplier => [0.0,1e2],
    cbdisc_resonance_damping => [undef,0,1],

    # output
    disc_log => 0,
    
    # timestepping
    disc_timestep_factor => 0.01,  # fixed at the moment
    
    # stop conditions
    cbdisc_minimum_luminosity => 1e-4,
    cbdisc_minimum_mass => 1e-6,
    cbdisc_minimum_fRing => 0.2, #[0.0,0.5],
    cbdisc_fail_ring_inside_separation => 0,
    cbdisc_max_lifetime => 1e6, # years
    );

$opts->{timeout}//='';

print "\n";

my @optkeys = $binary_c_opts->keys;

while(1)
{
    my $theseopts = {};
    foreach my $opt (@optkeys)
    {
        $theseopts->{$opt} = choose($binary_c_opts->get($opt));
    }

    my $args;
    foreach my $opt (@optkeys)
    {
        
        $args .= '--'.$opt .' '.$theseopts->{$opt}.' ';
    }
    
    my $cmd = "$opts->{timeout} $opts->{nice} binary_c $args | grep -v $opts->{grepv}";

    print "At ",scalar (localtime)," launch:\n\n";
    print $cmd,"\n\n";
    my $r = `$cmd`;

    if($r=~/Tick count/)
    {
        if($r=~/Error/i || $r=~/exit/i || $r=~/failed/)
        {
            print "Error detected\n";
            exit;
        }
        else
        {
            print "$r\nok\n";
        }
    }
    else
    {
        print "No tick count : failed?\n";
        exit;
    }
}
    
    
    
sub choose
{
    my $x = $_[0]; # input 
    my $y; # output

    if(ref $x eq 'ARRAY')
    {
        if(scalar @$x == 2)
        {
            # 2 elements : linear spacedrange
            $y = $x->[0] + ($x->[1] - $x->[0]) * rand();
        }
        elsif(scalar @$x == 1 && ref $x->[0] eq 'ARRAY')
        {
            # 1 element that's an array : log10 spaced range
            my $min = log10($x->[0]->[0]);
            $y = 10.0**($min + (log10($x->[0]->[1]) - $min) * rand());
        }
        else
        {
            # !2 elements : choose one
            my @y = grep {defined $_} @$x;
            $y = $y[rand @y];
        }
    }
    else
    {
        # scalar : just return
        $y = $x;
    }
    
    return $y;
}
