#!/usr/bin/env perl
use strict; # highly recommended
use 5.16.0; # highly recommended
use distribution_functions;
use binary_grid2; # required
use binary_grid::C; # required (uses C backend )
use rob_misc qw/ncpus/;



############################################################
#
# Example script to demonstrate how to use the
# binary_grid2 module.
#
# For full documentation, please see binary_grid2.pdf
# in the doc/ directory of binary_c
#
############################################################

# number of computational threads to launch, usually one
# per CPU
my $nthreads = rob_misc::ncpus();

############################################################
# Binary_c should output data that we can understand here.
# There are two ways to do this:
#
# 1) Put output statements, using Printf, in binary_c's
#    log_every_timestep() function. This requires a rebuild
#    of libbinary_c.so and a reinstall of the binary_grid module
#    every time you change the Printf statement.
#
#
# 2) Put a list of hashes in the C_auto_logging grid option
#
#  $population->set(
#    C_auto_logging => {
#        'MY_STELLAR_DATA' =>
#            [
#             'model.time',
#             'star[0].mass',
#             'model.probability',
#             'model.dt'
#            ]
#    });
#
#  where MY_STELLAR_DATA is the key of the hash {...} and is also
#  the header matched in the parse_data function (below). The list [...]
#  contains the names of the variables to be output, which are all
#  assumed to be in stardata.
#
#  This option does not require a rebuild of libbinary_c.so or a
#  reinstall of binary_grid.
#
#
# 3) Put output statements, using Printf, into the C_logging_code
#    grid option
#
#  $population->set( C_logging_code => ' Printf("...\n"); ');
#
#    You have access to the stardata variable, so you can
#    output everything that is available to log_every_timestep();
#
#  This option does not require a rebuild of libbinary_c.so or a
#  reinstall of binary_grid.
#
############################################################
# make a new stellar population
my $population = binary_grid2->new(
    # options can be given now ...
    metallicity => 0.02, # mass fraction of "metals"
    max_evolution_time => 15000, # Myr
    nthreads=>$nthreads, # number of threads,
    check_args=>1, # check that args exist
    );

# ... or options can be set manually later.
$population->set(
    vb=>1, # turn on verbose logging (can be 0,1,2...)
    return_array_refs=>1, # quicker data parsing mode
    log_args=>1,
    sort_args=>1,
    save_args=>1,
    log_args_dir=>'/tmp',
    #log_dt_secs=>1,
    C_auto_logging => {
        'MY_STELLAR_DATA' =>
            [
             'model.time',
             'star[0].mass',
             'model.probability',
             'model.dt'
            ],
    },
## or enter more complicated code yourself:
#
#    C_logging_code => '
#             Printf("MY_STELLAR_DATA %g %g %g %g\n",
#                    stardata->model.time,
#                    stardata->star[0].mass,
#                    stardata->model.probability,
#                    stardata->model.dt);
#                       ',

    );

# check structures are up to date
exit if($population->check_structure_sizes());

############################################################
# scan command line arguments for extra options
$population->parse_args();

############################################################
#
# Now we set up the grid of stars.
#
# duplicity is 0 for single stars, 1 for binary stars
# and 2 for a mixed population sampled at equal times
my $duplicity = 0;
$population->{_grid_options}{multiplicity} =
    $duplicity == 0 ? 1 : 2;

if($duplicity == 0)
{
    # make a grid of $nstars single stars, log-spaced,
    # with masses between $mmin and $mmax
    my $nstars = 100;
    my $mmin = 0.1;
    my $mmax = 80.0;
    $population->add_grid_variable(
        'name'       => 'lnm1',
        'longname'   =>'Primary mass',
        'range'      =>[log($mmin),log($mmax)],
        'resolution' => $nstars, # just a counter for the grid
        'spacingfunc'=>"const(log($mmin),log($mmax),$nstars)",
        'precode'    =>'$m1=exp($lnm1);',
        'probdist'   =>'Kroupa2001($m1)*$m1',
        'method'     =>'grid',
        'gridtype'   => 'centred',
        );
    $population->set(
        multiplicity=>1
        );
}
elsif($duplicity == 1)
{
    # make a population of binary stars
    my $nres = 20;
    my $resolution = {
        m1 => $nres,
        q => $nres,
        P => $nres,
    };
    my $mmin = 0.1;
    my $mmax = 80.0;
    $population->{_grid_options}{binary} = 1;
    $population->add_grid_variable
        (
        'name'       => 'lnm1',
        'longname'   =>'Primary mass',
        'range'      =>[log($mmin),log($mmax)],
        'resolution' => $resolution->{m1},
        'spacingfunc'=>"const(log($mmin),log($mmax),$resolution->{m1})",
        'precode'    =>'$m1=exp($lnm1);',
        'probdist'   =>'Kroupa2001($m1)*$m1',
        'method'     =>'grid',
        'gridtype'   =>'centred',
        );
    # q=M1/M2 distribution flat in q between 0.1/M1 and 1.0
    $population->add_grid_variable
        (
        'condition'  =>'$self->{_grid_options}{binary}==1',
        'name'       =>'q',
        'longname'   =>'Mass ratio',
        'range'      =>['0.1/$m1',1.0],
        'resolution'=>$resolution->{q},
        'spacingfunc'=>"const(0.1/\$m1,1.0,$resolution->{q})",
        'precode'     =>'$m2=$q*$m1;',
        'probdist'   =>'
flatsections($q,[
  {min=>0.1/$m1,max=>0.8,height=>1.0},
  {min=>0.8,max=>1.0,height=>1.0},
         ])',
        'method'     => 'grid',
        'gridtype'   => 'centred',
        );
     # orbital period Duquennoy and Mayor 1991 distribution
    my $Prange = [-2.0,12.0];
    $population->add_grid_variable
         (
          'name'       =>'logper',
          'longname'   =>'log(Orbital_Period)',
          'range'      =>$Prange,
          'resolution' =>$resolution->{P},
          'spacingfunc'=>"const($Prange->[0],$Prange->[1],$resolution->{P})",
          'precode'=>'
$per = 10.0 ** $logper;
my $eccentricity = 0.0;
$sep = calc_sep_from_period($m1,$m2,$per) if(defined $m1 && defined $m2);
',
          'probdist'=>"gaussian(\$logper,4.8,2.3,$Prange->[0],$Prange->[1])",
          'method'     =>'grid',
          'gridtype'   => 'centred',
         );
}
else
{
    # Make a mixed population of single and binary stars
    # with equal sampling in equal times.
    my $dt = 100.0;
    $population->set(
        time_adaptive_mass_grid_step => $dt,
        );
    my $sampling_factor = 0.5; # deliberate oversampling (<0.5)
    my $time_adaptive_options = {
        max_evolution_time =>
            $population->{_bse_options}{max_evolution_time},
        stellar_lifetime_table_nm=>1000,
        time_adaptive_mass_grid_log10_time=>0,
        time_adaptive_mass_grid_step=>$dt,
        time_adaptive_mass_grid_nlow_mass_stars=>10,
        nthreads           =>$nthreads,
        thread_sleep       =>1,
        mmin               =>0.1,
        mmax               =>80.0,
        mass_grid_log10_time=>0,
        mass_grid_step      =>$dt*$sampling_factor,
        extra_flash_resolution=>0, # 1 = broken?
        mass_grid_nlow_mass_stars=>10,
        debugging_output_directory=>'/tmp',
        max_delta_m         =>1.0,
        savegrid            =>1,
        vb                  =>0,
        metallicity=>$population->{_bse_options}{metallicity},
        agbzoom             =>0,
    };
    distribution_functions::bastard_distribution(
        $population, {
            mmin         =>0.1,
            mmax         =>80.0,
            m2min        =>0.1,
            nm2          =>10,
            nper         =>10,
            qmin         =>0.0,
            qmax         =>1.0,
            necc         =>undef,
            useecc       =>undef,
            agbzoom      =>0,
            time_adaptive=>$time_adaptive_options,
        });
}

############################################################
# link population to custom data parser function
$population->set(
    parse_bse_function_pointer => \&main::parse_data
    );

my %init = $population->initial_abundance_hash('Karakas2002',0.02);
my %isotope_hash = $population->isotope_hash();
my @isotope_list = $population->isotope_list();
my %nuclear_mass_hash = $population->nuclear_mass_hash();
my @nuclear_mass_list = $population->nuclear_mass_list();
my @sources = $population->source_list();
my @ensemble = $population->ensemble_list();

# you can use Data::Dumper to see the contents
# of the above lists and hashes
if(0){
    print Data::Dumper->Dump([
        #\%init,
        #\%isotope_hash,
        #\@isotope_list,
        #\%nuclear_mass_hash,
        \@nuclear_mass_list,
        #\@sources,
        #\@ensemble
                         ]);
}

# uncomment this to show version information
#print $population->evcode_version_string();

# uncomment this to show the evcode's default args list
#print join("\n",@{$population->evcode_args_list()});

# evolution the stellar population (this takes some time)
$population->evolve();

# output the data
output($population);

# done : exit
exit;

############################################################
# subroutines
############################################################

sub parse_data
{
    my ($population, $results) = @_;

    my $progenitor = $population->progenitor();

    # get initial mass and probability for the initial mass function
    my $progenitor_initial_mass =
        $progenitor->{m1} +
        ($duplicity ? $progenitor->{m2} : 0.0);

    my $progenitor_probability = $progenitor->{prob};

    # bin initial mass
    my $binned_initial_mass = $population->rebin($progenitor_initial_mass,1.0);

    # initial mass function
    $results->{initial_mass_function}->{$binned_initial_mass} +=
        $progenitor_probability;

    while(1)
    {
        # subsequent calls to tbse_line contain
        # (references to) arrays of data
        my $la = $population->tbse_line();

        #print "DATA @$la\n";

        # first element is the "header" line
        my $header = shift @$la;

        # break out of the loop if this is 'fin'
        last if ($header eq 'fin');

        # check if $header matches one of your
        # expected data lines, if so, act
        if($header eq 'MY_STELLAR_DATA')
        {
            #print "GOT @$la\n";

            # matched MY_STELLAR_DATA header
            #
            # get time, mass, probability etc. as specified above
            #
            # (note that $probability is possibly not the same as
            #  the progenitor's probability!)
            my $time = $la->[0];
            my $mass = $la->[1];
            my $probability = $la->[2];
            my $timestep = $la->[3];

            # bin mass to nearest 1.0 Msun
            $mass = $population->rebin($mass, 1.0);

            # add up the mass distribution
            $results->{mass_distribution}->{$mass} += $probability * $timestep;
        }
    }
}

############################################################

sub output
{
    my ($population) = @_;

    # $results is a hash reference containing
    # the results that were added up in parse_data()
    my $results = $population->results;

    if(0)
    {
        # show results hash in full
        print "############################################################\n";
        print Data::Dumper::Dumper($results);
        print "############################################################\n";
    }

    # output the initial mass function
    foreach my $mass (sort {$a<=>$b} keys %{$results->{initial_mass_function}})
    {
        printf "IMF %g %g\n",$mass,$results->{initial_mass_function}->{$mass};
    }

    # output the mass distribution
    foreach my $mass (sort {$a<=>$b} keys %{$results->{mass_distribution}})
    {
        printf "MASS %g %g\n",$mass,$results->{mass_distribution}->{$mass};
    }
}
