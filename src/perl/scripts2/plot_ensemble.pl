#!/usr/bin/env perl
use strict;
use 5.16.0;
use Carp qw(confess);
use File::Path qw(make_path);
use File::Basename;
use rob_misc;
use Data::Dumper;
use subs qw(vprint vsay);
use List::Util qw/max min/;
use List::MoreUtils qw(uniq);
use File::Temp qw(tempdir);

# given a set of directories passed in, make plots based on the ensemble
# data

# TODO filter cumulate, rebin

my $help="
    Usage :

    plot_ensemble.pl <datadirs> filter=... filter=... <options>

    where the <datadirs> are made by make_population_ensemble.pl

  Filters:

    filter=xyz where xyz is

    * datatypeXYZ
    XYZ is the datatype label, e.g. SNII, BH
    You can combine them e.g. filter=datatypeSNII+BH

    xyz can be an element or datatype label e.g. filter=SNIbc
    If no elements or datatypes are specified, we try to plot them all,
    you can then choose datatypes to be omitted with e.g. filter=\!Li7

    * rebin:<opts>
    This pipes data through yieldset_rebin.pl to merge time bins.
    Typical usage: filter=rebin:modulo=10:tstart=10000
    where modulo is the number of old bins to make a new bin, and this
    is applied after tstart Myr.

    * cumulate
    with filter=cumulate you integrate the yields instead of plotting
    the time derivative.

  Options:
    You can change any of the options given in the options hash
    on the command line. See %options for details.

    ";

# options (can be overridden by x=y on command line)
my %options=(vb=>1, # verbosity

             # filters
             filters=>[],

             # (gnu)plot options
             run_gnuplot=>1,
             #gp_terminal=>'postscript enhanced landscape size 29.7cm,21.0cm',
             #gp_terminal=>'pdfcairo enhanced size 29.7cm,21.0cm',
             gp_terminal=>'pdfcairo enhanced size 35cm,21.0cm',
             gp_colour=>'colour',
             gp_fortalk=>0,
             gp_font=>'Helvetica',
             gp_fontsize=>22,
             gp_fontscale=>1.0,
             gp_solid=>'',
             gp_linewidth=>6,
             gp_dashlength=>4,
             gp_pltfile=>'ensemble.plt',
             gp_outfile=>'ensemble.pdf',
             gp_with=>'lp ps 0.86 lw 1',
             gp_key=>1,
             gp_key_location=>'top right outside samplen 0.8 opaque font "Helvetica,14"',
             gp_linetitle_fontsize=>14,
             gp_logscale=>{'x'=>1,'y'=>1,'x2'=>0,'y2'=>0},
             gp_colours=>['0000ff','007f00','ff0000','00bfbf','bf00bf','bfbf00','3f3f3f','bf3f3f','bb9900','3f3fbf','bfbfbf','00ff00','c1912b','89a038','5691ea','ff1999','e0bfba','197c77','a856a5','fc683a'],
             gp_colour_by=>'datatype', # can be : order, dir, datatype
             gp_points_by=>'dir', # can be : order, dir, datatype
             gp_title=>1, # 1 = title, 0 = no title
             gp_key_width=>0,
             gp_offsets=>'0.1,0.1,0,0',
             gp_margins=>{
                 l=>'screen 0.1',
                 b=>undef,
                 t=>undef,
                 r=>undef
             },

             line_end_labels=>0, # put data at the end of the lines, instead of in the gnuplot line titles

             yield_sigfigs=>2,

             skip_zerocurves=>1, # if 1 skip curves which are just zero

             xlow=>'*',
             xhigh=>'*',

             ylow=>'*',
             yhigh=>'*',

             overplot=>0,

             # data directories
             datadirs=>[],

             # other
             tinyyield=>1e-50,

             # threshold : if yield < this fraction, ignore
             threshold=>0.0001,

             # if maxtime is undef, it should be ignored
             maxtime=>undef,

             # if times are logged, set this to 1
             ensemble_logtimes=>0,
    );

# tmp dir
$options{tmpdir} //= tempdir(CLEANUP=>1,
                             DIR=>(-d '/var/tmp' ? '/var/tmp':'/tmp'));

my @infotable;

parse_args();

do_work();

#output_infotable();

exit;


sub do_work
{
    # do the work to make the plots
    make_path($options{tmpdir});

    # first, find all the data types in each dir,
    # and make a global list of data types
    my %info;
    my %datatypes;
    foreach my $datadir (@{$options{datadirs}})
    {
        $info{$datadir} = get_info($datadir);
        map
        {
            $datatypes{$_}++;
        }keys %{$info{$datadir}{column_hash}};
        $options{ensemble_logtimes} = $info{$datadir}{ensemble_logtimes} // $info{$datadir}{yields_logtimes};
    }

    # make global list of datatypes
    my @datatypes = sort keys %datatypes;
    print "Found datatypes : @datatypes\n";

    # open gnuplot file
    my $gpfile = $options{run_gnuplot} ? $options{tmpdir}.'/'.$options{gp_pltfile} : '/dev/null';
    open(my $gp,'>',$gpfile)||confess("cannot open $gpfile for output");
    print {$gp} "set terminal $options{gp_terminal} $options{gp_colour} ";

    # gnuplot labels file
    my $labelscount=0;
    my $labelsfile = $options{run_gnuplot} ? $options{tmpdir}.'/'.$options{gp_pltfile}.'labels'.$labelscount : '/dev/null';
    open(my $labels_fp,'>',$labelsfile)||confess("cannot open labels file");

    my $postscript = $options{gp_terminal}=~/(postscript|eps)/;

    print {$gp} $postscript ? " \"$options{gp_font}\" $options{gp_fontsize} "
        : " font \"$options{gp_font}\" fontscale $options{gp_fontscale} ";

    print {$gp} " $options{gp_solid} linewidth $options{gp_linewidth} dashlength $options{gp_dashlength}
    set output \"$options{gp_outfile}\"
set auto fix
set offsets $options{gp_offsets}
set zero 0.0
set xlabel \"Time / Myr\"
set format y ",'"%g"',"
set xrange\[$options{xlow}:$options{xhigh}\]
set yrange\[$options{ylow}:$options{yhigh}\]
load \"$labelsfile\"
\n\n";
    foreach my $margin (keys %{$options{gp_margins}})
    {
        if(defined $options{gp_margins}{$margin})
        {
            print {$gp} 'set '.$margin."margin $options{gp_margins}{$margin}\n";
        }
    }


    # override key width if required
    if(defined $options{gp_key_width} &&
       $options{gp_key_location}!~s/width -?\d+/width $options{gp_key_width}/)
    {
        $options{gp_key_location}.=" width $options{gp_key_width} ";
    }

    # key location
    say {$gp} $options{gp_key} ? "set key $options{gp_key_location}" : 'unset key';

    # logscales
    foreach my $axis (keys %{$options{gp_logscale}})
    {
        say {$gp} ($options{gp_logscale}{$axis}==0 ? 'un' : '') . 'set logscale '.$axis;
        if($axis=~/y/)
        {
            say {$gp} 'set format '.$axis.' '.
                ($options{gp_logscale}{$axis}==0 ? '"%g"' : '"10^{%L}"');
        }
    }

    # make a list of datatype names to plot
    my %datatypes_toplot;
    my %datatypes_tonotplot;
    my $regexp_filter=0;

    # regexp filters
    foreach my $filter (grep { /regexp\:/ } @{$options{filters}})
    {
        print "Found regexp filter $filter\n";
        if($filter=~/regexp:(\S+)/)
        {
            my $regexp = $1;
            foreach my $datatype (@datatypes)
            {
                if($datatype=~/$regexp/)
                {
                    print "ADD $datatype\n";
                    $datatypes_toplot{$datatype} = 1;
                }
            }
        }
    }

    foreach my $datatype (grep { /^filter/ && !-d $_} @{$options{filters}})
    {
        # TODO : better filtering here?
        $datatypes_toplot{$datatype} = 1;
        print "PLOT $datatype\n";
    }

    # if we're not asked to plot any datatypes, plot all datatypes
    if(!scalar keys %datatypes_toplot)
    {
        foreach my $datatype (@datatypes)
        {
            $datatypes_toplot{$datatype} = 1;
        }
    }

    # make sure datatypes chosen for non-plotting are not plotted
    foreach my $datatype (keys %datatypes_tonotplot)
    {
        delete $datatypes_toplot{$datatype};
    }

    # hence the final list of datatypes (TODO needs better sorting)
    my @datatypes_toplot = sort keys %datatypes_toplot;

    # make plots : loop over all binary_c datatypes ... not all will be available
    # so we have to find those that are, and also those with > 0 yield
    my $plotcomma='plot';
    my $colour_count=-1;
    my $points_count=-1;

    foreach my $datatype (@datatypes_toplot) # datatype strings
    {
    # make new plot, or append if overplot is set
    if(!$options{overplot})
    {
        $plotcomma='plot';

        if($options{gp_title})
        {
                my $t = $datatype;
                $t=~s/_/ /g;
                printf {$gp} "set title \"$t\"\n";
        }

        # reset counts
        $colour_count=-1;
        $points_count=-1;

            # reset labels
            close $labels_fp;
            $labelscount++;
            $labelsfile = $options{run_gnuplot} ? $options{tmpdir}.'/'.$options{gp_pltfile}.'labels'.$labelscount : '/dev/null';
            open($labels_fp,'>',$labelsfile)||confess("cannot open labels file");
            print {$gp} "unset label\nload \"$labelsfile\"\n";
    }
    if($options{gp_colour_by} eq 'datatype')
    {
        $colour_count++;
    }
    if($options{gp_points_by} eq 'datatype')
    {
        $points_count++;
    }

    state $datadircount;
    $datadircount=0;

    foreach my $datadir (@{$options{datadirs}})
    {
        # get datatype number and column number (used lots)
        my $ncol = $info{$datadir}{column_hash}{$datatype};

        my $max = $info{$datadir}->{max}->[$ncol-1];
        my $min = $info{$datadir}->{min}->[$ncol-1];
        my $range = $info{$datadir}->{range}->[$ncol-1];

        print "Datadir $datadir, datatype \"$datatype\", column $info{$datadir}{column_hash}{$datatype} range $range from max = $max, min = $min (col $ncol)\n";
        next if($range==0.0);

        my @datadir_infotable;
        $datadircount++;

        # check we have this datatype
        next if (!defined $info{$datadir}{column_hash}{$datatype});

        $colour_count++ if($options{gp_colour_by} eq 'dir');
        $points_count++ if($options{gp_points_by} eq 'dir');

        # make a local list of filters
        my @filters = @{$options{filters}};

        # if datatype changes, up count
        state $prev_datatype;
        state $datatype_count=-1;

        if(!defined $prev_datatype || $datatype ne $prev_datatype)
        {
            $prev_datatype=$datatype;
            $datatype_count++;
        }

        # construct line titles
        my $t;
        $t .= gptitle($info{$datadir});

        # reformat exponential notation
        $t =~ s/e([\+-])0*(\d+)/\{\/Symbol \\264\}10^\{\/*0.8 $1$2\}/g;

        $colour_count++ if($options{gp_colour_by} eq 'order');
        $points_count++ if($options{gp_points_by} eq 'order');

        my $lineopts;
        if($options{gp_with}=~/^l/)
        {
            # lines options
            $lineopts = ' lt '.($datatype_count+1);
        }
        if($options{gp_with}=~/^(lp|points|linespoints)/)
        {
            # lines-points options
            $lineopts .= ' pt '.$datadircount;
        }
        $lineopts .= ' ';

        # line colour
        $lineopts .= " linecolor rgbcolor \"\#".
            $options{gp_colours}[($colour_count)%$#{$options{gp_colours}}].'" ';


        # check for inline pipe filters
        my $maxtimefilter = defined $options{maxtime} ? " maxtime=$options{maxtime} " : '';

        # first pipe 'filter' is just the data file
        my @pipe_filters =  '< cat '.$info{$datadir}{ensemble_file};

        #("< add_yieldsets.pl $maxtimefilter ".join(' ',(map {$_=$info{$datadir}{sourcedatafilepaths}[$_]} @subsources)));

        foreach my $filter (@{$options{filters}})
        {
            if($filter=~/rebin(\S+)$/)
            {
                my $opts=$1;
                print "Rebin filter $opts\n";
                push(@pipe_filters,"yieldset_rebin.pl $opts");
            }
            elsif($filter=~/cumulate/)
            {
                push(@pipe_filters,'cumulate_yields.pl');
            }
        }

        # construct the plotfile string
        my $plotfile = join(' | ',@pipe_filters);
        print "Constructed plotfile = \"$plotfile\"\n";

        state $prevt;

        # remove repeated information
        if(defined $prevt)
        {
            my @y;

            my $prevt_strip = $prevt;
            my $t_strip = $t;

            map
            {
                s/\{([^\}]+)\s+/{$1+++/g;
            }($prevt_strip,$t_strip);

            my @x=split(/\s+/,$prevt_strip);
            @y=split(/\s+/,$t_strip);

            map
            {
                foreach my $x (@x)
                {
                    if($x eq $_)
                    {
                        # match! delete!
                        $_='';
                    }
                }
            }@y;

            $prevt = $t;
            $t=join(' ',@y);
            $t=~s/\+\+\+/ /g;

        }
        else
        {
            $prevt = $t;
        }

        if($info{$datadir}->{totals}->[$ncol-1] > 1e-100)
        {
            my $n_non_zero = $info{$datadir}->{n_non_zero}->[$ncol-1];
            my $n_distinct_non_zero = $info{$datadir}->{n_distinct_non_zero_non_zero}->[$ncol-1];
            my $ycol;
            if($options{gp_logscale}{y} &&
               $n_distinct_non_zero<2)
            {
                $ycol = '($'.$ncol.'>0?$'.$ncol.':NaN)';
            }
            else
            {
                $ycol = $ncol;
            }

            my $xcol = $options{ensemble_logtimes} ? '(10.0**$1)' : '1';

            print {$gp} "$plotcomma '$plotfile' u $xcol:$ycol with $options{gp_with} $lineopts ";

            if($options{line_end_labels})
            {
                my $x = "set label \"\{/=$options{gp_linetitle_fontsize} $t\}\" at $info{$datadir}{maxtime}\n";
                print "$x\n";
                print {$gp} 'notitle ';
                print {$labels_fp} $x;
            }
            else
            {
                print {$gp} "title \"$t\" "
            }

            $plotcomma =',';
        }

        push(@datadir_infotable,
             sprintf "Datatype %s",
             $datatype);
        push(@infotable,@datadir_infotable);
    } # datadir loop
    say {$gp} '' if(!$options{overplot});
    } # datatype loop


    if($options{run_gnuplot})
    {
        print "Running gnuplot\n";
        say {$gp} '';
        close $gp;
        close $labels_fp;

        say `gnuplot $options{tmpdir}/$options{gp_pltfile}`;
        if(-s $options{gp_outfile})
        {
            say "See $options{gp_outfile} (size ",-s $options{gp_outfile},' bytes)';
    }
    else
    {
        say "No output file generated: check that the data is ok, the directories are correct, and that your filters are not too restrictive.";
    }
    }

}

sub get_info
{
    # return an information hash (reference) about a given directory
    my $dir = shift;
    my $ensemble_file = find_ensemble_file($dir);
    my $info;
    $info->{ensemble_file}=$ensemble_file;
    $info->{column_hash} = {};
    $info->{datatypes_array} = [];
    $info->{totals} = [];
    $info->{range} = [];


    # determine if times should be logged :
    # this is always available in the yields_vs_time files,
    # but may not be in the ensemble file. Try yields_vs_time first.
    my (@yieldfiles) = grep {
        -e -f -s $_
    }(
        'info',
        'yields_vs_time.z=0.02.binary.source0.dat',
        'yields_vs_time.z=0.02.single.source0.dat',
        'yields_vs_time.z=0.02.binary.dat',
        'yields_vs_time.z=0.02.single.dat',
    );

    if($#yieldfiles > -1)
    {
        open(my $yf,'<',$yieldfiles[0])||die("cannot open $yieldfiles[0]\n");
        if($yf)
        {
            while(my $l = <$yf>)
            {
                #print $l;
                if($l=~/^\# _bse_options (ensemble|yields)_logtimes = (\d)/)
                {
                    $info->{$1.'_logtimes'} = $1;
                    close $yf;
                }
            }
            close $yf;
        }
    }

    if(defined $ensemble_file)
    {
        open(FP, '<', $ensemble_file) || confess ("cannot open $ensemble_file");
        while(my $l=<FP>)
        {
            if($l=~/^# Column (\d+) is (\S+)/)
            {
                # meta data
                $info->{column_hash}->{$2} = $1;
                $info->{datatypes_array}->[$1] = $2;
            }
            elsif($l=~/^\#/)
            {
                if($l=~/^\# _bse_options (ensemble|yields)_logtimes = (\d)/)
                {
                    $info->{$1.'_logtimes'} = $1;
                }
                # ignore other comments
            }
            else
            {
                # save up totals for each column
                my @x = split(' ',$l);
                for(my $i=0;$i<=$#x;$i++)
                {
                    $info->{totals}->[$i] += $x[$i];

                    $info->{max}->[$i] = max($info->{max}->[$i],$x[$i])
                        // $x[$i];
                    $info->{min}->[$i] = min($info->{min}->[$i],$x[$i])
                        // $x[$i];
                    $info->{range}->[$i] =
                        $info->{max}->[$i] - $info->{min}->[$i];

                    $info->{n}->[$i] ++;
                    $info->{n_non_zero}->[$i] ++ if(abs($x[$i]) > 0.0);


                    if($x[$i]!=0.0 &&
                       (!defined $info->{'last'}->[$i] ||
                        $x[$i] != $info->{'last'}->[$i]))
                    {
                        $info->{n_distinct_non_zero}->[$i] ++;
                        $info->{'last'}->[$i] = $x[$i];
                    }

                    if(!defined $info->{'last'}->[$i] ||
                       $x[$i] != $info->{'last'}->[$i])
                    {
                        $info->{n_distinct}->[$i] ++;
                        $info->{'last'}->[$i] = $x[$i];
                    }
                }
            }
        }
        close FP;
    }
    else
    {
        say "Unable to find ensemble data file in directory '$dir'";
    }

    return $info;
}

if($options{run_gnuplot})
{
    # run gnuplot to make the plot
    say `gnuplot $options{tmpdir}/ensemble.plt 2>\&1`;
    say "See $options{gp_outfile}\n";
}

############################################################

sub format_title
{
    # title for each gnuplot line
    my $dir=$_[0];
    my $fontsize=$_[1];
    my $t=$dir;
    if(defined $fontsize)
    {
        $t="{/*$fontsize $t}";
    }
    # most words > 3 chars are not acronyms
    $t=~s/([A-Z])([A-Z]{3,})/$1\L$2/g;

    # but some are....
    foreach my $string ('Cowd','Hewd','Tpagb','Eagb','Bhbh','Lmms','Nsbh','Nsns',
                        'Wdns','Wdwd','Wdbh')
    {
        $t=~s/$string/\U$string/g;
    }

    # and some are not
    $t=~s/PRE/pre/g;
    $t=~s/GK PER/GK Per/g;
    $t=~s/HOT/Hot/g;
    $t=~s/LUM/Luminosity/g;
    $t=~s/_OF_/_of_/g;
    $t=~s/_IN_/_in_/g;


    # common changes
    $t=~s!Z=!{/Italic Z}=!g;
    $t=~s!binary=0!single!g;
    $t=~s!binary=1!binary!g;

    # subscripts
    my $sf=0.8;
    $t=~s!_(ce)!__{/*$sf CE}!g;
    $t=~s!_(ionisation)!__{/*$sf ion}!g;

    # greek
    $t=~s!dt!{/Symbol d}t!g;
    $t=~s!alpha!{/Symbol a}!g;
    $t=~s!lambda!{/Symbol l}!g;
    $t=~s!epsnov!{/Symbol e}__{/*$sf nov}!g;

    # underscores > spaces
    $t=~s/__/UNDERSCORE/g;
    $t=~s/_/ /g;
    $t=~s/UNDERSCORE/_/g;

    return $t;
}

sub find_ensemble_file
{
    # given a directory name, construct possible ensemble files
    # and return the first good one found, otherwise return
    # undef
    my $dir=$_[0];
    my $binary=($dir=~/binary=([01])/)[0];
    my $Z=($dir=~/Z=(0\.\d+)/)[0];

    foreach my $file ('ensemble.z='.$Z.'.binary='.$binary.'.dat')
    {
        my $f=$dir.'/'.$file;
        $f=~s!/+!/!;
        if(-e -f -s $f)
        {
            return $f;
        }
    }

    # nothing found, try generic search
    return (grep
            {s/\n$// && /^ensemble/ && /\.dat$/}
            `ls $dir`)[0];

    return undef;
}

sub parse_args
{
    # parse cmd line args
    while(my $arg=shift @ARGV)
    {
        if($arg=~/^-?h(?:elp)?/)
        {
            print $help;
            exit;
        }
        if($arg eq 'vb' || $arg eq 'verbose')
        {
            # set verbosity on
            $options{vb}=1;
        }
        elsif($arg=~/(\S+)=(.+)/ && exists $options{$1})
        {
            # override one of the default options
            $options{$1}=$2;
        }
        elsif(-d $arg)
        {
            # arg is a directory presumably containing ensemble data
            push(@{$options{datadirs}},$arg);
        }
        elsif($arg=~/(?:filter=)?(\S+)/)
        {
            # arg is a filter, save the details
            my $filter=$1;
            push(@{$options{filters}},$1);
        }
        else
        {
            # er? unknown!
            confess "What is arg $arg? It's not a yield directory".-d $arg." or recongised as a filter.";
        }
    }
}


sub gptitle
{
    # clean up title for gnuplotting
    my $info=$_[0];
    my $t=$$info{datadir}; # start with the data directory
    $t=~s/\/$//;

    $t=~s/_/ /g;

    my $single = $t=~s/binary=0/single/;
    $t=~s/binary=1/binary/;

    if($single)
    {
    # single stars : remove binary-specific stuff
    $t=~s/lambda\S+//;
    $t=~s/alpha\S+//;
    $t=~s/epsilon\S+//;
    }

    my %greek=('alpha'=>'a',
           'omega'=>'o',
           'Omega'=>'O',
           'lambda'=>'l',
           'Lambda'=>'L',
    );
    foreach my $greek (keys %greek)
    {
    $t=~s/$greek(_?)([a-zA-Z0-9]+)?/\{\/Symbol $greek{$greek}\}UNDER\{$2\}/g;
    }
    $t=~s/yields_vs_time\.//;
    $t=~s/binary=\d//g;
    $t=~s/z=/{\/Italic Z}=/;
    $t=~s/\.dat$//;
    $t=~s/_/ /g;

    if($options{gp_fortalk})
    {
    # remove details
    $t=~s/dt=\S+//;
    $t=~s/Z=\S+//;
    #$t=~s/single//;
    #$t=~s/binary//;
    $t=~s/THORN ZYTKOW/TZ/;
    $t=~s/CHAND/M_{Ch}/g;
    $t=~s/Coel/Merge/g;
    }
    else
    {
    $t=~s/dt/\{\/Symbol d\}t/g;
    }

    $t=~s/\.([a-zA-Z])/ $1/g;
    $t=~s/source(\d+)/$$info{sources}[$1]/g;
    $t=~s/UNDER/__/g;
    $t=~s/epsnov/\{\/Symbol e\}_\{nov\}/g;
    $t=~s/ionisation/ion/g;

    $t=~s/\s+/ /g;

    return $t;
}
