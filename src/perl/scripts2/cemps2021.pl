#!/usr/bin/env perl
use strict; # highly recommended
use 5.16.0; # highly recommended
use distribution_functions;
use binary_grid2; # required
use binary_grid::C; # required (uses C backend )
use POSIX qw/strftime log10/;
use File::Basename;
use JSON::PP;
use JSON::Parse qw/parse_json json_file_to_perl/;
use Sort::Key qw/nsort/;
use Sort::Naturally qw/ncmp/;
use Sys::Hostname;
use Term::ANSIColor;
use rob_misc qw/dumpfile force_numeric_elements MAX mkdirhier ncpus slurp/;

############################################################
#
# CEMP stars 2021
#
############################################################

# number of computational threads to launch, usually one
# per CPU
my $nthreads = rob_misc::ncpus();
my $args = "@ARGV"; # used often
my $date_today = strftime '%d%m%Y', localtime;
my $voldisk = $ENV{VOLDISK} // $ENV{HOME};
my $outdir = ($args=~/outdir=(\S+)/)[0] //
    $voldisk.'/data/pops/population_ensemble-EMP-'.$date_today;
make_outdir();

############################################################
# make a new stellar population
my $population = binary_grid2->new(
    # options can be given now ...
    metallicity => 0.0001, # mass fraction of "metals"
    max_evolution_time => 15000, # Myr
    nthreads=>$nthreads, # number of threads,
    check_args=>0, # check that args exist
    vb=>1, # turn on verbose logging (can be 0,1,2...)
    return_array_refs=>1, # quicker data parsing mode
    log_args=>1,
    sort_args=>1,
    save_args=>1,
    log_args_dir=>'/tmp',
    # turn on (C)EMP filters
    ensemble=>1,
    ensemble_defer=>1,
    ensemble_filters_off=>1,
    ensemble_filter_EMP=>1,
    ensemble_dt=>1e3, # 1Gyr resolution
    CEMP_cfe_minimum=>0.7,
    EMP_logg_maximum=>4.0,
    EMP_minimum_age=>10e3,
    minimum_envelope_mass_for_third_dredgeup=>0.0,
    delta_mcmin=>-0.1,
    lambda_min=>0.8,
    WRLOF_method=>0,
    Bondi_Hoyle_accretion_factor=>1.5,
    CRAP_parameter=>0,
    no_thermohaline_mixing=>0,
    );

# check structures are up to date
exit if($population->check_structure_sizes());

############################################################
# scan command line arguments for extra options
$population->parse_args();

############################################################
#
# Now we set up the grid of stars.
#
# duplicity is 0 for single stars, 1 for binary stars
# and 2 for a mixed population sampled at equal times
my $duplicity = 1;
$population->{_grid_options}{multiplicity} =
    $duplicity == 0 ? 1 : 2;

# mass ranges
my $mmin = 0.4;
my $mmax = 6.0;
my $m2min = 0.1;
my $nstars = 400; # M1 resolution
my $nres = 100; # M2 (q), period resolution

if($duplicity == 0)
{
    # make a grid of $nstars single stars, log-spaced,
    # with masses between $mmin and $mmax
    $population->add_grid_variable(
        'name'       => 'lnm1',
        'longname'   =>'Primary mass',
        'range'      =>[log($mmin),log($mmax)],
        'resolution' => $nstars, # just a counter for the grid
        'spacingfunc'=>"const(log($mmin),log($mmax),$nstars)",
        'precode'    =>'$m1=exp($lnm1);',
        'probdist'   =>'Kroupa2001($m1)*$m1',
        'method'     =>'grid',
        'gridtype'   => 'centred',
        );
    $population->set(
        multiplicity=>1
        );
}
elsif($duplicity == 1)
{
    # make a population of binary stars
    my $resolution = {
        m1 => $nstars,
        q => $nres,
        P => $nres,
    };
    $population->{_grid_options}{binary} = 1;
    $population->add_grid_variable
        (
        'name'       => 'lnm1',
        'longname'   =>'Primary mass',
        'range'      =>[log($mmin),log($mmax)],
        'resolution' => $resolution->{m1},
        'spacingfunc'=>"const(log($mmin),log($mmax),$resolution->{m1})",
        'precode'    =>'$m1=exp($lnm1);',
        'probdist'   =>'Kroupa2001($m1)*$m1',
        'method'     =>'grid',
        'gridtype'   =>'centred',
        );
    # q=M1/M2 distribution flat in q between 0.1/M1 and 1.0
    $population->add_grid_variable
        (
        'condition'  =>'$self->{_grid_options}{binary}==1',
        'name'       =>'q',
        'longname'   =>'Mass ratio',
        'range'      =>["$m2min/\$m1",1.0],
        'resolution'=>$resolution->{q},
        'spacingfunc'=>"const($m2min/\$m1,1.0,$resolution->{q})",
        'precode'     =>'$m2=$q*$m1;',
        'probdist'   =>'
flatsections($q,[
  {min=>'.$m2min.'/$m1,max=>0.8,height=>1.0},
  {min=>0.8,max=>1.0,height=>1.0},
         ])',
        'method'     => 'grid',
        'gridtype'   => 'centred',
        );
     # orbital period Duquennoy and Mayor 1991 distribution
    my $Prange = [-2.0,12.0];
    $population->add_grid_variable
         (
          'name'       =>'logper',
          'longname'   =>'log(Orbital_Period)',
          'range'      =>$Prange,
          'resolution' =>$resolution->{P},
          'spacingfunc'=>"const($Prange->[0],$Prange->[1],$resolution->{P})",
          'precode'=>'
$per = 10.0 ** $logper;
my $eccentricity = 0.0;
$sep = calc_sep_from_period($m1,$m2,$per) if(defined $m1 && defined $m2);
',
          'probdist'=>"gaussian(\$logper,4.8,2.3,$Prange->[0],$Prange->[1])",
          'method'     =>'grid',
          'gridtype'   => 'centred',
         );
}
else
{
    die("Duplicity is duplicitous : error! should be 0 or 1 \n");
}

############################################################
# link population to custom data parser function
$population->set(
    parse_bse_function_pointer => \&main::parse_data
    );

my %init = $population->initial_abundance_hash('Karakas2002',0.02);
my %isotope_hash = $population->isotope_hash();
my @isotope_list = $population->isotope_list();
my %nuclear_mass_hash = $population->nuclear_mass_hash();
my @nuclear_mass_list = $population->nuclear_mass_list();
my @sources = $population->source_list();
my @ensemble = $population->ensemble_list();

# you can use Data::Dumper to see the contents
# of the above lists and hashes
if(0){
    print Data::Dumper->Dump([
        #\%init,
        #\%isotope_hash,
        #\@isotope_list,
        #\%nuclear_mass_hash,
        \@nuclear_mass_list,
        #\@sources,
        #\@ensemble
                         ]);
}

# uncomment this to show version information
#print $population->evcode_version_string();

# uncomment this to show the evcode's default args list
#print join("\n",@{$population->evcode_args_list()});

# evolution the stellar population (this takes some time)
$population->evolve();

# output the data
output($population);

# done : exit
exit;

############################################################
# subroutines
############################################################

sub parse_data
{
    my ($population,$h) = @_;
    while(1)
    {
        my $linearray = $population->tbse_line();
        my $header = shift @$linearray;
        last if ($header eq 'fin');
        if($header eq 'ENSEMBLE_JSON')
        {
            $population->add_ensemble($h,$linearray);
            print "Added ensemble : pop=$population, h=$h\n";
        }
    }
}

############################################################

sub output
{
    my ($population) = @_;

    # $results is a hash reference containing
    # the results that were added up in parse_data()
    my $results = $population->results;

    # output ND JSON data
    output_json($population);

}
sub output_json
{
    my ($population) = @_;
    my $h = $population->results;
    print "Output json pop=$population, results=$h\n";

    # output formatted JSON to ensemble.json
    rob_misc::force_numeric_elements($h->{'ensemble'},
                                     {
                                         vb => 0,
                                         modulator => 1.0,
                                         numeric_keyformat => '%g'
                                     }
        );

    # add info hash
    $h->{'ensemble'}->{'info'} = $population->info_hash($population);

    #open(FP,'>','/tmp/test.hash');
    #print FP Data::Dumper::Dumper($h);
    #close FP;

    # make text version of the data, sort numerically, pretty print etc.
    my $json = JSON::PP->new->utf8->pretty->allow_nonref->allow_blessed->allow_unknown->sort_by(
        sub
        {
            Sort::Naturally::ncmp($JSON::PP::a,
                                  $JSON::PP::b)
        })->encode($h);

    # and output
    my $file = "$outdir/ensemble.json";
    open(FP,'>',$file)||die("opening $outdir/ensemble.json failed");
    print FP $json,"\n";
    close FP;
    print "See $file\n";
}

sub make_outdir
{
    # make output directory
    my $d = $outdir . (defined $_[0] ? ('/'.$_[0]) :'');
    if(! -d $d)
    {
        mkdirhier($d);
        print "Made outdir : $d\n";
    }
    return $d;
}
