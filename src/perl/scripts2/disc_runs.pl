#!/usr/bin/env perl

use strict;
use warnings;
use Sys::Hostname qw/hostname/;
use 5.16.0;
use rob_misc;

#############################################################
#
# wrapper script for cbdiscs from comenv
#
# call with args:
#
# launch 
#       launches the scripts
#
# slurm=n
#       set to 1 to launch on slurm
#
# slurm_njobs=n
#       set the number of slurm jobs
#
# force
#       force overwrite of jobs, even if they already have been done
#
#############################################################

my $script = "$ENV{HOME}/progs/stars/binary_c/src/perl/scripts2/cbdiscs.pl vb=1 ";
my $hostname = hostname() // `hostname` // $ENV{HOSTNAME} // $ENV{HOST};
my $launch = "@ARGV"=~/launch/ ? 1 : 0;
my $slurm = ("@ARGV"=~/slurm=(\d+)/)[0] // ($hostname=~/eureka/ ? 1 : 0);
my $force = "@ARGV"=~/force/ ? 1 : 0;
my $offset = ("@ARGV"=~/offset=(\d+)/)[0] // 0;
my $modulo = ("@ARGV"=~/modulo=(\d+)/)[0] // 1;
my $njob = 0;
my $slurm_njobs = ("@ARGV"=~/slurm_njobs=(\d+)/)[0] // 40;

print "Disc run wrapper running on $hostname

Launch jobs? $launch
On slurm? $slurm (slurm_njobs=$slurm_njobs);
\n";

# wrapper script to run many disc populations
my $n = ("@ARGV"=~/\bn=(\d+)/)[0] // 100; # grid resolution
my $outdir=$ENV{HOME}.'/data/cbdiscpop6-'.$n;

# default options
my $defaults = {
    # stellar physics
    minimum_donor_menv_for_comenv => 0.1,
    metallicity=>0.02,
    max_evolution_time=>13700,

    minimum_timestep=>1e-8,
    
    # comenv options
    minimum_donor_menv_for_comenv => 0.1,
    alpha_ce=>1.0,
    lambda_ce=>-1.0,
    lambda_ionisation=>0.1,
    lambda_enthalpy=>0.0,
    comenv_post_eccentricity => 1e-5,

    # AGB Mc,R,L evolution algorithm
    # 0 : compatibility mode : Hurley (if no NUCSYN), Karakas (if NUCSYN)
    # 1 : Hurley 2002 (overshooting)
    # 2 : Karakas 2002 (non-overshooting) 
    #
    # Third dredge up has the extra choice:
    # 3 : Stancliffe (LMC/SMC metallicity only)
    AGB_core_algorithm=>2,
    AGB_radius_algorithm=>2,
    AGB_luminosity_algorithm=>2,
    AGB_3dup_algorithm=>2,

    # CB disc options
    comenv_disc_mass_fraction => 0.01,
    comenv_disc_angmom_fraction => 0.01,
    wind_disc_mass_fraction => 0.0,
    wind_disc_angmom_fraction => 0.0,
    
    disc_timestep_factor=>0.01,
    disc_log => 0,
    disc_n_monte_carlo_guesses => 0,
    
    cbdisc_gamma => 1.4, # monatomic gas = 1.6666666666,
    cbdisc_alpha => 1e-3, # 1e-3
    cbdisc_kappa => 1e-2, # 1e-2
    cbdisc_torquef => 1e-3, # 1e-3
   
    cbdisc_init_dM => 0.0, # 0 means do not adapt initial masses 
    cbdisc_init_dJdM => 0.5, # ignored if cbdisc_init_dM == 0

    cbdisc_minimum_luminosity => 1e-4, # 1e-4
    cbdisc_minimum_mass => 1e-6, # 1e-6
    cbdisc_minimum_fRing => 0.2, # 0.2
    cbdisc_fail_ring_inside_separation => 0, # 0
    cbdisc_max_lifetime => 1e6, # 1e6 y
    
    cbdisc_mass_loss_constant_rate => 0.0, # 0.0
    
    cbdisc_mass_loss_inner_viscous_multiplier => 1.0, # 1
    cbdisc_mass_loss_inner_viscous_accretion_method => 1, # 1
    cbdisc_mass_loss_inner_L2_cross_multiplier => 1.0, # 1
    cbdisc_viscous_L2_coupling => 1, # 1
    cbdisc_mass_loss_ISM_pressure => 3000.0, # 1
    cbdisc_mass_loss_ISM_ram_pressure_multiplier => 1.0, # 1
    cbdisc_mass_loss_FUV_multiplier => 0.0, # 0
    cbdisc_mass_loss_Xray_multiplier => 1.0, # 1
    cbdisc_viscous_photoevaporation_coupling => 1, # 1
    cbdisc_inner_edge_stripping => 1, # 1
    cbdisc_outer_edge_stripping => 1, # 1
    cbdisc_inner_edge_stripping_timescale => 0, # 0=instant, 1=slow, 
    cbdisc_outer_edge_stripping_timescale => 0, # 2=viscous, 3=orbital
    cbdisc_eccentricity_pumping_method => 1, # 1
    cbdisc_resonance_multiplier => 1.0, # 1
    cbdisc_resonance_damping => 1, # 1

};

my %runs;

makerun({}); # defaults

# now vary the parameters
foreach my $comenv_disc_mass_fraction (1e-4,1e-3,0.01,0.1,0.2)
{
    makerun({
        comenv_disc_mass_fraction => $comenv_disc_mass_fraction
            });
}

foreach my $comenv_disc_angmom_fraction (1e-4,1e-3,0.01,0.1,
                                         -1,-2,-3)
{
    makerun({
        comenv_disc_angmom_fraction => $comenv_disc_angmom_fraction
            });
}
   

foreach my $torquef (1e-4,1e-3,1e-2)
{
    makerun({
        cbdisc_torquef=>$torquef
            });
}

foreach my $kappa (0.5e-2,5e-2)
{
    makerun({
        cbdisc_kappa=>$kappa
            });
}

foreach my $alpha (1e-5,1e-4,1e-3,1e-2)
{
    makerun({
        cbdisc_alpha=>$alpha
            });
}

foreach my $mass_loss_inner_viscous_multiplier (1.0,0.1,1e-2,1e-3,0.0)
{
    makerun({
        cbdisc_mass_loss_inner_viscous_multiplier=>$mass_loss_inner_viscous_multiplier
            });
}


foreach my $mass_loss_inner_viscous_accretion_method (0,1,2)
{
    makerun({
        cbdisc_mass_loss_inner_viscous_accretion_method=>$mass_loss_inner_viscous_accretion_method
            });
}


foreach my $mass_loss_Xray_multiplier (1.0,0.1)
{
    makerun({
        cbdisc_mass_loss_Xray_multiplier=>$mass_loss_Xray_multiplier
            });
}

foreach my $P (1e4)
{
    makerun({
        cbdisc_mass_loss_ISM_pressure=>$P
            });
}

foreach my $coupling (0,1)
{
    makerun({
        cbdisc_viscous_photoevaporation_coupling=>$coupling
            });
}

foreach my $resonance_multiplier (0,0.1,1)
{
    makerun({
        cbdisc_resonance_multiplier=>$resonance_multiplier
            });
}

foreach my $cbdisc_resonance_damping (0,1)
{
    makerun({
        cbdisc_resonance_damping=>$cbdisc_resonance_damping
            });
}


foreach my $cbdisc_minimum_fRing (0.2,0.1,1.0)
{
    makerun({
        cbdisc_minimum_fRing=> $cbdisc_minimum_fRing
            });
}
foreach my $comenv_post_eccentricity (1e-3,1e-5,1e-7)
{
    makerun({
        comenv_post_eccentricity => $comenv_post_eccentricity
            });
}

foreach my $metallicity (0.02,0.004,0.0001)
{
    makerun({
        metallicity => $metallicity
            });
}
exit;

sub makerun
{
    my $opts = shift; # hash ref
    my %params = %$defaults;
    my $dir = $outdir.'/';
    my $args;
    my %o = %$opts;

    # loop over opts, make arg string and outdir 
    if(defined $opts && ref $opts eq 'HASH')
    {
        foreach my $k (sort keys %o) 
        {
            my $v = $o{$k};
            
             if(!defined $defaults->{$k} ||
               $defaults->{$k} != $v ||
               $defaults->{$k} ne $v)
            {
                $args .= " $k=$v ";
                $dir .= $k.'='.$v.'_';
            }
        }
    }
    else
    {
        print "Bad opts given (not {})\n";
    }
    $dir=~s/_$//; # remove trailing _
    $dir=~s/cbdisc_//g; # remove cbdisc_ 
    
    # if no extra args, just use defaults
    if(!$args || $args eq '')
    {
        $dir = $outdir.'/defaults';
    }

    $args .= " outdir=$dir n=$n ";

    if(!$runs{$args})
    {
        $runs{$args}=1;
        print "Args : $args\n";
        my $cmd = $script.' '.(
            $slurm ? 
            ('slurm=1 slurm_njobs='.$slurm_njobs) :
            '').' '.$args;
        print " ... CMD: $cmd\n";
        if($launch)
        {
	    #print "CF njob $njob offset $offset modulo $modulo\n";
	    my $boolean = 
		($njob+$offset) % $modulo == 0 ?  1 : 0;
	    #print "Boolean $boolean\n";

            if(!-d $dir || $force)
            {
		if($boolean)
		{
		    print " ... Launch! ".(($force && -d $dir) ? '(forced)' : '')."\n";
		    runcmd($cmd,'screen');
		}
		$njob++;
            }
            elsif(-d $dir)
            {
                print " ... skipped launch : already run\n";
            }
        }
    }
}

