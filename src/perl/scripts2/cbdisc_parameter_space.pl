#!/usr/bin/env perl
use strict;
use rob_misc qw/MIN MAX log10 mkdirhier/;

############################################################
#
# Script to explore the cbdisc parameter space
#
############################################################
#
# parameters are either:
#
# SCALAR : they are then just set to the scalar value
#
# [ARRAY] : this is an array pointer containing:
#           the values to be sampled
#
#           (see also the arraysample function)
#
############################################################

my $vb = 0;

my $options = {
    outdir => $ENV{HOME}.'/data/cbdiscpops4-n10',
    gridscript => $ENV{HOME}.'/progs/stars/binary_c/src/perl/scripts2/cbdiscs.pl',
    
};



my $parameters = {
    mass_accretion_for_eld => 1e6,

    # comenv options
    minimum_donor_menv_for_comenv => 0.1,
    alpha_ce=>1.0,
    lambda_ce=>-1.0,
    lambda_ionisation=>0.1,
    lambda_enthalpy=>0.0,

    # use Amanda's cores (etc.)
    AGB_core_algorithm=>2,
    AGB_radius_algorithm=>2,
    AGB_luminosity_algorithm=>2,
    AGB_3dup_algorithm=>2,

    # CB disc options. Fixed parameters ...
    wind_disc_mass_fraction => 0.0,
    wind_disc_angmom_fraction => 0.0,
    cbdisc_mass_loss_constant_rate => 0.0,
    cbdisc_mass_loss_inner_L2_cross_multiplier => 0.0,
    cbdisc_mass_loss_FUV_multiplier => 0.0,
    minimum_timestep=>1e-8,    
    cbdisc_gamma => 5.0/3.0,

    #
    # ... and those that vary. These are array references
    # containing all the possible values.
    #
    # Numbers with * after are 1D distributed.
    #
    # Numbers with ^ after are combined into an ND grid.
    #
    comenv_disc_mass_fraction => [1e-1,'1e-2^',1e-3],
    comenv_disc_angmom_fraction => [1e-1,'1e-2^',1e-3],
    cbdisc_alpha => [1e-4,'1e-3*',1e-2],
    cbdisc_kappa => [1e-3,'1e-2*',1e-1],
    cbdisc_torquef => [1e-2,'1e-4*',1e-6],
    cbdisc_mass_loss_inner_viscous_accretion_method => [0,'1*'],
    cbdisc_mass_loss_inner_viscous_multiplier => [0.0,'1.0*'],
    cbdisc_mass_loss_ISM_ram_pressure_multiplier => [0.0,'1.0*'],
    cbdisc_mass_loss_ISM_pressure => ['3000.0*',1e4],
    cbdisc_mass_loss_Xray_multiplier => [0.0,0.1,'1.0*'],
    cbdisc_viscous_photoevaporation_coupling => [0,'1*'],
    cbdisc_minimum_luminosity => [1e-6,'1e-4*'],
    cbdisc_minimum_mass => ['1e-6*',1e-8],
    cbdisc_minimum_fRing => ['0.2*',0.5],
    cbdisc_eccentricity_pumping_method => [0,'1*'],
    cbdisc_resonance_multiplier => [0.1,'1.0*'],
    cbdisc_resonance_damping => [0,'1*'],
    comenv_post_eccentricity => [1e-8,'1e-5*'],
};

mkdirhier($options->{outdir});
my $grid = makegrid($parameters);
my $set = 'A';
foreach my $gridstring (@$grid)
{
    print "Model set $set :\n $gridstring\n\n";
    $set++;
}






exit;

sub makegrid
{
    my @grid = ('defaults');
    my @ndgrid = ('defaults');
    
    my $defaults = {};
    
    foreach my $parameter (sort keys %$parameters)
    {
        my $values = $parameters->{$parameter};
        if(ref $values eq 'ARRAY')
        {
            if("@$values" =~ /(\S+)\*/)
            {
                # We have a list of parameters of which one is the
                # default. These form a 1D variation set.
                $defaults->{$parameter} = $1;
                foreach my $value (grep {
                    strip_number($_) != $defaults->{$parameter} 
                                   } @$values)
                {
                    push(@grid,
                         "defaults $parameter=$value");
                }
            }
            elsif("@$values" =~ /(\S+)\^/)
            {
                # We have a list of parameters of which one is the
                # default. These form an ND variation set.
                $defaults->{$parameter} = $1;
                my @newnd;
                foreach my $nd (@ndgrid)
                {
                    foreach my $value (grep {
                        $_ = strip_number($_)
                                       }@$values)
                    {
                        push(@newnd,"$nd $parameter=$value");
                    }
                }
                @ndgrid = @newnd;
            }
            else
            {
                print "Array of values for parameter \"$parameter\" specified, but no default is given\n";
                exit;
            }
        }
        else
        {
            $defaults->{$parameter} = $values;
        }
    }
    
    # combine the grids
    push(@grid,@ndgrid);

    # list the default parameters
    if($vb)
    {
        print "Defaults : \n";
        foreach my $parameter (sort keys %$defaults) 
        {
            print "    $parameter = $defaults->{$parameter}\n";
        }
        print "\n\n\n";
    }
    
    
    # append to defaults string
    my $defaultstring;
    foreach my $parameter (sort keys %$defaults) 
    {
        $defaultstring .= "$parameter=$defaults->{$parameter} ";
    }
    
    # list the runs
    if($vb)
    {
        print "\n\n\nRuns :\n\n";
        foreach my $run (@grid)
        {
            print "$run\n";
        }
    }

    # but replace 'default' with the defaults string 
    map
    {
        s/defaults\s*/$defaultstring/;
    }@grid;

    # verbose
    if(0)
    {
        print "\n\n\nRuns :\n\n";
        foreach my $run (@grid)
        {
            print "$run\n";
        }
    }

    # return the grid
    return \@grid;
}

sub arraysample
{
    ############################################################
    #
    # Return an array between min and max
    # with n objects, making sure to
    # sampling both min and max.
    #
    # The final argument can be 'linear' (the default if not 
    # given) or 'log' for linear or log spacing respectively.
    #
    # Returns an array reference.
    #
    ############################################################
    my ($min,$max,$n,$mode) = @_;
    my $array;
    $mode //= 'linear';
    
    if($mode eq 'log')
    {
        $min = log10($min);
        $max = log10($max);
    }
        
    my $dx = ($max - $min) / ($n-1);
    $max += $dx;
    for(my $x = $min; $x < $max; $x+=$dx)
    {
        my $X = $mode eq 'log' ? (10.0**$x) : $x;
        push(@$array,$X);
    } 
    return $array;
}


sub strip_number
{
    # strip out any number passed in and return it
    return ($_[0]=~/(-?\d+(?:\.\d*)?e[-\+]?\d+|-?\d+(?:\.\d*)?)/)[0] // undef;
}
