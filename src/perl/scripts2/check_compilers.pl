#!/usr/bin/env perl
use strict;
use Cwd;
use Sort::Versions;
use rob_misc;
use subs 'printx';
use Time::HiRes qw/time/;
$|=1;

#
# Script to build binary_c with each available gcc and clang,
# checking for errors and warnings, and run unit tests
#
# Should be run from the root source directory
#
my $pwd = cwd();
my $logfile = '/tmp/check_compilers.log';
open(my $logfp,'>',$logfile)||die("cannot open $logfile");
my $args = "@ARGV";
my $vb = ($args=~/-v/) ? 1 : 0;
my @compilers =
    $args=~/compilers?=(\S+)/
    ? split(/,/,$1)
    : (
        'gcc',
        'clang'
    );
print "Compilers: @compilers\n";
my $compiler_regexp = join('|',@compilers);
my $speedtests = $args=~/speedtests=(\S+)/ ? $1 : 0;
my $nms = $args=~/nms=(\S+)/ ? $1 : 1000;
my $n_test_systems = $args=~/n_test_systems=(\S+)/ ? $1 : 100;
my $system_list_file = $args=~/system_list_file=(\S+)/ ? $1 : '/tmp/binary_c_random_system_list';
my $meson_version = (`meson --version`=~/^(\d+\.\d+\.\d+)/)[0]; # set to 1 to use meson
my $meson = defined $meson_version ? 1 : 0;
my $meson_builddir = "builddir_check_compilers";
my $system_list_generated = 0;
# exclude these compilers
# note: the keys should be expressions,
# e.g. '<10' or '==11'
my $exclude_compilers =
{
    'gcc' => {
        '<8' => 'binary_c requires GCC 8 or later'
    },
    'clang' => {
        '<12' => 'binary_c requires Clang 12 or later'
    },
};
my %durations;

# build options
my @build_options = (
    '--buildtype release',
    '--buildtype debug',
    '-Dvalgrind=true',
    '-Dgeneric=true',
    '-Dgeneric=true -Dallow_libs=true',
    '-Dusepch=false',
    '-Daccurate=true',
    );

printx "Check binary_c compilers (vb = $vb), logging to $logfile, building with ",($meson ? "meson $meson_version" : 'make')."\n";

foreach my $compiler (@compilers)
{
    my @executables = find_executables($compiler);

    printx "Compiler    : $compiler\n";
    printx "Executables : @executables\n";

    foreach my $executable (@executables)
    {
        foreach my $build_options (@build_options)
        {
            build($executable,$build_options);
            unit_tests($executable,$build_options);
            if($speedtests)
            {
                speed_tests($executable,$build_options);
            }
        }
    }
}

if(%durations)
{
    printx "System list durations\n";
    foreach my $compiler (sort keys %durations)
    {
        printx "$compiler : $durations{$compiler}\n";
    }
}

close $logfile;
exit 0;


sub find_executables
{
    # find available executables of the compiler
    my ($compiler) = @_;

    # find the standard executables
    my @executables = grep{
        /$compiler-?\d*(\.\d+)*$/
    }split(/\n/,`bash -c "compgen -ca $compiler"`);
    chomp @executables;

    # sort by string length
    @executables = sort {
        length($a) <=> length($b)
    } @executables;

    # get versions : we want a unique list by version
    # and we assume they are of the form x.y.z (y and z optional)
    my %versions;
    foreach my $executable (@executables)
    {
        my $v = version_of($executable);
        my $lv = long_version_of($executable);
        if(!defined $v)
        {
            printx "Warning: $executable gave no version\n";
        }
        else
        {
            my $skip = 0;
            my $excludes = $exclude_compilers->{$compiler} // undef;

            if(defined $excludes)
            {
                foreach my $exclusion (keys %$excludes)
                {
                    my $expression = $exclusion;
                    $expression =~ s/(\d+(?:\.\d+(?:\.\d+)?)?)/long_version_number($1)/ge;
                    my $ev = "my \$x = ($lv $expression ? 1:0); \$x;";
                    my $check = eval $ev;
                    if($check)
                    {
                        print "Skipping $compiler $v because it is excluded ($excludes->{$exclusion})\n";
                        $skip = 1;
                    }
                }
            }
            if($skip == 0)
            {
                $versions{$v} = $executable;
            }
        }
    }

    # order by version
    @executables = sort {versioncmp($a,$b)} values %versions;

    return @executables;
}

sub version_of
{
    # return version of the compiler, or undef
    my ($executable) = @_;
    return (`$executable -v  2>\&1`=~/version (\S+)/)[0] //
        (`$executable --version 2>\&1`=~/($compiler_regexp) \S+ (\d.*)/)[0] //
        undef;
}

sub long_version_number
{
    my $version = shift;
    if($version =~ /(\d+)(?:\.(\d+))?(?:\.(\d+))?/)
    {
        return $1 * 1000000 + ($2//0) * 1000 + ($3//0);
    }
    else
    {
        return undef;
    }
}

sub long_version_of
{
    my ($executable) = @_;
    my $version_of = version_of($executable);
    my $lv = long_version_number($version_of);
    if(defined $lv)
    {
        return $lv;
    }
    else
    {
        print "Long version of failed on $executable which has native version $version_of\n";
        exit;
    }
}

sub build
{
    my ($compiler,$build_options) = @_;
    my $v = version_of($compiler)//'';

    # environment for commnads
    my $env = "unset CC; export CC=$compiler; ";

    # run configure or meson
    printx "  Configure with $compiler (version $v) build options: $build_options\n";
    my $cmd =
        $meson
        ?
        "$env rm -rf $meson_builddir; $env rm -f ./src/binary_c.h.gch ./src/binary_c.h.gch.d; $env meson $meson_builddir $build_options"
        :
        "$env ./configure 2>/dev/stdout";

    printx "CMD $cmd\n"if($vb);
    my $r = `$cmd`;
    printx $r if($vb);

    if($meson ?
       $r=~/Found ninja/ :
       $r=~/Now run .\/make/)
    {
        # configure successful : do the build
        printx "Building\n";
        $cmd =
            $meson ?
            "cd $meson_builddir; ninja 2>/dev/stdout" :
            "$env ./make clean 2>/dev/stdout; ./make 2>/dev/stdout";
        printx "CMD $cmd\n" if($vb);
        $r = `$cmd`;

        # clean the results of successful statements
        if($meson)
        {
            $r =~ s/^\[\d+\/\d+].*//mg;
        }
        else
        {
            $r =~ s/(?:rm -f|Make on|Done|BUILD|LINK|DONE).*//g;

        }
        $r =~ s/\n\s*\n//g;
        $r =~ s/^\s+//;
        $r =~ s/\s+$//;
        $r = rob_misc::remove_ANSI($r);

        # check for remaining warnings or errors
        if($r)
        {
            # error or warning found
            printx "Found error or warning\n\n\"$r\"\n";
            close $logfile;
            print "\n\n\nSee $logfile\n";
            exit 1;
        }
        else
        {
            printx "Build was clean\n";
        }
    }
    else
    {
        printx "Configure with compiler \"$compiler\" (version $v) failed \n";
        exit 1;
    }
}


sub printx(@)
{
    my $s = join('',@_);
    print {$logfp} $s if(defined $logfp);
    print $s;
}

sub unit_tests
{
    # do unit tests, if there is an error stop
    my $testpy = './src/python/do_unit_tests.py compare force';
    printx "Doing unit tests from $testpy\n";
    my $unit_test_output = `env BINARY_C=$pwd BINARY_C_EXECDIR=$meson_builddir $testpy`;
    print {$logfp} "###########\n",
        $unit_test_output,
        "###########\n";

    my $ret = $?;
    printx "Unit tests returned $ret (0 is good)\n";
    if($ret != 0)
    {
        printx "One of the unit tests failed : stopping here\n";
        exit 1;
    }
}

sub speed_tests
{
    my ($compiler) = @_;
    # binary_c's speedtests
    printx "Running binary_c's speed tests\n";
    my $start = time;
    print {$logfp} `env NMS=$nms BINARY_C=$pwd ./binary_c speedtests true version`;
    my $duration = time - $start;
    printx "Speed tests took $duration\n";

    # list of systems
    if($system_list_generated == 0)
    {
        # needs making
        `binary_c repeat $n_test_systems random_system_list true > $system_list_file`;
        $system_list_generated = 1;
    }

    # run list of systems (same for each test)
    printx "Running system list ($n_test_systems systems)\n";
    $start = time;
    print {$logfp} `binary_c system_list $system_list_file`;
    $duration = time - $start;
    printx "System list took $duration\n";
    $durations{$compiler} = $duration;
}
