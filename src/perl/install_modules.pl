#!/usr/bin/env perl

#
# Perl script to install modules required for binary_grid
#

use 5.16.0;

foreach my $mod (
    'POSIX',
    'Carp',
    'Clone',
    'common::sense',
    'Compress::Zlib',
    'Cwd',
    'Data::Dumper',
    'Data::Serializer',
    'Fcntl',
    'File::Basename',
    'File::Temp',
    'File::Path',
    'File::Find',
    'Math::Trig',
    'FindBin',
    'Inline',
    'Inline::C',
    'IO::File',
    'IO::Handle',
    'IO::Interactive',
    'IO::Select',
    'IPC::Open3',
    'Sort::Key',
    'Sys::Hostname',
    'Sys::Info',
    'Sys::Info::Constants',
    'Sys::Info::Device::CPU',
    'Time::HiRes',
    'threads',
    'threads::shared',
    'Thread::Queue',
    'Data::Dumper',
    'Data::Serializer',
    'Data::Serializer::Raw',
    'Proc::ProcessTable',
    'Term::ANSIColor',
    'Math::Trig',
    'constant',
    'List::Util',
    'List::MoreUtils',
    'Memoize',
    'Inline::C',
    'Sub::Identify',
    'Cpanel::JSON::XS'
    )
{ 
    install($mod);
}

my $pwd=$ENV{PWD};
say "\nAll CPAN modules installed with (apparently) no failures.\n";

say "Now installing Rob's modules (binary_grid and friends)\n";
open(my $c,'-|',"cd modules_targz ; ./install_all.pl 2>\&1")||
    die("cannot open perl installation dir and/or script");
while(my $l = <$c>)
{
    print $l;
}    
close $c;
say;
say "Module installation attempt complete.\n";

exit;

sub install
{
    my $mod=$_[0];
    say "Installing $mod with cpanminus";

    open(my $c,'-|',"cpanm -v $mod 2>\&1")||
	die("cannot open 'cpanm -v --force --reinstall $mod' for reading : is cpanm installed?");
    my $fail=0;
    while(<$c>)
    {
	print 'cpanm : '.$_; 
	$fail=1 if(!/(finding|abstract)/i && /fail/i);
    }
    close $c;
    say;

    if($fail)
    {
	say "Something failed to install for module $mod";
	exit;
    }
}
