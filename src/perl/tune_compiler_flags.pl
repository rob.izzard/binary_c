#!/usr/bin/env perl
$|=1;
use 5.10.0;
use strict;
use warnings;
use Term::ANSIColor;
use rob_misc;
use Perl::Version qw/vcmp/;
use Data::Dumper;
#
# script to tune the compiler flags for binary_c/nucsyn automatically
#
# NB currently only for gcc
#

print "Binary_c/nucsyn compiler flag tuner\n";

get_proccpu();
my $cpu=get_cpu(); # use it to determine the cpu type
my $on_build_fail=''; # on fail either 'exit', 'pause', or carry on
my $n_burn_in=20; # use random flags for $n_burn_in iterations
my $merit_power=2; # bias towards the best flags
my $outtable;
my $best_time;
my $best_flags='';
my $prev_added;
my %cpuinfo;
my $cc = $ENV{'CC'} // 'gcc';
my $cc_version = (`$cc --version`=~m/\W(\d+\.\d+(?:\.\d+)?)\W/)[0]; # compiler version
my %flag_merit;
parse_cmdline_args_to_perlvars();
my $compiler_flags = compflags($cc);
my $compiler = compiler($cc);
print "Compiler (CC) = $cc (which I think is $compiler)\nCompiler version $cc_version\n";

# possible flags : 
#
# arrays are treated as being one from many (of which
# the first is the slowest and the last the fastest, e.g.
# ['-O0','-O1','-O2','-O3','-Ofast']),
# otherwise scalars are booleans (i.e. on or off) when
# having it on is supposed to be faster.
#
# You can specify flags for different compiler versions using
# the onlyfor(..) function, e.g.
#
# onlyfor('4.5.2','-fdo-more-optimization'),
#
# which enables the flag only for compiler versions 
# 4.5.2 and above.
#
# undef elements are ignored (the 'grep defined,' does this)
#

my @possible_flags=grep defined, (
    ['-O0','-O1','-O2','-O3',
     onlyfor('gcc','-Ofast'),
     onlyfor('clang','-O4')
    ],
    onlyfor('gcc',
	    '-fbranch-target-load-optimize',
	    '-fbtr-bb-exclusive',
	    '-fdata-sections',
	    onlyfor('5.1.0','-fdevirtualize-at-ltrans'),
	    '-ffast-math',
	    '-ffunction-sections',
	    '-fgcse-las',
	    '-fgcse-after-reload',
	    onlyfor('5.0.0','-fgraphite-identity'),
	    '-fipa-pta',
	    '-fivopts',
	    onlyfor('5.0.0','-floop-block'),
	    onlyfor('5.0.0','-floop-interchange'),
	    onlyfor('5.0.0','-floop-nest-optimize'),
	    onlyfor('5.0.0','-floop-strip-mine'),
	    '-fmodulo-sched',
	    '-fno-ira-share-save-slots',
	    '-fno-ira-share-spill-slots',
	    '-fno-math-errno',
	    '-fno-signed-zeros',
	    '-fpeel-loops',
	    '-fprefetch-loop-arrays',
	    '-frename-registers',
	    '-freorder-blocks-and-partition',
	    '-freschedule-modulo-scheduled-loops',
	    #'-fsection-anchors', not supported
	    '-fselective-scheduling',
	    '-fselective-scheduling2',
	    '-fsel-sched-pipelining',
	    '-fsel-sched-pipelining-outer-loops',
	    onlyfor('5.1.0','-fstdarg-opt'),
	    '-ftracer', 
	    onlyfor('5.0.0','-ftree-loop-distribution'),
	    '-ftree-loop-distribute-patterns',
	    '-ftree-loop-im',
	    '-ftree-loop-if-convert',
	    onlyfor('5.0.0','-ftree-loop-linear'),
	    '-funit-at-a-time',
	    '-funroll-loops',
	    '-funroll-all-loops',
	    '-funswitch-loops',
	    '-fweb',    
	    '-static-libgcc'),

    onlyfor('gcc','-static'),
    onlyfor('clang',
	    '-fno-stack-protector',
	    '-fslp-vectorize-aggressive'),
    );

# convert scalars to lists of two
map
{
    $_ = ['!'.$_,$_] if(ref $_ ne 'ARRAY')
}@possible_flags;

my $cache_flags = ($#possible_flags<5);

dumpflags();

# first configure to make src/Makefile
print "Running ./configure ...\n";
`./configure cc=$cc`;

my $current_flags='';
$best_time=1e10; # huge time, we must be faster!

my $i_merit_power = 1.0/$merit_power;

while(1)
{    
    # construct trial flags
    my @trial_flags;
    state $count=0;

    if($count==0)
    {
	# first time : everything off
	print "First time : everything off\n";
	@trial_flags = map{
	    $$_[0]
	}@possible_flags;
    }
    elsif($count==1)
    {
	# second time : everything on
	print "Second time : everything on\n";
	@trial_flags = map{
	    $$_[-1]
	}@possible_flags;
    }
    elsif($count<$n_burn_in+2)
    {
	# burn in : random flags
	@trial_flags = map{
	    $$_[rand @$_]
	}@possible_flags;
	printf "Using random flags in burn in (%d < %d)\n",
	$count,$n_burn_in+2;
    }
    else
    {
	# after the burn in, use a merit
	# function to select flags
	@trial_flags = map
	{  
	    my @x = reverse sort { 
		# sort by ascending merit function
		merit_function($a)<=>merit_function($b)
	    } @$_;
	    #my @m = map { merit_function($_) } @$_;
	    my $n = int(((1.0-rand())**$i_merit_power)*(scalar @x));
	    my $y = $x[$n];
	    #print "Flagset @$_ : merit @m : sorted @x : selected $n = $y\n";
	    #$selcount{$y}++;
	    $y;
	}@possible_flags;
    }

    $count++;
    #next if($count==1);

    # join to a string and always use -mtune=native
    my $trial_flags=$compiler_flags.' -mtune=native -march=native -pipe '.join(' ',@trial_flags);
    
    print "Trial flags : ",filterflags($trial_flags),"
Current best flags: \"",color('cyan'),filterflags($best_flags),color('reset'),"\" ($best_time s)
";

    if($cache_flags)
    { 
	# skip this run if we have already done it
	state %flagcache;
	next if (defined $flagcache{$trial_flags});
	$flagcache{$trial_flags}=1;
    }

    # then patch then Makefile
    print "creating Makefile.tune...";
    patch_makefile($trial_flags);
    
    # build code
    print "make...";
    unlink "src/binary_c"if(-s -f "src/binary_c");
    `cd src; make cleanall; make -j $cpuinfo{nprocessors} -f Makefile.tune >/tmp/binary_c.Make.stdout 2>/tmp/binary_c.Make.stderr`; 
    
    # check for binary_c
    if(!-s -f -e 'src/binary_c')
    {
	print "\nbinary_c build failed : see /tmp/binary_c.Make.stderr\n";
	if($on_build_fail eq 'pause')
	{
	    <STDIN>;
	}
	elsif($on_build_fail eq 'exit')
	{
	    exit(1)
	}
	else
	{
	    next;
	}
    }

    # call tbse to test runtime
    print "call tbse for timing tests...";
    my $this=(`tbse`=~/runtime = (\S+)/)[0];
    #my $this = 3-($trial_flags=~/-O(\d+)/)[0]+0.1;
    #my $this = $trial_flags=~/\!-static/ ? 1.0 : 0.1;
    
    # now we should use the best time available, not the mean, as that
    # is likely to be affected by the OS and system load
    if(defined $this && $this>0.0)
    {
	if($this<$best_time)
	{
	    $best_flags=$trial_flags;
	    $best_time=$this;	    
	    print color('green'),"This is better than before, set new best flags: $best_flags\n",color('reset');
	}
	print "\n";
	
	# update flag merit data
	foreach my $trial_flag (@trial_flags)
	{
	    $flag_merit{$trial_flag}{time_total} += $this;
	    $flag_merit{$trial_flag}{count}++;
	    $flag_merit{$trial_flag}{best} = MIN($this,$flag_merit{$trial_flag}{best}//$this);
	}
    }
    else
    {
	print color('magenta'),"Build failed :(\n",color('reset');
    }

    foreach my $flagset (@possible_flags)
    {
	print "Flagset : ",color('yellow'),join(' ',@$flagset),color('reset'),"\n";
	
	no warnings;
	foreach my $flag (grep {$flag_merit{$_}{count}} @$flagset)
	{
	    printf "     % 50s : % 6d : %15.2f : %5.2f : %s%5.2f%s\n",
	    $flag,
	    $flag_merit{$flag}{count}, 
	    $flag_merit{$flag}{time_total}, 
	    $flag_merit{$flag}{time_total}/MAX(1,$flag_merit{$flag}{count}),
	    color('red'),
	    $flag_merit{$flag}{best},
	    color('reset');
	}
	use warnings;
    }

}

print "The best compiler flags were: $best_flags\n";

exit;

sub merit_function
{
    return 
	$flag_merit{$_[0]}{count} ?
	$flag_merit{$_[0]}{time_total}/$flag_merit{$_[0]}{count} :
	0.0;
       
}


sub patch_makefile
{
    my $newflags=shift;

    # remove negative flags
    $newflags = filterflags($newflags);

    open(MAKEORIG,"<src/Makefile")||die("cannot open /src/Makefile for reading");
    open(MAKETUNE,">src/Makefile.tune")||die("cannot open src/Makefile.tune for writing");
    my $lorig;

    while($lorig=<MAKEORIG>)
    {
	chomp $lorig;
	if($lorig=~s/(CFLAGS\s+\:=)(.*)/$1/)
	{
	    my $h=$1;
	    my $flags=$2;
	    $flags=~s/\s(?:\-[^WD]\S+)//og;
	    $lorig.=$flags.' '.$newflags;
	    $lorig=~s/\s+/ /go;
	}
	print MAKETUNE $lorig,"\n";
    }
    close MAKEORIG;
    close MAKETUNE;
}

sub get_proccpu
{
    open(PROC,'</proc/cpuinfo')||die('cannot open /proc/cpuinfo');
    while(<PROC>)
    {
	chomp;
	if(/processor/)
	{
	    $cpuinfo{nprocessors}++;
	}
	if(/^(.*)\s+\:\s+(.*)$/)
	{
	    my $k=$1;
	    my $d=$2;
	    $k=~s/\s+$//;
	    $d=~s/\s+$//;
	    $cpuinfo{$k}=$d;
	}
    }
    close PROC;
}

sub get_cpu
{

    if($cpuinfo{'model name'}=~/intel/i)
    {
	# intel chips
	if($cpuinfo{'model name'}=~/Pentium\(R\) M/)
	{
	    return("pentium-m");
	}
	elsif($cpuinfo{'model name'}=~/Core\(TM\)/)
	{
	    return("core");
	}
	elsif($cpuinfo{"cpu family"}>=15)
	{
	    return("pentium4");
	}
    }
    elsif($cpuinfo{'model name'}=~/amd/i)
    {
	# amd chips
	if($cpuinfo{'model name'}=~/opteron/i)
	{
	    return("opteron");
	}
	elsif($cpuinfo{'model name'}=~/Athlon\(tm\) 64/)
	{
	    return("athlon64");
	}
	elsif($cpuinfo{'model'}>=8)
	{
	    return("athlon-xp");
	}
	elsif($cpuinfo{'model'}>=4)
	{
	    return("athlon-tbird");	    
	}
	else
	{
	    # this is the default for modern machines
	    return('native');
	}
    }
    elsif($cpuinfo{"cpu"} =~/750/)
    {
	# ibook
	return("750");
    }
    else
    {
	return("");
    }
}

no warnings;
sub MIN
{
    return $_[0] < $_[1] ? $_[0] : $_[1];
}

sub MAX
{
    return $_[0] > $_[1] ? $_[0] : $_[1];
}
use warnings;

sub filterflags
{
    # filter out 'negative' flags and whitespace
    my $flags = $_[0];
    $flags=~s/\!\S+//g;
    $flags=~s/\s+/ /g;
    $flags=~s/^\s+//;
    $flags=~s/\s+$//;
    return $flags;
}

sub laterthan
{ 
    # if the version given by $_[0]
    # is equal or greater than the 
    # current compiler version, return 1
    # otherwise return 0
    my $v1 = Perl::Version->new($_[0]);
    state $v2;
    $v2 //= Perl::Version->new($cc_version);
    return $v2->vcmp($v1)>=0 ? 1 : 0;
}

sub onlyfor
{
    # check a version string or compiler name,
    # and return the flags ONLY if either the compiler
    # is being used, or the version of the compiler is
    # later or equal to that specified.
    # (Otherwise return an empty list)
    my ($version_or_compiler,@flags) = @_;
    return (
	# probably a version number, not a compiler
	($version_or_compiler=~/^\d/ && 
	 laterthan($version_or_compiler))||
	# probably a compiler
	($version_or_compiler eq $compiler)
	)
	? @flags : ();
}

sub compiler
{
    my $cc = $_[0];
    my $v = `$cc --version`;
    return $v=~/^gcc/ ? 'gcc' :
	$v=~/clang/ ? 'clang' :
	$v=~/(intel|icc)/ ? 'icc' :
	undef;
}

sub compflags
{
    # Flags specific to a compiler that must always 
    # be used.
    my $cc=$_[0];
    my $r = $cc=~/clang/ ? ' -fbracket-depth=512 ' :
	$cc=~/gcc/ ? '' :
	'';
    return $r;
}


sub dumpflags
{
    my $y=color('yellow');
    my $r=color('reset');
    my $d=Dumper(@possible_flags);
    $d=~s/([\[,])\s+/$1/g;
    $d=~s/\s+\];/\],/g;
    $d=~s/\$VAR\d+\s+=\s+//g;
    $d=~s/\[/\[$y/g;
    $d=~s/\]/$r\]/g;
    print $d;
}
