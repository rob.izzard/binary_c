set output "test_holly.pdf"
set terminal pdfcairo enhanced font "Helvetica,16" linewidth 2 size 10cm,8cm

set xrange[0.75:20.1]

set logscale x
set xlabel "Initial mass / M_{⊙}"
set ylabel "[C/N] after first dredge up"
set key outside below


plot \
"holly_test_output-Z0.02.dat" u 1:2 title "STARS Z=0.02" w p ps 0.5,\
"holly_test_output-Z0.02.dat" u 1:3 title "binary\\_c: AG89" w p ps 0.5,\
;

plot \
"holly_test_output-Z0.004.dat" u 1:2 title "STARS Z=0.004" w p ps 0.5,\
"holly_test_output-Z0.004.dat" u 1:3 title "binary\\_c: AG89" w p ps 0.5,\
;

plot \
"holly_test_output-Z0.001.dat" u 1:2 title "STARS Z=0.001" w p ps 0.5,\
"holly_test_output-Z0.001.dat" u 1:3 title "binary\\_c: AG89" w p ps 0.5,\
;

plot \
"holly_test_output-Z0.0001.dat" u 1:2 title "STARS Z=0.0001" w p ps 0.5,\
"holly_test_output-Z0.0001.dat" u 1:3 title "binary\\_c: AG89" w p ps 0.5,\
;

#exit

pwd="`pwd`"
cd "/home/izzard/tex/papers/CN/images"

set bmargin at screen 0.17
set tmargin at screen 0.95
# 0.18 = 1.8cm
set lmargin at screen 0.18 
# 0.9 = 9cm
set rmargin at screen 0.9
set yrange[-0.9:0.1]
set xlabel offset 0,1


set output "holly-Z0.02.pdf"
set label "{/Helvetica-Italic Z}=0.02" at screen 0.2,0.9
plot \
pwd."/holly_test_output-Z0.02.dat" u 1:2 title "" w p ps 0.5,\
pwd."/holly_test_output-Z0.02.dat" u 1:3 title "" w p ps 0.5,\
;
unset label

set output "holly-Z0.004.pdf"
set label "{/Helvetica-Italic Z}=0.004" at screen 0.2,0.9
plot \
pwd."/holly_test_output-Z0.004.dat" u 1:2 title "" w p ps 0.5,\
pwd."/holly_test_output-Z0.004.dat" u 1:3 title "" w p ps 0.5,\
;
unset label

set output "holly-Z0.0001.pdf"
set label "{/Helvetica-Italic Z}=0.0001" at screen 0.2,0.9
plot \
pwd."/holly_test_output-Z0.0001.dat" u 1:2 title "" w p ps 0.5,\
pwd."/holly_test_output-Z0.0001.dat" u 1:3 title "" w p ps 0.5,\
;
unset label

# NB Z=0.001 is twice as wide to allow for the key
# to be below all the other plots
# 0.18 = 1.8cm
set lmargin at screen 0.09 
# 0.9 = 9cm
set rmargin at screen 0.45

set terminal pdfcairo enhanced font "Helvetica,16" linewidth 2 size 20cm,8cm
set output "holly-Z0.001.pdf"
set label "{/Helvetica-Italic Z}=0.001" at screen 0.1,0.9
set key at screen 0.51,0.05
plot \
pwd."/holly_test_output-Z0.001.dat" u 1:2 title "{/Helvetica-Italic STARS}" w p ps 0.5,\
pwd."/holly_test_output-Z0.001.dat" u 1:3 title "{/Helvetica-Italic binary\\_c}" w p ps 0.5,\
;
