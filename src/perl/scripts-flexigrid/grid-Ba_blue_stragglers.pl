#!/usr/bin/env perl 
$|=1; # enable this line for auto-flushed output
use strict;
use rob_misc;
use binary_grid;
use binary_grid::condor;
use threads;
use Histogram;
use Sort::Key qw(nsort);
use Data::Dumper;
#
# Grid to calculate statistics regarding the nature of
# binary barium star systems 

# set up data structures
my $results={};
my $outdir;


# first set some defaults for the grid
defaults();
# then parse the command line to override the defaults
parse_args(); 

# ... and set up dependent variables
secondary_variables();

# now set up the grid
setup_binary_grid();

my $nthreads=ncpus();
flexigrid($nthreads); 

output();

exit(0);

############################################################
############################################################
############################################################

sub parse_bse
{
    # this gets called by the binary_grid module, it parses the 
    # lines of output from binary_c/nucsyn
    my $brk=0; 

    # you should set data in the $h hash pointer, which is 
    # then passed to the join_thread function (below) for you
    # to add up

    # if you want to use the binary_cd you should make sure this function
    # contains all the data it needs because the remote machine will
    # want to do data processing on the fly!
    my $h=shift;
    


    my $argd=0;
    while($brk==0)
    {
	$_=tbse_line(); 
	my $l=$_;
	chomp;

	if($_ eq 'fin')
	{
	    $brk=1; # the end of output
	}
	else
	{
	    my $head=0; # must be FALSE

	    # detect appropriate data line
	    if(s/^MS(\d) //)
	    { 
		# found a main sequence star
		$head = 'Main sequence';
	    }
	    elsif(s/^BSSBa(\d+) //)
	    {
		$head = 'Blue straggler';
	    }
	    
	    # no appropriate data line found : skip
	    next if(!$head);

	    if(!defined $h->{histograms})
	    {
		# set up thread histograms
		make_new_histograms($h);
	    }
	    
	    # extract data
	    my $nstars = $1; # 1=single, 2=binary
	    $head .= ' '.$nstars.' ';
	    my @x = split(/\s+/,$_); # splits $_ into the array @x for data processing
	    my $dtp = $x[0];
	    my $Pdays = log10(MAX(1e-30,$x[2]));
	    my $CFe = $x[4];
	    my $BaFe = $x[5];
	    
	    # save number counts
	    $h->{number_counts}{$head}+=$dtp;
	    $h->{number_counts}{$head.'[Ba/Fe]>0.2'} += $dtp if($BaFe>0.2);
	    $h->{number_counts}{$head.'[Ba/Fe]>0.5'} += $dtp if($BaFe>0.5);
	    $h->{number_counts}{$head.'[C/Fe]>0.2'} += $dtp if($CFe>0.2);
	    $h->{number_counts}{$head.'[C/Fe]>0.5'} += $dtp if($CFe>0.5);
	    
	    # save property distriutions
	    $h->{histograms}{$head.'period'}->add_data($Pdays,$dtp); 
	    $h->{histograms}{$head.'BaFe'}->add_data($BaFe,$dtp);
	    $h->{histograms}{$head.'CFe'}->add_data($CFe,$dtp);
	    $h->{histograms}{$head.'CFe vs BaFe'}->add_data($BaFe,$CFe,$dtp);
	    $h->{histograms}{$head.'BaFe vs period'}->add_data($Pdays,$BaFe,$dtp);
	    $h->{histograms}{$head.'CFe vs period'}->add_data($Pdays,$CFe,$dtp);
	}
    }
}

sub defaults
{
    grid_defaults();

    # set up a pointer to the parser function (see parse_bse())
    $binary_grid::grid_options{'parse_bse_function_pointer'}=\&parse_bse;

    # code options
    $binary_grid::grid_options{'nice -n +0'}='';#'nice -n +10'; 
    $binary_grid::grid_options{'timeout'}=30; # seconds until timeout
    $binary_grid::grid_options{'log_args'}=0;
    $binary_grid::grid_options{'save_args'}=0;
    $binary_grid::grid_options{'vb'}=1;
    $binary_grid::grid_options{'prog'}='binary_c-blue_stragglers_barium';

    # physics
    $binary_grid::bse_options{'no_thermohaline_mixing'}=0;
    $binary_grid::bse_options{'z'}=0.004;
    $binary_grid::bse_options{'wd_sigma'}=0.0;
    $binary_grid::bse_options{'c13_eff'}=1.0;
    $binary_grid::bse_options{'acc2'}=1.5;
    $binary_grid::bse_options{'alpha_ce'}=0.25; # 0.25
    $binary_grid::bse_options{'lambda_ce'}=-1; # -1 = automatically set (Tauris)
    $binary_grid::bse_options{'lambda_ionisation'}=0.0; # 0.0
    $binary_grid::bse_options{'initial_abundance_mix'}=2; # 2 = lodders (required!)
    $binary_grid::bse_options{'comenv_ms_accretion_mass'}=0.0;
    $binary_grid::bse_options{'jorb_loss'}=0;
    $binary_grid::bse_options{'lw'}=2.0;
    $binary_grid::bse_options{'WRLOF_method'}=0;
    $binary_grid::bse_options{'tidal_strength_factor'}=1.0;
    $binary_grid::bse_options{'initial_abundance_mix'}=2; # 2 = Lodders, required for barium
    $binary_grid::bse_options{'maximum_timestep'}=10; #  10 Myr seems reasonable 

    # extra 3DUP
    $binary_grid::bse_options{'delta_mcmin'}=-0.4;
    $binary_grid::bse_options{'lambda_min'}=0.5;
    $binary_grid::bse_options{'minimum_envelope_mass_for_third_dredgeup'}=0.5;

    # canonical 
    #$binary_grid::bse_options{'delta_mcmin'}=0.0;
    #$binary_grid::bse_options{'lambda_min'}=0.0;
    #$binary_grid::bse_options{'minimum_envelope_mass_for_third_dredgeup'}=0.5;

    #$binary_grid::bse_options{'idum'}=-10;
    $binary_grid::grid_options{results_hash}=$results;
    $binary_grid::grid_options{save_args}=1;

    my $r=40;

    distribution_functions::bastard_distribution({
	mmin=>0.7,
	mmax=>3.0,
	m2min=>0.1,
	qmin=>0.0,
	qmax=>1.0,
	useecc=>undef,
	nm1=>400,#40
	nm2=>4*$r,#40
	nper=>10*$r,#100
	necc=>undef,#4*$r,#40
						 });
}

sub parse_args
{
    # parse command line arguments
    binary_grid::parse_grid_args(@ARGV);
}

sub secondary_variables
{
    # from the defaults and/or passed in variables,
    # calculate useful things

    # default outdir if not given
    $outdir //= $ENV{HOME}.'/data/blue_straggler_barium/Z0.004-maxdt10-r40';

    if(-d $outdir)
    { 
	print "Outdir \"$outdir\" already exists : please give me a non-existant directory name\n";
	exit;
    }

    rob_misc::mkdirhier($outdir);

    # use colour?
    if($binary_grid::grid_options{'colour'}==1)
    {
        # use ANSI color for output to xterms/rxvt etc.
        use Term::ANSIColor;
        # set to white on black
        print color("white on_black");
    }
    else
    {
        # dummy function to replace the ANSI color() function
        eval("sub\ color\{ \}");
    }
}


sub output
{
    # output
    print "OUTPUT to $outdir\n";
    mkdirhier($outdir);

    open(my $out, '>', "$outdir/log");
    if($out)
    {
	print {$out} Dumper(\%binary_grid::grid_options);
	print {$out} Dumper(\%binary_grid::bse_options);
    }
    close $out;

    my %histograms = %{$results->{histograms}};
    
    foreach my $k (keys %histograms)
    {
	# make histogram
	my $f="$outdir/$k";
	$f=~s/\s+/_/g;

	my $histogram = $histograms{$k};

	print "Dump histogram $k (object $histograms{$k} or $results->{histograms}{$k}, ref ",ref $histogram,") to file $f\n";

	$histogram->format('gnuplot_surface');
	$histogram->dump($f.'.surf');

	$histogram->format('gnuplot');
	$histogram->dump($f.'.dat');

    }

    my %number_counts = %{$results->{number_counts}};

    print "Output number counts\n";
    open(FP,">$outdir/number_counts")||die("cannot open $outdir/number_counts");
    foreach my $k (sort keys %number_counts)
    {
	# number count
	printf "SCALAR : %s = %g\n",$k,$number_counts{$k};
	printf FP "%s = %g\n",$k,$number_counts{$k};
    }
    close FP;
}

sub make_new_histograms
{
    my $h=$_[0]; # thread-private results_hash

    foreach my $nstars (1,2)
    {
	foreach my $head ('Main sequence',
			  'Blue straggler')
	{
	    my $x = $head.' '.$nstars.' ';
	    foreach my $hist (
		{
		    name=>$x.'period',
		    binwidths=>[0.1]
		},
		{
		    name=>$x.'BaFe',
		    binwidths=>[0.1]
		},
		{
		    name=>$x.'CFe',
		    binwidths=>[0.1]
		},
		{
		    name=>$x.'CFe vs BaFe',
		    binwidths=>[0.1,0.1]
		},
		{
		    name=>$x.'BaFe vs period',
		    binwidths=>[0.1,0.1]
		},
		{
		    name=>$x.'CFe vs period',
		    binwidths=>[0.1,0.1]
		})
	    {
		$h->{histograms}{$hist->{name}} = Histogram->new();
		$h->{histograms}{$hist->{name}}->binwidths(@{$hist->{binwidths}});
	    }
	}
    }

    $h->{number_counts}={};
}

