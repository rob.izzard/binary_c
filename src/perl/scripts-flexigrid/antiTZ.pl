#!/usr/bin/env perl

# wrapper to run or plot multiple populations for anti-TZ project

# call with a single argument:
#
# run : this runs the grids
#
# plot : this plots the resulting files

use strict;
use Term::ANSIColor;
use rob_misc;
use binary_grid qw/gnuplot_title/;
use 5.16.0;
use File::Temp qw/tempfile/;


# grid script to be called
my $grid = './src/perl/scripts-flexigrid/grid-antiTZ-flexigrid.pl';
my %defaults=get_defaults();

my $mode = ("@ARGV"=~/(run|plot)/)[0];
if(!defined $mode)
{
    print "Please specify mode: 'run' or 'plot'\n"; 
    exit;
}


# global plot options
my $global_plot_options={logx=>0,
			 logy=>1
};
vary(metallicity=>[0.004],
     plot_options=>{logx=>1});
exit;
vary('defaults'); # defaults

vary(alpha_ce=>[0.1,0.5,1.0,3.0],
     lambda_ce=>[-1,0.5],
     lambda_ion=>[0,0.1,0.5],
    );

vary(metallicity=>[0.0001,0.001,0.008,0.004,0.01,0.02],
     plot_options=>{logx=>1});

vary(sn_sigma=>[0,190,50,500]);

vary(BH_prescription=>[0,1]);

vary(wr_wind=>[0,1,2,3]);

vary(tidal_strength_factor=>[0.1,1.0,100],
     plot_options=>{logx=>1});

vary('last');

exit;

############################################################


sub vary
{
    if($_[0] eq 'defaults')
    {
	if($mode eq 'run')
	{
	    rungrid('defaults');
	}
    }
    else
    {
	# given a set of variables, make a list of runs 
	
	my $datadir='/var/tmp/antiTZplot';
	mkdirhier($datadir);
	my $gpfile = $datadir.'/plot';
	my $ps = '/tmp/antiTZ.ps';
	state $gp;
	
	# open gnuplot file if required
	if(!defined $gp)
	{
	    print "open gp\n";
	    open($gp,'>',$gpfile)||die("cannot open $gpfile");
	    print {$gp} "set terminal postscript enhanced colour \"Helvetica\" 14
set output \"$ps\"
";
	}
	
	
	if($_[0] eq 'last')
	{
	    if($mode eq 'plot')
	    {
		# close gnuplot file and run it
		print "close $gp\n";
		close $gp;
		`gnuplot $gpfile`;
		print "See $ps\n";
	    }
	}
	else
	{ 
	    my @code;
	    my %h = @_;

	    # get plot options
	    my $plot_options = (delete $h{plot_options});
	    my $with = 'w p ps 1.5';

	    # save list of SORTED variables
	    my @vars = sort keys %h;

	    # what would the defaults be?
	    my $default = join(' ',map{"$_=".get_default($_)}@vars);
	    
	    foreach my $var (@vars)
	    {
		my $vals = join(',',@{$h{$var}});
		$code[1] .= "foreach my \$$var ($vals)\n\{\n";
		$code[3] .= "}\n";
	    }

	    $code[1] .= 'push(@runs,"'.join(' ',map{"$_=\$$_"}@vars)."\");\n";
	    $code[1] .= 'push(@vals,"'.join(' ',map{"\$$_"}@vars)."\");\n";

	    # get a list of runs
	    my @runs;
	    my @vals;
	    eval "@code";

	    # get a list of directories for the runs
	    my @dirs = @runs;
	    map{ 
		$_ = $_ eq $default ? outdir('defaults') : outdir($_);
	    }@dirs;

	    # do grid runs
	    if($mode eq 'run')
	    {
		for(my $i=0;$i<=$#runs;$i++)
		{
		    print "Attempt to run $runs[$i] in dir $dirs[$i]\n";
		    rungrid($runs[$i]);
		}
	    }
	    elsif($mode eq 'plot')
	    {
		# make plots : more complicated! 
		my %datafiles;
		my %datafp;
		

		# first, extract the data in the order we want it
		my $col=0;
		foreach my $varyvar (@vars)
		{

		    $col++;
		    print "Vary $varyvar (column $col)\n";
		    my $smallgptitle=binary_grid::gnuplot_title($varyvar);
		    my $gptitle='Vary '.$smallgptitle;

		    for(my $i=0;$i<=$#runs;$i++)
		    {
			print "Plot $runs[$i] in dir $dirs[$i]\n";
			
			# get data from the log file
			my %data = get_data($dirs[$i]);
			
			foreach my $k (keys %data)
			{
			    # open data files if required
			    if(!defined $datafiles{$k})
			    {
				$datafiles{$k} = $datadir.'/vary_'.$varyvar.'_'.$k.'.dat';
				#print "Opened $datafiles{$k}\n";
				open($datafp{$k},'>',$datafiles{$k})||
				    die("cannot open $datafiles{$k} for writing");
			    }
			    
			    print {$datafp{$k}} "$vals[$i] $data{$k}\n"; 
			}
		    }

		    # close files
		    foreach my $fp (keys %datafp)
		    {
			close $fp;
		    }


		    say {$gp} "unset multiplot
set size 1,1
set origin 0,0
set key below
set size 1,1";
		    # apply global then local plot options
		    foreach my $p (grep{defined $_}($global_plot_options,$plot_options))
		    {
			foreach my $x (grep{defined $$p{'log'.$_}}('x','y','x2','y2'))
			{
			    print {$gp} "set ",($$p{'log'.$x} ? '' : 'no'),"logscale $x\n";
			}
		    }
		    # combined plot
		    my $plotcomma='plot';

		    say {$gp} "set title \"$gptitle\"";
		    my $ndata = 1+ scalar @vars;
		    my $lt=1;
		    foreach my $file (sort keys %datafiles)
		    {
			my $title = $file;
			print {$gp} "$plotcomma \"$datafiles{$file}\" u $col:$ndata $with lt $lt title \"$title\" ";
			$plotcomma=',';
			$lt++;
		    }
		    print {$gp} "\n\n";

		    # panels
		    say {$gp} "set multiplot layout 3,3 columnsfirst scale 1.0,1.1
unset logscale y
unset title
set key inside
";
		    $lt=1;
		    foreach my $file (sort keys %datafiles)
		    {
			my $title = $file;
			say {$gp} "
set label \"$title vs $smallgptitle\" at graph 0.1,0.1
plot \"$datafiles{$file}\" u $col:$ndata $with lt $lt title \"\"\n
unset label
 ";
			$lt++;
		    }
		    say {$gp} "unset multiplot\nset size 1,1\n";
		    
		    %datafiles=();
		}

	    }
	}
    }
}

sub get_data
{
    # get data in a hash from $dir
    my $dir = $_[0];
    my %hash;

    # scan log for numbers
    open(my $log,"<$dir/log")||die("cannot open $dir/log!");
    while(<$log>)
    {
	if(/Number of (\S+) = (\S+)/)
	{
	    $hash{$1}=$2;
	}
    }
    close $log;
    return %hash;
}



sub rungrid
{
    # run grid with extra options
    my $opts=$_[0]//'defaults';
    my $outdir=outdir($opts);

    # if we have already run (e.g. repeated defaults), do nothing
    state %already_run;
    return if($already_run{$outdir});
    $already_run{$outdir}=1;

    # run the grid
    print "Run grid with extra opts ",color('yellow'),$opts,color('reset')," to outdir $outdir\n";
    mkdirhier($outdir);
    my $cmd ="$grid $opts outdir=$outdir";
    print "CMD = $cmd\n";
    open(my $cmd,"ionice -c3 nice -n 19 $cmd |")||die("cannot open script");
    while(<$cmd>)
    {
	print $_;
    }
    close $cmd;
}

sub outdir
{
    # make output directory
    my $outdir = $_[0];
    $outdir=~s/\s+/_/go;
    return $ENV{VOLDISK}.'/data/antiTZ/'.$outdir;
}


sub get_defaults
{
    # get list of default variable settings
    my $defaults = `$grid dump_defaults`;
    $defaults=~s/.*Default variable settings\:\n//s;
    $defaults.="\n\n";
    my %defaults;
    map
    {
	if(/(\S+) \: (\S+) \: (\S+)/)
	{
	    $defaults{$1}{$2}=$3;
	}
    }split(/\n/,$defaults);
    return %defaults;
}

sub get_default
{
    # return default value of a variable
    my $varname = $_[0];
    return $defaults{bse_options}{$varname} // $defaults{grid_options}{$varname} // undef;
}
