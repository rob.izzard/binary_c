#!/usr/bin/env perl
use strict;

############################################################
#                                                          #
# Example of how to use the binary_grid flexigrid.         #
#                                                          #
############################################################

modules_etc();# load modules

my $results={}; # hash to store results

# first set some defaults for the grid
defaults();

# then parse the command line to override the defaults
parse_grid_args(@ARGV);

# now set up the grid
setup_binary_grid();

# run flexigrid with two threads
flexigrid(2); 

# output results
output(); 

exit(0); # done!

############################################################
############################################################
############################################################

sub output
{
    # output results
    foreach my $result (sort keys %$results)
    {
	next if($result eq 'thread_number');
	print "Saving results for output subhash '$result'\n";
	my $h=$$results{$result};
	
	open(FP ,">/tmp/imf.dat")||die;
	my $ptot=0.0; 
	foreach my $k (nsort(keys %{$h}))
	{ 
	    print FP "$k $$h{$k}\n";
	    $ptot+=$$h{$k};
	}
	close FP;

	print "Total probability $ptot\n"; 
    }
}

sub parse_bse
{
    # this gets called by the binary_grid module, it parses the 
    # lines of output from binary_c/nucsyn
    my $brk=0; 

    # you should set data in the $h hash pointer, which is 
    # then passed to the join_thread function (below) for you
    # to add up. (Note: $h is local to each thread.)
    my $h=$_[0];

    my $nthread=$binary_grid::grid_options{'thread_num'};

    while($brk==0)
    {
	$_=tbse_line();
	#print $_;
	chomp;
	if($_ eq 'fin')
	{
	    $brk=1; # the end of output
	   
	    {
		# do stuff once per star, e.g. add up initial mass function
		my $mbin=bin_data($binary_grid::grid_options{'progenitor_hash'}{'m1'},1.0);
		$$h{'IMF'}{$mbin} += $binary_grid::grid_options{'progenitor_hash'}{'prob'};
	    }
 	}
	else
	{
	    #...parse your data here ...
	    my @x=split(' ',$_);
	    # ... do something with @x, put results in $h ...
	    # e.g. 
	    #
	    # $$h{'luminosity distribution'}{rebin($x[1],1.0)}=$x[0];
	}
    }

}


sub defaults
{
    grid_defaults();
  
    # physics options
    $binary_grid::bse_options{'z'}=0.02;
    $binary_grid::bse_options{'wd_sigma'}=0.0;
    $binary_grid::bse_options{'c13_eff'}=1.0;
    $binary_grid::bse_options{'acc2'}=1.5;
    $binary_grid::bse_options{'tidal_strength_factor'}=1.0;
    $binary_grid::bse_options{'alpha_ce'}=1.0; # 1
    $binary_grid::bse_options{'lambda_ce'}=-1; # -1 = automatically set
    $binary_grid::bse_options{'lambda_ionisation'}=0.0; # 0.0
    $binary_grid::bse_options{'delta_mcmin'}=0.0;
    $binary_grid::bse_options{'lambda_min'}=0.0;
    $binary_grid::bse_options{'minimum_envelope_mass_for_third_dredgeup'}=0.5;
    $binary_grid::bse_options{'max_evolution_time'}=13700;
   
    # grid options
    $binary_grid::grid_options{'results_hash'}=$results;
    $binary_grid::grid_options{'parse_bse_function_pointer'}=\&parse_bse;
    $binary_grid::grid_options{'threads_stack_size'}=32; # MBytes
    $binary_grid::grid_options{'nice'}='nice -n +10'; # nice command e.g. 'nice -n +10' or '' 
    $binary_grid::grid_options{'binary'}=1; # single stars -> 0, binary stars -> 1
    $binary_grid::grid_options{'timeout'}=30; # seconds until timeout
    $binary_grid::grid_options{'log_args'}=1;
    $binary_grid::grid_options{'vb'}=1;

    # This is the path to the binary_c directory
    #$binary_grid::grid_options{'rootpath'}='directory';

    # The name of the binary_c executable
    #$binary_grid::grid_options{'prog'}='binary_c';

    print "You must set the rootpath and prog variables in binary_grid!
Once you have, you can safely remove this warning.\n";exit;

    # Mass 1
    my $nvar=0;
    my $mmin=0.1;
    my $mmax=80.0;
    my $n=4; # resolution
    $binary_grid::grid_options{'flexigrid'}{'grid variable '.$nvar++}=
    {
	'name'=> 'lnm1', 
	'longname'=>'Primary mass', 
	'range'=>["log($mmin)","log($mmax)"],
	'resolution'=> $n,
	'spacingfunc'=>"const(log($mmin),log($mmax),$n)",
	'precode'=>'my $m1=exp($lnm1); my $eccentricity=0.0;',
	'probdist'=>"ktg93(\$m1)*\$m1",
	'dphasevol'=>'$dlnm1'
    };

    # Binary stars: Mass 2 and Separation
    if($binary_grid::grid_options{'binary'})
    {
	my $m2min=0.1;
	$binary_grid::grid_options{'flexigrid'}{'grid variable '.$nvar++}=
	{
	    # name of the variable
	    'name'=>'m2',
	    # long name (for verbose logging)
	    'longname'=>'Secondary mass',
	    # range of the parameter space
	    'range'=>[$m2min,'$m1'],
	    # resolution 
	    'resolution'=>$n,
	    # constant grid spacing function
	    'spacingfunc'=>"const($m2min,\$m1,$n)",
	    # flat-q distribution between m2min and m1
	    'probdist'=>"const($m2min,\$m1)",
	    # phase volume contribution
	    'dphasevol','$dm2'
	};

	
	$binary_grid::grid_options{'flexigrid'}{'grid variable '.$nvar++}=
	{
	    # name of the variable
	    'name'=>'lnsep',
	    # long name (for verbose logging)
	    'longname'=>'ln(Orbital_Separation)',
	    # range of the separation space
	    'range'=>['log(3.0)','log(1e4)'],
	    # resolution
	    'resolution'=>$n*10,
	    # constant spacing in ln-separation
	    'spacingfunc'=>"const(log(3.0),log(1e4),$n)",
	    # precode has to calculation the period (required for binary_c)
	    'precode'=>"my\$sep=exp(\$lnsep);my\$per=calc_period_from_sep(\$m1,\$m2,\$sep);",
	    # flat in log-separation (dN/da~1/a) distribution (Opik law)
	    'probdist'=>'const(log(3.0),log(1e4))',
	    # phase volume contribution
	    'dphasevol'=>'$dlnsep'
	}
    }
}


sub modules_etc
{
    $|=1; # enable this line for auto-flushed output
    use strict;
    use rob_misc;
    use threads;
    use Sort::Key qw(nsort);
    use 5.16.0;
    use binary_grid;
    use threads::shared;
    use Carp qw(confess);
    use Proc::ProcessTable;
}
