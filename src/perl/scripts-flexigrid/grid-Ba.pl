#!/usr/bin/env perl 

$|=1;
use strict;
use rob_misc;
use binary_grid;
use binary_grid::C;
#use binary_grid::Perl;
use binary_grid::condor;
use threads;
use threads::shared;

# timings with $n=10
#
# C grid :  
#
# 16 : 80178.89user 439.02system 1:26:43elapsed 1549%CPU (0avgtext+0avgdata 7411104maxresident)k

# perl grid :
# 16 : 27056.12user 16058.59system 2:57:03elapsed 405%CPU (0avgtext+0avgdata 195504maxresident)k


use Histogram;
use Sort::Key qw(nsort);
use Data::Dumper;
#
# Grid to calculate statistics regarding the nature of
# binary barium star systems 
#
# set up data structures
my $results={};
print "RESULTS IN $results\n";
my $outdir;

# first set some defaults for the grid
defaults();

# then parse the command line to override the defaults
parse_args(); 

# ... and set up dependent variables
secondary_variables();

# now set up the grid
setup_binary_grid();

my $nthreads=ncpus();
#$nthreads -= 4 if($nthreads>10);
flexigrid($nthreads); # run flexigrid on $nthreads threads
#condor_grid(); # run on condor

output();

exit(0);

############################################################
############################################################
############################################################

sub parse_bse
{
    # this gets called by the binary_grid module, it parses the 
    # lines of output from binary_c/nucsyn
    my $brk=0; 

    # you should set data in the $h hash pointer, which is 
    # then passed to the join_thread function (below) for you
    # to add up

    # if you want to use the binary_cd you should make sure this function
    # contains all the data it needs because the remote machine will
    # want to do data processing on the fly!
    my $h = shift;
    
    if(!defined $h->{histograms})
    {
	# set up thread histograms
	make_new_histograms($h);
    }

    while($brk==0)
    {
	$_=tbse_line();
        next if(/^Tick/);
	#print "TBSE_LINE >> ",$_,"\n";
	chomp;

        $h->{nlines}++;

	if($_ eq 'fin')
	{
	    $brk=1; # the end of output
	}
	elsif(/GIANT/o)
	{
	    #print "LINE $_\n";
 	    s/^(\S+) //;
	    my $id=$1;
	    my @x = split(/\s+/,$_); # splits $_ into the array @_
	    my $dtp=$x[1]*$x[2];
	    my $p = $x[3];
	    my $ecc = $x[4];

            $h->{'nlines_'.$id}++;

	    if($p>0.0 && $ecc>=0.0)
	    {
                # binary stars
                my $logp = log10($p);

                if($id eq 'GK_GIANT')
		{ 
		    $h->{number_counts}{'GK giant'}+=$dtp;
                    $h->{number_counts}{'GK giant binary'}+=$dtp;
                    $h->{histograms}{'GK giant binary logP-e'}->add_data($logp,$ecc,$dtp);
                }
                elsif($id=~/^BARIUM(strong|mild)_GIANT/)
		{ 
                    my $type = $1;
		    $h->{histograms}{'Ba giant binary logP-e'}->add_data($logp,$ecc,$dtp);
		    $h->{histograms}{'Ba giant binary '.$type.' logP-e'}->add_data($logp,$ecc,$dtp);
		    $h->{histograms}{'Ba giant binary '.$type.' logP-e'}->add_data($logp,$ecc,$dtp);
		    $h->{number_counts}{'Ba giant binary '.$type}+=$dtp;
                    $h->{number_counts}{'Ba giant '.$type}+=$dtp;
		    $h->{number_counts}{'Ba giant'}+=$dtp;
		}
	    }
            else
            {
                # single stars
                if($id eq 'GK_GIANT')
                {
                    $h->{number_counts}{'GK giant'} += $dtp;
                    $h->{number_counts}{'GK giant single'} += $dtp;
                }
                elsif($id=~/^BARIUM(strong|mild)_GIANT/)
                {  
                    my $type = $1;
		    $h->{number_counts}{'Ba giant single '.$type}+=$dtp;
                    $h->{number_counts}{'Ba giant '.$type}+=$dtp;
		    $h->{number_counts}{'Ba giant'}+=$dtp;
                }
            }
	}
    }
}



sub defaults
{
    grid_defaults();

    # set up a pointer to the parser function (see parse_bse())
    $binary_grid::grid_options{'parse_bse_function_pointer'}=\&parse_bse;

    # condor
    $binary_grid::grid_options{condor_memory}=1000;
    $binary_grid::grid_options{condor_njobs}=30;
    $binary_grid::grid_options{condor_dir}=$ENV{HOME}.'/data/condor/barium';
    $binary_grid::grid_options{reset_stars_defaults}=1;
    
    # code options
    $binary_grid::grid_options{'nice'}='';#'nice -n +10'; 
    $binary_grid::grid_options{'timeout'}=3000; # seconds until timeout
    $binary_grid::grid_options{'thread_max_freeze_time_before_warning'}=60;
    $binary_grid::grid_options{'log_args'}=0;
    $binary_grid::grid_options{'save_args'}=0;
    $binary_grid::grid_options{'vb'}=1;
    $binary_grid::grid_options{'prog'}='binary_c-Ba';
    $binary_grid::bse_options{'log_filename'}='/dev/null';

    # physics
    $binary_grid::bse_options{'no_thermohaline_mixing'}=0;
    $binary_grid::bse_options{'z'}=0.008;
    $binary_grid::bse_options{'wd_sigma'}=0.0;
    $binary_grid::bse_options{'c13_eff'}=1.0;
    $binary_grid::bse_options{'acc2'}=1.5;
    $binary_grid::bse_options{'alpha_ce'}=0.25; # 0.25
    $binary_grid::bse_options{'lambda_ce'}=-1; # -1 = automatically set (Tauris)
    $binary_grid::bse_options{'lambda_ionisation'}=0.0; # 0.0
    $binary_grid::bse_options{'initial_abundance_mix'}=2; # 2 = lodders (required!)
    $binary_grid::bse_options{'rotationally_enhanced_mass_loss'}=0;
    $binary_grid::bse_options{'comenv_ms_accretion_mass'}=0.0;
    $binary_grid::bse_options{'jorb_loss'}=0;
    $binary_grid::bse_options{'lw'}=1.0;
    $binary_grid::bse_options{'tidal_strength_factor'}=1.0;

    # extra 3DUP
    $binary_grid::bse_options{'delta_mcmin'}=-0.4;
    $binary_grid::bse_options{'lambda_min'}=0.5;
    $binary_grid::bse_options{'minimum_envelope_mass_for_third_dredgeup'}=0.0;
    $binary_grid::bse_options{'max_evolution_time'}=13700.0;
    
    # canonical 
    #$binary_grid::bse_options{'delta_mcmin'}=0.0;
    #$binary_grid::bse_options{'lambda_min'}=0.0;
    #$binary_grid::bse_options{'minimum_envelope_mass_for_third_dredgeup'}=0.5;


    $binary_grid::grid_options{results_hash}=$results;

#    $binary_grid::grid_options{flexigrid}{'grid type'} = 'list';
#    $binary_grid::grid_options{flexigrid}{'list filename'} = './starlist';


    # for debugging
    my $debug = 0;
    
    $binary_grid::grid_options{sort_args}=$debug; 
    $binary_grid::grid_options{log_args}=$debug; 
    $binary_grid::grid_options{threacomms}=$debug;
    $binary_grid::grid_options{save_args}=$debug;;
    
    my $r=10;

    if(0)
    {
        distribution_functions::bastard_distribution({
            mmin=>0.7,
            mmax=>6.0,
            m2min=>0.1,
            qmin=>0.0,
            qmax=>1.0,
            useecc=>1,
            nm1=>10,#40
            nm2=>4*$r,#40
            nper=>4*$r,#100
            necc=>4*$r,#40
                                                     });
    }
    else
    {
        my $nvar=0;
        my $mmin=0.1;
        my $mmax=80.0;
        my $n=1000; # resolution
        $binary_grid::grid_options{'flexigrid'}{'grid variable '.$nvar++}=
        {
            'name'=> 'lnm1', 
            'longname'=>'Primary mass', 
            'range'=>["log($mmin)","log($mmax)"],
            'resolution'=> $n,
            'spacingfunc'=>"const(log($mmin),log($mmax),$n)",
            'precode'=>'my $m1=exp($lnm1); my $eccentricity=0.0;',
            'probdist'=>"ktg93(\$m1)*\$m1",
            'dphasevol'=>'$dlnm1'
        };
    }

}

sub parse_args
{
    # parse command line arguments
    binary_grid::parse_grid_args(@ARGV);
}

sub secondary_variables
{
    # from the defaults and/or passed in variables,
    # calculate useful things

    # default outdir if not given
    $outdir = $main::outdir;
    $outdir //= '/'. (-d '/home/izzard' ? 'home' : 'users') .'/izzard/data/ba2/';

    # use colour?
    if($binary_grid::grid_options{'colour'}==1)
    {
        # use ANSI color for output to xterms/rxvt etc.
        use Term::ANSIColor;
        # set to white on black
        print color("white on_black");
    }
    else
    {
        # dummy function to replace the ANSI color() function
        eval("sub\ color\{ \}");
    }
}


sub output
{
    # output
    return if($binary_grid::grid_options{condor_command} eq 'run_flexigrid');
    print "OUTPUT to $outdir\n";
    mkdirhier($outdir);

    open(my $out, '>', "$outdir/log");
    if($out)
    {
	print {$out} Dumper(\%binary_grid::grid_options);
	print {$out} Dumper(\%binary_grid::bse_options);
    }
    close $out;

    my %histograms = %{$results->{histograms}};
    
    foreach my $k (keys %histograms)
    {
	# make histogram
	my $f="$outdir/$k";
	$f=~s/\s+/_/g;

	my $histogram = $histograms{$k};

	print "Dump histogram $k (object $histograms{$k} or $results->{histograms}{$k}, ref ",ref $histogram,") to file $f\n";


	$histogram->format('gnuplot_surface');
	$histogram->dump($f.'.surf');

	$histogram->format('gnuplot');
	$histogram->dump($f.'.dat');

    }

    my %number_counts = %{$results->{number_counts}};

    print "Output number counts\n";
    open(FP,">$outdir/number_counts")||die("cannot open $outdir/number_counts");
    foreach my $k (sort keys %number_counts)
    {
	# number count
	printf "SCALAR : %s = %g\n",$k,$number_counts{$k};
	printf FP "%s = %g\n",$k,$number_counts{$k};
    }
    close FP;

    print "----------------------------------------
testing
----------------------------------------
\%results:
";
    foreach my $k (grep {/nlines/} sort keys %$results)
    {
        print "$k : $results->{$k}\n";
    }


    print "------------------------------------------------------------\ndone\n\n";
}
sub make_new_histograms
{
    my $h=$_[0]; # thread-private results_hash

    foreach my $hist ('Ba giant binary logP-e',
		      'Ba giant binary strong logP-e',
		      'Ba giant binary mild logP-e',
		      'GK giant binary logP-e'
	)
    {
	$h->{histograms}{$hist} = P_e_histogram(); 
    }
    $h->{number_counts}={};
}

sub P_e_histogram
{
    my $h = Histogram->new;
    $h->binwidths(0.2,0.05);
    return $h;
}



sub fix_aifa_environment
{
    if(`hostname`=~/aibn/)
    {
	map
	{
	    #print "CHECK $_\n";
	    if($ENV{$_}=~s!/export/aibn(\d+)_(\d+)/!/vol/aibn$1/aibn$1\_$2/!g)
	    {
		#print "CONV $_=$ENV{$_}\n";
	    }
	}%ENV;
    }
}
