#!/usr/bin/env perl
use strict;
use 5.16.0;
use Carp qw(confess);

# given either a flexigrid condor tmp directory - or 
# using the current directory - restart condor jobs

my $dir = $ARGV[0] || $ENV{PWD};

say "Restarting condor jobs from $dir";

chdir $dir;
my @restart;

opendir (my $statusdir,'status') ||confess("cannot open status directory for reading");
while(my $f = readdir $statusdir)
{
    next if ($f=~/^\./);
    #say "Dir entry $f";
    
    open(my $status_fp , "<status/$f") || confess("cannot open status/$f");
    if(<$status_fp> =~ /finished/)
    {
	#say "finished";
    }
    else
    {
	#say "not finished!";
	push(@restart,$f);
    }

    close $status_fp;
  
}
closedir $statusdir;

say "Resubmit jobs ? @restart\n<y/N>" ;

if(<STDIN>=~/y/io)
{ 
    foreach my $job (@restart)
    {
	say "Resubmitting $job ... ";
	my $scriptfile = "scripts/$job";
	if(-e $scriptfile && -f $scriptfile && -s $scriptfile>10)
	{
	    my $cmd="condor_submit $scriptfile";
	    say "EXEC $cmd";
	    `$cmd`;
	    say "Done (re)submit $job\n";
	}
    }
}
say;

