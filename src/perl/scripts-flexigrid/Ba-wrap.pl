#!/usr/bin/env perl
use strict;
use rob_misc;



foreach my $tidal_strength_factor (1e-3)
{
    my $outdir = "$ENV{HOME}/data/ba2-tides-$tidal_strength_factor";
    my $cmd = "nice -n +19 ionice -c3 ./src/perl/scripts-flexigrid/grid-Ba.pl tidal_strength_factor=$tidal_strength_factor outdir=$outdir";
    print $cmd,"\n";
    rob_misc::runcmd($cmd,'screen');

}


