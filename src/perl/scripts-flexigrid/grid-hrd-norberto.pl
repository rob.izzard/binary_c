#!/usr/bin/env perl 
modules_etc();
use 5.16.0;
use Hash::RobMerge qw/arithmetically_add_hashes/;

################################################################################
# grid script for the binary_c/nucsyn code: HRD project with Norbert and Luca. #
################################################################################

# important global options and variables
my $maxlogt;
my $outdir;
my $vb=1; # verbosity 
my $snapshots=0; # set to 1 to output time snapshots (lots of disk space!)
my $save_inits=0; # set to 1 to save initial information

# resolution settings

# first set parameters from the command line
my $testing = ("@ARGV"=~/testing=(\d)/)[0];
my $nres = ("@ARGV"=~/nres=(\S+)/)[0];
my $fshannon = ("@ARGV"=~/fshannon=(\S+)/)[0];
my $maxlogt = ("@ARGV"=~/maxlogt=(\S+)/)[0];
my %binsizes;

# defaults if no cmd line arg given
#
$testing//=0; # testing flag
$nres //= $testing ? 40 : 100; # m1=this*100; raw gives m2, period resolution (100) nres = 3,10,20,40
$fshannon //= $testing ? 0.25 : 0.1; # sampling multiplier (0.1) fshannon=0.1,0.25,0.5,1.0
$maxlogt //= $testing ? 7.0 : 10.11; # maximum (log10) time (10.11) (set to 7 for debugging, 10.11 = 13Gyr, 10.25 ~ 17Gyr)

print "Send HRD grid to condor : testing=$testing, nres=$nres, fshannon=$fshannon, maxlogt=$maxlogt (ARGV @ARGV)\n";

my %results; # results hash

############################################################
# Choose distribution of stars:
#
# separate = single or binary 
# bastard = one (single+binary) population
#my $distribution_types = 'bastard'; 
my $distribution_types='separate';

############################################################
# Setup
############################################################
fix_aifa_environment();
defaults();
parse_grid_args(); 
setup_binary_grid();

############################################################
# run grid
############################################################
flexigrid(16);
#condor_grid();

############################################################
# Output
############################################################
output();

exit(0);

############################################################
############################################################
############################################################

sub output
{
    if($binary_grid::grid_options{condor_command} eq 'run_flexigrid')
    {
	print "Skip output because I am running flexigrid\n";
    }
    else
    {
	# build $outdir : the output directory
	$outdir = $binary_grid::grid_options{condor_dir};
	$outdir =~ s!^/users/izzard/data/condor/hrd-!$ENV{VOLDISK}/data/norberto/hrd/condor-!;

	mkdirhier($outdir);
	print "Made outdir=$outdir\n";

	print "Pre-output_options_hashes mem use = ",proc_mem_usage(),"\n";
	output_options_hashes();
	print "Pre-outhash mem use = ",proc_mem_usage(),"\n";
	outhash($results{hrd_norberto},'resolved-norberto'); #works!
	print "Pre-outsumhash mem use = ",proc_mem_usage(),"\n";
	outsumhash($results{sum}); #?
	print "Post-outsumhash mem use = ",proc_mem_usage(),"\n";

	outinit();

	print "Pre-Final mem use = ",proc_mem_usage(),"\n";
	%results=undef;
	print "Final mem use = ",proc_mem_usage(),"\n";
    }
}

sub join_thread
{
    # add thread's data to the global results hash

    my $h=$_[0];
    my $threadinfo=$_[1];

    print "Join thread mem use = ",proc_mem_usage(),"\n";
    Hash::RobMerge::arithmetically_add_hashes(\%results,$h);
    print "Post-Join thread mem use = ",proc_mem_usage(),"\n";
    undef $h;
    print "Post-Join thread (post undef) mem use = ",proc_mem_usage(),"\n";

    return (@_);
}

sub parse_bse
{
    # this gets called by the binary_grid module, it parses the 
    # lines of output from binary_c/nucsyn
    my $brk=0; 
    my $h=$_[0];

    while($brk==0)
    {
	$_=tbse_line();


	if(s/^HRD2 (\S+) //o)
	{
	    # second version of HRD : this time with loggeff
	    my $logt=$1; # log10 time
	   
	    if($logt<=$maxlogt)
	    { 
		my @x=split(' ',$_);
		my ($dtp, # dt * probability = weighting
		    $fill, # timestep fill factor
		    $pf, # probability * fill factor - good for debugging
		    $pburning, # ditto if nuclear-burning at this time
		    $m, # mass * probability = mass weight
		    $mburning, # m *probability if nuclear-burning at this time
		    $L,
		    $Lburning)=splice(@x,0,8);
		#my $lsum=0.0; # debug variable

		# modulate dtp with SFR
		$dtp *= SFR($binary_grid::bse_options{max_evolution_time}-10.0**$logt);
		
		# save sums of dtp,p,m as f(time)
		$$h{sum}{dtp}{$logt}+=$dtp;
		$$h{sum}{p}{$logt}+=$pf;
		$$h{sum}{pburning}{$logt}+=$pburning;
		$$h{sum}{m}{$logt}+=$m;
		$$h{sum}{mburning}{$logt}+=$mburning;
		$$h{sum}{L}{$logt}+=$L;
		$$h{sum}{Lburning}{$logt}+=$Lburning;

		# star 1 : select only non-NS/BH/massless
		if($x[0]<13)
		{		    
		    # Leff=[Teff^4/g] (already binned) vs log(teff) 
		    # 0.02 bins for log(teff)
		    my $s_norberto = $logt.' '.bin_data(log10($x[1]),0.02).' '.bin_data($x[4],0.02);
		    $$h{hrd_norberto}{$s_norberto}+=$dtp;

		}
		
		# star 2 
		my $offset=6; # = num columns of star 1's output
		if(defined($x[$offset+1]) && ($x[$offset]<13))
		{
		    my $s_norberto = $logt.' '.bin_data(log10($x[$offset+1]),0.02).' '.bin_data(log10($x[$offset+4]),0.02);
		    $$h{hrd_norberto}{$s_norberto}+=$dtp;
		}
		
	    }
	}
	elsif(s/^HRDINIT //o)
	{
	    if($save_inits)
	    {
		# save initial distributions
		my @x=split(' ',$_);
	
		# @x = 0=M1, 1=M2, 2=P, 3=a, 4=prob
		if($x[2]<1e45)
		{
		    # binary star
		    my $y = bin_data($x[1]/$x[0],0.1);
		    $$h{initial}{'q_gt_0.9'}{$y} += $x[4] if($x[0]>0.9);
		    $$h{initial}{q}{$y} += $x[4];

		    # calc once
		    $x[0] = bin_data($x[0],0.1);
		    $x[2] = bin_data(log10($x[2]),0.5);

		    $$h{initial}{M1}{bin_data($x[0],0.1)} += $x[4];
		    $$h{initial}{M2}{bin_data($x[1],0.1)} += $x[4];
		    $$h{initial}{P}{$x[2]} += $x[4];
		    $$h{initial}{M_P}{$x[0].' '.$x[2]} += $x[4];
		    $$h{initial}{M_a}{$x[0].' '.$x[2]} += $x[4];
		    $$h{initial}{a}{bin_data(log10($x[3]),0.5)} += $x[4];
		}
		else
		{
		    # single star (has a stupidly long initial period)
		    $$h{initial}{M}{bin_data($x[0],0.1)} += $x[4];
		}
	    }
	}
	elsif($_ eq 'fin')
	{
	    $brk=1; # the end of output
	}
    }
}

sub defaults
{
    $SIG{PIPE}='IGNORE';
    grid_defaults();
    renice_me(5); # play nice

    # metallicity : set early to make directory names
    $binary_grid::bse_options{z} = $main::Z // 0.019;

    print "Run grid with Z=$binary_grid::bse_options{z}\n";
    
    # Z = 0.0001 *, 0.0002, 0.0004, 0.0006, 0.0008, 0.001*, 0.002, 0.004, 0.006, 0.008, 0.010, 0.015, 0.020*, 0.025, 0.030

    my $host=`hostname`;chomp $host;
    say "Host $host";

    # single or binary? only used if $distribution_types is set to separate
    # otherwise overridden
    $binary_grid::grid_options{binary}=0;

    # output directory
    if($host=~/aibn/ || $host=~/klaipeda/ || $host=~/science\d+/) 
    {
	# at the aifa
	$binary_grid::grid_options{condor_dir}='/users/izzard/data/condor/norberto/hrd-Z'.$binary_grid::bse_options{z};
	say "Running on $host : condor_dir=",$binary_grid::grid_options{condor_dir},"\n";

#	if($testing)
	{
	    my $post = sprintf "-N%d-samp%g",$nres,$fshannon;
	    $binary_grid::grid_options{condor_dir} .= $post;
	}
    }
    else
    {
	# elsewhere has a standard home directory setup
	$binary_grid::grid_options{condor_dir}='/home/izzard/data/condor/norberto/hrd-Z'.$binary_grid::bse_options{z};
    }

    if($distribution_types eq 'separate')
    {
	# if we're not using a mixed population, add -single or -binary
	$binary_grid::grid_options{condor_dir} .= '-'.
	    $binary_grid::grid_options{binary} ? 'binary' : 'single' 
    }
    else
    {
	# bastard distribution: blended single/binary
	$binary_grid::grid_options{condor_dir} .= '-bastard';
    }

    if($host=~/klaipeda/)
    {
	# klaipeda : postpone join, do it on aibn36
	$binary_grid::grid_options{condor_postpone_join}=1;
	$binary_grid::grid_options{condor_njobs}=64;
    }
    else
    {
	# normal PC
	# postpone join, uses more RAM than on the condor machines
	$binary_grid::grid_options{condor_postpone_join}=1;
	
	# condor requirements
	$binary_grid::grid_options{condor_options}{Requirements}=
	    'Machine!="aibn73.astro.uni-bonn.de" && Machine!="aibn79.astro.uni-bonn.de"';
# && Machine!="klaipeda1.astro.uni-bonn.de" && Machine!="klaipeda2.astro.uni-bonn.de" && Machine!="klaipeda3.astro.uni-bonn.de" && Machine!="klaipeda4.astro.uni-bonn.de"';

	$binary_grid::grid_options{condor_memory}=0;
	$binary_grid::grid_options{condor_options}{ImageSize}=1024;
	$binary_grid::grid_options{condor_njobs}=MAX(10,condor_free());
	$binary_grid::grid_options{condor_njobs}=10; # testing
	say "Use $binary_grid::grid_options{condor_njobs} condor slots"; 
    }
    
    $binary_grid::grid_options{prog}='binary_c-hrd';
    my $version=evcode_version_string();
    $binary_grid::grid_options{results_hash}=\%results;

    # set up a pointer to the parser function (see parse_bse())
    $binary_grid::grid_options{parse_bse_function_pointer}=\&parse_bse;
    $binary_grid::grid_options{threads_join_function_pointer}=\&join_thread;

    $binary_grid::grid_options{log_args}=0; # log args for debugging (not production runs!)
    $binary_grid::grid_options{save_args}=0; 

    $binary_grid::grid_options{nmod}=10; # log every 1000 models
    $binary_grid::grid_options{log_dt_secs}=5; # log every 5 minutes

    $binary_grid::bse_options{'max_evolution_time'}=10.0**($maxlogt-6.0+0.15); # up to 10**10.2 yr + next bin

    if($testing)
    {
	# if testing, prevent SN kick monte carloness
	$binary_grid::bse_options{'sn_sigma'}=0;
	$binary_grid::bse_options{'wd_sigma'}=0;
    }
    
    # 1MYR overkill resolution
    $binary_grid::bse_options{'maximum_timestep'}=1.0;

    # code options
    $binary_grid::grid_options{'nice'}='nice -n +10'; # nice command
    $binary_grid::grid_options{'ionice'}='ionice -c3'; # ionice command

    $binary_grid::grid_options{'timeout'}=0; # 0=off
    $binary_grid::grid_options{'vb'}=1; 
    $binary_grid::grid_options{'tvb'}=0; 
    $binary_grid::grid_options{'thread_max_freeze_time_before_warning'}=120; # lots of output = slow
    $binary_grid::grid_options{'threads_stack_size'}=1024; # big stack for lots of data

    # Schroeder + Cuntz : gbwind=1, gb_reimers_eta=1.0
    $binary_grid::bse_options{gbwind}=0;
    $binary_grid::bse_options{gb_reimers_eta}=10.0;

    # physics
    $binary_grid::grid_options{'dredge_up_calibration'}=0;

    # automatically set log10 bin
    %binsizes=(Teff=>($version=~/HRDIAG_BIN_SIZE_TEFF=(\S+)/)[0],
	       logL=>($version=~/HRDIAG_BIN_SIZE_LOGL=(\S+)/)[0],
	       logg=>($version=~/HRDIAG_BIN_SIZE_LOGG=(\S+)/)[0],
	       Leff=>($version=~/HRDIAG_BIN_SIZE_LOGL=(\S+)/)[0],
	       dlog10time=>($version=~/HRDIAG_BIN_SIZE_LOGTIME=(\S+)/)[0]);

    my %resolution=('m'=>$nres*100,
		    'm1'=>$nres,
		    'm2'=>$nres, # (100)
		    'per'=>$nres, # (100)
		    'log10time'=>$binsizes{dlog10time}*$fshannon);
    
    # Mass 1
    my $nvar=0;
    my $mmin=0.1; # 0.1
    my $mmax=100.0; # 80.0

    # Binary stars: Mass 2 and Separation
    if($distribution_types eq 'bastard')
    {
	# new bastard distribution of single and binary stars

	distribution_functions::bastard_distribution({
	    mmin=>$mmin,
	    mmax=>$mmax,

	    # use time-adaptive mass resolution for M1
	    time_adaptive=>{
		mass_grid_log10_time=>1,
		mass_grid_log10_step=>$resolution{log10time},
		mass_grid_step=>100,
		extra_flash_resolution=>0,
		mass_grid_nlow_mass_stars=>10,
		debugging_output_directory=>undef,
		max_delta_m=>2.0
	    },	    

	    m2min=>$mmin,
	    nm2=>$resolution{m2},
	    nper=>$resolution{per},
	    necc=>undef,
	    qmin=>0.0,
	    qmax=>1.0,
	    useecc=>undef	    
						     });
    }
    elsif($distribution_types eq 'separate')
    {
	# traditional single- or binary-star population
	my $nm1=$binary_grid::grid_options{binary} ? $resolution{m1} : $resolution{m};
	$binary_grid::grid_options{'flexigrid'}{'grid variable '.$nvar++}=
	{
	    'name'=> 'lnm1', 
	    'longname'=>'Primary mass', 
	    'range'=>["log($mmin)","log($mmax)"],
	    'resolution'=> $nm1,

	    ## const dm spacing function
	    'spacingfunc'=>"const(log($mmin),log($mmax),$nm1)",

	    # use const_dt function
	    #'spacingfunc'=>"const_dt(\$const_dt_opts,'next')",
#	    'preloopcode'=>"
#my \$const_dt_opts=\{
#    max_evolution_time=>20000,
#    stellar_lifetime_table_nm=>1000,
#    nthreads=>4,
#    thread_sleep=>1,
#    mmin=>0.1,
#    mmax=>100.0,
#    time_adaptive_mass_grid_log10_time=>1,
#    time_adaptive_mass_grid_log10_step=>$resolution{log10time},
#    time_adaptive_mass_grid_step=>100,
#    extra_flash_resolution=>0,
#    time_adaptive_mass_grid_nlow_mass_stars=>40,
#    debugging_output_directory=>undef,
#    max_delta_m=>1.0,
#    savegrid=>undef,
#    vb=>0,
#\};
#spacing_functions::const_dt(\$const_dt_opts,'reset');
#",

	    # and its resolution
	    #'resolution'=> "spacing_functions::const_dt(\$const_dt_opts,'resolution');",


	    'precode'=>'my $m1=exp($lnm1); my $eccentricity=0.0;',
	    #'probdist'=>"Kroupa2001(\$m1)*\$m1",
	    'probdist'=>"ktg93(\$m1)*\$m1",
	    'dphasevol'=>'$dlnm1'
	};

	if($binary_grid::grid_options{'binary'})
	{
	    # q-distribution : always flat
	    my $m2min=0.1;
	    $binary_grid::grid_options{'flexigrid'}{'grid variable '.$nvar++}=
	    {
		'name'=>'m2',
		'longname'=>'Secondary mass',
		'range'=>[$m2min,'$m1'],
		'resolution'=>$resolution{m2},
		'spacingfunc'=>"const($m2min,\$m1,$resolution{m2})",
		'probdist'=>"const($m2min,\$m1)",
		'dphasevol','$dm2'
	    };

	    # period distribution : rob's bastard
	    $binary_grid::grid_options{'flexigrid'}{'grid variable '.$nvar++}=
	    {
		'name'=>'log10per',
		'longname'=>'log10(Orbital_Period)',
		'range'=>['-1.0','10.0'],
		'resolution',$resolution{per},
		'spacingfunc',"const(-1.0,10.0,$resolution{per})",
		'precode'=>"my \$per=10.0**\$log10per;my\$sep=calc_sep_from_period(\$m1,\$m2,\$per);",
		'probdist'=>'Izzard2012_period_distribution($per,$m1)',
		'dphasevol'=>'$dlog10per'
	    }
	}
    }
    else
    {
	print "Unknown distribution type\n";
	exit;
    }



    # make sure the directories are made!
    mkdirhier($binary_grid::grid_options{condor_dir});
}


sub output_options_hashes
{
    open(FP,">$outdir/settings")||confess("cannot open $outdir/settings for output");
    print FP options_string();
    close FP;
}

sub outhash
{
    my $hash=$_[0];
    return if(!defined $hash);
    my $name=$_[1];
    print "Hash : $name (mem=",proc_mem_usage(),") var=$hash\n";

    open(OP,">$outdir/$name")||confess("cannot open $outdir/$name for output");    
    print OP "# log(time) Teff log(L/Lsun) log(g) weight\n";
    # output resolved systems
    my $t=-1.0;
    
    # this is the memory intensive loop!
    foreach my $k (sort rob_misc::numerical_list_sorter 
		   (keys %$hash))
    {
	my $kk=$k;
	if($k=~s/^(\S+)\s+//o)
	{
	    if($1 != $t)
	    {
		$t=$1;
		if($t<=$maxlogt)
		{
		    print OP "\n";
		    if($snapshots)
		    {
			close FP;
			my $f="$outdir/$name.$t";
			open(FP,'>'.$f)||confess("cannot open $f for output\n");
			print FP "# snapshot at log(time)=$t\n# Teff log(L/Lsun) log(g) weight\n";
		    }
		}
	    }

	    if($t<=$maxlogt)
	    {
		my $l=$k.' '.$$hash{$kk}."\n";
		print OP $t,' ',$l; 
		print FP $l if($snapshots);
	    }
	}
    }
    close FP if($snapshots);
    close OP;
}


sub outsumhash
{

    my $h=$_[0];  # hash of summed data
    my $st=$_[1];  # stellar type
    #print "OUT SUM HASH $h\n";
    foreach my $k (keys %$h)
    {
	if(defined($st))
	{
	    print "Out sumhash for st=$st, key $k\n";
	    my $f="$outdir/sum.$k-$st";
	    open(FP,'>'.$f)||confess("cannot open $f for output\n");
	    print FP "# $k vs time (stellar type $st)\n";
	}
	else
	{
	    print "Out sumhash for key $k (no \$st)\n";
	    my $f="$outdir/sum.$k";
	    open(FP,'>'.$f)||confess("cannot open $f for output\n");
	    print FP "# $k vs time\n";
	}

	# output at all times
	map
	{
	    print FP $_,' ', (defined($$h{$k}{$_}) ? $$h{$k}{$_} : '0'), "\n"if($_<=$maxlogt);
	}nsort(keys %{$$h{$k}});
	close FP;	
    }
}

sub SFR
{
    return 1.0; # constant SFR

    # SFR as a function of Galactic age in Myr
    #my $t=$_[0];
    #return $t<1.0e3 ? 1.0 : 0.0; # const for a given time period
    #return exp(-$t/$opts{'SF timescale'}); # exponential
}

sub outinit
{
    # output initial distributions
    mkdir "$outdir/init";
    my $h=$results{initial};
    foreach my $init (keys %$h)
    {
	print "Out init $init\n";
	open(FP,">$outdir/init/$init.dat")||confess("cannot open $outdir/init/$init.dat for output");

	if($init eq 'M_P' || $init eq 'M_a')
	{
	    # normalize to M1 (which we have saved) 
	    my $mwas=undef;
	    my $cwas=undef;
	    my $tot=0.0;
	    map
	    {
		my $m=($_=~/^(\S+)/o)[0];
		$$h{$init}{$_} /= $$h{M1}{$m};
		if(defined($mwas)&&($m!=$mwas))
		{
		    $$h{$init}{$cwas} .= "\n# tot=$tot\n";
		    $tot=0.0;
		}
		else
		{
		    $tot+=$$h{$init}{$_};
		}		
		$mwas=$m;
		$cwas=$_;
	    }sort rob_misc::numerical_list_sorter keys %{$$h{$init}};
	}

	map
	{
	    print FP $_,' ',$$h{$init}{$_},"\n";
	}sort rob_misc::numerical_list_sorter keys %{$$h{$init}};
	close FP;
    }
}

sub modules_etc
{
    $|=1; # enable this line for auto-flushed output
    print "load modules\n";
    use strict;
    use rob_misc;
    use threads;
    use Sort::Key qw(nsort);
    use 5.16.0;
    use binary_grid;
    use binary_grid::condor;
    use threads::shared;
    use Carp qw(confess);
    use Proc::ProcessTable;
    use Hash::RobMerge;
    print "module loaded\n";
}

sub fix_aifa_environment
{
    if(`hostname`=~/aibn/)
    {
	map
	{
	    print "CHECK $_\n";
	    if($ENV{$_}=~s!/export/aibn(\d+)_(\d+)/!/vol/aibn$1/aibn$1\_$2/!g)
	    {
		print "CONV $_=$ENV{$_}\n";
	    }
	}%ENV;
    }
}

sub condor_free
{
    # return number of free condor slots
    my $cmd='condor_status|grep -A2 Claimed\ Unclaimed|tail -1|gawk "{print \$5}"';
    my $n=`$cmd`;
    chomp $n;
    return $n;
}
