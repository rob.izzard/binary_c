#!/usr/bin/perl


# script to compare two binary_c trees and tell
# you which source files are different
#
# usage:
#
# compare_tree.pl <tree 1> <tree 2>
#

use strict;
use Term::ANSIColor;
$|=1;

my @trees=@ARGV;
my %filehash;
my @output;

my %log=(
    'all'=>0, # log everything
    'missing'=>0, # log missing files
    'identical'=>0, # log identical files
    'vb'=>0, # verbose logging
    'diff'=>1, # log differences (should always be 1)
    );
my $colour=0; # set to 1 to use ANSI colours, 0 for no colours

# ANSI Term colours
my %colours;
make_colours() if($colour);

# log all if selected
map{$_=1}%log if($log{all});

# make file list
foreach my $tree (@trees)
{
    make_filehash($tree);
}

print "============================================================\n";

# do comparison
compare_trees(@trees);

print "============================================================\n";
print "Done\n";
print "============================================================\n";


exit;

sub make_filehash
{
    my $tree=shift;
    print "Make filehash $tree\n"if($log{vb});

    # get file list (could be done in Perl!)
    my @files=`cd $tree; find .`;

    foreach my $f (@files)
    {
        chomp $f;

        # remove ./ from the filename
        $f=~s/^\.\///;

        # make full pathname
        my $full=$tree.'/'.$f;

        # skip directories
        next if(-d $full);

        # make a version of the filename without the path
        my $fonly=$f;
        $fonly=~s/.*\///;

        next if (skipfile($fonly));

        # store in the hash with the filename $fonly as the key
        # while the value is the full path
        $filehash{$tree}{$fonly}=$full;

        # screen output
        #print "File $f ($fonly) [$full]\n";
    }
}

sub skipfile
{
    # check if we should skip file $f
    my $f=shift;

    $f=~s!^\.\/!!; # remove initial ./ if it's there

    return (
        # backup files
        ($f=~/~$/)||($f=~/^\#/)

        # svn stuff
        ||($f=~/\.svn/)||($f=~/svn-commit\.tmp/)||($f=~/^svn/)

        # git stuff
        ||($f=~/EDITMSG$/) || ($f=~/^FETCH/) || ($f=~/ORIG_HEAD/)

        # tar or tar.gz files (these are binary)
        ||($f=~/\.tar\.gz$/ || $f=~/\.tar$/)

        # . and ..
        ||($f eq '.')||($f eq '..')

        # object files, shared libraries, etc.
        ||($f =~/\.s?o$/) || ($f=~/\.d$/)

        # special binary_c files
        ||($f eq '(')||($f eq 'AS_NEEDED')||($f eq '.lread')


        );
}

sub compare_trees
{
    # compare two trees
    my @trees=@_;

    # make a list of files from all the trees
    my @files;
    {
        my %files; # use hash to avoid multiple entires
        foreach my $tree (@trees)
        {
            map
            {
                $files{$_}=1;
            }keys %{$filehash{$tree}};
        }

        # alpha sort
        @files=sort keys %files;
    }

    foreach my $file (@files)
    {
        # do stuff for $file
        my $fileinfo = "File : ".$colours{'magenta'}.$file.$colours{'reset'}."\n";
        my $o; # output string
        my @locations;

        # check in trees
        foreach my $tree (@trees)
        {
            my $fullpath=$filehash{$tree}{$file};
            if(defined($fullpath))
            {
                $o .= sprintf "In %s at %s%s%s\n",$tree,$colours{'cyan'},$fullpath,$colours{'reset'} if($log{vb});
                push(@locations,$fullpath);
            }
            else
            {
                $o .= $colours{'red'}."Not in tree $tree\n".$colours{'reset'} if($log{missing});
            }
        }

        # diff the locations
        {
            my $first=shift @locations;
            foreach my $l (@locations)
            {
                my $diff=`diff $first $l`;
                chomp $diff;

                if($diff)
                {
                    # pretty output
                    $diff=~s/</$colours{'red'}</g;
                    $diff=~s/>/$colours{'yellow'}>/g;
                    $diff=~s/---/$colours{'reset'}---/g;

                    $o .= "Diff $first ($colours{red}<$colours{reset}) vs $l ($colours{yellow}>$colours{reset}) :\n".$diff.$colours{'reset'} if($log{diff});
                }
                else
                {
                    #$o .= "Files are identical :)\n";
                    $o = undef if(!$log{identical});
                }
            }
        }

        print "$fileinfo$o\n============================================================\n\n" if(defined $o);
    }
}


sub make_colours
{
    # cache of colour strings
    foreach ('red','yellow','green','blue','cyan','magenta','reset')
    {
        $colours{$_}=color($_);
    }
}
