#!/usr/bin/env perl
$|=1; # enable this line for auto-flushed output
use strict;
use Sort::Key qw(nsort);
use rob_misc;
use binary_grid;
use binary_grid::condor;
use threads;
use threads::shared;
use Fcntl ':flock'; # import LOCK_* constants
use subs qw(logprint logprintf);
use Carp qw(confess);
use File::Path qw(make_path);
#
# example of how to use the binary_grid flexigrid
#

# grid resolution : full recommended resolution after #s
my $nm=1000; # 1000
my $nm1=271; # 100
my $nm2=271; # 40
my $nper=271; # 100
my $period_distribution='bastard'; # 'DM91' or 'bastard'

$nm=100;
$nm1=10;
$nm2=10;
$nper=10;

my $outlock : shared;
my $results={}; # hash to store results
my @spectral_type_strings=('O','B','A','F','G','K','M');
my %spectral_type;  revspechash();
my @stellar_type_strings;
my @short_stellar_type_strings;
my %short_stellar_type_strings;
my $period_binwidth=0.1; # width in logP of histograms
my $outdir;
my $log_fp;
my %opts; # local options

#binary_grid::inspect_snapshot('/tmp/binary_c-snapshot');exit;

# fix aifa environment/paths
fix_aifa_environment();

# combined single/binary star grid
defaults();
condor_grid(1);
#flexigrid(ncpus()); # 

output();
exit(0);

############################################################
############################################################
############################################################

sub output
{
    # if we're running condor, do not output unless for the final call
    return if($binary_grid::grid_options{condor_command} eq 'run_flexigrid');

    # open log file
    
    # check for an output directory, otherwise assume a default in /tmp
    $outdir = $ENV{VOLDISK}.'/data/condor/sigint_output' 
	if(!defined($outdir));
    make_path($outdir); # nb we assume this works...

    # output binary_grid::grid_options/bse_options
    open($log_fp,">$outdir/grid_options")||confess("cannot open $outdir/grid_options for output");
    print $log_fp "grid_options\n\n";
    map
    {
	print {$log_fp} $_.' : '.$binary_grid::grid_options{$_}."\n";
    }sort keys %binary_grid::grid_options;
    close $log_fp;
    open($log_fp,">$outdir/bse_options")||confess("cannot open $outdir/bse_options for output");
    print $log_fp "bse_options\n\n";
    map
    {
	print {$log_fp} $_.' : '.$binary_grid::bse_options{$_}."\n";
    }sort keys %binary_grid::bse_options;
    close $log_fp;
    
    # output grid results
    open ($log_fp,">$outdir/log") || confess ("cannot open $outdir/log for output");
    logprint "\n\n\nCANBERRA Grid results\n============================================================\n\n";
    logprint "SVN Revision ",`svn info|grep Revision|gawk \"\{print \\\$2\}\"`;
    logprint "Run at ",scalar(localtime),"\n";
    logprint "Resolution: nm=$nm nm1=$nm1 nm2=$nm2 nper=$nper\n";
    logprint "\n\nParameters:\n";
    logprint "Initial binary fraction : Raghavan 2010\n";
    foreach my $p ('alpha_ce','lambda_ce','lambda_ionisation','z','wd_sigma')
    {
	my $opt = ($p eq 'lambda_ce' && $binary_grid::bse_options{$p}==-1) 
	    ? 'Fitted to detailed stellar models' 
	    : 
	    sprintf "%g",$binary_grid::bse_options{$p};

	logprint sprintf "% -30s = %s\n",$p eq'z'?'Metallicity':$p,$opt;
    }

    
    my $dtp_wd=1e-30; # total number count of WDs (single or binary)
    my %dtp_wd;

    {
	my $k='number count';
	# get total WD number count
	foreach my $duplicity (sort keys %{$results->{$k}})
	{ 
	    foreach my $wd_type (sort keys %{$results->{$k}->{$duplicity}})
	    {
		$dtp_wd += $$results{$k}->{$duplicity}->{$wd_type};
		$dtp_wd{$duplicity} += $$results{$k}->{$duplicity}->{$wd_type};
	    } 
	}
    }

    my $dtp_wdms=0.0;
    {
	# get total WD-MS number count
	my $k='MSWD spectral types';
	foreach my $spectral_type (nsort keys %{$results->{$k}})
	{
	    $dtp_wdms += $$results{$k}{$spectral_type};
	}    
    }


    foreach my $k (sort keys %$results)
    {
	next if($k eq 'thread_number');
	if($k eq 'number count')
	{
	    logprint sprintf "WD binary fraction %3.2f %%\n",$dtp_wd{'Binary'}/$dtp_wd*100.0;
	    foreach my $duplicity (sort keys %{$results->{$k}})
	    {
		foreach my $wd_type (nsort keys %{$results->{$k}->{$duplicity}})
		{
		    logprint sprintf "% -6s % -30s %3.2f %%\n",
		    $duplicity,
		    $stellar_type_strings[$wd_type], 
		    $$results{$k}->{$duplicity}->{$wd_type}/$dtp_wd*100.0;
		} 
	    }
	}
	elsif($k eq 'companion types')
	{
	    my $test_tot=0.0;
	    logprint "\n\nWhite Dwarf - ? pairs (as a fraction of all White Dwarf Binaries)\n";
	    foreach my $wd1 (nsort keys %{$results->{$k}})
	    {
		foreach my $wd2 (nsort keys %{$results->{$k}->{$wd1}})
		{
		    logprint sprintf "% -40s - % -40s     % 5.2f %%\n",
		    $stellar_type_strings[$wd1],
		    $stellar_type_strings[$wd2],
		    $$results{$k}{$wd1}{$wd2}
		    /$dtp_wd{'Binary'}*100.0;
		}
	    }
	    logprint "\n";
	}
	elsif($k eq 'test')
	{
	    # test initial binary fraction
	    my $test_tot=0.0;
	    logprint "\n\nTest numbers\n\n";
	    foreach my $duplicity (sort keys %{$results->{$k}})
	    {
		$test_tot += $$results{$k}->{$duplicity};
	    }
	    foreach my $duplicity (sort keys %{$results->{$k}})
	    {
		logprint sprintf "MS $duplicity %3.2f %%\n", 100.0*$$results{$k}->{$duplicity}/$test_tot;
	    }
	}
	elsif($k eq 'test dist spectypes')
	{
	    print "ZAMS Binary fraction by spectral type : ",sort keys %{$results->{$k}->{'Binary'}},"\n\n";
	    foreach my $spec_type (@spectral_type_strings)
	    {
		my $t=$$results{$k}->{'Single'}->{$spec_type}+$$results{$k}->{'Binary'}->{$spec_type};
		logprint sprintf "%s %5.2f %%\n",$spec_type,$$results{$k}->{'Binary'}->{$spec_type}/$t*100.0 if($t>0.0);
	    }
	}
	elsif($k eq 'test dist')
	{
	    # test initial distributions
	    foreach my $duplicity (sort keys %{$results->{$k}})
	    {
		foreach my $dist (sort keys %{$results->{$k}->{$duplicity}})
		{
		    my $f="$outdir/init_".$duplicity.'_'.$dist.'.dat';
		    logprint "Output distribution to $f\n";
		    open(FP,">$f")||confess("can't open $f");
		    foreach my $x (nsort keys %{$results->{$k}->{$duplicity}->{$dist}})
		    {
			printf FP "%g %g\n",$x,$$results{$k}{$duplicity}{$dist}{$x};
		    }
		    close FP;
		}
	    }
	}

	elsif($k eq 'MSWD spectral types')
	{
	    my $f="$outdir/MSWD_spectral_types.dat";
	    open(FP,'>'.$f)||confess("cannot open $f for output");
	    logprint "MSWD binaries: spectral types (see datafile $f)\n";
	    pad_datahash($results->{$k},1);
	    foreach my $spectral_type (nsort keys %{$results->{$k}})
	    {
		printf FP "%d %g\n",$spectral_type,$$results{$k}{$spectral_type}/$dtp_wdms;
		logprint sprintf "MSWD spectral type %d = %s = %g\n",
		$spectral_type,$spectral_type_strings[$spectral_type],$$results{$k}{$spectral_type}/$dtp_wdms;
	    } 
	    logprint "\n";
	    close FP;
	}
	# misc period distributions
	elsif($k =~/(\S+) period distribution/)
	{
	    my $distname=$1;
	    my $f="$outdir/$distname".'_period_distribution.dat';
	    #print "Pad datahash $results->{$k}\n";
	    pad_datahash($results->{$k},$period_binwidth);
	    open(FP,'>'.$f)||confess("cannot open $f for output");
	    logprint "$1 binaries: period distribution (see datafile $f)\n";
	    foreach my $logP (nsort keys %{$results->{$k}})
	    {
		printf FP "%g %g\n",$logP,$$results{$k}{$logP};
	    } 
	    close FP;
	}
    }

    # compare to Holberg 2009
    {
	my %obs=(
	    # Holberg 2009
	    'Single WDs'=>0.70, 
	    'WD +dM'=>0.12,
	    'Sirius-Like'=>0.08,
	    'WD + WD'=>0.10,
	    'Total WDs'=>1.0,
	    );
	map{$obs{$_}*=100.0}sort keys %obs;

	my $format="\% -30s \% 5.2f \%\% (% 5.2f)\n";
	logprint "\n=============\nHolberg 2009 comparison\n\n";

	# Single WD fraction
	my $single_WD = $dtp_wd{'Single'}/$dtp_wd*100.0;
	logprintf $format,'Single WDs',$single_WD,$obs{'Single WDs'};

	# WD + dM fraction
	my $WD_dM=$$results{'MSWD spectral types'}{$spectral_type{'M'}}/$dtp_wd*100.0;
	logprintf $format,'WD +dM',$WD_dM,$obs{'WD +dM'};
	
	# Sirius-like fraction
	my $Sirius=0.0;
	map{$Sirius+=$$results{'MSWD spectral types'}{$spectral_type{$_}}}('O','B','A','F','G','K');
	$Sirius = $Sirius/$dtp_wd * 100.0;
	logprintf $format,'Sirius-Like',$Sirius,$obs{'Sirius-Like'};

	# WD + WD fraction
	my $WDWD=0.0;
	foreach my $wd1 (10,11,12)
	{
	    foreach my $wd2 (10,11,12)
	    {
		$WDWD += $$results{'companion types'}{$wd1}{$wd2};
	    }
	}
	$WDWD = $WDWD/$dtp_wd*100.0;
        logprintf $format,'WD + WD',$WDWD,$obs{'WD + WD'};
	
	# total (should be 100%!)
	logprintf $format,'Total WDs',($single_WD + $WD_dM + $Sirius + $WDWD),$obs{'Total WDs'};
    }

    logprint "\n============================================================\n\n\n";

    close $log_fp;

#    use Data::Dumper;
#    open($log_fp,">$outdir/datadump")||confess("cannot open $outdir/datadump"); 
#    print {$log_fp} Dumper($binary_grid::grid_options{results_hash});
#    close $log_fp;
}

sub parse_bse
{
    # this gets called by the binary_grid module, it parses the 
    # lines of output from binary_c/nucsyn
    my $brk=0; 

    # you should set data in the $h hash pointer, which is 
    # then passed to the join_thread function (below) for you
    # to add up. (Note: $h is local to each thread.)
    my $h=shift;

    my $nthread=$binary_grid::grid_options{'thread_num'};

    while($brk==0)
    {
	$_=tbse_line();
	print "PARSE $_\n";
	chomp;
	if($_ eq 'fin')
	{
	    $brk=1; # the end of output
 	}
	else
	{
	    my $l=$_;
	    if(s/CANBERRA_TEST(\d+) (\S+)\s*//)
	    {
		# test MS star
		my $duplicity= !$1 ? 'Single' : 'Binary';
		my $dp = $2 ;
		$$h{'test'}{$duplicity} += $dp;
		
		# save initial distributions
		my @x=split(/\s+/o,$_);

		#printf "PARSE_BSE Kopt %g,%g results=$results results_hash=$binary_grid::grid_options{results_hash}\n",
		#$$results{'test dist spectypes'}{'Single'}{K},
		#${$binary_grid::grid_options{results_hash}}{'test dist spectypes'}{'Single'}{K};

		if($duplicity eq 'Binary')
		{
		    #print "LOGM1 $binary_grid::count x0=$x[0] dp=$dp x2=$x[2] x3=$x[3]\n";
		    $$h{'test dist'}{'Binary'}{'M1'}{bin_data(log10($x[0]),0.1)}+=$dp;
		    $$h{'test dist'}{'Binary'}{'q'}{bin_data($x[2],0.1)}+=$dp;
		    $$h{'test dist'}{'Binary'}{'logP'}{bin_data(log10(MAX(1e-10,$x[3])),0.1)}+=$dp;
		    
		    $$h{'zero age'}{binary}{$x[0]}{$x[2]}{$x[3]}=$dp;
		}
		else
		{
		    #print "LOG M $x[0] from $l\n";
		    $$h{'test dist'}{'Single'}{'M'}{bin_data(log10($x[0]),0.1)}+=$dp;
		    $$h{'zero age'}{single}{$x[0]}=$dp;
		}
		
		#print "add spectype $duplicity $x[1] $spectral_type_strings[$x[1]] (@x)\n";
		$$h{'test dist spectypes'}{$duplicity}{$spectral_type_strings[$x[1]]}+=$dp;
	    }
	    elsif(s/CANBERRA(\d+) //)
	    {
		my $duplicity= $1 ? 'Binary' : 'Single';
		my @x=split(/\s+/o,$_);
		
		# x:
		my $dtp=shift @x; # 0 = dtp
		my $t=shift @x; # 2 = model time (Myr)

		# weight dtp according to SFR (i.e. age)
		$dtp *= SFR($binary_grid::bse_options{'max_evolution_time'}-$t);

		if($dtp>0.0)
		{
		    #print "AGE $t\n";
		    my $wd1=shift @x; # 3 = WD stellar type
		    my $wd2=shift @x; # 4 = companion stellar type
		    my $wdspec=shift @x; # 5 = WD spectral type
		    my $compspec=shift @x; # 6 = companion spectral type
		    my $P=shift @x; # 7 = period (days)

		    # bin the orbital period
		    $P=bin_data(log10(MAX(1e-10,$P)),0.1);

		    # save WD number counts
		    $$h{'number count'}{$duplicity}{$wd1} += $dtp;
		    if($wd1==15)
		    {
			print "FACK : found massless remnant when I should have a WD!\n";
			tbse_land();
			exit;
		    }

		    if($duplicity eq 'Binary')
		    {
			# now for the different binary types
			$$h{'companion types'}{$wd1}{$wd2} += $dtp;

			# distributions

			# WD - MS binaries
			if($wd2<=1)
			{
			    # spectral type distribution of MS-WD binaries
			    $$h{'MSWD spectral types'}{$compspec}+=$dtp;
			    
			    # (binned) period distribution of MS-WD binaries
			    $$h{'MSWD period distribution'}{$P} += $dtp;
			    
			    # log period distribution for subtypes
			    if($compspec == $spectral_type{'M'})
			    {
				# M-dwarf companions
				$$h{'WD+dM period distribution'}{$P} += $dtp;	
			    }
			    else
			    {
				# Sirius-like binaries (K-O type companions)
				$$h{'Sirius-like period distribution'}{$P} += $dtp;
			    }
			}

			# period distributions as a function of both stellar types

			# stellar types as strings
			my @st = ($short_stellar_type_strings[$wd1],
				  $short_stellar_type_strings[$wd2==0 ? 1 : $wd2]);
			$$h{$st[0].$st[1].' period distribution'}{$P} += $dtp;	
		    }
		}
	    }
	}
    }
}


sub defaults
{
    binary_grid::parse_args();
    grid_defaults();
    renice_me(19);

    # condor directory : this stores the temp files and the final 
    # output : you should make sure it is read/writeable on all
    # condor notes (e.g. via an NFS link)
    $binary_grid::grid_options{condor_dir}='/users/izzard/data/condor/sigint_test';

    # force use of particular machine
    $binary_grid::grid_options{condor_options}{Requirements} = 'Machine == "klaipeda3.astro.uni-bonn.de" || Machine == "klaipeda1.astro.uni-bonn.de"';

    # force checkpointing 
    $binary_grid::grid_options{condor_checkpoint_interval} = 0;
    $binary_grid::grid_options{condor_checkpoint_stamp_times} = 0;

    # always link binary_grid to our local results hash
    $binary_grid::grid_options{results_hash}=$results;

    # use which parser function?    
    $binary_grid::grid_options{parse_bse_function}='parse_bse';
    
    $binary_grid::grid_options{threads_stack_size}=50; # MBytes
    
    $binary_grid::grid_options{prog}='binary_c-canberra';
    $binary_grid::grid_options{'gdb wait'}=undef;

    $binary_grid::bse_options{z}=0.02;
    $binary_grid::bse_options{wd_sigma}=0.0;
    $binary_grid::bse_options{sn_sigma}=0.0; # no kicks
    
    $binary_grid::bse_options{alpha_ce}=1.0; # 1
    $binary_grid::bse_options{lambda_ce}=0.5; # -1 = automatically set
    $binary_grid::bse_options{lambda_ionisation}=0.0; # 0.0

    # star formation stuff
    $binary_grid::bse_options{max_evolution_time}=10e3;
    $opts{'SF timescale'}=10e3; # SF timescale in MYr

    # time resolution
    $binary_grid::bse_options{'maximum_timestep'}=10;

    # code options
    $binary_grid::grid_options{'nice'}='nice -n +10'; # nice command e.g. 'nice -n +10' or '' 
    
    # use C routines when available (should be faster)
    $binary_grid::grid_options{'code flavour'}='C';
    
    $binary_grid::grid_options{'timeout'}=0; # seconds until timeout
    
    $binary_grid::grid_options{'log_args'}=1;
    $binary_grid::grid_options{'vb'}=1; # make verbose
    $binary_grid::grid_options{'tvb'}=1; # thread logfile

    # activate flexigrid
    # build flexigrid
    
    # maximum stack size before we pause to allow stars to finish
    $binary_grid::grid_options{'flexigrid'}{'maxstack'}=10;

    # use local threads or a daemon
    $binary_grid::grid_options{'flexigrid'}{'method'}='local threads';

    # binary_c approximate runtime in s
    $binary_grid::grid_options{'binary_c_runtime'}=1e-3;
    
    # first attempt at an MC grid
    #$binary_grid::grid_options{'flexigrid'}{'grid type'}='monte carlo';

    my $nvar=0; 

    # M1 (and M2)
    my $mmin=0.1;
    my $mmax=8.0;  
    
    # period distribution D+M91 options
    my $logpermin=-2.0;
    my $logpermax=12.0;
    my $gauss_logmean=4.8;
    my $gauss_sigma=2.3;

    # duplicity
    $binary_grid::grid_options{'flexigrid'}{'grid variable '.$nvar++}=
    {
	'name'=>'duplicity',
	'longname'=>'Duplicity',
	'range'=>[0,1],
	'resolution'=>1,
	'spacingfunc'=>'number(1.0)',
	'precode'=>'$binary_grid::grid_options{binary}=$duplicity;binary_grid::flexigrid_grid_resolution_shift();',
	'gridtype'=>'edge',
	'noprobdist'=>1,
    };
    
    
    # Mass 1
    $binary_grid::grid_options{'flexigrid'}{'grid variable '.$nvar++}=
    {
	'name'=> 'lnm1', 
	'longname'=>'Primary mass', 
	'range'=>["log($mmin)","log($mmax)"],
	'resolution'=> "$nm1", # ignore single stars in approximate resolution calculation
	'spacingfunc'=>"const(log($mmin),log($mmax),\$binary_grid::grid_options{binary} ? $nm1 : $nm)",
	'precode'=>'$m1=exp($lnm1);  $eccentricity=0.0; my $binary_fraction=distribution_functions::raghavan2010_binary_fraction($m1);  $binary_grid::grid_options{weight}= $binary_grid::grid_options{binary}==0 ? 1.0-$binary_fraction : $binary_fraction;   ',
	#'probdist'=>"ktg93(\$m1)*\$m1", # KTG93 (all masses)
	'probdist'=>"Kroupa2001(\$m1)*\$m1",
	#'probdist'=>'powerlaw(0.8,100.0,-2.35,$m1)*$m1', # salpeter (0.8-100)
	'dphasevol'=>'$dlnm1',
    };
    
    # Binary stars: Mass 2 and Separation
    my $m2min=0.01;
    $binary_grid::grid_options{'flexigrid'}{'grid variable '.$nvar++}=
    {
	'condition'=>'$binary_grid::grid_options{binary}==1',
	'name'=>'q',
	'longname'=>'Mass ratio',
	'range'=>[$m2min.'/$m1',1.0],
	'resolution'=>$nm2,
	'spacingfunc'=>'const('.$m2min.'/$m1,1.0,'.$nm2.')',
	'probdist'=>"flatsections\(\$q,\[
\{min=>$m2min/\$m1,max=>0.8,height=>1.0\},
\{min=>0.8,max=>1.0,height=>4.0\},
\]\)",
	    'precode'=>'$m2=$q*$m1;',
	    'dphasevol'=>'$dq',
    };
    
    if($period_distribution eq 'DM91')
    {
	# old code
	$binary_grid::grid_options{'flexigrid'}{'grid variable '.$nvar++}=
	{  
	    'name'=>'logper',
	    'longname'=>'log(Orbital_Period)',
	    'range'=>[$logpermin,$logpermax],
	    'resolution',$nper,
	    'spacingfunc',"const($logpermin,$logpermax,$nper)",
	    'precode'=>"\$per=10.0**\$logper;\$sep=calc_sep_from_period(\$m1,\$m2,\$per)if(defined(\$m1)&&defined(\$m2));",
	    'probdist'=>"gaussian(\$logper,$gauss_logmean,$gauss_sigma,$logpermin,$logpermax)",
	    'dphasevol'=>'$dln10per'
	}
    }
    elsif($period_distribution eq 'bastard')
    {
	
	# period distribution : rob's bastard
	$binary_grid::grid_options{'flexigrid'}{'grid variable '.$nvar++}=
	{
	    'name'=>'log10per',
	    'longname'=>'log10(Orbital_Period)',
	    'range'=>['-1.0','10.0'],
	    'resolution',$nper,
	    'spacingfunc',"const(-1.0,10.0,$nper)",
	    'precode'=>"my \$per=10.0**\$log10per;my\$sep=calc_sep_from_period(\$m1,\$m2,\$per);",
	    'probdist'=>'Izzard2012_period_distribution($per,$m1)',
	    'dphasevol'=>'$dlog10per'
	}
    }
    else
    {
	print "Unknown period distribution $period_distribution\n";
	exit;
    }

    printf "Flexigrid modulo = %d, offset = %d\n",
    $binary_grid::grid_options{modulo},
    $binary_grid::grid_options{offset};

    binary_grid::parse_args();
    secondary_variables();
    setup_binary_grid();

    @stellar_type_strings=@{(stellar_type_strings())[2]};
    @short_stellar_type_strings=@{(stellar_type_strings())[3]};
    %short_stellar_type_strings=%{(stellar_type_strings())[4]};
    #print "Short @short_stellar_type_strings\n";
}



sub secondary_variables
{
    # from the defaults and/or passed in variables,
    # calculate useful things

    # use colour?
    if($binary_grid::grid_options{'colour'}==1)
    {
        # use ANSI color for output to xterms/rxvt etc.
        use Term::ANSIColor;
        # set to white on black
        print color("white on_black");
    }
    else
    {
        # dummy function to replace the ANSI color() function
        eval("sub\ color\{ \}");
    }
}

sub logprint
{
    print {$log_fp} "@_";
    print "@_";
}
sub logprintf
{
    my $format=shift @_;
    printf {$log_fp} $format,@_;
    printf $format,@_;
}

sub revspechash
{
    for(my $i=0;$i<=$#spectral_type_strings;$i++)
    {
	$spectral_type{$spectral_type_strings[$i]}=$i;
    }
}

sub spec_to_float
{
    my $s=$_[0]; # spectral type to check
    if($s=~/([A-Z])(\d)/)
    {
	return $spectral_type{$1}+0.1*$2;
    }
    else
    {
	print "Failed to convert spec type $_[1] to float\n";
	exit;
    }
}

sub spec_type_sorter
{
    return spec_to_float($a) <=> spec_to_float($b);
}

sub pad_datahash
{
    # pad datahash $h so it has zeros where there's no data
    my $h=shift;
    my $d=shift;
    my @k=nsort keys %$h;
    my $xmax=$k[$#k]; # max value
    my $x=$k[0]; # start (min) value
    while($x<$xmax)
    {
	$$h{$x}=0.0 if(!defined($$h{$x}));
	$x=sprintf '%g',$x+$d;
    }
}

sub SFR
{
    # SFR as a function of Galactic age in Myr
    my $t=$_[0];

    return 1.0; # const SFR
    #return $t<1.0e3 ? 1.0 : 0.0; # const for a given time period
    #return exp(-$t/$opts{'SF timescale'}); # exponential
}

sub fix_aifa_environment
{
    if(`hostname`=~/aibn/)
    {
	map
	{
	    print "CHECK $_\n";
	    if($ENV{$_}=~s!/export/aibn(\d+)_(\d+)/!/vol/aibn$1/aibn$1\_$2/!g)
	    {
		print "CONV $_=$ENV{$_}\n";
	    }
	}%ENV;
    }
}
