#!/usr/bin/env perl
use strict;
use Sort::Key qw/nsort/;
use Data::Dumper;
use Sub::Override;
use Math::Trig;

# constants
use constant PI => acos(0.0)*2.0;
use constant GAUSSIAN_PREFACTOR => (1.0/sqrt(2.0*PI));
use constant logmin => 1e-30;
use constant value_of_log10 => log(10.0);
use constant log_ln_converter => (1.0/log(10.0));

############################################################
#                                                          #
# Grid of [C/N] in the thick disc
#
# Version 5
#                                                          #
############################################################

modules_etc();# load modules

my $results={}; # hash to store results
my $vb=0;
my $nthreads = $vb ? 1 : MAX(1,ncpus()); 

# bin widths: a negative value means take log10 first
my %binwidth=(

    # initial/current conditions
    M1=>-0.1, # log (to make initial mass function)
    M2=>-0.1, # log (to make initial mass function)
    McMratio=>0.1, # core mass to mass ratio
    mass_ratio=>0.1,
    orbital_period=>-0.1,
    orbital_separation=>-0.1,
    eccentricity=>0.05,
   
    stellar_mass=>0.1, # generic mass 
    square_bracket=>0.1, # e.g. [Fe/H], [C/N]
    logg=>0.1, # log10(g)
    );

my $observational_radial_velocity_limit=1.0;

# first set some defaults for the grid
defaults();

# then parse the command line to override the defaults
parse_grid_args(@ARGV);

# now set up the grid
setup_binary_grid();

# run flexigrid with two threads
flexigrid($nthreads);

if(0)
{
    my @data = (`tbse`,"fin\n");
    my $override = Sub::Override->new(tbse_line=>
                                      sub{
                                          my $x = shift @data;
                                          chomp $x;
                                          print "poo $x\n";
                                          return $x;
                                      });
    parse_bse($results,@data,"fin\n");
    $override->restore;
    print Data::Dumper::Dumper($results);
    exit;
}

# output results
output(); 

exit(0); # done!

############################################################
############################################################
############################################################

sub output
{
    # output results
    mkdir $binary_grid::grid_options{outdir};

    foreach my $result (sort keys %$results)
    {
	next if($result eq 'thread_number');
	print "Saving results for output subhash '$result' in directory $binary_grid::grid_options{outdir}\n";
        my $h = $results->{$result};

        if($result=~/CN vs(.*)vs(.*)/)
        {
            my $x = $1;
            my $y = 'CN';
            my $z = $2;

            # distributions as f(CN, mass)
            my $filename = $binary_grid::grid_options{outdir}.'/per_cell_map_M_CN_'.$z;
            $filename =~ s/\s/_/g;
            print "Dump per-cell map data to $filename\n";

            open(MAP,'>',$filename)||die;
            print MAP "# distribution of x=$x, y=$y, z=$z\n";
            map
            {
                print MAP "# bin $_ width $binwidth{$_}\n";
            }sort keys %binwidth;

            foreach my $m (nsort keys %$h)
            {
                foreach my $CN (nsort keys %{$h->{$m}})
                {
                    foreach my $logP (nsort keys %{$h->{$m}->{$CN}})
                    {
                        printf MAP "%g %g %g %s\n",$m,$CN,$logP,$h->{$m}->{$CN}->{$logP};
                    }
                    print MAP "\n";
                }
            }
            close MAP;
        }
        elsif($result=~/^CN/)
        {
            # surface pots 
            my $filename = $binary_grid::grid_options{outdir}.'/'.$result.'-'.
                'Z='.$binary_grid::bse_options{metallicity}.
                '-'.($binary_grid::grid_options{binary}==0 ? 'single' : 'binary');
            
            $filename =~ s/\s/_/g;
            print "Dump to $filename\n";

            open(CN,'>',$filename)||die;
            foreach my $m (nsort keys %$h)
            {
                foreach my $CN (nsort keys %{$h->{$m}})
                {
                    printf CN "%g %g %s\n",$m,$CN,$h->{$m}->{$CN};
                }
                print CN "\n";
            }
            close CN;
            print "See $filename\n";
        }
    }

    # save the stats in a Perl-loadable hash form
    open(FP,'>',$binary_grid::grid_options{outdir}.'/stats')||die;
    print FP Data::Dumper::Dumper($results);
    close FP;
}

sub parse_bse
{
    # this gets called by the binary_grid module, it parses the 
    # lines of output from binary_c/nucsyn
    my $brk=0; 

    # you should set data in the $h hash pointer, which is 
    # then passed to the join_thread function (below) for you
    # to add up. (Note: $h is local to each thread.)
    my $h=$_[0];

    my $nthread=$binary_grid::grid_options{'thread_num'};

    while($brk==0)
    {
	$_=tbse_line();
        chomp;
        print 'DATA >>> ',$_,"\n"if($vb);
        
	if($_ eq 'fin')
	{
	    $brk=1; # the end of output
        }
	elsif(s/^CND //)
	{
            my @x=split(' ',$_);
         
            my %initial;
            my %current;

            # time and dtp weight
            $current{time} = shift @x; 
            $current{dtp} = shift @x;

            # skip everything if dtp = 0
            next if(!$current{dtp});

            # initial conditions
            $initial{orbital_period} = bin((shift @x), 'orbital_period');
            $initial{orbital_separation} = bin((shift @x), 'orbital_separation');
            $initial{eccentricity} = bin((shift @x),'eccentricity');
            $initial{M1} = shift @x;
            $initial{M2} = shift @x; 
            if($initial{M2} ne '*')
            {
                $initial{mass_ratio} = bin($initial{M1}/$initial{M2},'mass_ratio');
                $initial{M2} = bin($initial{M2},'M2');
            }
            $initial{M1} = bin($initial{M1},'M1');

            # current conditions
            $current{orbital_period} = bin((shift @x), 'orbital_period');
            $current{orbital_separation} = bin((shift @x), 'orbital_separation');
            $current{eccentricity} = bin((shift @x),'eccentricity');
            $current{M1} = shift @x;
            $current{M2} = shift @x;
            if($current{M2} ne '*')
            {
                $current{mass_ratio} = bin($current{M1}/$current{M2},'mass_ratio');
                $current{M2} = bin($current{M2},'M2');
            }
            $current{M1} = bin($current{M1},'M1');

            if($vb)
            {
                map
                {
                    print "INITIAL $_ = $initial{$_}\n";
                }sort keys %initial;
                map
                {
                    print "CURRENT $_ = $current{$_}\n";
                }sort keys %current;
            }

            # log each star separately
            my $logged_initial_and_orbit = 0;

            foreach my $offset (0,7)
            {
                if($x[$offset] ne '*')
                {
                    my $st = $x[$offset+0]; # 0 is stellar type 
                    my $K =  $x[$offset+1]; # 1 is K (radial velocity)
                    my $m = bin($x[$offset+2],'stellar_mass'); # 2 is mass
                    my $CN = bin($x[$offset+3],'square_bracket'); # 3 = [C/N]
                    my $mc = bin($x[$offset+4],'stellar_mass'); # 4 is core mass
                    my $logg = bin($x[$offset+5],'logg'); # 5 is logg
                    my $was_blue_straggler = $x[$offset+6]; # 6 is whether it was a blue straggler (1) or not (0)
                    my $McMratio = bin($x[$offset+4]/MAX(1e-30,$x[$offset+2]),'McMratio'); # Mc/M
                    
                    print "LINE at t=$current{time} ($x[$offset]) m=$m, st=$st , [C/N]=$CN += $current{dtp} from $_ (K=$K)\n"if($vb);

                    # save cell maps in  mass-[C/N] plane
                    # current conditions
                    $$h{'CN vs mass vs orbital period'}{$m}{$CN}{$current{orbital_period}} += $current{dtp};
                    $$h{'CN vs mass vs orbital separation'}{$m}{$CN}{$current{orbital_separation}} += $current{dtp};
                    $$h{'CN vs mass vs eccentricity'}{$m}{$CN}{$current{eccentricity}} += $current{dtp};
                    $$h{'CN vs mass vs stellar type'}{$m}{$CN}{$st} += $current{dtp};
                    $$h{'CN vs mass vs mass ratio'}{$m}{$CN}{$current{mass_ratio}} += $current{dtp};
                    $$h{'CN vs mass vs M1'}{$m}{$CN}{$current{M1}} += $current{dtp};
                    $$h{'CN vs mass vs M2'}{$m}{$CN}{$current{M2}} += $current{dtp};
                    $$h{'CN vs mass vs coremass'}{$m}{$CN}{$mc} += $current{dtp};
                    $$h{'CN vs mass vs McMratio'}{$m}{$CN}{$McMratio} += $current{dtp};
                    $$h{'CN vs mass vs logg'}{$m}{$CN}{$logg} += $current{dtp};
                    
                    # initial conditions
                    $$h{'CN vs mass vs initial orbital period'}{$m}{$CN}{$initial{orbital_period}} += $current{dtp};
                    $$h{'CN vs mass vs initial orbital separation'}{$m}{$CN}{$initial{orbital_separation}} += $current{dtp};
                    $$h{'CN vs mass vs initial eccentricity'}{$m}{$CN}{$initial{eccentricity}} += $current{dtp};

                    # save distributions in CN vs whatever planes
                    # e.g. [C/N] vs mass, core_mass and logg
                    $$h{'CN vs mass'}{$m}{$CN} += $current{dtp};
                    $$h{'CN vs core_mass'}{$mc}{$CN} += $current{dtp};
                    $$h{'CN vs logg'}{$logg}{$CN} += $current{dtp};
                     
                    my $bssflag = $was_blue_straggler ? 'was_BSS' : 'never_BSS';
                    $$h{'CN vs mass '.$bssflag}{$m}{$CN} += $current{dtp};
                    $$h{'CN vs core_mass '.$bssflag}{$mc}{$CN} += $current{dtp};
                    $$h{'CN vs logg '.$bssflag}{$logg}{$CN} += $current{dtp};

                    # save as a function of stellar type
                    $$h{'CN vs mass stellar_type '.$st}{$m}{$CN} += $current{dtp};
                    $$h{'CN vs core_mass stellar_type '.$st}{$mc}{$CN} += $current{dtp};
                    $$h{'CN vs logg stellar_type '.$st}{$logg}{$CN} += $current{dtp};

                    # detect stars as single or binary depending on the 
                    # radial velocity.
                    my %weights;
                    if($binary_grid::grid_options{'binary'}==0 || abs($K)<1e-14)
                    {
                        # true single stars (includes mergers) 
                        $weights{'K.eq.0'} = 1.0;
                    }
                    else
                    {
                        # could be observed as a binary
                        my $w = probability_of_observed_binary($K,$observational_radial_velocity_limit);
                        # binary stars which would be observed as 'binary'
                        $weights{'K.gt.1'} = $w;
                        # binary stars which would be observed as 'single'
                        $weights{'K.lt.1'} = 1.0-$w;
                    }

                    foreach my $radial_velocity_class (keys %weights)
                    {
                        printf "Radial velocity class $radial_velocity_class : weight %g\n", $weights{$radial_velocity_class} if($vb);

                        # modulate dtp by the weight;
                        my $dtp = $weights{$radial_velocity_class} * $current{dtp};

                        # CN vs mass, core mass and logg for different radial velocity classes
                        $$h{'CN vs mass '.$radial_velocity_class}{$m}{$CN} += $dtp;
                        $$h{'CN vs mass '.$radial_velocity_class.' stellar_type '.$st}{$m}{$CN} += $dtp;
                        $$h{'CN vs core_mass '.$radial_velocity_class}{$mc}{$CN} += $dtp;
                        $$h{'CN vs core_mass '.$radial_velocity_class. ' stellar_type '.$st}{$mc}{$CN} += $dtp;
                        $$h{'CN vs logg '.$radial_velocity_class}{$logg}{$CN} += $dtp;
                        $$h{'CN vs logg '.$radial_velocity_class.' stellar_type '.$st}{$logg}{$CN} += $dtp;

                        # binary fraction vs mass only (histograms)
                        $$h{'binary fraction histograms'}{$radial_velocity_class}{$m} += $dtp;
                        $$h{'binary fraction histograms stellar_type '.$st}{$radial_velocity_class}{$m} += $dtp;
                        $$h{'binary fraction histograms mc'}{$radial_velocity_class}{$mc} += $dtp;
                        $$h{'binary fraction histograms logg'}{$radial_velocity_class}{$logg} += $dtp;

                        # binary fraction vs mass and CN (map)
                        $$h{'binary fraction map'}{$radial_velocity_class}{$m}{$CN} += $dtp;
                        $$h{'binary fraction map mc'}{$radial_velocity_class}{$mc}{$CN} += $dtp;
                        $$h{'binary fraction map stellar_type '.$st}{$radial_velocity_class}{$mc}{$CN} += $dtp;
                        $$h{'binary fraction map logg'}{$radial_velocity_class}{$logg}{$CN} += $dtp;
                    }
                }
            }
        }
    }

}


sub defaults
{
    grid_defaults();
  
    # physics options
    $binary_grid::bse_options{'metallicity'}=0.004;
    $binary_grid::bse_options{'wd_sigma'}=0.0;
    $binary_grid::bse_options{'c13_eff'}=1.0;
    $binary_grid::bse_options{'acc2'}=1.5;
    $binary_grid::bse_options{'tidal_strength_factor'}=1.0;
    $binary_grid::bse_options{'alpha_ce'}=0.2;
    $binary_grid::bse_options{'lambda_ce'}=-1; # -1 = automatically set
    $binary_grid::bse_options{'lambda_ionisation'}=0.0; # 0.0
    $binary_grid::bse_options{'delta_mcmin'}=0.0;
    $binary_grid::bse_options{'lambda_min'}=0.0;
    $binary_grid::bse_options{'minimum_envelope_mass_for_third_dredgeup'}=0.5;
    $binary_grid::bse_options{'max_evolution_time'}=13000;
    $binary_grid::bse_options{'RLOF_interpolation_method'}=0; # 0 = binary_c, 1 = BSE 
    $binary_grid::bse_options{'log_filename'}='/dev/null'; # no log required

    # grid options
    $binary_grid::grid_options{'results_hash'}=$results;
    $binary_grid::grid_options{'parse_bse_function_pointer'}=\&parse_bse;
    $binary_grid::grid_options{'threads_stack_size'}=32; # MBytes
    $binary_grid::grid_options{'nice'}='nice -n +1'; # nice command e.g. 'nice -n +10' or '' 
    $binary_grid::grid_options{'timeout'}=30000; # seconds until timeout
    $binary_grid::grid_options{'log_args'}=0;
    $binary_grid::grid_options{'save_args'}=0;
    $binary_grid::grid_options{'vb'}=1;

    $binary_grid::grid_options{internal_buffering}=0;
    $binary_grid::grid_options{internal_buffering_compression}=9;

    # This is the path to the binary_c directory
    $binary_grid::grid_options{'rootpath'}="$ENV{HOME}/progs/stars/binary_c";

    # The name of the binary_c executable
    $binary_grid::grid_options{'prog'}='binary_c-CND5';

    # VARIABLES TO BE CHANGED : maybe set on the command line!
    $binary_grid::grid_options{'binary'}=1; # single stars -> 0, binary stars -> 1
    $binary_grid::bse_options{'CRAP_parameter'}=1e3; # 1e3 = Tout
    $binary_grid::bse_options{'WRLOF_method'}=1;  # 1 = q-dep, 2=quadratic
    $binary_grid::bse_options{'RLOF_method'}=3; # 3 = Claeys et al 2014

    # age range for star formation in the thick disc
    # Snaith et al 2015 suggest start=13Gyr, end=8Gyr
    # and is ~ constant (and large!)
    $binary_grid::bse_options{'thick_disc_start_age'}=10e3;
    $binary_grid::bse_options{'thick_disc_end_age'}=4e3;


    # resolution
    $binary_grid::grid_options{nres} //= $binary_grid::grid_options{binary}==0 ? 1000 : 40; 
    
    # output direcory
    $binary_grid::grid_options{outdir} = '/tmp';

    # persep_distribution is
    # Opik : default flat in log-a grid
    # Izzard2012 : Rob's function, DM91 at low mass, Sana et al at high mass
    $binary_grid::grid_options{persep_distribution} = 'Opik';
    
    # override from command line
    parse_grid_args(@ARGV);

    # Mass 1
    my $nvar=0;

    my $mmin=0.1; my $mmax=6;

    # testing
    #my $mmin=0.9; my $mmax=6;

    my $n = $binary_grid::grid_options{nres};

    my $use_bastard = 0;

    if($use_bastard)
    {  
        my $sampling_factor=1.0/4.0; # over-resolution (Nyquist/Shannon)
        my $dt = 100.0; # time resolution in Myr

	# new bastard distribution of single and binary stars
	distribution_functions::bastard_distribution({
	    mmin=>$mmin,
	    mmax=>$mmax,

	    # use time-adaptive mass resolution for M1
	    time_adaptive=>{
		max_evolution_time=>$binary_grid::bse_options{max_evolution_time},
		stellar_lifetime_table_nm=>1000,
		nthreads=>10,
		thread_sleep=>1,
		mmin=>$mmin,
		mmax=>$mmax,
		mass_grid_log10_time=>0,
		mass_grid_step=>($dt*$sampling_factor),
		extra_flash_resolution=>0, # 1 = broken?
		mass_grid_nlow_mass_stars=>10,
		debugging_output_directory=>'/tmp',
		max_delta_m=>1.0,
		savegrid=>1,
		vb=>0,
	    },	    

            m2min=>$mmin,
            nm2=>$n,
            nper=>$n,
            necc=>undef,
            qmin=>0.0,
            qmax=>1.0,
            useecc=>undef	    
				  		     });
    }
    else
    {
        $binary_grid::grid_options{'flexigrid'}{'grid variable '.$nvar++}=
        {
            'name'=> 'lnm1', 
            'longname'=>'Primary mass', 
            'range'=>["log($mmin)","log($mmax)"],
            'resolution'=> $n,
            'spacingfunc'=>"const(log($mmin),log($mmax),$n)",
            'precode'=>'my $m1=exp($lnm1); my $eccentricity=0.0;',
            'probdist'=>"ktg93(\$m1)*\$m1",
            'dphasevol'=>'$dlnm1'
        };

        # Binary stars: Mass 2 and Separation
        if($binary_grid::grid_options{'binary'})
        {
            my $m2min=0.1;
            $binary_grid::grid_options{'flexigrid'}{'grid variable '.$nvar++}=
            {
                # name of the variable
                'name'=>'m2',
                # long name (for verbose logging)
                'longname'=>'Secondary mass',
                # range of the parameter space
                'range'=>[$m2min,'$m1'],
                # resolution 
                'resolution'=>$n,
                # constant grid spacing function
                'spacingfunc'=>"const($m2min,\$m1,$n)",
                # flat-q distribution between m2min and m1
                'probdist'=>"const($m2min,\$m1)",
                # phase volume contribution
                'dphasevol','$dm2'
            };

            if($binary_grid::grid_options{persep_distribution} eq 'Opik')
            {
                $binary_grid::grid_options{'flexigrid'}{'grid variable '.$nvar++}=
                {
                    # name of the variable
                    'name'=>'lnsep',
                    # long name (for verbose logging)
                    'longname'=>'ln(Orbital_Separation)',
                    # range of the separation space
                    'range'=>['log(3.0)','log(1e4)'],
                    # resolution
                    'resolution'=>$n,
                    # constant spacing in ln-separation
                    'spacingfunc'=>"const(log(3.0),log(1e4),$n)",
                    # precode has to calculation the period (required for binary_c)
                    'precode'=>"my\$sep=exp(\$lnsep);my\$per=calc_period_from_sep(\$m1,\$m2,\$sep);",
                    # flat in log-separation (dN/da~1/a) distribution (Opik law)
                    'probdist'=>'const(log(3.0),log(1e4))',
                    # phase volume contribution
                    'dphasevol'=>'$dlnsep'
                }
            }
            elsif($binary_grid::grid_options{persep_distribution} eq 'Izzard2012')
            {
                # period distribution : Rob's bastard
                $binary_grid::grid_options{'flexigrid'}{'grid variable '.$nvar++}=
                {
                    'name'=>'log10per',
                    'longname'=>'log10(Orbital_Period)',
                    'range'=>['$log10permin','10.0'],
                    'resolution'=>$n,
                    'spacingfunc'=>"const(\$log10permin,10.0,$n)",
                    'preloopcode'=>'my $log10permin = distribution_functions::log10_minimum_period_without_RLOF($m1,$m2);',
                    'precode'=>"my \$per=10.0**\$log10per;my\$sep=calc_sep_from_period(\$m1,\$m2,\$per);",
                    'probdist'=>'Izzard2012_period_distribution($per,$m1,$log10permin)',
                    'dphasevol'=>'('.value_of_log10.'*$dlog10per)',
                };
            }
            else
            {
                printf STDERR "Unknown persep_distribtution option \"%s\"\n",
                $binary_grid::grid_options{persep_distribution};            
            }
        }
    }
}


sub modules_etc
{
    $|=1; # enable this line for auto-flushed output
    use strict;
    use rob_misc;
    use threads;
    use Sort::Key qw(nsort);
    use 5.16.0;
    use binary_grid;
    use threads::shared;
    use Carp qw(confess);
    use Proc::ProcessTable;
}

sub bin
{
    # bin $data of type $datatype
    # and log it if required
    my ($data, $datatype) = @_;
    return '*' if ($data eq '*');  # handle non-data 
    my $binwidth = $binwidth{$datatype};
    if(!$binwidth)
    {
        print STDERR "Binwidth $binwidth for datatype $datatype (data=$data) is undefined or zero : cannot continue!\n";
        exit;
    }

    if($binwidth < 0.0)
    {
        if(!$data)
        {
            print "Data is \"$data\" (datatype $datatype) but I am supposed to take the log of it :(\n";
            exit;
        }
        $data = log10($data);
        $binwidth = -$binwidth;
    }
    return binary_grid::rebin($data,$binwidth);
}

sub probability_of_observed_binary
{
    # probability that a binary with radial velocity $K
    # is observed as a binary given the observational cutoff $X
    my ($K,$X)=@_;
    return ($K>$X) ? (cos(asin($X/$K))) : 0.0; 
}
