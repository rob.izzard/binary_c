#!/usr/bin/env perl
$|=1; # enable this line for auto-flushed output
use strict;
use rob_misc;
use threads;
use Sort::Key qw(nsort);
use 5.16.0;
use binary_grid2;
use binary_grid::C;
use binary_grid::condor;
use threads::shared;
use Carp qw(confess);
use Proc::ProcessTable;
use Sort::Key qw/nsort/;
use Data::Dumper;
use Sub::Override;
use Math::Trig;

# constants
use constant PI => acos(0.0)*2.0;
use constant GAUSSIAN_PREFACTOR => (1.0/sqrt(2.0*PI));
use constant logmin => 1e-30;
use constant value_of_log10 => log(10.0);
use constant log_ln_converter => (1.0/log(10.0));

############################################################
#                                                          #
# Grid of [C/N] in the thick disc
#
# Version 6 : rewritten for binary_grid2
#                                                          #
############################################################

# number of computational threads to launch
my $nthreads = MAX(1,rob_misc::ncpus()-1);
my $vb=0;
my $highC=1;

# bin widths: a negative value means take log10 first
my %binwidth=(

    # initial/current conditions
    M1=>-0.1, # log (to make initial mass function)
    M2=>-0.1, # log (to make initial mass function)
    McMratio=>0.1, # core mass to mass ratio
    mass_ratio=>0.1,
    orbital_period=>-0.1,
    orbital_separation=>-0.1,
    eccentricity=>0.05,
   
    stellar_mass=>0.1, # generic mass 
    square_bracket=>0.1, # e.g. [Fe/H], [C/N]
    logg=>0.1, # log10(g)
    );

my $observational_radial_velocity_limit=1.0;

# make and evolve the stellar population
my $population = binary_grid2->new(defaults());
# set extra grid vars
$population->set(
    {default_to=>'grid'},
    vb=>$vb,
    outdir => '/tmp',
    # persep_distribution is
    # Opik : default flat in log-a grid
    # Izzard2012 : Rob's function, DM91 at low mass, Sana et al at high mass
    persep_distribution => 'Izzard2012',
    nres=>undef,
    nthreads=>$nthreads
    );
$population->parse_args();
setup_grid($population);

############################################################
# set up condor
$population->set(
    condor=>0, # to use condor, set condor=1 on cmd line
    condor_njobs=>20, # run 4 jobs
    condor_dir=>'/home/rgi/data/condor_control/CND6', # working directory for scripts etc.
    condor_streams=>0, # if 1 stream to stdout/stderr (warning: lots of network!)
    condor_memory=>1024, # RAM (MB) per job
    condor_universe=>'vanilla', # always vanilla
    condor_resubmit_finished=>0, # do not resubmit finished jobs
    condor_resubmit_crashed=>0, # do not resubmit crashed jobs
    condor_resubmit_submitted=>0, # do not resubmit submitted jobs
    condor_resubmit_running=>0, # do not resubmit running jobs
    condor_postpone_join=>1, # if 1 do not join on condor, join elsewhere
    condor_join_machine=>undef,
    condor_join_pwd=>undef,
    );
$population->parse_args();

if($population->{_grid_options}{condor})
{
    print "Running on Condor : DIR $population->{_grid_options}{condor_dir} on $population->{_grid_options}{condor_njobs} jobs\n";
}
else
{
    print "Running on local machine\n";
}

############################################################
# evolve the population
$population->evolve();

# output results
output($population) if($population->output_allowed());

exit(0); # done!

############################################################
############################################################
############################################################

sub output
{
    my $population = shift;
    my $results = $population->results;

    # output results
    mkdir $population->{_grid_options}{outdir};
    
    foreach my $result (sort keys %$results)
    {
	next if($result eq 'thread_number');
	print "Saving results for output subhash '$result' in directory $population->{_grid_options}{outdir}\n";
        my $h = $results->{$result};

        if($result=~/CN vs(.*)vs(.*)/)
        {
            my $x = $1;
            my $y = 'CN';
            my $z = $2;

            # distributions as f(CN, mass)
            my $filename = $population->{_grid_options}{outdir}.'/per_cell_map_M_CN_'.$z;
            $filename =~ s/\s/_/g;
            print "Dump per-cell map data to $filename\n";

            open(MAP,'>',$filename)||die;
            print MAP "# distribution of x=$x, y=$y, z=$z\n";
            map
            {
                print MAP "# bin $_ width $binwidth{$_}\n";
            }sort keys %binwidth;

            foreach my $m (nsort keys %$h)
            {
                foreach my $CN (nsort keys %{$h->{$m}})
                {
                    foreach my $logP (nsort keys %{$h->{$m}->{$CN}})
                    {
                        printf MAP "%g %g %g %s\n",$m,$CN,$logP,$h->{$m}->{$CN}->{$logP};
                    }
                    print MAP "\n";
                }
            }
            close MAP;
        }
        elsif($result=~/^CN/)
        {
            # surface pots 
            my $filename = $population->{_grid_options}{outdir}.'/'.$result.'-'.
                'Z='.$population->{_bse_options}{metallicity}.
                '-'.($population->{_grid_options}{binary}==0 ? 'single' : 'binary');
            
            $filename =~ s/\s/_/g;
            print "Dump to $filename\n";

            open(CN,'>',$filename)||die;
            foreach my $m (nsort keys %$h)
            {
                foreach my $CN (nsort keys %{$h->{$m}})
                {
                    printf CN "%g %g %s\n",$m,$CN,$h->{$m}->{$CN};
                }
                print CN "\n";
            }
            close CN;
            print "See $filename\n";
        }
    }

    # save the stats in a Perl-loadable hash form
    open(FP,'>',$population->{_grid_options}{outdir}.'/stats')||die;
    print FP Data::Dumper::Dumper($results);
    close FP;
}

sub parse_data
{
    my ($population,$results) = @_;
    my $Crichstar = 0;
    my $nthread=$population->{_grid_options}{'thread_num'};
    #my $vb=1;
    while(1)
    {
        my $la = $population->tbse_line();

        print "PARSE LINE ",join(' ',@$la),"\n" if($vb);
        my $header = shift @$la;
        last if($header eq 'fin');

	if($header eq 'CND')
	{         
            print "HEADER IS CND\n" if($vb);
            my %initial;
            my %current;

            # time and dtp weight
            $current{time} = shift @$la; 
            $current{dtp} = shift @$la;

            # skip everything if dtp = 0
            next if(!$current{dtp});

            # initial conditions
            $initial{orbital_period} = $population->bin((shift @$la), 'orbital_period');
            $initial{orbital_separation} = $population->bin((shift @$la), 'orbital_separation');
            $initial{eccentricity} = $population->bin((shift @$la),'eccentricity');

            $initial{M1} = shift @$la;
            $initial{M2} = shift @$la; 
            if($initial{M2} ne '*')
            {
                $initial{mass_ratio} = $population->bin($initial{M1}/$initial{M2},'mass_ratio');
                $initial{M2} = $population->bin($initial{M2},'M2');
            }
            $initial{M1} = $population->bin($initial{M1},'M1');

            # current conditions
            $current{orbital_period} = $population->bin((shift @$la), 'orbital_period');
            $current{orbital_separation} = $population->bin((shift @$la), 'orbital_separation');
            $current{eccentricity} = $population->bin((shift @$la),'eccentricity');
            $current{M1} = shift @$la;
            $current{M2} = shift @$la;
            if($current{M2} ne '*')
            {
                $current{mass_ratio} = $population->bin($current{M1}/$current{M2},'mass_ratio');
                $current{M2} = $population->bin($current{M2},'M2');
            }
            $current{M1} = $population->bin($current{M1},'M1');

            if($vb)
            {
                map
                {
                    print "INITIAL $_ = $initial{$_}\n";
                }sort keys %initial;
                map
                {
                    print "CURRENT $_ = $current{$_}\n";
                }sort keys %current;
            }

            # log each star separately
            my $logged_initial_and_orbit = 0;

            foreach my $offset (0,7)
            {
                if($la->[$offset] ne '*')
                {
                    my $st = $la->[$offset+0]; # 0 is stellar type 
                    my $K =  $la->[$offset+1]; # 1 is K (radial velocity)
                    my $m = $population->bin($la->[$offset+2],'stellar_mass'); # 2 is mass
                    my $CN = $population->bin($la->[$offset+3],'square_bracket'); # 3 = [C/N]
                    my $mc = $population->bin($la->[$offset+4],'stellar_mass'); # 4 is core mass
                    my $logg = $population->bin($la->[$offset+5],'logg'); # 5 is logg
                    my $was_blue_straggler = $la->[$offset+6]; # 6 is whether it was a blue straggler (1) or not (0)
                    my $McMratio = $population->bin($la->[$offset+4]/MAX(1e-30,$la->[$offset+2]),'McMratio'); # Mc/M
                    
                    print "LINE at t=$current{time} ($la->[$offset]) m=$m, st=$st , [C/N]=$CN += $current{dtp} from $_ (K=$K)\n"if($vb);

                    # detect C-rich stars
                    $Crichstar = 1 if($CN > 2.0);
                                        
                    # save cell maps in  mass-[C/N] plane
                    # current conditions
                    $$results{'CN vs mass vs orbital period'}{$m}{$CN}{$current{orbital_period}} += $current{dtp};
                    $$results{'CN vs mass vs orbital separation'}{$m}{$CN}{$current{orbital_separation}} += $current{dtp};
                    $$results{'CN vs mass vs eccentricity'}{$m}{$CN}{$current{eccentricity}} += $current{dtp};
                    $$results{'CN vs mass vs stellar type'}{$m}{$CN}{$st} += $current{dtp};
                    $$results{'CN vs mass vs mass ratio'}{$m}{$CN}{$current{mass_ratio}} += $current{dtp};
                    $$results{'CN vs mass vs M1'}{$m}{$CN}{$current{M1}} += $current{dtp};
                    $$results{'CN vs mass vs M2'}{$m}{$CN}{$current{M2}} += $current{dtp};
                    $$results{'CN vs mass vs coremass'}{$m}{$CN}{$mc} += $current{dtp};
                    $$results{'CN vs mass vs McMratio'}{$m}{$CN}{$McMratio} += $current{dtp};
                    $$results{'CN vs mass vs logg'}{$m}{$CN}{$logg} += $current{dtp};
                    
                    # initial conditions
                    $$results{'CN vs mass vs initial orbital period'}{$m}{$CN}{$initial{orbital_period}} += $current{dtp};
                    $$results{'CN vs mass vs initial orbital separation'}{$m}{$CN}{$initial{orbital_separation}} += $current{dtp};
                    $$results{'CN vs mass vs initial eccentricity'}{$m}{$CN}{$initial{eccentricity}} += $current{dtp};

                    # save distributions in CN vs whatever planes
                    # e.g. [C/N] vs mass, core_mass and logg
                    $$results{'CN vs mass'}{$m}{$CN} += $current{dtp};
                    $$results{'CN vs core_mass'}{$mc}{$CN} += $current{dtp};
                    $$results{'CN vs logg'}{$logg}{$CN} += $current{dtp};
                     
                    my $bssflag = $was_blue_straggler ? 'was_BSS' : 'never_BSS';
                    $$results{'CN vs mass '.$bssflag}{$m}{$CN} += $current{dtp};
                    $$results{'CN vs core_mass '.$bssflag}{$mc}{$CN} += $current{dtp};
                    $$results{'CN vs logg '.$bssflag}{$logg}{$CN} += $current{dtp};

                    # save as a function of stellar type
                    $$results{'CN vs mass stellar_type '.$st}{$m}{$CN} += $current{dtp};
                    $$results{'CN vs core_mass stellar_type '.$st}{$mc}{$CN} += $current{dtp};
                    $$results{'CN vs logg stellar_type '.$st}{$logg}{$CN} += $current{dtp};

                    # detect stars as single or binary depending on the 
                    # radial velocity.
                    my %weights;
                    if($population->{_grid_options}{'binary'}==0 || abs($K)<1e-14)
                    {
                        # true single stars (includes mergers) 
                        $weights{'K.eq.0'} = 1.0;
                    }
                    else
                    {
                        # could be observed as a binary
                        my $w = probability_of_observed_binary($K,$observational_radial_velocity_limit);
                        # binary stars which would be observed as 'binary'
                        $weights{'K.gt.1'} = $w;
                        # binary stars which would be observed as 'single'
                        $weights{'K.lt.1'} = 1.0-$w;
                    }

                    foreach my $radial_velocity_class (keys %weights)
                    {
                        printf "Radial velocity class $radial_velocity_class : weight %g\n", $weights{$radial_velocity_class} if($vb);

                        # modulate dtp by the weight;
                        my $dtp = $weights{$radial_velocity_class} * $current{dtp};

                        # CN vs mass, core mass and logg for different radial velocity classes
                        $$results{'CN vs mass '.$radial_velocity_class}{$m}{$CN} += $dtp;
                        $$results{'CN vs mass '.$radial_velocity_class.' stellar_type '.$st}{$m}{$CN} += $dtp;
                        $$results{'CN vs core_mass '.$radial_velocity_class}{$mc}{$CN} += $dtp;
                        $$results{'CN vs core_mass '.$radial_velocity_class. ' stellar_type '.$st}{$mc}{$CN} += $dtp;
                        $$results{'CN vs logg '.$radial_velocity_class}{$logg}{$CN} += $dtp;
                        $$results{'CN vs logg '.$radial_velocity_class.' stellar_type '.$st}{$logg}{$CN} += $dtp;

                        # binary fraction vs mass only (histograms)
                        $$results{'binary fraction histograms'}{$radial_velocity_class}{$m} += $dtp;
                        $$results{'binary fraction histograms stellar_type '.$st}{$radial_velocity_class}{$m} += $dtp;
                        $$results{'binary fraction histograms mc'}{$radial_velocity_class}{$mc} += $dtp;
                        $$results{'binary fraction histograms logg'}{$radial_velocity_class}{$logg} += $dtp;

                        # binary fraction vs mass and CN (map)
                        $$results{'binary fraction map'}{$radial_velocity_class}{$m}{$CN} += $dtp;
                        $$results{'binary fraction map mc'}{$radial_velocity_class}{$mc}{$CN} += $dtp;
                        $$results{'binary fraction map stellar_type '.$st}{$radial_velocity_class}{$mc}{$CN} += $dtp;
                        $$results{'binary fraction map logg'}{$radial_velocity_class}{$logg}{$CN} += $dtp;
                    }
                }
            }
        }
    }

    if($Crichstar)
    {
        open(CRICH,'>>','/tmp/Crich');
        print CRICH $population->{_grid_options}->{args},"\n";
        close CRICH;
    }
}


sub defaults
{
    return (
        # physics options
        'metallicity' => 0.004,
        'wd_sigma' => 0.0,
        'c13_eff' => 1.0,
        'acc2' => 1.5,
        'tidal_strength_factor' => 1.0,
        'alpha_ce' => 0.2,
        'lambda_ce' => -1, # -1 = automatically set
        'lambda_ionisation' => 0.0, # 0.0
        'delta_mcmin' => 0.0,
        'lambda_min' => 0.0,
        'minimum_envelope_mass_for_third_dredgeup' => 0.5,
        'max_evolution_time' => 13000,
        'RLOF_interpolation_method' => 0, # 0 = binary_c, 1 = BSE 
        'log_filename' => '/dev/null', # no log required

        # grid options
        'parse_bse_function_pointer' => \&parse_data,
        'threads_stack_size' => 32, # MBytes
        'nice' => 'nice -n +1', # nice command e.g. 'nice -n +10' or '' 
        'timeout' => 30000, # seconds until timeout
        'log_args' => 0,
        'save_args' => $highC,
        'vb' => 1,

        internal_buffering => 0,
        internal_buffering_compression => 9,

        # This is the path to the binary_c directory
        'rootpath' => "$ENV{HOME}/progs/stars/binary_c",

        # The name of the binary_c executable
        'prog' => 'binary_c-CND6',

        # VARIABLES TO BE CHANGED : maybe set on the command line!
        'binary' => 1, # single stars -> 0, binary stars -> 1
        'CRAP_parameter' => 1e3, # 1e3 = Tout
        'WRLOF_method' => 1,  # 1 = q-dep, 2=quadratic
        'RLOF_method' => 3, # 3 = Claeys et al 2014

        # age range for star formation in the thick disc
        # Snaith et al 2015 suggest start=13Gyr, end=8Gyr
        # and is ~ constant (and large!)
        'thick_disc_start_age' => 10e3,
        'thick_disc_end_age' => 4e3,

        # logg range : set to -+100.0 for all stars
        'thick_disc_logg_min'=>-100.0,
        'thick_disc_logg_max'=>+100.0,
        
        # we want array refs, not lines, from tbse_line
        return_array_refs => 1,

        );
}

sub setup_grid
{
    my $population = shift;
 
    # resolution
    $population->{_grid_options}{nres} //=  ($population->{_grid_options}{binary}==0 ? 1000 : 40); 
    
    # Mass 1
    my $nvar=0;
    my $mmin=0.1; my $mmax=6;

    # testing
    if($highC)
    {
        $mmin=1.0; 
        $mmax=6;
    }

    my $n = $population->{_grid_options}{nres};

    my $use_bastard = 0;

    if($use_bastard)
    {  
        my $sampling_factor=1.0/4.0; # over-resolution (Nyquist/Shannon)
        my $dt = 100.0; # time resolution in Myr

	# new bastard distribution of single and binary stars
	distribution_functions::bastard_distribution(
            $population, {
	    mmin=>$mmin,
	    mmax=>$mmax,

	    # use time-adaptive mass resolution for M1
	    time_adaptive=>{
		max_evolution_time=>$population->{_bse_options}{max_evolution_time},
		stellar_lifetime_table_nm=>1000,
		nthreads=>$nthreads,
		thread_sleep=>1,
		mmin=>$mmin,
		mmax=>$mmax,
		mass_grid_log10_time=>0,
		mass_grid_step=>($dt*$sampling_factor),
		extra_flash_resolution=>0, # 1 = broken?
		mass_grid_nlow_mass_stars=>10,
		debugging_output_directory=>'/tmp',
		max_delta_m=>1.0,
		savegrid=>1,
		vb=>0,
	    },	    

            m2min=>$mmin,
            nm2=>$n,
            nper=>$n,
            necc=>undef,
            qmin=>0.0,
            qmax=>1.0,
            useecc=>undef	    
				  		     });
    }
    else
    {
        $population->add_grid_variable(
            'name'=> 'lnm1', 
            'longname'=>'Primary mass', 
            'range'=>["log($mmin)","log($mmax)"],
            'resolution'=> $n,
            'spacingfunc'=>"const(log($mmin),log($mmax),$n)",
            'precode'=>'my $m1=exp($lnm1); my $eccentricity=0.0;',
            'probdist'=>"ktg93(\$m1)*\$m1",
            'dphasevol'=>'$dlnm1'
        );

        # Binary stars: Mass 2 and Separation
        if($population->{_grid_options}{'binary'})
        {

            $population->add_grid_variable(
                # name of the variable
                'name'=>'m2',
                # long name (for verbose logging)
                'longname'=>'Secondary mass',
                # range of the parameter space
                'range'=>[0.1,'$m1'],
                # resolution 
                'resolution'=>$n,
                # constant grid spacing function
                'spacingfunc'=>"const(0.1,\$m1,$n)",
                # flat-q -> M2 distribution between 0.1 and m1
                'probdist'=>"const(0.1,\$m1)",
                # phase volume contribution
                'dphasevol','$dm2'
                );

            if($population->{_grid_options}{persep_distribution} eq 'Opik')
            {
                $population->add_grid_variable(
                    # name of the variable
                    'name'=>'lnsep',
                    # long name (for verbose logging)
                    'longname'=>'ln(Orbital_Separation)',
                    # range of the separation space
                    'range'=>['log(3.0)','log(1e4)'],
                    # resolution
                    'resolution'=>$n,
                    # constant spacing in ln-separation
                    'spacingfunc'=>"const(log(3.0),log(1e4),$n)",
                    # precode has to calculation the period (required for binary_c)
                    'precode'=>"my\$sep=exp(\$lnsep);my\$per=calc_period_from_sep(\$m1,\$m2,\$sep);",
                    # flat in log-separation (dN/da~1/a) distribution (Opik law)
                    'probdist'=>'const(log(3.0),log(1e4))',
                    # phase volume contribution
                    'dphasevol'=>'$dlnsep'
                    );
            }
            elsif($population->{_grid_options}{persep_distribution} eq 'Izzard2012')
            {
                # period distribution : Rob's bastard
                $population->add_grid_variable(
                    'name'=>'log10per',
                    'longname'=>'log10(Orbital_Period)',
                    'range'=>['$log10permin','10.0'],
                    'resolution'=>$n,
                    'spacingfunc'=>"const(\$log10permin,10.0,$n)",
                    'preloopcode'=>'my $log10permin = POSIX::log10($self->minimum_period_for_RLOF($m1,$m2));',
                    'precode'=>"my \$per=10.0**\$log10per;my\$sep=calc_sep_from_period(\$m1,\$m2,\$per);",
                    'probdist'=>'Izzard2012_period_distribution($per,$m1,$log10permin)',
                    'dphasevol'=>'('.value_of_log10.'*$dlog10per)',
                    );
            }
            else
            {
                printf STDERR "Unknown persep_distribtution option \"%s\"\n",
                $population->{_grid_options}{persep_distribution};            
            }
        }
    }
    $population->parse_args();
}


sub binary_grid2::bin
{
    # bin $data of type $datatype
    # and log it if required
    my $population = shift;
    my ($data, $datatype) = @_;
    return '*' if ($data eq '*');  # handle non-data 
    my $binwidth = $binwidth{$datatype};
    if(!$binwidth)
    {
        print STDERR "Binwidth $binwidth for datatype $datatype (data=$data) is undefined or zero : cannot continue!\n";
        exit;
    }

    if($binwidth < 0.0)
    {
        if(!$data)
        {
            $population->report_system_error("Data is \"$data\" (datatype $datatype) but I am supposed to take the log of it :(\n");
            exit;
        }
        $data = log10($data);
        $binwidth = -$binwidth;
    }
    return $population->rebin($data,$binwidth);
}

sub probability_of_observed_binary
{
    # probability that a binary with radial velocity $K
    # is observed as a binary given the observational cutoff $X
    my ($K,$X)=@_;
    return ($K>$X) ? (cos(asin($X/$K))) : 0.0; 
}
