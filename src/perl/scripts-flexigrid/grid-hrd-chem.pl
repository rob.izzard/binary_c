#!/usr/bin/env perl 
$|=1; # enable this line for auto-flushed output
use 5.16.0;
use strict;
use rob_misc;
use binary_grid;
use Sort::Key qw/nsort/;

#
# Grid to calculate statistics regarding the nature of
# binary systems at the moment of supernova explosion.
#

my $mmin;
my $mmax;
my $outdir;
my $nm1;

# cmd line might give mass range
if(defined $ARGV[0])
{
    ($mmin,$mmax,$nm1)=($ARGV[0]=~/^(\d+(?:\.\d*)?)-(\d+(?:\.\d*)?)-?(\d+(?:\.\d*)?)?/);
    $outdir = $ENV{HOME}.'/tex/talks/Mongolia/images/HRDchem/M'.$ARGV[0];
    shift @ARGV;
}

print "range: $mmin $mmax $nm1\n";

# default grid 
$mmin //= 4;
$mmax //= 8;
$nm1 //= int($mmax-$mmin+0.01);

# bin data in the HRD?
my $bindata = 0; # 0 = no binning, 1 = use binning
my $binwidth=0.01;

# results hash
my %results;

# output directory
$outdir //= $ENV{HOME}.'/data/HRDchem';

# do stuff
defaults();
parse_args(); 
setup_binary_grid();
secondary_setup();

# go
flexigrid(4);

output();

exit(0);

############################################################
############################################################
############################################################

sub parse_bse
{
    # this gets called by the binary_grid module, it parses the 
    # lines of output from binary_c/nucsyn
    my $brk=0; 

    # you should set data in the $h hash pointer, which is 
    # then passed to the join_thread function (below) for you
    # to add up

    # if you want to use the binary_cd you should make sure this function
    # contains all the data it needs because the remote machine will
    # want to do data processing on the fly!
    my $h=shift;
    
    while($brk==0)
    {
	$_=tbse_line();
	chomp;
	if($_ eq 'fin')
	{
	    $brk=1; # the end of output
	}
	elsif(s/^HRDCHEM //o)
	{
	    # split data
	    my %data;
	    while(/(\S+)=(\S+)/g)
	    {
		$data{$1}=$2;
		#print "Set $1 = $2\n";
	    }

	    # make HRD co-ord
	    my $coord = sprintf "%g %g",
	    ($bindata ? bin_data($data{logTeff},$binwidth) : $data{logTeff}),
	    ($bindata ? bin_data($data{logL},$binwidth) : $data{logL});

	    # make mass label at the start of the HRD
	    if(!defined $$h{ZAMS}{$data{ZAMSmass}})
	    {
		$$h{ZAMS}{$data{ZAMSmass}} = $coord;
	    }

	    # remove non-isotope data
	    delete $data{logL};
	    delete $data{logTeff};
	    delete $data{ZAMSmass};

	    foreach my $isotope (keys %data)
	    {		
		$$h{HRD}{$isotope}{$coord} = $data{$isotope};
	    }
	}
    }
}


sub defaults
{
    $SIG{PIPE}='IGNORE';
    grid_defaults();
    renice_me(5); # play nice

    # metallicity : set early to make directory names
    $binary_grid::bse_options{z}=0.019; 


    # memory use in MB
    my $mem = 400;
    $binary_grid::grid_options{'threads_stack_size'}=$mem; # big stack for lots of data

    # only single stars
    $binary_grid::grid_options{binary}=0;

    $binary_grid::grid_options{prog}='binary_c';

    my $version=evcode_version_string();

    $binary_grid::grid_options{results_hash}=\%results;

    # set up a pointer to the parser function (see parse_bse())
    $binary_grid::grid_options{parse_bse_function_pointer}=\&parse_bse;

    $binary_grid::grid_options{log_args}=0;
    $binary_grid::grid_options{save_args}=0; 
    $binary_grid::grid_options{lazy_arg_sending}=1; 

    # combine reset_stars and defaults calls
    $binary_grid::grid_options{reset_stars_defaults}=1;

    $binary_grid::bse_options{max_evolution_time}=13700;

    # buffering
    $binary_grid::bse_options{internal_buffering}=1;
    $binary_grid::bse_options{internal_buffering_compression}=0;
    
    # 1MYR overkill resolution
    $binary_grid::bse_options{maximum_timestep}=1000.0;

    # code options
    $binary_grid::grid_options{nice}='nice -n +19'; 
    
    $binary_grid::grid_options{vb}=1; 
    $binary_grid::grid_options{tvb}=0; 
    $binary_grid::grid_options{nmod}=10;
    $binary_grid::grid_options{log_dt_secs}=5;
    $binary_grid::grid_options{timeout}=0; # 0=off

    $binary_grid::grid_options{thread_max_freeze_time_before_warning}=120; # lots of output = slow
}


sub parse_args
{
    # parse command line arguments
    my $i;
    parse_grid_args(@ARGV);
    for($i=0;$i<=$#ARGV;$i++)
    {
        my $arg=$ARGV[$i];
	if($arg =~/help/o)
        {
            help();
        }
        elsif($arg=~/([^=]+)\=(.*)/o)
        {
            if($binary_grid::bse_options{$1} ne '')
	    {
		# this is a BSE option
		#print "SET bse option $1 to $2\n";
		#eval "\$binary_grid::bse_options{'$1'}=\"$2\"";
	    }
	    elsif($binary_grid::grid_options{$1} ne '')
	    {
		# this is a grid option
		#print "do NOT set grid option $1 to $2, it is already ",$binary_grid::grid_options{$1},"\n";
		#eval "\$binary_grid::grid_options{'$1'}=\"$2\"";
	    }
	    else
	    {
		eval "\$$1=$2";
		print "EVAL \$$1=$2\n";
	    }
	}
        else
        {
            print STDERR "EH? what is $arg ???\n";
        }
    }
}


sub output
{
    # output data
    my $d=$binwidth*0.5;


    # output ZAMS as a function of mass
    open(OUT,'>',"$outdir/zams.dat")||die;
    foreach my $m (nsort keys %{$results{ZAMS}})
    {
	printf OUT "%s %g\n",$results{ZAMS}{$m},$m;
    }
    close OUT;

    # output per-isotope HRD
    foreach my $isotope (keys %{$results{HRD}})
    { 
	print "Process isotope $isotope\n";

	open(OUT,'>',"$outdir/chem-hrd-$isotope.dat")||die;

	# first normalize by probability per box
	# (gives weight where it's due)
	foreach my $coord 
	    # sort to output smallest abundances first
	    (sort {
	    $results{HRD}{$isotope}->{$a} <=> 
		$results{HRD}{$isotope}->{$b}
	    }keys %{$results{HRD}{$isotope}})
	{
	    my @x=split(/\s+/o,$coord);
	    my $y=$results{HRD}{$isotope}->{$coord};
	    #/$results{HRD}{'ptot'}->{$coord};
	    if($bindata)
	    {

		printf OUT "%g %g %g\n%g %g %g\n%g %g %g\n%g %g %g\n\n%g %g %g\n%g %g %g\n%g %g %g\n%g %g %g\n\n\n",

		$x[0]-$d,$x[1]-$d,$y,
		$x[0]+$d,$x[1]-$d,$y,
		$x[0]+$d,$x[1]+$d,$y,
		$x[0]-$d,$x[1]+$d,$y,
		
		$x[0]-$d,$x[1]+$d,$y,
		$x[0]+$d,$x[1]+$d,$y,
		$x[0]+$d,$x[1]-$d,$y,
		$x[0]-$d,$x[1]-$d,$y;
	    }
	    else
	    {
		printf OUT "%g %g %g\n",$x[0],$x[1],$y;
		state $prev;
		if((!defined $prev) || ($prev != $x[0]))
		{
		    $prev=$x[0];
		    #print OUT "\n";
		} 
	    }
	}
	close OUT;
    }

}


sub secondary_setup
{
    # things to set up after everything else
    

    ##########################################
    # build $outdir : the output directory
    ##########################################
    print "Make outdir=$outdir\n";
    mkdirhier($outdir);
    
    ############################################################
    # set up grid and appropriate resolution
    ############################################################
    
    # Mass 1
    my $nvar=0;

    # simple evenly-spaced grid
    $binary_grid::grid_options{'flexigrid'}{'grid variable '.$nvar++}=
    {
	'name'=> 'm1', 
	'longname'=>'Primary mass', 
	'range'=>["$mmin","$mmax"],
	'resolution'=> $nm1,
	# const dm spacing function
	'spacingfunc'=>"const($mmin,$mmax,$nm1)",
	'precode'=>'my $eccentricity=0.0;',
	'dphasevol'=>'$dm1',
	'gridtype'=>'edge',
	'noprobdist'=>1,
    };
}
