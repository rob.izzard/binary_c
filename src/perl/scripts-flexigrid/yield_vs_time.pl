#!/usr/bin/env perl

use 5.16.0;
use strict;
use Sort::Key qw(nsort);
use rob_misc;
use binary_grid2;
use binary_grid::C;
use binary_grid::condor;
use Data::Dumper;
use Carp qw(confess);
use File::Path qw(make_path);
use File::Basename;
use Cwd;
use Term::ANSIColor;

$|=1; # enable this line for auto-flushed output

# Version 2.0pre26 : to match binary_c version number

#
# yvt2 is the original dataset : this should be regenerated
#
# yvt2a is new as of 6/5/2014 : should be the same, but just rebuilt
# yvt2a-20 is for resolution 20 (m2,per)
# yvt2a-40 is running
#
# yvt3 is the rerun, but it's buggy: binaries are losing mass :(
#
# yvt4 is the new IoA run
# yvt5 is @ the IoA on binary_c version 2.0+, OO

my $voldisk = $ENV{VOLDISK};
my $outdir = $voldisk.'/data/pops/yvttest2-condor';
print "Yield vs time : outdir=$outdir : voldisk=$voldisk, outdir=$outdir\n";

use vars qw($dt $condor_count $differential $distribution $distribution_options $ensemble %ensemble_opts @ensemble_label $exacttimes $force $gnuplot $gridstring @isotopes $idt $meantimes $nbins $nensemble $nisotopes $nensemble1 $nisotopes1 $norm_output $nsources $sources @source_label $version_string $yieldoutfile ); 

my $nthreads = MAX(1,ncpus());
my $population = binary_grid2->new(defaults(),nthreads=>$nthreads);
local_setup($population);
$population->evolve();
all_output($population) if($population->output_allowed());

exit(0);

############################################################
############################################################
############################################################

sub all_output
{
    my $population = shift;

    make_path($outdir); # nb we assume this works...

     # output
    print "\n\nOutputting to $outdir ...\n" if($population->{_grid_options}->{vb});

    $population->results->{total_mass_ejected}=total_mass_ejected();
    printf "Total mass ejected: %g\n",$population->results->{total_mass_ejected};
    printf "Total mass into stars: %g\n",$population->results->{mass_into_stars};
    printf "Return fraction %3.2f %%\n",$population->results->{total_mass_ejected}/$population->results->{mass_into_stars}*100.0;

    if($sources)
    {
	for(my $i=0;$i<=$nsources;$i++)
	{
	    output($i); # output for every source
	}
    }
    else
    {
	output(0); # default to source 0
    }
    
    print "\n";  

    ############################################################
    # The end, output results
    # perhaps plot stuff (could be slow!)
    output_gp() if($gnuplot);

    output_ensemble() if($ensemble);

    print color('reset');
}

sub parse_bse
{ 
    # parser function
    # args (@_) are :
    # 0 : binary_grid2 population object
    # 1 : $h data hash to be populated
    my ($population,$h) = @_;

    # add up mass into stars and notstars (planets/brown dwarfs)
    my $p = $population->{_grid_options}->{progenitor_hash}{prob};
    ($population->{_grid_options}->{progenitor_hash}{m1}>0.1) ? $$h{mass_into_stars} : $$h{mass_into_notstars} += $population->{_grid_options}->{progenitor_hash}{m1} * $p;
    ($population->{_grid_options}->{progenitor_hash}{m2}>0.1) ? $$h{mass_into_stars} : $$h{mass_into_notstars} += $population->{_grid_options}->{progenitor_hash}{m2} * $p;

    while(1)
    {
        # tbse_line is setup to get an array ref 
 	my $linearray = $population->tbse_line();
       
        # next action is based on the line's header 
        my $header = shift @$linearray;
        last if ($header eq 'fin');

	if($ensemble && $header=~/^STELLARPOPENSEMBLE\d+__/o)
        {
            process_ensemble_line($h,$linearray);
        }
        elsif($header=~/^DXYIELDbin(\d+)?__/o)
        {
            if(process_yield_line($h,$1//0,$linearray)==666)
            {
                print "666 error\n";
                print $population->{_threadinfo}->{lastargs},"\n";
                print $population->{_grid_options}->{args},"\n";
                exit;
            }
        }
        else
        {
            if($header ne 'SINGLE_STAR_LIFETIME')
            {
                #print "L: $header @$linearray\n";
            }
        }
    }
}

sub defaults
{
    # local options
    
    my @opts = (

        backend => 'binary_grid::C',

        # always link binary_grid to our local results hash
        vb => 0,
        tvb => 0,
        
        # set the rootpath and the binary_c executable
        rootpath => $ENV{HOME}.'/progs/stars/binary_c',
        srcpath => $ENV{HOME}.'/progs/stars/binary_c/src',
        prog => 'binary_c-pops',

        # no timeouts, no logging
        timeout => 0,
        log_filename => '/dev/null',
        log_dt_secs => 5,
        nmod => 0,

        # use which parser function?
        parse_bse_function_pointer => \&main::parse_bse,
        return_array_refs => 1,

        threads_stack_size => 50, # MBytes

        comenv_prescription => 0,
        alpha_ce => 0.5, # 1
        lambda_ce => -1.0, # -1 = automatically set
        lambda_ionisation => 0.0, # 0.0
        hachisu_disk_wind => 1, # 1 for on
        hachisu_qcrit => -1, # ignore if negative
        
        RLOF_method => 3, # RLOF method: 
        # 0 = H02, 1 = Adaptive R=RL,  3 = Claeys et al 2014 

        # Evolution splitting
        evolution_splitting => 0,
        evolution_splitting_sn_n => 100,
        evolution_splitting_maxdepth => 1,
        
        BH_prescription => 2,

        mass_accretion_for_eld => 0.15,

        # star formation stuff
        max_evolution_time => 15000, # 200 = testing

        # time resolution
        maximum_timestep => 1000,

        # core and 3dup algorithms:
        # 0 : Hurley (if no NUCSYN), Karakas (if NUCSYN)
        # 1 : Hurley 2002 (overshooting)
        # 2 : Karakas 2002 (non-overshooting)
        # 3 : Stancliffe (3dup only)
        AGB_core_algorithm => 1,
        AGB_3dup_algorithm => 2,
        

        # code options
        nice => 'nice -n +0', # nice command e.g. 'nice -n +10' or '' 
        
        timeout => 0, # seconds until timeout
        
        log_args => 1,
        save_args => 1,
        sort_args => 1,

        tvb => 1, # thread logfile

        # fast as possible
        'no signals' => 1, # 0=80%, 1=80%
        timers => 0,
        );

    return @opts;
};

sub local_setup
{
    my $population = shift;

    # parse cmd line args
    $population->parse_grid_args();

    # yvt options
    # follow different sources of matter?
    $sources=1; # 0 : no, 1 : yes (requires appropriate switches
    # in binary_c header files)
    $differential=1; # if set to 1 output the time derivative 
    $ensemble=1; #  do ensembles by default

    # NB as of V1.20pre68 (SVN 1979+) $sources, $ensemble and $differential
    # are set based on binary_c --version
    
    $yieldoutfile=''; # if blank use default (compatible with gce.pl)

    $norm_output='mass_into_stars'; # how to normalize output
    # can be 'mass_into_stars' (default), 'total_mass_ejected','none'
    
    # output options (see output_ensemble())
    $ensemble_opts{largefile}=1;
    $ensemble_opts{smallfiles}=0;
    
    @isotopes = $population->isotope_list(); # uses binary_c --version
    
    $gnuplot=0; # plot or not

    $meantimes=0; # if 1 then output at time bin midpoints
    # otherwise set the time to the end of the time bin

    # set this to 1 if the stellar code outputs at exact intervals
    # (this is true for all modern binary_c)
    $exacttimes=1;
    
    $version_string = $population->evcode_version_string();
    
    # used lots, calculate once (* is faster than /, or at least used to be ...)
    $idt=1.0/$dt;

    # calculate number of bins (+1, so we can use < instead of <= in fors)
    $nbins=int($population->{_bse_options}{max_evolution_time}/$dt)+1;

    # check if differential is correct
    if($version_string=~/NUCSYN_LOG_BINARY_DX_YIELD is defined/)
    {
	if(!$differential)
	{
	    $differential=1;
	    print "Overriding differential to $differential based on binary_c --version\n";
	}
    }
    else
    {
	if($differential)
	{
	    $differential=0;
	    print "Overriding differential to $differential based on binary_c --version\n";
	}
    }

    # check if ensemble is correct
    if($version_string=~/GCE population ensemble is disabled/)
    {
	if($ensemble)
	{
	    $ensemble=0;
	    print "Overriding ensemble to $ensemble based on binary_c --version\n";
	}
    }
    else
    {
	if(!$ensemble)
	{
	    $ensemble=1;
	    print "Overriding ensemble to $ensemble based on binary_c --version\n";
	}
    }

    if($version_string=~/GCE population ensemble is enabled with N=(\d+) types/)
    {
	$nensemble=$1;
	$nensemble1=$nensemble+1;
    }

    if($version_string=~/NUCSYN_GCE is enabled with (\d+) isotopes/)
    {
	$nisotopes=$1;
        $nisotopes1 = $nisotopes+1;
    }

    # check if sources is correct
    if($version_string=~/NUCSYN_ID_SOURCES is defined/)
    {
	if(!$sources)
	{
	    $sources=1;
	    print "Overriding sources to $sources based on binary_c --version\n";	}
    }
    else
    {
	if($sources)
	{
	    $sources=0;
	    print "Overriding sources to $sources based on binary_c --version\n";
	}
    }

    if($sources)
    {
	# use version string to get source number
        $nsources = ($version_string=~/NUMBER_OF_SOURCES=(\d+)/)[0];
        if(!defined $nsources)
        {
            $nsources = ($version_string=~/NUCSYN_SOURCE_NUM=(\d+)/)[0];
        }
	if(!defined $nsources)
	{
	    # fall back on header file
	    $nsources=get_from_header('NUMBER_OF_SOURCES',
				      'nucsyn/nucsyn_macros.h',
				      $population->{_grid_options}{srcpath})//21;
        }
	if(!defined $nsources)
	{
	    # fall back on header file
	    $nsources=get_from_header('NUCSYN_SOURCE_NUM',
				      'nucsyn/nucsyn_macros.h',
				      $population->{_grid_options}{srcpath})//21;
        }
    }
    else
    {
        $nsources = 0;
    }

    print "Control Parameters: \n";
    print color('red bold'),'Sources? ',color('green bold'),$sources,color('reset')," (nsources=",color('green'),$nsources,color('reset'),")\n";
    print color('red bold'),'Ensemble? ',color('green bold'),$ensemble,color('reset'),"\n";
    print color('red bold'),'Differential? ',color('green bold'),$differential,color('reset'),"\n";
    print color('red bold'),'nisotopes = ',color('green bold'),$nisotopes,color('reset'),"\n";

    # get the source and ensemble labels
    @source_label = $population->source_list() if($sources); 
    @ensemble_label = $population->ensemble_list()if($ensemble);

    #if($population->{_bse_options}{yields_dt} eq '')
    {
	# yields_dt controls how often yields are output from binary_c
	# set it : small for greater time accuracy
	#        : large for higher speed (fewer data lines)
	# really you want a happy medium, e.g. 0.1*dt
	$population->{_bse_options}{yields_dt} = $exacttimes!=1 ? 0.1 * $dt : $dt;
    }

    # use time-adaptive mass grid to get yields smooth as f(time)
    $population->{_grid_options}{time_adaptive_mass_grid_step}=$dt;

    #
    # set up the grid
    #

    # grid resolution
    my $sampling_factor=1.0/4.0; # over-resolution (Nyquist/Shannon)

    # M1 and M2
    my $mmin=0.1;
    my $mmax=80.0;
    my $m2min=0.1;

    # HBB in this mass range (for possible period zoom)
    my $m1hbbmin=3.0;
    my $m1hbbmax=8.0; 

    # period distribution D+M91 options
    my $DM91_logpermin=-2.0;
    my $DM91_logpermax=12.0;
    my $DM91_gauss_logmean=4.8;
    my $DM91_gauss_sigma=2.3;
    my (@DM91) = ($DM91_logpermin,
                  $DM91_logpermax,
                  $DM91_gauss_logmean,
                  $DM91_gauss_sigma);

    # grid distribution options hash
    $distribution_options = {
        distribution => 
	    (defined $distribution && $distribution ne '') ? $distribution : 
	    'binary', # 'bastard', 'mixed', 'single' or 'binary'

        # if distribution is 'bastard', everything is handled automatically
        #
        # otherwise:
        # if distribution is "mixed", "single" or "binary"

        # mass 1 spacing : 'log' or 'fixed dt'
        m1spacing => 'fixed dt',

        # period distribution : either DM91 or 'bastard' (f(M1))
        period_distribution => 'bastard',
    
        # period spacing :  'const' or 'agbzoom'
        period_spacing => 'agbzoom',
    };

    my $agbzoom = $distribution_options->{period_spacing} eq 'const' ? 0 : 1;

    # options for the time-adaptive grid: declare them once 
    # so that it's easy to run a bastard distribution or a 
    # single/binary/mixed distribution with the same options
    my $time_adaptive_options = '{
        max_evolution_time=>'.$population->{_bse_options}{max_evolution_time}.',
        stellar_lifetime_table_nm=>1000,
        time_adaptive_mass_grid_log10_time=>0,
        time_adaptive_mass_grid_step=>'.$population->{_grid_options}{time_adaptive_mass_grid_step}.',
        time_adaptive_mass_grid_nlow_mass_stars=>10,
        nthreads=>'.$nthreads.',
        thread_sleep=>1,
        mmin=>'.$mmin.',
        mmax=>'.$mmax.',
        mass_grid_log10_time=>0,
        mass_grid_step=>('.($dt*$sampling_factor).'),
        extra_flash_resolution=>0, # 1 = broken?
        mass_grid_nlow_mass_stars=>10,
        debugging_output_directory=>"/tmp",
        max_delta_m=>1.0,
        savegrid=>1,
        vb=>0,
        metallicity=>'.$population->{_bse_options}{metallicity}.',
        agbzoom=>'.$agbzoom.',
    }';
   


    # $res is the resolution multiplier. 
    # If using the time-adaptive mass grid, m and m1 are ignored.
    my $res = 20; # 100
    my $mres = 20; # 100
    my %resolution=(
        m=>$mres, # ignored if distribution is "bastard"
        m1=>$mres, # ignored if distribution is "bastard"
        m2=>$res,
        per=>$res,
        perzoomf=>0.1, # zoom in ~ AGB RLOF region (0.1)
        perzoomd=>1.0, # delta log P for AGB RLOF (1.0)
        ); 

    my $nvar=0;
    if($distribution_options->{distribution} eq 'bastard')
    {
	# 'Bastard' distribution of single and binary stars,
        # handled automatically by the distribution_functions module.
        distribution_functions::bastard_distribution(
            $population,
            {
                mmin=>$mmin,
                mmax=>$mmax,
                m2min=>$m2min,
                nm2=>$resolution{m2},
                nper=>$resolution{per},
                qmin=>0.0,
                qmax=>1.0,
                necc=>undef,
                useecc=>undef,
                agbzoom=>$agbzoom,
                time_adaptive=>eval($time_adaptive_options),
            }
            );
    }
    else
    {
	# 'mixed' -> mixed single/binary population (binary fraction=f(M1))
        # 'single -> single stars only
        # 'binary' -> binary stars only

	if($distribution_options->{distribution} eq 'mixed')
	{
	    # mixture of single and binary stars, with binary fraction
            # as given by the distribution_functions modules as a function of m1
            $population->add_grid_variable(
		'name'=>'duplicity',
		'longname'=>'Duplicity',
		'range'=>[0,1], 
		'resolution'=>1,
		'spacingfunc'=>'number(1.0)',
		'precode'=>'$self->{_grid_options}{binary}=$duplicity;$self->flexigrid_grid_resolution_shift();',
		'gridtype'=>'edge',
		'noprobdist'=>1,
                );
	}
	elsif($distribution_options->{distribution} eq 'single')
	{
	    # only single stars
	    $population->{_grid_options}{binary}=0;
	}
        elsif($distribution_options->{distribution} eq 'binary')
        {
            $population->{_grid_options}{binary}=1;
        }
        else
        {
            print STDERR "distribution_options->distribution (=$distribution_options->{distribution}) is unknown\n";
            exit;
        }

 	print "Set duplicity = $population->{_grid_options}{binary}\n";
	
        # binary fraction depends on mass, or is 1.0 for 
        # a uniquely single or binary distribution
        my $precode = ' $m1=exp($lnm1);  $eccentricity=0.0; ';

        $precode .= 
            ($distribution_options->{distribution} eq 'mixed') ?
             
            # mixed single/binary : weighting is binary fraction
            'my $binary_fraction = main::binary_fraction($self,$m1);  $self->{_grid_options}{weight}= $self->{_grid_options}{binary}==0 ? 1.0-$binary_fraction : $binary_fraction;   ' :

            # single or binary stars : weighting is just 1.0
            '  my $binary_fraction=($self->{_grid_options}{binary}==0); $self->{_grid_options}{weight}=1.0; ';

	# Mass 1
	if($distribution_options->{m1spacing} eq 'log')
	{
	    # log mass spacing
            $precode = '$m1=exp($lnm1); '.$precode;

            $population->add_grid_variable(
                'name'=> 'lnm1', 
                'longname'=>'Primary mass', 
                'range'=>["log($mmin)","log($mmax)"],
                'resolution'=> "$resolution{m1}", # ignore single stars in approximate resolution calculation
                'spacingfunc'=>"const(log($mmin),log($mmax),\$self->{_grid_options}{binary} ? $resolution{m1} : $resolution{m})",
                'precode'=>$precode,
                'probdist'=>"Kroupa2001(\$m1)*\$m1",
                'dphasevol'=>'$dlnm1',
                );
	}
	elsif($distribution_options->{m1spacing} eq 'fixed dt')
	{
            #print "FIXED DT with opts ",
            #print Data::Dumper->Dump([$time_adaptive_options]),"\n";
            #exit;


	    # fixed-dt mass spacing
	    $population->add_grid_variable(
		'name'=> 'lnm1', 
		'longname'=>'Primary mass', 
		'range'=>["log($mmin)","log($mmax)"],

		# const_dt spacing function options
		'preloopcode'=>"
my \$const_dt_opts=$time_adaptive_options;
spacing_functions::const_dt(\$const_dt_opts,'reset');
",
		# use const_dt function
		'spacingfunc'=>"const_dt(\$const_dt_opts,'next')",

		# and its resolution
		'resolution'=> "spacing_functions::const_dt(\$const_dt_opts,'resolution');",

		'precode'=>$precode,

		# IMF : default to Kroupa 2001
		'probdist'=>"Kroupa2001(\$m1)*\$m1",

		'dphasevol'=>'$dlnm1',
                );
	}
        else
        {
            print STDERR "What is distribution_options->m1spacing?\n";
            exit;
        }

        if($distribution_options->{distribution} ne 'single')
        {
            # Binary stars
            #
            # Mass 2
            $population->add_grid_variable(
                'condition'=>'$self->{_grid_options}{binary}==1',
                'name'=>'q',
                'longname'=>'Mass ratio',
                'range'=>[$m2min.'/$m1',1.0],
                'resolution'=>$resolution{m2},
                'spacingfunc'=>'const('.$m2min.'/$m1,1.0,'.$resolution{m2}.')',
                'probdist'=>"flatsections\(\$q,\[
\{min=>$m2min/\$m1,max=>0.8,height=>1.0\},
\{min=>0.8,max=>1.0,height=>1.0\},
\]\)",
                precode=>'$m2=$q*$m1;',
                dphasevol=>'$dq',
                );

            # period spacing
            my $perspacing;
            if($distribution_options->{period_spacing} eq 'const')
            {
                $perspacing = "const(-1.0,10.0,$resolution{per})";
                
            }
            elsif($distribution_options->{period_spacing} eq 'agbzoom')
            {
                $perspacing = 'agbzoom({
vb=>0,
logperiod=>$log10per//$log10permin,
logpermin=>$log10permin, # set by RLOF minimum
logpermax=>12.0, # DM91 log Period maximum
dlogperiod=>1.0,
m1=>$m1, 
m2=>$m2,
metallicity=>$self->{_bse_options}{metallicity},
resolution=>'.$resolution{per}.',
m1hbbmin=>3.0,
m1hbbmax=>8.0,
zoomfactor=>0.1
})';
            }
            else
            { 
                print STDERR "Period spacing function unknown\n";
                exit;
            }
            
            # period distribution
            if($distribution_options->{period_distribution} eq 'DM91')
            {
                # old code
                $population->add_grid_variable
                (
                    'name'=>'logper',
                    'longname'=>'log(Orbital_Period)',
                    'range'=>[$DM91_logpermin,$DM91_logpermax],
                    'resolution',$resolution{per},
                    'spacingfunc'=>$perspacing,
                    'precode'=>"\$per=10.0**\$logper;\$sep=calc_sep_from_period(\$m1,\$m2,\$per)if(defined(\$m1)&&defined(\$m2));",
                    'probdist'=>"gaussian(\$logper,@DM91)",
                    'dphasevol'=>'$dln10per'
                );
            }
            elsif($distribution_options->{period_distribution} eq 'bastard')
            {
                # period distribution : rob's bastard
                $population->add_grid_variable(
                    'name'=>'log10per',
                    'longname'=>'log10(Orbital_Period)',
                    'range'=>['$log10permin','10.0'],
                    'resolution'=>int($resolution{per}+$resolution{per}/$resolution{perzoomf}),
                    
                    # avoid instant RLOF
                    'preloopcode'=>'my $log10permin = POSIX::log10($self->minimum_period_for_RLOF($m1,$m2));',

                    # spacing function
                    'spacingfunc'=>$perspacing,
                    
                    precode=>"my \$per=10.0**\$log10per;my\$sep=binary_stars::calc_sep_from_period(\$m1,\$m2,\$per);",
                    probdist=>'Izzard2012_period_distribution($per,$m1,$log10permin)',
                    dphasevol=>'$dlog10per'
                    );
            }
            else
            {
                print STDERR "Unknown period distribution\n";
                exit;
            }
        }
    }

    # unique string for this model grid, based on dt, Z, maxtime
    my $default_gridstring //= 
	'dt='.$dt.
	# single-star options
	'_Z='.$population->{_bse_options}{metallicity}.
	'_maxtime='.$population->{_bse_options}{max_evolution_time};

    # and input distribution
    $default_gridstring .= '_' .
        ($distribution_options->{distribution} eq 'bastard' ?
         'auto' :
         $distribution_options->{distribution});

    if($distribution_options->{period_spacing} eq 'agbzoom')
    {
        $default_gridstring .= '_agbzoom';
    }

#	'_binary='.$population->{_grid_options}->{binary};
#	# + binary-star options if relevant
#	($population->{_grid_options}->{binary} ? 
#	 ('_alpha_ce='.$population->{_bse_options}->{alpha_ce}.
#	  '_lambda_ce='.$population->{_bse_options}->{lambda_ce}.
#	  '_lambda_ionisation='.$population->{_bse_options}->{lambda_ionisation}.
#	  '_epsnov='.$population->{_bse_options}->{epsnov}) : '');

    $gridstring //= $default_gridstring;

    print "Made gridstring : $gridstring\n";

    # output directory
    $outdir .= '/'.$gridstring;
    mkdirhier($outdir);
    print "Made outdir : $outdir\n";
    

    # skip existing grids 
    {
	my @testfiles = (output_filename('yields_vs_time'),
                         output_filename('yields_vs_time',0));
        foreach my $testfile (@testfiles)
        {  
            if(-e -f -s $testfile)
            {
                print "Grid already run (file found at $testfile)\n";
                exit if(!$force);
            }
        }
    }

    # make output filename IF not already set
    $yieldoutfile=output_filename('yields_vs_time') if(!$yieldoutfile);
 
    $ensemble_opts{tmp}="$outdir/burstensemble.binary".$population->{_grid_options}->{binary}."/" if($ensemble_opts{tmp} eq '');
    print "Set output dir $outdir\n";
    
    
############################################################
    # set up condor
    my $working_dir = '/home/rgi/data/condor_control/yvt'.$condor_count;
    mkdirhier($working_dir);
    $population->set(
        condor=>0, # to use condor, set condor=1 on cmd line
        condor_njobs=>50, # run this many jobs
        condor_dir=>$working_dir, # working directory for scripts etc.
        condor_streams=>0, # if 1 stream to stdout/stderr (warning: lots of network!)
        condor_memory=>1024, # RAM (MB) per job
        condor_universe=>'vanilla', # always vanilla
        condor_resubmit_finished=>0, # do not resubmit finished jobs
        condor_resubmit_crashed=>1, # do not resubmit crashed jobs
        condor_resubmit_submitted=>0, # do not resubmit submitted jobs
        condor_resubmit_running=>0, # do not resubmit running jobs
        condor_postpone_join=>1, # if 1 do not join on condor, join elsewhere
        condor_join_machine=>undef,
        condor_join_pwd=>undef,
	condor_requirements=>'((Machine!="calx173.ast.cam.ac.uk") && (Machine!="calx141.ast.cam.ac.uk"))',
        );
    $population->parse_args();
    
    
    if($population->{_grid_options}{condor})
    {
        print "Running on Condor : DIR $population->{_grid_options}{condor_dir} on $population->{_grid_options}{condor_njobs} jobs\n";
    }
    else
    {
        print "Running on local machine\n";
    }
}


sub cumulate_yields
{
    my $source=$_[0];
    if($sources)
    {
	printf "Cumulating source %d/%d\n",$source,$nsources-1;
    }
    else
    {
	print "Cumulating...\n";
    }
    
    my @sum;
    
    for(my $tbin=0;$tbin<$nbins;$tbin++)
    {
	my $t2=$tbin-1;
	for(my $j=0;$j<$nisotopes1;$j++)
	{
	    $population->results->{yield}[$source][$tbin][$j] +=
                $population->results->{yield}[$source][$t2][$j];
	}
    }
}

sub pad_datahash
{
    # pad datahash $h so it has zeros where there's no data
    my ($h,$d)=@_;
    my @k=nsort keys %$h;
    my $xmax=$k[$#k]; # max value
    my $x=$k[0]; # start (min) value
    while($x<$xmax)
    {
	$$h{$x}//=0.0;
	$x=sprintf'%g',$x+$d;
    }
}


sub process_yield_line
{
    my ($h,$source,$line) = @_;
    
    # shift off the time
    my $t = shift @$line;

    # time bin = $t*$idt-1
    my $timebin = $t*$idt - 1;

    # create array if required, and reference the timebin
    $$h{yield}[$source][$timebin] //= [];
    my $p = $$h{yield}[$source][$timebin];

    # loop over line items
    my $j = 0;
    while(scalar @$line)
    {
        my $x = shift @$line;
        if($x=~/^(\d+)x0$/o)
        {
            # skip $1 items
            $j += $1;
        }
        else
        {
            $p->[$j] += $x;
            if($p->[$j] > 100.0)
            {
                print STDERR "YIELD $j ($isotopes[$j]) of source $source is $x > thresh error\n";
                return 666;
            }
            $j++;
        }
    }
    return 0;
}

sub process_ensemble_line
{
    # process a line of ensemble data
    my ($h,$line)=@_;
 
    # get the time
    my $t = shift @$line;
    
    # time bin = $t*$idt-1 
    my $timebin = $t * $idt-1;

    # create array if required, and reference the timebin
    $$h{ensemble}[$timebin] //= [];
    my $p = $$h{ensemble}[$timebin];

    # save data
    my $j = 0;
    while(scalar @$line)
    {
        my $x = shift @$line;
        if($x=~/^(\d+)x0$/o)
        {
            # skip $1 items
            $j += $1;
        }
        else
        {
            $p->[$j] += $x;
            $j++;
        }
    }
}


sub total_mass_ejected
{
    # calculate total ejected mass (for each source as well, if appropriate)
    # and return the total
    my $m=0.0;
    my $h = $_[0] // $population->{_results};

    if($sources)
    {
	for(my $source=0;$source<=$nsources;$source++)
	{
	    for(my $tbin=0;$tbin<=$nbins;$tbin++)
	    {
		for(my $i=0;$i<$nisotopes1;$i++)
		{
		    $m += $h->{yield}[$source][$tbin][$i];
		}
	    }
	}
    }
    else
    {
	for(my $tbin=0;$tbin<=$nbins;$tbin++)
	{
	    for(my $i=0;$i<$nisotopes1;$i++)
	    {
		$m += $h->{yield}[0][$tbin][$i];
	    }
	}	
    }

    return $m;
}

sub get_colranges
{
    my $f=$_[0];
    my @colranges;
    my @datamax;
    my @datamin;

    my @x=`check_col_ranges.pl $f`;

    chomp @x;
    @colranges=split(/\s+/o,$x[0]);
    shift @colranges; # first is time
    
    @datamax=split(/\s+/o,$x[1]);
    shift @datamax; # MAX
    shift @datamax; # :
    shift @datamax; # the time
    #print "Datamax @datamax\n";

    @datamin=split(/\s+/o,$x[2]);
    shift @datamin; # MIN
    shift @datamin; # :
    shift @datamin; # the time
    #print "Datamin @datamin\n";

    return (\@colranges,\@datamax,\@datamin);
}


sub output_gp
{
    my @colranges;
    my @datamin;
    my @datamax;
    my @source_colranges;
    my $ymin=1e-10;

    if($sources==0)
    {
	my @x;
	@x=get_colranges($yieldoutfile);
	@colranges=@{$x[0]};
	@datamax=@{$x[1]};
	@datamin=@{$x[2]};
    }
    else
    {
	for(my $j=0;$j<=$nsources;$j++)
	{  
	    my $f=$yieldoutfile;
	    $f=~s/\.dat/\.source$j\.dat/;
	    $source_colranges[$j]=[]; # create anon array
	    my $x=$source_colranges[$j];
	    #@x=get_colranges($f);
	}
    }
    # set gnuplot output files
    my $gpfile="$outdir/cyields.plt"; # gnuplot plot file
    my $psfile="$outdir/cyields.ps"; # gnuplot postscript file
 
    my @gp;
    my $fontsize=18;
    open(GP,'>',$gpfile)||confess("cannot open gnuplot file");
    print GP "set terminal postscript solid enhanced colour \"Times-Roman\" $fontsize linewidth 1\n";
    
    print GP "set output \"$psfile\"\nset zero 1e-50\n";
    print GP "set xlabel \"Time\"\n";
    print GP "set key below\n";
    if($differential==1)
    {
	print GP "set logscale x\n";
	print GP "set logscale y\n";
	print GP "set yrange [$ymin:*]\n";
    }
    else
    {
	print GP "set yrange[$ymin:*]\n";
    }
    my $z=$population->{_bse_options}->{metallicity}; 
    my $bin=$population->{_grid_options}->{binary}==0?'Single':'Binary' ;
    my @linetypes=(1..21);
    
    for(my $i=0;$i<$nisotopes1;$i++)
    {
	my $comma='plot ';
	if($sources)
	{
	    for(my $j=0;$j<=$nsources;$j++)
	    {
		my $lt=$linetypes[$j]//1;
		my $f=$yieldoutfile;
		$f=~s/\.dat/\.source$j\.dat/;
		my $x=$source_colranges[$j];
		@colranges=@$x;

		if(($colranges[$i]>$ymin)&&
		   ($datamax[$i]>$ymin))
		{
		    # save output prior to gnuplotting
		    $gp[$i]=$gp[$i]." $comma \"$f\" using 1:".($i+2)." with lp lt $lt pt $lt title \"$source_label[$j]\" ";
		    $comma=', ';
		}
	    }
	}
	else
	{
	    print "Isotope $i = $isotopes[$i] colrange = $colranges[$i], data range $datamin[$i] to $datamax[$i]\n"; 
	    $gp[$i] = (($colranges[$i]==0.0)||($datamax[$i]<1e-20)) ?
		"plot (100),(100) title \"No Yield\"\n":
		# intelligent y range e.g. for log scale
		"set yrange[".($datamin[$i]==0.0 ? ($differential ? $datamax[$i]*1e-5 : 0.0) : '*').":*]\n".$gp[$i]."plot \"$yieldoutfile\" using 1:".($i+2)." with lp notitle ";
	    
	}
	
	print GP "set nologscale x\nset title \"";
	print GP "Cumulative "if(!$differential);
	my $isotope=$isotopes[$i];
	my $superfontsize=int($fontsize*0.8);
	$isotope=~s/(\D+)(\d+)/^\{\/=$superfontsize $2\}$1/;
	print GP "$isotope yield ";
	print GP 'rate 'if($differential);
	print GP "{/Italic Z}=$z; $bin stars\"\nset xrange[0:*]\n",$gp[$i]," \# $isotopes[$i] $colranges[$i]\nset xrange[",0.5*$dt,":*]\nset logscale x\n",$gp[$i],"\n";
    }
    
    close GP;

    # run gnuplot
    `gnuplot $gpfile`;
    
    # convert colours
    my $ps=slurp($psfile);
    $ps=~s/1 1 0/0.5 0.5 0/o;
    $ps=~s/0 0 1/0 0 0.7/o;
    dumpfile($psfile,$ps);

    print "See $psfile\n";

}


sub output_ensemble
{
    # process, and output information about, the stellar ensemble
    # data we have saved
    my $t;
    
    print "Process ensemble: $nensemble observations\n" if ($population->{_grid_options}->{vb});

    my $massfac=1.0/$population->results->{mass_into_stars};

    # output as one big file, or lots of little ones, or both
    if($ensemble_opts{smallfiles})
    {
	# many small files in one output directory
	mkdir $ensemble_opts{tmp};

	# loop over observation number
	for(my $o=0;$o<=$nensemble;$o++)
	{
	    open(ENSEMBLE,'>',"$ensemble_opts{tmp}/ensemble$o.dat")||
		confess("cannot open ensemble output file $ensemble_opts{tmp}/ensemble$o.dat");
	    # loop over time bin
	    print ENSEMBLE  "\# stellar ensemble, variable $o ($ensemble_label[$o]) normalized with IMF, divided by mass input to stars = $population->results->{mass_into_stars}\n";

	    for(my $tbin=0;$tbin<$nbins;$tbin++)
	    {
		print ENSEMBLE $dt*($meantimes ?($tbin+0.5) : ($tbin+1)),' ',$massfac*$population->results->{ensemble}[$tbin][$o],"\n";
	    }
	    close ENSEMBLE;
	}
    }

    if($ensemble_opts{largefile})
    {
	# output the large file with zillions of columns
	my $file=output_filename('ensemble');
	open(ENSEMBLE,'>',$file)||confess("cannot open ensemble output file $file");
	
	print ENSEMBLE "# Stellar ensemble normalized with IMF, divided by mass input to stars = $population->results->{mass_into_stars}, ensemble n=$nensemble\n";
	for(my $o=0;$o<=$nensemble;$o++)
	{
	    printf ENSEMBLE "# Column %d is %s\n",$o+2,$ensemble_label[$o];
	}

	for(my $tbin=0;$tbin<$nbins;$tbin++)
	{
	    my @output = ($dt*($meantimes ? ($tbin+0.5) : ($tbin+1)));
	    for(my $o=0;$o<$nensemble;$o++)
	    {
		push(@output, $massfac * $population->results->{ensemble}[$tbin][$o]);
	    }
	    print ENSEMBLE join(' ',@output,"\n");
	}
	close ENSEMBLE;
    }
}

sub output_filename
{
    # standard output filename
    my ($stub,$source) = @_;

    my $f = $outdir.'/'.$stub.
        '.z='.$population->{_bse_options}->{metallicity};

    if($distribution_options->{distribution} eq 'single' ||
       $distribution_options->{distribution} eq 'binary')
    {
        $f .= '.'.$distribution_options->{distribution};
    }
    else
    {
        $f .= '.mixed';
    }

    if(defined $source)
    {
        $f .= '.'.$source;
    }

    $f .= '.dat';

    return $f;
}


sub binary_fraction
{ 
    my $population = shift;
    return $population->{_grid_options}->{binary} ; # force single or binary stars only
    my $m1=$_[0];
    return distribution_functions::raghavan2010_binary_fraction($m1);
}


sub output
{
    my $t;
    my $bin;
    my $source = $_[0];

    # cumulate yields if required
    cumulate_yields($source) if(!$differential);
    
    # convert the yieldoutfile to account for the fact that 
    # we have multiple sources of stuff
    my $file = $yieldoutfile;
    $file=~s/\.dat/\.source$source\.dat/ if($sources);

    # new output file
    open(FP,'>',$file)||
	confess("cannot open output file $file (source=$source)");
    
    # make human-readable symlink, or warn if we cannot (not a fatal error)
    my $symlink = $file;  # full filename
    $symlink=~s/source(\d+)\.dat$/source$source_label[$source].dat/;
    
    # get dir and relative filename
    my $dirname = dirname($symlink);
    my $relfile = basename($file);
    my $rellink = basename($symlink); 

    # save current dir
    my $cwd = getcwd;

    if(chdir $dirname && !-l $symlink && !symlink $relfile,$rellink)
    {
	print "Warning: Unable to create symlink ($relfile > $rellink in $dirname) for this source (are symlinks available?)\n";	
    }

    # go back to cwd
    chdir $cwd;

    # output version information
    {
	my @v=split(/\n/o,$version_string);
	map
	{  
	    if(/^Isotope (\d+)/ && ($1>=$nisotopes))
	    {
		$_ .= " (not output because nisotopes=$nisotopes < number of isotopes in binary_c)";
	    }
	    print FP "# $_\n";
	}@v;
    }    

    # dump options for 
    map
    {
	print FP "# grid_option $_ = $population->{_grid_options}->{$_}\n";
    }sort keys %{$population->{_grid_options}};
    map
    {
	print FP "# bse_option $_ = $population->{_bse_options}->{$_}\n";
    }sort keys %{$population->{_bse_options}};
    
    print FP '# yields (mass ejected, normalized with IMF, divided by mass input to stars = ',$population->results->{mass_into_stars};    
    print FP " per unit time"if($differential);    
    print FP ") vs time\n",join(' ','# time',@isotopes,"\n");
    
    printf FP "# Total mass ejected: %g\n",$population->results->{total_mass_ejected};
    printf FP "# Total mass into stars: %g\n",$population->results->{mass_into_stars};
    printf FP "# Return fraction %3.2f %%\n",$population->results->{total_mass_ejected}/$population->results->{mass_into_stars}*100.0;

    my $n1 = $nisotopes+1;

    my $denominator=denominator();
    print "Source $source ($source_label[$source]) : Denominator = $denominator ($population->results->{mass_into_stars}) dt=$dt\n";
    print FP "# Source $source ($source_label[$source]) : Denominator = $denominator ($population->results->{mass_into_stars}) dt=$dt\n";

    for(my $tbin=0;$tbin<=$nbins;$tbin++)
    {
	#print "Output tbin $tbin\n";
        # calculated just once
	my $sum=0.0; # sum of yields
        my @dyield; # yield time derivative

        if($differential)
        { 
	    my $i2=1.0/($denominator*$dt); # inverse denominator

	    # yields are already stored as differential yields
	    # but we want per unit mass per unit time
	    for(my $i=0;$i<$n1;$i++)
	    {
		#printf "Output results{yield}{$source}{$tbin}{$i} = %g * $i2\n",$population->results->{yield}{$source}{$tbin}{$i};
		$dyield[$i]=$population->results->{yield}[$source][$tbin][$i]*$i2;
	    }
	}
        else
        {
            # set sum to something >0 so output is forced when
            # we want cumulative yields
            $sum=1.0;
	}
	
        # now only output yields if the sum of the yields is > 0
        # i.e. if anything happens, otherwise nothing happens and we do not care!
        # so don't output lots of lines of zeros!
        #if($sum>0.0) # (NB comment out this line for debugging) 
        {
	    my $idenom=1.0/$denominator;
	    my $tt = $meantimes ? ($tbin+0.5)*$dt # midpoint of the bin
		: ($tbin+1)*$dt; # end of the bin
	   
	    print FP "$tt ";

	    for(my $i=0;$i<$n1;$i++)
            {
                #print "T=$t $isotopes[$i] $yield[$source}{$t}{$i]\n";
                # normalize to mass put into stars
                if($differential)
                {
		    # already normalized (see above)
		    print FP $dyield[$i]==0 ? '0 ' : (sprintf '%5.5e ',$dyield[$i])
			    if($t<=$population->{_bse_options}->{max_evolution_time});
                }
                else
                {
                    print FP (sprintf  '%5.5e ',$population->results->{yield}[$source][$tbin][$i]*$idenom);
                }
	    }
            print FP "\n";
        }
	
    }
    print FP "\n";
    close FP;
}

sub denominator
{
    my $denominator;
    if($norm_output eq 'none')
    {
	$denominator=1.0;
	#print "Denominator = 1.0 (i.e. none)\n";
    }
    elsif($norm_output eq 'total_mass_ejected')
    {
	$denominator=1e-40+$population->results->{total_mass_ejected};
	#print "Denominator = mass ejected = $population->results->{total_mass_ejected}\n";
    }
    else
    {
	$denominator=1e-40+$population->results->{mass_into_stars};
	#print "Denominator = mass into stars = $results{mass_into_stars}\n";
    }
    return $denominator;
}


