#!/usr/bin/env perl 

$|=1;
use strict;
use rob_misc;
use Histogram;
use Sort::Key qw(nsort);
use Data::Dumper;

use binary_grid2; 
use binary_grid::C;
#use binary_grid::Perl;
#use binary_grid::condor;
#
# Grid to calculate statistics regarding the nature of
# binary barium star systems 
#

# output directory
my $outdir //= '/'. (-d '/home/izzard' ? 'home' : 'users') .'/izzard/data/ba2/';

# make population object
my $population = binary_grid2->new(defaults(),nthreads=>16);

my $r=1;

if(0)
{
    # todo : make bastard OO
    distribution_functions::bastard_distribution({
        mmin=>0.7,
        mmax=>6.0,
        m2min=>0.1,
        qmin=>0.0,
        qmax=>1.0,
        useecc=>1,
        nm1=>10,#40
        nm2=>4*$r,#40
        nper=>4*$r,#100
        necc=>4*$r,#40
                                                 });
}
else
{
    my $nvar=0;
    my $mmin=0.1;
    my $mmax=80.0;
    my $n=4000; # resolution
    $population->add_grid_variable(
        'name'=> 'lnm1', 
        'longname'=>'Primary mass', 
        'range'=>["log($mmin)","log($mmax)"],
        'resolution'=> $n,
        'spacingfunc'=>"const(log($mmin),log($mmax),$n)",
        'precode'=>'my $m1=exp($lnm1); my $eccentricity=0.0;',
        'probdist'=>"ktg93(\$m1)*\$m1",
        'dphasevol'=>'$dlnm1'
        );
}

# evolve the population
$population->evolve();

# output to files
output($population);

exit;

############################################################
############################################################
############################################################

sub parse_bse
{
    my $self = shift; # population object

    # this gets called by the binary_grid module, it parses the 
    # lines of output from binary_c/nucsyn
    my $brk = 0; 

    # you should set data in the $h hash pointer, which is 
    # then passed to the join_thread function (below) for you
    # to add up
    my $h = shift;
    
    if(!defined $h->{histograms})
    {
	# set up thread histograms
	make_new_histograms($h);
    }

    while($brk==0)
    {
	$_ = $self->tbse_line();
        next if(/^Tick/);
	#print "TBSE_LINE >> ",$_,"\n";
	chomp;

        $h->{nlines}++;

	if($_ eq 'fin')
	{
	    $brk=1; # the end of output
	}
	elsif(/GIANT/o)
	{
	    #print "LINE $_\n";
 	    s/^(\S+) //;
	    my $id=$1;
	    my @x = split(/\s+/,$_); # splits $_ into the array @_
	    my $dtp=$x[1]*$x[2];
	    my $p = $x[3];
	    my $ecc = $x[4];

            $h->{'nlines_'.$id}++;

	    if($p>0.0 && $ecc>=0.0)
	    {
                # binary stars
                my $logp = log10($p);

                if($id eq 'GK_GIANT')
		{ 
		    $h->{number_counts}{'GK giant'}+=$dtp;
                    $h->{number_counts}{'GK giant binary'}+=$dtp;
                    $h->{histograms}{'GK giant binary logP-e'}->add_data($logp,$ecc,$dtp);
                }
                elsif($id=~/^BARIUM(strong|mild)_GIANT/)
		{ 
                    my $type = $1;
		    $h->{histograms}{'Ba giant binary logP-e'}->add_data($logp,$ecc,$dtp);
		    $h->{histograms}{'Ba giant binary '.$type.' logP-e'}->add_data($logp,$ecc,$dtp);
		    $h->{histograms}{'Ba giant binary '.$type.' logP-e'}->add_data($logp,$ecc,$dtp);
		    $h->{number_counts}{'Ba giant binary '.$type}+=$dtp;
                    $h->{number_counts}{'Ba giant '.$type}+=$dtp;
		    $h->{number_counts}{'Ba giant'}+=$dtp;
		}
	    }
            else
            {
                # single stars
                if($id eq 'GK_GIANT')
                {
                    $h->{number_counts}{'GK giant'} += $dtp;
                    $h->{number_counts}{'GK giant single'} += $dtp;
                }
                elsif($id=~/^BARIUM(strong|mild)_GIANT/)
                {  
                    my $type = $1;
		    $h->{number_counts}{'Ba giant single '.$type}+=$dtp;
                    $h->{number_counts}{'Ba giant '.$type}+=$dtp;
		    $h->{number_counts}{'Ba giant'}+=$dtp;
                }
            }
	}
    }
}



sub defaults
{ 
    # for debugging
    my $debug = 0;

    my @defaults = (
        
        # set up a pointer to the parser function (see parse_bse())
        'parse_bse_function_pointer' => \&main::parse_bse,

        # condor
        condor_memory => 1000,
        condor_njobs => 30,
        condor_dir => $ENV{HOME}.'/data/condor/barium',
        reset_stars_defaults => 1,
        
        # code options
        'nice' => '',#'nice -n +10', 
        'timeout' => 3000, # seconds until timeout
        'thread_max_freeze_time_before_warning' => 60,
        'log_args' => 0,
        'save_args' => 0,
        'vb' => undef,
        'prog' => 'binary_c-Ba',
        'log_filename' => '/dev/null',

        # physics
        'z' => 0.008,
        'wd_sigma' => 0.0,
        'c13_eff' => 1.0,
        'acc2' => 1.5,
        'alpha_ce' => 0.25, # 0.25
        'lambda_ce' => -1, # -1 = automatically set (Tauris)
        'lambda_ionisation' => 0.0, # 0.0
        'initial_abundance_mix' => 2, # 2 = lodders (required!)
        'rotationally_enhanced_mass_loss' => 0,
        'comenv_ms_accretion_mass' => 0.0,
        'jorb_loss' => 0,
        'lw' => 1.0,
        'tidal_strength_factor' => 1.0,

        # extra 3DUP
        'delta_mcmin' => -0.4,
        'lambda_min' => 0.5,
        'minimum_envelope_mass_for_third_dredgeup' => 0.0,
        'max_evolution_time' => 13700.0,
        
        # canonical 
        #'delta_mcmin' => 0.0,
        #'lambda_min' => 0.0,
        #'minimum_envelope_mass_for_third_dredgeup' => 0.5,

        sort_args => $debug, 
        log_args => $debug, 
        save_args => $debug,
        );

   
    return @defaults;
}


sub output
{
    my $population = shift;

    # output
    return if($population->condor_command eq 'run_flexigrid');
    print "OUTPUT to $outdir\n";
    mkdirhier($outdir);

    open(my $out, '>', "$outdir/log");
    if($out)
    {
	print {$out} Dumper(\%{$population->{_grid_options}});
	print {$out} Dumper(\%{$population->{_bse_options}});
    }
    close $out;

    my %histograms = %{$population->results->{histograms}};
    
    foreach my $k (keys %histograms)
    {
	# make histogram
	my $f="$outdir/$k";
	$f=~s/\s+/_/g;

	my $histogram = $histograms{$k};

	print "Dump histogram $k (object $histograms{$k}, ref ",ref $histogram,") to file $f\n";


	$histogram->format('gnuplot_surface');
	$histogram->dump($f.'.surf');

	$histogram->format('gnuplot');
	$histogram->dump($f.'.dat');

    }

    my %number_counts = %{$population->results->{number_counts}};

    print "Output number counts\n";
    open(FP,">$outdir/number_counts")||die("cannot open $outdir/number_counts");
    foreach my $k (sort keys %number_counts)
    {
	# number count
	printf "SCALAR : %s = %g\n",$k,$number_counts{$k};
	printf FP "%s = %g\n",$k,$number_counts{$k};
    }
    close FP;

    print "----------------------------------------
testing
----------------------------------------
\%results:
";

    
    foreach my $k (grep {/nlines/} sort keys %{$population->results})
    {
        print $k,' : ',$population->results->{$k},"\n";
    }


    print "------------------------------------------------------------\ndone\n\n";
}
sub make_new_histograms
{
    my $h=$_[0]; # thread-private results_hash

    foreach my $hist ('Ba giant binary logP-e',
		      'Ba giant binary strong logP-e',
		      'Ba giant binary mild logP-e',
		      'GK giant binary logP-e'
	)
    {
	$h->{histograms}{$hist} = P_e_histogram(); 
    }
    $h->{number_counts}={};
}

sub P_e_histogram
{
    my $h = Histogram->new;
    $h->binwidths(0.2,0.05);
    return $h;
}


