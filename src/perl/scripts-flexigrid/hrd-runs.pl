#!/usr/bin/env perl
use strict;

# run HRD parameter space for Peter

foreach my $poptype ('single',
		     'binary',
		     'mixed')
{
    my $popstring = 
	$poptype eq 'mixed' ? 'distribution_types=mixed' :
	$poptype eq 'single' ? 'distribution_types=separate binary=0' : 
	'distribution_types=separate binary=1';

    foreach my $Z (0.0001,0.001,0.01,0.02)
    {
	my $args = " z=$Z $popstring testing=0 ";

	my $cmd = "src/perl/scripts-flexigrid/grid-hrd-condor.pl $args";
	print $cmd,"\n";
	#next;

	open(CMD,"-|",$cmd)
	    or die("cannot open grid-hrd-condor.pl script");
	while(<CMD>) { print $_ } 
	close CMD;
    }
}

