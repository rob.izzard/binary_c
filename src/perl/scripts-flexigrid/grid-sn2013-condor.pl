#!/usr/bin/env perl
$|=1; # enable this line for auto-flushed output
use strict;
use Sort::Key qw(nsort);
use rob_misc;
use binary_grid;
use binary_grid::condor;
use threads;
use threads::shared;
use Fcntl ':flock'; # import LOCK_* constants
use subs qw(logprint logprintf);
use Carp qw(confess);
use File::Path qw(make_path);

#
# Flexigrid-condor script for the 2013 SN project(s)
#

my $nm; my $nm1; my $nm2; my $nper; my $necc;

# grid resolution : full recommended resolution after #s
my $nm=100; # 1000
my $nm1=100; # 100
my $nm2=100; # 40
my $nper=100; # 40
my $necc=40;
my $useecc=0;

# set $one_population to either single or binary for 
# a population of only single or only binary stars:
# if undef then a bastard population is used.
my $one_population = undef;
#$one_population = 'single';
#$one_population = 'binary';

# bin data in linear or log time?
# (either way, we need to normalize to the bin width)
# 0/undef = no
# 'linear' = linear in time,
# 'log' = in log time bins
my $time_bin_method = 'log';
my $time_bin_linear_width=1.0; # in Myr
my $time_bin_log_width=0.1; # (in decades)

# Sampling factor: prevent spikes if we can.
# Should use 1.0/INTEGER for smooth filling of bins.
# the results with 1/10 are very similar to 1/2 : so
# use 1/2 (both tested with time_bin_log_width=0.1)
my $sampling_factor=1.0/2.0;
#$sampling_factor=0.1;

# bin widths for final SN rate plots
my $dt=$time_bin_linear_width*$sampling_factor; # grid time resolution
my $dlogt=$time_bin_log_width*$sampling_factor; # or in log time (should be < time_bin_log_width)

# grid spacing scheme:
# if $one_population is defined, use these options:
# 0/undef = no
# 'linear' = linear in time,
# 'log' = in log time bins
my $time_adaptive_m1='log'; 

my %results;
my @sntype=('IaHe',
	    'IaELD',
	    'IaChand',
	    'AIC',
	    'ECAP',
	    'IaHeCoal',
	    'IaChandCoal',
	    'GRBNSNS',
	    'GRBCollapsar',
	    'SNHeStarIa',
	    'Ibc',
	    'II',
	    'IIa');

# lores
#$nm=10; $nm1=10; $nm2=4; $nper=4;

# midres
#$nm=100; $nm1=100; $nm2=20; $nper=20;

# M1 and M2 grid limits
my $m1min=3.0; # > this for any helium stars to form
my $m2min=0.1; # < this are brown dwarfs/planets
my $mmax=80.0; # for M1 and M2

# period distribution D+M91 options
my $logpermin=-2.0;
my $logpermax=12.0;
my $gauss_logmean=4.8;
my $gauss_sigma=2.3;

# q distribution
my $qbeta=0.0; # use qbeta by default

# period distribution
my $period_distribution='bastard'; # 'DM91' or 'bastard'

my $outlock : shared;
my @spectral_type_strings=('O','B','A','F','G','K','M');
my %spectral_type;  revspechash();
my @stellar_type_strings;
my @short_stellar_type_strings;
my %short_stellar_type_strings;
my %binwidth=(
    'm1'=>0.1,
    'log10m1'=>0.1,
    'q'=>0.05,
    'period'=>0.1, # log period
    'eccentricity'=>0.05);
my $outdir;
my $log_fp;

# fix aifa environment/paths
fix_aifa_environment();
my $results={}; # reset results
defaults();
condor_grid();
#flexigrid(4);
output();
exit(0);

############################################################
############################################################
############################################################

sub defaults
{
    binary_grid::parse_args();
    grid_defaults();
    renice_me(19);
    binary_grid::parse_args();

    my $args="@ARGV";
    $outdir=($args=~/outdir=(\S+)/)[0];
    
    
    # condor directory : this stores the temp files and the final 
    # output : you should make sure it is read/writeable on all
    # condor notes (e.g. via an NFS link)
    $binary_grid::grid_options{condor_dir}=$ENV{HOME}.'/data/condor/sn2013';

    # condor requirements
    $binary_grid::grid_options{condor_options}{Requirements}=
	'Machine!="aibn73.astro.uni-bonn.de" && Machine!="aibn79.astro.uni-bonn.de" && Machine!="aibn103.astro.uni-bonn.de"';
# && Machine!="aibn62.astro.uni-bonn.de"';
#	'Machine=="aibn36.astro.uni-bonn.de"';	

    $binary_grid::grid_options{condor_memory}=0;

    if(!defined($ENV{HOSTNAME}))
    {
	$ENV{HOSTNAME} = `hostname`;
	chomp $ENV{HOSTNAME};
    }

    $binary_grid::grid_options{condor_njobs}=MAX(4,MIN(20,condor_free()));
    print "Use $binary_grid::grid_options{condor_njobs} condor slots on $ENV{HOST}\n"; 
 
    $binary_grid::grid_options{condor_njobs}=4
	if($ENV{HOSTNAME} eq 'buntspecht');

    # always link binary_grid to our local results hash
    $binary_grid::grid_options{results_hash}=$results;

    # use which parser function?    
    $binary_grid::grid_options{parse_bse_function}='parse_bse';
    
    $binary_grid::grid_options{threads_stack_size}=50; # MBytes
    
    $binary_grid::grid_options{prog}='binary_c-sn2013';
    $binary_grid::grid_options{'gdb wait'}=undef;

    $binary_grid::bse_options{z}=0.02;
    $binary_grid::bse_options{wd_sigma}=0.0;
    #$binary_grid::bse_options{maximum_timestep}=0.1;

    # tides: 1.0=normal 0=no tides
    $binary_grid::bse_options{tidal_strength_factor}=1.0;

    $binary_grid::bse_options{alpha_ce}=1.0; # 1
    $binary_grid::bse_options{lambda_ce}=-1; # -1 = automatically set
    $binary_grid::bse_options{lambda_ionisation}=0.0; # 0.0
    #$binary_grid::bse_options{lw}=1.0;

    # use nelemans/tout gamma 
    if(0)
    {
	$binary_grid::bse_options{comenv_prescription}=1; # 1 = nelemans
	$binary_grid::bse_options{nelemans_minq}=0.0; #0.0
	$binary_grid::bse_options{nelemans_max_frac_j_change}=1.0; # 1.0
	$binary_grid::bse_options{nelemans_gamma}=1.0; # 1.0
	$binary_grid::bse_options{nelemans_n_comenvs}=1; # all CEE if >=100
    }

    # wind: 0=default, 1=mm, 2=nl, 3=jje/vink
    $binary_grid::bse_options{wr_wind}=0;

    # Black hole prescription: 0=Hurley, 1=Belczynski
    $binary_grid::bse_options{BH_prescription}=0;

    # star formation stuff
    $binary_grid::bse_options{max_evolution_time}=10e3; # Myr
    
    # time resolution
    $binary_grid::bse_options{'maximum_timestep'}=10;

    # code options
    $binary_grid::grid_options{'nice'}='nice -n +10'; # nice command e.g. 'nice -n +10' or '' 
        
    $binary_grid::grid_options{'timeout'}=0; # seconds until timeout (0=disable)
    
    $binary_grid::grid_options{'log_args'}=1;
    $binary_grid::grid_options{vb}=1; # make verbose
    $binary_grid::grid_options{tvb}=1; # thread logfile

    $binary_grid::grid_options{nmod}=1000; # log every 1000 models
    $binary_grid::grid_options{log_dt_secs}=3; # 300 = log every 5 minutes

    # activate flexigrid
    # build flexigrid
    
    # maximum stack size before we pause to allow stars to finish
    $binary_grid::grid_options{'flexigrid'}{'maxstack'}=10;

    # first attempt at an MC grid
    #$binary_grid::grid_options{'flexigrid'}{'grid type'}='monte carlo';

    if(!(defined $one_population))
    {
	distribution_functions::bastard_distribution({
	    binary_fraction_shift=>0.0,
	    mmin=>$m1min,
	    mmax=>$mmax,

	    # use time-adaptive mass resolution for M1
	    time_adaptive=>{
		mass_grid_log10_time=>1,
		mass_grid_step=>$dlogt,
		mass_grid_log10_step=>$dlogt,
		extra_flash_resolution=>0,
		mass_grid_nlow_mass_stars=>10,
		debugging_output_directory=>undef,
		max_delta_m=>2.0},	   

	    # no longer required
	    #nm=>$nm,
	    #nm1=>$nm1,

	    m2min=>$m2min,
	    nm2=>$nm2,
	    nper=>$nper,
	    necc=>$necc,
	    qmin=>0.0,
	    qmax=>1.0,
	    useecc=>undef	    
						     });
    }
    elsif($one_population eq 'single')
    {
	my $nvar=0;
	$binary_grid::grid_options{binary}=0;

	# Mass 1
	if($time_adaptive_m1)
	{
	    my $use_logt = ($time_adaptive_m1 eq 'log') ? 1 : 0;
	    
	    my $preloopcode = "
my \$const_dt_opts=\{
    max_evolution_time=>$binary_grid::bse_options{max_evolution_time},
    stellar_lifetime_table_nm=>400,
    nthreads=>1,
    thread_sleep=>1,
    mmin=>$m1min,
    mmax=>$mmax,
    time_adaptive_mass_grid_log10_time=>$use_logt,
    time_adaptive_mass_grid_step=>$dt,
    time_adaptive_mass_grid_log10_step=>$dlogt,
    extra_flash_resolution=>0,
    time_adaptive_mass_grid_nlow_mass_stars=>10,
    debugging_output_directory=>'/users/izzard/data/sn2013/adaptive_debug',
    max_delta_m=>2.0,
    savegrid=>0,
    vb=>1,
\};
spacing_functions::const_dt(\$const_dt_opts,'reset');
";
	    print $preloopcode,"\n";

	    $binary_grid::grid_options{'flexigrid'}{'grid variable '.$nvar}=
	    {
		# const_dt spacing function options
		'preloopcode'=>$preloopcode,
		# use const_dt function
		spacingfunc=>"const_dt(\$const_dt_opts,'next')",
		# and its resolution
		resolution=> "spacing_functions::const_dt(\$const_dt_opts,'resolution');",
		precode=>'$m1=exp($lnm1);  $eccentricity=0.0; $binary_grid::grid_options{weight}= 1.0;   ',
		name=>'lnm1',
		longname=>'Primary mass', 
		range=>["log($m1min)","log($mmax)"],
		#probdist=>"ktg93(\$m1)*\$m1", # KTG93 (all masses)
		probdist=>"Kroupa2001(\$m1)*\$m1",
		#probdist=>'powerlaw(0.8,100.0,-2.35,$m1)*$m1', # salpeter (0.8-100)
		dphasevol=>'$dlnm1',
	    };
	}
	else
	{
	    my $precode='$m1=exp($lnm1);  $eccentricity=0.0; $binary_grid::grid_options{weight}= 1.0;   ';

	    print "M1 precode = $precode\n";

	    $binary_grid::grid_options{'flexigrid'}{'grid variable '.$nvar++}=
	    {
		'name'=> 'lnm1', 
		'longname'=>'Primary mass', 
		'range'=>["log($m1min)","log($mmax)"],
		'resolution'=> "$nm1", # ignore single stars in approximate resolution calculation
		'spacingfunc'=>"const(log($m1min),log($mmax),\$binary_grid::grid_options{binary} ? $nm1 : $nm)",
		'precode'=>$precode,
		#'probdist'=>"ktg93(\$m1)*\$m1", # KTG93 (all masses)
		'probdist'=>"Kroupa2001(\$m1)*\$m1",
		#'probdist'=>'powerlaw(0.8,100.0,-2.35,$m1)*$m1', # salpeter (0.8-100)
		'dphasevol'=>'$dlnm1',
	    };
	}
    }
    elsif($one_population eq 'binary')
    {
	my $nvar=0;
	$binary_grid::grid_options{binary}=1;

	# Mass 1
	{
	    my $precode='$m1=exp($lnm1);  $eccentricity=0.0; $binary_grid::grid_options{weight}=1.0;   ';

	    print "M1 precode = $precode\n";

	    $binary_grid::grid_options{'flexigrid'}{'grid variable '.$nvar++}=
	    {
		'name'=> 'lnm1', 
		'longname'=>'Primary mass', 
		'range'=>["log($m1min)","log($mmax)"],
		'resolution'=> "$nm1", # ignore single stars in approximate resolution calculation
		'spacingfunc'=>"const(log($m1min),log($mmax),\$binary_grid::grid_options{binary} ? $nm1 : $nm)",
		'precode'=>$precode,
		#'probdist'=>"ktg93(\$m1)*\$m1", # KTG93 (all masses)
		'probdist'=>"Kroupa2001(\$m1)*\$m1",
		#'probdist'=>'powerlaw(0.8,100.0,-2.35,$m1)*$m1', # salpeter (0.8-100)
		'dphasevol'=>'$dlnm1',
	    };
	}


	# Binary stars: Mass 2 and Separation
	if(defined($qbeta))
	{
	    # use a q powerlaw with slope $qbeta
	    $binary_grid::grid_options{'flexigrid'}{'grid variable '.$nvar++}=
	    {
		'condition'=>'$binary_grid::grid_options{binary}==1',
		'name'=>'q',
		'longname'=>'Mass ratio',
		'range'=>[$m2min.'/$m1',1.0],
		'resolution'=>$nm2,
		'spacingfunc'=>'const('.$m2min.'/$m1,1.0,'.$nm2.')',
		'probdist'=>"powerlaw(0.0,1.0,$qbeta,\$q)",
		'precode'=>'$m2=$q*$m1;',
		'dphasevol'=>'$dq',
	    };
	}
	else
	{
	    print STDERR "Mass ratio distribution unknown\n";
	    print "Mass ratio distribution unknown\n";
	    exit;
	}

	
	if($period_distribution eq 'DM91')
	{
	    # old code
	    $binary_grid::grid_options{'flexigrid'}{'grid variable '.$nvar++}=
	    {  
		'name'=>'logper',
		'longname'=>'log(Orbital_Period)',
		'range'=>[$logpermin,$logpermax],
		'resolution',$nper,
		'spacingfunc',"const($logpermin,$logpermax,$nper)",
		'precode'=>"\$per=10.0**\$logper;\$sep=calc_sep_from_period(\$m1,\$m2,\$per)if(defined(\$m1)&&defined(\$m2));",
		'probdist'=>"gaussian(\$logper,$gauss_logmean,$gauss_sigma,$logpermin,$logpermax)",
		'dphasevol'=>'$dln10per'
	    }
	}
	elsif($period_distribution eq 'bastard')
	{
	    # period distribution : rob's bastard
	    $binary_grid::grid_options{'flexigrid'}{'grid variable '.$nvar++}=
	    {
		'name'=>'log10per',
		'longname'=>'log10(Orbital_Period)',
		'range'=>['-1.0','10.0'],
		'resolution',$nper,
		'spacingfunc',"const(-1.0,10.0,$nper)",
		'precode'=>"my \$per=10.0**\$log10per;my\$sep=calc_sep_from_period(\$m1,\$m2,\$per);",
		'probdist'=>'Izzard2012_period_distribution($per,$m1)',
		'dphasevol'=>'$dlog10per'
	    };
	}
	else
	{
	    print "Unknown period distribution $period_distribution\n";
	    exit;
	}


	if($useecc)
	{
	    # eccentricity
	    $binary_grid::grid_options{'flexigrid'}{'grid variable '.$nvar++}=
	    {
		'name'=>'ecc',
		'longname'=>'log10(Eccentricity)',
		'range'=>[0,0.999],
		'resolution',$necc,
		'spacingfunc',"const(0.0,0.999,$necc)",
		'precode','$eccentricity=$ecc;',
		# gaussian as in Meiborn & Mathieu 2005
		'probdist'=>'gaussian($ecc,0.38,0.23,0.0,0.999)',
		'dphasevol'=>'$decc'
	    };
	}



    }
    else
    {
	my $nvar=0; 
	
	# duplicity
	$binary_grid::grid_options{'flexigrid'}{'grid variable '.$nvar++}=
	{
	    'name'=>'duplicity',
	    'longname'=>'Duplicity',
	    'range'=>[0,1],
	    'resolution'=>1,
	    'spacingfunc'=>'number(1.0)',
	    'precode'=>'$binary_grid::grid_options{binary}=$duplicity;binary_grid::flexigrid_grid_resolution_shift();',
	    'gridtype'=>'edge',
	    'noprobdist'=>1,
	};
	
	
	# Mass 1
	{
	    my $precode='$m1=exp($lnm1);  $eccentricity=0.0; my $binary_fraction=MAX(0.0,MIN(1.0,distribution_functions::raghavan2010_binary_fraction($m1)));  $binary_grid::grid_options{weight}= $binary_grid::grid_options{binary}==0 ? 1.0-$binary_fraction : $binary_fraction;   ';

	    print "M1 precode = $precode\n";

	    $binary_grid::grid_options{'flexigrid'}{'grid variable '.$nvar++}=
	    {
		'name'=> 'lnm1', 
		'longname'=>'Primary mass', 
		'range'=>["log($m1min)","log($mmax)"],
		'resolution'=> "$nm1", # ignore single stars in approximate resolution calculation
		'spacingfunc'=>"const(log($m1min),log($mmax),\$binary_grid::grid_options{binary} ? $nm1 : $nm)",
		'precode'=>$precode,
		#'probdist'=>"ktg93(\$m1)*\$m1", # KTG93 (all masses)
		'probdist'=>"Kroupa2001(\$m1)*\$m1",
		#'probdist'=>'powerlaw(0.8,100.0,-2.35,$m1)*$m1', # salpeter (0.8-100)
		'dphasevol'=>'$dlnm1',
	    };
	}


	# Binary stars: Mass 2 and Separation
	if(defined($qbeta))
	{
	    # use a q powerlaw with slope $qbeta
	    $binary_grid::grid_options{'flexigrid'}{'grid variable '.$nvar++}=
	    {
		'condition'=>'$binary_grid::grid_options{binary}==1',
		'name'=>'q',
		'longname'=>'Mass ratio',
		'range'=>[$m2min.'/$m1',1.0],
		'resolution'=>$nm2,
		'spacingfunc'=>'const('.$m2min.'/$m1,1.0,'.$nm2.')',
		'probdist'=>"powerlaw(0.0,1.0,$qbeta,\$q)",
		'precode'=>'$m2=$q*$m1;',
		'dphasevol'=>'$dq',
	    };
	}
	else
	{
	    print STDERR "Mass ratio distribution unknown\n";
	    print "Mass ratio distribution unknown\n";
	    exit;
	}

	
	if($period_distribution eq 'DM91')
	{
	    # old code
	    $binary_grid::grid_options{'flexigrid'}{'grid variable '.$nvar++}=
	    {  
		'name'=>'logper',
		'longname'=>'log(Orbital_Period)',
		'range'=>[$logpermin,$logpermax],
		'resolution',$nper,
		'spacingfunc',"const($logpermin,$logpermax,$nper)",
		'precode'=>"\$per=10.0**\$logper;\$sep=calc_sep_from_period(\$m1,\$m2,\$per)if(defined(\$m1)&&defined(\$m2));",
		'probdist'=>"gaussian(\$logper,$gauss_logmean,$gauss_sigma,$logpermin,$logpermax)",
		'dphasevol'=>'$dln10per'
	    }
	}
	elsif($period_distribution eq 'bastard')
	{
	    # period distribution : rob's bastard
	    $binary_grid::grid_options{'flexigrid'}{'grid variable '.$nvar++}=
	    {
		'name'=>'log10per',
		'longname'=>'log10(Orbital_Period)',
		'range'=>['-1.0','10.0'],
		'resolution',$nper,
		'spacingfunc',"const(-1.0,10.0,$nper)",
		'precode'=>"my \$per=10.0**\$log10per;my\$sep=calc_sep_from_period(\$m1,\$m2,\$per);",
		'probdist'=>'Izzard2012_period_distribution($per,$m1)',
		'dphasevol'=>'$dlog10per'
	    };
	}
	else
	{
	    print "Unknown period distribution $period_distribution\n";
	    exit;
	}


	if($useecc)
	{
	    # eccentricity
	    $binary_grid::grid_options{'flexigrid'}{'grid variable '.$nvar++}=
	    {
		'name'=>'ecc',
		'longname'=>'log10(Eccentricity)',
		'range'=>[0,0.999],
		'resolution',$necc,
		'spacingfunc',"const(0.0,0.999,$necc)",
		'precode','$eccentricity=$ecc;',
		# gaussian as in Meiborn & Mathieu 2005
		'probdist'=>'gaussian($ecc,0.38,0.23,0.0,0.999)',
		'dphasevol'=>'$decc'
	    };
	}
    }
    printf "Flexigrid modulo = %d, offset = %d\n",
    $binary_grid::grid_options{modulo},
    $binary_grid::grid_options{offset};

    binary_grid::parse_args();
    secondary_variables();
    setup_binary_grid();

    @stellar_type_strings=@{(stellar_type_strings())[2]};
    @short_stellar_type_strings=@{(stellar_type_strings())[3]};
    %short_stellar_type_strings=%{(stellar_type_strings())[4]};
    #print "Short @short_stellar_type_strings\n";
}

############################################################

sub output
{
    # if we're running condor, do not output unless for the final call
    return if($binary_grid::grid_options{condor_command} eq 'run_flexigrid');

    # open log file
    $outdir = $ENV{VOLDISK}.'/data/sn2013/outdir' 
	if(!defined($outdir));
    make_path($outdir); # nb we assume this works...

    # output binary_grid::grid_options/bse_options
    open($log_fp,">$outdir/grid_options")||
	confess("cannot open $outdir/grid_options for output");
    print $log_fp "grid_options\n\n";
    map
    {
	print {$log_fp} $_.' : '.$binary_grid::grid_options{$_}."\n";
    }keys %binary_grid::grid_options;
    close $log_fp;
    open($log_fp,">$outdir/bse_options")||
	confess("cannot open $outdir/bse_options for output");
    print $log_fp "bse_options\n\n";
    map
    {
	print {$log_fp} $_.' : '.$binary_grid::bse_options{$_}."\n";
    }keys %binary_grid::bse_options;
    close $log_fp;
    
    open($log_fp,">$outdir/binary_c-version")||
	confess("cannot open $outdir/binary_c-version");
    print {$log_fp} `$binary_grid::grid_options{rootpath}/$binary_grid::grid_options{prog} --version`;
    close $log_fp;

    # output grid results
    open ($log_fp,">$outdir/log") || 
	confess ("cannot open $outdir/log for output");
    logprint "\n\n\SN2013 Grid results\n============================================================\n\n";
    logprint "SVN Revision ",`svn info|grep Revision|gawk \"\{print \\\$2\}\"`;
    logprint "Run at ",scalar(localtime),"\n";

    logprint "Resolution: nm=$nm nm1=$nm1 nm2=$nm2 nper=$nper necc=$necc (useecc=$useecc)\n";
    logprint "$m1min < M1 < $mmax\n";
    logprint "$m2min < M2 < $mmax\n";
    logprint "Period distribution : $period_distribution : $logpermin < logP < $logpermax : guassian mu=$gauss_logmean sigma=$gauss_sigma\n";

    logprint "One population ? $one_population\n";
    logprint "sampling factor : $sampling_factor\n";
    logprint "time_bin_method : $time_bin_method\n";
    logprint "time_bin_linear_width : $time_bin_linear_width\n";
    logprint "time_bin_log_width : $time_bin_log_width\n";
    logprint "dt : $dt\n";
    logprint "dlogt : $dlogt\n";
    logprint "time_adaptive_m1 : $time_adaptive_m1\n";

    logprint "Initial binary fraction : Raghavan 2010\n";
    foreach my $p ('alpha_ce','lambda_ce','lambda_ionisation','z','wd_sigma','lw')
    {
	my $opt = ($p eq 'lambda_ce' && $binary_grid::bse_options{$p}==-1) 
	    ? 'Fitted to detailed stellar models' 
	    : 
	    sprintf "%g",$binary_grid::bse_options{$p};

	logprint sprintf "% -30s = %s\n",$p eq'z'?'Metallicity':$p,$opt;
    }

    print "Make outdir=$outdir\n";
    mkdir $outdir;

    foreach my $k (sort keys %$results)
    {
	next if($k eq 'thread_number');

	print "Output to disk $k\n";


	# sort through hash keys here
	if($k eq 'histograms')
	{
	    # output histogram data
	    mkdir "$outdir/histograms";
	    
	    foreach my $histogram (keys %{$results->{histograms}})
	    {
		my $hist = $results->{$k}->{$histogram};
		my $of="$outdir/histograms/$histogram";

		open(my $out, '>'.$of)||confess("cannot open $of for output");
		print "Output histogram '$histogram' to $of\n";
		
		
		# convert to true histogram by dividing by the bin
		# width, but convert also from log to linear space if required
		foreach my $t (nsort keys %$hist)
		{
		    my $mult = $time_bin_method eq 'log' ? 
			# if we store dN in log10 bins, convert to dN/dt=dN/dlog10t * log10 * t
			1.0/($t*log(10.0)*$time_bin_log_width) :
			# otherwise, just divide by dt
			1.0/$time_bin_linear_width;
		    
		    printf "Hist : At t=%g convert from %g to %g\n",
		    $t,$$hist{$t},$$hist{$t}*$mult;

		    $$hist{$t} *= $mult;		    
		}

		# add up total
		my $total = 0.0;
		foreach my $t (nsort keys %$hist)
		{
		    $total += $$hist{$t};
		}
		$total = MAX(1e-30,$total);
		my $cumulate = 0.0;
		
		# output
		foreach my $t (nsort keys %$hist)
		{
		    $cumulate += $$hist{$t};
		    printf {$out} "%g %g %g %g\n",
		    $t,$$hist{$t},$cumulate,$cumulate/$total;
		}

		close $out;
	    }
	}
    }

    logprint "\n============================================================\n\n\nGrid finished at ".scalar(localtime())."\n";
    close $log_fp;
}

sub parse_bse
{
    # this gets called by the binary_grid module, it parses the 
    # lines of output from binary_c/nucsyn
    my $brk=0; 

    # you should set data in the $h hash pointer, which is 
    # then passed to the join_thread function (below) for you
    # to add up. (Note: $h is local to each thread.)
    my $h=shift;

    my $nthread=$binary_grid::grid_options{'thread_num'};
    my @init_spectypes;
    
    while($brk==0)
    {
	$_=tbse_line();
	next if(/^\#/o);

	#print "PARSE $_\n";
	chomp;
	if($_ eq 'fin')
	{
	    $brk=1; # the end of output
 	}
	elsif(s/^SNCOMP //o)
	{
	    # SN 2013 data line : do something with the data

	    s/[\{\}]//g; # convert human-readable to computer-readable 

	    # split to data array
	    my @x=split(/\s+/,$_);
	    
	    # what's in the data array?

	    # 0 probability
	    # 1 SN type
	    # 2 time of explosion (Myr)

	    # log late explosions
	    #print "LATE EXPLOSION t=$x[2] : $binary_grid::grid_options{'args'}\n"if($x[2]>5000 && $sntype[$x[1]] eq 'Ibc');
	    
	    # the explosion time must be binned, either log or linear
	    # nb this is NOT the rate, as we haven't yet divided by the bin width
 	    my $t_binned = bin_time($x[2]);

	    #print "bin time $x[2] to $t_binned (method=$time_bin_method, widths $time_bin_linear_width,$time_bin_log_width\n"; 

	    # 3 binarity at time of explosion

	    # pre explosion orbital information
	    # 4,5,6 = P,a,e
	    
	    # post explosion orbital information
	    # 7,8,9 = P,a,e

	    # progentior ZAMS informatiojn
	    # 10,11,12,13 : M1,M2,a,P
	    
	    # exploder information
	    # 14 stellar type
	    # 15 mass
	    # 16 luminosity
	    # 17 Teff
	    # 18 Mass loss rate
	    
	    # companion information at time of explosion
	    # 19 stellar type
	    # 20 mass
	    # 21 luminosity
	    # 22 Teff
	    # 23 Mass loss rate
	    
	    # save explosion times
	    $$h{histograms}{explosion_times}{$t_binned} += $x[0];

	    # save explosion times by SN type
	    $$h{histograms}{"explosion_times_$sntype[$x[1]]"}{$t_binned} += $x[0];
	}
    }
    return $h;
}




sub secondary_variables
{
    # from the defaults and/or passed in variables,
    # calculate useful things

    # use colour?
    if($binary_grid::grid_options{'colour'}==1)
    {
        # use ANSI color for output to xterms/rxvt etc.
        use Term::ANSIColor;
        # set to white on black
        print color("white on_black");
    }
    else
    {
        # dummy function to replace the ANSI color() function
        eval("sub\ color\{ \}");
    }
}

sub logprint
{
    print {$log_fp} "@_";
    print "@_";
}
sub logprintf
{
    my $format=shift @_;
    printf {$log_fp} $format,@_;
    printf $format,@_;
}

sub revspechash
{
    for(my $i=0;$i<=$#spectral_type_strings;$i++)
    {
	$spectral_type{$spectral_type_strings[$i]}=$i;
    }
}

sub spec_to_float
{
    my $s=$_[0]; # spectral type to check
    if($s=~/([A-Z])(\d)/)
    {
	return $spectral_type{$1}+0.1*$2;
    }
    else
    {
	print "Failed to convert spec type $_[1] to float\n";
	exit;
    }
}

sub spec_type_sorter
{
    return spec_to_float($a) <=> spec_to_float($b);
}

sub pad_datahash
{
    # pad datahash $h so it has zeros where there's no data
    my $h=shift;
    my $d=shift;
    my @k=nsort keys %$h;
    my $xmax=$k[$#k]; # max value
    my $x=$k[0]; # start (min) value
    while($x<$xmax)
    {
	$$h{$x}=0.0 if(!defined($$h{$x}));
	$x=sprintf '%g',$x+$d;
    }
}

sub SFR
{
    # SFR as a function of Galactic age in Myr
    my $t=$_[0];

    return 1.0; # const SFR
    #return $t<1.0e3 ? 1.0 : 0.0; # const for a given time period
    #return exp(-$t/$SF_timescale}); # exponential
}

sub fix_aifa_environment
{
    if(`hostname`=~/aibn/)
    {
	map
	{
	    print "CHECK $_\n";
	    if($ENV{$_}=~s!/export/aibn(\d+)_(\d+)/!/vol/aibn$1/aibn$1\_$2/!g)
	    {
		print "CONV $_=$ENV{$_}\n";
	    }
	}%ENV;
    }
}

sub condor_free
{
    # return number of free condor slots
    my $cmd='condor_status|grep -A2 Claimed\ Unclaimed|tail -1|gawk "{print \$5}" | grep -v aibn73 | grep -v aibn79';
    my $n=`$cmd`;
    chomp $n;
    return $n;
}

sub bin_time
{
    # bin time according to linear or log scheme
    return 
	$time_bin_method eq 'log' ?
	10.0**(bin_data(log10(MAX(1e-30,$_[0])),$time_bin_log_width)) :
	bin_data($_[0],$time_bin_linear_width);
}
