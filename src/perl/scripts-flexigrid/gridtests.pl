#!/usr/bin/env perl
use strict; $|=1;
use binary_grid;
use Time::HiRes qw(sleep gettimeofday tv_interval); # for timers
use rob_misc;
use 5.16.0;

# scripts to test the speed etc. of binary grid

# global variables
my $ncpus=ncpus();
my $results={};
my @threads=nthreads_list($ncpus);

# output data
open(DATA,'>','/tmp/gridtest.dat')||die;

my $runtime=1.0; # seconds
my $mode='spin'; # 'sleep' or 'spin'
# sleep means just call the sleep function for a given time
# spin means hammer the CPU at 100% for a given time

foreach my $nlines (0,10,100,1000,10000)# lines of output to be processed
{
    foreach my $nstars (10,100,1000)# number of stars
    {	
	my $thread1_dt;

	foreach my $nthreads (@threads)
	{
	    # run with no stars to test the thread setup time
	    my $dt_setup=run_test_grid(
		{
		    nstars=>1,
		    nthreads=>$nthreads,
		    binary_c_lines=>0,
		    binary_c_runtime=>0,
		    binary_c_mode=>$mode,
		}
		);

	    my $dt=run_test_grid(
		{
		    nstars=>$nstars,
		    nthreads=>$nthreads,
		    binary_c_lines=>$nlines,
		    binary_c_runtime=>$runtime,
		    binary_c_mode=>$mode,
		}
		);
	    # remove thread setup time : this is a constant that
	    # we don't care about (it's small)
	    $dt -= $dt_setup;

	    # save the time of the first thread
	    $thread1_dt = $dt if($nthreads==1);
	    
	    printf DATA "%d %d %d %g %g %5.2f%% %5.2f%% %5.2f\n",
	    $nlines,$nstars,$nthreads,
	    $dt,$dt/$nstars,
	    100.0 * $dt/MAX(1e-10,$thread1_dt),
	    100.0/$nthreads,
	    $dt*$nthreads/($thread1_dt),
	    ;
	}
	print DATA "\n";
    }
}
close DATA;

sub run_test_grid
{
    # run a test grid for the given nstars, nthreads
    my $opts = $_[0];

    my $t0 = [gettimeofday];
    defaults($opts);
    $results={};

    # run flexigrid
    flexigrid($$opts{nthreads});

    my $t1 = [gettimeofday];

    output_binary_grid_timers();
    
    # return time interval
    return tv_interval($t0,$t1);
}

sub defaults
{
    # the default grid
    my $opts = $_[0];

    # clean binary grid
    clean_binary_grid_internal_variables();

    # set binary_grid defaults
    grid_defaults();

    # use dummy code
    $binary_grid::grid_options{prog}='src/perl/scripts-flexigrid/binary_c-dummy.pl';

    # set up threads
    $binary_grid::grid_options{threads_stack_size}=50; # MBytes

    # set up to be a fast run
    $binary_grid::grid_options{results_hash}=$results;
    $binary_grid::grid_options{parse_bse_function}='parse_bse';
    $binary_grid::grid_options{log_args}=0;
    $binary_grid::grid_options{sort_args}=0;
    $binary_grid::grid_options{save_args}=0;
    $binary_grid::grid_options{current_log_filename}=undef;
    $binary_grid::grid_options{nfs_sleep}=0;
    $binary_grid::grid_options{flexigrid_timeout_check_interval}=1.0;
    $binary_grid::grid_options{'always flush binary_err'}=0;
    $binary_grid::grid_options{timers}=0;
    $binary_grid::grid_options{timeout}=0;
    $binary_grid::grid_options{thread_presleep}=undef;
    $binary_grid::grid_options{timeout_vb}=0;
    $binary_grid::grid_options{arg_checking}=0;
    $binary_grid::grid_options{cpu_cap}=0;
    $binary_grid::grid_options{cpu_affinity}=0;
    $binary_grid::grid_options{binary_grid_code_filtering}=1;
    $binary_grid::grid_options{thread_max_freeze_time_before_warning}=100000;
    $binary_grid::grid_options{threadcomms}=0;
    $binary_grid::grid_options{'post exec wait'}=0;

    # be somewhat verbose
    $binary_grid::grid_options{vb}=1;
    $binary_grid::grid_options{tvb}=0;
    $binary_grid::grid_options{nmod}=10; 
    $binary_grid::grid_options{log_dt_secs}=5;

    # and not very nice
    $binary_grid::grid_options{nice}='';
    $binary_grid::grid_options{ionice}='';

    # thread options

    $binary_grid::grid_options{maxq_per_thread}=10;
    $binary_grid::grid_options{threads_stack_size}=32; # MB

    # no join
    $binary_grid::grid_options{threads_join_function}=undef;

    # binary_c dummy simulation
    foreach ('runtime','lines','mode')
    {
	$binary_grid::bse_options{'binary_c_'.$_} = $$opts{'binary_c_'.$_};
    }

    # set up simple single-star grid
    my $precode='$m1=exp($lnm1);  $eccentricity=0.0; $binary_grid::grid_options{weight}= 1.0;   ';
    my $nvar=0;
    my $m1min=0.1;
    my $mmax=100.0;

    $binary_grid::grid_options{'flexigrid'}{'grid variable '.$nvar++}=
    {
	'name'=> 'lnm1', 
	'longname'=>'Primary mass', 
	'range'=>["log($m1min)","log($mmax)"],
	'resolution'=> "$$opts{nstars}", 
	'spacingfunc'=>"const(log($m1min),log($mmax),$$opts{nstars})",
	'precode'=>$precode,
	'probdist'=>"Kroupa2001(\$m1)*\$m1",
	'dphasevol'=>'$dlnm1',
    };

    # final variable setup
    setup_binary_grid();
}

sub parse_bse
{
    if(0)
    {
	# use tbse_lines
	foreach my $x (@{binary_grid::tbse_lines()})
	{
	    chomp $x;
	    if($x eq 'fin')
	    {
		state $n;
		$n++;
		#print "Done star $n\n";
		return;
	    }
	}
    }
    else
    {
	# or individual tbse_line calls
	while(my $x = tbse_line())
	{
	    #print $x,"\n";
	    if($x eq 'fin')
	    {
		state $n;
		$n++;
		#print "Done star $n\n";
		return; 
	    }
	}
    }
}

sub joinfunc
{
    return;
}

sub nthreads_list
{
    # make a list of thread numbers up to 2*ncpus, 
    # e.g. for 4 CPUs try 1,2,4,8,
    # for 16 CPUs 1,2,4,8,16,32
    # etc.
    my @n=1;
    my $n=1;
    while($n<=$ncpus)
    {
	$n*=2;
	push(@n,$n);
    }
    return @n;
}
