#!/usr/bin/env perl
$|=1; # enable this line for auto-flushed output
use strict;
use Sort::Key qw(nsort);
use rob_misc;
use binary_grid;
use binary_grid::condor;
use threads;
use threads::shared;
use Fcntl ':flock'; # import LOCK_* constants
use subs qw(logprint logprintf);
use Carp qw(confess);
use File::Path qw(make_path);
use 5.16.0;
use List::Gen;
#
# Flexigrid-condor script for the canberra WD project
#
# V2 : with eccentricity, q power law and binary fraction shift

#
# todo : repeat basic calculation for WD Teff > 12kK only
#
#  
#

my $nm; my $nm1; my $nm2; my $nper; my $necc;

# grid resolution : full recommended resolution after #s
my $nm=100; # 1000
my $nm1=100; # 100
my $nm2=40; # 40
my $nper=40; # 40
my $necc=10;
my $useecc=0;

# resolution
#my $n=10 ; $nm=$n; $nm1=$n; $nm2=$n; $nper=$n;

# midres
#$nm=20; $nm1=20; $nm2=20; $nper=20;

# this gives me 63% $nm=100; $nm1=100; $nm2=10; $nper=10; and a load of missing G types (?) perhaps I need extra m2 resolution? same problem with 40x40x40x10

# M1 (and M2)
my $mmin=0.1; # should be 0.01 to include planets (?) or 0.1 for stars only
my $mmax=8.0; 

# impose selection effects?
my $selection_effects=1;

# ignore Sirius-like with a WD with temperature < this (5e3) [3000-6000]
my @sirius_teff_cutoffs = @{range 0,10000,250};

# WD-WD systems in which the WD temperatures
# differ by < this look single (1e3K, Lilia says 5e3K)
# (Holberg: 0.06) [100-900]
my @wdwd_teff_min_diffs = @{range 0,3000,100}; # 

# period distribution D+M91 options
my $logpermin=-2.0;
my $logpermax=12.0;
my $gauss_logmean=4.8;
my $gauss_sigma=2.3;

# q distribution
my $qbeta=0.0; # use qbeta by default
my $binary_fraction_shift=0.0;

# period distribution
my $period_distribution='bastard'; # 'DM91' or 'bastard'

my $outlock : shared;
my @spectral_type_strings=('O','B','A','F','G','K','M');
my %spectral_type;  revspechash();
my @stellar_type_strings;
my @short_stellar_type_strings;
my %short_stellar_type_strings;
my %binwidth=('period'=>0.1, # log period
	      'eccentricity'=>0.05);
my $outdir;
my $log_fp;

# fix aifa environment/paths
fix_aifa_environment();
my $results={}; # reset results
defaults();
condor_grid();
output();
exit(0);

############################################################
############################################################
############################################################

sub defaults
{
    binary_grid::parse_args();
    grid_defaults();
    renice_me(19);
    binary_grid::parse_args();

    my $args="@ARGV";
    $outdir=($args=~/outdir=(\S+)/)[0];
    
    # optional parameters
    $qbeta=($args=~/qbeta=(\S+)/)[0] // $qbeta;
    $binary_fraction_shift=($args=~/binary_fraction_shift=(\S+)/)[0]//$binary_fraction_shift;

    # condor directory : this stores the temp files and the final 
    # output : you should make sure it is read/writeable on all
    # condor notes (e.g. via an NFS link)
    $binary_grid::grid_options{condor_dir}='/users/izzard/data/condor/canberra_tmp';
    $binary_grid::grid_options{condor_dir}=$ENV{HOME}.'/data/condor/canberra_tmp' if(`hostname`=~/buntspecht/);

    # condor requirements
    $binary_grid::grid_options{condor_options}{Requirements}=
	'Machine!="aibn73.astro.uni-bonn.de" && Machine!="aibn79.astro.uni-bonn.de"';
#	'Machine=="aibn36.astro.uni-bonn.de"';	

    $binary_grid::grid_options{condor_njobs}=condor_free();
    print "Use $binary_grid::grid_options{condor_njobs} condor slots\n"; 
 
    # always link binary_grid to our local results hash
    $binary_grid::grid_options{results_hash}=$results;

    # use which parser function?    
    $binary_grid::grid_options{parse_bse_function}='parse_bse';
    
    $binary_grid::grid_options{threads_stack_size}=50; # MBytes
    
    $binary_grid::grid_options{prog}='binary_c-canberra';
    $binary_grid::grid_options{'gdb wait'}=undef;

    $binary_grid::bse_options{z}=0.02;
    $binary_grid::bse_options{wd_sigma}=0.0;

    # tides: 1.0=normal 0=no tides
    $binary_grid::bse_options{tidal_strength_factor}=1.0;

    $binary_grid::bse_options{alpha_ce}=0.1; # 1
    $binary_grid::bse_options{lambda_ce}=-1; # -1 = automatically set
    $binary_grid::bse_options{lambda_ionisation}=0.5; # 0.0
    #$binary_grid::bse_options{lw}=1.0;

    # star formation stuff
    $binary_grid::bse_options{max_evolution_time}=10e3; # Myr
    
    # time resolution
    #$binary_grid::bse_options{'maximum_timestep'}=1.0;

    # code options
    $binary_grid::grid_options{'nice'}='nice -n +10'; # nice command e.g. 'nice -n +10' or '' 
    
    # use C routines when available (should be faster)
    #$binary_grid::grid_options{'code flavour'}='C';
    
    $binary_grid::grid_options{'timeout'}=0; # seconds until timeout
    
    $binary_grid::grid_options{'log_args'}=1;
    $binary_grid::grid_options{'vb'}=1; # make verbose
    $binary_grid::grid_options{'tvb'}=1; # thread logfile

    # activate flexigrid
    # build flexigrid
    
    # maximum stack size before we pause to allow stars to finish
    $binary_grid::grid_options{'flexigrid'}{'maxstack'}=10;

    # use local threads or a daemon
    $binary_grid::grid_options{'flexigrid'}{'method'}='local threads';

    # binary_c approximate runtime in s
    $binary_grid::grid_options{'binary_c_runtime'}=1e-3;
    
    # first attempt at an MC grid
    #$binary_grid::grid_options{'flexigrid'}{'grid type'}='monte carlo';

    my $nvar=0; 

    # duplicity
    $binary_grid::grid_options{'flexigrid'}{'grid variable '.$nvar++}=
    {
	'name'=>'duplicity',
	'longname'=>'Duplicity',
	'range'=>[0,1],
	'resolution'=>1,
	'spacingfunc'=>'number(1.0)',
	'precode'=>'$binary_grid::grid_options{binary}=$duplicity;binary_grid::flexigrid_grid_resolution_shift();',
	'gridtype'=>'edge',
	'noprobdist'=>1,
    };
    
    
    # Mass 1
    $binary_grid::grid_options{'flexigrid'}{'grid variable '.$nvar++}=
    {
	'name'=> 'lnm1', 
	'longname'=>'Primary mass', 
	'range'=>["log($mmin)","log($mmax)"],
	'resolution'=> "$nm1", # ignore single stars in approximate resolution calculation
	'spacingfunc'=>"const(log($mmin),log($mmax),\$binary_grid::grid_options{binary} ? $nm1 : $nm)",
	'precode'=>'$m1=exp($lnm1);  $eccentricity=0.0; my $binary_fraction=MAX(0.0,MIN(1.0,distribution_functions::raghavan2010_binary_fraction($m1)+$main::binary_fraction_shift));  $binary_grid::grid_options{weight}= $binary_grid::grid_options{binary}==0 ? 1.0-$binary_fraction : $binary_fraction;   ',
	#'probdist'=>"ktg93(\$m1)*\$m1", # KTG93 (all masses)
	'probdist'=>"Kroupa2001(\$m1)*\$m1",
	#'probdist'=>'powerlaw(0.8,100.0,-2.35,$m1)*$m1', # salpeter (0.8-100)
	'dphasevol'=>'$dlnm1',
    };
    
    # Binary stars: Mass 2 and Separation
    my $m2min=$mmin;

    if(defined($qbeta))
    {
	# use a q powerlaw with slope $qbeta
	$binary_grid::grid_options{'flexigrid'}{'grid variable '.$nvar++}=
	{
	    'condition'=>'$binary_grid::grid_options{binary}==1',
	    'name'=>'q',
	    'longname'=>'Mass ratio',
	    'range'=>[$m2min.'/$m1',1.0],
	    'resolution'=>$nm2,
	    'spacingfunc'=>'const('.$m2min.'/$m1,1.0,'.$nm2.')',
	    'probdist'=>"powerlaw(0.0,1.0,$qbeta,\$q)",
	    'precode'=>'$m2=$q*$m1;',
	    'dphasevol'=>'$dq',
	};
    }
    else
    {
	print STDERR "Mass ratio distribution unknown\n";
	print "Mass ratio distribution unknown\n";
	exit;
    }

    
    if($period_distribution eq 'DM91')
    {
	# old code
	$binary_grid::grid_options{'flexigrid'}{'grid variable '.$nvar++}=
	{  
	    'name'=>'logper',
	    'longname'=>'log(Orbital_Period)',
	    'range'=>[$logpermin,$logpermax],
	    'resolution',$nper,
	    'spacingfunc',"const($logpermin,$logpermax,$nper)",
	    'precode'=>"\$per=10.0**\$logper;\$sep=calc_sep_from_period(\$m1,\$m2,\$per)if(defined(\$m1)&&defined(\$m2));",
	    'probdist'=>"gaussian(\$logper,$gauss_logmean,$gauss_sigma,$logpermin,$logpermax)",
	    'dphasevol'=>'$dln10per'
	}
    }
    elsif($period_distribution eq 'bastard')
    {
	# period distribution : rob's bastard
	$binary_grid::grid_options{'flexigrid'}{'grid variable '.$nvar++}=
	{
	    'name'=>'log10per',
	    'longname'=>'log10(Orbital_Period)',
	    'range'=>['-1.0','10.0'],
	    'resolution',$nper,
	    'spacingfunc',"const(-1.0,10.0,$nper)",
	    'precode'=>"my \$per=10.0**\$log10per;my\$sep=calc_sep_from_period(\$m1,\$m2,\$per);",
	    'probdist'=>'Izzard2012_period_distribution($per,$m1)',
	    'dphasevol'=>'$dlog10per'
	};
    }
    else
    {
	print "Unknown period distribution $period_distribution\n";
	exit;
    }


    if($useecc){
    # eccentricity
    $binary_grid::grid_options{'flexigrid'}{'grid variable '.$nvar++}=
    {
	'name'=>'ecc',
	'longname'=>'log10(Eccentricity)',
	'range'=>[0,0.999],
	'resolution',$necc,
	'spacingfunc',"const(0.0,0.999,$necc)",
	'precode','$eccentricity=$ecc;',
	# gaussian as in Meiborn & Mathieu 2005
	'probdist'=>'gaussian($ecc,0.38,0.23,0.0,0.999)',
	'dphasevol'=>'$decc'
    };
    }

    printf "Flexigrid modulo = %d, offset = %d\n",
    $binary_grid::grid_options{modulo},
    $binary_grid::grid_options{offset};

    binary_grid::parse_args();
    secondary_variables();
    setup_binary_grid();

    @stellar_type_strings=@{(stellar_type_strings())[2]};
    @short_stellar_type_strings=@{(stellar_type_strings())[3]};
    %short_stellar_type_strings=%{(stellar_type_strings())[4]};
    #print "Short @short_stellar_type_strings\n";
}

############################################################

sub output
{
    # if we're running condor, do not output unless for the final call
    return if($binary_grid::grid_options{condor_command} eq 'run_flexigrid');

    # open log file
    $outdir = $ENV{VOLDISK}.'/data/canberra/test-selection-effects' 
	if(!defined($outdir));
    make_path($outdir); # nb we assume this works...

    my $outdir2;

    # copy results to original_results so we can reuse it repeatedly
    my $original_results = $results;

    foreach my $wdwd_teff_min_diff (nsort keys %$original_results)
    {
	# ignore test distributions
	next if ($wdwd_teff_min_diff eq 'test');
	next if ($wdwd_teff_min_diff eq 'thread_number');

	#print "FOUND WDWD TEFF MIN DIFF $wdwd_teff_min_diff\n";
	foreach my $sirius_teff_cutoff (nsort keys %{$original_results->{$wdwd_teff_min_diff}})
	{
	    my $outdir2 = $outdir.'/wdwd_teff_min_diff_'.$wdwd_teff_min_diff.'-sirius_teff_cutoff_'.$sirius_teff_cutoff;
	    mkdirhier($outdir2);

	    # set results hash pointer
	    $results = $original_results->{$wdwd_teff_min_diff}->{$sirius_teff_cutoff};

	    # open output log
	    open ($log_fp,">$outdir2/log") || 
		confess ("cannot open $outdir2/log for output");

	    logprint "OUTPUT $wdwd_teff_min_diff : $sirius_teff_cutoff : to $outdir2\nUsing results hash $results (original_results=$original_results)\n";
	    
	    # and output

	    # output binary_grid::grid_options/bse_options
	    open(my $grid_fp,">$outdir2/grid_options")||
		confess("cannot open $outdir2/grid_options for output");
	    print $grid_fp "grid_options\n\n";
	    map
	    {
		print {$grid_fp} $_.' : '.$binary_grid::grid_options{$_}."\n";
	    }keys %binary_grid::grid_options;
	    close $grid_fp;

	    open(my $bse_fp,">$outdir2/bse_options")||
		confess("cannot open $outdir2/bse_options for output");
	    print $bse_fp "bse_options\n\n";
	    map
	    {
		print {$bse_fp} $_.' : '.$binary_grid::bse_options{$_}."\n";
	    }keys %binary_grid::bse_options;
	    close $bse_fp;
	    
	    open(my $version_fp,">$outdir2/binary_c-version")||
		confess("cannot open $outdir2/binary_c-version");
	    print {$version_fp} `$binary_grid::grid_options{rootpath}/$binary_grid::grid_options{prog} --version`;
	    close $version_fp;

	    # output grid results
	    logprint "\n\n\nCANBERRA Grid results\n============================================================\n\n";
	    logprint "SVN Revision ",`svn info|grep Revision|gawk \"\{print \\\$2\}\"`;
	    logprint "Run at ",scalar(localtime),"\n";
	    logprint "Resolution: nm=$nm nm1=$nm1 nm2=$nm2 nper=$nper necc=$necc (useecc=$useecc)\n";
	    logprint "$mmin < M < $mmax\n";
	    logprint "Period distribution : $period_distribution : $logpermin < logP < $logpermax : guassian mu=$gauss_logmean sigma=$gauss_sigma\n";
	    logprint "Sirius teff cutoff = $sirius_teff_cutoff\n";
	    logprint "WDWD min teff difference = $wdwd_teff_min_diff\n";
	    logprint "\n\nParameters:\n";
	    logprint "Initial binary fraction : Raghavan 2010\n";
	    foreach my $p ('alpha_ce','lambda_ce','lambda_ionisation','z','wd_sigma','lw')
	    {
		my $opt = ($p eq 'lambda_ce' && $binary_grid::bse_options{$p}==-1) 
		    ? 'Fitted to detailed stellar models' 
		    : 
		    sprintf "%g",$binary_grid::bse_options{$p};

		logprint sprintf "% -30s = %s\n",$p eq'z'?'Metallicity':$p,$opt;
	    }

	    logprint "Q: power law slope $qbeta\n";
	    logprint "binary_fraction_shift = $binary_fraction_shift\n";
	    
	    my $dtp_wd=1e-30; # total number count of WDs (single or binary)
	    my %dtp_wd;

	    {
		my $k='number count';
		# get total WD number count
		foreach my $duplicity (keys %{$results->{$k}})
		{ 
		    foreach my $wd_type (keys %{$results->{$k}->{$duplicity}})
		    {
			my $x = $$results{$k}->{$duplicity}->{$wd_type};
			$dtp_wd += $x;
			$dtp_wd{$duplicity} += $x;
		    } 
		}
	    }

	    my $dtp_wdms=0.0;
	    {
		# get total WD-MS number count
		my $k='MSWD spectral types';
		foreach my $spectral_type (nsort keys %{$results->{$k}})
		{
		    $dtp_wdms += $$results{$k}{$spectral_type};
		}
	    }
	    foreach my $k (sort keys %$results)
	    {
		next if($k eq 'thread_number');
		if($k eq 'number count')
		{
		    logprint sprintf "WD binary fraction %3.2f %%\n",$dtp_wd{'Binary'}/$dtp_wd*100.0;
		    foreach my $duplicity (keys %{$results->{$k}})
		    {
			foreach my $wd_type (nsort keys %{$results->{$k}->{$duplicity}})
			{
			    logprint sprintf "% -6s % -30s %3.2f %%\n",
			    $duplicity,
			    $stellar_type_strings[$wd_type], 
			    $$results{$k}->{$duplicity}->{$wd_type}/$dtp_wd*100.0;
			} 
		    }
		}
		elsif($k eq 'WD initial spectral types')
		{
		    foreach my $spectral_type (@spectral_type_strings)
		    {
			logprint sprintf "Contribution to WD space from initial primary spectral type %s = %g\n",
			$spectral_type,
			$$results{$k}{$spectral_type}/$dtp_wd;
		    }
		}
		elsif($k eq 'companion types')
		{
		    my $test_tot=0.0;
		    logprint "\n\nWhite Dwarf - ? pairs (as a fraction of all White Dwarf Binaries)\n";
		    foreach my $wd1 (nsort keys %{$results->{$k}})
		    {
			foreach my $wd2 (nsort keys %{$results->{$k}->{$wd1}})
			{
			    logprint sprintf "% -40s - % -40s     % 5.2f %%\n",
			    $stellar_type_strings[$wd1],
			    $stellar_type_strings[$wd2],
			    $$results{$k}{$wd1}{$wd2}
			    /$dtp_wd{'Binary'}*100.0;
			}
		    }
		    logprint "\n";
		}
		elsif($k eq 'test')
		{
		    # test initial binary fraction
		    my $test_tot=0.0;
		    logprint "\n\nTest numbers\n\n";
		    foreach my $duplicity (keys %{$results->{$k}})
		    {
			$test_tot += $$results{$k}->{$duplicity};
		    }
		    foreach my $duplicity (nsort keys %{$results->{$k}})
		    {
			logprint sprintf "MS $duplicity %3.2f %%\n", 100.0*$$results{$k}->{$duplicity}/$test_tot;
		    }
		}
		elsif($k eq 'test dist spectypes')
		{
		    print "ZAMS Binary fraction by spectral type : ",keys %{$results->{$k}->{'Binary'}},"\n\n";
		    foreach my $spec_type (@spectral_type_strings)
		    {
			my $t=$$results{$k}->{'Single'}->{$spec_type}+$$results{$k}->{'Binary'}->{$spec_type};
			logprint sprintf "%s %5.2f %%\n",$spec_type,$$results{$k}->{'Binary'}->{$spec_type}/$t*100.0 if($t>0.0);
		    }
		}
		elsif($k eq 'test dist')
		{
		    # test initial distributions
		    foreach my $duplicity (keys %{$results->{$k}})
		    {
			foreach my $dist (keys %{$results->{$k}->{$duplicity}})
			{
			    my $f="$outdir2/init_".$duplicity.'_'.$dist.'.dat';
			    logprint "Output distribution to $f\n";
			    open(FP,">$f")||confess("can't open $f");
			    foreach my $x (nsort keys %{$results->{$k}->{$duplicity}->{$dist}})
			    {
				printf FP "%g %g\n",$x,$$results{$k}{$duplicity}{$dist}{$x};
			    }
			    close FP;
			}
		    }
		}
		elsif($k eq 'MSMS q distribution')
		{
		    # test initial distributions
		    foreach my $obsqmin (nsort keys %{$results->{$k}})
		    {
			my $f="$outdir2/MSMSobsq_gt_$obsqmin.dat";
			logprint "Output distribution to $f\n";
			open(FP,">$f")||confess("can't open $f");
			foreach my $q (nsort keys %{$results->{$k}->{$obsqmin}})
			{

			    printf FP "%g %g\n",$q,$$results{$k}{$obsqmin}{$q};
			}
			close FP;
		    }
		}
		elsif($k eq 'MSWD spectral types')
		{
		    my $f="$outdir2/MSWD_spectral_types.dat";
		    open(FP,'>'.$f)||confess("cannot open $f for output");
		    logprint "MSWD binaries: spectral types (see datafile $f)\n";
		    pad_datahash($results->{$k},1);
		    foreach my $spectral_type (nsort keys %{$results->{$k}})
		    {
			printf FP "%d %g\n",$spectral_type,$$results{$k}{$spectral_type}/$dtp_wdms;
			logprint sprintf "MSWD spectral type %d = %s = %g\n",
			$spectral_type,$spectral_type_strings[$spectral_type],$$results{$k}{$spectral_type}/$dtp_wdms;
		    } 
		    logprint "\n";
		    close FP;
		}
		# misc period distributions
		elsif($k =~/(\S+) period distribution/)
		{
		    my $distname=$1;
		    my $f="$outdir2/$distname".'_period_distribution.dat';
		    #print "Pad datahash $results->{$k}\n";
		    pad_datahash($results->{$k},$binwidth{period});
		    open(FP,'>'.$f)||confess("cannot open $f for output");
		    logprint "$1 binaries: period distribution (see datafile $f)\n";
		    foreach my $logP (nsort keys %{$results->{$k}})
		    {
			printf FP "%g %g\n",$logP,$$results{$k}{$logP};
		    } 
		    close FP;
		}
		# misc ecc distributions
		elsif($k =~/(\S+) eccentricity distribution/)
		{
		    my $distname=$1;
		    my $f="$outdir2/$distname".'_eccentricity_distribution.dat';
		    #print "Pad datahash $results->{$k}\n";
		    pad_datahash($results->{$k},$binwidth{eccentricity});
		    open(FP,'>'.$f)||confess("cannot open $f for output");
		    logprint "$1 binaries: eccentricity distribution (see datafile $f)\n";
		    foreach my $logP (nsort keys %{$results->{$k}})
		    {
			printf FP "%g %g\n",$logP,$$results{$k}{$logP};
		    } 
		    close FP;
		}
	    }

	    # compare to Holberg 2009
	    {
		my $chisq=0.0;
		my %obs=(
		    # Holberg 2009 : converted to number of systems
		    'Single WDs'=>{f=>0.736,sigma=>0.077}, 
		    'WD +dM'=>{f=>0.128,sigma=>0.032},
		    'Sirius-Like'=>{f=>0.080,sigma=>0.025},
		    'WD + WD'=>{f=>0.056,sigma=>0.021},
		    'Total WDs'=>{f=>1.0,sigma=>0.0},
		    );

		my $format="\% -30s \% 5.2f (% 5.2f +- % 5.2f) dchisq=%g\n";
		logprint "\n=============\nHolberg 2009 comparison\n\n";

		# Single WD fraction
		my $single_WD = $dtp_wd{'Single'}/$dtp_wd;

		my $dchisq = (($single_WD-$obs{'Single WDs'}{f})/$obs{'Single WDs'}{sigma})**2.0;
		logprintf $format,'Single WDs',$single_WD,$obs{'Single WDs'}{f},$obs{'Single WDs'}{sigma},$dchisq;

		$chisq += $dchisq;

		# WD + dM fraction
		my $WD_dM=$$results{'MSWD spectral types'}{$spectral_type{'M'}}/$dtp_wd;

		$dchisq = (($WD_dM-$obs{'WD +dM'}{f})/$obs{'WD +dM'}{sigma})**2.0;
		logprintf $format,'WD +dM',$WD_dM,$obs{'WD +dM'}{f},$obs{'WD +dM'}{sigma},$dchisq;
		$chisq += $dchisq;

		# Sirius-like fraction
		my $Sirius=0.0;
		map{$Sirius+=$$results{'MSWD spectral types'}{$spectral_type{$_}}}('O','B','A','F','G','K');

		$Sirius = $Sirius/$dtp_wd ;
		$dchisq = (($Sirius-$obs{'Sirius-Like'}{f})/$obs{'Sirius-Like'}{sigma})**2.0;

		logprintf $format,'Sirius-Like',$Sirius,$obs{'Sirius-Like'}{f},$obs{'Sirius-Like'}{sigma},$dchisq;

		$chisq += $dchisq;

		# WD + WD fraction
		my $WDWD=0.0;
		foreach my $wd1 (10,11,12)
		{
		    foreach my $wd2 (10,11,12)
		    {
			$WDWD += $$results{'companion types'}{$wd1}{$wd2};
		    }
		}
		$WDWD = $WDWD/$dtp_wd;
		$dchisq = (($WDWD-$obs{'WD + WD'}{f})/$obs{'WD + WD'}{sigma})**2.0;
		logprintf $format,'WD + WD',$WDWD,$obs{'WD + WD'}{f},$obs{'WD + WD'}{sigma},$dchisq;
		
		$chisq += $dchisq;

		# total (should be 100%!)
		logprintf $format,'Total WDs',($single_WD + $WD_dM + $Sirius + $WDWD),$obs{'Total WDs'}{f},$obs{'Total WDs'}{sigma};
		
		logprint "\n\nCHISQ $chisq\n\n";
	    }

	    logprint "\n============================================================\n\n\n";

	    close $log_fp;
	}
    }

}

sub parse_bse
{
    # this gets called by the binary_grid module, it parses the 
    # lines of output from binary_c/nucsyn
    my $brk=0; 

    # you should set data in the $h hash pointer, which is 
    # then passed to the join_thread function (below) for you
    # to add up. (Note: $h is local to each thread.)
    my $h=shift;

    my $nthread=$binary_grid::grid_options{'thread_num'};
    my @init_spectypes;

    while($brk==0)
    {
	$_=tbse_line();
	#print "PARSE $_\n";
	chomp;
	if($_ eq 'fin')
	{
	    $brk=1; # the end of output
 	}
	else
	{
	    my $l=$_;
	    if(s/^CANBERRA_TEST(\d+) (\S+)\s*//)
	    {
		# test MS star
		my $duplicity= !$1 ? 'Single' : 'Binary';
		my $dp = $2 ;
		$$h{'test'}{'test'}{'test'}{$duplicity} += $dp;
		
		# save initial distributions
		my @x=split(/\s+/o,$_);

	#	printf "PARSE_BSE Kopt %g,%g results=$results results_hash=$binary_grid::grid_options{results_hash}\n",
	#	$$results{'test dist spectypes'}{'Single'}{K},
	#	${$binary_grid::grid_options{results_hash}}{'test dist spectypes'}{'Single'}{K};

		# save the initial spectral types
		$init_spectypes[1] = $spectral_type_strings[$x[1]];

		if($duplicity eq 'Binary')
		{
		    #print "LOGM1 $binary_grid::count x0=$x[0] dp=$dp x2=$x[2] x3=$x[3]\n";
		    $$h{'test'}{'test'}{'test dist'}{'Binary'}{'M1'}{bin_data(log10($x[0]),0.1)}+=$dp;
		    $$h{'test'}{'test'}{'test dist'}{'Binary'}{'q'}{bin_data($x[2],0.1)}+=$dp;
		    #printf "INIT Q %g (M1=%g M2=%g) p=%g : into qbin %g\n",$x[2],$x[0],$x[2]*$x[0],$dp,bin_data($x[2],0.1);
		    $$h{'test'}{'test'}{'test dist'}{'Binary'}{'logP'}{bin_data(log10(MAX(1e-10,$x[3])),$binwidth{period})}+=$dp;
		    $$h{'test'}{'test'}{'test dist'}{'Binary'}{'ecc'}{bin_data($x[4],$binwidth{eccentricity})}+=$dp;
		    $init_spectypes[2] = $spectral_type_strings[$x[5]];
		}
		else
		{
		    #print "LOG M $x[0] from $l\n";
		    $$h{'test'}{'test'}{'test dist'}{'Single'}{'M'}{bin_data(log10($x[0]),0.1)}+=$dp;
		}
		
		#print "add spectype $duplicity $x[1] $spectral_type_strings[$x[1]] (@x)\n";
		$$h{'test'}{'test'}{'test dist spectypes'}{$duplicity}{$spectral_type_strings[$x[1]]}+=$dp;
	    }
	    elsif(/^CANBERRAMSMSQ (\S+) (\S+)/)
	    {
		#print $_,"\n";
		# MS-MS q distribution
		my $q=$1;
		my $dtp=$2;

		foreach my $obsqmin (0,0.1,0.2,0.3,0.5,0.8)
		{
		    if($q>$obsqmin)
		    {
			$$h{'test'}{'test'}{'MSMS q distribution'}{$obsqmin}{bin_data($q,0.05)}+=$dtp;
		    }
		}
	    }
	    elsif(s/^CANBERRA(\d+) //)
	    {
		my $orig_duplicity= $1 ? 'Binary' : 'Single';
		my @x=split(/\s+/o,$_);

		my $vb=0; # verbosity

		# x:
		my $dtp=shift @x; # 1 = dtp
		my $t=shift @x; # 2 = model time (Myr)

		# weight dtp according to SFR (i.e. age)
		$dtp *= SFR($binary_grid::bse_options{'max_evolution_time'}-$t);

		if($dtp>0.0)
		{
		    #print "AGE $t\n";
		    my $wd_st=shift @x; # 3 = WD stellar type
		    my $wdspec=shift @x; # 4 = WD spectral type
		    my $wdteff=shift @x; # 5 = WD teff
		    my $comp_st=shift @x; # 6 = companion stellar type
		    my $compspec=shift @x; # 7 = companion spectral type
		    my $compteff=shift @x; # 8 = teff of companion 
		    my $P=shift @x; # 9 = period (days)
		    my $ecc=shift @x; # 10 = eccentricity 

		    if($orig_duplicity eq 'Binary')
		    {
			# bin the orbital period
			$P=bin_data(log10(MAX(1e-10,$P)),$binwidth{period});
			
			# bin the eccentricity
			$ecc=bin_data($ecc,$binwidth{eccentricity});
		    }

		    printf "t=%g dtp=%g : wd_st=%d wdspec=%d wdteff=%g ( comp_st=%d compspec=%d compteff=%g) P=%g ecc=%g\n",$t,$dtp,$wd_st,$wdspec,$wdteff,$comp_st,$compspec,$compteff,$P,$ecc if($vb); 
		
		    # subtypes:
		    # M-dwarf companions or 
		    # Sirius-like binaries (K-O type companions)    
		    my $xxx= $compspec == $spectral_type{'M'} ? 'WD+dM' : 'Sirius-like';

		    foreach my $sirius_teff_cutoff (@sirius_teff_cutoffs)
		    {
			foreach my $wdwd_teff_min_diff (@wdwd_teff_min_diffs)
			{
			    # assume visible
			    my $visible=1;

			    # perhaps override original duplicity
			    my $duplicity = $orig_duplicity;

			    printf "Check vis xxx='$xxx' wdteff=$wdteff<$sirius_teff_cutoff? wdteff-compteff=%g<$wdwd_teff_min_diff\n",abs($wdteff-$compteff) if($vb);

			    # apply selection effects
			    if($selection_effects && $duplicity eq 'Binary')
			    {
				# ignore Sirius-like with a WD with temperature < $sirius_teff_cutoff:
				# the WD is invisible in these cases
				if(($xxx eq 'Sirius-like') && ($wdteff < $sirius_teff_cutoff))
				{
				    $visible=0;
				    print "FILTER ignore Sirius-like system\n"if($vb);
				}

				# WD-WD systems in which the WD temperatures do not
				# differ by >1e3K (Lilia says 5e3K) look single
				$duplicity='Single' if($comp_st>9 && $comp_st<13 && abs($wdteff-$compteff)<$wdwd_teff_min_diff);
			    }

			    printf "%sisible system; $duplicity\n",($visible ? 'V' : 'Inv') if($vb); 

			    if($visible)
			    {
				# save the contribution from each initial spectral types 
				$$h{$wdwd_teff_min_diff}{$sirius_teff_cutoff}{'WD initial spectral types'}{$init_spectypes[1]}+=$dtp;

				print "At t=$t dtp=$dtp WD primary initial spec type is $init_spectypes[1] duplicity=$duplicity\n"if($vb);

				# save WD number counts
				$$h{$wdwd_teff_min_diff}{$sirius_teff_cutoff}{'number count'}{$duplicity}{$wd_st}+=$dtp;
				
				if($wd_st==15)
				{
				    print "FACK : found massless remnant when I should have a WD!\n";
				    tbse_land();
				    exit;
				}

				if($duplicity eq 'Binary')
				{
				    # now for the different binary types
				    $$h{$wdwd_teff_min_diff}{$sirius_teff_cutoff}{'companion types'}{$wd_st}{$comp_st}+=$dtp;
				    # distributions
				    
				    print "Add binary with types $wd_st - $comp_st += $dtp\n"if($vb);
				    
				    # WD - MS binaries
				    if($comp_st<=1)
				    {	
					# spectral type distribution of MS-WD binaries
					$$h{$wdwd_teff_min_diff}{$sirius_teff_cutoff}{'MSWD spectral types'}{$compspec}+=$dtp;
					#print "Add MSWD with secondary type $compspec\n";
					
					# (binned) period distribution of MS-WD binaries
					$$h{$wdwd_teff_min_diff}{$sirius_teff_cutoff}{'MSWD period distribution'}{$P}+=$dtp;

					# (binned) eccentricity distribution of MS-WD binaries
					#$$h{'MSWD eccentricity distribution'}{$ecc} += $dtp;
					$$h{$wdwd_teff_min_diff}{$sirius_teff_cutoff}{'MSWD eccentricity distribution'}{$ecc}+=$dtp;

					# log period distribution for
					$$h{$wdwd_teff_min_diff}{$sirius_teff_cutoff}{$xxx.' period distribution'}{$P}+=$dtp;	
					$$h{$wdwd_teff_min_diff}{$sirius_teff_cutoff}{$xxx.' eccentricity distribution'}{$ecc}+=$dtp;	
				    }
				    
				    # period/ecc distributions as a function of both stellar types
				    
				    # stellar types as strings
				    my @st = ($short_stellar_type_strings[$wd_st],
					      $short_stellar_type_strings[$comp_st==0 ? 1 : $comp_st]);
				    $$h{$wdwd_teff_min_diff}{$sirius_teff_cutoff}{$st[0].$st[1].' period distribution'}{$P}+=$dtp;	
				    $$h{$wdwd_teff_min_diff}{$sirius_teff_cutoff}{$st[0].$st[1].' eccentricity distribution'}{$ecc}+=$dtp;
				}
			    }
			}
		    }
		}
	    }
	}
    }
}


sub secondary_variables
{
    # from the defaults and/or passed in variables,
    # calculate useful things

    # use colour?
    if($binary_grid::grid_options{'colour'}==1)
    {
        # use ANSI color for output to xterms/rxvt etc.
        use Term::ANSIColor;
        # set to white on black
        print color("white on_black");
    }
    else
    {
        # dummy function to replace the ANSI color() function
        eval("sub\ color\{ \}");
    }
}

sub logprint
{
    print {$log_fp} "@_" if(defined $log_fp);
    print "@_";
}
sub logprintf
{
    my $format=shift @_;
    printf {$log_fp} $format,@_;
    printf $format,@_;
}

sub revspechash
{
    for(my $i=0;$i<=$#spectral_type_strings;$i++)
    {
	$spectral_type{$spectral_type_strings[$i]}=$i;
    }
}

sub spec_to_float
{
    my $s=$_[0]; # spectral type to check
    if($s=~/([A-Z])(\d)/)
    {
	return $spectral_type{$1}+0.1*$2;
    }
    else
    {
	print "Failed to convert spec type $_[1] to float\n";
	exit;
    }
}

sub spec_type_sorter
{
    return spec_to_float($a) <=> spec_to_float($b);
}

sub pad_datahash
{
    # pad datahash $h so it has zeros where there's no data
    my $h=shift;
    my $d=shift;
    my @k=nsort keys %$h;
    my $xmax=$k[$#k]; # max value
    my $x=$k[0]; # start (min) value
    while($x<$xmax)
    {
	$$h{$x}=0.0 if(!defined($$h{$x}));
	$x=sprintf '%g',$x+$d;
    }
}

sub SFR
{
    # SFR as a function of Galactic age in Myr
    my $t=$_[0];

    return 1.0; # const SFR
    #return $t<1.0e3 ? 1.0 : 0.0; # const for a given time period
    #return exp(-$t/$SF_timescale}); # exponential
}

sub fix_aifa_environment
{
    if(`hostname`=~/aibn/)
    {
	map
	{
	    print "CHECK $_\n";
	    if($ENV{$_}=~s!/export/aibn(\d+)_(\d+)/!/vol/aibn$1/aibn$1\_$2/!g)
	    {
		print "CONV $_=$ENV{$_}\n";
	    }
	}%ENV;
    }
}

sub condor_free
{
    # return number of free condor slots
    my $cmd='condor_status|grep -A2 Claimed\ Unclaimed|tail -1|gawk "{print \$5}"';
    my $n=`$cmd`;
    chomp $n;
    return $n;
}

