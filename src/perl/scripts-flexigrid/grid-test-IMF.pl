#!/usr/bin/env perl

use strict;
use binary_grid;
use rob_misc;


#
# Flexigrid script to test the IMF
#

# grid resolution : full recommended resolution after #s
my $mult = 1; # default = 4
my $nm=50*$mult; # 1000
my $nm1=100*$mult; # 100
my $nm2=10*$mult; # 40
my $nper=10*$mult; # 40
my $useecc=0; # set useecc to 1 to have non-circular binaries 
my $necc=40; 

# $population can be:
# 'single' : only single stars
# 'binary' : only binary stars
# 'mixed'  : a mixed population 
my $population = 'binary';

# bin data in linear or log time?
# (either way, we need to normalize to the bin width)
# 0/undef = no
# 'linear' = linear in time,
# 'log' = in log time bins
my $time_bin_method = 'log';
my $time_bin_linear_width=1.0; # in Myr
my $time_bin_log_width=0.1; # (in decades)

# Sampling factor: prevent spikes if we can.
# Should use 1.0/INTEGER for smooth filling of bins.
# the results with 1/10 are very similar to 1/2 : so
# use 1/2 (both tested with time_bin_log_width=0.1)
my $sampling_factor=1.0/2.0;

# bin widths for final SN rate plots
my $dt=$time_bin_linear_width*$sampling_factor; # grid time resolution
my $dlogt=$time_bin_log_width*$sampling_factor; # or in log time (should be < time_bin_log_width)

# grid spacing scheme:
# 0/undef = no
# 'linear' = linear in time,
# 'log' = in log time bins
my $time_adaptive_m1=undef;#'log'; 

my %results;

# M1 and M2 grid limits
my $m1min=0.1; 
my $m2min=0.1; 
my $mmax=80.0; 

# period distribution D+M91 options
my $logpermin=-2.0;
my $logpermax=12.0;
my $gauss_logmean=4.8;
my $gauss_sigma=2.3;

# q distribution
my $qbeta=0.0; # use qbeta by default

# period distribution
my $period_distribution='bastard'; # 'DM91' or 'bastard'

my $results={}; # reset results

defaults();
flexigrid(4);

printf "RESULT : mean mass = %g\n",$$results{mass};

exit(0);

############################################################
############################################################
############################################################

sub defaults
{
    binary_grid::parse_args();
    grid_defaults();
    renice_me(10);
    binary_grid::parse_args();

    my $args="@ARGV";


    if(!defined($ENV{HOSTNAME}))
    {
	$ENV{HOSTNAME} = `hostname`;
	chomp $ENV{HOSTNAME};
    }

    # always link binary_grid to our local results hash
    $binary_grid::grid_options{results_hash}=$results;

    # use which parser function?    
    $binary_grid::grid_options{parse_bse_function}='parse_bse';
    
    $binary_grid::grid_options{threads_stack_size}=50; # MBytes
    
    $binary_grid::grid_options{prog}='binary_c-sn2014';
    $binary_grid::grid_options{'gdb wait'}=undef;

    $binary_grid::bse_options{z}=0.02;
    $binary_grid::bse_options{wd_sigma}=0.0;
    #$binary_grid::bse_options{maximum_timestep}=0.1;

    # tides: 1.0=normal 0=no tides
    $binary_grid::bse_options{tidal_strength_factor}=1.0;

    $binary_grid::bse_options{alpha_ce}=1.0; # 1
    $binary_grid::bse_options{lambda_ce}=-1; # -1 = automatically set
    $binary_grid::bse_options{lambda_ionisation}=0.0; # 0.0
    #$binary_grid::bse_options{lw}=1.0;

    # wind: 0=default, 1=mm, 2=nl, 3=jje/vink
    $binary_grid::bse_options{wr_wind}=0;

    # Black hole prescription: 0=Hurley, 1=Belczynski
    $binary_grid::bse_options{BH_prescription}=0;

    # star formation stuff
    $binary_grid::bse_options{max_evolution_time}=0.0001;#10e3; # Myr
    
    # time resolution
    $binary_grid::bse_options{maximum_timestep}=10;

    # code options
    $binary_grid::grid_options{nice}='nice -n +10'; # nice command e.g. 'nice -n +10' or '' 
        
    $binary_grid::grid_options{timeout}=0; # seconds until timeout (0=disable)
    
    $binary_grid::grid_options{log_args}=0;
    $binary_grid::grid_options{sort_args}=0;
    $binary_grid::grid_options{vb}=1; # make verbose
    $binary_grid::grid_options{tvb}=0; # thread logfile
    

    $binary_grid::grid_options{nmod}=1000; # log every 1000 models
    $binary_grid::grid_options{log_dt_secs}=5; # 300 = log every 5 minutes

    # activate flexigrid
    # build flexigrid
    
    # maximum stack size before we pause to allow stars to finish
    $binary_grid::grid_options{flexigrid}{maxstack}=10;

    if($population eq 'mixed')
    {
	distribution_functions::bastard_distribution({
	    binary_fraction_shift=>0.0,
	    mmin=>$m1min,
	    mmax=>$mmax,

	    # use time-adaptive mass resolution for M1
	    time_adaptive=>{
		vb=>1,
		mass_grid_log10_time=>1,
		mass_grid_step=>$dlogt,
		mass_grid_log10_step=>$dlogt,
		extra_flash_resolution=>0,
		mass_grid_nlow_mass_stars=>10,
		debugging_output_directory=>"/tmp/griddebug",
		max_delta_m=>2.0},	   

	    m2min=>$m2min,
	    nm2=>$nm2,
	    nper=>$nper,
	    necc=>$necc,
	    qmin=>0.0,
	    qmax=>1.0,
	    useecc=>undef,

	    # mass distribution options: -2.3 by default 
	    mass_dist_opts=>{p2=>-2.2, 
			     p3=>-2.2},
						     });
    }
    elsif($population eq 'single')
    {
	my $nvar=0;
	$binary_grid::grid_options{binary}=0;

	# Mass 1
	if($time_adaptive_m1)
	{
	    my $use_logt = ($time_adaptive_m1 eq 'log') ? 1 : 0;
	    
	    my $preloopcode = "
my \$const_dt_opts=\{
    max_evolution_time=>$binary_grid::bse_options{max_evolution_time},
    stellar_lifetime_table_nm=>400,
    nthreads=>1,
    thread_sleep=>1,
    mmin=>$m1min,
    mmax=>$mmax,
    time_adaptive_mass_grid_log10_time=>$use_logt,
    time_adaptive_mass_grid_step=>$dt,
    time_adaptive_mass_grid_log10_step=>$dlogt,
    extra_flash_resolution=>0,
    time_adaptive_mass_grid_nlow_mass_stars=>10,
    debugging_output_directory=>'/users/izzard/data/sn2014/adaptive_debug',
    max_delta_m=>2.0,
    savegrid=>0,
    vb=>1,
\};
spacing_functions::const_dt(\$const_dt_opts,'reset');
";
	    print $preloopcode,"\n";

	    $binary_grid::grid_options{'flexigrid'}{'grid variable '.$nvar}=
	    {
		# const_dt spacing function options
		'preloopcode'=>$preloopcode,
		# use const_dt function
		spacingfunc=>"const_dt(\$const_dt_opts,'next')",
		# and its resolution
		resolution=> "spacing_functions::const_dt(\$const_dt_opts,'resolution');",
		precode=>'$m1=exp($lnm1);  $eccentricity=0.0; $binary_grid::grid_options{weight}= 1.0;   ',
		name=>'lnm1',
		longname=>'Primary mass', 
		range=>["log($m1min)","log($mmax)"],
		#probdist=>"Kroupa2001(\$m1)*\$m1",
		probdist=>'powerlaw(0.8,100.0,-2.35,$m1)*$m1', # salpeter (0.8-100)
		dphasevol=>'$dlnm1',
	    };
	    exit;
	}
	else
	{
	    my $precode='$m1=exp($lnm1);  $eccentricity=0.0; $binary_grid::grid_options{weight}= 1.0;   ';

	    print "M1 precode = $precode\n";

	    $binary_grid::grid_options{'flexigrid'}{'grid variable '.$nvar++}=
	    {
		'name'=> 'lnm1', 
		'longname'=>'Primary mass', 
		'range'=>["log($m1min)","log($mmax)"],
		'resolution'=> "$nm1", # ignore single stars in approximate resolution calculation
		'spacingfunc'=>"const(log($m1min),log($mmax),\$binary_grid::grid_options{binary} ? $nm1 : $nm)",
		'precode'=>$precode,
		#'probdist'=>"ktg93(\$m1)*\$m1", # KTG93 (all masses)
		'probdist'=>"Kroupa2001(\$m1)*\$m1",
		#'probdist'=>'powerlaw(0.1,100.0,-2.35,$m1)*$m1', # salpeter (0.8-100)
		#'probdist'=>'powerlaw(0.1,100.0,-2.35,$m1)*$m1', # salpeter (0.1-100)
		'dphasevol'=>'$dlnm1',
	    };
	}
    }
    elsif($population eq 'binary')
    {
	my $nvar=0;
	$binary_grid::grid_options{binary}=1;

	# Mass 1
	{
	    my $precode='$m1=exp($lnm1);  $eccentricity=0.0; $binary_grid::grid_options{weight}=1.0;   ';

	    print "M1 precode = $precode\n";

	    $binary_grid::grid_options{'flexigrid'}{'grid variable '.$nvar++}=
	    {
		'name'=> 'lnm1', 
		'longname'=>'Primary mass', 
		'range'=>["log($m1min)","log($mmax)"],
		'resolution'=> "$nm1", # ignore single stars in approximate resolution calculation
		'spacingfunc'=>"const(log($m1min),log($mmax),\$binary_grid::grid_options{binary} ? $nm1 : $nm)",
		'precode'=>$precode,
		#'probdist'=>"ktg93(\$m1)*\$m1", # KTG93 (all masses)
		#'probdist'=>"Kroupa2001(\$m1)*\$m1",
		#'probdist'=>'powerlaw(0.8,100.0,-2.35,$m1)*$m1', # salpeter (0.8-100)
		'probdist'=>'powerlaw(0.1,100.0,-2.35,$m1)*$m1', # salpeter (0.1-100)
		'dphasevol'=>'$dlnm1',
	    };
	}
	


	# Binary stars: Mass 2 and Separation
	if(defined($qbeta))
	{
	    # use a q powerlaw with slope $qbeta
	    $binary_grid::grid_options{'flexigrid'}{'grid variable '.$nvar++}=
	    {
		'condition'=>'$binary_grid::grid_options{binary}==1',
		'name'=>'q',
		'longname'=>'Mass ratio',
		'range'=>[$m2min.'/$m1',1.0],
		'resolution'=>$nm2,
		'spacingfunc'=>'const('.$m2min.'/$m1,1.0,'.$nm2.')',
		'probdist'=>"powerlaw(0.0,1.0,$qbeta,\$q)",
		'precode'=>'$m2=$q*$m1;',
		'dphasevol'=>'$dq',
	    };
	}
	else
	{
	    print STDERR "Mass ratio distribution unknown\n";
	    print "Mass ratio distribution unknown\n";
	    exit;
	}

	
	if($period_distribution eq 'DM91')
	{
	    # old code
	    $binary_grid::grid_options{'flexigrid'}{'grid variable '.$nvar++}=
	    {  
		'name'=>'logper',
		'longname'=>'log(Orbital_Period)',
		'range'=>[$logpermin,$logpermax],
		'resolution',$nper,
		'spacingfunc',"const($logpermin,$logpermax,$nper)",
		'precode'=>"\$per=10.0**\$logper;\$sep=calc_sep_from_period(\$m1,\$m2,\$per)if(defined(\$m1)&&defined(\$m2));",
		'probdist'=>"gaussian(\$logper,$gauss_logmean,$gauss_sigma,$logpermin,$logpermax)",
		'dphasevol'=>'$dln10per'
	    }
	}
	elsif($period_distribution eq 'bastard')
	{
	    # period distribution : rob's bastard
	    $binary_grid::grid_options{'flexigrid'}{'grid variable '.$nvar++}=
	    {
		'name'=>'log10per',
		'longname'=>'log10(Orbital_Period)',
		'range'=>['-1.0','10.0'],
		'resolution',$nper,
		'spacingfunc',"const(-1.0,10.0,$nper)",
		'precode'=>"my \$per=10.0**\$log10per;my\$sep=calc_sep_from_period(\$m1,\$m2,\$per);",
		'probdist'=>'Izzard2012_period_distribution($per,$m1)',
		'dphasevol'=>'$dlog10per'
	    };
	}
	else
	{
	    print "Unknown period distribution $period_distribution\n";
	    exit;
	}


	if($useecc)
	{
	    # eccentricity
	    $binary_grid::grid_options{'flexigrid'}{'grid variable '.$nvar++}=
	    {
		'name'=>'ecc',
		'longname'=>'log10(Eccentricity)',
		'range'=>[0,0.999],
		'resolution',$necc,
		'spacingfunc',"const(0.0,0.999,$necc)",
		'precode','$eccentricity=$ecc;',
		# gaussian as in Meiborn & Mathieu 2005
		'probdist'=>'gaussian($ecc,0.38,0.23,0.0,0.999)',
		'dphasevol'=>'$decc'
	    };
	}



    }
    else
    {
	my $nvar=0; 
	
	# duplicity
	$binary_grid::grid_options{'flexigrid'}{'grid variable '.$nvar++}=
	{
	    'name'=>'duplicity',
	    'longname'=>'Duplicity',
	    'range'=>[0,1],
	    'resolution'=>1,
	    'spacingfunc'=>'number(1.0)',
	    'precode'=>'$binary_grid::grid_options{binary}=$duplicity;binary_grid::flexigrid_grid_resolution_shift();',
	    'gridtype'=>'edge',
	    'noprobdist'=>1,
	};
	
	
	# Mass 1
	{
	    my $precode='$m1=exp($lnm1);  $eccentricity=0.0; my $binary_fraction=MAX(0.0,MIN(1.0,distribution_functions::raghavan2010_binary_fraction($m1)));  $binary_grid::grid_options{weight}= $binary_grid::grid_options{binary}==0 ? 1.0-$binary_fraction : $binary_fraction;   ';

	    print "M1 precode = $precode\n";

	    $binary_grid::grid_options{'flexigrid'}{'grid variable '.$nvar++}=
	    {
		'name'=> 'lnm1', 
		'longname'=>'Primary mass', 
		'range'=>["log($m1min)","log($mmax)"],
		'resolution'=> "$nm1", # ignore single stars in approximate resolution calculation
		'spacingfunc'=>"const(log($m1min),log($mmax),\$binary_grid::grid_options{binary} ? $nm1 : $nm)",
		'precode'=>$precode,
		#'probdist'=>"ktg93(\$m1)*\$m1", # KTG93 (all masses)
		'probdist'=>"Kroupa2001(\$m1)*\$m1",
		#'probdist'=>'powerlaw(0.8,100.0,-2.35,$m1)*$m1', # salpeter (0.8-100)
		'dphasevol'=>'$dlnm1',
	    };
	}


	# Binary stars: Mass 2 and Separation
	if(defined($qbeta))
	{
	    # use a q powerlaw with slope $qbeta
	    $binary_grid::grid_options{'flexigrid'}{'grid variable '.$nvar++}=
	    {
		'condition'=>'$binary_grid::grid_options{binary}==1',
		'name'=>'q',
		'longname'=>'Mass ratio',
		'range'=>[$m2min.'/$m1',1.0],
		'resolution'=>$nm2,
		'spacingfunc'=>'const('.$m2min.'/$m1,1.0,'.$nm2.')',
		'probdist'=>"powerlaw(0.0,1.0,$qbeta,\$q)",
		'precode'=>'$m2=$q*$m1;',
		'dphasevol'=>'$dq',
	    };
	}
	else
	{
	    print STDERR "Mass ratio distribution unknown\n";
	    print "Mass ratio distribution unknown\n";
	    exit;
	}

	
	if($period_distribution eq 'DM91')
	{
	    # old code
	    $binary_grid::grid_options{'flexigrid'}{'grid variable '.$nvar++}=
	    {  
		'name'=>'logper',
		'longname'=>'log(Orbital_Period)',
		'range'=>[$logpermin,$logpermax],
		'resolution',$nper,
		'spacingfunc',"const($logpermin,$logpermax,$nper)",
		'precode'=>"\$per=10.0**\$logper;\$sep=calc_sep_from_period(\$m1,\$m2,\$per)if(defined(\$m1)&&defined(\$m2));",
		'probdist'=>"gaussian(\$logper,$gauss_logmean,$gauss_sigma,$logpermin,$logpermax)",
		'dphasevol'=>'$dln10per'
	    }
	}
	elsif($period_distribution eq 'bastard')
	{
	    # period distribution : rob's bastard
	    $binary_grid::grid_options{'flexigrid'}{'grid variable '.$nvar++}=
	    {
		'name'=>'log10per',
		'longname'=>'log10(Orbital_Period)',
		'range'=>['-1.0','10.0'],
		'resolution',$nper,
		'spacingfunc',"const(-1.0,10.0,$nper)",
		'precode'=>"my \$per=10.0**\$log10per;my\$sep=calc_sep_from_period(\$m1,\$m2,\$per);",
		'probdist'=>'Izzard2012_period_distribution($per,$m1)',
		'dphasevol'=>'$dlog10per'
	    };
	}
	else
	{
	    print "Unknown period distribution $period_distribution\n";
	    exit;
	}


	if($useecc)
	{
	    # eccentricity
	    $binary_grid::grid_options{'flexigrid'}{'grid variable '.$nvar++}=
	    {
		'name'=>'ecc',
		'longname'=>'log10(Eccentricity)',
		'range'=>[0,0.999],
		'resolution',$necc,
		'spacingfunc',"const(0.0,0.999,$necc)",
		'precode','$eccentricity=$ecc;',
		# gaussian as in Meiborn & Mathieu 2005
		'probdist'=>'gaussian($ecc,0.38,0.23,0.0,0.999)',
		'dphasevol'=>'$decc'
	    };
	}
    }
    printf "Flexigrid modulo = %d, offset = %d\n",
    $binary_grid::grid_options{modulo},
    $binary_grid::grid_options{offset};

    binary_grid::parse_args();
    secondary_variables();
    setup_binary_grid();

}

############################################################


sub parse_bse
{
    # this gets called by the binary_grid module, it parses the 
    # lines of output from binary_c/nucsyn
    my $brk=0; 

    # you should set data in the $h hash pointer, which is 
    # then passed to the join_thread function (below) for you
    # to add up. (Note: $h is local to each thread.)
    my $h=shift;
    
#    my $m = $binary_grid::grid_options{progenitor_hash}{m};

    my $m = $binary_grid::grid_options{progenitor_hash}{m1}+
	$binary_grid::grid_options{progenitor_hash}{m2};

    my $p = $binary_grid::grid_options{progenitor_hash}{prob};
#    print "PARSE with M=$m p=$p\n";

    # ignore all the output
    while($brk==0)
    {
	$_=tbse_line();
	chomp;
	$brk=1 if($_ eq 'fin');
    }

    $$h{mass} += $m * $p;

    return $h;
}




sub secondary_variables
{
    # from the defaults and/or passed in variables,
    # calculate useful things

    # use colour?
    if($binary_grid::grid_options{'colour'}==1)
    {
        # use ANSI color for output to xterms/rxvt etc.
        use Term::ANSIColor;
        # set to white on black
        print color("white on_black");
    }
    else
    {
        # dummy function to replace the ANSI color() function
        eval("sub\ color\{ \}");
    }
}
