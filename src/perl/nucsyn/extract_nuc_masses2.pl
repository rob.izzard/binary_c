#!/usr/bin/env perl
use strict;
use binary_grid;

# script to extract nuclear masses in AMU 

# location of data
my $file="./src/perl/nucsyn/mass.mas12";

# atomic mass unit in eV and grams
my $amuev=931.478*1000.0;
my $amugrams=1.660538921e-24;

# first, get appropriate elements from nucsyn_isotopes.h
$binary_grid::grid_options{srcpath} = $ENV{PWD};
my %isotope_hash;

open(my $fp,'<','./src/nucsyn/nucsyn_isotope_list.h')||die;
while(<$fp>)
{
    if(/\#define X(\D+)(\d+)/)
    {
        $isotope_hash{$1.$2}=1;
    }
}

############################################################

open(FP,'<'.$file)||die("cannot open input file $file\n");

while(my $l=<FP>)
{
    next if($l=~/^\#/); # skip comments
    next if($l=~/^\./); # skip comments
    chomp($l);
    #print $l,"\n";

    # mass in AMU, Z, A
    my $m_amu = ($l=~/^.{96}(.{16})/)[0];
    my $Z =     ($l=~/^.{11}(.{4})/)[0];
    my $A =     ($l=~/^.{15}(.{4})/)[0];
    #print "A=$A, Z=$Z, $m_amu\n";

    my $element = ($l=~/^.{20}(.{2})/)[0];
    map
    {
        s/[#\s]//g;
    }($m_amu,$A,$Z,$element);
    $m_amu *= 1e-6;
    my $isotope = $element.$A;

    #print "m(AMU)=$m_amu N=$A Z=$Z element=$element isotope=$isotope\n";

    if($isotope_hash{$isotope})       
    {
        # binary_c 2.0+ format
        my $X = 'X'.$isotope;
        print "

    SET_ISOTOPIC_PROPERTY(mnuc,$X,$m_amu*AMU_GRAMS);
    SET_ISOTOPIC_PROPERTY(atomic_number,$X,$Z);
    SET_ISOTOPIC_PROPERTY(nucleon_number,$X,$A);

";
    }
    else
    { 
        print STDERR "$isotope not in binary_c\n";
    } 
}

close FP;

exit;

############################################################

sub get_isotope_number
{
    my ($elem) = @_;
    my $i;
    my $tot;
    my @l=`grep $elem\\\  ab.txt`;
    if(!(@l))
    {
	@l=`grep $elem awm95.txt|gawk "{print \\\$1,-1}"`;
	for($i=0;$i<=$#l;$i++)
	{
	    $l[$i]=$l[$i]." ".(100.0*1.0/($#l));
	}
    }

    #print @l;
    $tot=0;
    map {
	if($_=~/\s*(\d+)(\D+)\s+(\S+)\s+(\S+)/)
	{
	    # 1: mass number
	    # 2: elem name (not needed because of the grep)
	    # 3: Z (not needed)
	    # 4: % abundance
	    #print "1:$1 2:$2 3:$3 4:$4\n";
	    $tot += ($4/100.0)*$1;
	}
    } @l;
    if($tot>0)
    {
	#print "Mean mass of $1 : $tot\n";
    }
    # convert to an integer
    $tot=int($tot+0.5);
    return($tot);
}
