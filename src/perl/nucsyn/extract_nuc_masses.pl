#!/usr/bin/perl
use strict;
# script to extract nuclear masses in AMU 

print "Use version 2 of this script\n";exit;

my $file="awm95.txt";

# atomic mass unit in eV and grams
my $amuev=931.478*1000.0;
my $amugrams=1.660538921e-24;

# first, get appropriate elements from nucsyn_isotopes.h

my $f="../../nucsyn/nucsyn_isotope_list.h";
open(FP,"<$f")||die("cannot open nucsyn isotopes file $f\n");
my $isokey;
my $noisonumkey;
while(my $l=<FP>)
{
    if($l=~/\#define X(\D+)(\d+)/)
    {
	# isotope : $1=element $2=mass number
	$isokey=$isokey." $1$2 ";
    }
    elsif($l=~/\#define X(\D+)/)
    {
	# isotope : $1=element, mass number unkown
	# IGNORE for now...
	my $elem=$1;
	if(!(($elem eq "TRACE")||($elem eq "n")))
	{
	    my $massnum=get_isotope_number($elem);
	    print "Element $elem found with no isotope number -> guess $massnum\n";
	    $isokey=$isokey." $elem$massnum ";
	    $noisonumkey=$noisonumkey." $elem ";
	}
    }
}
$isokey=$isokey." ";
$noisonumkey=$noisonumkey." ";

close FP;

############################################################

print STDERR "Isotopes defined in the code:\n",$isokey,"\n\n";

############################################################

open(FP,"<$file")||die("cannot open input file $file\n");

while(my $l=<FP>)
{
    chomp($l);$l=~s/^\s+//;
    # add extra whitespace after first 5 characters...
    $l=~s/(.....)/$1 /;

    my @l=split(/\s+/,$l);

    # list is : element, Z, mass excess

    # convert element to code form
    if($l[0]=~s/(\d+)(\D+)/$2$1/)
    {
        my $massnum=$1;
	my $elem=$2;
        
	if($isokey=~/$l[0] /)
	{
	    #print "Element $l[0] match \n";
	
	    # we have an element!
	    # calculate mass from massnum*AMU + mass excess
	    my $m=$massnum*$amuev+$l[2];
	    # this is in eV, convert to AMU
	    $m/=$amuev;
	    # convert to grams
	    $m*=$amugrams;

            #print "$l[0] has mass $m g and atomic number $l[1]\n";
            if(0)
            {
                # binary_c 1.2
                if($noisonumkey=~/$elem /)
                {
                    print "mnuc\[X$elem\]=$m; atomic_number\[X$elem\]=$l[1];\n";
                }
                else
                {
                    print "mnuc\[X$l[0]\]=$m; atomic_number\[X$l[0]\]=$l[1];\n";
                }
            }

            if(1)
            {
                # binary_c 2.0+
                my $X = 'X'.$l[0];
                my $m_amu = $m / (1.660538921e-24);
                print "
    SET_ISOTOPIC_PROPERTY(mnuc,$X,$m_amu*AMU_GRAMS);
    SET_ISOTOPIC_PROPERTY(atomic_number,$X,$l[1]);
    SET_ISOTOPIC_PROPERTY(nucleon_number,$X,$massnum);

";
            }
	}
	else
	{
	    #print "Element $l[0] not in code\n";
	}
    }
}

close FP;

exit;

############################################################

sub get_isotope_number
{
    my ($elem) = @_;
    my $i;
    my $tot;
    my @l=`grep $elem\\\  ab.txt`;
    if(!(@l))
    {
	@l=`grep $elem awm95.txt|gawk "{print \\\$1,-1}"`;
	for($i=0;$i<=$#l;$i++)
	{
	    $l[$i]=$l[$i]." ".(100.0*1.0/($#l));
	}
    }

    #print @l;
    $tot=0;
    map {
	if($_=~/\s*(\d+)(\D+)\s+(\S+)\s+(\S+)/)
	{
	    # 1: mass number
	    # 2: elem name (not needed because of the grep)
	    # 3: Z (not needed)
	    # 4: % abundance
	    #print "1:$1 2:$2 3:$3 4:$4\n";
	    $tot += ($4/100.0)*$1;
	}
    } @l;
    if($tot>0)
    {
	#print "Mean mass of $1 : $tot\n";
    }
    # convert to an integer
    $tot=int($tot+0.5);
    return($tot);
}
