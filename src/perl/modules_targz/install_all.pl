#!/usr/bin/perl
use strict;

# script to install Perl modules
# from .tar.gz files.
#
# Assumes you have cpanminus (cpanm) installed.
#
$|=1;

my $logfile = './cpanm.log';
foreach my $file
(qw/
rob_misc-0.17.tar.gz
binary_stars-0.05.tar.gz
cosmology-0.01.tar.gz
Binning-0.03.tar.gz
rinterpolate-1.5.tar.gz
Data-Serializer-RobJSON-0.05.tar.gz
Hash-RobMerge-0.14.tar.gz
Histogram-0.01.tar.gz
IMF-0.05.tar.gz
Maths_Double-0.01.tar.gz
RobInterpolation-0.04.tar.gz
robqueue-0.05.tar.gz
distribution_functions-0.06.tar.gz
spacing_functions-0.02.tar.gz
binary_grid-v2.2.0.tar.gz/)
{
    print "Installing $file ... ";
    my $cmd = "cpanm --notest -v $file 2>\&1 > $logfile";
    `$cmd`;
    my $r = $?;
    if($r != 0)
    {
        print "\n$cmd returned $r : this is an error, please check $logfile\n";
        exit;
    }
    print "done\n";
}
