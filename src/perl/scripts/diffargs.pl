#!/usr/bin/env perl
use strict;
use rob_misc;
use List::Uniq qw/uniq/;
use Term::ANSIColor;
#
# Diff sets of arguments, treating the first
# as the reference.
#

my @files = @ARGV;
my %args;

foreach my $file (@files)
{
    $args{$file} = parse($file);
}

my $ref = shift @files;
foreach my $file (@files)
{
    diff($ref,$file);
}

exit;

############################################################

sub parse
{
    my ($file) = @_;
    my $txt = slurp($file);
    my $arghash = {};
    map
    {
        s/^(\S+)\s+//;
        my $arg = $1;
        s/\s+$//;
        $arghash->{$arg} = $_; 
    }split(/--/,$txt);
    return $arghash;
}

sub diff
{
    # diff the args from $file with respect to $ref

    my ($ref, $file) = @_;
    my @k = uniq (
        sort keys %{$args{$ref}},
        sort keys %{$args{$file}}
        );
    foreach my $k (@k)
    {
        if(!defined $args{$file}->{$k})
        {
            printf "%s%s%s is in %s (set to %s%s%s) but is not in %s%s%s\n",
            color('bold red'),$k,color('reset'),
            color('bold green'),$ref,color('reset'),
            color('bold yellow'),$args{$ref}->{$k},color('reset'),
            color('bold cyan'),$file,color('reset');
        }
        elsif(!defined $args{$ref}->{$k})
        {
            printf "%s%s%s is in %s%s%s (set to %s%s%s) but not in %s%s%s\n",
            color('bold red'),$k,color('reset'),
            color('bold green'),$file,color('reset'),
            color('bold yellow'),$args{$file}->{$k},color('reset'),
            color('bold cyan'),$ref,color('reset');
        }
        elsif($args{$ref}->{$k} != $args{$file}->{$k} ||
              $args{$ref}->{$k} ne $args{$file}->{$k})
        {
            printf "%s%s%s is %s%s%s in %s%s%s, but %s%s%s in %s%s%s\n",
            color('bold red'),$k,color('reset'),
            color('bold green'),$args{$ref}->{$k},color('reset'),color('yellow'),$ref,color('reset'),
            color('bold cyan'),$args{$file}->{$k},color('reset'),color('magenta'),$file,color('reset');
        }
    }
}
