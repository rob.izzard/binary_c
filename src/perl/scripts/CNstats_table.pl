#!/usr/bin/env perl
use strict;
use robqueue;
use threads::shared;
use Math::SigFigs;

# CN stats for the paper
my $sep = ' & ';
my $vertlinesep = ' | ';
my $newline = "\\tabularnewline \n";
my $hline = "\\hline\n";
my %results : shared;
my $Mlimit = $ENV{CN_PROJECT_MASS_LIMIT} // 1.3;
my $M = $Mlimit.'\,\mathrm{M}_{\odot}';
my $q = robqueue->new(
    subr=>\&process
    );
my @sets = model_set_list();

#@sets = ($sets[0]);#,$sets[1],$sets[2]);
#@sets=('B1');
print STDERR "Sets @sets\n";

foreach my $set (@sets)
{
    if($set =~ /^[^SB]/)
    {
        # mixed set
        if($set eq 'X')
        {
            # S1 + B1
            $q->q(['X','S1','B1']);
        }
        elsif($set eq 'Y')
        {
            # S6 + B21
            $q->q(['Y','S6','B21']);
        }
    }
    else
    {
        $q->q([$set,$set,undef]);
    }
}
$q->end();

make_table();

exit;

############################################################

sub make_table
{
    # make table
    my $table;
    my $macros;
    my @cols = (
        'of_giants extra_massive_giant|',
        'of_massive extra_massive_giant_C',
        'of_massive extra_massive_giant_N',
        'of_massive extra_massive_CEMP',
        'of_massive extra_massive_giant_single',
        'of_massive extra_massive_giant_binary',
        'of_massive extra_massive_was_BSS',
        'last:of_massive extra_massive_never_BSS',
        );
    my $C = '\mathrm{C}';
    my $N = '\mathrm{N}';
    my $Fe = '\mathrm{Fe}';
    my %label = (
#        of_giants => "\\multicolumn\{2\}\{l\}\{Of all giants\}",
        of_giants => "Of all giants",
        of_massive => "\\multicolumn\{2\}\{l\}\{Of giants with \$M>$M\$\}",
        
        extra_massive_giant => "\$M>$M\$",
        extra_massive_giant_C => "\$\\left[$C/$N\\right]\\geq 0\$",
        extra_massive_giant_N => "\$\\left[$C/$N\\right]< 0\$",
        extra_massive_CEMP => "\$\\left[$C/$N\\right]> 0.5\$",

        extra_massive_giant_single => "Single",
        extra_massive_giant_binary => "Binary",
        extra_massive_was_BSS => "was BSS",
        extra_massive_never_BSS => "never BSS",
        );
    my %colsep = 
        map{
            $_ => s/\|$// ? $vertlinesep : '';
    }@cols;

    my $twixt = ' \\\\ ';
    map
    {
        if($label{$_} =~s!\:\:\:!$twixt!g)
        {
            $label{$_} = '\shortstack{'.$label{$_}.'}';
        }
    }keys %label;
    
    $table .=  "\\begin\{tabular\}\{";
    foreach my $col ('Model set',@cols)
    {
        $table .=  'c'.$colsep{$col};
    }
    $table .=  "\}\n";
    
    my $prev='';
    my $skipc=0;
    $table .=  $hline;
    foreach my $col ('Model set',@cols)
    {
        my ($hash,$item) = split(' ',$col);
        my $lst = (($hash=~s/^last\://)[0]) ? 1 : 0;
        $item = defined $label{$hash} ? $label{$hash} : $hash ;
        $skipc--;
        if($hash ne $prev && $skipc<=0)
        {
            $prev = $hash;
            $item =~s/_(?=[a-zA-Z])//g;
            $table .=  ' '.$item.' ';
            $skipc = ($item=~/multicolumn\{(\d+)\}/)[0];
        }
        $table .=  " $sep " unless($lst || $skipc>0);
    }
    $table .=  $newline;

    foreach my $col ('Dataset',@cols)
    {
        my ($hash,$item) = split(' ',$col);
        my $lst = (($hash=~s/^last\://)[0]) ? 1 : 0;
        $item = defined $label{$item} ? $label{$item} : $item ;
        $item =~s/_(?=[a-zA-Z])//g;
        $table .=  $item.' ';
        $table .=  $sep unless($lst);
    }
    $table .=  $newline.$hline;
        
    foreach my $set (@sets)
    {
        my $data = $results{$set};

        # extract
        my %data;
        $data=~s/Extra-massive(.*)//s;
        $data{of_massive} = $1;
        $data=~s/Counts relative to ntot = \S+(.*)//s;
        $data{of_giants} = $1;
        
        #print "OF GIANTS $data{of_giants}\n";
        #print "OF MASSIVE $data{of_massive}\n";

        $table .=  $set.$sep;
        my $count=0;
        foreach my $col (@cols)
        {
            $count++;
            my ($hash,$item) = split(' ',$col);
            my $lst = (($hash=~s/^last\://)[0]) ? 1 : 0;
            my $s = ($data{$hash}=~/$item\s+\:\s+\S+\s+\:\s+(\S+)/)[0];
            my $x = format_num($s);

           
            
            #print $x;
            my $macro = 'data'.$set.'.'.$count;
            
            $macros .= "\\expandafter\\newcommand\\csname $macro\\endcsname\{\\ensuremath\{$x\}\}\n";
            $table .= "\\csname $macro\\endcsname ";
            $table .= $sep unless($lst);
        }
        $table .= $newline;
    
        {
            # special case : algol macros
            my $x = format_num(($results{$set}=~/low_mass_algol\s+\:\s+\S+\s+\:\s+(\S+)/)[0]);
            my $macro = 'data'.$set.'.algol';
            $macros .= "\\expandafter\\newcommand\\csname $macro\\endcsname\{\\ensuremath\{$x\}\}\n";
        }
    }
    $table .= "$hline\n\\end\{tabular\}\n";

    print $macros,"\n";
    print $table,"\n";

}

sub process
{
    my ($label,$set1,$set2) = @{$_[0]};
    print STDERR "Process set $label (set1=$set1, set2=$set2, Mlimit=$Mlimit)\n";
    my $cmd = "../../src/perl/scripts/CNstats2.pl $set1 $set2";
    print STDERR "Run : \n$cmd\n";
    $results{$label} = `$cmd 2>/dev/null`;
}

sub model_set_list
{
    my @list;
    foreach (1..6)
    {
        push (@list, 'S'.$_);
    }
    foreach (1..21)
    {
        push (@list, 'B'.$_);
    }
    push(@list,'X','Y');
    return @list;
}

sub format_num
{
    my $s = shift;
    my $x;
    if($s==0.0)
    {
        $x = '0';
    }
    else
    { 
        $x = FormatSigFigs($s,2);
        $x=~s/\s+$//;
        $x=~s/\.$//;
        $x .= '\,\%';
    }
    return $x;
}
