#!/usr/bin/env perl
use strict;
use rob_misc;

############################################################
# script run multiple CN grids for different physics
#
# Currently for version 9 without condor
#
############################################################
my $version = $ENV{CN_PROJECT_VERSION} // 9;
print "CN VERSION $version\n";

# default parameters
my $resolution = shift @ARGV // 'low'; # low, mid, hi, vhi

my %defaults=(
    binary=>0,

    nres=>
    $resolution eq 'low' ? '$opts{binary}==1 ? 10 : 100' : # low resolution
    $resolution eq 'mid' ? '$opts{binary}==1 ? 40 : 1000' : # medium resolution
    $resolution eq 'mid2' ? '$opts{binary}==1 ? 40 : 1000' : # medium resolution
    $resolution eq 'hi' ? '$opts{binary}==1 ? 100 : 10000' : # high resolution
    $resolution eq 'vhi' ? '$opts{binary}==1 ? 171 : 100000' : # very high resolution
    undef, # will cause failure

    alpha_ce=>0.2, # 0.2
    metallicity=>0.008, # 0.004
    WRLOF_method=>1, # 1
    RLOF_method=>3, # 3 
    CRAP_parameter=>0, # 0 
    rlof_gamma=>-2, # -2
    qcrit_MS=>1.6, # 1.6 (Claeys et al 2014)
    persep_distribution=>'Izzard2012', # Opik or Izzard2012

    # experiment with maximum timestep
    maximum_nuclear_burning_timestep=>0.1,
    maximum_timestep=>100,

    # not for variation
    vb=>1, # 1
    log_filename=>'"/dev/null"', # must be in '"<string>"' to eval correctly
    );

# chose start and end ages depending on metallicity
# to avoid any single stars being in the >1.2Msun range
$defaults{thick_disc_start_age} = '10e3';
$defaults{thick_disc_end_age} = 
    $defaults{metallicity}==0.004 ? '4e3' : 
    $defaults{metallicity}==0.008 ? '5e3' : 
    '4e3';

my $nicelevel = 10; # how nice should we be? +19a

# script name
my $script = "./src/perl/scripts-flexigrid/grid-CND$version.pl"; 

my $overwrite = 0; # set to 1 to rerun grids that have already been run

# directory in which to store final output
my $outdir = $ENV{HOME}."/progs/stars/binary_c/CNgrids$version/CNgrids-$resolution".'res';

# condor control space
my $condor_dir = $ENV{HOME}."/data/condor_control/CNgrids$version/CNgrids-$resolution".'res';
my $condor_command = undef;
my $condor_resubmit_running = 0;
my $condor_resubmit_submitted = 0;
my $condor_resubmit_finished = 0;
my $condor_resubmit_crashed = 0;

my $dir_delimiter = ','; # delimiter on directory names

# do not use condor by default
my $condor=0;
my $condor_njobs=50;

# cmd line overrides
foreach my $arg (@ARGV)
{
    if($arg=~/(\S+)=(.*)/)
    {
	eval "\$$1 = \"$2\"";
    }
}

if(!defined $defaults{nres})
{
    print "Resolution undefined!\n";exit;
}

############################################################

# default grids
if(1)
{
    run(binary=>0);
    run(binary=>1);
    #run(binary=>1,qslope=>0.0);
    #run(binary=>1,qslope=>0.5);
    #run(binary=>1,qslope=>1.0);
    #run(binary=>1,qslope=>0.0);
    #run(binary=>1,qslope=>0.5);
}

if(0)
{
    run(thick_disc_start_age=>'13e3',thick_disc_end_age=>'8e3',binary=>0);
    run(thick_disc_start_age=>'13e3',thick_disc_end_age=>'6e3',binary=>0);
    run(thick_disc_start_age=>'13e3',thick_disc_end_age=>'12e3',binary=>0);
    run(thick_disc_start_age=>'12e3',thick_disc_end_age=>'10e3',binary=>0);
    run(thick_disc_start_age=>'10e3',thick_disc_end_age=>'8e3',binary=>0);
    run(thick_disc_start_age=>'8e3',thick_disc_end_age=>'6e3',binary=>0);
    run(thick_disc_start_age=>'6e3',thick_disc_end_age=>'4e3',binary=>0);
    run(thick_disc_start_age=>'4e3',thick_disc_end_age=>'2e3',binary=>0);
    run(thick_disc_start_age=>'10e3',thick_disc_end_age=>'5e3',binary=>0);
    run(thick_disc_start_age=>'13e3',thick_disc_end_age=>'5e3',binary=>0);
}


if(0)
{
# test metallicities
    foreach my $Z (1e-4,0.001,0.004,0.02)
    {
        run(metallicity=>$Z,binary=>0);
        run(metallicity=>$Z,binary=>1);
    }

# resolution tests
    foreach my $nres (20,40,65,100)
    {
        run(nres=>$nres,binary=>1);
    }
    foreach my $nres (100,1000,10000)
    {
        run(nres=>$nres,binary=>0);
    }

# test qcrit_MS
    foreach my $qcrit_MS (1.6,1.8,3)
    {
        run(qcrit_MS=>$qcrit_MS,binary=>1);
    }

# test alpha_CE
    foreach my $alpha_ce (0.2,0.5,1.0)
    {
        run(alpha_ce=>$alpha_ce,binary=>1);
    }

# test WRLOF_method
    foreach my $WRLOF_method (0,1,2)
    {
        run(WRLOF_method=>$WRLOF_method,binary=>1);
    }

    foreach my $RLOF_method (0,1,3)
    {
        run(RLOF_method=>$RLOF_method,binary=>1);
    }
    
# test CRAP parameter
    foreach my $CRAP_parameter (0,1e3,1e4)
    {
        run(CRAP_parameter=>$CRAP_parameter,binary=>1);
    }

# test angmom loss
    foreach my $RLOF_gamma (-2,-1,0,1,2)
    {
        run(rlof_gamma=>$RLOF_gamma,binary=>1);
    }

# test per/sep distribution
    foreach my $persep_distribution ('Opik','Izzard2012')
    {
        run(persep_distribution=>$persep_distribution,binary=>1);
    }

# test alternative times for thick disc SFR
    foreach my $binary (0,1)
    {
        run(thick_disc_start_age=>'1.3e4',
            thick_disc_end_age=>'8e3',
            binary=>$binary);
    }

# restrict logg
    foreach my $binary (0,1)
    {
        run(thick_disc_logg_min=>2.0,
            thick_disc_logg_max=>3.0,
            binary=>$binary);
    }
}

exit;

############################################################
sub run
{
    my (%opts) = @_;

    mkdirhier($outdir);

    # set opts to default if not passed in
    map
    {
        $opts{$_} //= $defaults{$_};
    }keys %defaults;

    # make arg string and output directory
    my $args;
    my $stub = '/CN-';
    map
    {

        if($opts{binary}==1 ||
           # skip some args in single stars
           ($_ !~ /rlof/i && 
            $_ ne 'persep_distribution' &&
            $_ ne 'alpha_ce' &&
            $_ ne 'CRAP_parameter' && 
            $_ !~ /qcrit/i 
           ))
        {

            $args .= $_.'='.(eval $opts{$_} // $opts{$_}).' ';
            
            if($_ eq 'metallicity')
            {
                # do not compact metallicity 
                $stub .= 'Z'.$opts{$_}.$dir_delimiter;
            }
            elsif($_ eq 'persep_distribution')
            {
                # non-Opik persep requires a label
                $stub .= 'persep_distribution='.$opts{$_};
            }
            # some args should not be in the output directory
            elsif($_ ne 'vb' && 
                  $_ ne 'log_filename' && 
                  $_ ne 'save_args' && 
                  $_ ne 'binary' &&
                  $_ !~ /timestep/ # ignore timestep info
                )
            {
                $stub .= $_.compactnumber(eval $opts{$_}).$dir_delimiter;
            }
        }
    }sort{
        $a eq 'binary' ? -1 : $b eq 'binary' ? 1 : $a cmp $b
    } keys %opts;

    # postpend duplicity status
    $stub .= $opts{binary} ? 'binary' : 'single';

    # shorten directory name if possible
    $stub =~s/persep_distribution/padist/;
    $stub =~s/RLOF_method/RLOF/;
    $stub =~s/WRLOF_method/WRLOF/;
    $stub =~s/CRAP_parameter/CRAP/;
    $stub =~s/thick_disc_end_age/TDend/;
    $stub =~s/thick_disc_start_age/TDstart/;
    $stub =~s/nres/N/;

    #  hence the output dir
    my $od = $outdir.$stub;
    
    # make the command to be executed
    my $cmd = "nice -n +$nicelevel $script $args outdir=$od condor=$condor ";
    if($condor)
    {
	$cmd .= " condor_njobs=$condor_njobs condor_dir=$condor_dir$stub ";
	if(defined $condor_command)
	{
	    # use condor_command=join_datafiles if required
	    $cmd .= " condor_command=$condor_command ";
	}
	# append resubmit status
	$cmd .= " condor_resubmit_running=$condor_resubmit_running condor_resubmit_submitted=$condor_resubmit_submitted condor_resubmit_finished=$condor_resubmit_finished condor_resubmit_crashed=$condor_resubmit_crashed ";
    }
    
    print $cmd,"\n";
        
    # but only execute if we haven't already, and the data hasn't been
    # made, or we force the overwrite
    `ls -ld $od 2>/dev/null`; # NFS sync attempt
    if(!$overwrite && -d $od && -f "$od/stats" && -s "$od/stats")
    {
        print "Directory $od already exists (",`ls -ld $od`,"): if you want to rerun it, remove the directory or set \$overwrite=1\n"; 
    }
    else
    {
        # execute
        mkdirhier($od);
	my $stdout;
	if(defined $condor_command)
	{
	    $stdout = "|tee $od/CN$version.out";
	}
	else
	{
	    $stdout = ">$od/CN$version.out";
	}
        $cmd .= " 2>$od/CN$version.err $stdout ";
        rungrid($cmd);
    }

}

sub rungrid
{
    # run grid command, send output to the screen
    my ($cmd) = @_;
    print "RUN $cmd\n";
    runcmd($cmd,'screen');
}

