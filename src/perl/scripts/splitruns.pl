#!/usr/bin/env perl
use strict;
my $n = 0;
while(<>)
{
    if(/SHOW_STARDATA_START/)
    {
        $n++;
        open(FP,'>',"/tmp/stardata$n.dat");
    }
    elsif(/SHOW_STARDATA_END/)
    {
        close FP;
    }
    else
    {
        print FP $_;
    }
}
