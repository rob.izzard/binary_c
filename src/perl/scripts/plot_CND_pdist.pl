#!/usr/bin/env perl
use strict;
use rob_misc qw/MIN MAX/;
use Data::Dumper;

#
# Extract the period distribution for a given 
# point in the [C/N] vs M plane
#
#
# Args are:
# 1 Data file
# 2 Mass 
# 3 [C/N]
#

my ($file,$M,$CN) = @ARGV;

my %coord = (
    M=>$M,
    CN=>$CN
    );

# these must match the simulation 
my %binwidth=(
    M=>0.1,
    CN=>0.1,
    logP=>0.1
    );

# check for correct args
if(!defined $file || !defined $M || !defined $CN)
{ 
    print "Args must be : file, M, [C/N]\n";
    exit;
}

# loop over the datafile
open(my $data,'<',$file) || die("cannot open datafile");

# first parse : get mass and period ranges, find nearest
my %range; 
my %nearest;
while(my $line = <$data>)
{
    next if($line=~/^#/);
    chomp($line);
    my @x = split(/\s+/,$line);
    next if($#x<3);

    # line data is in x
    my %x = (
        M=>$x[0],
        CN=>$x[1],
        logP=>$x[2],
        dtp=>$x[3]
        );

    # get data ranges
    foreach my $k ('M','CN','logP')
    {
        $range{$k}{min} = MIN($x{$k},$range{$k}{min});
        $range{$k}{max} = MAX($x{$k},$range{$k}{max});
    }
    
    # get nearest data
    foreach my $k ('M','CN')
    {
        if(!defined $nearest{$k})
        {
            $nearest{$k}{value} = $x{$k};
            $nearest{$k}{distance} = abs($coord{$k} - $x{$k});
        }
        else
        {
            my $d = abs($coord{$k} - $x{$k});
            if($d < $nearest{$k}{distance})
            {
                $nearest{$k}{value} = $x{$k};
                $nearest{$k}{distance} = $d;
            }
        }
    }
}

seek $data,0,0;
my %out;
while(my $line = <$data>)
{
    next if($line=~/^#/);
    chomp($line);
    my @x = split(/\s+/,$line);
    next if($#x<3);

    # line data is in x
    my %x = (
        M=>$x[0],
        CN=>$x[1],
        logP=>$x[2],
        dtp=>$x[3]
        );

    my $match = 1;
    foreach my $k ('M','CN')
    {
        #print "CF $x{$k} != $nearest{$k}{value}\n";
        if($x{$k} != $nearest{$k}{value})
        {
            $match=0;
            last;
        }
    }

    $out{sprintf '%g',$x{logP}} = $x{dtp};
}

close $data;

# output
my $logP = $range{logP}{min};
my $dlogP = $binwidth{logP};
    
while($logP < $range{logP}{max}+$dlogP*0.5)
{
    printf "%g %g\n",$logP, $out{$logP};
    $logP = sprintf'%g',($logP+$dlogP);
}
    
