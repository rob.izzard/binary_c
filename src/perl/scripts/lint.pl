#!/usr/bin/env perl
use strict;
use rob_misc;
use Regexp::Common;


#
# "Lint" script for binary_c
#


foreach my $file (`meson/c_sourcefiles.sh`)
{
    chomp $file;
    print "Check $file\n";

    my $source = slurp($file);

    # some files shouldn't be linted (e.g. external code)
    next if($source=~/^#define __BINARY_C_LINT_SKIP/);
    
    # remove comments
    $source =~ s/$RE{comment}{C}//g;
    
    # check that the first thing we do is include binary_c.h
    if($source !~ /^\s*\#include\s+\"(?:\.\.\/)?binary_c(?:_main)?.h\"/)
    {
        print "$file does not start with \#include \"binary_c.h\"\n";
        exit;
    }
}


print "\nDone\n";
