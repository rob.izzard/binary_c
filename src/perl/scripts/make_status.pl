#!/usr/bin/env perl
use strict;
use rob_misc;
use Regexp::Common qw/comment/;
#
# Script to make a logging function which 
# outputs the contents of the stardata struct
#

my $h = header_data();
$h=~s/libbinary_c_//g;

# extract the raw stardata struct
my $stardata = extract($h,'stardata_t');
#print $stardata;

# hierarchical parse it
my ($n,$stardata) = parse ($stardata,'stardata');
exit;

sub parse
{
    my ($data,$prefix) = @_;
    my $n = 0;

    # replace structs
    while($data=~/struct\s*(\S+)\s*(\*?)\s*(\S+?)(?:\[(\d+)\])?;/g)
    {
        my $type = $1;
        my $resolver = '->';#$2 eq '*' ? '->' : '.';
        my $name = $3;
        my $narray = $4;
        #print "Found $prefix$resolver$name of type $type, array size $narray\n";

        my $localprefix;
        my $struct_contents = extract($h,$type);
        if($narray)
        {
            # expand array
            for(my $i=0;$i<$narray;$i++)
            {
                $localprefix = $prefix.$resolver.$name.'['.$i.']';
                dumpvars($localprefix,$struct_contents);
            }
        }
        else
        {
            # expand struct
            $localprefix = $prefix.$resolver.$name;
            dumpvars($localprefix,$struct_contents);
        }
    }
    
#    print "PARSE $data\n";
    exit;
}

sub dumpvars
{
    my ($prefix, $contents) = @_;
    
    while($contents=~/(Boolean|long int|float|double|long double)\s*([^;]+);/gs)
    {
        my $type = $1;
        my @names = split(/,/,$2);
        map
        {
            s/^\s+//;
            s/\s+$//;
            s/\s+\*//;
            s/\*\s+//;
        }@names;
        #print "Found ",scalar @names," x $type named @names\n";
        foreach my $name (@names)
        {
            my @array = ($name=~/\[(\d+)\]/g);
            $name=~s/\[.*//;
            if($#array == -1)
            {
                # scalar
                output($type,
                       $prefix.'.'.$name);
            }
            else
            {
                # array
                my @counters = @array;
                map{$_--}@counters;
                $counters[$#counters]++;
                my $go = 1;
                while($go)
                {
                    my $n = $#counters;
                    $counters[$n]--;
                    while($counters[$n]==-1)
                    {
                        $counters[$n] = $array[$n]-1;
                        $counters[--$n]--;
                    }
                    if($n==-1)
                    {
                        $go = 0;
                    }
                    else
                    {
                        output($type,
                               $prefix.'.'.$name.'['.
                               ($#counters>0 ? join('][',@counters) : $counters[0])
                               .']');
                    }
                }
            }
        }
    }
}

sub output
{
    my ($type,$var) = @_;
    my $format = $var=~/\*/ ? '%p' : formatter($type);
    
    printf "printf(\"$var = %s\\n\", %s);\n",$format,$var;
}

sub formatter
{
    my ($type) = @_;
    return
        $type eq 'double' ? '%g' : 
        $type eq 'long double' ? '%g' :
        $type eq 'int' ? '%d' :
        $type eq 'long int' ? '%ld' :
        $type eq 'Boolean' ? '%d' : 
        '%p';
}

sub extract
{
    # extract struct with $name from $data
    my ($data,$type) = @_;
    #print "Extract $type\n";
    my $struct = ($data=~/struct\s*$type\s*\{([^\}]+)\}/s)[0];
    $struct =~ s/\s*\n/\n/g;
    # evalulate simple array counter constants
    $struct =~ s/\[([^\]]+)\]/'['.(eval $1).']'/eg;
    return $struct;
}


sub header_data
{
    my $h = `gcc -E src/binary_structures.h`;
    $h=~s/$RE{comment}{'C'}//g;
    $h=~s/$RE{comment}{'C++'}//g;
    $h=~s/\#.*//g;
    return $h;
}
