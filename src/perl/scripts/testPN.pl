#!/usr/bin/env perl

use binary_grid2;
use binary_grid::C;
use strict;
use warnings;
use Data::Dumper;
use POSIX qw/log10/;
use rob_misc qw/MAX MIN/;


my $modelfile = $ARGV[0] // '/home/izzard/svn/binary_c/branches/dkeller/flexigrid/grid_output/grid_denise_PN_table_CE_7Dec_binary=1_n=50_alpha=0.2_lambda=0.5_Z=0.008_grebe.dat';
my $metallicity = ($modelfile=~/Z=(0\.0\d+)/)[0];
my $m1; # set below
my $m2 = 0.7; # a mass that cannot possibly evolve

my $population = binary_grid2->new(
    metallicity=>$metallicity,
    parse_bse_function_pointer=>\&parse_data,
    vb=>2
    );
my $RSUN=6.9598e10;
my $MSUN=1.9891e33;
my $PI=3.141592653589793;
my $PIFOURSQ=39.47841760435742848212;
my $G=6.67408E-8;
my $SECDY=86400.0;
    
my $results = {};
$population->tbse_launch();
$population->set(
    max_evolution_time=>14000,
    jorb_loss=>0,
    lw=>1.0,
    WRLOF_method=>1,
    timestep_modulator=>0.1,
    save_args=>1,
    sort_args=>1,
    );


    
open(my $models, '<', $modelfile) || die("can't open $modelfile");
my $count=0;

while(my $l = <$models>)
{
    next if($l=~/^#/ || $l eq "\n" || $l eq '');
    #print "\n\n\n\nSTAR $count\n\n",$l;
    next if($count++!=635);
    #$count++;
    
    my ( # numbers are 1 more than they actually are
        $modelnumber_CE,                # 1
        $n,
        $CE_PN_model_time,
        $CE_PN_probability,
        $CE_PN_separation,              # 5
        $CE_PN_finalsep,                   
        $CE_PN_star1_stellartype_was,
        $CE_PN_star1_initmass,
        $CE_PN_star1_mass,
        $CE_PN_star1_coremass,          #10
        $CE_PN_star1_teff,                 
        $CE_PN_star1_lum,
        $CE_PN_star2_stellartype_was,
        $CE_PN_star2_initmass,
        $CE_PN_star2_mass,              #15       
        $CE_PN_star2_coremass,
        $CE_PN_star2_teff,
        $CE_PN_star2_lum,
        $CE_PN_CE_counter,
        $CE_PN_Nsurf_H,                 #20
        $CE_PN_Nsurf_He,
        $CE_PN_Nsurf_C,
        $CE_PN_Nsurf_N,                         
        $CE_PN_Nsurf_O,
        $CE_PN_Nsurf_Ne,                #25
        $CE_PN_Xsurf_H,
        $CE_PN_Xsurf_He,
        $CE_PN_Xsurf_C,
        $CE_PN_Xsurf_N,                         
        $CE_PN_Xsurf_O,                 #30
        $CE_PN_Xsurf_Ne,
        $CE_PN_obstime_LS_1, 
        $CE_PN_transtime_1,
        $CE_PN_number_LS_1,
        $CE_PN_obstime_LS_2,            #35
        $CE_PN_transtime_2,
        $CE_PN_number_LS_2) = split(/\s+/,$l);



    my $Pdays_init = sep_to_per($CE_PN_separation,
                                $CE_PN_star1_initmass,
                                $CE_PN_star2_initmass);

    #(($CE_PN_separation*$RSUN)**3 * $PIFOURSQ/($G*$MSUN*($CE_PN_star1_initmass+$CE_PN_star2_initmass)))**0.5/$SECDY; # initial period in days
    
    my $Pdays_final = sep_to_per($CE_PN_finalsep,
                                $CE_PN_star1_initmass,
                                $CE_PN_star2_mass);
    #(($CE_PN_finalsep*$RSUN)**3 * $PIFOURSQ/($G*$MSUN*($CE_PN_star1_mass+$CE_PN_star2_mass)))**0.5/$SECDY; # final period in days

    print "FINAL PER $Pdays_final d from sep = $CE_PN_finalsep\n";
    
    #if($Pdays_final > 1e3)
    {
        printf "Run star with M1=%g M2=%g P=%g Z=%g\n",
        $CE_PN_star1_initmass,
        $CE_PN_star2_initmass,
        $Pdays_init,
        $metallicity;

        # run with two sets of parameters
        my @params = (
            {
                alpha_ce=>1,
                lambda_ce=>0.5,
                lambda_ionisation=>0.1,
                lambda_enthalpy=>0.1,
            },
            {
                alpha_ce=>1,
                lambda_ce=>-1,
                lambda_ionisation=>0.1,
                lambda_enthalpy=>0.1,
            },
            {
                alpha_ce=>1,
                lambda_ce=>-2,
                lambda_ionisation=>0.1,
                lambda_enthalpy=>0,
            }
            );

        # initial conditions

        foreach my $metallicity (0.008,0.02)
        {
            my %afinal;
            my %pfinal;
            foreach my $paramset (@params)
            {
                my $system = { 
                    'M_1'=>$CE_PN_star1_initmass,
                    'M_2'=>$CE_PN_star2_initmass,
                    'orbital_period'=>$Pdays_init,
                    'metallicity'=>$metallicity,
                    'eccentricity'=>0.0,
                    'probability'=>1,
                    'phasevol'=>1.0,
                    'separation'=>0.0,
                    'max_evolution_time'=>14000,
                    %$paramset 
                };
                my $r = run_denise($system);
                #print $r;
                map
                {
                    print $_,"\n";
                }grep {/PN/i || /comenv/i || /lam/i} split(/\n/,$r);

                # extract comenv data
                $l = ($r=~/DENISE_COMENV_PN (.*)/)[0];
                if($l)
                {
                    $l=~s/^\s+//;
                    $l=~s/\s+$//;
                    my (# NB numbers are wrong 
                        
                        $newCE_PN_model_time,
                        $x,
                        $newCE_PN_probability,
                        $newCE_PN_separation,              # 5
                        $newCE_PN_finalsep,                   
                        $newCE_PN_star1_stellartype_was,
                        $newCE_PN_star1_initmass,
                        $newCE_PN_star1_mass,
                        $newCE_PN_star1_coremass,          #10
                        $newCE_PN_star1_teff,                 
                        $newCE_PN_star1_lum,
                        $newCE_PN_star2_stellartype_was,
                        $newCE_PN_star2_initmass,
                        $newCE_PN_star2_mass,              #15       
                        $newCE_PN_star2_coremass,
                        $newCE_PN_star2_teff,
                        $newCE_PN_star2_lum,
                        $newCE_PN_CE_counter,
                        $newCE_PN_Nsurf_H,                 #20
                        $newCE_PN_Nsurf_He,
                        $newCE_PN_Nsurf_C,
                        $newCE_PN_Nsurf_N,                         
                        $newCE_PN_Nsurf_O,
                        $newCE_PN_Nsurf_Ne,                #25
                        $newCE_PN_Xsurf_H,
                        $newCE_PN_Xsurf_He,
                        $newCE_PN_Xsurf_C,
                        $newCE_PN_Xsurf_N,                         
                        $newCE_PN_Xsurf_O,                 #30
                        $newCE_PN_Xsurf_Ne,
                        $newCE_PN_obstime_LS_1, 
                        $newCE_PN_transtime_1,
                        $newCE_PN_number_LS_1,
                        $newCE_PN_obstime_LS_2,            #35
                        $newCE_PN_transtime_2,
                        $newCE_PN_number_LS_2) = split(/\s+/,$l);

                    my $P = sep_to_per($newCE_PN_separation,
                                       $newCE_PN_star1_mass,
                                       $newCE_PN_star2_mass);
                    
                    print "Found period $newCE_PN_separation $P\n";

                    $afinal{$paramset->{lambda_ce}} = $newCE_PN_separation;
                    $pfinal{$paramset->{lambda_ce}} = $P;
                    
                    #$population->set(%$paramset);
                    #$population->{_results} = {};
                    #my $r = $population->run_system(
                    #    {
                    #       'M_1'=>$CE_PN_star1_initmass,
                    #      'M_2'=>$CE_PN_star2_initmass,
                    #     'orbital_period'=>$Pdays_init,
                    #    'metallicity'=>$metallicity,
                    #   'eccentricity'=>0.0,
                    #  'probability'=>1,
                    # 'phasevol'=>1.0,
                    #      'separation'=>0.0,
                    #     'max_evolution_time'=>14000,
                    # }
                    #);

                    #$pfinal{$paramset->{lambda_ce}} = $population->{_results}->{final_period}; 
                    #$pfinal{argstring} = $population->{_grid_options}->{args};
                }
            }

            printf "PNCE % 5d Z=%g : ",$count,$metallicity;
            if(!$pfinal{-1} && !$pfinal{0.5})
            {
                printf "no comenv\n",$metallicity;
            }
            else
            {
                if($pfinal{-1} && !$pfinal{0.5})
                {
                    printf "-1 gives %g while 0.5 merges\n",$pfinal{-1};
                }
                elsif($pfinal{0.5} && !$pfinal{-1})
                {
                    printf "0.5 gives %g while -1 merges\n",$pfinal{0.5};
                }
                else
                {
                    printf "-1 gives %g (a=%g), 0.5 gives %g (a=%g), ratio %5.2f %%\n",
                    $pfinal{-1},$afinal{-1},
                    $pfinal{0.5},$pfinal{0.5},
                    $pfinal{-1}/$pfinal{0.5} * 100.0;

                    #     if($pfinal{-1} == $pfinal{0.5})
                    #     {
                    #         print "FAILED : ",$pfinal{argstring},"\n\n\n";

                    #         my $a = $pfinal{argstring};

                    #         $a=~s/--qcrit\S+ \S+//g;
                    #         $a=~s/--internal_buffering\S* \S+//g;
                    #         $a=~s/--CRAP_parameter/--bb/;
                    #         $a=~s/--Bondi_Hoyle_accretion_factor/--acc2/;
                    #         $a=~s/--thick_disc\S+ \S+//g;
                    #         $a=~s/--lithium\S+ \S+//g;
                    #         $a=~s/--rlperi \S+//g;
                    #         $a=~s/--minimum_orbit\S+ \S+//g;
                    #         $a=~s/--minimum_sep\S+ \S+//g;
                    #         $a=~s/--evolution_split\S+ \S+//g;
                    #         $a=~s/--pre_main\S+ \S+//g;
                    #         $a=~s/--RLOF_interpolation_method \S+//g;
                    
                    #         print "\n$a\n";
                    #         exit;
                    #     }
                }
            }
        }        
        #        foreach my $lambda (sort {$a<=>$b} keys %pfinal)
        ##       {
        #         print "Lambda = $lambda -> $pfinal{$lambda}\n";
        #    }
        #exit;        
    }
}



sub fading_time
{
    my $mc = shift;
    return $mc < 0.45 ? 0.0 : 10.0 ** (2.947 + 0.239 / ( 0.356 + log10($mc)));
}

sub parse_data
{
    my ($population,$results) = @_;
    my $pre;
    
    while(1)
    {
        my $la = $population->tbse_line();
        print "PARSE LINE ",join(' ',@$la),"\n";
        my $header = shift @$la;
        last if($header !~/COMENV/ || $header eq 'fin');
        my $l = "@$la\n";
        print "$l\n";
        if($header eq 'PRE_COMENV')
        {
            $pre = ($l=~/PER=(\S+)/)[0];
        }
        elsif($header eq 'POST_COMENV')
        {
            my $p = ($l=~/PER=(\S+)/)[0];
            my $m = ($l=~/M1=(\S+)/)[0];
            my $mc = ($l=~/Mc1=(\S+)/)[0];
            my $menv = $m - $mc;
            my $tfade = fading_time($mc);
            my $tdyn = dynamical_time($metallicity);
            my $ttrans = 1e4;
            my $tPN = MIN(MAX($tdyn-$ttrans,0.0),$tfade);
            my $st = ($l=~/st1=(\d+)/)[0];
            printf "post-comenv P=%g (%g) st=%d mc=%g menv=%g : tfade = %g tdyn = %g ttrans = %g : tPN = %g\n",
            $p,$p/$pre,$st,$mc,$menv,$tfade,$tdyn,$ttrans,$tPN;
            
            #printf "PLOT %g %g %g min(%g - %g, %g) menv=%g mc=%g\n",$m1,$logP,$tPN,
            #$tdyn,$ttrans,$tfade,$menv,$mc;

            # save the final period
            $population->{_results}->{final_period} = $p;
        }
    }

}

sub dynamical_time
{
    my $Z = shift;
    return $Z == 0.02 ? 130000 : 35000;
}

sub run_denise
{
    my $system = shift;
    my $args;

    foreach my $x ('M_1','M_2','orbital_period','metallicity','max_evolution_time')
    {
        my $y = delete $system->{$x};
        $args.=' --'.$x.' '.$y;
    }

    # add default physics
    $args .= " --bb 0 --comenv_prescription 0  --nelemans_n_comenvs 1 --yields_dt 100000     --nelemans_max_frac_j_change 1 --comenv_ns_accretion_fraction 0    --mass_accretion_for_eld 0.15 --wd_kick_pulse_number 0  --tpagb_reimers_eta 1 --comenv_ms_accretion_mass 0 --log_filename /dev/null --vw93_mira_shift 0 --comenv_ns_accretion_mass 0 --escape_velocity 1000000000 --minimum_envelope_mass_for_third_dredgeup 0.5 --hachisu_disk_wind 0    --monte_carlo_kicks   --superwind_mira_switchon 500  --nelemans_minq 0   --lambda_ionisation 0.1 --RLOF_mdot_factor 1 --lambda_ce -1   --rotationally_enhanced_mass_loss 0 --lambda_min 0  --vw93_multiplier 1 --wd_kick_direction 0  --phasevol 1 --acc2 1.5   --WRLOF_method 1  --jorb_loss 0  --wr_wind 0 --delta_mcmin 0 --alpha_ce 1 --sn_sigma 190     --tidal_strength_factor 1  --minimum_timestep 1e-06  --escape_fraction 0 --separation 0 --wd_sigma 0 --epsnov 0.001 --tpagbwind 0  --vrot2 0  --lw 1 --wr_wind_fac 1   --RLOF_method 0  --vrot1 0    --sn_kick_distribution 1 --hachisu_qcrit 1.15  --nelemans_gamma 1 --wd_kick_when 0  --gb_reimers_eta 0.5 --BH_prescription 0  --eddfac 1000000 --log_filename /dev/stdout ";        

    foreach my $x (keys %$system)
    {
        $args .= "--$x $system->{$x} ";
    }
    
    $args = "/home/izzard/svn/binary_c/branches/dkeller/flexigrid/binary_c ".$args;
    #$args = "/home/izzard/progs/stars/binary_c/binary_c ".$args;
    
    #print "RUN ",$args,"\n";
    return `$args`;
}


sub sep_to_per
{
    my ($sep,$m1,$m2) = @_; # solar units
    return (($sep*$RSUN)**3 * $PIFOURSQ/($G*$MSUN*($m1+$m2)))**0.5/$SECDY; # initial period in days
}
