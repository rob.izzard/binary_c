#!/usr/bin/env perl
use strict;
#use warnings;
############################################################
#
# Script to make C code to show the contents of a struct.
#
# Usage:
#
# make_showstruct.pl <header_file> <struct_name> <struct_location> [...options...]
#
# Where the header_file is a C header (.h) file that contains the
# struct named struct_name.
#
# struct_location is the root pointer to which we should access the struct
# data.
#
# ...options... are the remaining arguments in the form OPT=VAL
#
# You can set the following environment variables:
#
# SHOWSTRUCT_CC : changes the default compiler
# SHOWSTRUCT_INC : additional header include path
#
# (C) Robert Izzard 2017-2019
#
# NB This script should not use non-stanard external modules
#    because it will be called as part of the configure process.
#
#
# Exit code:
#
# 0 on success, 1 on failure. This is REQUIRED for the meson
# build process.
#
############################################################

my $args = "@ARGV";
my $header_file = shift @ARGV;
my $struct_name = shift @ARGV;
my $struct_location = shift @ARGV;
my $Ccode; # result

# $opts contains the various options, can be overwridden
# on the command line with opt=val
my $opts = {
    # C compiler: please use gcc
    'cc' => $ENV{SHOWSTRUCT_CC} // 'gcc',

        # command to extract the header file and replace all macros
        'header_extract' => ($ENV{SHOWSTRUCT_CC}//'gcc') .' '.
        ($ENV{SHOWSTRUCT_INC} // '') .
        ' -E 2>/dev/null ',
        # Recursion depth. 
        # 0 = infinite
        'depth' => '0', 

        # make struct diffs?
        'diff' => '0',
        
        # string used as a prefix on output
        'prefix'=>'STARDATA ',
        
        # expand_pointer_list is a list of pointers which should be expanded
        # if possible.
        #
        # This should be a comma-separated string.
        'expand_pointer_list' => '',

        # show pointer locations? 
        # for comparing data between runs, set this to 0
        'show_pointer_locations' => 0,

        # formats for variable types
        formats=>{
            'double' =>  '%g',
                'float' =>   '%g',
                'char*' =>   '%s',
                'char'  =>   '%s',
                'int'=>      '%d',
                'unsignedint'=>'%u',
                'size_t'=>'%u',
                'Boolean'=>  '%d',
                'boolean'=>  '%d',
                'bool'=>  '%d',
                '_Bool'=>  '%d',
                '_bool'=>  '%d',
                'longint'=> '%l',
                'unsignedlongint'=> '%l',
                'longlongint'=> '%ll',
                'unsignedlonglongint'=> '%ll',
                'pointer'=>  '%p', # artificial
                'funcpointer' => '%p',# what to do with void?
    },
        
        # vb : 1 for debugging
        vb => 1,

        # output file, if not given defaults to /dev/stdout
        outfile => '-'
};

parse_args();
@{$opts->{format_types}} = grep{$_ ne 'pointer'}sort keys %{$opts->{formats}};
    
my $header_data = parse_header($header_file);

my $struct = extract_struct($header_data,
                            $struct_name);

recurse_struct($struct_location,
               $header_data,
               $struct);

finalise();

output();

# exit 0 on success: this is required for the build process
exit(0);

############################################################

sub output
{
    # output
    my $fp;
    if($opts->{outfile} eq '-')
    {
        $fp = \*STDOUT;
    }
    else
    {
        open($fp ,
             '>',
             $opts->{outfile})||
                 die("cannot open $opts->{outfile} for writing");
    }
    print {$fp} "
/*
 * Output from make_showstruct.pl $args
 */
\n";
    print {$fp} $Ccode;
    close $fp;
}

sub parse_args
{
    # parse cmd line args

    # get args in opt hash
    foreach my $arg (@ARGV)
    {
        if($arg=~/(\S+)=(\S+)/)
        {
            $opts->{$1}=$2;
        }
    }
    
    # convert expand pointer list to an array and a hash
    # for quick lookup
    $opts->{expand_pointer_array} = [];
    $opts->{expand_pointer_hash} = {};
    @{$opts->{expand_pointer_array}} = 
        split(/,/,$opts->{expand_pointer_list});
    map
    {
        $opts->{expand_pointer_hash}->{$_} = 1;
    }@{$opts->{expand_pointer_array}};
    
}

sub recurse_struct
{
    # given a string containing a struct's contents,
    # find the contents of the structs it contains, if possible.
    my ($location,
        $header_data,
        $struct) = @_;
    vb("PARSE at $location\n$struct\n\n\n");

    # loop over inner structs
    while($struct=~/struct\s+(\S+)\s*(\*?)\s*([^;]+);/g)
    {
        my $type = $1;
        my $pointer = $2;
        my $innerstruct = $3;
        my $array = $innerstruct=~s/\[([^\]]+)\]// ? $1 : '';
        
        vb( "Found inner struct $innerstruct of type $type (pointer? $pointer; array? $array)\n");

        # in the show list?
        if($opts->{expand_pointer_hash}->{$innerstruct})
        {
            vb( "Expand $innerstruct\n");
            my $substruct = extract_struct($header_data,
                                           $type);
            
            if($array)
            {
                if($array=~/^\d+$/ ||
                   (eval $array) =~/^\d+$/
                    )
                {
                    for(my $i=0;$i<$array;$i++)
                    {
                        recurse_struct($location.'.'.$innerstruct.$pointer.'['.$i.']',
                                       $header_data,
                                       $substruct);
                    }
                }
                else
                {
                    print STDERR "We cannot yet parse >=2D arrays of nested structs\n";
                    exit(1);
                }
            }
            else
            {
                recurse_struct($location.'.'.$innerstruct.$pointer,
                               $header_data,
                               $substruct);
            }
        }
    }

    # find function pointers
    while($struct=~s/\W(void|double|int|float|char|Boolean|boolean|bool)\s*(\(\s*\*(\s*([^()]|(?2))*)\))\s*(\(.*\))/funcpointer $3;/gxs)
    {
        my $type = $1;
        my $name = $3;
        my $fargs = $4;
        $fargs=~s/\n/ /g;
        #foreach my $x (1..10)
        #{
        #    print "Match $x : ",eval "\$$x","\n";
        #}
        print "Found function pointer with type $type, name $name, args $fargs\n";
    }
    
    # loop over other variables
    foreach my $type (@{$opts->{format_types}})
    {
        my $format = $opts->{formats}->{$type};
        my $roottype = $type;
        my $pointer = '';
        my $re = $type;
        my $prettytype = sprintf '%-30s',$type;
        if($re=~s/(\*+)$//)
        {
            $pointer = $1;
            $pointer =~ s/\*/\\\*/g;
        }

        # compare this data type to the struct contents,
        # see if we should output
        vb( "Match $type $pointer\n");
        while($struct=~/\W$re\W\s*$pointer\s*([^;]+?);/g)
        {
            vb("Found type=\"$type\" pointer=\"$pointer\" $1\n");
            my @x = split(/\s*,\s*/,$1);
            
            foreach my $var (@x)
            {
                
                my $varlocation = $location.'.'.$var;
                $varlocation =~s/\s+//g;
                sprintf '%-60s',$varlocation;
                
                vb("VAR : $var\n");
                if($var=~/^\*/)
                {
                    # generic pointer
                    if($opts->{show_pointer_locations})
                    {
                        addCcode( "
        printf ( \"$opts->{prefix}$prettytype: $varlocation = $opts->{formats}->{pointer}\\n\",  $location.$var ) ;
");                 
                    }
                    else
                    {
                        addCcode( "
        printf ( \"$opts->{prefix}$prettytype: $varlocation = pointer\\n\" ) ;
");
                    }
                    next;
                }

                # make C code to show the contents
                my @arraydims;
                while($var=~s/\[([^\]]+)\]//)
                {
                    push(@arraydims,'('.$1.')');
                }

                # if char, reduce arraydims by 1 to output
                # as a string rather than chars
                my $is_string = 0;
                if($type=~/char/)
                {
                    $is_string = 1;
                    pop @arraydims;
                    vb("IS STRING\n");
                }
                
                vb( "Show $location.$var (type $type, pointer $pointer, root $roottype, dimensions @arraydims)\n");

                if(scalar @arraydims)
                {
                    # found an array, possibly multi-dimensional
                    #
                    # NB char arrays have their arraydims reduced by one
                    #    and are output as strings
                    my $n=0;
                    my $lookup;
                    my $lookupformat;
                    my $lookupvars;
                    foreach my $arraydim (@arraydims)
                    {
                        my $arraydim = clean_arraydim($arraydim);

                        addCcode( "
\{
    int i$n;
    for(i$n = 0; i$n < $arraydim; i$n ++)
    \{
");
                        $lookup .= "\[i$n\]";
                        $lookupformat .= ',% 3d';
                        $lookupvars .= "i$n, ";
                        $n++;
                    }

                    $lookupformat=~s/^,//;

                    my $v = "$location.$var\[$lookupformat\]";
                    $v = sprintf '%-*s',60+$n,$v;
                    addCcode( "
        printf ( \"$opts->{prefix}$prettytype: $v = $format\\n\", $lookupvars $location.$var$lookup ) ;
");
                    
                    foreach my $arraydim (@arraydims)
                    {
                        addCcode("
    \}
\}
");
                    }
                }
                else
                {
                    # normal scalar
                    addCcode("
        printf ( \"$opts->{prefix}$prettytype: $varlocation = $format\\n\", $location.$var ) ;
                ");
}
            }
        }
    }
}

sub parse_header
{
    # extract the header file data
    my ($header_file) = @_;
    my $cmd = $opts->{header_extract}.' '.$header_file;
    my $r = `$cmd`;
    # strip comment lines starting in #
    $r=~s/\n\#.*//g;
    $r=~s/^\#.*//g;
    # strip double empty lines
    while($r=~s/\n\n/\n/g){};
    # convert "long int" to "longint", etc.
    $r=~s/long\s*double/longdouble/g;
    $r=~s/unsigned\s*long\s*long(?: int)?/unsignedlonglongint/g;
    $r=~s/unsigned\s*long(?: int)?/unsignedlongint/g;
    $r=~s/long\s*long\s*int/longlongint/g;
    $r=~s/long\s*int/longint/g;
    $r=~s/unsigned int/unsignedint/g;
    return $r;
}

sub extract_struct
{
    # given header file (as data) extract the required struct 
    my ($header_data,
        $struct_name) = @_;

    if($header_data=~/struct\s+$struct_name\s*\{\s*([^}]+)\s*\}\s*;/s)
    {
        my $x = $1;
        $x=~s/^\s+//g;
        $x=~s/\n\s+/\n/g;
        return $x;
    }
    else
    {
        return undef;
    }
}

sub addCcode
{
    # add $code to the C code
    my ($code) = @_;
    $Ccode .= $code;
    vb( "ADD C CODE : $code\n");
}

sub finalise
{
    # final changes to the C code
    $Ccode=~s/\*\./\-\>/g;
    while($Ccode=~s/\n\s*\n+/\n/g){};
    $Ccode=~s/printf/Printf/g;
    $Ccode=~s/->\s*\*+/->/g;
    $Ccode=~s/\.\s*\*+/./g;

    if(1&&$opts->{diff})
    {
        # ints, unsigned ints
        $Ccode=~s/(.*= )(\%[cdu])\\n\",\s+stardata->(\S+)/if(((stardata->previous_stardata->$3)!=(stardata->$3)))$1\[prev $2, now $2, diff $2\]\\n\", stardata->previous_stardata->$3, stardata->$3, (stardata->$3 - stardata->previous_stardata->$3)/g;
        $Ccode=~s/(.*= )(\%[cdu])\\n\", i0,\s+stardata->(\S+)/if(((stardata->previous_stardata->$3)!=(stardata->$3)))$1\[prev $2, now $2, diff $2\]\\n\", i0, stardata->previous_stardata->$3, stardata->$3, (stardata->$3 - stardata->previous_stardata->$3)/g;

        # floats, doubles
        $Ccode=~s/(.*= )(\%[efg])\\n\",\s+stardata->(\S+)/if(!FEQUAL((stardata->previous_stardata->$3),(stardata->$3)))$1\[prev $2, now $2, diff $2\]\\n\", stardata->previous_stardata->$3, stardata->$3, (stardata->$3 - stardata->previous_stardata->$3)/g;
        $Ccode=~s/(.*= )(\%[efg])\\n\", i0,\s+stardata->(\S+)/if(!FEQUAL((stardata->previous_stardata->$3),(stardata->$3))) $1\[prev $2, now $2, diff $2\]\\n\", i0, stardata->previous_stardata->$3, stardata->$3, (stardata->$3 - stardata->previous_stardata->$3)/g;

        # chars, strings, funcpointers, pointers : show nothing
        $Ccode=~s/.*string.*//g;
        $Ccode=~s/.*char.*//g;
        $Ccode=~s/.*funcpointer.*//g;
        $Ccode=~s/.*pointer.*//g;
    }
    $Ccode=~s/_Bool/Boolean/g;
}

sub vb
{
    # debugging
    if($opts->{vb})
    {
        print @_;
    }
}

sub clean_arraydim
{
    my $array = shift;

    # clean up the arraydim which is often
    # a compound macro which can be greatly simplified
    
    $array =~ s/\(\s*0\s*\+1\s*\)/1/;
    while($array =~ s/\((\d+)\+(\d+)\)/$1+$2/e){};
    while($array =~ s/\((\d+)\)/$1/){};
    
    return $array;
}
