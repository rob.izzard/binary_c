#!/usr/bin/env perl
use 5.16.0;
use strict;
use rob_misc qw/slurp/;
use List::MoreUtils qw/uniq/;

my $unit_test_script_version = 1.0;
    
# 
# Script to run binary_c unit tests in multiple directories, 
# which are given as arguments.
#
# See help below for more information.
#

# TODO : add date, SVN revision

if(!-d $ARGV[0] && $ARGV[0] =~/^-*h/)
{
    print "

 Script to run binary_c unit tests in multiple directories. 
 
 unit_tests.pl <testrange> <dir1> <dir2>

 The <testrange> should be either a list of numbers, e.g.
 1,2,3,4 or a range 1-4 or some combination of the above.
 
 Test scripts are found in the unit_tests/tests directory.

 The directories, <dir1>, <dir2>, etc. contain binary_c builds,
 where <dir1> contains the \"master\" version which is the standard 
 against which all the other builds should be tested. The master
 is usually the latest version of binary_c.

 Please run this script from within the binary_c root directory
 of the master version.
";
    exit;
}

# define and make (if necessary) the test directories
my $unit_test_dir = './unit_tests';
my $tests_dir = 'tests';
my $plots_dir = 'plots';
my $plts_dir = 'plts';
my $results_dir = 'results';
my $logs_dir = 'logs';
foreach my $dir ($unit_test_dir,
                 $unit_test_dir.'/'.$tests_dir,
                 $unit_test_dir.'/'.$plots_dir,
                 $unit_test_dir.'/'.$results_dir,
                 $unit_test_dir.'/'.$logs_dir,
                 $unit_test_dir.'/'.$plts_dir
    )
{
    mkdir $dir;
    die("Required directory $dir is missing\n")if(!-d $dir);
}

my $tests = make_comma_separated_list(shift @ARGV);
my @dirs = @ARGV;
$dirs[0] //= '.'; # defau3l to current dir if none are given

my @header;
my %versions;
foreach my $test (@$tests)
{
    my ($args,$teststring,$comment,$gnuplot,$extra) = get_test_info($test);
    print "Running test $test:\n$teststring\n";
    
    foreach my $dir (@dirs)
    {
        my $version = version($dir);
        $versions{$dir} = $version;
        print "Dir $dir : version $version\n";
        my $vargs = fixargs($version,$args); 
        run($test,$dir,$version,$vargs);
    }
    plot($test,\@dirs,\@header,$gnuplot,$extra);
}

exit;

sub get_test_info
{
    my ($test) = @_; 
    my $x = slurp("$unit_test_dir/$tests_dir/$test");
    my $args = ($x=~/args\:(.*)/)[0];
    my $teststring = ($x=~/test\:(.*)/)[0];
    my $comment = ($x=~/comment\:(.*)/)[0];
    my $gnuplot = {};
    while($x=~s/gnuplot\:(\S+)\:(.*)//)
    {
        $gnuplot->{$1}=$2;
    }
    $gnuplot->{global} = ($x=~/gnuplot\:(.*)/)[0];
    my $extra = {};
    while($x=~s/extra\:(\S+)\:(.*)//)
    {
        print "EXTRA $1 -> $2\n";
        $extra->{$1}=$2;
    }    
    return ($args,$teststring,$comment,$gnuplot,$extra);
}

sub plot
{
    my ($test,$dirs,$header,$gnuplot,$extra) = @_;
    my $pltfile = "$unit_test_dir/$plts_dir/$test.plt";
    my $pdffile = "$unit_test_dir/$plots_dir/binary_c_unit_test.$test.pdf";
    
    open(GP,'>',$pltfile)||die("cannot open $pltfile");
    print GP "
set terminal pdfcairo size 10,5 enhanced
set output \"$pdffile\"
set pointsize 0.3
set datafile missing \"*\"
set zero 0.0

";

    foreach my $dir (@$dirs)
    {
        my $version = $versions{$dir};
        my $logfile = "$unit_test_dir/$logs_dir/$test/$version.dat";
        my $log = slurp($logfile);
        $log=~s/\n/\\n/g;
        $log=~s/_/ /g;
        my $svn = svn_revision($dir);
        my $t = localtime();
        $log = "Unit test $test, version $version, SVN revision $svn at $t\\n".$log;
        print GP "
unset title
set size 1,10
set label \"$log\" at screen 0.01,0.99 font \"Courier,9\"
set yrange[0:1]
set xrange[0:1]
unset border
unset xtics
unset ytics
plot 0 lw 0 notitle
set size 1,1
";
    }

    if(1){
    my %header;
    {
        # make reverse lookup hash for column numbers
        my $i = 1;
        map
        {
            $header{$_} = $i++;
        }@header;
    }
    
    foreach my $x (@$header)
    {
        my $t = title($x);
        print GP "set title \"$t\"
unset label 
set border
set xtics
set ytics
set xrange[*:*]
set yrange[*:*]
unset logscale x
unset logscale y
xmult=1.0
xoffset=0.0
ymult=1.0
yoffset=0.0
set format x '%g'
set format y '%g'
$gnuplot->{global}
";
        if(defined $gnuplot->{$x})
        {
            print GP "$gnuplot->{$x}\n";
        }
        my $plotcomma = 'plot';
        foreach my $dir (@$dirs)
        {
            my $version = $versions{$dir};
            my $file = "$unit_test_dir/$results_dir/$test/$version.dat";
            print GP "$plotcomma \"$file\" u (\$$header{'Time'}*xmult+xoffset):(\$$header{$x}*ymult+yoffset) title \"$version\" ";
            $plotcomma = ',';
        }
        #print "PLOT $x $header{$x} : extra $extra->{$x}\n";
        if(defined $extra->{$x})
        {
            print GP $plotcomma.$extra->{$x};
        }
        print GP "\n";
    }
}
    close GP;
    `gnuplot $pltfile`;
    print "See $pdffile\n";
}

sub version
{
    my ($dir) = @_;
    return (`cd $dir; ./binary_c --version` =~ /Version\s+(\S+)/)[0];
}

sub run
{
    my ($test,$dir,$version, $args) = @_;

    mkdir "$unit_test_dir/$results_dir/$test";
    mkdir "$unit_test_dir/$logs_dir/$test";
    my $outfile = "$unit_test_dir/$results_dir/$test/$version.dat";
    my $logfile = "$unit_test_dir/$logs_dir/$test/$version.dat";

    open(my $fp, '>', $outfile) || die("cannot open $outfile for writing");
    print {$fp} "# binary_c unit test $1 for version $version using unit test script version $unit_test_script_version\n";
    close $fp;
    
    print `cd $dir; ./src/binary_c $args --log_filename $logfile | grep BINARY_C_UNIT_TEST|sed s/BINARY_C_UNIT_TEST\\ //|sed 's/-666\.6/*/g' >> $outfile`;
        
    if($#header == -1)
    {
        # from the reference set, get the headers
        open(my $fp , '<', $outfile)||die;
        <$fp>; # first line is comment section
        my $head = <$fp>;
        chomp $head;
        @header = split(/\s+/,$head);
        close $fp;
    }
}

sub fixargs
{
    my ($version, $args) = @_;

    if($version eq '2.0pre28')
    {
        return $args;
    }
    elsif($version eq '2.0pre27')
    {
        $args =~ s/accretion_limit_eddington_multiplier/eddfac/g;
        $args =~ s/wind_angular_momentum_loss/jorb_loss/g;
        foreach my $x (
            'accretion_limit_thermal_multiplier',
            'accretion_limit_dynamical_multiplier',
            'donor_limit_thermal_multiplier',
            'donor_limit_dynamical_multiplier',
            'donor_limit_dynamical_multiplier',
            'comenv_splitmass',
            'lambda_enthalpy',
            'comenv_merger_spin_method',
            'comenv_ejection_spin_method',
            'comenv_disc_mass_fraction',
            'comenv_disc_angmom_fraction',
            'cbdisc_.*',
            )
        {
            $args =~ s/--$x\s+\S+//g;
        }
        return $args;
    }
    else
    {
        print "Unknown arg conversion mechanism for version $version\n";
    }
}

sub title
{
    my $t = shift;
    $t=~s/_/ /g;
    return $t;
}

sub make_comma_separated_list
{
    # given numbers comma separated or in a range, just make a
    # comma-separated list
    my ($range) = @_;
    my @list;
    $range =~ s/(\d+)-(\d+)/join(',',map{$_}($1 .. $2));/e;
    my @list = uniq split(/,/,$range);
    print "@list\n";
    return \@list;
}

sub svn_revision
{
    # given a directory, find its SVN revision
    my ($dir) = @_;
    state %cache;
    $cache{$dir} //= (`cd $dir; svn info`=~/Revision\:\s+(\d+)/)[0];
    return $cache{$dir};
}
