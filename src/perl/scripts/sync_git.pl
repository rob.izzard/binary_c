#!/usr/bin/env perl
use strict;
use File::Path qw/make_path rmtree/;
use rob_misc;

print "DEPRECATED\n";
exit;

# script to sync rob's git repos to this source tree
# so that we have a local copy in binary_c

# make temp directory
my $tmp = '/tmp/.binary_c_gitmerge';
if(-d $tmp)
{
    rmtree($tmp);
}
make_path($tmp);

if(-d $tmp)
{
    # list of gits
    my $gits = {

        'librinterpolate' => {
            url => 'git@gitlab.com:rob.izzard/librinterpolate.git',
            source_dir => 'src',
            target_dir => 'src/librinterpolate',
            flag => '__HAVE_LIBRINTERPOLATE__',
        },

            'libmemoize' => {
                url => 'git@gitlab.com:rob.izzard/libmemoize.git',
                source_dir => 'src',
                target_dir => 'src/libmemoize',
                flag => '__HAVE_LIBMEMOIZE__',
        },
    };

    # loop
    foreach my $git (sort keys %$gits)
    {
        print "\n\n$git\n";

        # Clone
        print "Clone from $gits->{$git}->{url}:\n";
        my $cmd = "cd \"$tmp\"; git clone \"$gits->{$git}->{url}\" $git";
        execcmd($cmd);

        # make target
        make_path($gits->{$git}->{target_dir});

        # copy to target
        foreach my $pattern ('*.c',
                             '*.h',
                             'README',
                             '../README',
                             'LICENCE',
                             '../LICENCE',
                             'CHANGELOG',
                             '../CHANGELOG',

            )
        {
            if($pattern =~/^\*\.[ch]/)
            {
                my @f = `ls $tmp/$git/$gits->{$git}->{source_dir}/$pattern`;
                chomp @f;
                foreach my $f (@f)
                {
                    print "FILE $f\n";
                    my $code = slurp($f);
                    if($code!~/binary_c\.h/)
                    {
                        my @code = split(/\n/,$code);
                        my $inject = 0;
                        while($code[$inject] =~/(pragma)/ ||
                              $code[$inject]=~/(\#ifndef)/)
                        {
                            my $m = $1;
                            $inject++;
                            if($m=~/ifndef/ && $code[$inject]=~/\#define/)
                            {
                                $inject++;
                            }
                        }
                        # include binary_c.h and disable
                        # if using the shared library
                        $code[$inject] = "
#include \"../binary_c_error_codes.h\"
#include \"../binary_c_exit_macros.h\"
#include \"../breakpoints/breakpoints_prototypes.h\"
#include \"../binary_c_exit_prototypes.h\"


#ifndef $gits->{$git}->{flag}
                                    \n".$code[$inject];
                        $code = join("\n",@code);
                        $code .= "
#endif // $gits->{$git}->{flag}
                                    ";

                        # append binary_c to version
                        $code =~s/VERSION \"(\d+\.\d+)\"/VERSION \"$1-binary_c\"/;

                        # change exit() to exit_binary_c_no_stardata
                        $code =~s/exit\(.*?\)\s*;/Exit_binary_c_no_stardata\(BINARY_C_IMPORTED_CODE_ERROR,\"Error in $git in binary_c\"\);/;

                        # overwrite
                        dumpfile($f,$code);
                    }
                }
            }

            # copy appropriate files with rsync
            $cmd = "rsync -avP --exclude '*test*'  $tmp/$git/$gits->{$git}->{source_dir}/$pattern \"$gits->{$git}->{target_dir}/\"";
            print "Copying source from $git/$gits->{$git}->{source_dir} to target $gits->{$git}->{target_dir}\n";
            execcmd($cmd);

            # add to git
            $cmd = "git add $gits->{$git}->{target_dir}/$pattern";
            execcmd($cmd);
        }
    }
}
else
{
    print "Temporary directory at $tmp is not available\n";
}

############################################################

sub execcmd
{
    my ($cmd) = @_;
    print "--\n  $cmd\n--\n";
    `$cmd`;
    #<STDIN>;
}
