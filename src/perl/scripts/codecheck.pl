#!/usr/bin/env perl 
use strict;
use rob_misc;
use 5.16.0;
use Term::ANSIColor;
# 
# Script to check binary_c prior to a release,
# e.g. to check for unused macros.
#

my @errors;
my ($src,$c_src,$h_src) = srcfiles();

check_for_unused_macros();
show_errors();
exit;


############################################################

sub show_errors
{
    # show all errors found
    say color('reset'),@errors,color('reset');
}

sub srcfiles
{
    # 
    my @c = `ls src/*.c src/*/*.c`;
    my @h = `ls src/*.h src/*/*.h`;
    my @h = 'src/binary_macros.h';
    chomp (@c,@h);
    return [@c,@h],\@c,\@h;
}

sub check_for_unused_macros
{
    # check that macros are used
    my $macros = macro_hash();
    foreach my $macro (sort keys %$macros)
    {
        my $in = join(',',@{$macros->{$macro}});
        print "Look for macro $macro (defined in $in) : ";
        my $res = rgrep($macro,$src);
        print "RES $res\n";
        if($res == 0)
        {
            my $str = color('red').
                "Macro $macro never found (!) this should not happen\n".
                color('white');
            push(@errors,$str);
            print $str;
        }
        elsif($res == 1)
        {
            my $str = color('red').
                "Macro $macro (defined in $in) unused other than definition.\n".
                color('white');
            push(@errors,$str);
            print $str;                 
        }
        else
        {
            print "Found $res times\n";
        }
    }
}

sub rgrep
{
    # return number of matches
    my ($regexp_string,$srcfiles) = @_;
    my $n = 0;
    foreach my $file (@$srcfiles)
    {
        my $x = slurp($file);
        while($x=~/$regexp_string/g) { $n ++ } 
    }
    return $n;
}

sub macro_hash
{ 
    print "Making macro hash\n";
    my %macros;
    foreach my $file (@$src)
    {
        print " ... \"$file\" \n";
        my $x = slurp $file;
        while($x =~ /\#define ([A-Za-z][A-Za-z_\d]+)/g)
        {
            $macros{$1} //= [];
            push(@{$macros{$1}}, $file); 
        }
    }
    return \%macros;
}
