#!/usr/bin/env perl

print "Use CNSTATS2\n";
exit;

use rob_misc;
use strict;
use Data::Dumper;
$Data::Dumper::Sortkeys=1;



# binary (system) fraction
my $binfrac = 0.5;
my $m_blue_straggler=1.3;
my $dir = $ARGV[0]//'.';
my $n = {
    blue_stragglers=>{},
    normal=>{},
    all=>{},
};

foreach my $file (`ls $dir`)
{
    # only use files with radial velocity information
    next if($file!~/\.(?:[gl]t|eq)\./);

    # make long filename
    chomp $file;
    $file = $dir.'/'.$file;

    # store some stats on a by-file basis
    $n->{$file} = {};
    
    # stars with K>1 km/s are binaries, 
    # stars with 0>K>1 are undetectable binaries
    # stars with K=0 are truly single
    my $binary_status = 
        # truly single stars : born that way, always that way
        $file=~/-single$/ ? 'always_single' :
        # binaries still detectable as binaries
        $file=~/.gt.1/ ? 'detectable_as_binary' : 
        # binaries that are disrupted and/or merged
        $file=~/.eq.0/ ? 'was_binary_now_single' : 
        # binaries that would be detected as single stars
        'undetectable_binary';

    my $weight = $file=~/-binary$/ ? $binfrac : (1.0-$binfrac);

    open(FP,'<',$file)||die("could not open $file");
    while(<FP>)
    {
        my @x = split;
        if(scalar @x)
        {
            my ($m,$CN,$dtp) = @x;

            # binary fraction weighting
            $dtp *= $weight;

            $n->{$file}->{all} += $dtp;
     
            $n->{all}->{all} += $dtp;
            $n->{all}->{$binary_status} += $dtp;

            if($m > $m_blue_straggler)
            {
                # weird!
                $n->{blue_stragglers}->{all} += $dtp;
                $n->{blue_stragglers}->{$binary_status} += $dtp;
            }
            else
            {
                $n->{normal}->{all} += $dtp;
                $n->{normal}->{$binary_status} += $dtp;
            }        
        }
    }
}

print Data::Dumper::Dumper($n);


############################################################

# use stats file to do more complicated things

# slurp in and eval the data
my $stats;
#eval (slurp ($dir.'/stats')=~s/VAR1/stats/);

print Data::Dumper::Dumper($stats);

