#!/usr/bin/env perl
use strict;
use binary_grid;

my $nstart = $ARGV[0];
my $n = 0;
my $array = [];

my @stellar_type_strings = @{[stellar_type_strings()]->[3]};
foreach my $st1 (0..$#stellar_type_strings-1)
{
    foreach my $st2 (0..$#stellar_type_strings-1)
    {
        if($st2>=$st1)
        {
            printf "%s%s_%s %d\n",
            '#define NUCSYN_ENSEMBLE_MERGER_',
            $stellar_type_strings[$st2],
            $stellar_type_strings[$st1],
            $n + $nstart;
            $array->[$st1]->[$st2] = $n + $nstart;
            $array->[$st2]->[$st1] = $n + $nstart;
            $n++;
        }
    }
}

print "\n\{\n";
for(my $i=0;$i<=$#$array;$i++)
{
    print '{';
    my $nmax = $#{$array->[$i]};
    for(my $j=0;$j<=$nmax;$j++)
    {
        printf "%g%s",$array->[$i]->[$j],($j!=$nmax?',':''); 
    }
    print "\}";
    print ',' if($i!=$#$array);
    print "\n";
}
print "\}\n";


