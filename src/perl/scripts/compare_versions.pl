#!/usr/bin/env perl
use strict;
use robqueue;
use rob_misc;


# script to compare two (or more) version of binary_c
# to ensure that they have (almost) identical output 


$|=1;    
my $args0 = `tbse echo`;
chomp $args0;    
#print "Args 0 $args0\n";

my @executables = ('binary_c-ref','binary_c');
my @firstargs = @ARGV;
my %out;
my %runtime;
threads::shared::share(%out);
threads::shared::share(%runtime);
my $n=0;
while(1)
{
    $n++;
    print "Run $n\n";
    # make queue to run on multiple cpus
    my $q = robqueue->new(
        nthreads=>2,
        subr=>sub{

            my ($executable,$cmd) = @{$_[0]};
            print "Running $executable ...\n";
            $out{$executable} = `$executable $cmd`;
            $runtime{$executable} = ($out{$executable}=~/runtime = (\S+)/)[0];
            $out{$executable} =~ s/Tick count.*//g;
            $out{$executable} =~ s/XYIELDbin__.*//g;
            print "Done $executable\n";
            return 0;
        }
        );
    %out=();
    %runtime=();
    my $args = $args0;
    my $extra = shift @firstargs;

    if(!defined $extra)
    {        
        # fix idum
        $extra .= ' --idum -'.int(2+65000*rand()).' ';

        # randomize M1, M2, per etc.
        my $m1 = randval(0.1,100,1);
        $extra .= " --M_1 $m1 ";
        $extra .= ' --M_2 '.randval(0.1,$m1,1).' ';
        $extra .= ' --orbital_period '.randval(0.1,100,1).' --separation 0 ';
        $extra .= ' --metallicity '.randval(1e-4,0.03,1).' ';

        # common envelope 
        my $comenv = 0;#rand()>0.5 ? 0 : 1;
        $extra .= ' --comenv_prescription '.$comenv.' ';
        $extra .= ' --alpha_ce '.randval(0.0,5.0,0).' ';
        $extra .= ' --lambda_ce '.(rand()>0.5 ? -1 : randval(0,1,0)).' ';
        if($comenv == 1)
        {
            $extra .= ' --nelemans_gamma '.randval(1,2,0).' --nelemans_n_comenvs 1 ';
        }
        
    }    
    print "$extra\n";

    # append extra args
    $args .= $extra.' --log_filename /dev/stdout ';
    
    # run binary_c codes
    foreach my $executable (@executables)
    {
        $q->q([$executable,$args]);
    }
    $q->end();

    # compare all codes to the reference code
    my $ref = $executables[0];
    open(FP,'>',"/tmp/out.$ref");
    print FP $out{$ref};
    close FP;
    
    foreach my $executable (grep {$_ ne $ref} @executables)
    {
        open(FP,'>',"/tmp/out.$executable");
        print FP $out{$executable};
        close FP;
        
        if($out{$ref} ne $out{$executable})
        {
            print "Difference error\n";
            my @ref = split(/\n/,$out{$ref});
            my @new = split(/\n/,$out{$executable});

            my $n = 1 + MAX($#ref,$#new);
            for(my $i=0; $i<$n; $i++)
            {
                if($ref[$i] ne $new[$i])
                {
                    print "Line $i diff:\nREF : $ref[$i]\nNEW : $new[$i]\n";
                }
            }
            exit;
        }
    }

    printf "Runtime % 20s : % 6g s\n",
    $ref,
    $runtime{$ref};

    foreach my $executable (grep {$_ ne $ref} @executables)
    {
        printf "Runtime % 20s : % 6g s : %.2f %%\n",
        $executable,
        $runtime{$executable},
        ($runtime{$executable}-$runtime{$ref})/$runtime{$ref}*100.0
    }
}

sub randval
{
    # return a random value between $min and $max
    # (using a logscale if uselog is true)
    my ($min,$max,$uselog) = @_;
    $uselog//=0;
    if($uselog)
    {
        $min=log10($min);
        $max=log10($max);
    }
    my $val = rand() * ($max-$min) + $min;
    $val = 10.0**$val if($uselog);
    return sprintf '%g', $val; 
}
