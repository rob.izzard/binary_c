#!/usr/bin/env perl
use rob_misc;
use strict;
use Data::Dumper;
use Sort::Key qw/nsort/;
use 5.16.0;
use utf8;
$Data::Dumper::Sortkeys=1;

# binary (system) fraction
my $intrinsic_binary_fraction = $ENV{CN_PROJECT_BINARY_FRACTION} // 0.5;

# stars > Mlimit are "extra massive"
my $Mlimit = $ENV{CN_PROJECT_MASS_LIMIT} // 1.3;

# should be zero in the final model set
my $CNoffset = $ENV{CN_PROJECT_CN_OFFSET} // 0.2;

# ignore buggy merged systems with [C/N]>2 
# (these pick up the COWD abundance for a single timestep,
# fixed in latest binary_c)
my $ignore_buggy_mergers = 1;

my $gpsurf = 'gnuplot_surface3.pl';
my %histograms;
my @map_types = ('mass','core_mass','logg');
my @stellar_types = ('All',2,3,4,5,6);
my %stellar_type_strings = (
    'All'=>'All',
    '2'=>'Hertzsprung Gap',
    '3'=>'First Giant Branch',
    '4'=>'Core Helium Burning',
    '5'=>'Early AGB',
    '6'=>'TP AGB'
    );  

my %gpopts = (
    'mass'=>{
        xrange=>'[0.55:2.4]',
        yrange=>'[-0.9:0.4]'
    },
    'logg'=>{
        xrange=>'[0:4]',
        yrange=>'[-0.9:0.4]',
    },
    'core_mass'=>{
        yrange=>'[-0.9:0.4]',
    },
    );

printf "Threshold mass for 'extra massive': %g\n\n",$Mlimit;

# remove options which are given after the directories 
my %opts;
for(my $i=1;$i<=$#ARGV;$i++)
{
    if($ARGV[$i] eq 'nognuplot')
    {
        delete $ARGV[$i];
        $opts{nognuplot}=1;
    }
}

# directories : first arg is for single stars, second is for binaries
# these can be "undef" which means skip it
my @dirs = ($ARGV[0]);
push(@dirs,$ARGV[1])if(defined $ARGV[1]);
my @weights;
my @dirsin = @dirs;
# convert symlink dirs to directories
map
{
    if(-l $_)
    {
        $_ = readlink $_;
    }
}@dirs;

# loop over dirs
if(scalar @dirs==2)
{
    # two dirs : weight is single fraction/binary fraction
    @weights = (1.0 - $intrinsic_binary_fraction,
                $intrinsic_binary_fraction);

    printf "Using binary fraction %g\n",$intrinsic_binary_fraction;

    if($dirs[0]!~/single/ || $dirs[1]!~/binary/)
    {
        print "You have specified two directories. The first should be single stars, the second binary stars, but I cannot find the correct label (single/binary) in one or both of the directory names. Please check that you have entered them correctly.\n";
        exit;
    }
}
elsif(scalar @dirs==1)
{
    # weight is 1 for only 1 dir
    @weights = (1.0);
    printf "Using no single/binary fraction (all equal weights)\n";
}
else
{
    print "No idea what to do with ",scalar @dirs," directories! You should specify 1 or 2.\n";
    exit;
}

########################################################
# Do the work to add up statistics and/or make figures #
########################################################

my %numbermap;
my %binfracmap;
my %bssmap;
my %count;

for(my $i=0;$i<=$#dirs;$i++)
{
    my $dir = $dirs[$i];
    next if($dir eq 'undef');

    # slurp in and eval the data into $stats
    my $stats = slurp ($dir.'/stats');
    $stats =~ s/VAR1/stats/;
    $stats = eval $stats;

    # weight : see above
    my $weight = $weights[$i];
    
    # was/was not BSS stats
    foreach my $map_type (@map_types)
    {
        foreach my $bss ('was_BSS','never_BSS')
        { 
            my $k = 'CN vs '.$map_type.' '.$bss;
            my $h=$stats->{$k};
            foreach my $m (nsort keys %$h)
            {
                foreach my $CN (nsort keys %{$h->{$m}})
                {
                    my $dtp =$h->{$m}->{$CN} * $weight;
                    $bssmap{$map_type}{$m}{$CN}{$bss} += $dtp; 
                    if($map_type eq 'mass')
                    {
                        $count{$bss}+=$dtp;
                        $count{'extra_massive_'.$bss}+=$dtp if($m>$Mlimit);
                    }
                }
            }
        }
    }

    # make maps
    foreach my $map_type (@map_types)
    {
        foreach my $stellar_type (@stellar_types)
        {
            foreach my $radial_velocity_class ('K.eq.0','K.lt.1','K.gt.1')
            {
                my $k = 'CN vs '.$map_type.' '.$radial_velocity_class;

                # key requires stellar type if not 'All'
                if($stellar_type ne 'All')
                {
                    $k .= ' stellar_type '.$stellar_type;
                }

                my $h = $stats->{$k};
                if($h)
                {
                    # detect whether the system would be observed as 
                    # a binary or not
                    my $binary = (#$dir=~/binary/ && 
                                  $radial_velocity_class eq 'K.gt.1') ? 1 : 0;

                    foreach my $m (nsort keys %$h)
                    {
                        foreach my $CN (nsort keys %{$h->{$m}})
                        {
                            next if($ignore_buggy_mergers && $CN > 1.9);

                            my $dtp = $h->{$m}->{$CN} * $weight;
                            
                            # add to maps
                            # add to specific stellar type map
                            $binfracmap{$map_type}{$stellar_type}{$m}{$CN}{$binary} += $dtp;
                            $numbermap{$map_type}{$stellar_type}{$m}{$CN} += $dtp;
                            
                            # histograms
                            if($stellar_type eq 'All')
                            {
                                if($map_type eq 'core_mass')
                                {
                                    $histograms{$map_type}{$stellar_type}{$m} += $dtp;
                                }
                                elsif($map_type eq 'mass')
                                {
                                    $histograms{$map_type}{$stellar_type}{$m} += $dtp;
                                }                         
                            }   
                            
                            # region counts
                            if($map_type eq 'mass' && $stellar_type eq 'All')
                            {
                                # count total number of stars 
                                $count{ntot} += $dtp;
                                
                                # extra-massive giants
                                if($m>$Mlimit)
                                {
                                    $count{extra_massive_giant} += $dtp;
                                    if($CN<0)
                                    {
                                        $count{extra_massive_giant_N} += $dtp;
                                    }
                                    else
                                    {
                                        $count{extra_massive_giant_C} += $dtp;                            
                                    }

                                    if($binary)
                                    {
                                        $count{'extra_massive_giant_binary'} += $dtp;
                                    }
                                    else
                                    {
                                        $count{'extra_massive_giant_single'} += $dtp;
                                    }
                                    
                                    

                                }
                                # low mass "algols"
                                elsif($m<0.5)
                                {
                                    $count{low_mass_algol} += $dtp;
                                }
                                
                                # "CEMP" and "CH" stars : define by [C/N]>0.5
                                if($CN>0.5)
                                {
                                    $count{CEMP} += $dtp;
                                    if($m>$Mlimit)
                                    {
                                        $count{extra_massive_CEMP} += $dtp;
                                    }
                                    if($CN>2.0)
                                    {
                                        if($m>$Mlimit)
                                        {
                                            $count{extra_massive_superCEMP} += $dtp;
                                        }
                                        $count{superCEMP} += $dtp;
                                    }
                                }
                            }
                        }
                    }

                }
            }
        }
    }

}


############################################################
# save histograms
############################################################

# make histograms
my %histoplots;
foreach my $k (sort %histograms)
{
    foreach my $stellar_type (sort keys %{$histograms{$k}})
    {
        my $file = '/tmp/histo-'.$k.'-'.$stellar_type.'.dat';
        open(my $fp, '>:raw:perlio',$file)||die("cannot open $file"); 
        print {$fp} "# $k - $stellar_type\n";

        my $plot = $k.' distribution';
        $histoplots{$plot}={
            filename => $file,
            gnuplot => "
set xlabel \"".setxlabel($k)."\"
set ylabel \"Number of stars (per bin)\"
set xrange[*:*]
set yrange[*:*]
",
           sum=>0.0,
        };
        
        # get minx, maxx, miny, maxy, bin size
        my $maxx = undef;
        my $minx = undef;
        my $miny = undef;
        my $maxy = undef;
        my $binsize = 1e200;
        foreach my $x (nsort keys %{$histograms{$k}{$stellar_type}})
        {
            my $y = $histograms{$k}{$stellar_type}{$x};
            $maxx = defined $maxx ? MAX($x, $maxx) : $x;
            $minx = defined $minx ? MIN($x, $minx) : $x;
            $maxy = defined $maxy ? MAX($y,$maxy) : $y;
            $miny = defined $miny ? MIN($y,$miny) : $y;
            my $b = $x - $minx;
            if(abs($b)>1e-50 && $b<$binsize)
            {
                $binsize = $b;
            }               
            $histoplots{$plot}{sum} += $y;
        }
        $histoplots{$plot}{sum} /= $maxy;
        $histoplots{$plot}{binsize} = $binsize;

        # pad data
        for(my $x = $minx - $binsize; $x<=$maxx + $binsize; $x+=$binsize)
        {
            $histograms{$k}{$stellar_type}{$x} //= 0.0;
        }
        $histoplots{$plot}{xrange} = [$minx - $binsize, $maxx + $binsize];
        
        # save normalized
        foreach my $x (nsort keys %{$histograms{$k}{$stellar_type}})
        {
            printf {$fp} "%g %g\n",$x,$histograms{$k}{$stellar_type}{$x}/$maxy;
        }
        
        close $fp;
    }
}


############################################################
# save data maps
############################################################


my $mapfilestub = '/tmp/binfracmap';
foreach my $map_type (@map_types)
{
    foreach my $stellar_type ('All')# (@stellar_types)
    {
        my $file = "$mapfilestub-$map_type-$stellar_type";
        my $numbermap = $numbermap{$map_type}{$stellar_type};
        my $binfracmap = $binfracmap{$map_type}{$stellar_type};

        if($numbermap && $binfracmap)
        {
            open(MAP,'>:raw:perlio',$file)||die;
            foreach my $m (nsort keys %{$numbermap})
            {
                foreach my $CN (nsort keys %{$numbermap->{$m}})
                {
                    # binary fraction
                    my $f = $numbermap->{$m}->{$CN} ? 
                        (sprintf '%g',$binfracmap->{$m}->{$CN}{1} / 
                         $numbermap->{$m}->{$CN}) : '-1';
                    
                    if(0)
                    {
                        print "At m=$m CN=$CN have binfrac ",
                        $binfracmap->{$m}->{$CN}{1}," and num ",
                        $numbermap->{$m}->{$CN},
                        "\n";
                    }

                    # all stats on the map
                    printf MAP "%g %g %g %s\n", $m,$CN,$numbermap->{$m}->{$CN}/$count{ntot},$f;
                }
                print MAP "\n";
            }
            close MAP;
        }
    }
}

#  blue straggler map
foreach my $map_type (@map_types)
{
    my $file = '/tmp/bssfracmap-'.$map_type;
    open(BSS,'>:raw:perlio',$file)||die;
    my $map = $bssmap{$map_type};
    foreach my $m (nsort keys %{$map})
    {
        foreach my $CN (nsort keys %{$map->{$m}})
        {
            printf BSS "%g %g %g\n",
            $m,$CN,
            $map->{$m}{$CN}{was_BSS}/
                (1e-100 + 
                 $map->{$m}{$CN}{was_BSS}+
                 $map->{$m}{$CN}{never_BSS});
        }
    }
    close BSS;
}

############################################################
#
# make surface plots
#
############################################################
if(!$opts{nognuplot})
{
open(GP,'>:utf8','/tmp/CNstats.plt')||die;
print GP "
set terminal pdfcairo enhanced font \"Helvetica,16\"
set output \"/tmp/CNstats.pdf\"
set pm3d map
set zero 0.0
set lmargin at screen 0.13
set rmargin at screen 0.82

set bmargin at screen 0.16
set tmargin at screen 0.9 
set format '%h'
set xtics offset 0,0.7
set ytics offset 1,0
set xlabel offset 0,1.3
set ylabel offset 0.8,0
";

my $titles = $ENV{CNOPTS}!~/notitle/;

foreach my $map_type (@map_types)
{
    foreach my $stellar_type (@stellar_types)
    {
        my $datafile = "$mapfilestub-$map_type-$stellar_type";
        if(-s -e -f $datafile)
        {
            my $surf = "$gpsurf $datafile";

            # per maptype ranges!
            my $xlabel = setxlabel($map_type);

            my $title = $stellar_type_strings{$stellar_type}.' stars';

            if(0){
            my $ll = '©';
            $ll='CNL';
            print GP "
set label \"$ll\" at graph 0.04,0.9 center font \"Helvetica-Bold,20\" front textcolor rgb \"#00FF00\"
set label \"$ll\" at graph 0.04,0.9 center font \"Helveticap-Bold,16\" front textcolor rgb \"#000000\"
";
            }
            print GP "
set cbrange[*:*]
unset colorbox


# greyscale palette
set palette defined (0 1 1 1, 1 0 0 0) positive


set xlabel \"$xlabel\"
set ylabel \"[C/N]\"

";
            if($titles)
            {
                print GP "\nset title \"$title : Number of stars\"\n";
            }
print GP "
set xrange[*:*]
set yrange[*:*]
splot \"<$surf  1 2 3\" u 1:(\$2+$CNoffset):3 notitle
";
            my $replot = 0;
            if($gpopts{$map_type}{xrange})
            {
                print GP "
set xrange $gpopts{$map_type}{xrange}
";
                $replot = 1;
            }
            if($gpopts{$map_type}{yrange})
            {
                print GP "
set yrange $gpopts{$map_type}{yrange}
";
                $replot = 1;
            }

            if($replot)
            {
                print GP "
replot
";
            }


            print GP "

set logscale cb
set xrange[*:*]
set yrange[*:*]
splot \"<$surf  1 2 3\" u 1:(\$2+$CNoffset):3 notitle";

            if($xlabel =~/^Mass/)
            {
                # overlay APOGEE results if given
                #print STDERR "Overlay?\n";
                if($ENV{CNOPTS}=~/overlay_obs=(\S+)/)
                {
                    #print STDERR "OVERLAY! $1\n";
                    print GP ", \"$1\" u 1:2:(10) w p lc 'red' ps 0.5 title \"\" ";
                    #print GP "\n";close GP;
                }
            }
            
                        if($gpopts{$map_type}{xrange})
            {
                print GP "
set xrange $gpopts{$map_type}{xrange}
";
                $replot = 1;
            }
            if($gpopts{$map_type}{yrange})
            {
                print GP "
set yrange $gpopts{$map_type}{yrange}
";
                $replot = 1;
            }

            if($replot)
            {
                print GP "
replot
";
            }

            if($titles)
            {
                print GP "set title \"$title : Binary fraction of stars\"
";
            }
            print GP "
unset logscale cb
set cbrange[0:1]
#set cblabel \"Binary Fraction\"

set palette defined (0 1 0 0, 1 0 0 1)

splot \"<$surf 1 2 4\" u 1:(\$2+$CNoffset):3 notitle

# combined binary fraction and number of stars
";
            if($titles)
            {
                print GP "set title \"$title : Combined number and binary fraction\"\n";
            }
            print GP "
            set cbrange[0:1]

# find min,max of the data
maxd = `get_max_from_column $datafile 3 2>/dev/null`
mind = `get_min_from_column $datafile 3 2>/dev/null`
logmind = log(mind)
dd = maxd - mind
dlogd = log(maxd) - logmind

# relative density function
reld(d) = (d - mind) / dd
logreld(d) = (log(d) - logmind) / dlogd

";
            foreach my $prefix ('','log')
            {
                print GP "
set yrange[*:*]
set xrange[*:*]
# d = density (lightness = value), f = binary fraction (color = hue)
color(d,f) = hsv2rgb(1.0 - f*0.35, ".$prefix."reld(d), 1.0)


unset logscale xy
splot \"<$surf 1 2 3 4\" u 1:(\$2+$CNoffset):3:(color(\$3,\$4)) notitle with pm3d lc rgb variable ";

                if($xlabel =~/^Mass/ || $xlabel=~/^Surface gravity/)
                {
                    # overlay APOGEE results if given

                    my $col = $xlabel =~/^Mass/ ? 1 : 6;

                    print STDERR "Overlay? $xlabel\n";
                    if($ENV{CNOPTS}=~/overlay_obs=(\S+)/)
                    {
                        print STDERR "OVERLAY! $1\n";
                        print GP ", \"$1\" u $col:2:(10) w p lc 'black' ps 0.3 title \"\" ";
                        #print GP "\n";close GP;
                    }
                }
                           if($gpopts{$map_type}{xrange})
            {
                print GP "
set xrange $gpopts{$map_type}{xrange}
";
                $replot = 1;
            }
            if($gpopts{$map_type}{yrange})
            {
                print GP "
set yrange $gpopts{$map_type}{yrange}
";
                $replot = 1;
            }

            if($replot)
            {
                print GP "
replot
";
            }
 
            }
            
            print GP "
unset cblabel 

# reset to default greyscale
set palette defined (0 1 1 1, 1 0 0 0) positive

";

        }
    }
}

# BSS map
if($titles)
{
    print GP "

# blue straggler fraction map
set title \"Fraction of stars that were blue stragglers\"

"
}

print GP "
set xrange[*:*]
set yrange[*:*]
set cbrange[-0.2:1]
";

foreach my $map_type (@map_types)
{
    my $xlabel = setxlabel($map_type);
    print GP "
set xlabel \"$xlabel\"
set ylabel \"[C/N]\"
splot \"<$gpsurf /tmp/bssfracmap-$map_type 1 2 3\" u 1:(\$2+$CNoffset):3 notitle
";
}

############################################################
# make histogram plots
############################################################

foreach my $plot (sort keys %histoplots)
{
    print "output histogram $histoplots{$plot}{title}, data in $histoplots{$plot}{filename}\n"; 
    print GP $histoplots{$plot}{gnuplot}//'';
    print GP "set title $histoplots{$plot}{title}
set yrange[-0.05:1.05]
set xrange[".join(':',($histoplots{$plot}{xrange}//['*','*']))."
set bmargin at screen 0.15
set xtics offset 0,0.3
set xlabel offset 0,0.4
plot \"$histoplots{$plot}{filename}\" u 1:2 w boxes notitle
set yrange[*:*]

# cumulative
__cumulate_sum=0

set yrange[0:1.05]
set ylabel \"Fraction of stars\"
plot \"$histoplots{$plot}{filename}\" u 1:(cumulate(\$2)/$histoplots{$plot}{sum}) w l notitle
";
}


############################################################
# make colour scale boxes
############################################################

make_colorbox_data();


foreach my $prefix ('','log')
{
    print GP "
maxd = 1.0
mind = 0.0
logmind = log(mind)
dd = maxd - mind
dlogd = log(maxd) - logmind

# relative density function
reld(d) = (d - mind) / dd
logreld(d) = (log(d) - logmind) / dlogd

set xrange[*:*]
# d = density (lightness = value), f = binary fraction (color = hue)
color(d,f) = hsv2rgb(1.0 - f*0.35, ".$prefix."reld(d), 1.0)

# horizontal : x = stellar frequency, y = binary fraction 

set format x '".($prefix eq 'log' ? '10^{%T}' : '%h')."'
set format y '%g'
set xrange[*:*]
set yrange[0:1]
".($prefix eq 'log' ? '' : 'un' )."set logscale x
unset logscale y
set xtics autofreq
set ytics (\"0.0\" 0.0, \"0.5\" 0.5, \"1.0\" 1.0)
set xlabel \"Normalized stellar frequency\"
set ylabel \"Binary fraction\"
set size ratio 0.2
set bmargin at screen 0.16
splot \"/tmp/cbox.dat\" u 1:2:2:(color(\$1, \$2)) notitle with pm3d lc rgb variable 

# vertical : x = binary fraction, y = stellar frequency

set format x '%g'
set format y '".($prefix eq 'log' ? '10^{%T}' : '%h')."'
set xrange[0:1]
set yrange[*:*]
unset logscale x
".($prefix eq 'log' ? '' : 'un' )."set logscale y
set xtics (\"0.0\" 0.0, \"0.5\" 0.5, \"1.0\" 1.0)
set ytics autofreq
set xlabel \"Binary fraction\"
set ylabel \"Normalized stellar frequency\" rotate 90
set size ratio 4
set bmargin at screen 0.2
splot \"/tmp/cbox.dat\" u 2:1:2:(color(\$1, \$2)) notitle with pm3d lc rgb variable 
"
}

############################################################
# do plot
############################################################

close GP;

    print `gnuplot < /tmp/CNstats.plt 2>\&1 |grep -v usable`;
    print "See /tmp/CNstats.pdf\n\n";
}

############################################################

# output counts
print "Counts relative to ntot = $count{ntot}\n\n";
foreach my $k (sort keys %count)
{
    printf "% 25s : %10.3e : % 8.2f \%\n",
    $k,
    $count{$k},
    100.0*$count{$k}/(1e-100+$count{ntot});
}

print "\nExtra-massive giants relative to the total number of extra massive giants\n";
foreach my $k (sort grep {/^extra_massive/}  keys %count)
{
    printf "% 25s : %10.3e : % 8.2f \%\n",
    $k,
    $count{$k},
    100.0*$count{$k}/(1e-100+$count{extra_massive_giant});
}

exit;

############################################################

sub setxlabel 
{
    my ($xlabel) = @_; 
    state $msun =  "\x{2609}";
    $xlabel =~ s/^mass/Mass {\/:Italic M\}\/ M_\{$msun\}/;
    $xlabel =~ s/^core_mass/Core mass \{\/:Italic M}_c \/ M_\{$msun\}/;
    $xlabel =~ s/^logg/Surface gravity log_\{10\} ({\/:Italic g} \/ cm s^\{-2\})/;
    return $xlabel;
}

sub plot
{
    my ($plotstring) = @_;
    print GP "

";
}

sub make_colorbox_data
{
    open(my $boxdata,'>','/tmp/cbox.dat')||die;

    for(my $i=-12;$i<=0.0;$i+=0.01)
    {
        for(my $j=0.0;$j<=1.0;$j+=0.01)
        {
            printf {$boxdata} "%g %g\n",10.0**$i,$j; 
        }
        print {$boxdata} "\n";
    }
    close $boxdata;
}
