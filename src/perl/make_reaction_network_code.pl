#!/usr/bin/env perl
use Math::Algebra::Symbols;
use strict;
use rob_misc;


# script to make the code for rob's reaction network

open(FP,"<networks")||die; # contains network info

my $network; # current network
my %nethash;
my %species;

# set these to ignore H1 and He4 in the network
my $ignore_H1=0;
my $ignore_He4=0;
my $ignore_gamma=1; # ignore escaping photons, should always be 1
my $ignore_beta=1; # ignore beta decay electrons, should always be 1
my $ignore_nu=1; # ignore neutrinos, should always be 1
my $ignore_n=1; # ignore neutrons, might be 1
my $ignore_e=1; # ignore electrons (they're fudged into the network)

my $netselect = $ARGV[0]; # perhaps choose on cmd line?
my $incomment = 0;
my @eqlist;
my @eqreactions;
my @reaclist;

while(<FP>)
{
    s/^\s+//o; # remove whitespace
    chomp;
    next if /^\#/o;
    s/\#.*//o; # remove everything after a comment

    # conversions
    s/gamma/gamma0/g;

    if(/exit/)
    {
        flush_network();
        close FP;
    }
    elsif(/skip/)
    {
        cleanup();
    }
    elsif(/Network\s*(.*)/)
    {
        flush_network();
        $network=$1;
        #print "Set network $1\n";
    }
    elsif(/(\S+) equilibrium/)
    {
        # this species is assumed to be in equilbrium
        push(@eqlist,$1);
    }
    elsif(/(SIGMAV_\S+) (\d*)(.*)\(([\d\.]*)(.*),([\d\.]*)(.*)\)(\d*)(.*)/)
    {
        #print "$1: $2*$3+$5*$5->$6*$7+$8*$9\n";

        # if we have a reaction m alpha (n beta, p gamma) q delta
        # (where m,n,p,q are integers) then:
        # m*alpha react: this consumes n*beta, and makes p*gamma and q*delta
        # alternatively:
        # 1*alpha reacts: this consumes (n/m)*beta, and makes (p/m)*gamma and (p/m)*delta
        #
        # so d(alpha)/dt = -<sigmav> alpha*beta
        #    d(beta)/dt  = -(n/m) <sigmav> alpha*beta
        #    d(gamma)/dt =  (p/m) <sigmav> alpha*beta
        #    d(delta)/dt =  (q/m) <sigmav> alpha*beta
        #
        # We have problems with reactions with doubled species:
        # e.g. H1(H1,beta+)H2 which could be written 2H1(,beta+)H2
        #
        # then m=2, n=0, p=q=1 (neglect the beta+)
        #
        # dH1/dt = -<sigmav> H*H
        # dH2/dt = (1/2) <sigmav> H*H
        #
        # as required. If we assume H(H,beta+)D we get m=n=p=q=1
        #
        # dH/dt = -<sigmav> H*H
        # dH/dt = -<sigmav> H*H
        # dD/dt = <sigmav> H*H
        #
        # which is not quite right! So provided we group like species we
        # should be fine: checks are included for simple cases!
        #print "REAC $_\n";
        my ($reacn,$m,$alpha,$n,$beta,$p,$gamma,$q,$delta) = ($1,$2,$3,$4,$5,$6,$7,$8,$9);
        #print "FOUND ",join(':',$reacn,$m,$alpha,$n,$beta,$p,$gamma,$q,$delta),"\n";

        my $c=0;
        my $str; #="N[X$alpha]*N[X$beta]*sigmav[$reacn] ";
        my $diffeq;

        map{$_=1 if(!okstring($_));}($m,$n,$p,$q);
        map{$_=~s/\s+//go;}($m,$n,$p,$q,$alpha,$beta,$gamma,$delta);

        # check for same input species
        if($alpha eq $beta)
        {
            $m += $n;
            undef $n;
            undef $beta;
        }


        my $l = "/*\n * Reaction $_\n * Alpha=$alpha, beta=$beta, gamma=$gamma, delta=$delta\n * m=$m, n=$n, p=$p, q=$q\n */\n";
        push(@reaclist,$l);
        #print $l;

        if(okstring($alpha))
        {
            for(my $i=0;$i<$m;$i++)
            {
                $str .= "*N[X$alpha]";
            }
        }
        if(okstring($beta))
        {
            for(my $i=0;$i<$n;$i++)
            {
                $str .= "*N[X$beta]";
            }
        }


        $str .= "*sigmav[$reacn] ";
        $str =~ s/^\*//o;

        # check for same output species
        if($gamma eq $delta)
        {
            $q += $p;
            undef $p;
            undef $gamma;
        }

        # save species for later replacement
        map
        {
            if(defined $_ &&
               !/^\s*$/o)
            {
                $species{$_} = 1;
            }
        }($alpha,$beta,$gamma,$delta);

        foreach my $input ($alpha,$beta)
        {
            if(okstring($input))
            {
                # destruction of input
                {
                    my $x = $m!=1.0 ? (1.0/$m.'*') : '';
                    $nethash{$input} .= "-$x$str";
                }
                # creation of output
                if($c == 0)
                {
                    $c = 1;
                    my $string;
                    if(okstring($gamma))
                    {
                        my $x = $p/$m;
                        $nethash{$gamma} .= '+'.($x!=1.0 ? "$x*" : '').$str;
                    }
                    if(okstring($delta))
                    {
                        my $x = $q/$m;
                        $nethash{$delta} .= '+'.($x!=1.0 ? "$x*" : '').$str;
                    }
                }
            }
        }
    }

}
flush_network();


sub flush_network
{
    #print "Flush $network\n";
    return if(!okstring($network));

    # remove equilibrium reactions
    remove_eq_reactions($network);

    if(!(okstring($netselect)&&
         ($network ne $netselect)))
    {
        print "

\#include \"../binary_c.h\"
No_empty_translation_unit_warning;

\#ifdef NUCSYN

/*
 * Network code : $network
 * Automatically generated by make_reaction_network_code.pl
 */

@reaclist

/*
 * Species / Reactions list:
 *
";

        # sort list of species
        my @species = sort {
            (($a=~/(\d+)/)[0] // 0) <=> (($b=~/(\d+)/)[0] // 0);
        } keys %species;

        # remove unwanted species
        my @s2;
        my $extra_electrons_start;
        my $extra_electrons_end;
        push(@s2,'');
        map
        {

            if(0 && $_ eq 'e')
            {
                # boost the e-capture reaction in PPIII to pretend we are including electrons in the network
                $extra_electrons_start="//make sure this code is in the function which calls this function//const double edens=nucsyn_free_electron_density(Nin,stardata->common.atomic_number);\n\t//sigmav[SIGMAV_Be7_e]*=edens;\n";
                $extra_electrons_end='//sigmav[SIGMAV_Be7_e]/=edens;';
            }
            elsif(($_ ne 'H1' || $ignore_H1!=1)&&
                  ($_ ne 'He4' || $ignore_He4!=1)&&
                  ($_ ne 'gamma0' || $ignore_gamma!=1)&&
                  ($_ ne 'beta' || $ignore_beta!=1)&&
                  ($_ ne 'nu' || $ignore_nu!=1)&&
                  ($_ ne 'n' || $ignore_n!=1)&&
                  #($_ ne 'e' || $ignore_e!=1)&&
                  ($species{$_}==1)
                )
            {
                push(@s2,$_);
            }
        }@species;
        @species=@s2;
        # print "SPECIES @species\n";

        my %species_hash; # reverse lookup hash
        for(my $i=1;$i<=$#species;$i++)
        {
            my $x = $nethash{$species[$i]};
            $x=~s/N\[X//g;
            $x=~s/sigmav\[//g;
            $x=~s/\]//go;

            # remove 2*0.5
            #$x=~s/2\*0.5\*//g;

            print " * $i : $species[$i] : $x\n";
            $species_hash{$species[$i]} = $i;
        }
        shift(@species);

        # list for y array
        my $n  = $#species+1;
        my $n2 = $n+1;

        map
        {
            print " * ...  $_ is assumed to be in equilibrium\n";
        }@eqlist;

        print " *\n */

                                                                                                                                                                                                                                      ";

        print "void $network"."jacobian(double ** RESTRICT J, const double * RESTRICT N, const double * RESTRICT sigmav);
void $network"."derivatives(const double * RESTRICT y,double * RESTRICT dydt,const double * RESTRICT sigmav,const double * RESTRICT N MAYBE_UNUSED);

double nucsyn_burn_$network (double * RESTRICT const Nin,
                             const double * RESTRICT const sigmav,
                             const double h)
\{

        ";
        print "\tstatic double *J$network\[$n2\];
        \tdouble err;
        \tunsigned int i;

\tfor(i=1;i<=$n;i++)
\t\{
\t\tJ$network\[i\] = Malloc($n2\*sizeof(double));
\t\}

\tstatic double y$network\[$n2\],y2$network\[$n2\],f1$network\[$n2\],f2$network\[$n2\],f3$network\[$n2\],f4$network\[$n2\];
\tstatic int indx$network\[$n2\];
";

        map
        {
            print "\ty$network\[$species_hash{$_}\]=Nin\[X$_\];\n";
        }@species;

        print "\n
\terr = kaps_rentrop_GSL(h,
                         y$network,
                         y2$network,
                         $n,
                         J$network,
                         sigmav,
                         Nin,
                         \&$network"."jacobian,
                         \&$network"."derivatives,
                         f1$network,
                         f2$network,
                         f3$network,
                         f4$network,
                         indx$network);\n";
        print "
\tfor(i=1;i<=$n;i++)
\t\{
\t\tSafe_free(J$network\[i\]);
\t\}

        ";

        print "\tif(err<=1.0)\n\t{\n";

        map
        {
            print "\t\tNin[X$_]=y$network\[$species_hash{$_}\];\n";
        }@species;


        map
        {
            s/N\[/Nin\[/go;
            print "\t\t$_\n";
        }@eqreactions;
        print "\t}\n";
        if(defined($extra_electrons_end))
        {
            print "\t/* decrease Be7 rate back to what it should be */
                                                                                                                                                                                              $extra_electrons_end
                                                                                                                                                                                              ";
        }
        print "\treturn err;

        }
        ";


        # derivatives function
        print "void $network"."derivatives(const double * RESTRICT y,double * RESTRICT dydt,const double * RESTRICT sigmav,const double * RESTRICT N MAYBE_UNUSED)
\{
    ";

        my $derivatives;
        map
        {
            # substitute for y where appropriate
            my $d=$nethash{$_};
            foreach my $s (@species)
            {
                $d=~s/N\[X($s)\]/y\[$species_hash{$1}\]/g;
            }

            # check for beta decays (we then deal in timescale, not cross section)
            $d=~s/\*N\[Xbeta\]\*sigmav/\/sigmav/;
            # ditto for electron captures
            $derivatives.= "\t dydt[$species_hash{$_}]=$d;\n";

        }@species;

        $derivatives=~s/N\[Xe\]\*//go; # remove Xe
        $derivatives = optimize_func($derivatives);

        print "$derivatives\n\}\n\n\n/*\n * Jacobian\n */\n\n\n
                                                              void $network"."jacobian(double ** RESTRICT J,const double * RESTRICT N, const double * RESTRICT sigmav)\n\{\n
                                                                                                                                                                              ";

        # Jacobian function
        my $jacobian;
        foreach my $y (@species)
        {
            foreach my $x (@species)
            {
                my $i = $species_hash{$x};
                my $j = $species_hash{$y};
                my $elem;

                # calculate derivative
                my $d=$nethash{$x};
                $d=~s/\s+//g;

                # use symbolic algebra to calculate the derivative
                my ($eq,%ascconv) = convasc($d);

                my $diffvar = $ascconv{"N\[X$y\]"};

                if(defined($diffvar))
                {
                    my $cmd = algebra_header(%ascconv).' my $sol=('.$eq.')->d(qw('.$diffvar.')); return $sol;';
                    my $solution = eval $cmd;
                    $elem = convback($solution,%ascconv);
                }

                $jacobian .= "\t J\[$i\]\[$j\] = ".($elem ? $elem : 0.0).";\n";
            }
        }
        # fix beta decays
        $jacobian=~s/N\[Xbeta\]\*/1.0\//go;

        # and electron capture
        #$jacobian=~s/N\[Xe\]\*//go; # remove Xe

        $jacobian=optimize_func($jacobian);

        $jacobian=sort_jacobian($jacobian);

        print "$jacobian\n\}\n\n\#endif\n\n";

    }
    cleanup();
}

sub cleanup
{
    @reaclist = ();
    @eqlist = ();
    @eqreactions = ();
    $network = '';
    %species = ();
    %nethash = ();

}


sub optimize_func
{
    my ($f) = @_;

    # optimize a function by replacing calls to multiple
    # identical expressions with pre-calculated expressions

    # will not work on anything complicated, but might work on rob's
    # auto-generated code

    # make sure - or + are space-padded
    $f=~s/([\-\+])/ $1 /go;

    # split into lines
    my @f = split(/\n/o,$f);

    my %expression_count;
    foreach my $l (@f)
    {

    # remove + or -
    $l=~s/[\+\-]//go;
    $l=~s/;//go;

    #print "line $l\n";

    # get everything on the RHS
    if($l=~/=\s+(.*)/o)
    {
        my $c=$1;
        my @c=split(/[\s\)\(\/]+/o,$c);

        # remove simple factors
        map
        {
        s/^(\d+(?:\.\d+)?)\*?//;
        }@c;

        # remove brackets
        map
        {
        s/[\(\)]//;
        }@c;

        # remove empty strings
        my @d;
        map
        {
        if($_ ne '')
        {
            push(@d,$_);
        }
        }@c;
        @c=@d;

        #print "Exp ",join(',',@c),"\n";

        # now get a count of expressions
        map
        {
        $expression_count{$_}++;
        }@c;
    }
    }

    my $precode;
    my $precode_count=0;

    my @expressions=reverse sort
    {
        my ($la,$lb) = (length($a),length($b));
        return $la<$lb ? -1 : $la>$lb ? +1 : 0;
    }
    keys %expression_count;

    #print "EXPRE @expressions\n";#exit;

    foreach my $c (keys %expression_count)
    {
        if(($expression_count{$c}>=2)&&
           (!is_numeric($c)))
        {
            $precode.="\tconst double dummy$precode_count=$c ;\n";
            # format expression as regexp
            while($c=~s/([^\\])([\*\[\]])/$1\\$2/g){};
            $c =~ s/([\(\)])/\\$1/g;
            #print "REGEXP $c\n";
            $f=~s/$c/dummy$precode_count/g;
            $precode_count++;
        }
    }
    $f=$precode.$f;

    # fix factors, should be floats
    $f=~s/ (\d)\*/ $1.0\*/g;

    return $f;
}


sub okstring
{
    return (defined $_[0] && $_[0] ne '' && $_[0]!~/^\s+$/) ? 1: 0;
}

sub remove_eq_reactions
{
    # automatically remove equilibrium species
    #print "Eq reactions @eqlist\n";
    #map
    #{
    #    s/N\[Xe\]\*//go;
    #}@eqlist;

    # clean up nethash
    my %nh;
    map
    {
        if(defined $_ &&
           defined $nethash{$_})
        {
            $nh{$_} = $nethash{$_};
        }
    }keys %nethash;
    %nethash=%nh;

    foreach my $eqspecies (@eqlist)
    {
        my $reaction=$nethash{$eqspecies};
        $reaction=~s/N\[Xe\]\*//go;
        $reaction=~s/([\-\+])/ $1/g;
        my $orig=$reaction;
        my ($eq,%ascconv)=convasc($reaction);
        $eq='0 eq '.$eq;
        my $solvefor=$ascconv{"N\[X$eqspecies\]"};

        my $intermsof=join(' ',values(%ascconv));
        my $cmd=algebra_header(%ascconv).' my $sol=('.$eq.')->solve(qw('.$solvefor.' in terms of '.$intermsof.')); return $sol;';
        my $solution=eval $cmd;

        $solution=convback($solution,%ascconv);
        $solution=~s/N\[Xe\]\*//go; # electron numbers are folded into reaction rates
        push(@eqreactions,"N\[X$eqspecies\] =  $solution; /* $eqspecies in equilibrium */");

        # substitute into the network
        $solution=convback($solution,%ascconv);

        map
        {
            if($nethash{$_}=~s/N\[X$eqspecies\]/$solution/)
            {
                $nethash{$_}=~s/\+\+/\+/g;
                $nethash{$_}=simplify_reaction($nethash{$_});
            }
        }keys %nethash;

        # clean up nethash

        map
        {
            if(defined($_)&&defined($nethash{$_}))
            {
                $nh{$_}=$nethash{$_};
            }
        }keys %nethash;
        %nethash=%nh;

        # remove the $eqspecies from the net hash and the %species
        undef($nethash{$eqspecies});
        $species{$eqspecies}=-1;
    }

}

sub simplify_reaction
{
    my $vb=0;
    my $equation_orig=shift;
    #print "INITIAL $equation_orig\n"if($vb);
    $equation_orig=~s/N\[Xe\]\*//go; # remove electrons

    # convert to ascii variable names so we can do algebra
    my ($equation,%ascconv)=convasc($equation_orig);

    # make command to simplify the equation
    my $cmd=algebra_header(%ascconv).' my $simp=eval($equation); return $simp;';
    $equation=eval $cmd;

    # convert back to original form
    $equation=convback($equation,%ascconv);

    # return
    return $equation;
}

sub algebra_header
{
    my %ascconv=@_;
    my @symbols=sort values %ascconv;
    my @s2=@symbols;
    map{$_='$'.$_;}@s2;
    return('my ('.join(',',@s2).')=symbols(qw('.join(' ',@symbols).'));');
}

sub order_eq
{
    my $eq=shift;


    # put the sigmavs after the N[]
    while($eq=~s/(sigmav\[[^\]]+\])\*(N\[[^\]]+\])/$2\*$1/g){};

    # get the order of N[X] N[Y] and sigmav correct
    foreach my $s ('N','sigmav')
    {
        while(reorder($s,$eq)==1)
        {
            $eq=~s/($s\[[^\]]+\])\*($s\[[^\]]+\])/$2\*$1/;
        }
    }


    return $eq;
}

sub reorder
{
    my $eq=shift;
    my $string=shift;
    my $state=0;
    if($eq=~/$string\[([^\]]+)\]\*$string\[([^\]]+)\]/)
    {
        $state = $1 cmp $2;
    }
    return ($state);
}


sub convasc
{
    # convert an expression to ascii
    my $vb = 0;
    my $equation_orig=shift;
    my %factors;
    my @factors;
    my $equation=$equation_orig;
    #print "ORIG EQ $equation\n";

    while($equation=~s/((?:N|sigmav)\[[^\]]+\])//)
    {
        $factors{$1}=1;
    }
    @factors=keys %factors;

    $equation=$equation_orig;
    map
    {
        my $y=$_;
        $y=~s/([\[\]\*])/\\$1/g;
        $equation=~s/$y/\$$_/g;
    }@factors;
    if($vb)
    {
        print "EQ $equation\n";
        print "FACTORS @factors\n";
    }

    my %ascconv;
    # convert factors to ascii names
    my $asccount=97;

    for(my $i=0; $i<=$#factors; $i++)
    {
        $ascconv{$factors[$i]}='NetW'.chr($asccount);
        print "Assign $factors[$i] to ascii $ascconv{$factors[$i]}\n"if($vb);
        $asccount++;

        # convert equation
        my $r=$factors[$i];
        $r=~s/([\[\]\*])/\\$1/g;
        $equation=~s/$r/$ascconv{$factors[$i]}/g;
    }
    print "EQ is now $equation\n"if($vb);

    return ($equation, %ascconv);
}

sub convback
{
    my $equation=shift;
    my %ascconv=@_;
    # convert back to original form

    $equation=~s/(1\/2)/0.5/go;
    $equation=~s/(3\/2)/1.5/go;
    $equation=~s/(\$NetW[a-z])\*\*2/$1\*$1/go;
    $equation=~s/(\$NetW[a-z])\*\*3/$1\*$1\*$1/go;

    map
    {
        $equation=~s/$ascconv{$_}/$_/g;
    }keys %ascconv;


    $equation=~s/\$//go;
    $equation=~s/^/+/o if(!($equation=~/^[\-\+]/));
    $equation=~s/([\+\-])/ $1/g;

    $equation=order_eq($equation);

    return($equation);
}

sub sort_jacobian
{
    # function to force jacobian into C order (should be faster?)
    my ($jacobian) = @_;

    $jacobian=~s/(J\[.*\].*\n)/__HERE__/s;
    my $j=$1;
    my @j=split(/\n/o,$j);

    @j= sort {
        my ($i1,$j1) = ($a=~/\[(\d+)\]\[(\d+)\]/g);
        my ($i2,$j2) = ($b=~/\[(\d+)\]\[(\d+)\]/g);
        return $i1 <=> $i2 || $j1 <=> $j2;
    }@j;

    $j=join("\n",@j);
    $jacobian=~s/__HERE__/$j/;

    return $jacobian;
}
