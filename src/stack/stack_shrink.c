#include "../binary_c.h"
No_empty_translation_unit_warning;

void stack_shrink(struct stardata_t * Restrict const stardata,
                  struct binary_c_stack_t * const Restrict stack)
{
    /*
     * Attempt to shrink passed in stack.
     */
    const size_t want_alloc = Max((size_t)1,stack->allocated);
    if(stack->allocated > want_alloc)
    {
        void * const new_items = Realloc(stack->items,
                                         sizeof(void*)*want_alloc);
        if(new_items)
        {
            stack->items = new_items;
            stack->allocated = want_alloc;
        }
    }
}
