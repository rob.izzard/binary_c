#include "../binary_c.h"
No_empty_translation_unit_warning;

void stack_free(struct binary_c_stack_t ** Restrict stackp)
{
    /*
     * Free stack (pointer to which is passed in)
     * and its array of items.
     */
    if(stackp != NULL)
    {
        struct binary_c_stack_t * stack = *stackp;
        if(stack != NULL)
        {
            Safe_free(stack->items);
            Safe_free(stack);
        }
    }
}
