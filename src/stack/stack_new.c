#include "../binary_c.h"
No_empty_translation_unit_warning;

struct binary_c_stack_t * stack_new(void)
{
    /*
     * Allocate new stack and return it
     */
    struct binary_c_stack_t * const stack =
        Malloc(sizeof(struct binary_c_stack_t));

    if(stack != NULL)
    {
        stack->items = NULL;
        stack->count = 0;
        stack->allocated = 0;
    }

    return stack;
}
