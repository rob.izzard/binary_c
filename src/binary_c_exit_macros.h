#pragma once
#ifndef BINARY_C_EXIT_MACROS_H
#define BINARY_C_EXIT_MACROS_H
/*
 */
/*
 * The binary_c stellar population nucleosynthesis framework.
 *
 * Copyright Robert Izzard 2000-2025
 *
 * Contact rob.izzard@gmail.com
 *
 * Code repository:
 * https://gitlab.com/binary_c
 *
 * Documentation is in doc/
 * Code is in src/
 *
 * To build, run
 *
 * ---
 *
 * meson setup builddir
 * cd builddir
 * ninja binary_c_install
 *
 * ---
 *
 * Please see the files README, LICENCE and CHANGES,
 * and the doc/ directory for documentation which is mirrored
 * at https://binary_c.gitlab.io
 *
 **********************
 *
 * Purpose:
 *
 * This file contains the function macros used by binary_c
 * to allow it to exit safely. The most commonly used is
 *
 * Exit_binary_c(...)
 *
 * which takes arguments expected to be
 * 1) error code (see binary_c_error_codes.h)
 * 2) format statement (see printf)
 * 3) optional variables for the format statement
 *
 **********************
 */

#include <errno.h>

/*
 * Macro to exit binary_c: the args passed in are
 * sent to exit_binary_c_with_stardata, and are
 * expected to be
 * 1: Error code
 * 2: sprintf format statement
 * 3: (optional) variables associated with the format statement
 */
#define Exit_binary_c(...)                          \
    {                                               \
        int errsv = errno;                          \
        binary_c_breakpoint(stardata);              \
        exit_binary_c_with_stardata(stardata,       \
                                    __FILE__,       \
                                    __LINE__,       \
                                    errsv,          \
                                    __VA_ARGS__);   \
                                                    \
    }

/*
 * varargs version of Exit_binary_c
 */
#define vExit_binary_c(...)                         \
    {                                               \
        int errsv = errno;                          \
        binary_c_breakpoint(stardata);              \
        vexit_binary_c_with_stardata(stardata,      \
                                     __FILE__,      \
                                     __LINE__,      \
                                     errsv,         \
                                     __VA_ARGS__);  \
                                                    \
    }

/*
 * Exit call when stardata is not available
 */
#define Exit_binary_c_no_stardata(...)          \
    {                                           \
        int errsv = errno;                      \
        binary_c_breakpoint(NULL);              \
        exit_binary_c_no_stardata(__FILE__,     \
                                  __LINE__,     \
                                  errsv,        \
                                  __VA_ARGS__); \
    }

/*
 * varargs version of Exit_binary_c_no_stardata
 */
#define vExit_binary_c_no_stardata(...)             \
    {                                               \
        int errsv = errno;                          \
        binary_c_breakpoint(NULL);                  \
        vexit_binary_c_no_stardata(__FILE__,        \
                                   __LINE__,        \
                                   errsv,           \
                                   __VA_ARGS__);    \
    }

#define MAX_EXIT_STATEMENT_PRINT_SIZE (KIBIBYTE*16)

#define Exit_or_return_void(RETVAL,...)                         \
    {                                                           \
        if(stardata &&                                          \
           stardata->preferences &&                             \
           stardata->preferences->batchmode==BATCHMODE_LIBRARY) \
        {                                                       \
            uncatch_timeouts();                                 \
            return ;                                            \
        }                                                       \
        else                                                    \
        {                                                       \
            Exit_binary_c(RETVAL,__VA_ARGS__);                  \
        }                                                       \
    }


/*
 * If we're in library batchmode, return, otherwise exit.
 */

#define Exit_or_return(RETVAL,...)                              \
    {                                                           \
        if(stardata &&                                          \
           stardata->preferences &&                             \
           stardata->preferences->batchmode==BATCHMODE_LIBRARY) \
        {                                                       \
            uncatch_timeouts();                                 \
            return RETVAL;                                      \
        }                                                       \
        else                                                    \
        {                                                       \
            Exit_binary_c(RETVAL,__VA_ARGS__);                  \
        }                                                       \
    }

/*
 * Assert macro: if X is false, exit with the args given
 * after X and with the BINARY_C_ASSERT_FAILED error code.
 */
#define Binary_c_assert(X,...)                  \
    if(!(X))                                    \
    {                                           \
        Exit_binary_c(BINARY_C_ASSERT_FAILED,   \
                      __VA_ARGS__);             \
    }

#endif//BINARY_C_EXIT_MACROS
