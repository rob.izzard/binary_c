#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef GALACTIC_MODEL
double apparent_luminosity(struct star_t * star,
                           struct stardata_t * stardata)
{
    return star->luminosity / stardata->common.distance;
}
#endif
