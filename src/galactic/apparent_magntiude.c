#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef GALACTIC_MODEL
double apparent_magnitude(struct star_t * star,
                          struct stardata_t * stardata)
{
    return
        SOLAR_APPARENT_MAGNITUDE -
        2.5 * log10(star->luminosity/
                    Pow2(stardata->common.distance));
}
#endif
