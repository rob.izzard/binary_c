#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef GALACTIC_MODEL
double bolometric_magnitude(struct star_t * star)
{
    return 4.75 - 2.5*log10(star->luminosity);
}
#endif
