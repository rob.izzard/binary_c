#ifndef EQUATION_OF_STATE_PROTOTYPES_H
#define EQUATION_OF_STATE_PROTOTYPES_H

#ifdef EQUATION_OF_STATE_ALGORITHMS
#include "equation_of_state.h"

void equation_of_state(Equation_of_state_args_in);

#ifdef EQUATION_OF_STATE_ENABLE_ALGORITHM_PACZYNSKI
void equation_of_state_paczynski(Equation_of_state_args_in);
#endif

#endif // EQUATION_OF_STATE_ALGORITHMS
#endif // EQUATION_OF_STATE_PROTOTYPES_H
