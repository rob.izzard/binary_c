#ifndef EQUATION_OF_STATE_H
#define EQUATION_OF_STATE_H

#define Equation_of_state_args_in               \
    struct stardata_t * stardata,               \
        struct equation_of_state_t * eq,        \
        int algorithm

#define Equation_of_state_args                  \
    stardata,                                   \
        eq,                                     \
        algorithm

#endif // EQUATION_OF_STATE_H
