#include "../binary_c.h"
No_empty_translation_unit_warning;


double dust_condensation_temperature(const struct star_t * Restrict star Maybe_unused)
{
    /*
     * Dust condensation temperature (K) as in
     * Abate et al. 2014,2015
     *
     * CAB: we need to calculate the dust formation radius Rd
     *      Hoefner (2007) relates Rd with R_*, T_eff, T_cond, where
     *      T_cond is the condensation temperature of the dust, depends on the composition
     *              T_cond = 1500 K for C/O>1
     *                     = 1000 K for C/O<1
     */
    double T;
#ifdef NUCSYN
    if(star->Xenv[XC12] / star->Xenv[XO16] * 16.0/12.0 > 1.0)
    {
        /* carbon-rich stars */
        T = 1500.0;
    }
    else
    {
        /* oxygen-rich stars */
        T = 1000.0;
    }
#else
    /*
     * No abundance information
     */
    T = 1000.0;
#endif

#ifdef CFDENISE
    T= 1500.0;
#endif

    return T;
}
