#pragma once
#ifndef DUST_PROTOTYPES_H
#define DUST_PROTOTYPES_H

double Pure_function dust_formation_radius(const struct star_t * Restrict const star);

double Pure_function dust_condensation_temperature(const struct star_t * Restrict const star Maybe_unused);


double Pure_function dust_formation_radius_given_condensation_temperature(const struct star_t * Restrict const star,
                                                                          const double Tcond);
#endif // DUST_PROTOTYPES_H
