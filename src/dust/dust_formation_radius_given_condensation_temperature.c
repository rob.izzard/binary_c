#include "../binary_c.h"
No_empty_translation_unit_warning;


double Pure_function dust_formation_radius_given_condensation_temperature(const struct star_t * Restrict const star,
                                                                          const double Tcond)
{
    /*
     * Dust formation radius as in
     * Abate et al. 2014,2015
     */
    return 0.5 * star->radius *
        Pow2p5(Teff_from_star_struct(star) / Tcond);
}
