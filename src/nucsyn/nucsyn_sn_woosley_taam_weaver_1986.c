#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef NUCSYN

void nucsyn_sn_woosley_taam_weaver_1986(Abundance * Restrict X)
{
    /*
     * Woosley, Taam and Weaver 1986 give yields for accretion onto a progenitor
     * star of mass 0.664 M_sun. Newer yields, with modern codes, would be good
     * but I cannot find any in the literature. Assume dm is the mass of the
     *  exploding star (and its envelope)
     * and assume the yields are the typical of any star around that mass.
     */
    double wtw__factor=(1.0/0.664);
    Clear_isotope_array(X);
    X[XHe4] += 1.7e-1 * wtw__factor;
    X[XC12] += 1.3e-3 * wtw__factor;
    X[XO16] += 1.5e-6 * wtw__factor;
    X[XNe20] += 2.3e-6 * wtw__factor;
    X[XMg24] += 6.4e-6 * wtw__factor;
    X[XSi28] += 7.3e-5 * wtw__factor;
    X[XFe56] += 4.5e-1 * wtw__factor; //Ni56
#ifdef NUCSYN_ALL_ISOTOPES
    X[XS32] += 3.0e-4 * wtw__factor;
    X[XAr36] += 6.6e-4 * wtw__factor;
    X[XCa40] += 3.9e-3 * wtw__factor;
    X[XTi44] += 8.9e-3 * wtw__factor;
    X[XCr48] += 9.4e-3 * wtw__factor;
    X[XFe52] += 1.8e-2 * wtw__factor;
#endif
}

#endif // NUCSYN
