#include "../binary_c.h"
No_empty_translation_unit_warning;


/*
 * Estimate the dust mass ejected from a common-envelope ejection
 */

void nucsyn_dust_from_common_envelope(struct stardata_t * const stardata Maybe_unused,
                                      const double common_envelope_mass,
                                      double * mass_carbon_grains,
                                      double * mass_MgSiO3_grains)
{
    /*
     * From Iaconi et al. (2020)
     * https://arxiv.org/pdf/2003.06151.pdf
     * section 4.5
     */
    *mass_carbon_grains = 0.5 * 0.01 * common_envelope_mass;
    *mass_MgSiO3_grains = 0.2 * 0.01 * common_envelope_mass;
}
