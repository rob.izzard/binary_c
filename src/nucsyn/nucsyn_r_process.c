#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef NUCSYN

inline void nucsyn_core_collapse_r_process(Abundance * Restrict const Xr,
                                           struct stardata_t * Restrict const stardata)
{
    /*
     * Wrapper function for the core-collapse supernova
     * r-process routines
     */
    if(stardata->preferences->core_collapse_rprocess_algorithm == NUCSYN_CCSN_RPROCESS_ARLANDINI1999)
    {
        nucsyn_r_process_Arlandini1999(Xr,stardata);
    }
    else if(stardata->preferences->core_collapse_rprocess_algorithm == NUCSYN_CCSN_RPROCESS_SIMMERER2004)
    {
        nucsyn_r_process_Simmerer2004(Xr,stardata);
    }
    else
    {
        Exit_binary_c(BINARY_C_ALGORITHM_OUT_OF_RANGE,
                      "Unknown core-collapse r-process algorithm %d\n",
                      stardata->preferences->electron_capture_supernova_algorithm);
    }
}

#endif // NUCSYN
