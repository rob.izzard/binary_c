#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef NUCSYN

/*
 * Return an array of core abundances
 */

Abundance * nucsyn_core_abundances(struct stardata_t * const stardata,
                                   struct star_t * const star)
{
    Abundance * const X = New_clear_isotope_array;
    Dprint("core abundances star %d st %d \n",
           star->starnum,
           star->stellar_type);
    if(star->stellar_type <= MAIN_SEQUENCE ||
       star->stellar_type == HeMS)
    {
        /*
         * No well-defined core
         */
        Copy_abundances(star->Xenv,X);
        Dprint("No well-defined core, use envelope 1-X=%g\n",
               1.0-nucsyn_totalX(X));
    }
    else if(star->stellar_type <= HeGB)
    {
        /*
         * Use the white-dwarf core abundances
         */
        const Stellar_type st = Core_stellar_type(star->stellar_type);
        nucsyn_set_WD_abunds(stardata,
                             X,
                             st);
        Dprint("Map stellar type %d to %d : hence use WD abundances 1-X=%g\n",
               star->stellar_type,
               st,
               1.0-nucsyn_totalX(X));
    }
    else if(WHITE_DWARF(star->stellar_type))
    {
        nucsyn_set_WD_abunds(stardata,
                             X,
                             star->stellar_type);
        Dprint("use WD abundances 1-X=%g\n",
               1.0-nucsyn_totalX(X));
    }
    else if(star->stellar_type == NS ||
            star->stellar_type == BH)
    {
        /*
         * NS and BH, all "neutrons"
         */
        X[Xn] = 1.0;
        Dprint("all neutrons\n");
    }

    return X;
}

#endif //NUCSYN
