#pragma once
#ifndef NUCSYN_FUNCTIONS_H
#define NUCSYN_FUNCTIONS_H


/* nucsyn network burn function */
typedef double(*nucsyn_burnfunc)(struct stardata_t * const,
                                 double * const ,
                                 const double * const,
                                 const double);

/* nucsyn network check function */
typedef Boolean(*nucsyn_checkfunc)(struct stardata_t * const,
                                   double * const,
                                   const double,
                                   const double);

/* nucsyn network logging function */
typedef void(*nucsyn_logfunc)(struct stardata_t * const stardata,
                              const double * const Nin,
                              const double t);

/* solver wrapper */
typedef double(*nucsyn_solver_func)(double * const,
                                    const double * const,
                                    const double,
                                    nucsyn_burnfunc,
                                    nucsyn_logfunc,
                                    const double,
                                    struct stardata_t * const,
                                    const Reaction_network,
                                    const Boolean);

#endif // NUCSYN_FUNCTIONS_H
