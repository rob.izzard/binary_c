#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef NUCSYN

void nucsyn_make_icache(struct store_t * const store)
{
    /*
     * Set up the element identifier cache
     */
    Isotope ** icache = store->icache =
        Malloc(sizeof(Isotope*)*NUMBER_OF_ELEMENTS);
    Atomic_number el;
    store->nisotopes = Malloc(sizeof(unsigned int) * NUMBER_OF_ELEMENTS);
    for(el=0;el<NUMBER_OF_ELEMENTS;el++)
    {
        icache[el] = Calloc(sizeof(Isotope),MAX_ISOTOPES_PER_ELEMENT);
        unsigned int c = 0;
        Forward_isotope_loop(i)
        {
            if(store->atomic_number[i] == el)
            {
                icache[el][c++] = i;
            }
        }
        store->nisotopes[el] = c;
    }
}

#endif // NUCSYN
