#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef NUCSYN

double nucsyn_sn_core_collapse(Abundance * const X, //space calloc'd previously
                               Abundance * const Xenv,
                               const double mco,// co core mass
                               const double menv,//env mass
                               double dm,// mass to be ejected
                               Abundance z,
                               const Yield_source src_id,
                               struct stardata_t * const stardata,
                               const struct star_t * const pre_explosion_star,
                               struct star_t * const exploder)
{
    /*
     * Calculate yields from core collapse supernovae.
     *
     * The mass ejected in the supernova is dm. However, this is made up of
     * material both from the envelope and the core so treat them separately.
     *
     * pre_explosion_star->stellar_type is the stellar type of the star just prior to explosion
     *
     * exploder is the structure after explosion
     *
     * The abundance array X stores the ejected mass fractions,
     * this can be used to accrete onto the companion star,
     * if required.
     *
     * Returns total ejected mass.
     */
    Dprint("SN core collapse : m = %g, mco = %g, menv = %g, src_id = %d, max_He_core_mass = %g, stellar type %d\n",
           pre_explosion_star->mass,
           mco,
           menv,
           src_id,
           pre_explosion_star->max_EAGB_He_core_mass,
           pre_explosion_star->stellar_type);
    Dprint("M=%g MHe=%g MCO=%g\n",
           pre_explosion_star->mass,
           pre_explosion_star->core_mass[CORE_He],
           pre_explosion_star->core_mass[CORE_CO]);

    if(X==NULL)
    {
        Exit_binary_c(
            BINARY_C_POINTER_FAILURE,
            "nucsyn_sn_core_collapse has X null on input - oops\n");
    }
    Clear_isotope_array(X);

    double ejecta_mass = 0.0;
    const double mcut = menv + mco - dm; // location of the mass cut (NS/BH mass)
    double dm_env; // envelope mass (H and He) to be ejected
    double dm_core; // core mass (CO) to be ejected

    /*
     * if there is no expelled material, just return
     */
    if(dm < TINY)
    {
        Dprint("no material expelled : nothing to do");
    }
    else
    {

        /*
         * Set some ejecta mass to be from r-process
         */
        dm -= stardata->preferences->core_collapse_rprocess_mass;

        /*
         * If nucsyn_metallicity is set, use it
         */
        if(stardata->common.nucsyn_metallicity > -TINY)
            z = stardata->common.nucsyn_metallicity;

        Dprint("Nucsyn source %d from SN_type %d %s\n",
               src_id,
               exploder->SN_type,
               supernova_type_strings[exploder->SN_type]);

        /*
         * decide from where mass is expelled
         */
        if(dm < menv)
        {
            /*
             * All material is from the envelope,
             * only some of which is ejected
             */
            dm_env = dm;
            dm_core = 0.0;
        }
        else
        {
            /*
             * All the envelope is ejected,
             * some of the core too
             */
            dm_env = menv;
            dm_core = dm - menv;
        }
        Dprint("dm_env = %g, dm_core = %g\n",dm_env,dm_core);

        /*
         * make sure both dm_env and dm_core are at least TINY to prevent
         * numerical problems
         */
        dm_env = Max(TINY, dm_env);
        dm_core = Max(TINY, dm_core);

        if(dm_env > TINY)
        {
            /*
             * Stellar envelopes
             *
             * for stellar types EAGB and TPAGB (AGB) the envelope is uniform and
             * the helium core is (almost) coincident with the CO core
             * so we can treat the envelope as homogeneous with composition
             * the same as the surface
             *
             * Deal with helium stars a bit differently, however.
             */
            Dprint("pre-explosion stellar type %d\n",
                   pre_explosion_star->stellar_type);
            if(NAKED_HELIUM_STAR(pre_explosion_star->stellar_type) ||
               pre_explosion_star->max_EAGB_He_core_mass > pre_explosion_star->mass)
            {
#ifdef NUCSYN_HETEROGENEOUS_HE_STAR
                /* This is based on a 56Msun star just prior to explosion...
                 * Between the surface and 1.5Msun outside the CO core the C,O and He
                 * mass fractions change ~ linearly to C=0.3 O=0.6 He=0.06 Ne20=0.03
                 * The core is dealt with by the SN fits so we
                 * just need to worry about the section with
                 * any helium in it
                 * Xenv is the surface abundance
                 */

                /***** DEPRECATED *****/
                /*
                  m1=Max(0.0,dm_env-1.5); // region of changing abunds
                  m2=dm_env-m1; // homogeneous region outside the CO core
                  Xenv[XC12]=(m1*0.5*(Xenv[XC12]+0.3)+m2*0.6)/dm_env;
                  Xenv[XO16]=(m1*0.5*(Xenv[XO16]+0.6)+m2*0.3)/dm_env;
                  Xenv[XHe4]=(m1*0.5*(Xenv[XHe4]+0.06)+m2*0.06)/dm_env;
                  Xenv[XNe20]=(m1*0.5*(Xenv[XNe20]+0.03)+m2*0.03)/dm_env;
                */

                /*
                 * Improved fit based on many Z=0.02 models from Lynnette, where
                 * the region outside the CO core is defined by Y>0.05.
                 */

                /*
                 * At Z=0.02:
                 * Combining the low-core-mass (MM) and high-core-mass(NL)
                 * data, we have the homogeneous helium abundance given as
                 * a function of core mass
                 *
                 * he = Max(0.01,Min(0.1,0.0135760+4.9056*exp(-mco)))
                 *
                 * (note the 0.01 lower limit is somewhat arbitrary)
                 *
                 * with
                 * c=2.43910e-01-7.48800e-03*mco+1.92790e+00*mco*exp(-mco)
                 * o=6.78700e-01+8.04440e-03*mco-2.59100e+00*mco*exp(-mco)
                 *
                 * The size of the region is 0.5Msun for Mcore < 10,
                 * the whole envelope mass for Mcore>10. This is a sudden
                 * change (it would seem...)
                 *
                 * At other metallicities...
                 * It seems the core mass is always > 10 for Z<=0.004
                 * (EXCEPT IN BINARIES!)
                 * so the heterogeneous zone is always the entire envelope.
                 * The fit for the internal he, c and o is reasonable for all Z
                 * down to 1e-4.
                 *
                 * Note that for MM mass-loss it's very difficult because
                 * the star is very evolved just prior to the end of the evolution
                 * and indeed is mostly oxygen! It is unlikely that the nucsyn_wr function
                 * actually follows this behaviour... indeed I have tried to fit the high
                 * oxygen abundances to M, Mc etc. and cannot. Ooops...
                 */

                // mass of heterogenous region
                const double m1 = mco<10 ? Min(dm_env,0.5) : dm_env;

                const double m2 = dm_env-m1; // mass of homogeneous region

                // limit the core mass to the range given by
                preheco=Xenv[XHe4]+Xenv[XC12]+Xenv[XO16];

                /*
                 * Assume 80% O, 20% C at the
                 * internal boundary (or less C if the surface has less)
                 */
                he = 0.0;
                c = Min(0.2,Xenv[XC12]);
                o = 1.0 - he - c;

                /*
                 * In m1: triangle area + rectangle below
                 * In m2: just the rectangle
                 */
                Xenv[XHe4]=(m1*0.5*(Xenv[XHe4]-he)+m1*he+m2*Xenv[XHe4])/dm_env;
                Xenv[XC12]=(m1*0.5*(Xenv[XC12]-c)+m1*c+m2*Xenv[XC12])/dm_env;
                Xenv[XO16]=(m1*0.5*(Xenv[XO16]-o)+m1*o+m2*Xenv[XO16])/dm_env;

#endif // NUCSYN_HETEROGENEOUS_HE_STAR
            }
            else if(pre_explosion_star->stellar_type==CHeB ||
                    pre_explosion_star->stellar_type==EAGB ||
                    pre_explosion_star->stellar_type==TPAGB)
            {
                /*
                 * Star explodes on EAGB : some of the star is helium, not hydrogen!
                 * NB This also applies on the TPAGB, for which we should set mcx,
                 * the CO core mass
                 */
                if(pre_explosion_star->stellar_type==TPAGB)
                {
                    exploder->max_EAGB_He_core_mass = exploder->core_mass[CORE_He];
                }


                /* this mass of envelope is hydrogen-rich */
                double mH = pre_explosion_star->mass - pre_explosion_star->core_mass[CORE_He];

                /* this mass of envelope is helium-rich */
                double mHe = pre_explosion_star->core_mass[CORE_He] - pre_explosion_star->core_mass[CORE_CO];

                /*
                 * Decide how much that is ejected is H-rich (m1) or He-rich (mHe)
                 */
                if(dm_env < mH)
                {
                    /*
                     * all from the hydrogen envelope,
                     * none from the helium envelope
                     */
                    mH = dm_env;
                    mHe = 0.0;
                }
                else
                {
                    /*
                     * all the hydrogen envelope,
                     * and some of the helium envelope
                     */
                    mHe = Max(0.0, Min(mHe, dm_env - mH));
                }

                /* convert to mass fractions */
                const double mtot = mH + mHe;
                mH /= mtot;
                mHe /= mtot;

                /*
                 * Convert all H to He in the burnt region
                 * and convert all CNO to N in the burnt region
                 */
                Xenv[XHe4]=(mH * (Xenv[XHe4]+Xenv[XH1]) +
                            mHe * (Xenv[XHe4]));
                Xenv[XH1]=(mH * Xenv[XH1] +
                           mHe * 0.0);
                Xenv[XN14]=(mHe * (Xenv[XN14]+Xenv[XC12]+Xenv[XC13]+Xenv[XN15]+Xenv[XO16]+Xenv[XO17]+Xenv[XO18]) +
                            mH * Xenv[XN14]);
                Xenv[XC12]=(mHe * 0.0+
                            mH * Xenv[XC12]);
                Xenv[XC13]=(mHe * 0.0+
                            mH * Xenv[XC13]);
                Xenv[XN15]=(mHe * 0.0+
                            mH * Xenv[XN15]);
                Xenv[XO16]=(mHe * 0.0+
                            mH * Xenv[XO16]);
                Xenv[XO17]=(mHe * 0.0+
                            mH * Xenv[XO17]);
                Xenv[XO18]=(mHe * 0.0+
                            mH * Xenv[XO18]);
            }
            /*
             * normalize the abundance array to 1.0 : this prevents small errors,
             * or in the chieffi/limongi case it normalizes the abundances array
             * correctly
             */
            nucsyn_renormalize_to_max(Xenv);

            /*
             * Put the ejecta in X
             */
            nucsyn_mix_shells(dm_env,Xenv,
                              ejecta_mass,X);
            ejecta_mass += dm_env;
        }

        /*
         * now yield from the core: first calculate the abundances from the
         * explosion of the CO core according to the chosen prescription
         */
        if(dm_core > 2.0*TINY)
        {
            Abundance * Xcore = New_clear_isotope_array;
            nancheck("SN X",Xcore,ISOTOPE_ARRAY_SIZE);

            /*
             * call the appropriate algorithm: the function
             * should return mass fractions
             */
            if(stardata->preferences->core_collapse_supernova_algorithm == NUCSYN_CCSN_WOOSLEY_WEAVER_1995A ||
               stardata->preferences->core_collapse_supernova_algorithm == NUCSYN_CCSN_WOOSLEY_WEAVER_1995B ||
               stardata->preferences->core_collapse_supernova_algorithm == NUCSYN_CCSN_WOOSLEY_WEAVER_1995C)
            {
                nucsyn_sn_woosley_weaver_1995(stardata,Xcore,mco,z);
            }
            else if(stardata->preferences->core_collapse_supernova_algorithm == NUCSYN_CCSN_CHIEFFI_LIMONGI_2004)
            {
                nucsyn_sn_chieffi_limongi_2004(stardata,Xcore,mco,z,mcut,exploder);
            }
            else if(stardata->preferences->core_collapse_supernova_algorithm == NUCSYN_CCSN_LIMONGI_CHIEFFI_2018)
            {
                nucsyn_sn_limongi_chieffi_2018(stardata,Xcore,mco,z,mcut,exploder);
            }
            else if(stardata->preferences->core_collapse_supernova_algorithm == NUCSYN_CCSN_NONE)
            {
                Clear_isotope_array(Xcore);
            }
            else
            {
                Exit_binary_c(BINARY_C_ALGORITHM_OUT_OF_RANGE,
                              "Unknown core-collapse supernova yield algorithm %d\n",
                              stardata->preferences->core_collapse_supernova_algorithm);
            }
            nancheck("SN Xcore",Xcore,ISOTOPE_ARRAY_SIZE);

            /*
             * Now add yield lost from the core, dm_core. remember Xtot normalization factor.
             */
            {
#undef X
#define X(CCTYPE,STRING) STRING,
                static char * cc_sn_strings[] = { CORE_COLLAPSE_SUPERNOVA_ALGORITHMS };
                const char * const s = cc_sn_strings[stardata->preferences->core_collapse_supernova_algorithm];
#undef X

                Dprint("exploder is star %d, using yields of %s with mco=%g menv=%g : explosive yields (M=%g) : C=%g O=%g Fe56=%g) : Yields H1=%g He4=%g C12=%g O16=%g\n",
                       exploder->starnum,
                       s,
                       mco,
                       menv,
                       mco + menv,
                       dm_core*X[XC12],
                       dm_core*X[XO16],
                       dm_core*X[XFe56],
                       exploder->Xyield[XH1],
                       exploder->Xyield[XHe4],
                       exploder->Xyield[XC12],
                       exploder->Xyield[XO16]);
            }

            /*
             * Put the ejecta in X
             */
            nucsyn_mix_shells(dm_core,Xcore,
                              ejecta_mass,X);
            ejecta_mass += dm_core;
            Safe_free(Xcore);
        }


        /*
         * We still have a mass to
         * be ejected as r-process material, so do it.
         */
        if(stardata->preferences->core_collapse_rprocess_algorithm != NUCSYN_CCSN_RPROCESS_NONE &&
           stardata->preferences->core_collapse_rprocess_mass > REALLY_TINY)
        {
            Abundance * Xr = New_clear_isotope_array;
            nancheck("SN Xr",Xr,ISOTOPE_ARRAY_SIZE);
            nucsyn_core_collapse_r_process(Xr,stardata); // set r-process abundances
            nucsyn_mix_shells(stardata->preferences->core_collapse_rprocess_mass,Xr,
                              ejecta_mass,X);
            ejecta_mass += stardata->preferences->core_collapse_rprocess_mass;

            Safe_free(Xr);
        }
        nancheck("SN X",X,ISOTOPE_ARRAY_SIZE);
    }

    nucsyn_renormalize_to_max(X);

    Dprint("Returning ejecta_mass %g : X H1=%g He4=%g C12=%g Fe56=%g : 1-tot %g\n",
           ejecta_mass,
           X[XH1],
           X[XHe4],
           X[XC12],
           X[XFe56],
           1.0-nucsyn_totalX(X));

    return ejecta_mass;
}

#endif // NUCSYN
