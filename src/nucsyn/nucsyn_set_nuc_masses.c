#include "../binary_c.h"
No_empty_translation_unit_warning;


/* Function to set the nuclear masses into the mnuc array */

#ifdef NUCSYN
void nucsyn_set_nuc_masses(Nuclear_mass * Restrict const mnuc,
                           Nuclear_mass * Restrict const imnuc,
                           Nuclear_mass * Restrict const mnuc_amu, // (as mnuc /= AMU_GRAMS)
                           Nuclear_mass * Restrict const imnuc_amu,
                           Molecular_weight * Restrict  const molweight,
                           double * Restrict const ZonA,
                           Atomic_number * Restrict const atomic_number,
                           Nucleon_number * Restrict const nucleon_number,
                           double * Restrict const ionised_molecular_weight_multiplier)
{
    /*
     * Nuclear masses from http://wwwndc.jaea.go.jp/NuC/
     */
#include "nucsyn_isotope_list.h"

    /*
     * Copy data directly when possible
     */
    memcpy(mnuc,
           atomic_masses,
           ISOTOPE_MEMSIZE);

    memcpy(atomic_number,
           atomic_numbers,
           ISOTOPE_ARRAY_SIZE * sizeof(Atomic_number));

    memcpy(nucleon_number,
           nucleon_numbers,
           ISOTOPE_ARRAY_SIZE * sizeof(Nucleon_number));

    Forward_isotope_loop(i)
    {
#ifdef Xelse
        if(i!=Xelse)
#endif
        {
            const Nuclear_mass temp = 1.0 / mnuc[i];
            const double Z = (double) atomic_number[i];
            imnuc[i] = temp;
            ZonA[i]  =  Z * temp;
            mnuc_amu[i] = mnuc[i] / AMU_GRAMS;
            imnuc_amu[i] = imnuc[i] * AMU_GRAMS;
            molweight[i] = (Z + 1.0) * temp;
            ionised_molecular_weight_multiplier[i] =
                imnuc_amu[i] * (1.0 + atomic_number[i]);
        }
    }
}
#endif /* NUCSYN */
