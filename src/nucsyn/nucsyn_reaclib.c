#include "../binary_c.h"
No_empty_translation_unit_warning;


Constant_function double nucsyn_reaclib(
    const double t9, // temperature (/1e9)
    const double i9, // 1.0/t9
    const double tm13, // pow(t9,-0.3333333333)
    const double t913, // pow(t9,0.333333333333333333333333)
    const double t953, // pow(t9,1.66666666666666666666)
    const double lt9, // (natural) log(t9)
    const double ra, // reaclib parameters
    const double rb,
    const double rc,
    const double rd,
    const double re,
    const double rf,
    const double rg)
{
    /* function to evaluate reaclib-style cross sections */
    return exp(ra+
               rb*i9+
               rc*tm13+
               rd*t913+
               re*t9+
               rf*t953+
               rg*lt9);
}
