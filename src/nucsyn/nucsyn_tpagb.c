#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef NUCSYN

/*
 * Function to evolve a star during the Thermally Pulsing Asymptotic Giant
 * Branch stage of evolution and do nucleosynthesis in that star.
 */

void nucsyn_tpagb(double * Restrict const age,
                  double * Restrict const mc,
                  double * Restrict const mt,
                  double * Restrict const lum,
                  double * Restrict const r,
                  struct star_t * const newstar,
                  struct stardata_t * Restrict const stardata)
{
    const double menv = *mt - *mc; /* Envelope mass */
    const double m0 = newstar->mc_1tp + newstar->menv_1tp; /* initial TPAGB mass */
    Boolean logged = FALSE; /* logging boolean : avoid excessive log files */
#ifdef NUCSYN_TPAGB_HBB
    Boolean hbb = FALSE;
#endif

    /*
     * Calculate time on the TPAGB (Myr)
     */
    const double tagb = newstar->age - newstar->time_first_pulse;


    /************************************************************/
    Dprint("TPAGB debug for star %d at model %d, pulse number %g, mt=%g mc=%g, ABUNDS tagb=%g H1=%g He4=%g C12=%g N14=%g 1-total=%g\n",
           newstar->starnum,
           stardata->model.model_number,
           newstar->num_thermal_pulses,
           *mt,
           *mc,
           tagb,
           newstar->Xenv[XH1],
           newstar->Xenv[XHe4],
           newstar->Xenv[XC12],
           newstar->Xenv[XN14],
           1.0-nucsyn_totalX(newstar->Xenv));


    /* Initialise stuff that needs to be done on the first pulse */
    if(First_pulse(newstar) || Is_zero(tagb))
    {
        Dprint("First pulse at %g (t=%g) kw=%d\n",
               newstar->time_first_pulse,
               stardata->model.time,
               newstar->stellar_type);
        nucsyn_init_first_pulse(newstar,
                                stardata);
    }
    else
    {
        Dprint("Not first pulse\n");
    }
    Dprint("TIPF %g\n",newstar->interpulse_period);


    /* save the time we were last called */
    newstar->prev_tagb = tagb;

    /*
     * Does not work if we're interpolating for RLOF
     * (but you should use Rob's algorithm to do this
     * and hence AVOID such interpolation)
     */

    if(stardata->model.intpol==0)
    {
        Dprint("tagb : tagb=%g from age=%g macro=%g (dt=%g Myr)\n",
               tagb,
               *age,
               newstar->time_first_pulse,
               stardata->model.dt);

        /*
         * If the envelope mass is zero, just return - we can't do any burning
         * or third dredge-up anyway
         */
        if(menv > TINY)
        {

#ifdef NUCSYN_TPAGB_HBB
            /**********************/
            /* HOT BOTTOM BURNING */
            /**********************/
            /* Set maximum temperature this star can achieve */
            const double Thbbmax = nucsyn_set_hbb_conditions(newstar,
                                                             stardata,
                                                             m0,
                                                             menv);
            hbb = HBB_Conditions(newstar->temp,
                                 newstar->rho,
                                 menv,
                                 m0);

            Dprint("Next pulse at %g, NTP=%g, tmax %g T=%g rho=%g risefac %g\n",
                   newstar->time_next_pulse,
                   newstar->num_thermal_pulses,
                   Thbbmax,
                   newstar->temp,
                   newstar->rho,
                   newstar->temp_rise_fac);

            if(hbb == TRUE)
            {
                Dprint("Confirm Burn!\n");

                /*
                 * As time goes on, burn the shell stored in Xhbb for the time given
                 * by the timestep. Each timestep, mix the Xhbb shell with the
                 * envelope shell abundances and save this in "tempshell" which we
                 * only use for logging. At the end of each pulse finally mix the HBB
                 * shell with the envelope.
                 */

                /*
                 * calculate burn time in this (or these) pulses
                 */
                const double burn_time_this_pulse =
                    newstar->interpulse_period *
                    newstar->f_burn *
                    newstar->dntp;

                Dprint("TPAGBHBB Pulse NTP=%g at t=%g burntime %g yrs c.f. tipf=%g Myr * f_burn=%g c.f. dt=%g Myr from tagb=%g time_prev_pulse=%g diff=%g\n",
                       newstar->num_thermal_pulses,
                       newstar->time_next_pulse,
                       burn_time_this_pulse,
                       newstar->interpulse_period,
                       newstar->f_burn,
                       stardata->model.dt,
                       tagb,
                       newstar->time_prev_pulse,
                       newstar->age - newstar->time_prev_pulse
                    );

                /*
                 * copy envelope abundance, Xenv, into Xhbb
                 * because we burn Xhbb
                 */
                Copy_abundances(newstar->Xenv,
                                newstar->Xhbb);

                /*
                 * Do the nuclear burning with mixture in Xhbb.
                 */
                Dprint("HBB for time %g was C12=%g, N14=%g \n",
                       burn_time_this_pulse,
                       newstar->Xhbb[XC12],
                       newstar->Xhbb[XN14]);

                nucsyn_hbb(newstar->temp,
                           newstar->rho,
                           burn_time_this_pulse, // NB this is already in years
                           newstar->Xhbb,
                           newstar,
                           stardata);

                Dprint("Done burn (Env: C12=%g N14=%g)\n",
                       newstar->Xhbb[XC12],
                       newstar->Xhbb[XN14]);

#ifdef NUCSYN_LOG2
                /*
                 * For logging, copy the surface abundances into tempshell, pretend it's the
                 * size of the envelope (minus the small hbb layer) and dilute(/enhance) with
                 * the stuff in the hbb layer. Then output. If you set the timestep to be small
                 * (or dtfac to be small) then this logging will be activated.
                 */
                Abundance * tempshell = New_isotope_array_from(newstar->Xenv);

                /* Dilute into tempshell */
                nucsyn_dilute_shell((1.0-newstar->f_hbb)*menv,
                                    tempshell,
                                    newstar->f_hbb*menv,
                                    newstar->Xhbb);
                Log_nucsyn("HBB intermediate:",tagb,tempshell,0.0);
                logged = TRUE;
                Safe_free(tempshell);
#endif // NUCSYN_LOG2
                Dprint("Done log\n");

            }
#if (DEBUG==1)
            else
            {
                Dprint("not hot/dense enough or m0 is not high enough\n");
            }
#endif
            Dprint("Done HBB\n");

#endif /* NUCSYN_TPAGB_HBB */
        }
        /*
         * check if we've gone through some pulses
         */
        Dprint("Check for pulses : dntp = %g\n",
               newstar->dntp);

        if(newstar->dntp > 0.5)
        {
            Dprint("NTP=%g tipf=%g\n",
                   newstar->num_thermal_pulses,
                   newstar->interpulse_period);

#ifdef NUCSYN_TPAGB_HBB
            if(hbb==TRUE)
            {
                /*
                 * Mix the HBB layer into the envelope since we've had enough
                 * of burning it
                 */
                Dprint("Mix HBB: was H=%g He=%g (shell H=%g He=%g) -> ",
                       newstar->Xenv[XH1],
                       newstar->Xenv[XHe4],
                       newstar->Xhbb[XH1],
                       newstar->Xhbb[XHe4]);
                nucsyn_dilute_shell(1.0 - newstar->f_hbb,
                                    newstar->Xenv,
                                    newstar->f_hbb,
                                    newstar->Xhbb);
                Dprint("now H=%g He=%g\n",
                       newstar->Xenv[XH1],
                       newstar->Xenv[XHe4]);
            }
#endif //NUCSYN_TPAGB_HBB

            /* Do third dredge up but only if we've gone through a pulse! */
#ifdef NUCSYN_THIRD_DREDGE_UP

            if(newstar->dntp > TINY)
            {
                Dprint("Pre 3DUP abundances: H=%g He=%g\n",
                       newstar->Xenv[XH1],
                       newstar->Xenv[XHe4]);

                /*********************/
                /** Third Dredge Up **/
                /*********************/
                nucsyn_third_dredge_up(stardata,newstar);
            }
#endif
            Dprint("Post 3DUP abundances: H=%g He=%g\n",
                   newstar->Xenv[XH1],
                   newstar->Xenv[XHe4]);
        }


        Dprint("prev %g\n",newstar->prev_tagb);
        if(logged==FALSE)
        {
            Log_nucsyn("Exit\n",tagb,newstar->Xenv,0.0);
        }

        Dprint("end of tpagb function loop n=%g, ntp=%g\n\n",
               newstar->Xenv[Xn],newstar->num_thermal_pulses);


        Dprint("ABUNDS tagb=%g H1=%g He4=%g C12=%g N14=%g 1-total=%g\n",
               tagb,
               newstar->Xenv[XH1],
               newstar->Xenv[XHe4],
               newstar->Xenv[XC12],
               newstar->Xenv[XN14],
               1.0-nucsyn_totalX(newstar->Xenv));

        Dprint("endTPAGB Mc=%g Menv=%g L=%g R=%g\n",*mc,*mt-*mc,*lum,*r);
    }
}

#endif /* NUCSYN */
