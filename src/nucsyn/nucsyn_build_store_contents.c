#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef NUCSYN
void nucsyn_build_store_contents(struct store_t * Restrict const store)
{

    /*
     * Build store contents for nucleosynthesis
     *
     * See also memory/build_store_contents.c
     *
     * Note:
     * NOTHING in here can depend on the current stardata.
     * You will NOT have access to stardata here.
     */
    store->mnuc = Calloc(ISOTOPE_ARRAY_SIZE,sizeof(Nuclear_mass));
    store->imnuc=Calloc(ISOTOPE_ARRAY_SIZE,sizeof(Nuclear_mass));
    store->mnuc_amu=Calloc(ISOTOPE_ARRAY_SIZE,sizeof(Nuclear_mass));
    store->imnuc_amu=Calloc(ISOTOPE_ARRAY_SIZE,sizeof(Nuclear_mass));
    store->molweight=Calloc(ISOTOPE_ARRAY_SIZE,sizeof(Molecular_weight));
    store->ZonA=Calloc(ISOTOPE_ARRAY_SIZE,sizeof(double));
    store->atomic_number=Calloc(ISOTOPE_ARRAY_SIZE,sizeof(Atomic_number));
    store->nucleon_number=Calloc(ISOTOPE_ARRAY_SIZE,sizeof(Nucleon_number));
    store->ionised_molecular_weight_multiplier=Calloc(ISOTOPE_ARRAY_SIZE,sizeof(double));

    nucsyn_set_nuc_masses(store->mnuc,
                          store->imnuc,
                          store->mnuc_amu,
                          store->imnuc_amu,
                          store->molweight,
                          store->ZonA,
                          store->atomic_number,
                          store->nucleon_number,
                          store->ionised_molecular_weight_multiplier);

    Nuclear_network_isotope_range_set(NUCSYN_NETWORK_PP,Xe,XBe7);
    Nuclear_network_isotope_range_set(NUCSYN_NETWORK_COLDCNO,XH1,XF19);
    Nuclear_network_isotope_range_set(NUCSYN_NETWORK_HOTCNO,XH1,XF19);
    Nuclear_network_isotope_range_set(NUCSYN_NETWORK_NeNaMgAl,XH1,XSi28);
    Nuclear_network_isotope_range_set(NUCSYN_NETWORK_NeNa,XH1,XNa23);
    Nuclear_network_isotope_range_set(NUCSYN_NETWORK_NeNaMgAlnoleak,XH1,XSi28);
    Nuclear_network_isotope_range_set(NUCSYN_NETWORK_PPfast,XH1,XHe4);

    nucsyn_elemental_abundance(NULL,NULL,NULL,store);
    nucsyn_element_to_atomic_number(store,NULL,1);
    /* allocate space for sigmav (reaction rates) table */
#ifdef NUCSYN_SIGMAV_PRE_INTERPOLATE
    table_sigmav(store);
#endif//NUCSYN_SIGMAV_PRE_INTERPOLATE

    /*
     * Data tables
     */
    table_1DUP(store);
#if defined NUCSYN && defined NUCSYN_STRIP_AND_MIX
    table_TAMS(store);
#endif
#ifdef NUCSYN_S_PROCESS
    table_s_process(store);
#endif
    #ifdef NUCSYN_NOVAE
    table_novae(store);
#endif
    nucsyn_make_isotope_names(store);

    /*
     * Attach nuclear burning functions.
     * If KAPS_RENTROP is NULL,
     */

#undef X
#ifdef __HAVE_LIBSUNDIALS_CVODE__
#define _CVODE(X) (X)
#else
#define _CVODE(X) NULL
#endif // __HAVE_LIBSUNDIALS_CVODE__
#define X(CODE,                                                         \
          ONOFF,                                                        \
          STRING,                                                       \
          KAPS_RENTROP_FUNCTION,                                        \
          LSODA_FUNCTION,                                               \
          CVODE_FUNCTION,                                               \
          CHECKFUNC)                                                    \
    if(NUCSYN_NETWORK_##CODE != NUCSYN_NETWORK_NUMBER)                  \
    {                                                                   \
        store->nucsyn_network_function[NUCSYN_SOLVER_KAPS_RENTROP][NUCSYN_NETWORK_##CODE] = KAPS_RENTROP_FUNCTION; \
        store->nucsyn_network_function[NUCSYN_SOLVER_LSODA][NUCSYN_NETWORK_##CODE] = LSODA_FUNCTION; \
        store->nucsyn_network_function[NUCSYN_SOLVER_CVODE][NUCSYN_NETWORK_##CODE] = _CVODE(CVODE_FUNCTION); \
        store->nucsyn_network_checkfunction[NUCSYN_NETWORK_##CODE] = CHECKFUNC; \
    }
    NUCSYN_NETWORKS_LIST;
#undef X
}

#endif  //NUCSYN
