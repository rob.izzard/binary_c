#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef NUCSYN

void nucsyn_sn_iwamoto_1999_DD2(Abundance * Restrict const X)
{
    /*
     * Iwamoto's 1999 SNIa model DD2
     */
#define IWAMOTO_1999_NORMALIZATION_CONST (1.0/1.3385343318)
    // These add up to IWAMOTO_1999_NORMALIZATION_CONST Msun, we want mass of stuff so
    // multiply by norm
    Clear_isotope_array(X);
    Increment_isotopic_property(X,XC12, IWAMOTO_1999_NORMALIZATION_CONST * 8.99e-3);
    Increment_isotopic_property(X,XC13, IWAMOTO_1999_NORMALIZATION_CONST * 3.30e-7);
    Increment_isotopic_property(X,XN14, IWAMOTO_1999_NORMALIZATION_CONST * 2.69e-4);
    Increment_isotopic_property(X,XN15, IWAMOTO_1999_NORMALIZATION_CONST * 5.32e-7);
    Increment_isotopic_property(X,XO16, IWAMOTO_1999_NORMALIZATION_CONST * 6.58e-02);
    Increment_isotopic_property(X,XO17, IWAMOTO_1999_NORMALIZATION_CONST * 4.58e-6);
    Increment_isotopic_property(X,XO18, IWAMOTO_1999_NORMALIZATION_CONST * 6.35e-7);
    Increment_isotopic_property(X,XF19, IWAMOTO_1999_NORMALIZATION_CONST * 4.50e-10);
    Increment_isotopic_property(X,XNe20, IWAMOTO_1999_NORMALIZATION_CONST * 6.22e-4);
    Increment_isotopic_property(X,XNe21, IWAMOTO_1999_NORMALIZATION_CONST * 1.39e-06);
    Increment_isotopic_property(X,XNe22, IWAMOTO_1999_NORMALIZATION_CONST * 4.21e-4);
    Increment_isotopic_property(X,XNa23, IWAMOTO_1999_NORMALIZATION_CONST * 2.61e-5);
    Increment_isotopic_property(X,XMg24, IWAMOTO_1999_NORMALIZATION_CONST * 4.47e-3);
    Increment_isotopic_property(X,XMg25, IWAMOTO_1999_NORMALIZATION_CONST * 2.66e-5);
    Increment_isotopic_property(X,XMg26, IWAMOTO_1999_NORMALIZATION_CONST * 2.59e-5);
    Increment_isotopic_property(X,XAl27, IWAMOTO_1999_NORMALIZATION_CONST * 2.47e-4);
    Increment_isotopic_property(X,XSi28, IWAMOTO_1999_NORMALIZATION_CONST * 2.06e-1); // Silicon!
    Increment_isotopic_property(X,XSi29, IWAMOTO_1999_NORMALIZATION_CONST * 3.40e-4);
    Increment_isotopic_property(X,XSi30, IWAMOTO_1999_NORMALIZATION_CONST * 6.41e-4);
    Increment_isotopic_property(X,XFe56, IWAMOTO_1999_NORMALIZATION_CONST * 7.13e-1); // Iron!
#ifdef NUCSYN_ALL_ISOTOPES
    Increment_isotopic_property(X,XP31, IWAMOTO_1999_NORMALIZATION_CONST * 1.60e-4);
    Increment_isotopic_property(X,XS32, IWAMOTO_1999_NORMALIZATION_CONST * 1.22e-1);
    Increment_isotopic_property(X,XS33, IWAMOTO_1999_NORMALIZATION_CONST * 1.92e-4);
    Increment_isotopic_property(X,XS34, IWAMOTO_1999_NORMALIZATION_CONST * 2.04e-3);
    Increment_isotopic_property(X,XS36, IWAMOTO_1999_NORMALIZATION_CONST * 1.31e-7);
    Increment_isotopic_property(X,XCl35, IWAMOTO_1999_NORMALIZATION_CONST * 7.07e-5);
    Increment_isotopic_property(X,XCl37, IWAMOTO_1999_NORMALIZATION_CONST * 2.26e-5);
    Increment_isotopic_property(X,XAr36, IWAMOTO_1999_NORMALIZATION_CONST * 2.41e-2);
    Increment_isotopic_property(X,XAr38, IWAMOTO_1999_NORMALIZATION_CONST * 9.9e-4);
    Increment_isotopic_property(X,XAr40, IWAMOTO_1999_NORMALIZATION_CONST * 5.19e-9);
    Increment_isotopic_property(X,XK39, IWAMOTO_1999_NORMALIZATION_CONST * 5.67e-5);
    Increment_isotopic_property(X,XK41, IWAMOTO_1999_NORMALIZATION_CONST * 4.52e-6);
    Increment_isotopic_property(X,XCa40, IWAMOTO_1999_NORMALIZATION_CONST * 2.43e-2);
    Increment_isotopic_property(X,XCa42, IWAMOTO_1999_NORMALIZATION_CONST * 2.55e-5);
    Increment_isotopic_property(X,XCa43, IWAMOTO_1999_NORMALIZATION_CONST * 2.22e-7);
    Increment_isotopic_property(X,XCa44, IWAMOTO_1999_NORMALIZATION_CONST * 2.95e-5);
    Increment_isotopic_property(X,XCa46, IWAMOTO_1999_NORMALIZATION_CONST * 4.73e-9);
    Increment_isotopic_property(X,XCa48, IWAMOTO_1999_NORMALIZATION_CONST * 1.64e-6);
    Increment_isotopic_property(X,XSc45, IWAMOTO_1999_NORMALIZATION_CONST * 2.09e-7);
    Increment_isotopic_property(X,XTi46, IWAMOTO_1999_NORMALIZATION_CONST * 1.12e-5);
    Increment_isotopic_property(X,XTi47, IWAMOTO_1999_NORMALIZATION_CONST * 1.56e-6);
    Increment_isotopic_property(X,XTi48, IWAMOTO_1999_NORMALIZATION_CONST * 6.11e-4);
    Increment_isotopic_property(X,XTi49, IWAMOTO_1999_NORMALIZATION_CONST * 4.39e-5);
    Increment_isotopic_property(X,XTi50, IWAMOTO_1999_NORMALIZATION_CONST * 3.51e-4);
    Increment_isotopic_property(X,XV50, IWAMOTO_1999_NORMALIZATION_CONST * 9.33e-9);
    Increment_isotopic_property(X,XV51, IWAMOTO_1999_NORMALIZATION_CONST * 1.16e-4);
    Increment_isotopic_property(X,XCr50, IWAMOTO_1999_NORMALIZATION_CONST * 3.53e-4);
    Increment_isotopic_property(X,XCr52, IWAMOTO_1999_NORMALIZATION_CONST * 1.37e-2);
    Increment_isotopic_property(X,XCr53, IWAMOTO_1999_NORMALIZATION_CONST * 1.38e-3);
    Increment_isotopic_property(X,XCr54, IWAMOTO_1999_NORMALIZATION_CONST * 1.60e-3);
    Increment_isotopic_property(X,XMn55, IWAMOTO_1999_NORMALIZATION_CONST * 7.05e-3);
    Increment_isotopic_property(X,XFe54, IWAMOTO_1999_NORMALIZATION_CONST * 5.91e-2);

    Increment_isotopic_property(X,XFe57, IWAMOTO_1999_NORMALIZATION_CONST * 1.67e-2);
    Increment_isotopic_property(X,XFe58, IWAMOTO_1999_NORMALIZATION_CONST * 3.23e-3);
    Increment_isotopic_property(X,XCo59, IWAMOTO_1999_NORMALIZATION_CONST * 6.25e-4);
    Increment_isotopic_property(X,XNi58, IWAMOTO_1999_NORMALIZATION_CONST * 4.29e-2); // Nickel!
    Increment_isotopic_property(X,XNi60, IWAMOTO_1999_NORMALIZATION_CONST * 1.15e-2);
    Increment_isotopic_property(X,XNi61, IWAMOTO_1999_NORMALIZATION_CONST * 3.58e-4);
    Increment_isotopic_property(X,XNi62, IWAMOTO_1999_NORMALIZATION_CONST * 3.69e-3);
    Increment_isotopic_property(X,XNi64, IWAMOTO_1999_NORMALIZATION_CONST * 2.31e-4);
    Increment_isotopic_property(X,XCu63, IWAMOTO_1999_NORMALIZATION_CONST * 4.88e-6);
    Increment_isotopic_property(X,XCu65, IWAMOTO_1999_NORMALIZATION_CONST * 2.04e-6);
    Increment_isotopic_property(X,XZn64, IWAMOTO_1999_NORMALIZATION_CONST * 3.10e-5);
    Increment_isotopic_property(X,XZn66, IWAMOTO_1999_NORMALIZATION_CONST * 6.42e-5);
    Increment_isotopic_property(X,XZn67, IWAMOTO_1999_NORMALIZATION_CONST * 6.55e-7);
    Increment_isotopic_property(X,XZn68, IWAMOTO_1999_NORMALIZATION_CONST * 8.81e-8);
#endif // NUCSYN_ALL_ISOTOPES
}
#endif // NUCSYN
