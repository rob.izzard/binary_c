#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef NUCSYN
Boolean nucsyn_helium_checkfunction(struct stardata_t * const stardata Maybe_unused,
                                    double * const N,
                                    const double temperature,
                                    const double density Maybe_unused)
{
    /*
     * Check temperature, density and input abundance
     * array (number densities): return TRUE if we should
     * burn helium, FALSE otherwise
     */
    return (N[XHe4]>TINY && temperature >= 0.8e8) ? TRUE : FALSE;
}
#endif//NUCSYN
