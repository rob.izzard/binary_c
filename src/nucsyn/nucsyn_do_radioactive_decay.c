#include "../binary_c.h"
No_empty_translation_unit_warning;


#if defined NUCSYN && defined NUCSYN_RADIOACTIVE_DECAY

/*
 * Radioactive decay of abundances in X for the current timestep
 *
 * We want to calculate X_parent * exp(-dt / timescale)
 * and put the change into the child nucleus.
 *
 * We pre-calculate -dt to reduce operations.
 *
 * Note that we assume the masses of the nuclei are similar
 * so we can operate on X rather than N. Most of the time,
 * this is true: we only have alpha decays of elements
 * from Nd144 upwards (144 vs 140 is a small difference).
 *
 * Note that the timescale is NOT the half life, it's the
 * time to drop to 1/e of the original value.
 */

#define Do_radioactive_decay(PARENT,                    \
                             TIMESCALE,                 \
                             CHILD)                     \
    if(Isotope_is_valid(PARENT) &&                      \
       X[(PARENT)] > ABUND_ZERO)                        \
    {                                                   \
        const Abundance dx =                            \
            X[(PARENT)] * (1.0 - exp(mdt/(TIMESCALE))); \
        X[(PARENT)] -= dx ;                             \
        X[(CHILD)] += dx;                               \
    }

void nucsyn_do_radioactive_decay(const struct stardata_t * Restrict const stardata,
                                 Abundance * const X)
{
    if(Is_not_zero(stardata->model.dt))
    {
        const double mdt = -stardata->model.dt;

        /*
         * Beta decays
         */
        /*
        if(!Is_zero(X[XTc99]))
            printf("TC decay from %g for time %g on timescale %g : f = %g\n",
                   X[XTc99],
                   -1.0/mdt,
                   XTc99_BETA_DECAY,
                   exp(mdt/XTc99_BETA_DECAY)
                );
        */
        Do_radioactive_decay(XAl26,XAl26_BETA_DECAY,XMg26);
        Do_radioactive_decay(XBe7,XBe7_BETA_DECAY,XLi7);
        Do_radioactive_decay(XNa22,XNa22_BETA_DECAY,XNe22);

#ifdef NUCSYN_ALL_ISOTOPES
        Do_radioactive_decay(XTc99,XTc99_BETA_DECAY,XRu99);
        //Do_radioactive_decay(XCa41,XNi59_BETA_DECAY,XK41);
        //Do_radioactive_decay(XFe60,XFe60_BETA_DECAY,XNi60); // OOPS!
        //Do_radioactive_decay(XNi59,XNi59_BETA_DECAY,XCo59); // OOPS!
        Do_radioactive_decay(XV50,XV50_BETA_DECAY,XTi50);
        Do_radioactive_decay(XS30,XS30_BETA_DECAY,XP30);
        Do_radioactive_decay(XO15,XO15_BETA_DECAY,XN15);
        //Do_radioactive_decay(XF17,XF17_BETA_DECAY,XO17);
        Do_radioactive_decay(XP30,XP30_BETA_DECAY,XSi30);
        Do_radioactive_decay(XK40,XK40_BETA_DECAY,XCa40);
        // broken? Do_radioactive_decay(XN13,XN13_BETA_DECAY,XC13);
        Do_radioactive_decay(XSe79,XSe79_BETA_DECAY,XBr79);
        Do_radioactive_decay(XKr81,XKr81_BETA_DECAY,XBr81);
        Do_radioactive_decay(XZr93,XZr93_BETA_DECAY,XNb93);
        Do_radioactive_decay(XCs135,XCs135_BETA_DECAY,XBa135);
        Do_radioactive_decay(XAg111,XAg111_BETA_DECAY,XCd111);
        Do_radioactive_decay(XAs76,XAs76_BETA_DECAY,XSe76);
        Do_radioactive_decay(XAs77,XAs77_BETA_DECAY,XSe77);
        Do_radioactive_decay(XAu198,XAu198_BETA_DECAY,XHg198);
        Do_radioactive_decay(XAu199,XAu199_BETA_DECAY,XHg199);
        Do_radioactive_decay(XBa139,XBa139_BETA_DECAY,XLa139);
        Do_radioactive_decay(XBa140,XBa140_BETA_DECAY,XLa140);
        Do_radioactive_decay(XBi210,XBi210_BETA_DECAY,XPo210);
        Do_radioactive_decay(XBr80,XBr80_BETA_DECAY,XKr80);
        Do_radioactive_decay(XBr82,XBr82_BETA_DECAY,XKr82);
        Do_radioactive_decay(XBr83,XBr83_BETA_DECAY,XKr83);
        Do_radioactive_decay(XCd109,XCd109_BETA_DECAY,XAg109);
        Do_radioactive_decay(XCd113,XCd113_BETA_DECAY,XIn113);
        Do_radioactive_decay(XCd115,XCd115_BETA_DECAY,XIn115);
        Do_radioactive_decay(XCd117,XCd117_BETA_DECAY,XIn117);
        Do_radioactive_decay(XCe141,XCe141_BETA_DECAY,XPr141);
        Do_radioactive_decay(XCe143,XCe143_BETA_DECAY,XPr143);
        Do_radioactive_decay(XCl36,XCl36_BETA_DECAY,XAr36);
        Do_radioactive_decay(XCs134,XCs134_BETA_DECAY,XBa134);
        Do_radioactive_decay(XCs136,XCs136_BETA_DECAY,XBa136);
        Do_radioactive_decay(XCs137,XCs137_BETA_DECAY,XBa137);
        Do_radioactive_decay(XCu67,XCu67_BETA_DECAY,XZn67);
        Do_radioactive_decay(XDy165,XDy165_BETA_DECAY,XHo165);
        Do_radioactive_decay(XDy166,XDy166_BETA_DECAY,XHo166);
        Do_radioactive_decay(XEr165,XEr165_BETA_DECAY,XHo165);
        Do_radioactive_decay(XEr169,XEr169_BETA_DECAY,XTm169);
        Do_radioactive_decay(XEr171,XEr171_BETA_DECAY,XTm171);
        Do_radioactive_decay(XEr172,XEr172_BETA_DECAY,XTm172);
        Do_radioactive_decay(XEu152,XEu152_BETA_DECAY,XSm152);
        Do_radioactive_decay(XEu154,XEu154_BETA_DECAY,XGd154);
        Do_radioactive_decay(XEu155,XEu155_BETA_DECAY,XGd155);
        Do_radioactive_decay(XEu156,XEu156_BETA_DECAY,XGd156);
        Do_radioactive_decay(XEu157,XEu157_BETA_DECAY,XGd157);
        Do_radioactive_decay(XGa72,XGa72_BETA_DECAY,XGe72);
        Do_radioactive_decay(XGa73,XGa73_BETA_DECAY,XGe73);
        Do_radioactive_decay(XGd153,XGd153_BETA_DECAY,XEu153);
        Do_radioactive_decay(XGd159,XGd159_BETA_DECAY,XTb159);
        Do_radioactive_decay(XHf181,XHf181_BETA_DECAY,XTa181);
        Do_radioactive_decay(XHf182,XHf182_BETA_DECAY,XTa182);
        Do_radioactive_decay(XHg203,XHg203_BETA_DECAY,XTl203);
        Do_radioactive_decay(XHo163,XHo163_BETA_DECAY,XDy163);
        Do_radioactive_decay(XHo164,XHo164_BETA_DECAY,XDy164);
        Do_radioactive_decay(XHo166,XHo166_BETA_DECAY,XEr166);
        Do_radioactive_decay(XHo167,XHo167_BETA_DECAY,XEr167);
        Do_radioactive_decay(XI128,XI128_BETA_DECAY,XXe128);
        Do_radioactive_decay(XI129,XI129_BETA_DECAY,XXe129);
        Do_radioactive_decay(XI130,XI130_BETA_DECAY,XXe130);
        Do_radioactive_decay(XI131,XI131_BETA_DECAY,XXe131);
        Do_radioactive_decay(XIn115,XIn115_BETA_DECAY,XSn115);
        Do_radioactive_decay(XIn117,XIn117_BETA_DECAY,XSn117);
        Do_radioactive_decay(XIr192,XIr192_BETA_DECAY,XPt192);
        Do_radioactive_decay(XIr194,XIr194_BETA_DECAY,XPt194);
        Do_radioactive_decay(XIr195,XIr195_BETA_DECAY,XPt195);
        Do_radioactive_decay(XKr85,XKr85_BETA_DECAY,XRb85);
        Do_radioactive_decay(XLa140,XLa140_BETA_DECAY,XCe140);
        Do_radioactive_decay(XLa141,XLa141_BETA_DECAY,XCe141);
        Do_radioactive_decay(XLu176,XLu176_BETA_DECAY,XHf176);
        Do_radioactive_decay(XLu177,XLu177_BETA_DECAY,XHf177);
        Do_radioactive_decay(XLu178,XLu178_BETA_DECAY,XHf178);
        Do_radioactive_decay(XMo93,XMo93_BETA_DECAY,XNb93);
        Do_radioactive_decay(XMo99,XMo99_BETA_DECAY,XTc99);
        Do_radioactive_decay(XNb94,XNb94_BETA_DECAY,XMo94);
        Do_radioactive_decay(XNb95,XNb95_BETA_DECAY,XMo95);
        Do_radioactive_decay(XNb96,XNb96_BETA_DECAY,XMo96);
        Do_radioactive_decay(XNd147,XNd147_BETA_DECAY,XPm147);
        Do_radioactive_decay(XNd149,XNd149_BETA_DECAY,XPm149);
        Do_radioactive_decay(XOs191,XOs191_BETA_DECAY,XIr191);
        Do_radioactive_decay(XOs193,XOs193_BETA_DECAY,XIr193);
        Do_radioactive_decay(XOs194,XOs194_BETA_DECAY,XIr194);
        Do_radioactive_decay(XPb205,XPb205_BETA_DECAY,XTl205);
        Do_radioactive_decay(XPd107,XPd107_BETA_DECAY,XAg107);
        Do_radioactive_decay(XPd109,XPd109_BETA_DECAY,XAg109);
        Do_radioactive_decay(XPm145,XPm145_BETA_DECAY,XNd145);
        Do_radioactive_decay(XPm146,XPm146_BETA_DECAY,XNd146);
        Do_radioactive_decay(XPm147,XPm147_BETA_DECAY,XSm147);
        Do_radioactive_decay(XPm148,XPm148_BETA_DECAY,XSm148);
        Do_radioactive_decay(XPm149,XPm149_BETA_DECAY,XSm149);
        Do_radioactive_decay(XPr142,XPr142_BETA_DECAY,XNd142);
        Do_radioactive_decay(XPr143,XPr143_BETA_DECAY,XNd143);
        Do_radioactive_decay(XPt193,XPt193_BETA_DECAY,XIr193);
        Do_radioactive_decay(XPt197,XPt197_BETA_DECAY,XAu197);
        Do_radioactive_decay(XRb86,XRb86_BETA_DECAY,XSr86);
        Do_radioactive_decay(XRb87,XRb87_BETA_DECAY,XSr87);
        Do_radioactive_decay(XRe186,XRe186_BETA_DECAY,XOs186);
        Do_radioactive_decay(XRe187,XRe187_BETA_DECAY,XOs187);
        Do_radioactive_decay(XRe188,XRe188_BETA_DECAY,XOs188);
        Do_radioactive_decay(XRh105,XRh105_BETA_DECAY,XPd105);
        Do_radioactive_decay(XRu103,XRu103_BETA_DECAY,XRh103);
        Do_radioactive_decay(XRu97,XRu97_BETA_DECAY,XTc97);
        Do_radioactive_decay(XSb122,XSb122_BETA_DECAY,XTe122);
        Do_radioactive_decay(XSb124,XSb124_BETA_DECAY,XTe124);
        Do_radioactive_decay(XSb125,XSb125_BETA_DECAY,XTe125);
        Do_radioactive_decay(XSm145,XSm145_BETA_DECAY,XPm145);
        Do_radioactive_decay(XSm151,XSm151_BETA_DECAY,XEu151);
        Do_radioactive_decay(XSm153,XSm153_BETA_DECAY,XEu153);
        Do_radioactive_decay(XSn121,XSn121_BETA_DECAY,XSb121);
        Do_radioactive_decay(XSn123,XSn123_BETA_DECAY,XSb123);
        Do_radioactive_decay(XSr89,XSr89_BETA_DECAY,XY89);
        Do_radioactive_decay(XSr90,XSr90_BETA_DECAY,XY90);
        Do_radioactive_decay(XTa179,XTa179_BETA_DECAY,XHf179);
        Do_radioactive_decay(XTa180,XTa180_BETA_DECAY,XHf180);
        Do_radioactive_decay(XTa182,XTa182_BETA_DECAY,XW182);
        Do_radioactive_decay(XTa183,XTa183_BETA_DECAY,XW183);
        Do_radioactive_decay(XTa184,XTa184_BETA_DECAY,XW184);
        Do_radioactive_decay(XTb160,XTb160_BETA_DECAY,XDy160);
        Do_radioactive_decay(XTb161,XTb161_BETA_DECAY,XDy161);
        Do_radioactive_decay(XTc97,XTc97_BETA_DECAY,XMo97);
        Do_radioactive_decay(XTc98,XTc98_BETA_DECAY,XRu98);
        Do_radioactive_decay(XTe127,XTe127_BETA_DECAY,XI127);
        Do_radioactive_decay(XTl204,XTl204_BETA_DECAY,XPb204);
        Do_radioactive_decay(XTm170,XTm170_BETA_DECAY,XYb170);
        Do_radioactive_decay(XTm171,XTm171_BETA_DECAY,XYb171);
        Do_radioactive_decay(XTm172,XTm172_BETA_DECAY,XYb172);
        Do_radioactive_decay(XW181,XW181_BETA_DECAY,XTa181);
        Do_radioactive_decay(XW185,XW185_BETA_DECAY,XRe185);
        Do_radioactive_decay(XW187,XW187_BETA_DECAY,XRe187);
        Do_radioactive_decay(XW188,XW188_BETA_DECAY,XRe188);
        Do_radioactive_decay(XXe133,XXe133_BETA_DECAY,XCs133);
        Do_radioactive_decay(XXe135,XXe135_BETA_DECAY,XCs135);
        Do_radioactive_decay(XY90,XY90_BETA_DECAY,XZr90);
        Do_radioactive_decay(XY91,XY91_BETA_DECAY,XZr91);
        Do_radioactive_decay(XYb175,XYb175_BETA_DECAY,XLu175);
        Do_radioactive_decay(XZr95,XZr95_BETA_DECAY,XNb95);

        /*
         * Alpha decays
         */
        Do_radioactive_decay(XNd144,XNd144_ALPHA_DECAY,XCe140);
        Do_radioactive_decay(XPm145,XPm145_ALPHA_DECAY,XPr141);
        Do_radioactive_decay(XSm146,XSm146_ALPHA_DECAY,XNd142);
        Do_radioactive_decay(XSm147,XSm147_ALPHA_DECAY,XNd143);
        Do_radioactive_decay(XSm148,XSm148_ALPHA_DECAY,XNd144);
        Do_radioactive_decay(XGd152,XGd152_ALPHA_DECAY,XSm148);
        Do_radioactive_decay(XOs186,XOs186_ALPHA_DECAY,XW182);
        Do_radioactive_decay(XPo210,XPo210_ALPHA_DECAY,XPb206);
#endif // NUCSYN_ALL_ISOTOPES
    }
}

#endif // NUCSYN_RADIOACTIVE_DECAY && NUCSYN
