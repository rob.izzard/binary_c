#ifndef NUCSYN_SN_YIELD_H
#define NUCSYN_SN_YIELD_H

/*
 *if you enable SN_TEST then the code will be built but instead of actually 
 * giving a SN you'll be presented with the yields from differing models
 * for different masses at the current metallicity
 */

//#define SN_TEST
#ifdef SN_TEST
static void nucsyn_sn_test(struct stardata_t *stardata);
#endif //SN_TEST

/*
 * Other_exploder catches cases that are not dealt with.
 * These should be considered errors.
 */
#define Other_exploder(A) Exit_binary_c(        \
        BINARY_C_OUT_OF_RANGE,                           \
        "Other exploder error %d : stellar types pre_explosion_star %d, exploder %d, companion %d\n", \
        A,                                                              \
        pre_explosion_star->stellar_type,                               \
        exploder->stellar_type,                                         \
        companion->stellar_type);

#endif // NUCSYN_SN_YIELD_H
