#ifndef NUCSYN_SIGMAV_H
#define NUCSYN_SIGMAV_H


/* Macros for the sigmav elements, to make things more readable */

#include "nucsyn_sigmav.def"


/*
 * Construct evolution loop codes
 */
#undef X
#define X(CODE,STRING) SIGMAV_##CODE,
enum {
    NUCSYN_SIGMAV_CODES
    SIGMAV_SIZE
} ;

#undef X
#define X(CODE,STRING) #CODE,
static char * NUCSYN_SIGMAV_code_macros[] Maybe_unused = { NUCSYN_SIGMAV_CODES };

#undef X
#define X(CODE,STRING) #CODE,
static char * NUCSYN_SIGMAV_code_strings[] Maybe_unused = { NUCSYN_SIGMAV_CODES };

#define NUCSYN_SIGMAV_string(N) Array_string(NUCSYN_SIGMAV_code_strings,(N))

//#define SIGMAV_SIZE Array_size(NUCSYN_SIGMAV_code_strings)

#undef X

#endif //NUCSYN_SIGMAV_H
