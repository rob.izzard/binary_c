#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef NUCSYN

double Pure_function nucsyn_envelope_mass(const struct star_t * Restrict const star)
{
    /*
     * Return the envelope mass through which thermohaline mixing
     * should occur. The mixing is into star
     */
    if(star->stellar_type < HERTZSPRUNG_GAP)
    {
        return star->mass;
    }
    else if(star->stellar_type >= HeWD)
    {
        return 0.0;
    }
    else
    {
        return star->mass - Outermost_core_mass(star);
    }
}


#endif // NUCSYN
