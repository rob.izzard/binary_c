#include "../binary_c.h"
No_empty_translation_unit_warning;
#define COMPRESS_YIELD_OUTPUT
#ifdef NUCSYN
#include "../ensemble/ensemble_macros.h"
#include "nucsyn_binary_yield.h"
#include "nucsyn_strided_isotope_loops.h"

Nonnull_some_arguments(1)
void nucsyn_binary_yield(struct stardata_t * Restrict const stardata,
                         const Boolean final Maybe_unused)
{
    /*
     * Function to log chemical yields from the binary system
     * often as a function of source type.
     *
     * Note: this function does not add up the yields, it only
     * outputs them. When the timestep is zero, it does nothing.
     * This function relies on the binary_c timestep being short
     * enough to resolve yield timesteps (this is done in the
     * timestepping code automatically).
     */
#ifdef NUCSYN_YIELDS
    const Boolean ensemble_yields = Use_ensemble(STELLAR_POPULATIONS_ENSEMBLE_FILTER_CHEMICAL_YIELDS);
    /*
     * The time we report is set (and binned) by the ensemble, if we're using
     * that, otherwise it's the model's time.
     */
    const Time T =
        stardata->preferences->ensemble == TRUE ?
        ensemble_output_time(stardata) :
        stardata->model.time;
    Dprint("at T %g\n",T);

    Star_number Maybe_unused k;

#ifdef STELLAR_POPULATIONS_ENSEMBLE
    if(stardata->preferences->ensemble == TRUE &&
       unlikely(stardata->model.ensemble_cdict == NULL))
    {
        make_ensemble_cdict(stardata);
    }
#endif // STELLAR_POPULATIONS_ENSEMBLE

    /*
     * First timestep setup
     */
    if(unlikely(Is_zero(stardata->model.time)))
    {
        first_timestep_setup(stardata);
    }

    /*
     * When running with log times, we require a start time
     * and cannot output until then.
     */
    if(stardata->preferences->ensemble_logtimes == TRUE &&
       unlikely(stardata->model.time < stardata->preferences->ensemble_startlogtime))
    {
        return;
    }

    /*
     * Disable end logging?
     */
    else if(final == TRUE &&
            stardata->preferences->disable_end_logging == TRUE)
    {
        return;
    }

    else if(
#ifndef NUCSYN_LOG_YIELDS_EVERY_TIMESTEP
        /*
         * We only want to output the yields if "final" is TRUE
         */
        final == FALSE ||
#endif // NUCSYN_LOG_YIELDS_EVERY_TIMESTEP
        /*
         * Prevent yield output in old BSE algorithm
         * and when the timestep is zero: yields will be saved
         * for the next positive timestep.
         */
        stardata->model.dt < TINY)
    {
        return;
    }

    const Boolean _legacy = stardata->preferences->legacy_yields;

#ifdef NUCSYN_SPARSE_YIELDS
    /*
     * only output yields when the stellar population
     * ensemble triggers (unless legacy yields are enabled)
     */
    const Boolean output = timestep_fixed_trigger(stardata,
                                                  FIXED_TIMESTEP_ENSEMBLE);

    if(final==FALSE && output==FALSE)
    {
        /*
         * No output required
         */
        Dprint("Timestep FIXED_TIMESTEP_ENSEMBLE failed to trigger at t=%g T=%g\n",
               stardata->model.time,
               T);
        if(_legacy == FALSE)
        {
            return;
        }
    }
#endif // NUCSYN_SPARSE_YIELDS

#ifdef NUCSYN_IGNORE_ZERO_BINARY_DX_YIELD
    /*
     * Test if the summed yields are > 0, if not, do nothing
     */
    Boolean yields_are_zero = TRUE;
    yields_are_zero = FALSE;
    Forward_isotope_loop(i)
    {
        if(Total_yield(i,stardata)>TINY)
        {
            yields_are_zero = FALSE;
            break;
        }
    }

    if(yields_are_zero==TRUE)
    {
        /*
         * no yield : just return
         */
        Dprint("yields are zero (star0 H = %g, He = %g) : return\n",
               stardata->star[0].Xyield[XH1],
               stardata->star[0].Xyield[XHe4]
            );
        return;
    }
#endif // NUCSYN_IGNORE_ZERO_BINARY_DX_YIELD

#ifdef NUCSYN_YIELD_COMPRESSION
    /*
     * Check if yield integrals have changed.
     * If not, don't do anything!
     * (except the first timestep)
     */
    if(stardata->model.time > TINY)
    {
        Boolean changed = FALSE;
        Forward_isotope_loop(i)
        {
            if(!Fequal(Total_yield(i,stardata),stardata->model.oldyields[i]))
            {
                changed = TRUE;
                break;
            }
        }
        if(changed == FALSE)
        {
            return;
        }
    }
#endif // NUCSYN_YIELD_COMPRESSION

#ifdef STELLAR_POPULATIONS_ENSEMBLE
    /*
     * Make sure the ensemble has initial abundance
     * information if it hasn't already been set
     */
    if(ensemble_yields == TRUE &&
       stardata->model.ensemble_cdict != NULL)
    {
        const struct cdict_entry_t * const entry =
            CDict_nest_get_entry(stardata->model.ensemble_cdict,
                                 (char*)"Xinit");
        if(entry == NULL)
        {
            for(Isotope i=0;i<ISOTOPE_ARRAY_SIZE;i++)
            {
                Set_ensemble_initial_abundance(
                    (char*)"Xinit",
                    (char*)"isotope",(char*)stardata->store->isotope_strings[i],
                    (double)stardata->preferences->zero_age.XZAMS[i]
                    );
            }
        }
    }
#endif // STELLAR_POPULATIONS_ENSEMBLE

#ifdef NUCSYN_ID_SOURCES_GCE
    /*
     * Pre-fetch all the Xsource and oldsource pointers:
     * these will always be checked in the following loop.
     */
    Foreach_star(star)
    {
        Sourceloop(s)
        {
            __builtin_prefetch(&star->Xsource[s]);
            __builtin_prefetch(&star->oldsource[s]);
        }
    }
#endif // NUCSYN_ID_SOURCES_GCE

    const double timestep Maybe_unused =
        stardata->preferences->ensemble_logtimes ?
        stardata->preferences->ensemble_logdt :
        stardata->preferences->ensemble_dt;

    Sourceloop(s)
    {
        /*
         * Yield from source s
         */
#ifdef COMPRESS_YIELD_OUTPUT
        unsigned int zerocount = 0;
#endif // COMPRESS_YIELD_OUTPUT

        /*
         * Output "legacy", i.e. pre-ensemble, yields.
         * Still useful for testing!
         */
        if(
            _legacy == TRUE
#ifndef NUCSYN_LOG_BINARY_X_YIELDS
            && unlikely(final==TRUE)
#endif // NUCSYN_LOG_BINARY_X_YIELDS
            )
        {
            _Print_Time;
        }

        /*
         * Loop to output yield of each isotope
         */
#ifdef NUCSYN_LOG_BINARY_X_YIELDS
#ifdef NUCSYN_ID_SOURCES_GCE
        if(_legacy == TRUE)
        {
            for(Isotope i=0;i<ISOTOPE_ARRAY_SIZE;i++)
            {
                const double dy = Total_yield_from_source6(stardata,s,i);
                if(Yield_is_zero(dy))
                {
                    Print_zero_yield;
                }
                else
                {
                    Dump_any_zeros_and_reset;
                    Yield_print(dy);
                    if(ensemble_yields == TRUE)
                    {
                        Ensemble_yield;
                    }
                }
            }
        }
        else
        {
            /*
             * No legacy yields, but still perhaps save
             * the yields for the ensemble
             */
            Ensemble_yields;
        }

#else // NUCSYN_ID_SOURCES_GCE
        if(_legacy == TRUE)
        {
            for(Isotope i=0;i<ISOTOPE_ARRAY_SIZE;i++)
            {
                if(Yield_is_zero(Total_yield(i,stardata)))
                {
                    Print_zero_yield;
                }
                else
                {
                    Dump_any_zeros_and_reset;
                    Yield_print(Total_yield(i,stardata));
                }
            }
#endif //NUCSYN_ID_SOURCES_GCE

#else // NUCSYN_LOG_BINARY_X_YIELDS
            if(unlikely(final==TRUE))
            {
                Dump_any_zeros_and_reset;
                Yield_print(Total_yield(i,stardata));
            }
        }
#endif // NUCSYN_LOG_BINARY_X_YIELDS

        if(_legacy == TRUE)
        {
            Dump_any_zeros;
#ifdef NUCSYN_ID_SOURCES_GCE
            Printf("\n");
#endif // NUCSYN_ID_SOURCES_GCE
        }
    } // end of loop over source types

    /*
     * Binary mp yields
     */
    if(_legacy == TRUE)
    {
#if defined NUCSYN_LOG_BINARY_X_YIELDS && \
    !defined NUCSYN_ID_SOURCES_GCE
        Printf("\n");
#else
        if(unlikely(final==TRUE))
        {
            Printf("\n");
        }
#endif // NUCSYN_LOG_BINARY_X_YIELDS
    }

#ifdef NUCSYN_LOG_BINARY_MPYIELDS
    Printf("MPYIELDbin__ %0.4e ",T);
    const double * XZAMS = stardata->preferences->zero_age.XZAMS;
    const double total_ZAMS_mass = stardata->common.zero_age.mass[0] + stardata->common.zero_age.mass[1];
    double mass_ejected[NUMBER_OF_STARS]={0.0};
    Foreach_star(star)
    {
        mass_ejected[star->starnum] = stardata->common.zero_age.mass[star->starnum] - star->baryonic_mass;
    }

    Forward_isotope_loop(i)
    {
        double mp_yield_i = 0.0;
        Foreach_star(star)
        {
            /*
             * Yield p_m : star->Xyield[i] is mass ejected as isotope i
             */
            const double d = star->Xyield[i] - XZAMS[i] * mass_ejected[star->starnum];
            star->mpyield[i] = d;
            mp_yield_i += d;
        }

        if(_legacy == TRUE)
        {
            /*
             * remember that to get the p(i) you must divide by the zams mass
             */
            Yield_print(mp_yield_i/total_ZAMS_mass);
        }

#ifdef STELLAR_POPULATIONS_ENSEMBLE
        if(0)
        {
            /* not yet tested! */
            Set_ensemble_yield(
                (char*)"mpyield",
                (char*)"time",(double)T,
                (char*)"isotope",(char*)stardata->store->isotope_strings[i],
                (double)(mp_yield_i/total_ZAMS_mass*Ensemble_timestep_linear)
                );
        }
#endif//STELLAR_POPULATIONS_ENSEMBLE
    }
    Printf("\n");
#endif//NUCSYN_LOG_BINARY_MPYIELDS

#endif // NUCSYN_YIELDS

#ifdef SAVE_YIELDS
    /*
     * We need to reset oldyields
     *
     * First, set from star 0 with a copy.
     */
    {
        Abundance * const _oldyields = stardata->model.oldyields;
        Copy_abundances(stardata->star[0].Xyield,
                        _oldyields);

        /*
         * Now *add* the contents of stars > 0
         */
        Starloop(k,1)
        {
            Nucsyn_isotope_stride_loop(_oldyields[i] += stardata->star[k].Xyield[i]);
        }
    }

    Foreach_star(star)
    {
#ifdef NUCSYN_ID_SOURCES
        /*
         * Save old source yields so we can save or output the
         * differential yield.
         */
        Sourceloop(s)
        {
            if(star->Xsource[s] != NULL)
            {
                if(star->oldsource[s] == NULL)
                {
                    star->oldsource[s] = New_isotope_array;
                }
                Copy_abundances(star->Xsource[s],
                                star->oldsource[s]);
            }
        }
#endif //NUCSYN_ID_SOURCES
#endif//SAVE_YIELDS

#ifdef NANCHECKS
        nancheck("nucsyn_binary_yield",
                 star->Xyield,
                 ISOTOPE_ARRAY_SIZE);
#endif//NANCHECKS
    }

    return;

}

#ifdef NUCSYN_ID_SOURCES_GCE

static inline Abundance Pure_function Hot_function Maybe_unused
Total_yield_from_source(
    const struct stardata_t * Restrict const stardata,
    const Yield_source s,
    const Isotope i
    )
{
    /*
     * Return the total yield of isotope i from source s,
     * or the yield since the previous call
     */
    Abundance totalyield = 0.0;
#ifdef NUCSYN_ID_SOURCES
    //Foreach_star(star) // 7.41s
    Star_number k;
    Starloop(k) // 5.68
    {
        const struct star_t * Restrict const star = &stardata->star[k]; // 1.37
        //const Abundance * const Xsource = stardata->star[k].Xsource[s];//1.95
        //const Abundance * const oldsource = stardata->star[k].oldsource[s];//1.95
        const Abundance * const Xsource = star->Xsource[s];//1.17
        const Abundance * const oldsource = star->oldsource[s];//1.17

        //if(star->Xsource[s] != NULL) //1.37s
        if(Xsource)//0.39
        {
            //totalyield += star->Xsource[s][i];//0.29s
            totalyield += Xsource[i];//0.19s
        }

#ifdef NUCSYN_LOG_BINARY_DX_YIELDS
        /*
         * Require differential yield
         */
        //if(star->oldsource[s] != NULL)//1.37s
        if(oldsource)//0.39
        {
            //totalyield -= star->oldsource[s][i];//0.29s
            totalyield -= oldsource[i];//0.19s
        }
#endif
    }
#endif
    return totalyield;
}

static inline Abundance Pure_function Hot_function Maybe_unused
Total_yield_from_source1(
    const struct stardata_t * Restrict const stardata,
    const Yield_source s,
    const Isotope i
    )
{
    /*
     * Return the total yield of isotope i from source s,
     * or the yield since the previous call
     */
    Abundance totalyield = 0.0;
#ifdef NUCSYN_ID_SOURCES
    //Foreach_star(star) // 7.41s
    Star_number k;
    Starloop(k) // 5.68
    {
        const struct star_t * Restrict const star = &stardata->star[k]; // 1.37
        //const Abundance * const Xsource = stardata->star[k].Xsource[s];//1.95
        //const Abundance * const oldsource = stardata->star[k].oldsource[s];//1.95
        const Abundance * const Xsource = star->Xsource[s];//1.17
        const Abundance * const oldsource = star->oldsource[s];//1.17

        //if(star->Xsource[s] != NULL) //1.37s
        if(Xsource)//0.39
        {
            //totalyield += star->Xsource[s][i];//0.29s
            totalyield += Xsource[i];//0.19s
        }

#ifdef NUCSYN_LOG_BINARY_DX_YIELDS
        /*
         * Require differential yield
         */
        //if(star->oldsource[s] != NULL)//1.37s
        if(oldsource)//0.39
        {
            //totalyield -= star->oldsource[s][i];//0.29s
            totalyield -= oldsource[i];//0.19s
        }
#endif
    }
#endif
    return totalyield;
}

static inline Abundance Pure_function Hot_function Maybe_unused
Total_yield_from_source2(
    const struct stardata_t * Restrict const stardata,
    const Yield_source s,
    const Isotope i
    )
{
    /*
     * Return the total yield of isotope i from source s,
     * or the yield since the previous call
     */
    Abundance totalyield = 0.0;
#ifdef NUCSYN_ID_SOURCES
    //Foreach_star(star) // 7.41s
    Star_number k;
    Starloop(k) // 5.68
    {
        const Abundance * const Xsource = stardata->star[k].Xsource[s];//1.95
        const Abundance * const oldsource = stardata->star[k].oldsource[s];//1.95

        //if(star->Xsource[s] != NULL) //1.37s
        if(Xsource)//0.39
        {
            //totalyield += star->Xsource[s][i];//0.29s
            totalyield += Xsource[i];//0.19s
        }

#ifdef NUCSYN_LOG_BINARY_DX_YIELDS
        /*
         * Require differential yield
         */
        //if(star->oldsource[s] != NULL)//1.37s
        if(oldsource)//0.39
        {
            //totalyield -= star->oldsource[s][i];//0.29s
            totalyield -= oldsource[i];//0.19s
        }
#endif
    }
#endif
    return totalyield;
}

static inline Abundance Pure_function Hot_function Maybe_unused
Total_yield_from_source3(
    const struct stardata_t * Restrict const stardata,
    const Yield_source s,
    const Isotope i
    )
{
    /*
     * Return the total yield of isotope i from source s,
     * or the yield since the previous call
     */
    Abundance totalyield = 0.0;
#ifdef NUCSYN_ID_SOURCES
    Foreach_star(star) // 7.41s
    {
        //const struct star_t * Restrict const star = &stardata->star[k]; // 1.37
        //const Abundance * const Xsource = stardata->star[k].Xsource[s];//1.95
        //const Abundance * const oldsource = stardata->star[k].oldsource[s];//1.95
        const Abundance * const Xsource = star->Xsource[s];//1.17
        const Abundance * const oldsource = star->oldsource[s];//1.17
        //if(star->Xsource[s] != NULL) //1.37s
        if(Xsource)//0.39
        {
            //totalyield += star->Xsource[s][i];//0.29s
            totalyield += Xsource[i];//0.19s
        }

#ifdef NUCSYN_LOG_BINARY_DX_YIELDS
        /*
         * Require differential yield
         */
        //if(star->oldsource[s] != NULL)//1.37s
        if(oldsource)//0.39
        {
            //totalyield -= star->oldsource[s][i];//0.29s
            totalyield -= oldsource[i];//0.19s
        }
#endif
    }
#endif
    return totalyield;
}



static inline Abundance Pure_function Hot_function Maybe_unused
Total_yield_from_source4(
    const struct stardata_t * Restrict const stardata,
    const Yield_source s,
    const Isotope i
    )
{
    /*
     * Return the total yield of isotope i from source s,
     * or the yield since the previous call
     */
    Abundance totalyield = 0.0;
#ifdef NUCSYN_ID_SOURCES
    //Foreach_star(star) // 7.41s
    Star_number k;
    Starloop(k) // 5.68
    {
        const struct star_t * Restrict const star = &stardata->star[k]; // 1.37

        if(star->Xsource[s] != NULL) //1.37s
        {
            totalyield += star->Xsource[s][i];//0.29s
        }

#ifdef NUCSYN_LOG_BINARY_DX_YIELDS
        /*
         * Require differential yield
         */
        if(star->oldsource[s] != NULL)//1.37s
        {
            totalyield -= star->oldsource[s][i];//0.29s
        }
#endif
    }
#endif
    return totalyield;
}

static inline Abundance Pure_function Maybe_unused
Total_yield_from_source5(
    const struct stardata_t * Restrict const stardata,
    const Yield_source s,
    const Isotope i
    )
{
    /*
     * Return the total yield of isotope i from source s,
     * or the yield since the previous call
     */
    Abundance totalyield = 0.0;
#ifdef NUCSYN_ID_SOURCES
    Foreach_star(star) // 7.41s
    {
        Abundance * X;
        if((X = star->Xsource[s]))
        {
            totalyield += X[i];
        }

#ifdef NUCSYN_LOG_BINARY_DX_YIELDS
        /*
         * Require differential yield
         */
        if((X = star->oldsource[s]))
        {
            totalyield -= X[i];
        }
#endif
    }
#endif
    return totalyield;
}


static inline Abundance Pure_function Hot_function Maybe_unused
Total_yield_from_source6(
    const struct stardata_t * Restrict const stardata,
    const Yield_source s,
    const Isotope i
    )
{
    /*
     * Return the total yield of isotope i from source s,
     * or the yield since the previous call
     */
    Abundance totalyield = 0.0;
#ifdef NUCSYN_ID_SOURCES
    Foreach_star(star)
    {
        {
            Abundance * X;
            if((X = star->Xsource[s])) // 0.78
            {
                totalyield += X[i]; // 0.03
            }
        }

#ifdef NUCSYN_LOG_BINARY_DX_YIELDS
        /*
         * Require differential yield
         */
        {
            Abundance * X;
            if((X = star->oldsource[s])) // 0.78
            {
                totalyield -= X[i]; // 0.03
            }
        }
#endif
    }
#endif
    return totalyield;
}


static inline Abundance Pure_function Maybe_unused
Total_yield_from_source6a(
    const struct stardata_t * Restrict const stardata,
    const Yield_source s,
    const Isotope i
    )
{
    /*
     * Return the total yield of isotope i from source s,
     * or the yield since the previous call
     */
    Abundance totalyield = 0.0;
#ifdef NUCSYN_ID_SOURCES
    Star_number k;
    Starloop(k) // 5.68
    {
        const struct star_t * const star = &stardata->star[k];
        {
            Abundance * X;
            if((X = star->Xsource[s]))
            {
                totalyield += X[i];
            }
        }

#ifdef NUCSYN_LOG_BINARY_DX_YIELDS
        /*
         * Require differential yield
         */
        {
            Abundance * X;
            if((X = star->oldsource[s]))
            {
                totalyield -= X[i];
            }
        }
#endif
    }
#endif
    return totalyield;
}

static inline Abundance Pure_function Maybe_unused
Total_yield_from_source6b(
    const struct stardata_t * Restrict const stardata,
    const Yield_source s,
    const Isotope i
    )
{
    /*
     * Return the total yield of isotope i from source s,
     * or the yield since the previous call
     */
    Abundance totalyield = 0.0;
#ifdef NUCSYN_ID_SOURCES
    Star_number k;
    Starloop(k) // 5.68
    {
        {
            Abundance * X;
            if((X = stardata->star[k].Xsource[s]))
            {
                totalyield += X[i];
            }
        }

#ifdef NUCSYN_LOG_BINARY_DX_YIELDS
        /*
         * Require differential yield
         */
        {
            Abundance * X;
            if((X = stardata->star[k].oldsource[s]))
            {
                totalyield -= X[i];
            }
        }
#endif
    }
#endif
    return totalyield;
}


static inline Abundance Pure_function Hot_function Maybe_unused
Total_yield_from_source7(
    const struct stardata_t * Restrict const stardata,
    const Yield_source s,
    const Isotope i
    )
{
    /*
     * Return the total yield of isotope i from source s,
     * or the yield since the previous call
     */
    Abundance totalyield = 0.0;
#ifdef NUCSYN_ID_SOURCES
    Foreach_star(star) // 7.41s
    {
        totalyield += star->Xsource[s] ? star->Xsource[s][i] : 0.0;

#ifdef NUCSYN_LOG_BINARY_DX_YIELDS
        /*
         * Require differential yield
         */
        totalyield -= star->oldsource[s] ? star->oldsource[s][i] : 0.0;
#endif
    }
#endif
    return totalyield;
}

static inline Abundance Pure_function Maybe_unused
Total_yield_from_source8(
    const struct stardata_t * Restrict const stardata,
    const Yield_source s,
    const Isotope i
    )
{
    /*
     * Return the total yield of isotope i from source s,
     * or the yield since the previous call
     */
    Abundance totalyield = 0.0;
#ifdef NUCSYN_ID_SOURCES
    Foreach_star(star) // 7.41s
    {
        totalyield += *(star->Xsource+s) ? (*(star->Xsource+s))[i] : 0.0;

#ifdef NUCSYN_LOG_BINARY_DX_YIELDS
        /*
         * Require differential yield
         */
        totalyield -= *(star->oldsource+s) ? (*(star->oldsource+s))[i] : 0.0;
#endif
    }
#endif
    return totalyield;
}

static inline Abundance Pure_function Maybe_unused
Total_yield_from_source9(
    const struct stardata_t * Restrict const stardata,
    const Yield_source s,
    const Isotope i
    )
{
    /*
     * Return the total yield of isotope i from source s,
     * or the yield since the previous call
     */
    Abundance totalyield = 0.0;
#ifdef NUCSYN_ID_SOURCES
    Foreach_star(star) // 7.41s
    {
        totalyield += *(star->Xsource+s) ? *(*(star->Xsource+s)+i) : 0.0;

#ifdef NUCSYN_LOG_BINARY_DX_YIELDS
        /*
         * Require differential yield
         */
        totalyield -= *(star->oldsource+s) ? *(*(star->oldsource+s)+i) : 0.0;
#endif
    }
#endif
    return totalyield;
}



#endif // NUCSYN_ID_SOURCES_GCE

static Abundance Pure_function Maybe_unused Hot_function
Total_yield(const Isotope i,
            struct stardata_t * Restrict const stardata)
{
    /*
     * Return the total yield of isotope i,
     * or the yield since the previous call
     */
    Abundance totalyield = 0.0;
    Star_number k;

    Starloop(k)
    {
        totalyield += stardata->star[k].Xyield[i];
    }
#ifdef NUCSYN_LOG_BINARY_DX_YIELDS
    /*
     * Require differential yield
     */
    totalyield -= stardata->model.oldyields[i];
#endif
    return totalyield;
}


static void first_timestep_setup(struct stardata_t * Restrict const stardata Maybe_unused)
{
#ifdef SAVE_YIELDS

#if defined NUCSYN_YIELD_COMPRESSION || \
    defined NUCSYN_LOG_BINARY_DX_YIELDS
    memset(stardata->model.oldyields,0,ISOTOPE_MEMSIZE);
#endif

#endif // SAVE_YIELDS
}


#endif // NUCSYN
