#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef NUCSYN

/*
 * enable REACTION_RATES_TABLE to output a table of rates
 * to stderr
 */
//#define REACTION_RATES_TABLE

#define Reaclib(...) nucsyn_reaclib(t9,i9,tm13,t913,t953,lt9,__VA_ARGS__)

void nucsyn_set_sigmav(struct stardata_t * const stardata,
                       struct store_t * const store,
                       const double temp, /* log temperature */
                       Reaction_rate * const sigmav, /* reaction rates */
                       const double * Restrict const multipliers /* reaction rate multipliers (ignored if NULL) */
#ifdef RATES_OF_AMANDA_NE22PG_FIX
                       , const Boolean ne22pg_cf88
#endif
    )
{
#ifdef NUCSYN_SIGMAV_PRE_INTERPOLATE
    /*
     * if temp < 0 then we do not want to use the
     * interpolation table, and we really want -1 * temp
     */
    const double t9 = exp10(fabs(temp) - 9.0) ;
    Boolean called_by_preset = temp < 0.0 ? TRUE : FALSE;
#else
    const double t9 = exp10(temp-9.0);
#endif

    /* default to zero, as some rates are not set */
    memset(sigmav,0,sizeof(double)*SIGMAV_SIZE);

#ifdef NUCSYN_SIGMAV_PRE_INTERPOLATE
    if(called_by_preset==FALSE &&
       In_range(t9,
                NUCSYN_SIGMAV_INTERPOLATION_MIN,
                NUCSYN_SIGMAV_INTERPOLATION_MID))
    {
        nucsyn_set_sigmav_from_table(stardata,store,t9,sigmav);
    }
    else
    {
#endif

        /*
         * Calculate the array of rate constants at the temperature temp
         *
         * These formulae were mostly taken from the NACRE compliation. Page numbers
         * refer to their (currently downloadable) document nacre2.(ps.gz/dvi)
         *
         * See that paper and also Clayton, D.D., his famous book p.395. He deals
         * with tau (the timescale) instead of <sigma v> but that's ok, convert via
         * <sigma v> x (number density) x (tau) = 1.0
         *
         * Updated 13/09/2006: now you can use one of four reaction rate sets:
         * DEFAULT : (mostly) NACRE
         * MARIA, for the NeNa/MgAl variation paper (NACRE + updated)
         * AMANDA, from the Monash code (old rates + some updated NACRE)
         * AREND_JAN, from Arend Jan Poelarend/Norbert Langer's code (STERN)
         */

        /*
         * Determine temperature of previous call to sigmav, if it's not very
         * different then don't reset the sigmav array
         */

        /* speed advances (not pretty, but functional) */
        const double tm13 = 1.0/cbrt(t9); // t9^(-1/3)
        const double t23 = Pow2(tm13);    // t9^(-2/3)
        const double t15 = 1.0/Pow1p5(t9);// t9^(-3/2)
        const double t92 = Pow2(t9);      // t9^2
        const double t93 = t92*t9;        // t9^3
        const double t94 = t93*t9;        // t9^4
        const double t95 = t94*t9;        // t9^5
        //const double logt9 = log10(t9);
        const double i9 = 1.00/t9;        // t9^-1
        const double t953 = pow(t9,5.0/3.0);
        const double lt9 = log(t9); // ln(T9)
        const double t913 = cbrt(t9);

#ifdef RATES_OF_AREND_JAN
        /* Sorry for the fortran variables */
        const double  T9 = t9;
        const double  TP12 = sqrt(T9);
        const double  TP13 = pow(T9,(1.0e0/3.0e0));
        const double  TP14 = sqrt(TP12);
        const double  TP15 = pow(T9,(1.0e0/5.0e0));
        const double  TP23 = Pow2(TP13);
        const double  TP32 = T9*TP12;
        const double  TP43 = T9*TP13;
        const double  TP53 = TP43*TP13;
        const double  TP54 = T9*TP14;
        const double  TP65 = T9*TP15;
        const double  TM13 = 1.0e0/TP13;
        const double  TM23 = pow(TM13,2);
        const double  TM32 = 1.0e0/TP32;
        const double  TM9 = 1.0e0/T9;
        const double  TM12 = 1.0e0/TP12;
        const double  TM54 = 1.0e0/TP54;
        const double  TM65 = 1.0e0/TP65;
        const double T913 = TP13;
        const double T923 = TP23;
        const double T943 = TP43;
        const double T953 = TP53;
        const double T915 = TP15;
        const double T912 = sqrt(T9);
        const double T932 = pow(T9,3.0/2.0);
        const double T972 = pow(T9,7.0/2.0);
        const double T935 = pow(T9,3.0/5.0);
        const double T958 = pow(T9,5.0/8.0);
        const double T914 = pow(T9,1.0/4.0);
        const double T927 = pow(T9,2.0/7.0);
#endif // RATES_OF_AREND_JAN

#ifdef RATES_OF_MARIA



#endif//RATES_OF_MARIA

#ifdef RATES_OF_AMANDA
        /* extra stuff for calculating the NeNa and MgAl rates
           which Amanda's models used */
        double a0,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10;
        double a11,a12,a13,a14,a15,a16,a17,a18,a19,a20;
        double a21,a22,a23,a24,a25,a26,a27,a28,a29,a30;
        double a31,a32,a33,a34,a35,adc;
        double amu,amu32,b1;
        double a;
        /* taken from Maria's list */
        const double  t913=cbrt(t9);
        const double  t923=Pow2(t913);
        const double  t943=Pow2(t923);
        const double  t953=t913*t943;
        const double  t932=pow(t9,1.5);
        const double  t914=pow(t9,0.25);
#endif // RATES_OF_AMANDA


        /************************************************************/
        /************************************************************/
        /************************************************************/
        /*** Light elements (H,H2,He3,He4, Be,Li,B etc) *************/
        /************************************************************/
        /************************************************************/
        /************************************************************/

        /*
         * H + H -> D + nu + beta+
         * NACRE Page 112
         */
        sigmav[SIGMAV_PP]=4.08e-15*t23*exp(-3.381*tm13)*
            (1.0+3.82*t9+1.51*t92+0.144*t93-1.14e-2*t94);
        sigmav[SIGMAV_PP] /= AVOGADRO_CONSTANT;

        /*
         * H2 + H1 -> He3 + beta
         */
        if(t9<=0.11)
        {
            sigmav[SIGMAV_DP] = 1.81e3*t23*exp(-3.721*tm13)*(1.0+14.3*t9-90.5*t92+395.0*t93);
        }
        else
        {
            sigmav[SIGMAV_DP] = 2.58e3*t23*exp(-3.721*tm13)*(1.0+3.96*t9+0.116*t92);
        }
        sigmav[SIGMAV_DP]/=AVOGADRO_CONSTANT;

        /*
         * H2 + H2 -> He4 + gamma
         */
        sigmav[SIGMAV_DD]=42.1*t23*exp(-4.259*tm13)*(1.0+0.514*t9+0.339*t92-1.18e-2*t93);
        sigmav[SIGMAV_DD]/=AVOGADRO_CONSTANT;

        /*
         * H2 + H2 -> n + He3
         */
        /*
          sigmav[]=4.67e8*t23*exp(-4.259*tm13)*(1.0+1.079*t9-0.1124*t92+5.68e-3*t93);
          sigmav[]/=AVOGADRO_CONSTANT;
        */

        /*
         * H2 + H2 -> H3 + p
         */
        /*
          sigmav[]=4.66e8*t23*exp(-4.259*tm13)*(1.0+0.759*t9-0.0612*t92+2.78e-3*t93);
          sigmav[]/=AVOGADRO_CONSTANT;
        */

        /*
         * H2 + alpha -> Li6
         */
        /*
          sigmav[]=14.82*t23*exp(-7.435*tm13)*(1.0+6.572*t9+0.076*t92+0.0248*t93)+82.8*t15*exp(-7.904*i9);
          sigmav[]/=AVOGADRO_CONSTANT;
        */

        /*
         * H3 + D -> He4 + n
         */
        /*
          sigmav[]=8.29e10*t23*exp(-4.524*tm13-t92/Pow2(0.08))*(1.0+17.2*t9+175*t92)+8.12e8*pow(t9,-0.712)*exp(-0.506/t9);
          sigmav[]/=AVOGADRO_CONSTANT;
        */


        /*
         * He3 + He3 -> He4 +2p
         * Page 113
         */
        sigmav[SIGMAV_He3_He3]=5.59e10*t23*exp(-12.277*tm13)*
            (1.0-0.135*t9+2.54e-2*t92-1.29e-3*t93);
        sigmav[SIGMAV_He3_He3] /= AVOGADRO_CONSTANT;

        /*
         * He3 + He4 -> Be7 (+ gamma)
         */
        sigmav[SIGMAV_He3_He4]=5.46e6*t23*exp(-12.827*tm13)
            *(1.0-0.307*t9+8.81e-2*t92-1.06e-2*t93+4.46e-4*t94);
        sigmav[SIGMAV_He3_He4] /= AVOGADRO_CONSTANT;

        /* Li6 + p -> Be7 + gamma */
#ifdef LITHIUM_TABLES
        sigmav[SIGMAV_Li6_p]=0.0;
#else
        sigmav[SIGMAV_Li6_p]=1.25e6*t23*exp(-8.415*tm13)*(1.0-0.252*t9+5.19e-2*t92-2.92e-3*t93);
        sigmav[SIGMAV_Li6_p]/=AVOGADRO_CONSTANT;
#endif

        /* Li6 + p -> He4 + He3 */
#ifdef LITHIUM_TABLES
        sigmav[SIGMAV_Li6_p_alpha]=0.0;
#else
        sigmav[SIGMAV_Li6_p_alpha]=3.54e10*t23*exp(-8.415*tm13)*(1.0-0.137*t9+2.41e-2*t92-1.28e-3*t93);
        sigmav[SIGMAV_Li6_p_alpha]/=AVOGADRO_CONSTANT;
#endif


        /* Li7+p->Be8->He4+He4 */
        sigmav[SIGMAV_Li7_p]=1.75e7*t23*
            exp(-8.473*tm13-Pow2(t9/0.8))*(1.0-1.47*t9+4.43*t92)+
            1.60e6*t15*exp(-4.441*i9)+
            4.32e4*pow(t9,0.309)*exp(-2.811*i9);
        sigmav[SIGMAV_Li7_p] /= AVOGADRO_CONSTANT;

        /* Li7+p->He4 + He4 */
        sigmav[SIGMAV_Li7_p_alpha]=7.20e8*t23*exp(-8.473*tm13-t92/Pow2(6.5))*
            (1.0+1.05*t9-0.653*t92+0.185*t93-2.12e-2*t94+9.3e-4*t95)+
            9.85e6*pow(t9,0.576)*exp(-10.415*i9);
        sigmav[SIGMAV_Li7_p_alpha]/=AVOGADRO_CONSTANT;

        /* Be7+p->gamma+B8 (->Be8->2He4) */
        sigmav[SIGMAV_Be7_p]=2.61e5*t23*exp(-10.264*tm13)
            *(1.0-5.11e-2*t9+4.68e-2*t92-6.60e-3*t93+3.12e-4*t94)
            +2.05e3*t15*exp(-7.345*i9);
        sigmav[SIGMAV_Be7_p] /= AVOGADRO_CONSTANT;

        /* Be7 + e -> Li7 */
        sigmav[SIGMAV_Be7_e] = 7.05e-33/sqrt(t9*1e3);


        /************************************************************/
        /************************************************************/
        /************************************************************/
        /**** C N O Cycles ******************************************/
        /************************************************************/
        /************************************************************/
        /************************************************************/


        /************************************************************/

        /* C12 + H1 -> N13 + gamma */

#if (defined(RATES_OF_AREND_JAN))

        /* C12(P,G)N13..C13 FCZ1975 */
        sigmav[SIGMAV_T12]=(2.04e+07*TM23*exp(-13.690*TM13-Pow2(T9/1.5))*
                            (1.+0.030*TP13/*1*/ +1.19*TP23+0.254*T9+2.06*TP43+1.12*TP53)
                            + 1.08e+05*TM32/*2*/ *exp(-4.925*TM9) +2.15e+05*TM32*exp(-18.179*TM9));

#else // RATES_OF_AREND_JAN

        /*
         * Reaction 1 (from NACRE)
         * C12 + H1 -> N13 + gamma
         * Page 116-7
         * Equivalent to Clayton's tau12
         */
        sigmav[SIGMAV_T12]=2.0e7*t23*
            exp(-13.6920*tm13 - t92*4.72589790) *
            (1.00+9.890*t9-59.80*t92+266.00*t93) +
            t15*(1.0e5*exp(-4.9130*i9) + 4.24e5*exp(-21.620*i9));
#endif // RATE CHOICE
        sigmav[SIGMAV_T12] /= AVOGADRO_CONSTANT;


        /************************************************************/
        /*
         * Reaction 2
         * N15 + H1 -> C12 + He4, Page 119, Equivalent to Clayton's tau15
         */
#if (defined(RATES_OF_AREND_JAN))
        sigmav[SIGMAV_T15]=(1.08e+12*TM23*exp(-15.251*TM13-Pow2(T9/0.522))*
                            (1.+0.027*TP13+2.62*TP23+0.501*T9+5.36*TP43+2.6*TP53) + 1.19e+08*TM32
                            *exp(-3.676*TM9)+5.41e+08*TM12*exp(-8.926*TM9) + /*F1=*/0.1
                            *(4.72e+08*TM32*exp(-7.721*TM9) +2.2e+09*TM32*exp(-11.418*TM9)));
#else
#ifdef NUCSYN_HOT_SIGMAV
        /* NACRE */
        if(t9<=2.5)
        {
#endif//NUCSYN_HOT_SIGMAV
            sigmav[SIGMAV_T15]=1.12e12*t23
                *exp((-15.2530*tm13) - t92*12.755102)
                *(1.00+4.950*t9+143.00*t92)
                +t15*1.01e8*(exp(-3.6430*i9)
                             + 11.7821780*exp(-7.4060*i9));
#ifdef NUCSYN_HOT_SIGMAV
        }
        else
        {
            sigmav[SIGMAV_T15]=4.17e7*pow(t9,(0.9170))*exp(-3.2920*i9);
        }
#endif // NUCSYN_HOT_SIGMAV
#endif  // RATE_CHOOSER

        sigmav[SIGMAV_T15] /= AVOGADRO_CONSTANT;

        /************************************************************/

#if (defined(RATES_OF_AREND_JAN))
        sigmav[SIGMAV_T15_BRANCH]=
            (9.78e+08*TM23*exp(-15.251*TM13-Pow2(T9/0.45))*
             (1.+0.027*TP13+0.219*TP23+0.042*T9+6.83*TP43+3.32*TP53) + 1.11e+04*TM32
             *exp(-3.328*TM9)+1.49e+04*TM32*exp(-4.665*TM9)+3.8e+06*TM32
             *exp(-11.048*TM9)) ;
#else
        /*
         * Default rate
         * Reaction 2b N15 + p -> O16 + gamma : NACRE
         */
#ifdef NUCSYN_HOT_SIGMAV
        if(t9<3.5)
        {
#endif // NUCSYN_HOT_SIGMAV
            sigmav[SIGMAV_T15_BRANCH]=1.08e9*pow(t9,(-0.6666666666))*
                exp(-15.254*pow(t9,(-0.3333333333))-Pow2(t9/0.34))*
                (1.0+6.15*t9+16.4*t9*t9)+9.23e3*pow(t9,-1.5)*
                exp(-3.597*i9)+3.27e6*pow(t9,-1.5)*exp(-11.024*i9);
#ifdef NUCSYN_HOT_SIGMAV
        }
        else
        {
            sigmav[SIGMAV_T15_BRANCH]=3.54e4*pow(t9,0.095)*exp(-2.306*i9);
        }
#endif // NUCSYN_HOT_SIGMAV
#endif  // RATE_CHOOSER

        sigmav[SIGMAV_T15_BRANCH] /= AVOGADRO_CONSTANT;

        /************************************************************/

        /*
         * Reaction 3 N13 -> C13 + e+ + neutrino Just give the timescale from Clayton
         * (check Clayton's value!) NB Units are seconds
         */
        //sigmav[SIGMAV_TBETA13]=870.00;
        /* updated value from Tuli, J. 2000, Nuclear Wallet Cards (Brookhaven Natl. Lab.)
           see URL http://www.nndc.bnl.gov/wallet/walletcontent.shtml */
        sigmav[SIGMAV_TBETA13]=597.54;

        /************************************************************/

/*
 * Reaction 4 N13 + p -> O14 + gamma Clayton says "If O14, F16 or Ne18 are
 * formed, they quickly decay to N14, O16 and O18." So ignore this? No.
 * Equivalent to Clayton's tau_p(N13) Page 117
 */
        sigmav[SIGMAV_TPN13]=4.02e7*t23*
            exp((-15.2050*tm13) - t92*3.42935530) *
            (1.00 +3.810*t9+18.60*t92+32.30*t93) +
            3.25e5 * pow(t9,-1.350) * exp(-5.9260*i9);
        sigmav[SIGMAV_TPN13] /= AVOGADRO_CONSTANT;

        /************************************************************/
        /* O14(beta,nu)N14 */
        sigmav[SIGMAV_TO14BETA]=3.22689285275244e-06;

        /************************************************************/

#if (defined(RATES_OF_AREND_JAN))
        sigmav[SIGMAV_T13]=(8.01e+07*TM23*exp(-13.717*TM13-Pow2(T9/2.0))
                            *(1.+0.030*TP13
                              +0.958*TP23+0.204*T9+1.39*TP43+0.753*TP53)
                            + 1.21e+06*TM65  *exp(-5.701*TM9));
#else

        /*
         * Default NACRE rate.
         * Reaction 5 C13 + H1 -> N14 + gamma Page 117 Equivalent to Clayton's tau13
         */
        sigmav[SIGMAV_T13]=9.57e7*t23*(1+3.560*t9) *exp((-13.7200*tm13) - t92)
            + 1.5e6 * t15*exp(-5.9300*i9) +
            6.83e5*pow(t9,(-0.8640))*exp(-12.0570*i9);
        /*
         * NB NACRE provide a "thermalized" rate appropriate to the inside of a star:
         * hence this extra factor
         */
#ifdef NUCSYN_THERMALIZED_CORRECTIONS
        sigmav[SIGMAV_T13] *= 1.00-2.0700*exp(-37.9380*i9);
#endif // NUCSYN_THERMALIZED_CORRECTIONS
#endif // RATE_CHOOSER

        sigmav[SIGMAV_T13] /= AVOGADRO_CONSTANT;

        /************************************************************/

#if (defined(RATES_OF_AREND_JAN))
        sigmav[SIGMAV_T14]=(4.90e7*TM23*exp(-15.228*TM13-Pow2(T9/3.294))
                            *(1.+0.027*TP13-0.778*TP23-0.149*T9+0.261*TP43+0.127*TP53)
                            +2.37e3*TM32*exp(-3.011*TM9)+2.19e4*exp(-12.530*TM9));
#else
/*
 * Reaction 6 N14 + H1 -> O15 + gamma Page 117-8 Equivalent to Clayton's tau14
 */
        sigmav[SIGMAV_T14]=4.83e7*t23*
            exp(-15.2310*tm13-t92*1.56250) *
            (1.00-2.00*t9+3.410*t92-2.430*t93) +
            (2.36e3*t15*exp(-3.0100*i9)) +
            6.72e3*pow(t9,0.3800)*exp(-9.5300*i9);
#endif // RATE_CHOOSER

        sigmav[SIGMAV_T14] /= AVOGADRO_CONSTANT;

        /************************************************************/

/*
 * Reaction 7 O17 + H1 -> N14 + He4 Page 119-20 Equivalent to Clayton's tau17
 */

#ifdef RATES_OF_AREND_JAN
        sigmav[SIGMAV_T17]=(1.53e+07*TM23*exp(-16.712*TM13-Pow2(T9/0.565))*
                            (1.+0.025*TP13+5.39*TP23+0.940*T9+13.5*TP43+5.98*TP53)+2.92e+06*T9*
                            exp(-4.247*TM9)+/*F2=*/0.1 *(4.81e+10*T9*exp(-16.712*TM13-Pow2(T9/0.04))
                                                         +5.05e-05*TM32*exp(-0.723*TM9))+/*F3=*/
                            0.1 *1.31e+01*TM32*exp(-1.961*TM9));
#else
#ifdef NUCSYN_HOT_SIGMAV
        if(t9<6)
        {
#endif// NUCSYN_HOT_SIGMAV
/* Again, most likely for HBB in TPAGB stars */
            sigmav[SIGMAV_T17]=9.20e8*t23 *
                exp((-16.7150*tm13) - t92*277.77770)*
                (1.00 - 80.310*t9 + 2211.00*t92)
                + t15* (9.13e-4 * exp(-0.76670*i9)
                        + 9.680 * exp(-2.0830*i9)
                        + 8.13e6 * exp(-5.6850*i9) )
                + 1.85e6 * pow(t9,1.5910) * exp(-4.8480*i9);
#ifdef NUCSYN_HOT_SIGMAV
        }
        else
        {
            sigmav[SIGMAV_T17]=8.73e6*pow(t9,0.9500)*exp(-7.5080*i9);
        }
#endif// NUCSYN_HOT_SIGMAV
#ifdef NUCSYN_THERMALIZED_CORRECTIONS
        /* Thermalized correction term */
        sigmav[SIGMAV_T17] *= (1.00+1.0330*exp(-10.0340*i9-0.1650*t9));
#endif
#endif // RATE_CHOOSER

        sigmav[SIGMAV_T17] /= AVOGADRO_CONSTANT;

        /************************************************************/

/*
 * Reaction 8 O15 -> N15 + e+ + neutrino Clayton says: (taubeta15)
 */
        //    sigmav[SIGMAV_TBETA15] = 178.00;

        /* updated value from Tuli, J. 2000, Nuclear Wallet Cards (Brookhaven Natl. Lab.)
           see URL http://www.nndc.bnl.gov/wallet/walletcontent.shtml */
        sigmav[SIGMAV_TBETA15]=122.2;

        /************************************************************/

/*
 * Reaction 9 O15 + p -> F16 + gamma Seems not to be in the NACRE compilation!
 * Look this up later! This is another one that almost instantaneously decays
 * to O16. According to Clayton we can neglect it for t<1e8, so we do
 */
        // actually it's not in ANY compilation! :)
        sigmav[SIGMAV_T15P] = 0.00;

        /************************************************************/
/*
 * Reaction 10 O16 + H1 -> F17 + gamma Page 119 Equivalent to Clayton's tau16
 */

#if (defined(RATES_OF_AREND_JAN))
        sigmav[SIGMAV_T16]=(1.5e+08/(TP23*(1.0+2.13*(1.0-exp(-0.728*TP23)))) *exp(-16.692*TM13));
#else
        sigmav[SIGMAV_T16]=7.37e7*exp(-16.696*tm13)*pow(t9,(-0.820));
#ifdef NUCSYN_THERMALIZED_CORRECTIONS
        /* Thermalized correction */
        sigmav[SIGMAV_T16] *= (1.00+202.00*exp(-70.3480*i9-0.1610*t9));
#endif
#endif // RATE_CHOOSER
        sigmav[SIGMAV_T16] /= AVOGADRO_CONSTANT;

        /************************************************************/

/*
 * Reaction 11 F17 -> O17 + e+ + neutrino Clayton says lifetime in seconds is
 * (taubeta17)
 */
        //sigmav[SIGMAV_TBETA17]=95.00;
        /* updated value from Tuli, J. 2000, Nuclear Wallet Cards (Brookhaven Natl. Lab.)
           see URL http://www.nndc.bnl.gov/wallet/walletcontent.shtml */
        sigmav[SIGMAV_TBETA17]=64.78;


        /************************************************************/
/*
 * Reaction 12 F17 + p -> Ne18 + gamma Again not in the NACRE rates and
 * supposedly very unlikely to happen for T < 1d9
 */

        /************************************************************/

        // CF89
        sigmav[SIGMAV_TF17] =
            4.81e07*pow(t9,-0.666666666666666666)*exp(-18.0*pow(t9,-0.3333333333))
            *(1.0 + 2.31e-02*pow(t9,0.333333333333333333333333) - 2.32e-01*pow(t9,2.0/3.0) - 4.40e-02*t9
              + 4.26e-02*pow(t9,4.0/3.0) + 1.54e-02*pow(t9,1.66666666666666666666))
            + 2.45e03*pow(t9,-3.0/2.0)*exp(-7.40*i9)
            + 6.300e02*pow(t9,-3.0/2.0)*exp(-6.94*i9)
            + 1.399e01*pow(t9,-3.0/2.0)*exp(-7.75*i9);
        sigmav[SIGMAV_TF17]/=AVOGADRO_CONSTANT;

        /************************************************************/
        /*
         * Not really reactions 13 and 14 These are the branching ratios, as suggested
         * by Clayton
         */

        /* Update: use the NACRE value */
        sigmav[SIGMAV_BRANCH_GAMMA]= sigmav[SIGMAV_T15_BRANCH]/Max(1e-200,sigmav[SIGMAV_T15]);
        sigmav[SIGMAV_BRANCH_ALPHA] = 1.00-sigmav[SIGMAV_BRANCH_GAMMA];

        /************************************************************/

        /************************************************************/
        /************************************************************/
        /************************************************************/
        /**** NeNa and MgAl Cycles **********************************/
        /************************************************************/
        /************************************************************/
        /************************************************************/

        /******* NeNa cycle ********/
        // F19(p,gamma)Ne20 (into the cycle)


#ifdef RATES_OF_AREND_JAN
        sigmav[SIGMAV_T19]=
            (6.04e+07/T923*exp(-18.113/T913-Pow2(T9/0.416))
             *(1.+0.023*T913+2.06*T923+0.332*T9+3.16*T943+1.30*T953)
             +6.32e+02/T932*exp(-3.752/T9) + 7.56e4/T927*exp(-5.722/T9));
        sigmav[SIGMAV_T19]/=1.0+4.0*exp(-2.090/T9)+7.0*exp(-16.440/T9);

#else
        /* NACRE rate */
#ifdef NUCSYN_HOT_SIGMAV
        if(t9<=1.5)
        {
#endif// NUCSYN_HOT_SIGMAV
            sigmav[SIGMAV_T19]=6.37e7*t23 *exp(-18.116*tm13)*(1.0+0.775*t9+36.1*t92)+8.27e2*t15*exp(-3.752*i9)+1.28e6*pow(t9,-3.667)*exp(-9.120*i9);
#ifdef NUCSYN_HOT_SIGMAV
        }
        else
        {
            sigmav[SIGMAV_T19]=3.66e3*t9/0.947*exp(-2.245*i9);
        }
#endif// NUCSYN_HOT_SIGMAV
#ifdef NUCSYN_THERMALIZED_CORRECTIONS
        sigmav[SIGMAV_T19] *= (1-0.990*exp(-1.207*i9-0.0886*t9));
#endif
#endif // RATE_CHOOSER
        sigmav[SIGMAV_T19] /= AVOGADRO_CONSTANT;

        /************************************************************/
        /************************************************************/
        /********************* Ne20(p,g) Na21 ***********************/
        /************************************************************/
        /************************************************************/

#if (defined(RATES_OF_AMANDA))
        // NE20(P,G)NA21 from CF88
        sigmav[SIGMAV_T20] = 9.55e+06*exp(-19.447/t913)/
            (t92*Pow2(1.0+0.0127/t923))
            +2.05e+08/t923*exp(-19.447/t913)*(1.0+2.67
                                              *exp(-pow(t9/0.210,0.5)))+1.80e+01/t932*exp(-4.242*i9)+1.02e+01
            /t932*exp(-4.607*i9)+3.60e+04/t914*exp(-11.249*i9);
        //sigmav[SIGMAV_T20] *= 4.63e+09*t932*exp(-28.216*i9);

#elif (defined(RATES_OF_MARIA))

        /*
         * Maria's REACTION Ne20pgNa21 -> SIGMAV_T20
         */
        sigmav[SIGMAV_T20]=
            Reaclib(0.112078e+03,-0.705964e-01,-0.300779e+01,-0.149812e+03,0.893819e+02,-0.929669e+02,0.270204e+02)+
            Reaclib(0.247838e+01,-0.691248,-0.409804e+01,0.119259e+02,-0.954890e+01,-0.511767,0.593970e+01)+
            Reaclib(0.863238e+01,-0.836539e+01,0.451031,-0.300658,-0.312270,0.314422e-01,0.148890e+01);

#elif (defined(RATES_OF_AREND_JAN))
        sigmav[SIGMAV_T20]=(9.55e+06*exp(-19.447/T913)/(Pow2(T9)*Pow2(1.+0.0127/T923))
                            +2.05e+08/T923*exp(-19.447/T913)
                            *(1.+2.67*exp(-sqrt(T9/0.210)))
                            +1.80e+01/T932*exp(-4.242/T9)+1.02E1/T932*exp(-4.607/T9)
                            +3.60e+04/T914*exp(-11.249/T9));
#else
        // Ne20(p,gamma)Na21 from NACRE
        sigmav[SIGMAV_T20]=2.35e7*pow(t9,-1.84)*exp(-19.451*tm13)*(1+10.8*t9)+18.0*t15*exp(-4.247*i9)+9.83*t15*exp(-4.619*i9)+6.76e4*pow(t9,-0.641)*exp(-11.922*i9);
#ifdef NUCSYN_THERMALIZED_CORRECTIONS
        sigmav[SIGMAV_T20] *= (1.0-7.929*exp(-20.108*i9-0.327));
#endif
#endif // RATE_CHOOSER

        sigmav[SIGMAV_T20] /= AVOGADRO_CONSTANT;

        /************************************************************/
        /************************************************************/
        /********************* Ne21(p,g) Na22 ***********************/
        /************************************************************/
        /************************************************************/

#if (defined(RATES_OF_AMANDA))
        // Rate for ne21(p,g)na22 from EL95
        a1=3.4e+08/t923*exp(-19.41/t913);
        b1=1.0+0.56*exp(-Pow2(16.7*t9-1.0));
        a1=a1*b1;
        a2=6.12/t932*exp(-1.403*i9);
        a3=1.35e+04/t932*exp(-3.008*i9);
        a4=3.12e+06*pow(t9,-0.72)*exp(-8.268*pow(t9,-0.67));
        // the unknown factor is the 0.1 here:
        a5=0.1*1.1e-3/t932*exp(-1.114*i9);
        sigmav[SIGMAV_T21]=(a1+a2+a3+a4+a5);

#elif (defined(RATES_OF_MARIA))
        /* Maria's REACTION Ne21pgNa22 -> SIGMAV_T21 */
        sigmav[SIGMAV_T21]=
            Reaclib(0.296371e+02,-0.179861e-01,-0.163786e+02,-0.153383e+02,0.300382e+01,-0.900470,0.345694e+01)+Reaclib(-0.437079e+02,-0.192696,0.765401e-02,-0.280351e-01,0.305416e-02,-0.269857e-03,-0.149092e+01)+Reaclib(-0.114485e+02,-0.109787e+01,0.497670e-02,-0.154510e-01,0.147205e-02,-0.119000e-03,-0.149456e+01)+Reaclib(0.181545e+01,-0.140335e+01,-0.454476e-02,0.134943e-01,-0.123631e-02,0.971986e-04,-0.150486e+01)+Reaclib(0.768386e+01,-0.300261e+01,-0.713715,0.290262e+01,-0.260861,0.180284e-01,-0.237968e+01)+Reaclib(0.305967,-0.540027e+01,-0.125000e+02,0.257126e+02,-0.154104e+01,0.780952e-01,-0.118132e+02);

#elif (defined(RATES_OF_AREND_JAN))
        sigmav[SIGMAV_T21]=(4.37e+08/T923*exp(-19.462/T913)
                            + 5.85/T932*exp(-1.399/T9)
                            +1.29e+04/T932*exp(-3.009/T9)
                            +3.15e+05/T935*exp(-5.763/T9)
                            +                       (0.1)*
                            (2.95e+08/T923*exp(-19.462/T913-Pow2(T9/0.058))
                             *(1.+0.021*T913+13.29*T923+1.99*T9+124.1*T943+47.29*T953)
                             +7.80E-01/T932*exp(-1.085/T9))) ;
#else
        // Ne21(p,gamma)Na22 from NACRE
#ifdef NUCSYN_HOT_SIGMAV
        if(t9<=2)
        {
#endif// NUCSYN_HOT_SIGMAV
            sigmav[SIGMAV_T21]=4.68e8*t23
                *exp(-19.465*tm13
                     -Pow2((t9/0.2)))+8.18e-4*t15*exp(-1.085*i9)+6.11*t15*exp(-1.399*i9)+1.34e4*t15*exp(-3.009*i9)+1.26e5*pow(t9,-0.128)*exp(-4.962*i9);
#ifdef NUCSYN_HOT_SIGMAV
        }
        else
        {
            sigmav[SIGMAV_T21]=3.04e4*pow(t9,0.420)*exp(-2.650*i9);
        }
#endif// NUCSYN_HOT_SIGMAV
#ifdef NUCSYN_THERMALIZED_CORRECTIONS
        sigmav[SIGMAV_T21] *= (1.0-0.708*exp(-3.851*i9-0.156*t9));
#endif
#endif // RATE_CHOOSER

        sigmav[SIGMAV_T21] /= AVOGADRO_CONSTANT;

        /************************************************************/
        /************************************************************/
        /********************* Ne22(p,g) Na23 ***********************/
        /************************************************************/
        /************************************************************/

#if (defined(RATES_OF_MARIA))
        /* Maria's REACTION Ne22pgNa23 -> SIGMAV_T22 */
        sigmav[SIGMAV_T22]=Reaclib(0.208200e+02,-0.108809e-03,-0.194097e+02,-0.372081e-01,0.739377e-02,-0.128009e-02,-0.648178)+Reaclib(-0.212579e+02,-0.410772,-0.580367e-02,0.189368e-01,-0.188510e-02,0.157242e-03,-0.150650e+01)+Reaclib(-0.645203e+01,-0.175119e+01,-0.100753e-02,0.318931e-02,-0.309677e-03,0.253352e-04,-0.150111e+01)+Reaclib(-0.134847e+02,-0.207387e+01,-0.109783e+01,0.137621e+02,-0.175262e+01,0.147358,-0.434513e+01)+Reaclib(0.101447e+02,-0.413567e+01,0.487085e+01,-0.130202e+02,0.973889,-0.663540e-01,0.368155e+01)+Reaclib(-0.716413e+01,-0.479423e+01,-0.459213e+01,0.256030e+02,-0.219524e+01,0.130067,-0.771031e+01);

#elif (defined(RATES_OF_AMANDA))
        // Ne22(pg)Na23 NACRE rate
#ifdef NUCSYN_HOT_SIGMAV
        if(t9<=2)
        {
#endif// NUCSYN_HOT_SIGMAV
            sigmav[SIGMAV_T22]=1.11e-9*t15*exp(-0.422*i9)+6.83e-5*t15*exp(-0.810*i9)+9.76e-3*t15*exp(-1.187*i9)+1.06e-1*t15*exp(-1.775*i9)+8.51e4*pow(t9,0.725)*exp(-4.315*i9);
#ifdef NUCSYN_HOT_SIGMAV
        }
        else
        {
            sigmav[SIGMAV_T22]=6.30e4*pow(t9,0.816)*exp(-3.910*i9);
        }
#endif// NUCSYN_HOT_SIGMAV
#ifdef NUCSYN_THERMALIZED_CORRECTIONS
        sigmav[SIGMAV_T22] *= (1-1.410*exp(-14.651*i9-0.020*t9));
#endif

#ifdef RATES_OF_AMANDA_NE22PG_FIX
        /* for M=5,6 we should use the old CF88 rate */
        if(ne22pg_cf88==TRUE)
        {
            const double t912=sqrt(t9);
            sigmav[SIGMAV_T22] = (1.15e+09/t923*exp(-19.475/t913)+9.77e-12/t932
                                  *exp(-0.348*i9)+8.96e+03/t932*exp(-4.840*i9)+6.52e+04/t932
                                  *exp(-5.319*i9)+7.97e+05/t912*exp(-7.418*i9)+
                                  // NB the 0.1 can be 0-1 (resonance multiplier) usually set to 0
                                  0.1*1.63e-01/t932*exp(-1.775*i9));

        }
#endif //RATES_OF_AMANDA_NE22PG_FIX

#elif (defined(RATES_OF_AREND_JAN))
        sigmav[SIGMAV_T22]=(1.15e+09/T923*exp(-19.475/T913)
                            + 9.77E-12/T932*exp(-0.348/T9)
                            +8.96e+03/T932*exp(-4.840/T9)+6.52e+04/T932*exp(-5.319/T9)
                            +7.97e+05/T912*exp(-7.418/T9)
                            +                       (0.1)*
                            1.63e-01/T932*exp(-1.775/T9));
#else
        // Ne22(p,gamma)Na23 from NACRE
#ifdef NUCSYN_HOT_SIGMAV
        if(t9<=2)
        {
#endif// NUCSYN_HOT_SIGMAV
            sigmav[SIGMAV_T22]=1.11e-9*t15*exp(-0.422*i9)+6.83e-5*t15*exp(-0.810*i9)+9.76e-3*pow(t9,(-1.5))*exp(-1.187*i9)+1.06e-1*t15*exp(-1.775*i9)+8.51e4*pow(t9,0.725)*exp(-4.315*i9);
#ifdef NUCSYN_HOT_SIGMAV
        }
        else
        {
            sigmav[SIGMAV_T22]=6.30e4*pow(t9,0.816)*exp(-3.910*i9);
        }
#endif// NUCSYN_HOT_SIGMAV
#ifdef NUCSYN_THERMALIZED_CORRECTIONS
        sigmav[SIGMAV_T22] *= (1.0-1.410*exp(-14.651*i9-0.020*t9));
#endif

#endif // RATE CHOOSER
        sigmav[SIGMAV_T22] /= AVOGADRO_CONSTANT;

        /************************************************************/

#ifdef HELIUM_BURNING_REACTIONS
        // Ne22(alpha,gamma)Mg26
#ifdef NUCSYN_HOT_SIGMAV
        if(t9<=1.25)
        {
#endif// NUCSYN_HOT_SIGMAV
            n=3.55e-9*(t9**-1.5)*exp(-3.927*i9)+
                7.07e-1*(t9**-1.064)*exp(-7.759*i9)+
                1.27e-3*(t9**-2.556)*exp(-6.555*i9);
#ifdef NUCSYN_HOT_SIGMAV
        }
        else
        {
            n=1.76*(t9**3.322)*exp(-12.412*i9);
        }
#endif// NUCSYN_HOT_SIGMAV
        n*=(1.0-0.005*exp(-5.109*i9+0.373*t9));

        //   # Ne22(alpha,n)Mg25
#ifdef NUCSYN_HOT_SIGMAV
        if(t9<=2.0)
        {
#endif// NUCSYN_HOT_SIGMAV
            n=7.40*exp(-7.79*i9)+
                1.30e-4*(t9**0.83)*exp(-5.52*i9)+
                9.41e3*(t9**2.78)*exp(-11.7*i9)+
                8.59e6*(t9**0.892)*exp(-24.4*i9);
#ifdef NUCSYN_HOT_SIGMAV
        }
        else
        {
            n=1.51e5*(t9**2.879)*exp(-16.717*i9);
        }
#endif// NUCSYN_HOT_SIGMAV
        n*=(1.0+2.674*exp(-15.025*i9-0.321*t9));
#endif // HELIUM_BURNING_REACTIONS

        /************************************************************/
        /************************************************************/
        /********************* Na23(p,a) Ne20 ***********************/
        /************************************************************/
        /************************************************************/
#if (defined(RATES_OF_AMANDA))
        a1=1.26e+10/t923*exp((-20.758/t913)-Pow2(t9/0.13));
        b1=1.0+0.02*t913-13.8*t923-1.93*t9+234.0*t943+83.6*t953;
        a1=a1*b1;
        a2=4.38/t932*exp(-1.979*i9);
        a3=6.50e+06*pow(t9,-1.336)*exp(-6.490*i9);
        a4=1.19e+08*pow(t9,-1.055)*exp(-11.411*i9);
        // the unknown factor is the 0.1 here:
        a5=0.1*9.91e-14/t932*exp(-0.418*i9);

        sigmav[SIGMAV_T23]=(a1+a2+a3+a4+a5);
#elif (defined(RATES_OF_MARIA))
        /* Maria's REACTION Na23paNe20 -> SIGMAV_T23 */
        sigmav[SIGMAV_T23]=Reaclib(0.156632e+03,-0.105827,0.326018e+01,-0.206279e+03,0.832450e+02,-0.499856e+02,0.403909e+02)+Reaclib(0.525919e+03,-0.301515,0.601990e+02,-0.777241e+03,0.364372e+03,-0.315476e+03,0.147864e+03)+Reaclib(-0.119895e+03,-0.633817e-01,-0.581045e-01,0.192651,-0.190087e-01,0.154373e-02,-0.156567e+01)+Reaclib(-0.360244e+02,-0.430617,0.911788e-02,-0.293146e-01,0.281655e-02,-0.224405e-03,-0.148985e+01)+Reaclib(-0.776022e+01,-0.160524e+01,0.481135,-0.969025,0.395533e-01,-0.114858e-02,-0.101910e+01)+Reaclib(0.323173e+01,-0.199800e+01,0.328151e+01,-0.426201e+01,-0.945722e-01,0.254831e-01,0.141237e+01)+Reaclib(0.142176e+02,-0.323598e+01,0.589178e+01,-0.109880e+02,0.481526,-0.178061e-01,0.398717e+01)+Reaclib(0.132213e+02,-0.656750e+01,-0.132927e+01,0.406530e+01,-0.303075,0.182623e-01,-0.293149e+01)+Reaclib(0.154555e+01,-0.104906e+02,0.927879e+01,0.799835e+01,-0.143715e+01,0.114795,0.121158e+01);

#elif (defined(RATES_OF_AREND_JAN))
        sigmav[SIGMAV_T23]=(8.56e+09*TM23*exp(-20.766*TM13-Pow2(T9/0.131))*
                            (1.+.02*T913+8.21*T923+1.15*T9+44.36*T943+15.84*T953)+4.02*TM32
                            *exp(-1.99*TM9)+1.18e+04*TM54*exp(-3.148*TM9)+8.59e+05*T943*
                            exp(-4.375*TM9)+/*ALPHA=*/0.1*3.06E-12*TM32*exp(-0.447*TM9));
#else
        /*
         * Na23(p,alpha)Ne20 : NACRE
         */
#ifdef NUCSYN_HOT_SIGMAV
        if(t9<=5)
        {
#endif
            sigmav[SIGMAV_T23]=8.39e9* t23 *
                exp(-20.770* tm13 - Pow2(t9/0.1))*(1+45.2*t9)+
                3.09e-13* t15 *exp(-0.420*i9)
                +8.12e-3* t15*exp(-1.601*i9)+
                4.37*t15*exp(-1.934*i9)+
                7.5e3*pow(t9,-1.48)*
                exp(-3.150*i9)+
                1.05e6*pow(t9,1.456)*exp(-4.482*i9);
#ifdef NUCSYN_HOT_SIGMAV
        }
        else
        {
            sigmav[SIGMAV_T23]=3.96e6*pow(t9,1.291)*exp(-9.277*i9);
        }
#endif

#endif // RATE_CHOOSER

        sigmav[SIGMAV_T23] /= AVOGADRO_CONSTANT;

        /************************************************************/
        /************************************************************/
        /********************* Na23(p,g) Mg24 ***********************/
        /************************************************************/
        /************************************************************/

#if  (defined(RATES_OF_AMANDA))
        /* rate of El Eid and Champagne 1995, as used in Amanda's
         * first set of models */
        sigmav[SIGMAV_T23P] = 2.47e9 * t23 * exp(-20.758*tm13) +
            91.9*t15*exp(-2.789*i9)+1.72e4*t15*exp(-3.433*i9) +
            3.44e4*pow(t9,0.323)*exp(-5.219*i9)+
            2.34e-4*t15*exp(-1.590*i9);
#elif (defined(RATES_OF_MARIA))
        /* Maria's REACTION Na23pgMg24 -> SIGMAV_T23P */
        sigmav[SIGMAV_T23P]=Reaclib(0.321402e+02,-0.226477e-01,-0.169774e+02,-0.188483e+02,0.333428e+01,-0.862626,0.444836e+01)+Reaclib(-0.122215e+03,-0.638582e-01,0.455271e-02,-0.174054e-01,0.192201e-02,-0.168311e-03,-0.149447e+01)+Reaclib(-0.460634e+02,-0.430413,-0.171111e-01,0.560886e-01,-0.546019e-02,0.438306e-03,-0.151924e+01)+Reaclib(-0.694817e+01,-0.160160e+01,-0.104266,0.129067e+01,-0.164065,0.133457e-01,-0.174908e+01)+Reaclib(0.444413e+01,-0.278984e+01,-0.636430e-03,0.196576e-02,-0.187741e-03,0.149812e-04,-0.150069e+01)+Reaclib(0.978039e+01,-0.343464e+01,0.613711e-01,-0.958165e-01,0.487948e-02,-0.299863e-03,-0.144865e+01)+Reaclib(-0.889387e+01,-0.570055e+01,-0.367651e+01,0.254056e+02,-0.205687e+01,0.120096,-0.795560e+01);

#elif (defined(RATES_OF_AREND_JAN))
        sigmav[SIGMAV_T23P]=(2.93e+08*TM23*exp(-20.776*TM13-Pow2(T9/0.297))
                             *(1.+.02*T913+1.61*T923+.226*T9+4.94*T943+1.76*T953)
                             +9.34e+01*TM32*exp(-2.789*TM9)+1.89e+04*TM32*exp(-3.434*TM9)
                             +5.1e+04*T915*exp(-5.510*TM9));
        sigmav[SIGMAV_T23P]/=1.0+1.5*exp(-5.105*TM9);
#else

        // Na23(p,gamma)Mg24 : NACRE
        /* Leaking from NeNa cycle to MgAl chain */
#ifdef NUCSYN_HOT_SIGMAV
        if(t9<=5)
        {
#endif// NUCSYN_HOT_SIGMAV
            sigmav[SIGMAV_T23P]=9.55e7* t23 *
                exp(-20.770* tm13-Pow2(t9/0.3))*
                (1.0-10.80*t9+61.08*t92)+
                8.20e-2* t15 *exp(-1.601*i9)+
                85.2* t15 *exp(-2.808*i9)+
                1.70e4* t15 *exp(-3.458*i9)+
                5.94e4*exp(-5.734*i9);
#ifdef NUCSYN_HOT_SIGMAV
        }
        else
        {
            sigmav[SIGMAV_T23P]=5.60e3*pow(t9,1.112)*exp(-2.337*i9);
        }
#endif// NUCSYN_HOT_SIGMAV
#ifdef NUCSYN_THERMALIZED_CORRECTIONS
        sigmav[SIGMAV_T23P] *= (1-0.560*exp(-5.119*i9-0.050*t9));
#endif
#endif //RATES CHOOSER

        sigmav[SIGMAV_T23P] /= AVOGADRO_CONSTANT;

        /************************************************************/
        /************************************************************/
        /********************* Mg24(p,g) Al25 ***********************/
        /************************************************************/
        /************************************************************/
#if (defined(RATES_OF_AMANDA))
        // The following function gives rubbish, so use the NACRE version
        // which is correct to at least 30%

        // CF88 rate
        /*
          sigmav[SIGMAV_T24] = (5.60e+08/t923*exp(-22.019/t913)*
          (1.0+0.019*t913-0.173
          *t923-0.023*t9)+1.48e+03/t932*
          exp(-2.484*i9)+4.00e+03
          *exp(-4.180*i9))/(1.0+5.0*exp(-15.882*i9));
          */


        /*
         * Amanda now uses NACRE
         */
        // Mg24(p,gamma)Al25 (which then beta decays quickly to Mg25) from NACRE

#ifdef NUCSYN_HOT_SIGMAV
        if(t9<=7)
        {
#endif// NUCSYN_HOT_SIGMAV
            sigmav[SIGMAV_T24]=5.97e8* t23 *exp(-22.023* tm13- Pow2((t9/0.1)))+1.59e3* t15 *exp(-2.483*i9)+3.33e3* pow(t9,0.122)*exp(-3.981*i9);
#ifdef NUCSYN_HOT_SIGMAV
        }
        else
        {
            sigmav[SIGMAV_T24]=3.81e1*pow(t9,2.113)*exp(0.860*i9);
        }
#endif// NUCSYN_HOT_SIGMAV
#elif (defined(RATES_OF_MARIA))
        /* Maria's REACTION Mg24pgAl25 -> SIGMAV_T24 */
        sigmav[SIGMAV_T24]=Reaclib(0.199565e+02,-0.697781e-04,-0.219525e+02,-0.162580e-01,0.366352e-02,-0.321486e-03,-0.655153)+Reaclib(0.767211e+01,-0.248344e+01,0.204845e-01,-0.574066e-01,0.527151e-02,-0.420928e-03,-0.147899e+01)+Reaclib(-0.178763e+02,-0.441813e+01,-0.215198e+02,0.509589e+02,-0.289045e+01,0.132303,-0.220245e+02);

#elif (defined(RATES_OF_AREND_JAN))
        sigmav[SIGMAV_T24]=(5.60e+08/T923*exp(-22.019/T913)
                            *(1.+0.019*T913-0.173*T923-0.023*T9)
                            +1.48e+03/T932*exp(-2.484/T9) + 4.00e3*exp(-4.180/T9));
        sigmav[SIGMAV_T24]/=1.+5.0*exp(-15.882/T9);
#else
        // Mg24(p,gamma)Al25 (which then beta decays quickly to Mg25) from NACRE
#ifdef NUCSYN_HOT_SIGMAV
        if(t9<=7)
        {
#endif// NUCSYN_HOT_SIGMAV
            sigmav[SIGMAV_T24]=5.97e8* t23 *exp(-22.023* tm13- Pow2((t9/0.1)))+1.59e3* t15 *exp(-2.483*i9)+3.33e3* pow(t9,0.122)*exp(-3.981*i9);
#ifdef NUCSYN_HOT_SIGMAV
        }
        else
        {
            sigmav[SIGMAV_T24]=3.81e1*pow(t9,2.113)*exp(0.860*i9);
        }
#endif// NUCSYN_HOT_SIGMAV
#endif // RATE CHOOSER
        sigmav[SIGMAV_T24] /= (AVOGADRO_CONSTANT);

        /************************************************************/
        /************************************************************/
        /********************** Mg25(p,g)Al26g **********************/
        /************************************************************/
        /************************************************************/

#if (defined(RATES_OF_AMANDA))
        /*      mg25(p,g)al26g from I+96 (I+90, e+86)
                c The rate is valid only for t9>0.01, because at lower temperature
                c other contribution must considered.
                c For the resonance only with an upper limit of the strenght (38 and 135)
                c a factor 0.1 has been considered.
                c For 0.03 < t9 < 0.07 the uncertainty is 50%,
                c for 0.07 < t9 < 0.15 the rate could be lower by a factor 2 or more,
                c for 0.2 < t9 < 2.0 the uncertainty is 20%.
        */
        amu=25.0/26.0;
        amu32=1.54e5*pow(amu,-1.5)/t932;
        a=-11.604*amu/t9;
        a1=exp(a*38.0e-03)   ;
        a1=amu32*2.4e-20*0.1*a1;
        a2=exp(a*60.0e-03);
        a2=amu32*2.82e-13*a2;
        a3=exp(a*96.0e-03);
        a3=amu32*1.16e-10*a3;
        a4=exp(a*112.0e-03);
        a4=amu32*2.10e-11*a4 ;
        a5=exp(a*135.0e-03);
        a5=amu32*1.5e-10*0.1*a5 ;
        // from I+90
        a6=exp(a*198.0e-03);
        a6=amu32*7.4e-07*a6;
        a7=exp(a*254.0e-03);
        a7=amu32*5.1e-06*a7;
        a8=exp(a*304.0e-03);
        a8=amu32*5.1e-05*a8;
        a9=exp(a*317.0e-03);
        a9=amu32*3.0e-02*a9;
        // from e+86 [Endt 1986 Nucl.Phys. A 487,221]
        //obtaine//d as s(p,g)/12
        a10=exp(a*390.0e-03);
        a10=amu32*7.1e-02*a10;
        a11=exp(a*435.0e-03);
        a11=amu32*1.3e-01*a11;
        a12=exp(a*496.0e-03);
        a12=amu32*8.3e-02*a12;
        a13=exp(a*502.0e-03);
        a13=amu32*6.9e-02*a13  ;
        a14=exp(a*515.0e-03);
        a14=amu32*4.6e-02*a14  ;
        a15=exp(a*529.0e-03);
        a15=amu32*3.2e-05*0.1*a15  ;
        a16=exp(a*531.0e-03);
        a16=amu32*2.0e-02*a16  ;
        a17=exp(a*567.0e-03);
        a17=amu32*3.0e-01*a17  ;
        a18=exp(a*591.0e-03);
        a18=amu32*2.2e-01*a18;
        a19=exp(a*609.0e-03);
        a19=amu32*1.4e-03*a19  ;
        a20=exp(a*655.0e-03);
        a20=amu32*3.1e-02*a20  ;
        a21=exp(a*684.0e-03);
        a21=amu32*4.5e-01*a21  ;
        a22=exp(a*722.0e-03);
        a22=amu32*1.8e-01*a22  ;

        sigmav[SIGMAV_T25]=a1+a2+a3+a4+a5+a6+a7+a8+a9+a10+
            a11+a12+a13+a14+a15+a16+a17+a18+
            a19+a20+a21+a22;

#elif (defined(RATES_OF_MARIA))
        /* Maria's REACTION Mg25pgAl26 -> SIGMAV_T25 */
        sigmav[SIGMAV_T25]=Reaclib(-0.169033e+02,-0.666234,-0.130374e-02,0.455645e-02,-0.478223e-03,0.412771e-04,-0.150151e+01)+Reaclib(-0.940794e+01,-0.107258e+01,0.682242,-0.212774e+01,0.167209,-0.116951e-01,-0.692396)+Reaclib(0.336968e+02,-0.258601e+01,0.367150e+02,-0.727831e+02,0.395660e+01,-0.196978,0.322550e+02)+Reaclib(-0.197844e+02,-0.348095e+01,-0.847000e+01,0.416773e+02,-0.358991e+01,0.238561,-0.132509e+02)+Reaclib(-0.171000e+02,-0.666234,-0.130471e-02,0.456896e-02,-0.480198e-03,0.414636e-04,-0.150151e+01)+Reaclib(-0.979089e+01,-0.107201e+01,0.589102,-0.183108e+01,0.143440,-0.100112e-01,-0.803649)+Reaclib(0.381956e+02,-0.261289e+01,0.397609e+02,-0.811782e+02,0.462963e+01,-0.244607,0.354598e+02)+Reaclib(0.840186e+01,-0.352648e+01,0.304344e-01,-0.852458e-01,0.782572e-02,-0.624801e-03,-0.146880e+01)+Reaclib(0.522874e+01,-0.440515e+01,0.408244e+01,0.115893e+01,0.650368e-01,-0.321815e-01,0.677192)+Reaclib(-0.185678e+02,-0.666233,-0.131744e-02,0.460061e-02,-0.482097e-03,0.415574e-04,-0.150153e+01)+Reaclib(-0.100318e+02,-0.107603e+01,0.125711e+01,-0.399984e+01,0.319997,-0.226480e-01,0.125236e-02)+Reaclib(0.212179e+02,-0.251613e+01,0.291324e+02,-0.528962e+02,0.242770e+01,-0.919813e-01,0.244690e+02)+Reaclib(0.650467e+01,-0.352648e+01,0.303677e-01,-0.850962e-01,0.781329e-02,-0.623843e-03,-0.146886e+01)+Reaclib(-0.225344e+02,-0.425986e+01,-0.115838e+02,0.471413e+02,-0.391512e+01,0.256920,-0.159977e+02);

#elif (defined(RATES_OF_AREND_JAN))
        sigmav[SIGMAV_T25]=(3.57e+09/T923*exp(-22.031/T913-Pow2(T9/0.06))
                            *(1.+0.019*T913+7.669*T923+1.015*T9+167.4*T943+56.35*T953)
                            +3.07e-13/T932*exp(-0.435/T9)+1.94e-07/T932*exp(-0.673/T9)
                            +3.15e-05*pow(T9,-3.40)*exp(-1.342/T9-Pow2(T9/13.))
                            +1.77e+04*T958*exp(-3.049/T9-Pow2(T9/13.)));
        /* Correction: Illiades (sic) et al. Nucl. Phys. A512, 509 (1990): */
        sigmav[SIGMAV_T25]/=4.5;
#else
        // Mg25(p,gamma)Al26 (ground state!) from NACRE
#ifdef NUCSYN_HOT_SIGMAV
        if(t9<=2)
        {
#endif// NUCSYN_HOT_SIGMAV
            sigmav[SIGMAV_T25]=3.07e-16* t15 *exp(-0.435*i9)+3.70e-8* t15 *exp(-0.673*i9)+1.60e-5* t15 *exp(-1.074*i9)+1.27e4* pow(t9,0.647) *exp(-3.055*i9);
#ifdef NUCSYN_HOT_SIGMAV
        }
        else
        {
            sigmav[SIGMAV_T25]=8.75e3*t9*exp(-2.997*i9);
        }
#endif// NUCSYN_HOT_SIGMAV
#ifdef NUCSYN_THERMALIZED_CORRECTIONS
        sigmav[SIGMAV_T25] *= (1-0.352*exp(-7.221*i9+0.068*t9));
#endif
#endif // RATE CHOOSER
        sigmav[SIGMAV_T25] /= AVOGADRO_CONSTANT;

        /************************************************************/
        /************************************************************/
        /********************* Mg25(p,g) Al26m **********************/
        /************************************************************/
        /************************************************************/
#ifdef NUCSYN_HOT_SIGMAV
        if(t9<=2.0)
        {
#endif// NUCSYN_HOT_SIGMAV
            sigmav[SIGMAV_T25m] = 8.15e-17*t15*exp(-0.435*i9) +
                8.68e-9*t15*exp(-0.673*i9)+
                2.82e-6*t15*exp(-1.074*i9)+
                3.48e3*pow(t9,1.362)*exp(-2.906*i9)
                ;
#ifdef NUCSYN_HOT_SIGMAV
        }
        else
        {
            sigmav[SIGMAV_T25m]=3.91e3*pow(t9,1.262)*exp(-3.229*i9);
        }
#endif// NUCSYN_HOT_SIGMAV
#ifdef NUCSYN_THERMALIZED_CORRECTIONS
        sigmav[SIGMAV_T25m]*=(1.0-0.352*exp(-7.221*i9+0.068*t9));
#endif
        sigmav[SIGMAV_T25m] /= AVOGADRO_CONSTANT;

        /************************************************************/
        /************************************************************/
        /********************* Al26(beta+)Mg26 **********************/
        /************************************************************/
        /************************************************************/

        /* Al26(beta+)Mg26 is NOT quick ~ 1e6 years */

#if (defined(RATES_OF_AREND_JAN))
        sigmav[SIGMAV_TBETA26] = 3.2779e13; // in seconds
        //7.2e5 * YEAR_LENGTH_IN_SECONDS / log(2.0);
#else
        sigmav[SIGMAV_TBETA26]=XAl26_BETA_DECAY*YEAR_LENGTH_IN_SECONDS;
#endif // RATE CHOOSER

        /************************************************************/
        /************************************************************/
        /********************** Al26(p,g)Si27 ***********************/
        /************************************************************/
        /************************************************************/

#if (defined(RATES_OF_AMANDA))
        /* NACRE rate */
#ifdef NUCSYN_HOT_SIGMAV
        if(t9<0.9)
        {
#endif// NUCSYN_HOT_SIGMAV
            sigmav[SIGMAV_T26P]=3.54e-9* t15 *exp(-0.789*i9)+8.54e-7*
                t15 *exp(-1.079*i9)+10.3*
                t15*exp(-2.182*i9)+6.12e2*t15*exp(-3.203*i9)+1.05e4*t15*exp(-4.213*i9);
#ifdef NUCSYN_HOT_SIGMAV
        }
        else
        {
            sigmav[SIGMAV_T26P]=1.63e4*pow(t9,0.348)*exp(-4.285*i9);
        }
#endif// NUCSYN_HOT_SIGMAV
#ifdef NUCSYN_THERMALIZED_CORRECTIONS
        sigmav[SIGMAV_T26P]*=(1.0+2.17*exp(-4.616*i9-0.076*t9));
#endif
#elif (defined(RATES_OF_MARIA))
        /* Maria's REACTION Al26pgSi27 -> SIGMAV_T26P */
        sigmav[SIGMAV_T26P]=Reaclib(0.211853e+02,-0.181149e-03,-0.231640e+02,-0.806922e-01,0.126339e-01,-0.218412e-02,-0.635626)+Reaclib(-0.194622e+02,-0.799519,-0.833775e-02,0.263742e-01,-0.254759e-02,0.207772e-03,-0.150919e+01)+Reaclib(-0.140023e+02,-0.108957e+01,-0.192068e-01,0.655138e-01,-0.672448e-02,0.570960e-03,-0.152200e+01)+Reaclib(-0.915706e+01,-0.149593e+01,0.568974e-02,-0.184887e-01,0.183024e-02,-0.151859e-03,-0.149364e+01)+Reaclib(0.217872e+01,-0.218510e+01,-0.111226e-01,0.274505e-01,-0.223807e-02,0.163224e-03,-0.151072e+01)+Reaclib(0.615109e+02,-0.285261e+01,0.251283e+02,-0.845335e+02,0.641356e+01,-0.435046,0.306598e+02)+Reaclib(0.185333e+02,-0.820942e+01,0.122707e+02,-0.209107e+02,0.105220e+01,-0.540533e-01,0.905230e+01);

#elif (defined(RATES_OF_AREND_JAN))
        sigmav[SIGMAV_T26P]=(6.78e+13/T923*exp((-23.261/T913)
                                               *(1.+2.004e-01*T9-1.538e-02*Pow2(T9)+5.723e-04*Pow3(T9)))
                             +6.13e+02/T932*exp(-3.202/T9)+9.45e+03/T9*exp(-4.008/T9));
        sigmav[SIGMAV_T26P]=sigmav[SIGMAV_T26P] -1/11.*exp(-2.651/T9)
            *(1.36e+14/T923*exp((-23.261/T913)
                                *(1.+2.004e-01*T9-1.538e-02*Pow2(T9)+5.723e-04*Pow3(T9))));
#else
        // Al26(p,gamma)Si27 which then beta decays quickly to Al27
        if(t9<0.9)
        {
            sigmav[SIGMAV_T26P]=3.54e-9* t15 *exp(-0.789*i9)+8.54e-7*
                t15 *exp(-1.079*i9)+10.3*
                t15*exp(-2.182*i9)+6.12e2*t15*exp(-3.203*i9)+1.05e4*t15*exp(-4.213*i9);
        }
        else
        {
            sigmav[SIGMAV_T26P]=1.63e4*pow(t9,0.348)*exp(-4.285*i9);
        }
#ifdef NUCSYN_THERMALIZED_CORRECTIONS
        sigmav[SIGMAV_T26P]*=(1.0+2.17*exp(-4.616*i9-0.076*t9));
#endif
#endif // RATE CHOOSER
        sigmav[SIGMAV_T26P]/=AVOGADRO_CONSTANT;

        /************************************************************/
        /************************************************************/
        /********************** Mg26(p,g)Al27 ***********************/
        /************************************************************/
        /************************************************************/


#if (defined(RATES_OF_AMANDA))
        /*
          c UPDATING: mg26(p,g)al27 from I+90
          c  Resonant contributions, where there are upper limit a factor 0.1
          c  is introduced. There are many unknown factor (0.1) and the error on
          c  the measured is from 12% to 80%.
          c  The higher resonance are taken from Smit+82 Nucl.Phys. A377,15 e
          c  Keinonen +80 Nucl.Phys. A341,345 as said by I+90.
          c  For t9>0.4 mine is lower of <5% because i didn't put ALL the higher
          c  resonance.
          c  The direct capture process and the low
          c  energy tails produce a similar contribution which do something
          c  only for t9<0.01, where the rate is < 10-34 anyway. So I don't put
          c  them.
          c */
        amu=26.0/27.0;
        amu32=1.54e5*pow(amu,-1.5)/t932;
        a=-11.604*26.0/(27.0*t9);
        a1=exp(a*16.0e-03*i9)  ;
        a1=amu32*1.5e-38*0.1*a1;
        a2=exp(a*54.0e-03);
        a2=amu32*5.0e-17*a2;
        a3=exp(a*93.0e-03);
        a3=amu32*1.2e-10*0.1*a3;
        a4=exp(a*108.0e-03);
        a4=amu32*1.7e-11*a4;
        a5=exp(a*129.0e-03);
        a5=amu32*1.3e-15*0.1*a5;
        a6=exp(a*142.0e-03);
        a6=amu32*3.0e-08*0.1*a6;
        a7=exp(a*154.0e-03);
        a7=amu32*8.0e-08*a7;
        a8=exp(a*177.0e-03);
        a8=amu32*6.0e-11*0.1*a8;
        a9=exp(a*227.0e-03);
        a9=amu32*4.9e-05*a9;
        a10=exp(a*258.0e-03);
        a10=amu32*2.0e-06*0.1*a10;
        a11=exp(a*275.0e-03);
        a11=amu32*1.0e-06*a11;
        a12=exp(a*292.0e-03);
        a12=amu32*6.0e-03*a12;
        a13=exp(a*326.0e-03);
        a13=amu32*3.0e-05*0.1*a13;
        a14=exp(a*338.0e-03);
        a14=amu32*0.25*a14;
        a15=exp(-11.604*437.0e-03*i9);
        a15=amu32*1.2*a15;
        a16=exp(-11.604*445.0e-03*i9);
        a16=amu32*3.4e-02*a16;
        a17=exp(-11.604*460.0e-03*i9);
        a17=amu32*8.2e-03*a17;
        a18=exp(-11.604*482.0e-03*i9);
        a18=amu32*3.9e-03*a18;
        a19=exp(-11.604*502.0e-03*i9);
        a19=amu32*9.6e-03*a19;
        a20=exp(-11.604*625.0e-03*i9);
        a20=amu32*0.11*a20;
        a21=exp(-11.604*633.0e-03*i9);
        a21=amu32*1.9e-02*a21;
        a22=exp(-11.604*637.0e-03*i9);
        a22=amu32*0.53*a22;
        a23=exp(-11.604*691.0e-03*i9);
        a23=amu32*0.37*a23;
        a24=exp(-11.604*779.0e-03*i9);
        a24=amu32*1.3*a24 ;
        a25=exp(-11.604*808.0e-03*i9);
        a25=amu32*1.5*a25 ;
        a26=exp(-11.604*918.0e-03*i9);
        a26=amu32*0.82*a26;
        a27=exp(-11.604*944.0e-03*i9);;
        a27=amu32*0.48*a27;
        a28=exp(-11.604*964.0e-03*i9);
        a28=amu32*1.4*a28;
        a29=exp(-11.604*968.0e-03*i9);
        a29=amu32*0.29*a29;
        a30=exp(-11.604*1000.0e-03*i9);
        a30=amu32*0.091*a30;
        a31=exp(-11.604*1002.0e-03*i9);
        a31=amu32*0.43*a31;
        a32=exp(-11.604*1005.0e-03*i9);
        a32=amu32*0.67*a32;
        a33=exp(-11.604*1240.0e-03*i9);
        a33=amu32*1.6*a33  ;
        a34=exp(-11.604*1329.0e-03*i9);
        a34=amu32*2.6*a34  ;
        a35=exp(-11.604*1357.0e-03*i9);
        a35=amu32*1.7*a35  ;
        sigmav[SIGMAV_T26]=a1+a2+a3+a4+a5+a6+a7+a8+a9+a10+a11+
            a12+a13+a14+a15+a16+a17+a18+a19+a20
            +a21+a22+a23+a24+a25+a26+a27+a28+a29
            +a30+a31+a32+a33+a34+a35;

#elif (defined(RATES_OF_MARIA))
        /* Maria's REACTION Mg26pgAl27 -> SIGMAV_T26 */

        sigmav[SIGMAV_T26]=Reaclib(0.367326e+02,-0.285822e-01,-0.171860e+02,-0.241996e+02,0.454763e+01,-0.134721e+01,0.584008e+01)+Reaclib(-0.264345e+02,-0.610945,-0.387739e-01,0.130991,-0.134051e-01,0.113848e-02,-0.154416e+01)+Reaclib(0.557003e+01,-0.106206e+01,0.564415e+01,-0.242421e+02,0.228001e+01,-0.177490,0.653801e+01)+Reaclib(-0.266603e+02,-0.124876e+01,-0.277958e+02,0.532718e+02,-0.384878e+01,0.264868,-0.245900e+02)+Reaclib(0.224299e+01,-0.254252e+01,0.101529,-0.263173,0.221733e-01,-0.168360e-02,-0.139841e+01)+Reaclib(-0.799631e+01,-0.305480e+01,-0.152255e+02,0.322869e+02,-0.244178e+01,0.171841,-0.149407e+02)+Reaclib(0.106582e+02,-0.378911e+01,0.303665,-0.780598,0.127192e+01,-0.144673,-0.136316e+01);


#elif (defined(RATES_OF_AREND_JAN))
        sigmav[SIGMAV_T26]=(7.39e+08/T923*exp(-22.042/T913-Pow2(T9/0.299))
                            *(1.+0.019*T913+3.61*T923+0.478*T9+9.78*T943+3.29*T953)
                            +1.32e-10/T932*exp(-0.603/T9)+2.90e-05/T932*exp(-1.056/T9)
                            +6.45e-05/T932*exp(-1.230/T9)+5.64e-02/T932*exp(-1.694/T9)
                            +2.86e+03/T932*exp(-3.265/T9)+7.99e+04/T932*exp(-3.781/T9)
                            +4.23e+04*T912*exp(-3.661/T9));
        sigmav[SIGMAV_T26]/=1.+5.0*exp(-20.990/T9);


#else
        // Mg26(p,gamma)Al27 NACRE
#ifdef NUCSYN_HOT_SIGMAV
        if(t9<=3.5)
        {
#endif// NUCSYN_HOT_SIGMAV
            sigmav[SIGMAV_T26]=8.54e-12*t15*exp(-0.605*i9)+1.93e-6*
                t15*exp(-1.044*i9)+9.67e-3*
                t15*exp(-1.726*i9)+9.5e4*
                t15*exp(-3.781*i9)+10.2*
                pow(t9,-1.565)*exp(-2.521*i9)+7.07e4*
                pow(t9,0.215)*exp(-3.947*i9);
#ifdef NUCSYN_HOT_SIGMAV
        }
        else
        {
            sigmav[SIGMAV_T26]=3.95e4*pow(t9,1.068)*exp(-4.990*i9);
        }
#endif// NUCSYN_HOT_SIGMAV
#ifdef NUCSYN_THERMALIZED_CORRECTIONS
        sigmav[SIGMAV_T26] *= (1.0-1.259*exp(-20.076*i9+0.069*t9));
#endif
#endif // RATE CHOOSER
        sigmav[SIGMAV_T26] /= AVOGADRO_CONSTANT;

        /************************************************************/

#if (defined(RATES_OF_AREND_JAN))
        const double TT=T9/(1.+2.69*T9);
        const double T56=pow(TT,(5.0/6.0));
        const double T13=1.0/cbrt(TT);
        sigmav[SIGMAV_O17G]=(7.97e+07*T56*TM32*exp(-16.712*T13)+1.51e+08*TM23*exp(-16.712*TM13)*
                             (1.+0.025*TP13-0.051*TP23-8.82e-03*T9)+1.56e+05*TM9*exp
                             ( -6.272*TM9)+ /*F3=*/0.1 *1.31e+01*TM32*exp(-1.961*TM9));

#else
        /* O17(p,gamma)F18 : NACRE*/
#ifdef NUCSYN_HOT_SIGMAV
        if(t9<=3)
        {
#endif// NUCSYN_HOT_SIGMAV
            sigmav[SIGMAV_O17G]=1.5e8*t23*
                exp(-16.710*tm13-Pow2(t9/0.2))+
                9.79e-6*t15*exp(-0.7659*i9)+
                4.15*t15*exp(-2.083*i9)+
                7.74e4*pow(t9,1.16)*exp(-6.342*i9);
#ifdef NUCSYN_HOT_SIGMAV
        }
        else
        {
            sigmav[SIGMAV_O17G]=1.74e3*pow(t9,0.700)*exp(-1.072*i9);
        }
#endif// NUCSYN_HOT_SIGMAV
#ifdef NUCSYN_THERMALIZED_CORRECTIONS
        sigmav[SIGMAV_O17G] *= (1.0-0.287*exp(-10.011*i9-0.062*t9));
#endif
#endif // RATE CHOOSER

        sigmav[SIGMAV_O17G] /= AVOGADRO_CONSTANT;

        /************************************************************/

        /* O18(p,alpha)N15 */
#if (defined(RATES_OF_AREND_JAN))
        sigmav[SIGMAV_O18A]=
            (3.63e+11*TM23*exp(-16.729*TM13-Pow2(T9/1.361))*
             (1.+0.025*TP13+1.88*TP23+0.327*T9+4.66*TP43+2.06*TP53)+2.35e-14*TM32*
             exp(-0.253*TM9)+2.66e+04*TM32*exp(-1.67*TM9)+2.41e+09*TM32*exp
             (-7.638*TM9)+1.46e+09*TM9*exp(-8.31*TM9)+/*F4=*/0.1*
             (7.88e14/(TP23*(0.439*Pow2(1.+5.18*TP23)+0.561))*exp(-16.729*TM13-0.534*TP23)));

#else
        /* NACRE */
        sigmav[SIGMAV_O18A]=5.58e11*t23*exp(-16.732*tm13-Pow2(t9/0.51))*(1+3.2*t9+21.8*t92)+
            9.91e-14*t15*exp(-0.232*i9)+
            2.58e4*t15*exp(-1.665*i9)
            +3.24e8*pow(t9,-0.378)*exp(-6.395*i9);
#ifdef NUCSYN_THERMALIZED_CORRECTIONS
        sigmav[SIGMAV_O18A] *= (1.0-1.968*exp(-25.673*i9-0.083*t9));
#endif
#endif // RATE CHOOSER

        sigmav[SIGMAV_O18A] /= AVOGADRO_CONSTANT;

        /************************************************************/

        /* O18(p,gamma)F19 NACRE */
#ifdef NUCSYN_HOT_SIGMAV
        if(t9<=2)
        {
#endif// NUCSYN_HOT_SIGMAV
            sigmav[SIGMAV_O18G]=4.59e8*t23*
                exp(-16.732*tm13-Pow2(t9/0.15))*
                (1.0-9.02*t9+506*t92-2400*t93)+
                9.91e-17*t15*exp(-0.232*i9)+
                3.30e-3*t15*exp(-1.033*i9)+
                1.61e2*t15*exp(-1.665*i9)+
                1.25e4*pow(t9,0.458)*exp(-5.297*i9);
#ifdef NUCSYN_HOT_SIGMAV
        }
        else
        {
            sigmav[SIGMAV_O18G]=1.38e4*pow(t9,0.829)*exp(-5.919*i9);
        }
#endif// NUCSYN_HOT_SIGMAV
#ifdef NUCSYN_THERMALIZED_CORRECTIONS
        sigmav[SIGMAV_O18G] *= (1.0-0.475*exp(-15.513*i9-0.102*t9));
#endif
        sigmav[SIGMAV_O18G] /= AVOGADRO_CONSTANT;

        /************************************************************/

        /* F18(p,alpha)O15 : we need to find this */
        //sigmav[SIGMAV_F18pa]=
        //sigmav[SIGMAV_F18pa]/=AVOGADRO_CONSTANT;

        /************************************************************/
        /* F18 (beta) O18 */
        sigmav[SIGMAV_F18BETA]=0.000301791312514561; // years

        /************************************************************/

/* F19(p,alpha)O16 */
#if (defined(RATES_OF_AREND_JAN))
        sigmav[SIGMAV_F19A]=(3.55e+11/T923*exp(-18.113/T913-Pow2(T9/0.845))
                             *(1.+0.023*T913+1.96*T923+0.316*T9+2.86*T943+1.17*T953)
                             +3.67e+06/T932*exp(-3.752/T9) + 3.07e8*exp(-6.019/T9));
        sigmav[SIGMAV_F19A]/=1.+4.*exp(-2.090/T9)+7.*exp(-16.440/T9);
#else
        sigmav[SIGMAV_F19A]=
            // NB -0.3333333333 used instead of 0.333333333333333333333333 - typo in NACRE
            2.62e11*t23*exp(-18.116*tm13-
                            Pow2(t9/0.185))*
            (1.0+6.26e-2*t9+0.285*t92+4.94e-3*t93+
             11.5*t94+7.40e4*t95)+
            3.8e6*t23*exp(-3.752*i9)+
            3.27e7*pow(t9,-0.193)*exp(-6.587*i9)+
            7.30e8*pow(t9,-0.201)*exp(-16.249*i9);

#ifdef NUCSYN_THERMALIZED_CORRECTIONS
        sigmav[SIGMAV_F19A]*=(1.0+0.755*exp(-1.755*i9-0.174*t9));
#endif
#endif
        sigmav[SIGMAV_F19A] /= AVOGADRO_CONSTANT;

        /************************************************************/

/* F19(p,n)Ne19(b+)F18(b+)O18 */
        sigmav[SIGMAV_F19n]=4.42e7*exp(-46.659*i9)*
            (1.0+1.19*t9-0.150*t92+6.68e-3*t93);
        sigmav[SIGMAV_F19n]*=(1.536+2.848*exp(-0.0331*i9-0.943*t9));
        sigmav[SIGMAV_F19n]/= AVOGADRO_CONSTANT;

#if (defined(RATES_OF_MARIA))
        /* Maria's REACTION Al27pgSi28 -> SIGMAV_T27 */
        sigmav[SIGMAV_T27]=Reaclib(0.365503e+02,-0.275874e-01,-0.185636e+02,-0.234291e+02,0.429262e+01,-0.117105e+01,0.562347e+01)+Reaclib(-0.229525e+02,-0.832147,0.892092e-02,-0.292581e-01,0.291664e-02,-0.243224e-03,-0.148998e+01)+Reaclib(-0.188665e+02,-0.980667,0.539901e-02,-0.161642e-01,0.149001e-02,-0.117713e-03,-0.149421e+01)+Reaclib(0.173054e+02,-0.233495e+01,0.972645e+01,-0.248307e+02,0.148285e+01,-0.831689e-01,0.932530e+01)+Reaclib(0.901190e+01,-0.379595e+01,0.115778e+02,-0.137693e+02,-0.876306e-01,0.573784e-01,0.762694e+01)+Reaclib(-0.258564e+02,-0.549543e+01,-0.180073e+02,0.585367e+02,-0.443583e+01,0.278242,-0.214648e+02);


#elif (defined(RATES_OF_AREND_JAN))
        sigmav[SIGMAV_T27]=
            (1.67e+08/T923*exp(-23.261/T913-Pow2(T9/0.155))
             *(1.+0.018*T913+5.81*T923+0.728*T9+27.31*T943+8.71*T953)
             +2.20e+00/T932*exp(-2.269/T9)+1.22e+01/T932*exp(-2.491/T9)
             +1.50e+04*T9*exp(-4.112/T9)
             +                       (0.1)*
             (6.50e-10/T932*exp(-0.853/T9)
              + 1.63e-10/T932*exp(-1.001/T9)))  ;

        sigmav[SIGMAV_T27]/=1.+exp(-9.792/T9)/3. + 2.*exp(-11.773/T9)/3.;

#else
        /* Al27(p,g)Si28 NACRE */
#ifdef NUCSYN_HOT_SIGMAV
        if(t9<=6)
        {
#endif// NUCSYN_HOT_SIGMAV
            sigmav[SIGMAV_T27]=2.51e-11*t15*exp(-0.839*i9)+
                48.2*pow(t9,-0.2)*exp(-2.223*i9)+
                1.76e3*pow(t9,1.12)*exp(-3.196*i9)+3.25e4*pow(t9,0.251)*
                exp(-5.805*i9);
#ifdef NUCSYN_HOT_SIGMAV
        }
        else
        {
            sigmav[SIGMAV_T27]=1.62e5*pow(t9,0.549)*exp(-17.222*i9);
        }
#endif// NUCSYN_HOT_SIGMAV
#ifdef NUCSYN_THERMALIZED_CORRECTIONS
        sigmav[SIGMAV_T27]*=(1.0-0.669*exp(-10.426*i9+0.008*t9));
#endif
#endif //RATES_OF_MARIA

        sigmav[SIGMAV_T27]/= AVOGADRO_CONSTANT;

        /************************************************************/

        /* Al27(p,alpha)Mg24 */

#if (defined(RATES_OF_AREND_JAN))
        sigmav[SIGMAV_T27a]=(1.10e+08/T923*exp(-23.261/T913-Pow2(T9/0.157))
                             *(1.+0.018*T913+12.85*T923+1.61*T9+89.87*T943+28.66*T953)
                             +1.29e+02/T932*exp(-2.517/T9)+5.66e+03*T972*exp(-3.421/T9)
                             +                       (0.1)*
                             (3.89e-08/T932*exp(-0.853/T9)
                              + 8.18e-09/T932*exp(-1.001/T9)))  ;
        sigmav[SIGMAV_T27a]/=1.+exp(-9.792/T9)/3. + 2.*exp(-11.773/T9)/3.;

#else
        /* NACRE */
#ifdef NUCSYN_HOT_SIGMAV
        if(t9<=6)
        {
#endif// NUCSYN_HOT_SIGMAV
            sigmav[SIGMAV_T27a]=4.76e10*t23*
                exp(-23.265*tm13-Pow2(t9/0.15))*
                (1.0-22.3*t9+126.7*t92)+
                9.65e-11*t15*exp(-0.834*i9)+2.09e-3*t15*
                exp(-2.269*i9)+1.17e-2*t15*
                exp(-3.273*i9)+2.84e4*exp(-5.623*i9)*i9+
                1.38e6*t9*exp(-10.01*i9);
#ifdef NUCSYN_HOT_SIGMAV
        }
        else
        {
            sigmav[SIGMAV_T27a]=6.02e5*pow(t9,1.862)*exp(-14.352*i9);
        }
#endif// NUCSYN_HOT_SIGMAV
#ifdef NUCSYN_THERMALIZED_CORRECTIONS
        sigmav[SIGMAV_T27a]*= (1.0+3.471*exp(-11.091*i9-0.129*t9));
#endif
#endif // RATE CHOOSER

        sigmav[SIGMAV_T27a] /= AVOGADRO_CONSTANT;

        /************************************************************/

#ifdef HELIUM_BURNING_REACTIONS
        /* Al27(alpha,n)P30 */
        sigmav[SIGMAV_T27an]=8.15e4*exp(-30.667*i9)*
            (1.0-1.351*t9+1.086*t92+0.354*t93+
             0.014*t94-2.13e-3*t95);
#ifdef NUCSYN_THERMALIZED_CORRECTIONS
        sigmav[SIGMAV_T27an]*=(1.0+0.227*exp(-3.588+0.0172*t9));
#endif
        sigmav[SIGMAV_T27an] /= AVOGADRO_CONSTANT;
#endif

        /* Si28(p,gamma)P29 : NACRE */
#ifdef NUCSYN_HOT_SIGMAV
        if(t9<=3.0)
        {
#endif // NUCSYN_HOT_SIGMAV
            sigmav[SIGMAV_T28]=8.71e8*t23*exp(-24.452*tm13-Pow2(t9/1.1))
                *(1.0+0.301*t9+0.069*t92)+3.37e2*t15*exp(-4.155*i9)
                +1.14e3*pow(t9,1.654)*exp(-11.055*i9);
#ifdef NUCSYN_HOT_SIGMAV
        }
        else
        {
            sigmav[SIGMAV_T28]=32.4*pow(t9,2.052)*exp(-1.525*i9);
        }
#endif// NUCSYN_HOT_SIGMAV
#ifdef NUCSYN_THERMALIZED_CORRECTIONS
        sigmav[SIGMAV_T28]*=(1.0-23.813*exp(-22.872*i9-0.353*t9));
#endif
        sigmav[SIGMAV_T28] /= AVOGADRO_CONSTANT;


        /*
         * triple-alpha rate : NACRE
         */

        /*
         * He4+He4+n -> Be9 + n
         * This is NA<sigmav>^{alphalpha}_{gs}
         */
        const double sigmav_alpha_alpha =
            2.43e9 * t23 * exp(-13.490 * tm13 - t92/Pow2(0.15)) * (1.0 + 74.5*t9)
            +
            6.09e5 * t23 * exp(-1.054 * i9);

        /*
         * This is NA<sigmav>^{alphaalphan}_{gs}

        const double sigmav_alpha_alpha_n =
            sigmav_alpha_alpha *
            t9 < 0.03
            ?
            (
                6.69e-12 * (1.0 - 192*t9 + 2.48e4*t92 - 1.50e6*t93 + 4.13e7*t94 - 3.90e8 * t95)
                )
            :
            (
                2.42e-12 * (1.0 - 1.52*logt9 + 0.448*Pow2(logt9) + 0.435*Pow3(logt9))
                );
        */

        /*
         * He4+He4 -> Be8
         * This is NA<sigmav>^{alphaBe8}_{gs}
         */
        const double sigmav_Be8 =
            2.76e7 * t23 * exp(-23.570*tm13 - t92/Pow2(0.4)) *
            (1.0 + 5.47 * t9 + 326.0 * t92)
            +
            130.7 * t15 * exp(-3.338 * i9)
            +
            2.51e4 * t15 * exp(-20.307 * i9);

        /*
         * hence triple-alpha rate
         * Table on p42-43
         * Equations on p148
         */
        const double part3 =
            t9 <= 0.03
            ?
            ( 3.07e-16 * (1.0 - 29.1*t9 + 1308.0*t92) )
            :
            ( 3.44e-16 * (1.0 + 0.0158 * pow(t9,-0.65)) );

        sigmav[SIGMAV_3He4] =
            sigmav_alpha_alpha *
            sigmav_Be8 *
            part3;

        /*
         * reaclib
         */

        sigmav[SIGMAV_3He4] =
            Reaclib(-9.710520e-01,	0.000000e+00,	-3.706000e+01,	2.934930e+01,	-1.155070e+02,	-1.000000e+01,	-1.333330e+00) +
            Reaclib(-1.178840e+01,	-1.024460e+00,	-2.357000e+01,	2.048860e+01,	-1.298820e+01,	-2.000000e+01,	-2.166670e+00) +
            Reaclib(-2.435050e+01,	-4.126560e+00,	-1.349000e+01,	2.142590e+01,	-1.347690e+00,	8.798160e-02,	-1.316530e+01);

        sigmav[SIGMAV_3He4] /= Pow2(AVOGADRO_CONSTANT);

        /*
         * Reaction rate:
         *
         * Two particle
         * r = lambda * N = <sigmav> N1 N2
         *
         * Three particle
         * r = <sigmav> N1 N2 N3
         *
         * hence, if N is the number of helium, which
         * is the rate of C12 production,
         *
         * r = 1/6 * NA^2 * rho^2 * Y^3 <sigmav> N*N*N
         *
         * and the rate of He4 destruction is 3* this.
         *
         * lambda = ln2/tau
         *
         *
         */


        /*
         * C12 + He4 -> O16 + gamma
         * page 67
         * formulae on page 151
         */
        const double sigmav_C12_tmp = -32.123 * tm13;
        const double sigmav_C12_E1 =
            6.66e7 * Pow2(i9) * exp(sigmav_C12_tmp - t92/Pow2(4.6)) *
            (1.0 + 2.54*t9 + 1.04*t92 - 0.226*t93)
            +
            1.39e3 * t15 * exp(-28.930*i9);

        const double sigmav_C12_E2 =
            6.56e7 * Pow2(i9) * exp(sigmav_C12_tmp - t92/Pow2(1.3)) *
            (1.0 + 9.23*t9 - 13.7*t92 + 7.4*t93);

        const double sigmav_C12_res =
            19.2 * t92 * exp(-26.9*i9);

        sigmav[SIGMAV_C12_He4] =
            sigmav_C12_E1 + sigmav_C12_E2 + sigmav_C12_res;

        sigmav[SIGMAV_C12_He4] /= AVOGADRO_CONSTANT;

        /*
         * O16 + He4 -> Ne20 + gamma
         * page 87
         * formulae on page 157
         */
        sigmav[SIGMAV_O16_He4] =
            2.68e10 * t23 * exp(-39.760*tm13 - t92/Pow2(1.6))
            +
            51.1 * t15 * exp(-10.32 * i9)
            +
            616.6 * t15 * exp(-12.200 * i9)
            +
            0.41 * pow(t9,2.966) * exp(-11.900 * i9);

        sigmav[SIGMAV_O16_He4] /= AVOGADRO_CONSTANT;

        /*
         * Ne20 + alpha -> Mg24 + gamma
         * page 110
         * formulae on page 160
         */
        sigmav[SIGMAV_Ne20_He4] =
            t9 <= 1.0
            ?
            ( 8.72 * pow(t9,-0.532) * exp(-8.995 * i9) )
            :
            ( 3.74e2 * pow(t9,2.229) * exp(-12.681 * i9) );
#ifdef NUCSYN_THERMALIZED_CORRECTIONS
        sigmav[SIGMAV_Ne20_He4] *=
            (1.0 - 7.787 * exp(-19.821 * i9 - 0.114 * t9));
#endif // NUCSYN_THERMALIZED_CORRECTIONS

        sigmav[SIGMAV_Ne20_He4] /= AVOGADRO_CONSTANT;

        /************************************************************/
        /************************************************************/
        /************************************************************/
        /************************************************************/
        /************************************************************/

        /*
         * Check the sigmav is not zero, if it is then problems
         * can occur during 1/sigmav (timescale) calculations
         */
        for(Reaction_rate_index i=1;i<SIGMAV_SIZE;i++)
        {
            sigmav[i]=Max(SIGMAV_TINY,sigmav[i]);
        }

#ifdef NUCSYN_SIGMAV_PRE_INTERPOLATE
    }
#endif

    if(multipliers!=NULL)
    {
        for(Reaction_rate_index i=1;i<SIGMAV_SIZE;i++)
        {
            sigmav[i] *= multipliers[i];
        }
    }

#ifdef REACTION_RATES_TABLE
    FILE * fp = stderr;
    fprintf(fp,"Reaction rates table : log10 T=%g, T=%g, t9=%g\n",temp,exp10(temp),t9);
    fprintf(fp," Reac# .......enum...............................................................Reaction        Rate    Rate*6e23 \n");

#undef X
    unsigned int i = 0;
#define X(REACTION,STRING)                          \
    fprintf(fp,                                     \
            "%3u %14s %70s  %10.3e   %10.3e\n",    \
            i,                                      \
            #REACTION,                              \
            STRING,                                 \
            sigmav[i],                              \
            sigmav[i]*AVOGADRO_CONSTANT);           \
    i++;
    NUCSYN_SIGMAV_CODES;
#undef X
    fprintf(fp,"\n");
    fflush(NULL);
    Exit_binary_c_no_stardata(BINARY_C_NORMAL_EXIT,"Sigmav table exit\n");
#endif /* REACTION_RATES_TABLE */

}

#endif /* NUCSYN */
