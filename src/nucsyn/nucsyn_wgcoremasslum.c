#include "../binary_c.h"
No_empty_translation_unit_warning;

#if defined NUCSYN &&                           \
    WAGENHUBER_GROENEWEGEN_LUMFUNC

Constant_function double nucsyn_wgcoremasslum(const double mc,
                                       const double mass,
                                       const double mc_1tp,
                                       const Abundance z,
                                       const double alpha,
                                       const Boolean hbbB)
{
    /*
     * core-mass luminosity relation according to Wagenhuber & Groenewegen eq.
     * (5abcd)
     */
    const Abundance zz = log10(z/0.02);
    double ya,yb,yc=1.0,yd=0.0;
    const double Dmc=mc-mc_1tp;
    const double menv=mass-mc;
    double f1,f2,f3; /* fudge factors */
    Dprint("WGcmlr mc=%g mc_1tp=%g m=%g z=%g alpha=%g \n",mc,mc_1tp,mass,z,alpha);
    Dprint("WG Dmc = %g - %g = %g\n",mc,mc_1tp,Dmc);
    ya = (18160 + 3980*zz)*Max((mc -  0.4468),0.0) ; /* 5a */
    yb = exp10((2.705 + 1.649*mc)); /* 5b */
    Dprint("WG calc b %g \n",(2.705 + 1.649*mc));

    /*
     * NB impose lower limit on menv because this function tends not to work
     * very well if menv < 0.1 - this is ok because we expect HBB to switch off
     * at menv<0.1 (or probably more than 0.1) anyway
     */
    if((hbbB==TRUE)&&(menv>0.1))
    {
/*
 * this is their 5c factor, to take account of HBB
 */
        /* fudge factors : = 1 for normal W+G */
        f1=2.19; /* 2.19 */
        f2=0.92; /* 0.92 */
        f3=0.6; /* 0.6 */

        /* overwrite with W+G's values? */
        //f1=1;f2=1;f3=1;
        Dprint("WG calc c : %g %g \n",pow(menv,2.0*f2),exp(-Dmc/(0.01*f3)));

        yc = exp10(
                 (f1*0.0237*(alpha-1.447)*Pow2(mc_1tp)*pow(menv,2.0*f2)*
                  (1.0 - exp(-Dmc/(0.01*f3))))
            ); /* this is ok! */
    }
    if(mc_1tp > 0.45)
    {
        yd = exp10((3.529 - Max((mc_1tp - 0.4468),0.0)*Dmc/0.01)); /* 5d */
        Dprint("WG calc d %g %g\n",Max((mc_1tp - 0.4468),0.0),Dmc/0.01);
    }

    Dprint("\nWGy %g %g %g %g %g\n",ya,yb,yc,yd,ya+yb*yc-yd);

    return (ya+yb*yc-yd);
}
#endif // NUCSYN && WAGENHUBER_GROENEWEGEN_LUMFUNC
