#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "nucsyn.h"

/* Function to output the results of nucleosynthesis calculations */

#ifdef NUCSYN

#include "nucsyn_log.h"

/*
 * Function to output details of nucleosynthesis to a log
 * used for TPAGB debugging (in Rob's PhD)
 */

#define LOG_VARS_LIST                                                         \
    X(                                                1e6*t,          "Time") \
    X(                                                    m,          "Mass") \
    X(                                                   mc,            "Mc") \
    X(                                    XRATIO(XC12,XC13),     "XC12/XC13") \
    X(                                             XCORATIO,         "XC/XO") \
    X(                                            star->rho,           "Rho") \
    X(                                           star->temp,          "Temp") \
    X(                                                  lum,         "Lsurf") \
    X(                                                 m-mc,          "Menv") \
    X(                              star->core_mass_no_3dup,       "Mcnodup") \
    X(                                                    r,             "R") \
    X(                                                   rc,            "Rc") \
    X(                                    star->lambda_3dup,        "Lambda") \
    X( log10(star->core_mass_no_3dup - star->mc_1tp + TINY), "log10dMcnodup") \
    X(                                 fabs(Mdot_net(star)),          "Mdot") \
    X(                                                  0.0,    "MiraPeriod") \
    X(                                         star->radius,         "R_eff") \
    X(                         1e-6*star->interpulse_period,          "T_ip") \
    X(                                        tpagb_runtime,    "AGBruntime") \
    X(                                   stardata->model.dt,      "Timestep") \
    X(                                    NRATIO(XC12,XC13),     "NC12/NC13") \
    X(                                             NCORATIO,         "NC/NO") \
    X(                             star->num_thermal_pulses,           "NTP") \
    X(                                           star->dntp,          "DNTP")

void nucsyn_log(const double t,
                const double m,
                const double mc,
                double r,
                const double rc,
                const double lum,
                struct star_t * star,
                struct stardata_t *stardata,
                double lambda Maybe_unused,
                Abundance * Restrict Xabund,
                const double tpagb_runtime)
{
    if(stardata->preferences->cf_amanda_log == TRUE)
    {
        char * headstring = NULL;
        if(asprintf(&headstring,HEAD_STRING)>0)
        {
            if(stardata->common.nucsyn_log_count==0)
            {
                /*
                 * First time header
                 */
                fprintf(OUTPUT,"%s",headstring);
#undef X
#define X(VAR,STRING) fprintf(OUTPUT,"%-14.14s ",STRING);
                LOG_VARS_LIST;
#undef X
#define X(ISOTOPE,Z,A,MASS) fprintf(OUTPUT,"%-14.14s ",#ISOTOPE);
                NUCSYN_ISOTOPE_LIST
#undef X
                    fprintf(OUTPUT,"\n");
            }
            stardata->common.nucsyn_log_count++;

            if(star->stellar_type==TPAGB)
            {
                /*
                 * Every TPAGB step : data
                 */
                fprintf(OUTPUT,"%s",headstring);
#undef X
#define X(VAR,STRING) fprintf(OUTPUT,"% 8e ",VAR);
                LOG_VARS_LIST;
#undef X
#define X(ISOTOPE,Z,A,MASS) fprintf(OUTPUT,"% 8e ",log10(Max(1e-30,Xabund[X##ISOTOPE])));
                NUCSYN_ISOTOPE_LIST
#undef X
                    fprintf(OUTPUT,"\n");
                FLUSH;
            }
            Safe_free(headstring);
        }
    }
}
/* undef these just in case they are used elsewhere */
#undef OUTPUT
#undef HEADER
#undef NUM_INDICES

#endif /* NUCSYN */
