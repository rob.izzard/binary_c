#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef NUCSYN
/*
 * Function to set the conditions for hot bottom burning : temperature and
 * density
 */

double nucsyn_set_hbb_conditions(struct star_t * const newstar,
                                 struct stardata_t * Restrict const stardata,
                                 const double m0,
                                 const double menv)
{

    /*
     * calculate temperature and density of the HBB layer, might need this in the
     * third dredge up code
     */
    double Thbbmax=stardata->preferences->hbbtfac*
        nucsyn_hbbtmax(newstar->menv_1tp,
                       newstar->mc_1tp,
                       stardata->common.metallicity);


    /* modulate temperatue by temp_mult, which is a fitted correction
     * to the temperature fitting function
     */
    newstar->temp = Thbbmax * newstar->temp_mult;
    Dprint("Thbbmax = %g * temp_mult = %g -> \n",Thbbmax,newstar->temp_mult);

    /* exponential temperature rise */
    if(newstar->temp_rise_fac > TINY)
    {
        newstar->temp *= (1.0-exp(-Max(0.0,newstar->num_thermal_pulses)/
                                   newstar->temp_rise_fac));
    }
    Dprint(" (ntp=%g fac=%g) -> %g -> ",
           newstar->num_thermal_pulses,
           newstar->temp_rise_fac,
           newstar->temp);

    newstar->temp *= pow(menv/newstar->menv_1tp,0.02);

    /* turn off HBB effect when menv <~ 1.x? - this is arbitrary! */
    if(menv<MIN_MENV_FOR_HBB) newstar->temp=3.0;

    newstar->rho=nucsyn_tpagb_rhomax(m0,
                                     stardata->common.metallicity);

    /* turn on */
    newstar->rho *= 1.0 - exp(-Max(0.0,newstar->num_thermal_pulses)/
                              Max(TINY,newstar->temp_rise_fac));

    /* turn off */
    newstar->rho *= menv / newstar->menv_1tp;

    Dprint("rho = %g T = %g\n",
           newstar->rho,
           newstar->temp);

    return(Thbbmax);
}
#endif /* NUCSYN */
