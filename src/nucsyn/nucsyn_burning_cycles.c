#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef NUCSYN


//#define TESTING

/*
 * Wrapper to call the various nuclear-burning cycles
 * in binary_c
 */

void nucsyn_burning_cycles(Abundance * Restrict const N,
                           Reaction_rate * Restrict const sigmav,
                           const double dt,
                           const double temperature,
                           const double density,
                           struct stardata_t * Restrict stardata)
{
#include "nucsyn_solver_functions.h"

    /*
     * We have a vb parameter otherwise
     * the debug output would be too much
     */
    const Boolean vb = FALSE;

#undef NUCSYN_NORMALIZE_NUCLEONS
    if(vb)Dprint("nucsyn_burning_cycles...\n");

    /*
     * Burn only if dt != 0
     */
    if(!Is_zero(dt))
    {

#ifdef TESTING
        /*
         * Crank the density
         */
        for(Isotope i=0; i<ISOTOPE_ARRAY_SIZE;i++)
        {
            N[i] *= 100.0;
        }
#endif // TESTING

        /*
         * Given abundances (by number) N, reaction rates sigmav,
         * burn for a time dt (years).
         */
#ifdef NUCSYN_NORMALIZE_NUCLEONS
        /* count nucleons */
        const double nucleon_density_in =
            nucsyn_nucleon_density(
#ifdef NANCHECKS
                stardata,
#endif // NANCHECKS
                N,
                stardata->store->nucleon_number
#ifdef NANCHECKS
                ,0
#endif // NANCHECKS
                );
#endif // NUCSYN_NORMALIZE_NUCLEONS


        /*
         * Now we loop over the various networks, and if they
         * are on and aavailable with the nucsyn_solver we call
         * them to do the burn.
         */
        for(Reaction_network i=0; i<NUCSYN_NETWORK_NUMBER; i++)
        {

            /*
             * If we have a function which tests T,rho to see
             * if they are in the appropriate range for burning, use it,
             * otherwise assume we must burn
             */
            const Boolean test_T_rho =
                stardata->store->nucsyn_network_checkfunction[i] != NULL ?
                stardata->store->nucsyn_network_checkfunction[i](stardata,
                                                                 N,
                                                                 temperature,
                                                                 density) :
                TRUE;

            if(vb)Dprint("Network %u \"%s\" : burn? %s && %s : for time %g\n",
                   i,
                   nuclear_network_strings[i],
                   Yesno(stardata->preferences->nucsyn_network[i]),
                   Yesno(test_T_rho),
                   dt);

#ifdef TESTING
            const double max_time = 1e6; // years
            const double time_step = 1e3; // years
            nucsyn_set_sigmav(stardata,stardata->store,
                              8.0, // log10(T/K)
                              sigmav,
                              NULL);
            double Nref[ISOTOPE_ARRAY_SIZE];
            double Nin[ISOTOPE_ARRAY_SIZE];
            Copy_abundances(N,Nin);
            Copy_abundances(N,Nref);
            char logstring[1024]; logstring[0] = '\0';
            char prevlogstring[1024];prevlogstring[0] = '\0';
#else
            const double time_step = dt;
#endif

            if(test_T_rho == TRUE &&
               stardata->preferences->nucsyn_network[i] == TRUE)
            {
#ifdef TESTING
                double time = 0.0; // years
                while(time < max_time)
                {
#endif // TESTING

                    if(nucsyn_network_burn_functions[stardata->preferences->nucsyn_solver] == NULL)
                    {
                        Exit_binary_c(BINARY_C_ALGORITHM_OUT_OF_RANGE,
                                      "Nuclear burning solver %d = \"%s\" is not available, even though it was requested.",
                                      stardata->preferences->nucsyn_solver,
                                      nucsyn_solver_strings[stardata->preferences->nucsyn_solver]);
                    }
                    else
                    {
                        /*
                         * Call the burn wrapper
                         */
                        stardata->common.dtguess[i] =
                            nucsyn_network_burn_functions[stardata->preferences->nucsyn_solver]
                            (
                                N,
                                sigmav,
                                time_step * YEAR_LENGTH_IN_SECONDS,
                                stardata->store->nucsyn_network_function[stardata->preferences->nucsyn_solver][i],
                                NULL,
                                stardata->common.dtguess[i],
                                stardata,
                                i,
                                vb
                                );
                    }
#ifdef TESTING

                    /* do the same burn with the Kaps-Rentrop */
                    nucsyn_network_burn_functions[NUCSYN_SOLVER_KAPS_RENTROP]
                        (
                            Nref,
                            sigmav,
                            time_step * YEAR_LENGTH_IN_SECONDS,
                            stardata->store->nucsyn_network_function[NUCSYN_SOLVER_KAPS_RENTROP][i],
                            NULL,
                            stardata->common.dtguess[i],
                            stardata,
                            i,
                            vb
                            );

                    time += time_step;
                    snprintf(logstring,
                             1024,
                             "%g %g %g %g\n",
                             N[XH1],N[XHe4],
                             Nref[XH1],Nref[XHe4]
                        );

                    printf("NOUT %g %s\n",
                           time,
                           logstring);
                    if(!Strings_equal(logstring,
                                      prevlogstring))
                    {
                        strlcpy(prevlogstring,logstring,1024);
                        fflush(stdout);
                    }
                }
                Flexit;
#endif // TESTING
            }
        }

#ifdef NUCSYN_NORMALIZE_NUCLEONS

        for(Isotope i=0; i<ISOTOPE_ARRAY_SIZE; i++)
        {
            N[i] = Max(0.0, N[i]);
        }

        const double nucleon_density_out=
            nucsyn_nucleon_density(
#ifdef NANCHECKS
                stardata,
#endif // NANCHECKS
                N,
                stardata->store->nucleon_number
#ifdef NANCHECKS
                ,1
#endif // NANCHECKS
                );

        if(Is_not_zero(nucleon_density_out) &&
           Is_not_zero(nucleon_density_in))
        {
            /* normalize: preserve nucleons! */
            const double fnucleon = nucleon_density_in/nucleon_density_out;
            printf("fnucleon %g / %g = %g, 1 - fnucleon = %g\n",
                   nucleon_density_in,
                   nucleon_density_out,
                   fnucleon,
                   1.0-fnucleon);
            Xmult(N,fnucleon);
#ifdef NANCHECKS
            {
                Nucsyn_isotope_stride_loop(
                    if(isnan(N[i])!=0 || !isfinite(N[i]))
                    {
                        Exit_binary_c(BINARY_C_EXIT_NAN,"nucsyn_burning_cycles (post-burning and normalization) isotope %u/%u is %g (fnucleon = %g from nucleon density in = %g and out = %g)\n",
                                      i,
                                      ISOTOPE_ARRAY_SIZE,
                                      N[i],
                                      fnucleon,
                                      nucleon_density_in,
                                      nucleon_density_out
                            );
                    }
                    );
            }

#endif // NANCHECKS
        }
#endif // NUCSYN_NORMALIZE_NUCLEONS
    }
}



#endif//NUCSYN
