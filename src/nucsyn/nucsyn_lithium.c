#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef NUCSYN
#ifdef LITHIUM_TABLES

static double logepsilon_to_X(const struct stardata_t * Restrict const stardata,
                              const struct star_t * Restrict const star,
                              const int i,
                              const double logepsilon);

#ifdef LITHIUM_LOGGING
static double logepsilon_from_X(const struct stardata_t * Restrict const stardata,
                                const struct star_t * Restrict const star,
                                const int i);
#endif // LITHIUM_LOGGING

static double lithium_table1(struct stardata_t * Restrict const stardata,
                             struct star_t * Restrict const star);

void nucsyn_lithium(struct star_t * Restrict const star,
                    struct stardata_t * Restrict const stardata)
{
#ifdef LITHIUM_LOGGING
    double tagb= star->stellar_type==TPAGB ?
        1.0e6*(star->age -TPAGB_start_time(star)) : 0.0;
#endif // LITHIUM_LOGGING

    if(star->stellar_type<=MAIN_SEQUENCE)
    {
        /*
         * Main sequence star: log epsilon = 1
         */
        //
        if(star->mass < 1.2)
        {
            /* ~convective */
            star->Xenv[XLi7] = logepsilon_to_X(stardata,star,XLi7,1.0);
        }
        else
        {
            /* radiative */
            star->Xenv[XLi7] = logepsilon_to_X(stardata,star,XLi7,3.37);
        }
    }
    else if(star->stellar_type>=HERTZSPRUNG_GAP &&
            star->stellar_type<=EAGB)
    {
        /*
         * Giant : log epsilon = 0
         */
        star->Xenv[XLi7] = logepsilon_to_X(stardata,star,XLi7,0.0);

        /* except in post-1st DUP red giants */
        /*
          if(star->first_dredge_up && star->stellar_type == GIANT_BRANCH)
          {
          star->Xenv[XLi7] = logepsilon_to_X(stardata,star,XLi7,stardata->preferences->lithium_GB_post_1DUP);
          }*/

        /* post He-flash only */
        if(star->stellar_type>=CHeB)
        {
            star->Xenv[XLi7] = logepsilon_to_X(stardata,star,XLi7,stardata->preferences->lithium_GB_post_Heflash);
        }
    }
    else if(star->stellar_type==TPAGB)
    {
        /*
         * TPAGB star : might have HBB!
         */
        if(star->phase_start_mass < 2.5)
        {
            /*
             * No HBB : assume ~ 0
             * because the convective envelope should be hot enough
             * to kill most of the lithium even if there was some
             * on the EAGB
             */
            star->Xenv[XLi7] = logepsilon_to_X(stardata,star,XLi7,0.0);
        }
        else
        {
            /*
             * Use lookup table during first 50 pulses, otherwise assume
             * HBB has destroyed all lithium
             */

            double ntp = star->num_thermal_pulses;
            if(star->num_thermal_pulses<=50)
            {
                /* in lookup range */
                star->Xenv[XLi7] = lithium_table1(stardata,star) *
                    stardata->preferences->lithium_hbb_multiplier;
            }
            else
            {
                /* very late : lithium destroyed */
                star->Xenv[XLi7] = 1e-30;
            }

            star->num_thermal_pulses=ntp;
        }
    }
    else
    {
        /*
         * All other types: no lithium
         */
        star->Xenv[XH1] += star->Xenv[XLi6] + star->Xenv[XLi7];
        star->Xenv[XLi6] = 0.0;
        star->Xenv[XLi7] = 0.0;
    }

    /*
     * Normalize abundances to 1.0
     */
    star->Xenv[XH1] = 0.0;
    star->Xenv[XH1] = 1.0 - nucsyn_totalX(star->Xenv);

#ifdef LITHIUM_LOGGING
    if(0 && star->stellar_type<7)
    {
        printf("LITHIUM star %d M=%g (M0=%g) : ntp=%g st=%d : Li7=%g logeps = %g\n",
               star->starnum,
               star->mass,
               star->phase_start_mass,
               star->num_thermal_pulses,
               star->stellar_type,
               star->Xenv[XLi7],
               logepsilon_from_X(stardata,star,XLi7));
    }

    if(1 && star->stellar_type<7 && star->starnum==0)
        //if(1 && star->stellar_type==TPAGB && star->starnum==0)
    {
        printf("LITHIUMF %g %g %g %g %d %g %g %g %g\n",
               stardata->model.time,
               star->mass,//2
               star->phase_start_mass,
               star->radius,//4
               star->stellar_type,
               star->num_thermal_pulses,//6
               star->Xenv[XLi7],
               star->Xenv[XLi7]>1e-42 ? logepsilon_from_X(stardata,star,XLi7) : (-42+12), //8
               tagb
            );
    }
#endif//LITHIUM_LOGGING
}


static double logepsilon_to_X(const struct stardata_t * Restrict const stardata,
                              const struct star_t * Restrict const star,
                              const int i,
                              const double logepsilon)
{
    /*
     * Convert log epslion to mass fraction
     *
     * log epsilon =
     *        12 + log( N / N_H )
     *
     * So:
     *
     * N/N_H = 10^(log epsilon - 12)
     *
     * with
     *
     * N/N_H = X/X_H (1/7) for lithium
     *
     * we have
     *
     * X = 7 * X_H * 10^(log epsilon - 12)
     *
     */


    double X= stardata->store->mnuc_amu[i] *
        star->Xenv[XH1] * exp10(logepsilon-12.0);

    /*
      printf("convert log epsilon = %g for element %d (mass %g) with XH1=%g : %g\n",
      logepsilon,
      i,
      stardata->store->mnuc_amu[i],
      star->Xenv[XH1],
      X);
    */

    return X;
}

#ifdef LITHIUM_LOGGING
static double logepsilon_from_X(const struct stardata_t * Restrict const stardata,
                                const struct star_t * Restrict const star,
                                const int i)
{
    double logepsilon = 12.0+log10(star->Xenv[i]/(stardata->store->mnuc_amu[i] * star->Xenv[XH1]));
    return logepsilon;
}
#endif//LITHIUM_LOGGING


static double lithium_table1(struct stardata_t * stardata,
                             struct star_t * star)
{
    /*
     * Use lookup table based on Amanda Karakas/Cherie Fishlock models
     * to get lithium as a function of pulse number
     */
#include "lithium_v2.h"
    Const_data_table lithium_table[]={LITHIUM_TABLE_V2};

    /* lookup co-ordinates */
    double x[3];
    x[0] = stardata->common.metallicity;
    x[1] = star->phase_start_mass;
    x[2] = Max(0.0,star->num_thermal_pulses);
    double r[1]; // result


    /* lookup ... */
    rinterpolate(lithium_table,
                 stardata->persistent_data->rinterpolate_data,
                 3,
                 1,
                 LITHIUM_TABLE_V2_LINES,
                 x,
                 r,
                 1);

    /* only result is lithium mass fraction */
    return r[0];
}

#endif // LITHIUM_TABLES
#endif // NUCSYN
