#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef NUCSYN

void nucsyn_NSNS(struct stardata_t * const stardata,
                 Abundance * const Xej)
{
    /*
     * Set mass fractions of NSNS yields
     */
    Dprint("in NSNS\n");
    if(stardata->preferences->NS_merger_yield_algorithm == NS_MERGER_YIELD_ALGORITHM_RADICE2018)
    {
        Dprint("Use Radice 2018\n");
        nucsyn_Radice2018(stardata,Xej);
    }
    else
    {
        Exit_binary_c(BINARY_C_ALGORITHM_OUT_OF_RANGE,
                      "Unknown NS merger yield algorithm %d\n",
                      stardata->preferences->NS_merger_yield_algorithm);
    }

}
#endif//NUCSYN
