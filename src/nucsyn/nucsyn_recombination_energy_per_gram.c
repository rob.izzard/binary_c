#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef NUCSYN

#include "nucsyn_elements.h"
/*
 * Calculate the recombination energy per gram
 * of the material given in X.
 */
double Pure_function nucsyn_recombination_energy_per_gram(
    struct stardata_t * Restrict const stardata,
    Abundance * Restrict const X
    )
{
    double E = 0.0;
    double M = 0.0;

    /*
     * Ionization energies from NIST
     * in eV.
     * http://physics.nist.gov/PhysRefData/ASD/ionEnergy.html
     *
@Misc{NIST_ASD,
author = {A.~Kramida and {Yu.~Ralchenko} and
J.~Reader and {and NIST ASD Team}},
HOWPUBLISHED = {{NIST Atomic Spectra Database
(ver. 5.3), [Online]. Available:
{\tt{http://physics.nist.gov/asd}} [2017, April 4].
National Institute of Standards and Technology,
Gaithersburg, MD.}},
year = {2015},
}
    */
    static const double Eion[] = {
        0 , // 0 : n
        13.5984 , // 1 : H
        79.0052 , // 2 : He
        203.486 , // 3 : Li
        399.149 , // 4 : Be
        670.981 , // 5 : B
        1030.11 , // 6 : C
        1486.06 , // 7 : N
        2043.84 , // 8 : O
        2715.89 , // 9 : F
        3511.7 , // 10 : Ne
        4420 , // 11 : Na
        5451.08 , // 12 : Mg
        6604.95 , // 13 : Al
        7888.06 , // 14 : Si
        9305.79 , // 15 : P
        10859.6 , // 16 : S
        12556.4 , // 17 : Cl
        14400.8 , // 18 : Ar
        16382.3 , // 19 : K
        18510.1 , // 20 : Ca
        20787.7 , // 21 : Sc
        23221.3 , // 22 : Ti
        25819.2 , // 23 : V
        28582 , // 24 : Cr
        31514.2 , // 25 : Mn
        33159.4 , // 26 : Fe
        34570.4 , // 27 : Co
        36071.9 , // 28 : Ni
        39240 , // 29 : Cu
        42581 , // 30 : Zn
        42182.5 , // 31 : Ga
        44886.8 , // 32 : Ge
        45300.9 , // 33 : As
        47473 , // 34 : Se
        50457.5 , // 35 : Br
        51832.4 , // 36 : Kr
        51471.4 , // 37 : Rb
        53590.1 , // 38 : Sr
        56477 , // 39 : Y
        59421.6 , // 40 : Zr
        63346.3 , // 41 : Nb
        66979.6 , // 42 : Mo
        67968.8 , // 43 : Tc
        66620.9 , // 44 : Ru
        70246.8 , // 45 : Rh
        73177.6 , // 46 : Pd
        76576.5 , // 47 : Ag
        80054 , // 48 : Cd
        81556.4 , // 49 : In
        84595.6 , // 50 : Sn
        85944.7 , // 51 : Sb
        89508.9 , // 52 : Te
        93145.5 , // 53 : I
        96863.3 , // 54 : Xe
        97153.2 , // 55 : Cs
        100839 , // 56 : Ba
        104592 , // 57 : La
        108412 , // 58 : Ce
        112069 , // 59 : Pr
        115817 , // 60 : Nd
        119738 , // 61 : Pm
        124296 , // 62 : Sm
        128673 , // 63 : Eu
        133134 , // 64 : Gd
        137692 , // 65 : Tb
        142339 , // 66 : Dy
        146576 , // 67 : Ho
        151372 , // 68 : Er
        156262 , // 69 : Tm
        161252 , // 70 : Yb
        166334 , // 71 : Lu
        171497 , // 72 : Hf
        180773 , // 73 : Ta
        220066 , // 74 : W
        187968 , // 75 : Re
        193699 , // 76 : Os
        199564 , // 77 : Ir
        205570 , // 78 : Pt
        211671 , // 79 : Au
        218003 , // 80 : Hg
        224431 , // 81 : Tl
        230781 , // 82 : Pb
        237244 , // 83 : Bi
        243123 , // 84 : Po
        246102 , // 85 : At
        252735 , // 86 : Rn
        259482 , // 87 : Fr
        266315 , // 88 : Ra
        273326 , // 89 : Ac
        280429 , // 90 : Th
        287731 , // 91 : Pa
        163353 , // 92 : U
        166987 , // 93 : Np
        171048 , // 94 : Pu
        175513 , // 95 : Am
        179986 , // 96 : Cm
        184654 , // 97 : Bk
        189432 , // 98 : Cf
        194331 , // 99 : Es
        199354 , // 100 : Fm
        914.98 , // 101 : Md
        1149.45 , // 102 : No
        1158.3 , // 103 : Lr
        1171.41 , // 104 : Rf
        1164.7 , // 105 : Db
        1169.7 , // 106 : Sg
        1161.2 , // 107 : Bh
        1145 , // 108 : Hs
        877.9 , // 109 : Mt
        752.9 , // 110 : Uun
        0 , // 111 : Uuu
        0 , // 112 : Uub
        0 , // 113 : Uut
        0 , // 114 : Uuq
        0 , // 115 : Uup
        0 , // 116 : Uuh
        0 , // 117 : Uus
        0  // 118 : Uuo
    };

    /*
     * Add up the total recombination energy
     * and the total mass.
     */
    Isotope_loop(i)
    {
#ifdef Xelse
        if(i!=Xelse)
#endif
        {
            /*
             * Three ways to calculate this:
             * 1) Z^2 formula (crude)
             * 2) Better formula summed over shells
             * 3) Use the NIST data (above)
             */
            //fac = Z * Z * 1.0/6.0 * RYDBERG_ENERGY;
            //fac = (1.0/6.0) * Z * (1.0 + Z) * (1.0 + 2.0 * Z) * RYDBERG_ENERGY;
            E += Eion[stardata->store->atomic_number[i]] * ELECTRON_VOLT * X[i] * stardata->store->imnuc[i];
            M += X[i];
        }
    }

    //printf("return E/M=%g\n",E/M);

    return  E / M;
}
#endif // NUCSYN
