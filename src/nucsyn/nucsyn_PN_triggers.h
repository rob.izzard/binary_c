#pragma once
#ifndef NUCSYN_PN_TRIGGERS_CODES_H
#define NUCSYN_PN_TRIGGERS_CODES_H

#include "nucsyn_PN_triggers.def"

/*
 * Construct PN codes
 */
#undef X
#define X(CODE,TEXT) CODE,
enum { NUCSYN_PN_TRIGGERS_CODES };

#undef X
#define X(CODE,TEXT) #CODE,
static char * NUCSYN_PN_TRIGGERS_code_macros[] Maybe_unused = { NUCSYN_PN_TRIGGERS_CODES };

#undef X
#define X(CODE,TEXT) TEXT,
static char * NUCSYN_PN_TRIGGERS_code_strings[] Maybe_unused = { NUCSYN_PN_TRIGGERS_CODES };

#define NUCSYN_PN_TRIGGERS_string(N) Array_string(NUCSYN_PN_TRIGGERS_code_strings,(N))

#undef X

#endif // NUCSYN_PN_TRIGGERS_CODES_H
