#pragma once
#ifndef NUCSYN_IA_ALGORITHMS_CODES_H
#define NUCSYN_IA_ALGORITHMS_CODES_H

#include "nucsyn_Ia_algorithms.def"

/*
 * Construct Ia MCh exploder types
 */
#undef X
#define X(CODE,VAL,TEXT) TYPE_IA_SUPERNOVA_ALGORITHM_##CODE = VAL,
enum { NUCSYN_IA_ALGORITHMS_CODES };

#undef X
#define X(CODE,VAL,TEXT) #CODE,
static char * nucsyn_IA_ALGORITHMS_code_macros[] Maybe_unused = { NUCSYN_IA_ALGORITHMS_CODES };

#undef X
#define X(CODE,VAL,TEXT) TEXT,
static char * nucsyn_IA_ALGORITHMS_code_strings[] Maybe_unused = { NUCSYN_IA_ALGORITHMS_CODES };

#define Nucsyn_IA_ALGORITHMS_string(N) Array_string(nucsyn_IA_ALGORITHMS_code_strings,(N))

#undef X

/*
 * Type Ia sub-Mch algorithms
 */
#define TYPE_IA_SUB_SUPERNOVA_ALGORITHM_LIVNE_ARNETT_1995 0


#endif // NUCSYN_IA_ALGORITHMS_CODES_H
