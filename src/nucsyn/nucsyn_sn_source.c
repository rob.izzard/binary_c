#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef NUCSYN
Yield_source Pure_function nucsyn_sn_source(struct star_t * Restrict const exploder)
{
    /*
     * Given a supernova type, return the nucsyn source ID
     */
    Yield_source src_id;
    switch(exploder->SN_type)
    {
    case SN_IA_He_Coal:
    case SN_IA_He:
        src_id = SOURCE_SNIa_He;
        break;
    case SN_IA_COWD_DDet:
        src_id = SOURCE_SNIa_COWD_DDet;
        break;
    case SN_IA_ONeWD_DDet:
        src_id = SOURCE_SNIa_ONeWD_DDet;
        break;
    case SN_IA_CHAND_Coal:
    case SN_IA_CHAND:
        src_id = SOURCE_SNIa_CHAND;
        break;
    case SN_AIC:
        src_id = SOURCE_SNAIC;
        break;
    case SN_GRB_COLLAPSAR:
    case SN_IBC:
        src_id = SOURCE_SNIbc;
        break;
    case SN_II:
        src_id = SOURCE_SNII;
        break;
    default:
        src_id = -1;
        break;
    }
    return src_id;
}
#endif //NUCSYN
