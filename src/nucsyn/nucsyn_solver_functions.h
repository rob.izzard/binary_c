#pragma once
#ifndef NUCSYN_SOLVER_FUNCTIONS_H
#define NUCSYN_SOLVER_FUNCTIONS_H

#include "nucsyn_solvers.def"

#undef X
/* function pointer array */
#define X(CODE,STRING,FUNC) FUNC,
nucsyn_solver_func (nucsyn_network_burn_functions[])    \
    Maybe_unused = { NUCSYN_SOLVERS_LIST };
#undef X

#endif // NUCSYN_SOLVER_FUNCTIONS_H
