#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef NUCSYN

void nucsyn_init_first_pulse(struct star_t * const newstar,
                             struct stardata_t * Restrict const stardata)
{
    Dprint("First pulse mc=%g ntp=%g ",
           newstar->core_mass[CORE_He],
           newstar->num_thermal_pulses);

    /*
     * set the free parameters that deal with AGB nucleosynthesis, use
     * current mass (i.e. mass at first pulse) and zams metallicity
     */
    nucsyn_set_tpagb_free_parameters(newstar->mass,
                                     stardata->common.metallicity,
                                     newstar,
                                     stardata);
    /*
     * Determine if we have to do second dredge up
     */
    double mcbagb=mcagbf(newstar->mass,
                         stardata->common.giant_branch_parameters);

    if(newstar->second_dredge_up==FALSE &&
       mcbagb>=0.8 &&
       mcbagb<stardata->common.max_mass_for_second_dredgeup)
    {
        nucsyn_set_post_2nd_dup_abunds(newstar->Xenv,
                                       newstar->mass,
                                       stardata->common.metallicity,
                                       mcagbf(newstar->mass,
                                              stardata->common.giant_branch_parameters),
                                       newstar,
                                       stardata);
    }

    /*
     * we need to say this here even if the star has not gone through 2nd
     * dredge up! This is so that when the core gets big, we don't
     * accidentally go through 2nd dredge up again and again! Things like
     * that tend to screw the HBB routines up ;)
     */
    newstar->second_dredge_up = TRUE;

    /*
     * Set the hbb shell to have the surface abundance
     */
    Copy_abundances(newstar->Xenv,
                    newstar->Xhbb);

    /*
     * set density and temperature to low values - esp. the temperature,
     * this sort of temperature (1000k!) will switch off the CNO cycle
     * quite (!) easily...
     */
    newstar->temp = 3.0;
    newstar->rho = 0.001;

#ifdef NUCSYN_HUGE_PULSE_CRISTALLO
    nucsyn_huge_pulse(star,stardata);
#endif

    newstar->time_first_pulse = newstar->age;
}
#endif /* NUCSYN */
