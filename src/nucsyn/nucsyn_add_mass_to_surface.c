#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef NUCSYN
void nucsyn_add_mass_to_surface(const Star_number k,
                                const double dm,
                                const Abundance * Restrict const X,
                                double * Restrict const ndmacc,
                                Abundance ** Restrict newXacc)
{
    /*
     * Add material to the surface of a star by putting it
     * in its accretion layer
     */
    if(ndmacc[k]>VERY_TINY)
    {
        /* Existing accretion layer : have to mix */
        nucsyn_dilute_shell(
            ndmacc[k],newXacc[k],
            dm,X
            );
    }
    else
    {
        /* new accretion layer */
        Copy_abundances(X,newXacc[k]);
    }
    ndmacc[k] += dm;
}
#endif//NUCSYN
