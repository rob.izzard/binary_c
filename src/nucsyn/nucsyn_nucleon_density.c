#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef NUCSYN

/*
 * Return the density of nucleons
 */

double Pure_function nucsyn_nucleon_density(
#ifdef NANCHECKS
    struct stardata_t * const stardata,
#endif
    const Abundance * const N, /* number density array */
    const int * const A /* nucleon number array */
#ifdef NANCHECKS
    ,const Boolean inout Maybe_unused
#endif
    )
{
    double nsum[NUCSYN_STRIDE_N] = {0.0};
    Nucsyn_isotope_stride_leftarray_loop(nsum, += N[i] ? (N[i] * (double)(A[i])) : 0.0);
    return Nucsyn_stride_sum(nsum);
}
#endif//NUCSYN
