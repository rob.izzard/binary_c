#include "../binary_c.h"
No_empty_translation_unit_warning;


#if defined NUCSYN && defined NUCSYN_SECOND_DREDGE_UP

void nucsyn_check_for_second_dredge_up(struct stardata_t * const stardata,
                                       struct star_t * const newstar,
                                       const double mcbagb)
{
    double mc1tp = Karakas2002_mc1tp(newstar->mass,
                                     newstar->phase_start_mass,
                                     stardata);

    if(newstar->second_dredge_up==FALSE &&
       AGB(newstar->stellar_type) &&
       mcbagb > mc1tp && 
       mcbagb < stardata->common.max_mass_for_second_dredgeup)
    {
        Dprint("Set post 2nd dup abunds...");
        /* set the second dredge up abundances */
        nucsyn_set_post_2nd_dup_abunds(newstar->Xenv,
                                       newstar->phase_start_mass,
                                       stardata->common.metallicity,
                                       mcbagb,
                                       newstar,
                                       stardata);
    }
}

#endif // NUCSYN && NUCSYN_SECOND_DREDGE_UP
