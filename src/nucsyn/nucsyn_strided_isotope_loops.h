#pragma once
#ifndef NUCSYN_ISOTOPE_LOOPS_H
#define NUCSYN_ISOTOPE_LOOPS_H



#define _expand(...) __VA_ARGS__
#define _separate(...) _expand(__VA_ARGS__)

#ifdef NUCSYN_STRIDED_ISOTOPE_LOOPS
/*
 * Strided loops over isotopes
 *
 * We set the stride to NUCSYN_STRIDE_N
 * which is by trial and error.
 */
#define NUCSYN_STRIDE_N 16
#define _stride_max Min(ISOTOPE_ARRAY_SIZE,((ISOTOPE_ARRAY_SIZE/16) * 16 + XH1))

/*
 * Simple loop over the isotopes.
 *
 * This just applies the varargs given to each item.
 */
#define Nucsyn_isotope_stride_loop(...)                 \
    {                                                   \
        { Isotope i=0;_separate(__VA_ARGS__); }         \
        /* skip Xe == 1 (electrons) */                  \
        PRAGMA_GCC_IVDEP                                \
        for(Isotope i=XH1;i<_stride_max;)               \
        {                                               \
            _separate(__VA_ARGS__); i++;                \
            _separate(__VA_ARGS__); i++;                \
            _separate(__VA_ARGS__); i++;                \
            _separate(__VA_ARGS__); i++;                \
            _separate(__VA_ARGS__); i++;                \
            _separate(__VA_ARGS__); i++;                \
            _separate(__VA_ARGS__); i++;                \
            _separate(__VA_ARGS__); i++;                \
            _separate(__VA_ARGS__); i++;                \
            _separate(__VA_ARGS__); i++;                \
            _separate(__VA_ARGS__); i++;                \
            _separate(__VA_ARGS__); i++;                \
            _separate(__VA_ARGS__); i++;                \
            _separate(__VA_ARGS__); i++;                \
            _separate(__VA_ARGS__); i++;                \
            _separate(__VA_ARGS__); i++;                \
        }                                               \
        for(Isotope i=_stride_max;i<ISOTOPE_ARRAY_SIZE;i++) \
        {                                               \
            _separate(__VA_ARGS__);                     \
        }                                               \
    }

/*
 * Strided loop to sum something over the isotope array.
 * For example, to sum the abundances:
 *
 *    Abundance Xsum[NUCSYN_STRIDE_N] = {0.0};
 *    Nucsyn_isotope_stride_leftarray_loop(Xsum, += X[i]);
 *    return Nucsyn_stride_sum(Xsum);
 */
#define Nucsyn_isotope_stride_leftarray_loop(ARRAY,...) \
    {                                                   \
        { Isotope i=0;ARRAY[0] _separate(__VA_ARGS__);} \
        /* skip Isotope 1 == Xe (electrons) */          \
        PRAGMA_GCC_IVDEP                                \
        for(Isotope i=XH1;i<_stride_max;)               \
        {                                               \
            ARRAY[0] _separate(__VA_ARGS__); i++;       \
            ARRAY[1] _separate(__VA_ARGS__); i++;       \
            ARRAY[2] _separate(__VA_ARGS__); i++;       \
            ARRAY[3] _separate(__VA_ARGS__); i++;       \
            ARRAY[4] _separate(__VA_ARGS__); i++;       \
            ARRAY[5] _separate(__VA_ARGS__); i++;       \
            ARRAY[6] _separate(__VA_ARGS__); i++;       \
            ARRAY[7] _separate(__VA_ARGS__); i++;       \
            ARRAY[8] _separate(__VA_ARGS__); i++;       \
            ARRAY[9] _separate(__VA_ARGS__); i++;       \
            ARRAY[10] _separate(__VA_ARGS__); i++;      \
            ARRAY[11] _separate(__VA_ARGS__); i++;      \
            ARRAY[12] _separate(__VA_ARGS__); i++;      \
            ARRAY[13] _separate(__VA_ARGS__); i++;      \
            ARRAY[14] _separate(__VA_ARGS__); i++;      \
            ARRAY[15] _separate(__VA_ARGS__); i++;      \
        }                                               \
        for(Isotope i=_stride_max;i<ISOTOPE_ARRAY_SIZE;i++)    \
        {                                               \
            ARRAY[0] _separate(__VA_ARGS__);            \
        }                                               \
    }

#define Nucsyn_stride_sum(Xsum)                                         \
    (Xsum[0] + Xsum[1] + Xsum[2] + Xsum[3] + Xsum[4] + Xsum[5] + Xsum[6] + Xsum[7] + Xsum[8] + Xsum[9] + Xsum[10] + Xsum[11] + Xsum[12] + Xsum[13] + Xsum[14] + Xsum[15])

#else

/*
 * Linear versions of the above
 */
#define NUCSYN_STRIDE_N 1

#define Nucsyn_isotope_stride_loop(...)                 \
    {                                                   \
        Isotope_loop(i)                                 \
        {                                               \
            _separate(__VA_ARGS__);                     \
        }                                               \
    }

#define Nucsyn_isotope_stride_leftarray_loop(ARRAY,...) \
    {                                                   \
        Isotope_loop(i)                                 \
        {                                               \
            ARRAY[0] _separate(__VA_ARGS__);            \
        }                                               \
    }

#define Nucsyn_stride_sum(Xsum) (Xsum[0])

#endif // NUCSYN_STRIDED_ISOTOPE_LOOPS



#endif // NUCSYN_ISOTOPE_LOOPS_H
