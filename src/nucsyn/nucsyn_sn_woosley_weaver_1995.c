#include "../binary_c.h"

No_empty_translation_unit_warning;


#ifdef NUCSYN
#define SN_DEBUG

void nucsyn_sn_woosley_weaver_1995(struct stardata_t * const stardata,
                                   Abundance * const X,
                                   double mco,
                                   Abundance z)
{
    /*
     * Woosley & Weaver 1995 SN yields, corrected by Portinari's method,
     * as mass fractions ejected from the CO core
     */
    char * filename;
    struct data_table_t * table;
    struct data_table_t ** table_pointer;
    if(stardata->persistent_data->cc_sn_tables == NULL)
    {
        stardata->persistent_data->cc_sn_tables = Calloc(NUCSYN_CCSN_NUMBER,
                                                  sizeof(struct data_table_t *));
    }

    if(stardata->preferences->core_collapse_supernova_algorithm == NUCSYN_CCSN_WOOSLEY_WEAVER_1995A)
    {
        table = stardata->persistent_data->cc_sn_tables[NUCSYN_CCSN_WOOSLEY_WEAVER_1995A];
        table_pointer = &stardata->persistent_data->cc_sn_tables[NUCSYN_CCSN_WOOSLEY_WEAVER_1995A];
        filename = "src/nucsyn/WW95_Amodels.dat";
        Limit_range(mco,1.35,16.37);
    }
    else if(stardata->preferences->core_collapse_supernova_algorithm == NUCSYN_CCSN_WOOSLEY_WEAVER_1995B)
    {
        table = stardata->persistent_data->cc_sn_tables[NUCSYN_CCSN_WOOSLEY_WEAVER_1995B];
        table_pointer = &stardata->persistent_data->cc_sn_tables[NUCSYN_CCSN_WOOSLEY_WEAVER_1995B];
        filename = "src/nucsyn/WW95_Bmodels.dat";
        Limit_range(mco,1.68,15.4082);
    }
    else if(stardata->preferences->core_collapse_supernova_algorithm == NUCSYN_CCSN_WOOSLEY_WEAVER_1995C)
    {
        table = stardata->persistent_data->cc_sn_tables[NUCSYN_CCSN_WOOSLEY_WEAVER_1995C];
        table_pointer = &stardata->persistent_data->cc_sn_tables[NUCSYN_CCSN_WOOSLEY_WEAVER_1995C];
        filename = "src/nucsyn/WW95_Cmodels.dat";
        Limit_range(mco,1.68,15.4082);
    }
    else
    {
        table = NULL;
        filename = NULL;
    }

    if(filename == NULL)
    {
        return;
    }

    if(table == NULL)
    {
        /*
         * Load the data into the table
         */
        size_t maxwidth;
        size_t nlines;
        double * const data = load_data_as_doubles(stardata,
                                        filename,
                                        &nlines,
                                        NULL,
                                        &maxwidth,
                                        ' ',
                                        0);
        NewDataTable_from_Pointer(data,
                                  table,
                                  2,
                                  maxwidth-2,
                                  nlines);
        *table_pointer = table;
    }

    Limit_range(z,0.0,0.02);

#ifdef SN_DEBUG
    printf("Table interpolation : Z=%g MCO=%g\n",z,mco);
#endif

    /*
     * Interpolate for yields
     */
    Clear_isotope_array(X);
    const double x[2] = {z, mco};
    double * y = Calloc(table->ndata,sizeof(double));

#ifdef SN_DEBUG
    printf("Interpolate...\n");fflush(stdout);
#endif
    Interpolate(table,
                x,
                y,
                TRUE);
#ifdef SN_DEBUG
    printf("Interpolate complete\n");fflush(stdout);
#endif

    /*
     * Map WW95 isotopes to binary_c isotopes
     */
    X[XH1]+=y[1]; X[XH2]+=y[2]; X[XHe3]+=y[3]; X[XHe4]+=y[4]; X[XBe7]+=y[5]; X[XLi7]+=y[6]; X[XBe9]+=y[7]; X[XB10]+=y[8]; X[XB11]+=y[9]; X[XC11]+=y[10]; X[XC12]+=y[11]; X[XC13]+=y[12]; X[XN13]+=y[13]; X[XC14]+=y[14]; X[XN14]+=y[15]; X[XN15]+=y[16]; X[XO16]+=y[17]; X[XO17]+=y[18]; X[XO18]+=y[19]; X[XF19]+=y[20]; X[XNe20]+=y[21]; X[XNe21]+=y[22]; X[XNa22]+=y[23]; X[XNe22]+=y[24]; X[XNa23]+=y[25]; X[XMg24]+=y[26]; X[XNa24]+=y[27]; X[XMg25]+=y[28]; X[XAl26]+=y[29]; X[XMg26]+=y[30]; X[XAl27]+=y[31]; X[XAl28]+=y[32]; X[XSi28]+=y[33]; X[XSi29]+=y[34]; X[XSi30]+=y[35]; X[XP31]+=y[36]; X[XS32]+=y[37]; X[XS33]+=y[38]; X[XS34]+=y[39]; X[XCl35]+=y[40]; X[XS35]+=y[41]; X[XAr36]+=y[42]; X[XCl36]+=y[43]; X[XS36]+=y[44]; X[XAr37]+=y[45]; X[XCl37]+=y[46]; X[XAr38]+=y[47]; X[XK39]+=y[48]; X[XAr40]+=y[49]; X[XCa40]+=y[50]; X[XK40]+=y[51]; X[XCa41]+=y[52]; X[XK41]+=y[53]; X[XCa42]+=y[54]; X[XCa43]+=y[55]; X[XSc43]+=y[56]; X[XCa44]+=y[57]; X[XTi44]+=y[58]; X[XCa45]+=y[59]; X[XSc45]+=y[60]; X[XTi45]+=y[61]; X[XCa46]+=y[62]; X[XTi46]+=y[63]; X[XCa47]+=y[64]; X[XTi47]+=y[65]; X[XV47]+=y[66]; X[XCa48]+=y[67]; X[XCr48]+=y[68]; X[XTi48]+=y[69]; X[XV48]+=y[70]; X[XCr49]+=y[71]; X[XTi49]+=y[72]; X[XV49]+=y[73]; X[XCr50]+=y[74]; X[XTi50]+=y[75]; X[XV50]+=y[76]; X[XCr51]+=y[77]; X[XMn51]+=y[78]; X[XV51]+=y[79]; X[XCr52]+=y[80]; X[XFe52]+=y[81]; X[XMn52]+=y[82]; X[XCr53]+=y[83]; X[XFe53]+=y[84]; X[XMn53]+=y[85]; X[XCr54]+=y[86]; X[XFe54]+=y[87]; X[XMn54]+=y[88]; X[XCo55]+=y[89]; X[XFe55]+=y[90]; X[XMn55]+=y[91]; X[XCo56]+=y[92]; X[XFe56]+=y[93]; X[XNi56]+=y[94]; X[XCo57]+=y[95]; X[XFe57]+=y[96]; X[XNi57]+=y[97]; X[XCo58]+=y[98]; X[XFe58]+=y[99]; X[XNi58]+=y[100]; X[XCo59]+=y[101]; X[XCu59]+=y[102]; X[XFe59]+=y[103]; X[XNi59]+=y[104]; X[XCo60]+=y[105]; X[XCu60]+=y[106]; X[XFe60]+=y[107]; X[XNi60]+=y[108]; X[XZn60]+=y[109]; X[XCo61]+=y[110]; X[XCu61]+=y[111]; X[XNi61]+=y[112]; X[XZn61]+=y[113]; X[XCu62]+=y[114]; X[XNi62]+=y[115]; X[XZn62]+=y[116]; X[XCu63]+=y[117]; X[XNi63]+=y[118]; X[XZn63]+=y[119]; X[XCu64]+=y[120]; X[XGa64]+=y[121]; X[XGe64]+=y[122]; X[XNi64]+=y[123]; X[XZn64]+=y[124]; X[XCu65]+=y[125]; X[XGa65]+=y[126]; X[XGe65]+=y[127]; X[XNi65]+=y[128]; X[XZn65]+=y[129]; X[XCu66]+=y[130]; X[XGa66]+=y[131]; X[XGe66]+=y[132]; X[XZn66]+=y[133]; X[XGa67]+=y[134]; X[XGe67]+=y[135]; X[XZn67]+=y[136]; X[XGa68]+=y[137]; X[XGe68]+=y[138]; X[XZn68]+=y[139]; X[XGa69]+=y[140]; X[XGe69]+=y[141]; X[XZn69]+=y[142]; X[XGa70]+=y[143]; X[XGe70]+=y[144]; X[XGe71]+=y[145];

    /*
     * Normalize so the sum is 1.0
     */
    nucsyn_renormalize_abundance(X);

#ifdef SN_DEBUG
    printf("Mass fractions ejected: H1=%g He4=%g C12=%g N14=%g O16=%g Fe56=%g Ni56=%g : yield sum %g\n",
           X[XH1],X[XHe4],X[XC12],X[XN14],X[XO16],X[XFe56],X[XNi56],nucsyn_totalX(X));
#endif//SN_DEBUG

    /*
     * Free and return
     */
    Safe_free(y);
}


#endif // NUCSYN
