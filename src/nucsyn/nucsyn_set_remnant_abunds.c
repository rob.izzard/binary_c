#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef NUCSYN

void Nonnull_some_arguments(1) nucsyn_set_remnant_abunds(Abundance * Restrict const Xenv)
{
    /*
     * Function to set the abundance of a NS, BH
     * or MASSLESS_REMNANT
     *
     * First, assume there is nothing on the surface of a
     * NS, BH or MASSLESS_REMNANT
     */
    Clear_isotope_array(Xenv);

    /*
     * And set everything to neutrons
     */
    Xenv[Xn] = 1.0;
}

#endif // NUCSYN
