/* function to set the free parameters involved in TPAGB nucleosynthesis */
#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef NUCSYN

#define NEW_CALIBRATION

/* This file contains the tabular fit to  the free parameters */
#include "nucsyn_tpagb_free_parameters2.h"

void nucsyn_set_tpagb_free_parameters(double m,
                                      Abundance z,
                                      struct star_t * const star,
                                      struct stardata_t * const stardata)
{


#ifdef NEW_CALIBRATION
    double x[2],r[4];
    Const_data_table table[]={NEW_CALIBRATION_TABLE2};
#else  //NEW_CALIBRATION

    /*
     * might as well set a cap on m at 6.5, becuase 1) we don't fit above it 2)
     * above 6.5 all the stars act the same as ~6.0
     */
    m=Min(6.5,m);
    /* sim for z */
    z=Max(0.004,Min(0.02,z));
#endif  //NEW_CALIBRATION

#ifndef NEW_CALIBRATION
    /* f_burn */
    star->f_burn=Max(((4.41740e+00)*z+(8.33810e-01))/(1+( pow((((-6.96710e+03)*z+(1.43620e+02))),((9.61550e+01)*z+(3.74660e+00)-m) ))),0.0);
    /* f_HBB */
    star->f_hbb=Max(((-1.08520e+01)*z+(9.30680e-01))/(1+( pow((((-1.07240e+03)*z+(4.09970e+01))),((6.54380e+01)*z+(3.91810e+00)-m)))),0.0);

    /*
     * this might go negative for large mass at low Z, so make sure it's always
     * > 0 otherwise the exponentials will not be happy!
     * was global_temp_rise_fac
     */
    star->temp_rise_fac=Max(0.51*m*m-6.6*m+169*z-5.8e3*z*z+21,TINY);
#endif // NEW_CALIBRATION

#ifdef NEW_CALIBRATION
    /*
     * Use the table to fix up the values of the interpolation
     */
    if(m>=2.0)
    {
        x[0]=Max(2.0,Min(6.5,m));
        x[1]=Max(0.0001,Min(0.02,z));
        rinterpolate(table,
                     stardata->persistent_data->rinterpolate_data,
                     2,
                     4,
                     NEW_CALIBRATION_TABLE2_LINES,
                     x,
                     r,
                     1);
        star->temp_rise_fac=r[0];
        star->f_burn=r[1];
        star->f_hbb=r[2];
        star->temp_mult=r[3];
        Dprint("New calibration from %g %g -> %g %g %g %g\n",x[0],x[1],r[0],r[1],r[2],r[3]);

    }

    // define SHOWTABLE to output a table of parameters
#undef SHOWTABLE
#ifdef SHOWTABLE
    for(z=0.004;z<=0.02001;z+=0.004)
    {
        if(Fequal(z,0.02)||Fequal(z,0.008)||Fequal(z,0.004))
        {
            for(m=1.0;m<=6.5001;m+=0.5)
            {
                /* f_burn */
                star->f_burn=Max(((4.41740e+00)*z+(8.33810e-01))/(1+( pow((((-6.96710e+03)*z+(1.43620e+02))),((9.61550e+01)*z+(3.74660e+00)-m) ))),0.0);
                /* f_HBB */
                star->f_hbb=Max(((-1.08520e+01)*z+(9.30680e-01))/(1+( pow((((-1.07240e+03)*z+(4.09970e+01))),((6.54380e+01)*z+(3.91810e+00)-m)))),0.0);

                /*
                 * this might go negative for large mass at low Z, so make sure it's always
                 * > 0 otherwise the exponentials will not be happy!
                 * was global_temp_rise_fac
                 */
                star->temp_rise_fac=Max(0.51*m*m-6.6*m+169*z-5.8e3*z*z+21,TINY);
                //printf("FREEPARAMS %g %g %g %g %g\n",z,m,star->f_burn,star->f_hbb,star->temp_rise_fac);
            }
        }
    }
    Exit_binary_c(BINARY_C_NORMAL_EXIT,"");
#endif//SHOWTABLE
#endif

    star->fmdupburn=0.0;

    // no DUP burning by default
    star->ftimeduphbb=0.0;

    Dprint("(Automatically M=%g Z=%g) Set AGB free parameters to ftimehbb %g, fmenvhbb %g, temp rise fac %g\n",
           m,z,
           star->f_burn,
           star->f_hbb,
           star->temp_rise_fac);
}



#endif /* NUCSYN */
