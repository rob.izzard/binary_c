#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef NUCSYN



static void nucsyn_check_abundance_array_oops(struct stardata_t * Restrict const stardata,
                                              const Abundance * const Restrict X,
                                              const Isotope i,
                                              const char *const s) ;

void nucsyn_check_abundance_array(struct stardata_t * Restrict const stardata,
                                  const Abundance * Restrict const X,
                                  const char * Restrict const s)
{

    /*
     * Generic function to check an abundance array for errors
     */
    const double xsum=nucsyn_totalX(X);

    Dprint("%s Check abundance array: sum %g\n",s,xsum);

    /* 10% threshold? */
    if(xsum>1.1 || xsum<0.9)
    {
        Exit_binary_c(BINARY_C_OUT_OF_RANGE,"X sum = %g error\n",xsum);
    }

    /*
     * check for nans, <0
     */
    Isotope_loop(i)
    {
        if(
#ifdef NANCHECKS
            isnan(X[i]) !=0 ||
#endif
            X[i] < 0.0)
        {
            nucsyn_check_abundance_array_oops(stardata,X,i,s);
        }
    }
}

static void nucsyn_check_abundance_array_oops(struct stardata_t * Restrict const stardata,
                                              const Abundance * Restrict const X,
                                              const Isotope i,
                                              const char * const s)
{
    Exit_binary_c(BINARY_C_OUT_OF_RANGE,
                  "X[%u] = %g : error : %s\nX : H1=%g He4=%g C12=%g N14=%g O16=%g Fe56=%g\n",
                  i,X[i],s,X[XH1],X[XHe4],X[XC12],X[XN14],X[XO16],X[XFe56]);
}

#endif
