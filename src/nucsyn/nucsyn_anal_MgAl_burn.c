#include "../binary_c.h"
No_empty_translation_unit_warning;

#if (defined NUCSYN && defined NUCSYN_ANAL_BURN)

/* MgAl "cycle" burning */

void nucsyn_anal_MgAl_burn(struct stardata_t * stardata,
                           double * Restrict t,
                           Number_density * Restrict Nin,
                           const double dtseconds)
{
    Number_density mgalsum; // sum of MgAl isotopes
    /* Nin is I,Mg24,Mg25,Mg26,Al26 and Al27 and is a 1-based array */
    /* where I is a source term, possibly from Na23(p,gamma)Mg24    */
    Number_density I=Nin[0]; // source term in integrals
    Number_density Nout[6]; // output
    /*  t is an array of timescales where
     *  t[0]=t24 is for Mg24(p,gamma)Al25
     *  t[1]=t25 is for Mg25(p,gamma)Al26
     *  t[2]=t26p is for Al26g(p,gamma)Si27
     *  t[3]=t26b is for Al26(beta+)Mg26
     *  t[4]=t26 is for Mg26(p,gamma)Si27
     *  Si27 is then a sink for the MgAl process isotopes
     */

    Dprint("Timescales, fraction of burntime, estimate\nburntime : %g\nMg24(p,gamma)Al25 : %g %g %g\nMg25(p,gamma)Al26 : %g %g %g\nAl26(p,gamma)Si27 : %g %g %g\nAl26(beta+)Mg26   : %g %g %g\nMg26(p,gamma)Si27 : %g %g %g\n",
           dtseconds,
           t[0],t[0]/dtseconds,(exp(-dtseconds/t[0])),
           t[1],t[1]/dtseconds,(exp(-dtseconds/t[1])),
           t[2],t[2]/dtseconds,(exp(-dtseconds/t[2])),
           t[3],t[3]/dtseconds,(exp(-dtseconds/t[3])),
           t[4],t[4]/dtseconds,(exp(-dtseconds/t[4])));

    double t24=t[0],t25=t[1],t26p=t[2],t26b=t[3],t26=t[4];
    double invt24=1.0/t24,invt25=1.0/t25,invt26=1.0/t26;
    double i24z=exp(-dtseconds/t24),i25z=exp(-dtseconds/t25),i26z=exp(-dtseconds*invt26),ii;
#ifdef NUCSYN_Al26m
    double t25g=t[5],t25m=t[6];
    double invt25g=1.0/t25g,invt25m=1.0/t25m;
    double i25gz=exp(-dtseconds/t25g),i25mz=exp(-dtseconds/t25m);
#endif
    // integration constants
    double a24,b24;
    double a24p,b24p;
    double a25,b25,c25;
    double a25p,b25p,c25p;
    double a26,b26,c26,d26,f26,g26,h26,i26,naccretor6;
    double a26p,b26p,c26p,d26p,f26p,g26p,h26p,i26p,naccretor6p;
    double a26s,b26s,c26s,d26s;
    double a27,b27,c27,d27,f27;
    double tauprime,itauprime;
    double x;

    double realin[6];
    realin[0]=Nin[0];
    realin[1]=Nin[1];
    realin[2]=Nin[2];
    realin[3]=Nin[3];
    realin[4]=Nin[4];
    realin[5]=Nin[5];


    /* set sum of MgAl isotopes */
    mgalsum=Nin[1]+Nin[2]+Nin[3]+Nin[4]+Nin[5];

    /* we cannot burn nothing... */
    if(mgalsum<TINY) return;


    Dprint("\nMgAl burn\n");
    Dprint("Mgal mgalsum=%g + source I=%g\n",mgalsum,I);
    Dprint("             %10s %10s %10s %10s %10s\n","Mg24","Mg25","Mg26","Al26","Al27");
    Dprint("Abunds in : %10.5g %10.5g %10.5g %10.5g %10.5g\n",Nin[1],Nin[2],Nin[3],Nin[4],Nin[5]);
    Dprint("Numb fracs: %10.5g %10.5g %10.5g %10.5g %10.5g\n",Nin[1]/mgalsum,Nin[2]/mgalsum,
           Nin[3]/mgalsum,Nin[4]/mgalsum,Nin[5]/mgalsum);
    Dprint("Timescales: %10.5g %10.5g p%10.5g b%10.5g %10.5g\n",t[0],t[1],t[2],t[3],t[4]);
    Dprint("   /years : %10.5g %10.5g %10.5g %10.5g %10.5g\n",
           t[0]/YEAR_LENGTH_IN_SECONDS,
           t[1]/YEAR_LENGTH_IN_SECONDS,
           t[2]/YEAR_LENGTH_IN_SECONDS,
           t[3]/YEAR_LENGTH_IN_SECONDS,
           t[4]/YEAR_LENGTH_IN_SECONDS);
    Dprint("Burn time : %10.5g s\n",dtseconds);

    /***************************************************/
    /* do the burn */

    /* Mg24 is created from the source term and destroyed by the
     * Mg24(p,gamma)Al25(beta)Mg25 reaction where the slowest point
     * in the chain is the proton capture */
    a24=t24*I;
    b24=Nin[1]-t24*I;
    Nout[1]=a24+b24*i24z;

    Dprint("Calc Mg24 from a24=%g b24=%g dtseconds/t24=%g exp(-prev)=%g\n",
           a24,b24,dtseconds/t24,i24z);

    /* Mg25 is created above and destroyed by Mg25(p,gamma)Al26 */

    a24p=a24*invt24;
    b24p=b24*invt24;

    a25=t25*a24p;
    b25=b24p/(invt25-invt24);
    c25=Nin[2]-a25-b25;

    Nout[2]=a25+b25*i24z+c25*i25z;

    /* Al26 (Nout[4]!!!) is created above and destroyed by either proton capture to Si27
     * or beta decay to Mg26 */
    x=1.0/t26b;

#ifdef NUCSYN_Al26m
    a25p=a25*invt25g;
    b25p=b25*invt25g;
    c25p=c25*invt25g;
#else
    a25p=a25*invt25;
    b25p=b25*invt25;
    c25p=c25*invt25;
#endif

    tauprime=1.0/(1.0/t26p+x);
    itauprime=1.0/tauprime;
    a26=tauprime*a25p;
    b26=b25p/(itauprime-invt24);
    c26=c25p/(itauprime-invt25);
    d26=Nin[4]-a26-b26-c26;
    ii=-dtseconds*itauprime;
    Nout[4]=a26+b26*i24z+c26*i25z+d26*exp(ii);

    /* Mg26 (Nout[3]) is produced by beta decay of Al26 */

    a26p=a26*x;
    b26p=b26*x;
    c26p=c26*x;
    d26p=d26*x;
#ifdef NUCSYN_Al26m
    a26p+=a25*invt25m;
    b26p+=b25*invt25m;
    c26p+=c25*invt25m;
#endif
    f26=t26*a26p;
    g26=b26p/(invt26-invt24);
    h26=c26p/(invt26-invt25);
    i26=d26p/(invt26-itauprime);
    naccretor6=Nin[3]-f26-g26-h26-i26;

    Nout[3]=f26+g26*i24z+h26*i25z+i26*exp(ii)+naccretor6*i26z;

    f26p=f26*invt26;
    g26p=g26*invt26;
    h26p=h26*invt26;
    i26p=i26*invt26;
    naccretor6p=naccretor6*invt26;
    x=1.0/t26p;
    a26s=a26*x;
    b26s=b26*x;
    c26s=c26*x;
    d26s=d26*x;

    a27=f26p+a26s;
    b27=g26p+b26s;
    c27=h26p+c26s;
    d27=naccretor6p;
    f27=i26p+d26s;

    /* Al27 (Nout[5]) is then produced by Al26 (via Si27 decay) and proton capture on Mg26  */
    Nout[5]=a27*dtseconds-
        (b27*t24*i24z+c27*t25*i25z+d27*t26*i26z+
         f27*tauprime*exp(ii))+
        Nin[5]+(b27*t24+c27*t25+d27*t26+f27*tauprime);

    Nin[1]=Nout[1];
    Nin[2]=Nout[2];
    Nin[3]=Nout[3];
    Nin[4]=Nout[4];
    Nin[5]=Nout[5];

    /***************************************************/


    Dprint("Abunds out: %10.5g %10.5g %10.5g %10.5g %10.5g\n",Nin[1],Nin[2],Nin[3],Nin[4],Nin[5]);
    Dprint("Fracs out/in: %10.5g %10.5g %10.5g %10.5g %10.5g\n",Nin[1]/realin[1],Nin[2]/realin[2],Nin[3]/realin[3],Nin[4]/realin[4],Nin[5]/realin[5]);
    Dprint("Numb fracs: %10.5g %10.5g %10.5g %10.5g %10.5g\n",Nin[1]/mgalsum,Nin[2]/mgalsum,
           Nin[3]/mgalsum,Nin[4]/mgalsum,Nin[5]/mgalsum);
    Dprint("Sum in/out = %g\n",
           (Nin[1]+Nin[2]+Nin[3]+Nin[4]+Nin[5])/mgalsum);

    if(((Nin[1]+Nin[2]+Nin[3]+Nin[4]+Nin[5])/mgalsum > 1.01)||
       ((Nin[1]+Nin[2]+Nin[3]+Nin[4]+Nin[5])/mgalsum < 0.99))
    {
        Exit_binary_c(BINARY_C_OUT_OF_RANGE,
                      "MgAl not conserved error! %2.2f\n",
                      (Nin[1]+Nin[2]+Nin[3]+Nin[4]+Nin[5])/mgalsum);
    }
    return;
}
#endif
