#ifdef NUCSYN
/*
 * New reaction matrix
 *
 * timescale[i][j] where i=row j=column
 *
 * corresponds to a term
 *
 * d/dt X_i = X_j / timescale[i][j]
 *
 * e.g.
 *
 * timescale_matrix[i][i] = -Inverse_nuclear_burning_timescale(SIGMAV_...) -> destruction of i due to p-cap
 * timescale_matrix[i][j] = +Inverse_nuclear_burning_timescale(SIGMAV_...) -> creation of i from j
 *
 * NB for beta decays, the timescale is not required
 */


// C12(pg)N13(beta+)C13
timescale_matrix[XC12][XC12]=-Inverse_nuclear_burning_timescale(SIGMAV_T12);
timescale_matrix[XC13][XC12]=Inverse_nuclear_burning_timescale(SIGMAV_T12);

// N13(pg)O14(beta)N14 assume N13 decays instantly
//timescale_matrix[XN13][XN13]=-Inverse_nuclear_burning_timescale(SIGMAV_TPN13);
//timescale_matrix[XN13][XN14]=Inverse_nuclear_burning_timescale(SIGMAV_TPN13);

// N13(beta+)C13 assume instant
//timescale_matrix[XN13][XN13]+=-1.0/(SIGMAV_TBETA13*YEAR_LENGTH_IN_SECONDS);
//timescale_matrix[XN13][XC13]=1.0/(SIGMAV_TBETA13*YEAR_LENGTH_IN_SECONDS);

// C13(pg)N14
timescale_matrix[XC13][XC13]=-Inverse_nuclear_burning_timescale(SIGMAV_T13);
timescale_matrix[XN14][XC13]=Inverse_nuclear_burning_timescale(SIGMAV_T13);

// N14(pg)O15(beta-)N15
timescale_matrix[XN14][XN14]=-Inverse_nuclear_burning_timescale(SIGMAV_T14);
//timescale_matrix[XC12][XN14]=Inverse_nuclear_burning_timescale(SIGMAV_T14);// close CN
timescale_matrix[XN15][XN14]=Inverse_nuclear_burning_timescale(SIGMAV_T14);


// O15(beta-)N15 : assume instant
//timescale_matrix[XO15][XO15]=-1.0/(SIGMAV_TBETA15*YEAR_LENGTH_IN_SECONDS);
//timescale_matrix[XN15][XO15]=1.0/(SIGMAV_TBETA15*YEAR_LENGTH_IN_SECONDS);

// O15(pg)F16(beta+)O16 not in compilation
//timescale_matrix[XO15][XO15]+=-Inverse_nuclear_burning_timescale(SIGMAV_T15P);
//timescale_matrix[XO15][XO16]=Inverse_nuclear_burning_timescale(SIGMAV_T15P);

// N15(pa)C12
timescale_matrix[XN15][XN15]=-Inverse_nuclear_burning_timescale(SIGMAV_T15);
timescale_matrix[XC12][XN15]=Inverse_nuclear_burning_timescale(SIGMAV_T15);

// N15(pg)O16
timescale_matrix[XN15][XN15]+=-Inverse_nuclear_burning_timescale(SIGMAV_T15_BRANCH);
timescale_matrix[XO16][XN15]=Inverse_nuclear_burning_timescale(SIGMAV_T15_BRANCH);

// O16(pg)F17(beta)O17
timescale_matrix[XO16][XO16]=-Inverse_nuclear_burning_timescale(SIGMAV_T16);
timescale_matrix[XO17][XO16]=Inverse_nuclear_burning_timescale(SIGMAV_T16);

// F17(pg)Ne18(beta)O18
//timescale_matrix[XF17][XF17]=-Inverse_nuclear_burning_timescale(SIGMAV_TF17);
//timescale_matrix[XF17][XO18]=Inverse_nuclear_burning_timescale(SIGMAV_TF17);

// F17(beta)O17 assume instant
//timescale_matrix[XF17][XF17]+=-1.0/(SIGMAV_TBETA17*YEAR_LENGTH_IN_SECONDS);
//timescale_matrix[XF17][XO17]=1.0/(SIGMAV_TBETA17*YEAR_LENGTH_IN_SECONDS);

// O17(pa)N14
timescale_matrix[XO17][XO17]=-Inverse_nuclear_burning_timescale(SIGMAV_T17);
timescale_matrix[XN14][XO17]=Inverse_nuclear_burning_timescale(SIGMAV_T17);

// O17(pg)F18(beta?)O18
timescale_matrix[XO17][XO17]+=-Inverse_nuclear_burning_timescale(SIGMAV_T17);
timescale_matrix[XO18][XO17]=Inverse_nuclear_burning_timescale(SIGMAV_T17);

// O18(pa)N15
timescale_matrix[XO18][XO18]=-Inverse_nuclear_burning_timescale(SIGMAV_O18A);
timescale_matrix[XN15][XO18]=Inverse_nuclear_burning_timescale(SIGMAV_O18A);

// O18(pg)F19
timescale_matrix[XO18][XO18]+=-Inverse_nuclear_burning_timescale(SIGMAV_O18G);
timescale_matrix[XF19][XO18]=Inverse_nuclear_burning_timescale(SIGMAV_O18G);

// F19(pg)Ne19(beta)F18(beta)O18
timescale_matrix[XF19][XF19]=-Inverse_nuclear_burning_timescale(SIGMAV_F19n);
timescale_matrix[XO18][XF19]=Inverse_nuclear_burning_timescale(SIGMAV_F19n);

// F19(pa)O16
timescale_matrix[XF19][XF19]+=-Inverse_nuclear_burning_timescale(SIGMAV_F19A);
timescale_matrix[XO16][XF19]=Inverse_nuclear_burning_timescale(SIGMAV_F19A);

// F19(pg)Ne20
timescale_matrix[XF19][XF19]=-Inverse_nuclear_burning_timescale(SIGMAV_T19);
timescale_matrix[XNe20][XF19]=Inverse_nuclear_burning_timescale(SIGMAV_T19);

// NeNa cycle
timescale_matrix[XNe20][XNe20]=-Inverse_nuclear_burning_timescale(SIGMAV_T20);
timescale_matrix[XNe21][XNe20]=Inverse_nuclear_burning_timescale(SIGMAV_T20);

timescale_matrix[XNe21][XNe21]=-Inverse_nuclear_burning_timescale(SIGMAV_T21);
timescale_matrix[XNe22][XNe21]=Inverse_nuclear_burning_timescale(SIGMAV_T21);

timescale_matrix[XNe22][XNe22]=-Inverse_nuclear_burning_timescale(SIGMAV_T22);
timescale_matrix[XNa23][XNe22]=Inverse_nuclear_burning_timescale(SIGMAV_T22);

timescale_matrix[XNa23][XNa23]=-Inverse_nuclear_burning_timescale(SIGMAV_T23);
timescale_matrix[XNe20][XNa23]=Inverse_nuclear_burning_timescale(SIGMAV_T23);

// NeNa leak
timescale_matrix[XNa23][XNa23]+=-Inverse_nuclear_burning_timescale(SIGMAV_T23P);
timescale_matrix[XMg24][XNa23]=Inverse_nuclear_burning_timescale(SIGMAV_T23P);

// MgAl 
timescale_matrix[XMg24][XMg24]=-Inverse_nuclear_burning_timescale(SIGMAV_T24);
timescale_matrix[XMg25][XMg24]=Inverse_nuclear_burning_timescale(SIGMAV_T24);

timescale_matrix[XMg25][XMg25]=-Inverse_nuclear_burning_timescale(SIGMAV_T25);
timescale_matrix[XAl26][XMg25]=Inverse_nuclear_burning_timescale(SIGMAV_T25);

timescale_matrix[XMg26][XMg26]=-Inverse_nuclear_burning_timescale(SIGMAV_T26);
timescale_matrix[XAl27][XMg24]=Inverse_nuclear_burning_timescale(SIGMAV_T26);

timescale_matrix[XAl26][XAl26]=-Inverse_nuclear_burning_timescale(SIGMAV_T26P);
timescale_matrix[XAl27][XAl26]=Inverse_nuclear_burning_timescale(SIGMAV_T24);

timescale_matrix[XAl27][XAl27]=-Inverse_nuclear_burning_timescale(SIGMAV_T27);
timescale_matrix[XMg24][XAl27]=-Inverse_nuclear_burning_timescale(SIGMAV_T27);

#endif
