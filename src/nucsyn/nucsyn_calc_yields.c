#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef NUCSYN
#include "nucsyn_strided_isotope_loops.h"

Hot_function Nonnull_some_arguments(1,2)
void nucsyn_calc_yields(struct stardata_t * Restrict const stardata,
                                     struct star_t * const star,
                                     const double dmlose, /* mass lost as wind/otherwise */
                                     Abundance * const Xlose, /* abundance of lost material */
                                     const double dmacc, /* mass gained by accretion */
                                     Abundance * const Xacc, /* abundance of this accretion */
                                     const Star_number starnum,
                                     const int final,
                                     const Yield_source source)
{
    Dprint("Calc yields! star=%d int %s source=%d=%s final=%d, M=%g Menv=%g, dmlose=%g, Xlose=%p (1-Xlose %g: H1 %g He4 %g C12 %g N14 %g O16 %g), dmacc=%g, Xacc=%p (1-Xacc %g, C12 %g) : stellar yield tot %g\n",
           starnum,
           Yesno(stardata->model.intermediate_step),
           source,
           Source_string(source),
           final,
           star->mass,
           star->mass - Outermost_core_mass(star),
           dmlose,
           (void*)Xlose,
           (Xlose ? (1.0-nucsyn_totalX(Xlose)) : -1.0),
           (Xlose ? Xlose[XH1] : -1.0),
           (Xlose ? Xlose[XHe4] : -1.0),
           (Xlose ? Xlose[XC12] : -1.0),
           (Xlose ? Xlose[XN14] : -1.0),
           (Xlose ? Xlose[XO16] : -1.0),
           dmacc,
           (void*)Xacc,
           (Xacc ? (1.0-nucsyn_totalX(Xacc)) : -1.0),
           (Xacc ? Xacc[XC12] : -1.0),
           nucsyn_total_yield(stardata)
        );
#ifdef NUCSYN_YIELDS
    /*
     * Subroutine to compute nucleosynthetic yields.
     *
     * This function should ONLY be called by calc_yields().
     *
     * The mass lost by a star is dmlose, with the mass gained dmacc (both are positive).
     * The mass of each element gained or lost can then be calculated from the
     * abundances in Xlose and Xacc
     *
     * These values CAN BE Negative_nonzero in the case of accretion being higher than
     * loss to the ISM, however you WOULD EXPECT that in the single star case
     * this is impossible, and in most binary stars by the end of their lives
     * (1) both stars will have all positive values of yields (2) overall the
     * yield is positive
     *
     * Final is either:
     * YIELD_FINAL : the usual, we compute yields and, if legacy logging, output
     * YIELD_NOT_FINAL : return before output is attempted
     *
     * source tells you where the mass came from: GB/AGB/WR/SN etc.
     * the numbers are defined in binary_c_sources.h
     */


    /* If dm is too small, do nothing! */
    if(final==YIELD_NOT_FINAL &&
       Is_zero(dmlose) &&
       Is_zero(dmacc))
    {
        Dprint("NO Add_yield dmlose=%g dmacc=%g\n",dmlose,dmacc);
    }
    else if(stardata->model.time <
            (stardata->preferences->ensemble_logtimes == TRUE ?
             stardata->preferences->ensemble_startlogtime : 0.0))
    {
        Dprint("too early for yield (time %g < startlogtime %g)\n",
               stardata->model.time,
               stardata->preferences->ensemble_startlogtime);
        /*
         * The "other yield" is mass ejected prior to being added up in the
         * yield arrays. We need to take this into account to avoid
         * the sanity checks failing, but it's just a scalar.
         */
        Dprint("add other yield dm = %g * p = %g\n",
               dmlose - dmacc,
               stardata->model.probability);
        star->other_yield += (dmlose - dmacc) * stardata->model.probability;
    }
    else
    {
        if(Xlose != NULL)
        {
            const Abundance _x = nucsyn_totalX(Xlose);
            if(!Float_same_within_eps(_x,1.0,1e-10))
            {
                Backtrace;
                Exit_binary_c(2,"Xlose sum != 1.0 : diff %g\n",
                              1.0 - _x);
            }
        }
        if(Xacc != NULL)
        {
            const Abundance _x = nucsyn_totalX(Xacc);
            if(!Float_same_within_eps(_x,1.0,1e-10))
            {
                Backtrace;
                Exit_binary_c(2,"Xacc sum != 1.0 star %d : diff %g\n",
                              star->starnum,
                              1.0 - _x);
            }
        }
        nucsyn_renormalize_abundance(Xlose);
        nucsyn_renormalize_abundance(Xacc);
        Abundance * const Xyield = star->Xyield;

        /*
         * If this timestep traverses the ensemble_startlogtime, we should
         * not count all of it. Adapt the yields with the multiplier to
         * compensate.
         */
        const double multiplier =
            (stardata->preferences->ensemble_logtimes == TRUE &&
             stardata->model.time - stardata->model.dt*1e-6 < stardata->preferences->ensemble_startlogtime)
            ?
            ((stardata->model.time - stardata->preferences->ensemble_startlogtime) / (1e-6*stardata->model.dt))
            :
            1.0;
        Dprint("timestep multiplier %g\n",multiplier);

#ifdef NUCSYN_LOG_MPYIELDS
        const double dm = (dmacc - dmlose) * multiplier;
        const double dmp = dm * multiplier * stardata->model.probability;
        double * mpyield;
        const Abundance * const XZAMS = stardata->preferences->zero_age.XZAMS;
#endif//NUCSYN_LOG_MPYIELDS
#ifdef NUCSYN_GCE_OUTFLOW_CHECKS
        double t;
#endif//GCE_OUTFLOW_CHECKS

        /************/

#ifdef NANCHECKS
        nancheck("calc_yields start Xyield",star->Xyield,ISOTOPE_ARRAY_SIZE);
        nancheck("calc_yields start Xenv",star->Xenv,ISOTOPE_ARRAY_SIZE);
        nancheck("calc_yields start Xlose",Xlose,ISOTOPE_ARRAY_SIZE);
#endif//NANCHECKS

#ifdef NUCSYN_GCE_OUTFLOW_CHECKS
        Dprint("Outflow checks...\n");

        if(star->vwind > stardata->preferences->escape_velocity && Xlose!=NULL)
        {
// modulate by escape fraction
            t = 1.0 - stardata->preferences->escape_fraction;
            Nucsyn_isotope_stride_loop( Xlose[i] *= t );
        }
#endif//GCE_OUTFLOW_CHECKS

        Dprint("calc_yields (star %d,type %d,M=%30.20e,source=%d,intermediate=%d) dmlose=%g, dmacc=%g, multiplier=%g, Xenv XH1=%g mass to ISM (or other star) %g [wind dM/dt=%g expect %g lost]\n",
               star->starnum,
               star->stellar_type,
               star->mass,
               source,
               stardata->model.intermediate_step,
               dmlose,
               dmacc,
               multiplier,
               star->Xenv[XH1],
               Max(dmlose-dmacc,0.0),
               star->derivative[DERIVATIVE_STELLAR_MASS_WIND_LOSS],
               star->derivative[DERIVATIVE_STELLAR_MASS_WIND_LOSS] * stardata->model.dt
            );

        /* We always want to calculate the yields... */
#ifdef NUCSYN_LOG_INDIVIDUAL_DX_YIELDS
        Printf("DXYIELD%d__ ",starnum);
        Dprint("(dmlose=%g dmacc=%g) ",starnum,dmlose,dmacc);
#endif//NUCSYN_LOG_INDIVIDUAL_DX_YIELDS

#ifdef NUCSYN_PLANETARY_NEBULAE
        /*
         * Wind loss causes a PN in TPAGB stars, but goes via
         * comenv (logged elsewhere)
         */
        if(star->stellar_type==TPAGB && dmlose > 0.0)
        {
            nucsyn_planetary_nebulae(stardata,
                                     PNE_AGB,
                                     star->starnum,
                                     dmlose*multiplier,
                                     star->Xenv,
                                     0.0);
        }
#endif

#ifdef NUCSYN_ID_SOURCES
#define _CODE_sources Xsource[i] += dy;
#define _Set_Xsource                                            \
        if(star->Xsource[source] == NULL)                       \
        {                                                       \
            star->Xsource[source] = New_clear_isotope_array;    \
        }                                                       \
        Abundance * const Xsource = star->Xsource[source];
#else
#define _CODE_sources /* do nothing */
#define _Set_Xsource /* do nothing */
#endif //NUCSYN_ID_SOURCES

/* add mpyields */
#ifdef NUCSYN_LOG_MPYIELDS
#define _CODE_mpyields mpyield[i] += dy + dmp * XZAMS[i];
#else
#define _CODE_mpyields /* do nothing */
#endif // NUCSYN_LOG_MPYIELDS

/* always Xyield */
#define _CODE_Xyield Xyield[i] += dy;

#if defined NUCSYN_LOG_YIELDS_EVERY_TIMESTEP && \
    defined NUCSYN_LOG_INDIVIDUAL_DX_YIELDS
#define _CODE_printf Printf("%0+.3e ",dy);
#else
#define _CODE_printf /* do nothing */
#endif // NUCSYN_LOG_YIELDS_EVERY_TIMESTEP && NUCSYN_LOG_INDIVIDUAL_DX_YIELDS

        if(Is_zero(dmacc))
        {
            /*
             * Material is only lost, Xacc can be NULL
             */
            if(Is_not_zero(dmlose) && Xlose != NULL)
            {
                /*
                 * this is the most likely case, hence it is first
                 */
                const double dmlp = dmlose * multiplier * stardata->model.probability;
                Dprint("dmlose = %g > TINY (dmacc = 0) -> dmlp = %g (1-Xlose = %g)\n",
                       dmlose,
                       dmlp,
                       1.0 -nucsyn_totalX(Xlose));

                _Set_Xsource;
                Nucsyn_isotope_stride_loop(
                    {
                        const double dy = dmlp * Xlose[i];
                        _CODE_printf;
                        _CODE_sources;
                        _CODE_mpyields;
                        _CODE_Xyield;
                    }
                    );
            }
            Dprint("Done add yield\n");
        }

        /*
         * To get here, dmacc is non-zero.
         * If dmlose is zero, we only accrete.
         */
        else if(Is_zero(dmlose) && Xacc!=NULL)
        {
            Dprint("dmlose = 0, dmacc=%g\n",dmacc);
            const double dmap = - dmacc * stardata->model.probability * multiplier;
            _Set_Xsource;
            Nucsyn_isotope_stride_loop(
                {
                    const double dy = dmap * Xacc[i];
                    _CODE_printf;
                    _CODE_sources;
                    _CODE_mpyields;
                    _CODE_Xyield;
                }
                );
        }
        else if(Xacc!=NULL && Xlose!=NULL)
        {
            /*
             * General case : both dmacc and dmlose are
             * non-zero, but we require the X arrays to be non-NULL
             */
            Dprint("general case : dmlose = %g, dmacc = %g\n",
                   dmlose,
                   dmacc);
            const double dmlosep = dmlose * stardata->model.probability * multiplier;
            const double dmaccp = dmacc * stardata->model.probability * multiplier;
            _Set_Xsource;
            Nucsyn_isotope_stride_loop(
                {
                    const double dy = dmlosep * Xlose[i] - dmaccp * Xacc[i];
                    _CODE_printf;
                    _CODE_sources;
                    _CODE_mpyields;
                    _CODE_Xyield;
                }
                );
        }

#ifdef NUCSYN_LOG_YIELDS_EVERY_TIMESTEP

#ifdef NUCSYN_LOG_INDIVIDUAL_DX_YIELDS
        Printf("\n");
#endif // NUCSYN_LOG_INDIVIDUAL_DX_YIELDS

#else

        if(final==YIELD_NOT_FINAL)
        {
            Dprint("yield not final : return\n");
            return;
        }

#endif // NUCSYN_LOG_YIELDS_EVERY_TIMESTEP



#ifdef NUCSYN_LOG_SINGLE_X_YIELDS
        /* now log the yields */
        Printf("XYIELD%d__ ",starnum);
        Forward_isotope_loop(i)
        {
            Printf("%0+.3e ",Xyield[i]);
        }
        Printf("\n");
#endif //NUCSYN_LOG_SINGLE_X_YIELDS

#ifdef NUCSYN_LOG_MPYIELDS
        Printf("MPYIELD%d__ ",starnum);
        // Output p(i) !
        const double pmsm = stardata->common.zero_age.mass[star->starnum];
        Forward_isotope_loop(i)
        {
            /* remember that to get the p(i) you must divide by the zams mass */
            Printf("%0+.3e ",mpyield[i]/pmsm);
        }
        Printf("\n");
#endif //NUCSYN_LOG_MPYIELDS

#ifdef NANCHECKS
        nancheck("calc_yields end",
                 star->Xyield,
                 ISOTOPE_ARRAY_SIZE);
#endif//NANCHECKS
    }
#endif /* NUCSYN_YIELDS */
    Dprint("return\n");
}

#endif /* NUCSYN */
