#include "../binary_c.h"
No_empty_translation_unit_warning;
#ifdef THIS_CODE_IS_DEPRECATED

/****** DEPRECATED ******/
/************************************************************/

/* see nucsyn_anal_NeNa_burn1 instead */

/************************************************************/
/************************************************************/
/************************************************************/
/************************************************************/
/************************************************************/
/************************************************************/
/************************************************************/
/************************************************************/
/************************************************************/
/************************************************************/
/************************************************************/
/************************************************************/
/************************************************************/
/************************************************************/
/************************************************************/
/************************************************************/
/************************************************************/


#include "nucsyn.h"
#if (defined NUCSYN && defined NUCSYN_ANAL_BURN)

/* NeNa cycle burning */





int nucsyn_anal_NeNa_burn(double * Restrict t,
                          Number_density * Restrict Nin,
                          const double dtseconds,
                          const Boolean eqonly)
{

    /* Nin is Ne20, Ne21, Ne22 and Na23 and is a 1-based array */
    /* t is an array of timescales corresponding to tau20,21,22,23 */
    /* dtseconds is the burn time in seconds */
    double alpha,beta,gamma,delta; /* 1.0/timescales */
    double nenasum; /* sum of all abundances */
    /* equilibrium abundances */
    double eqabunds[5];
    /* set the timescales */
    double t20=t[1],t21=t[2],t22=t[3],t23=t[4];
    double tsum; /* sum of timescales */
    double a=1,b,c,d; /* cubic equation coeffs */
    double ee,f; /* used later on (dummy vars) */
    double U[5][5]; /* eigenvectors */
    double A[5]; /* normalizing coeffs for the eigenvectors */
    double deltaab[5]; /* current abund - eq abund */
    double real_solutions[4]; /* real part of the cubic solutions */
    double imaginary_solutions[4]; /* imaginary part */
    double lambda; /* the working value of lambda_i */
    int solutions; /* number of solutions of the cubic */
    double Nnew[6]; /* The new abundances */
    // NB Nnew must be set to zero here
    int i,j; /* counters */
    double Utot; /* sum of the eigenvector components */
    double dF; /* change in Fluorine */
    double aminb,cminb,cmina;

    /********************************************************/
    double tscaler;

    return(0);
    //    dtseconds*=0;

    /* Burn fluorine source */
    dF=Nin[0]*(1.0-exp(-dtseconds/t[0]));
    Nin[0]=Nin[0]-dF;
    // Leakage into the cycle
    // removed because it causes numerical instability and
    // is generally negligible
    Nin[1]=Nin[1]+dF;

    Dprint("dF19=%g F19 now %g\n",dF,Nin[0]);

    /* NeNa cycle */
    /* Define sum of number density */
    nenasum=Nin[1]+Nin[2]+Nin[3]+Nin[4];


    // Rescale the timescales at the beginning of the function, then later
    // scale them back just before the exponential is taken. The
    // eigenvalue stuff should not be affected!
    tscaler=Min(t20,Min(t21,Min(t22,t23)));
    tscaler=1.0/tscaler;
    t20*=tscaler;
    t21*=tscaler;
    t22*=tscaler;
    t23*=tscaler;

    /* Define sum of timescales */
    tsum=t20+t21+t22+t23;

    /* Calculate lambda=0 solution : equilibrium values */
    eqabunds[1]=nenasum*t20/tsum; /* Ne20 */
    eqabunds[2]=nenasum*t21/tsum; /* Ne21 */
    eqabunds[3]=nenasum*t22/tsum; /* Ne22 */
    eqabunds[4]=nenasum*t23/tsum; /* Na23 */

    /*
      Nin[1]=eqabunds[1];
      Nin[2]=eqabunds[2];
      Nin[3]=eqabunds[3];
      Nin[4]=eqabunds[4];
    */

    Dprint("\nNeNa burn\n");
    Dprint("NeNa tsum=%g nenasum=%g\n",tsum,nenasum);
    Dprint("            %10s %10s %10s %10s\n","Ne20","Ne21","Ne22","Na23");
    Dprint("Abunds in : %10.5g %10.5g %10.5g %10.5g\n",Nin[1],Nin[2],Nin[3],Nin[4]);
    Dprint("Numb fracs: %10.5g %10.5g %10.5g %10.5g\n",Nin[1]/nenasum,Nin[2]/nenasum,
           Nin[3]/nenasum,Nin[4]/nenasum);
    Dprint("Eq abunds : %10.5g %10.5g %10.5g %10.5g\n",eqabunds[1],eqabunds[2],eqabunds[3],eqabunds[4]);
    Dprint("Eq abunds : %10.5g %10.5g %10.5g %10.5g\n",eqabunds[1]/nenasum,eqabunds[2]/nenasum,
           eqabunds[3]/nenasum,eqabunds[4]/nenasum);

    Dprint("Timescales: %10.5g %10.5g %10.5g %10.5g\n",t20/tscaler,t21/tscaler,t22/tscaler,t23/tscaler);
    Dprint("Scaled    : %10.5g %10.5g %10.5g %10.5g\n",t20,t21,t22,t23);
    Dprint("   /years : %10.5g %10.5g %10.5g %10.5g\n",
           t20/YEAR_LENGTH_IN_SECONDS,t21/YEAR_LENGTH_IN_SECONDS,
           t22/YEAR_LENGTH_IN_SECONDS,t23/YEAR_LENGTH_IN_SECONDS);

    Dprint("Burn time : %10.5g\n",dtseconds);
#endif

    /* there are always problems when one timescale is << the others
       which are due to propagating numerical errors. I have not yet
       managed to cure these! */

    /* define alpha to delta */
    alpha=1.0/t20;
    beta=1.0/t21;
    gamma=1.0/t22;
    delta=1.0/t23;

    /* calculate b,c,d for the cubic eigenvalue equation */
    b=alpha+beta+gamma+delta;
    c=(gamma+alpha)*delta+
        (alpha+beta)*gamma +
        (alpha+delta)*beta;
    d=(alpha+beta)*gamma*delta +
        alpha*beta*(delta +gamma) ;
    Dprint("Gnuplot\nx*x*x+%g*x*x+%g*x+%g\n",b,c,d);

    /* obtain eigenvalues (or at least the real parts) */
    SolveCubic2(1,b,c,d,
                &solutions,
                real_solutions,
                imaginary_solutions);


    Dprint("Nsolutions : %d\n",solutions);
    Dprint("Cubic roots are : %g %g+%g*i %g+%g*i  (Re/Im : %g %g)\n",
           real_solutions[0],
           real_solutions[1],
           imaginary_solutions[1],
           real_solutions[2],
           imaginary_solutions[2],
           real_solutions[1]/imaginary_solutions[1],
           real_solutions[2]/imaginary_solutions[2]
        );


    // dump if accuracy is bad
    Dprint("Check cubic accuracy\n");
#if (DEBUG==1)
    for(i=0;i<3;i++)
    {
        Dprint("At root %d terms are:\nxxx=%g bxx=%g cx=%g d=%g : Total %g\n",
               i,
               Pow3(real_solutions[i]),
               b*Pow2(real_solutions[i]),
               c*real_solutions[i],
               d,
               Pow3(real_solutions[i])+
               b*Pow2(real_solutions[i])+
               c*real_solutions[i]+
               d
            );
    }
#endif

    if(solutions<3)
    {
        /*
         * There are not enough real solutions for this to work, so skip for a
         * timestep
         */
        Dprint("Skipping NeNa on this timestep because there is only one real solution to the cubic - and this leads to problems.\n");

        return(-1);
    }

    for(i=1;i<=4;i++)
    {
        if(i>=2)
        {
            lambda=real_solutions[i-2];
        }
        else
        {
            lambda=0;
        }

        if(i==1)
        {
            if(fabs(1.0/t20+lambda)<1e-9)
            {
                Dprint("Undeterminable determinant! (%g)\n",
                       fabs(-1.0/t20-lambda));
            }
        }

        Dprint("Matrix Diagonal for eigenvalue %d=%g is : %g %g %g %g\n",
               i,lambda,
               -1.0/t20-lambda,
               -1.0/t21-lambda,
               -1.0/t22-lambda,
               -1.0/t23-lambda);
    }
    Dprint("\n");

    /* now we have the roots calculate the eigenvector components */
    /*
     * store as U[i][j] where i is the lambda value (the real part of the
     * eigenvalue) and j is the isotope
     */
    for(i=1;i<=3;i++)
    {
        /* use real part of lambda as a surrogate for lambda */
        lambda=real_solutions[i-1]; /*
                                     * real_solutions is a zero based array
                                     * unlike U
                                     */
        Dprint("Setting eigenvector %d\n",i);


        /* Now enter the components of the eigenvectors */
        U[i][1]=t20*(lambda+1.0/t21); /* Ne20 */

        // Alternative, more stable?
        U[i][1]=t20*lambda+t20/t21;

        Dprint("Set U[%d][1]=%g from %g*(%g+1.0/%g)=%g or alternative=%g\n",
               i,U[i][1],t20,lambda,t21,
               t20*(lambda+1.0/t21),
               t20*lambda+t20/t21);

        U[i][2]=1.0; /* Ne21 */
        Dprint("Set U[%d][2]=1\n",i);

        // While this expression is correct it does not
        // deal with cases where lambda~1/t22 very well
        //U[i][3]=(1.0/(t21*(lambda+1.0/t22))); /* Ne22 */

        /* alternative, deals better with 1/0 problems (but it's
         * not perfect!) */
        U[i][3]=
            (t22/t21)*
            (lambda*t20+1.0)*
            (lambda*t21+1.0)*
            (lambda*t23+1.0);

        Dprint("Set U[%d][%d]=%g from  (t22=%g/t21=%g) *( lambda=%g*t20=%g +1)*(lambda=%g*t21=%g +1)*(lambda=%g*t23=%g+1) = (%g)*(%g)*(%g)*(%g)\n\n",
               i,3,U[i][3],
               t22,t21,
               lambda,t20,
               lambda,t21,
               lambda,t23,
               t22/t21,lambda*t20+1.0,lambda*t21+1.0,lambda*t23+1.0);
        Dprint("alternative %g\n",(1.0/(t21*(lambda+1.0/t22))));
        Dprint("altalternative %g\n",
               (t22/t21)*(lambda*lambda*lambda*t20*t21*t23+
                          lambda*lambda*(t20*t23+t21*t23+t20*t21)+
                          lambda*(t20+t21+t23)+
                          1.0));

        U[i][4]=t20*t23*(lambda+1.0/t21)*(lambda+1.0/t20); /* Na23 */
        Dprint("U4 0 : %g\n",U[i][4]);

        U[i][4]=(t23/t21)*(lambda*t21+1.0)*(lambda*t20+1.0); /* Na23 */
        Dprint("U4 1 : %g\n",U[i][4]);

        U[i][4]=(t23/t21)*((lambda*t21)*(lambda*t20)+(lambda*t20)+(lambda*t21)+1.0);
        Dprint("U4 2 : %g\n",U[i][4]);


        Dprint("Set U[%d][%d]=%g from (t23=%g/t21=%g)*(t21=%g*lambda=%g+1.0)*(lambda=%g*t20=%g+1.0)=(%g)*(%g)*(%g)\n\n",
               i,4,U[i][4],
               t23,t21,t21,lambda,lambda,t20,
               t23/t21,lambda*t21+1.0,lambda*t20+1.0
            );
        Dprint("alternative =%g\n",
               t20*t23*(lambda+1.0/t21)*(lambda+1.0/t20));

        Utot=U[i][1]+U[i][2]+U[i][3]+U[i][4];

        Dprint("Eigenvector %d has eigenvalue %g and cpts %g %g %g %g tot=%g\n",
               i,lambda,
               U[i][1],
               U[i][2],
               U[i][3],
               U[i][4],
               Utot);
    }

    /*
     * So we have the equilibrium abunds and the eigenvectors, now we need to
     * set the normalizing factors
     */

    /* Set the delta array where delta = current abund - eq abund */
    for(i=1;i<=4;i++)
    {
        deltaab[i]=Nin[i]-eqabunds[i];
        Dprint("Set deltaab[%d] to %g\n",i,deltaab[i]);

    }
    /* set a,b,c,d,e,f */
    a=t20*(real_solutions[0]+1.0/t21);
    b=t20*(real_solutions[1]+1.0/t21);
    c=t20*(real_solutions[2]+1.0/t21);
    //d=1.0/(t21*(real_solutions[0]+1.0/t22));


    // Alternative, more stable, calculation method
    //d=(real_solutions[0]*t20+1.0)*(real_solutions[0]+1.0/t21)*
    //(real_solutions[0]*t23+1.0)*t22;
    d=(real_solutions[0]*t20+1.0)*(t22*real_solutions[0]+t22/t21)*
        (real_solutions[0]*t23+1.0);
    //ee=1.0/(t21*(real_solutions[1]+1.0/t22));
    //f=1.0/(t21*(real_solutions[2]+1.0/t22));
    ee=(real_solutions[1]*t20+1.0)*(t22*real_solutions[1]*+t22/t21)*
        (real_solutions[1]*t23+1.0);
    f=(real_solutions[2]*t20+1.0)*(t22*real_solutions[2]+t22/t21)*
        (real_solutions[2]*t23+1.0);

    Dprint("a=%g b=%g c=%g d=%g ee=%g f=%g\n",a,b,c,d,ee,f);

    /* solve for A */
    A[3]  = deltaab[3]-deltaab[2]-((ee-d)/(b-1))*(deltaab[1]-deltaab[2]);
    A[3] /= ((ee-d)/(b-1))*(1-c)+(f-d);
    A[2]  = deltaab[1]-deltaab[2]-A[3]*(c-1);
    A[2] /= b-1;
    A[1]  = deltaab[2] - A[2] - A[3];
    Dprint("Normalizing functions A[1]=%g A[2]=%g A[3]=%g\n",
           A[1],A[2],A[3]);

    // Newer way of solving them: might be more stable, should
    // give the same answer (usually does, but not when the
    // solutions are unstable!)

    aminb=t20*(real_solutions[0]-real_solutions[1]);
    cminb=t20*(real_solutions[2]-real_solutions[1]);
    cmina=t20*(real_solutions[2]-real_solutions[0]);

    Dprint("xMINy %g vs %g ; %g vs %g; %g vs %g\n",
           aminb,a-b,
           cminb,c-b,
           cmina,c-a);


    A[3]=aminb*deltaab[3]+(d*b-a*ee)*deltaab[2]+(ee-d)*deltaab[1];
    Dprint("A[3] numerator=%g ",A[3]);

    A[3]/=aminb*f+d*cminb+ee*cmina;
    Dprint("denominator=%g : result=%g\n",
           aminb*f+d*cminb+ee*cmina,A[3]);
    A[2]=deltaab[1]-a*deltaab[2]-A[3]*cmina;
    Dprint("A[2] numerator=%g ",A[2]);

    A[2]/=-aminb;
    Dprint("denominator=%g : result=%g\n",
           -aminb,A[2]);
    A[1]=deltaab[2] - A[2] - A[3];

    Dprint("Calc A[1] = %g - %g - %g = %g\n",deltaab[2],A[2],A[3],A[1]);
    Dprint("NEW Normalizing functions A[1]=%g A[2]=%g A[3]=%g\n",
           A[1],A[2],A[3]);

    /*
     * Finally we can calculate the new abundances
     * from the above calculations
     */

    /* isotope : j */
    /* eigenvalue : i */
    for(j=1;j<=4;j++)
    {

        Nnew[j] = eqabunds[j]; /* U_0 */
        Dprint("\nSet iso %d to eqabunds = %g\n",j,eqabunds[j]);

        for(i=1;i<=3;i++) /* i loops over the eigenvector/value */
        {
#if (DEBUG==1)
            Dprint("Add bit %d to iso %d : lambdat=%g (lambda=%g) U=%g A=%g bit=%g ",
                   i,j,real_solutions[i-1]*dtseconds,
                   real_solutions[i-1],
                   U[i][j],
                   A[i],
                   A[i]*exp(real_solutions[i-1]*dtseconds/tscaler)*U[i][j]
                );
#endif
            Nnew[j] += A[i]*exp(real_solutions[i-1]*dtseconds/tscaler)*U[i][j];
            Dprint("Nnew now %g (%2.2f %% of old)\n",
                   Nnew[j],Nnew[j]*100.0/Nin[j]);
        }
    }

    // check that the abundances are all ok
    j=0;
    for(i=1;i<=4;i++)
    {
        if(Nnew[i]<0)
        {
            Dprint("WARNING: isotope %d has abundance %g < 0\n",i,Nnew[i]);

            j=-1;
            i=5;
        }
    }

    if(j==0)
    {
        for(i=1;i<=4;i++)
        {
            // abundance > 0 - this is probably an ok solution
            Nin[i]=Nnew[i];
        }
    }
#if (DEBUG==1)
    else
    {
        Dprint("Solution has a negative abundance!\n");
        Dprint("total %g\n",Nin[1]+Nin[2]+Nin[3]+Nin[4]);
        /*Dprint("Assume eq\n");
          Nin[1]=eqabunds[1];
          Nin[2]=eqabunds[2];
          Nin[3]=eqabunds[3];
          Nin[4]=eqabunds[4];
        */
        Exit_binary_c(BINARY_C_NORMAL_EXIT,"");
    }
#endif


    // Note: if the solution is NOT ok then just leave the
    // abundances as is - there is a numerical instability around
    // lambda=1/t20 (the determinant cannot be calculted!)
    // but this is temporary :)

    Dprint("Abunds out: %10.5g %10.5g %10.5g %10.5g\n",Nin[1],Nin[2],Nin[3],Nin[4]);
    Dprint("Numb fracs: %10.5g %10.5g %10.5g %10.5g\n",Nin[1]/nenasum,Nin[2]/nenasum,
           Nin[3]/nenasum,Nin[4]/nenasum);
    Dprint("Sum in/out = %g\n",
           (Nin[1]+Nin[2]+Nin[3]+Nin[4])/nenasum);

    if((Nin[1]+Nin[2]+Nin[3]+Nin[4])/nenasum > 1.01)
    {
        Dprint("Nena not conserved error!\n");
        Exit_binary_c(BINARY_C_NORMAL_EXIT,"");
    }

    return(j);
}


#endif
