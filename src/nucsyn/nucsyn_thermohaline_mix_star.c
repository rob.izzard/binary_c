#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef NUCSYN

/*
 * Wrapper for nucsyn_thermohaline_mix to mix a star
 * given in the star struct
 */

void nucsyn_thermohaline_mix_star(struct stardata_t * const stardata,
                                  struct star_t * const star)
{
    nucsyn_thermohaline_mix(stardata,
                            star->starnum,
                            star->Xacc,
                            star->dmacc,
                            star->Xenv,
                            star->Xenv);
}

#endif
