#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef NUCSYN
void nucsyn_set_WD_abunds(struct stardata_t * const stardata,
                          Abundance * Restrict const Xenv,
                          const Stellar_type stellar_type)
{
    /*
     * Function to set the star with white dwarf abundances according to the
     * stellar type
     */

    Dprint("Setting WD abunds type = %d\n",stellar_type);
    if(stellar_type==COWD)
    {
        /* Convert H,He,C,N,O to WD abunds */
        Xenv[XH1]=0.0;
        Xenv[XHe4]=0.0;
        Xenv[XC13]=0.0;
        Xenv[XN13]=0.0;
        Xenv[XN14]=0.0;
        Xenv[XN15]=0.0;
        Xenv[XO15]=0.0;
        Xenv[XO16]=0.2;
        Xenv[XF17]=0.0;
        Xenv[XF19]=0.0;
        Xenv[XC12]=0.0;
        Xenv[XC12]=1.0-nucsyn_totalX(Xenv);

        /* Leave everything heavier - renormalize at the end */
    }
    else if(stellar_type==ONeWD)
    {
        /* as above but kill the C12 */
        Xenv[XH1]=0.0;
        Xenv[XHe4]=0.0;
        Xenv[XC12]=0.0;
        Xenv[XC13]=0.0;
        Xenv[XN13]=0.0;
        Xenv[XN14]=0.0;
        Xenv[XN15]=0.0;
        Xenv[XO15]=0.0;
        Xenv[XF17]=0.0;
        Xenv[XF19]=0.0;
        Xenv[XNe20]=0.2;
        Xenv[XO16]=0.0;
        Xenv[XO16]=1.0-nucsyn_totalX(Xenv);
    }
    else
    {
        /* He WD? */
        Xenv[XH1]=0.0;
        Xenv[XC12]=0.0;
        Xenv[XC13]=0.0;
        Xenv[XN13]=0.0;
        Xenv[XN14]=0.0;
        Xenv[XN15]=0.0;
        Xenv[XO15]=0.0;
        Xenv[XO16]=0.0;
        Xenv[XF17]=0.0;
        Xenv[XF19]=0.0;
        Xenv[XNe22]=0.0;
        Xenv[XHe4]=0.0;
        Xenv[XHe4]=1.0-nucsyn_totalX(Xenv);
    }

    Dprint("Set WD abunds Abunds : H %g He %g C12 %g O16 %g : 1.0 - total %g\n",
           Xenv[XH1],
           Xenv[XHe4],
           Xenv[XC12],
           Xenv[XO16],
           1.0 - nucsyn_totalX(Xenv));
}
#endif /* NUCSYN */
