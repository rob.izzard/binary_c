#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef NUCSYN

#ifdef USE_TABULAR_INTERSHELL_ABUNDANCES_KARAKAS_2002
#include "nucsyn_intershell_abundances.newmethod.h"
#endif

#ifdef USE_TABULAR_INTERSHELL_ABUNDANCES_KARAKAS_2012
#include "nucsyn_intershell_abundances_karakas_2012.h"
#endif

void nucsyn_set_third_dredgeup_abunds(Abundance * Restrict const dup_material,
                                      struct star_t * const newstar,
                                      const Abundance z /* Metallicity */,
                                      struct stardata_t * Restrict const stardata,
                                      const double mc Maybe_unused)
{
#ifdef USE_TABULAR_INTERSHELL_ABUNDANCES_KARAKAS_2002

    /*
     * set isotopic abundances of dredgeup material according to Amanda's paper
     * (Karakas et al. 2002). The array dup_material contains the abundances, *star
     * is provided so that we can extend these to be time dependent
     *
     * Table format is : Z,M,Npulse
     */
    Const_data_table intershell_table[NUCSYN_INTERSHELL_TABLE_LINES*NUCSYN_INTERSHELL_TABLE_LINE_LENGTH]={NUCSYN_INTERSHELL_TABLE};
    double x[3]; // input data

    double intershell[NUCSYN_INTERSHELL_TABLE_LINE_LENGTH]; // output data

    x[0] = Max(1e-4,Min(0.02,z));
    x[1] = newstar->menv_1tp + newstar->mc_1tp;
    x[2] = Max(1,Min(137,newstar->num_thermal_pulses));

    /*
     * First copy envelope abundances to intershell abundances
     */
    Copy_abundances(newstar->Xenv,dup_material);

    /*
     * Remove H1 and CNO
     */
    dup_material[XH1]=0.0;
    dup_material[XN14]=0.0;
    dup_material[XC13]=0.0;
    dup_material[XO15]=0.0;
    dup_material[XO17]=0.0;
    dup_material[XN15]=0.0;

    Dprint("Interpolate intershell : in z=%g ntp=%g mc=%g \n",
           x[0],x[1],x[2]);
    rinterpolate(intershell_table,
                 stardata->persistent_data->rinterpolate_data,
                 (long int)3,
                 (long int)NUCSYN_INTERSHELL_TABLE_LINE_LENGTH-3,
                 (long int)NUCSYN_INTERSHELL_TABLE_LINES,
                 x,
                 intershell,
                 0);


    Dprint("Interpolate intershell : in z=%g ntp=%g mc=%g : out %g %g %g ... \n",
           x[0],x[1],x[2],intershell[0],intershell[1],intershell[2]);

    /*
     * Intershell abundances are (from the table)
     * He4 C12 O16 F19 Ne20 Ne21 Ne22 Na23 Mg24 Mg25 Mg26 Al26 Al27 Si28 Si29 Si30
     */
    dup_material[XHe4]=intershell[0];
    dup_material[XC12]=intershell[1];
    dup_material[XO16]=intershell[2];
    dup_material[XF19]=intershell[3];

    //dup_material[XF19]*=3.0; // selma fudge
    dup_material[XNe20]=intershell[4];
    dup_material[XNe21]=intershell[5];
    dup_material[XNe22]=intershell[6];
    dup_material[XNa23]=intershell[7];
    dup_material[XMg24]=intershell[8];
    dup_material[XMg25]=intershell[9];
    dup_material[XMg26]=intershell[10];
    dup_material[XAl26]=intershell[11];
    dup_material[XAl27]=intershell[12];
    dup_material[XSi28]=intershell[13];
    dup_material[XSi29]=intershell[14];
    dup_material[XSi30]=intershell[15];
#ifdef NUCSYN_ALL_ISOTOPES
    dup_material[XP31]=intershell[16];
#endif//NUCSYN_ALL_ISOTOPES
#ifdef NUCSYN_S_PROCESS
    /*
     * Separate treatment for s-process
     */
    nucsyn_s_process(dup_material,
                     newstar,
                     stardata->common.metallicity,
                     stardata);

#endif//NUCSYN_S_PROCESS

#ifdef NUCSYN_THIRD_DREDGE_UP_MULTIPLIERS
    /*
     * Allow boost of dredge-up material
     */
    if(stardata->preferences->boost_third_dup==TRUE)
    {
        Nucsyn_isotope_stride_loop(
            dup_material[i] *= stardata->preferences->third_dup_multiplier[i];
            );
    }
#endif

    /*
     * Normalize to 1.0 by putting the error in helium (the
     * most abundant element)
     */
    dup_material[XHe4]=0.0;
    dup_material[XHe4]=1.0-nucsyn_totalX(dup_material);

#if (DEBUG==1)
    {
        Nucsyn_isotope_stride_loop(
            if((dup_material[i]<-TINY)||(dup_material[i]>1.0))
            {
                Exit_binary_c(BINARY_C_OUT_OF_RANGE,
                              "Isotope %u = %g: this cannot be correct!\n",i,dup_material[i]);
            }
            );
    }
    Dprint("INTERSHELL TOTAL X = %g : H1=%3.3e He4=%3.3e C12=%3.3e O16=%3.3e Ne20=%3.3e Ne21=%3.3e Ne22=%3.3e Mg24=%3.3e Mg25=%3.3e Mg26=%3.3e Al27=%3.3e Si28=%3.3e Si29=%3.3e, SUM %g\n",
           nucsyn_totalX(dup_material),
           dup_material[XH1],
           dup_material[XHe4],
           dup_material[XC12],
           dup_material[XO16],
           dup_material[XNe20],
           dup_material[XNe21],
           dup_material[XNe22],
           dup_material[XMg24],
           dup_material[XMg25],
           dup_material[XMg26],
           dup_material[XAl27],
           dup_material[XSi28],
           dup_material[XSi29],
           nucsyn_totalX(dup_material)
        );
#endif



#endif // USE_TABULAR_INTERSHELL_ABUNDANCES_KARAKAS_2002


#ifdef USE_TABULAR_INTERSHELL_ABUNDANCES_KARAKAS_2012 //CAB


    /* check we're not using the old s-process! */
#ifdef NUCSYN_S_PROCESS
    you should not use NUCSYN_S_PROCESS with USE_TABULAR_INTERSHELL_ABUNDANCES_KARAKAS_2012
#endif

        /*
         * set isotopic abundances of dredgeup material according to new models
         * (Karakas et al 2010, MNRAS,403,1413) + (Lugaro et al 2012, ApJ 747,2)
         */
        static const double intershell_table[NUCSYN_INTERSHELL_TABLE_LINES*NUCSYN_INTERSHELL_TABLE_LINE_LENGTH]={NUCSYN_INTERSHELL_TABLE};
    double x[4]; // input data

    double intershell[NUCSYN_INTERSHELL_TABLE_LINE_LENGTH]; // output data

    x[0]=Max(1e-4,Min(0.02,z));
    x[1]=stardata->preferences->pmz_mass; /* you can set this in your tbse with --mass_of_pmz */
    x[2] = newstar->menv_1tp + newstar->mc_1tp; /* menv_1tp\mc_1tp is the envelope\core-mass at the 1st TP */
    x[3]=Max(1,Min(105,newstar->num_thermal_pulses)); /* max number of pulses in Amanda's data is 105 for a 6Msun star */

    /*
     * First copy envelope abundances to intershell abundances
     */
    Copy_abundances(newstar->Xenv,dup_material);

    /*
     * Remove H1 and CNO
     */
    dup_material[XH1]=0.0;
    dup_material[XN14]=0.0;
    dup_material[XC13]=0.0;
    dup_material[XO15]=0.0;
    dup_material[XO17]=0.0;
    dup_material[XN15]=0.0;

    Dprint("Interpolate intershell : in z=%g pmz=%g M*1tp=%g ntp=%g \n",
           x[0],x[1],x[2],x[3]);
    interpolate(intershell_table, /* static array, you defined this above */
                (long int)4,      /* number of fit paramters, in this case Z, pmz, M*1tp, ntp */
                (long int)NUCSYN_INTERSHELL_TABLE_LINE_LENGTH-4, /* number of columns in the table of abunds */
                (long int)NUCSYN_INTERSHELL_TABLE_LINES,         /* number of columns in the table of abunds */
                x,                                               /* input array, defined above */
                intershell,                                      /* output array, defined above */
                INTERPOLATE_USE_CACHE);                          /* Rob's trick to speed up */

    /*
     * Intershell abundances are (from the table)
     * n, H1, H2, He3, He4, ...
     * Some isotopes are used in Amanda's code but not in binary_c and the
     * radioactive decay function does not take them into account. So let's copy them
     * in their decay product directly
     */
    dup_material[Xn]=intershell[0];
    dup_material[XH1]=intershell[1];
    dup_material[XH2]=intershell[2];
    dup_material[XHe3]=intershell[3];
    dup_material[XHe4]=intershell[4];
    dup_material[XHe4]+=intershell[7]; // CAB: this is B8, decays in He4, half life=0.77s
#if defined LITHIUM_TABLES
    dup_material[XLi7]=0.0;
#elif defined NUCSYN_LITHIUM_ANGELOU
    /* do nothing */
#else
    dup_material[XLi7]=intershell[5];
#endif

    dup_material[XBe7]=intershell[6];
    dup_material[XC12]=intershell[8];
    dup_material[XC13]=intershell[9];
    dup_material[XN13]=intershell[11];
    dup_material[XN14]=intershell[12];
    dup_material[XN14]+=intershell[10]; // CAB: this is C14, beta- to N14, half life=5700y
    dup_material[XN15]=intershell[13];
    dup_material[XO14]=intershell[14];
    dup_material[XO15]=intershell[15];
    dup_material[XO16]=intershell[16];
    dup_material[XO17]=intershell[17];
    dup_material[XO18]=intershell[18];
    dup_material[XF17]=intershell[20];
    dup_material[XF18]=intershell[21];
    dup_material[XF19]=intershell[22];
    dup_material[XF19]+=intershell[19]; // CAB: this is O19, beta- to F19, half life=27s
    dup_material[XF19]+=intershell[24]; // CAB: this is Ne19, beta+ to F19, half life=17.2s
    dup_material[XNe20]=intershell[25];
    dup_material[XNe20]+=intershell[23]; // CAB: this is F20, beta- to Ne20, half life=11.2s
    dup_material[XNe21]=intershell[26];
    dup_material[XNe21]+=intershell[29]; // CAB: this is Na21, beta+ to Ne21, half life=22.5s
    dup_material[XNe22]=intershell[27];
    dup_material[XNa22]=intershell[30];
    dup_material[XNa23]=intershell[31];
    dup_material[XNa23]+=intershell[28]; // CAB: this is Ne23, beta- to Na23, half life=37.2s
    dup_material[XNa23]+=intershell[33]; // CAB: this is Mg23, e-cap to Na23, half life=11.3s
    dup_material[XMg24]=intershell[34];
    dup_material[XMg24]+=intershell[32]; // CAB: this is Na24, beta- to Mg24, half life=15h
    dup_material[XMg25]=intershell[35];
    dup_material[XMg25]+=intershell[38]; // CAB: this is Al25, beta+ to Mg25, half life=7.2s
    dup_material[XMg26]=intershell[36];
    dup_material[XAl26]=intershell[39]; //CAB: this is Al-6
    dup_material[XAl26]+=intershell[40];//CAB: this is Al*6
    dup_material[XAl27]=intershell[41];
    dup_material[XAl27]+=intershell[37]; // CAB: this is Mg27, beta- to Al27, half life=9.5m
    dup_material[XAl27]+=intershell[43]; // CAB: this is Si27, e-cap to Al27, half life=4.16s
    dup_material[XSi28]=intershell[44];
    dup_material[XSi28]+=intershell[42]; // CAB: this is Al28, beta- to Si28, half life=2.2m
    dup_material[XSi29]=intershell[45];
    dup_material[XSi30]=intershell[46];
    dup_material[XFe56]=intershell[91];

#ifdef NUCSYN_ALL_ISOTOPES

    dup_material[XP31]=intershell[47];
    dup_material[XS32]=intershell[50];
    dup_material[XS32]+=intershell[48]; // CAB: this is P32, beta- to S32, half life=14.3d
    dup_material[XS33]=intershell[51];
    dup_material[XS33]+=intershell[49]; // CAB: this is P33, beta- to S33, half life=25.3d
    dup_material[XS34]=intershell[52];
    dup_material[XS36]=intershell[54];
    dup_material[XCl35]=intershell[55];
    dup_material[XCl35]+=intershell[53]; // CAB: this is S35, beta- to Cl35, half life=87.5d
    dup_material[XCl36]=intershell[56];
    dup_material[XCl37]=intershell[57];
    dup_material[XCl37]+=intershell[59]; // CAB: this is Ar37, e-cap to Cl37, half life=35d
    dup_material[XAr36]=intershell[58];
    dup_material[XAr38]=intershell[60];
    dup_material[XAr40]=intershell[62];
    dup_material[XK39]=intershell[63];
    dup_material[XK39]+=intershell[61]; // CAB: this is Ar39, beta- to K39, half life=269y
    dup_material[XK40]=intershell[64];
    dup_material[XK41]=intershell[65];
    dup_material[XK41]+=intershell[67]; // CAB: this is Ca41, e-cap to K41, half life=1e5y !!!
    dup_material[XCa40]=intershell[66];
    dup_material[XCa42]=intershell[68];
    dup_material[XCa43]=intershell[69];
    dup_material[XCa44]=intershell[70];
    dup_material[XCa46]=intershell[72];
    dup_material[XCa48]=intershell[74];
    dup_material[XSc45]=intershell[75];
    dup_material[XSc45]+=intershell[71]; // CAB: this is Ca45, beta- to Sc45, half life=162d
    dup_material[XTi46]=intershell[79];
    dup_material[XTi46]+=intershell[76]; // CAB: this is Sc46, beta- to Ti46, half life=88d
    dup_material[XTi47]=intershell[80];
    dup_material[XTi47]+=intershell[73]; // CAB: this is Ca47, 2*beta- to Ti47, half life=7.8d
    dup_material[XTi47]+=intershell[77]; // CAB: this is Sc47, beta- to Ti47, half life=3.3d
    dup_material[XTi48]=intershell[81];
    dup_material[XTi48]+=intershell[78]; // CAB: this is Sc48, beta- to Ti48, half life=43.7h
    dup_material[XTi49]=intershell[82];
    dup_material[XTi50]=intershell[83];
    dup_material[XV51]=intershell[84];
    dup_material[XCr52]=intershell[85];
    dup_material[XCr53]=intershell[86];
    dup_material[XCr54]=intershell[87];
    dup_material[XMn55]=intershell[88];
    dup_material[XMn55]+=intershell[90]; // CAB: this is Fe55, e-cap to Mn55, half life=2.7y
    dup_material[XFe54]=intershell[89];
    dup_material[XFe57]=intershell[92];
    dup_material[XFe58]=intershell[93];
    dup_material[XCo59]=intershell[96];
    dup_material[XCo59]+=intershell[94]; // CAB: this is Fe59, beta- to Co59, half life=44.5d
    dup_material[XCo59]+=intershell[99]; // CAB: this is Ni59, e-cap to Co59, half life=7.6e4y
    dup_material[XNi58]=intershell[98];
    dup_material[XNi60]=intershell[100];
    dup_material[XNi60]+=intershell[95]; // CAB: this is Fe60, 2*beta- to Ni60, half life=1.5e6y !!!
    dup_material[XNi60]+=intershell[97]; // CAB: this is Co60, beta- to Ni60, half life=1.9e3d
    dup_material[XNi61]=intershell[101];
    dup_material[XNi62]=intershell[102];
    dup_material[XNi64]=intershell[104];
    dup_material[XNi64]+=intershell[106]; // CAB: this is Cu64, e-cap to Ni64, half life=12.7h
    dup_material[XCu63]=intershell[105];
    dup_material[XCu63]+=intershell[103]; // CAB: this is Ni63, beta- to Cu63, half life=100y
    dup_material[XCu65]=intershell[107];
    dup_material[XCu65]+=intershell[109]; // CAB: this is Zn65, e-cap to Cu65, half life=244d
    dup_material[XZn64]=intershell[108];
    dup_material[XZn66]=intershell[110];
    dup_material[XZn67]=intershell[111];
    dup_material[XZn68]=intershell[112];
    dup_material[XZn70]=intershell[114];
    dup_material[XGa69]=intershell[115];
    dup_material[XGa69]+=intershell[113]; // CAB: this is Zn69, beta- to Ga69, half life=14h
    dup_material[XGa71]=intershell[117];
    dup_material[XGa71]+=intershell[119]; // CAB: this is Ge71, e-cap to Ga71, half life=11.4d
    dup_material[XGe70]=intershell[118];
    dup_material[XGe70]+=intershell[116]; // CAB: this is Ga70, beta- to Ge70, half life=21.1m
    dup_material[XGe72]=intershell[120];
    dup_material[XGe73]=intershell[121];
    dup_material[XGe74]=intershell[122];
    dup_material[XAs75]=intershell[123];
    dup_material[XSe76]=intershell[124];
    dup_material[XSe77]=intershell[125];
    dup_material[XSe78]=intershell[126];
    dup_material[XSe79]=intershell[127];
    dup_material[XSe80]=intershell[128];
    dup_material[XBr79]=intershell[129];
    dup_material[XBr80]=intershell[130];
    dup_material[XBr81]=intershell[131];
    dup_material[XKr80]=intershell[132];
    dup_material[XKr81]=intershell[133];
    dup_material[XKr82]=intershell[134];
    dup_material[XKr83]=intershell[135];
    dup_material[XKr84]=intershell[136];
    dup_material[XKr85]=intershell[137]; //CAB: this is Kr-5
    dup_material[XKr85]+=intershell[138];//CAB: this is Kr*5
    dup_material[XKr86]=intershell[139];
    dup_material[XRb85]=intershell[140];
    dup_material[XRb86]=intershell[141];
    dup_material[XRb87]=intershell[142];
    dup_material[XSr86]=intershell[143];
    dup_material[XSr87]=intershell[144];
    dup_material[XSr88]=intershell[145];
    dup_material[XSr89]=intershell[146];
    dup_material[XSr90]=intershell[147];
    dup_material[XY89]=intershell[148];
    dup_material[XY90]=intershell[149];
    dup_material[XY91]=intershell[150];
    dup_material[XZr90]=intershell[151];
    dup_material[XZr91]=intershell[152];
    dup_material[XZr92]=intershell[153];
    dup_material[XZr93]=intershell[154];
    dup_material[XZr94]=intershell[155];
    dup_material[XZr95]=intershell[156];
    dup_material[XZr96]=intershell[157];
    dup_material[XNb93]=intershell[158];
    dup_material[XNb94]=intershell[159];
    dup_material[XNb95]=intershell[160];
    dup_material[XMo94]=intershell[161];
    dup_material[XMo95]=intershell[162];
    dup_material[XMo96]=intershell[163];
    dup_material[XMo97]=intershell[164];
    dup_material[XMo98]=intershell[165];
    dup_material[XTc99]=intershell[166];
    dup_material[XRu99]=intershell[167];
    dup_material[XRu100]=intershell[168];
    dup_material[XRu101]=intershell[169];
    dup_material[XRu102]=intershell[170];
    dup_material[XRh103]=intershell[171];
    dup_material[XPd104]=intershell[172];
    dup_material[XPd105]=intershell[173];
    dup_material[XPd106]=intershell[174];
    dup_material[XPd107]=intershell[175];
    dup_material[XPd108]=intershell[176];
    dup_material[XPd108]+=intershell[178]; // CAB: this is Ag108, e-cap to Pd108, half life 2.4m
    dup_material[XAg107]=intershell[177];
    dup_material[XAg109]=intershell[179];
    dup_material[XCd108]=intershell[180];
    dup_material[XCd109]=intershell[181];
    dup_material[XCd110]=intershell[182];
    dup_material[XCd111]=intershell[183];
    dup_material[XCd112]=intershell[184];
    dup_material[XCd113]=intershell[185];
    dup_material[XCd114]=intershell[186];
    dup_material[XIn115]=intershell[187];
    dup_material[XSn116]=intershell[188];
    dup_material[XSn117]=intershell[189];
    dup_material[XSn118]=intershell[190];
    dup_material[XSn119]=intershell[191];
    dup_material[XSn120]=intershell[192];
    dup_material[XSb121]=intershell[193];
    dup_material[XTe122]=intershell[194];
    dup_material[XTe123]=intershell[195];
    dup_material[XTe124]=intershell[196];
    dup_material[XTe125]=intershell[197];
    dup_material[XTe126]=intershell[198];
    dup_material[XI127]=intershell[199];
    dup_material[XXe128]=intershell[200];
    dup_material[XXe129]=intershell[201];
    dup_material[XXe130]=intershell[202];
    dup_material[XXe131]=intershell[203];
    dup_material[XXe132]=intershell[204];
    dup_material[XXe133]=intershell[205];
    dup_material[XXe134]=intershell[206];
    dup_material[XCs133]=intershell[207];
    dup_material[XCs134]=intershell[208];
    dup_material[XCs135]=intershell[209];
    dup_material[XCs136]=intershell[210];
    dup_material[XCs137]=intershell[211];
    dup_material[XBa134]=intershell[212];
    dup_material[XBa135]=intershell[213];
    dup_material[XBa136]=intershell[214];
    dup_material[XBa137]=intershell[215];
    dup_material[XBa138]=intershell[216];
    dup_material[XLa139]=intershell[217];
    dup_material[XCe140]=intershell[218];
    dup_material[XCe141]=intershell[219];
    dup_material[XCe142]=intershell[220];
    dup_material[XPr141]=intershell[221];
    dup_material[XNd142]=intershell[222];
    dup_material[XNd143]=intershell[223];
    dup_material[XNd144]=intershell[224];
    dup_material[XNd145]=intershell[225];
    dup_material[XNd146]=intershell[226];
    dup_material[XNd147]=intershell[227];
    dup_material[XNd148]=intershell[228];
    dup_material[XPm147]=intershell[229];
    dup_material[XPm148]=intershell[230];
    dup_material[XSm147]=intershell[231];
    dup_material[XSm148]=intershell[232];
    dup_material[XSm149]=intershell[233];
    dup_material[XSm150]=intershell[234];
    dup_material[XSm151]=intershell[235];
    dup_material[XSm152]=intershell[236];
    dup_material[XEu151]=intershell[237];
    dup_material[XEu152]=intershell[238];
    dup_material[XEu153]=intershell[239];
    dup_material[XEu154]=intershell[240];
    dup_material[XEu155]=intershell[241];
    dup_material[XGd152]=intershell[242];
    dup_material[XGd153]=intershell[243];
    dup_material[XGd154]=intershell[244];
    dup_material[XGd155]=intershell[245];
    dup_material[XGd156]=intershell[246];
    dup_material[XGd157]=intershell[247];
    dup_material[XGd158]=intershell[248];
    dup_material[XTb159]=intershell[249];
    dup_material[XDy160]=intershell[250];
    dup_material[XDy161]=intershell[251];
    dup_material[XDy162]=intershell[252];
    dup_material[XDy163]=intershell[253];
    dup_material[XDy164]=intershell[254];
    dup_material[XHo165]=intershell[255];
    dup_material[XEr166]=intershell[256];
    dup_material[XEr167]=intershell[257];
    dup_material[XEr168]=intershell[258];
    dup_material[XEr169]=intershell[259];
    dup_material[XEr170]=intershell[260];
    dup_material[XTm169]=intershell[261];
    dup_material[XTm170]=intershell[262];
    dup_material[XTm171]=intershell[263];
    dup_material[XYb170]=intershell[264];
    dup_material[XYb171]=intershell[265];
    dup_material[XYb172]=intershell[266];
    dup_material[XYb173]=intershell[267];
    dup_material[XYb174]=intershell[268];
    dup_material[XLu175]=intershell[269];
    dup_material[XLu176]=intershell[270];
    dup_material[XLu177]=intershell[271];
    dup_material[XHf176]=intershell[272];
    dup_material[XHf177]=intershell[273];
    dup_material[XHf178]=intershell[274];
    dup_material[XHf179]=intershell[275];
    dup_material[XHf180]=intershell[276];
    dup_material[XTa181]=intershell[277];
    dup_material[XTa182]=intershell[278];
    dup_material[XTa183]=intershell[279];
    dup_material[XW182]=intershell[280];
    dup_material[XW183]=intershell[281];
    dup_material[XW184]=intershell[282];
    dup_material[XW185]=intershell[283];
    dup_material[XW186]=intershell[284];
    dup_material[XRe185]=intershell[285];
    dup_material[XRe186]=intershell[286];
    dup_material[XRe187]=intershell[287];
    dup_material[XOs186]=intershell[288];
    dup_material[XOs187]=intershell[289];
    dup_material[XOs188]=intershell[290];
    dup_material[XOs189]=intershell[291];
    dup_material[XOs190]=intershell[292];
    dup_material[XOs191]=intershell[293];
    dup_material[XOs192]=intershell[294];
    dup_material[XIr191]=intershell[295];
    dup_material[XIr192]=intershell[296];
    dup_material[XIr193]=intershell[297];
    dup_material[XPt192]=intershell[298];
    dup_material[XPt193]=intershell[299];
    dup_material[XPt194]=intershell[300];
    dup_material[XPt195]=intershell[301];
    dup_material[XPt196]=intershell[302];
    dup_material[XAu197]=intershell[303];
    dup_material[XHg198]=intershell[304];
    dup_material[XHg199]=intershell[305];
    dup_material[XHg200]=intershell[306];
    dup_material[XHg201]=intershell[307];
    dup_material[XHg202]=intershell[308];
    dup_material[XTl203]=intershell[309];
    dup_material[XTl204]=intershell[310];
    dup_material[XTl205]=intershell[311];
    dup_material[XPb204]=intershell[312];
    dup_material[XPb205]=intershell[313];
    dup_material[XPb206]=intershell[314];
    dup_material[XPb207]=intershell[315];
    dup_material[XPb208]=intershell[316];
    dup_material[XBi209]=intershell[317];
    dup_material[XBi210]=intershell[318];
    dup_material[XPo210]=intershell[319];
#endif//NUCSYN_ALL_ISOTOPES

    Dprint("PULSE %g x=%g %g %g %g set dup material He4=%g C12=%g Ne20=%g Ne22=%g Mg24=%g Mg25=%g Mg26=%g\n",
           newstar->num_thermal_pulses,
           x[0],x[1],x[2], x[3],
           dup_material[XHe4],
           dup_material[XC12],
           dup_material[XNe20],
           dup_material[XNe22],
           dup_material[XMg24],
           dup_material[XMg25],
           dup_material[XMg26]);



#ifdef NUCSYN_THIRD_DREDGE_UP_MULTIPLIERS
    /*
     * Allow boost of dredge-up material
     */
    if(stardata->preferences->boost_third_dup==TRUE)
    {
        Nucsyn_isotope_stride_loop(
            dup_material[i]*=stardata->preferences->third_dup_multiplier[i];
        );
    }
#endif

    /*
     * Normalize to 1.0 by putting the error in helium (the
     * most abundant element)
     */
    dup_material[XHe4]=0.0;
    dup_material[XHe4]=1.0-nucsyn_totalX(dup_material);

#if (DEBUG==1)
    {
        Nucsyn_isotope_stride_loop(
            if((dup_material[i]<0)||(dup_material[i]>1.0))
            {
                Exit_binary_c(BINARY_C_OUT_OF_RANGE,
                              "Isotope %d = %g : this cannot be correct!\n",
                              i,dup_material[i]);
            }
            );
    }

    Dprint("TOTAL X = %g\n",nucsyn_totalX(dup_material));

    Dprint("INTERSHELL H1=%3.3e He4=%3.3e C12=%3.3e O16=%3.3e Ne20=%3.3e Ne21=%3.3e Ne22=%3.3e Mg24=%3.3e Mg25=%3.3e Mg26=%3.3e Al27=%3.3e Si28=%3.3e Si29=%3.3e, SUM %g\n",
           dup_material[XH1],
           dup_material[XHe4],
           dup_material[XC12],
           dup_material[XO16],
           dup_material[XNe20],
           dup_material[XNe21],
           dup_material[XNe22],
           dup_material[XMg24],
           dup_material[XMg25],
           dup_material[XMg26],
           dup_material[XAl27],
           dup_material[XSi28],
           dup_material[XSi29],
           nucsyn_totalX(dup_material)
        );
#endif //(DEBUG==1)
#endif // USE_TABULAR_INTERSHELL_ABUNDANCES_KARAKAS_2012




    /* add up everything assume everything else is He4 */
    Dprint("3DUP tot %g (H1 %g He4 %g C12 %g O16 %g Ne22 %g Al26 %g surfAl26=%g)\n",
           nucsyn_totalX(dup_material),
           dup_material[XH1],
           dup_material[XHe4],
           dup_material[XC12],
           dup_material[XO16],
           dup_material[XNe22],
           dup_material[XAl26],
           newstar->Xenv[XAl26]);

}
#endif /* NUCSYN */
