#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef NUCSYN
Abundance Pure_function * nucsyn_observed_surface_abundances(struct star_t * Restrict const star)
{
    /*
     * This function selects one of Xenv or Xacc depending on whether
     * there is an accretion layer. The work is now done by the
     * Observed_surface macro.
     */
    return Observed_surface(star);
}

#endif // NUCSYN
