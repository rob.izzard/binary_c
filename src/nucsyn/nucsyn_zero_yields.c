#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef NUCSYN

/*
 * Zero the yields in a star struct
 */
void nucsyn_zero_yields(struct star_t * const Restrict star)
{
    memset(star->Xyield,0,ISOTOPE_MEMSIZE);
#ifdef NUCSYN_ID_SOURCES
    Sourceloop(j)
    {
        if(star->Xsource[j] != NULL)
        {
            memset(star->Xsource[j],0,ISOTOPE_MEMSIZE);
        }
    }
#endif // NUCSYN_ID_SOURCES
    star->other_yield = 0.0;
}

#endif // NUCSYN
