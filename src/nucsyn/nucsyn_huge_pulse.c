#include "../binary_c.h"
No_empty_translation_unit_warning;

#if defined NUCSYN &&                           \
    defined NUCSYN_HUGE_PULSE_CRISTALLO

#define SQUARE_BRACKET(A,B)                                             \
    nucsyn_square_bracket(star->Xenv,                                   \
                          stardata->common.Xsolar,                      \
                          (A),(B))
/*
 * Simulate the "huge pulse" at the first thermal pulse
 */
void nucsyn_huge_pulse(struct star_t * Restrict const star,
                       struct stardata_t * Restrict const stardata)
{
    const double m=star->mass;
    const double mc = star->mc_1tp;
    const double menv=star->mass - mc;

    if((Less_or_equal(m , 2.0))&&
       (More_or_equal(m, 0.5)) && (menv > 0.0))
    {
        /*
         * Based on Sergio's Z=5e-5 ([Fe/H]=-2.3, using the new
         * solar abundances, I guess!) models
         */

        /* table given to me by Sergio */
        Const_data_table table[]={
            0.85,       2.56E-02,       2.23E-01,       1.40E-02,       1.82E-02,       2.95E-02,       4.02E-07,7.91E-05,      3.99E-08,
            1.00,       2.10E-02,       1.53E-01,       1.96E-02,       9.78E-03,       2.71E-02,       3.96E-07,4.60E-05,      3.19E-08,
            1.50,       1.70E-02,       1.64E-01,       1.85E-02,       9.05E-03,       2.84E-02,       3.66E-07,3.97E-05,      2.54E-08,
            2.00,       9.00E-03,       1.46E-01,       2.71E-02,       5.15E-03,       3.08E-02,       4.72E-07,2.21E-05,      1.37E-08
        };


        /* Interpolate and put the results in array x */
        double x[8];
        double coeffs[1]={Max(0.85,m)};
        interpolate(table,1,8,4,coeffs,x,TABLE_HUGE_PULSE_CRISTALLO,FALSE);
        double mdup=x[0]; /* mass dredged up */

        /* set abundances of dredged up material */
        Abundance *Xintershell = New_clear_isotope_array;
        Xintershell[XC12]=x[1];
        Xintershell[XC13]=x[4];
        Xintershell[XN14]=x[2];
        Xintershell[XN15]=x[5];
        Xintershell[XO16]=x[3];
        Xintershell[XO17]=x[6];
        Xintershell[XO18]=x[7];

        /* everything else is helium */
        Xintershell[XHe4]=1.0-nucsyn_totalX(stardata,Xintershell);

        Dprint("Sergio table : m=%g (star->mass=%g, mc=%g menv=%g) -> intershell He=%g C12=%g C13=%g N14=%g N15=%g O16=%g O17=%g O18=%g\n",
               m,star->mass,
               Outermost_core_mass(star),
               menv,
               Xintershell[XHe4],
               Xintershell[XC12],Xintershell[XC13],
               Xintershell[XN14],Xintershell[XN15],
               Xintershell[XO16],Xintershell[XO17],
               Xintershell[XO18]);

        Dprint("Before huge pulse: H1=%g He4=%g C12=%g C13=%g N14=%g N15=%g O16=%g O17=%g O18=%g, with [C/Fe]=%g [N/Fe]=%g [O/Fe]=%g [C/N]=%g\n",
               star->Xenv[XH1],
               star->Xenv[XHe4],
               star->Xenv[XC12],
               star->Xenv[XC13],
               star->Xenv[XN14],
               star->Xenv[XN15],
               star->Xenv[XO16],
               star->Xenv[XO17],
               star->Xenv[XO18],
               SQUARE_BRACKET(XC12,XFe56),
               SQUARE_BRACKET(XN14,XFe56),
               SQUARE_BRACKET(XO16,XFe56),
               SQUARE_BRACKET(XC12,XN14));

        Dprint("mix menv = %g (m=%g mc=%g st=%d), mdup=%g\n",menv,star->mass,mc,star->stellar_type,mdup);

        /* mix into the envelope */
        nucsyn_mix_shells(menv,star->Xenv,
                          mdup,Xintershell);

        Dprint("After huge pulse : H1=%g He4=%g C12=%g C13=%g N14=%g N15=%g O16=%g O17=%g O18=%g, with [C/Fe]=%g [N/Fe]=%g [O/Fe]=%g [C/N]=%g\n",
               star->Xenv[XH1],
               star->Xenv[XHe4],
               star->Xenv[XC12],
               star->Xenv[XC13],
               star->Xenv[XN14],
               star->Xenv[XN15],
               star->Xenv[XO16],
               star->Xenv[XO17],
               star->Xenv[XO18],
               SQUARE_BRACKET(XC12,XFe56),
               SQUARE_BRACKET(XN14,XFe56),
               SQUARE_BRACKET(XO16,XFe56),
               SQUARE_BRACKET(XC12,XN14));
    }


}

#endif //NUCSYN_HUGE_PULSE_CRISTALLO && NUCSYN
