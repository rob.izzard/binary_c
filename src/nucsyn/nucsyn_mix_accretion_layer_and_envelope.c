#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef NUCSYN
void nucsyn_mix_accretion_layer_and_envelope(struct stardata_t * const stardata,
                                             struct star_t * Restrict const star,
                                             double menv)
{
    /*
     * Function to mix the accretion layer or a star and its
     * envelope so that there is no longer an accretion layer
     *
     * if the envelope mass is <0 then set menv according to the
     * current stellar core mass
     */
    if(menv < 0.0)
    {
        menv = star->mass - Outermost_core_mass(star) - star->dmacc;
    }

    Dprint("\nMixing star %d's accretion layer with envelope (M=%g Mc=%g type=%d)\n",
           star->starnum,
           star->mass,
           Outermost_core_mass(star),
           star->stellar_type);
    Dprint("Was :\n");
    Dprint("ENV : menv=%g : H1=%g He4=%g C12=%g\n",menv,
           star->Xenv[XH1],star->Xenv[XHe4],star->Xenv[XC12]);
    Dprint("ACC : macc=%g : H1=%g He4=%g C12=%g\n",star->dmacc,
           star->Xacc[XH1],star->Xacc[XHe4],star->Xacc[XC12]);
    Dprint("type=%d M=%g Menv=%g dmacc=%g\n",
           star->stellar_type,star->mass,menv,star->dmacc);

    if(star->dmacc > TINY && menv > TINY)
    {
        // allocate memory
        nucsyn_dilute_shell(menv, // envelope mass
                            // (not including the accretion layer!)
                            star->Xenv, // envelope abundance
                            star->dmacc, // accretion layer thickness
                            star->Xacc);

        star->dmacc=0.0; // remove the accretion layer
        Dprint("mixed\n");
    }
    else
    {
        Dprint("Mix skipped (dmacc<TINY)\n");
    }
}
#endif // NUCSYN
