#pragma once
#ifndef NUCSYN_ISOTOPES_H
#define NUCSYN_ISOTOPES_H

#include "../binary_c_parameters.h"

#ifdef NUCSYN

#include "nucsyn_strided_isotope_loops.h"

/*
 * The isotopes used in binary_c are stored in an array
 *
 * This array is an array of double precision floating point
 * numbers ('doubles') and contains ISOTOPE_ARRAY_SIZE elements
 *
 * The elements are individually referred to using the macros defined below as
 * XH1, XHe4, XC12 etc. but you should use the macros Set_isotopic_property
 * and Increment_isotopic_property to set e.g. abundances.
 *
 * NB all functions should access the isotopes via the X... macros defined
 * below, unless looping over all the isotopes (e.g. to mix things) in which
 * case you don't care which isotope is which, just that it's there. In this
 * way direct isotope access is removed and the system is totally flexible :
 * assuming you keep this file up to date. It is also as fast as you'd like it,
 * because all the macros are compiled in. Just be careful if you change
 * something! You'll have to recompile all objects/libraries which use these
 * macro definitions.
 *
 * If you change the size of arrays, you'll need to do a full rebuild.
 *
 * Extra, strided, loop macros are in nucsyn_strided_isotope_loops.h
 * Really, you should use these if you can, but they rely on the
 * look being vectorizable (i.e. no dependence from one isotope to the next).
 */


/*
 * If NUCSYN_ALL_ISOTOPES is defined, then binary_c uses > 400 isotopes.
 *
 * If it is not, a reduced set of ~30 isotopes is used instead, which
 * makes the code a lot faster, but only includes light isotopes and Fe56.
 */

#define NUCSYN_ALL_ISOTOPES
//#undef NUCSYN_ALL_ISOTOPES

/* load in isotope list */
#include "nucsyn_isotope_list.h"

/*******************************************************************/
/* REMEMBER to set a nuclear mass for each of the isotopes YOU ADD */
/*******************************************************************/

// S-process macros
#define S_process_hs_abundance(X) (0.5*(nucsyn_square_bracket((X),XY,XFe) +          \
                      nucsyn_square_bracket((X),XZr,XFe)))
#define S_process_ls_abundance(X) (0.2*(nucsyn_square_bracket((X),XBa,XFe) +         \
                      nucsyn_square_bracket((X),XLa,XFe) +         \
                      nucsyn_square_bracket((X),XCe,XFe) +         \
                      nucsyn_square_bracket((X),XNd,XFe) +         \
                      nucsyn_square_bracket((X),XSm,XFe)))
#define S_process_hs_minus_ls(X) (S_process_hs_abundance(X) - S_process_ls_abundance(X))
#define S_process_overabundance(X) ((X[XNd] + X[XY])/\
                            (0.02*1.61434282262822e-07 +0.02*5.58660876791233e-07))

// below this abundance, an isotope may be considered to have zero abundance
#define ABUND_ZERO 1e-30

/*
 * Macro to check if an isotope number A is valid
 */
#define Isotope_is_valid(A) (likely((A)<(ISOTOPE_ARRAY_SIZE)))

/*
 * Safe macros to set/increment abundances, nuclear masses etc.
 * The property of the isotope to be set (or incremented) is X.
 * The isotope is A and the value (or change of value) is B.
 */
#define Set_isotopic_property(X,A,B) {if(Isotope_is_valid(A)) *((X)+(A))=(B);}
#define Increment_isotopic_property(X,A,B) {if(Isotope_is_valid(A)) *((X)+(A))+=(B);}

/*
 * Isotope loops can be forward or backwards. Often it doesn't
 * matter, and you can choose whichever is fastest (usually
 * forward). These all skip the electrons.
 *
 * Note: these are simple, linear loops. See nucsyn_strided_isotope_loops.h
 *       for faster options.
 */

/*
 * Forward-ordered isotope loop, skips electrons
 */
#define Forward_isotope_loop(I)                             \
    Forward_isotope_loop_implementation(I,                  \
                                        Forwardisotopeloop, \
                                        __COUNTER__)

#define Forward_isotope_loop_implementation(I,LABEL,LINE)   \
    for(Isotope I,Concat3(brk,LABEL,LINE)=1;                \
        Concat3(brk,LABEL,LINE)==1;                         \
        Concat3(brk,LABEL,LINE)=0)                          \
        for((I) = 0;                                        \
                                                            \
            (I) < (Isotope)(ISOTOPE_ARRAY_SIZE);            \
                                                            \
            (I) += (Isotope)((I) == (Xe-1) ? 2 : 1)         \
            )


/*
 * Reverse order loop, skips electrons.
 */
#define Reverse_isotope_loop(I)                             \
    Reverse_isotope_loop_implementation(I,                  \
                                        Reverseisotopeloop, \
                                        __COUNTER__)

#define Reverse_isotope_loop_implementation(I,LABEL,LINE)   \
    for(Isotope I,Concat3(brk,LABEL,LINE)=1;                \
        Concat3(brk,LABEL,LINE)==1;                         \
        Concat3(brk,LABEL,LINE)=0)                          \
        for((I) = (ISOTOPE_ARRAY_SIZE)-1;                   \
                                                            \
            (I) != (Isotope)(-1);                           \
                                                            \
            (I) -= (Isotope)((I) == (Xe+1) ? 2 : 1)         \
            )

/*
 * With the latest gcc (11) or clang (12), the forward ordered loop
 * is fastest.
 */
#define Isotope_loop(I) Forward_isotope_loop(I)

/*
 * Make a new isotope array
 */
#define New_isotope_array Malloc((ISOTOPE_MEMSIZE))
#define New_clear_isotope_array calloc(ISOTOPE_ARRAY_SIZE,  \
                                       sizeof(Abundance))


/*
 * Clear an isotope array (including electrons)
 */
#define Clear_isotope_array(X) memset((Abundance*)(X),          \
                                      0,                        \
                                      (ISOTOPE_MEMSIZE))

/*
 * Copy abundances : the use of memcpy, which should be fastest,
 * relies on the regions NOT overlapping.
 */
#define Copy_abundances(FROM,TO) memcpy((Abundance*)(TO),       \
                                        (Abundance*)(FROM),     \
                                        (ISOTOPE_MEMSIZE))

#define New_isotope_array_from(X) Copy_abundances((X),                  \
                                                  (New_isotope_array))

#endif /* NUCSYN */
#endif /* NUCSYN_ISOTOPES_H */
