#pragma once
#ifndef NUCSYN_H
#define NUCSYN_H

// Wrapper to call all the header files required for the nucsyn library to
// work.

#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef NUCSYN

/* function declarations */
#include "nucsyn_functions.h"

// parameters
#include "nucsyn_parameters.h"

/* core-collapse supernovae */
#include "nucsyn_sn_core_collapse.h"
#include "nucsyn_sn_core_collapse_rprocess.h"
#include "nucsyn_sn_electron_capture.h"

// then macros
#include "nucsyn_macros.h"
#include "nucsyn_isotopes.h"
#include "nucsyn_elements.h"
#include "nucsyn_beta_decay_timescales.h"

// then the structures
#include "nucsyn_structures.h"
#endif /* NUCSYN */

// then the prototypes
#include "nucsyn_prototypes.h"


#endif /* NUCSYN_H */
