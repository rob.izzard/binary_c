#pragma once
#ifndef NUCSYN_NETWORKS_H
#define NUCSYN_NETWORKS_H


/*
 * Nuclear networks in binary_c header file
 *
 * See nucsyn_networks.def for the main
 * list of networks.
 */

#include "nucsyn_networks.def"

/*
 * Make enum list for the NUCSYN_NETWORK_XXX values
 */
#undef X
#define X(CODE,ONOFF,STRING,KAPS_RENTROP,LSODA,CVODE,CHECKFUNC) NUCSYN_NETWORK_##CODE,
enum { NUCSYN_NETWORKS_LIST };
#undef X

/*
 * Make strings for logging
 */
#define X(CODE,ONOFF,STRING,KAPS_RENTROP,LSODA,CVODE,CHECKFUNC) STRING,
static const char * const nuclear_network_strings[] = { NUCSYN_NETWORKS_LIST };
#undef X


/* Numerical burning options */

/*
 * Define this to allow cool CN(O?) burning over long
 * timescales - this actually speeds up the code during pp chains
 * (and possibly elsewhere!)
 */
#define NUCSYN_ALLOW_COOL_CNO_BURNING


/*
 * For the non-analytic burn allow only so
 * many attempts to solve the equations
 */
#define NUCSYN_NETWORK_BURN_MAX_FAILURE_COUNT 10000

/*
 * Enable this to explicitly preserve the number of nucleons
 * (i.e. baryons) in the burning code. This cleans up small
 * numerical problems which become big if allowed to propagate.
 */
#define NUCSYN_NORMALIZE_NUCLEONS

/*
 * Default error on nuclear network
 */
#define NUCSYN_NETWORK_DEFAULT_ERROR 1e-3




/**********************************************************/
/* the following code is kept for historical reasons only */
/**********************************************************/

#ifdef ___DEPRECATED




/************************************************************
 * Analytic solution method options
 *
 * Note: these have not been used for a decade or more.
 * Please use the numerical solver, it's faster and more
 * flexible.
 */

#ifdef NUCSYN_ANAL_BURN

/* Activate CN cycle in HBB : you want this */
#define NUCSYN_TPAGB_HBB_CN_CYCLE

/* Sometimes, particularly at the beginning of HBB, there
 * are no solutions to the CNO equations (which means they
 * oscillate) - allow this to just assume there is a stable
 * solution (ignore the oscillation)
 */
#define NUCSYN_CNO_ASSUME_STABLE

/* Activate ON cycle in HBB : you want this */
#define NUCSYN_TPAGB_HBB_ON_CYCLE

/* Activate NeNa cycle in HBB : you want this */
#define NUCSYN_TPAGB_HBB_NeNa_CYCLE

/* Leak from NeNa to MgAl : you want this (effect is usually small) */
#define NUCSYN_NENA_LEAK
//#define NUCSYN_NENA_LEAK_DEBUG

/* estimate Na22 abundance */
#define NUCSYN_Na22_EQUILIBRIUM

/* Activate MgAl chain in HBB : you want this */
#define NUCSYN_TPAGB_HBB_MgAl_CYCLE

/*
 * Allow Al26m state : this has an effect of 0.06 dex
 * in the M=6, Z=0.0001, less at lower mass/higher metallicity
 */
#define NUCSYN_Al26m

/* Activate Al27(p,a)Mg24 (should be negligible) */
#define NUCSYN_MGAL_LEAKBACK

#endif // NUCSYN_ANAL_BURN





/*
 * Define this to renormalize small (numerical?) errors in the HBB?
 * NOTE: bad if you want accurate molecular weights
 */
//#define NUCSYN_HBB_RENORMALIZE_MASS_FRACTIONS

#endif // ___DEPRECATED



#endif // NUCSYN_NETWORKS_H
