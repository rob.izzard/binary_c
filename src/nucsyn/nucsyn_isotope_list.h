#pragma once
#ifndef NUCSYN_ISOTOPE_LIST_H
#define NUCSYN_ISOTOPE_LIST_H


#include "nucsyn_isotope_list.def"

/*
 * Construct isotopes
 */
#undef X

/*
 * Make the list of isotopes.
 *
 * If we have NUCSYN_ALL_ISOTOPES defined, we use
 * both light and heavy, otherwise just light.
 *
 * If we only use the light isotopes, define
 * NUCSYN_MISSING_ISOTOPE to the isotope that
 * will store the "missing metals". Usually this is
 * Si30, because we don't care very much about Si30.
 */
#ifdef NUCSYN_ALL_ISOTOPES
#define NUCSYN_ISOTOPE_LIST                     \
    NUCSYN_ISOTOPE_LIST_LIGHT                   \
    NUCSYN_ISOTOPE_LIST_HEAVY
#else
#define NUCSYN_ISOTOPE_LIST                     \
    NUCSYN_ISOTOPE_LIST_LIGHT
#endif // NUCSYN_ALL_ISOTOPES

/*
 * Enumerate the list of isotopes, with the
 * final being __ISOTOPE_ARRAY_SIZE which
 * is mapped to ISOTOPE_ARRAY_SIZE
 */
#define X(ISOTOPE,Z,A,M) X##ISOTOPE,
enum {
    NUCSYN_ISOTOPE_LIST
    __ISOTOPE_ARRAY_SIZE
};

#define ISOTOPE_ARRAY_SIZE ((Isotope)__ISOTOPE_ARRAY_SIZE)
#define ISOTOPE_MEMSIZE (sizeof(Abundance)*(ISOTOPE_ARRAY_SIZE))

#ifndef NUCSYN_ALL_ISOTOPES
/* missing metals go into NUCSYN_MISSING_ISOTOPE */
enum { NUCSYN_MISSING_ISOTOPE = XSi30 };
#endif

#undef X
#define X(ISOTOPE,Z,A,M) #ISOTOPE,
static const char * const nucsyn_isotope_macros[] Maybe_unused = { NUCSYN_ISOTOPE_LIST };

#undef X
#define X(ISOTOPE,Z,A,M) #ISOTOPE,
static const char * const nucsyn_isotope_strings[] Maybe_unused = { NUCSYN_ISOTOPE_LIST };

#define nucsyn_isotope_list_string(N) Array_string(nucsyn_isotope_strings,(N))

#undef X
#define X(ISOTOPE,Z,A,M) Z,
static const Atomic_number atomic_numbers[] Maybe_unused = { NUCSYN_ISOTOPE_LIST };

#undef X
#define X(ISOTOPE,Z,A,M) A,
static const Nucleon_number nucleon_numbers[] Maybe_unused = { NUCSYN_ISOTOPE_LIST };

#undef X
#define X(ISOTOPE,Z,A,M) M,
static const double atomic_masses[] Maybe_unused = { NUCSYN_ISOTOPE_LIST };


#undef X


#endif // NUCSYN_ISOTOPE_LIST_H
