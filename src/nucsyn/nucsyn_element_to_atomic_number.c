#include "../binary_c.h"
No_empty_translation_unit_warning;

/*
 * Convert an element string to atomic number
 *
 * If USEHASH is defined, we build a hash and use that
 * for the lookups. Otherwise a simple, but inefficient,
 * linear array search is performed.
 *
 * If no element is found:
 * if error_strategy == 0 : return -1
 * if error_strategy == 1 : exit
 */
#ifdef NUCSYN



#ifdef __HAVE_HSEARCH_DATA__
#include <search.h>
#define USEHASH
#endif // __HAVE_HSEARCH_DATA__

/* hash debugging */
//#define HDEBUG

#ifdef USEHASH
struct element_info_t { Atomic_number Z; } ;
#endif

Atomic_number nucsyn_element_to_atomic_number(struct store_t * Restrict const store Maybe_unused,
                                              const char * Restrict const element,
                                              const unsigned int error_strategy Maybe_unused)
{

    static const char element_strings[][4] = NUCSYN_SHORT_ELEMENT_STRINGS;

#ifdef USEHASH
    if(element==NULL)
    {
        if(store->atomic_number_hash!=NULL)
        {
            /*
             * Destroy the hash and free its memory
             */
            hdestroy_r(store->atomic_number_hash);
            Safe_free(store->atomic_number_hash);
            Safe_free(store->element_info);
        }
        else
        {
            /*
             * Build the lookup hash
             * see e.g. http://pleac.sourceforge.net/pleac_cposix/hashes.html
             */

            Atomic_number Z;
            /* allocate memory */
            store->element_info = Calloc(sizeof(struct element_info_t),NUMBER_OF_ELEMENTS);
            store->atomic_number_hash = Calloc(sizeof(struct hsearch_data),1);

            if(hcreate_r(NUMBER_OF_ELEMENTS,store->atomic_number_hash) == 0)
            {
                Exit_binary_c_no_stardata(BINARY_C_ALLOC_FAILED,"Could not allocate hash for elemental number conversion\n");
            }

            /* enter the element data into the hash table */
            for(Z=0;Z<NUMBER_OF_ELEMENTS;Z++)
            {
                /* data for the hash */
                store->element_info[Z].Z = Z;
#ifdef HDEBUG
                printf("Set element info Z=%d from %d\n",
                       store->element_info[Z].Z,Z);
#endif//HDEBUG

                /* set the hash item */
                ENTRY item;
                ENTRY * ret;
                item.key = (char*)element_strings[Z];
                item.data = &(store->element_info[Z]);
                hsearch_r(item,ENTER,&ret,store->atomic_number_hash);

#ifdef HDEBUG
                printf("set %s with data %p with Z=%d\n",
                       element_strings[Z],
                       (void*)item.data,
                       ((struct element_info_t*)(item.data))->Z
                    );

                /* test that entry worked */
                ENTRY *found_item;
                hsearch_r(item,FIND,&found_item,store->atomic_number_hash);
                printf("found %p with Z=%d\n",found_item,
                       ((struct element_info_t*)(found_item->data))->Z
                    );
#endif//HDEBUG
            }
        }
    }
    else
    {
        ENTRY item;
        ENTRY *found_item;
        item.key = (char*) element;
        item.data = NULL;
        hsearch_r(item,FIND,&found_item,store->atomic_number_hash);

        if(found_item == NULL)
        {
            if(error_strategy == 1)
            {
                Exit_binary_c_no_stardata(BINARY_C_OUT_OF_RANGE,
                                          "no atomic number found for element %s\n",
                                          element);
            }
            else
            {
                return -1;
            }
        }
        else
        {
            const Atomic_number Z = ((struct element_info_t*)(found_item->data))->Z;
#ifdef HDEBUG
            printf("element %s gave Z=%d\n",element,Z);
#endif
            return Z;
        }
    }

#else
    /*
     * Non-hashed version
     */

    if(element!=NULL)
    {
        /*
         * Simple array search.
         */
        Atomic_number Z = -1,i;
        for(i=0;i<NUMBER_OF_ELEMENTS;i++)
        {
            if(strncmp(element_strings[i],element,3)==0)
            {
                Z = i;
                break;
            }
        }

        if(Z==-1)
        {
            Exit_binary_c_no_stardata(BINARY_C_OUT_OF_RANGE,
                                      "Element %s failed to match any element in nature... please check!\n",
                                      element);
        }
        else
        {
            return Z;
        }
    }
#endif //USEHASH

    return 0;
}


#endif // NUCSYN
