#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef NUCSYN

/*
 * Returns a string containing yield details.
 *
 * The user should free the string.
 */
char * nucsyn_yield_string(struct star_t * const star)
{
    char * string = NULL;
    if(asprintf(&string,
                "Star %d yieldtot %g",
                star->starnum,
                sum_C_double_array(star->Xyield, ISOTOPE_ARRAY_SIZE))>0)
    {
#ifdef NUCSYN_ID_SOURCES
        Sourceloop(j)
        {
            const double yield = sum_C_double_array(star->Xsource[j],ISOTOPE_ARRAY_SIZE);
            if(Is_not_zero(yield))
            {
                char * c = NULL;
                if(asprintf(&c,
                            "%s %s=%g ",
                            string,
                            Source_string(j),
                            yield)>0)
                {
                    string = c;
                }
            }
        }
#endif // NUCSYN_ID_SOURCES
    }
    return string;
}

#endif // NUCSYN
