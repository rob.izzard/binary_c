#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef NUCSYN

Moles Pure_function nucsyn_mole_fraction(const Isotope i, /* the species */
                                         const Abundance * Restrict const X, /* mass fractions */
                                         const Abundance * Restrict const mnuc /* nuclear masses */)
{
    /*
     * Function to calculate the mole fraction (or the number fraction) of a
     * species
     */
    Moles molefrac=0.0;
    Isotope_loop(j)
    {
        molefrac += X[j] / mnuc[j];
    }
    return X[i] / (molefrac * mnuc[i]);
}

#endif // NUCSYN
