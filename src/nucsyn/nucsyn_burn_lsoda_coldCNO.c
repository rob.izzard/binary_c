#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef NUCSYN

/*
 * Network code : coldCNO
 * Automatically generated by make_reaction_network_code.pl
 */

/*
 * Reaction SIGMAV_T12 C12(H1,gamma0)C13
 * Alpha=C12, beta=H1, gamma=gamma0, delta=C13
 * m=1, n=1, p=1, q=1
 */
/*
 * Reaction SIGMAV_T13 C13(H1,gamma0)N14
 * Alpha=C13, beta=H1, gamma=gamma0, delta=N14
 * m=1, n=1, p=1, q=1
 */
/*
 * Reaction SIGMAV_T14 N14(H1,gamma0)N15
 * Alpha=N14, beta=H1, gamma=gamma0, delta=N15
 * m=1, n=1, p=1, q=1
 */
/*
 * Reaction SIGMAV_T15 N15(H1,He4)C12
 * Alpha=N15, beta=H1, gamma=He4, delta=C12
 * m=1, n=1, p=1, q=1
 */
/*
 * Reaction SIGMAV_T15_BRANCH N15(H1,gamma0)O16
 * Alpha=N15, beta=H1, gamma=gamma0, delta=O16
 * m=1, n=1, p=1, q=1
 */
/*
 * Reaction SIGMAV_T16 O16(H1,gamma0)O17
 * Alpha=O16, beta=H1, gamma=gamma0, delta=O17
 * m=1, n=1, p=1, q=1
 */
/*
 * Reaction SIGMAV_T17 O17(H1,He4)N14
 * Alpha=O17, beta=H1, gamma=He4, delta=N14
 * m=1, n=1, p=1, q=1
 */
/*
 * Reaction SIGMAV_O17G O17(H1,gamma0)O18
 * Alpha=O17, beta=H1, gamma=gamma0, delta=O18
 * m=1, n=1, p=1, q=1
 */
/*
 * Reaction SIGMAV_O18A O18(H1,He4)N15
 * Alpha=O18, beta=H1, gamma=He4, delta=N15
 * m=1, n=1, p=1, q=1
 */
/*
 * Reaction SIGMAV_O18G O18(H1,gamma0)F19
 * Alpha=O18, beta=H1, gamma=gamma0, delta=F19
 * m=1, n=1, p=1, q=1
 */
/*
 * Reaction SIGMAV_F19A F19(H1,He4)O16
 * Alpha=F19, beta=H1, gamma=He4, delta=O16
 * m=1, n=1, p=1, q=1
 */


/*
 * Species / Reactions list:
 *
 * 1 : H1 : -C12*H1*SIGMAV_T12 -C13*H1*SIGMAV_T13 -N14*H1*SIGMAV_T14 -N15*H1*SIGMAV_T15 -N15*H1*SIGMAV_T15_BRANCH -O16*H1*SIGMAV_T16 -O17*H1*SIGMAV_T17 -O17*H1*SIGMAV_O17G -O18*H1*SIGMAV_O18A -O18*H1*SIGMAV_O18G -F19*H1*SIGMAV_F19A
 * 2 : He4 : +N15*H1*SIGMAV_T15 +O17*H1*SIGMAV_T17 +O18*H1*SIGMAV_O18A +F19*H1*SIGMAV_F19A
 * 3 : C12 : -C12*H1*SIGMAV_T12 +N15*H1*SIGMAV_T15
 * 4 : C13 : +C12*H1*SIGMAV_T12 -C13*H1*SIGMAV_T13
 * 5 : N14 : +C13*H1*SIGMAV_T13 -N14*H1*SIGMAV_T14 +O17*H1*SIGMAV_T17
 * 6 : N15 : +N14*H1*SIGMAV_T14 -N15*H1*SIGMAV_T15 -N15*H1*SIGMAV_T15_BRANCH +O18*H1*SIGMAV_O18A
 * 7 : O16 : +N15*H1*SIGMAV_T15_BRANCH -O16*H1*SIGMAV_T16 +F19*H1*SIGMAV_F19A
 * 8 : O17 : +O16*H1*SIGMAV_T16 -O17*H1*SIGMAV_T17 -O17*H1*SIGMAV_O17G
 * 9 : O18 : +O17*H1*SIGMAV_O17G -O18*H1*SIGMAV_O18A -O18*H1*SIGMAV_O18G
 * 10 : F19 : +O18*H1*SIGMAV_O18G -F19*H1*SIGMAV_F19A
 *
 */
static void coldCNOderivatives_lsoda(double t Maybe_unused,
                                     double * y,
                                     double * dydt,
                                     void * data);

double nucsyn_burn_lsoda_coldCNO(struct stardata_t * Restrict const stardata,
                                 double * Restrict const Nin,
                                 const double * Restrict const sigmav,
                                 const double h)
{
    double err = 0.0;
#define NSPECIES 10
    double ycoldCNO[NSPECIES+1];

    ycoldCNO[0]=Nin[XH1];
    ycoldCNO[1]=Nin[XHe4];
    ycoldCNO[2]=Nin[XC12];
    ycoldCNO[3]=Nin[XC13];
    ycoldCNO[4]=Nin[XN14];
    ycoldCNO[5]=Nin[XN15];
    ycoldCNO[6]=Nin[XO16];
    ycoldCNO[7]=Nin[XO17];
    ycoldCNO[8]=Nin[XO18];
    ycoldCNO[9]=Nin[XF19];

    struct lsoda_t * l = stardata->tmpstore->lsoda;

    if(unlikely(l == NULL))
    {
        /* make lsoda space and set it up */
        stardata->tmpstore->lsoda = Malloc(sizeof(struct lsoda_t));
        l = stardata->tmpstore->lsoda;
        lsoda_setup(l);
    }

    l->Jacobian = NULL;

    /* data passed in to the derivatives function */
    struct lsoda_data_t data;
    data.sigmav = (double*)sigmav;
    data.N = (double*)Nin;


    l->max_steps = INT_MAX; // max number of lsoda steps
    double t = 0.0e0;

    const int istate =
        n_lsoda(l,
                ycoldCNO,
                NSPECIES,
                &t,
                h,
                1e-6,
                &coldCNOderivatives_lsoda,
                &data);

    if(istate != 0)
    {
        Exit_binary_c(BINARY_C_NUCSYN_SOLVER_FAILED,
                      "LSODA error (coldCNO) : istate %d\n",
                      istate);
    }

    Nin[XH1]=ycoldCNO[0];
    Nin[XHe4]=ycoldCNO[1];
    Nin[XC12]=ycoldCNO[2];
    Nin[XC13]=ycoldCNO[3];
    Nin[XN14]=ycoldCNO[4];
    Nin[XN15]=ycoldCNO[5];
    Nin[XO16]=ycoldCNO[6];
    Nin[XO17]=ycoldCNO[7];
    Nin[XO18]=ycoldCNO[8];
    Nin[XF19]=ycoldCNO[9];

    return err;
}

static void coldCNOderivatives_lsoda(double t Maybe_unused,
                                     double * y,
                                     double * dydt,
                                     void * data)
{
    /*
     * When called, should subtract 1 from
     * y and dydt because ppderivatives accesses the
     * arrays with [1] as the first element.
     */
    struct lsoda_data_t * const Restrict lsoda_data =
        (struct lsoda_data_t * const Restrict)data;
    coldCNOderivatives(y-1,
                       dydt-1,
                       (const double * const Restrict) lsoda_data->sigmav,
                       NULL);
}



//Jacobian



#endif // NUCSYN
