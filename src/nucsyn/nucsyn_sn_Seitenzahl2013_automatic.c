#include "../binary_c.h"
No_empty_translation_unit_warning;


/*
 * Wrapper for the nucsyn_sn_Seitenzahl2013 function to
 * automatically select one of the N100 models and interpolate
 * the metallicity
 */

#ifdef NUCSYN

void nucsyn_sn_Seitenzahl2013_automatic(struct stardata_t * Restrict const stardata,
                                        Abundance * Restrict const X)
{
    /*
     * type Ia MCh supernovae yields from
     * Seitenzahl et al. (2013) MNRAS 429,1156
     *
     * This function interpolates based on the metallicity
     * and the N100 models.
     *
     * Note that Zsolar is from Lodders 2010
     */

#define Z (stardata->common.nucsyn_metallicity)
#define Zsolar (0.0153293)

    char was[13];
    strlcpy(was,stardata->preferences->Seitenzahl2013_model,12);

    if(Z>Zsolar)
    {
        strlcpy(stardata->preferences->Seitenzahl2013_model,
                "N100",
                5);
        nucsyn_sn_Seitenzahl2013(stardata,X);
        Dprint("use solar yields\n");
    }
    else if(Z<0.01*Zsolar)
    {
        strlcpy(stardata->preferences->Seitenzahl2013_model,
                "N100_Z0.01",
                11);
        nucsyn_sn_Seitenzahl2013(stardata,X);
        Dprint("use 0.01 Z yields\n");
    }
    else
    {
        /*
         * Interpolate
         */
        Abundance * Xlow = New_clear_isotope_array;
        Abundance * Xhigh = New_clear_isotope_array;
        double Zlow,Zhigh;

        Dprint("compare Z = %g to Zsolar = %g\n",Z,Zsolar);
        if(Z>0.5*Zsolar)
        {
            /*
             * Interpolate between Zsolar and 0.5 * Zsolar
             */
            strlcpy(stardata->preferences->Seitenzahl2013_model,
                    "N100",
                    5);
            nucsyn_sn_Seitenzahl2013(stardata,Xhigh);
            Zhigh = Zsolar;
            strlcpy(stardata->preferences->Seitenzahl2013_model,
                    "N100_Z0.5",
                    11);
            nucsyn_sn_Seitenzahl2013(stardata,Xlow);
            Zlow = 0.5 * Zsolar;
        }
        else if(Z>0.1*Zsolar)
        {
            /*
             * Interpolate between 0.5 * Zsolar and 0.1 * Zsolar
             */
            strlcpy(stardata->preferences->Seitenzahl2013_model,
                    "N100_Z0.5",
                    11);
            nucsyn_sn_Seitenzahl2013(stardata,Xhigh);
            Zhigh = 0.5 * Zsolar;
            strlcpy(stardata->preferences->Seitenzahl2013_model,
                    "N100_Z0.1",
                    11);
            nucsyn_sn_Seitenzahl2013(stardata,Xlow);
            Zlow = 0.1 * Zsolar;
        }
        else
        {
            /*
             * Interpolate between 0.01 * Zsolar and 0.1 * Zsolar
             */
            strlcpy(stardata->preferences->Seitenzahl2013_model,
                    "N100_Z0.1",
                    11);
            nucsyn_sn_Seitenzahl2013(stardata,Xhigh);
            Zhigh = 0.1 * Zsolar;
            strlcpy(stardata->preferences->Seitenzahl2013_model,
                    "N100_Z0.01",
                    11);
            nucsyn_sn_Seitenzahl2013(stardata,Xlow);
            Zlow = 0.01 * Zsolar;
        }



        /*
         * Do the interpolation
         */
        const double f = (Z-Zlow)/(Zhigh-Zlow);
        const double f1 = 1.0 - f;
        Dprint("interpolate %g to %g : f = %g\n",Zlow,Zhigh,f);
        nucsyn_dilute_shell_to(f1,Xlow,
                               f,Xhigh,
                               X);
        Safe_free(Xlow);
        Safe_free(Xhigh);
    }

    /* restore preference */
    strlcpy(stardata->preferences->Seitenzahl2013_model,was,12);
}

#endif // NUCSYN
