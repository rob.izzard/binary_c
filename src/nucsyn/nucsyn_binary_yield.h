#pragma once
#ifndef NUCSYN_BINARY_YIELD_H
#define NUCSYN_BINARY_YIELD_H
/*
 * Headers for nucsyn_binary_yield.c
 */

/************************************************************/
/* local functions */
static inline Abundance Pure_function Maybe_unused
Total_yield(const Isotope i,
            struct stardata_t * Restrict const stardata);

#ifdef NUCSYN_ID_SOURCES_GCE
static inline Abundance Pure_function Hot_function Total_yield_from_source(const struct stardata_t * Restrict const stardata,
                                                                           const Yield_source s,
                                                                           const Isotope i);
static inline Abundance Pure_function Hot_function Total_yield_from_source1(const struct stardata_t * Restrict const stardata,
                                                                            const Yield_source s,
                                                                            const Isotope i);
static inline Abundance Pure_function Hot_function Total_yield_from_source2(const struct stardata_t * Restrict const stardata,
                                                                            const Yield_source s,
                                                                            const Isotope i);
static inline Abundance Pure_function Hot_function Total_yield_from_source3(const struct stardata_t * Restrict const stardata,
                                                                            const Yield_source s,
                                                                            const Isotope i);
static inline Abundance Pure_function Hot_function Total_yield_from_source4(const struct stardata_t * Restrict const stardata,
                                                                            const Yield_source s,
                                                                            const Isotope i);
static inline Abundance Pure_function Hot_function Total_yield_from_source5(const struct stardata_t * Restrict const stardata,
                                                                            const Yield_source s,
                                                                            const Isotope i);
static inline Abundance Pure_function Hot_function Total_yield_from_source6(const struct stardata_t * Restrict const stardata,
                                                                            const Yield_source s,
                                                                            const Isotope i);
static inline Abundance Pure_function Hot_function Total_yield_from_source6a(const struct stardata_t * Restrict const stardata,
                                                                             const Yield_source s,
                                                                             const Isotope i);
static inline Abundance Pure_function Hot_function Total_yield_from_source6b(const struct stardata_t * Restrict const stardata,
                                                                             const Yield_source s,
                                                                             const Isotope i);
static inline Abundance Pure_function Hot_function Total_yield_from_source7(const struct stardata_t * Restrict const stardata,
                                                                            const Yield_source s,
                                                                            const Isotope i);
static inline Abundance Pure_function Hot_function Total_yield_from_source8(const struct stardata_t * Restrict const stardata,
                                                                            const Yield_source s,
                                                                            const Isotope i);
static inline Abundance Pure_function Hot_function Total_yield_from_source9(const struct stardata_t * Restrict const stardata,
                                                                            const Yield_source s,
                                                                            const Isotope i);
static inline Abundance Pure_function Hot_function Total_yield_from_source(const struct stardata_t * Restrict const stardata,
                                                                           const Yield_source s,
                                                                           const Isotope i);
#endif// NUCSYN_ID_SOURCES_GCE

static void first_timestep_setup(struct stardata_t * Restrict const stardata Maybe_unused);
/************************************************************/


/*
 * Define when a yield is zero. It's best not to use
 * the Is_zero macro, because the yield output could be
 * <1e-14 for rare elements.
 */
#define Yield_is_zero(Y) (fabs(Y)<1e-100)

#define TOTAL_MPYIELD (stardata->star[0].mpyield[i]+stardata->star[1].mpyield[i])

/*
 * Should we save the yields output in a separate array?
 */
#if defined NUCSYN &&                           \
    (defined NUCSYN_YIELD_COMPRESSION ||        \
     defined NUCSYN_LOG_BINARY_DX_YIELDS)
#define SAVE_YIELDS
#endif


#define Dump_zeros(N)                           \
    {                                           \
        if(zerocount<3)                         \
        {                                       \
            if(zerocount==1)                    \
            {                                   \
                Printf("0 ");                   \
            }                                   \
            else if(zerocount==2)               \
            {                                   \
                Printf("0 0 ");                 \
            }                                   \
        }                                       \
        else                                    \
        {                                       \
            Printf("%ux0 ",zerocount);          \
        }                                       \
    }

#ifdef NUCSYN_LONG_YIELD_FORMAT
#define YIELD_FORMAT "%0+.12e "
#else
#define YIELD_FORMAT "%0+.3e "
#endif // NUCSYN_LONG_YIELD_FORMAT



/*
 * Output and storage macros for the yields
 */
#define Yield_print(Y)                          \
    if(Yield_is_zero(Y))                        \
    {                                           \
        Printf("0 ");                           \
    }                                           \
    else                                        \
    {                                           \
        Printf(YIELD_FORMAT,(Y));               \
    }

#define Set_ensemble_yield(...)                 \
    CDict_nest(stardata->model.ensemble_cdict,  \
                LAST_BUT1(__VA_ARGS__),         \
                LAST(__VA_ARGS__),              \
                EXCEPT_LAST2(__VA_ARGS__));
#define Set_ensemble_initial_abundance(...)     \
    CDict_nest(stardata->model.ensemble_cdict,  \
                LAST_BUT1(__VA_ARGS__),         \
                LAST(__VA_ARGS__),              \
                EXCEPT_LAST2(__VA_ARGS__));


#define Get_ensemble_yield(...)                 \
    CDict_nest_get_entry(stardata->model.ensemble_cdict,  \
                         LAST_BUT1(__VA_ARGS__),          \
                         LAST(__VA_ARGS__),               \
                         EXCEPT_LAST2(__VA_ARGS__));




#ifdef NUCSYN_LOG_BINARY_DX_YIELDS
#define _Delta "D"
#else
#define _Delta ""
#endif

#ifdef NUCSYN_ID_SOURCES_GCE
#define _Print_Time Printf(_Delta "XYIELDbin%d__ %0.4e ",s,T);
#else
#define _Print_Time Printf(_Delta "XYIELDbin__ %0.4e ",T);
#endif

#ifdef COMPRESS_YIELD_OUTPUT
#define Print_zero_yield zerocount++;
#define Dump_any_zeros                          \
    if(zerocount != 0)                          \
    {                                           \
        Dump_zeros(zerocount);                  \
    }
#define Dump_any_zeros_and_reset                \
    if(zerocount != 0)                          \
    {                                           \
        Dump_zeros(zerocount);                  \
        zerocount = 0;                          \
    }
#else
#define Print_zero_yield Printf("0 ");
#define Dump_any_zeros /* do nothing */
#define Dump_any_zeros_and_reset /* do nothing */
#endif //COMPRESS_YIELD_OUTPUT

#ifdef STELLAR_POPULATIONS_ENSEMBLE
#define Ensemble_yield                                                  \
    Set_ensemble_yield(                                                 \
        (char*)"Xyield",                                                \
        (char*)"time",(double)T,                                        \
        (char*)"source",(char*)Source_string(s),                        \
        (char*)"isotope",(char*)stardata->store->isotope_strings[i],    \
        (double)(dy/timestep)                                           \
        );
#define Ensemble_yields                                                 \
    if(ensemble_yields == TRUE)                                         \
    {                                                                   \
        for(Isotope i=0;i<ISOTOPE_ARRAY_SIZE;i++)                       \
        {                                                               \
            const double dy = Total_yield_from_source6(stardata,s,i);   \
            if(!Yield_is_zero(dy))                                      \
            {                                                           \
                Ensemble_yield;                                         \
            }                                                           \
        }                                                               \
    }
#else
#define Ensemble_yield /* do nothing */
#define Ensemble_yields /* do nothing */
#endif//STELLAR_POPULATIONS_ENSEMBLE

#endif // NUCSYN_BINARY_YIELD_H
