#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef NUCSYN

void nucsyn_r_process_Arlandini1999(Abundance * const Restrict Xr Maybe_unused,
                                    struct stardata_t * const Restrict stardata Maybe_unused)
{
    /*
     * Function to set the r-process abundances into X, which should
     * be memory space which have been set to zero (i.e. allocated
     * with calloc)
     *
     * Note:
     *
     * for isotopes with atomic mass below R_PROCESS_MASS_CUT we assume
     * zero yield from this function, these isotopes are usually modelled
     * by the usual (Woosley+Weaver 1995, or Chieffi+Limongi 2004)
     * supernova models.
     *
     * The resulting abundance array has a mass fraction of 1.0
     * and you are expected to dilute this into the target material
     * manually (the physics is for you to make up).
     *
     * Note: if you are using the extended s-process then this might not
     * actually be true (the total mass fraction could be > 1), because
     * we follow isotopes and elements at the same time. You might want
     * to fix this behaviour!
     */


#ifdef NUCSYN_ALL_ISOTOPES
    /*
     * Now the isotopic abundances
     */
    Xr[XAg107]+=1.65754103e-04;


    Xr[XAg109]+=1.38198299e-04;


    Xr[XAs75]+=3.39387372e-03;


    Xr[XAu197]+=2.50746606e-04;


    Xr[XBa134]+=1.93736990e-06;


    Xr[XBa135]+=2.13258080e-04;


    Xr[XBa136]+=3.46039917e-04;


    Xr[XBa137]+=1.72347991e-04;


    Xr[XBa138]+=4.58841699e-04;


    Xr[XBi209]+=2.06864779e-04;


    Xr[XBr79]+=3.11669900e-03;


    Xr[XBr81]+=3.08958068e-03;


    Xr[XCd108]+=1.11518979e-05;


    Xr[XCd110]+=4.77083746e-06;


    Xr[XCd111]+=1.26207276e-04;


    Xr[XCd112]+=1.48119305e-04;


    Xr[XCd113]+=1.04958360e-04;


    Xr[XCd114]+=1.38421696e-04;


    Xr[XCd116]+=8.35858473e-05;


    Xr[XCe140]+=1.66788631e-04;


    Xr[XCe142]+=1.00694934e-04;


    Xr[XCs133]+=3.05683958e-04;


    Xr[XCu63]+=1.63051748e-01;


    Xr[XCu65]+=7.46810693e-02;


    Xr[XDy160]+=1.34172438e-06;


    Xr[XDy161]+=8.19036346e-05;


    Xr[XDy162]+=9.90692840e-05;


    Xr[XDy163]+=1.11555027e-04;


    Xr[XDy164]+=1.00648882e-04;


    Xr[XEr164]+=8.29822675e-07;


    Xr[XEr166]+=8.61578276e-05;


    Xr[XEr167]+=6.35857796e-05;


    Xr[XEr168]+=5.85370156e-05;


    Xr[XEr170]+=4.26445626e-05;


    Xr[XEu151]+=4.74323169e-05;


    Xr[XEu153]+=5.33263201e-05;


    Xr[XGa69]+=1.08862241e-02;


    Xr[XGa71]+=7.32114692e-03;


    Xr[XGd152]+=8.46122056e-08;


    Xr[XGd154]+=3.78492906e-07;


    Xr[XGd155]+=5.14388956e-05;


    Xr[XGd156]+=6.32461057e-05;


    Xr[XGd157]+=5.22763284e-05;


    Xr[XGd158]+=6.79476065e-05;


    Xr[XGd160]+=8.24270336e-05;


    Xr[XGe70]+=1.15351534e-02;


    Xr[XGe72]+=1.56798964e-02;


    Xr[XGe73]+=4.65084577e-03;


    Xr[XGe74]+=2.18133323e-02;


    Xr[XGe76]+=5.09484588e-03;


    Xr[XHf176]+=3.56167393e-07;


    Xr[XHf177]+=2.99514087e-05;


    Xr[XHf178]+=2.31584427e-05;


    Xr[XHf179]+=1.71556829e-05;


    Xr[XHf180]+=1.73026808e-05;


    Xr[XHg198]+=4.73567742e-05;


    Xr[XHg199]+=6.06935735e-05;


    Xr[XHg200]+=3.90291059e-05;


    Xr[XHg201]+=3.28381554e-05;


    Xr[XHg202]+=2.86215517e-05;


    Xr[XHg204]+=3.12963110e-05;


    Xr[XHo165]+=9.77272583e-05;


    Xr[XI127]+=7.82518436e-04;


    Xr[XIn113]+=6.45231369e-06;


    Xr[XIn115]+=9.28549530e-05;


    Xr[XIr191]+=3.34447042e-04;


    Xr[XIr193]+=5.71374072e-04;


    Xr[XKr80]+=5.10127641e-04;


    Xr[XKr82]+=1.92001067e-03;


    Xr[XKr83]+=2.70489185e-03;


    Xr[XKr84]+=1.34556108e-02;


    Xr[XKr86]+=3.55466729e-03;


    Xr[XLa139]+=1.69782339e-04;


    Xr[XLu175]+=3.71438935e-05;


    Xr[XLu176]+=9.87571031e-07;


    Xr[XMo100]+=1.71016231e-04;


    Xr[XMo94]+=1.59297939e-04;


    Xr[XMo95]+=1.24285064e-04;


    Xr[XMo96]+=2.76910802e-04;


    Xr[XMo97]+=7.08166558e-05;


    Xr[XMo98]+=1.05578528e-04;


    Xr[XNb93]+=6.85665835e-05;


    Xr[XNd142]+=1.74493743e-05;


    Xr[XNd143]+=7.06967125e-05;


    Xr[XNd144]+=1.00967719e-04;


    Xr[XNd145]+=5.21996697e-05;


    Xr[XNd146]+=5.37139053e-05;


    Xr[XNd148]+=4.13530153e-05;


    Xr[XOs186]+=4.03315856e-07;


    Xr[XOs187]+=2.01386105e-06;


    Xr[XOs188]+=9.86467468e-05;


    Xr[XOs189]+=1.42486426e-04;


    Xr[XOs190]+=2.15073240e-04;


    Xr[XOs192]+=3.80372869e-04;


    Xr[XPb204]+=5.16113731e-06;


    Xr[XPb206]+=3.72253169e-04;


    Xr[XPb207]+=3.50116166e-04;


    Xr[XPb208]+=1.80222719e-03;


    Xr[XPd104]+=1.09888823e-04;


    Xr[XPd105]+=2.02856995e-04;


    Xr[XPd106]+=1.41761051e-04;


    Xr[XPd108]+=9.98901858e-05;


    Xr[XPd110]+=1.24875586e-04;


    Xr[XPr141]+=8.73400926e-05;


    Xr[XPt192]+=2.77536634e-07;


    Xr[XPt194]+=5.93555787e-04;


    Xr[XPt195]+=6.27766357e-04;


    Xr[XPt196]+=4.32167160e-04;


    Xr[XPt198]+=1.37786535e-04;


    Xr[XRb85]+=2.63245786e-03;


    Xr[XRb87]+=8.57679850e-04;


    Xr[XRe185]+=1.94172555e-05;


    Xr[XRe187]+=4.73455436e-05;


    Xr[XRh103]+=2.21327824e-04;


    Xr[XRu100]+=7.95096380e-06;


    Xr[XRu101]+=1.95429828e-04;


    Xr[XRu102]+=2.46971972e-04;


    Xr[XRu104]+=2.54452502e-04;


    Xr[XRu99]+=1.21007892e-04;


    Xr[XSb121]+=9.54998778e-05;


    Xr[XSb123]+=1.10193727e-04;


    Xr[XSe76]+=2.60385827e-03;


    Xr[XSe77]+=2.44049126e-03;


    Xr[XSe78]+=7.40184519e-03;


    Xr[XSe80]+=1.62896369e-02;


    Xr[XSe82]+=3.37562129e-03;


    Xr[XSm147]+=3.36379351e-05;


    Xr[XSm148]+=1.06962592e-06;


    Xr[XSm149]+=3.35548404e-05;


    Xr[XSm150]+=0.00000000e+00;


    Xr[XSm152]+=5.83380443e-05;


    Xr[XSm154]+=6.47158557e-05;


    Xr[XSn114]+=2.07619385e-05;


    Xr[XSn115]+=1.04672216e-05;


    /*
     * Everything lighter than the mass cut is not pure r-process
     */
    const double limit = NUCSYN_R_PROCESS_ARLANDINI1999_MASS_CUT * AMU_GRAMS;
    Nucsyn_isotope_stride_loop(
        if(stardata->store->mnuc[i] < limit)
        {
            Xr[i] = 0.0;
        }
        );
    nancheck("r Arlandini pre-renorm",Xr,ISOTOPE_ARRAY_SIZE);

    /*
     * Renormalize
     */
    nucsyn_renormalize_abundance(Xr);
    nancheck("r Arlandini post-renorm",Xr,ISOTOPE_ARRAY_SIZE);
#endif // NUCSYN_ALL_ISOTOPES

}


#endif // NUCSYN
