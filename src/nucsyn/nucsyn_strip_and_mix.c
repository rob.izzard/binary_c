#include "../binary_c.h"
No_empty_translation_unit_warning;



#if (defined NUCSYN && defined NUCSYN_STRIP_AND_MIX)
#include "nucsyn.h"
#include "nucsyn_strip_and_mix.h"
#include "../tables/table_TAMS.h"

/*
 * Strip the envelope of a star, and do first dredge up,
 * based on a table of abundance profiles defined
 * at the end of the main sequence.
 *
 * Currently uses a table of H,He,C,N,O made by RI for
 * 0.8<M<20, 1e-4<Z<0.03 using the TWIN code, or a set
 * of tables for 0.5<M<20, 1e-4<Z<0.03 using the Stars code
 * made by Holly Preece. I recommend you use Holly's models.
 *
 * NB *Requires* RLOF_interpolation_method=0
 */

/*
 * Enable CORRECT_FOR_INIT_ABUNDS to correct the abundance
 * lookup table to be adjusted according to the initial abundance
 * mixture.
 */
#define CORRECT_FOR_INIT_ABUNDS

/*
 * Limit the depth of mixing to that given by the 1DUP mass
 * fitting function, rather than BSE's convective_envelope_mass_and_radius (which gives very
 * deep dredge up).
 *
 * This seems to do nothing.
 */
//#define LIMIT_TO_1DUP_FIT

/*
 * Limit to the depth of mixing given by the set of models
 * computed by Holly Preece with the Stars code (15/06/2016).
 */
#define LIMIT_TO_1DUP_HOLLY_TABLE

/*
 * Artificial offset to the 1DUP max
 */
#define DUP1_MAXDEPTH_OFFSET (0.0)


/*
 * Debug statements: usually these are identical
 * to Dprint, but StripDebug() can override them to
 * output only for this file.
 */
#define StripDebug Dprint
//#define StripDebug(...) _printf(__VA_ARGS__);

/* log to file */
//#define MIXLOGGING
/* extra logging */
//#define ENVDEBUG

/*
 * Enable or disable mixing debugging
 */
#define MIXDEBUG 0

void nucsyn_strip_and_mix(struct stardata_t * const stardata,
                          struct star_t * const star)
{
    /*
     * table is a modified version of store->TAMS
     * to match the initial abundances.
     */

    StripDebug("*** STRIP and MIX called at t=%g : star %d type %d : M=%g (mshells=%g) dmacc=%g : mshells=%g, nshells=%d fp=%p,%p\n",
               stardata->model.time,
               star->starnum,
               star->stellar_type,
               star->mass,
               mshells(star),
               star->dmacc,
               mshells(star),
               star->topshell,
#ifdef MIXLOGGING
               stardata->tmpstore->strip_and_mix_fp[0],stardata->tmpstore->strip_and_mix_fp[1]
#else
               NULL,NULL
#endif
        );

    const double pms_mass = stardata->common.zero_age.mass[star->starnum];

    if(can_use_strip_and_mix(stardata,star)==TRUE)
    {
        /*
         * Star's maximum shell mass
         */
        const double mshellmax = pms_mass /
            ((double)NUCSYN_STRIP_AND_MIX_NSHELLS_START);

        /*
         * Phase start mass
         */
        double phase_start_mass =
            star->stellar_type<=MAIN_SEQUENCE ?
            star->effective_zams_mass : star->phase_start_mass;

        /*
         * First timestep setup
         */
        if((Is_zero(stardata->model.time) && Is_zero(star->X[0][XHe4])) ||
           stardata->common.strip_and_mix_is_setup == FALSE)
        {
            first_timestep_setup(star,
                                 stardata,
                                 phase_start_mass);
            stardata->common.strip_and_mix_is_setup = TRUE;
        }

        /*
         * Burn lithium inside m/M=0.9 when on the main sequence.
         * Assume that after the main sequence, the envelope is
         * too cool to burn lithium.
         */
        burn_lithium(star);

        /*
         * Adapt H,He,CNO profile to account for mass accretion
         *
         * We do this at the end of the main sequence because
         * mass transfer, if any, probably occurs while the star
         * is on the main sequence. We do not want to instantly
         * adapt the profile because this results in less thermohaline
         * mixing than there should be. However, we do want to make
         * sure the profile is "correct" before first dredge up.
         *
         * We also only do it for mass accretion, because mass loss
         * would just leave the profile as set up early in the main
         * sequence.
         */
        TAMS_mass_accretion(star,stardata,phase_start_mass,pms_mass);

#ifdef MIXLOGGING
        if(stardata->tmpstore->strip_and_mix_fp[star->starnum]==NULL)
        {
            char filename[255];
            sprintf(filename,"/tmp/star%d",star->starnum);
            stardata->tmpstore->strip_and_mix_fp[star->starnum] = fopen(filename,"w");
            fprintf(stardata->tmpstore->strip_and_mix_fp[star->starnum],
                    "Time nshell m/M XH1 XHe4 XLi7 XC12 XN14 XO16 mu\n");
        }
#endif

        /*
         * Net change in the stellar mass
         */
        mass_changes(star,stardata,phase_start_mass,mshellmax);

        /*
         * And any consequent mixing
         */
        mixing(star,stardata,phase_start_mass,mshellmax);
    }
    else
    {
        /*
         * Star cannot use strip and mix because it
         * has evolved beyond the algorithm's validity.
         * Thus, disable strip and mix for both stars.
         */
        star->strip_and_mix_disabled = TRUE;
    }

    StripDebug("*** STRIP and MIX exit at t=%g : star %d : M=%g dmacc=%g : mshells=%g, nshells=%d\n",
           stardata->model.time,
           star->starnum,
           star->mass,
           star->dmacc,
           mshells(star),
           star->topshell
        );

}


void mix_down_to_core(struct star_t * Restrict const star)
{
    /*
     * Mix the whole envelope down to the core
     * but only when the core is well defined, i.e. for stars
     * off the main sequence
     */
    if(star->stellar_type==HERTZSPRUNG_GAP ||
       star->stellar_type==GIANT_BRANCH)
    {
        Shell_index ish,nsh;
        double m = star->mass;
        double * Xmixed = New_isotope_array_from(star->X[star->topshell]);
        double mmixed=star->mshell[star->topshell];

        for(nsh=star->topshell-1;nsh>0.0;nsh--)
        {
            if(m > Outermost_core_mass(star))
            {
                nucsyn_dilute_shell(mmixed,Xmixed,
                                    star->mshell[nsh], star->X[nsh]);
                mmixed += star->mshell[nsh];
            }
            else
            {
                break;
            }
            m-=star->mshell[nsh];
        }
        for(ish=star->topshell;ish>=nsh;ish--)
        {
            Copy_abundances(Xmixed, star->X[ish]);
        }
        Copy_abundances(star->X[star->topshell],star->Xenv);
        Safe_free(Xmixed);
    }
}


double mshells(struct star_t * Restrict const star)
{
    /*
     * Return mass in all shells
     */
    Shell_index i;
    double m=0.0;
    for(i=0;i<=star->topshell;i++)
    {
        m += star->mshell[i];
    }
    return m;
}

void sort_by_molecular_weight(struct star_t * Restrict const star,
                              struct stardata_t * Restrict const stardata)
{
    /*
     * Sort the layers in a star by molecular weight
     */
    int i;
    struct layerinfo_t * layerinfo = Malloc(sizeof(struct layerinfo_t)*(star->topshell+1));
    for(i=0;i<=star->topshell;i++)
    {
        layerinfo[i].orig_sh = i;
        layerinfo[i].mu = nucsyn_effective_molecular_weight(star->X[i],
                                                            star->stellar_type,
                                                            stardata->store->molweight);
        Copy_abundances(star->X[i],layerinfo[i].X);
    }

    void * arg = NULL;
    qsort_r((void *)layerinfo,
            star->topshell+1,
            sizeof(struct layerinfo_t),
#ifdef __HAVE_GNU_QSORT_R
            (comparison_fn_r)compare_layerinfo_by_mu,
            arg
#endif
#ifdef __HAVE_BSD_QSORT_R
            arg,
            (comparison_fn_r)compare_layerinfo_by_mu
#endif
        );
    Safe_free(arg);

    for(i=0;i<=star->topshell;i++)
    {
        Copy_abundances(layerinfo[i].X,star->X[i]);
    }

    Safe_free(layerinfo);
}

#ifdef __HAVE_GNU_QSORT_R
static int compare_layerinfo_by_mu(const void * Restrict a,
                                   const void * Restrict b,
                                   void * arg)
#endif
#ifdef __HAVE_BSD_QSORT_R
static int compare_layerinfo_by_mu(void * arg,
                                   const void * Restrict a,
                                   const void * Restrict b)
#endif
{
    /*
     * Compare the molecular weight in two layers, for
     * appropriate sorting.
     */
    double mu_a = ((struct layerinfo_t *) a)->mu;
    double mu_b = ((struct layerinfo_t *) b)->mu;
    return ((mu_a < mu_b)-(mu_a > mu_b));
}

void nucsyn_mix_stars(struct stardata_t * Restrict const stardata,
                      const double mlost,
                      const double m3)
{

    /*
     * Accrete star 1 onto star 0, then sort
     * the star by molecular weight.
     *
     * mlost is the mass lost in the merger process
     *
     * m3 is the final mass of the merged star
     */
    StripDebug("Mix stars (mix_stars) : m1=%g (st %d, disabled? %d) m2=%g (st %d, disable? %d) mlost=%g m3=%g (should be %g) mass in shells star0=%g star1=%g\n",
               stardata->star[0].mass,
               stardata->star[0].stellar_type,
               stardata->star[0].strip_and_mix_disabled,
               stardata->star[1].mass,
               stardata->star[1].stellar_type,
               stardata->star[1].strip_and_mix_disabled,
               mlost,
               m3,
               stardata->star[0].mass + stardata->star[1].mass - mlost,
               mshells(&stardata->star[0]),
               mshells(&stardata->star[1])
        );

    Star_number nstar;
    Foreach_star(star)
    {
        if(star->strip_and_mix_disabled)
        {
            StripDebug("Cannot use strip and mix for merged star because star %d has it disabled",
                   star->starnum);
            stardata->star[Other_star(star->starnum)].strip_and_mix_disabled = TRUE;
            return;
        }
    }

    /*
     * If simply merging the stars means the number of shells exceeds
     * NUCSYN_STRIP_AND_MIX_NSHELLS_MAX, halve the number of shells in
     * the more massive star
     */
    const Star_number n_massive = stardata->star[0].mass > stardata->star[1].mass ? 0 : 1;
    if(stardata->star[0].topshell + stardata->star[1].topshell >=
       NUCSYN_STRIP_AND_MIX_NSHELLS_MAX)
    {
        halve_shells(&stardata->star[n_massive]);
    }

    /*
     * if we're *still* over the limit, halve the other star as well
     */
    if(stardata->star[0].topshell + stardata->star[1].topshell >=
       NUCSYN_STRIP_AND_MIX_NSHELLS_MAX)
    {
        halve_shells(&stardata->star[Other_star(n_massive)]);
    }

    while(stardata->star[1].topshell>=0)
    {
        /*
         * make a new shell to accommodate accreted material if we can
         */
        if(stardata->star[0].topshell < NUCSYN_STRIP_AND_MIX_NSHELLS_MAX-1)
            stardata->star[0].topshell++;

        StripDebug("Add shell %d (%d) : ",
               stardata->star[0].topshell,
               NUCSYN_STRIP_AND_MIX_NSHELLS_MAX);

        /*
         * move material from star 2's topshell to star 1's topshell
         */
        nucsyn_dilute_shell(stardata->star[0].mshell[stardata->star[0].topshell],
                            stardata->star[0].X[stardata->star[0].topshell],
                            stardata->star[1].mshell[stardata->star[1].topshell],
                            stardata->star[1].X[stardata->star[1].topshell]);

        stardata->star[0].mshell[stardata->star[0].topshell] +=
            stardata->star[1].mshell[stardata->star[1].topshell];

        /*
         * remove star 2 topshell
         */
        stardata->star[1].mshell[stardata->star[1].topshell]=0.0;
        stardata->star[1].topshell--;
    }
    StripDebug("\n");

    // no more shells in star 2
    stardata->star[1].topshell=0;

    // disable mass loss/gain for this timestep
    stardata->star[0].derivative[DERIVATIVE_STELLAR_MASS_WIND_LOSS]=0.0;
    stardata->star[0].derivative[DERIVATIVE_STELLAR_MASS_RLOF_LOSS]=0.0;
    stardata->star[1].derivative[DERIVATIVE_STELLAR_MASS_WIND_LOSS]=0.0;
    stardata->star[1].derivative[DERIVATIVE_STELLAR_MASS_RLOF_LOSS]=0.0;

    /* only one star now: star 0 */
    struct star_t * star = &stardata->star[0];
    StripDebug("pre-sort mshells = %g, topshell=%d\n",
           mshells(star),star->topshell);

    // sort by molecular weight
    sort_by_molecular_weight(star,stardata);

    StripDebug("post-sort mshells = %g, topshell=%d\n",
           mshells(star),star->topshell);

    // strip excess mass
    double msh = mshells(star);
    if(msh > m3)
    {
        double m_to_strip = msh - m3;
        while(star->topshell >0 &&
              m_to_strip > 0.0)
        {
            /*
             * Remove required mass from top shell
             */
            double dm = Min(m_to_strip,
                            star->mshell[star->topshell]);
            star->mshell[star->topshell] -= dm;
            m_to_strip -= dm;

            /*
             * If shell is exahusted, remove it
             */
            if(Less_or_equal(star->mshell[star->topshell],0.0))
            {
                star->mshell[star->topshell] = 0.0;
                star->topshell--;
            }
        }
    }

    StripDebug("post-merge mass in shells 0 %g, topshell=%d\n",
           mshells(&stardata->star[0]),
           stardata->star[0].topshell
        );
    StripDebug("post-merge mass in shells 1 %g, topshell=%d\n",
           mshells(&stardata->star[1]),
           stardata->star[1].topshell
        );

    // set final stellar mass correctly
    star->mass = m3;
    star->dmacc = 0.0;
}

#endif // NUCSYN_STRIP_AND_MIX

Boolean can_use_strip_and_mix(struct stardata_t * const stardata,
                              struct star_t * Restrict const star)
{
    Boolean can;
#ifdef NUCSYN_STRIP_AND_MIX
    can = Boolean_(star->strip_and_mix_disabled==FALSE &&
                   star->stellar_type<=CHeB &&
                   Is_not_zero(star->mass));

    Dprint("can use strip and mix? : starnum=%d : disabled=%d : stellar_type=%d : mass=%g : X55=%g\n",
           star->starnum,
           star->strip_and_mix_disabled,
           star->stellar_type,
           star->mass,
           star->Xenv[55]
        );
#else
    can = FALSE;

    Dprint("can use strip and mix? : starnum=%d : disabled=1 (NUCSYN_STRIP_AND_MIX undefined) : stellar_type=%d : mass=%g \n",
           star->starnum,
           star->stellar_type,
           star->mass);

#endif

    return can;
}

#ifdef NUCSYN_STRIP_AND_MIX
void strip_and_mix_remove_mass(struct star_t * Restrict const star,
                               double dmlose)
{
    while(dmlose>VERY_TINY && star->topshell>0)
    {
        if(star->mshell[star->topshell] > dmlose)
        {
            /* all dmlose from top shell */
            star->mshell[star->topshell] -= dmlose;
            dmlose = 0.0;
        }
        else
        {
            /* remove all of topshell, and continue loop */
            dmlose -= star->mshell[star->topshell];
            star->mshell[star->topshell] = 0.0;
            star->topshell--;
        }
    }
}
#endif // NUCSYN_STRIP_AND_MIX

#ifdef NUCSYN_STRIP_AND_MIX
void halve_shells(struct star_t * Restrict const star)
{
    /*
     * NEW ALGORITHM :
     * Halve the number of shells to take into account
     * accreted material. This decreases resolution, but is a LOT
     * faster and probably accurate enough, given that the increased
     * mass of shells would be automatic if the stars were born with
     * such abundances.
     */

    Shell_index i;

    /*
     * Combine every two shells into one
     */
    for(i=0;i<star->topshell;i++)
    {
        nucsyn_dilute_shell(star->mshell[i], star->X[i],
                            star->mshell[i+1], star->X[i+1]);
        star->mshell[i] += star->mshell[i+1];
        star->mshell[i+1] = 0.0;
        i++;
    }

    /*
     * Remove shells with zero mass
     */
    for(i=0;i<star->topshell;i++)
    {
        const int j = i * 2;
        if(j>star->topshell)
        {
            star->mshell[i] = star->mshell[j];
            Copy_abundances(star->X[j],star->X[i]);
            break;
        }
        if(i!=j)
        {
            star->mshell[i] = star->mshell[j];
            Copy_abundances(star->X[j],star->X[i]);
        }
    }
    star->topshell = i;
    for(i=star->topshell; i<NUCSYN_STRIP_AND_MIX_NSHELLS_MAX;i++)
    {
        star->mshell[i]=0.0;
    }
}

static void first_timestep_setup(struct star_t * Restrict star,
                                 struct stardata_t * Restrict const stardata,
                                 const double phase_start_mass
    )
{
    /*
     * First time setup
     */
    double param[TAMS_NPARAM]={
        stardata->common.metallicity,
        phase_start_mass,
        Limit_range(star->mass / phase_start_mass,0.0,1.0)
    };
    double data[TAMS_NDATA];


    /*
     * Copy the raw TAMS data from the stellar evolution code data
     * into a version in common.strip_and_mix_table which is then altered
     * given the initial abundances.
     */
    struct data_table_t * table = &stardata->common.strip_and_mix_table;
    table->data = stardata->common.strip_and_mix_table_data;
    memcpy(table->data,
           stardata->store->TAMS->data,
           TAMS_TABLE_MEMSIZE);
    table->nlines = TABLE_TAMS_LINES;
    table->ndata = TAMS_NDATA;
    table->nparam = TAMS_NPARAM;

    /*
     * normalize to given initial abundances
     */
#ifdef CORRECT_FOR_INIT_ABUNDS
    double xwas = param[2];
    // interpolate at the (unchanged) surface
    param[2]=1.0;

    Interpolate(table,param,data,FALSE);

#ifdef ENVDEBUG
    printf("First table line has H=%g He=%g C=%g N=%g O=%g\n",
           data[H1],data[He4],data[C12],data[N14],data[O16]);
#endif

    // modulate CNO to initial CNO abundance
    int i;
    const Abundance * Xi = stardata->preferences->zero_age.XZAMS;
    const Abundance fHHe = (Xi[XH1]+Xi[XHe4])/(data[H1]+data[He4]);
    const Abundance fC = Xi[XC12]/data[C12];
    const Abundance fN = Xi[XN14]/data[N14];
    //const Abundance fCN = (Xi[XC12]+Xi[XN14])/(data[C12]+data[N14]);
    const Abundance fO = Xi[XO16]/data[O16];
    const Abundance fCNO = (Xi[XC12]+Xi[XN14]+Xi[XO16])/(data[C12]+data[N14]+data[O16]);
    StripDebug("fC=%g from Xi=%g data[C12]=%g\n",
               fC,
               Xi[XC12],
               data[C12]);

    double * tabledata = table->data;
    for(i=0;i<table->nlines;i++)
    {
        const int j = i*(TAMS_NDATA+TAMS_NPARAM)+3;
        double * tj = tabledata + j;
        if(*(tj+N14) > *(tj+C12) + *(tj+O16))
        {
            // N exceeds C+O, normalize all CNO
            tabledata[i*(TAMS_NDATA+TAMS_NPARAM)+N14+C12] *= fCNO;
            *(tj+N14) *= fCNO;
        }
        else
        {
            // only CN equilibrium, normalize either
            // C,N,O separately, or CN together and O
            // separately
            *(tj+C12) *= fC;
            *(tj+N14) *= fN;
            *(tj+O16) *= fO;
        }
        *(tj+He4) *= fHHe;

        // put leftover in hydrogen
        const double d = (*(tj+H1)+*(tj+He4)+*(tj+C12)+*(tj+N14)+*(tj+O16))-
            (Xi[XH1]+Xi[XHe4]+Xi[XC12]+Xi[XN14]+Xi[XO16]);

        *(tj+H1) += d;
    }

    Interpolate(table,param,data,FALSE);

#ifdef ENVDEBUG
    printf("First table line now has H=%g He=%g C=%g N=%g O=%g\n",
           data[H1],data[He4],data[C12],data[N14],data[O16]);
    printf("Xenv H=%g He=%g C=%g N=%g O=%g\n",
           star->Xenv[XH1],
           star->Xenv[XHe4],
           star->Xenv[XC12],
           star->Xenv[XN14],
           star->Xenv[XO16]);
#endif

    param[2]=xwas;
#endif //CORRECT_FOR_INIT_ABUNDS

    /*
     * First time profile set up
     */
    const double Li7_eps = 3.37; /* eps(Li7) = log10(N(Li7)/N(H1))+12 */
    const double m1dup = menv_1DUP(stardata,
                                   pms_mass,
                                   stardata->common.metallicity);
    double m = 0.0;
    Shell_index nsh = 0;

    while(m < pms_mass)
    {
        // if within 0.1 Msun above 1DUP depth, increase resolution
        const double weight = (m > m1dup && m - m1dup < 0.1) ? 0.5 : 1.0;
        double mshell = weight * pms_mass / ((double)NUCSYN_STRIP_AND_MIX_NSHELLS_START);
        mshell = Min(pms_mass - m, mshell);
        const double f = (m + mshell*0.5) / pms_mass;
        param[2] = Limit_range(f,0.0,1.0);

        Interpolate(table,param,data,FALSE);

        Copy_abundances(star->Xenv,
                        &star->X[nsh][0]);

        star->X[nsh][XH1]  = data[H1];
        star->X[nsh][XHe4] = data[He4];
        star->X[nsh][XLi7] = 7.0 * data[H1] * exp10(Li7_eps-12);
        star->X[nsh][XLi6] = 1e-2 * star->X[nsh][XLi7];
        star->X[nsh][XC12] = data[C12];
        star->X[nsh][XN14] = data[N14];
        star->X[nsh][XO16] = data[O16];
        star->mshell[nsh]  = mshell;
        m += mshell;
        nsh ++;

        /*
         * Check we won't exceed the number of shells next time
         * (this would be bad)
         */
        if(nsh>=NUCSYN_STRIP_AND_MIX_NSHELLS_MAX)
        {
            Exit_binary_c(BINARY_C_NOT_ENOUGH_SHELLS,
                          "Not enough shells available for initial mass distribution in nucsyn_strip_and_mix");
        }
    }
    star->topshell = nsh - 1;
}

static void burn_lithium(struct star_t * Restrict star)
{
    if(star->stellar_type<=MAIN_SEQUENCE)
    {
        Shell_index nsh;
        double m=0.0;
        for(nsh=0; nsh<=star->topshell; nsh++)
        {
            m+=star->mshell[nsh]*0.5;
            if(m/star->mass < 0.9)
            {
                // neglect protons and helium used up in lithium burning
                star->X[nsh][XLi7]=0.0;
                star->X[nsh][XLi6]=0.0;
            }
            m+=star->mshell[nsh]*0.5;
        }
    }
}

static void TAMS_mass_accretion(struct star_t * Restrict star,
                                struct stardata_t * Restrict const stardata,
                                const double phase_start_mass,
                                const double pms_mass)
{
    /*
     * Adapt H,He,CNO profile to account for mass accretion
     *
     * We do this at the end of the main sequence because
     * mass transfer, if any, probably occurs while the star
     * is on the main sequence. We do not want to instantly
     * adapt the profile because this results in less thermohaline
     * mixing than there should be. However, we do want to make
     * sure the profile is "correct" before first dredge up.
     *
     * We also only do it for mass accretion, because mass loss
     * would just leave the profile as set up early in the main
     * sequence.
     */

    if(star->stellar_type == HERTZSPRUNG_GAP &&
       star->age < star->tm && // forces this to happen only once
       star->mass > pms_mass)
    {
        /* get abundances at the surface */
        double pristine[TAMS_NDATA];
        double param[TAMS_NPARAM]={
            stardata->common.metallicity,
            phase_start_mass,
            1.0
        };

        Interpolate(&stardata->common.strip_and_mix_table,param,pristine,FALSE);

        /*
         * Post-main star's mass has increased,
         * presumably when it *was* on the main sequence.
         * Adapt He, CNO to the new mass profile prior to
         * first dredge up.
         */
        Shell_index nsh;
        double m=0.0;
        double data[TAMS_NDATA];
        for(nsh=0; nsh<=star->topshell; nsh++)
        {
            /*
             * Get the CNO abundances in an unperturbed star
             * of this mass
             */
            m+=star->mshell[nsh]*0.5;
            param[2] = Limit_range(m/star->mass,0.0,1.0);
            Interpolate(&stardata->common.strip_and_mix_table,param,data,FALSE);

            /* construct the new abundances in X */
            double * X = star->X[nsh];

            StripDebug("Adapt CNO @ shell %d : star He4=%g C12=%g N14=%g : data He4=%g C12=%g N14=%g : pristine He4=%g C12=%g N14=%g : map ",
                       nsh,
                       X[XHe4],X[XC12],X[XN14],
                       data[He4],data[C12],data[N14],
                       pristine[He4],pristine[C12],pristine[N14]);

            /*
             * Define interpolation factors for CN and CNO
             */
            const double fint_C = 1.0-data[C12]/pristine[C12];
            const double fint_O = 1.0-data[O16]/pristine[O16];

            StripDebug("fint C=%g O=%g",fint_C,fint_O);

            /*
             * Check for carbon depletion relative to pristine
             */
            if(fint_C > VERY_TINY)
            {
                /*
                 * C burnt : CN cycling
                 */
                X[XC12] = X[XC12] * (1.0-fint_C) + fint_C * data[C12];
                X[XN14] = X[XN14] * (1.0-fint_C) + fint_C * data[N14];
                X[XO16] = X[XO16] * (1.0-fint_O) + fint_O * data[O16];
                X[XHe4] = Max(X[XHe4],data[He4]);
                X[XH1] = 0.0;
                X[XH1] = 1.0-nucsyn_totalX(stardata,X);
                StripDebug("He4=%g C12=%g N14=%g\n",X[XHe4],X[XC12],X[XN14]);
            }
            else
            {
                /* above CN-cycled region : too cool, stop */
                StripDebug("He4=%g C12=%g N14=%g\n",X[XHe4],X[XC12],X[XN14]);
                break;
            }
            m+=star->mshell[nsh]*0.5;
        }
    }

    StripDebug("DMACC%d st=%d M=%g (Mshells=%g) dmacc=%g : C12 {env=%g acc=%g star1=%g}\n",
               star->starnum,
               star->stellar_type,
               star->mass,
               mshells(star),
               star->dmacc,
               star->Xenv[XC12],
               star->Xacc[XC12],
               stardata->star[0].Xenv[XC12]
        );

    if(star->dmacc < -VERY_TINY)
    {
        Exit_binary_c(2,"dmacc < 0 fail");
    }
}



static void mass_changes(struct star_t * Restrict star,
                         struct stardata_t * Restrict const stardata,
                         const double phase_start_mass,
                         const double mshellmax
)
{


    const double dmwind =
        (star->derivative[DERIVATIVE_STELLAR_MASS_WIND_LOSS]+
         star->derivative[DERIVATIVE_STELLAR_MASS_WIND_GAIN])*
        stardata->model.dtm*1e6;

    const double dmRLOF =
        (star->derivative[DERIVATIVE_STELLAR_MASS_RLOF_LOSS]+
         star->derivative[DERIVATIVE_STELLAR_MASS_RLOF_GAIN])*
        stardata->model.dtm*1e6;

    const double dm = dmwind + dmRLOF;

    StripDebug("MASS CHANGE star %d, %d shells, mass in shells %g, mass %g, dm(net)=%g dmwind=%g dmRLOF=%g dmacc=%g C12=%g mu=%g (derivatives Wind %g %g RLOF %g %g, net=%g, dt=%g)\n",
               star->starnum,
               star->topshell,
               mshells(star),
               star->mass,
               dm,
               dmwind,
               dmRLOF,
               star->dmacc,
               star->Xacc[XC12],
               nucsyn_effective_molecular_weight(star->Xacc,
                                                 star->stellar_type,
                                                 stardata->store->molweight),
               star->derivative[DERIVATIVE_STELLAR_MASS_WIND_LOSS],
               star->derivative[DERIVATIVE_STELLAR_MASS_WIND_GAIN],
               star->derivative[DERIVATIVE_STELLAR_MASS_RLOF_LOSS],
               star->derivative[DERIVATIVE_STELLAR_MASS_RLOF_GAIN],
               star->derivative[DERIVATIVE_STELLAR_MASS_WIND_LOSS]+
               star->derivative[DERIVATIVE_STELLAR_MASS_WIND_GAIN]+
               star->derivative[DERIVATIVE_STELLAR_MASS_RLOF_LOSS]+
               star->derivative[DERIVATIVE_STELLAR_MASS_RLOF_GAIN],
               1e6*stardata->model.dtm
        );

    if(dm < 0.0)
    {
        /*
         * Net mass loss
         */
        StripDebug("LOSE%d a=%g stother=%d -(%g + %g) * dtm*1e6=%g dt=%g -> dmlose = %g (dmacc=%g)\n",
                   star->starnum,
                   stardata->common.orbit.separation,
                   stardata->star[Other_star(star->starnum)].stellar_type,
                   star->derivative[DERIVATIVE_STELLAR_MASS_WIND_LOSS],
                   star->derivative[DERIVATIVE_STELLAR_MASS_RLOF_LOSS],
                   stardata->model.dtm*1e6,
                   stardata->model.dt,
                   -dm,
                   star->dmacc);


        strip_and_mix_remove_mass(star,-dm);

        StripDebug("LOSE%d post-loss mshells=%g cf m=%g\n",
                   star->starnum,
                   mshells(star),
                   star->mass);
    }
    /*
     * Accretion of material. This is in the accretion layer
     * of mass star->dmacc.
     */
    if(star->dmacc > 0.0)
    {
        /*
         * put dmacc directly into a new top shell
         */
        StripDebug("Put dmacc = %g into new top shell (was m=%g C12=%g)\n",
                   star->dmacc,
                   star->mshell[star->topshell],
                   star->X[star->topshell][XC12]);

        if(star->topshell == NUCSYN_STRIP_AND_MIX_NSHELLS_MAX-1)
        {
            /*
             * OLD ALGORITHM : do not use, will fail
             * no shells left, mix old surface down into shell below
             * and leave the top shell empty
             */
            /*
              nucsyn_dilute_shell(star->mshell[star->topshell-1], star->X[star->topshell-1],
              star->mshell[star->topshell], star->X[star->topshell]);
              star->mshell[star->topshell-1] += star->mshell[star->topshell-1];
              star->mshell[star->topshell] = 0.0;
            */


            /*
             * NEW ALGORITHM :
             * Halve the number of shells to take into account
             * accreted material. This decreases resolution, but is a LOT
             * faster and probably accurate enough, given that the increased
             * mass of shells would be automatic if the stars were born with
             * such abundances.
             */

            halve_shells(star);
        }
        else
        {
            /* make new top shell */
            star->topshell++;
        }

        /*
         * Add dmacc into the (new) topshell so the surface is always of
         * exactly the correct abundance.
         */
        star->mshell[star->topshell] = star->dmacc;
        Copy_abundances(star->Xacc,star->X[star->topshell]);

        StripDebug("Top shell now has mshell=%g (cf max mshell=%g) C12=%g\n",
                   star->mshell[star->topshell],
                   mshellmax,
                   star->X[star->topshell][XC12]);

        /* limit shell masses to maximum mass, if possible */
        while(star->mshell[star->topshell] > mshellmax &&
              star->topshell < NUCSYN_STRIP_AND_MIX_NSHELLS_MAX-1)
        {
            /* require new shell */
            star->topshell++;
            star->mshell[star->topshell] = star->mshell[star->topshell-1] - mshellmax;
            star->mshell[star->topshell-1] = mshellmax;
            Copy_abundances(star->X[star->topshell-1],star->X[star->topshell]);
            StripDebug("made new top shell %d, dm = %15.10g, with mu = %15.10g\n",
                       star->topshell,
                       star->mshell[star->topshell],
                       nucsyn_effective_molecular_weight(star->X[star->topshell],
                                               star->stellar_type,
                                               stardata->store->molweight)

                );
        }
    }

    if(1)
    {
        double msh = mshells(star);
        /*
         * Tolerate small errors because of floating point
         * 1e-8% causies failure when it shouldn't: so use ~1e-6%
         */
        if(Abs_percent_diff(star->mass,msh)>1e-6)
        {
            printf("Star %d type %d : After Loss and Acc : M=% 15.10g Mc=%g Mshells=% 15.10g DIFF = %15.10g (< means too much in shells), topshell=%d\n",
                   star->starnum,
                   star->stellar_type,
                   star->mass,
                   Outermost_core_mass(star),
                   msh,
                   star->mass-msh,
                   star->topshell);
            Backtrace;
            static const char * c[100] = STELLAR_DERIVATIVE_STRINGS;
            int d;
            for(d=0;d<DERIVATIVE_STELLAR_NUMBER;d++)
            {
                printf("DERIV % 3d %30s : ",d,c[d]);
                Star_number n;
                Starloop(n)
                {
                    printf("% 20g ",
                           stardata->star[n].derivative[d]
                        );
                    if(n==1)
                    {
                        printf("\n");
                    }
                    else
                    {
                        printf(" : ");
                    }
                }

            }

            Exit_binary_c(2,
                          "Mass gone missing or created in star %d\n",
                          star->starnum);
        }
    }

}


static void mixing(struct star_t * Restrict star,
                   struct stardata_t * Restrict const stardata,
                   const double phase_start_mass,
                   const double mshellmax)
{
    /*
     * Mixing : the star is mixed to a depth mmixed, with
     *          mixed zone abundance Xmixed.
     */
    double mixdepth = 0.0; // for logging only

    /*
     * Convective mixing depth
     */
    double conv_mix_m = star->mass - star->menv;

    /*
     * do not go below the 1st DUP maximum depth
     */
    double max_depth=0.0;

#ifdef LIMIT_TO_1DUP_FIT
    /* do not go below the 1st DUP fit depth */
    max_depth = m_1dup(phase_start_mass,
                       stardata->common.metallicity,
                       stardata->store);
#endif // LIMIT_TO_1DUP_FIT
#ifdef LIMIT_TO_1DUP_HOLLY_TABLE
    max_depth = menv_1DUP(stardata,
                          phase_start_mass,
                          stardata->common.metallicity);
#endif // LIMIT_TO_1DUP_HOLLY_TABLE

    /*
     * Apply artificial offset
     */
    max_depth += DUP1_MAXDEPTH_OFFSET;

    /*
     * Hence limit on the convective depth
     */
    conv_mix_m = Max(max_depth,conv_mix_m);

#ifdef FIRST_DREDGE_UP_HOLLY_TESTING
    if(1 &&
       star->starnum==0 &&
       star->stellar_type<10 &&
       star->second_dredge_up==FALSE)
        printf("1DUP %d down to %g=Max(%g,%g), core at %g, fit=%g vs Holly=%g (m=%g Z=%g) surface C=%g N=%g (init %g,%g) : [C/N]=%g\n",
               star->stellar_type,
               conv_mix_m,
               max_depth,
               conv_mix_m,
               Outermost_core_mass(star),
               m_1dup(phase_start_mass,stardata->common.metallicity),
               menv_1DUP(stardata,
                         phase_start_mass,
                         stardata->common.metallicity),
               phase_start_mass,
               stardata->common.metallicity,
               star->Xenv[XC12],
               star->Xenv[XN14],
               stardata->preferences->the_initial_abundances[XC12],
               stardata->preferences->the_initial_abundances[XN14],
               nucsyn_square_bracket(star->Xenv,
                                     stardata->preferences->the_initial_abundances,
                                     XC12,XN14)
               //nucsyn_square_multibracket(star->Xenv,stardata->preferences->the_initial_abundances,(Isotope[]){XC12,XC13},2,(Isotope[]){XN14,XN15},2)
            );
#endif

    /* certainly not into the core! */
    conv_mix_m = Max(Outermost_core_mass(star),
                     conv_mix_m);




    double * Xmixed = New_isotope_array_from(star->X[star->topshell]);
    double mmixed=star->mshell[star->topshell];

    if(MIXDEBUG)
        StripDebug("Above starts at mmixed=%g Xmixed[XC12]=%g muabove=%15.10g\n",
                   mmixed,
                   Xmixed[XC12],
                   nucsyn_effective_molecular_weight(Xmixed,
                                                     star->stellar_type,
                                                     stardata->store->molweight)
            );

    /*
     * Loop down into the star, keep mixing as long as we have to
     */
#define MIX_STABLE 0
#define MIX_CONVECTIVE 1
#define MIX_THERMOHALINE 2

    Shell_index nsh;
    double m = star->mass;

    for(nsh=star->topshell-1; nsh>=1; nsh--)
    {
        Boolean mix = MIX_STABLE;

        if(MIXDEBUG)
            StripDebug("Star%d At shell %d m=% 15.10g dm=% 15.10g : ",
                       star->starnum,
                       nsh,
                       m,
                       star->mshell[nsh]);

        /* test for convective mixing */
        if(m > conv_mix_m) mix = MIX_CONVECTIVE;

        if(MIXDEBUG)
        {
            if(mix)
            {
                StripDebug("conv unstbl");
            }
            else
            {
                StripDebug("conv stable");
            }
        }

        /*
         * If not already mixing AND have accreted AND
         * want to do thermohaline mixing...
         */
        if(!mix &&
           star->dmacc > 0.0 &&
           stardata->preferences->no_thermohaline_mixing==FALSE)
        {
            /*
             * Test for thermohaline mixing because
             * of accreted material. This requires the accreted
             * material to have a higher molecular weight.
             */

            /* determine already-mixed region molecular weight */
            const double mu_above = nucsyn_effective_molecular_weight(Xmixed,
                                                            star->stellar_type,
                                                            stardata->store->molweight);

            /* determine molecular weight of this shell */
            const double mu_this = nucsyn_effective_molecular_weight(star->X[nsh],
                                                           star->stellar_type,
                                                           stardata->store->molweight);

            /* if dmu >= 0, mix more */
            const double dmu = mu_above - mu_this;

            mix = dmu >= 0.0 ? TRUE : mix;

            if(MIXDEBUG)
            {
                if(mix)
                {
                    StripDebug(": th unstbl");
                }
                else
                {
                    StripDebug(": th stable");
                }
                StripDebug(" mu=%g muabv=%g dmu=%g ",mu_this,mu_above,dmu);
            }
        }

        if(mix)
        {
            /*
             * In a mixed region :
             * If this shell is convective and the next is too,
             * skip the mixing. Otherwise, we have to mix
             * all the way from the top.
             */
            nucsyn_dilute_shell(mmixed, Xmixed,
                                star->mshell[nsh], star->X[nsh]);
            mmixed += star->mshell[nsh];
            mixdepth = mmixed;

            if(MIXDEBUG)
                StripDebug(": post-mix mu = %15.10g\n",
                           nucsyn_effective_molecular_weight(star->X[nsh],
                                                             star->stellar_type,
                                                             stardata->store->molweight));
        }
        else
        {
            /* no more mixing */
            nsh++; // shift to next shell up for final X copy loop
            if(MIXDEBUG) StripDebug(": no mix\n");
            break;
        }

        /* update mass co-ordinate */
        m -= star->mshell[nsh];
    }

    /* copy mixed abundances to all mixed shells */
    Shell_index ish;
    for(ish=star->topshell;ish>=nsh;ish--)
    {
        Copy_abundances(Xmixed,star->X[ish]);
    }
    Safe_free(Xmixed);

    /*
     * Regrid : in mixed shells regrid to maximum
     *          shell mass if possible.
     */
    if(1)
    {
        for(ish=star->topshell-1;ish>=nsh;ish--)
        {
            if(star->mshell[ish] + star->mshell[ish-1] < mshellmax)
            {
                /* shift all shells above this down by one */
                star->mshell[ish-1] += star->mshell[ish];
                Shell_index b;
                for(b=ish; b<star->topshell; b++)
                {
                    star->mshell[b] = star->mshell[b+1];
                    Copy_abundances(star->X[b+1],star->X[b]);
                }
                star->topshell--;
            }
        }
    }


    /* remove dmacc to disable traditional thermohaline mixing code */
    star->dmacc = 0.0;

    /* copy top shell to Xenv for other binary_c functionality */
    Copy_abundances(star->X[star->topshell],star->Xenv);

#ifdef MIXLOGGING
    {
        double m = star->mass;
        Shell_index n;
        for(n=star->topshell; n>=0; n--)
        {
            m -= 0.5* star->mshell[n];
            /*
              StripDebug("Log star %d time %g shell %d m %g dm %g\n",
                       star->starnum,
                       stardata->model.time,
                       n,
                       m,
                       star->mshell[n]);
            */
            double *XX = &star->X[n][0];
            fprintf(stardata->tmpstore->strip_and_mix_fp[star->starnum],
                    "%g %d %g %g %g %g %g %g %g %g\n",
                    stardata->model.time,
                    n,
                    m,
                    XX[XH1],
                    XX[XHe4],
                    XX[XLi7],
                    XX[XC12],
                    XX[XN14],
                    XX[XO16],
                    nucsyn_effective_molecular_weight(XX,
                                                      star->stellar_type,
                                                      stardata->store->molweight)
                );

            m -= 0.5* star->mshell[n];
            if(m<0 && n>0)
            {
                Exit_binary_c(2,"negative mass error : m = %g",m);
            }
        }
        fprintf(stardata->tmpstore->strip_and_mix_fp[star->starnum],"\n");
        fflush(stardata->tmpstore->strip_and_mix_fp[star->starnum]);

    }
#endif


    //if(star->starnum==1)
    if(0)
    {
        printf("XSURF%d st=%d H1=%g He4=%g C12=%g (companion He4=%g C12=%g mu=%g) mutop=%g munext=%g, mixdepth=%g\n",
               star->starnum,
               star->stellar_type,
               star->Xenv[XH1],
               star->Xenv[XHe4],
               star->Xenv[XC12],
               stardata->star[Other_star(star->starnum)].Xenv[XHe4],
               stardata->star[Other_star(star->starnum)].Xenv[XC12],
               nucsyn_effective_molecular_weight(stardata->star[Other_star(star->starnum)].Xenv,
                                                 star->stellar_type,
                                                 stardata->store->molweight),
               nucsyn_effective_molecular_weight(star->X[star->topshell],
                                                 star->stellar_type,
                                                 stardata->store->molweight),
               nucsyn_effective_molecular_weight(star->X[star->topshell-1],
                                                 star->stellar_type,
                                                 stardata->store->molweight),
               mixdepth
            );
    }
}

#endif // NUCSYN && NUCSYN_STRIP_AND_MIX
