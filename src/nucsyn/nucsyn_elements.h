#ifndef NUCSYN_ELEMENTS_H
#define NUCSYN_ELEMENTS_H

/* Constants for converting atomic mass and number (A,Z) to index numbers */
#define NUCSYN_A_MIN 1
#define NUCSYN_A_MAX 272
#define NUCSYN_N_MIN 1
#define NUCSYN_N_MAX 162
#define NUCSYN_Z_MIN 1
#define NUCSYN_Z_MAX (NUCSYN_A_MAX-NUCSYN_N_MAX)

/*
 * strings for element names (NB first (index 0) is for neutrons! index is 
 * atomic number
 */
#define NUCSYN_SHORT_ELEMENT_STRINGS {"n","H","He","Li","Be","B","C","N","O","F","Ne","Na","Mg","Al","Si","P","S","Cl","Ar","K","Ca","Sc","Ti","V","Cr","Mn","Fe","Co","Ni","Cu","Zn","Ga","Ge","As","Se","Br","Kr","Rb","Sr","Y","Zr","Nb","Mo","Tc","Ru","Rh","Pd","Ag","Cd","In","Sn","Sb","Te","I","Xe","Cs","Ba","La","Ce","Pr","Nd","Pm","Sm","Eu","Gd","Tb","Dy","Ho","Er","Tm","Yb","Lu","Hf","Ta","W","Re","Os","Ir","Pt","Au","Hg","Tl","Pb","Bi","Po","At","Rn","Fr","Ra","Ac","Th","Pa","U","Np","Pu","Am","Cm","Bk","Cf","Es","Fm","Md","No","Lr","Rf","Db","Sg","Bh","Hs","Mt","Uun","Uuu","Uub","Uut","Uuq","Uup","Uuh","Uus","Uuo"}
#define NUMBER_OF_ELEMENTS 118

#endif // NUCSYN_ELEMENTS_H

