#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef NUCSYN

double nucsyn_effective_molecular_weight(const Abundance * const Restrict X,
                                         const Stellar_type stellar_type,
                                         const double * Restrict const molweights)
{
    /*
     * Function to obtain the (mean) molecular weight of an abundance array (X)
     * - requires stardata for atomic numbers and weights.
     *
     * Note that this returns an "effective" mean moleuclar weight
     * which is useful for sorting a star for thermohaline mixing:
     *
     * Compact objects return an anomalously high weight (=10*stellar-type)
     * to force them to sink.
     *
     * The definition of the molecular weight, mu, is,
     *
     * 1/mu = 1/mu_e + 1/mu_I
     *
     * which includes terms due to electrons and ions,
     *
     * mu_e = sum of X_i * Z_i / A_i,
     *
     * and
     *
     * mu_I = sum of X_i / A_i.
     *
     * We assume full ionisation.
     */
    double mu;

    /*
     * Check if star is degenerate using the stellar type. If it is, then mu
     * has to be altered to the value appropriate for degenerate material.
     */
    if(COMPACT_OBJECT(stellar_type))
    {
        /*
         * Set to something very high!
         * Assume black holes are denser than neutron stars, which are denser
         * than ONeWDs, then COWDs, then HeWDs - seems reasonable!
         */
        mu = fabs( (double) stellar_type ) * 10.0;
    }
    else
    {
        /* non-degenerate star */
        double mu_sum[NUCSYN_STRIDE_N] = {0.0};
        Nucsyn_isotope_stride_leftarray_loop(mu_sum, += X[i] ? (X[i] * molweights[i]) : 0.0);
        mu = 1.0 / (AMU_GRAMS * Nucsyn_stride_sum(mu_sum));
    }

    return mu;
}

#endif
