#ifndef NUCSYN_SN_RPROCESS_H
#define NUCSYN_SN_RPROCESS_H

#include "nucsyn_sn_core_collapse_rprocess.def"
#undef X
#define X(CCTYPE,STRING) NUCSYN_CCSN_RPROCESS_##CCTYPE,
enum { CORE_COLLAPSE_RPROCESS_ALGORITHMS };
#undef X

#endif // NUCSYN_SN_RPROCESS_H
