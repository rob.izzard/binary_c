#pragma once
#ifndef NUCSYN_NOVA_ALGORITHMS_CODES_H
#define NUCSYN_NOVA_ALGORITHMS_CODES_H

#include "nucsyn_nova_algorithms.def"

/*
 * construct nova algorithm codes
 */

/*
 * CO WD accretors
 */
#undef X
#define X(CODE,VAL,TEXT) NOVA_YIELD_CO_ALGORITHM_##CODE = VAL,
enum { NOVA_YIELD_CO_ALGORITHMS_CODES };

#undef X
#define X(CODE,VAL,TEXT) #CODE,
static char * nova_yield_CO_algorithms_code_macros[] Maybe_unused = { NOVA_YIELD_CO_ALGORITHMS_CODES };

#undef X
#define X(CODE,VAL,TEXT) TEXT,
static char * nova_yield_CO_algorithms_code_strings[] Maybe_unused = { NOVA_YIELD_CO_ALGORITHMS_CODES };

#define nova_CO_algorithms_string(N) Array_string(nova_yield_CO_algorithms_code_strings,(N))


/* ONe WD accretors */

#undef X
#define X(CODE,VAL,TEXT) NOVA_YIELD_ONe_ALGORITHM_##CODE = VAL,
enum { NOVA_YIELD_ONe_ALGORITHMS_CODES };

#undef X
#define X(CODE,VAL,TEXT) #CODE,
static char * nova_yield_ONe_algorithms_code_macros[] Maybe_unused = { NOVA_YIELD_ONe_ALGORITHMS_CODES };

#undef X
#define X(CODE,VAL,TEXT) TEXT,
static char * nova_yield_ONe_algorithms_code_strings[] Maybe_unused = { NOVA_YIELD_ONe_ALGORITHMS_CODES };

#define nova_ONe_ALGORITHMS_string(N) Array_string(nova_yield_ONe_algorithms_code_strings,(N))



#undef X


#endif // NUCSYN_NOVA_ALGORITHMS_CODES_H
