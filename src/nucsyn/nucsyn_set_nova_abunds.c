#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef NUCSYN
#include "nucsyn_novae.h"
#include "nucsyn_novae_Jose2022.h"

void nucsyn_set_nova_abunds(struct stardata_t * const stardata,
                            const struct star_t * const star,
                            Abundance * Restrict const Xdonor,
                            Abundance * Restrict const Xnova)
{

    /*
     * Start with the abunances of the donor so that heavy
     * elements are unchanged.
     */
    Copy_abundances(Xdonor,Xnova);

#ifdef NUCSYN_NOVAE

    /*
     * Choose the tables of yields we use for novae.
     */
    struct data_table_t * table_JH98 = NULL;
    struct data_table_t * table_extra = NULL;
    if(star->stellar_type == COWD)
    {
        /*
         * All COWDs currently use Jose and Hernanz 1998
         */
        table_JH98 = stardata->store->novae_JH98_CO;
    }
    if(star->stellar_type == ONeWD)
    {
        /*
         * For ONeWDs we use Jose and Hernanz 1998
         * with the option of updating some isotopes
         * from Jose 2022 (provided by Gavin Lotay)
         */
        table_JH98 = stardata->store->novae_JH98_ONe;
        if(stardata->preferences->nova_yield_ONe_algorithm ==
           NOVA_YIELD_ONe_ALGORITHM_JOSE_2022)
        {
            table_extra = stardata->store->novae_Jose2022_ONe;
        }
    }
    else
    {
        /* default for all other types */
        table_JH98 = stardata->store->novae_JH98_CO;
    }

    if(table_JH98)
    {
        /* fitting parameter (x) and return values (r) */
        const double x[1] = {star->mass};
        double r[NUCSYN_NOVAE_JH98_NUM_ISOTOPES];

        /* Do the interpolation */
        Interpolate(table_JH98,x,r,INTERPOLATE_USE_CACHE);

        /*
         * data are:
         * 1H 3He 4He 7Be 11B 12C 13C 14N 15N 16O 17O 18O
         * 19F 20Ne 21Ne 22Na 22Ne 23Na 24Mg 25Mg 26Al
         * 26Mg 27Al 28Si 29Si 30Si 31P 32S 33S
         * 34S 35Cl 36S 36Ar 37Cl 38Ar 39K
         */
        Xnova[XH1]=r[0];
        Xnova[XHe3]=r[1];
        Xnova[XHe4]=r[2];
        Xnova[XLi7]=r[3]; // was Be7 but this will decay to Li7
        Xnova[XB11]=r[4];
        Xnova[XC12]=r[5];
        Xnova[XC13]=r[6];
        Xnova[XN14]=r[7];
        Xnova[XN15]=r[8];
        Xnova[XO16]=r[9];
        Xnova[XO17]=r[10];
        Xnova[XO18]=r[11];
        Xnova[XF19]=r[12];
        Xnova[XNe20]=r[13];
        Xnova[XNe21]=r[14];
        Xnova[XNa22]=r[15];
        Xnova[XNe22]=r[16];
        Xnova[XNa23]=r[17];
        Xnova[XMg24]=r[18];
        Xnova[XMg25]=r[19];
        Xnova[XAl26]=r[20];
        Xnova[XMg26]=r[21];
        Xnova[XAl27]=r[22];
        Xnova[XSi28]=r[23];
        Xnova[XSi29]=r[24];
        Xnova[XSi30]=r[25];
#ifdef NUCSYN_ALL_ISOTOPES
        Xnova[XP31]=r[26];
        Xnova[XS32]=r[27];
        Xnova[XS33]=r[28];
        Xnova[XS34]=r[29];
        Xnova[XCl35]=r[30];
        Xnova[XS36]=r[31];
        Xnova[XAr36]=r[32];
        Xnova[XCl37]=r[33];
        Xnova[XAr38]=r[34];
        Xnova[XK39]=r[35];
#endif//NUCSYN_ALL_ISOTOPES
#ifdef NANCHECKS
        {
            Boolean isanan=FALSE;
            /* extra nanchecks for novae */
            Forward_isotope_loop(i)
            {
                if(isnan(Xnova[i])!=0)
                {
                    printf("Nova abund %u is NAN!\n",i);
                    isanan=TRUE;
                }
            }
            if(isanan==TRUE)
            {
                Forward_isotope_loop(i)
                {
                    printf("Nova abund %u is %g\n",i,Xnova[i]);
                }
            }
        }
#endif // NANCHECKS

    }

    if(table_extra &&
       star->stellar_type == ONeWD &&
       stardata->preferences->nova_yield_ONe_algorithm ==
       NOVA_YIELD_ONe_ALGORITHM_JOSE_2022)
    {
        /*
         * replace mass fractions calculated above
         * with Jose 2022 yields: these are only for some
         * isotopes, so it's just a replacement
         */
        const double x[1] = {star->mass};
        double r[NUCSYN_NOVAE_JOSE2022_ONe_NUM_ISOTOPES];

        /* Do the interpolation */
        Interpolate(table_extra,x,r,INTERPOLATE_USE_CACHE);

        /* set the Xnova values */
        static const Isotope isotopes[] = { NOVAE_JOSE2022_ISOTOPES };
        for(int i=0; i<NUCSYN_NOVAE_JOSE2022_ONe_NUM_ISOTOPES; i++)
        {
            Xnova[isotopes[i]] = r[i];
            Dprint("Set nova yield of isotope %s (%u) to %g using Jose 2022 table\n",
                   nucsyn_isotope_strings[isotopes[i]],
                   isotopes[i],
                   Xnova[isotopes[i]]);
        }
    }

    /* normalize abundance to 1.0 */
    nucsyn_renormalize_abundance(Xnova);

    return;

#endif /* NUCSYN_NOVAE */
}
#endif // NUCSYN
