#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef NUCSYN
double nucsyn_total_yield(struct stardata_t * Restrict const stardata)
{
    /*
     * Return the total yielded mass
     */
    double Myield = 0.0;
    Foreach_star(star)
    {
        Myield += sum_C_double_array(star->Xyield, ISOTOPE_ARRAY_SIZE);
        Myield += star->other_yield;
    }
    return Myield;
}

#endif//NUCSYN
