#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef NUCSYN

void nucsyn_thermohaline_mix(struct stardata_t * const stardata,
                             const Star_number k, // accreting star number
                             Abundance * const Xacc, // accretion abundances
                             const double dmacc, // mass accreted
                             const Abundance * const Xenv, // envelope abundances: pre mix
                             Abundance * const nXenv // envelope abundances: post mix (written)
    )
{
    SETstar(k); // the accreting star

    /*
     * Material sinks into the envelope because its molecular weight is
     * greater than that in the envelope. In this function we determine
     * the depth to which it sinks.
     *
     * Remember: star k is accreting while Other_star(k) is losing material.
     */
#ifdef MIXDEBUG
    printf("MIXING sink or swim? SINK!\n");
    printf("MIXING Accretion layer sinks and mixes with the envelope so delete any existing accretion layer\n");
    printf("MIXING mix mass %g with mass %g\n",dmacc,Envelope_mass(k));
#endif//MIXDEBUG

      /* Accretion layer sinks */
    const double menv = nucsyn_envelope_mass(star);
    //const double * const Xsolar = stardata->preferences->zero_age.Xsolar;

    Dprint("menv = %g, dmacc = %g\n",
           menv,
           dmacc);

    /* Fully mix accretion layer with envelope */
    if(likely(Xenv != nXenv))
    {
        nucsyn_dilute_shell_to(
            /* Accretion layer... */
            dmacc,
            Xacc,
            /* envelope ... */
            menv,
            Xenv,
            /* New envelope */
            nXenv);
    }
    else
    {
        nucsyn_dilute_shell(
            /* Accretion layer... */
            dmacc,
            Xacc,
            /* envelope ... */
            menv,
            Xenv
            );
    }

    if(ON_MAIN_SEQUENCE(star->stellar_type))
    {
        /*
         * If the star is a (hydrogen) main sequence star
         * then redefine Xinit and MZAMS
         */
        star->effective_zams_mass=Max(star->mass,star->effective_zams_mass);
#ifdef MIXDEBUG
        printf("MIXING Redefine Xinit star %d (type %d) X was %g now %g (diff %10.10f %%) \n",
               k,
               star->stellar_type,
               star->Xinit[XH1],
               nXenv[XH1],
               (star->Xinit[XH1]-nXenv[XH1])*100.0/
               (star->Xinit[XH1]+TINY)

            );
#endif//MIXDEBUG
    }
}
#endif // NUCSYN
