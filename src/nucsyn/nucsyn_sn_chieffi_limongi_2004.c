#include "../binary_c.h"
No_empty_translation_unit_warning;


#if defined NUCSYN

void nucsyn_sn_chieffi_limongi_2004(struct stardata_t * const stardata,
                                    Abundance * const X,
                                    const double mco,
                                    const Abundance z,
                                    const double mcut,
                                    struct star_t * const star Maybe_unused)
{
    Abundance y[150];// interpolation result

#ifdef NUCSYN_CCSNE_CHIEFFI_LIMONGI_2004_PORTINARI

/*
 * Supernovae yields of Chieffi and Limongi 2004, corrected by
 * Portinari method to show only the yield from the CO core
 * if NUCSYN_CCSNE_CHIEFFI_LIMONGI_2004_PORTINARI is defined,
 * or as a function of the mass cut if it is not. You should
 * NOT define NUCSYN_CCSNE_CHIEFFI_LIMONGI_2004_PORTINARI except
 * as an academic exericise, as it is less accurate.
 *
 * Sets X as mass fractions.
 */

#include "nucsyn_sn_chieffi_limongi_2004_2.h"
    double x[2]; // free parameters
    /*
     * Use the old table, with yields from their paper corrected
     * via Portinari's method
     */
    Const_data_table table[]={CHIEFFI_LIMONGI_2004_SN_YIELDS_TABLE}; // data table

    // 38 table lines
    // 143 entries in each
    // 2 free parameters
    x[0]=z;
    x[1]=Max(2.09,Min(10.8163,mco));

    Dprint("Chieffi/Limongi 2004 (portinari) : Input x[0]=%g x[1]=%g\n",x[0],x[1]);

#ifdef NUCSYN_CCSNE_CHIEFFI_LIMONGI_2004_EXTRAPOLATE
    /*
     * Enable extrapolation of yields to Z>0.02
     */
    double m,Dz=z-0.02; // extra z
    Abundance ylow[150];
    Abundance yhigh[150];
    Isotope j;

    if(z>0.02)
    {
        /*
         * CL04 give tables for 0.006 and 0.02,
         * we want to extrapolate from these
         */
        Dprint("Extrapolate CL04 Dz=%g\n",Dz);
        x[0]=0.006;
        rinterpolate(table,
                     stardata->persistent_data->rinterpolate_data,
                     2,
                     120,
                     CHIEFFI_LIMONGI_2004_SN_YIELDS_TABLE_LINES,
                     x,
                     ylow,
                     0);
        x[0]=0.02;
        rinterpolate(table,
                     stardata->persistent_data->rinterpolate_data,
                     2,
                     120,
                     CHIEFFI_LIMONGI_2004_SN_YIELDS_TABLE_LINES,
                     x,
                     yhigh,
                     0);
        for(j=0;j<150;j++)
        {
            m=(yhigh[j]-ylow[j])/0.014; // slope
            y[j]=yhigh[j]+m*Dz; // high value + extra slope
        }
        Dprint("Na23 low %g high %g extrap %g\n",ylow[XNa23],yhigh[XNa23],y[XNa23]);

    }
    else
    {
#endif//NUCSYN_CCSNE_CHIEFFI_LIMONGI_2004_EXTRAPOLATE

      /* Interpolate on the table of yields */
        rinterpolate(table,
                     stardata->persistent_data->rinterpolate_data,
                     2,
                     120,
                     CHIEFFI_LIMONGI_2004_SN_YIELDS_TABLE_LINES,
                     x,
                     y,
                     0);

#ifdef NUCSYN_CCSNE_CHIEFFI_LIMONGI_2004_EXTRAPOLATE
    }
#endif

    // set ejecta abundances
    X[XH1]+=y[2]; X[XH2]+=y[3]; X[XHe3]+=y[4]; X[XHe4]+=y[5]; X[XLi6]+=y[6]; X[XLi7]+=y[7]; X[XBe9]+=y[8]; X[XB10]+=y[9]; X[XB11]+=y[10]; X[XC12]+=y[11]; X[XC13]+=y[12]; X[XN14]+=y[13]; X[XN15]+=y[14]; X[XO16]+=y[15]; X[XO17]+=y[16]; X[XO18]+=y[17]; X[XF19]+=y[18]; X[XNe20]+=y[19]; X[XNe21]+=y[20]; X[XNe22]+=y[21]; X[XNa23]+=y[22]; X[XMg24]+=y[23]; X[XMg25]+=y[24]; X[XMg26]+=y[25]; X[XAl27]+=y[26]; X[XSi28]+=y[27]; X[XSi29]+=y[28]; X[XSi30]+=y[29];

#ifdef NUCSYN_ALL_ISOTOPES
    X[XP31]+=y[30]; X[XS32]+=y[31]; X[XS33]+=y[32]; X[XS34]+=y[33]; X[XS36]+=y[34]; X[XCl35]+=y[35]; X[XCl37]+=y[36]; X[XAr36]+=y[37]; X[XAr38]+=y[38]; X[XAr40]+=y[39]; X[XK39]+=y[40]; X[XK40]+=y[41]; X[XK41]+=y[42]; X[XCa40]+=y[43]; X[XCa42]+=y[44]; X[XCa43]+=y[45]; X[XCa44]+=y[46]; X[XCa46]+=y[47]; X[XCa48]+=y[48]; X[XSc45]+=y[49]; X[XTi46]+=y[50]; X[XTi47]+=y[51]; X[XTi48]+=y[52]; X[XTi49]+=y[53]; X[XTi50]+=y[54]; X[XV50]+=y[55]; X[XV51]+=y[56]; X[XCr50]+=y[57]; X[XCr52]+=y[58]; X[XCr53]+=y[59]; X[XCr54]+=y[60]; X[XMn55]+=y[61]; X[XFe54]+=y[62]; X[XFe56]+=y[63]; X[XFe57]+=y[64]; X[XFe58]+=y[65]; X[XCo59]+=y[66]; X[XNi58]+=y[67]; X[XNi60]+=y[68]; X[XNi61]+=y[69]; X[XNi62]+=y[70]; X[XNi64]+=y[71]; X[XCu63]+=y[72]; X[XCu65]+=y[73]; X[XZn64]+=y[74]; X[XZn66]+=y[75]; X[XZn67]+=y[76]; X[XZn68]+=y[77]; X[XZn70]+=y[78]; X[XGa69]+=y[79]; X[XGa71]+=y[80]; X[XGe70]+=y[81];

    /* radioactive isotopes : these should be completely ignored! */
    //  X[XC14]+=y[121]; X[XNa22]+=y[122]; X[XAl26]+=y[123];  X[XCl36]+=y[125]; X[XCa41]+=y[127]; X[XCa45]+=y[128]; X[XTi44]+=y[129]; X[XV49]+=y[130]; X[XMn53]+=y[131]; X[XMn54]+=y[132]; X[XFe55]+=y[133]; X[XFe60]+=y[134]; X[XCo57]+=y[135]; X[XCo60]+=y[136]; X[XNi56]+=y[137]; X[XNi57]+=y[138]; X[XNi59]+=y[139]; X[XNi63]+=y[140]; X[XZn65]+=y[141]; X[XGe68]+=y[142];

#endif

#ifdef NUCSYN_CCSNE_CHIEFFI_LIMONGI_ELEMENTAL_Na
    /*
     * Override Portinari-method yields for Na23 with Chieffi's
     * elemental yield for Na, which should be almost all Na23 anyway
     */
#include "Na23.h"
    Const_data_table Na23table[]={CL04_Na23_TABLE};
    double xNa[3];
    double Na[1];
    xNa[0]=z;
    xNa[1]=Max(1.6912,Min(6.4905,mco));
    xNa[2]=mcut/mco;

    rinterpolate(Na23table,
                 stardata->persistent_data->rinterpolate_data,
                 3,
                 1,
                 CL04_Na23_TABLE_LINES,
                 xNa,
                 Na,
                 0);

    //printf("CL04Na23 %g c.f. Portinari %g\n",Na[0],y[22]);
    y[22]=Na[0]; // Override Portinari method result
#endif

#ifdef NUCSYN_ALL_ISOTOPES
    /* If extended s-process is defined, we have these isotopes */
    X[XGe72]+=y[82]; X[XGe73]+=y[83]; X[XGe74]+=y[84]; X[XGe76]+=y[85]; X[XAs75]+=y[86];  X[XSe76]+=y[88]; X[XSe77]+=y[89]; X[XSe78]+=y[90]; X[XSe80]+=y[91]; X[XSe82]+=y[92]; X[XBr79]+=y[93]; X[XBr81]+=y[94]; X[XKr80]+=y[96]; X[XKr82]+=y[97]; X[XKr83]+=y[98]; X[XKr84]+=y[99]; X[XKr86]+=y[100]; X[XRb85]+=y[101]; X[XRb87]+=y[102];  X[XSr86]+=y[104]; X[XSr87]+=y[105]; X[XSr88]+=y[106]; X[XY89]+=y[107]; X[XZr90]+=y[108]; X[XZr91]+=y[109]; X[XZr92]+=y[110]; X[XZr94]+=y[111]; X[XZr96]+=y[112]; X[XNb93]+=y[113]; X[XMo92]+=y[114]; X[XMo94]+=y[115]; X[XMo95]+=y[116]; X[XMo96]+=y[117]; X[XMo97]+=y[118]; X[XMo98]+=y[119];X[XKr81]+=y[144]; X[XKr85]+=y[145]; X[XSr90]+=y[146]; X[XZr93]+=y[147];
    X[XSe74]+=y[87];
    X[XKr78]+=y[95];
    X[XSe79]+=y[143];
    X[XSr84]+=y[103];
#endif

/* These isotopes are not defined at present */
/*
  X[XH3]+=y[120];
  X[XAr39]+=y[126];
  X[XSi32]+=y[124];
*/
#else // PORTINARI check

    /*
     * 17/09/2006
     * Now we have the isotopic yields from Chieffi! :)
     * Yields are in a table of Z,mco,mcut/mco, ...yields...
     */

#include "nucsyn_sn_chieffi_limongi_2004_mcut_isotopic.h"
    /*
     * Interpolate directly using the mass cut
     */
    Const_data_table table[]={CHIEFFI_LIMONGI_2004_ISOTOPIC_YIELDS_TABLE_DATA};
    Dprint("Chieffi/Limongi 2004\n");

#ifdef NUCSYN_CCSNE_CHIEFFI_LIMONGI_2004_EXTRAPOLATE
#warning Extrapolation is not yet available for non-portinari CL04 method
#endif

    /* Interpolate on the table of yields */
    rinterpolate(table,
                 stardata->persistent_data->rinterpolate_data,
                 3,
                 111,
                 CHIEFFI_LIMONGI_2004_ISOTOPIC_YIELDS_TABLE_LINES,
                 (const double[]){
                     Max(0.0,Min(0.02,z)),
                     mco,
                     Max(0.0,Min(1.0,mcut/mco))
                 },
                 y,
                 0);

    // set ejecta abundances

    /* This set of isotopes includes those involved in the s-process */
    X[XH2]+=y[0]; X[XHe4]+=y[1]; X[XLi6]+=y[2]; X[XLi7]+=y[3]; X[XB10]+=y[4]; X[XB11]+=y[5]; X[XC12]+=y[6]; X[XC13]+=y[7]; X[XN14]+=y[8]; X[XN15]+=y[9]; X[XO16]+=y[10]; X[XO17]+=y[11]; X[XO18]+=y[12]; X[XF19]+=y[13]; X[XNe20]+=y[14]; X[XNe21]+=y[15]; X[XNe22]+=y[16]; X[XNa23]+=y[17]; X[XMg24]+=y[18]; X[XMg25]+=y[19]; X[XMg26]+=y[20]; X[XAl27]+=y[21]; X[XSi28]+=y[22]; X[XSi29]+=y[23]; X[XSi30]+=y[24]; X[XFe56]+=y[58];

#ifdef NUCSYN_ALL_ISOTOPES
    X[XP31]+=y[25]; X[XS32]+=y[26]; X[XS33]+=y[27]; X[XS34]+=y[28]; X[XS36]+=y[29]; X[XCl35]+=y[30]; X[XCl37]+=y[31]; X[XAr36]+=y[32]; X[XAr38]+=y[33]; X[XAr40]+=y[34]; X[XK39]+=y[35]; X[XK40]+=y[36]; X[XK41]+=y[37]; X[XCa40]+=y[38]; X[XCa42]+=y[39]; X[XCa43]+=y[40]; X[XCa44]+=y[41]; X[XCa46]+=y[42]; X[XCa48]+=y[43]; X[XSc45]+=y[44]; X[XTi46]+=y[45]; X[XTi47]+=y[46]; X[XTi48]+=y[47]; X[XTi49]+=y[48]; X[XTi50]+=y[49]; X[XV50]+=y[50]; X[XV51]+=y[51]; X[XCr50]+=y[52]; X[XCr52]+=y[53]; X[XCr53]+=y[54]; X[XCr54]+=y[55]; X[XMn55]+=y[56]; X[XFe54]+=y[57];  X[XFe57]+=y[59]; X[XFe58]+=y[60]; X[XCo59]+=y[61]; X[XNi58]+=y[62]; X[XNi60]+=y[63]; X[XNi62]+=y[64]; X[XNi64]+=y[65]; X[XCu63]+=y[66]; X[XCu65]+=y[67]; X[XZn64]+=y[68]; X[XZn66]+=y[69]; X[XZn67]+=y[70]; X[XZn68]+=y[71]; X[XZn70]+=y[72]; X[XGa69]+=y[73]; X[XGa71]+=y[74]; X[XGe70]+=y[75]; X[XGe72]+=y[76]; X[XGe73]+=y[77]; X[XGe74]+=y[78]; X[XGe76]+=y[79]; X[XAs75]+=y[80]; X[XSe76]+=y[81]; X[XSe77]+=y[82]; X[XSe78]+=y[83]; X[XSe80]+=y[84]; X[XSe82]+=y[85]; X[XBr79]+=y[86]; X[XBr81]+=y[87]; X[XKr80]+=y[88]; X[XKr82]+=y[89]; X[XKr83]+=y[90]; X[XKr84]+=y[91]; X[XKr86]+=y[92]; X[XRb85]+=y[93]; X[XRb87]+=y[94]; X[XSr86]+=y[95]; X[XSr87]+=y[96]; X[XSr88]+=y[97]; X[XY89]+=y[98]; X[XZr90]+=y[99]; X[XZr91]+=y[100]; X[XZr92]+=y[101]; X[XZr94]+=y[102]; X[XZr96]+=y[103]; X[XNb93]+=y[104]; X[XMo92]+=y[105]; X[XMo94]+=y[106]; X[XMo95]+=y[107]; X[XMo96]+=y[108]; X[XMo97]+=y[109]; X[XMo98]+=y[110];
#endif // NUCSYN_ALL_ISOTOPES

#ifdef NUCSYN_LIMONGI_CHIEFFI_2006_Al26
    /*
     * Al26 from Limongi and Chieffi 2006: note this includes the
     * wind loss AND SN loss.
     *
     * We interpolate as a function of mass, but in binaries
     * we should really use the maximum MS mass as this sets
     * the eventual core mass.
     */
    const double m = star->phase[MAIN_SEQUENCE].maximum_mass;
    rinterpolate((const double[]){
            11.0, 1.6,
            12.0, 2.11,
            13.0, 2.45,
            14.0, 10.4,
            15.0, 13.2,
            16.0, 6.8,
            17.0, 6.87,
            20.0, 5.43,
            25.0, 8.61,
            30.0, 9.93,
            35.0, 8.38,
            40.0, 12.1,
            60.0, 25.2,
            80.0, 40.0,
            120.0, 70.3
        },
        stardata->persistent_data->rinterpolate_data,
        1,
        1,
        15,
        (const double[]){m},
        y,
        0);
    const double Al = y[0]*1e-5;
    X[XMg26] -= Al;
    X[XAl26] += Al;
#endif // Al26

    nucsyn_renormalize_abundance(X);

    Dprint("SN CL04 mco=%g Z=%g C=%g C13=%g O=%g Fe56=%g\n",mco,z,X[XC12],X[XC13],X[XO16],X[XFe56]);

#endif // NUCSYN_CCSNE_CHIEFFI_LIMONGI_2004_PORTINARI


}


#endif // NUCSYN
