#include "../binary_c.h"
No_empty_translation_unit_warning;


#if defined NUCSYN &&                           \
    defined NUCSYN_THIRD_DREDGE_UP

/* Various prescriptions for third dredge-up */

/************************************************************/
/****************** Third dredge up *************************/
/************************************************************/

/*
 * This function does third dredge up in TPAGB stars:
 * 1) the core mass is reduced
 * 2) the ex-core material is mixed into the envelope.
 *
 * It assumes dredgeup occurs instantaneouly.
 * It can deal with dntp lots of dredgeup at once (so you can
 * choose long timesteps).
 * It returns lambda, the dredge-up efficiency.
 */

double nucsyn_third_dredge_up(struct stardata_t * const Restrict stardata,
                              struct star_t * const newstar)
{
    double dm_dredge_up; /* mass dredged up in 3rd dredge up */
    const double menv = newstar->mass - Outermost_core_mass(newstar);

    if(newstar->dntp > 0.5 &&
       newstar->lambda_3dup > TINY)
    {
        /*
         * Third dredgeup has occurred
         *
         * calculate mass dredged up from the change in core mass due to
         * hydrogen burning during the previous pulse(s) - dntp is not required
         * provided it is always positive - if it should ever be negative then
         * this screws up
         */
        dm_dredge_up = newstar->dm_3dup;

        if(dm_dredge_up>TINY)
        {
            /*
             * Set the abundances of the third dredge up material - it may be
             * burned in the hot bottom burning shell: dup_material contains
             * unburned 3dup material dup_burned contains 3dup material that is
             * burned
             */
            Abundance * dup_material = New_clear_isotope_array;
            nucsyn_set_third_dredgeup_abunds(dup_material,
                                             newstar,
                                             stardata->common.metallicity,
                                             stardata,
                                             Outermost_core_mass(newstar));

            Dprint("DUPB mix dredge up material masses: intershell %g (H=%g He=%g C=%g), envelope: %g (H=%g He=%g C=%g)\n",
                   dm_dredge_up,
                   dup_material[XH1],
                   dup_material[XHe4],
                   dup_material[XC12],
                   menv - dm_dredge_up,
                   newstar->Xenv[XH1],
                   newstar->Xenv[XHe4],
                   newstar->Xenv[XC12]
                );
#if DEBUG==1
            double x=dup_material[XC12];
#endif

            /* Mix the dredgeup "shell" with the envelope */
            nucsyn_mix_shells(dm_dredge_up,
                              dup_material,
                              menv,
                              newstar->Xenv);

#if DEBUG==1
            Dprint("DUP ntp=%g intershell C12=%g log C12=%g\n",
                   newstar->num_thermal_pulses,x,log10(newstar->Xenv[XC12]));
#endif

            Safe_free(dup_material);
        }
        Dprint("DUP dmc += %g\n",-dm_dredge_up);

    }

#ifdef NUCSYN_THIRD_DREDGE_UP_HYDROGEN_SHELL
    /*
     * The hydrogen burning shell should be dredged up,
     * and if we've had a few pulses the abundance in the
     * shell will depend on the surface/convective envelope
     * abundance too!
     */
    if(newstar->num_thermal_pulses>2 && menv>0.01)
    {
        /* set up dup material abundances */
        Abundance * dup_material = New_isotope_array_from(newstar->Xenv);

        /*
         * this fits M=1.25,1.75,2 reasonably well:
         * Amanda's M=1 model is not reliable (she says!)
         */
        dup_material[XC13]=dup_material[XC12]*0.006;
        dup_material[XN14]=dup_material[XC12]*0.28;
        dm_dredge_up=2e-3
            // H-shell mass (RS says this is from X=0.1 to X=0.7)
            // Note my fudge turn-off around 2.2Msun
            *(1.0/(1.0+pow(1e-1,(2.2-newstar->mass))))
            *Min(1.0,Pow2(newstar->num_thermal_pulses/10.0)) // turn-on parameter (heat up)
            *1.0/(1.0+pow(1e-20,(menv-0.1)))
            ; // phase-out parameter for small envelopes

        if(dm_dredge_up>TINY)
        {
            /* force Xtot=1 (minor correction):
             * NB put in Helium to prevent problems in STPAGB stars which
             * have H->0 (due to HBB)
             */
            dup_material[XHe4] += 1.0-nucsyn_totalX(dup_material);

            /* mix into the envelope */
            nucsyn_mix_shells(dm_dredge_up,dup_material,
                              menv,newstar->Xenv);
        }
        Safe_free(dup_material);
    }
#endif // NUCSYN_THIRD_DREDGE_UP_HYDROGEN_SHELL


    return newstar->lambda_3dup;
}

#endif /* NUCSYN_THIRD_DREDGE_UP && NUCSYN */
