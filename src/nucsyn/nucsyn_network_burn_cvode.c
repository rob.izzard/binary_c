#include "../binary_c.h"
No_empty_translation_unit_warning;

#if defined NUCSYN &&                           \
    defined __HAVE_LIBSUNDIALS_CVODE__

double nucsyn_network_burn_cvode(double * Restrict const Nin Maybe_unused,
                                 const double * Restrict const sigmav Maybe_unused,
                                 const double maxt Maybe_unused,
                                 nucsyn_burnfunc burnfunc Maybe_unused,
                                 nucsyn_logfunc logfunc Maybe_unused,
                                 const double dtguess Maybe_unused,
                                 struct stardata_t * const Restrict stardata Maybe_unused,
                                 const Reaction_network network_id Maybe_unused,
                                 const Boolean vb
    )
{

    /*
     * This step is not required with CVODE: time evolution
     * is handled entirely in the CVODE routine. Eventually
     * deprecate this function, but we still need it
     * to wrap the functional form of the burnfunc()
     */

    return burnfunc == NULL ? 0.0 :
        burnfunc(stardata,
                 Nin,
                 sigmav,
                 maxt);
}

#endif //NUCSYN && __HAVE_LIBSUNDIALS_CVODE__
