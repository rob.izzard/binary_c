#include "../binary_c.h"
No_empty_translation_unit_warning;

double Constant_function nucsyn_main_sequence_convective_envelope_size(double x,
                                                                       double y,
                                                                       double z)
{
    /* 
     * Function which fits the MS outer convective envelope mass
     * as a function of 
     * x=metallicity (solar scaled mixture)
     * y=ZAMS mass
     * z=fractional MS age
     *
     * Valid for 0.1<=M_ZAMS<=1.2, 1e-4<=Z<=0.02, 0.0<=z<=1.0
     */
    x=Max(1e-4,Min(0.02,x));
    y=Max(0.1,Min(1.2,y));
    z=Max(0.0,Min(1.0,z));
    
    return(Max(0.0,Min(((1.33880e-01)+(1.03170e+01)*x)*y,((8.88890e-03)+(3.14510e+00)*x)*pow(y,((-3.10390e+00)+(6.57920e+01)*x))+(-1.12180e-02)+(-1.16710e+00)*x)*(1.0+(-8.03840e-01)*z+(3.48890e-01)*z*z)));

}
