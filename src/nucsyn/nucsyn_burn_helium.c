#include "../binary_c.h"

No_empty_translation_unit_warning;


#ifdef NUCSYN

static void helium_Jacobian(double *const*const Restrict  J,
                           const double * Restrict const N,
                           const double * Restrict const sigmav);
static void helium_derivatives(const double * Restrict const y,
                              double * Restrict const dydt,
                              const double * Restrict const sigmav,
                              const double * Restrict const N);

double nucsyn_burn_helium(struct stardata_t * Restrict const stardata,
                          double * Restrict const Nin,
                          const double * Restrict const sigmav,
                          const double h)
{
    double err;
    double **Jhelium;
#define NSPECIES 5
    if(stardata->tmpstore->network_jacobians[NUCSYN_NETWORK_helium] == NULL)
    {
        Allocate_network_jacobian(NUCSYN_NETWORK_helium,
                                  NSPECIES);
    }

    Jhelium = stardata->tmpstore->network_jacobians[NUCSYN_NETWORK_helium];
    double y_heburn[NSPECIES+1],
        y2_heburn[NSPECIES+1],
        f1_heburn[NSPECIES+1],
        f2_heburn[NSPECIES+1],
        f3_heburn[NSPECIES+1],
        f4_heburn[NSPECIES+1];
    int indx_heburn[NSPECIES+1];
    y_heburn[1] = Nin[XHe4];
    y_heburn[2] = Nin[XC12];
    y_heburn[3] = Nin[XO16];
    y_heburn[4] = Nin[XNe20];
    y_heburn[5] = Nin[XMg24];

    err=kaps_rentrop_GSL(stardata,
                         h,
                         y_heburn,
                         y2_heburn,
                         NSPECIES,
                         Jhelium,
                         sigmav,
                         Nin,
                         &helium_Jacobian,
                         &helium_derivatives,
                         f1_heburn,
                         f2_heburn,
                         f3_heburn,
                         f4_heburn,
                         stardata->preferences->nucsyn_network_error[NUCSYN_NETWORK_helium],
                         TRUE,
                         indx_heburn);
    if(err<=1.0)
    {
        Nin[XHe4] = y_heburn[1];
        Nin[XC12] = y_heburn[2];
        Nin[XO16] = y_heburn[3];
        Nin[XNe20] = y_heburn[4];
        Nin[XMg24] = y_heburn[5];
    }
    return err;
}

#define HE_FAC1 (3.0/6.0)
#define HE_FAC2 (1.0/6.0)

static void helium_derivatives(const double * Restrict const y,
                               double * Restrict const dydt,
                               const double * Restrict const sigmav,
                               const double * Restrict const N Maybe_unused)
{
    const double a = sigmav[SIGMAV_3He4] * Pow3(y[1]);
    const double b = sigmav[SIGMAV_C12_He4] * y[2] * y[1];
    const double c = sigmav[SIGMAV_O16_He4] * y[3] * y[1];
    const double d = sigmav[SIGMAV_Ne20_He4] * y[4] * y[1];

    dydt[1] = -a*HE_FAC1 -b -c -d;
    dydt[2] = a*HE_FAC2 - b;
    dydt[3] = b - c;
    dydt[4] = c - d;
    dydt[5] = d;

    /*
    dydt[1] =
        -1/2*sigmav[SIGMAV_3He4] * Pow3(N[XHe4])
        -sigmav[SIGMAV_C12_He4] * N[XC12] * N[XHe4]
        -sigmav[SIGMAV_O16_He4] * N[XO16] * N[XHe4];
        -sigmav[SIGMAV_Ne20_He4] * N[XNe20] * N[XHe4];
    dydt[2] =
        +1/6*sigmav[SIGMAV_3He4] * Pow3(N[XHe4])
        - sigmav[SIGMAV_C12_He4] * N[XC12] * N[XHe4];
    dydt[3] =
        + sigmav[SIGMAV_C12_He4] * N[XC12] * N[XHe4]
        - sigmav[SIGMAV_O16_He4] * N[XO16] * N[XHe4];
    dydt[4] =
        + sigmav[SIGMAV_O16_He4] * N[XO16] * N[XHe4]
        - sigmav[SIGMAV_Ne20_He4] * N[XNe20] * N[XHe4];
    dydt[5] =
        + sigmav[SIGMAV_Ne20_He4] * N[XNe20] * N[XHe4];
    */
}

static void helium_Jacobian(double *const*const Restrict J,
                            const double * Restrict const N,
                            const double * Restrict const sigmav)
{
    const double dummy0 = sigmav[SIGMAV_3He4] * 3.0 * Pow2(N[XHe4]) ;
    const double dummy1 = sigmav[SIGMAV_C12_He4] * N[XC12];
    const double dummy2 = sigmav[SIGMAV_O16_He4] * N[XO16];
    const double dummy3 = sigmav[SIGMAV_Ne20_He4] * N[XNe20];
    const double dummy4 = sigmav[SIGMAV_C12_He4] * N[XHe4];
    const double dummy5 = sigmav[SIGMAV_O16_He4] * N[XHe4];
    const double dummy6 = sigmav[SIGMAV_Ne20_He4] * N[XHe4];

    J[1][1] =
        -dummy0*HE_FAC1
        -dummy1
        -dummy2
        -dummy3;
    J[1][2] = -dummy4;
    J[1][3] = -dummy5;
    J[1][4] = -dummy6;
    J[1][5] = 0.0;

    J[2][1] =
        + dummy0*HE_FAC2
        - dummy1;
    J[2][2] = - dummy4;
    J[2][3] = 0.0;
    J[2][4] = 0.0;
    J[2][5] = 0.0;

    J[3][1] =
        + dummy1
        - dummy2;
    J[3][2] =
        + dummy4;
    J[3][3] =
        - dummy5;
    J[3][4] = 0.0;
    J[3][5] = 0.0;

    J[4][1] =
        + dummy2
        - dummy3;
    J[4][2] = 0.0;
    J[4][3] =
        + dummy5;
    J[4][4] =
        - dummy6;
    J[4][5] = 0.0;

    J[5][1] = dummy3;
    J[5][2] = 0.0;
    J[5][3] = 0.0;
    J[5][4] = dummy6;
    J[5][5] = 0.0;
}

#endif//NUCSYN
