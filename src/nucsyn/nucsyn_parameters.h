#pragma once
#ifndef NUCSYN_PARAMETERS_H
#define NUCSYN_PARAMETERS_H

#include "../binary_c_parameters.h"

#ifdef NUCSYN
/*
 * The binary_c stellar population nucleosynthesis framework.
 *
 * Contact:  rob.izzard@gmail.com
 *
 * https://binary_c.gitlab.io/
 * https://gitlab.com/binary_c/binary_c
 * https://groups.google.com/forum/#!forum/binary_c-nucsyn-announce
 * https://groups.google.com/forum/#!forum/binary_c-nucsyn-devel
 * https://twitter.com/binary_c_code
 * https://www.facebook.com/groups/149489915089142/
 *
 * Please see the files README, LICENCE and CHANGES
 *
 *
 * nucsyn_parameters.h
 *
 *
 * Use these macros to switch certain parts of the nucleosynethesis on/off
 * and select optional parts of the code. Some options are required: for
 * example turning off NUCSYN_TPAGB will probably mess lots of things up. You
 * can always turn off NUCSYN in binary_c_parameters.h (the code must compile
 * either with or without NUCSYN defined) which will disable all
 * nucleosynthesis routines.
 */

#include "nucsyn_networks.h"


/*
 * Enable tabular strip/dredge up, based on RI's models
 * made for 1e-4<Z<0.03, 0.8<M<20 using the TWIN code.
 *
 * This enables you to do case A/B mass transfer,
 * and get the H, He, C12, N14 and O16 right at first dredge up.
 *
 * Currently limited and does not do C13, N15, O17 etc.
 * RI hopes to fix this at some point! (Holly to help?)
 *
 * NB The depth of first dredge up is based on older
 * models for Z=0.02 only, but this is expected to be
 * weak function of metallicity. RI: Hopefully Holly
 * will better fit these models soon...
 *
 * Enable this if you want to follow CNO changes during
 * case A and B mass transfer, and it will do first dredge
 * up for these elements as well.
 *
 * Model details are in Izzard et al. (2018).
 *
 * Does nothing post-first dredge up: these stars are treated as
 * previously.
 */
//#define NUCSYN_STRIP_AND_MIX

#ifdef NUCSYN_STRIP_AND_MIX

#define NUCSYN_STRIP_AND_MIX_NSHELLS_START 200
#define NUCSYN_STRIP_AND_MIX_NSHELLS_MAX 750

#endif//NUCSYN_STRIP_AND_MIX

/*
 * Either use Evert or Holly's table for first
 * dredge up envelope depths.
 */
//#define FIRST_DREDGE_UP_EVERT
#define FIRST_DREDGE_UP_HOLLY
//#define FIRST_DREDGE_UP_HOLLY_TESTING

#include "../tables/table_TAMS.h"


/*
 * First dredge up
 *
 * Activate first dredge up. There are several algorithms which can be
 * used, as defined below.
 */
#define NUCSYN_FIRST_DREDGE_UP

#ifdef NUCSYN_FIRST_DREDGE_UP

/*
 * Guess first dredge up changes at the end of the main sequence, rather than
 * on the giant branch. In this way, only the actual envelope abundances
 * which burn are considered, rather than the real envelope abundance.
 *
 * The easiest way to do this is to mix in the abundances
 * according to the given prescription (based on TAMS envelope abundance)
 * and *then* mix in the accretion layer, to give Delta X... complicated!
 */
#define NUCSYN_PREGUESS_FIRST_DREDGE_UP

/*
 * Use a table of abundance changes at first dredge up
 * based on Amanda Karakas' 2002 and 2010 models.
 *
 * NB If you enable NUCSYN_STRIP_AND_MIX
 * above, then elements which are dealt with by that
 * algorithm (currently H, He, C12, N14, O16) are *not*
 * set by the Karakas tables, rather they are dealt
 * with by the strip tables.
 */
#define NUCSYN_FIRST_DREDGE_UP_AMANDAS_TABLE

#ifdef NUCSYN_FIRST_DREDGE_UP_AMANDAS_TABLE

/*
 * Correct first DUP changes for accretion effects when using
 * Amanda's table
 */
#define NUCSYN_FIRST_DREDGE_UP_ACCRETION_CORRECTION
/*
 * Define this to use the terminal-age main sequence abundances in the
 * envelope (which may or _may not_ have been mixed by thermohaline
 * mixing) as the guide for the 1st DUP accretion correction factor.
 * This is recommended, because it allows you to turn off thermohaline mixing.
 *
 * NB Doing this requires an extra array (XTAMS) for each star.
 */
//#define NUCSYN_FIRST_DREDGE_UP_ACCRETION_CORRECTION_FROM_TAMS

/*
 * Phase in first DUP as a function of Mc or L?
 */
#define NUCSYN_FIRST_DREDGE_UP_PHASE_IN

/*
 * define NUCSYN_FIRST_DREDGE_UP_RICHARDS_TABLE on top of
 * NUCSYN_FIRST_DREDGE_UP_AMANDAS_TABLE. If the M/Z is in the range
 * of Richard's models, it is used, otherwise Amanda's changes are used.
 */
#define NUCSYN_FIRST_DREDGE_UP_RICHARDS_TABLE

#endif // NUCSYN_FIRST_DREDGE_UP_AMANDAS_TABLE

#endif //NUCSYN_FIRST_DREDGE_UP

/************************************************************/

/*
 *
 * This option is DEPRECATED and will be REMOVED.
 *
 * At some point the core masses will be inconsistent between
 * Amanda's models (mc1tp == EAGB he core mass)
 * and Jarrod's (CHeB/EAGB/TPAGB transition).
 *
 * If you define NUCSYN_SMOOTH_CORE_MASS_TRANSITION then
 * the core mass will be smooth BUT the core mass at the first
 * thermal pulse will be that of the Hurley et al 2002 models,
 * not Amanda Karakas' 2002 models. In this case the nucleosynthesis
 * on the TPAGB will be "less consistent" with Amanda's models and
 * *during the EAGB* the mcx (CO core mass) and the EAGB lifetime
 * will be wrong.
 *
 * If you do not define NUCSYN_SMOOTH_CORE_MASS_TRANSITION then
 * there will be a jump at the CHeB/EAGB boundary which forces
 * the EAGB (helium) core mass to be that at the first thermal
 * pulse. Then the TPAGB is correct, and will match Amanda's models,
 * but there is a jump in the core mass at the start of the EAGB,
 * which may also be bad and will cause the luminosity to jump
 * in the case of second dredge up. However, the CO core mass (mcx)
 * during the EAGB will be correct.
 *
 * You choose. I would go with it, because without it you'll have
 * problems around 10-20Msun with too many electron capture SNe!
 */
//#define NUCSYN_SMOOTH_CORE_MASS_TRANSITION

/************************************************************/

/*
 * Second dredge up
 * Activate second dredge up : you want this
 */
#define NUCSYN_SECOND_DREDGE_UP

/************************************************************/

/*
 * TPAGB stuff
 *
 * by default we use the Karakas et al 2002 fits for
 * mc1tp, mcmin, lambda etc.
 */

/* define this to use fits to the Padova group Mc1tp over the mass
 * range M<3 Msun rather than Amanda's fits */
//#define PADOVA_MC1TP

/*
 * Activate third dredge up : you want this.
 * If you want no dredge up please set lambda = 0 instead.
 */
#define NUCSYN_THIRD_DREDGE_UP

#ifdef NUCSYN_THIRD_DREDGE_UP


/*
 * Activate crude code to include the H-burning shell
 * in the dredged-up material. Might be required
 * at low metallicity.
 */
#define NUCSYN_THIRD_DREDGE_UP_HYDROGEN_SHELL

/*
 * Allow boost of third DUP material by command line multipliers
 */
//#define NUCSYN_THIRD_DREDGE_UP_MULTIPLIERS



/*
 * Intershell abundances: these are the abundances of the
 * material dredged up during third dredge up.
 *
 * Choose either :
 * The 2002 model:
 *
 * This uses Amanda's models for the light elements, and
 * Gallino/Busso tables (courtesy of Maria Lugaro) if NUCSYN_S_PROCESS
 * is also defined.
 *
 * The 2012 model:
 *
 * This combines the third dredge up abundances with s-process
 * but only for Z=1e-4 (pending extra data).
 */
#define USE_TABULAR_INTERSHELL_ABUNDANCES_KARAKAS_2002

/* if you're using the old table, use the old s-process as well */
#ifdef USE_TABULAR_INTERSHELL_ABUNDANCES_KARAKAS_2002
#define NUCSYN_S_PROCESS
#define C13_POCKET_CORRECTION
#define STD_M_C13_POCKET 7.3e-4
#define DEFAULT_MC13_FRACTION 0.05
#define DEFAULT_C13_EFFICIENCY 1.0
#endif //USE_TABULAR_INTERSHELL_ABUNDANCES_KARAKAS_2002

#endif // NUCSYN_THIRD_DREDGE_UP

/*************************************************************/

/* TPAGB stage - you definitely want this */
#define NUCSYN_TPAGB

#ifdef NUCSYN_TPAGB


/*
 * Lithium up to the TPAGB as a function of mass and pulse number
 */
#define LITHIUM_TABLES
//#define LITHIUM_LOGGING


/*
 * Activate George Angelou's lithium algorithm
 */
//#define NUCSYN_ANGELOU_LITHIUM

#ifdef NUCSYN_ANGELOU_LITHIUM
/*
 * minimum lithium to be considered non-zero:
 * algorithms are disabled below this
 */
#define NUCSYN_ANGELOU_LITHIUM_MINIMUM_Li7 1e-30

/*
 * maxmimum timestep factor: use this multiplied
 * by the decay time to set the maximum timestep
 * when the lithium abundance exceeds
 * NUCSYN_ANGELOU_LITHIUM_MINIMUM_Li7
 */
#define NUCSYN_ANGELOU_LITHIUM_MAX_TIMESTEP_FACTOR 0.1

/* turn on logging */
#define NUCSYN_ANGELOU_LITHIUM_LOGGING

#endif // NUCSYN_ANGELOU_LITHIUM



// Enable this to check if we should get a late thermal pulse: this
// is not currently implemented so ignore it.
//#define LATE_THERMAL_PULSES
// This is the fraction of the thermal pulse we should have gone
// through after which a late thermal pulse might occur
//#define LATE_THERMAL_PULSE_THRESHOLD 0.9

/* HBB in TPAGB : you want this */
#define NUCSYN_TPAGB_HBB

/* HBB temperature multiplication factor */
#define HBBTFAC_DEFAULT 1.0


/*
 * How would you like to burn?
 *
 * Choose NUCSYN_ANAL_BURN for the old-fashioned, analytic nuclear
 * network solutions as described in RGI's PhD thesis.
 *
 * Choose NUCSYN_NUMERICAL_BURN to solve the equations numerically
 * with a Kaps-Rentrop 4th order semi-implicit method with
 * automatically varying timestep)
 *
 * On a modern CPU, NUCSYN_NUMERICAL_BURN should be faster. Use it.
 */
//#define NUCSYN_ANAL_BURN
#define NUCSYN_NUMERICAL_BURN

/* redefine mass fractions in calls to nucsyn_hbb? */
//#define NUCSYN_HBB_RENORMALIZE_MASS_FRACTIONS

/*
 * Allow command line boost of HBB with --HBBboost
 */
//#define NUCSYN_BOOST_HBB

/*
 * Nuclear reaction rates
 *
 * Choose one of
 *
 * RATES_OF_AMANDA : Uses the rates of the MSSSP code which made
 * the models for Amanda
 *
 * RATES_OF_MARIA Maria's rate (from Christian Illiadis)
 *
 * Or none of the above: defaults to NACRE rates
 */

/* define this to use the rates in Amanda's models for calibration */
//#define RATES_OF_AMANDA

/* define this to use the Ne22(pg) rate of CF88 for some masses: only M=5 Z=1e-4 */
//#define RATES_OF_AMANDA_NE22PG_FIX

/*
 * define this to use Maria's rates
 */
//#define RATES_OF_MARIA

/* define this to use Arend Jan Poelarends' rates where applicable */
//#define RATES_OF_AREND_JAN




/************************************************************/
/************************************************************/
/************************************************************/

// s-process logging (HS/LS etc) : only if you care (I don't)
//#define NUCSYN_HS_LS_LOG

/*
 * Simulate proton ingestion on the first pulse, also
 * known as the "dual shell flash": this has not been well tested.
 */
//#define NUCSYN_HUGE_PULSE_CRISTALLO


#endif //NUCSYN_TPAGB

/*
 * Activate radioactive decay of elements : you want this. NOTE I have
 * not included all isotopes in the decay, just those that have been relevant
 * to me in the past. You want to check the list and make sure it includes
 * all those you are looking at!
 */
#define NUCSYN_RADIOACTIVE_DECAY

/************************************************************/

/*
 * Enable this to force dredge up in giants which go into common
 * envelope. This implies that if the envelope is completely ejected
 * we get the correct yields, and also if it *isn't* then the secondary
 * must have moved all the way through it, mixing it up anyway.
 */
#define NUCSYN_FORCE_DUP_IN_MERGERS

/************************************************************/

/* WR evolution and options : you want this,
 * but be warned that it will clash with
 * NUCSYN_STRIP_AND_MIX, so when that is applied
 * this is turned off. Hmm.
 */

/*
 *  for stars with initial mass > NUCSYN_WR_MASS_BREAK
 *  the surface abundances are followed by the nucsyn_WR
 *  function. Below this mass the nucsyn_set_1st_dup_abunds
 *  function is used to set the surface abundances after
 *  first dredge-up. A value of 8 is usual, but if you want to
 *  work on stars which undergo HBB you will want to set it
 *  higher (e.g. 13 to work with STPAGB stars).
 *  NB nucsyn_WR sets the first_dredge_up parameter to *prevent*
 *  stars from having two first dredge-ups. So this should be ok
 *  at 8.0
 *
 * this mass also serves as the mass BELOW which the TPAGB luminosity
 * function is used, above which the old function is extrapolated
 */
#define NUCSYN_WR_MASS_BREAK 8.0

#ifndef NUCSYN_STRIP_AND_MIX
#define NUCSYN_WR

/* Perhaps use experimental tabular fits for WR abundances */
#define NUCSYN_WR_TABLES

/* Perhaps use Richard Stancliffe's surface abundances */
#define NUCSYN_WR_RS_TABLE

// enable this to allow use of the WR code at non-solar metallicity
// you probably want this option!
#define NUCSYN_WR_METALLICITY_CORRECTIONS

/*
 * Activate Rob's section attempt to get the accretion
 * abundances correct
 */
#define NUCSYN_WR_ACCRETION
#endif // NUCSYN_STRIP_AND_MIX

/************************************************************/

/* Novae and Supernovae */

// enable this to return yields for Z=0 supernovae below a threshold
// metallicity
//#define NUCSYN_LOWZ_SUPERNOVAE

#ifdef NUCSYN_LOWZ_SUPERNOVAE
#define NUCSYN_LOWZ_SNE_THRESHOLD 0.02e-4
#endif

/* novae : you want this */
#define NUCSYN_NOVAE

/*
 * Supernovae
 */


/*
 * Chieffi & Limongi (2004) options
 */

/*
 * Enable this to use CL04's yields from their paper
 * with the portinari method. However, you probably do NOT
 * want this, as Chieffi has provided me with the yields as
 * a function of the mass cut!
 */
//#define NUCSYN_CCSNE_CHIEFFI_LIMONGI_2004_PORTINARI

/*
 * Enable this to extrapolate beyond CL04's Z=0.02 models to
 * higher metallicity
 */
//#define NUCSYN_CCSNE_CHIEFFI_LIMONGI_2004_EXTRAPOLATE

/*
 * Enable this to use Chieff's elemental Na yield instead of the Portinari
 * estimate (which is probably wrong)
 */
//#define NUCSYN_CCSNE_CHIEFFI_LIMONGI_ELEMENTAL_Na

/* Al26 separate from Mg26, Z=0.02 as f(M_ZAMS) only */
#define NUCSYN_LIMONGI_CHIEFFI_2006_Al26



/*
 * Include electron-capture SN yields from Wanajo et al 2008
 * ApJ submitted. Also define the ejected mass and the model
 * to use.
 */
#define NUCSYN_SN_ELECTRON_CAPTURE_WANAJO_2008
#define ELECTRON_CAPTURE_EJECTED_MASS 1.39e-2
// choose ST (standard) or FP3 model but not both
#define NUCSYN_SN_ELECTRON_CAPTURE_WANAJO_2008_ST
//#define NUCSYN_SN_ELECTRON_CAPTURE_WANAJO_2008_FP3


/*
 * Enable this to have heterogeneous He stars at explosion:
 * very dodgy!
 */
//#define NUCSYN_HETEROGENEOUS_HE_STAR


/*
 * r-process yield macros
 *
 * Choose either Arlandini 1999 or Simmerer 2004 r-process
 *
 * The Arlandini function will give you isotopic abundances if they
 * are available.
 *
 * Simmerer's data, while newer, is for elements only
 */

/*
 * The mass cut is the lower limit atomic mass of r-process isotopes:
 * 88 is used by Arlandini et al 1999, which gives you r-processing for
 * Y89 and heavier.
 * But you might want to set the cut to 72 (Ge72) when using Woosley
 * and Weaver's SN yields, or 69 when using Chieffi and Limongi (2004)'s
 * yields, in order to maintain continuity.
 */
#define NUCSYN_R_PROCESS_ARLANDINI1999_MASS_CUT 88

/************************************************************/

/* Logging and yield calculations */


/* Logging : generic on/off. Not sure what it does now...
 * probably best to leave it and turn logging on and off below for
 * each specific case.
 */
#define NUCSYN_LOGGING

// Used to calibrate the free parameters for HBB to amanda's models.
// You probably don't want it because I have done the calibration and
// all attempts to improve it have failed.
//#define NUCSYN_CALIBRATION_LOG

/* Yield output:
 * Do calculations: (needed for logging) you want this (or lots of
 * other things break!
 */
#define NUCSYN_YIELDS

/* define this for more accurate floating point yield output */
//#define NUCSYN_LONG_YIELD_FORMAT

/* show m_p style yields for binaries */
//#define NUCSYN_LOG_BINARY_MPYIELDS

/*
 * Log yields every timestep (final yields will always be logged if NUCSYN_YIELDS
 * is defined) : useful for time-dependent GCE
 */
#define NUCSYN_LOG_YIELDS_EVERY_TIMESTEP

/*
 * X yields are just the mass of each isotope which is expelled from
 * the system. MP yields are the M*p_M type yields (which are less useful
 * to me).
 *
 * X yields for binary - this is standard and you want this
 */
#define NUCSYN_LOG_BINARY_X_YIELDS

// Enable this for differential yields (see also NUCSYN_LOG_YIELDS_EVERY_TIMESTEP)
#define NUCSYN_LOG_BINARY_DX_YIELDS

// X yields for each star
//#define NUCSYN_LOG_SINGLE_X_YIELDS

// Also MP yields
//#define NUCSYN_LOG_MPYIELDS

// differentials for each star
//#define NUCSYN_LOG_INDIVIDUAL_DX_YIELDS

/* surface abundances vs time */
//carlo-change
//#define NUCSYN_ABUNDANCE_LOG

/* Other logs */

/* define NUCSYN_GCE to modify yield output for gce.pl
 * NB this just activates other defines, and turns some off.
 */
//#define NUCSYN_GCE

#ifdef NUCSYN_GCE

#ifndef STELLAR_POPULATIONS_ENSEMBLE_SPARSE
#define STELLAR_POPULATIONS_ENSEMBLE_SPARSE
#endif

#ifndef STELLAR_COLOURS
#define STELLAR_COLOURS
#endif

#ifndef SINGLE_STAR_LIFETIMES
#define SINGLE_STAR_LIFETIMES
#endif

#endif //NUCSYN_GCE

/* extras */

// output velocity of winds from the system which might be outflows
// from the galaxy
//#define NUCSYN_GCE_OUTFLOW_CHECKS

#ifdef NUCSYN_GCE_OUTFLOW_CHECKS
// outflow velocity factor, so the wind is this * escape velocity (not
// for supernovae) : NB this is similar to BETA defined for wind collisions
#define NUCSYN_GCE_OUTFLOW_FACTOR BETA

// outflow velocity from supernovae in km/s
#define NUCSYN_GCE_SUPERNOVA_OUTFLOW_VELOCITY 2000.0

// escape fraction for material that IS faster than the limit
#define NUCSYN_GCE_OUTFLOW_ESCAPE_FRACTION 0.5
#endif//NUCSYN_GCE_OUTFLOW_CHECKS

/*
 * Enable this to have yields EXACTLY every STELLAR_POPULATIONS_ENSEMBLE_DT_DEFAULT
 */
//#define NUCSYN_YIELDS_VS_TIME

/*
 * Experimental: if yields have not changed, do not output
 */
//#define NUCSYN_YIELD_COMPRESSION

/* nucsyn_short_log stuff - needed for CSLF determinations */
//#define NUCSYN_SHORT_LOG

/* normal log : use with cf_amanda.pl and cf_models-varMZ.pl */
#define NUCSYN_CF_AMANDA_LOG

/* special log for testing globular cluster abundance ratios */
//#define NUCSYN_Globular_Cluster_LOG

/* long log stuff -broken? */
//#define NUCSYN_LONG_LOG

/* J-star logging */
//#define NUCSYN_J_LOG

/* R-star logging */
//#define NUCSYN_R_STAR_LOG

/* Giants : intrinsic and extrinsic */
//#define NUCSYN_GIANT_ABUNDS_LOG

/* s process logging */
//#define NUCSYN_S_PROCESS_LOG

/*x structural information */
//#define NUCSYN_STRUCTURE_LOG

/* Planetary Nebula checks */
//#define NUCSYN_PLANETARY_NEBULAE

/* Log accretion layer abundances for Richard Stancliffe */
//#define RS_LOG_ACCREITON_LOG

/************************************************************/
/* Code options */

/*
 * Enable this to see information on how long the TPAGB routine takes to do
 * stuff: does this still work?
 */
//#define NUCSYN_TPAGB_RUNTIME

/* Enable this to identify the source of yields */
#define NUCSYN_ID_SOURCES

/* This is a newer version (requires NUCSYN_ID_SOURCES) */
#ifdef NUCSYN_ID_SOURCES
#define NUCSYN_ID_SOURCES_GCE
#endif//NUCSYN_ID_SOURCES

// enable this to normalize dodgy abundances prior to comenv
#define COMENV_NORMALIZE_ABUNDS

/* If you have NaNs after introducing a new isotope, try this to see
 * what is wrong */
//#define NUC_MASSES_DEBUG


/*
 * Interpolate sigmav rather than calculate from formulae: faster?
 */
#define NUCSYN_SIGMAV_PRE_INTERPOLATE

/*
 * Resolution in t9 for the sigmav table
 *
 * We have this many points between _MIN and _MID,
 * then the same again between _MID and _MAX
 */
#define NUCSYN_SIGMAV_INTERPOLATION_RESOLUTION 200

/*
 * Range in t9,
 * _MIN : 0.001 for >MS
 * _MID : 0.13 is ~ max of T9 in STPAGB stars
 * _MAX : 10.0 is ~NSE
 */
#define NUCSYN_SIGMAV_INTERPOLATION_MIN 0.001
#define NUCSYN_SIGMAV_INTERPOLATION_MID 0.13
#define NUCSYN_SIGMAV_INTERPOLATION_MAX 10.0


/*
 * Define this to interpolate in log(t9) rather than t9
 */
#define NUCSYN_SIGMAV_INTERPOLATE_LOGT9
/*
 * Define this to interpolate in log(sigmav) rather than sigmav - this
 * allows you to use many fewer points in the interpolation table
 * yet maintain accuracy
 */
#define NUCSYN_SIGMAV_INTERPOLATE_LOGSIGMAV

/*
 * Minimum value for each sigmav: note this should be small but
 * if you square it should still be a number (e.g. 1e-200 squared
 * is usually zero, but 1e-100 squared is 1e-200)
 */
#define SIGMAV_TINY 1e-100

/*
 * Enable this to allow hot reactions, i.e. logT>>8, which really
 * you shouldn't need (not enabling it removes a load of if statements
 * from the code, so should be faster)
 */
//#define NUCSYN_HOT_SIGMAV

/*
 * Enable this to include thermalized corrections to nuclear reaction
 * rates. Not usually required.
 */
//#define NUCSYN_THERMALIZED_CORRECTIONS

/*
 * Enable this to calculate helium-burning reaction rates. Not used at present.
 */
//#define HELIUM_BURNING_REACTIONS

/*
 * Define this to allow --no_production on the command line
 * which prevents the surface abundances from changing i.e.
 * there is no production.
 *
 * NB Not tested with binary_c V2.0
 */
//#define NUCSYN_ALLOW_NO_PRODUCTION

/************************************************************/

/*
 * If NUCSYN_GCE is defined, make sure we define NUCSYN_CF_AMANDA_LOG,
 * NUCSYN_LOG_YIELDS_EVERY_TIMESTEP, NUCSYN_YIELDS and
 * NUCSYN_LOG_BINARY_X_YIELDS
 */
#ifdef NUCSYN_GCE

/*
 * The latest yields_vs_time can make use of all
 * these switches to VASTLY improve yield
 * calculation!
 */
#define COMPRESS_YIELD_OUTPUT
#define NUCSYN_SPARSE_YIELDS
#define NUCSYN_LOG_BINARY_DX_YIELDS
#define NUCSYN_IGNORE_ZERO_BINARY_DX_YIELD
#define NUCSYN_YIELDS_VS_TIME
#define NUCSYN_YIELD_COMPRESSION
#define NUCSYN_ID_SOURCES
#define NUCSYN_ID_SOURCES_GCE
#define NUCSYN_ALL_ISOTOPES

#ifndef NUCSYN_LOG_YIELDS_EVERY_TIMESTEP
#define NUCSYN_LOG_YIELDS_EVERY_TIMESTEP
#endif //NUCSYN_LOG_YIELDS_EVERY_TIMESTEP

#ifndef NUCSYN_YIELDS
#define NUCSYN_YIELDS
#endif//NUCSYN_YIELDS

#ifndef NUCSYN_LOG_BINARY_X_YIELDS
#define NUCSYN_LOG_BINARY_X_YIELDS
#endif//NUCSYN_LOG_BINARY_X_YIELDS

#endif // NUCSYN_GCE

#ifdef RATES_OF_MARIA
// remember to use long yield formats with Maria's rates!
#ifndef NUCSYN_LONG_YIELD_FORMAT
#define NUCSYN_LONG_YIELD_FORMAT
#endif//NUCSYN_LONG_YIELD_FORMAT
// and nuclear reaction rate multipliers
// and not amanda's abundances or rates
#undef RATES_OF_AMANDA
#undef RATES_OF_AREND_JAN
// and not supernovae, WR, novae, s-process etc.
#undef NUCSYN_NOVAE
#undef NUCSYN_WR
#undef NUCSYN_ITERATIVE_BURN

#endif //RATES_OF_MARIA


/* experimental nuclear burning */
//#define NUCSYN_NETWORK_TEST

#ifdef RSTARS
/*
 * R stars stuff: if you are making models of R stars
 * you probably don't need the whole nucleosynthesis suite
 */

#undef RATES_OF_AMANDA
#undef RATES_OF_MARIA
#undef RATES_OF_AREND_JAN
#undef NUCSYN_NOVAE
#undef NUCSYN_WR
#undef NUCSYN_ITERATIVE_BURN
#endif//RSTARS

/*
 * Enable this to test the nuclear network code.
 */
//#define NUCSYN_NETWORK_TEST
//#define NUCSYN_NETWORK_STATS

/* barium star log */
//#define LOG_BARIUM_STARS

#ifdef LOG_BARIUM_STARS
#define NUCSYN_ALL_ISOTOPES
#endif


/*
 * Accretion layers sink if mu_acc > NUCSYN_MU_FUZZ * mu_env
 * ... the nucsyn_WR routine introduces slight errors on H1,He4
 * which make accretion stable when it shouldn't be, and this
 * fudge factor takes that into account.
 */
#define NUCSYN_MU_FUZZ 0.99

/*
 * Enable Lars Mattsson's mass-loss prescription for C-rich stars
 */
//#define MATTSSON_MASS_LOSS

/*
 * Mg26/Mg24 ratio project with Dave Yong
 */
//#define YONG_PROJECT

#ifdef LIKE_AMANDA
/*
 * Make binary_c/nucsyn as similar to Amanda's models as possible.
 * NB This might be difficult with all the changes to binary_c V2.0
 */

#undef NUCSYN_FIRST_DREDGE_UP
#define NUCSYN_FIRST_DREDGE_UP

#undef NUCSYN_PREGUESS_FIRST_DREDGE_UP
#undef NUCSYN_FIRST_DREDGE_UP_AMANDAS_TABLE
//#define NUCSYN_FIRST_DREDGE_UP_AMANDAS_TABLE

#undef NUCSYN_FIRST_DREDGE_UP_ACCRETION_CORRECTION
#undef NUCSYN_FIRST_DREDGE_UP_ACCRETION_CORRECTION_FROM_TAMS
#undef NUCSYN_FIRST_DREDGE_UP_PHASE_IN
#undef NUCSYN_FIRST_DREDGE_UP_RICHARDS_TABLE

#undef NUCSYN_SECOND_DREDGE_UP
#define NUCSYN_SECOND_DREDGE_UP

#undef NUCSYN_THIRD_DREDGE_UP
#define NUCSYN_THIRD_DREDGE_UP

#undef THIRD_DREDGE_UP_KARAKAS2002
#define THIRD_DREDGE_UP_KARAKAS2002

/* probably best to use the old tables */
#undef USE_TABULAR_INTERSHELL_ABUNDANCES_KARAKAS_2002
#undef USE_TABULAR_INTERSHELL_ABUNDANCES_KARAKAS_2012
#define USE_TABULAR_INTERSHELL_ABUNDANCES_KARAKAS_2002

#undef STELLAR_MASS_IN_LAMBDA_CALC
#define STELLAR_MASS_IN_LAMBDA_CALC *mt

#undef NUCSYN_THIRD_DREDGE_UP_TABULAR_NCAL
#undef NUCSYN_THIRD_DREDGE_UP_HYDROGEN_SHELL
#undef NUCSYN_THIRD_DREDGE_UP_MULTIPLIER

#undef USE_TABULAR_INTERSHELL_ABUNDANCES

#undef NUCSYN_TPAGB
#define NUCSYN_TPAGB

#undef TPAGB_LUMTYPE
#define TPAGB_LUMTYPE TPAGB_LUMINOSITY_AVERAGE

#undef KARAKAS2002_REFITTED_TPAGB_INTERPULSES

#undef NUCSYN_TPAGB_HBB
#define NUCSYN_TPAGB_HBB

#undef RATES_OF_AMANDA
#define RATES_OF_AMANDA

#undef NUCSYN_NUMERICAL_BURN
#define NUCSYN_NUMERICAL_BURN

#undef NUCSYN_SMOOTH_CORE_MASS_TRANSITION

#undef NUCSYN_ANAL_BURN

#endif//LIKE_AMANDA

/* some features do not work with the API */
#include "../API/binary_c_API_excluded_features.h"

//#define NUCSYN_AND_HRD

//#define BLUE_STRAGGLER_BARIUM_PROJECT

/* CN giant project with Paula and Thomas */
//#define CN_THICK_DISC
#ifdef CN_THICK_DISC
#define CN_THICK_DISC_LOGG_MIN_DEFAULT -100.0
#define CN_THICK_DISC_LOGG_MAX_DEFAULT +100.0
#define CN_THICK_DISC_START_AGE_DEFAULT 10e3
#define CN_THICK_DISC_END_AGE_DEFAULT 4e3
#undef NUCSYN_S_PROCESS
#ifndef NUCSYN_STRIP_AND_MIX

/*
 * The paper required NUCSYN_STRIP_AND_MIX ...
 * latest work may not.
 */

//you must build with NUCSYN_STRIP_AND_MIX
#endif // NUCSYN_STRIP_AND_MIX
#endif // CN_THICK_DISC



/*
 * Cannot use NUCSYN_ANGELOU_LITHIUM
 * with the pp-chain HBB, or with
 * LITHIUM_TABLES.
 */
#ifdef NUCSYN_ANGELOU_LITHIUM
#undef NUCSYN_NETWORK_PP
#ifdef LITHIUM_TABLES
You cannot define LITHIUM_TABLES and NUCSYN_ANGELOU_LITHIUM together
#endif
#endif // NUCSYN_ANGELOU_LITHIUM



/*
 * If a square bracket calculation, e.g. [Fe/H], is less
 * in magntiude than NUCSYNSQUARE_BRACKET_FLOOR then it
 * is set to zero. This avoids small errors.
 */
#define NUCSYN_SQUARE_BRACKET_FLOOR 1e-5


/*
 * Turn on strided isotope loops. These are usually faster,
 * but their syntax is more complex.
 */
#define NUCSYN_STRIDED_ISOTOPE_LOOPS

#endif /* NUCSYN */
#endif /* NUCSYN_PARAMETERS_H */
