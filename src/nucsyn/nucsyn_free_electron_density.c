#include "../binary_c.h"
No_empty_translation_unit_warning;


/* a function which might be useful? */
#ifdef NUCSYN
#include "nucsyn_strided_isotope_loops.h"

double Pure_function nucsyn_free_electron_density(const double * Restrict const N,// number density
                                                  const int * Restrict const Z) // atomic numbers

{
    /*
     * calculate electron density assuming all
     * atoms are fully ionised
     */
    Abundance ne_sum[NUCSYN_STRIDE_N] = {0.0};
    Nucsyn_isotope_stride_leftarray_loop(ne_sum, += ((Abundance)Z[i])*N[i]);
    return Nucsyn_stride_sum(ne_sum);
}

#endif // NUCSYN
