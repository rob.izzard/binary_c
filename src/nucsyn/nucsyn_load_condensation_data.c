#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef NUCSYN

/*
 * Load elemental condensation temperatures
 *
 * Currently, we use Lodders' data from 2003 but
 * you could choose from multiple data sets if required
 */


void nucsyn_load_condensation_data(struct stardata_t * const stardata)
{

    size_t lines,length,maxwidth;
    struct string_array_t ** s =
        load_data_as_strings(stardata,
                             "src/dust/Lodders2003_condensation.dat",
                             &lines,
                             &length,
                             &maxwidth,
                             '\t',
                             0,
                             0);
    if(s != NULL)
    {
        stardata->store->condensation_temperatures = Malloc(sizeof(double) * NUMBER_OF_ELEMENTS);
        for(size_t i=0; i<NUMBER_OF_ELEMENTS; i++)
        {
            /*
             * Default to negative (unphysical) temperatures
             */
            stardata->store->condensation_temperatures[i] = -1.0;
        }

        for(size_t i=0; i<length; i++)
        {
            char * const element = s[i]->strings[0];
            printf("DO ALLOC\n");
            const Atomic_number Z = nucsyn_element_to_atomic_number(stardata->store,
                                                                    element,
                                                                    0);
            if(Z>=0 && Z<NUMBER_OF_ELEMENTS)
            {
                for(ssize_t j=0; j<s[i]->n; j++)
                {
                    char * const string = s[i]->strings[j];

                    if(string[0] >= '0' && string[0] <= '9')
                    {
                        stardata->store->condensation_temperatures[Z] = strtod(string,NULL);
                        break;
                    }
                }
            }
            free_string_array(&s[i]);
        }
        Safe_free(s);
    }
}
#endif // NUCSYN
