#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef NUCSYN

/*
 * Function to choose the wind mixing factor f for the binary as a function of
 * the DONOR and ACCRETOR's wind speeds and mass loss rates.
 *
 *
 * Typical values:
 * 0 : RLOF, direct stream accretion, accretor has zero wind
 * 1 : All accreted from accretor's wind, donor has no effect
 */

/* Define donor and accretor */
#define ACCRETOR (k)
#define DONOR (Other_star(k))

double Pure_function nucsyn_choose_wind_mixing_factor(const struct stardata_t * const stardata,
                                                      const Star_number k)
{
    return wind_mixing_factor(stardata,k);
}

#endif //NUCSYN
