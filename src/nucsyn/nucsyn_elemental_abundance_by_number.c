#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef NUCSYN


Abundance nucsyn_elemental_abundance_by_number(const char * Restrict const element,
                                               const Abundance * Restrict const X,
                                               struct stardata_t * const stardata,
                                               struct store_t * const store,
                                               const double density)
{
    /*
     * Given an element string, calculate the elemental abundance
     * by number, given an array of mass fractions and the density.
     *
     * Return -1.0 (unphysical) on a set up call.
     */

    Abundance Nelem; // calculated number density (returned)

    if(unlikely(element==NULL && X==NULL))
    {
        /* set up call */
        if(store->icache==NULL)
        {
            nucsyn_make_icache(store);
        }
        else
        {
            nucsyn_free_icache(store->icache);
        }
        Nelem = -1.0; /* unphysical */
    }
    else
    {
        /*
         * ID isotopes of this element
         */
        /* calculate the atomic number of the element */
        const Atomic_number Z = nucsyn_element_to_atomic_number(store,
                                                                element,
                                                                1);
        const Nuclear_mass * const imnuc = stardata->store->imnuc;
        const unsigned int nisotopes = store->nisotopes[Z];
        const Isotope ** const icache = (const Isotope ** const)store->icache;
        Nelem = 0.0;

        /* add abundances from isotope numbers stored in icache */
        for(Element c=0;c<nisotopes;c++)
        {
            const Isotope i = icache[Z][c];
            Nelem += imnuc[i] * density * X[i];
        }
    }

    return Nelem;
}




#endif // NUCSYN
