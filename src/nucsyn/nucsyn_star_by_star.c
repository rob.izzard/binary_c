#include "../binary_c.h"
No_empty_translation_unit_warning;

#if defined NUCSYN &&                           \
    NUCSYN_STAR_BY_STAR_MATCHING

/*
 * Code to match abundances from a given star to the model,
 * and report a chi-squared
 */

#define HUGE_CHISQ 1e100

double nucsyn_match_chi_squared(const struct stardata_t * Restrict const stardata,
                                const struct star_t * Restrict const star)
{
    double chisq=0.0;
    if(star->stellar_type<HeMS)
    {

        /*
         * Function to match the loaded star to models
         * and return the chi-squared value
         */
        Isotope i;
        Abundance *X = nucsyn_observed_surface_abundances(star);
        Abundance *o = stardata->preferences->observed_star;
        Abundance *err = stardata->preferences->observed_star_error;

        Forward_isotope_loop(i)
        {
            if(err[i]>0.0)
            {
                double Xbracket=nucsyn_square_bracket(X,
                                                      stardata->preferences->Xsolar,
                                                      i,
                                                      XFe56);

                // add up the statistic here
                // this is the reduced chisq:
                chisq += Pow2((o[i]-Xbracket)/err[i]);

                /*
                  if(star->starnum==0)
                  fprintf(stderr,"Star %d (kw=%d m=%g m0=%g): Compare Xbracket of %d =%g with obs %g +- %g : chisq=%g\n",
                  star->starnum,
                  star->stellar_type,
                  star->mass,
                  stardata->common.zero_age.mass[star->starnum],
                  i,Xbracket,o[i],err[i],chisq);
                */

            }
        }
        //if(star->starnum==0) fprintf(stderr,"Final chisq %g (t=%g)\n",chisq,stardata->model.time);


    }
    else
    {
        chisq=HUGE_CHISQ;
    }
    printf("CHISQ %d %g %g %g %d %g %g\n",
           star->starnum,
           chisq,
           star->mass,
           stardata->common.zero_age.mass[star->starnum],
           star->stellar_type,
           stardata->model.time,
           star->num_thermal_pulses);

    return chisq;

}

void nucsyn_match_set_abundance(const Isotope  i,
                                const Abundance abundance,
                                const Abundance error,
                                struct stardata_t * Restrict const stardata)
{
    /*
     * Function to load stellar abundances:
     * For isotope i set the abundance
     */
    _printf("Set match of isotope %d to %g\n",i,abundance);

    stardata->preferences->observed_star[i]=abundance;
    stardata->preferences->observed_star_error[i]=error;
    return;
}


#endif // NUCSYN && NUCSYN_STAR_BY_STAR_MATCHING
