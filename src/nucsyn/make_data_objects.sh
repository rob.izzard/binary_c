#!/bin/bash

# make data objects for nucsyn with binary_c
echo "make data objects for nucsyn with binary_c"

source ../make_data_objects_setup.sh

$CC -fPIC -O0 ../../double2bin.c -o ./double2bin

# large s-process table

HFILE=nucsyn_extended_s_process.h
TMPFILE=nucsyn_extended_s_process.dat
OBJFILE=nucsyn_extended_s_process.o
echo "HFILE=$HFILE TMPFILE=$TMPFILE OBJFILE=$OBJFILE"

if [ $HFILE -nt $OBJFILE ]  || [ !-z $BINARY_C_REBUILD_DATA_OBJECTS] ; then

    cat $HFILE | tr , ' ' | sed s/\\\\// | grep -v define |grep -v \* |grep . | ./double2bin > $TMPFILE && \
    objcopy $OBJCOPY_OPTS $TMPFILE $OBJFILE && \
    ls -l $TMPFILE $OBJFILE double2bin && \
    rm $TMPFILE &
fi
echo "DATAOBJECT $OBJFILE"

# novae

HFILE=nucsyn_novae_JH98_CO.h
TMPFILE=nucsyn_novae_JH98_CO.dat
OBJFILE=nucsyn_novae_JH98_CO.o
echo "HFILE=$HFILE TMPFILE=$TMPFILE OBJFILE=$OBJFILE"

if [ $HFILE -nt $OBJFILE ]  || [ !-z $BINARY_C_REBUILD_DATA_OBJECTS] ; then

    cat $HFILE | tr , ' ' | sed s/\\\\// | grep -v define |grep -v \* |grep . | ./double2bin > $TMPFILE && \
    objcopy $OBJCOPY_OPTS $TMPFILE $OBJFILE && \
    ls -l $TMPFILE $OBJFILE double2bin && \
    rm $TMPFILE &
fi
echo "DATAOBJECT $OBJFILE"

HFILE=nucsyn_novae_JH98_ONe.h
TMPFILE=nucsyn_novae_JH98_ONe.dat
OBJFILE=nucsyn_novae_JH98_ONe.o
echo "HFILE=$HFILE TMPFILE=$TMPFILE OBJFILE=$OBJFILE"

if [ $HFILE -nt $OBJFILE ]  || [ !-z $BINARY_C_REBUILD_DATA_OBJECTS] ; then

    cat $HFILE | tr , ' ' | sed s/\\\\// | grep -v define |grep -v \* |grep . | ./double2bin > $TMPFILE && \
    objcopy $OBJCOPY_OPTS $TMPFILE $OBJFILE && \
    ls -l $TMPFILE $OBJFILE double2bin && \
    rm $TMPFILE &
fi
echo "DATAOBJECT $OBJFILE"

wait
rm ./double2bin
