#include "../binary_c.h"
No_empty_translation_unit_warning;

#if defined NUCSYN &&                           \
    NUCSYN_PLANETARY_NEBULAE
#define HEADER "PLANNEB__"

/* some definitions of a planetary nebula */

// minimum mass loss rate (msun/yr)
#define MDOT_MIN 1e-6
// minimum central star luminosity (lsun)
#define LCENT_MIN 100
// central star stellar type
// WD or WR star?
#define CENT_TYPE(A) (WHITE_DWARF(A))

#define TOTAL_LUMINOSITY (stardata->star[0].luminosity+stardata->star[1].luminosity)

void nucsyn_planetary_nebulae(const struct stardata_t * Restrict const stardata,
                              const Boolean PNetype,
                              const Star_number starnum,
                              const double dmass,
                              const Abundance * X,
                              const double vexp)
{
    /*
     * Planetary nebula check building
     */
    Isotope i;
    struct star_t *star=&(stardata->star[starnum]);

    printf("PN%d %d %g %g %g ",
           PNetype,
           star->stellar_type,
           dmass,
           star->num_thermal_pulses,
           stardata->model.time);
    if(PNetype==PNE_COMENV)
    {
        /* Common envelope PN */
        Forward_isotope_loop(i)
        {
            printf("%g ",X[i]);
        }
    }
    else
    {
        /* (post-)AGB PN */
        Forward_isotope_loop(i)
        {
            printf("%g ",X[i]);
        }
    }

    printf("\n");

#ifdef OLD_PNE
    /************************************************************/
    int i;
    double teff[NUMBER_OF_STARS];
    Starloop(i)
    {
        teff[i] = stardata->star[0].stellar_type==MASSLESS_REMNANT ? 0.0 : Teff(i);
    }
    printf("PLANNEBX=%g Y=%g C12=%g N14=%g O16=%g\n",X[XH1],X[XHe4],X[XC12],X[XN14],X[XO16]);
    printf("PLANNEB__ %d %d %d %d %g %g %g %g %g %g %g %g %g ",
           PNetype,
           starnum,
           stardata->star[starnum].stellar_type,
           stardata->star[Other_star(starnum)].stellar_type,
           stardata->star[0].mass,
           stardata->star[1].mass,
           teff[1],
           teff[2],
           dmass,
           1e6*stardata->model.time, // convert to years
           vexp,
           stardata->common.orbit.separation,
           stardata->common.orbit.period);

    Forward_isotope_loop(i)
    {
        printf("%g ",X[i]);
    }
    printf("\n");
#endif
}

#endif // NUCSYN && NUCSYN_PLANETARY_NEBULAE
