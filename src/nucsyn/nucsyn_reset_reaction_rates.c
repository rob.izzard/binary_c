#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef NUCSYN

void nucsyn_reset_reaction_rates(struct preferences_t * Restrict const preferences)
{
    for(Reaction_rate_index i=0;i<SIGMAV_SIZE;i++)
    {
        /* By default set all reaction rate multiplies to 1.0 i.e. no change */
        preferences->reaction_rate_multipliers[i] = 1.0;
    }
}

#endif // NUCSYN
