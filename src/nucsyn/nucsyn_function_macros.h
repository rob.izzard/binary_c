#pragma once
#ifndef NUCSYN_FUNCTION_MACROS_H
#define NUCSYN_FUNCTION_MACROS_H
#ifdef NUCSYN
/*
 * The binary_c stellar population nucleosynthesis framework.
 *
 * Contact:  rob.izzard@gmail.com
 *
 * https://binary_c.gitlab.io/
 * https://gitlab.com/binary_c/binary_c
 * https://groups.google.com/forum/#!forum/binary_c-nucsyn-announce
 * https://groups.google.com/forum/#!forum/binary_c-nucsyn-devel
 * https://twitter.com/binary_c_code
 * https://www.facebook.com/groups/149489915089142/
 *
 * Please see the files README, LICENCE and CHANGES
 *
 *
 * This file contains function macros used for nucleosynthesis.
 */

/*
 * Observed surface abundance
 */
#define Observed_surface(S)                                 \
    ((S)->dmacc>TINY ? &((S)->Xacc[0]) : &((S)->Xenv[0]))

/*
 * Observed surface abundance of isotope I
 * in star S
 */
#define Observed_abundance(S,I)                 \
    (*(Observed_surface(S)+(int)I))


/* define conditions for burning of the dredge-up material */
#define Third_dredge_up_burning_conditions(TEMP,DENS)           \
    ((TEMP >= T_MIN_HBB) &&                                     \
     (DENS >= RHO_MIN_HBB) &&                                   \
     (m0 > nucsyn_mhbbmin(stardata->common.metallicity) ||      \
      stardata->common.metallicity <= 0.004))
/* or NEVER dup burn */
//#define Third_dredge_up_burning_conditions(A,B) (1==0)



/*
 * Define an expression that is true if hbb should occur, otherwise it's false
 */
#define HBB_Conditions(T,RHO,MENV,MZERO)        \
    (                                           \
        (T) >= (T_MIN_HBB) &&                   \
        (RHO) >= (RHO_MIN_HBB) &&               \
        (MENV) > 0.2*(MZERO)                    \
        )

/* Logging command */

#ifndef NUCSYN_THIRD_DREDGE_UP
#define __LAMBDA 0.0
#else
#define __LAMBDA (stardata->star[0].lambda_3dup)
#endif

#define Log_nucsyn(C,A,B,D) \
    nucsyn_log(                                 \
        (A),                                    \
        newstar->mass,                          \
        newstar->core_mass[CORE_He],            \
        newstar->radius,                        \
        newstar->core_radius,                   \
        newstar->luminosity,                    \
        newstar,                                \
        stardata,                               \
        (__LAMBDA),                             \
        (B),                                    \
        D);


/* macro for timescale against proton capture */
#define Nuclear_burning_timescale(A) (invNH1/sigmav[(A)])
#define Inverse_nuclear_burning_timescale(A) (sigmav[(A)]/invNH1)


/*
 * Macros to access the min and max isotope numbers
 * used in a given network, N
 */
#define Nuclear_network_isotope_range_set(N,MIN,MAX)    \
    store->nuclear_network_range[(N)].min = (MIN);      \
    store->nuclear_network_range[(N)].max = (MAX);      \

/*
 * Macro to allocate a Jacobian matrix for network I
 * which has NUM species.
 */
#define Allocate_network_jacobian(I,NUM)                    \
    Allocate_network_jacobian_implementation(AllocJ,        \
                                             __COUNTER__,   \
                                             (I),           \
                                             (NUM));        \

#define Allocate_network_jacobian_implementation(LABEL,LINE,I,NUM)  \
    stardata->tmpstore->network_jacobians_n[I] = (NUM);             \
    stardata->tmpstore->network_jacobians[I] =                      \
        Malloc(((NUM)+1) * sizeof(double*));                        \
    for(unsigned int Concat3(i,LABEL,LINE)=1;                       \
        Concat3(i,LABEL,LINE)<((NUM)+1);                            \
        Concat3(i,LABEL,LINE)++)                                    \
    {                                                               \
        stardata->tmpstore->network_jacobians[I][Concat3(i,LABEL,LINE)] = \
            Malloc(((NUM)+1) * sizeof(double));                     \
    }


#endif // NUCSYN
#endif // NUCSYN_FUNCTION_MACROS_H
