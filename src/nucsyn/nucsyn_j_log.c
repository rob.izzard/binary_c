#include "../binary_c.h"
No_empty_translation_unit_warning;

/* logs J stars */

#if defined NUCSYN &&                                \
    defined NUCSYN_J_LOG
void nucsyn_j_log(struct stardata_t * Restrict const stardata)
{
    double corat[NUMBER_OF_STARS]; /* C/O */
    double c12c13rat[NUMBER_OF_STARS]; /* C12/C13 */
    struct star_t *star;
    Abundance *X; // the abundances
    int out=0;
    Star_number k;
    /* J-star stuff */
    /* Conditions for J-star : C/O>1 and C12/C13<15 */
    /* Actually, we want the J/C ratio so output for C-stars too */

    Starloop(k)
    {
        SETstar(k);
        X=nucsyn_observed_surface_abundances(star);
        corat[k]=(X[XC12]+X[XC13])/
            (X[XO16]+X[XO17]);
        c12c13rat[k]= (X[XC12]/X[XC13]);
        /* output for all C stars */
        /* ignore He stars! */
        if((corat[k]>1)&&(star->stellar_type<=6)) out=1;
    }

    if(out==1)
    {
        printf("NUCSYNJ__ %g ",stardata->model.time);
        Starloop(k)
        {
            SETstar(k);


            printf("%g %g %d %g %g %g ",
                   corat[k], /* C/O ratio */
                   c12c13rat[k], /* C12/C13 ratio */
                   star->stellar_type, /* stellar type */
                   Teff(k), /* effective temperature */
                   star->luminosity, /* luminosity */
                   star->mass
                );

            if(corat[k]>1.0)
            {
                /* type : C or J */
                if(c12c13rat[k]<=15.0)
                {
                    printf("J ");
                }
                else
                {
                    printf("C ");
                }
            }
            else
            {
                printf("X ");
            }
        }
        printf("\n");
    }
}
#endif // NUCSYN && NUCSYN_J_LOG
