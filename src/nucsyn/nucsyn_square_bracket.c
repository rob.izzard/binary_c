#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef NUCSYN

Abundance_ratio Pure_function nucsyn_square_bracket(const Abundance * Restrict const X,
                                                    const Abundance * Restrict const Xsolar,
                                                    const Isotope top_isotope,
                                                    const Isotope bottom_isotope)
{
    /*
     *
     * Square bracket notation:
     * [A/B] = log(N_A/N_B) - log(N_A_solar/N_B_solar)
     *       = log(X_A/X_B) - log(X_A_solar/X_B_solar)
     * where log=log10
     *
     * Note that this is for isotopes only:
     * if you want elements use nucsyn_elemental_square_bracket()
     */

    double sqb
        =
        log10((X[top_isotope]+REALLY_TINY)/(Xsolar[top_isotope]+REALLY_TINY))-
        log10((REALLY_TINY+X[bottom_isotope])/(Xsolar[bottom_isotope]+REALLY_TINY));
    /*
     * Limit sqb: in the case where abundances are really close to solar,
     * the REALLY_TINY terms will give you a result around 10^-6, so just truncate
     * at 1e-5, which is 4 orders of magnitude better than stellar observations
     * (except with grains... perhaps...:)
     */

    if(Abs_less_than(sqb,1e-5)) sqb=0.0;
    return(sqb);
}



#endif
