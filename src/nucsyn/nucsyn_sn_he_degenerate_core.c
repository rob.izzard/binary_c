#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef NUCSYN

void nucsyn_sn_he_degenerate_core(Abundance * const X, //space calloc'd previously
                                  struct stardata_t * const stardata,
                                  const struct star_t * const pre_explosion_star)
{
    /*
     * Explosion of a helium giant with a degenerate CO core,
     * so it looks like a Ia, but with a helium envelope.
     */
    const double mcore = pre_explosion_star->core_mass[CORE_CO];
    const double menv = pre_explosion_star->mass - mcore;

    /*
     * This is like a SNIa with an envelope
     * of helium. First compute the core ejecta
     * which are like the SNIa.
     */
    if(stardata->preferences->triggered_SNIa_algorithm == TYPE_IA_SUPERNOVA_ALGORITHM_DD2)
    {
        nucsyn_sn_iwamoto_1999_DD2(X);
    }
    else if(stardata->preferences->triggered_SNIa_algorithm == TYPE_IA_SUPERNOVA_ALGORITHM_SEITENZAHL2013)
    {
        nucsyn_sn_Seitenzahl2013(stardata,X);
    }
    else if(stardata->preferences->triggered_SNIa_algorithm == TYPE_IA_SUPERNOVA_ALGORITHM_SEITENZAHL2013_AUTOMATIC)
    {
        nucsyn_sn_Seitenzahl2013_automatic(stardata,X);
    }
    else if(stardata->preferences->triggered_SNIa_algorithm == TYPE_IA_SUPERNOVA_ALGORITHM_LIVNE_ARNETT_1995)
    {
        nucsyn_sn_livne_arnett_1995(X,
                                    mcore,
                                    menv);
    }

    /*
     * But remmeber to add the envelope abundances
     * (mostly helium).
     */
    nucsyn_dilute_shell(mcore,X,
                        menv,pre_explosion_star->Xenv);
}

#endif // NUCSYN
