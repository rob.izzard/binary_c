#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef NUCSYN

/*****************************************************/
/* Function to set the post-2nd dredge up abundances */
/*****************************************************/

void nucsyn_set_post_2nd_dup_abunds(Abundance * Restrict const X,
                                    const double m,
                                    const Abundance z,
                                    const double mcbagb,
                                    struct star_t * const star,
                                    struct stardata_t * Restrict const stardata)
{
#ifdef NUCSYN_SECOND_DREDGE_UP


/*
 * Second dredge-up surface changes - fitted to Amanda Karakas' Models
 */
    double mc1tp,a,b;

    star->second_dredge_up=TRUE;

    Dprint("2DUP  was X=%g Y=%g C12=%g C13=%g N14=%g O16=%g\nNe20=%g Ne21=%g Ne22=%g Na23=%g\nMg24=%g Mg25=%g Mg26=%g Al27=%g\n",
           X[XH1],X[XHe4],X[XC12],X[XC13],X[XN14],X[XO16],
           X[XNe20],X[XNe21],X[XNe22],X[XNa23],
           X[XMg24],X[XMg25],X[XMg26],X[XAl27]);


    /* get core mass at 1st thermal pulse */
    mc1tp = Is_zero(star->mc_1tp) ?
        guess_mc1tp(star->phase_start_mass,
                    star->phase_start_mass,
                    star,
                    stardata) :
        star->mc_1tp;

    // Use mcbagb, the helium core mass, which does not change on the EAGB
    // (the CO core mass does, but is not relevant!)
    a = (m-mcbagb)/(m-mc1tp); // envelope matter fraction
    b=1.0-a; // core-burnt matter fraction

    Dprint("2DUPAB M=%g Z=%g a=%g b=%g mc1tp=%g\n",m,z,a,b,mc1tp);

    // This should not happen, but if there is no envelope or core
    // then it might and this function should not be called

    if(a<TINY || b<TINY) return;

    // a * envelope abundance +
    // b * CNO cycled material (which was the core)
    Abundance * Xburnt = New_isotope_array_from(X);
    double xcno=(X[XC12]+
                 X[XC13]+
                 X[XN14]+
                 X[XN15]+
                 X[XO16]+
                 X[XO17]+
                 X[XO18]+
                 X[XF17]);

    // convert all H -> He
    Xburnt[XHe4]+=Xburnt[XH1];
    Xburnt[XH1]=0.0;

    // all CNO -> N14 (should do this by number...)
    Xburnt[XC12]=0.0;
    Xburnt[XC13]=0.0;
    Xburnt[XN14]=xcno;
    Xburnt[XN15]=0.0;
    Xburnt[XO16]=0.0;
    Xburnt[XO17]=0.0;
    Xburnt[XO18]=0.0;
    Xburnt[XF17]=0.0;

    /*
     * Heavier isotopes:
     * Use an Al26 amount scaled to metallicity (i.e. Mg abundance?)
     */
    Xburnt[XAl26]=3.5e-5*z;
    nucsyn_dilute_shell(a,X,b,Xburnt);

    // fits to amanda's results for other elements when important
    X[XH2]=1e-17; // very little D (experimental)

    Dprint("2DUP   is X=%g Y=%g C12=%g C13=%g N14=%g O16=%g tot=%g\n", X[XH1],
           X[XHe4],X[XC12],X[XC13],X[XN14],X[XO16],
           nucsyn_totalX(X));

    /* fudge to make Xtot 1: put the remainder in hydrogen */
    X[XH1]+=(1.0-nucsyn_totalX(X));

    Dprint("2DUP is X=%g Y=%g C12=%g C13=%g N14=%g O16=%g\nNe20=%g Ne21=%g Ne22=%g Na23=%g\nMg24=%g Mg25=%g Mg26=%g Al26=%g Al27=%g\n",
           X[XH1],X[XHe4],X[XC12],X[XC13],X[XN14],X[XO16],
           X[XNe20],X[XNe21],X[XNe22],X[XNa23],
           X[XMg24],X[XMg25],X[XMg26],X[XAl26],X[XAl27]);

    Safe_free(Xburnt);
#endif // NUCSYN_SECOND_DREDGE_UP

}


#endif // NUCSYN
