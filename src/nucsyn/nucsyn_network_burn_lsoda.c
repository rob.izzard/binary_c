#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef NUCSYN
double nucsyn_network_burn_lsoda(double * Restrict const Nin,
                                 const double * Restrict const sigmav,
                                 const double maxt,
                                 nucsyn_burnfunc burnfunc,
                                 nucsyn_logfunc logfunc Maybe_unused,
                                 const double dtguess Maybe_unused,
                                 struct stardata_t * const Restrict stardata,
                                 const Reaction_network network_id Maybe_unused,
                                 const Boolean vb Maybe_unused
    )
{

    /*
     * This step is not required with lsoda: time evolution
     * is handled entirely in the lsoda routine. Eventually
     * deprecate this function, but we still need it
     * to wrap the functional form of the burnfunc()
     */
    return burnfunc == NULL ? 0.0 :
        burnfunc(stardata,
                 Nin,
                 sigmav,
                 maxt);
}

#endif//NUCSYN
