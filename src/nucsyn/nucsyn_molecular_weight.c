#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef NUCSYN
double nucsyn_molecular_weight(const Abundance * Restrict const X,
                               const struct stardata_t * Restrict const stardata,
                               const double ionisation_fraction)
{
    /*
     * Function to obtain the (mean) molecular weight of an abundance array (X)
     * - requires stardata for atomic numbers and weights.
     *
     * The definition of the molecular weight, mu, is,
     *
     * mu = rho / (n * m_proton)
     *
     * where rho is the density, n the particle density, then given,
     *
     * n = n_e + n_I,
     *
     * where n_e is the number density of electrons and n_I
     * the number density of ions, we have,
     *
     * 1/mu = 1/mu_e + 1/mu_I ,
     *
     * where,
     *
     * mu_e = sum of X_i * Z_i / A_i,
     *
     * and
     *
     * mu_I = sum of X_i / A_i.
     *
     * Here we use
     *
     * 1/mu = f_ion / mu_e + 1 / mu_I
     *
     * to allow an ionisation fraction less than 1.0.
     *
     * We neglect the contribution of electrons to mu_I if the ionisation
     * fraction is less than 1.0.
     */
    double mu = 0.0;

    if(Fequal(ionisation_fraction,1.0))
    {
        /*
         * Fully ionised material
         */
        mu = dot_product(X,
                         stardata->store->ionised_molecular_weight_multiplier,
                         ISOTOPE_ARRAY_SIZE);
    }
    else if(Is_zero(ionisation_fraction))
    {
        /*
         * Atomic ("recombined") material. Usually this applies
         * if temperatures are sufficiently cool (<1000K or so).
         */
        const Nuclear_mass * const imnuc_amu = stardata->store->imnuc_amu;
        double mu_sum[NUCSYN_STRIDE_N] = {0.0};
        /* "unionised ion" == atom term = X / A */
        Nucsyn_isotope_stride_leftarray_loop(mu_sum,+= X[i] * imnuc_amu[i]);
        mu = Nucsyn_stride_sum(mu_sum);
    }
    else
    {
        /*
         * Generic ionisation fraction
         */
        const Atomic_number * const atomic_number = stardata->store->atomic_number;
        const Nuclear_mass * const imnuc_amu = stardata->store->imnuc_amu;
        double mu_sum[NUCSYN_STRIDE_N] = {0.0};
        Nucsyn_isotope_stride_leftarray_loop(mu_sum,+= X[i] * imnuc_amu[i] * (1.0 + ionisation_fraction * atomic_number[i]));
        mu = Nucsyn_stride_sum(mu_sum);
    }

    /* NB 1.0 here means the mass fraction sum, which should be 1.0 */
    return 1.0/mu;
}

#endif // NUCSYN
