#pragma once
#ifndef NUCSYN_MACROS_H
#define NUCSYN_MACROS_H

#include "../binary_c_parameters.h"

#ifdef NUCSYN
/*
 * The binary_c stellar population nucleosynthesis framework.
 *
 * Contact:  rob.izzard@gmail.com
 *
 * https://binary_c.gitlab.io/
 * https://gitlab.com/binary_c/binary_c
 * https://groups.google.com/forum/#!forum/binary_c-nucsyn-announce
 * https://groups.google.com/forum/#!forum/binary_c-nucsyn-devel
 * https://twitter.com/binary_c_code
 * https://www.facebook.com/groups/149489915089142/
 *
 * Please see the files README, LICENCE and CHANGES
 *
 *
 * This file contains macros for nucleosynthesis in binary_c,
 * while function macros are in nucsyn_function_macros.h
 */

#include "nucsyn_function_macros.h"
#include "nucsyn_init_abunds.h"
#include "nucsyn_Ia_algorithms.h"
#include "nucsyn_PN_triggers.h"
#include "nucsyn_WR_types.h"
#include "nucsyn_solvers.h"
#include "nucsyn_nova_algorithms.h"

// these define the mass range for the core mass corrections "required" to
// reproduce the carbon star luminosity functions of the LMC and SMC
#define MIN_MASS_FOR_FUDGES 1.5
#define MAX_MASS_FOR_FUDGES 2.5

/*
 * minimum envelope mass for HBB to occur - hard to test with amanda's code,
 * and really shouldn't be all that necessary anyway since the temperature
 * will drop off before this
 */
#define MIN_MENV_FOR_HBB 0.1
/*
 * (log10 of the) Temperature and Density above which Hot Bottom Burning occurs
 */

// this is for all HBB (defaults 7.0 and 1e-5 respectively)
#define T_MIN_HBB 7.0
#define RHO_MIN_HBB 1e-5
// this is for the NeNa cycle (7.0)
#define T_MIN_NeNa_HBB 6.5
// this is for the MgAl cycle (7.0)
#define T_MIN_MgAl_HBB 7.0



/* a very small mass */
#define DM_TINY 1e-15

// use Hurley et al 2002 fit
//#define CALCRAD star->radius=ragbf(*mt,*lum,stardata->common.metallicity_parameters[ZPAR_MASS_HE_FLASH],stardata->common.giant_branch_parameters,stardata->common.metallicity,0.0);

/* reaction rate definitions */
#include "nucsyn_sigmav.h"

/* WR stuff */
// WR spectral types

// Planetary nebulae
#define PNE_AGB 0
#define PNE_COMENV 1

/* macros for the temperature, density, convection tables */
#define TEMP_DENS_CONV_N_PARAMETERS 4
#define TEMP_DENS_CONV_N_DATA 3
#define TEMP_DENS_CONV_TABLE_WIDTH (TEMP_DENS_CONV_N_PARAMETERS+TEMP_DENS_CONV_N_DATA)

/*
 * Ionisation fractions
 */
#define GAS_ATOMIC (0.0)
#define GAS_FULLY_IONISED_PLASMA (1.0)



#endif /* NUCSYN */


/***************************************************************
 * Macros required for command line arguments, so should never
 * be enclosed in NUCSYN checks
 **************************************************************/

/*
 * Type Ia Mch algorithms
 */


/*
 * George Angelou's lithium project
 */
#define ANGELOU_LITHIUM_DECAY_FUNCTION_EXPONENTIAL 0

/*
 * Maximum number of elements to associate with an
 * element in the icache.
 *
 * Tin (Sn) has 10, so this is slight overkill.
 */
#define MAX_ISOTOPES_PER_ELEMENT 12

#endif /* NUCSYN_MACROS */
