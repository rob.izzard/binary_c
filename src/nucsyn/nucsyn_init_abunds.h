#pragma once
#ifndef NUCSYN_INIT_ABUNDS_CODES_H
#define NUCSYN_INIT_ABUNDS_CODES_H

#include "nucsyn_init_abunds.def"

/*
 * Construct nucsyn init abunds codes
 */
#undef X
#define X(CODE,TEXT) CODE,
enum { NUCSYN_INIT_ABUNDS_CODES };

#undef X
#define X(CODE,TEXT) #CODE,
static char * nucsyn_init_abunds_code_macros[] Maybe_unused = { NUCSYN_INIT_ABUNDS_CODES };

#undef X
#define X(CODE,TEXT) TEXT,
static char * nucsyn_init_abunds_code_strings[] Maybe_unused = { NUCSYN_INIT_ABUNDS_CODES };

#define Nucsyn_init_abunds_string(N) Array_string(nucsyn_init_abunds_code_strings,(N))

#undef X


#endif // NUCSYN_INIT_ABUNDS_CODES_H
