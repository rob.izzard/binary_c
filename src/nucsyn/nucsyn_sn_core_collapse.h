#ifndef NUCSYN_SN_CORE_COLLAPSE_H
#define NUCSYN_SN_CORE_COLLAPSE_H

#include "nucsyn_sn_core_collapse.def"
#undef X
#define X(CCTYPE,STRING) NUCSYN_CCSN_##CCTYPE,
enum { CORE_COLLAPSE_SUPERNOVA_ALGORITHMS };
#undef X

#endif // NUCSYN_SN_CORE_COLLAPSE_H
