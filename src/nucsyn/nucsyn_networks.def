#pragma once
#ifndef NUCSYN_NETWORKS_DEF
#define NUCSYN_NETWORKS_DEF

/*
 * List of nuclear networks used in binary_c
 *
 * Columns are:
 *
 * 1: Code name
 * 2: Boolean: whether to burn by default (set in
 *    preferences->nucsyn_network[])
 * 3: String for logging
 * 4: function to call to do the burning with the Kaps-Rentrop solver
 * 5: function to call to do the burning with the LSODA solver
 * 6: function to call to do the burning with the CVODE solver
 * 7: function to test T,rho and abundances to determine whether
 *    we should attempt a burn (or NULL)
 */

#ifdef __HAVE_LIBSUNDIALS_CVODE__
#define _CVODE(X) (X)
#else
#define _CVODE(X) NULL
#endif // __HAVE_LIBSUNDIALS_CVODE__

#define NUCSYN_NETWORKS_LIST                                                                                                                                                                      \
    X(             PP,  TRUE,               "pp",       &nucsyn_burn_kaps_rentrop_pp,      &nucsyn_burn_lsoda_pp, _CVODE(       &nucsyn_burn_cvode_pp) ,       &nucsyn_hydrogen_pp_checkfunction) \
    X(         PPfast, FALSE,          "pp fast",                &nucsyn_burn_ppfast,                       NULL, _CVODE(                        NULL) ,       &nucsyn_hydrogen_pp_checkfunction) \
    X(        COLDCNO,  TRUE,         "Cold CNO",  &nucsyn_burn_kaps_rentrop_coldCNO, &nucsyn_burn_lsoda_coldCNO, _CVODE(  &nucsyn_burn_cvode_coldCNO) ,      &nucsyn_hydrogen_CNO_checkfunction) \
    X(         HOTCNO, FALSE,          "Hot CNO",                &nucsyn_burn_hotCNO,                       NULL, _CVODE(                        NULL) ,      &nucsyn_hydrogen_CNO_checkfunction) \
    X(           NeNa, FALSE,             "NeNa",                               NULL,                       NULL, _CVODE(                        NULL) , &nucsyn_hydrogen_NeNaMgAl_checkfunction) \
    X( NeNaMgAlnoleak, FALSE, "NeNaMgAl no leak",                               NULL,                       NULL, _CVODE(                        NULL) , &nucsyn_hydrogen_NeNaMgAl_checkfunction) \
    X(       NeNaMgAl,  TRUE,         "NeNaMgAl", &nucsyn_burn_kaps_rentrop_NeNaMgAl, &nucsyn_burn_lsoda_coldCNO, _CVODE( &nucsyn_burn_cvode_NeNaMgAl) , &nucsyn_hydrogen_NeNaMgAl_checkfunction) \
    X(         helium,  TRUE,           "helium",                &nucsyn_burn_helium,                       NULL, _CVODE(                        NULL) ,            &nucsyn_helium_checkfunction) \
                                                                                                                                                                                                  \
                                                                                                                              /* this last line should not be changed */                          \
    X(         NUMBER, FALSE,                 "",                               NULL,                       NULL,   NULL,                        NULL)



#endif // NUCSYN_NETWORKS_DEF
