#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef NUCSYN

/*
 * Massive-star abundances from tables based on
 * Lynnette Dray and Richard Stancliffe's models
 */

#define R_He 0
#define R_C 1
#define R_N 2
#define R_O 3
#define R_Ne 4
#define R_Mg 5
#define R_COLS 6

/* debugging */
//#define WRDEBUG2

#if defined NUCSYN_WR_TABLES && defined WRDEBUG2
static double CNOsum(Abundance *X);
#endif // NUCSYN_WR_TABLES && WRDEBUG2

void nucsyn_WR_table(struct stardata_t * Restrict const stardata,
                     struct star_t * const star)
{
#ifdef NUCSYN_WR
    Dprint("WR table star %d Xn %g\n",
           star->starnum,
           star->Xenv[Xn]);

#ifdef WRDEBUG2
    fprintf(stderr,"Table lookup: Z=%g MZAMS=%g fM=%g (mass=%g)\n",
            stardata->common.metallicity,
            star->effective_zams_mass,
            Min(1.0,star->mass/star->effective_zams_mass),
            star->mass);

#endif // WRDEBUG2

#ifdef NUCSYN_WR_ACCRETION
    Abundance *const r_PMS = star->XWR0; /* copy pointer to local variable */

    /*
     * first element is helium :if <0.01 then clearly we haven't
     *  loaded in the t=0 abundances
     */
    if(star->XWR0[0] < 0.01)
    {
        Dprint("GET PMS WR0 from %g\n",star->XWR0[0]);

        /* get PMS abundances : cache these! */
        const double x_PMS[3] = {
            stardata->common.metallicity,
            star->effective_zams_mass,
            1.0
        };
        nucsyn_WR_table_lookup(stardata,
                               x_PMS,
                               r_PMS,
                               stardata->preferences->wr_wind);
        Dprint("GET PMS WR0 now %g\n",star->XWR0[0]);
    }
#endif //NUCSYN_WR_ACCRETION


    /* work on Xenv */
    Abundance * const X = star->Xenv;
    //X[Xn] = 0.0;

    /* save initial abundances */
    //double he4=X[XHe4];
    double c12 = X[XC12];
    double c13 = X[XC13];
    double n14;
    double o16 = X[XO16];
    double ne20 = X[XNe20];
    double ne22 = X[XNe22];

    /* save NeNa and MgAl to accommodate accretion */
#ifdef NUCSYN_WR_RS_TABLE
    double star_NeNa=X[XNe20]/20.0+X[XNe21]/21.0+X[XNe22]/22.0+X[XNa23]/23.0;
    double star_MgAl=X[XMg24]/24.0+X[XMg25]/25.0+X[XMg26]/26.0+X[XAl26]/26.0+X[XAl27]/27.0;
#endif // NUCSYN_WR_RS_TABLE
    Dprint("nucsyn_WR_table: m=%g in XC12=%g XN14=%g\n",star->mass,X[XC12],X[XN14]);

#ifdef WRDEBUG2
    if(star->starnum==0) fprintf(stderr,"CNO in %g N=%g\n",CNOsum(X),X[XN14]);
#endif // WRDEBUG2

    double x[3] = {
        stardata->common.metallicity,
        star->effective_zams_mass,
        star->mass / star->effective_zams_mass
    };
    Abundance r[R_COLS];

    /*
     * Note that x[2] = f = M/Minit is only
     * tabulated to 2 decimal places, so
     * calculate to 2 decimal places to allow table
     * cacheing
     */
    x[2]=( (int) (100.0*x[2]) ) / 100.0;

    /*
     * Mass must be > NUCSYN_WR_MASS_BREAK
     * in order to use Lynnette's table of data.
     */
    if(star->effective_zams_mass > NUCSYN_WR_MASS_BREAK)
    {
        /*
         * Look up data from Lynnette's tables
         */
        nucsyn_WR_table_lookup(
            stardata,
            x,
            r,
            stardata->preferences->wr_wind);

        /*
         * Take helium directly from Lynnette's results, but don't let it
         * drop due to a different initial abundance. Apply the correction
         * if he4<0.5 only so that evolved values follow the table.
         */
        Abundance he4 = r[R_He];
        Dprint("Lynnette helium %g\n",he4);

        if(he4<0.5) he4 *= stardata->preferences->zero_age.XZAMS[XHe4]/star->XWR0[R_He];
        Dprint("Lynnette helium corrected %g\n",he4);

        /*
         * Do not allow He to be > 1-Z
         */
        const double Z = nucsyn_totalX(X)-X[XH1]-X[XHe4]-X[XHe3]-X[XH2];
        he4 = Min(he4,1.0-Z);

#ifdef WRDEBUG2
        fprintf(stderr,"Table He =%g * factor %g = %g\n",r[R_He],
                stardata->preferences->zero_age.XZAMS[XHe4]/star->XWR0[R_He],
                he4);
#endif // WRDEBUG2

        /*
         * Assume Lynnette's values for CNO, then scale to the
         * present CNO abundance just in case of accretion
         */
        const double lynnette_CNO = Max(1e-30,r[R_C]/12.0+r[R_N]/14.0+r[R_O]/16.0); // Lynnette's CNO(time)

        /*
         * Is this star a helium star according to
         * our stellar evolution algorithm or
         * to Lynnette's table?
         */
        const Boolean Hestar = star->stellar_type>TPAGB || (r[R_He]<0.9 && r[R_C]+r[R_O]<0.1);
        Dprint("Lynnette helium star? %d (stellar type %d)\n",Hestar,star->stellar_type);

        /*
         * If there is CNO, and Lynnette's star is NOT a helium star
         */
        if(Hestar==FALSE && lynnette_CNO)
        {
#ifdef NUCSYN_WR_ACCRETION
            const double nCNO_init=
                star->Xinit[XC12]/12.0+
                star->Xinit[XN14]/14.0+
                star->Xinit[XO16]/16.0;
            const double nCNO_table = r[R_C]/12.0+r[R_N]/14.0+r[R_O]/16.0;
            const double xx = nCNO_init / nCNO_table;

            n14 = Max(1.0,xx) * r[R_N];

#ifdef WRDEBUG2
            fprintf(stderr,"Guess new N=%g (table=%g fCNO=%g)\n",
                    n14,r[R_N],xx);
#endif // WRDEBUG2

            /*
             * Only change abundances if n14 increases
             * (i.e. if there is burning)
             * This fails if N14->Ne22 is active...
             */
            if(More_or_equal(n14, X[XN14]))
            {
                c12 = xx * r[R_C];
                o16 = xx * r[R_O];
#ifdef WRDEBUG2
                fprintf(stderr,"Scaled C=%g O=%g (table C=%g O=%g)\n",c12,o16,r[R_C],r[R_O]);
#endif // WRDEBUG2
            }
            else
            {
                n14 = X[XN14];
            }
#endif // NUCSYN_WR_ACCRETION

#ifdef NANCHECKS
            if(isnan(c12)!=0)
            {
                Exit_binary_c(BINARY_C_OUT_OF_RANGE,
                              "Abundance is nan (C=%g N=%g O=%g) in nucsyn_WR_table accretion\n",c12,n14,o16);
            }
#endif //NANCHECKS
            /*
             * close to the helium core we should allow the total
             * CNO to increase: this is based on the tabular abundances
             */
            const double f = nCNO_table/(r_PMS[R_C]/12.0+r_PMS[R_N]/14.0+r_PMS[R_O]/16.0);
            if(f>1.05)
            {
                Dprint("NewWR star=%d near He core CNO boost f=%g\n",star->starnum,f);
                c12=Max(c12,r[R_C]);
                n14=Max(n14,r[R_N]);
                o16=Max(o16,r[R_O]);
            }

#if (DEBUG==1)
            const Star_number k2=Other_star(star->starnum); // other star number
            Dprint("NewWR t=%g star=%d type=%d (other: %d C=%g) m=%g MZAMS=%g (frac=%g) : C,N,O=%g,%g,%g; Xinit %g,%g,%g; table %g,%g,%g (sum=%g frac of orig %g)\n",
                   stardata->model.time,
                   star->starnum,
                   star->stellar_type,
                   stardata->star[k2].stellar_type,
                   stardata->star[k2].Xenv[XC12],
                   star->mass,
                   star->effective_zams_mass,
                   star->mass/star->effective_zams_mass,
                   c12,n14,o16,
                   star->Xinit[XC12],
                   star->Xinit[XN14],
                   star->Xinit[XO16],
                   r[R_C],r[R_N],r[R_O],
                   r[R_C]+r[R_N]+r[R_O],
                   nCNO_table/(r_PMS[R_C]/12.0+r_PMS[R_N]/14.0+r_PMS[R_O]/16.0)
                );
#endif // DEBUG==1

#ifdef NANCHECKS
            if((isnan(c12)!=0)||(isnan(n14)!=0)||(isnan(o16)!=0))
            {
                Exit_binary_c(BINARY_C_OUT_OF_RANGE,
                              "Abundance is nan (C=%g N=%g O=%g) in nucsyn_WR_table accretion (2)\n",c12,n14,o16);
            }
#endif // NANCHECKS
        }
        else
        {
            /*
             * helium star: assume lynnette's abundances
             * because c12 and o16 can go UP, N14 can DROP
             */
            Dprint("Helium star when Lynnette's star is a helium star\n");

            c12 = r[R_C];
            n14 = r[R_N];
            o16 = r[R_O];

#ifndef NUCSYN_WR_RS_TABLE
            c13 = 0.01 * 0.25 * r[2]; // assume in eq. with N14 (???)
#endif   //NUCSYN_WR_RS_TABLE
            Dprint("From her models: N14=%g\n",r[2]);
        }

        Dprint("LRS%d n14 %g r2=%g kw=%d\n",star->starnum,n14,r[2],star->stellar_type);


        /* final sanity checks for CNO */
        Clamp(c12,0.0,1.0);
#ifdef NUCSYN_WR_RS_TABLE
        Clamp(c13,0.0,1.0);
#endif//DEBUG
        Clamp(n14,0.0,1.0);
        Clamp(o16,0.0,1.0);

        /*** other elements from Lynnette's tables ***/

        /*
         * Neon is mainly Ne20, which doesn't change...
         *
         * But the remainder is ne22, which might be accreted too
         * so make sure we're the max of the two
         */
        ne22=Max(r[R_Ne]-ne20,ne22);

        /*
         * dN > 0 : star has excess N14 c.f. tables -> it has to be burned
         */
        const double dN = X[XN14] - n14;

        /*
         * N14 which disappears has been burnt to Ne22
         */
        if(dN>0.0) ne22 += (22.0/14.0)*dN;

        /*
         * In extreme helium stars, the neon goes *up*, which
         * is from O16(alpha,gamma)Ne20
         */
        if(r[R_Ne] > ne20 + ne22) ne20 = r[R_Ne] - ne22;

        /* finally set the abundances */
        X[XHe4] = Max(he4,X[XHe4]);
        X[XC12] = c12;
        X[XC13] = c13;
        X[XN14] = n14;
        X[XO16] = o16;
        X[XNe20] = ne20;
        X[XNe22] = ne22;

#ifdef NUCSYN_WR_RS_TABLE
        /*
         * Use Richard Stancliffe's surface abundance data for
         * minor isotopes
         */
        Abundance rr[50];

        /* do the interpolation */
        nucsyn_WR_RS_table_lookup(stardata,
                                  x,
                                  rr);

        /*
         * But scale to initial abundances...
         */

        X[XH2]=rr[0];
        X[XHe3]=rr[1];
        X[XLi7]=rr[2];
        X[XBe7]=rr[3];
        X[XB11]=rr[4];

        /* we should merge the following into the CNO treatment above */
        // X[XC13]=rr[5]; X[XN15]=rr[7]; X[XO17]=rr[8]; X[XO18]=rr[9];

        X[XF19]=rr[10];
        X[XNe21]=rr[11];
        X[XNe22]=rr[12];
        X[XNa22]=rr[13];
        X[XNa23]=rr[14];
        X[XMg24]=rr[15];
        X[XMg25]=rr[16];
        X[XMg26]=rr[17];
        X[XAl26]=rr[18];
        X[XMg26]+=rr[19]; // Al26g
        X[XAl27]=rr[20];

        /* neglect changes in Si which just cause numerical issues */
        //X[XSi28]=rr[21]; X[XSi29]=rr[22]; X[XSi30]=rr[23];

        /* ignore heavier isotopes (which do nothing) */
        //X[XP31]=rr[24]; X[XS32]=rr[25]; X[XS33]=rr[26]; X[XS34]=rr[27];
        //X[XFe56]=rr[28]; X[XFe57]=rr[29]; X[XFe58]=rr[30]; X[XFe59]=rr[31]; X[XFe60]=rr[32]; X[XCo59]=rr[33]; X[XNi58]=rr[34]; X[XNi59]=rr[35]; X[XNi60]=rr[36]; X[XNi61]=rr[37];

        /* We have these from Lynnette's models - good enough?
           X[XH1]=rr[38]; X[XHe4]=rr[39]; X[XC12]=rr[40]; X[XN14]=rr[41]; X[XO16]=rr[42]; X[XNe20]=rr[43];
        */

        /* normalize MgAl to total in the star */
        double fm = star_MgAl/(X[XMg24]/24.0+X[XMg25]/25.0+X[XMg26]/26.0+X[XAl26]/26.0+X[XAl27]/27.0);
        X[XMg24] *= fm;
        X[XMg25] *= fm;
        X[XMg26] *= fm;
        X[XAl26] *= fm;
        X[XAl27] *= fm;

        /* ditto for NeNa */
        fm=star_NeNa/(X[XNe20]/20.0+X[XNe21]/21.0+X[XNe22]/22.0+X[XNa23]/23.0);
        X[XNe20] *= fm;
        X[XNe21] *= fm;
        X[XNe22] *= fm;
        X[XNa23] *= fm;

        Dprint("RSgives m=%g f=%g Ne22=%g Na23=%g Mg: 24=%g 25=%g 26=%g\n",star->mass,x[2],
               X[XNe22],X[XNa23],X[XMg24],X[XMg25],X[XMg26]);

#endif // NUCSYN_WR_RS_TABLE

        /* hydrogen is everything else */
        X[XH1] = 0.0;
        X[XH1] = Max(0.0,1.0-nucsyn_totalX(X));
        /* .. but may still be zero! */

        if(X[XHe4]>0.5)
        {
            /*
             * Normalize assuming He4 is reasonably abundant
             */
            X[XHe4] = 0.0;
            X[XHe4] = 1.0-nucsyn_totalX(X);
        }
        else
        {
            /*
             * Calculate hydrogen from everything left
             */
            X[XH1] = 0.0;
            X[XH1] = Max(1.0-nucsyn_totalX(X),0.0);
        }
    }



    /*
     * Helium stars which are NOT helium stars by Lynnette's
     * prescription... e.g. low mass helium stars,still
     * require their surface CNO to be converted to N
     *
     * We cannot say much about NeNa and MgAl here... but
     * in these stars perhaps it wasn't so hot that NeNaMgAl was
     * activated (but it probably was)
     */

    const Boolean is_He_star =
        star->stellar_type >= HeMS &&
        (X[XO16] + X[XC12] < 0.05);

    if(is_He_star)
    {
        /*
         * Convert all H1 to He4
         */
        X[XHe4] += X[XH1];
        X[XH1] = 0.0;

#define CNO_LIST     \
    Y( XC12,   0.01) \
    Y( XC13, 0.0025) \
    Y( XN14,    0.0) \
    Y( XN15, 0.0001) \
    Y( XO16,   0.01) \
    Y( XO17,  0.001) \
    Y( XO18, 0.0001)

#undef Y
#define Y(ISOTOPE,EQFRAC) #ISOTOPE"=%g "
        static const char * _format1 = "BSE He star, Lynnette not : "CNO_LIST"\n";
#undef Y
#define Y(ISOTOPE,EQFRAC) ,X[ISOTOPE]
        Dprint(_format1 CNO_LIST);

        /*
         * Calculate Ncno
         */
#undef Y
#define Y(ISOTOPE,EQFRAC) star->Xinit[ISOTOPE]/stardata->store->mnuc[ISOTOPE] +
        const double Ncno = CNO_LIST 0.0;

        /*
         * Set minor isotopes to ~equilibrium values
         */
#undef Y
#define Y(ISOTOPE,EQFRAC) X[ISOTOPE] = EQFRAC * Ncno;
        CNO_LIST;

        /*
         * Compute the remainder, put it in nitrogen
         */
#undef Y
#define Y(ISOTOPE,EQFRAC) X[ISOTOPE] +
        X[XN14] = Ncno - (CNO_LIST 0.0);

        /*
         * convert back to mass fraction
         */
#undef Y
#define Y(ISOTOPE,EQFRAC) X[ISOTOPE] *= stardata->store->mnuc[ISOTOPE];
        CNO_LIST

        /* Final normalization */
        X[XHe4] = 0.0;
        X[XHe4] = 1.0 - nucsyn_totalX(X);

#undef Y
#define Y(ISOTOPE,EQFRAC) #ISOTOPE"=%g "
        static const char * _format2 = "Prescription gives : "CNO_LIST"\n";
#undef Y
#define Y(ISOTOPE,EQFRAC) ,X[ISOTOPE]
        Dprint(_format2 CNO_LIST);
    }

#ifdef WRDEBUG2
    if(star->starnum==0)fprintf(stderr,"CNO out n=%g N14=%g\n",CNOsum(X),X[XN14]);
#endif // WRDEBUG2

    /*
     * no more changes if we're below the mass break
     */
    if(star->effective_zams_mass > NUCSYN_WR_MASS_BREAK)
    {
        if((star->stellar_type==CHeB)&&
           (X[XF19]<10.0*star->Xinit[XF19]))
        {
            /*
             * if surface F19 is the same as ZAMS then increase it - note there
             * must be a better way to do this!
             */
            X[XF19] = star->Xinit[XF19]*50.0;
        }

        if(X[XH1]>0.01)
        {
            // O18,O17 ~ O16  if X>0 (again from MM94)
            X[XO18] = 2e-3 * X[XO16];
            X[XO17] = 6e-4 * X[XO16];
        }
        else
        {
            X[XO18]=0.0;
            X[XO17]=0.0;
        }

        /*
         * Al26 is destroyed during and after CHeB
         */
        if(star->stellar_type>=4)
        {
            X[XAl26]=0.0;
        }

        /* Final normalization */
        X[XHe4] = 0.0;
        X[XHe4] = 1.0 - nucsyn_totalX(X);

        Dprint("WRtable XH=%g rHe=%g (XHe=%g) rC=%g (XC12=%g XC13=%g) rN=%g (XN=%g) O=%g (%g)  Ne=%g (%g %g) Mg=%g (%g)\n",
               X[XH1],
               r[R_He],
               X[XHe4],
               r[R_C],X[XC12],X[XC13],
               r[R_N],X[XN14],
               r[R_O],X[XO16],
               r[R_Ne],X[XNe20],X[XNe22],
               r[R_Mg],X[XMg24]
            );

        Dprint("nucsyn_WR_table: out H1=%g He4=%g XC12=%g XN14=%g XO16=%g tot=%g\n\n",
               X[XH1],X[XHe4],X[XC12],X[XN14],X[XO16],nucsyn_totalX(X));

        /* final checks! */
#define XCHECK(A) (X[(A)]<0.0 || X[(A)]>1.0)

        if(XCHECK(XH1) ||
           XCHECK(XHe4) ||
           XCHECK(XC12) ||
           XCHECK(XC13) ||
           XCHECK(XN14))
        {
            Backtrace;
            Exit_binary_c(BINARY_C_OUT_OF_RANGE,
                          "nucsyn_WR_table error model %d time %g star %d stellar type %d: H1=%g He4=%g C12=%g C13=%g N14=%g O16=%g Ne20=%g Ne22=%g n=%g tot=%g  : Input x 0=%g 1=%g 2=%g; initX H1=%g He4=%g C12=%g N14=%g O16=%g Ne20=%g Ne22=%g n=%g\n",
                          stardata->model.model_number,
                          stardata->model.time,
                          star->starnum,
                          star->stellar_type,
                          X[XH1],
                          X[XHe4],
                          X[XC12],
                          X[XC13],
                          X[XN14],
                          X[XO16],
                          X[XNe20],
                          X[XNe22],
                          X[Xn],
                          nucsyn_totalX(X),
                          x[0],
                          x[1],
                          x[2],
                          star->Xinit[XH1],
                          star->Xinit[XHe4],
                          star->Xinit[XC12],
                          star->Xinit[XN14],
                          star->Xinit[XO16],
                          star->Xinit[XNe20],
                          star->Xinit[XNe22],
                          star->Xinit[Xn]
                );
        }
    }

#endif//NUCSYN_WR
}

#if defined NUCSYN_WR_TABLES && defined WRDEBUG2
static double CNOsum(Abundance *X)
{
    // CNO by number
    return (X[XC12]/12.0+X[XC13]/13.0+X[XN14]/14.0+X[XN15]/15.0+X[XO16]/16.0+X[XO17]/17.0+X[XO18]/18.0);
}
#endif

#endif//NUCSYN
