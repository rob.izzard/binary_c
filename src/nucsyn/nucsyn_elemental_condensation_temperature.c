#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef NUCSYN

double nucsyn_elemental_condensation_temperature(struct stardata_t * const stardata,
                                                 const char * const element)
{
    if(stardata->store->condensation_temperatures == NULL)
    {
        nucsyn_load_condensation_data(stardata);
    }

    const Atomic_number Z = nucsyn_element_to_atomic_number(stardata->store,
                                                            element,
                                                            0);

    if(Z >= 0 && Z < NUMBER_OF_ELEMENTS)
    {
        return stardata->store->condensation_temperatures[Z];
    }
    else
    {
        return -1;
    }
}

#endif // NUCSYN
