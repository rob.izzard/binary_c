#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef NUCSYN

static void _process_data_table(struct data_table_t * const table);

double nucsyn_sn_Gronow2021a(struct stardata_t * const stardata,
                             Abundance * Restrict const X,
                             const double mcore,
                             const double mshell)
{
    /*
     * Yields of Gronow et al. (2021)
     * a) A+A 649 A155 (fixed metallicity)
     */

    if(stardata->persistent_data->Gronow2021a_core_ejecta == NULL)
    {
        /*
         * Load the table data and process it as required
         */
        Isotope * isotope_list = NULL;
        size_t nlines,length,maxwidth;

        char * files[] = {
            "src/supernovae/Gronow2021a/Gronow2021.1.dat", // stable
            //"src/supernovae/Gronow2021a/Gronow2021.3.dat", // unstable
        };
        size_t isotope_offset = 0;
        for(size_t nfile=0; nfile<Array_size(files); nfile++)
        {
            /*
             * Get list of isotopes
             */
            char * file = files[nfile];
            struct string_array_t ** strings =
                load_data_as_strings(stardata,
                                     file,
                                     &nlines,
                                     &length,
                                     &maxwidth,
                                     ' ',
                                     0,
                                     0);

            isotope_list = Realloc(isotope_list,
                                   sizeof(Isotope)*(isotope_offset+nlines));
            for(size_t i=0; i<nlines; i++)
            {
                char * s = strings[i]->strings[0];
                size_t ndigits = 0;
                size_t nalpha = 0;

                while(isdigit(s[ndigits]))
                {
                    ndigits++;
                }
                while(isalpha(s[nalpha + ndigits]))
                {
                    nalpha++;
                }
                char isotope[6] = {'\0'};
                memcpy(isotope,
                       s + ndigits,
                       nalpha*sizeof(char));
                memcpy(isotope + nalpha,
                       s,
                       ndigits*sizeof(char));
                Isotope n_isotope = Xn;

                /*
                 * linear search : slow but we
                 * do it only once per isotope
                 */
                for(size_t j=0; j<ISOTOPE_ARRAY_SIZE; j++)
                {
                    if(Strings_equal(nucsyn_isotope_strings[j],
                                     isotope))
                    {
                        n_isotope = j;
                        break;
                    }
                }

                /*
                 * Now, if the isotope is Xn, we know it has no
                 * match and should be ignored
                 */
                Dprint("%zu %s : ndigits %zu, nalpha %zu : isotope \"%s\" %u\n",
                       i,
                       s,
                       ndigits,
                       nalpha,
                       isotope,
                       n_isotope);

                isotope_list[i+isotope_offset] = n_isotope;
            }
            isotope_offset += nlines;
            free_array_of_string_arrays(&strings,
                                        nlines);

        }

#define _load_tables(NSTABLE,                                           \
                     NUNSTABLE)                                         \
        __extension__                                                   \
            ({                                                          \
                struct data_table_t * t;                                \
                struct data_table_t * stable;                           \
                New_data_table(stable);                                 \
                Load_table(stable,                                      \
                           "src/supernovae/Gronow2021a/Gronow2021."#NSTABLE".dat",  \
                           All_cols,                                    \
                           0,                                           \
                           0,                                           \
                           FALSE);                                      \
                                                                        \
                if((NUNSTABLE)==0)                                      \
                {                                                       \
                    t = stable;                                         \
                }                                                       \
                else                                                    \
                {                                                       \
                    struct data_table_t * unstable;                     \
                    New_data_table(unstable);                           \
                    Load_table(unstable,                                \
                               "src/supernovae/Gronow2021a/Gronow2021."#NUNSTABLE".dat", \
                               All_cols,                                \
                               0,                                       \
                               0,                                       \
                               FALSE);                                  \
                    t = join_data_tables(stardata,                      \
                                         stable,                        \
                                         unstable,                      \
                                         0,1,                           \
                                         0,1);                          \
                    Delete_data_table(stable);                          \
                    Delete_data_table(unstable);                        \
                }                                                       \
                _process_data_table(t);                                 \
                t;                                                      \
            })

        /*
         * load low- and high-mass tables and join them
         */
        struct data_table_t * table_low = _load_tables(1,0); // 1,3
        struct data_table_t * table_high = _load_tables(2,0); // 2,4
        const double metadata[][3] =
            {
                /* MCO, Mshell, Mshell (at detonation) */
                /* table 1 */
                {0.795, 0.109, 0.109},
                {0.803, 0.053, 0.075},
                {0.803, 0.028, 0.040},
                {0.888, 0.108, 0.108},
                {0.899, 0.053, 0.074},
                {0.905, 0.026, 0.043},

                /* table 2 */
                {1.105, 0.090, 0.133},
                {1.002, 0.052, 0.074},
                {1.028, 0.027, 0.047},
                {1.005, 0.020, 0.028},
                {1.100, 0.054, 0.123}
            };

        /*
         * join tables 1 and 2, selecting every other row
         * so we have shell and core ejecta separately
         */
        struct data_table_t * shell_ejecta = join_data_tables(stardata,
                                                              table_low,
                                                              table_high,
                                                              0,2,
                                                              0,2);
        struct data_table_t * core_ejecta = join_data_tables(stardata,
                                                             table_low,
                                                             table_high,
                                                             1,2,
                                                             1,2);

        Delete_data_table(table_low);
        Delete_data_table(table_high);

        /*
         * Add parameters from metadata
         */
        {
            const size_t _width = shell_ejecta->nparam + shell_ejecta->ndata;
            for(size_t row=0; row<shell_ejecta->nlines; row++)
            {
                double * const line = shell_ejecta->data + row*_width;
                *(line + 0) = ((int)(10.0*(0.05+metadata[row][0])))/10.0;;
                *(line + 1) = metadata[row][1]; // 1 = shell, 2 = at detonation
            }
        }
        {
            const size_t _width = core_ejecta->nparam + core_ejecta->ndata;
            for(size_t row=0; row<core_ejecta->nlines; row++)
            {
                double * const line = core_ejecta->data + row*_width;
                *(line + 0) = ((int)(10.0*(0.05+metadata[row][0])))/10.0;;
                *(line + 1) = metadata[row][1]; // 1 = shell, 2 = at detonation
            }
        }

        /*
         * Sort tables
         */
#define _sort(TABLE)                                \
        {                                           \
            struct data_table_t * sorted_table;     \
            sorted_table = sort_data_table(TABLE);  \
            memcpy(TABLE->data,                     \
                   sorted_table->data,              \
                   Data_table_data_size(TABLE));    \
            Delete_data_table(sorted_table);        \
        }

        _sort(shell_ejecta);
        _sort(core_ejecta);


        if(X == NULL)
        {
            /*
             * return before unmapping : we do this if called
             * from nucsyn_sn_Gronow2021a()
             */
            stardata->persistent_data->Gronow2021a_shell_ejecta = shell_ejecta;
            stardata->persistent_data->Gronow2021a_core_ejecta = core_ejecta;
            Safe_free(isotope_list);
            return 0.0;
        }
        else
        {

            /*
             * Dummy interpolation
             */
            Interpolate(shell_ejecta,NULL,NULL,FALSE);
            Interpolate(core_ejecta,NULL,NULL,FALSE);

            /*
             * Make rinterpolate tables
             */
            struct rinterpolate_table_t * unmapped_core_ejecta =
                rinterpolate_table_struct(stardata->persistent_data->rinterpolate_data,
                                          core_ejecta->data,
                                          NULL);
            struct rinterpolate_table_t * unmapped_shell_ejecta =
                rinterpolate_table_struct(stardata->persistent_data->rinterpolate_data,
                                          shell_ejecta->data,
                                          NULL);

            /*
             * Make mapped data tables
             *
             * We only need to map the shell masses in
             * the second column (number 1)
             */
            Boolean change_resolution[] = { FALSE, TRUE };
            double new_resolutions_f[] = { 0, 5 };

            Dprint("map nlines %u nparam %u ndata %u\n",
                   unmapped_core_ejecta->l,
                   unmapped_core_ejecta->n,
                   unmapped_core_ejecta->d);

            struct rinterpolate_table_t * const mapped_core_ejecta =
                rinterpolate_map_right_columns(stardata->persistent_data->rinterpolate_data,
                                               unmapped_core_ejecta,
                                               change_resolution,
                                               new_resolutions_f,
                                               NULL);

            struct rinterpolate_table_t * const mapped_shell_ejecta =
                rinterpolate_map_right_columns(stardata->persistent_data->rinterpolate_data,
                                               unmapped_shell_ejecta,
                                               change_resolution,
                                               new_resolutions_f,
                                               NULL);

            /*
             * Free temporary tables
             */
            Delete_data_table(shell_ejecta);
            Delete_data_table(core_ejecta);

            /*
             * Put in the store
             */
            NewDataTable_from_Pointer(mapped_core_ejecta->data,
                                      stardata->persistent_data->Gronow2021a_core_ejecta,
                                      mapped_core_ejecta->n,
                                      mapped_core_ejecta->d,
                                      mapped_core_ejecta->l);
            NewDataTable_from_Pointer(mapped_shell_ejecta->data,
                                      stardata->persistent_data->Gronow2021a_shell_ejecta,
                                      mapped_shell_ejecta->n,
                                      mapped_shell_ejecta->d,
                                      mapped_shell_ejecta->l);
            stardata->persistent_data->Gronow2021a_isotope_list = isotope_list;
            stardata->persistent_data->Gronow2021a_nisotopes = isotope_offset;
            Dprint("We have an isotope list of %zu isotopes\n",isotope_offset);
        }
    }
    Clear_isotope_array(X);

    if(Is_zero(mcore+mshell))
    {
        /*
         * We eject nothing. This is useful when calling this
         * function to simply set up the table.
         */
        return 0.0;
    }
    else
    {
        /*
         * Interpolate for SNIa yields
         */
        const double x[] = {mcore,mshell};
        Isotope * const list = stardata->persistent_data->Gronow2021a_isotope_list;
        double * r = Malloc(sizeof(double)*stardata->persistent_data->Gronow2021a_core_ejecta->ndata);

        /* get core */
        Interpolate(stardata->persistent_data->Gronow2021a_core_ejecta,
                    x,
                    r,
                    FALSE);

        const double frac = mcore / (mcore + mshell);
        for(size_t i=0; i<stardata->persistent_data->Gronow2021a_nisotopes; i++)
        {
            if(list[i] != Xn)
            {
                X[list[i]] += r[i] * frac;
            }
        }

        /* helium shell */
        Interpolate(stardata->persistent_data->Gronow2021a_shell_ejecta,
                    x,
                    r,
                    FALSE);
        const double frac1 = 1.0 - frac;
        for(size_t i=0; i<stardata->persistent_data->Gronow2021a_nisotopes; i++)
        {
            if(list[i] != Xn)
            {
                X[list[i]] += r[i] * frac1;
            }
        }
        Safe_free(r);

        /*
         * Enforce sum of isotopes = 1
         */
        nucsyn_renormalize_abundance(X);

        /*
          printf("SN Ia DD yields\n");
          for(Isotope i=0; i<ISOTOPE_ARRAY_SIZE; i++)
          {
          if(Is_not_zero(X[i]))
          {
          printf("%s = %g\n",
          nucsyn_isotope_strings[i],
          X[i]);
          }
          }
          printf("total %g\n",nucsyn_totalX(stardata,X));
        */

        /* return mass ejected */
        return mcore + mshell;
    }
}

static void _process_data_table(struct data_table_t * const table)
{
    /*
     * Process a data table:
     * 1) normalize columns to be mass fractions
     * 2) transpose
     */

    /* normalize yields to mass fractions */
    const size_t width = table->nparam + table->ndata;
    for(size_t col=table->nparam; col<width; col++)
    {
        double * const p0 = table->data + col;
        double * p = p0;
        double sum = 0.0;
        for(size_t row=0; row<table->nlines; row++)
        {
            sum += *p;
            p += width;
        }
        p = p0;
        for(size_t row=0; row<table->nlines; row++)
        {
            *p /= sum;
            p += width;
        }
    }

    transpose_data_table(table,table->nparam);
    remove_data_table_leading_rows(table,1);
    add_leading_columns_to_data_table(table,2);
}


#endif // NUCSYN
