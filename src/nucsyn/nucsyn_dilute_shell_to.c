#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef NUCSYN

void Hot_function Nonnull_some_arguments(2,4,5) nucsyn_dilute_shell_to(const double m1,
                                                                       const Abundance * const Restrict X1,
                                                                       const double m2,
                                                                       const Abundance * const Restrict X2,
                                                                       Abundance * Restrict X3)
{
    /*
     * Mix shell 2 into shell 1 without changing shells 1 or 2, but put the
     * result in shell 3.
     *
     * We assume X1 != X3 and X2 != X3 so that
     * a) the pointers can be declared with Restrict
     * b) memcpy is well defined.
     *
     * If X1==X3 or X2==X3 you should use nucsyn_dilute_shell()
     * instead. We will exit suddenly in this case.
     *
     * If m1 is zero (< TINY) just set the abundances to X2.
     * If m1 > zero and m2 < zero just set the abundances to X1.
     *
     * We assume X1!=X2!=X3 so that we can Restrict the pointers.
     */
    if(X1 == X3 || X2 == X3)
    {
        Backtrace;

        Exit_binary_c_no_stardata(BINARY_C_POINTER_FAILURE,
                                  "X1 = %p == X3 = %p or X2 = %p == X3 = %p in nucsyn_dilute_shell_to : X1, X2 and X3 should all be different pointers. You should use nucsyn_dilute_shell() instead.\n",
                                  (void*)X1,
                                  (void*)X3,
                                  (void*)X2,
                                  (void*)X3);
    }

    if(m1<TINY)
    {
        // m1==0, assume m2>0, so just copy X2 to X3
        Copy_abundances(X2,X3);
    }
    else if(m2<TINY)
    {
        // m2==0, but m1>0, copy X1 to X3
        Copy_abundances(X1,X3);
    }
    else if(m1>TINY)
    {
        // both m2 and m1 are >0
        const double f1 = m1/(m1+m2);
        const double f2 = 1.0 - f1;
        Nucsyn_isotope_stride_loop(X3[i] = f1 * X1[i] + f2 * X2[i]);
    }
}
#endif // NUCSYN
