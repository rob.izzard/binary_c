#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef NUCSYN

void nucsyn_TZ_surface_abundances(struct star_t * Restrict const star Maybe_unused)
{
    /*
     * Set the surface abundances in the star to the those
     * appropriate for a Thorne-Zytkow object prior to
     * ejection of its envelope.
     *
     * At present, do nothing, just leave the star as it is.
     *
     * Note that the stellar type of the star could be anything
     * up to and including ONeWD.
     */


}

#endif
