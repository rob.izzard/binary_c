#ifdef __DEP
//#define MIXDEBUG
#ifdef MIXDEBUG
#define MDEBUG(...) _printf(__VA_ARGS__);
#else
#define MDEBUG(...)
#endif

#ifdef MIXDEBUG
static double coratio(double c,double o);

double coratio(double c,double o)
{
    return o>0.0 ? c/o : 1e10;
}
#endif//MIXDEBUG


#ifdef MIXDEBUG
#define MIXDEBUG_START                                                  \
    printf("MIXING \nMIXING      New call\nMIXING\nMIXING ************************************************************\nMIXING premix abundances: \n"); \
    Starloop(k)                                                         \
    {                                                                   \
        SETstar(k);                                                     \
        printf("MIXING Star %d : Type %2d      Mu        :   %12s %12s %12s %12s %12s %12s %12s\n",k,star->stellar_type,"H1","He4","C12","N14","O16","Ba","NC/NO"); \
        if(star->dmacc>VERY_TINY)                                       \
        {                                                               \
            const double mu_acc =                                       \
                nucsyn_effective_molecular_weight(                      \
                    Xacc[k],                                            \
                    star->stellar_type,                                 \
                    stardata->store->molweight);                        \
            printf("MIXING        : Acc. Layer (mu=%2.2e) : %8e %8e %8e %8e %8e %8e\n",mu_acc,Xacc[k][XH1],Xacc[k][XHe4],Xacc[k][XC12],Xacc[k][XN14],Xacc[k][XO16],coratio(Xacc[k][XC12],Xacc[k][XO16])); \
        }                                                               \
        else                                                            \
        {                                                               \
            printf("MIXING        : Acc. Layer : none!");               \
            if(CONVECTIVE_ENVELOPE(star->stellar_type))                 \
            {                                                           \
                printf(" (convective)");                                \
            }                                                           \
            else                                                        \
            {                                                           \
                printf(" (radiative) ");                                \
            }                                                           \
            printf("\n");                                               \
        }                                                               \
        const double mu_env = nucsyn_effective_molecular_weight(        \
            Xenv[k],                                                    \
            star->stellar_type,                                         \
            stardata->store->molweight);                                \
                                                                        \
        printf("MIXING        : Env. Layer (mu=%2.2e) : %8e %8e %8e %8e %8e %8e\n",mu_env,Xenv[k][XH1],Xenv[k][XHe4],Xenv[k][XC12],Xenv[k][XN14],Xenv[k][XO16],coratio(Xenv[k][XC12],Xenv[k][XO16])); \
                                                                        \
        if(ndmacc[k]>VERY_TINY)                                         \
        {                                                               \
            printf("MIXING        : (dmacc[%d]=%g ndmacc[%d]=%g) Difference : %8e %8e %8e %8e %8e %8e\n",k,dmacc[k],k,ndmacc[k],Xacc[k][XH1]-Xenv[k][XH1],Xacc[k][XHe4]-Xenv[k][XHe4],Xacc[k][XC12]-Xenv[k][XC12],Xacc[k][XN14]-Xenv[k][XN14],Xacc[k][XO16]-Xenv[k][XO16],coratio(Xacc[k][XC12],Xacc[k][XO16])); \
        }                                                               \
    }
#else
#define MIXDEBUG_START
#endif


/*
 * Mass in this timestep from derivative D of star S
 */
#define Dm(DERIV) (star->derivative[DERIV] * dt)
/*
 * or the systematic derivative
 */
#define Dm_system(DERIV) (stardata->model.derivative[DERIV] * dt)

#ifdef MIXDEBUG
#define MIXDEBUG_END                                                    \
    printf("MIXING New (postmix) abundances : \n************************************************************\n"); \
    Starloop(k)                                                         \
    {                                                                   \
        printf("MIXING Star %d : Type %2d      Mu        :   %12s %12s %12s %12s %12s %12s %12s\n",k,stardata->star[k].stellar_type,"H1","He4","C12","N14","O16","Ba","NC/NO"); \
        if(fabs(stardata->star[k].dmacc)>VERY_TINY)                     \
        {                                                               \
            const double mu_acc = nucsyn_effective_molecular_weight(    \
                Xacc[k],                                                \
                stardata->star[k].stellar_type,                         \
                stardata->store->molweight);                            \
            printf("MIXING        : Acc. Layer (mu=%2.2e) : %8e %8e %8e %8e %8e %8e\n",mu_acc,Xacc[k][XH1],Xacc[k][XHe4],Xacc[k][XC12],Xacc[k][XN14],Xacc[k][XO16],Xacc[k][XC12]/Xacc[k][XO16]*16.0/12.0); \
        }                                                               \
        else                                                            \
        {                                                               \
            printf("MIXING        : Acc. Layer : none!");               \
            if(CONVECTIVE_ENVELOPE(stardata->star[k].stellar_type))     \
            {                                                           \
                printf(" (convective)");                                \
            }                                                           \
            else                                                        \
            {                                                           \
                printf(" (radiative) ");                                \
            }                                                           \
            printf("\n");                                               \
        }                                                               \
        const double mu_env = nucsyn_effective_molecular_weight(        \
            Xenv[k],                                                    \
            stardata->star[k].stellar_type,                             \
            stardata->store->molweight                                  \
            );                                                          \
        printf("MIXING        : Env. Layer (mu=%2.2e) : %8e %8e %8e %8e %8e %8e\n",mu_env,Xenv[k][XH1],Xenv[k][XHe4],Xenv[k][XC12],Xenv[k][XN14],Xenv[k][XO16],coratio(Xenv[k][XC12],Xenv[k][XO16])); \
                                                                        \
        if(fabs(ndmacc[k])>VERY_TINY)                                   \
        {                                                               \
            printf("MIXING        : (dmacc[%d]=%g ndmacc[%d]=%g) Difference : %8e %8e %8e %8e %8e\n",k,dmacc[k],k,ndmacc[k],Xacc[k][XH1]-Xenv[k][XH1],Xacc[k][XHe4]-Xenv[k][XHe4],Xacc[k][XC12]-Xenv[k][XC12],Xacc[k][XN14]-Xenv[k][XN14],Xacc[k][XO16]-Xenv[k][XO16]); \
        }                                                               \
    }
#else
#define MIXDEBUG_END
#endif


#define ID_wind_source(STAR,STARDATA) id_wind_source((STAR),(STARDATA))
#else
#define ID_wind_source(STAR,STARDATA) 0
#endif

#define SOURCE_NEEDS_ID

#define Add_yield(DM_LOSE,X_LOSE,DM_GAIN,X_GAIN,SOURCE) \
    calc_yields(                                        \
        stardata,                                       \
        star,                                           \
        (DM_LOSE),                                      \
        (X_LOSE),                                       \
        (DM_GAIN),                                      \
        (X_GAIN),                                       \
        (k),                                            \
        YIELD_NOT_FINAL,                                \
        (SOURCE)                                        \
        );



#define Free_if_allocated(S)                    \
    if(allocated_X ## S == TRUE)                \
    {                                           \
        Safe_free(X ## S);                      \
    }
#endif __DEP
