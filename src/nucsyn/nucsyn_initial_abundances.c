#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef NUCSYN
static void Lodders2003_mix(Abundance * const X,
                            const Abundance Z,
                            struct stardata_t * Restrict const stardata);
static void Lodders2010_mix(Abundance * const X,
                            const Abundance Z,
                            struct stardata_t * Restrict const stardata);
static void Amanda_quadratic_fit(Abundance * const X,
                                 const Abundance Z);
static void Anders_Grevesse_1989_mix(Abundance * const X,
                                     const Abundance Z);
static void Garcia_Berro_changes(Abundance * const X);
static void Grevesse_Noels_changes(Abundance * const X,
                                   const Abundance Z);
static void Asplund2009_mix(Abundance * const X,Abundance Z,
                            struct stardata_t * Restrict const stardata);
static void Asplund2009_Kobayashi2011_mix(Abundance * const X,Abundance Z,
                                          struct stardata_t * Restrict const stardata);

void nucsyn_initial_abundances(struct stardata_t * Restrict const stardata Maybe_unused,
                               Abundance * const X,
                               const Abundance Z,
                               const Abundance_mix mix,
                               const Boolean modulate)
{
    /*
     * function to set the initial abundances in array X
     * according to metallicity Z and abundance mixture 'mix'.
     *
     * 'mix' is an integer, as defined in nucsyn_isotopes.h
     *
     * To set the abundances, please use the macros:
     *
     * Set_isotopic_property(X,X<iso>,<abund>)
     * Increment_isotopic_property(X,X<iso>,<abund>)
     *
     * Where X<iso> is the isotope macro (e.g. XH1, XO16)
     * and <abund> is a floating point number to which
     * the abundance array X is either set or incremented.
     *
     * The reason we use these macros is so that the number
     * of isotopes can be adjusted. The macros check the number
     * of the isotope (from the X<iso> macro) and decide whether
     * or not it is valid prior to setting it. This way, you
     * never have to change the code that is in this subroutine.
     */

    Dprint("init abunds\n");
    Dprint("init abunds stardata=%p\n",(void*)stardata);
    Dprint("init abunds X=%p\n",(void*)X);
    Dprint("init abunds Z=%g\n",Z);
    Dprint("init abunds mix=%u\n",mix);

    Dprint("Set init abunds Z=%g, mix=%u=%s, stardata=%p, X=%p\n",
           Z,
           mix,
           nucsyn_init_abunds_code_strings[mix],
           (void*)stardata,
           (void*)X);

    /* first, set all to zero */
    Clear_isotope_array(X);

    if(mix==NUCSYN_INIT_ABUND_MIX_AG89)
    {
        /* Anders and Grevesse 1989 */
        Anders_Grevesse_1989_mix(X,Z);
    }
    else if(mix==NUCSYN_INIT_ABUND_MIX_KARAKAS2002)
    {
        if(More_or_equal(Z,0.004) && Less_or_equal(Z,0.02))
        {
            /* if 0.004>=Z>=0.03 use Amanda's values */
            Amanda_quadratic_fit(X,Z);
        }
        else
        {
            /* Outside fit range, use AG89 */
            Anders_Grevesse_1989_mix(X,Z);
        }
    }
    else if(mix==NUCSYN_INIT_ABUND_MIX_LODDERS2003)
    {
        Lodders2003_mix(X,Z,stardata);
    }
    else if(mix==NUCSYN_INIT_ABUND_MIX_LODDERS2010)
    {
        Lodders2010_mix(X,Z,stardata);
    }
    else if(mix==NUCSYN_INIT_ABUND_MIX_ASPLUND2005)
    {
        // TODO
    }
    else if(mix==NUCSYN_INIT_ABUND_MIX_ASPLUND2009)
    {
        Asplund2009_mix(X,Z,stardata);
    }
    else if(mix==NUCSYN_INIT_ABUND_MIX_KOBAYASHI2011_ASPLUND2009)
    {
        Asplund2009_Kobayashi2011_mix(X,Z,stardata);
    }
    else if(mix==NUCSYN_INIT_ABUND_MIX_GARCIABERRO)
    {
        /*
         * Use AG89 but override some abundances to match
         * the STPAGB models of Garcia Berro, Iben etc.
         */
        Anders_Grevesse_1989_mix(X,Z);
        Garcia_Berro_changes(X);
    }
    else if(mix==NUCSYN_INIT_ABUND_MIX_GREVESSE_NOELS_1993)
    {
        /* most isotopes are like AG89 */
        Anders_Grevesse_1989_mix(X,Z);
        Grevesse_Noels_changes(X,Z);
    }

#ifdef YONG_PROJECT
    /* Mg26 should be 2% of Mg24 */
    X[XMg26] = 0.02 * X[XMg24];
#endif

    /*
     * Apply any initial abundances given on
     * the command line
     */
    if(modulate == TRUE)
    {
        Nucsyn_isotope_stride_loop(
        {
            if(stardata->preferences->the_initial_abundances[i]>-0.5)
            {
                Dprint("Override (by command line argument) initial abundance of isotope %u to %g\n",
                       i,
                       stardata->preferences->the_initial_abundances[i]);
                X[i] = stardata->preferences->the_initial_abundances[i];
            }

            if(stardata->preferences->initial_abundance_multiplier[i]>-0.5)
            {
                fprintf(stderr,"Override (by command line argument multiplier x %g) initial abundance of isotope %u from %g to %g\n",
                       stardata->preferences->initial_abundance_multiplier[i],
                       i,
                       X[i],
                       X[i] * stardata->preferences->initial_abundance_multiplier[i]
                    );
                X[i]*=stardata->preferences->initial_abundance_multiplier[i];
            }
        });
    }

#ifndef NUCSYN_ALL_ISOTOPES
    /*
     * Our metals will not add to the total metallicity,
     * so put the difference in NUCSYN_MISSING_ISOTOPE
     */
    {
        const double dZ = 1.0 - nucsyn_totalX(X);
        X[NUCSYN_MISSING_ISOTOPE] += dZ;
    }
#endif // NUCSYN_ALL_ISOTOPES

    /*
     * Check Xi is not NaN, is in the range 0<Xi<1
     * and put any total error in hydrogen
     */
    Nucsyn_isotope_stride_loop(
        Nancheck(X[i]);
        Clamp(X[i],0.0,1.0);
    );
    X[XH1] += (1.0-nucsyn_totalX(X));
    Dprint("Abundance mixture from Z=%g (Xtot now %g star's Z=%g)\n",Z,nucsyn_totalX(X),nucsyn_totalX(X)-X[XH1]-X[XHe4]);
    Dprint("H %g He %g C12 %g C13 %g N14 %g O16 %g Fe56 %g\nLOGS : H %g He %g C12 %g C13 %g N14 %g O16 %g Fe56 %g\n",
           X[XH1],
           X[XHe4],
           X[XC12],
           X[XC13],
           X[XN14],
           X[XO16],
           X[XFe56],
           log10(X[XH1]),
           log10(X[XHe4]),
           log10(X[XC12]),
           log10(X[XC13]),
           log10(X[XN14]),
           log10(X[XO16]),
           log10(X[XFe56]));
    Dprint("Initial abundances:\nH1=%g He4=%g C12=%g C13=%g Mg24=%g Mg25=%g Mg26=%g\n\n\n",
           X[XH1],
           X[XHe4],
           X[XC12],
           X[XC13],
           X[XMg24],
           X[XMg25],
           X[XMg26]
        );
}

static void Lodders2010_mix(Abundance * const X,
                            Abundance Z,
                            struct stardata_t * Restrict const stardata Maybe_unused)
{
    /* Lodders 2010 solar metalliciy 0.0153293 */
    Set_isotopic_property(X,XH1,0.76-3.0*Z);
    Set_isotopic_property(X,XH2,0.00180047*Z);
    Set_isotopic_property(X,XHe3,0.00552146*Z);
    Set_isotopic_property(X,XLi6,4.4901e-08*Z);
    Set_isotopic_property(X,XLi7,6.40995e-07*Z);
    Set_isotopic_property(X,XBe9,9.80476e-09*Z);
    Set_isotopic_property(X,XB10,6.58217e-08*Z);
    Set_isotopic_property(X,XB11,2.95447e-07*Z);
    Set_isotopic_property(X,XC12,0.15167*Z);
    Set_isotopic_property(X,XC13,0.00184679*Z);
    Set_isotopic_property(X,XN14,0.0527617*Z);
    Set_isotopic_property(X,XN15,0.000207446*Z);
    Set_isotopic_property(X,XO16,0.446335*Z);
    Set_isotopic_property(X,XO17,0.000178286*Z);
    Set_isotopic_property(X,XO18,0.00100787*Z);
    Set_isotopic_property(X,XF19,2.71506e-05*Z);
    Set_isotopic_property(X,XNe20,0.108746*Z);
    Set_isotopic_property(X,XNe21,0.000273529*Z);
    Set_isotopic_property(X,XNe22,0.00879362*Z);
    Set_isotopic_property(X,XNa23,0.00235758*Z);
    Set_isotopic_property(X,XMg24,0.0345287*Z);
    Set_isotopic_property(X,XMg25,0.00457425*Z);
    Set_isotopic_property(X,XMg26,0.00521877*Z);
    Set_isotopic_property(X,XAl27,0.00405694*Z);
    Set_isotopic_property(X,XSi28,0.0458469*Z);
    Set_isotopic_property(X,XSi29,0.00241042*Z);
    Set_isotopic_property(X,XSi30,0.00164652*Z);
    Set_isotopic_property(X,XFe56,0.0773682*Z);

#ifdef NUCSYN_ALL_ISOTOPES
    Set_isotopic_property(X,XP31,0.000456968*Z);
    Set_isotopic_property(X,XS32,0.0227473*Z);
    Set_isotopic_property(X,XS33,0.000185201*Z);
    Set_isotopic_property(X,XS34,0.00107441*Z);
    Set_isotopic_property(X,XS36,4.60295e-06*Z);
    Set_isotopic_property(X,XCl35,0.000243651*Z);
    Set_isotopic_property(X,XCl37,8.21304e-05*Z);
    Set_isotopic_property(X,XAr36,0.00501197*Z);
    Set_isotopic_property(X,XAr38,0.00096482*Z);
    Set_isotopic_property(X,XAr40,1.56237e-06*Z);
    Set_isotopic_property(X,XK39,0.000242412*Z);
    Set_isotopic_property(X,XK40,4.26178e-07*Z);
    Set_isotopic_property(X,XK41,1.84223e-05*Z);
    Set_isotopic_property(X,XCa40,0.00415544*Z);
    Set_isotopic_property(X,XCa42,2.91599e-05*Z);
    Set_isotopic_property(X,XCa43,6.26122e-06*Z);
    Set_isotopic_property(X,XCa44,9.8439e-05*Z);
    Set_isotopic_property(X,XCa46,1.63347e-07*Z);
    Set_isotopic_property(X,XCa48,9.62863e-06*Z);
    Set_isotopic_property(X,XSc45,2.74899e-06*Z);
    Set_isotopic_property(X,XTi46,1.66609e-05*Z);
    Set_isotopic_property(X,XTi47,1.53562e-05*Z);
    Set_isotopic_property(X,XTi48,0.000155128*Z);
    Set_isotopic_property(X,XTi49,1.16574e-05*Z);
    Set_isotopic_property(X,XTi50,1.13639e-05*Z);
    Set_isotopic_property(X,XV50,6.21425e-08*Z);
    Set_isotopic_property(X,XV51,2.5872e-05*Z);
    Set_isotopic_property(X,XCr50,5.05111e-05*Z);
    Set_isotopic_property(X,XCr52,0.0010157*Z);
    Set_isotopic_property(X,XCr53,0.000116705*Z);
    Set_isotopic_property(X,XCr54,2.9623e-05*Z);
    Set_isotopic_property(X,XMn55,0.000900237*Z);
    Set_isotopic_property(X,XFe54,0.00475495*Z);

    Set_isotopic_property(X,XFe57,0.00182135*Z);
    Set_isotopic_property(X,XFe58,0.00024613*Z);
    Set_isotopic_property(X,XCo59,0.00024613*Z);
    Set_isotopic_property(X,XNi58,0.00343917*Z);
    Set_isotopic_property(X,XNi60,0.00137384*Z);
    Set_isotopic_property(X,XNi61,6.05377e-05*Z);
    Set_isotopic_property(X,XNi62,0.000195899*Z);
    Set_isotopic_property(X,XNi64,5.15875e-05*Z);
    Set_isotopic_property(X,XCu63,4.18349e-05*Z);
    Set_isotopic_property(X,XCu65,1.92703e-05*Z);
    Set_isotopic_property(X,XZn64,7.15624e-05*Z);
    Set_isotopic_property(X,XZn66,4.24155e-05*Z);
    Set_isotopic_property(X,XZn67,6.30492e-06*Z);
    Set_isotopic_property(X,XZn68,2.9336e-05*Z);
    Set_isotopic_property(X,XZn70,9.94175e-07*Z);
    Set_isotopic_property(X,XGa69,2.69549e-06*Z);
    Set_isotopic_property(X,XGa71,1.84027e-06*Z);
    Set_isotopic_property(X,XGe70,3.02036e-06*Z);
    Set_isotopic_property(X,XGe72,4.05237e-06*Z);
    Set_isotopic_property(X,XGe73,1.1403e-06*Z);
    Set_isotopic_property(X,XGe74,5.41317e-06*Z);
    Set_isotopic_property(X,XGe76,1.14682e-06*Z);
    Set_isotopic_property(X,XAs75,8.12171e-07*Z);
    Set_isotopic_property(X,XSe74,7.88034e-08*Z);
    Set_isotopic_property(X,XSe76,8.52616e-07*Z);
    Set_isotopic_property(X,XSe77,7.03881e-07*Z);
    Set_isotopic_property(X,XSe78,2.22124e-06*Z);
    Set_isotopic_property(X,XSe80,4.7556e-06*Z);
    Set_isotopic_property(X,XSe82,8.57835e-07*Z);
    Set_isotopic_property(X,XBr79,7.6194e-07*Z);
    Set_isotopic_property(X,XBr81,7.59331e-07*Z);
    Set_isotopic_property(X,XKr78,2.76986e-08*Z);
    Set_isotopic_property(X,XKr80,1.84679e-07*Z);
    Set_isotopic_property(X,XKr82,9.47859e-07*Z);
    Set_isotopic_property(X,XKr83,9.50468e-07*Z);
    Set_isotopic_property(X,XKr84,4.73995e-06*Z);
    Set_isotopic_property(X,XKr86,1.46713e-06*Z);
    Set_isotopic_property(X,XRb85,7.7303e-07*Z);
    Set_isotopic_property(X,XRb87,3.25651e-07*Z);
    Set_isotopic_property(X,XSr84,1.93877e-08*Z);
    Set_isotopic_property(X,XSr86,3.51223e-07*Z);
    Set_isotopic_property(X,XSr87,2.47174e-07*Z);
    Set_isotopic_property(X,XSr88,3.00014e-06*Z);
    Set_isotopic_property(X,XY89,7.31932e-07*Z);
    Set_isotopic_property(X,XZr90,8.86538e-07*Z);
    Set_isotopic_property(X,XZr91,1.95508e-07*Z);
    Set_isotopic_property(X,XZr92,3.01906e-07*Z);
    Set_isotopic_property(X,XZr94,3.12604e-07*Z);
    Set_isotopic_property(X,XZr96,5.14831e-08*Z);
    Set_isotopic_property(X,XNb93,1.28773e-07*Z);
    Set_isotopic_property(X,XMo92,6.04399e-08*Z);
    Set_isotopic_property(X,XMo94,3.88863e-08*Z);
    Set_isotopic_property(X,XMo95,6.81702e-08*Z);
    Set_isotopic_property(X,XMo96,7.24756e-08*Z);
    Set_isotopic_property(X,XMo97,4.22003e-08*Z);
    Set_isotopic_property(X,XMo98,1.08224e-07*Z);
    Set_isotopic_property(X,XMo100,4.43921e-08*Z);
    Set_isotopic_property(X,XRu96,1.68762e-08*Z);
    Set_isotopic_property(X,XRu98,5.7426e-09*Z);
    Set_isotopic_property(X,XRu99,3.9904e-08*Z);
    Set_isotopic_property(X,XRu100,3.97735e-08*Z);
    Set_isotopic_property(X,XRu101,5.45231e-08*Z);
    Set_isotopic_property(X,XRu102,1.01766e-07*Z);
    Set_isotopic_property(X,XRu104,6.1314e-08*Z);
    Set_isotopic_property(X,XRh103,6.76483e-08*Z);
    Set_isotopic_property(X,XPd102,2.5174e-09*Z);
    Set_isotopic_property(X,XPd104,2.794e-08*Z);
    Set_isotopic_property(X,XPd105,5.65323e-08*Z);
    Set_isotopic_property(X,XPd106,6.98663e-08*Z);
    Set_isotopic_property(X,XPd108,6.88225e-08*Z);
    Set_isotopic_property(X,XPd110,3.10582e-08*Z);
    Set_isotopic_property(X,XAg107,4.82606e-08*Z);
    Set_isotopic_property(X,XAg109,4.56838e-08*Z);
    Set_isotopic_property(X,XCd106,3.76469e-09*Z);
    Set_isotopic_property(X,XCd108,2.68506e-09*Z);
    Set_isotopic_property(X,XCd110,3.84819e-08*Z);
    Set_isotopic_property(X,XCd111,3.96235e-08*Z);
    Set_isotopic_property(X,XCd112,7.56069e-08*Z);
    Set_isotopic_property(X,XCd113,3.85276e-08*Z);
    Set_isotopic_property(X,XCd114,9.15241e-08*Z);
    Set_isotopic_property(X,XCd116,2.43064e-08*Z);
    Set_isotopic_property(X,XIn113,1.60542e-09*Z);
    Set_isotopic_property(X,XIn115,3.47179e-08*Z);
    Set_isotopic_property(X,XSn112,6.96053e-09*Z);
    Set_isotopic_property(X,XSn114,4.85867e-09*Z);
    Set_isotopic_property(X,XSn115,2.45086e-09*Z);
    Set_isotopic_property(X,XSn116,1.07963e-07*Z);
    Set_isotopic_property(X,XSn117,5.75565e-08*Z);
    Set_isotopic_property(X,XSn118,1.82918e-07*Z);
    Set_isotopic_property(X,XSn119,6.52998e-08*Z);
    Set_isotopic_property(X,XSn120,2.50436e-07*Z);
    Set_isotopic_property(X,XSn122,3.61856e-08*Z);
    Set_isotopic_property(X,XSn124,4.60295e-08*Z);
    Set_isotopic_property(X,XSb121,3.84688e-08*Z);
    Set_isotopic_property(X,XSb123,2.92708e-08*Z);
    Set_isotopic_property(X,XTe120,1.06528e-09*Z);
    Set_isotopic_property(X,XTe122,2.64331e-08*Z);
    Set_isotopic_property(X,XTe123,9.39378e-09*Z);
    Set_isotopic_property(X,XTe124,4.9774e-08*Z);
    Set_isotopic_property(X,XTe125,7.43674e-08*Z);
    Set_isotopic_property(X,XTe126,1.98966e-07*Z);
    Set_isotopic_property(X,XTe128,3.3785e-07*Z);
    Set_isotopic_property(X,XTe130,3.65966e-07*Z);
    Set_isotopic_property(X,XI127,2.48087e-07*Z);
    Set_isotopic_property(X,XXe124,1.54149e-09*Z);
    Set_isotopic_property(X,XXe126,1.34253e-09*Z);
    Set_isotopic_property(X,XXe128,2.77378e-08*Z);
    Set_isotopic_property(X,XXe129,3.4346e-07*Z);
    Set_isotopic_property(X,XXe130,5.51819e-08*Z);
    Set_isotopic_property(X,XXe131,2.76856e-07*Z);
    Set_isotopic_property(X,XXe132,3.37132e-07*Z);
    Set_isotopic_property(X,XXe134,1.25446e-07*Z);
    Set_isotopic_property(X,XXe136,1.03658e-07*Z);
    Set_isotopic_property(X,XCs133,8.76101e-08*Z);
    Set_isotopic_property(X,XBa130,1.15465e-09*Z);
    Set_isotopic_property(X,XBa132,1.17227e-09*Z);
    Set_isotopic_property(X,XBa134,2.57024e-08*Z);
    Set_isotopic_property(X,XBa135,7.07143e-08*Z);
    Set_isotopic_property(X,XBa136,8.4805e-08*Z);
    Set_isotopic_property(X,XBa137,1.22184e-07*Z);
    Set_isotopic_property(X,XBa138,7.85425e-07*Z);
    Set_isotopic_property(X,XLa138,0*Z);
    Set_isotopic_property(X,XLa139,1.12856e-07*Z);
    Set_isotopic_property(X,XCe136,4.83127e-10*Z);
    Set_isotopic_property(X,XCe138,7.35194e-10*Z);
    Set_isotopic_property(X,XCe140,2.59373e-07*Z);
    Set_isotopic_property(X,XCe142,3.30413e-08*Z);
    Set_isotopic_property(X,XPr141,4.30744e-08*Z);
    Set_isotopic_property(X,XNd142,5.82675e-08*Z);
    Set_isotopic_property(X,XNd143,2.61656e-08*Z);
    Set_isotopic_property(X,XNd144,5.19267e-08*Z);
    Set_isotopic_property(X,XNd145,1.9316e-08*Z);
    Set_isotopic_property(X,XNd146,3.81231e-08*Z);
    Set_isotopic_property(X,XNd148,1.28838e-08*Z);
    Set_isotopic_property(X,XNd150,1.27925e-08*Z);
    Set_isotopic_property(X,XSm144,2.04641e-09*Z);
    Set_isotopic_property(X,XSm147,1.0705e-08*Z);
    Set_isotopic_property(X,XSm148,7.88686e-09*Z);
    Set_isotopic_property(X,XSm149,9.79171e-09*Z);
    Set_isotopic_property(X,XSm150,5.32901e-09*Z);
    Set_isotopic_property(X,XSm152,1.91724e-08*Z);
    Set_isotopic_property(X,XSm154,1.6413e-08*Z);
    Set_isotopic_property(X,XEu151,1.26359e-08*Z);
    Set_isotopic_property(X,XEu153,1.39733e-08*Z);
    Set_isotopic_property(X,XGd152,1.88985e-10*Z);
    Set_isotopic_property(X,XGd154,2.13382e-09*Z);
    Set_isotopic_property(X,XGd155,1.46778e-08*Z);
    Set_isotopic_property(X,XGd156,2.03989e-08*Z);
    Set_isotopic_property(X,XGd157,1.5702e-08*Z);
    Set_isotopic_property(X,XGd158,2.50957e-08*Z);
    Set_isotopic_property(X,XGd160,2.23689e-08*Z);
    Set_isotopic_property(X,XTb159,1.79069e-08*Z);
    Set_isotopic_property(X,XDy156,5.54298e-11*Z);
    Set_isotopic_property(X,XDy158,1.12269e-10*Z);
    Set_isotopic_property(X,XDy160,2.67201e-09*Z);
    Set_isotopic_property(X,XDy161,2.17949e-08*Z);
    Set_isotopic_property(X,XDy162,2.95839e-08*Z);
    Set_isotopic_property(X,XDy163,2.91012e-08*Z);
    Set_isotopic_property(X,XDy164,3.32436e-08*Z);
    Set_isotopic_property(X,XHo165,2.66744e-08*Z);
    Set_isotopic_property(X,XEr162,1.15139e-10*Z);
    Set_isotopic_property(X,XEr164,1.2238e-09*Z);
    Set_isotopic_property(X,XEr166,2.59503e-08*Z);
    Set_isotopic_property(X,XEr167,1.78025e-08*Z);
    Set_isotopic_property(X,XEr168,2.11947e-08*Z);
    Set_isotopic_property(X,XEr170,1.17814e-08*Z);
    Set_isotopic_property(X,XTm169,1.21923e-08*Z);
    Set_isotopic_property(X,XYb168,8.95671e-11*Z);
    Set_isotopic_property(X,XYb170,2.29561e-09*Z);
    Set_isotopic_property(X,XYb171,1.09659e-08*Z);
    Set_isotopic_property(X,XYb172,1.69936e-08*Z);
    Set_isotopic_property(X,XYb173,1.26947e-08*Z);
    Set_isotopic_property(X,XYb174,2.53828e-08*Z);
    Set_isotopic_property(X,XYb176,1.04114e-08*Z);
    Set_isotopic_property(X,XLu175,1.15074e-08*Z);
    Set_isotopic_property(X,XLu176,3.43982e-10*Z);
    Set_isotopic_property(X,XHf174,9.27636e-11*Z);
    Set_isotopic_property(X,XHf176,2.53306e-09*Z);
    Set_isotopic_property(X,XHf177,9.1198e-09*Z);
    Set_isotopic_property(X,XHf178,1.34449e-08*Z);
    Set_isotopic_property(X,XHf179,6.74526e-09*Z);
    Set_isotopic_property(X,XHf180,1.74959e-08*Z);
    Set_isotopic_property(X,XTa180,8.31741e-13*Z);
    Set_isotopic_property(X,XTa181,6.75178e-09*Z);
    Set_isotopic_property(X,XW180,6.3969e-11*Z);
    Set_isotopic_property(X,XW182,1.17422e-08*Z);
    Set_isotopic_property(X,XW183,6.37342e-09*Z);
    Set_isotopic_property(X,XW184,1.37319e-08*Z);
    Set_isotopic_property(X,XW186,1.28904e-08*Z);
    Set_isotopic_property(X,XRe185,6.80397e-09*Z);
    Set_isotopic_property(X,XRe187,1.24272e-08*Z);
    Set_isotopic_property(X,XOs184,3.26956e-11*Z);
    Set_isotopic_property(X,XOs186,3.56964e-09*Z);
    Set_isotopic_property(X,XOs187,2.85793e-09*Z);
    Set_isotopic_property(X,XOs188,3.01971e-08*Z);
    Set_isotopic_property(X,XOs189,3.69424e-08*Z);
    Set_isotopic_property(X,XOs190,6.04333e-08*Z);
    Set_isotopic_property(X,XOs192,9.48511e-08*Z);
    Set_isotopic_property(X,XIr191,8.48702e-08*Z);
    Set_isotopic_property(X,XIr193,1.44364e-07*Z);
    Set_isotopic_property(X,XPt190,6.75178e-11*Z);
    Set_isotopic_property(X,XPt192,3.41177e-09*Z);
    Set_isotopic_property(X,XPt194,1.44821e-07*Z);
    Set_isotopic_property(X,XPt195,1.49322e-07*Z);
    Set_isotopic_property(X,XPt196,1.12138e-07*Z);
    Set_isotopic_property(X,XPt198,3.20171e-08*Z);
    Set_isotopic_property(X,XAu197,6.82354e-08*Z);
    Set_isotopic_property(X,XHg196,3.48288e-10*Z);
    Set_isotopic_property(X,XHg198,1.61847e-08*Z);
    Set_isotopic_property(X,XHg199,2.72289e-08*Z);
    Set_isotopic_property(X,XHg200,3.7673e-08*Z);
    Set_isotopic_property(X,XHg201,2.14296e-08*Z);
    Set_isotopic_property(X,XHg202,4.91804e-08*Z);
    Set_isotopic_property(X,XHg204,1.12399e-08*Z);
    Set_isotopic_property(X,XTl203,1.94791e-08*Z);
    Set_isotopic_property(X,XTl205,4.6995e-08*Z);
    Set_isotopic_property(X,XPb204,2.39281e-08*Z);
    Set_isotopic_property(X,XPb206,2.24798e-07*Z);
    Set_isotopic_property(X,XPb207,2.50175e-07*Z);
    Set_isotopic_property(X,XPb208,7.19538e-07*Z);
    Set_isotopic_property(X,XBi209,5.13331e-08*Z);
    Set_isotopic_property(X,XTh232,1.81483e-08*Z);
    Set_isotopic_property(X,XU234,2.03858e-13*Z);
    Set_isotopic_property(X,XU235,2.42281e-09*Z);
    Set_isotopic_property(X,XU238,7.61288e-09*Z);
#endif // NUCSYN_ALL_ISOTOPES

    // He4 is everything else
    Set_isotopic_property(X,XHe4,0.0);
    Set_isotopic_property(X,XHe4,1.0-nucsyn_totalX(X));


}

static void Lodders2003_mix(Abundance * const X,
                            Abundance Z,
                            struct stardata_t * Restrict const stardata Maybe_unused)
{
    /*
     * Lodders 2003 element mixture.
     */
    Increment_isotopic_property(X,XH1,0.76-3.0*Z);
    Increment_isotopic_property(X,XH2,0.00184696789924466*Z);
    Increment_isotopic_property(X,XHe3,0.00228462409674379*Z);
    Increment_isotopic_property(X,XLi6,4.94639621871502e-08*Z);
    Increment_isotopic_property(X,XLi7,7.02638911797041e-07*Z);
    Increment_isotopic_property(X,XBe9,1.29957455047997e-08*Z);
    Increment_isotopic_property(X,XB10,6.72247752131777e-08*Z);
    Increment_isotopic_property(X,XB11,2.99127730475951e-07*Z);
    Increment_isotopic_property(X,XC12,0.164511733621016*Z);
    Increment_isotopic_property(X,XC13,0.00199629971857926*Z);
    Increment_isotopic_property(X,XN14,0.0532667735318631*Z);
    Increment_isotopic_property(X,XN15,0.000209810618707134*Z);
    Increment_isotopic_property(X,XO16,0.441768403381674*Z);
    Increment_isotopic_property(X,XO17,0.000175101642865201*Z);
    Increment_isotopic_property(X,XO18,0.000996446231138293*Z);
    Increment_isotopic_property(X,XF19,3.12936909468183e-05*Z);
    Increment_isotopic_property(X,XNe20,0.0781710756338495*Z);
    Increment_isotopic_property(X,XNe21,0.000196810173538469*Z);
    Increment_isotopic_property(X,XNe22,0.0063241810086944*Z);
    Increment_isotopic_property(X,XNa23,0.00259016099381668*Z);
    Increment_isotopic_property(X,XMg24,0.0378651917664484*Z);
    Increment_isotopic_property(X,XMg25,0.00499339285737265*Z);
    Increment_isotopic_property(X,XMg26,0.00571753273135555*Z);
    Increment_isotopic_property(X,XAl27,0.00444646947499454*Z);
    Increment_isotopic_property(X,XSi28,0.0505691664729154*Z);
    Increment_isotopic_property(X,XSi29,0.00265936354424003*Z);
    Increment_isotopic_property(X,XSi30,0.0018134827942011*Z);
    Increment_isotopic_property(X,XFe56,0.0843166694156449*Z);

#ifdef NUCSYN_ALL_ISOTOPES
    Increment_isotopic_property(X,XP31,0.000508274521662046*Z);
    Increment_isotopic_property(X,XS32,0.0264873055474374*Z);
    Increment_isotopic_property(X,XS33,0.000215832063035143*Z);
    Increment_isotopic_property(X,XS34,0.00124834821434316*Z);
    Increment_isotopic_property(X,XCl35,0.000271953882365456*Z);
    Increment_isotopic_property(X,XS36,5.35761680696924e-06*Z);
    Increment_isotopic_property(X,XAr36,0.00611261780700399*Z);
    Increment_isotopic_property(X,XCl37,9.1943048953811e-05*Z);
    Increment_isotopic_property(X,XAr38,0.00117309484669264*Z);
    Increment_isotopic_property(X,XK39,0.000262940318709579*Z);
    Increment_isotopic_property(X,XCa40,0.00477384022711044*Z);
    Increment_isotopic_property(X,XK40,4.20619915985743e-07*Z);
    Increment_isotopic_property(X,XAr40,1.879865546305e-06*Z);
    Increment_isotopic_property(X,XK41,1.99510522068005e-05*Z);
    Increment_isotopic_property(X,XCa42,3.34733558838934e-05*Z);
    Increment_isotopic_property(X,XCa43,7.14877619780797e-06*Z);
    Increment_isotopic_property(X,XCa44,0.000112956421013602*Z);
    Increment_isotopic_property(X,XSc45,3.0136594539202e-06*Z);
    Increment_isotopic_property(X,XTi46,1.80153781520896e-05*Z);
    Increment_isotopic_property(X,XCa46,2.2519222690112e-07*Z);
    Increment_isotopic_property(X,XTi47,1.65663151268128e-05*Z);
    Increment_isotopic_property(X,XCa48,1.10912067231995e-05*Z);
    Increment_isotopic_property(X,XTi48,0.000167778000007721*Z);
    Increment_isotopic_property(X,XTi49,1.25696426476373e-05*Z);
    Increment_isotopic_property(X,XCr50,5.4731502103359e-05*Z);
    Increment_isotopic_property(X,XV50,7.04949579864374e-08*Z);
    Increment_isotopic_property(X,XTi50,1.23366176476265e-05*Z);
    Increment_isotopic_property(X,XV51,2.87299851441793e-05*Z);
    Increment_isotopic_property(X,XCr52,0.0010971756933278*Z);
    Increment_isotopic_property(X,XCr53,0.000126824345804156*Z);
    Increment_isotopic_property(X,XFe54,0.00517926456326356*Z);
    Increment_isotopic_property(X,XCr54,3.21457008418155e-05*Z);
    Increment_isotopic_property(X,XMn55,0.0009873993781967*Z);
    Increment_isotopic_property(X,XFe57,0.00198231821857862*Z);
    Increment_isotopic_property(X,XFe58,0.000268037495810654*Z);
    Increment_isotopic_property(X,XNi58,0.00369585091151462*Z);
    Increment_isotopic_property(X,XCo59,0.000268384096020754*Z);
    Increment_isotopic_property(X,XNi60,0.00147240468914339*Z);
    Increment_isotopic_property(X,XNi61,6.51001355071975e-05*Z);
    Increment_isotopic_property(X,XNi62,0.000210885666816427*Z);
    Increment_isotopic_property(X,XCu63,4.49669713255988e-05*Z);
    Increment_isotopic_property(X,XZn64,7.46933243731852e-05*Z);
    Increment_isotopic_property(X,XNi64,5.53933714311206e-05*Z);
    Increment_isotopic_property(X,XCu65,2.06834164925485e-05*Z);
    Increment_isotopic_property(X,XZn66,4.42003386574963e-05*Z);
    Increment_isotopic_property(X,XZn67,6.59930716416924e-06*Z);
    Increment_isotopic_property(X,XZn68,3.06261428585523e-05*Z);
    Increment_isotopic_property(X,XGa69,2.92119356736132e-06*Z);
    Increment_isotopic_property(X,XGe70,3.50908235310266e-06*Z);
    Increment_isotopic_property(X,XZn70,1.04175882357735e-06*Z);
    Increment_isotopic_property(X,XGa71,1.99510522068005e-06*Z);
    Increment_isotopic_property(X,XGe72,4.70906319349402e-06*Z);
    Increment_isotopic_property(X,XGe73,1.32941741602757e-06*Z);
    Increment_isotopic_property(X,XGe74,6.27444289944841e-06*Z);
    Increment_isotopic_property(X,XSe74,8.40456554660526e-08*Z);
    Increment_isotopic_property(X,XAs75,8.9425791495712e-07*Z);
    Increment_isotopic_property(X,XGe76,1.33940420174231e-06*Z);
    Increment_isotopic_property(X,XSe76,9.16747764748071e-07*Z);
    Increment_isotopic_property(X,XSe77,7.56920029446598e-07*Z);
    Increment_isotopic_property(X,XKr78,3.05478151274562e-08*Z);
    Increment_isotopic_property(X,XSe78,2.38883914296708e-06*Z);
    Increment_isotopic_property(X,XBr79,8.87962323570275e-07*Z);
    Increment_isotopic_property(X,XSe80,5.11323428594959e-06*Z);
    Increment_isotopic_property(X,XKr80,2.00518991605866e-07*Z);
    Increment_isotopic_property(X,XBr81,8.85064197519722e-07*Z);
    Increment_isotopic_property(X,XSe82,9.21682411807121e-07*Z);
    Increment_isotopic_property(X,XKr82,1.03247698744247e-06*Z);
    Increment_isotopic_property(X,XKr83,1.0353163676947e-06*Z);
    Increment_isotopic_property(X,XKr84,5.16164082376695e-06*Z);
    Increment_isotopic_property(X,XSr84,2.15874360009934e-08*Z);
    Increment_isotopic_property(X,XRb85,7.89455410750616e-07*Z);
    Increment_isotopic_property(X,XSr86,3.90814921896136e-07*Z);
    Increment_isotopic_property(X,XKr86,1.59815986141808e-06*Z);
    Increment_isotopic_property(X,XRb87,3.3237785232622e-07*Z);
    Increment_isotopic_property(X,XSr87,2.76296117212295e-07*Z);
    Increment_isotopic_property(X,XSr88,3.35062691964999e-06*Z);
    Increment_isotopic_property(X,XY89,8.03078561381495e-07*Z);
    Increment_isotopic_property(X,XZr90,1.02746401265233e-06*Z);
    Increment_isotopic_property(X,XZr91,2.26664788245725e-07*Z);
    Increment_isotopic_property(X,XMo92,6.95393596670657e-08*Z);
    Increment_isotopic_property(X,XZr92,3.500387974951e-07*Z);
    Increment_isotopic_property(X,XNb93,1.37567385762633e-07*Z);
    Increment_isotopic_property(X,XZr94,3.62434160941049e-07*Z);
    Increment_isotopic_property(X,XMo94,4.43609105062431e-08*Z);
    Increment_isotopic_property(X,XMo95,7.70157416001829e-08*Z);
    Increment_isotopic_property(X,XRu96,1.97949842025916e-08*Z);
    Increment_isotopic_property(X,XMo96,8.15861647096369e-08*Z);
    Increment_isotopic_property(X,XZr96,5.95917378178684e-08*Z);
    Increment_isotopic_property(X,XMo97,4.72962422290673e-08*Z);
    Increment_isotopic_property(X,XMo98,1.2051504706437e-07*Z);
    Increment_isotopic_property(X,XRu98,6.81255441207821e-09*Z);
    Increment_isotopic_property(X,XRu99,4.69143945399741e-08*Z);
    Increment_isotopic_property(X,XRu100,4.68008193298848e-08*Z);
    Increment_isotopic_property(X,XMo100,4.91506512627661e-08*Z);
    Increment_isotopic_property(X,XRu101,6.40799168096716e-08*Z);
    Increment_isotopic_property(X,XPd102,2.91614142870563e-09*Z);
    Increment_isotopic_property(X,XRu102,1.19641692862649e-07*Z);
    Increment_isotopic_property(X,XRh103,7.47881009278114e-08*Z);
    Increment_isotopic_property(X,XRu104,7.20928437007966e-08*Z);
    Increment_isotopic_property(X,XPd104,3.25843361359533e-08*Z);
    Increment_isotopic_property(X,XPd105,6.57952941206749e-08*Z);
    Increment_isotopic_property(X,XPd106,8.1366847062568e-08*Z);
    Increment_isotopic_property(X,XCd106,4.1098560506093e-09*Z);
    Increment_isotopic_property(X,XAg107,5.33664455696828e-08*Z);
    Increment_isotopic_property(X,XCd108,2.9819367228263e-09*Z);
    Increment_isotopic_property(X,XPd108,8.03642521045386e-08*Z);
    Increment_isotopic_property(X,XAg109,5.05006297082064e-08*Z);
    Increment_isotopic_property(X,XPd110,3.61874117663712e-08*Z);
    Increment_isotopic_property(X,XCd110,4.26494495817946e-08*Z);
    Increment_isotopic_property(X,XCd111,4.41239691196776e-08*Z);
    Increment_isotopic_property(X,XCd112,8.37793411803261e-08*Z);
    Increment_isotopic_property(X,XSn112,7.95026470624822e-09*Z);
    Increment_isotopic_property(X,XIn113,1.72595155470128e-09*Z);
    Increment_isotopic_property(X,XCd113,4.29275130271856e-08*Z);
    Increment_isotopic_property(X,XSn114,5.49155722714348e-09*Z);
    Increment_isotopic_property(X,XCd114,1.01571485298792e-07*Z);
    Increment_isotopic_property(X,XIn115,3.89582552538937e-08*Z);
    Increment_isotopic_property(X,XSn115,2.84868167029916e-09*Z);
    Increment_isotopic_property(X,XSn116,1.23251817988865e-07*Z);
    Increment_isotopic_property(X,XCd116,2.70309000012439e-08*Z);
    Increment_isotopic_property(X,XSn117,6.56396177551215e-08*Z);
    Increment_isotopic_property(X,XSn118,2.0893060664827e-07*Z);
    Increment_isotopic_property(X,XSn119,7.46845125034369e-08*Z);
    Increment_isotopic_property(X,XTe120,1.08092268912537e-09*Z);
    Increment_isotopic_property(X,XSn120,2.85904051273661e-07*Z);
    Increment_isotopic_property(X,XSb121,4.46160630902885e-08*Z);
    Increment_isotopic_property(X,XTe122,2.99341173543187e-08*Z);
    Increment_isotopic_property(X,XSn122,4.12818473968578e-08*Z);
    Increment_isotopic_property(X,XSb123,3.39368602326542e-08*Z);
    Increment_isotopic_property(X,XTe123,1.05254846853583e-08*Z);
    Increment_isotopic_property(X,XSn124,5.24725303385492e-08*Z);
    Increment_isotopic_property(X,XXe124,1.68514280680024e-09*Z);
    Increment_isotopic_property(X,XTe124,5.63090226076333e-08*Z);
    Increment_isotopic_property(X,XTe125,8.41288786803421e-08*Z);
    Increment_isotopic_property(X,XXe126,1.48532876477424e-09*Z);
    Increment_isotopic_property(X,XTe126,2.25143272069184e-07*Z);
    Increment_isotopic_property(X,XI127,2.48068819864357e-07*Z);
    Increment_isotopic_property(X,XTe128,3.8248997648819e-07*Z);
    Increment_isotopic_property(X,XXe128,3.007784874088e-08*Z);
    Increment_isotopic_property(X,XXe129,3.73858260521406e-07*Z);
    Increment_isotopic_property(X,XBa130,1.17099957988582e-09*Z);
    Increment_isotopic_property(X,XXe130,6.00773697506639e-08*Z);
    Increment_isotopic_property(X,XTe130,4.14177460103094e-07*Z);
    Increment_isotopic_property(X,XXe131,3.01414900223955e-07*Z);
    Increment_isotopic_property(X,XXe132,3.67302229428668e-07*Z);
    Increment_isotopic_property(X,XBa132,1.13731865551452e-09*Z);
    Increment_isotopic_property(X,XCs133,9.56074160338116e-08*Z);
    Increment_isotopic_property(X,XBa134,2.7604258992867e-08*Z);
    Increment_isotopic_property(X,XXe134,1.36709305468476e-07*Z);
    Increment_isotopic_property(X,XBa135,7.58173273144134e-08*Z);
    Increment_isotopic_property(X,XXe136,1.12917257148054e-07*Z);
    Increment_isotopic_property(X,XBa136,9.09995914327592e-08*Z);
    Increment_isotopic_property(X,XCe136,5.77902000026595e-10*Z);
    Increment_isotopic_property(X,XBa137,1.31104760489227e-07*Z);
    Increment_isotopic_property(X,XBa138,8.43119697517791e-07*Z);
    Increment_isotopic_property(X,XLa138,1.08362499584819e-10*Z);
    Increment_isotopic_property(X,XCe138,7.91775869784336e-10*Z);
    Increment_isotopic_property(X,XLa139,1.19790319732403e-07*Z);
    Increment_isotopic_property(X,XCe140,2.83468058836574e-07*Z);
    Increment_isotopic_property(X,XPr141,4.7959482292123e-08*Z);
    Increment_isotopic_property(X,XCe142,3.61204415562841e-08*Z);
    Increment_isotopic_property(X,XNd142,6.30898151247521e-08*Z);
    Increment_isotopic_property(X,XNd143,2.82015672757306e-08*Z);
    Increment_isotopic_property(X,XNd144,5.61345146889698e-08*Z);
    Increment_isotopic_property(X,XSm144,2.2022624874963e-09*Z);
    Increment_isotopic_property(X,XNd145,1.96882626689733e-08*Z);
    Increment_isotopic_property(X,XNd146,4.10089535817191e-08*Z);
    Increment_isotopic_property(X,XSm147,1.13011642064024e-08*Z);
    Increment_isotopic_property(X,XSm148,8.28864050458312e-09*Z);
    Increment_isotopic_property(X,XNd148,1.38965144124042e-08*Z);
    Increment_isotopic_property(X,XSm149,1.02411550214797e-08*Z);
    //Increment_isotopic_property(X,XKr150,1.37905761560968e-08*Z); // cannot be! what is this? Eu?
    Increment_isotopic_property(X,XSm150,5.52210504227093e-09*Z);
    Increment_isotopic_property(X,XEu151,1.34478531686861e-08*Z);
    Increment_isotopic_property(X,XGd152,1.99422403370522e-10*Z);
    Increment_isotopic_property(X,XSm152,2.02398857152171e-08*Z);
    Increment_isotopic_property(X,XEu153,1.48753173221131e-08*Z);
    Increment_isotopic_property(X,XSm154,1.74302700008021e-08*Z);
    Increment_isotopic_property(X,XGd154,2.18330717657106e-09*Z);
    Increment_isotopic_property(X,XGd155,1.49180059355605e-08*Z);
    Increment_isotopic_property(X,XGd156,2.0763349942132e-08*Z);
    Increment_isotopic_property(X,XDy156,6.59832806753054e-11*Z);
    Increment_isotopic_property(X,XGd157,1.5980541189811e-08*Z);
    Increment_isotopic_property(X,XGd158,2.5518861480166e-08*Z);
    Increment_isotopic_property(X,XDy158,1.14785373534694e-10*Z);
    Increment_isotopic_property(X,XTb159,1.83916058264766e-08*Z);
    Increment_isotopic_property(X,XDy160,2.83233075643286e-09*Z);
    Increment_isotopic_property(X,XGd160,2.27495062195343e-08*Z);
    Increment_isotopic_property(X,XDy161,2.30146455892944e-08*Z);
    Increment_isotopic_property(X,XDy162,3.12468901274884e-08*Z);
    Increment_isotopic_property(X,XEr162,1.11029558828639e-10*Z);
    Increment_isotopic_property(X,XDy163,3.07056455056147e-08*Z);
    Increment_isotopic_property(X,XEr164,1.3195794530019e-09*Z);
    Increment_isotopic_property(X,XDy164,3.49725486570716e-08*Z);
    Increment_isotopic_property(X,XHo165,2.90339359046975e-08*Z);
    Increment_isotopic_property(X,XEr166,2.79031576147295e-08*Z);
    Increment_isotopic_property(X,XEr167,1.9150190320209e-08*Z);
    Increment_isotopic_property(X,XEr168,2.25085701186829e-08*Z);
    Increment_isotopic_property(X,XYb168,1.0625940000489e-10*Z);
    Increment_isotopic_property(X,XTm169,1.22445825635887e-08*Z);
    Increment_isotopic_property(X,XEr170,1.26932046434413e-08*Z);
    Increment_isotopic_property(X,XYb170,2.51367396440139e-09*Z);
    Increment_isotopic_property(X,XYb171,1.18771667589499e-08*Z);
    Increment_isotopic_property(X,XYb172,1.82651652865548e-08*Z);
    Increment_isotopic_property(X,XYb173,1.35744112002045e-08*Z);
    Increment_isotopic_property(X,XHf174,9.36995483236397e-11*Z);
    Increment_isotopic_property(X,XYb174,2.69411755852734e-08*Z);
    Increment_isotopic_property(X,XLu175,1.19253970593723e-08*Z);
    Increment_isotopic_property(X,XHf176,3.04501561123257e-09*Z);
    Increment_isotopic_property(X,XLu176,3.47399152957164e-10*Z);
    Increment_isotopic_property(X,XYb176,1.09251519332759e-08*Z);
    Increment_isotopic_property(X,XHf177,1.09514575184872e-08*Z);
    Increment_isotopic_property(X,XHf178,1.61577733469621e-08*Z);
    Increment_isotopic_property(X,XHf179,8.11270662957502e-09*Z);
    Increment_isotopic_property(X,XW180,5.39286428596246e-11*Z);
    Increment_isotopic_property(X,XTa180,9.09384958025042e-13*Z);
    Increment_isotopic_property(X,XHf180,2.10103172782778e-08*Z);
    Increment_isotopic_property(X,XTa181,7.43848501861962e-09*Z);
    Increment_isotopic_property(X,XW182,1.20602774123197e-08*Z);
    Increment_isotopic_property(X,XW183,6.55062647929305e-09*Z);
    Increment_isotopic_property(X,XW184,1.40988349418253e-08*Z);
    Increment_isotopic_property(X,XOs184,4.79209058845582e-11*Z);
    Increment_isotopic_property(X,XRe185,7.11852211167213e-09*Z);
    Increment_isotopic_property(X,XW186,1.32213293703563e-08*Z);
    Increment_isotopic_property(X,XOs186,3.90703030732266e-09*Z);
    Increment_isotopic_property(X,XOs187,3.12426604300092e-09*Z);
    Increment_isotopic_property(X,XRe187,1.29774951434544e-08*Z);
    Increment_isotopic_property(X,XOs188,3.29573954536175e-08*Z);
    Increment_isotopic_property(X,XOs189,4.03962544871531e-08*Z);
    Increment_isotopic_property(X,XPt190,6.92025504233527e-11*Z);
    Increment_isotopic_property(X,XOs190,6.60400682803501e-08*Z);
    Increment_isotopic_property(X,XIr191,8.98757843108587e-08*Z);
    Increment_isotopic_property(X,XPt192,3.99283442035181e-09*Z);
    Increment_isotopic_property(X,XOs192,1.03648266761072e-07*Z);
    Increment_isotopic_property(X,XIr193,1.52873211876783e-07*Z);
    Increment_isotopic_property(X,XPt194,1.69947364848157e-07*Z);
    Increment_isotopic_property(X,XPt195,1.75302455585798e-07*Z);
    Increment_isotopic_property(X,XPt196,1.31465028888403e-07*Z);
    Increment_isotopic_property(X,XHg196,2.4179770589348e-10*Z);
    Increment_isotopic_property(X,XAu197,7.54168767891849e-08*Z);
    Increment_isotopic_property(X,XPt198,3.76904817622387e-08*Z);
    Increment_isotopic_property(X,XHg198,1.59353852528342e-08*Z);
    Increment_isotopic_property(X,XHg199,2.71607282155356e-08*Z);
    Increment_isotopic_property(X,XHg200,3.73231638672638e-08*Z);
    Increment_isotopic_property(X,XHg201,2.14116685724139e-08*Z);
    Increment_isotopic_property(X,XHg202,4.87719366829167e-08*Z);
    Increment_isotopic_property(X,XTl203,2.16644713245264e-08*Z);
    Increment_isotopic_property(X,XHg204,1.13050414290917e-08*Z);
    Increment_isotopic_property(X,XPb204,2.57950685583299e-08*Z);
    Increment_isotopic_property(X,XTl205,5.21858508427377e-08*Z);
    Increment_isotopic_property(X,XPb206,2.4239977199855e-07*Z);
    Increment_isotopic_property(X,XPb207,2.69542935220387e-07*Z);
    Increment_isotopic_property(X,XPb208,7.75242452304584e-07*Z);
    Increment_isotopic_property(X,XBi209,5.68056204227822e-08*Z);
    Increment_isotopic_property(X,XTh232,1.99846939673062e-08*Z);
    Increment_isotopic_property(X,XU235,2.72331813667995e-09*Z);
    Increment_isotopic_property(X,XU238,8.72119365040135e-09*Z);
#endif // NUCSYN_ALL_ISOTOPES

    // He4 is everything else
    Set_isotopic_property(X,XHe4,0.0);
    Set_isotopic_property(X,XHe4,1.0-nucsyn_totalX(X));
}


static void Amanda_quadratic_fit(Abundance * const X,
                                 const Abundance Z)
{

    /*
     * Amanda Karakas' initial abundances fitted to
     * MW/LMC/SMC abundance patterns
     *
     * These are quadratic fits to Z=0.02, 0.008 and 0.004.
     * Outside that range, please use something else.
     */

    // bit dubious scaling Li and Be with metallicity - big bang abundances
    // would be better!
    const double Z2 = Z / 0.02;


    // fits to the surffiles at time=0
    Increment_isotopic_property(X,XH1,7.57180e-01+-1.64200e+00 * Z + -8.87800e+01 * Z*Z );
    Increment_isotopic_property(X,XHe4,2.42560e-01+1.01910e+00 * Z + 7.63670e+01 * Z*Z );
    Increment_isotopic_property(X,XC12,7.66300e-05+9.12280e-02 * Z + 2.57920e+00 * Z*Z );
    Increment_isotopic_property(X,XC13,-1.58320e-08+2.06030e-03 * Z + -3.16050e-05 * Z*Z );
    Increment_isotopic_property(X,XN14,2.40370e-05+-4.47870e-03 * Z + 2.41380e+00 * Z*Z );
    Increment_isotopic_property(X,XN15,-1.86260e-09+2.07670e-04 * Z + -4.61000e-06 * Z*Z );
    Increment_isotopic_property(X,XO16,1.64800e-04+2.49820e-01 * Z + 7.52250e+00 * Z*Z );
    Increment_isotopic_property(X,XO17,-9.31320e-10+1.94280e-04 * Z + 1.49660e-06 * Z*Z );
    Increment_isotopic_property(X,XNe22,-5.77420e-08+7.28270e-03 * Z + -1.28530e-04 * Z*Z );


    /* I don't trust any of these : they have the same formulae! */
    /*
      Increment_isotopic_property(X,XO15,5.55180e-04+-1.28350e-01 * Z + 5.13610e+00 * Z*Z );
      Increment_isotopic_property(X,XN13,5.55180e-04+-1.28350e-01 * Z + 5.13610e+00 * Z*Z );
      Increment_isotopic_property(X,XLi7,5.55170e-04+-1.28340e-01 * Z + 5.13610e+00 * Z*Z );
      Increment_isotopic_property(X,XBe7,5.55180e-04+-1.28350e-01 * Z + 5.13610e+00 * Z*Z );
      Increment_isotopic_property(X,XH2,5.55180e-04+-1.28350e-01 * Z + 5.13610e+00 * Z*Z );
      Increment_isotopic_property(X,XHe3,5.55180e-04+-1.28350e-01 * Z + 5.13610e+00 * Z*Z );
      Increment_isotopic_property(X,XF17,5.55180e-04+-1.28350e-01 * Z + 5.13610e+00 * Z*Z );
    */
    Increment_isotopic_property(X,XO18,-8.38190e-09+1.08330e-03 * Z + -1.51230e-05 * Z*Z );
    Increment_isotopic_property(X,XF19,0.00000e+00+2.32280e-05 * Z + 3.82770e-07 * Z*Z );
    Increment_isotopic_property(X,XNe20,-7.24570e-07+9.06830e-02 * Z + -1.64500e-03 * Z*Z );
    Increment_isotopic_property(X,XNe21,-1.86260e-09+2.31730e-04 * Z + -4.35580e-06 * Z*Z );
    Increment_isotopic_property(X,XNa23,-1.49010e-08+1.91490e-03 * Z + -3.03980e-05 * Z*Z );
    Increment_isotopic_property(X,XMg24,-2.35620e-07+2.96120e-02 * Z + -5.32680e-04 * Z*Z );
    Increment_isotopic_property(X,XMg25,-3.07340e-08+3.88700e-03 * Z + -6.66250e-05 * Z*Z );
    Increment_isotopic_property(X,XMg26,-3.53900e-08+4.45980e-03 * Z + -7.48900e-05 * Z*Z );
    Increment_isotopic_property(X,XAl27,-2.60770e-08+3.33130e-03 * Z + -5.63390e-05 * Z*Z );
    Increment_isotopic_property(X,XSi28,-2.98950e-07+3.74970e-02 * Z + -6.78100e-04 * Z*Z );
    Increment_isotopic_property(X,XSi29,-1.58320e-08+1.96360e-03 * Z + -3.54220e-05 * Z*Z );
    Increment_isotopic_property(X,XSi30,-1.02450e-08+1.35110e-03 * Z + -2.05750e-05 * Z*Z );
    Increment_isotopic_property(X,XFe56,-5.35510e-07+6.71300e-02 * Z + -1.21450e-03 * Z*Z );
    Set_isotopic_property(X,XAl26,0.0);
    Increment_isotopic_property(X,XLi6,Z2*6.5e-10);
    Increment_isotopic_property(X,XLi7,Z2*9.45e-9);
    Increment_isotopic_property(X,XBe9,Z2*1.66e-10);
    Increment_isotopic_property(X,XB10,Z2*1.07e-9);
    Increment_isotopic_property(X,XB11,Z*  2.526003e-07);

#ifdef NUCSYN_ALL_ISOTOPES
    Increment_isotopic_property(X,XP31,-3.72530e-09+4.68000e-04 * Z + -7.97580e-06 * Z*Z );
    Increment_isotopic_property(X,XS32,-1.81610e-07+2.27910e-02 * Z + -4.10490e-04 * Z*Z );
    Increment_isotopic_property(X,XS33,-9.31320e-10+1.85270e-04 * Z + 7.03150e-07 * Z*Z );
    Increment_isotopic_property(X,XS34,-8.38190e-09+1.07090e-03 * Z + -1.91220e-05 * Z*Z );
    Increment_isotopic_property(X,XFe57,-1.30390e-08+1.63810e-03 * Z + -2.55330e-05 * Z*Z );
    Increment_isotopic_property(X,XFe58,-1.86260e-09+2.12380e-04 * Z + -4.53550e-06 * Z*Z );
    Increment_isotopic_property(X,XCo59,-9.31320e-10+1.92810e-04 * Z + 1.65120e-06 * Z*Z );
    Increment_isotopic_property(X,XNi58,-2.23520e-08+2.83920e-03 * Z + -4.69160e-05 * Z*Z );
    Increment_isotopic_property(X,XNi60,-8.38190e-09+1.12540e-03 * Z + -1.54410e-05 * Z*Z );
    Increment_isotopic_property(X,XNi62,-9.31320e-10+1.59490e-04 * Z + -1.51810e-06 * Z*Z );
    Increment_isotopic_property(X,XCl35,Z*  1.353617e-04);
    Increment_isotopic_property(X,XCl37,Z*  4.568189e-05);
    Increment_isotopic_property(X,XAr36,Z*  4.137833e-03);
    Increment_isotopic_property(X,XK39,Z*  1.854540e-04);
    Increment_isotopic_property(X,XCa40,Z*  3.202276e-03);
    Increment_isotopic_property(X,XCa44,Z*  7.586027e-05);
    Increment_isotopic_property(X,XSc45,Z*  2.081212e-06);
    Increment_isotopic_property(X,XTi48,Z*  1.149398e-04);
    Increment_isotopic_property(X,XTi50,Z*  8.788885e-06);
    Increment_isotopic_property(X,XV51,Z*  2.013852e-05);
    Increment_isotopic_property(X,XCr52,Z*  7.954903e-04);
    Increment_isotopic_property(X,XMn55,Z*  7.104883e-04);
    Increment_isotopic_property(X,XFe54,Z*  3.811724e-03);
    Increment_isotopic_property(X,XCu65,Z*  1.415096e-05);
    Increment_isotopic_property(X,XZn64,Z*  5.305407e-05);


    Increment_isotopic_property(X,XS30,Z2*2.35e-5);
    Increment_isotopic_property(X,XS36,Z2*9.38e-8);
    Increment_isotopic_property(X,XAr38,Z2*1.54e-5);
    Increment_isotopic_property(X,XAr40,Z2*2.53e-8);
    Increment_isotopic_property(X,XK40,Z2*5.54e-9);
    Increment_isotopic_property(X,XK41,Z2*2.63e-7);
    Increment_isotopic_property(X,XCa42,Z2*4.2e-7);
    Increment_isotopic_property(X,XCa43,Z2*8.97e-8);
    Increment_isotopic_property(X,XCa46,Z2*2.79e-9);
    Increment_isotopic_property(X,XCa48,Z2*1.38e-7);
    Increment_isotopic_property(X,XTi46,Z2*2.23e-7);
    Increment_isotopic_property(X,XTi47,Z2*2.08e-7);
    Increment_isotopic_property(X,XTi49,Z2*1.64e-7);
    Increment_isotopic_property(X,XV50,Z2*3.77e-7);
    Increment_isotopic_property(X,XCr50,Z2*7.42e-7);
    Increment_isotopic_property(X,XCr53,Z2*1.72e-6);
    Increment_isotopic_property(X,XCr54,Z2*4.36e-7);
    Increment_isotopic_property(X,XNi64,Z2*7.27e-7);
    Increment_isotopic_property(X,XCu63,Z2*5.75e-7);
    Increment_isotopic_property(X,XZn66,Z2*5.88e-7);
    Increment_isotopic_property(X,XZn67,Z2*8.76e-8);
    Increment_isotopic_property(X,XZn68,Z2*4.06e-7);
    Increment_isotopic_property(X,XZn70,Z2*1.38e-8);
    Increment_isotopic_property(X,XGa69,Z2*3.96e-8);
    Increment_isotopic_property(X,XGa71,Z2*2.71e-8);
    Increment_isotopic_property(X,XGe70,Z2*4.32e-8);
#endif // NUCSYN_ALL_ISOTOPES
    // deuterium - special case that doesn't scale with the metallicity
    Increment_isotopic_property(X,XH2,X[XH1]*6.8e-5);
    Increment_isotopic_property(X,XH1,-X[XH2]);

}

static void Anders_Grevesse_1989_mix(Abundance * const X,
                                     const Abundance Z)
{
    /* Use scaled Anders and Grevesse 1989 (meteorite) values */
    Set_isotopic_property(X,XH1,0.76-3.0*Z);
    Set_isotopic_property(X,XHe4,1.0-X[XH1]-Z);

    Set_isotopic_property(X,XC12,Z*  1.620918e-01);
    Set_isotopic_property(X,XC13,Z*  1.951304e-03);// *2.0 to match MM94 models
    Set_isotopic_property(X,XN14,Z*  5.907371e-02);
    Set_isotopic_property(X,XN15,Z*  2.332476e-04);
    Set_isotopic_property(X,XO16,Z*  5.127919e-01);
    Set_isotopic_property(X,XO17,Z*  2.078005e-04);
    Set_isotopic_property(X,XNe22,Z*  6.960540e-03);
    Set_isotopic_property(X,XF19,Z*  2.165680e-05);
    Set_isotopic_property(X,XHe3,(0.24+2.0*Z)*  1.064204e-04);
    Set_isotopic_property(X,XB11,Z*  2.526003e-07);
    Set_isotopic_property(X,XMg24,Z*  2.752140e-02);
    Set_isotopic_property(X,XNe20,Z*  8.655234e-02);
    Set_isotopic_property(X,XMg26,Z*  4.148525e-03);
    Set_isotopic_property(X,XNa23,Z*  1.785042e-03);
    Set_isotopic_property(X,XAl27,Z*  3.099632e-03);
    Set_isotopic_property(X,XAl26,0.0);
    Set_isotopic_property(X,XSi28,Z*  3.490962e-02);
    Set_isotopic_property(X,XSi30,Z*  1.257388e-03);
    Set_isotopic_property(X,XFe56,Z*  6.249517e-02);
#ifdef NUCSYN_ALL_ISOTOPES
    Set_isotopic_property(X,XP31,Z*  4.359693e-04);
    Set_isotopic_property(X,XS32,Z*  2.115961e-02);
    Set_isotopic_property(X,XCl35,Z*  1.353617e-04);
    Set_isotopic_property(X,XCl37,Z*  4.568189e-05);
    Set_isotopic_property(X,XAr36,Z*  4.137833e-03);
    Set_isotopic_property(X,XK39,Z*  1.854540e-04);
    Set_isotopic_property(X,XCa40,Z*  3.202276e-03);
    Set_isotopic_property(X,XCa44,Z*  7.586027e-05);
    Set_isotopic_property(X,XSc45,Z*  2.081212e-06);
    Set_isotopic_property(X,XTi48,Z*  1.149398e-04);
    Set_isotopic_property(X,XTi50,Z*  8.788885e-06);
    Set_isotopic_property(X,XV51,Z*  2.013852e-05);
    Set_isotopic_property(X,XCr52,Z*  7.954903e-04);
    Set_isotopic_property(X,XMn55,Z*  7.104883e-04);
    Set_isotopic_property(X,XFe54,Z*  3.811724e-03);
    Set_isotopic_property(X,XFe57,Z*  1.526294e-03);
    Set_isotopic_property(X,XCo59,Z*  1.795199e-04);
    Set_isotopic_property(X,XNi58,Z*  2.643081e-03);
    Set_isotopic_property(X,XNi60,Z*  1.046754e-03);
    Set_isotopic_property(X,XCu65,Z*  1.415096e-05);
    Set_isotopic_property(X,XZn64,Z*  5.305407e-05);
    Set_isotopic_property(X,XO18,Z*  1.158486e-03);
    Set_isotopic_property(X,XLi6,Z*  3.472786e-08);
    Set_isotopic_property(X,XLi7,Z*  5.000148e-07);
    Set_isotopic_property(X,XBe9,Z*  8.885114e-09);
    Set_isotopic_property(X,XB10,Z*  5.704222e-08);
    Set_isotopic_property(X,XNe21,Z*  2.206310e-04);
    Set_isotopic_property(X,XMg25,Z*  3.617129e-03);
    Set_isotopic_property(X,XSi29,Z*  1.831552e-03);
    Set_isotopic_property(X,XS33,Z*  1.722493e-04);
    Set_isotopic_property(X,XS34,Z*  9.975705e-04);
    Set_isotopic_property(X,XS36,Z*  5.014048e-06);
    Set_isotopic_property(X,XAr38,Z*  8.222205e-04);
    Set_isotopic_property(X,XAr40,Z*  1.352013e-06);
    Set_isotopic_property(X,XK40,Z*  2.964378e-07);
    Set_isotopic_property(X,XK41,Z*  1.406542e-05);
    Set_isotopic_property(X,XCa42,Z*  2.243197e-05);
    Set_isotopic_property(X,XCa43,Z*  4.796999e-06);
    Set_isotopic_property(X,XCa46,Z*  1.493148e-07);
    Set_isotopic_property(X,XCa48,Z*  7.398915e-06);
    Set_isotopic_property(X,XTi46,Z*  1.194305e-05);
    Set_isotopic_property(X,XTi47,Z*  1.112510e-05);
    Set_isotopic_property(X,XTi49,Z*  8.746117e-06);
    Set_isotopic_property(X,XV50,Z*  4.949361e-08);
    Set_isotopic_property(X,XCr50,Z*  3.968898e-05);
    Set_isotopic_property(X,XCr53,Z*  9.195184e-05);
    Set_isotopic_property(X,XCr54,Z*  2.329268e-05);
    Set_isotopic_property(X,XFe58,Z*  1.976430e-04);
    Set_isotopic_property(X,XNi62,Z*  1.484060e-04);
    Set_isotopic_property(X,XNi64,Z*  3.886034e-05);
    Set_isotopic_property(X,XCu63,Z*  3.075575e-05);
    Set_isotopic_property(X,XZn66,Z*  3.141866e-05);
    Set_isotopic_property(X,XZn67,Z*  4.684198e-06);
    Set_isotopic_property(X,XZn68,Z*  2.169956e-05);
    Set_isotopic_property(X,XZn70,Z*  7.382877e-07);
    Set_isotopic_property(X,XGa69,Z*  2.118100e-06);
    Set_isotopic_property(X,XGa71,Z*  1.449845e-06);
    Set_isotopic_property(X,XGe70,Z*  2.309488e-06);
    Set_isotopic_property(X,XRu99,Z*  3.159508e-08);
#endif
}



static void Garcia_Berro_changes(Abundance * const X)
{
    Set_isotopic_property(X,XH1,0.706);
    Set_isotopic_property(X,XHe3,4.11e-6);
    Set_isotopic_property(X,XHe4,0.282);
    Set_isotopic_property(X,XC12,0.00284);
    Set_isotopic_property(X,XN14,0.00093);
    Set_isotopic_property(X,XO16,0.00847);
}

static void Grevesse_Noels_changes(Abundance * const X,
                                   const Abundance Z)
{

    /* with some changes */
    double f13=X[XC13]/X[XC12];//use previous C12/C13 ratio

    Set_isotopic_property(X,XC12,0.173285*Z);
    Set_isotopic_property(X,XC13,f13*X[XC12]);
    Set_isotopic_property(X,XN14,0.053152*Z);
    Set_isotopic_property(X,XO16,0.482273*Z);

    /* approximate the minor isotopes with the previous abundance ratio */

    double f1=X[XNe21]/X[XNe20];
    double f2=X[XNe21]/X[XNe20];

    Set_isotopic_property(X,XNe20,0.098668*Z);
    Set_isotopic_property(X,XNe21,X[XNe20]*f1);
    Set_isotopic_property(X,XNe22,X[XNe20]*f2);

    Set_isotopic_property(X,XNa23,0.001999*Z);

    f1=X[XMg25]/X[XMg24];
    f2=X[XMg26]/X[XMg24];
    Set_isotopic_property(X,XMg24,0.037573*Z);
    Set_isotopic_property(X,XMg25,f1*X[XMg24]);
    Set_isotopic_property(X,XMg26,f2*X[XMg26]);

    Set_isotopic_property(X,XAl27,0.003238*Z);

    f1=X[XSi29]/X[XSi28];
    f2=X[XSi30]/X[XSi28];
    Set_isotopic_property(X,XSi28,0.040520*Z);
    Set_isotopic_property(X,XSi29,f1*X[XSi28]);
    Set_isotopic_property(X,XSi30,f2*X[XSi30]);
}

static void Asplund2009_mix(Abundance * const X,
                            const Abundance Z,
                            struct stardata_t * Restrict const stardata Maybe_unused)
{
    /* CAB: this mix is based on Asplund et al. (2009)
     * The table of NUMBER FRACTIONS was provided by Amanda in Sep. 2013.
     * Here MASS FRACTIONS are given. Z_solar is 0.0143.
     * Th232, U235 and U238 are from Lodders03
     */
    Increment_isotopic_property(X,XH1,0.758-3.0*Z);
    Increment_isotopic_property(X,XH2,3.43971830985915e-03*Z);
    Increment_isotopic_property(X,XHe3,2.18788732394366e-02*Z);
    Increment_isotopic_property(X,XLi6,4.27732394366197e-08*Z);
    Increment_isotopic_property(X,XLi7,6.07570422535211e-07*Z);
    Increment_isotopic_property(X,XBe9,1.11435211267606e-08*Z);
    Increment_isotopic_property(X,XB10,6.33345070422535e-08*Z);
    Increment_isotopic_property(X,XB11,2.80422535211268e-07*Z);
    Increment_isotopic_property(X,XC12,1.80853521126761e-01*Z);
    Increment_isotopic_property(X,XC13,2.19059154929577e-03*Z);
    Increment_isotopic_property(X,XN14,5.34464788732394e-02*Z);
    Increment_isotopic_property(X,XN15,1.27933098591549e-04*Z);
    Increment_isotopic_property(X,XO16,4.42456338028169e-01*Z);
    Increment_isotopic_property(X,XO17,1.78595774647887e-04*Z);
    Increment_isotopic_property(X,XO18,9.97897183098591e-04*Z);
    Increment_isotopic_property(X,XF19,2.57958450704225e-05*Z);
    Increment_isotopic_property(X,XNe20,8.95436619718310e-02*Z);
    Increment_isotopic_property(X,XNe21,2.25380281690141e-04*Z);
    Increment_isotopic_property(X,XNe22,7.24249295774648e-03*Z);
    Increment_isotopic_property(X,XNa23,2.06303521126761e-03*Z);
    Increment_isotopic_property(X,XMg24,4.27132394366197e-02*Z);
    Increment_isotopic_property(X,XMg25,5.63274647887324e-03*Z);
    Increment_isotopic_property(X,XMg26,6.44983098591549e-03*Z);
    Increment_isotopic_property(X,XAl27,3.92773943661972e-03*Z);
    Increment_isotopic_property(X,XSi28,4.72943661971831e-02*Z);
    Increment_isotopic_property(X,XSi29,2.48726056338028e-03*Z);
    Increment_isotopic_property(X,XSi30,1.69618309859155e-03*Z);
    Increment_isotopic_property(X,XFe56,9.19583098591549e-02*Z);
#ifdef NUCSYN_ALL_ISOTOPES
    Increment_isotopic_property(X,XP31,4.11295774647887e-04*Z);
    Increment_isotopic_property(X,XS32,2.26636619718310e-02*Z);
    Increment_isotopic_property(X,XS33,1.87116971830986e-04*Z);
    Increment_isotopic_property(X,XS34,1.08823943661972e-03*Z);
    Increment_isotopic_property(X,XS36,5.37185915492958e-06*Z);
    Increment_isotopic_property(X,XCl35,2.32493661971831e-04*Z);
    Increment_isotopic_property(X,XCl37,7.85520422535211e-05*Z);
    Increment_isotopic_property(X,XAr36,4.32938028169014e-03*Z);
    Increment_isotopic_property(X,XAr38,8.30888732394366e-04*Z);
    Increment_isotopic_property(X,XAr40,1.39887323943662e-06*Z);
    Increment_isotopic_property(X,XK39,2.25398028169014e-04*Z);
    Increment_isotopic_property(X,XK40,3.64901408450704e-08*Z);
    Increment_isotopic_property(X,XK41,1.71001760563380e-05*Z);
    Increment_isotopic_property(X,XCa40,3.90253521126761e-03*Z);
    Increment_isotopic_property(X,XCa42,2.73488028169014e-05*Z);
    Increment_isotopic_property(X,XCa43,5.84224647887324e-06*Z);
    Increment_isotopic_property(X,XCa44,9.23752112676056e-05*Z);
    Increment_isotopic_property(X,XCa46,1.85185633802817e-07*Z);
    Increment_isotopic_property(X,XCa48,9.03380281690141e-06*Z);
    Increment_isotopic_property(X,XSc45,2.60613380281690e-06*Z);
    Increment_isotopic_property(X,XTi46,1.59221549295775e-05*Z);
    Increment_isotopic_property(X,XTi47,1.46709507042254e-05*Z);
    Increment_isotopic_property(X,XTi48,1.48461971830986e-04*Z);
    Increment_isotopic_property(X,XTi49,1.11219647887324e-05*Z);
    Increment_isotopic_property(X,XTi50,1.08665492957746e-05*Z);
    Increment_isotopic_property(X,XV50,5.88415492957746e-08*Z);
    Increment_isotopic_property(X,XV51,2.39480915492958e-05*Z);
    Increment_isotopic_property(X,XCr50,4.89507042253521e-05*Z);
    Increment_isotopic_property(X,XCr52,9.81701408450704e-04*Z);
    Increment_isotopic_property(X,XCr53,1.13457323943662e-04*Z);
    Increment_isotopic_property(X,XCr54,2.87747746478873e-05*Z);
    Increment_isotopic_property(X,XMn55,8.57341549295775e-04*Z);
    Increment_isotopic_property(X,XFe54,5.64908450704225e-03*Z);
    Increment_isotopic_property(X,XFe57,2.16166478873239e-03*Z);
    Increment_isotopic_property(X,XFe58,2.92728450704225e-04*Z);
    Increment_isotopic_property(X,XCo59,2.25753943661972e-04*Z);
    Increment_isotopic_property(X,XNi58,3.23006901408451e-03*Z);
    Increment_isotopic_property(X,XNi60,1.28712676056338e-03*Z);
    Increment_isotopic_property(X,XNi61,5.68846478873239e-05*Z);
    Increment_isotopic_property(X,XNi62,1.84340845070423e-04*Z);
    Increment_isotopic_property(X,XNi64,4.84597183098592e-05*Z);
    Increment_isotopic_property(X,XCu63,3.99987887323944e-05*Z);
    Increment_isotopic_property(X,XCu65,1.83940845070423e-05*Z);
    Increment_isotopic_property(X,XZn64,6.85295774647887e-05*Z);
    Increment_isotopic_property(X,XZn66,4.05449154929577e-05*Z);
    Increment_isotopic_property(X,XZn67,6.04840140845070e-06*Z);
    Increment_isotopic_property(X,XZn68,2.80734647887324e-05*Z);
    Increment_isotopic_property(X,XZn70,9.55598591549296e-07*Z);
    Increment_isotopic_property(X,XGa69,2.57374859154930e-06*Z);
    Increment_isotopic_property(X,XGa71,1.75765000000000e-06*Z);
    Increment_isotopic_property(X,XGe70,2.86275352112676e-06*Z);
    Increment_isotopic_property(X,XGe72,3.89119436619718e-06*Z);
    Increment_isotopic_property(X,XGe73,1.10733802816901e-06*Z);
    Increment_isotopic_property(X,XGe74,5.26859154929578e-06*Z);
    Increment_isotopic_property(X,XGe76,1.13496901408451e-06*Z);
    Increment_isotopic_property(X,XAs75,7.72394366197183e-07*Z);
    Increment_isotopic_property(X,XSe74,7.43700000000000e-08*Z);
    Increment_isotopic_property(X,XSe76,8.04154929577465e-07*Z);
    Increment_isotopic_property(X,XSe77,6.63447183098591e-07*Z);
    Increment_isotopic_property(X,XSe78,2.09369577464789e-06*Z);
    Increment_isotopic_property(X,XSe80,4.48174647887324e-06*Z);
    Increment_isotopic_property(X,XSe82,8.08392957746479e-07*Z);
    Increment_isotopic_property(X,XBr79,7.16674647887324e-07*Z);
    Increment_isotopic_property(X,XBr81,7.14853521126761e-07*Z);
    Increment_isotopic_property(X,XKr78,2.59174225352113e-08*Z);
    Increment_isotopic_property(X,XKr80,1.70800000000000e-07*Z);
    Increment_isotopic_property(X,XKr82,8.77226760563380e-07*Z);
    Increment_isotopic_property(X,XKr83,8.79624647887324e-07*Z);
    Increment_isotopic_property(X,XKr84,4.38734366197183e-06*Z);
    Increment_isotopic_property(X,XKr86,1.35837605633803e-06*Z);
    Increment_isotopic_property(X,XRb85,1.02921830985915e-06*Z);
    Increment_isotopic_property(X,XRb87,4.33541830985915e-07*Z);
    Increment_isotopic_property(X,XSr84,1.83528169014084e-08*Z);
    Increment_isotopic_property(X,XSr86,3.32280985915493e-07*Z);
    Increment_isotopic_property(X,XSr87,2.34912253521127e-07*Z);
    Increment_isotopic_property(X,XSr88,2.84878309859155e-06*Z);
    Increment_isotopic_property(X,XY89,7.45030281690141e-07*Z);
    Increment_isotopic_property(X,XZr90,8.09873239436620e-07*Z);
    Increment_isotopic_property(X,XZr91,1.78577887323944e-07*Z);
    Increment_isotopic_property(X,XZr92,2.75954647887324e-07*Z);
    Increment_isotopic_property(X,XZr94,2.85733521126761e-07*Z);
    Increment_isotopic_property(X,XZr96,4.70129577464789e-08*Z);
    Increment_isotopic_property(X,XNb93,1.23388732394366e-07*Z);
    Increment_isotopic_property(X,XMo92,6.00747042253521e-08*Z);
    Increment_isotopic_property(X,XMo94,3.86710704225352e-08*Z);
    Increment_isotopic_property(X,XMo95,6.76440140845070e-08*Z);
    Increment_isotopic_property(X,XMo96,7.19526760563380e-08*Z);
    Increment_isotopic_property(X,XMo97,4.18589154929578e-08*Z);
    Increment_isotopic_property(X,XMo98,1.07461830985915e-07*Z);
    Increment_isotopic_property(X,XMo100,4.41647887323944e-08*Z);
    Increment_isotopic_property(X,XRu96,1.57967323943662e-08*Z);
    Increment_isotopic_property(X,XRu98,5.44320985915493e-09*Z);
    Increment_isotopic_property(X,XRu99,3.75210000000000e-08*Z);
    Increment_isotopic_property(X,XRu100,3.74246478873239e-08*Z);
    Increment_isotopic_property(X,XRu101,5.11785492957746e-08*Z);
    Increment_isotopic_property(X,XRu102,9.55854929577465e-08*Z);
    Increment_isotopic_property(X,XRu104,5.75171267605634e-08*Z);
    Increment_isotopic_property(X,XRh103,6.10412816901408e-08*Z);
    Increment_isotopic_property(X,XPd102,2.39872394366197e-09*Z);
    Increment_isotopic_property(X,XPd104,2.67118873239437e-08*Z);
    Increment_isotopic_property(X,XPd105,5.40587323943662e-08*Z);
    Increment_isotopic_property(X,XPd106,6.67934366197183e-08*Z);
    Increment_isotopic_property(X,XPd108,6.58868450704225e-08*Z);
    Increment_isotopic_property(X,XPd110,2.99942323943662e-08*Z);
    Increment_isotopic_property(X,XAg107,4.53762887323944e-08*Z);
    Increment_isotopic_property(X,XAg109,4.29444647887324e-08*Z);
    Increment_isotopic_property(X,XCd106,3.50755492957746e-09*Z);
    Increment_isotopic_property(X,XCd108,2.54446478873239e-09*Z);
    Increment_isotopic_property(X,XCd110,3.63697183098592e-08*Z);
    Increment_isotopic_property(X,XCd111,3.76110211267606e-08*Z);
    Increment_isotopic_property(X,XCd112,7.15419718309859e-08*Z);
    Increment_isotopic_property(X,XCd113,3.65539084507042e-08*Z);
    Increment_isotopic_property(X,XCd114,8.67042253521127e-08*Z);
    Increment_isotopic_property(X,XCd116,2.29998591549296e-08*Z);
    Increment_isotopic_property(X,XIn113,1.43987464788732e-09*Z);
    Increment_isotopic_property(X,XIn115,3.26923943661972e-08*Z);
    Increment_isotopic_property(X,XSn112,6.58836056338028e-09*Z);
    Increment_isotopic_property(X,XSn114,4.56280985915493e-09*Z);
    Increment_isotopic_property(X,XSn115,2.37118661971831e-09*Z);
    Increment_isotopic_property(X,XSn116,1.02284225352113e-07*Z);
    Increment_isotopic_property(X,XSn117,5.44923380281690e-08*Z);
    Increment_isotopic_property(X,XSn118,1.66163943661972e-07*Z);
    Increment_isotopic_property(X,XSn119,6.19906197183099e-08*Z);
    Increment_isotopic_property(X,XSn120,2.37092957746479e-07*Z);
    Increment_isotopic_property(X,XSn122,3.42553661971831e-08*Z);
    Increment_isotopic_property(X,XSn124,4.35397183098592e-08*Z);
    Increment_isotopic_property(X,XSb121,3.65633028169014e-08*Z);
    Increment_isotopic_property(X,XSb123,2.77997323943662e-08*Z);
    Increment_isotopic_property(X,XTe120,8.43743661971831e-10*Z);
    Increment_isotopic_property(X,XTe122,2.43046338028169e-08*Z);
    Increment_isotopic_property(X,XTe123,8.55231126760563e-09*Z);
    Increment_isotopic_property(X,XTe124,4.59184225352113e-08*Z);
    Increment_isotopic_property(X,XTe125,6.90422535211267e-08*Z);
    Increment_isotopic_property(X,XTe126,1.85450704225352e-07*Z);
    Increment_isotopic_property(X,XTe128,3.17394929577465e-07*Z);
    Increment_isotopic_property(X,XTe130,3.46120422535211e-07*Z);
    Increment_isotopic_property(X,XI127,2.32588873239437e-07*Z);
    Increment_isotopic_property(X,XXe124,1.35692676056338e-09*Z);
    Increment_isotopic_property(X,XXe126,1.22060281690141e-09*Z);
    Increment_isotopic_property(X,XXe128,2.51213521126761e-08*Z);
    Increment_isotopic_property(X,XXe129,3.15368661971831e-07*Z);
    Increment_isotopic_property(X,XXe130,5.10277464788732e-08*Z);
    Increment_isotopic_property(X,XXe131,2.54905704225352e-07*Z);
    Increment_isotopic_property(X,XXe132,3.13927605633803e-07*Z);
    Increment_isotopic_property(X,XXe134,1.17674647887324e-07*Z);
    Increment_isotopic_property(X,XXe136,9.70292957746479e-08*Z);
    Increment_isotopic_property(X,XCs133,8.25349295774648e-08*Z);
    Increment_isotopic_property(X,XBa130,1.07652816901408e-09*Z);
    Increment_isotopic_property(X,XBa132,1.04159154929577e-09*Z);
    Increment_isotopic_property(X,XBa134,2.53024084507042e-08*Z);
    Increment_isotopic_property(X,XBa135,6.95240492957747e-08*Z);
    Increment_isotopic_property(X,XBa136,8.34484507042253e-08*Z);
    Increment_isotopic_property(X,XBa137,1.20212676056338e-07*Z);
    Increment_isotopic_property(X,XBa138,7.72984647887324e-07*Z);
    Increment_isotopic_property(X,XLa138,9.58750140845070e-11*Z);
    Increment_isotopic_property(X,XLa139,1.06021760563380e-07*Z);
    Increment_isotopic_property(X,XCe136,4.93737464788732e-10*Z);
    Increment_isotopic_property(X,XCe138,6.79737464788732e-10*Z);
    Increment_isotopic_property(X,XCe140,2.42998591549296e-07*Z);
    Increment_isotopic_property(X,XCe142,3.09700000000000e-08*Z);
    Increment_isotopic_property(X,XPr141,4.18799788732394e-08*Z);
    Increment_isotopic_property(X,XNd142,5.58660000000000e-08*Z);
    Increment_isotopic_property(X,XNd143,2.50109014084507e-08*Z);
    Increment_isotopic_property(X,XNd144,4.97083943661972e-08*Z);
    Increment_isotopic_property(X,XNd145,1.84844366197183e-08*Z);
    Increment_isotopic_property(X,XNd146,3.63827887323944e-08*Z);
    Increment_isotopic_property(X,XNd148,1.23069295774648e-08*Z);
    Increment_isotopic_property(X,XNd150,1.22112676056338e-08*Z);
    Increment_isotopic_property(X,XSm144,1.98740281690141e-09*Z);
    Increment_isotopic_property(X,XSm147,9.90614366197183e-09*Z);
    Increment_isotopic_property(X,XSm148,7.47848169014085e-09*Z);
    Increment_isotopic_property(X,XSm149,9.25720211267606e-09*Z);
    Increment_isotopic_property(X,XSm150,4.97661971830986e-09*Z);
    Increment_isotopic_property(X,XSm152,1.82785352112676e-08*Z);
    Increment_isotopic_property(X,XSm154,1.57502957746479e-08*Z);
    Increment_isotopic_property(X,XEu151,1.20576690140845e-08*Z);
    Increment_isotopic_property(X,XEu153,1.33368591549296e-08*Z);
    Increment_isotopic_property(X,XGd152,1.76063098591549e-10*Z);
    Increment_isotopic_property(X,XGd154,1.94430422535211e-09*Z);
    Increment_isotopic_property(X,XGd155,1.32852464788732e-08*Z);
    Increment_isotopic_property(X,XGd156,1.84936901408451e-08*Z);
    Increment_isotopic_property(X,XGd157,1.42295070422535e-08*Z);
    Increment_isotopic_property(X,XGd158,2.27297464788732e-08*Z);
    Increment_isotopic_property(X,XGd160,2.02557746478873e-08*Z);
    Increment_isotopic_property(X,XTb159,1.71462464788732e-08*Z);
    Increment_isotopic_property(X,XDy156,6.08279154929577e-11*Z);
    Increment_isotopic_property(X,XDy158,1.04512549295775e-10*Z);
    Increment_isotopic_property(X,XDy160,2.59459154929577e-09*Z);
    Increment_isotopic_property(X,XDy161,2.11749014084507e-08*Z);
    Increment_isotopic_property(X,XDy162,2.87356056338028e-08*Z);
    Increment_isotopic_property(X,XDy163,2.82552464788732e-08*Z);
    Increment_isotopic_property(X,XDy164,3.22698873239437e-08*Z);
    Increment_isotopic_property(X,XHo165,2.51346126760563e-08*Z);
    Increment_isotopic_property(X,XEr162,9.66752112676056e-11*Z);
    Increment_isotopic_property(X,XEr164,1.12725746478873e-09*Z);
    Increment_isotopic_property(X,XEr166,2.38771126760563e-08*Z);
    Increment_isotopic_property(X,XEr167,1.63965774647887e-08*Z);
    Increment_isotopic_property(X,XEr168,1.94584225352113e-08*Z);
    Increment_isotopic_property(X,XEr170,1.08821549295775e-08*Z);
    Increment_isotopic_property(X,XTm169,1.14993788732394e-08*Z);
    Increment_isotopic_property(X,XYb168,8.65519436619718e-11*Z);
    Increment_isotopic_property(X,XYb170,2.17492253521127e-09*Z);
    Increment_isotopic_property(X,XYb171,1.03441753521127e-08*Z);
    Increment_isotopic_property(X,XYb172,1.60165915492958e-08*Z);
    Increment_isotopic_property(X,XYb173,1.19579549295775e-08*Z);
    Increment_isotopic_property(X,XYb174,2.39274507042254e-08*Z);
    Increment_isotopic_property(X,XYb176,9.82290704225352e-09*Z);
    Increment_isotopic_property(X,XLu175,1.07993485915493e-08*Z);
    Increment_isotopic_property(X,XLu176,3.15225915492958e-10*Z);
    Increment_isotopic_property(X,XHf174,7.46190422535211e-11*Z);
    Increment_isotopic_property(X,XHf176,2.42545352112676e-09*Z);
    Increment_isotopic_property(X,XHf177,8.71787323943662e-09*Z);
    Increment_isotopic_property(X,XHf178,1.28623802816901e-08*Z);
    Increment_isotopic_property(X,XHf179,6.45811830985915e-09*Z);
    Increment_isotopic_property(X,XHf180,1.67247887323944e-08*Z);
    Increment_isotopic_property(X,XTa180,8.45746478873239e-13*Z);
    Increment_isotopic_property(X,XTa181,7.08615000000000e-09*Z);
    Increment_isotopic_property(X,XW180,4.98016901408451e-12*Z);
    Increment_isotopic_property(X,XW182,1.11199436619718e-08*Z);
    Increment_isotopic_property(X,XW183,6.03771126760563e-09*Z);
    Increment_isotopic_property(X,XW184,1.29979154929577e-08*Z);
    Increment_isotopic_property(X,XW186,1.21920380281690e-08*Z);
    Increment_isotopic_property(X,XRe185,6.19671830985915e-09*Z);
    Increment_isotopic_property(X,XRe187,1.13004626760563e-08*Z);
    Increment_isotopic_property(X,XOs184,4.25234366197183e-11*Z);
    Increment_isotopic_property(X,XOs186,3.43458169014085e-09*Z);
    Increment_isotopic_property(X,XOs187,2.74639788732394e-09*Z);
    Increment_isotopic_property(X,XOs188,2.89731830985915e-08*Z);
    Increment_isotopic_property(X,XOs189,3.55133661971831e-08*Z);
    Increment_isotopic_property(X,XOs190,5.80583802816901e-08*Z);
    Increment_isotopic_property(X,XOs192,9.11202253521127e-08*Z);
    Increment_isotopic_property(X,XIr191,7.68290774647887e-08*Z);
    Increment_isotopic_property(X,XIr193,1.30500619718310e-07*Z);
    Increment_isotopic_property(X,XPt190,5.72354929577465e-11*Z);
    Increment_isotopic_property(X,XPt192,3.23073802816901e-09*Z);
    Increment_isotopic_property(X,XPt194,1.37617042253521e-07*Z);
    Increment_isotopic_property(X,XPt195,1.41951760563380e-07*Z);
    Increment_isotopic_property(X,XPt196,1.06455605633803e-07*Z);
    Increment_isotopic_property(X,XPt198,3.05170985915493e-08*Z);
    Increment_isotopic_property(X,XAu197,6.41581830985916e-08*Z);
    Increment_isotopic_property(X,XHg196,2.24461408450704e-10*Z);
    Increment_isotopic_property(X,XHg198,1.50717042253521e-08*Z);
    Increment_isotopic_property(X,XHg199,2.56303591549296e-08*Z);
    Increment_isotopic_property(X,XHg200,3.52718309859155e-08*Z);
    Increment_isotopic_property(X,XHg201,2.02259788732394e-08*Z);
    Increment_isotopic_property(X,XHg202,4.60503098591549e-08*Z);
    Increment_isotopic_property(X,XHg204,1.06998000000000e-08*Z);
    Increment_isotopic_property(X,XTl203,1.82156760563380e-08*Z);
    Increment_isotopic_property(X,XTl205,4.39118661971831e-08*Z);
    Increment_isotopic_property(X,XPb204,2.30563098591549e-08*Z);
    Increment_isotopic_property(X,XPb206,2.16648169014084e-07*Z);
    Increment_isotopic_property(X,XPb207,2.40907183098592e-07*Z);
    Increment_isotopic_property(X,XPb208,6.92874366197183e-07*Z);
    Increment_isotopic_property(X,XBi209,4.81877464788732e-08*Z);
    Increment_isotopic_property(X,XTh232,1.99846939673062e-08*Z);
    Increment_isotopic_property(X,XU235,2.72331813667995e-09*Z);
    Increment_isotopic_property(X,XU238,8.72119365040135e-09*Z);
#endif // NUCSYN_ALL_ISOTOPES

    // He4 is everything else
    Set_isotopic_property(X,XHe4,0.0);
    Set_isotopic_property(X,XHe4,1.0-nucsyn_totalX(X));

}

static void Asplund2009_Kobayashi2011_mix(Abundance * const X,
                                          const Abundance Z,
                                          struct stardata_t * Restrict const stardata Maybe_unused)
{
    /* CAB: this mix is based on the predictions of the galactic chemical
     * evolution models by Kobayashi et al. (2011), in which
     * at low metallicity alpha-elements are enhanced.
     * Isotopes of Li, Be, B and from Ge76 on are taken from Asplund et al. (2009).
     * Th232, U235 and U238 are from Lodders03
     *
     * The table of NUMBER FRACTIONS was provided by Amanda in Sep. 2013.
     * Here MASS FRACTIONS are given.
     *
     * WARNING!! These abundances have been calculated for Z=10**-4, so
     * you should use them ONLY at that metallicity!
     */
    Increment_isotopic_property(X,XH1,0.758-3.0*Z);
    Increment_isotopic_property(X,XH2,5.01960000000000e-03*Z);
    Increment_isotopic_property(X,XHe3,2.26560000000000e-01*Z);
    Increment_isotopic_property(X,XLi6,4.27732394366197e-08*Z);
    Increment_isotopic_property(X,XLi7,6.07570422535211e-07*Z);
    Increment_isotopic_property(X,XBe9,1.11435211267606e-08*Z);
    Increment_isotopic_property(X,XB10,6.33345070422535e-08*Z);
    Increment_isotopic_property(X,XB11,2.80422535211268e-07*Z);
    //Increment_isotopic_property(X,XLi6,1.36278000000000e-09*Z); // original from Kobayashi
    //Increment_isotopic_property(X,XLi7,6.36174000000000e-06*Z); // this cannot be right
    //Increment_isotopic_property(X,XBe9,1.30365000000000e-13*Z); // seems to be way too low
    //Increment_isotopic_property(X,XB10,2.23510000000000e-12*Z); // For now, to have decent
    //Increment_isotopic_property(X,XB11,1.05111600000000e-11*Z); // plot, use solar scaled values
    Increment_isotopic_property(X,XC12,1.06130400000000e-01*Z);
    Increment_isotopic_property(X,XC13,6.34426000000000e-05*Z);
    Increment_isotopic_property(X,XN14,4.64730000000000e-03*Z);
    Increment_isotopic_property(X,XN15,1.41208500000000e-06*Z);
    Increment_isotopic_property(X,XO16,8.15296000000000e-01*Z);
    Increment_isotopic_property(X,XO17,1.39527500000000e-05*Z);
    Increment_isotopic_property(X,XO18,1.98378000000000e-04*Z);
    Increment_isotopic_property(X,XF19,6.76210000000000e-07*Z);
    Increment_isotopic_property(X,XNe20,1.77102000000000e-01*Z);
    Increment_isotopic_property(X,XNe21,7.66794000000000e-05*Z);
    Increment_isotopic_property(X,XNe22,3.69292000000000e-04*Z);
    Increment_isotopic_property(X,XNa23,1.22684300000000e-03*Z);
    Increment_isotopic_property(X,XMg24,9.94032000000000e-02*Z);
    Increment_isotopic_property(X,XMg25,6.55625000000000e-04*Z);
    Increment_isotopic_property(X,XMg26,6.00106000000000e-04*Z);
    Increment_isotopic_property(X,XAl27,3.70764000000000e-03*Z);
    Increment_isotopic_property(X,XSi28,9.42648000000000e-02*Z);
    Increment_isotopic_property(X,XSi29,8.72871000000000e-04*Z);
    Increment_isotopic_property(X,XSi30,1.05531000000000e-03*Z);
    Increment_isotopic_property(X,XFe56,9.06528000000000e-02*Z);

#ifdef NUCSYN_ALL_ISOTOPES
    Increment_isotopic_property(X,XP31,3.54082000000000e-04*Z);
    Increment_isotopic_property(X,XS32,4.38880000000000e-02*Z);
    Increment_isotopic_property(X,XS33,1.77262800000000e-04*Z);
    Increment_isotopic_property(X,XS34,4.72736000000000e-04*Z);
    Increment_isotopic_property(X,XS36,2.00541600000000e-07*Z);
    Increment_isotopic_property(X,XCl35,5.32070000000000e-05*Z);
    Increment_isotopic_property(X,XCl37,1.70255500000000e-05*Z);
    Increment_isotopic_property(X,XAr36,8.42652000000000e-03*Z);
    Increment_isotopic_property(X,XAr38,1.88605400000000e-04*Z);
    Increment_isotopic_property(X,XAr40,4.58120000000000e-08*Z);
    Increment_isotopic_property(X,XK39,2.48812200000000e-05*Z);
    Increment_isotopic_property(X,XK40,7.82560000000000e-09*Z);
    Increment_isotopic_property(X,XK41,4.00762700000000e-06*Z);
    Increment_isotopic_property(X,XCa40,7.40040000000000e-03*Z);
    Increment_isotopic_property(X,XCa42,4.73886000000000e-06*Z);
    Increment_isotopic_property(X,XCa43,1.49975400000000e-07*Z);
    Increment_isotopic_property(X,XCa44,3.15018000000000e-05*Z);
    Increment_isotopic_property(X,XCa46,2.08959600000000e-08*Z);
    Increment_isotopic_property(X,XCa48,6.68352000000000e-08*Z);
    Increment_isotopic_property(X,XSc45,1.88743500000000e-07*Z);
    Increment_isotopic_property(X,XTi46,4.29451400000000e-06*Z);
    Increment_isotopic_property(X,XTi47,5.04451000000000e-06*Z);
    Increment_isotopic_property(X,XTi48,9.12960000000000e-05*Z);
    Increment_isotopic_property(X,XTi49,3.16246000000000e-06*Z);
    Increment_isotopic_property(X,XTi50,2.07490000000000e-07*Z);
    Increment_isotopic_property(X,XV50,6.30900000000000e-09*Z);
    Increment_isotopic_property(X,XV51,1.10221200000000e-05*Z);
    Increment_isotopic_property(X,XCr50,2.15715000000000e-05*Z);
    Increment_isotopic_property(X,XCr52,1.03760800000000e-03*Z);
    Increment_isotopic_property(X,XCr53,6.11408000000000e-05*Z);
    Increment_isotopic_property(X,XCr54,5.64300000000000e-07*Z);
    Increment_isotopic_property(X,XMn55,1.74465500000000e-04*Z);
    Increment_isotopic_property(X,XFe54,1.50406200000000e-03*Z);
    Increment_isotopic_property(X,XFe57,9.99039000000000e-04*Z);
    Increment_isotopic_property(X,XFe58,1.84364600000000e-05*Z);
    Increment_isotopic_property(X,XCo59,1.26614000000000e-04*Z);
    Increment_isotopic_property(X,XNi58,4.75518800000000e-04*Z);
    Increment_isotopic_property(X,XNi60,1.53918000000000e-03*Z);
    Increment_isotopic_property(X,XNi61,3.21732300000000e-05*Z);
    Increment_isotopic_property(X,XNi62,5.06626800000000e-05*Z);
    Increment_isotopic_property(X,XNi64,9.18848000000000e-06*Z);
    Increment_isotopic_property(X,XCu63,6.41907000000000e-06*Z);
    Increment_isotopic_property(X,XCu65,3.59209500000000e-06*Z);
    Increment_isotopic_property(X,XZn64,1.15174400000000e-04*Z);
    Increment_isotopic_property(X,XZn66,7.08708000000000e-06*Z);
    Increment_isotopic_property(X,XZn67,6.98475000000000e-07*Z);
    Increment_isotopic_property(X,XZn68,4.63474400000000e-06*Z);
    Increment_isotopic_property(X,XZn70,3.44106000000000e-08*Z);
    Increment_isotopic_property(X,XGa69,5.69946900000000e-07*Z);
    Increment_isotopic_property(X,XGa71,5.18236100000000e-07*Z);
    Increment_isotopic_property(X,XGe70,9.93510000000000e-07*Z);
    Increment_isotopic_property(X,XGe72,1.21723200000000e-06*Z);
    Increment_isotopic_property(X,XGe73,1.36013600000000e-07*Z);
    Increment_isotopic_property(X,XGe74,2.34461600000000e-06*Z);
    Increment_isotopic_property(X,XGe76,1.13496901408451e-06*Z);
    Increment_isotopic_property(X,XAs75,7.72394366197183e-07*Z);
    Increment_isotopic_property(X,XSe74,7.43700000000000e-08*Z);
    Increment_isotopic_property(X,XSe76,8.04154929577465e-07*Z);
    Increment_isotopic_property(X,XSe77,6.63447183098591e-07*Z);
    Increment_isotopic_property(X,XSe78,2.09369577464789e-06*Z);
    Increment_isotopic_property(X,XSe80,4.48174647887324e-06*Z);
    Increment_isotopic_property(X,XSe82,8.08392957746479e-07*Z);
    Increment_isotopic_property(X,XBr79,7.16674647887324e-07*Z);
    Increment_isotopic_property(X,XBr81,7.14853521126761e-07*Z);
    Increment_isotopic_property(X,XKr78,2.59174225352113e-08*Z);
    Increment_isotopic_property(X,XKr80,1.70800000000000e-07*Z);
    Increment_isotopic_property(X,XKr82,8.77226760563380e-07*Z);
    Increment_isotopic_property(X,XKr83,8.79624647887324e-07*Z);
    Increment_isotopic_property(X,XKr84,4.38734366197183e-06*Z);
    Increment_isotopic_property(X,XKr86,1.35837605633803e-06*Z);
    Increment_isotopic_property(X,XRb85,1.02921830985915e-06*Z);
    Increment_isotopic_property(X,XRb87,4.33541830985915e-07*Z);
    Increment_isotopic_property(X,XSr84,1.83528169014084e-08*Z);
    Increment_isotopic_property(X,XSr86,3.32280985915493e-07*Z);
    Increment_isotopic_property(X,XSr87,2.34912253521127e-07*Z);
    Increment_isotopic_property(X,XSr88,2.84878309859155e-06*Z);
    Increment_isotopic_property(X,XY89,7.45030281690141e-07*Z);
    Increment_isotopic_property(X,XZr90,8.09873239436620e-07*Z);
    Increment_isotopic_property(X,XZr91,1.78577887323944e-07*Z);
    Increment_isotopic_property(X,XZr92,2.75954647887324e-07*Z);
    Increment_isotopic_property(X,XZr94,2.85733521126761e-07*Z);
    Increment_isotopic_property(X,XZr96,4.70129577464789e-08*Z);
    Increment_isotopic_property(X,XNb93,1.23388732394366e-07*Z);
    Increment_isotopic_property(X,XMo92,6.00747042253521e-08*Z);
    Increment_isotopic_property(X,XMo94,3.86710704225352e-08*Z);
    Increment_isotopic_property(X,XMo95,6.76440140845070e-08*Z);
    Increment_isotopic_property(X,XMo96,7.19526760563380e-08*Z);
    Increment_isotopic_property(X,XMo97,4.18589154929578e-08*Z);
    Increment_isotopic_property(X,XMo98,1.07461830985915e-07*Z);
    Increment_isotopic_property(X,XMo100,4.41647887323944e-08*Z);
    Increment_isotopic_property(X,XRu96,1.57967323943662e-08*Z);
    Increment_isotopic_property(X,XRu98,5.44320985915493e-09*Z);
    Increment_isotopic_property(X,XRu99,3.75210000000000e-08*Z);
    Increment_isotopic_property(X,XRu100,3.74246478873239e-08*Z);
    Increment_isotopic_property(X,XRu101,5.11785492957746e-08*Z);
    Increment_isotopic_property(X,XRu102,9.55854929577465e-08*Z);
    Increment_isotopic_property(X,XRu104,5.75171267605634e-08*Z);
    Increment_isotopic_property(X,XRh103,6.10412816901408e-08*Z);
    Increment_isotopic_property(X,XPd102,2.39872394366197e-09*Z);
    Increment_isotopic_property(X,XPd104,2.67118873239437e-08*Z);
    Increment_isotopic_property(X,XPd105,5.40587323943662e-08*Z);
    Increment_isotopic_property(X,XPd106,6.67934366197183e-08*Z);
    Increment_isotopic_property(X,XPd108,6.58868450704225e-08*Z);
    Increment_isotopic_property(X,XPd110,2.99942323943662e-08*Z);
    Increment_isotopic_property(X,XAg107,4.53762887323944e-08*Z);
    Increment_isotopic_property(X,XAg109,4.29444647887324e-08*Z);
    Increment_isotopic_property(X,XCd106,3.50755492957746e-09*Z);
    Increment_isotopic_property(X,XCd108,2.54446478873239e-09*Z);
    Increment_isotopic_property(X,XCd110,3.63697183098592e-08*Z);
    Increment_isotopic_property(X,XCd111,3.76110211267606e-08*Z);
    Increment_isotopic_property(X,XCd112,7.15419718309859e-08*Z);
    Increment_isotopic_property(X,XCd113,3.65539084507042e-08*Z);
    Increment_isotopic_property(X,XCd114,8.67042253521127e-08*Z);
    Increment_isotopic_property(X,XCd116,2.29998591549296e-08*Z);
    Increment_isotopic_property(X,XIn113,1.43987464788732e-09*Z);
    Increment_isotopic_property(X,XIn115,3.26923943661972e-08*Z);
    Increment_isotopic_property(X,XSn112,6.58836056338028e-09*Z);
    Increment_isotopic_property(X,XSn114,4.56280985915493e-09*Z);
    Increment_isotopic_property(X,XSn115,2.37118661971831e-09*Z);
    Increment_isotopic_property(X,XSn116,1.02284225352113e-07*Z);
    Increment_isotopic_property(X,XSn117,5.44923380281690e-08*Z);
    Increment_isotopic_property(X,XSn118,1.66163943661972e-07*Z);
    Increment_isotopic_property(X,XSn119,6.19906197183099e-08*Z);
    Increment_isotopic_property(X,XSn120,2.37092957746479e-07*Z);
    Increment_isotopic_property(X,XSn122,3.42553661971831e-08*Z);
    Increment_isotopic_property(X,XSn124,4.35397183098592e-08*Z);
    Increment_isotopic_property(X,XSb121,3.65633028169014e-08*Z);
    Increment_isotopic_property(X,XSb123,2.77997323943662e-08*Z);
    Increment_isotopic_property(X,XTe120,8.43743661971831e-10*Z);
    Increment_isotopic_property(X,XTe122,2.43046338028169e-08*Z);
    Increment_isotopic_property(X,XTe123,8.55231126760563e-09*Z);
    Increment_isotopic_property(X,XTe124,4.59184225352113e-08*Z);
    Increment_isotopic_property(X,XTe125,6.90422535211267e-08*Z);
    Increment_isotopic_property(X,XTe126,1.85450704225352e-07*Z);
    Increment_isotopic_property(X,XTe128,3.17394929577465e-07*Z);
    Increment_isotopic_property(X,XTe130,3.46120422535211e-07*Z);
    Increment_isotopic_property(X,XI127,2.32588873239437e-07*Z);
    Increment_isotopic_property(X,XXe124,1.35692676056338e-09*Z);
    Increment_isotopic_property(X,XXe126,1.22060281690141e-09*Z);
    Increment_isotopic_property(X,XXe128,2.51213521126761e-08*Z);
    Increment_isotopic_property(X,XXe129,3.15368661971831e-07*Z);
    Increment_isotopic_property(X,XXe130,5.10277464788732e-08*Z);
    Increment_isotopic_property(X,XXe131,2.54905704225352e-07*Z);
    Increment_isotopic_property(X,XXe132,3.13927605633803e-07*Z);
    Increment_isotopic_property(X,XXe134,1.17674647887324e-07*Z);
    Increment_isotopic_property(X,XXe136,9.70292957746479e-08*Z);
    Increment_isotopic_property(X,XCs133,8.25349295774648e-08*Z);
    Increment_isotopic_property(X,XBa130,1.07652816901408e-09*Z);
    Increment_isotopic_property(X,XBa132,1.04159154929577e-09*Z);
    Increment_isotopic_property(X,XBa134,2.53024084507042e-08*Z);
    Increment_isotopic_property(X,XBa135,6.95240492957747e-08*Z);
    Increment_isotopic_property(X,XBa136,8.34484507042253e-08*Z);
    Increment_isotopic_property(X,XBa137,1.20212676056338e-07*Z);
    Increment_isotopic_property(X,XBa138,7.72984647887324e-07*Z);
    Increment_isotopic_property(X,XLa138,9.58750140845070e-11*Z);
    Increment_isotopic_property(X,XLa139,1.06021760563380e-07*Z);
    Increment_isotopic_property(X,XCe136,4.93737464788732e-10*Z);
    Increment_isotopic_property(X,XCe138,6.79737464788732e-10*Z);
    Increment_isotopic_property(X,XCe140,2.42998591549296e-07*Z);
    Increment_isotopic_property(X,XCe142,3.09700000000000e-08*Z);
    Increment_isotopic_property(X,XPr141,4.18799788732394e-08*Z);
    Increment_isotopic_property(X,XNd142,5.58660000000000e-08*Z);
    Increment_isotopic_property(X,XNd143,2.50109014084507e-08*Z);
    Increment_isotopic_property(X,XNd144,4.97083943661972e-08*Z);
    Increment_isotopic_property(X,XNd145,1.84844366197183e-08*Z);
    Increment_isotopic_property(X,XNd146,3.63827887323944e-08*Z);
    Increment_isotopic_property(X,XNd148,1.23069295774648e-08*Z);
    Increment_isotopic_property(X,XNd150,1.22112676056338e-08*Z);
    Increment_isotopic_property(X,XSm144,1.98740281690141e-09*Z);
    Increment_isotopic_property(X,XSm147,9.90614366197183e-09*Z);
    Increment_isotopic_property(X,XSm148,7.47848169014085e-09*Z);
    Increment_isotopic_property(X,XSm149,9.25720211267606e-09*Z);
    Increment_isotopic_property(X,XSm150,4.97661971830986e-09*Z);
    Increment_isotopic_property(X,XSm152,1.82785352112676e-08*Z);
    Increment_isotopic_property(X,XSm154,1.57502957746479e-08*Z);
    Increment_isotopic_property(X,XEu151,1.20576690140845e-08*Z);
    Increment_isotopic_property(X,XEu153,1.33368591549296e-08*Z);
    Increment_isotopic_property(X,XGd152,1.76063098591549e-10*Z);
    Increment_isotopic_property(X,XGd154,1.94430422535211e-09*Z);
    Increment_isotopic_property(X,XGd155,1.32852464788732e-08*Z);
    Increment_isotopic_property(X,XGd156,1.84936901408451e-08*Z);
    Increment_isotopic_property(X,XGd157,1.42295070422535e-08*Z);
    Increment_isotopic_property(X,XGd158,2.27297464788732e-08*Z);
    Increment_isotopic_property(X,XGd160,2.02557746478873e-08*Z);
    Increment_isotopic_property(X,XTb159,1.71462464788732e-08*Z);
    Increment_isotopic_property(X,XDy156,6.08279154929577e-11*Z);
    Increment_isotopic_property(X,XDy158,1.04512549295775e-10*Z);
    Increment_isotopic_property(X,XDy160,2.59459154929577e-09*Z);
    Increment_isotopic_property(X,XDy161,2.11749014084507e-08*Z);
    Increment_isotopic_property(X,XDy162,2.87356056338028e-08*Z);
    Increment_isotopic_property(X,XDy163,2.82552464788732e-08*Z);
    Increment_isotopic_property(X,XDy164,3.22698873239437e-08*Z);
    Increment_isotopic_property(X,XHo165,2.51346126760563e-08*Z);
    Increment_isotopic_property(X,XEr162,9.66752112676056e-11*Z);
    Increment_isotopic_property(X,XEr164,1.12725746478873e-09*Z);
    Increment_isotopic_property(X,XEr166,2.38771126760563e-08*Z);
    Increment_isotopic_property(X,XEr167,1.63965774647887e-08*Z);
    Increment_isotopic_property(X,XEr168,1.94584225352113e-08*Z);
    Increment_isotopic_property(X,XEr170,1.08821549295775e-08*Z);
    Increment_isotopic_property(X,XTm169,1.14993788732394e-08*Z);
    Increment_isotopic_property(X,XYb168,8.65519436619718e-11*Z);
    Increment_isotopic_property(X,XYb170,2.17492253521127e-09*Z);
    Increment_isotopic_property(X,XYb171,1.03441753521127e-08*Z);
    Increment_isotopic_property(X,XYb172,1.60165915492958e-08*Z);
    Increment_isotopic_property(X,XYb173,1.19579549295775e-08*Z);
    Increment_isotopic_property(X,XYb174,2.39274507042254e-08*Z);
    Increment_isotopic_property(X,XYb176,9.82290704225352e-09*Z);
    Increment_isotopic_property(X,XLu175,1.07993485915493e-08*Z);
    Increment_isotopic_property(X,XLu176,3.15225915492958e-10*Z);
    Increment_isotopic_property(X,XHf174,7.46190422535211e-11*Z);
    Increment_isotopic_property(X,XHf176,2.42545352112676e-09*Z);
    Increment_isotopic_property(X,XHf177,8.71787323943662e-09*Z);
    Increment_isotopic_property(X,XHf178,1.28623802816901e-08*Z);
    Increment_isotopic_property(X,XHf179,6.45811830985915e-09*Z);
    Increment_isotopic_property(X,XHf180,1.67247887323944e-08*Z);
    Increment_isotopic_property(X,XTa180,8.45746478873239e-13*Z);
    Increment_isotopic_property(X,XTa181,7.08615000000000e-09*Z);
    Increment_isotopic_property(X,XW180,4.98016901408451e-12*Z);
    Increment_isotopic_property(X,XW182,1.11199436619718e-08*Z);
    Increment_isotopic_property(X,XW183,6.03771126760563e-09*Z);
    Increment_isotopic_property(X,XW184,1.29979154929577e-08*Z);
    Increment_isotopic_property(X,XW186,1.21920380281690e-08*Z);
    Increment_isotopic_property(X,XRe185,6.19671830985915e-09*Z);
    Increment_isotopic_property(X,XRe187,1.13004626760563e-08*Z);
    Increment_isotopic_property(X,XOs184,4.25234366197183e-11*Z);
    Increment_isotopic_property(X,XOs186,3.43458169014085e-09*Z);
    Increment_isotopic_property(X,XOs187,2.74639788732394e-09*Z);
    Increment_isotopic_property(X,XOs188,2.89731830985915e-08*Z);
    Increment_isotopic_property(X,XOs189,3.55133661971831e-08*Z);
    Increment_isotopic_property(X,XOs190,5.80583802816901e-08*Z);
    Increment_isotopic_property(X,XOs192,9.11202253521127e-08*Z);
    Increment_isotopic_property(X,XIr191,7.68290774647887e-08*Z);
    Increment_isotopic_property(X,XIr193,1.30500619718310e-07*Z);
    Increment_isotopic_property(X,XPt190,5.72354929577465e-11*Z);
    Increment_isotopic_property(X,XPt192,3.23073802816901e-09*Z);
    Increment_isotopic_property(X,XPt194,1.37617042253521e-07*Z);
    Increment_isotopic_property(X,XPt195,1.41951760563380e-07*Z);
    Increment_isotopic_property(X,XPt196,1.06455605633803e-07*Z);
    Increment_isotopic_property(X,XPt198,3.05170985915493e-08*Z);
    Increment_isotopic_property(X,XAu197,6.41581830985916e-08*Z);
    Increment_isotopic_property(X,XHg196,2.24461408450704e-10*Z);
    Increment_isotopic_property(X,XHg198,1.50717042253521e-08*Z);
    Increment_isotopic_property(X,XHg199,2.56303591549296e-08*Z);
    Increment_isotopic_property(X,XHg200,3.52718309859155e-08*Z);
    Increment_isotopic_property(X,XHg201,2.02259788732394e-08*Z);
    Increment_isotopic_property(X,XHg202,4.60503098591549e-08*Z);
    Increment_isotopic_property(X,XHg204,1.06998000000000e-08*Z);
    Increment_isotopic_property(X,XTl203,1.82156760563380e-08*Z);
    Increment_isotopic_property(X,XTl205,4.39118661971831e-08*Z);
    Increment_isotopic_property(X,XPb204,2.30563098591549e-08*Z);
    Increment_isotopic_property(X,XPb206,2.16648169014084e-07*Z);
    Increment_isotopic_property(X,XPb207,2.40907183098592e-07*Z);
    Increment_isotopic_property(X,XPb208,6.92874366197183e-07*Z);
    Increment_isotopic_property(X,XBi209,4.81877464788732e-08*Z);
    Increment_isotopic_property(X,XTh232,1.99846939673062e-08*Z);
    Increment_isotopic_property(X,XU235,2.72331813667995e-09*Z);
    Increment_isotopic_property(X,XU238,8.72119365040135e-09*Z);
#endif // NUCSYN_ALL_ISOTOPES

    // He4 is everything else
    Set_isotopic_property(X,XHe4,0.0);
    Set_isotopic_property(X,XHe4,1.0-nucsyn_totalX(X));
}

#endif // NUCSYN
