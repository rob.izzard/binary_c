/* Useful function to calculate the total mass fraction from Xmin to Xmax */
#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef NUCSYN
#include "nucsyn/nucsyn_strided_isotope_loops.h"

double Pure_function Hot_function Nonnull_some_arguments(1,2)
    nucsyn_totalX_range(struct stardata_t * const stardata Maybe_unused,
                        const Abundance * const Restrict X,
                        const Isotope Xmin,
                        const Isotope Xmax)
{
    /*
     * Rarely-used function to loop over a range of isotopes
     */
    Abundance Xsum;
    if(Xmax==ISOTOPE_ARRAY_SIZE && Xmin==0)
    {
        Xsum = nucsyn_totalX(X);
    }
    else
    {
        Xsum = 0.0;
        for(Isotope i=Xmin;i<Xmax;i++)
        {
            Xsum += X[i];
        }
    }
    return Xsum;
}

/*
double Pure_function Hot_function Nonnull_some_arguments(1)
    nucsyn_totalX(const Abundance * Restrict const X)
{
    Abundance Xsum = 0.0;
    Isotope_loop(i)
    {
        Xsum += X[i];
    }
    return Xsum;
}
*/

double Pure_function Hot_function Nonnull_some_arguments(1)
    nucsyn_totalX(const Abundance * Restrict const X)
{
    Abundance Xsum[NUCSYN_STRIDE_N] = {0.0};
    Nucsyn_isotope_stride_leftarray_loop(Xsum, += X[i]);
    return Nucsyn_stride_sum(Xsum);
}

#endif// NUCSYN
