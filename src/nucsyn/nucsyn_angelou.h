#pragma once
#ifndef NUCSYN_ANGELOU_H
#define NUCSYN_ANGELOU_H

/*
 * Macro to access a stellar-type dependent factor.
 * Call with:
 * TYPE = variable type e.g. time, massfrac
 * ST = stellar type (e.g. star->stellar_type)
 */
#define angelou_factor(TYPE,ST)                                         \
    (                                                                   \
        (ST)==LOW_MASS_MS ? stardata->preferences->angelou_lithium_LMMS_##TYPE : \
        (ST)==MAIN_SEQUENCE ? stardata->preferences->angelou_lithium_MS_##TYPE : \
        (ST)==HERTZSPRUNG_GAP ? stardata->preferences->angelou_lithium_HG_##TYPE : \
        (ST)==GIANT_BRANCH ? stardata->preferences->angelou_lithium_GB_##TYPE : \
        (ST)==CHeB ? stardata->preferences->angelou_lithium_CHeB_##TYPE : \
        (ST)==EAGB ? stardata->preferences->angelou_lithium_EAGB_##TYPE : \
        (ST)==TPAGB ? stardata->preferences->angelou_lithium_TPAGB_##TYPE : \
        0.0                                                             \
        )


#define NUCSYN_ANGELOU_LITHIUM_BOOST_NONE 0
#define NUCSYN_ANGELOU_LITHIUM_BOOST_IN_TIME_RANGE 1
#define NUCSYN_ANGELOU_LITHIUM_BOOST_VROT 2
#define NUCSYN_ANGELOU_LITHIUM_BOOST_VROTFRAC 3

#define NUCSYN_ANGELOU_LITHIUM_BOOST_String(N) (                        \
        (N)==NUCSYN_ANGELOU_LITHIUM_BOOST_NONE ? "None" :               \
        (N)==NUCSYN_ANGELOU_LITHIUM_BOOST_IN_TIME_RANGE ? "Time range" : \
        (N)==NUCSYN_ANGELOU_LITHIUM_BOOST_VROT ? "v_eq > trigger" :     \
        (N)==NUCSYN_ANGELOU_LITHIUM_BOOST_VROTFRAC ? "v_eq / v_rot > trigger" : \
        "Unknown")

#endif // NUCSYN_ANGELOU_H
