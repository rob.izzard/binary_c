#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef NUCSYN

void nucsyn_free_icache(Isotope ** icache)
{
    /*
     * Destroy and free the cache memory associated with the elemental
     * lookup cache
     */
    Atomic_number el;
    for(el=0;el<NUMBER_OF_ELEMENTS;el++)
    {
        Safe_free(icache[el]);
    }
    Safe_free(icache);
}

#endif // NUCSYN
