#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef NUCSYN

#include "nucsyn_sn_Seitenzahl2013.h"

void nucsyn_sn_Seitenzahl2013(struct stardata_t * Restrict const stardata,
                              Abundance * Restrict const X)
{
    /*
     * type Ia MCh supernovae yields from
     * Seitenzahl et al. (2013) MNRAS 429,1156
     */
    static const double data[SEITENZAHL2013_N_MODELS][SEITENZAHL2013_N_ISOTOPES] =
        SEITENZAHL2013_DATA;

    static const char * model_map[SEITENZAHL2013_N_MODELS*2] =
        SEITENZAHL2013_MODEL_MAP;

    int i,nmodel=-1;
    for(i=0;i<SEITENZAHL2013_N_MODELS;i++)
    {
        Dprint("Try to match \"%s\" to pref \"%s\"\n",
               model_map[i*2+1],
               stardata->preferences->Seitenzahl2013_model);
        if(Strings_equal(model_map[i*2+1],
                         stardata->preferences->Seitenzahl2013_model))
        {
            /*
             * Matched a model set
             */
            nmodel = strtol(model_map[i*2],NULL,10);
            i = SEITENZAHL2013_N_MODELS+1;
        }
    }

    if(nmodel == -1)
    {
        /*
         * Failed to match any model set
         */
        Exit_binary_c(BINARY_C_ALGORITHM_OUT_OF_RANGE,
                      "Failed to match one of Seitenzahl et al. 2013's models to string \"%s\n",
                      stardata->preferences->Seitenzahl2013_model);
    }

    Dprint("Want Seitenzahl model set %s which is %d\n",
           stardata->preferences->Seitenzahl2013_model,
           nmodel);

    /*
     * Select appropriate line of the data
     */
    const double * Y = data[nmodel];

    /*
     * Hence set the isotope abundances
     */
    Increment_isotopic_property(X,XC12,Y[0]);
    Increment_isotopic_property(X,XC13,Y[1]);
    Increment_isotopic_property(X,XN14,Y[2]);
    Increment_isotopic_property(X,XN15,Y[3]);
    Increment_isotopic_property(X,XO16,Y[4]);
    Increment_isotopic_property(X,XO17,Y[5]);
    Increment_isotopic_property(X,XO18,Y[6]);
    Increment_isotopic_property(X,XF19,Y[7]);
    Increment_isotopic_property(X,XNe20,Y[8]);
    Increment_isotopic_property(X,XNe21,Y[9]);
    Increment_isotopic_property(X,XNe22,Y[10]);
    Increment_isotopic_property(X,XNa23,Y[11]);
    Increment_isotopic_property(X,XMg24,Y[12]);
    Increment_isotopic_property(X,XMg25,Y[13]);
    Increment_isotopic_property(X,XMg26,Y[14]);
    Increment_isotopic_property(X,XAl27,Y[15]);
    Increment_isotopic_property(X,XSi28,Y[16]);
    Increment_isotopic_property(X,XSi29,Y[17]);
    Increment_isotopic_property(X,XSi30,Y[18]);
    Increment_isotopic_property(X,XFe56,Y[51]);
#ifdef NUCSYN_ALL_ISOTOPES
    Increment_isotopic_property(X,XP31,Y[19]);
    Increment_isotopic_property(X,XS32,Y[20]);
    Increment_isotopic_property(X,XS33,Y[21]);
    Increment_isotopic_property(X,XS34,Y[22]);
    Increment_isotopic_property(X,XCl35,Y[23]);
    Increment_isotopic_property(X,XAr36,Y[24]);
    Increment_isotopic_property(X,XS36,Y[25]);
    Increment_isotopic_property(X,XCl37,Y[26]);
    Increment_isotopic_property(X,XAr38,Y[27]);
    Increment_isotopic_property(X,XK39,Y[28]);
    Increment_isotopic_property(X,XAr40,Y[29]);
    Increment_isotopic_property(X,XCa40,Y[30]);
    Increment_isotopic_property(X,XK41,Y[31]);
    Increment_isotopic_property(X,XCa42,Y[32]);
    Increment_isotopic_property(X,XCa43,Y[33]);
    Increment_isotopic_property(X,XCa44,Y[34]);
    Increment_isotopic_property(X,XSc45,Y[35]);
    Increment_isotopic_property(X,XCa46,Y[36]);
    Increment_isotopic_property(X,XTi46,Y[37]);
    Increment_isotopic_property(X,XTi47,Y[38]);
    Increment_isotopic_property(X,XCa48,Y[39]);
    Increment_isotopic_property(X,XTi48,Y[40]);
    Increment_isotopic_property(X,XTi49,Y[41]);
    Increment_isotopic_property(X,XCr50,Y[42]);
    Increment_isotopic_property(X,XTi50,Y[43]);
    Increment_isotopic_property(X,XV50,Y[44]);
    Increment_isotopic_property(X,XV51,Y[45]);
    Increment_isotopic_property(X,XCr52,Y[46]);
    Increment_isotopic_property(X,XCr53,Y[47]);
    Increment_isotopic_property(X,XCr54,Y[48]);
    Increment_isotopic_property(X,XFe54,Y[49]);
    Increment_isotopic_property(X,XMn55,Y[50]);
    Increment_isotopic_property(X,XFe57,Y[52]);
    Increment_isotopic_property(X,XFe58,Y[53]);
    Increment_isotopic_property(X,XNi58,Y[54]);
    Increment_isotopic_property(X,XCo59,Y[55]);
    Increment_isotopic_property(X,XNi60,Y[56]);
    Increment_isotopic_property(X,XNi61,Y[57]);
    Increment_isotopic_property(X,XNi62,Y[58]);
    Increment_isotopic_property(X,XCu63,Y[59]);
    Increment_isotopic_property(X,XNi64,Y[60]);
    Increment_isotopic_property(X,XZn64,Y[61]);
    Increment_isotopic_property(X,XCu65,Y[62]);
    Increment_isotopic_property(X,XZn66,Y[63]);
    Increment_isotopic_property(X,XZn67,Y[64]);
    Increment_isotopic_property(X,XZn68,Y[65]);
    Increment_isotopic_property(X,XGa69,Y[66]);
    Increment_isotopic_property(X,XZn70,Y[67]);
    Increment_isotopic_property(X,XGa71,Y[68]);
#endif // NUCSYN_ALL_ISOTOPES
    nucsyn_renormalize_abundance(X);
    Dprint("Seitenzahl model %s (%d) yields C12=%g O16=%g Fe56=%g Msun : sum=%g (should be ~Mch)\n",
           stardata->preferences->Seitenzahl2013_model,
           nmodel,
           X[XC12],
           X[XO16],
           X[XFe56],
           nucsyn_totalX(X)
        );

}
#endif // NUCSYN
