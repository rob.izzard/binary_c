#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef NUCSYN
#include <math.h> /* For the log10 function */


/* Function to output all the details of nucleosynthesis and a couple of other things like the HRD */

#define HEADER "NUCSYNLL__"
#ifdef NUCSYN_LOGGING
#ifdef NUCSYN_LONG_LOG


void nucsyn_long_log(struct stardata_t * Restrict const stardata)
{
    Star_number k; // star number
    Isotope i; // isotope number
    struct star_t *star;

    // loop over stars
    //  Starloop(k)
    //  {
    k=1;

    // save pointer to star : must be quicker than always looking it up!
    SETstar(k);

    if(star->stellar_type==TPAGB)
    {
        if(stardata->common.tpagbcount==0)
        {
            stardata->common.tpagbtime=stardata->model.time;
            stardata->common.tpagbcount++;
        }

        // output header and model time
        printf("%s %g ",HEADER,1e6*(stardata->model.time-stardata->common.tpagbtime));


        // output ALL isotopic data!
        Forward_isotope_loop(i)
        {
            printf("%g ",star->Xenv[i]);
        }

        // HRD + others?
        printf("%g %g %g %g %d ",
               star->luminosity,
               Teff(k),
               star->mass,
               star->core_mass[CORE_He],
               stardata->star[k].stellar_type);
        //  }

        printf("\n"); // final newline
    }
}
#endif /* NUCSYN_LONG_LOG */
#endif /* NUCSYN_LOGGING */

#endif /* NUCSYN */
