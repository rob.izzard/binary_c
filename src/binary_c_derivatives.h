#pragma once
#ifndef BINARY_C_DERIVATIVES_H
#define BINARY_C_DERIVATIVES_H
/*
 * The binary_c stellar population nucleosynthesis framework.
 *
 * Copyright Robert Izzard 2000-2025
 *
 * Contact rob.izzard@gmail.com
 *
 * Code repository:
 * https://gitlab.com/binary_c
 *
 * Documentation is in doc/
 * Code is in src/
 *
 * To build, run
 *
 * ---
 *
 * meson setup builddir
 * cd builddir
 * ninja binary_c_install
 *
 * ---
 *
 * Please see the files README, LICENCE and CHANGES,
 * and the doc/ directory for documentation which is mirrored
 * at https://binary_c.gitlab.io
 *
 **********************
 *
 * Purpose:
 *
 * binary_c_derivatives.h contains definitions of the macros
 * used for derivative (rates of change) information in binary_c
 *
 **********************
 */

#include "binary_c_derivatives.def"

/*
 * Derivative addition methods
 */

#undef X
#define X(CODE) DERIVATIVE_ADD_##CODE,
enum{
    DERIVATIVE_ADDITIONS_LIST
};

/*
 * Derivative groups
 */
#undef X
#define X(CODE,STRING) DERIVATIVES_GROUP_##CODE,
enum {
    DERIVATIVES_GROUP_LIST
    X(NUMBER,"number")
};
static const unsigned int derivatives_order[] = {
    DERIVATIVES_GROUP_LIST
};
#undef X
#define X(CODE,STRING) STRING,
static const char * derivative_group_strings[] Maybe_unused = { DERIVATIVES_GROUP_LIST };
#undef X
#define Derivative_group_string(N) ((char*)derivative_group_strings[(N)])



/* stellar derivatives */
#define X(CODE,STRING,VAR,FUNC,GROUP,MINTCLEAR,ADDITION_METHOD) DERIVATIVE_STELLAR_##CODE,
enum { STELLAR_DERIVATIVES_LIST };
#undef X
#define X(CODE,STRING,VAR,FUNC,GROUP,MINTCLEAR,ADDITION_METHOD) STRING,
static const char * stellar_derivative_strings[] Maybe_unused = { STELLAR_DERIVATIVES_LIST };
#undef X
#define Stellar_derivative_string(N)            \
    ((char*) stellar_derivative_strings[(N)])

/* system derivatives */
#define X(CODE,STRING,VAR,FUNC,GROUP,MINTCLEAR) DERIVATIVE_##CODE,
enum { SYSTEM_DERIVATIVES_LIST };
#undef X
#define X(CODE,STRING,VAR,FUNC,GROUP,MINTCLEAR) STRING,
static const char * system_derivative_strings[] Maybe_unused = { SYSTEM_DERIVATIVES_LIST };
#undef X

/* orbiting object derivatives */
#define X(CODE,STRING,VAR,FUNC,GROUP,MINTCLEAR) DERIVATIVE_ORBITING_OBJECT_##CODE,
enum { ORBITING_OBJECT_DERIVATIVES_LIST };
#undef X
#define X(CODE,STRING,VAR,FUNC,GROUP,MINTCLEAR) STRING,
static const char * orbiting_object_derivative_strings[] Maybe_unused = { ORBITING_OBJECT_DERIVATIVES_LIST };
#undef X


#define System_derivative_string(N)             \
    ((char*) system_derivative_strings [(N)])

#define Orbiting_object_derivative_string(N)            \
    ((char*) orbiting_object_derivative_strings [(N)])

#define Derivative_string(GROUP,N)                                      \
    (                                                                   \
        (GROUP)==DERIVATIVES_GROUP_STELLAR ? Stellar_derivative_string(N) : \
        (GROUP)==DERIVATIVES_GROUP_SYSTEM ? System_derivative_string(N) : \
        (GROUP)==DERIVATIVES_GROUP_ORBITING_OBJECT ? Orbiting_object_derivative_string(N) : \
        (GROUP)==DERIVATIVES_GROUP_OTHER ? "other" :                     \
        "unknown"                                                       \
        )


#define Show_stellar_derivatives(S,SEPARATOR)                           \
    {                                                                   \
        int iii;                                                        \
        static const char * c[100] = STELLAR_DERIVATIVE_STRINGS;        \
        printf("star %d type %d : ",                                    \
               (S)->starnum,                                            \
               (S)->stellar_type);                                      \
        for(iii=0;iii<DERIVATIVE_STELLAR_NUMBER;iii++)                  \
        {                                                               \
            printf("%d %s = %g %s",                                     \
                   iii,                                                 \
                   c[iii],                                              \
                   (S)->derivative[iii],                                \
                   iii==DERIVATIVE_STELLAR_NUMBER-1?"":(SEPARATOR));    \
        }                                                               \
        printf("\n");                                                   \
    }

#define Zero_stellar_derivatives(S)                     \
    {                                                   \
        int iii;                                        \
        for(iii=0;iii<DERIVATIVE_STELLAR_NUMBER;iii++)  \
        {                                               \
            (S)->derivative[iii]=0.0;                   \
        }                                               \
    }

#define Zero_orbiting_object_derivatives(S)                             \
    {                                                                   \
        for(size_t iij=0; iij<(S)->common.n_orbiting_objects; iij++)    \
        {                                                               \
            for(int iii=0;iii<DERIVATIVE_ORBITING_OBJECT_NUMBER;iii++)  \
            {                                                           \
                (S)->common.orbiting_object[iij].derivative[iii]=0.0;   \
            }                                                           \
        }                                                               \
    }


/*
 * macro that is TRUE for a derivative that can be solved
 * exponentially, assuming dx/dt = x/tau where tau = constant.
 *
 * Good examples are tidal spin up/down and angular momentum
 * loss from the system (the latter because as J->0 the stars
 * merge, at which point J=0 and everything stops).
 *
 * Such a solver still needs short timesteps (so we can say
 * tau ~ constant) but should not suffer from the problem where
 * the x overshoots and becomes negative.
 *
 * S = stardata
 * P = pointer to table of derivatives
 * N = derivative number
 */

#define Exponential_derivative(S,P,N)                   \
    (                                                   \
        (                                               \
            (P)==(S)->model.derivative &&               \
            (                                           \
                (N) == DERIVATIVE_ORBIT_ECCENTRICITY || \
                (N) == DERIVATIVE_ORBIT_ANGMOM          \
                )) ||                                   \
        (                                               \
            (N) == DERIVATIVE_STELLAR_ANGMOM ||         \
            (N) == DERIVATIVE_STELLAR_MASS              \
            )                                           \
        )
#undef Exponential_derivative
#define Exponential_derivative(S,P,N)  (FALSE)

//#undef Exponential_derivative
//#define Exponential_derivative(S,P,N) (FALSE)

#define Jdot_orbit(S) (                                                 \
        (S)->model.derivative[DERIVATIVE_ORBIT_ANGMOM_GRAVITATIONAL_RADIATION] + \
        (S)->model.derivative[DERIVATIVE_ORBIT_ANGMOM_WIND_LOSS] +      \
        (S)->model.derivative[DERIVATIVE_ORBIT_ANGMOM_WIND_GAIN] +      \
        (S)->model.derivative[DERIVATIVE_ORBIT_ANGMOM_RLOF_LOSS] +      \
        (S)->model.derivative[DERIVATIVE_ORBIT_ANGMOM_RLOF_GAIN] +      \
        (S)->model.derivative[DERIVATIVE_ORBIT_ANGMOM_CBDISC] +         \
        (S)->model.derivative[DERIVATIVE_ORBIT_ANGMOM_TIDES] +          \
        (S)->model.derivative[DERIVATIVE_ORBIT_ANGMOM_NOVA] +           \
        (S)->model.derivative[DERIVATIVE_ORBIT_ANGMOM_NONCONSERVATIVE_LOSS] \
        )

#define edot_orbit(S) (                                                 \
        (S)->model.derivative[DERIVATIVE_ORBIT_ECCENTRICITY_GRAVITATIONAL_RADIATION] + \
        (S)->model.derivative[DERIVATIVE_ORBIT_ECCENTRICITY_TIDES] +    \
        (S)->model.derivative[DERIVATIVE_ORBIT_ECCENTRICITY_WINDS] +    \
        (S)->model.derivative[DERIVATIVE_ORBIT_ECCENTRICITY_CBDISC] +   \
        (S)->model.derivative[DERIVATIVE_ORBIT_ECCENTRICITY_RLOF]       \
        )

#define Zero_stellar_mass_derivatives(STAR)                             \
    {                                                                   \
        (STAR)->derivative[DERIVATIVE_STELLAR_MASS_WIND_LOSS] =         \
            (STAR)->derivative[DERIVATIVE_STELLAR_MASS_WIND_GAIN] =     \
            (STAR)->derivative[DERIVATIVE_STELLAR_MASS_RLOF_LOSS] =     \
            (STAR)->derivative[DERIVATIVE_STELLAR_MASS_RLOF_GAIN] =     \
            (STAR)->derivative[DERIVATIVE_STELLAR_MASS_NOVA] =          \
            (STAR)->derivative[DERIVATIVE_STELLAR_MASS_RLOF_TRANSFER] = \
            (STAR)->derivative[DERIVATIVE_STELLAR_MASS_DISC_GAIN] =     \
            (STAR)->derivative[DERIVATIVE_STELLAR_MASS_DISC_LOSS] =     \
            (STAR)->derivative[DERIVATIVE_STELLAR_MASS_DECRETION_DISC] = \
            (STAR)->derivative[DERIVATIVE_STELLAR_MASS_CBDISC_GAIN] =   \
            (STAR)->derivative[DERIVATIVE_STELLAR_MASS_NONCONSERVATIVE_LOSS] = \
            (STAR)->derivative[DERIVATIVE_STELLAR_MASS_ARTIFICIAL] =    \
            (STAR)->derivative[DERIVATIVE_STELLAR_MASS_COMMAND] =    \
            (STAR)->derivative[DERIVATIVE_STELLAR_MASS_IRRADIATIVE_LOSS] = \
            (STAR)->derivative[DERIVATIVE_STELLAR_H_LAYER_MASS] =       \
            (STAR)->derivative[DERIVATIVE_STELLAR_MASS] =               \
            0.0;                                                        \
    }


#define Modulate_stellar_mass_derivatives(STAR,F)                       \
    {                                                                   \
        (STAR)->derivative[DERIVATIVE_STELLAR_MASS_WIND_LOSS] *= (F);   \
        (STAR)->derivative[DERIVATIVE_STELLAR_MASS_WIND_GAIN] *= (F);   \
        (STAR)->derivative[DERIVATIVE_STELLAR_MASS_RLOF_LOSS] *= (F);   \
        (STAR)->derivative[DERIVATIVE_STELLAR_MASS_RLOF_GAIN] *= (F);   \
        (STAR)->derivative[DERIVATIVE_STELLAR_MASS_NOVA] *= (F);        \
        (STAR)->derivative[DERIVATIVE_STELLAR_MASS_RLOF_TRANSFER] *= (F); \
        (STAR)->derivative[DERIVATIVE_STELLAR_MASS_DISC_GAIN] *= (F);   \
        (STAR)->derivative[DERIVATIVE_STELLAR_MASS_DISC_LOSS] *= (F);   \
        (STAR)->derivative[DERIVATIVE_STELLAR_MASS_DECRETION_DISC] *= (F); \
        (STAR)->derivative[DERIVATIVE_STELLAR_MASS_CBDISC_GAIN] *= (F); \
        (STAR)->derivative[DERIVATIVE_STELLAR_MASS_NONCONSERVATIVE_LOSS] *= (F); \
        (STAR)->derivative[DERIVATIVE_STELLAR_MASS_ARTIFICIAL] *= (F);  \
        (STAR)->derivative[DERIVATIVE_STELLAR_MASS_COMMAND] *= (F);     \
        (STAR)->derivative[DERIVATIVE_STELLAR_MASS_IRRADIATIVE_LOSS] *= (F); \
        (STAR)->derivative[DERIVATIVE_STELLAR_H_LAYER_MASS] *= (F);     \
        (STAR)->derivative[DERIVATIVE_STELLAR_MASS] *= (F);             \
    }

#define Zero_stellar_angmom_derivatives(STAR)                           \
    {                                                                   \
        (STAR)->derivative[DERIVATIVE_STELLAR_ANGMOM_WIND_LOSS] =       \
            (STAR)->derivative[DERIVATIVE_STELLAR_ANGMOM_WIND_GAIN] =   \
            (STAR)->derivative[DERIVATIVE_STELLAR_ANGMOM_RLOF_LOSS] =   \
            (STAR)->derivative[DERIVATIVE_STELLAR_ANGMOM_RLOF_GAIN] =   \
            (STAR)->derivative[DERIVATIVE_STELLAR_ANGMOM_TIDES] =       \
            (STAR)->derivative[DERIVATIVE_STELLAR_ANGMOM_MAGNETIC_BRAKING] = \
            (STAR)->derivative[DERIVATIVE_STELLAR_ANGMOM_DECRETION_DISC] = \
            (STAR)->derivative[DERIVATIVE_STELLAR_ANGMOM_CBDISC_GAIN] = \
            (STAR)->derivative[DERIVATIVE_STELLAR_ANGMOM_NOVA] =        \
            (STAR)->derivative[DERIVATIVE_STELLAR_ANGMOM_NONCONSERVATIVE_LOSS] = \
            (STAR)->derivative[DERIVATIVE_STELLAR_ANGMOM_ARTIFICIAL] =  \
            (STAR)->derivative[DERIVATIVE_STELLAR_ANGMOM_COMMAND] =  \
            (STAR)->derivative[DERIVATIVE_STELLAR_ANGMOM] =             \
            0.0;                                                        \
    }

#define Modulate_stellar_angmom_derivatives(STAR,F)                     \
    {                                                                   \
        (STAR)->derivative[DERIVATIVE_STELLAR_ANGMOM_WIND_LOSS] *= (F); \
        (STAR)->derivative[DERIVATIVE_STELLAR_ANGMOM_WIND_GAIN] *= (F); \
        (STAR)->derivative[DERIVATIVE_STELLAR_ANGMOM_RLOF_LOSS] *= (F); \
        (STAR)->derivative[DERIVATIVE_STELLAR_ANGMOM_RLOF_GAIN] *= (F); \
        (STAR)->derivative[DERIVATIVE_STELLAR_ANGMOM_TIDES] *= (F);     \
        (STAR)->derivative[DERIVATIVE_STELLAR_ANGMOM_MAGNETIC_BRAKING] *= (F); \
        (STAR)->derivative[DERIVATIVE_STELLAR_ANGMOM_DECRETION_DISC] *= (F); \
        (STAR)->derivative[DERIVATIVE_STELLAR_ANGMOM_CBDISC_GAIN] *= (F); \
        (STAR)->derivative[DERIVATIVE_STELLAR_ANGMOM_NOVA] *= (F);      \
        (STAR)->derivative[DERIVATIVE_STELLAR_ANGMOM_NONCONSERVATIVE_LOSS] *= (F); \
        (STAR)->derivative[DERIVATIVE_STELLAR_ANGMOM_ARTIFICIAL] *= (F); \
        (STAR)->derivative[DERIVATIVE_STELLAR_ANGMOM_COMMAND] *= (F);   \
        (STAR)->derivative[DERIVATIVE_STELLAR_ANGMOM] *= (F);           \
    }

#define Zero_orbit_and_system_derivatives(S) {                          \
        (S)->model.derivative[DERIVATIVE_ORBIT_ANGMOM_GRAVITATIONAL_RADIATION] = \
            (S)->model.derivative[DERIVATIVE_ORBIT_ANGMOM_WIND_LOSS] =  \
            (S)->model.derivative[DERIVATIVE_ORBIT_ANGMOM_WIND_GAIN] =  \
            (S)->model.derivative[DERIVATIVE_ORBIT_ANGMOM_RLOF_LOSS] =  \
            (S)->model.derivative[DERIVATIVE_ORBIT_ANGMOM_RLOF_GAIN] =  \
            (S)->model.derivative[DERIVATIVE_ORBIT_ANGMOM_CBDISC] =     \
            (S)->model.derivative[DERIVATIVE_ORBIT_ANGMOM_TIDES] =      \
            (S)->model.derivative[DERIVATIVE_ORBIT_ANGMOM_NOVA] =       \
            (S)->model.derivative[DERIVATIVE_ORBIT_ANGMOM_ARTIFICIAL] = \
            (S)->model.derivative[DERIVATIVE_ORBIT_ANGMOM_COMMAND] =    \
            (S)->model.derivative[DERIVATIVE_ORBIT_ANGMOM_NONCONSERVATIVE_LOSS] = \
            (S)->model.derivative[DERIVATIVE_ORBIT_ECCENTRICITY_GRAVITATIONAL_RADIATION] = \
            (S)->model.derivative[DERIVATIVE_ORBIT_ECCENTRICITY_TIDES] = \
            (S)->model.derivative[DERIVATIVE_ORBIT_ECCENTRICITY_WINDS] = \
            (S)->model.derivative[DERIVATIVE_ORBIT_ECCENTRICITY_CBDISC] = \
            (S)->model.derivative[DERIVATIVE_ORBIT_ECCENTRICITY_RLOF] = \
            (S)->model.derivative[DERIVATIVE_SYSTEM_CBDISC_MASS_LOSS] = \
            0.0;                                                        \
    }



/*
 * Stellar accretion rates
 */
#define NEGLIGIBLE_ACCRETION_RATE 1e-40

/* mass gain only */
#define Mdot_gain(STAR)                                                 \
    (                                                                   \
        (STAR)->derivative[DERIVATIVE_STELLAR_MASS_WIND_GAIN] +         \
        (STAR)->derivative[DERIVATIVE_STELLAR_MASS_RLOF_GAIN] +         \
        (STAR)->derivative[DERIVATIVE_STELLAR_MASS_DISC_GAIN] +         \
        (STAR)->derivative[DERIVATIVE_STELLAR_MASS_CBDISC_GAIN]  +      \
        Max(0.0,(STAR)->derivative[DERIVATIVE_STELLAR_MASS_NOVA]) +     \
        Max(0.0,(STAR)->derivative[DERIVATIVE_STELLAR_MASS_ARTIFICIAL]) + \
        Max(0.0,(STAR)->derivative[DERIVATIVE_STELLAR_MASS_COMMAND]) \
        )

#define Mdot_gain_derivatives                   \
    DERIVATIVE_STELLAR_MASS_WIND_GAIN,          \
        DERIVATIVE_STELLAR_MASS_RLOF_GAIN,      \
        DERIVATIVE_STELLAR_MASS_DISC_GAIN,      \
        DERIVATIVE_STELLAR_MASS_CBDISC_GAIN,    \
        -DERIVATIVE_STELLAR_MASS_NOVA,          \
        -DERIVATIVE_STELLAR_MASS_ARTIFICIAL,    \
        DERIVATIVE_STELLAR_MASS_COMMAND


/* mass loss only */
#define Mdot_loss(STAR)                                                 \
    (                                                                   \
        (STAR)->derivative[DERIVATIVE_STELLAR_MASS_WIND_LOSS] +         \
        (STAR)->derivative[DERIVATIVE_STELLAR_MASS_RLOF_LOSS] +         \
        (STAR)->derivative[DERIVATIVE_STELLAR_MASS_DISC_LOSS] +         \
        (STAR)->derivative[DERIVATIVE_STELLAR_MASS_DECRETION_DISC] +    \
        (STAR)->derivative[DERIVATIVE_STELLAR_MASS_NONCONSERVATIVE_LOSS]+ \
        Min(0.0,(STAR)->derivative[DERIVATIVE_STELLAR_MASS_NOVA]) +     \
        Min(0.0,(STAR)->derivative[DERIVATIVE_STELLAR_MASS_ARTIFICIAL]) + \
        Min(0.0,(STAR)->derivative[DERIVATIVE_STELLAR_MASS_COMMAND]) \
        )

#define Mdot_loss_derivatives                           \
    DERIVATIVE_STELLAR_MASS_WIND_LOSS,                  \
        DERIVATIVE_STELLAR_MASS_RLOF_LOSS,              \
        DERIVATIVE_STELLAR_MASS_DISC_LOSS,              \
        DERIVATIVE_STELLAR_MASS_DECRETION_DISC,         \
        DERIVATIVE_STELLAR_MASS_NONCONSERVATIVE_LOSS,   \
        -DERIVATIVE_STELLAR_MASS_NOVA,                  \
        DERIVATIVE_STELLAR_MASS_ARTIFICIAL,             \
        DERIVATIVE_STELLAR_MASS_COMMAND

/* net gain of mass */
#define Mdot_net(STAR) ( Mdot_gain(STAR) + Mdot_loss(STAR) )

/* angular momentum gain only : these terms should be positive */
#define Jdot_gain(STAR)                                                 \
    (                                                                   \
        (STAR)->derivative[DERIVATIVE_STELLAR_ANGMOM_WIND_GAIN] +       \
        (STAR)->derivative[DERIVATIVE_STELLAR_ANGMOM_RLOF_GAIN] +       \
        Max(0.0,(STAR)->derivative[DERIVATIVE_STELLAR_ANGMOM_TIDES]) +  \
        Max(0.0,(STAR)->derivative[DERIVATIVE_STELLAR_ANGMOM_DECRETION_DISC]) + \
        (STAR)->derivative[DERIVATIVE_STELLAR_ANGMOM_CBDISC_GAIN] +     \
        Max(0.0,(STAR)->derivative[DERIVATIVE_STELLAR_ANGMOM_ARTIFICIAL]) + \
        Max(0.0,(STAR)->derivative[DERIVATIVE_STELLAR_ANGMOM_COMMAND]) + \
        Max(0.0,(STAR)->derivative[DERIVATIVE_STELLAR_ANGMOM_CORE_ENVELOPE_COUPLING]) \
        )


#define Jdot_gain_derivatives                               \
    DERIVATIVE_STELLAR_ANGMOM_RLOF_GAIN,                    \
        DERIVATIVE_STELLAR_ANGMOM_WIND_GAIN,                \
        -DERIVATIVE_STELLAR_ANGMOM_TIDES,                   \
        DERIVATIVE_STELLAR_ANGMOM_CBDISC_GAIN,              \
        -DERIVATIVE_STELLAR_ANGMOM_CORE_ENVELOPE_COUPLING

/* angular momentum loss only : these terms should be negative */
#define Jdot_loss(STAR)                                                 \
    (                                                                   \
        (STAR)->derivative[DERIVATIVE_STELLAR_ANGMOM_RLOF_LOSS] +       \
        (STAR)->derivative[DERIVATIVE_STELLAR_ANGMOM_WIND_LOSS] +       \
        Min(0.0,(STAR)->derivative[DERIVATIVE_STELLAR_ANGMOM_TIDES]) +  \
        (STAR)->derivative[DERIVATIVE_STELLAR_ANGMOM_MAGNETIC_BRAKING] + \
        Min(0.0,(STAR)->derivative[DERIVATIVE_STELLAR_ANGMOM_DECRETION_DISC]) + \
        (STAR)->derivative[DERIVATIVE_STELLAR_ANGMOM_NOVA] +            \
        (STAR)->derivative[DERIVATIVE_STELLAR_ANGMOM_NONCONSERVATIVE_LOSS] + \
        Min(0.0,(STAR)->derivative[DERIVATIVE_STELLAR_ANGMOM_ARTIFICIAL]) + \
        Min(0.0,(STAR)->derivative[DERIVATIVE_STELLAR_ANGMOM_COMMAND]) + \
        Min(0.0,(STAR)->derivative[DERIVATIVE_STELLAR_ANGMOM_CORE_ENVELOPE_COUPLING]) \
        )

#define Jdot_loss_derivatives                               \
    DERIVATIVE_STELLAR_ANGMOM_RLOF_LOSS,                    \
        DERIVATIVE_STELLAR_ANGMOM_WIND_LOSS,                \
        -DERIVATIVE_STELLAR_ANGMOM_TIDES,                   \
        DERIVATIVE_STELLAR_ANGMOM_MAGNETIC_BRAKING,         \
        DERIVATIVE_STELLAR_ANGMOM_DECRETION_DISC,           \
        DERIVATIVE_STELLAR_ANGMOM_NOVA,                     \
        DERIVATIVE_STELLAR_ANGMOM_NONCONSERVATIVE_LOSS,     \
        -DERIVATIVE_STELLAR_ANGMOM_CORE_ENVELOPE_COUPLING


/* net angular momentum */
#define Jdot_net(STAR) ( Jdot_gain(STAR) + Jdot_loss(STAR) )

/*
 * Stellar loss rates
 */

#define generic_derivative(STAR,N) ((STAR)->derivative[N])
#define mass_derivative(STAR,N) (generic_derivative((STAR),N))
#define angular_momentum_derivative(STAR,N) (generic_derivative((STAR),N))
#define specific_angular_momentum_derivative(STAR,N) (generic_derivative((STAR),N))

/*
 * Return values for apply_derivative
 */
#define BINARY_C_APPLY_DERIVATIVE_FAILED FALSE
#define BINARY_C_APPLY_DERIVATIVE_SUCCESS TRUE
#define Apply_derivative_string(X) ((X) == FALSE ? "Failed" : "Success")

#endif // BINARY_C_DERIVATIVES_H
