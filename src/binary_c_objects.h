#include "binary_c.h"
#pragma once
#ifndef BINARY_C_OBJECTS_H
#define BINARY_C_OBJECTS_H


/*
 * The binary_c stellar population nucleosynthesis framework.
 *
 * Copyright Robert Izzard 2000-2025
 *
 * Contact rob.izzard@gmail.com
 *
 * Code repository:
 * https://gitlab.com/binary_c
 *
 * Documentation is in doc/
 * Code is in src/
 *
 * To build, run
 *
 * ---
 *
 * meson setup builddir
 * cd builddir
 * ninja binary_c_install
 *
 * ---
 *
 * Please see the files README, LICENCE and CHANGES,
 * and the doc/ directory for documentation which is mirrored
 * at https://binary_c.gitlab.io
 *
 **********************
 *
 * Purpose:
 *
 * This file contains macros which, when defined, lead to the
 * inclusion of a given object file in the
 * binary_c executable. These particular object files contain
 * numerical data.
 *
 * The macro should be of the form:
 *
 * OBJECT_FILENAME
 *
 * where the filename is translated into capitals
 * with underscores (_) replacing non-alphabet characters.
 *
 * e.g.
 *
 * src/stellar_magnitudes/Kurucz.o
 *
 * becomes
 *
 * OBJECT_SRC_STELLAR_COLOURS_KURUCZ
 */

#ifdef STELLAR_COLOURS
#  define OBJECT_SRC_STELLAR_COLOURS_KURUCZ
#  define OBJECT_SRC_STELLAR_COLOURS_ELDRIDGE2015_OSTAR
#  define OBJECT_SRC_STELLAR_COLOURS_ELDRIDGE2015_BASEL
#endif

#if defined NUCSYN && defined NUCSYN_S_PROCESS
#  define OBJECT_SRC_NUCSYN_NUCSYN_EXTENDED_S_PROCESS
#endif

#if defined NUCSYN && defined NUCSYN_NOVAE
#  define OBJECT_NUCSYN_NUCSYN_NOVAE_JH98_CO
#  define OBJECT_NUCSYN_NUCSYN_NOVAE_JH98_ONE
#endif

#ifdef COMENV_POLYTROPES
#  define OBJECT_COMMON_ENVELOPE_COMMON_ENVELOPE_POLYTROPE_3D
#endif

#ifdef OPACITY_ALGORITHMS
#ifdef OPACITY_ENABLE_ALGORITHM_FERGUSON_OPAL
#  define OBJECT_OPACITY_FERGUSON_OPAL_16062017
#endif
#ifdef OPACITY_ENABLE_ALGORITHM_STARS
#  define OBJECT_OPACITY_STARS
#endif
#endif // OPACITY_ALGORITHMS
#endif// BINARY_C_OBJECTS_H
