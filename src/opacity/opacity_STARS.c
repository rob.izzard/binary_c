#include "../binary_c.h"
No_empty_translation_unit_warning;

/*
 * Opacity routine based on Ferguson (where available)
 * otherwise OPAL
 */
#if defined OPACITY_ALGORITHMS &&                       \
    defined OPACITY_ENABLE_ALGORITHM_FERGUSON_OPAL

#include "opacity.h"
#include "../opacity/opacity_STARS.h"

double opacity_STARS(Opacity_function_args_in)
{
/*
 * From the Cambridge stars code
 */
    //double Z = op->Z;
    double logT = log10(op->temperature);
    double logR = log10(op->density);
    double x[TABLE_OPACITY_STARS_NPARAMS] = {logT,logR};
    double rr[TABLE_OPACITY_STARS_NDATA];

    if(0)
    {
        /* show table contents */
        int i;
        for(i=0;i<TABLE_OPACITY_STARS_LINES;i++)
        {
            int j;
            printf("Stars %4d :",i);
            for(j=0;j<TABLE_OPACITY_STARS_NPARAMS+TABLE_OPACITY_STARS_NDATA;j++)
            {
                printf("%g ",
                       *(stardata->store->opacity_STARS->data +
                         i*(TABLE_OPACITY_STARS_NPARAMS+TABLE_OPACITY_STARS_NDATA)
                         + j));
            }
            printf("\n");
        }
    }

    Interpolate(stardata->store->opacity_STARS,
                x,
                rr,
                FALSE);

    return exp10(rr[0]);
}
#endif // OPACITY_ALGORITHMS
