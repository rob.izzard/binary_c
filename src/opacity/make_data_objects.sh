#!/bin/bash

# make data objects for the opacity library of binary_c
echo "make data objects for the opacity library of binary_c"

source ../make_data_objects_setup.sh

# Ferguson/Opal opacity table as generated on 16/06/2017
# based on the data Adam Jermyn sent RGI, with nearest-neighbour
# interpolation where gaps appear 
HFILE=ferguson_opal_16062017.dat.bz2
TMPFILE=ferguson_opal_16062017.dat
OBJFILE=ferguson_opal_16062017.o
echo "HFILE=$HFILE TMPFILE=$TMPFILE OBJFILE=$OBJFILE"

if [ $HFILE -nt $OBJFILE ]  || [ !-z $BINARY_C_REBUILD_DATA_OBJECTS] ; then
    
    $CC ../../double2bin.c -o ./double2bin

    bzcat $HFILE | ./double2bin > $TMPFILE

    # The opacity table can be read-only
    OPTS="--rename-section .data=.rodata,alloc,load,readonly,data,contents "

    objcopy $OBJCOPY_OPTS $OPTS $TMPFILE $OBJFILE
    # ld -r -b binary -o $OBJFILE $TMPFILE

    ls -l $TMPFILE $OBJFILE double2bin
    rm $TMPFILE
    rm ./double2bin
fi

echo "DATAOBJECT $OBJFILE"

# STARS code Z=0.02 opacity 
HFILE=STARS_Z0.02.dat.bz2
TMPFILE=STARS_Z0.02.dat
OBJFILE=STARS_Z0.02.dat.o
echo "HFILE=$HFILE TMPFILE=$TMPFILE OBJFILE=$OBJFILE"

if [ $HFILE -nt $OBJFILE ]  || [ !-z $BINARY_C_REBUILD_DATA_OBJECTS] ; then
    
    $CC ../../double2bin.c -o ./double2bin

    bzcat $HFILE | ./double2bin > $TMPFILE

    # The opacity table can be read-only
    OPTS="--rename-section .data=.rodata,alloc,load,readonly,data,contents "

    objcopy $OBJCOPY_OPTS $OPTS $TMPFILE $OBJFILE
    # ld -r -b binary -o $OBJFILE $TMPFILE

    ls -l $TMPFILE $OBJFILE double2bin
    rm $TMPFILE
    rm ./double2bin
fi

echo "DATAOBJECT $OBJFILE"
