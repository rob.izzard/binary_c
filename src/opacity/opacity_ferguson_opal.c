#include "../binary_c.h"
No_empty_translation_unit_warning;

/*
 * Opacity routine based on Ferguson (where available)
 * otherwise OPAL
 */
#if defined OPACITY_ALGORITHMS &&                       \
    defined OPACITY_ENABLE_ALGORITHM_FERGUSON_OPAL

#include "opacity.h"
#include "../opacity/opacity_ferguson_opal.h"

double opacity_ferguson_opal(Opacity_function_args_in)
{
/*
 * From
 * http://www.cita.utoronto.ca/~boothroy/data/kappa/z14xcotrin21.f
 *
 *      TEMPERATURES AND DENSITIES CONTAINED IN OPAL OPACITY TABLES:
 *
 * The OPAL opacities are tabulated in terms of logT and logR, where
 *  logR = logRHO - 3 * ( logT - 6 )  , i.e.,  R = RHO / T6^3
 * (with T being temperature in Kelvins and RHO being density in g/cm^3)
 * The "density" and temperature ranges contained in the tables are:
 *  logR : -8.0 to 1.0   [at logR = -8.0 (0.5) 1.0, i.e., delta_logRHO = 0.5]
 *  logT : 3.75 to 8.70  [at logT = 3.75 (0.05) 6.00 (0.10) 8.10 (0.20) 8.70]
 *
 */
    double X = op->H;
    double Z = op->Z;
    double logT = log10(op->temperature);
    double logR = log10(op->density) - 3.0 * ( logT - 6.0 );

    double x[4] = {X,Z,logR,logT};
    double rr[1];

    Interpolate(stardata->store->opacity_ferguson_opal,
                x,rr,FALSE);

    return exp10(rr[0]);
}
#endif // OPACITY_ALGORITHMS
