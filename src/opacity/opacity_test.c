#include "../binary_c.h"
No_empty_translation_unit_warning;

//#define TEST_OPACITY

#ifdef TEST_OPACITY
{
#ifndef TEST_OPACITY
#endif

/*
 * moved from main.c
 */
    struct opacity_t op;

    if(! argc == 5)
    {
         printf("You must run binary_c with four arguments (X,Z,T,rho) to test the opacity table\n");
    }
    op.H = fast_strtod(argv[1],NULL);
    op.Z = fast_strtod(argv[2],NULL);
    op.temperature = fast_strtod(argv[3],NULL);
    op.density = fast_strtod(argv[4],NULL);

    const double kap_pac = opacity_paczynski(stardata,&op,OPACITY_ALGORITHM_PACZYNSKI);
    const double kap_fop = opacity_ferguson_opal(stardata,&op,OPACITY_ALGORITHM_FERGUSON_OPAL);

    printf("%g %g %g %g %g %g\n",
           op.H,
           op.Z,
           op.temperature,
           op.density,
           kap_pac,
           kap_fop);

    fflush(stdout);
    Exit_binary_c(0,"Exit after testing opacity table");
}

#endif // TEST_OPACITY
