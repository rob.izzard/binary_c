#pragma once
#ifndef OPACITY_PROTOTYPES_H
#define OPACITY_PROTOTYPES_H

#ifdef OPACITY_ALGORITHMS

#include "opacity.h"

double opacity(Opacity_function_args_in);

#ifdef OPACITY_ENABLE_ALGORITHM_PACZYNSKI
double opacity_paczynski(Opacity_function_args_in);
#endif

#ifdef OPACITY_ENABLE_ALGORITHM_FERGUSON_OPAL
double opacity_ferguson_opal(Opacity_function_args_in);
#endif

#ifdef OPACITY_ENABLE_ALGORITHM_STARS
double opacity_STARS(Opacity_function_args_in);
#endif

#endif // OPACITY_ALGORITHMS

#endif // OPACITY_PROTOTYPES_H
