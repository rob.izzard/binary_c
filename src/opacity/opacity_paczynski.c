#include "../binary_c.h"
No_empty_translation_unit_warning;

/*
 * Opacity routine based on Paczynski's GOB code (of the 60s)
 */
#if defined OPACITY_ALGORITHMS && \
    defined OPACITY_ENABLE_ALGORITHM_PACZYNSKI
#include "opacity.h"

static Pure_function double kappa(double kap[52][32],
                                  double ro,
                                  double te);

double opacity_paczynski(Opacity_function_args_in)
{
    double kap = kappa(stardata->store->kap_paczynski,
                       op->density,
                       op->temperature);

    Dprint("opacity (paczynski) density=%g temperature=%g -> opacity = %g\n",
           op->density,
           op->temperature,
           kap);

    return kap;
}

static Pure_function double kappa(double kap[52][32],
                                  double ro,
                                  double te)
{
    /*
     * Opacity from gob84 (Paczynski's code)
     */

    double d,t,kapp;
    int di,ti;
    d = 2.0 * log10(ro) + 25.0;
    di = (int) d ;
    d -= di;
    t = 20.0 * log10(te) - 65.0;
    if(t > 35.0) t = 35.0 + ( t - 35.0 ) / 4.0;
    ti = (int) t;
    t -= ti;

    if(di<1)
    {
        di = 1;
        d = 0.0;
    }
    else if(di>30)
    {
        di = 30;
        d = 1.0;
    }

    if(ti < 1)
    {
        ti = 1;
        t = 0.0;
    }
    else if(ti > 50)
    {
        ti = 50;
        t = 1.0;
    }

    int di1 = di + 1;
    int ti1 = ti + 1;
    double d1 = 1.0 - d;
    kapp =
        ( 1.0 - t) *
        ( d1 * kap[ti][di] + d * kap[ti][di1] )
        +
        t * ( d1 * kap[ti1][di] + d * kap[ti1][di1] );

    return exp( 2.3026 * kapp);
}



#endif // OPACITY_ALGORITHMS && OPACITY_ALGORITHM_PACZYNSKI
