#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef KEMP_NOVAE
static double _upper_limit(const double log10Mdot Maybe_unused);
static double _lower_limit(const double log10Mdot Maybe_unused);
static double _3eneg7(const double log10Mdot);
static double _1eneg6(const double log10Mdot);
static double _3eneg6(const double log10Mdot);
static double _6eneg6(const double log10Mdot);
static double _1eneg5(const double log10Mdot);
static double _1p7eneg5(const double log10Mdot);
static double _3eneg5(const double log10Mdot);
static double _6eneg5(const double log10Mdot);
static double _1eneg4(const double log10Mdot);

double Hnova_dMH(struct star_t * const Restrict accretor)
{
    /*
     * from formulae fitted to data from Kato et al. 2014
     * https://ui.adsabs.harvard.edu/abs/2014ApJ...793..136K/abstract
     *
     * Returns: dMH, the critical ignition mass for H novae.
     * (i.e. how much H rich material does a WD of mass M_WD
     * and accretion rate mdot need to accumulate to undergo a
     * nova erruption?)
     *
     * Required units for inputs: M_sun and M_sun/yr
     * for M_WD and the accretion rate respectively.
     */

    /*
     * Limit mdot to the range of the fits
     */
    const double mdot = Max(1e-14,Mdot_gain(accretor));
    const double log10Mdot = log10(mdot);
    const double M_WD = accretor->mass;

    /*
     * FUNCLIST is a list of X macros containing
     * X(dm, function) pairs.
     *
     * Note: the dm is in descending order such that
     *       the masses array is in ascending order
     *       as required for the bisection.
     */
#define FUNCLIST            \
    X(   4e-4, upper_limit) \
    X(   1e-4,      1eneg4) \
    X(   6e-5,      6eneg5) \
    X(   3e-5,      3eneg5) \
    X( 1.7e-5,    1p7eneg5) \
    X(   1e-5,      1eneg5) \
    X(   6e-6,      6eneg6) \
    X(   3e-6,      3eneg6) \
    X(   1e-6,      1eneg6) \
    X(   3e-7,      3eneg7) \
    X(   5e-8, lower_limit)

    /*
     * Make lists of dm (shell masses) and corresponding functions
     */
#undef X
#define X(DM,FUNC) DM,
    static const double dm[] = { FUNCLIST };
#undef X
#define X(DM,FUNC) _ ## FUNC,
    static double (*funcs[])(const double) = { FUNCLIST };
#undef X

    const size_t n = Array_size(dm);
    double * masses = Malloc(sizeof(double)*n);
    for(size_t i = 0; i < n; i++)
    {
        masses[i] = funcs[i](log10Mdot);
    }

    /*
     * Find spanning indices based on current mdot
     */
    const rinterpolate_counter_t low =
        rinterpolate_bisearch(masses,
                              M_WD,
                              n);

    const rinterpolate_counter_t high = low + 1;

    /*
     * Return interpolation
     */
    const double dmH = interp_lin(M_WD,
                                  masses[low],
                                  masses[high],
                                  dm[low],
                                  dm[high]);

    Safe_free(masses);
    return dmH;
}

static double _lower_limit(const double log10Mdot Maybe_unused)
{
    return 1.44;
}

static double _upper_limit(const double log10Mdot Maybe_unused)
{
    return 0.2;
}

static double _3eneg7(const double log10Mdot)
{
    if(log10Mdot >= -7.7)
    {
        return -0.01995254 * Pow2(log10Mdot) - 0.31057708*log10Mdot + 0.17515477;
    }
    else
    {
        return -0.0062 * log10Mdot + 1.3336;
    }
}

static double _1eneg6(const double log10Mdot)
{
    if(log10Mdot >= -9.0)
    {
        return -0.00284309 * Pow3(log10Mdot) - 0.08134364 * Pow2(log10Mdot) - 0.77925207*log10Mdot - 1.13067755;
    }
    else
    {
        return -0.0047 * log10Mdot + 1.3239;
    }
}

static double _3eneg6(const double log10Mdot)
{
    if(log10Mdot >= -9.0)
    {
        return -0.00162294 * Pow3(log10Mdot) - 0.06120773 * Pow2(log10Mdot) - 0.71424226*log10Mdot - 1.34053269;
    }
    else
    {
        return -0.0092 * log10Mdot + 1.2307;
    }
}

static double _6eneg6(const double log10Mdot)
{
    if(log10Mdot >= -9.0)
    {
        return -0.00281513 * Pow3(log10Mdot) - 0.09252102 * Pow2(log10Mdot) - 1.00663332*log10Mdot - 2.35355189;
    }
    else
    {
        return -0.0141 * log10Mdot + 1.1325;
    }
}

static double _1eneg5(const double log10Mdot)
{
    if(log10Mdot >= -9.0)
    {
        return -0.00271579 * Pow3(log10Mdot) - 0.09167551 * Pow2(log10Mdot) - 1.02778587*log10Mdot - 2.59280133;
    }
    else
    {
        return -0.0148 * log10Mdot + 1.0727;
    }
}

static double _1p7eneg5(const double log10Mdot)
{
    if(log10Mdot >= -9.0)
    {
        return -0.00286937 * Pow3(log10Mdot) - 0.09804557 * Pow2(log10Mdot) - 1.11466878*log10Mdot - 3.04208220;
    }
    else
    {
        return -0.0173 * log10Mdot + 0.9779;
    }
}
static double _3eneg5(const double log10Mdot)
{
    if(log10Mdot >= -7.2)
    {
        return -0.05736748*Pow2(log10Mdot) - 0.94076143*log10Mdot - 2.94511325;
    }
    else if(log10Mdot >= -7.7)
    {
        return -0.02513162 * Pow2(log10Mdot) - 0.51596425*log10Mdot - 1.55913869;
    }
    else if(log10Mdot >= -9.0)
    {
        return 0.03007799 * Pow3(log10Mdot) + 0.69444233 * Pow2(log10Mdot) + 5.20316206*log10Mdot + 13.54164455;
    }
    else
    {
        return -0.0224 * log10Mdot + 0.8371;
    }
}

static double _6eneg5(const double log10Mdot)
{
    if(log10Mdot >= -7.2)
    {
        return -0.05119815*Pow2(log10Mdot) - 0.84433509*log10Mdot - 2.70816224;
    }
    else if(log10Mdot >= -9.0)
    {
        return -0.00223874 * Pow3(log10Mdot) - 0.07980979 * Pow2(log10Mdot) - 0.95727391*log10Mdot - 2.88226600;
    }
    else
    {
        return -0.0246 * log10Mdot + 0.6755;
    }
}
static double _1eneg4(const double log10Mdot)
{
    if(log10Mdot >= -7.2)
    {
        return -0.04596643*Pow2(log10Mdot) - 0.77067908*log10Mdot - 2.54725295;
    }
    else if(log10Mdot >= -7.7)
    {
        return -0.01136821 * Pow2(log10Mdot) - 0.28779422*log10Mdot - 0.87275137;
    }
    else if(log10Mdot >= -9.0)
    {
        return 0.03123628 * Pow3(log10Mdot) + 0.75350110 * Pow2(log10Mdot) + 5.94211003*log10Mdot + 16.00580843;
    }
    else
    {
        return -0.0225 * log10Mdot + 0.5856;
    }
}
#endif // KEMP_NOVAE
