#pragma once
#ifndef NOVA_PARAMETERS_H
#define NOVA_PARAMETERS_H

#include "nova_parameters.def"

#undef X
#define X(CODE) NOVA_RETENTION_ALGORITHM_##CODE,
enum { NOVA_RETENTION_ALGORITHMS };


#undef X
#define X(CODE,STRING) NOVA_STATE_##CODE,
enum { NOVA_STATES_LIST };
#undef X
#define X(CODE,STRING) STRING,
static const char * const nova_state_strings[] Maybe_unused = { NOVA_STATES_LIST };

#undef X
#define X(CODE) NOVA_TYPE_##CODE,
enum { NOVA_TYPES_LIST };
#undef X

#endif // NOVA_PARAMETERS_H
