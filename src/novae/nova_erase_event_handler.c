#include "../binary_c.h"
No_empty_translation_unit_warning;


/*
 * Event handling function for novae: this deals with
 * an erase action
 */

Event_handler_function nova_erase_event_handler(void * eventp Maybe_unused,
                                                struct stardata_t * stardata Maybe_unused,
                                                void * data Maybe_unused)
{
//    printf("nova erase event handler\n");
    return NULL;
}
