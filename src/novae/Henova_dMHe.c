#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef KEMP_NOVAE
static double _144(const double Mdotlog10 Maybe_unused);
static double _138(const double Mdotlog10);
static double _135(const double Mdotlog10);
static double _130(const double Mdotlog10);
static double _125(const double Mdotlog10);
static double _120(const double Mdotlog10);
static double _110(const double Mdotlog10);
static double _102(const double Mdotlog10);
static double _92(const double Mdotlog10);
static double _81(const double Mdotlog10);
static double _70(const double Mdotlog10);
static double _60(const double Mdotlog10);
static double _20(const double Mdotlog10 Maybe_unused);

double Henova_dMHe(struct star_t * const Restrict accretor)
{
    /*
     * From formulae fitted to data from Kato et al. 2018
     * https://ui.adsabs.harvard.edu/abs/2018ApJ...863..125K/abstract, Figure 1,
     * data obtained from M. Kato, private communication) for M_WD>1.0.
     * From formulae fitted to data from Piersanti et al. 2014
     * https://ui.adsabs.harvard.edu/abs/2014MNRAS.445.3239P/abstract
     * (supplementary data B7) for M_WD <1.0
     *
     * Note on variable naming: higher && lower in the 'fit' variable
     * name refers to the mass of the WD curve
     * (e.g. a 1.38 Msun WD curve will be the 'higher', compared
     * to a 1.35 Msun WD curve.)
     *
     * returns: dMHe, the critical ignition mass for He deflagrations
     *          (strong flash regime, see Piersanti et al. 2014).
     *          For dMHe in the detonation regime (Piersanti et al. 2014),
     *          Hedet_dMHe.c should be used.
     *
     * Required units for inputs: M_sun and M_sun/yr
     * (for M_WD and the accretion rate respectively)
     *
     * Note: we assume MCh = 1.44
     */

    const double mdot = Max(1e-10,Mdot_gain(accretor));
    const double log10Mdot = log10(mdot);
    const double M_WD = Limit_range(accretor->mass,0.2,1.44);

    /*
     * FUNCLIST X macros are a pair:
     * X(MASS, FUNC)
     * where FUNC corresponds to a function _FUNC
     */
#define FUNCLIST  \
    X( 0.20,  20) \
    X( 0.60,  60) \
    X( 0.70,  70) \
    X( 0.81,  81) \
    X( 0.92,  92) \
    X( 1.02, 102) \
    X( 1.10, 110) \
    X( 1.20, 120) \
    X( 1.25, 125) \
    X( 1.30, 130) \
    X( 1.35, 135) \
    X( 1.38, 138) \
    X( 1.44, 144) \

    /*
     * Make lists of masses and corresponding functions
     */
#undef X
#define X(MASS,FUNC) MASS,
    static const double masses[] = { FUNCLIST };
#undef X
#define X(MASS,FUNC) _ ## FUNC,
    static double (*funcs[])(const double) = { FUNCLIST };
#undef X

    /*
     * Find spanning indices based on current mdot
     */
    const rinterpolate_counter_t low =
        rinterpolate_bisearch(masses,
                              M_WD,
                              Array_size(masses));
    const rinterpolate_counter_t high = low + 1;

    /*
     * Return interpolation
     */
    return exp10(interp_lin(M_WD,
                            masses[low],
                            masses[high],
                            funcs[low](log10Mdot),
                            funcs[high](log10Mdot)));
}


static double _138(const double log10Mdot)
{
    if(log10Mdot < -6.0)
    {
        return 0.54437094*Pow4(log10Mdot) + 14.68324636*Pow3(log10Mdot) +
            148.24381872*Pow2(log10Mdot) + 663.22092056*log10Mdot + 1103.57417218;
    }
    else
    {
        return -0.1056*log10Mdot - 5.6752;
    }
}

static double _135(const double log10Mdot)
{
    if(log10Mdot < -6.0)
    {
        return 0.47596972*Pow4(log10Mdot) + 12.61002433*Pow3(log10Mdot)
            + 125.08958464*Pow2(log10Mdot) + 549.88858261*log10Mdot + 898.29715596;
    }
    else
    {
        return -0.0934*log10Mdot - 5.3203;
    }
}

static double _130(const double log10Mdot)
{
    if(log10Mdot < -6.0)
    {
        return 1.16410162*Pow4(log10Mdot) + 30.86542279*Pow3(log10Mdot)
            + 306.21947654*Pow2(log10Mdot) + 1346.49353679*log10Mdot + 2208.95849065;
    }
    else
    {
        return -0.1574*log10Mdot - 5.3083;
    }
}

static double _125(const double log10Mdot)
{
    if(log10Mdot < -6.0)
    {
        return 1.68244304*Pow4(log10Mdot) + 45.15739606*Pow3(log10Mdot)
            + 453.76725009*Pow2(log10Mdot) + 2022.35971770*log10Mdot + 3368.09376231;
    }
    else
    {
        return -0.1274*log10Mdot - 4.7732;
    }
}

static double _120(const double log10Mdot)
{

    if(log10Mdot < -6.0)
    {
        return 0.91755524*Pow4(log10Mdot) + 24.10100929*Pow3(log10Mdot)
            + 236.98730163*Pow2(log10Mdot) + 1033.14600808*log10Mdot + 1680.17435446;
    }
    else
    {
        return -0.0542*log10Mdot - 4.1937;
    }
}


static double _110(const double log10Mdot)
{
    if(log10Mdot < -6.0)
    {
        return 1.72662751*Pow4(log10Mdot) + 45.88762486*Pow3(log10Mdot)
            + 456.63345197*Pow2(log10Mdot) + 2015.69281770*log10Mdot + 3325.89047580;
    }
    else
    {
        return -0.0937*log10Mdot - 4.0218;
    }
}


static double _102(const double log10Mdot)
{
    if(log10Mdot > -7.09)
    {
        return -0.9181*log10Mdot - 8.6514;
    }
    else
    {
        return -2.0406*log10Mdot - 16.597;
    }
}

static double _92(const double log10Mdot)
{
    if(log10Mdot > -7.30)
    {
        return -0.8228*log10Mdot - 7.8311;
    }
    else
    {
        return -2.0714*log10Mdot - 16.929;
    }
}

static double _81(const double log10Mdot)
{
    if(log10Mdot > -7.40)
    {
        return -1.2462*log10Mdot - 10.729;
    }
    else
    {
        return -2.7234*log10Mdot - 21.695;
    }
}

static double _70(const double log10Mdot)
{
    if(log10Mdot > -7.39)
    {
        return -0.9928*log10Mdot - 8.8399;
    }
    else
    {
        return -2.8393*log10Mdot - 22.535;
    }
}

static double _60(const double log10Mdot)
{
    if(log10Mdot > -7.39)
    {
        return -1.2658*log10Mdot - 10.79;
    }
    else
    {
        return -2.3314*log10Mdot - 18.668;
    }
}

static double _20(const double log10Mdot Maybe_unused)
{
    return 0.2;
}


static double _144(const double log10Mdot Maybe_unused)
{
    return log10(8e-7); // was M_Heig_chand = log(8e-7);
}

#endif // KEMP_NOVAE
