#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef KEMP_NOVAE

static double _20(const double log10Mdot);
static double _70(const double log10Mdot);
static double _80(const double log10Mdot);
static double _90(const double log10Mdot);
static double _100(const double log10Mdot);
static double _110(const double log10Mdot);
static double _120(const double log10Mdot);
static double _125(const double log10Mdot);
static double _130(const double log10Mdot);
static double _135(const double log10Mdot);
static double _144(const double log10Mdot);

double Henova_etaHe(struct stardata_t * Restrict const stardata,
                    struct star_t * const Restrict accretor)
{
    /*
     * based on data from Wu et al. 2017
     * https://ui.adsabs.harvard.edu/abs/2017A%26A...604A..31W/abstract
     *
     * note on variable naming: higher && lower in the 'fit'
     * variable name refers to the mass of the WD curve
     * (e.g. a 1.38 Msun WD curve will be the 'higher', compared to a
     * 1.35 Msun WD curve.)
     *
     * returns: eta_He, the accretion efficiency (nova retention
     * fraction) for a H nova at a given M_WD and Mdot.
     *
     * Required units for inputs: M_sun and M_sun/yr
     * (for M_WD and the accretion rate respectively)
     */
    const double mdot = Mdot_net(accretor);

    if(mdot <= 0.0)
    {
        return 0.0;
    }
    else
    {
        const double log10Mdot = log10(mdot);
        const double M_WD = Limit_range(accretor->mass,0.2,1.44);

        /*
         * Interpolate based on mass
         */
#define FUNCLIST \
    X(  20)      \
    X(  70)      \
    X(  80)      \
    X(  90)      \
    X( 100)      \
    X( 110)      \
    X( 120)      \
    X( 125)      \
    X( 130)      \
    X( 135)      \
    X( 144)

#undef X
#define X(M100) (0.01*(M100)),
        static const double masses[] = { FUNCLIST };
#undef X
#define X(M100) _ ## M100,
        static double (*funcs[])(const double) = { FUNCLIST };
#undef X

        /*
         * Find spanning indices based on current mdot
         */
        const rinterpolate_counter_t low =
            rinterpolate_bisearch(masses,
                                  M_WD,
                                  Array_size(masses));

        const rinterpolate_counter_t high = low + 1;

        /*
         * Return interpolation, shifted by nova_eta_shift
         * and limited to the range [0,1].
         */
        return
            Limit_range(interp_lin(M_WD,
                                   masses[low],
                                   masses[high],
                                   funcs[low](log10Mdot),
                                   funcs[high](log10Mdot)) +
                        stardata->preferences->nova_eta_shift,
                        0.0,
                        1.0);
    }
}

static double _20(const double log10Mdot)
{
    if(log10Mdot > -7.421)
    {
        return 128.69653622*Pow4(log10Mdot) + 3755.62839546*Pow3(log10Mdot) +
            41097.77848236*Pow2(log10Mdot) + 199876.15570843*log10Mdot + 364523.39925814;
    }
    else
    {
        return 0.0284*log10Mdot + 0.4944;
    }
}
static double _70(const double log10Mdot)
{
    if(log10Mdot > -6.921)
    {
        return 128.69653622*Pow4(log10Mdot) + 3498.23532301*Pow3(log10Mdot) +
            35657.38069353*Pow2(log10Mdot) + 161530.75025466*log10Mdot + 274398.30600877;
    }
    else
    {
        return 0.0271*log10Mdot + 0.4214;
    }
}
static double _80(const double log10Mdot)
{
    if(log10Mdot > -6.495)
    {
        return -2.81727160*Pow4(log10Mdot) + 842.92305137*Pow3(log10Mdot) +
            17025.57779911*Pow2(log10Mdot) + 111404.51221858*log10Mdot + 241315.71447017;
    }
    else
    {
        return 0.0376*log10Mdot + 0.5774;
    }
}
static double _90(const double log10Mdot)
{
    if(log10Mdot > -6.585)
    {
        return 371.49410540*Pow4(log10Mdot) + 9601.91634884*Pow3(log10Mdot) +
            93061.48887580*Pow2(log10Mdot) + 400844.20977850*log10Mdot + 647425.88293061;
    }
    else
    {
        return 0.0328*log10Mdot + 0.4598;
    }
}
static double _100(const double log10Mdot)
{
    if(log10Mdot > -6.155)
    {
        return 8.3115*log10Mdot + 51.674;
    }
    else if(log10Mdot > -7.046)
    {
        return 0.42950525*Pow2(log10Mdot) + 6.08106227*log10Mdot + 21.66204702;
    }
    else
    {
        return 0.0274*log10Mdot + 0.331;
    }
}
static double _110(const double log10Mdot)
{
    if(log10Mdot > -5.921)
    {
        return 8.3856*log10Mdot + 50.358;
    }
    else if(log10Mdot>-7.097)
    {
        return -0.19075473*Pow4(log10Mdot) - 4.92869779*Pow3(log10Mdot) -
            47.22636728*Pow2(log10Mdot) - 198.28033421*log10Mdot - 306.30153647;
    }
    else
    {
        return 0.0275*log10Mdot + 0.2987;
    }
}
static double _120(const double log10Mdot)
{
    if(log10Mdot > -5.854)
    {
        return 6.7216*log10Mdot + 40.146;
    }
    else if(log10Mdot > -7.222)
    {
        return 0.61101079*Pow4(log10Mdot) + 16.16260323*Pow3(log10Mdot) +
            160.39740390*Pow2(log10Mdot) + 708.15701427*log10Mdot + 1174.46712731;
    }
    else
    {
        return 0.0278*log10Mdot + 0.2994;
    }
}
static double _125(const double log10Mdot)
{
    if(log10Mdot > -5.824)
    {
        return 7.3403*log10Mdot + 43.604;
    }
    else if(log10Mdot > -7.222)
    {
        return 0.70445443*Pow4(log10Mdot) + 18.61070131*Pow3(log10Mdot) +
            184.34484390*Pow2(log10Mdot) + 811.82891151*log10Mdot + 1342.09332478;
    }
    else
    {
        return 0.0278*log10Mdot + 0.2924;
    }
}
static double _130(const double log10Mdot)
{
    if(log10Mdot>-5.824)
    {
        return 5.2446*log10Mdot + 31.397;
    }
    else if(log10Mdot > -7.222)
    {
        return 0.35511452*Pow4(log10Mdot) + 9.43073062*Pow3(log10Mdot) +
            93.97568200*Pow2(log10Mdot) + 416.89622203*log10Mdot + 695.71187519;
    }
    else
    {
        return 0.0278*log10Mdot + 0.2754;
    }
}
static double _135(const double log10Mdot)
{
    if(log10Mdot > -5.824)
    {
        return 4.742*log10Mdot + 28.497;
    }
    else if(log10Mdot > -7.301)
    {
        return 0.17534165*Pow2(log10Mdot) + 2.81395280*log10Mdot + 11.31042978;
    }
    else
    {
        return 0.028*log10Mdot + 0.315;
    }
}
static double _144(const double log10Mdot)
{
    if(log10Mdot > -5.824)
    {
        return 4.742*log10Mdot + 28.597;
    }
    else if(log10Mdot > -7.301)
    {
        return 0.17534165*Pow2(log10Mdot) + 2.81395280*log10Mdot + 11.41042978;
    }
    else
    {
        return 0.028*log10Mdot + 0.415;
    }
}
#endif // KEMP_NOVAE
