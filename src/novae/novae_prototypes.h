#pragma once
#ifndef NOVAE_PROTOTYPES_H
#define NOVAE_PROTOTYPES_H


Constant_function double nova_recurrence_time(struct star_t * const accretor,
                                              const double mdot,
                                              const double mwd);

double Pure_function nova_explosion_layer_mass(struct stardata_t * Restrict const stardata Maybe_unused,
                                               struct star_t * const accretor);


double Pure_function nova_retention_fraction(struct stardata_t * const stardata,
                                             struct star_t * const accretor,
                                             struct star_t Maybe_unused * const donor,
                                             const double accretion_rate Maybe_unused,
                                             const double steady_burn_rate Maybe_unused);

double Pure_function dwarf_nova_critical_mass_transfer_rate(struct stardata_t * const stardata,
                                                            struct star_t * const donor,
                                                            struct star_t * const accretor);

Event_handler_function nova_event_handler(void * const eventp Maybe_unused,
                                          struct stardata_t * const stardata Maybe_unused,
                                          void * data Maybe_unused);

Event_handler_function nova_erase_event_handler(void * eventp,
                                                     struct stardata_t * stardata,
                                                void * data);

void nova_angular_momentum_changes(
    struct stardata_t * const stardata,
    struct star_t * const exploder,
    const double dm,
    double * const dJstar,
    double * const dJorb
    );

#ifdef KEMP_NOVAE

double Hnova_dMH(struct star_t * const Restrict accretor);
double Henova_dMHe(struct star_t * const Restrict accretor);
double Hnova_etaH(struct stardata_t * Restrict const stardata,
                  struct star_t * const accretor);
double Henova_etaHe(struct stardata_t * Restrict const stardata,
                    struct star_t * const Restrict accretor);

#endif // KEMP_NOVAE



#endif // NOVAE_PROTOTYPES_H
