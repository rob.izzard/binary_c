#include "../binary_c.h"
No_empty_translation_unit_warning;


Constant_function double nova_recurrence_time(struct star_t * const accretor Maybe_unused,
                                              const double mdot,
                                              const double mwd Maybe_unused)
{
    /*
     * Recurrence time for novae of mass mwd (solar masses) accreting at
     * a rate mdot (Msun/year).  See Livio and Truran ApJ 389, 695, 1992, Eq. 2
     *
     * Result is in years.
     */
#ifdef KEMP_NOVAE
    double t;

    /*
     * K2014 is Kato et al. 2014
     * https://ui.adsabs.harvard.edu/abs/2014ApJ...793..136K/abstract
     *
     * P2014 is Piersanti et al. 2014
     * https://ui.adsabs.harvard.edu/abs/2014MNRAS.445.3239P/abstract
     *
     * K2018 is Kato et al. 2018
     * https://ui.adsabs.harvard.edu/abs/2018ApJ...863..125K/abstract
     */

    double dmcrit;
    if(accretor->WD_accretion_regime==STRONGFLASH_H)
    {
        /*
         * uses K2014
         */
        dmcrit = Hnova_dMH(accretor);
    }
    else if(accretor->WD_accretion_regime==STRONGFLASH_He)
    {
        /*
         * uses P2014 and K2018
         */
        dmcrit = Henova_dMHe(accretor);
    }
    else if(accretor->WD_accretion_regime == DETONATION_He)
    {
        /*
         * a precaution, also potentially used
         * for immortal white dwarfs
         */
        dmcrit = Hedet_dMHe(accretor);
    }
    else
    {
        dmcrit = 1e20;
    }
    t = Is_really_zero(mdot) ? LONG_TIMESTEP : ( dmcrit / mdot );
#ifdef NOVA_DEBUG
    printf("H-nova recurrence dmcrit=%g / mdot=%g = %g y\n",dmcrit,mdot,t);
#endif
#else
    const double t =
        Is_really_zero(mdot) ? 1e50 :
        8.66e4 * (1e-9/mdot) *
        pow((1.54 * pow(mwd,-7.0/3.0)
             - 2.0 / mwd
             +0.65 * cbrt(mwd)),0.7);

#endif // KEMP_NOVAE
    return t;
}
