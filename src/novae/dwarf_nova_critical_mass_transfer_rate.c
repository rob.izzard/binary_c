#include "../binary_c.h"
No_empty_translation_unit_warning;


double Pure_function dwarf_nova_critical_mass_transfer_rate(struct stardata_t * const stardata,
                                                            struct star_t * const donor,
                                                            struct star_t * const accretor)
{
    /*
     * The mass transfer rate below which dwarf novae
     * occur.
     *
     * Note: Returns a POSITIVE result in Msun/year.
     *
     * Faulkner, Lin & Papaloizou, 1983, MNRAS 205, 359.
     */
    const double q = donor->mass / accretor->mass;

    const double alphaH = 0.1;

    /* mdot in grams / second */
    const double mdot =
        8.08e15 * pow(alphaH/0.3, 0.3) *
        pow(1.0 + q, 7.0/8.0) *
        pow(stardata->common.orbit.period * YEAR_LENGTH_IN_HOURS, 7.0/4.0)

        /* convert to Msun/year */
        * YEAR_LENGTH_IN_SECONDS / M_SUN;

    return mdot;
}
