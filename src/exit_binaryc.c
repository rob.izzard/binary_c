#define __BINARY_C_LINT_SKIP

/*
 * The binary_c stellar population nucleosynthesis framework.
 *
 * Copyright Robert Izzard 2000-2025
 *
 * Contact rob.izzard@gmail.com
 *
 * Code repository:
 * https://gitlab.com/binary_c
 *
 * Documentation is in doc/
 * Code is in src/
 *
 * To build, run
 *
 * ---
 *
 * meson setup builddir
 * cd builddir
 * ninja binary_c_install
 *
 * ---
 *
 * Please see the files README, LICENCE and CHANGES,
 * and the doc/ directory for documentation which is mirrored
 * at https://binary_c.gitlab.io
 *
 **********************
 *
 * Subroutine: exit_binary_c_handler (static)
 *
 * Purpose: Static function that handles the shutdown of binary_c cleanly.
 *          Note: you should not call this function, you should use the
 *          Exit_binary_c() macro.
 *          You can use
 *
 *    e.g.
 *       Exit_binary_c(binary_c_error_code, format-string, <arguments>);
 *
 * Arguments:
 *     stardata : binary_c's stardata struct (should be initialized)
 *     filename : the file from which Exit_binary_c was called.
 *     fileline : the line of the file at which Exit_binary_c was called.
 *     errsv    : C's errno (not the same as binary_c's error code)
 *     binary_c_error_code : binary_c's error code
 *     pre_format : a format statement used to output to stderr prior
 *                to the main format statement.
 *     pre_args : arguments used with pre_format
 *     args     : va_list of arguments to be sent to vfprintf for output
 *
 * Returns:
 *
 **********************
 *
 * Subroutine: exit_binary_c_with_stardata
 *
 * Purpose: Exits binary_c when you have a stardata available.
 *
 * Arguments: See exit_binary_c_handler
 *
 * Returns: nothing
 *
 **********************
 *
 * Subroutine: vexit_binary_c_with_stardata
 *
 * Purpose: As exit_binary_c_with_stardata but handles a va_list
 *          of pre_args
 *
 * Arguments: See exit_binary_c_handler
 *
 * Returns: nothing
 *
 **********************
 *
 * Subroutine: exit_binary_c_no_stardata
 *
 * Purpose: As exit_binary_c_with_stardata but without a stardata
 *          struct (sets this to NULL when calling exit_binary_c_handler)
 *
 * Arguments: See exit_binary_c_handler
 *
 * Returns: nothing
 *
 **********************
 * Note:
 * Please do not change the unusual header file list.
 */

/*
 * Generic code exit function which attempts some clean up of memory.
 */

#include <stdlib.h>
#include <stdarg.h>

#ifdef _GNU_SOURCE
#define GNU_SOURCE_WAS _GNU_SOURCE
#endif

#define _GNU_SOURCE
#include <stdio.h>
#undef __USE_GNU
#undef _GNU_SOURCE
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include "binary_c.h"

#undef Dprint
/*
#if defined DEBUG && DEBUG!=0
#define Dprint(...) fprintf(stderr,__VA_ARGS__);
#else
*/
#define Dprint(...) /* do nothing */
//#define Dprint(...) fprintf(stdout,__VA_ARGS__);
//#endif

static void
Gnu_format_args(8,0)
    exit_binary_c_handler(struct stardata_t * stardata,
                          const char * const filename,
                          const int fileline,
                          const int errsv Maybe_unused,
                          const int binary_c_error_code,
                          const char * const pre_format,
                          va_list pre_args,
                          const char * const format,
                          va_list args
        )
    No_return;


void Gnu_format_args(6,7)
    binary_c_API_function
    No_return
    exit_binary_c_with_stardata(struct stardata_t * stardata,
                                const char * const filename,
                                const int fileline,
                                const int errsv Maybe_unused,
                                const int binary_c_error_code,
                                const char * const format,
                                ...)
{
    /*
     * Function to handle the shutdown of BINARY_C in a clean manner:
     * You should never call this function directly, instead use the
     * Exit_binary_c macro.
     *
     * You must always call Exit_binary_c as:
     *
     * Exit_binary_c(binary_c_error_code, format-string, <arguments>)
     *
     * where
     * + the binary_c_error_code is defined in binary_c_macros.h
     * + the format string is appropriate for passing to printf
     * + the (optional) arguments requried for the format are also given.
     *
     * e.g.
     *
     * Exit_binary_c(10,"Code exited because mass = %g\n",mass);
     *
     * The error message is shown if binary_c_error_code is non-zero, or if
     * debugging is switched on (DEBUG=1)
     *
     * Note that errsv is a saved version of the C library's "errno"
     * variable. It is NOT the same as binary_c_error_code which is
     * a number defined by binary_c_error_codes.h
     *
     * See also vExit_binary_c and vexit_binary_c_with_stardata which
     * are identical but take a va_list as an argument.
     */
    Dprint("In Exit_binary_c : setting up varargs, stardata=%p\n",
           (void*)stardata);
    if(stardata!=NULL)
    {
        Dprint("preferences = %p\n",(void*)stardata->preferences);
#ifdef BATCHMODE
        Dprint("batchmode = %d\n",
               (stardata->preferences != NULL ? stardata->preferences->batchmode : BATCHMODE_ERROR));
#endif //BATCHMODE
    }

    Dprint("calling exit_binary_c_handler\n");
    va_list args,dummy Maybe_unused;
    va_start(args,format);
    va_start(dummy,format);
    exit_binary_c_handler(stardata,
                          filename,
                          fileline,
                          errsv,
                          binary_c_error_code,
                          NULL,
                          dummy,
                          format,
                          args
        );
    va_end(args);
    va_end(dummy);
}

void Gnu_format_args(6,0) Gnu_format_args(8,9)
    binary_c_API_function
    No_return vexit_binary_c_with_stardata(struct stardata_t * stardata,
                                           const char * const filename,
                                           const int fileline,
                                           const int errsv Maybe_unused,
                                           const int binary_c_error_code,
                                           const char * const pre_format,
                                           va_list pre_args,
                                           const char * const format,
                                           ...)
{
    /*
     * As exit_binary_c_with_stardata taking the va_list directly
     */
    Dprint("In vExit_binary_c : setting up varargs, stardata=%p, pre_format = \"%s\", format = \"%s\"\n",
           (void*)stardata,
           pre_format,
           format
        );
    if(stardata!=NULL)
    {
        Dprint("preferences = %p\n",(void*)stardata->preferences);
#ifdef BATCHMODE
        Dprint("batchmode = %d\n",
               (stardata->preferences != NULL ? stardata->preferences->batchmode : BATCHMODE_ERROR));
#endif //BATCHMODE
    }

    va_list args;
    va_start(args,format);
    exit_binary_c_handler(stardata,
                          filename,
                          fileline,
                          errsv,
                          binary_c_error_code,
                          pre_format,
                          pre_args,
                          format,
                          args
        );
    va_end(args);
}


void Gnu_format_args(5,6)
    No_return
    binary_c_API_function
    exit_binary_c_no_stardata(const char * const filename,
                              const int fileline,
                              const int errsv Maybe_unused,
                              const int binary_c_error_code,
                              const char * const format,
                              ...)
{
    va_list args;
    va_list dummy;
    va_start(args,format);
    va_start(dummy,format);
    exit_binary_c_handler(NULL,
                          filename,
                          fileline,
                          errsv,
                          binary_c_error_code,
                          NULL,
                          dummy,
                          format,
                          args);
    va_end(args);
    va_end(dummy);
}


static void No_return Gnu_format_args(8,0)
    exit_binary_c_handler(struct stardata_t * stardata,
                          const char * const filename,
                          const int fileline,
                          const int errsv Maybe_unused,
                          const int binary_c_error_code,
                          const char * const pre_format,
                          va_list pre_args,
                          const char * const format,
                          va_list args
        )
{
    char s[MAX_EXIT_STATEMENT_PRINT_SIZE];
    if(format != NULL)
    {
        vsnprintf(s,MAX_EXIT_STATEMENT_PRINT_SIZE,format,args);
        chomp(s);
    }
    else
    {
        s[0] = '\0';
    }
    const Boolean longjump = (binary_c_error_code != BINARY_C_NORMAL_BATCHMODE_EXIT &&
                              binary_c_error_code != BINARY_C_SPECIAL_EXIT)
#ifdef BATCHMODE
        && (stardata!=NULL)
        && (stardata->preferences != NULL)
        && (Batchmode_is_on(stardata->preferences->batchmode))
#else
        && 0
#endif // BATCHMODE
        ;


    Dprint("Check batchmode : longjump = %d from %p %p %d %d\n",
           longjump,
           (void*)stardata,
           (void*)(stardata!=NULL ? stardata->preferences : NULL),
           (stardata!=NULL && stardata->preferences!=NULL) ? stardata->preferences->batchmode : -1,
           (stardata!=NULL && stardata->preferences!=NULL) ? Batchmode_is_on(stardata->preferences->batchmode) : -1
        );

    if(stardata)
    {
        stardata->error_code = binary_c_error_code;
    }

    if(longjump)
    {
        /* in batchmode, we don't want to exit, just print a message */
        Dprint("batchmode : but we don't want to quit");
        if(binary_c_error_code != BINARY_C_SPECIAL_EXIT)
        {
            fflush(NULL);
            char * argstring = cmd_line_argstring(stardata);

            /* output system error with _printf */
            _printf("SYSTEM_ERROR file %s, line %d, with error code %d: %s (errno=%d) random_seed: "Random_seed_format", args: %s\n",
                    filename,
                    fileline,
                    binary_c_error_code,
                    s,
                    errno,
                    stardata->common.init_random_seed,
                    argstring);
            if(pre_format != NULL)
            {
                va_list pre_args_copy;
                va_copy(pre_args_copy,pre_args);
                vprintf(pre_format,pre_args);
                va_end(pre_args_copy);
            }
            _printf("\n");

            /* now with Printf */
            Printf("SYSTEM_ERROR file %s, line %d, with error code %d: %s (errno=%d) random_seed: "Random_seed_format", args: %s",
                   filename,
                   fileline,
                   binary_c_error_code,
                   s,
                   errno,
                   stardata->common.init_random_seed,
                   argstring
                );
            if(pre_format != NULL)
            {
                va_list pre_args_copy;
                va_copy(pre_args_copy,pre_args);
                vPrintf(pre_format,pre_args);
                va_end(pre_args_copy);
            }
            Printf("\n");
            free(argstring);
        }

        /* flush all files, close logs, longjmp back to batchmode function */
        Dprint("batchmode : long jump to %p\n",
               (void*)stardata->batchmode_setjmp_buf);
        fflush(NULL);
        stardata->store->debug_stopping = 0;
        longjmp(stardata->batchmode_setjmp_buf,1);
    }
    else
    {
        /*
         * Non-batchmode or batchmode is off : actually exit
         */
        Dprint("binary_c : we really want to exit (not a batchmode long jump)\n");
        if(Binary_c_error_code_is_bad(binary_c_error_code) || (DEBUG && Debug_expression))
        {
            /*
             * If stdout is not a terminal, e.g. it's a file,
             * output to it
             */
            if(!isatty(fileno(stdout)))
            {
                _printf("Exit binary_c from file %s, line %d, model %d at time %g, with error code %d: %s (errno=%d) ",
                        filename,
                        fileline,
                        stardata ? stardata->model.model_number : -1,
                        stardata ? stardata->model.time : -1.0,
                        binary_c_error_code,
                        s,
                        errno);
                if(pre_format != NULL)
                {
                    va_list pre_args_copy;
                    va_copy(pre_args_copy,pre_args);
                    vfprintf(stdout,pre_format,pre_args);
                    va_end(pre_args_copy);
                    fprintf(stdout,"\n");
                }
                fprintf(stdout,"\n");
                if(stardata !=NULL &&
                   stardata->preferences != NULL &&
                   stardata->preferences->exit_backtrace == TRUE)
                {
                    Print_trace(stdout);
                    fflush(NULL);
                }
            }

            /*
             * Always output to stderr
             */
            fprintf(stderr,
                    "Exit binary_c from file %s, line %d, model %d at time %g, with error code %d: %s (errno=%d) ",
                    filename,
                    fileline,
                    stardata ? stardata->model.model_number : -1,
                    stardata ? stardata->model.time : -1.0,
                    binary_c_error_code,
                    s,
                    errno);

            if(pre_format != NULL)
            {
                va_list pre_args_copy;
                va_copy(pre_args_copy,pre_args);
                vfprintf(stderr,pre_format,pre_args);
                va_end(pre_args_copy);
            }
            fprintf(stderr,"\n");
            if(stardata != NULL &&
               stardata->preferences != NULL &&
               stardata->preferences->exit_backtrace == TRUE)
            {
                Print_trace(stderr);
                fflush(NULL);
            }
        }

        /*
         * Free all allocated memory, including the preferences, store,
         * tmpstore and persistent data.
         *
         * Note: we should disable debugging at this point, otherwise
         *       we might end up in an infinite loop if there's a NaN
         *       in the debug statement.
         */
        if(stardata!=NULL)
        {
            if(stardata->preferences!=NULL)
            {
                stardata->preferences->disable_debug = TRUE;
            }
            free_memory(&stardata,TRUE,TRUE,TRUE,TRUE,TRUE);
        }
#undef exit

        if(binary_c_error_code == BINARY_C_NORMAL_EXIT ||
           binary_c_error_code == BINARY_C_SPECIAL_EXIT ||
           binary_c_error_code == BINARY_C_QUIET_EXIT ||
           binary_c_error_code == BINARY_C_NORMAL_BATCHMODE_EXIT)
        {
            if(binary_c_error_code != BINARY_C_QUIET_EXIT)
            {
                if(binary_c_error_code == BINARY_C_NORMAL_BATCHMODE_EXIT)
                {
                    _printf("Normal exit from binary_c (batchmode)\n");
                }
                else
                {
                    _printf("Normal exit from binary_c\n");
                }
            }

            /*
             * All the above cases should return 0, i.e.
             * BINARY_C_NORMAL_EXIT, so that Meson's unit tests
             * do not fail.
             */
            const char * const meson = getenv("MESON_UNIT_TEST");
            if(meson)
            {
                exit(BINARY_C_NORMAL_EXIT);
            }
            else
            {
                exit(binary_c_error_code);
            }
        }
        else
        {
            exit(binary_c_error_code);
        }
    }
}


#ifdef GNU_SOURCE_WAS
#undef _GNU_SOURCE
#define _GNU_SOURCE GNU_SOURCE_WAS
#endif
