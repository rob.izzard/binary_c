#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "tides.h"
/*
 * A generic form of tides, callable for stars and
 * orbiting objects.
 *
 * "object" is the orbiting object, uses the "orbit" struct
 * for its separation, eccentricity, orbital angular frequency
 * and orbital angular momentum, etc.
 *
 * "star" is the star in which tidal energy is dissipated
 *
 * If clear_orbit is TRUE we set the orbital derivatives
 * to zero at the start of the function. This is probably
 * NOT what you want to do.
 */
//#define Tprint(...) printf("TIDES: ");printf(__VA_ARGS__);
#define Tprint(...) Dprint(__VA_ARGS__);

static double slm(const double l,
                  const double m,
                  const double omega_orb,
                  const double omega_spin,
                  const double R,
                  const double M);
static double slm(const double l,
                  const double m,
                  const double omega_orb,
                  const double omega_spin,
                  const double R,
                  const double M)
{
    /*
     * Zahn's s_lm constant, where M,R are in
     * Solar units, and omega_orb and omega_spin are
     * in whatever units.
     */
    const double s = (l * omega_orb - m * omega_spin) * sqrt(Pow3(R)/M);
    printf("l %g, m %g, omega_orb %g, omega_spin %g, R %g Rsun, M %g Msun -> s_%g%g %g\n",
           l,
           m,
           omega_orb,
           omega_spin,
           R,
           M,
           l,
           m,
           s);
    return s;
}

#define Slm(_L,_M) slm(1.0*(_L),                    \
                       1.0*(_M),                    \
                       orbit->angular_frequency,    \
                       star->omega,                 \
                       star->radius,                \
                       star->mass);
#define Xlm(_L,_M)                                                      \
            __extension__                                               \
                ({                                                      \
                    const double _s = Slm((_L),(_M));                   \
                    pow(_s,8.0/3.0) * Sign(_s) / YEAR_LENGTH_IN_SECONDS; \
                })

void tides_generic(struct stardata_t * const stardata,
                   struct star_t * const star,
                   struct orbit_t * const orbit,
                   const double object_mass,
                   const double star_radius,
                   const double multiplier,
                   const Boolean RLOF_boolean,
                   double * const domega_dt_star,
                   double * const dJ_dt_star,
                   double * const dJ_dt_orbit,
                   double * const dedt_star,
                   double * const dedt_orbit,
                   const Boolean clear_orbit)
{
    Tprint("star%d: stellar type %s : ωorb = %g, ω* = %g\n",
           star->starnum,
           Stellar_type_string(star->stellar_type),
           orbit->angular_frequency,
           star->omega);

    /*
     * Initialize derivatives
     */
    *domega_dt_star = 0.0;
    *dJ_dt_star = 0.0;
    *dedt_star = 0.0;

    if(clear_orbit == TRUE)
    {
        *dJ_dt_orbit = 0.0;
        *dedt_orbit = 0.0;
    }

    if(star->stellar_type < BLACK_HOLE &&
       Is_really_not_zero(orbit->angular_frequency))
    {
        double tc,f,tcqr,rg2=0.0,ttid;
        const double cq         = object_mass / star->mass;
        const double starI      = moment_of_inertia(stardata,
                                                    star,
                                                    star_radius);
        const double ecc2       = Pow2(orbit->eccentricity);
        const double omecc2     = 1.0 - ecc2; // 1-e^2
        const double sqome2     = sqrt(omecc2); // sqrt(1-e^2)
        const double sqome3     = Pow3(sqome2); // (1-e^2)^(3/2)
        const double sq6        = Pow3(omecc2); // (1-e^2)^3
        const double isqome2_13 = 1.0 / (Pow2(sq6) * sqome2);
        const double menv       = envelope_mass(star);
        const Boolean use_Zahn_89 =
            stardata->preferences->tides_convective_damping == TIDES_CONVECTIVE_DAMPING_ZAHN1989 &&
            MINT_could_data_be_available(stardata,star,MINT_TIDAL_E_FOR_LAMBDA) == TRUE;

        /* variable declaration related to giocode */
        const double Maybe_unused lambda01 =
            use_Zahn_89 == TRUE
            ? tidal_lambda2(stardata,star,orbit,star_radius,0,1)
            : 0.0; //used for circularisation
        const double Maybe_unused lambda22 =
            use_Zahn_89 == TRUE
            ? tidal_lambda2(stardata,star,orbit,star_radius,2,2) :
            0.0; //used for synchronisation

        /*
         * Calculate tidal effects : circularization, orbital changes and spin up.
         */
        Tprint("star%d: Calculate circularization/shrinkage/spinup (tides)\n",
               star->starnum);
        Tprint("star%d: at beginning: lambda01=%g lambda2=%g\n",
               star->starnum,
               lambda01,
               lambda22);

        const double raa = star_radius / orbit->separation;
        const double raa2 = Pow2(raa);
        const double raa6 = Pow3(raa2);

        /*
         * Hut's polynomials.
         * Note: f1 is not required
         */
        const double f2 = Hut_f2(orbit->eccentricity);
        const double f3 = Hut_f3(orbit->eccentricity);
        const double f4 = Hut_f4(orbit->eccentricity);
        const double f5 = Hut_f5(orbit->eccentricity);
        //const double f1 = Hut_f1(orbit->eccentricity);
        double _E2 = 0.0;

        Tprint("star%d: Hut polynomials: f2=%g f3=%g f4=%g f5=%g\n",
               star->starnum,
               f2,
               f3,
               f4,
               f5);

        Tprint("star%d: Tides : radiative? %d : convective? %d (pms? %d age=%g pmst=%g CONVECTIVE_PMS=%d RADIATIVE_MAIN_SEQUENCE=%d)\n",
               star->starnum,
               USE_RADIATIVE_DAMPING,
               !USE_RADIATIVE_DAMPING && USE_CONVECTIVE_DAMPING,
               stardata->preferences->pre_main_sequence,
               star->age*1e6,
               preMS_lifetime(star->mass),
               CONVECTIVE_PMS,
               RADIATIVE_MAIN_SEQUENCE
            );

        if(USE_RADIATIVE_DAMPING)
        {
            /*
             * Radiative damping (Zahn, 1977, A&A, 57, 383 and 1975, A&A, 41, 329).
             */
            _E2 = E2(stardata,star);
            f = 1.9782e+04*star_radius*sqrt(star->mass/Pow5(orbit->separation))*
                _E2*pow(1.0+cq,5.0/6.0);

            tcqr = f * cq * raa6;
            Tprint("star%d: tcqr = %g * %g * %g = %g\n",
                   star->starnum,
                   f,
                   cq,
                   raa6,
                   tcqr);
            rg2 = star->moment_of_inertia_factor;
            Tprint("star%d: Zahn radiative damping tcqr=%g rg2=%g\n",
                   star->starnum,
                   tcqr,
                   rg2);
        }
        else if(USE_CONVECTIVE_DAMPING)
        {
            /*
             * Convective damping (Hut, 1981, A&A, 99, 126).
             */
            if(Is_really_not_zero(star->menv))
            {
                if(RLOF_boolean==TRUE) star->renv = Max(TINY,
                                                        Min(star->renv,star_radius - star->core_radius));

                tc = convective_turnover_time(star_radius,star,stardata);

                Tprint("star%d: tc = %g from menv=%g renv=%g radius = %g L = %g\n",
                       star->starnum,
                       tc,
                       star->menv,
                       star->renv,
                       star_radius,
                       star->luminosity);
                ttid = TWOPI/(1.0e-10 + fabs(orbit->angular_frequency - star->omega));

                /*
                 * This is correct! It is wrong in Jarrod's BSE code.
                 * DO NOT CHANGE IT (see MNRAS 329 897 eq 32)
                 */

                const double im = 1.0/star->mass;
                const double itc = 1.0/tc;

                if(use_Zahn_89 == TRUE)
                {
                    /*
                     * tidal timescale using Zahn's 1989 prescription
                     */
                    tcqr = 2.0 * lambda01 * itc * cq * raa6;
                    Tprint("star%d: tcqr = %g from lambda01=%g itc=%g cq=%g raa6=%g \n",
                           star->starnum,
                           tcqr,
                           lambda01,
                           itc,
                           cq,
                           raa6);
                    rg2 = star->moment_of_inertia_factor * Max(1e-30, menv) * im;
                    Tprint("star%d: Convective (Hut) damping tcqr=%g cq=%g rg2=%g raa6=%g moment_of_inertia_factor=%g menv=%g Envelope_mass=%g im=%g itc=%g\n",
                           star->starnum,
                           tcqr,
                           cq,
                           raa6,
                           rg2,
                           star->moment_of_inertia_factor,
                           star->menv,
                           menv,
                           im,
                           itc);
                }
                else
                {
                    /*
                     * BSE timescale (Hurley et al. 2002)
                     */
                    f = Min(1.0, Pow2(ttid * itc * 0.5));
                    tcqr = 2.0 * f * cq * raa6 * star->menv * im * itc * 1.0 / 21.0;
                    Tprint("star%d: tcqr = %g from f=%g cq=%g raa6=%g menv=%g im=%g itc=%g\n",
                           star->starnum,
                           tcqr,
                           f,
                           cq,
                           raa6,
                           star->menv,
                           im,
                           itc);
                    rg2 = star->moment_of_inertia_factor * Max(1e-30,menv) * im;
                    Tprint("star%d: Convective (Hut) damping tcqr=%g cq=%g rg2=%g raa6=%g moment_of_inertia_factor=%g menv=%g Envelope_mass=%g im=%g itc=%g\n",
                           star->starnum,
                           tcqr,
                           cq,
                           raa6,
                           rg2,
                           star->moment_of_inertia_factor,
                           star->menv,
                           menv,
                           im,
                           itc);
                }
            }
            else
            {
                /*
                 * No convective envelope
                 */
                tcqr = 0.0;
            }
        }
        else
        {
            /*
             * Degenerate damping (Campbell, 1984, MNRAS, 207, 433)
             */
            f = 7.33e-09*pow(star->luminosity/star->mass,5.0/7.0);
            tcqr = f * Pow2(raa2*cq) / (1.0+cq);
            rg2 = CORE_MOMENT_OF_INERTIA_FACTOR;
            Tprint("star%d: Degenerate (Campbell) damping tcqr=%g rg2=%g\n",
                   star->starnum,
                   tcqr,
                   rg2);
        }

        /*
         * Modify tidal timescale by strength factor
         */
        tcqr *= multiplier;

        Tprint("star%d: Modulate tcqr by multiplier = %g -> tcqr=%g\n",
               star->starnum,
               multiplier,
               tcqr);

        /*
         * Circularization.
         */
        if(stardata->preferences->tides_algorithm == TIDES_ALGORITHM_SCIARINI2023 &&
           USE_RADIATIVE_DAMPING)
        {
            /*
             * Sciarini et al. (2023) formalism
             * replaces the above in the case of radiative damping/
             * dynamical tides, their Eq. 9.
             */
            const double gmr = GRAVITATIONAL_CONSTANT * star->mass * M_SUN / (star->radius * R_SUN);
            const double gmr3 = gmr / Pow2(star->radius * R_SUN);
            const double sgmr3 = sqrt(gmr3);

            const double x10 = Xlm(1,0);
            const double x12 = Xlm(1,2);
            const double x22 = Xlm(2,2);
            const double x32 = Xlm(3,2);

            *dedt_star =
                -0.75 * orbit->eccentricity *
                sgmr3 *
                cq * sqrt(1.0 + cq)*
                _E2 *
                raa6 * sqrt(raa) * // == pow(raa,13.0/2.0)*
                (
                    + 1.5 * x10
                    - 0.25 * x12
                    - x22
                    + 49.0/4.0 * x32
                    );

            printf("X %g %g %g %g\n",
                   x10,x12,x22,x32);
            printf("ecc %g sqmr3 %g _E2 %g raa %g\n",
                   orbit->eccentricity,
                   sgmr3,
                   _E2,
                   raa);
            printf("cq %g\n",cq);

            const double dJdt =
                1.5 *
                gmr *
                _E2 *
                Pow2(cq) *
                raa6 *
                (
                    x22
                    +
                    ecc2 * (
                        + 0.25 * x12
                        - 5.0 * x22
                        + 49.0/4.0 * x32
                        )
                    );

            /*
             * Hence domega/dt assuming starI ~ constant.
             */
            *domega_dt_star = dJdt / starI;

            printf("dedt = %g, dJdt = %g\n",
                   *dedt_star,
                   dJdt);
        }
        else
        {
            /*
             * Hurley et al. (2002) formalism
             */
            *dedt_star =
                -27.0*tcqr*(1.0+cq)*raa2*(orbit->eccentricity*isqome2_13)*
                (f3 - 0.61111111111*sqome3*f4*star->omega/orbit->angular_frequency);

        }

        if(RLOF_boolean == TRUE &&
           star->RLOFing == TRUE &&
           stardata->preferences->force_circularization_on_RLOF == TRUE &&
           Is_really_not_zero(orbit->eccentricity)
            )
        {
            /*
             * If we have RLOF, but are eccentric (so there's RLOF at periastron),
             * and want to be circular, set the eccentricity derivative to be
             * very large and negative so the system (hopefully) immediately
             * becomes circular.
             */
            *dedt_star = -1.0e20;
        }

        *dedt_orbit += *dedt_star;

        Dprint("star%d: TIDES t=%g de/dt=%g from tcqr=%g cq=%g raa2=%g e=%g isqome2_13=%g f3=%g f4=%g omega*=%g omegaO=%g\n",
               star->starnum,
               stardata->model.time,
               *dedt_orbit,
               tcqr,cq,raa2,
               orbit->eccentricity,
               isqome2_13,f3,f4,star->omega,orbit->angular_frequency
            );


        /*
         * Save timescale
         * (this will be calculated twice (for each star), and only the second
         *  time is it correct, i.e. for both stars)
         */
        orbit->tcirc =

            /* no change -> long timescale : Note if e=0 then de/dt = 0 also */
            Is_really_zero(*dedt_orbit) ? LONG_TIDAL_TIME :

            /* estimate from timescale = e / de/dt */
            (Is_really_not_zero(orbit->eccentricity) ?
             fabs(orbit->eccentricity/(*dedt_orbit)) : 0.0);

        Dprint("Set tcirc e=%g de/dt=%g\n",
               orbit->eccentricity,
               *dedt_orbit);

        /*
         * Spin up of star.
         */
        const double sqome3f5 = sqome3 * f5;
        if(use_Zahn_89 == TRUE &&
           !USE_RADIATIVE_DAMPING &&
           USE_CONVECTIVE_DAMPING)
        {
            /*
             * If we used the lambda coefficient formalism (that is,
             * convective damping), we need to replace the circularisation
             * lambda01 with the synchronisation lambda22
             */
            tcqr *= Is_really_zero(lambda01) ? 0.0 :(lambda22/lambda01);
            Tprint("star%d: Synch tcqr = %g lambda22=%g lambda01=%g \n",
                   star->starnum,
                   tcqr,
                   lambda22,
                   lambda01);
        }

        if(Is_zero(*domega_dt_star))
        {
            const double c = Is_really_zero(rg2) ? 0.0 :
                (3.0 * cq * tcqr/  (rg2 * Pow6(omecc2)) * sqome3f5);
            Tprint("star%d: c= %g * %g / (%g * %g) * %g = %g\n",
                   star->starnum,
                   3.0*cq,
                   tcqr,
                   rg2,
                   Pow6(omecc2),
                   sqome3f5,
                   c);

            /*
             * x is the stellar rotation rate.
             * y is the pseudo-synchronous rate (from Hut 1981).
             */
            const double x = star->omega;
            const double y = f2*orbit->angular_frequency/sqome3f5;

            /*
             * Save y for logging
             */
            orbit->pseudo_sync_angular_frequency = y;

//#undef EXPONENTIAL_TIDES
#ifdef EXPONENTIAL_TIDES
            /*
             * Solve the differential equation analytically, and use the derivative that
             * would have were it linear.
             *
             * This is similar to the method of Guderley and Hsu 1972
             * https://www.ams.org/journals/mcom/1972-26-117/S0025-5718-1972-0298952-7/
             * for solving stiff terms analytically
             */
            *domega_dt_star =
                (y - x)/stardata->model.dt  * (1.0 - exp(- c * stardata->model.dt));

#else
            /*
             * Or assume a linear solution, as in BSE (tends to be unstable when tides are strong).
             */
            *domega_dt_star =
                c * (y - x);
#endif//EXPONENTIAL_TIDES
        }

        Tprint("star%d: Spin up domega/dt = %g\n",
               star->starnum,
               *domega_dt_star);
        Nancheck(*domega_dt_star);


        /*
         * Calculate the equilibrium spin at which no angular momentum
         * can be transferred. The stellar angular velocity derivative
         * cannot exceed the amount which would bring us to this equilibrium
         * value, because when omega = omega_eq we have d(omega)/dt = 0.
         *
         * Todo: should we use this to limit dynamical tides?
         */
        if(stardata->model.dt > REALLY_TINY)
        {
            const double eqspin = orbit->angular_frequency*f2/sqome3f5;
            Tprint("star%d: eqspin = %g, omega = %g : domega/dt was %g\n",
                   star->starnum,
                   eqspin,
                   star->omega,
                   *domega_dt_star);

            if(Is_really_not_zero(eqspin))
            {
                const double delta_omega_dt = (eqspin - star->omega)/stardata->model.dt;
                const double domega = stardata->model.dt * (*domega_dt_star);
                const double new_omega = star->omega + domega;
                if(*domega_dt_star > 0.0 &&
                   new_omega < eqspin)
                {
                    /*
                     * We're spinning up (domega/dt>0) and currently slower than eqspin
                     * (omega < eqspin) : domega/dt must be < delta_omega_dt
                     */
                    *domega_dt_star =
                        Min((*domega_dt_star),
                            delta_omega_dt);
                }
                else if(*domega_dt_star < 0.0 &&
                        new_omega > eqspin)
                {
                    /*
                     * We're spinning down (domega/dt<0) and currently faster than eqspin
                     * (omega > eqspin) : domega/dt must be > delta_omega_dt
                     */
                    *domega_dt_star =
                        Max(*domega_dt_star,
                            delta_omega_dt);
                }
            }
            Tprint("star%d : eqspin = %g, domega/dt is now %30.20e\n",
                   star->starnum,
                   eqspin,
                   *domega_dt_star);

            /*
             * Do not allow omega to change sign because of tides :
             * tides can never do this.
             */
            if(0)
            {
                const double omega_estimate = star->omega +
                    (*domega_dt_star) * stardata->model.dt;
                if(star->omega * omega_estimate < 0.0)
                {
                    /*
                     * Instead, assume omega = 0 on the next step
                     */
                    Tprint("star%d : Truncate : omega = %g, domega/dt was %g (would make omega = %g) ",
                           star->starnum,
                           star->omega,
                           *domega_dt_star,
                           omega_estimate);
                    *domega_dt_star =
                        - star->omega / stardata->model.dt;
                    Tprint("is now %g\n",
                           *domega_dt_star);
                }
            }
        }

        /*
         * Calculate angular momentum changes: what is taken from the star is
         * put into the orbit, and vice versa
         */
        const double dj_dt = starI * (*domega_dt_star);

        *dJ_dt_star = +dj_dt;
        *dJ_dt_orbit -= dj_dt;

        const double tspin =
            Is_really_zero(*domega_dt_star) ?
            1e100 : Min(1e100, fabs(star->omega/(*domega_dt_star)));

        Tprint("star%d: Jdot Tides -> djt from starI=%g domega/dt=%g (omega now %g, estimate next omega %g): dj/dt=%g (J now %g, estimate next J %g) : tcirc = %g, tspin = %g\n",
               star->starnum,
               starI,
               *domega_dt_star,
               star->omega,
               star->omega + (*domega_dt_star) * stardata->model.dt,
               dj_dt,
               star->angular_momentum,
               star->angular_momentum + dj_dt * stardata->model.dt,
               orbit->tcirc,
               tspin
            );

    }
}
