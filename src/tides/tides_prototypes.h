#pragma once
#ifndef TIDES_PROTOTYPES_H
#define TIDES_PROTOTYPES_H

void tides(struct stardata_t * const stardata,
           const Boolean RLOF_boolean,
           const double radius,
           struct star_t * const star,
           struct star_t * const companion);

double tidal_lambda2(struct stardata_t * const stardata,
                     struct star_t * const star,
                     struct orbit_t * const orbit,
                     const double radius,
                     unsigned int l,
                     unsigned int m);

void tides_generic(struct stardata_t * const stardata,
                   struct star_t * const star,
                   struct orbit_t * const orbit,
                   const double object_mass,
                   const double star_radius,
                   const double multiplier,
                   const Boolean RLOF_boolean,
                   double * const domega_dt_star,
                   double * const dJ_dt_star,
                   double * const dJ_dt_orbit,
                   double * const dedt_star,
                   double * const dedt_orbit,
                   const Boolean clear_orbit);

void tides_load_variables(struct stardata_t * const stardata);

Pure_function
double Hut_f5(const double ecc);
Pure_function
double Hut_f4(const double ecc);
Pure_function
double Hut_f3(const double ecc);
Pure_function
double Hut_f2(const double ecc);
Pure_function
double Hut_f1(const double ecc);



#endif // TIDES_PROTOTYPES_H
