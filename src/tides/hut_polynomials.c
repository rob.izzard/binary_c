#include "../binary_c.h"
No_empty_translation_unit_warning;


/*
 * Hut's polynomials for tidal physics
 */
Pure_function
double Hut_f5(const double ecc)
{
    const double ecc2 = Pow2(ecc);
    return Is_really_zero(ecc2) ? 1.0 : (1.0+ecc2*(3.0+ecc2*0.3750));
}

Pure_function
double Hut_f4(const double ecc)
{
    const double ecc2 = Pow2(ecc);
    return Is_really_zero(ecc2) ? 1.0 : (1.0+ecc2*(1.50+ecc2*0.1250));
}

Pure_function
double Hut_f3(const double ecc)
{
    const double ecc2 = Pow2(ecc);
    return Is_really_zero(ecc2) ? 1.0 : (1.0+ecc2*(3.750+ecc2*(1.8750+ecc2*7.8125e-02)));
}

Pure_function
double Hut_f2(const double ecc)
{
    const double ecc2 = Pow2(ecc);
    return Is_really_zero(ecc2) ? 1.0 : (1.0+ecc2*(7.50+ecc2*(5.6250+ecc2*0.31250)));
}

Pure_function
double Hut_f1(const double ecc)
{
    const double ecc2 = Pow2(ecc);
    return Is_really_zero(ecc2) ? 1.0 : (1.0+ecc2*(15.50+ecc2*(31.8750+ecc2*(11.56250+ecc2*0.3906250))));
}
