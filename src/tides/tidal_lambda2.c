#include "../binary_c.h"
No_empty_translation_unit_warning;

//#undef DEBUG
//#define DEBUG 1

/*
 * tidal parameter lambda2 from Zahn (1989)
 *
 * http://articles.adsabs.harvard.edu/pdf/1989A%26A...220..112Z
 *
 * todo: maybe use gsl_sf_hyperg_2F1() instead of numerical integration ?
 */


/*
 * Local function declarations
 */
static double Zahn_integral_13(const double x,
                               void * p);
static double Zahn_integral_14a(const double x,
                                void * p);
static double Zahn_Eq_14b_wrapper(const double xa,
                                  void * p);
static double Zahn_Eq_14b(const double xa,
                          const double RHS);


double tidal_lambda2(struct stardata_t * const stardata,
                     struct star_t * const star,
                     struct orbit_t * const orbit,
                     const double radius,
                     const unsigned int l,
                     const unsigned int m)
{
    double lambda; /* returned value */

    /* declare all the local variables that might not be used
     * it is useful for the diagnosis run */
    double xa=0;
    double integral=0;
    double integral1=0;
    double integral2=0;


    /* orbital period, modulated by l and m, years */
    const double Pi =
        2.0 * PI /
        fabs((double)l * orbit->angular_frequency -
             (double)m * star->omega);
    Dprint("Pi = %g\n",Pi);

    /* "tidal friction" time, years */
    const double tf = convective_turnover_time(radius,
                                               star,
                                               stardata);
    Dprint("tf= %g\n", tf);
    if(Is_zero(tf)) return 0.0;

    const double E = star->E_Zahn; /* from stellar model tables : ~ 50 = fully convective */
    const double alpha = 2.0; /* mixing length parameter from stellar model input */
    const double alphaprime = 0.762 * alpha;

    /*
     * Location of the base of the core convection zone
     * in terms of relative radial coordinate x=r/R
     *
     * WARNING - star->renv is a depth not a radial coordinate
     */
    const double xb = 1.0 - star->renv / star->radius;

    /*
     * The RHS to Eq. 14b
     */
    const double xx = Pow2(alphaprime)*E;
    Dprint("E=%g; alphaprime=%g; xb=%g; xx=%g\n",
           E,
           alphaprime,
           xb,
           xx);

    /*
     * We have to check xx > something small because it is
     * raised to the power -1.0/3.0. TINY seems to be fine.
     */
    if(xx > TINY)
    {
        const double RHS = Pow1p5(5.0/2.0) * pow(xx, -1.0/3.0)
            * Pi / (2.0 * tf);

        Dprint("xb %g ; Renv/R = %g; with Renv = %g and R = %g ;  E = %g; Eq.14b RHS = %g\n",
               xb,
               star->renv/star->radius,
               star->renv,
               star->radius,
               E,
               RHS);

        if(Fequal(xb,1.0))
        {
            /*
             * Simple case where there is no convection zone
             */
            lambda = 0.0;
        }
        else
        {
            const double integration_tolerance = 1e-10;
            double integration_error;

            /*
             * Note: 0.160815 = Zahn_Eq_14b(min=7/16,0.0)
             */
            if(RHS < 0.160814)
            {
                /*
                 * we have roots to Eq. 14b : find the outer root,
                 * xa, then integrate Eq. 14
                 */
                int err;
                const double min = 7.0/16.0; // max of the function: we require the root outside this
                const double max = 1.0;
                const double initial_guess = 0.5*(min + max);
                const double bisection_tolerance = 1e-10; /* bisection tolerance */
                const int itmax = 200;
                const Boolean uselog = FALSE;
                const double convergence_alpha = 1.0;
                xa = generic_bisect(&err,
                                    BISECT_USE_MONOCHECKS_FATAL,
                                    BISECTOR_TIDES_XA, // make negative for logging
                                    &Zahn_Eq_14b_wrapper,
                                    initial_guess,
                                    min,
                                    max,
                                    bisection_tolerance,
                                    itmax,
                                    uselog,
                                    convergence_alpha,
                                    /* va_args */
                                    RHS);
                Dprint("Check func at root xa = %g : %g\n",
                       xa,Zahn_Eq_14b(xa,RHS));
                Dprint("Check xa falls into CZ with xa>xb xa = %g, xb %g\n",
                       xa,xb);

                if(xa > xb)
                {
                    integral1 = GSL_integrator(xa, // lower
                                               1.0, // upper
                                                            integration_tolerance,
                                                            &integration_error,
                                                            GSL_INTEGRATOR_QAG,
                                                            &Zahn_integral_13);

                    integral2 = GSL_integrator(xb, // lower
                                                            xa, // upper
                                                            integration_tolerance,
                                                            &integration_error,
                                                            GSL_INTEGRATOR_QAG,
                                                            &Zahn_integral_14a)
                       * pow(xa,7.0/6.0) * Pow1p5(1.0 - xa);

                    integral = integral1 + integral2;
                    Dprint("have roots: xb = %g, xa = %g, integral = %g + %g = %g\n",
                           xb,
                           xa,
                           integral1,
                           integral2,
                           integral);
                }
                else
                {
                    /*
                     * There is a root but if falls below convective zone,  single integral in Eq. 13
                     */
                    integral = GSL_integrator(xb, // lower
                                              1.0, // upper
                                              integration_tolerance,
                                              &integration_error,
                                              GSL_INTEGRATOR_QAG,
                                              &Zahn_integral_13);
                    Dprint("irrelevant root ignored: xb = %g, integral = %g\n",
                           xb,
                           integral);
                }
            }
            else
            {
                /*
                 * No roots,  single integral in Eq. 13
                 */
                integral = GSL_integrator(xb, // lower
                                          1.0, // upper
                                          integration_tolerance,
                                          &integration_error,
                                          GSL_INTEGRATOR_QAG,
                                          &Zahn_integral_13);
                Dprint("no roots: xb = %g, integral = %g\n",
                       xb,
                       integral);
            }

            lambda = 0.8725 * pow(Pow2(alphaprime) * E, 2.0/3.0) * integral;
        }
    }
    else
    {
        lambda = 0.0;
    }

    const double Eq15 = 0.019 * pow(alpha,4.0/3.0) * sqrt(320.0 / (320.0 + Pow2((2.0 * tf) / Pi)));

    /*
     * We generate diagnostic output for testing
     */
     /*Foreach_star(star)
     *{
     *    const double tf = convective_turnover_time(star->radius,
     *                                               star,
     *                                               stardata);
     *    Printf("TIDES_TESTING_OUTPUT %d %g %g\n", star->starnum, tf, star->renv);
     *}
     */

#ifdef TIDES_DIAGNOSIS_OUTPUT
    if(stardata->preferences->tides_logging_function==1)
    {
        /* This output is meant to test tides,
         * it will be used in the unit test for lambda
         * and will be wrapped in a dedicated input keyword */
        Abundance Xhc =
#ifdef MINT
               star->mint->XHc
#else
               -1.0
#endif//MINT
            ;
        Printf("DIAGNOSTIC_OUTPUT_TIDES_"
               "%i %u %u %.10e %.10e "
               "%.10e %.10e %.10e %.10e "
               "%.10e %.10e %.10e %.10e "
               "%.10e %.10e %.10e %.10e "
               "%.10e %.10e %g\n",
               star->starnum, //1
               l,
               m,
               stardata->model.time,
               star->mass, //5
               star->menv,
               star->renv,
               star->radius,
               star->luminosity, //9
               tf,
               E, //11
               xb,
               xa, //13
               alphaprime,
               Pi, //15
               integral1,
               integral2,
               integral, //18
               lambda, //19
               Xhc);
               }
#endif //TIDES_DIAGNOSIS_OUTPUT

            Dprint("lambda = %g, approximate Eq15 gives %g\n",
                   lambda,
                   Eq15);

    if(lambda < 0.0)
    {
        Exit_binary_c(BINARY_C_ALGORITHM_OUT_OF_RANGE,
                      "lambda(%u,%u) = %g < 0 error\n",
                      l,
                      m,
                      lambda);
    }



    return lambda;
}



static double Zahn_integral_13(const double x,
                               void * p Maybe_unused)
{
    /*
     * Zahn's equation 13 (without the prefactor)
     */
    return pow(x,8.0 - 2.0/3.0) * Pow2(1.0 - x);
}

static double Zahn_integral_14a(const double x,
                                void * p Maybe_unused)
{
    /*
     * Zahn's equation 14a (without the prefactor)
     */
    return pow(x,6.0 + 1.0/6.0) * sqrt(1.0 - x);
}

static double Zahn_Eq_14b(const double xa,
                          const double RHS)
{
    /*
     * Zahn's equation 14b (all moved to the LHS).
     *
     * Note the negative sign to make sure the function is
     * monotonically increasing so we can use generic_bisect
     * on it.
     */
    return -( pow(xa,7.0/6.0) * Pow1p5(1.0 - xa) - RHS);
}

static double Zahn_Eq_14b_wrapper(const double xa,
                                  void * p)
{
    /*
     * Generic bisect wrapper for Zahn_Eq_14b
     */
    Map_GSL_params(p,args);
    Map_varg(const double,RHS,args);
    va_end(args);
    return Zahn_Eq_14b(xa,RHS);
}
