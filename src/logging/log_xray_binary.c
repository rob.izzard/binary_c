#include "../binary_c.h"

No_empty_translation_unit_warning;


#ifdef XRAY_BINARIES
void log_xray_binary(struct stardata_t * Restrict const stardata,
                     const Star_number ndonor Maybe_unused,
                     const Star_number naccretor,
                     const double accretion_rate)
{


    // Check to see if we're an xray binary
    double LX; // X-ray luminosity
    /* calculate X-ray luminosity (in solar units) */
    LX=(GRAVITATIONAL_CONSTANT*M_SUN*M_SUN/YEAR_LENGTH_IN_SECONDS)*
        (stardata->star[naccretor].mass*accretion_rate)/
        (2.0*stardata->star[naccretor].radius*R_SUN)
        /L_SUN;


    // must be NS/BH accretor
    if(((stardata->star[naccretor].stellar_type==NEUTRON_STAR)||
        (stardata->star[naccretor].stellar_type==BLACK_HOLE))&&
       // and accrete at > solar luminosity
       (LX > 1.0))

    {

        /*
         * Note Xray_luminosity is set in special_cases during RLOF
         * and evolution() for winds. This means the RLOF value will
         * overwrite the wind value when RLOF occurs. This is probably OK.
         */
        stardata->star[naccretor].Xray_luminosity=LX;

        //printf("XRAY ");

        /*if((ON_MAIN_SEQUENCE(stardata->star[ndonor].stellar_type))
          &&
          (stardata->star[ndonor].mass<=2.0))
          {
          // Low-mass X-ray Binary
          printf("LMXB ");
          }
          else if(WHITE_DWARF(stardata->star[ndonor].stellar_type))
          {
          // White-dwarf X-ray Binary
          printf("WDXB ");
          }
          else
          {
          printf("HMXB ");
          }

          printf("accretor %d type %d rate %g vs Xmin = frac %g\n",
          accretor,
          stardata->star[naccretor].stellar_type,
          accretion_rate,
          (GRAVITATIONAL_CONSTANT*M_SUN*M_SUN/YEAR_LENGTH_IN_SECONDS)*
          (stardata->star[naccretor].mass*accretion_rate)/
          (2.0*stardata->star[naccretor].radius*R_SUN)
          /L_SUN
          );
        */
    }

}
#endif // XRAY_BINARIES
