#include "../binary_c.h"
No_empty_translation_unit_warning;

#if defined NUCSYN &&                           \
    LOG_COMENV_RSTARS

void log_r_star(struct star_t *star,
                struct stardata_t *stardata)
{

    /*
     * Log post-common-envelope R star
     */
    double min_thick_disk_age=thick_disk_age(stardata->common.metallicity);

    if(stardata->model.time<min_thick_disk_age) return;

    if((star->r_star_progenitor==TRUE)&&
       (star->stellar_type==CHeB))
    {
        star->rstar=0;

        /*
         * Analyse R star type
         *
         * HeWD + GB core merger
         */
        if(((star->r_star_precomenv_stellar_type1==GIANT_BRANCH)&&
            (star->r_star_precomenv_stellar_type2==HeWD))
           ||
           ((star->r_star_precomenv_stellar_type2==GIANT_BRANCH)&&
            (star->r_star_precomenv_stellar_type1==HeWD)))
        {
            star->rstar=RSTAR_HeWD_GB;
        }
        /* HG + HeWD core merger */
        else if(((star->r_star_precomenv_stellar_type1==HG)&&
                 (star->r_star_precomenv_stellar_type2==HeWD))
                ||
                ((star->r_star_precomenv_stellar_type2==HG)&&
                 (star->r_star_precomenv_stellar_type1==HeWD)))
        {
            star->rstar=RSTAR_HeWD_HG;
        }
        /* HG + GB core merger */
        else if(((star->r_star_precomenv_stellar_type1==HG)&&
                 (star->r_star_precomenv_stellar_type2==GIANT_BRANCH))
                ||
                ((star->r_star_precomenv_stellar_type2==HG)&&
                 (star->r_star_precomenv_stellar_type1==GIANT_BRANCH)))

        {
            star->rstar=RSTAR_GB_HG;
        }
        /* HG + HG core merger */
        else if(((star->r_star_precomenv_stellar_type1==HG)&&
                 (star->r_star_precomenv_stellar_type2==HG)))
        {
            star->rstar=RSTAR_HG_HG;
        }
        /* GB + GB core merger */
        else if(((star->r_star_precomenv_stellar_type1==GIANT_BRANCH)&&
                 (star->r_star_precomenv_stellar_type2==GIANT_BRANCH)))
        {
            star->rstar=RSTAR_GB_GB;
        }
        /* AGB + something core merger */
        else if(AGB(star->r_star_precomenv_stellar_type1)
                ||
                AGB(star->r_star_precomenv_stellar_type2))
        {
            star->rstar=RSTAR_AGB;
        }
        /* He star + something core merger */
        else if(NAKED_HELIUM_STAR(star->r_star_precomenv_stellar_type1)
                ||
                NAKED_HELIUM_STAR(star->r_star_precomenv_stellar_type2))
        {
            star->rstar=RSTAR_He;
        }
        /* CONeWD + AGB -> CHeB ?*/
        else if(CONeWD(star->r_star_precomenv_stellar_type1)
                ||
                CONeWD(star->r_star_precomenv_stellar_type2))
        {
            star->rstar=RSTAR_CONeWD;
        }
        /* something else? */
        else
        {
            star->rstar=RSTAR_OTHER;
        }


        printf("COMENVRSTAR %g %d %d %d %d %d %g %g %g %g %g %g %d ",
               stardata->model.probability, // 0
               star->r_star_precomenv_stellar_type1,
               star->r_star_precomenv_stellar_type2,
               TRUE, // coalesced : MUST be true
               star->r_star_postcomenv_stellar_type1, //4
               star->r_star_postcomenv_stellar_type2,//5
               star->r_star_precomenv_m1, // 6
               star->r_star_precomenv_mc1,//7
               star->r_star_precomenv_l1, //8
               star->r_star_precomenv_m2,//9
               star->r_star_precomenv_mc2,//10
               star->r_star_precomenv_l2,//11
               star->rstar//12
            );
        printf("%d %g %g %g ",
               star->stellar_type,//13
               star->mass,//14
               Outermost_core_mass(star),//15
               star->luminosity//16
            );

        /*
         * Calculate mass of carbon required to turn
         * the envelope carbon rich: this analysis ignores C13 and O17
         * and assumes the oxygen content in the envelope does
         * not change (not likely, given CNO burning!).
         * c_required is DeltaM in the writeup
         */
        double c_required =
            envelope_mass(star)
            *(0.75*star->Xenv[XO16]-star->Xenv[XC12])
            /(1.0-star->Xenv[XC12]);

        printf("%g C=%g jstar=%g (jorb was %g, jstars was %g, jtot=%g) omega=%g/year vrot=%g km/s vrot2=%g vrot3=%g vcrit=%g km/s -> frac=%g frac2=%g frac3=%g delay=%g comenvn=%d postcomenv m=%g mc=%g l=%g\n",
               stardata->model.time, //17
               c_required,//18
               star->r_star_postjspin,//19
               star->r_star_prejorb,//20
               star->r_star_prejtot - star->r_star_prejorb,//21
               star->r_star_prejtot,//22
               star->r_star_postomega,  //23
               star->r_star_postvrot,//24
               star->r_star_postvrot2,//25
               star->r_star_postvrot3,//26
               star->r_star_postvcrit,//27
               star->r_star_postvrot/star->r_star_postvcrit,//28
               star->r_star_postvrot2/star->r_star_postvcrit,//29
               star->r_star_postvrot3/star->r_star_postvcrit,//30
               stardata->model.time-star->r_star_comenv_time,//31
               stardata->model.comenv_count,//32
               star->r_star_postcomenv_m,//33
               star->r_star_postcomenv_mc,//34
               star->r_star_postcomenv_l//35
            );

        double *Cshell=Calloc(ISOTOPE_ARRAY_SIZE,sizeof(Abundance));
        Cshell[XC12]=1.0;

        nucsyn_mix_shells(c_required,Cshell,
                          envelope_mass(star)-c_required,star->Xenv);
        Safe_free(Cshell);

        printf("R STAR abunds [C/Fe]=%g [N/Fe]=%g [O/Fe]=%g C12/C13=%g C/O=%g H=%g He=%g\n",
               nucsyn_square_bracket(star->Xenv,stardata->common.Xsolar,XC12,XFe56),
               nucsyn_square_bracket(star->Xenv,stardata->common.Xsolar,XN14,XFe56),
               nucsyn_square_bracket(star->Xenv,stardata->common.Xsolar,XO16,XFe56),
               star->Xenv[XC12]/star->Xenv[XC13],
               star->Xenv[XC12]/star->Xenv[XO16]*1.333333333,
               star->Xenv[XH1],
               star->Xenv[XHe4]
            );

        // reset r_star_progenitor flag
        star->r_star_progenitor=FALSE;
    }
}

#endif
