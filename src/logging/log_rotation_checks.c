#include "../binary_c.h"
No_empty_translation_unit_warning;


void log_rotation_checks(struct star_t *star Maybe_unused)
{
#ifdef ROTATION_CHECKS
    if(star->stellar_type==MASSLESS_REMNANT) return;
    double omega=star->angular_momentum/(star->k2*Pow2(star->radius)*envelope_mass(star)+
                              K3*star->mass*Pow2(star->radius)); // per year
    double vrot=star->radius*omega/OMEGA_FROM_VKM; // km/s
    double vcrit=1e-5*sqrt(GRAVITATIONAL_CONSTANT*(star->mass*M_SUN)/(star->radius*R_SUN)); // km/s

    // for omega_core we have to calculate j_core, which is jstar - jenv
    //double omega_core=(star->angular_momentum- star->k2*envelope_mass(star)*Pow2(star->radius))
    ///(K3*Outermost_core_mass(star)*Pow2(star->core_radius)); // per year
    double omega_core=omega;
    double vrot_core=star->core_radius*omega_core/OMEGA_FROM_VKM; // km/s
    double vcrit_core=1e-5*sqrt(GRAVITATIONAL_CONSTANT*(Outermost_core_mass(star)*M_SUN)/(star->core_radius*R_SUN)); // km/s

    printf("ROTCHECK star%d type=%d M=%g Mc=%g %d o=%g vrot=%g vcrit=%g f=%g ; ",
           star->starnum,
           star->stellar_type,
           star->mass,
           Outermost_core_mass(star),
           Outermost_core_mass(star)>0,
           omega,vrot,vcrit,vrot/vcrit);

    if(Outermost_core_mass(star)>0)
    {
        printf("%g %g %g %g",omega_core,vrot_core,vcrit_core,vrot_core/vcrit_core);
    }
    printf("\n");
#endif
}
