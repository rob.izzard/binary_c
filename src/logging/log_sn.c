#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef LOG_SUPERNOVAE
#define SN_LOG_EXTRAS

/*
 * This function outputs some data when there is a supernova
 *
 * Please note : this code has been cleaned up but it was for
 *               a VERY old project. You can probably do better!
 */
static void dump_sn(struct stardata_t * Restrict const stardata,
                    const Star_number star_number,
                    const Supernova sn_type,
                    const int temporal_position,
                    const double v1,
                    const double v2
    );


/*
 * Use SNprint to output, either to a file stream
 * or use Printf for the default (to screen or
 * binary_grid memory buffer).
 */
//#define SNprint fprintf(stdout,__VA_ARGS__)
#define SNprint(...) Printf(__VA_ARGS__);

void log_sn(struct stardata_t * Restrict stardata,
            const Star_number star_number,
            Supernova sn_type,
            const int temporal_position)
{
    double v1=0.0;
    double v2=0.0;
    const char s[NUM_SN_STRINGS][SN_STRING_LENGTH] = SN_STRINGS;

    /****************************************************/

    if(star_number == -1)
    {
        /*
         * First call when t=0 : initialize variables.
         */
        stardata->common.sn_count = 0;
        Star_number i;
    Starloop(i)
        {
            stardata->common.sn_last_time_type[i] = UNUSED_INT;
            stardata->common.gone_sn[i] = FALSE;
        }
    }
    else
    {
        /*
         * Found a supernova!
         * Up the supernova counter
         */
        stardata->common.sn_count++;
        if(stardata->common.sn_count == 1)
        {
            /* Dump ZAMS data - very important! */
            SNprint("SN_ZAMS %.6g %.6g %.6g %.6g %.6g %s %.6g\n",
                    stardata->common.zero_age.mass[0],
                    stardata->common.zero_age.mass[1],
                    stardata->common.zero_age.separation[0],
                    stardata->common.zero_age.eccentricity[0],
                    stardata->common.metallicity,
                    s[sn_type],
                    stardata->model.probability);
        }

        /* set the last_time flag */
        if(temporal_position == PRE_SN)
        {
            Dprint("dump sn PRE_SN ... \n");
            stardata->star[star_number].sn_last_time=TRUE;
            stardata->common.sn_last_time_type[star_number]=sn_type;
            stardata->common.gone_sn[star_number]=TRUE;

            /* We don't know the kick velocities yet */
            v1 = 0.0;
            v2 = 0.0;
        }
        else if(temporal_position == POST_SN)
        {
            Dprint("dump sn POST_SN ...\n");

            stardata->star[star_number].sn_last_time=FALSE;
            sn_type = stardata->common.sn_last_time_type[star_number];

            /* Set kick velocities */
            v1 = stardata->star[star_number].sn_v;
            v2 = stardata->star[star_number].sn_rel_v;
        }

        /* Star number star_number is the one that has gone SN */
        dump_sn(stardata,
                star_number,
                sn_type,
                temporal_position,
                v1,
                v2);

        /* Set star k's supernova variable for logging */
        stardata->star[star_number].supernova++;
    }
}

/*******************************************************************/
/* A static function to dump the details of the SN to the log file */
/*******************************************************************/

static void dump_sn(struct stardata_t *stardata,
                    const Star_number star_number,
                    const Supernova sn_type,
                    const int temporal_position,
                    const double v1,
                    const double v2)
{
    /*
     * Output data to a log
     */
    struct star_t * exploder = &stardata->star[star_number];
    struct star_t * companion = &stardata->star[Other_star(star_number)];

    /*
     * Output data
     */
    SNprint("SN_%s %d %d %.6g %.6g %.6g %.6g %.6g %d %.6g %.6g %d %.6g %.6g ",
            temporal_position==PRE_SN ? "PRE" : "POST",
            exploder->stellar_type,
            sn_type, /* 3 */

            stardata->model.probability,
            stardata->model.time,
            stardata->common.orbit.separation,

            /* Star that supernovas... */
            exploder->mass, //7
            Outermost_core_mass(exploder),
            exploder->stellar_type,

            /* Companion */
            companion->mass, //10
            Outermost_core_mass(companion),
            companion->stellar_type,

            /* Kick velocity information */
            v1, // 13
            v2
        );

    if(exploder->supernova > 0 &&
       companion->supernova > 0)
    {
        stardata->common.sn_count++;
        stardata->common.double_sn_prob += stardata->model.probability;
    }

    SNprint("\n");
}
#endif
