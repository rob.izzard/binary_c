#include "../binary_c.h"
No_empty_translation_unit_warning;


/*
 * Clean up SVN_REVISION
 */
#define __DO_EXPAND(M) M ## 1
#define __EXPAND(M) __DO_EXPAND(M)
#if (!defined(SVN_REVISION)||(__EXPAND(SVN_REVISION)==1))
#undef SVN_REVISION
#undef SVN_URL
#endif

void No_return dump_stardata(struct stardata_t * const stardata,
                             const char * const filename)
{
    /*
     * Dump a stardata struct to a file as binary data.
     */
    FILE * dumpfile = fopen(filename,"w");
    if(dumpfile==NULL)
    {
        Exit_binary_c(BINARY_C_FILE_OPEN_ERROR,
                      "Error when opening stardata dump file at %s\n",
                      filename);
    }

    struct stardata_dump_t * stardata_dump =
        Calloc(1,sizeof(struct stardata_dump_t));

    /*
     * Write a header as:
     *
     * int : SVN_REVISION
     * char[50] : version string
     * size_t : sizeof(struct stardata_t)
     * size_t : sizeof(struct preferences_t)
     */
    stardata_dump->dump_format = STARDATA_DUMP_FORMAT;
#ifdef SVN_REVISION
    stardata_dump->svn_revision = SVN_REVISION;
#else
    stardata_dump->svn_revision = SVN_REVISION_UNDEFINED;
#endif

#ifdef GIT_REVISION
    snprintf(stardata_dump->git_revision,10,"undefined");
#else
    strlcpy(stardata_dump->git_revision,
            Stringof(GIT_REVISION),
            STRING_LENGTH);
#endif//GIT_REVISION

    strlcpy(stardata_dump->versionstring,
            BINARY_C_VERSION,
            50);

    stardata_dump->sizeof_stardata_t = sizeof(struct stardata_t);
    stardata_dump->sizeof_preferences_t = sizeof(struct preferences_t);

    /*
     * Populate the data sections
     */
    memcpy(&stardata_dump->preferences,
           stardata->preferences,
           sizeof(struct preferences_t));
    memcpy(&stardata_dump->stardata,
           stardata,
           sizeof(struct stardata_t));

    /*
     * Erase pointers : these cannot be stored
     */
    struct stardata_t * s = &stardata_dump->stardata;

    s->model.log_fp = NULL;
    s->store = NULL;
    s->tmpstore = NULL;
    s->previous_stardatas = NULL;
    s->previous_stardata = NULL;
    s->stardata_stack = NULL;
#ifdef CODESTATS
    s->codestats = NULL;
#endif//CODESTATS
#ifdef BINARY_C_API
    s->model.api_log_fp = NULL;
#endif//BUFFERED_STACK
    s->common.events = NULL;
#ifdef MEMOIZE
    s->common.memo = NULL;
#endif//MEMOIZE
    s->tmpstore->file_log_prevstring = NULL;

    /*
     * Write to the file
     */
    if(fwrite(stardata_dump,
              1,
              sizeof(struct stardata_dump_t),
              dumpfile)< sizeof(struct stardata_dump_t))
    {
        Exit_binary_c(BINARY_C_FILE_CLOSE_ERROR,
                      "Error when writing stardata dump file to %s",
                      filename);
    }

    if(stardata->n_previous_stardatas)
    {
        /*
         * Write previous_stardatas stack
         */
        unsigned int i;
        for(i=0;i<stardata->n_previous_stardatas;i++)
        {
            if(fwrite(stardata->previous_stardatas[i],
                      1,
                      sizeof(struct stardata_t),
                      dumpfile) < sizeof(struct stardata_t))
            {
                Exit_binary_c(BINARY_C_FILE_CLOSE_ERROR,
                              "Error when writing stardata dump file (previous_stardata stack %u/%u) to %s",
                              i,
                              stardata->n_previous_stardatas,
                              filename);
            }
        }
    }

    /*
     * Close the file
     */
    if(fclose(dumpfile))
    {
        Exit_binary_c(BINARY_C_FILE_CLOSE_ERROR,
                      "Error when closing stardata dumpfile %s\n",
                      filename);
    }


    /*
     * Free memory and exit
     */
    Safe_free(stardata_dump);

    show_stardata(stardata);
    printf("STARDATA (PREV)\n");
    show_stardata(stardata->previous_stardata);

    Exit_binary_c(BINARY_C_NORMAL_EXIT,
        "Dumped stardata to %s\n",
        filename);


}
