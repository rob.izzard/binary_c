/*
 * Subroutine to do all the logging at the end of a timestep.
 *
 * Note: this is for custom logging. You should probably be
 * using the population ensemble (see src/ensemble/ensemble_log.c)
 * to do this kind of logging.
 *
 * To put your own logging in here, choose a string (e.g. "FRED") and put
 * your log function in #defines, e.g.
 *
 * #define FRED
 *
 *     Printf("FRED %g %g %g\n",
 *            stardata->model.time,
 *            stardata->star[0].stellar_type,
 *            stardata->common.orbit.separation);
 *
 * #endif // FRED
 *
 * You should use the Printf() binary_c macro instead
 * of printf().
 *
 */

#include "../binary_c.h"
No_empty_translation_unit_warning;

void log_every_timestep(struct stardata_t * Restrict const stardata)
{

    /*
     * No logging when RLOF-interpolating
     * (negative timesteps are bad!).
     */
    if(unlikely(stardata->model.dtm < -TINY)) return;

    /* timestep and dtp */
    const Boolean first_timestep = Is_zero(stardata->model.time);
    if(unlikely(first_timestep == TRUE)) stardata->model.prev_log_t = 0.0;
    const double dt = stardata->model.time - stardata->model.prev_log_t;
    const double dtp Maybe_unused = dt * stardata->model.probability;
    stardata->model.prev_log_t = stardata->model.time;
//    double tc;

    Dprint("top\n");
    /* single star condition */
    const Boolean single Maybe_unused = system_is_observationally_single(stardata);
    const Boolean binary Maybe_unused = Not(single);

    /* detect final timestep */
    const Boolean final Maybe_unused = Fequal(stardata->model.time,
                                              stardata->model.max_evolution_time);

    Dprint("next\n");
    if(0)Printf("TESTLOG %30.12e %g %g %d %g %g %g\n",
                     stardata->model.time, // 1
                     stardata->star[0].mass,//2
                     Outermost_core_mass(&stardata->star[0]),
                     stardata->star[0].stellar_type,//4
                     stardata->star[1].mass,
                     stardata->common.orbit.separation ,//6
                     stardata->common.orbit.eccentricity//7
                    );

    if(0)Printf("TESTLOG t=%30.12e M1=%g Mc1=%g k1=%d M2=%g a=%g e=%g R=%g RL=%g dt=%g\n",
                     stardata->model.time, // 1
                     stardata->star[0].mass,//2
                     Outermost_core_mass(&stardata->star[0]),
                     stardata->star[0].stellar_type,//4
                     stardata->star[1].mass,
                     stardata->common.orbit.separation ,//6

                     stardata->common.orbit.eccentricity//7
                     , stardata->star[0].radius //8
                     , stardata->star[0].roche_radius //9
                     , stardata->model.dt
                    );

    Dprint("pre event logging\n");
    /* Handle SN event logging */
#ifdef EVENT_BASED_LOGGING
    event_based_logging_handle_events(stardata);
#endif // EVENT_BASED_LOGGING
    Dprint("next\n");
#ifdef MINT
    if(
        1 &&
        stardata->preferences->stellar_structure_algorithm == STELLAR_STRUCTURE_ALGORITHM_MINT &&
        (stardata->star[0].stellar_type <= 3)
        )
    {
        struct star_t * const star = &stardata->star[0];
        static Boolean first = TRUE;
        if(first)
        {
            printf("HRDHRD t M st R logTeff logL XHc XHec Mcfrac degeneracy logLHe dMcfracdt ddegeneracydt logHeLfrac\n");
            first = FALSE;
        }
        printf("HRDHRD %g %g %d %g %g %g %g %g %g %g %g %g %g %g\n",
               stardata->model.time,
               star->mass,
               star->stellar_type,
               star->radius,
               HRdiagram(0),
               star->mint->XHc, // 6
               star->mint->XHec, // 7
               star->core_mass[CORE_He]/star->mass, //8
               star->mint->central_degeneracy,//9
               Safelog10(star->mint->helium_luminosity),//10
               star->derivative[DERIVATIVE_STELLAR_He_CORE_MASS],//11
               star->derivative[DERIVATIVE_STELLAR_CENTRAL_DEGENERACY],//12
               star->mint->log_helium_luminosity_fraction // He ignited
            );
        fflush(stdout);
    }

    /*
     * This output prints everything needed for tides
     * The reference to mint at the end causes segfaults if the star leaves the MS
     */

    /*
     * Here the criterion on TIDES_CONVECTIVE_DAMPING_ZAHN1989 is a proxy for using MINT
     * we want to make sure we use MINT to refer to XHc
     */
    if(0 &&
            stardata->preferences->tides_convective_damping == TIDES_CONVECTIVE_DAMPING_ZAHN1989 &&
            stardata->star[0].stellar_type <= 1 &&
            stardata->star[1].stellar_type <= 1)
    {
        Printf("TIDES %.10e %g %g %g "
               "%g %g %g %g "
               "%g %g %g %g "
               "%.10e %.10e %g %g "
               "%g %g %g %g "
               "%g %g %g %g "
               "%i %g %g %g "
               "%g %g\n",
               stardata->model.time,
               stardata->model.dt,
               stardata->star[0].mass, // 3
               stardata->star[1].mass,
               stardata->star[0].omega, //5
               stardata->star[1].omega,
               stardata->star[0].luminosity, // 7
               stardata->star[1].luminosity,
               stardata->star[0].radius,
               stardata->star[1].radius,
               stardata->common.orbit.separation, //11
               stardata->common.orbit.eccentricity,
               stardata->common.orbit.period,
               stardata->common.orbit.angular_momentum,//14
               stardata->common.metallicity,
               stardata->model.probability,
               stardata->star[0].derivative[DERIVATIVE_STELLAR_ECCENTRICITY],//17
               stardata->star[1].derivative[DERIVATIVE_STELLAR_ECCENTRICITY],
               stardata->model.derivative[DERIVATIVE_ORBIT_ECCENTRICITY_TIDES],
               stardata->star[0].derivative[DERIVATIVE_STELLAR_ANGULAR_VELOCITY_TIDES],//20
               stardata->star[1].derivative[DERIVATIVE_STELLAR_ANGULAR_VELOCITY_TIDES],
               stardata->star[0].derivative[DERIVATIVE_STELLAR_ANGMOM_TIDES],//22
               stardata->star[1].derivative[DERIVATIVE_STELLAR_ANGMOM_TIDES],
               stardata->model.derivative[DERIVATIVE_ORBIT_ANGMOM_TIDES],
               binary,//25
               stardata->common.orbit.pseudo_sync_angular_frequency,
               stardata->star[0].omega_crit,
               stardata->star[1].omega_crit, //28
               stardata->star[0].mint->XHc,
               stardata->star[1].mint->XHc //28
              );
    }

#else //MINT
    /* if we use -e.g.- BSE tides we cannot get to Xc: we put a dummy value instead */
    if(0 &&
            stardata->preferences->tides_convective_damping != TIDES_CONVECTIVE_DAMPING_ZAHN1989 &&
            stardata->star[0].stellar_type <= 1 &&
            stardata->star[1].stellar_type <= 1)
    {
        Printf("TIDES %.10e %g %g %g "
               "%g %g %g %g "
               "%g %g %g %g "
               "%.10e %.10e %g %g "
               "%g %g %g %g "
               "%g %g %g %g "
               "%i %g %g %g "
               "%g %g\n",
               stardata->model.time,
               stardata->model.dt,
               stardata->star[0].mass, // 3
               stardata->star[1].mass,
               stardata->star[0].omega, //5
               stardata->star[1].omega,
               stardata->star[0].luminosity, // 7
               stardata->star[1].luminosity,
               stardata->star[0].radius,
               stardata->star[1].radius,
               stardata->common.orbit.separation, //11
               stardata->common.orbit.eccentricity,
               stardata->common.orbit.period,
               stardata->common.orbit.angular_momentum,//14
               stardata->common.metallicity,
               stardata->model.probability,
               stardata->star[0].derivative[DERIVATIVE_STELLAR_ECCENTRICITY],//17
               stardata->star[1].derivative[DERIVATIVE_STELLAR_ECCENTRICITY],
               stardata->model.derivative[DERIVATIVE_ORBIT_ECCENTRICITY_TIDES],
               stardata->star[0].derivative[DERIVATIVE_STELLAR_ANGULAR_VELOCITY_TIDES],//20
               stardata->star[1].derivative[DERIVATIVE_STELLAR_ANGULAR_VELOCITY_TIDES],
               stardata->star[0].derivative[DERIVATIVE_STELLAR_ANGMOM_TIDES],//22
               stardata->star[1].derivative[DERIVATIVE_STELLAR_ANGMOM_TIDES],
               stardata->model.derivative[DERIVATIVE_ORBIT_ANGMOM_TIDES],
               binary,//25
               stardata->common.orbit.pseudo_sync_angular_frequency,
               stardata->star[0].omega_crit,
               stardata->star[1].omega_crit,
               -1.0, //dummy value for XHc in the primary
               -1.0  //dummy value for XHc in the secondary
              );
    }

#endif//MINT

    if(0)
    {
        if(0 && RLOF_overflowing(stardata, 1.0))
        {
            stardata->model.max_evolution_time = stardata->model.time - stardata->model.dtm;
            printf("There is a RLOF event at time=%g, e=%g, P=%g \n",
                   stardata->model.time,
                   stardata->common.orbit.eccentricity,
                   stardata->common.orbit.period);
        }

        if(stardata->model.time < stardata->model.max_evolution_time)
        {
            /*Foreach_star(star)
            * {
            * const double tf = convective_turnover_time(star->radius,
            *                                            star,
            *                                            stardata);
            *Printf("TIDES_TESTING_OUTPUT %d %g %g\n", star->starnum, tf, star->renv);
            *}
            */
//            tc = convective_turnover_time(stardata->star[0].radius, stardata.star[0],stardata);

            if(1)
            {
                Printf("TIDES %.10e %g %g %g "
                       "%g %g %g %g "
                       "%g %g %g %g "
                       "%.10e %.10e %g %g "
                       "%g %g %g %g "
                       "%g %g %g %g "
                       "%i %g %g %g \n",
                       stardata->model.time,
                       stardata->model.dt,
                       stardata->star[0].mass, // 3
                       stardata->star[1].mass,
                       stardata->star[0].omega, //5
                       stardata->star[1].omega,
                       stardata->star[0].luminosity, // 7
                       stardata->star[1].luminosity,
                       stardata->star[0].radius,
                       stardata->star[1].radius,
                       stardata->common.orbit.separation, //11
                       stardata->common.orbit.eccentricity,
                       stardata->common.orbit.period,
                       stardata->common.orbit.angular_momentum,//14
                       stardata->common.metallicity,
                       stardata->model.probability,
                       stardata->star[0].derivative[DERIVATIVE_STELLAR_ECCENTRICITY],//17
                       stardata->star[1].derivative[DERIVATIVE_STELLAR_ECCENTRICITY],
                       stardata->model.derivative[DERIVATIVE_ORBIT_ECCENTRICITY_TIDES],
                       stardata->star[0].derivative[DERIVATIVE_STELLAR_ANGULAR_VELOCITY_TIDES],//20
                       stardata->star[1].derivative[DERIVATIVE_STELLAR_ANGULAR_VELOCITY_TIDES],
                       stardata->star[0].derivative[DERIVATIVE_STELLAR_ANGMOM_TIDES],//22
                       stardata->star[1].derivative[DERIVATIVE_STELLAR_ANGMOM_TIDES],
                       stardata->model.derivative[DERIVATIVE_ORBIT_ANGMOM_TIDES],
                       binary,//25
                       stardata->common.orbit.pseudo_sync_angular_frequency,
                       stardata->star[0].omega_crit,
                       stardata->star[1].omega_crit//, //28
                      );
//                stardata->star[0].mint->XHc,
//                stardata->star[1].mint->XHc //28
            }

            /*
             * Check that the star does not go in the upperleft corner of
             * logP-e diagram when running a population.
             */
            /*        printf("binary %s : e %g : log10(P/d) %g\n",
                           Yesno(binary),
                           stardata->common.orbit.eccentricity,
                           log10(stardata->common.orbit.period * YEAR_LENGTH_IN_DAYS));
            */

            if( 0 && binary == TRUE &&
                    stardata->common.orbit.eccentricity > 0.5 &&
                    log10(stardata->common.orbit.period * YEAR_LENGTH_IN_DAYS) < 0.0)
            {
                Exit_binary_c(BINARY_C_ALGORITHM_BRANCH_FAILURE,
                              "waterfall model : eccentricity = %g and log(P/d) = %g.\n",
                              stardata->common.orbit.eccentricity,
                              log10(stardata->common.orbit.period * YEAR_LENGTH_IN_DAYS)
                             );
            }
        }
    }
    Dprint("next\n");
#if defined GAIA &&                             \
    defined STELLAR_COLOURS
    if(1)
    {
        /*
         * Colours for Gaia
         */
        const Boolean gaiavb = FALSE;
        Foreach_star(star)
        {
            /* Kurucz colours (Hurley) */
            double M_v, U_minus_B, B_minus_V, V_minus_I, M_b, M_u, M_i, M_bol;
            binary_magnitudes(stardata,
                              stardata->store,
                              star->starnum,
                              &M_v,
                              &U_minus_B,
                              &B_minus_V,
                              &V_minus_I,
                              &M_b,
                              &M_u,
                              &M_i,
                              &M_bol,
                              NULL,
                              NULL,
                              NULL,
                              NULL);
            /* star information */
            char specstring[10]; // spectral type string
            spectral_type_string(specstring, stardata, star);
            if(gaiavb == TRUE)
            {
                Printf("t=%g dt=%g P=%g sn=%d st=%d M=%g logL=%g Mbol=%g logTeff=%g logg=%g sp=%s\n",
                       stardata->model.time,
                       dt,
                       stardata->model.probability,
                       star->starnum,
                       star->stellar_type,
                       star->mass,
                       log10(star->luminosity),
                       M_bol,
                       log10(Teff_from_star_struct(star)),
                       logg(star),
                       specstring);

                /* Kurucz information */
                Printf("GAIA%d KURUCZ  U=% 8g B=% 8g V=% 8g R=******** I=% 8g\n",
                       star->starnum,
                       M_u,
                       M_b,
                       M_v,
                       M_i);
            }
            /* Eldridge colours */
            double magnitudes[STELLAR_MAGNITUDE_NUMBER];
            int c;
            /* 2012 */
            eldridge2012_magnitudes(stardata, star, magnitudes);
            if(gaiavb == TRUE)
            {
                Printf("GAIA%d JJE2012 ", star->starnum);
                for (c = 0; c < 8; c++)
                {
                    Printf("%s=% 8g ",
                           stellar_magnitude_strings[c],
                           magnitudes[c]);
                }
                Printf("\n");
            }

            /* 2015 */
            eldridge2015_magnitudes(stardata, star, magnitudes);

            gaia_magnitudes(stardata, star, magnitudes);

            if(gaiavb == TRUE)
            {
                Printf("GAIA%d JJE2015 ", star->starnum);
                for (c = 0; c < STELLAR_MAGNITUDE_NUMBER; c++)
                {
                    Printf("%s=% 8g ",
                           stellar_magnitude_strings[c],
                           magnitudes[c]);
                }
                Printf("\n");
            }
        }
    }
#endif // GAIA and STELLAR_COLOURS


    Dprint("next\n");
#ifdef NUCSYN
    /*
     * nucsyn logging: note that nucsyn_binary_yield()
     * MUST be called before ensemble_log()
     */
    nucsyn_binary_yield(stardata, FALSE);
    Reset_num_thermal_pulses(0);
    Reset_num_thermal_pulses(1);
#endif // NUCSYN

#ifdef STELLAR_POPULATIONS_ENSEMBLE
    if(stardata->preferences->ensemble == TRUE)
    {
        ensemble_log(stardata,
                     FALSE);
    }
#endif //STELLAR_POPULATIONS_ENSEMBLE

//#define MIMF
#ifdef MIMF
    if(stardata->star[0].stellar_type == TPAGB)
    {
        Printf("MCWD %g %g\n",
               stardata->common.zero_age.mass[0],
               Outermost_core_mass(&stardata->star[0]));
    }
#endif // MIMF

#ifdef TPAGBTRACKS
    if(first_timestep)
    {
        Printf("TPAGBSTAR\n");
    }
    else if(stardata->star[0].stellar_type == TPAGB)
    {
        Printf("TPAGBSTAR %g %g\n",
               Outermost_core_mass(&stardata->star[0]),
               stardata->star[0].mass);
    }
#endif // TPAGBTRACKS



#ifdef BLUE_STRAGGLER_PROJECT
    if(stardata->star[0].blue_straggler == TRUE ||
            stardata->star[1].blue_straggler == TRUE)
    {
        if(single)
        {
            Printf("BSS0 %g %g\n",
                   dtp,
                   stardata->model.time);
        }
        else
        {
            /* which star is the blue straggler? */
            const int kbs = stardata->star[0].mass > stardata->common.zero_age.mass[0] ? 0 : 1;
            struct star_t *bs = &(stardata->star[kbs]);
            struct star_t *comp = &(stardata->star[Other_star(kbs)]);

            Printf("BSS1 %g %g %g %g %g %d\n",
                   dtp,
                   stardata->model.time,
                   stardata->common.orbit.period * 365.25,
                   bs->mass - stardata->common.zero_age.mass[kbs], // mass accreted
                   comp->mass, // companion mass
                   comp->stellar_type // companion stellar type
                  );
        }
    }
#endif //BLUE_STRAGGLER_PROJECT

#ifdef BLUE_STRAGGLER_BARIUM_PROJECT




    // start counting at OLD_TIME
#define OLD_TIME 10e3

    if(stardata->model.time > OLD_TIME)
    {
        /* do not count back beyond OLD_TIME */
        double ddtp = Min(stardata->model.time - OLD_TIME, dt) *
                      stardata->model.probability;

        if(stardata->star[0].blue_straggler == TRUE ||
                stardata->star[1].blue_straggler == TRUE)
        {
            /* blue straggler statistics */

            /* which star is the blue straggler? */
            const int kbs = stardata->star[0].mass > stardata->conmon.zero_age.mass[0] ? 0 : 1;
            struct star_t *bs = &(stardata->star[kbs]);

            /* look for old, blue straggler dwarfs */
            if(ON_MAIN_SEQUENCE(bs->stellar_type))
            {
                //struct star_t *comp = &(stardata->star[Other_star(kbs)]);
                double *X = nucsyn_observed_surface_abundances(bs);

                if(0)
                    Printf("Ba=%g/%g, Fe=%g/%g\n",
                           nucsyn_elemental_abundance("Ba", X, stardata, stardata->store),
                           nucsyn_elemental_abundance("Ba", stardata->common.Xsolar, stardata, stardata->store),
                           nucsyn_elemental_abundance("Fe", X, stardata, stardata->store),
                           nucsyn_elemental_abundance("Fe", stardata->common.Xsolar, stardata, stardata->store)
                          );


//      Printf("BSSBa ddtp=%g t=%g P/days=%g dm=%g [C/Fe]=%g [Ba/Fe]=%g\n",
                Printf("BSSBa%d %g %g %g %g %g %g\n",
                       single ? 0 : 1,
                       ddtp,
                       stardata->model.time,
                       stardata->common.orbit.period * 365.25,
                       bs->mass - stardata->common.zero_age[bs->starnum], // mass accreted
                       nucsyn_elemental_square_bracket("C",
                                                       "Fe",
                                                       X,
                                                       stardata->common.Xsolar,
                                                       stardata),
                       nucsyn_elemental_square_bracket("Ba",
                                                       "Fe",
                                                       X,
                                                       stardata->common.Xsolar,
                                                       stardata)
                      );
            }
        }

        /*
         * MS dwarf statistics:
         * count all MS stars, also MS-MS binaries
         * but only log the primary (brightest) star
         */
        struct star_t * star = NULL;
        if(ON_MAIN_SEQUENCE(stardata->star[0].stellar_type))
        {
            // star 1 is a MS star
            int j;
            if(ON_MAIN_SEQUENCE(stardata->star[1].stellar_type))
            {
                // MS MS binary : log the "primary" == brightest star
                j = stardata->star[0].luminosity > stardata->star[1].luminosity ? 0 : 1;
            }
            else
            {
                // log only the MS star
                j = 1;
            }
            star = &(stardata->star[j]);
        }
        else if(ON_MAIN_SEQUENCE(stardata->star[1].stellar_type))
        {
            // star 1 is not MS, but star 2 is
            star = &(stardata->star[1]);
        }

        // proceed if we have a star to log
        if(star != NULL)
        {
            double *X = nucsyn_observed_surface_abundances(star);
            Printf("MS%d %g %g %g %g %g %g\n",
                   single ? 0 : 1,
                   ddtp,
                   stardata->model.time,
                   stardata->common.orbit.period * 365.25,
                   star->mass - stardata->common.zero_age.mass[star->starnum], // mass accreted
                   nucsyn_elemental_square_bracket("C",
                                                   "Fe",
                                                   X,
                                                   stardata->common.Xsolar,
                                                   stardata),
                   nucsyn_elemental_square_bracket("Ba",
                                                   "Fe",
                                                   X,
                                                   stardata->common.Xsolar,
                                                   stardata)
                  );
        }
    }
#endif // BLUE_STRAGGLER_BARIUM_PROJECT

#ifdef WTTS_LOG
    if(stardata->preferences->wtts_log == TRUE)
    {
        wtts_log(stardata, dt);
    }
#endif // WTTS_LOG

#if defined NUCSYN &&                           \
    defined YONG_PROJECT
    {
        /*
         * Project to plot the magnesium ratios
         */
#define YONGOUT(K)                                  \
        if(stardata->star[(K)].stellar_type>1 &&    \
           stardata->star[(K)].stellar_type<7)      \
        {                                           \
            Printf("YONG%c %g %g\n",                \
                   single?'s':'b',                  \
                   stardata->star[(K)].Xenv[XMg26]/ \
                   stardata->star[(K)].Xenv[XMg24], \
                   dtp);                            \
        }

        YONGOUT(0);
        YONGOUT(1);
    }
#endif//YONG_PROJECT

#ifdef CANBERRA_PROJECT
    {
        /* output MS star */
        if(first_timestep == TRUE)
        {
            if(stardata->common.canberra_done_first_timestep == FALSE)
            {
                stardata->common.canberra_done_first_timestep = TRUE;

                // choose non-zero mass star
                const int k = Is_zero(stardata->star[0].mass) ? 1 : 0;
                SETstar(k);
                Printf("CANBERRA_TEST%d %g %g %d",
                       1 - single,
                       stardata->model.probability,
                       star->mass,
                       spectral_type(stardata, star));

                if(binary == TRUE)
                {
                    Printf(" %g %g %g sp2=%d\n",
                           stardata->star[1].mass / star->mass,
                           stardata->common.orbit.period * 365.25,
                           stardata->common.orbit.eccentricity,
                           spectral_type(stardata, &stardata->star[1]));
                }
                else
                {
                    Printf("\n");
                }

                Printf("CANBERRA_ZAMS_SPECTRAL_TYPE %g %d %g %g\n",
                       star->mass,
                       spectral_type(stardata, star),
                       Teff_from_star_struct(star),
                       long_spectral_type(stardata, star));
            }
        }
        else
        {
            stardata->common.canberra_done_first_timestep = FALSE;
        }

        const Star_number more_massive =
            stardata->star[0].mass > stardata->star[1].mass ? 0 : 1;
        const Spectral_type more_massive_spectype = spectral_type(&stardata->star[more_massive]);

        /* MS (G/K) type binaries */
        if(binary == TRUE &&
                ON_MAIN_SEQUENCE(stardata->star[more_massive].stellar_type) &&
                (more_massive_spectype == SPECTRAL_TYPE_G ||
                 more_massive_spectype == SPECTRAL_TYPE_K))
        {
            /* MS q distribution */

            /* select only G/K binaries */

            /* q = M1/M2 or M2/M1, whichever is less */
            const double q = stardata->star[Other_star(more_massive)].mass /
                             stardata->star[more_massive].mass;
            Printf("CANBERRAMSMSQ %g %g (from M1=%g M2=%g)\n", q, dtp, stardata->star[0].mass, stardata->star[1].mass);
        }


        /* WD system logging */
        if(WHITE_DWARF(stardata->star[0].stellar_type) ||
                WHITE_DWARF(stardata->star[1].stellar_type))
        {
            /* always log the WD first: find out which star it is */
            const int k_wd = WHITE_DWARF(stardata->star[0].stellar_type) ? 0 : 1;

            if(single)
            {
                /* single WD : log its dtp */
                Printf("CANBERRA0 %g %g %d %d %g\n",
                       dtp,
                       stardata->model.time,
                       stardata->star[k_wd].stellar_type,
                       spectral_type(stardata, &stardata->star[k_wd]),
                       Teff(k_wd)
                      );
            }
            else
            {
                /* binary with a white dwarf */
                Printf("CANBERRA1 %g %g %d %d %g %d %d %g %g %g\n",
                       dtp,//1
                       stardata->model.time, //2
                       stardata->star[k_wd].stellar_type,//3
                       spectral_type(stardata, &stardata->star[k_wd]), //4
                       Teff(k_wd),//5
                       stardata->star[Other_star(k_wd)].stellar_type,//6
                       spectral_type(stardata, &stardata->star[Other_star(k_wd)]), //7
                       Teff(Other_star(k_wd)),//8
                       stardata->common.orbit.period * 365.25, //9
                       stardata->common.orbit.eccentricity//10
                      );
            }
        }
    }
#endif


#ifdef CARLO_OUTPUT //CAB
#ifndef NUCSYN
    Printf("testCarlo t= %1.5e  P= %1.5e  a= %1.5e  %i  m1= %1.5e  m2= %1.5e \n",
           stardata->model.time,
           stardata->common.orbit.period * 365.25,
           stardata->common.orbit.separation,
           stardata->star[0].stellar_type,
           stardata->star[0].mass,
           stardata->star[1].mass
          );
#endif
#ifdef NUCSYN
    {
        struct star_t * star = &(stardata->star[0]);

        Abundance * Xenv = nucsyn_observed_surface_abundances(star);
        /***** t     type M1 Menv ntp  C  N  O  F  Ne  ******/
        Star_number k;

        Starloop(k)
        {
            Printf("CAB %1.4e %i %i %1.5f %1.5f %g %1.2f %1.2f %1.2f %1.2f %1.2f \n",
                   stardata->model.time,
                   k,
                   stardata->star[k].stellar_type,
                   stardata->star[k].mass,
                   Outermost_core_mass(&stardata->star[k]),
                   stardata->star[k].num_thermal_pulses,
                   log10(stardata->star[k].Xenv[XC12] + stardata->star[k].Xenv[XC13]),
                   log10(stardata->star[k].Xenv[XN14] + stardata->star[k].Xenv[XN15]),
                   log10(stardata->star[k].Xenv[XO16] + stardata->star[k].Xenv[XO17] + stardata->star[k].Xenv[XO18]),
                   log10(stardata->star[k].Xenv[XF19]),
                   log10(stardata->star[k].Xenv[XNe22] + stardata->star[k].Xenv[XNe20] + stardata->star[k].Xenv[XNe21])
                  );
        }
    }
#endif // NUCSYN
#endif // CARLO_OUTPUT




#ifdef NUCSYN

    if(0)
    {
        Star_number k;

        Starloop(k)
        {
            if(stardata->star[k].stellar_type == TPAGB)
            {
                Printf("TPAGBPOP %d %g %g %g %g\n",
                       k,
                       stardata->common.zero_age.mass[k],
                       stardata->model.probability * dt,
                       stardata->common.zero_age.mass[Other_star(k)],
                       stardata->common.zero_age.separation[0]
                      );
            }
        }
    }


    if(0)
    {
        // MCa=(MOo+MOa)*16/12-MCo
        struct star_t *star = &(stardata->star[0]);
        if(first_timestep == TRUE)

            Printf("carbon required %g\n",
                   (star->mass * star->Xenv[XO16] * 12.0 / 16.0
                    -
                    star->mass * star->Xenv[XC12])
                  );

        double vorb = 1e-5 * sqrt ( GRAVITATIONAL_CONSTANT *
                                    (stardata->star[0].mass + stardata->star[1].mass) * M_SUN /
                                    (stardata->common.orbit.separation * R_SUN));

        if(stardata->star[0].stellar_type == COWD &&
                stardata->star[1].roche_radius > stardata->star[1].radius)
        {
            Printf("vorb %g %g %g\n",
                   stardata->model.time,
                   vorb,

                   1e-5 * sqrt(GRAVITATIONAL_CONSTANT * M_SUN * stardata->star[0].mass / (stardata->star[0].radius * R_SUN))

                  );
        }
    }
#endif//NUCSYN

#ifdef TEST_NATURE

    {


        if(dt > -TINY)
        {
            int spec[NUMBER_OF_STARS];
            char *cc[] = SPECTRAL_TYPE_STRINGS;
            Star_number k;

            Starloop(k)
            {
                if(stardata->star[k].stellar_type < 6)
                {
                    spec[k] = spectral_type(stardata,
                                            &stardata->star[k]);
                    //      Printf("SPEC TYPE %s -> stellar type %d\n",
                    //cc[spec[k]],stardata->star[k].stellar_type);
                }
                else
                {
                    spec[k] = -1; // ignore!
                }
            }

            /* select only systems with an O star */
            if((spec[1] == SPECTRAL_TYPE_O) || (spec[2] == SPECTRAL_TYPE_O))
            {
                /* single- or binary-star statistics */
                Starloop(k)
                {
                    if(spec[k] == SPECTRAL_TYPE_O)
                    {
                        Printf("OTYPE%d %d %g %g\n",
                               k,
                               stardata->model.sgl,
                               stardata->model.time,
                               dtp);
                    }
                }

                /* binary stars only */
                if(stardata->model.sgl == FALSE)
                {
                    if(spec[1] == SPECTRAL_TYPE_O &&
                            spec[2] == SPECTRAL_TYPE_O)
                    {
                        /* O-O binary */
                        Printf("binOO %g %g\n",
                               stardata->model.time,
                               dtp);
                    }
                    else
                    {
                        /* O-else binary */
                        Printf("binOX %g %g\n",
                               stardata->model.time,
                               dtp);
                    }
                }
            }
        }
    }
#endif //TEST_NATURE

#if defined SUPERNOVA_COMPANION_LOG &&          \
    defined LOG_SUPERNOVAE
    if(stardata->star[0].went_sn_last_time != 0)
    {
        log_supernova_sc(stardata);
    }
#endif

    Dprint("t=%g m1=%g m1_0=%g m2=%g m2_0=%g\n",
           stardata->model.time,
           stardata->star[0].mass, stardata->star[0].phase_start_mass,
           stardata->star[1].mass, stardata->star[1].phase_start_mass);
    if(0)
    {
        set_kelvin_helmholtz_times(stardata);
        if(stardata->star[0].stellar_type < 10)
        {
            const double tdyn1 = dynamical_timescale(&stardata->star[0]);
            const double tdyn2 = dynamical_timescale(&stardata->star[1]);

            Printf("TIMESCALES %g 1: %d dyn=%g kh=%g :2: %d dyn=%g kh=%g \n",
                   stardata->model.time,
                   stardata->star[0].stellar_type,
                   tdyn1,
                   stardata->star[0].tkh,
                   stardata->star[1].stellar_type,
                   tdyn2,
                   stardata->star[1].tkh);
        }
    }



#ifdef FABIAN_IMF_LOG
    {

        Boolean fabian_log = FALSE;

        // force logging at time 0.0
        if(Fequal(stardata->model.time, 0.0) && (!stardata->common.fabian_logged_zero))
        {
            /* first timestep */
            stardata->model.next_fabian_imf_log_time =
                stardata->model.fabian_imf_log_timestep;
            fabian_log = TRUE;
            stardata->common.fabian_logged_zero = TRUE;
        }
        else
        {
            //stardata->common.fabian_logged_zero=FALSE;
        }
        if((More_or_equal(stardata->model.time,
                           stardata->model.next_fabian_imf_log_time)))
        {
            // set next logging point and force logging
            stardata->model.next_fabian_imf_log_time = stardata->model.next_fabian_imf_log_time + stardata->model.fabian_imf_log_timestep;
            fabian_log = TRUE;
        }

        Dprint("Fabian IMF log t=%g next_fabian_imf_log_time=%g log_timestep=%g : do log? %d\n",
               stardata->model.time,
               stardata->model.next_fabian_imf_log_time,
               stardata->model.fabian_imf_log_timestep,
               fabian_log);

        if(fabian_log)
        {
            // do logging if star is on MS
            if((ON_MAIN_SEQUENCE(stardata->star[0].stellar_type)) ||
                    (ON_MAIN_SEQUENCE(stardata->star[1].stellar_type)) )
            {
                Printf("FABIAN %16.12e %16.12e %16.12e %16.12e %16.12e %d %d %16.12e %16.12e %16.12e %16.12e %16.12e %16.12e %g %g %g\n",
                       stardata->model.time,
                       stardata->star[0].mass,
                       stardata->star[1].mass,
                       stardata->star[0].luminosity,
                       stardata->star[1].luminosity,
                       stardata->star[0].stellar_type,
                       stardata->star[1].stellar_type,
                       stardata->common.orbit.separation,
                       stardata->common.orbit.eccentricity,
                       stardata->common.metallicity,
                       stardata->star[0].age,
                       stardata->star[1].age,
                       stardata->model.probability,
                       stardata->model.dt,
                       Teff(0),
                       Teff(1)
                      );
                //Printf("Teff %g %g\n",Teff(0),Teff(1));
            }
        }
    }

#endif

#ifdef PERIOD_ECC_PLOT
    {

        if((stardata->star[0].stellar_type < 10) &&
                (stardata->star[1].stellar_type < 10) &&
                (stardata->model.sgl == FALSE))
        {
            Printf("PE %d %g %g\n",
                   stardata->star[0].stellar_type,
                   stardata->common.orbit.period * 365.25,
                   stardata->common.orbit.eccentricity);
        }

        if(stardata->star[0].stellar_type > 6)
        {
            Exit_binary_c(BINARY_C_NORMAL_EXIT, "Post AGB star : exit\n");
        }
    }
#endif

#ifdef SINGLE_STAR_LIFETIMES
    if(stardata->star[0].stellar_type < HERTZSPRUNG_GAP)
    {
        stardata->common.done_single_star_lifetime = FALSE;
    }
    else if(stardata->common.done_single_star_lifetime == FALSE &&
            LATER_THAN_WHITE_DWARF(stardata->star[0].stellar_type))
    {
        Printf("SINGLE_STAR_LIFETIME %g %g\n",
               stardata->common.zero_age.mass[0],
               stardata->model.time);
#ifdef SINGLE_STAR_LIFETIMES_FLUSH
        fflush(NULL);
#endif
        stardata->common.done_single_star_lifetime = TRUE;
    }
#endif

#ifdef FILE_LOG
    {
#ifdef PRE_MAIN_SEQUENCE
        if(stardata->preferences->pre_main_sequence)
        {

            if(first_timestep) stardata->common.pms[0] = stardata->common.pms[1] = TRUE;

            Star_number k;
            Starloop(k)
            {
                if(stardata->common.pms[k] == TRUE &&
                        stardata->star[k].PMS == FALSE)
                {
                    Append_logstring(LOG_PREMAINSEQUENCE, " ZAMS %d", k);
                    stardata->common.pms[k] = FALSE;
                }
            }
        }
#endif // PRE_MAIN_SEQUENCE
    }
#endif //FILE_LOG

#ifdef NANCHECKS
    Dprint("execute not-a-number checks\n");
#define NCHECK(A,B,C) if(isnan((A))!=0){Printf("NaN %s star %d\n",(B),(C));fflush(stdout);fflush(stderr);kill(0,SIGSEGV);Exit_binary_c(BINARY_C_EXIT_NAN,"NaN detected");}
    {
        Foreach_star(star)
        {
            const Star_number k = star->starnum;
            NCHECK(star->mass, "Mass (evolution)", k);
            NCHECK(star->radius, "Radius (evolution)", k);
            NCHECK(star->luminosity, "Luminosity (evolution)", k);
            NCHECK(star->angular_momentum, "jspin", k);
        }
    }
    if(stardata->model.sgl == FALSE)
    {
        Nancheck(stardata->common.orbit.separation);
        Nancheck(stardata->common.orbit.eccentricity);
    }
    Nancheck(stardata->model.time);
#endif


#ifdef NUCSYN_LOWZ_SUPERNOVAE

    if(stardata->common.nucsyn_metallicity > -TINY &&
            stardata->common.nucsyn_metallicity < NUCSYN_LOWZ_SNE_THRESHOLD)
    {
        /* Use zero-Z supernovae for both stars */
        Abundance * X = New_clear_isotope_array;
        stardata->model.time = 0.0;
        nucsyn_binary_yield(stardata, TRUE);
        stardata->model.time = 4.000; // Assume output at 4Myr
#ifdef FILE_LOG
        output_string_to_log(stardata, "PopIII stars\n");
#endif

        // explode star 1
        nucsyn_lowz_yields(X, stardata->star[0].mass);
        calc_yields(stardata, &(stardata->star[0]), 1.0, X, 0, X, 1, YIELD_FINAL, SOURCE_POPIII);
        stardata->star[0].mass = 0.0;
        stardata->star[0].stellar_type = MASSLESS_REMNANT;
        // explode star 2
        nucsyn_lowz_yields(X, stardata->star[1].mass);
        calc_yields(stardata, &(stardata->star[1]), 1.0, X, 0, X, 2, YIELD_FINAL, SOURCE_POPIII);
        stardata->star[1].mass = 0.0;
        stardata->star[1].stellar_type = MASSLESS_REMNANT;

        /* Force output at 4Myr */
        nucsyn_binary_yield(stardata, TRUE);
        stardata->model.time = 4.001;
        end_of_iteration(stardata);
        nucsyn_binary_yield(stardata, TRUE);
        stardata->model.time = stardata->model.max_evolution_time;
        nucsyn_binary_yield(stardata, TRUE);

#ifdef FILE_LOG
        close_log_files(&(model->log_fp));
#endif
        Safe_free(X);
        return (0);
    }
#endif //NUCSYN_LOWZ_SUPERNOVAE


#ifdef NUCSYN_NETWORK_TEST
    nucsyn_network_test(stardata);
#endif

#if defined ADAPTIVE_RLOF &&                    \
    defined ADAPTIVE_RLOF_LOG
    /* new RLOF log */
    if( //(stardata->preferences->RLOF_method!=0)&&
        ((stardata->star[0].stellar_type < 10) ||
         (stardata->star[1].stellar_type < 10)))
    {
        /* find RLOFing star */
        int krl =
            (stardata->star[0].radius > stardata->star[0].roche_radius * 0.99) ? 0 :
            (stardata->star[1].radius > stardata->star[1].roche_radius * 0.99) ? 1 :
            -1;

        if(krl >= 0)
        {
            /* set thermal timescales and log */
            set_kelvin_helmholtz_times(stardata);
            struct star_t * donor = &stardata->star[krl];
            struct star_t * accretor = &stardata->star[Other_star(krl)];
            Printf("RLOFLOG %20.12e %g %g %g %g %g %g %g %g %d %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g\n",
                   stardata->model.time, // 1
                   stardata->common.mdot_RLOF, // 2 what we got
                   stardata->common.mdot_RLOF_adaptive, // 3 what the adaptive wants
                   donor->mass / donor->tkh, // 4 thermal rate
                   donor->radius, // 5
                   donor->roche_radius, // 6
                   donor->radius / donor->roche_radius, // 7
                   stardata->star[0].mass, //8
                   stardata->star[1].mass, // 9
                   krl, // 10 (number of rlofing star)
                   donor->rdot * 1e-6, // 11 da/dt (t=Myr)
                   donor->roldot, // 12 dRL/dt (t=yr)
                   stardata->common.orbit.separation, //13 (Rsun)
                   donor->mass, // 14
                   stardata->model.dt, // 15 dt (years)
                   stardata->model.dtm * 1e6, // 16 dtm (years)
                   accretor->radius, // 17
                   accretor->roche_radius, // 18
                   donor->nth, // 19
                   Max(0.0, donor->radius / donor->roche_radius), // 20
                   donor->radius, //21
                   accretor->radius, // 22
                   donor->luminosity, //23
                   accretor->luminosity, // 24
                   stardata->common.orbit.period * 365.25, // 25
                   stardata->common.orbit.angular_momentum // 26

                  );
        }
        else
        {
            //Printf("RLOFLOG * * * * * * * * * * * * * * * * * * * * * * *\n");
        }
        stardata->common.mdot_RLOF = 0.0;
        stardata->common.mdot_RLOF_adaptive = 0.0;

    }

    {
        double dM_RLOF_losedt;
        const double dt =  1e6 * stardata->model.dtm;
        if(stardata->model.time < TINY)
        {
            dM_RLOF_losedt = 0.0;
        }
        else if(dt > TINY)
        {
            dM_RLOF_losedt = (stardata->common.RLOF_log_m1was - stardata->star[0].mass) / dt;
        }
        else
        {
            dM_RLOF_losedt = 0.0;
        }


        if(dt > TINY)
        {
            /* thermal and dynamical rates (years) */
            double tkh1 = 1e7 * Pow2(stardata->star[0].mass) / (stardata->star[0].radius * stardata->star[0].luminosity);
            double tdyn1 = dynamical_timescale(&stardata->star[0]);

            /* omega crit (s^-1) */
            /* code omega = (seconds per year) * v (cm/s) / r(cm) */
            /* code omega units = per year */
            /* so to convert to cgs divide by YEAR_LENGTH_IN_SECONDS */
            calculate_rotation_variables(&(stardata->star[0]),
                                         stardata->star[0].radius);
            calculate_rotation_variables(&(stardata->star[1]),
                                         stardata->star[1].radius);

            Printf("MDOT %10.10e ", stardata->model.time); // 1
            if(stardata->model.in_RLOF == TRUE)
            {
                Printf("%g %g",
                       stardata->common.mdot_RLOF_adaptive, // 2
                       stardata->common.mdot_RLOF_BSE /* 3 */);
            }
            else
            {
                Printf("* *");
            }

            //        4  5  6  7  8  9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39
            Printf(" %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %d %d %d %d %d %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g\n",
                   dM_RLOF_losedt,//4
                   stardata->star[0].mass,//5
                   stardata->star[1].mass,
                   stardata->star[0].radius / Max(1e-14, stardata->star[0].roche_radius), //7
                   stardata->star[1].radius / Max(1e-14, stardata->star[1].roche_radius),
                   HRdiagram(0),//9,10  - macro (two elements)
                   HRdiagram(1),//11,12
                   stardata->common.orbit.separation,//13
                   stardata->star[0].v_eq,// 14, equatorial velocity km/s
                   stardata->star[1].v_eq,// 15
                   stardata->star[0].v_eq_ratio,//16
                   stardata->star[1].v_eq_ratio,//17
                   stardata->star[0].phase_start_mass,//18
                   stardata->star[1].phase_start_mass,//19
                   stardata->star[0].radius,//20
                   stardata->star[1].radius,//21
                   stardata->star[0].mass / tdyn1, //22
                   stardata->star[0].mass / tkh1, //23
                   stardata->star[0].roche_radius,//24
                   stardata->star[1].roche_radius,//25
                   stardata->common.used_rate,//26
                   stardata->common.runaway_rlof,//27
                   stardata->common.thermal_cap,//28
                   stardata->star[0].stellar_type,//29
                   stardata->star[1].stellar_type,//30
                   stardata->star[0].angular_momentum,//31
                   /* wind enhancement factors */
                   stardata->star[0].rotfac,//32
                   stardata->star[1].rotfac,//33
                   stardata->star[0].v_sph, //34
                   stardata->star[1].v_sph,//35
                   stardata->star[0].v_sph_ratio, //36
                   stardata->star[1].v_sph_ratio,//37
                   stardata->star[0].omega,//38
                   stardata->star[1].omega,//39
                   stardata->star[0].omega_ratio,//40
                   stardata->star[1].omega_ratio,//41
                   stardata->star[0].radius, //42
                   stardata->star[1].radius, //43
                   stardata->star[0].luminosity, //44
                   stardata->star[1].luminosity //45
                  );

            // reset RLOF parameters
            stardata->common.used_rate = RLOF_RATE_UNDEF;
            stardata->common.runaway_rlof = FALSE;
            stardata->common.thermal_cap = FALSE;

        }
        stardata->common.RLOF_log_m1was = stardata->star[0].mass;
    }
#endif //  ADAPTIVE_RLOF && ADAPTIVE_RLOF_LOG



#if DEBUG==1 &&                                 \
    defined NUCSYN
    {
        Abundance * Xenv = (Abundance*)
                           nucsyn_observed_surface_abundances(&(stardata->star[1]));
        if(stardata->star[1].stellar_type < 10)
        {
            Dprint("AB%d %g %g %d %g %g %g\n",
                   2,
                   stardata->model.time,
                   stardata->star[1].mass,
                   stardata->star[1].stellar_type,
                   Xenv[XC12],
                   Xenv[XC13],
                   Xenv[XN14]);
        }
    }
#endif

#ifdef NS_NS_BIRTH_LOG
    if(stardata->common.nsns == FALSE)
    {
        if((stardata->star[0].stellar_type == NEUTRON_STAR) &&
                (stardata->star[1].stellar_type == NEUTRON_STAR))
        {
            Printf("NASCENT_NSNS at t=%g p=%g\n",
                   stardata->model.time,
                   stardata->model.probability);
            stardata->common.nsns = TRUE;
        }
    }

    /* first timestep: restore nsns birth boolean to FALSE */
    if(first_timestep == TRUE)
    {
        stardata->common.nsns = FALSE;
    }
#endif

#ifdef FINAL_MASSES_LOG
    /* if star 1 is post-AGB then stop */
    if(stardata->star[0].stellar_type > TPAGB)
    {
        stardata->model.max_evolution_time =      stardata->model.time ;
        return;
    }
#endif
#ifdef LOG_COMENV_RSTARS
    Starloop(k)
    {
        log_r_star(&(stardata->star[k]), stardata);
    }
#endif
#ifdef LOG_HERBERT
    {
        struct star_t * star = &(stardata->star[0]);

        if((star->stellar_type < 10) && (star->stellar_type != 6))
        {

            double mch = 0.0, mche = 0.0, lhe = 0.0;

            /* H-burning shell */
            if((star->stellar_type >= HG) && (star->stellar_type <= TPAGB))
            {
                mch = star->core_mass[CORE_He];
            }
            /* He-burning shell */
            if((star->stellar_type >= EAGB) && (star->stellar_type <= HeGB))
            {
                mche = star->core_mass[CORE_CO];
            }
            /* He luminosity : guess */
            if((star->stellar_type == CHeB) || ((star->stellar_type >= HeMS) && (star->stellar_type <= HeGB)))
            {
                lhe = star->luminosity;
            }

            Printf("HERBERT 0 %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g\n",
                   stardata->model.time,
                   log10(Max(1e-20, star->radius)),
                   log10(Max(1e-20, Teff(0))), // (4)
                   log10(star->luminosity),
                   star->mass, // (6)
                   mch,
                   mche, // (8)
                   log10(Max(1e-20, lhe)),
                   0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, // convective boundaries 10-21
                   mch, mche, // masses of max energy generation
                   0.0, // (24) opacity
                   stardata->common.metallicity, // metallicity
                   stardata->model.dt, // 26 timestep
                   star->menv, // convective envelope
                   0.0, 0.0, 0.0, 0.0, 0.0, // H,He,C,N,O
                   0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, // 33-44 burning shell boundaries
                   0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, // 45-51 LH,C,th,nu,pp,CNO,pp/H
                   0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0); // 51-58 core H,He,C,N,O,T,density
        }
    }
#endif

#ifdef NUCSYN
#ifdef LOG_BARIUM_STARS
    log_barium_stars(stardata);
#endif
#endif


#ifdef NUCSYN
#ifdef RS_LOG_ACCREITON_LOG
    if((stardata->star[0].stellar_type == TPAGB) &&
            (stardata->star[1].stellar_type <= MAIN_SEQUENCE))
    {
        /* Choose to output (log) abundance or square bracket */

#define ACCABUND(A) (stardata->star[1].Xacc[(A)])


        Printf("ACCLAYER t=%g Macc2=%g ISOTOPES ",
               stardata->model.time,
               stardata->star[1].dmacc
              );

        Isotope a;
        Forward_isotope_loop(a)
        {
            Printf("%g ", ACCABUND(a));
        }
        Printf("\n");

    }

#endif
#endif


#ifdef TEST_AXEL_RGBF_FIX
    if(stardata->star[0].stellar_type == GIANT_BRANCH)
    {
        Printf("RADIUS %g %g %g %g\n",
               stardata->model.time,
               (1000.0 * pow((1130.0 * stardata->star[0].luminosity / Pow2(stardata->star[0].radius)), 0.25)),
               stardata->star[0].radius,
               stardata->star[0].luminosity);
    }
#endif


    Dprint("ITERATE t=%g sep=%6.8lf intpol=%d dtm=%g maxt=%g\n",
           stardata->model.time,
           stardata->common.orbit.separation,
           stardata->model.intpol,
           stardata->model.dtm,
           stardata->model.max_evolution_time);

#ifdef NUCSYN_GIANT_ABUNDS_LOG
    giant_abundance_log(stardata);
#endif //NUCSYN_GIANT_ABUNDS_LOG

#ifdef ANG_MOM_CHECKS
    determine_roche_lobe_radii(stardata, stardata->orbit); // Update RLOBes
    Printf("ANGMOM %g %d %g %g %d %g %g %g %g %g %g %g %g %g %g %g\n", stardata->model.time,
           stardata->star[0].stellar_type,
           Max(0, stardata->star[0].angular_momentum * M_SUN * R_SUN * R_SUN / YEAR_LENGTH_IN_SECONDS),
           Max(0, 2.0 * PI * stardata->star[0].omega / YEAR_LENGTH_IN_SECONDS),
           stardata->star[1].stellar_type,
           Max(0, stardata->star[1].angular_momentum * M_SUN * R_SUN * R_SUN / YEAR_LENGTH_IN_SECONDS),
           Max(0, 2.0 * PI * stardata->star[1].omega / YEAR_LENGTH_IN_SECONDS),
           stardata->common.orbit.separation,
           stardata->common.orbit.period,
           stardata->star[0].mass,
           stardata->star[1].mass,
           stardata->star[0].radius / stardata->star[0].roche_radius,
           stardata->star[1].radius / stardata->star[1].roche_radius,
           stardata->star[0].dmr, // dmr=wind
           stardata->star[1].dmr,
           stardata->star[0].dmr + stardata->common.dM_RLOF_transfer /* wind + RLOF */);
#endif/* ANG_MOM_CHECKS */



#ifdef STELLAR_COLOURS

    if(0)
    {
        double colours[NUMBER_OF_STARS][9]; /* colour data for each star */
        double l[NUMBER_OF_STARS][5]; /* luminosities */

//        if(stardata->star[0].stellar_type<=1)
        if(stardata->model.time < TINY)
        {
            /* Obtain colours for star k */
            Star_number k;
            Starloop(k)
            {
                binary_magnitudes(stardata,
                                  stardata->store,
                                  k,
                                  colours[k],
                                  colours[k] + 1,
                                  colours[k] + 2,
                                  colours[k] + 3,
                                  colours[k] + 4,
                                  colours[k] + 5,
                                  colours[k] + 6,
                                  colours[k] + 7,
                                  l[k],
                                  l[k] + 1,
                                  l[k] + 2,
                                  l[k] + 3);

                Printf("STELLAR_COLOURS# Mu Mb Mv Mi  U-B B-V V-I Mbol (star %d)\n", k);
                Printf("STELLAR_COLOURS %g %g %g %g %g %g %g %g\n",
                       colours[k][5], colours[k][4], colours[k][0], colours[k][6],
                       colours[k][1], colours[k][2], colours[k][3], colours[k][7]);

                Printf("\n\n");


            }

            /* calculate M_x for the binary */
            if(stardata->star[1].mass <= stardata->star[0].mass)
            {
                Printf("BINARY_BINARY_COLOURS %g %g %g %g %g %g %g\n",
                       6.34482 - 2.5 * log10(l[1][0] + l[2][0]),
                       6.07549 - 2.5 * log10(l[1][1] + l[2][1]),
                       5.35129 - 2.5 * log10(l[1][2] + l[2][2]),
                       4.60777 - 2.5 * log10(l[1][3] + l[2][3]),
                       stardata->star[0].mass,
                       stardata->star[1].mass,
                       stardata->common.orbit.separation
                      );
            }
            /* skip the rest of the code */
            stardata->model.time = 16000;

        }
    }
#endif


#ifdef HRDIAG
    log_hr(stardata);
#endif

    //#define STAR2_MONITOR
#ifdef STAR2_MONITOR
    if(stardata->star[1].stellar_type < 10)
    {
        Printf("STAR2 m=%g (accreted %g) kw=%d Xenv=%g ",
               stardata->star[1].mass,
               stardata->star[1].mass - stardata->star[1].effective_zams_mass,
               stardata->star[1].stellar_type,
               stardata->star[1].Xenv[XC12]);
        if(stardata->star[1].dmacc > 0.0)
        {
            Printf(" dmacc=%g Xacc=%g",
                   stardata->star[1].dmacc,
                   stardata->star[1].Xacc[XC12]);
        }
        else
        {
            Printf(" no acc layer ");
        }
        Printf("\n");
    }
#endif
#ifdef DETAILED_LOG
    detailed_log(stardata);
#endif /* DETAILED_LOG */

#ifdef XRAY_BINARIES
    // derivative[DERIVATIVE_STELLAR_MASS_WIND_GAIN] is msun /yr so multiply by M_SUN / YEAR_LENGTH_IN_SECONDS
    // to get into CGS
    log_xray_binary(stardata, 2, 1, stardata->star[0].derivative[DERIVATIVE_STELLAR_MASS_WIND_GAIN]);
    log_xray_binary(stardata, 1, 2, stardata->star[1].derivative[DERIVATIVE_STELLAR_MASS_WIND_GAIN]);
#endif

#ifdef STELLAR_TYPE_LOG
    stellar_type_log(stardata);
#endif


#if defined NUCSYN &&                           \
    defined NUCSYN_AND_HRD
    if(stardata->star[0].stellar_type < 10)
    {
        struct star_t * star = &(stardata->star[0]);
        Printf("HRDCHEM prob=%g ZAMSmass=%g logTeff=%g logL=%g H1=%g He4=%g Li7=%g C12=%g C13=%g N14=%g O16=%g Na23=%g Al26=%g CO=%g Ba=%g Pb=%g\n",
               stardata->model.probability,
               stardata->common.zero_age.mass[star->starnum],
               log10(Teff(0)),
               log10(star->luminosity),
               star->Xenv[XH1],
               star->Xenv[XHe4],
               star->Xenv[XLi7],
               star->Xenv[XC12],
               star->Xenv[XC13],
               star->Xenv[XN14],
               star->Xenv[XO16],
               star->Xenv[XNa23],
               star->Xenv[XAl26],
               nucsyn_elemental_abundance("C", star->Xenv, stardata, stardata->store) /
               nucsyn_elemental_abundance("O", star->Xenv, stardata, stardata->store),
               nucsyn_elemental_abundance("Ba", star->Xenv, stardata, stardata->store),
               nucsyn_elemental_abundance("Pb", star->Xenv, stardata, stardata->store)
              );
    }
#endif

    //#define TEST_JJES_RATES
#ifdef TEST_JJES_RATES

    /* test JJE's wind loss rates */

    for (stardata->star[0].Xenv[XH1] = 0.7;
            stardata->star[0].Xenv[XH1] > 0;
            stardata->star[0].Xenv[XH1] -= 0.1)
    {
        stardata->common.metallicity = 0.003;
        stardata->star[0].Xenv[XO16] = 0.2;
        stardata->star[0].Xenv[XC12] = 0.2;
        stardata->star[0].Xenv[XH1] = 0.005;
        stardata->star[0].Xenv[XHe4] = 1.0 -
                                       stardata->star[0].Xenv[XH1] -
                                       stardata->star[0].Xenv[XC12] -
                                       stardata->star[0].Xenv[XO16] -
                                       stardata->common.metallicity;

        for (stardata->star[0].luminosity = 100;
                stardata->star[0].luminosity <= 10000;
                stardata->star[0].luminosity += 100)
        {
            for (teff = 4000;
                    teff <= 40000;
                    teff += 1000)
            {
                /*      stardata->star[0].mass=20;
                        stardata->star[0].luminosity=10000;
                        teff=12603.9004;
                */
                stardata->star[0].radius = sqrt(1130.0 * stardata->star[0].luminosity) / Pow2(teff / 1e3);

                Printf("%g %g %g %g\n",
                       stardata->star[0].luminosity,
                       1000.0 * pow((1130.0 * stardata->star[0].luminosity /
                                     Pow2(stardata->star[0].radius)),
                                    0.25),
                       stardata->star[0].Xenv[XH1],
                       calc_stellar_wind_mass_loss(1,
                                                   stardata->star[0].luminosity,
                                                   stardata->star[0].radius,
                                                   20,
                                                   1,
                                                   1e6 * stardata->star[0].radius,
                                                   stardata->common.metallicity,
                                                   0.0,
                                                   stardata,
                                                   1)
                      );
            }
            Printf("\n");
        }
        Exit_binary_c(BINARY_C_NORMAL_EXIT, "Test JJE rates exit\n");
    }
    Exit_binary_c(BINARY_C_NORMAL_EXIT, "Test JJE rates exit\n");
#endif

#ifdef SELMA
    Starloop(k)
    {
        if((stardata->star[k].radius >= stardata->star[k].roche_radius)
                &&
                (stardata->star[Other_star(k)].radius < stardata->star[Other_star(k)].roche_radius)
                &&
                (stardata->star[Other_star(k)].stellar_type == MAIN_SEQUENCE)
                &&
                (stardata->star[k].stellar_type == MAIN_SEQUENCE)
           )
        {
            Printf("SELMA %d %g %g %g %g %g %g %g\n",
                   k,
                   stardata->model.time,
                   stardata->star[k].radius,
                   stardata->star[Other_star(k)].radius,
                   stardata->star[k].mass,
                   stardata->star[Other_star(k)].mass,
                   stardata->common.orbit.separation,
                   stardata->common.orbit.period
                  );
        }
    }
#endif //SELMA

#ifdef LTEST

    if(stardata->star[0].stellar_type < 10)
    {
        Printf("LTEST %g %g %g %g %g %g %g %g %g %g %g %g\n",
               1e6 * stardata->model.time,
               log10(stardata->star[0].luminosity),
               log10(1000.0 * pow((1130.0 * stardata->star[0].luminosity /
                                   Pow2(Max(stardata->star[0].radius, TINY))),
                                  0.25)),
               stardata->star[0].mass,
               stardata->star[0].Xenv[XH1],
               stardata->star[0].Xenv[XHe4],
               stardata->star[0].Xenv[XC12],
               stardata->star[0].Xenv[XN14],
               stardata->star[0].Xenv[XO16],
               ((stardata->star[0].Xenv[XC12] / 12.0 + stardata->star[0].Xenv[XC13] / 13.0 + stardata->star[0].Xenv[XO16] / 16.0) / (1e-14 + stardata->star[0].Xenv[XHe4] / 4.0))     ,
               log10(stardata->star[0].radius),
               Outermost_core_mass(&stardata->star[0])

              );
    }
#endif


#if defined CGI_EXTENSIONS &&                   \
    defined CGI_EXTENSIONS_HRD
    /*
     * HRD for cgi (web) output
     */
    if((stardata->model.log_fp != NULL) &&
            (stardata->model.time > 1e-5))
    {
        fprintf(stardata->model.log_fp,
                "CGIHRD %d %g %g %d %g %g\n",
                stardata->star[0].stellar_type,
                Teff(0),
                stardata->star[0].luminosity,
                stardata->star[1].stellar_type,
                Teff(1),
                stardata->star[1].luminosity);
    }
#endif

#if defined NUCSYN &&                           \
    defined CGI_EXTENSIONS &&                   \
    defined CGI_EXTENSIONS_ABUNDS &&            \
    defined FILE_LOG

    /* Extra stuff to the logfile for cgi (web) output */
    if((stardata->model.log_fp != NULL) &&
            (stardata->model.time > 1e-5))
    {
        fprintf(stardata->model.log_fp, "ABUNDS %10.10e ", stardata->model.time);
        Star_number j;
        double *X;
        Starloop(j)
        {
            X = nucsyn_observed_surface_abundances(&(stardata->star[j]));
            fprintf(stardata->model.log_fp,
                    " %3.3e %3.3e %3.3e %3.3e %3.3e %3.3e %3.3e",
                    X[XH1], X[XHe4], X[XC12], X[XN14], X[XO16],
                    X[XFe56],
                    nucsyn_elemental_abundance("Ba", X, stardata, stardata->store));
        }
        fprintf(stardata->model.log_fp, "\n");
    }
#endif/* NUCSYN */

    //#define NUCSYN_ABUNDANCE_LOG
#ifdef NUCSYN_ABUNDANCE_LOG

    Starloop(k)
    {
        if(stardata->star[k].stellar_type < 10)
        {
            Abundance * X = nucsyn_observed_surface_abundances(&(stardata->star[k]));
            Printf("ABUNDS %g %d %d ",
                   stardata->model.time,
                   k,
                   stardata->star[k].stellar_type);
            Isotope ii;
            Forward_isotope_loop(ii)
            {
                Printf("%g ", X[ii]);
            }
            Printf("\n");
        }
    }
#endif

#ifdef SDB_CHECKS
    sdb_check(stardata);
#endif
    /* and some logging */
#ifdef NUCSYN_J_LOG
    nucsyn_j_log(stardata);
#endif
#ifdef NUCSYN_SHORT_LOG
    nucsyn_short_log(stardata);
#endif
#ifdef NUCSYN_LONG_LOG
    nucsyn_long_log(stardata);
#endif
#ifdef LOG_JJE
    log_jje(stardata);
#endif
#ifdef NUCSYN_LOG_JL
    log_jl(stardata);
#endif


#ifdef LOG_RSTARS
    /*
     * Log R-stars
     */
    double teff, dt = stardata->model.dtm;
    double min_thick_disk_age = thick_disk_age(stardata->common.metallicity);

    if(stardata->model.time < TINY)
    {
        stardata->model.rstar_stardata->model.prev_log_time = 0.0;
    }
    dt = stardata->model.time - stardata->model.rstar_stardata->model.prev_log_time;
    stardata->model.rstar_stardata->model.prev_log_time = stardata->model.time;
    Boolean GKgiant = FALSE;
    /*
     * Convert C-rich CHeB stars into R-stars
     */
#ifdef NUCSYN
    Starloop(k)
    {
        if((stardata->star[k].stellar_type == CHeB) &&
                ((stardata->star[k].Xenv[XC12] / 12.0) /
                 (stardata->star[k].Xenv[XO16] / 16.0) > 1.0)
                && (stardata->star[k].rstar == 0)
           )
        {
            // should be an r-star!
            stardata->star[k].rstar = 4;
        }
    }
#endif

    /*
     * Star must be older than the minimum age of the thick disk
     */
    if((stardata->model.time > min_thick_disk_age) &&
            (dt > 0.0) &&
            (stardata->model.time > 0.0))
    {

        Starloop(k)
        {
            teff = 1000.0 * pow((1130.0 * stardata->star[k].luminosity /
                                 Pow2(stardata->star[k].radius)),
                                0.25);


            if((stardata->star[k].rstar > 0)
                    && (stardata->star[k].stellar_type == CHeB)
               )
            {
                /* calculate radial velocity parameter K1 */
                double v = radial_velocity_K(stardata, PI / 2.0, k + 1);

                Printf("RSTAR%d %g %g %g %g %g %g %g %g %g\n",
                       stardata->star[k].rstar,
                       stardata->model.probability * dt,
                       //stardata->model.time,
                       stardata->star[k].mass,
                       stardata->star[k].luminosity,
                       stardata->star[k].radius,
                       // binary information: M2,p(years),a,ecc,v_periastron (i.e. Max radial velocity, km/s)
                       stardata->star[Other_star(k)].mass,
                       stardata->common.orbit.period,
                       stardata->common.orbit.separation,
                       stardata->common.orbit.eccentricity,
                       v * 1e-5
                      );
            }

#ifdef NUCSYN
            if(((stardata->star[k].Xenv[XC12] / 12.0) / (stardata->star[k].Xenv[XO16] / 16.0) > 1.0)
                    && ((stardata->star[k].stellar_type == GIANT_BRANCH) ||
                        (stardata->star[k].stellar_type == EAGB) ||
                        (stardata->star[k].stellar_type == TPAGB))
                    && (teff < 3800)
               )
            {
                // giant C stars i.e. N stars (AGB-AGB binaries counted twice! oops)
                Printf("NSTAR %g %d\n",
                       stardata->model.probability * dt,
                       stardata->star[k].stellar_type);
            }
#endif
            /*
             * Detect K+G giants (numbers of Jaschek and Jaschek)
             */
            if((teff >= 3800.0) &&
                    (teff <= 5850.0) &&
                    ((stardata->star[k].stellar_type == GIANT_BRANCH) ||
                     (stardata->star[k].stellar_type == EAGB) ||
                     (stardata->star[k].stellar_type == TPAGB))
               )
            {
                GKgiant = TRUE;
            }
        } //k loop

        // check if one star or the other is a G/K giant
        if(GKgiant == TRUE)
        {
            Printf("GKgiant %g\n", stardata->model.probability * dt);
        }

        /* Choose how to select clump stars */

#define CLUMP_CHeB
//#define CLUMP_by_luminosity

#ifdef CLUMP_CHeB
        /* clump stars are any CHeB stars */
        if((stardata->star[0].stellar_type == CHeB) ||
                (stardata->star[1].stellar_type == CHeB))
#endif

#ifdef CLUMP_by_luminosity
            /* choose clump stars by luminosity */
            double lclump_min = exp10(1.8);
        double lclump_max = exp10(2.8);

        if(((stardata->star[0].stellar_type <= CHeB)
                && (stardata->star[0].luminosity > lclump_min) && (stardata->star[0].luminosity < lclump_max)
            )
                ||
                ((stardata->star[1].stellar_type <= CHeB)
                 && (stardata->star[1].luminosity > lclump_min) && (stardata->star[1].luminosity < lclump_max)
                )
           )
#endif
        {
            Printf("CHEB %g\n", stardata->model.probability * dt);
        }
    }
#endif

#ifdef NS_BH_AIC_LOG
    Foreach_star(star)
    {
        star->prev_luminosity = star->luminosity;
        star->prev_radius = star->radius;
        star->prev_mass = star->mass;
        star->prev_core_mass = Outermost_core_mass(star);
        star->prev_stellar_type = star->stellar_type;
    }
#endif

    //#define COMENV_LECTURE1
#ifdef COMENV_LECTURE1
    /* for comenv lecture */

    if(first_timestep == TRUE)
    {
        stardata->common.star1_agb = FALSE;
        /*      Printf("BINSTART %g %g %g\n",
                stardata->model.probability,
                stardata->common.orbit.separation,
                stardata->common.orbit.period);
        */
    }
    /*
      else if(Fequal(stardata->model.max_evolution_time,
      stardata->model.time))
      {
      Printf("BINSTOP %g %g %g\n",
      stardata->model.probability,
      stardata->common.orbit.separation,
      stardata->common.orbit.period);
      }
    */

    if(stardata->star[0].stellar_type == TPAGB)
    {
        if(stardata->common.star1_agb == FALSE)
        {
            /* start AGB */
            //Printf("Start AGB at t=%g\n",stardata->model.time);
            stardata->common.star1_agb = TRUE;
            stardata->common.maxrad = 0.0;
            stardata->common.minrad = stardata->star[0].radius;
        }
        stardata->common.maxrad = Max(stardata->common.maxrad, stardata->star[0].radius);
        //Printf("stardata->common.maxrad=%g\n",stardata->common.maxrad);
    }
    else if((stardata->common.star1_agb == TRUE) &&
             (stardata->star[0].stellar_type >= COWD))
    {
        /* post-AGB */
        Printf("TPAGBEND %g %g %g %g\n",
               stardata->common.zero_age.mass[0],
               stardata->star[0].mass,
               stardata->common.minrad, stardata->common.maxrad);
        stardata->common.star1_agb = FALSE;
    }

#endif

#ifdef COMENV_LECTURE2

#endif

#ifdef ADAPTIVE_RLOG
    /*
     * RLOF stuff for lectures
     */
    if(stardata->star[0].stellar_type < 10)
    {
        Printf("RLOF %g %g %g %g %g\n",
               stardata->model.time,
               stardata->star[0].mass,
               stardata->star[1].mass,
               stardata->star[1].v_eq,
               stardata->star[1].v_crit_eq

              );
    }
#endif


#ifdef DETAILED_COMPACT_LOG
    output_to_detailed_compact_logfile(stardata);
#endif

#ifdef ANTI_TZ
    /* model to debunk TZ object myth */
    {
        Foreach_star(star)
        {
            if(star->stellar_type == EAGB &&
                    star->started_EAGB == FALSE)
            {
                star->started_EAGB = TRUE;
                Printf("ANTI_TZ EAGB %g %g\n",
                       star->mass,
                       stardata->model.probability);
            }
            else if(star->TZ_object == TRUE)
            {
                Printf("ANTI_TZ TZ %d %g %g %g\n",
                       star->TZ_channel,
                       star->TZ_mass,
                       star->TZ_NS_mass,
                       stardata->model.probability);
                star->TZ_object = FALSE;
            }
        }
    }
#endif // ANTI_TZ

#ifdef WIND_ENERGY_LOGGING
    /*
     * Calculate energy outflow in winds
     */
    //if(binary==TRUE)
    {
        /* orbital velocity in cgs */
        const double vorb = orbital_velocity(stardata);

        Foreach_star(star)
        {
            if(star->stellar_type < HeWD)
            {
                /* vwind and mdot, in cgs */
                const double vwind = star->vwind;
                const double mdot = fabs(star->derivative[DERIVATIVE_STELLAR_MASS_WIND_LOSS]) * M_SUN / YEAR_LENGTH_IN_SECONDS;
                const double dt_seconds = dt * 1e6 * YEAR_LENGTH_IN_SECONDS;

                /* output only if there is a wind */
                if(mdot > TINY && star->vwind > TINY)
                {
                    const double Lwind = mdot * (Pow2(vorb) + Pow2(vwind));
                    star->Ewind += Lwind * dt_seconds;
                    star->Elum += star->luminosity * L_SUN * dt_seconds;
                    Printf("EWIND%d t=%g Myr, stellar_type=%d, mdot=%g (Msun/y) vorb=%g (km/s), vwind=%g (km/s), wind power=%g (erg s-1) enh.fac=%g Ewind=%g Elum=%g\n",
                           star->starnum,
                           stardata->model.time,
                           star->stellar_type,
                           mdot * YEAR_LENGTH_IN_SECONDS / M_SUN,
                           vorb * 1e-5,
                           vwind * 1e-5,
                           Lwind,
                           (mdot > TINY && vwind > TINY) ? Lwind : 0,
                           star->Ewind,
                           star->Elum);
                }
            }
        }
    }

#endif // WIND_ENERGY_LOGGING



#ifdef NS_BH_AIC_LOG

    /* TODO : needs updating */

    int this_star = star->starnum;
    int other_star = Other_star(star->starnum);
    if((*stellar_type == BLACK_HOLE) && (stellar_typein == NEUTRON_STAR))
    {
        //double r2=stardata->star[other_star].radius;

        double m1, r1, l1, m2, r2, l2;
        Stellar_type st1, st2;

        if(stardata->star[other_star].stellar_type == MASSLESS_REMNANT)
        {
            /* there is a problem when there is a merger and the other
               star is reported as a massless remnant when really it was
               something else (e.g. BLACK_HOLE) : in this case use the prev_
               variables
            */
            r1 = stardata->star[this_star].prev_radius;
            l1 = stardata->star[this_star].prev_luminosity;
            m1 = stardata->star[this_star].prev_mass;
            st1 = stardata->star[this_star].prev_stellar_type;
            r2 = stardata->star[other_star].prev_radius;
            l2 = stardata->star[other_star].prev_luminosity;
            m2 = stardata->star[other_star].prev_mass;
            st2 = stardata->star[other_star].prev_stellar_type;
        }
        else
        {
            r1 = stardata->star[this_star].radius;
            l1 = stardata->star[this_star].luminosity;
            m1 = stardata->star[this_star].mass;
            st1 = stardata->star[this_star].stellar_type;
            r2 = stardata->star[other_star].radius;
            l2 = stardata->star[other_star].luminosity;
            m2 = stardata->star[other_star].mass;
            st2 = stardata->star[other_star].stellar_type;
        }

        //double angle=2.0*r2/stardata->common.orbit.separation;
        //double solid_angle=4.0*PI*(PI*Pow2(r2))/(4.0*PI*Pow2(stardata->common.orbit.separation));
        Printf("AIC NS->BH sgl=%d comenv=%d p=%12.12e a=%12.12e e=%12.12e t=%12.12e star1 m=%12.12e r=%12.12e  L=%12.12e type=%d star2 m=%12.12e r=%12.12e L=%12.12e type=%d\n",
               stardata->model.sgl,
               stardata->model.comenv_type,
               stardata->model.probability,
               stardata->common.orbit.separation,
               stardata->common.orbit.eccentricity,
               stardata->model.time,
               m1,
               r1,
               l1,
               st1,
               m2,
               r2,
               l2,
               st2
              );

        /* we don't care about the subsequent evolution */
        stardata->model.max_evolution_time = stardata->model.time;

    }
#endif

#ifdef MASSERON
    {
        struct star_t *star = &(stardata->star[0]);
        if(star->stellar_type <= TPAGB &&
                Is_not_zero(stardata->model.time))
        {
            /* fix spin rate on the main sequence */
            if(star->stellar_type < HERTZSPRUNG_GAP)
            {
                star->v_eq = star->vrot0;
                star->omega = OMEGA_FROM_VKM * star->v_eq / star->radius;
                star->angular_momentum =
                    star->omega * moment_of_inertia(stardata,
                                                    star,
                                                    star->radius);
            }

            char spectype[10];
            spectral_type_string(spectype, stardata, star);

#define MASSERON_COLUMNS stardata->model.time,  \
                star->stellar_type,             \
                star->mass,                     \
                star->v_eq,                     \
                star->luminosity,               \
                star->radius,                   \
                logg(star),                     \
                Teff_from_star_struct(star),    \
                spectype

            Printf("MASSERON %g %d %g %g %g %g %g %g %s\n",
                   MASSERON_COLUMNS
                  );
        }
    }
#endif //MASSERON


#if defined NUCSYN &&                           \
    defined CN_THICK_DISC
    {
        /*
         * CN logging in thick-disc giants
         */
        Star_number k;

        /*
         * Selection:
         * choose only giants
         * with age 4-10Gyr i.e. single star mass 0.93-1.25Msun
         * if Z=Z_SMC=0.004
         */

#define THICK_DISC_STAR (                                               \
            star->stellar_type>=GIANT_BRANCH &&                         \
            star->stellar_type<HeMS &&                                  \
            stardata->model.time > stardata->preferences->thick_disc_end_age && \
            stardata->model.time < stardata->preferences->thick_disc_start_age && \
            In_range(loggv[k],                                          \
                     stardata->preferences->thick_disc_logg_min,        \
                     stardata->preferences->thick_disc_logg_max)        \
            )

        Boolean out = FALSE;
        double loggv[NUMBER_OF_STARS + 1];
        Foreach_star(star)
        {
            loggv[star->starnum] = logg(star);
            if(THICK_DISC_STAR) out = TRUE;
            /*
              printf("STAR %d, type %d : age = %g cf %g .. %g, logg = %g vs %g .. %g\n",
              star->starnum,
              star->stellar_type,
              stardata->model.time,
              stardata->preferences->thick_disc_end_age,
              stardata->preferences->thick_disc_start_age,
              loggv[k],
              stardata->preferences->thick_disc_logg_min,
              stardata->preferences->thick_disc_logg_max
              );
            */
            if(star->blue_straggler) star->was_blue_straggler = TRUE;
        }

        if(out == TRUE)
        {
            Printf("CND %g %g %g %g %g %g %g ",
                   stardata->model.time,
                   dtp,

                   /* initial conditions */
                   stardata->common.zero_age.orbital_period[0], // DAYS
                   stardata->common.zero_age.separation[0],
                   stardata->common.zero_age.eccentricity[0],
                   stardata->common.zero_age.mass[0],
                   stardata->common.zero_age.mass[1]);

            if(System_is_binary)
            {
                /* current binary conditions */
                Printf("%g %g %g %g %g ",
                       stardata->common.orbit.period, // YEARS
                       stardata->common.orbit.separation,
                       stardata->common.orbit.eccentricity,
                       stardata->star[0].mass,
                       stardata->star[1].mass
                      );
            }
            else
            {
                /* currently single : only give mass */
                Printf("* * * %g * ",
                       stardata->star[(stardata->star[0].stellar_type != MASSLESS_REMNANT ? 0 : 1)].mass
                      );
            }

            Foreach_star(star)
            {
                /*
                 * choose only hydrogen-rich 'giants' which are
                 * satisfy the "thick disc" criteria
                 */
                if(THICK_DISC_STAR)
                {
                    double * X = nucsyn_observed_surface_abundances(star);
                    Printf("%d %g %g %g %g %g %d ",
                           star->stellar_type,
                           1e-5 * radial_velocity_K(stardata, PI / 2.0, star->starnum + 1), // convert from cm/s to km/s
                           star->mass,
                           nucsyn_elemental_square_bracket(
                               "C", "N",
                               X,
                               stardata->common.Xsolar,
                               stardata),
                           Outermost_core_mass(star),
                           loggv[star->starnum],
                           star->was_blue_straggler
                          );
                }
                else
                {
                    Printf("* * * * * * * ");
                }
            }
            Printf("\n");
        }
    }
#endif //CN_THICK_DISC

    if(stardata->preferences->hrdiag_output == TRUE)
    {
        printf("HRDHRD %g %d %g %g %g \n",
               stardata->model.time,
               stardata->star[0].stellar_type,
               HRdiagram(0),
               logg(&stardata->star[0]));
    }


#ifdef CSJ_SDO_STARS
    {
        Foreach_star(star)
        {
            /*
             * Look for single sdO stars
             */
            if(NAKED_HELIUM_STAR(star->stellar_type)
                    &&
                    single == TRUE)
            {
                //printf("SDO t=%g #=%d st=%d dtp=%g Teff=%g L=%g logg=%g tphase=%g\n",

                Printf("SDO %g %d %d %g %g %g %g %g %g\n",
                       stardata->model.time,
                       star->starnum,
                       star->stellar_type,
                       dtp,
                       log10(Teff_from_star_struct(star)),
                       log10(star->luminosity),
                       logg(star),
                       stardata->model.time - star->stellar_type_tstart,
                       star->mass
                      );
            }
        }
    }

#endif

#if defined COMENV_POLYTROPES &&                \
    defined COMENV_POLYTROPE_LOGGING
    {
        struct star_t * star = & stardata->star[0];
        if(star->stellar_type >= HERTZSPRUNG_GAP &&
                star->stellar_type <= TPAGB &&
                star->mass - star->core_mass[CORE_He] > 0.1)
        {
            double lambda = common_envelope_polytrope(star->mass,
                            star->core_mass[CORE_He],
                            star->radius,
                            star->core_radius,
#ifdef NUCSYN
                            star->Xenv,
#endif
                            stardata);

            Printf("COMENVLOG %g %d %g %g %g %g %g\n",
                   stardata->model.time,
                   star->stellar_type,
                   star->mass,
                   star->radius,
                   lambda,
                   star->k2,
                   star->core_mass[CORE_He]
                  );

            /*
             * Output binding energy vs mass co-ordinate
             */


            // save splitmass
            double splitwas = stardata->preferences->comenv_splitmass;
            double splitmass;
            double dsplitmass = (star->mass - star->core_mass[CORE_He]) / 100.0;
            for (splitmass = star->core_mass[CORE_He]; splitmass < star->mass; splitmass += dsplitmass)
            {
                stardata->preferences->comenv_splitmass = splitmass / star->core_mass[CORE_He];
                double l =  common_envelope_polytrope(star->mass,
                                                      star->core_mass[CORE_He],
                                                      star->radius,
                                                      star->core_radius,
#ifdef NUCSYN
                                                      star->Xenv,
#endif
                                                      stardata);

                double Ebind = GRAVITATIONAL_CONSTANT * star->mass *
                               (star->mass - splitmass) * Pow2(M_SUN) /
                               (star->radius * R_SUN * l);

                Printf("COMENVLOG2 t=%g ms/m=%g ms=%g lambda=%g Ebind=%g Ebind/Menv=%g\n",
                       stardata->model.time,
                       splitmass / star->mass,
                       splitmass,
                       l,
                       Ebind,
                       Ebind / (M_SUN * (star->mass - splitmass)));
            }

            // restore split mass
            stardata->preferences->comenv_splitmass = splitwas;
        }
    }
#endif




    /*
      printf("LRLOF %g %g %g %g %g %g %g %g\n",
      stardata->model.time,
      stardata->star[0].mass,
      stardata->star[0].radius,
      stardata->star[0].roche_radius,
      stardata->star[1].mass,
      stardata->star[1].radius,
      stardata->star[1].roche_radius,
      stardata->common.orbit.separation);
    */

//#define LOG_INDIVIDUAL_NOVAE
#ifdef LOG_INDIVIDUAL_NOVAE
    if(WHITE_DWARF(stardata->star[0].stellar_type))
    {
        printf("NOVAE %20.12e %20.12e %20.12e %20.12e %20.12e %20.12e %20.12e %20.12e %20.12e %u %u %d %20.12e %20.12e %20.12e %g %g %20.12e %d %d %g %g\n",
               stardata->model.time,//1
               stardata->star[0].mass,
               dt,//3
               stardata->star[1].radius / stardata->star[1].roche_radius,
               stardata->star[1].mass,//5
               stardata->star[1].radius,
               Mdot_net(&stardata->star[1]),
               stardata->star[0].dm_novaH, //8
               stardata->star[0].derivative[DERIVATIVE_STELLAR_MASS_NOVA],
               stardata->star[0].nova,//10
               stardata->star[1].nova,
               (stardata->star[0].nova || stardata->star[1].nova) ? 1 : 0, //12
               Mdot_net(&stardata->star[0]), //13 accretor rate (+ve when accreting, -ve when exploding)
               Mdot_net(&stardata->star[1]), //14 donor rate (-ve)
               stardata->common.orbit.separation, //15
               stardata->star[0].dmacc,//16
               stardata->star[1].dmacc,//17
               stardata->model.nova_timeout, //18
               stardata->star[0].dtlimiter,//19
               stardata->star[1].dtlimiter, //20
               stardata->star[0].num_novae, //21
               stardata->star[1].num_novae //22
              );
    }
#endif//LOG_INDIVIDUAL_NOVAE

#ifdef UNIT_TESTS
    unit_tests(stardata);
#endif // UNIT_TESTS

#ifdef DISCS
    disc_logging(stardata, NULL, DISC_LOG_EVERY_TIMESTEP);
#ifdef LOG_POSTAGB_STARS
    /*
     * Count post-AGB stars for Hans
     */

    if(stardata->preferences->postagb_legacy_logging == TRUE)
    {
        Foreach_star(star)
        {
            if(Post_AGB(star))
            {
                Boolean has_disc = Boolean_(stardata->common.ndiscs > 0);
                Printf("POSTAGB %g %g %d %d %g %g %g\n",
                       stardata->model.time, // Myr
                       dtp,
                       star->stellar_type,
                       has_disc,
                       Teff(star->starnum),
                       stardata->common.orbit.period, // y
                       stardata->common.orbit.eccentricity);
            }
        }
    }
#endif // LOG_POSTAGB_STARS

#endif // DISCS




#if defined NUCSYN &&                           \
    defined NUCSYN_ANGELOU_LITHIUM &&           \
    defined NUCSYN_ANGELOU_LITHIUM_LOGGING
    /*
     * Log stellar lithium for George Angelou
     *
     * NB only log hydrogen-rich stars
     */
    {
        Foreach_star(star)
        {
            if(star->stellar_type <= TPAGB)
            {
                // surface lithium abundance (mass fraction)
                const Abundance XLi = Observed_abundance(star, XLi7);
                Number_density N[ISOTOPE_ARRAY_SIZE];
                X_to_N(stardata->store->imnuc,
                       1.0,
                       N,
                       Observed_surface(star),
                       ISOTOPE_ARRAY_SIZE);
                const Abundance ALi = Max(1e-30, log10(N[XLi7] / Max(1e-100, N[XH1])) + 12.0);

                Printf("ANGELOU%d %d %g %g %g %g %g %g %g %g %g %g %g\n",
                       star->starnum, // star number
                       star->stellar_type, // 1 stellar type (see stellar_types.h)
                       stardata->model.time, // 2 time
                       stardata->model.dt * stardata->model.probability, // 3 weighting
                       XLi, // 4 mass fraction
                       ALi, // 5 abundance in log(X/H)+12 notation
                       star->mass, // 6 mass/Msun
                       star->luminosity, // 7 L/Lsun
                       star->radius, // 8 R/Rsun
                       logg(star), // 9 logg (cgs)
                       star->v_eq, // 10 equatorial rotation rate v_eq (km/s)
                       star->v_crit_eq, // 11 critical equatorial rotation rate v_eq_crit (km/s)
                       star->v_eq_ratio // 12 v_eq / v_eq_crit
                      );
            }
        }
    }
#endif // NUCSYN, NUCSYN_ANGELOU_LITHIUM, NUCSYN_ANGELOU_LITHIUM_LOGGING

#ifdef FABIAN_COSMOLOGY
    struct star_t * star = & stardata->star[0];

    if(star->stellar_type != MASSLESS_REMNANT)
    {
        if(NUCLEAR_BURNING(star->stellar_type))
        {
            if(NAKED_HELIUM_STAR(star->stellar_type))
            {
                star->final_helium_core_mass = star->mass;
            }
            else
            {
                star->final_helium_core_mass = star->core_mass[CORE_He];
            }

            if(CARBON_CORE_STAR(star->stellar_type))
            {
                star->final_carbon_core_mass = star->core_mass[CORE_CO];
            }

            star->final_nuclear_burning_mass = star->mass;
            star->nuclear_burning_lifetime = stardata->model.time;

        }
    }

    if(final == TRUE)
    {
        Printf("FABIAN_COSMOLOGY %g %g %g %g %g %g %g %s \n",
               stardata->common.metallicity,
               stardata->common.zero_age.mass[star->starnum],
               star->final_helium_core_mass,
               star->final_carbon_core_mass,
               star->final_nuclear_burning_mass,
               star->mass,
               star->nuclear_burning_lifetime,
               Short_stellar_type_string_from_star(star));
    }
#endif//FABIAN_COSMOLOGY


#ifdef __CHECK_TIMESTEP
    printf("SYS %g %g %g %g %g %g %g %g %g %g %g %g\n",
           stardata->model.time,
           stardata->star[0].mass, // 2
           stardata->star[1].mass,
           stardata->star[0].angular_momentum, //4
           stardata->star[1].angular_momentum, //5
           stardata->common.orbit.separation, //6
           stardata->common.orbit.period,//7
           stardata->common.orbit.angular_momentum,
           stardata->star[0].derivative[DERIVATIVE_STELLAR_ANGMOM_TIDES],//9
           stardata->star[1].derivative[DERIVATIVE_STELLAR_ANGMOM_TIDES],
           stardata->star[0].derivative[DERIVATIVE_STELLAR_ANGULAR_VELOCITY_TIDES],//11
           stardata->star[1].derivative[DERIVATIVE_STELLAR_ANGULAR_VELOCITY_TIDES]
          );
#endif//__CHECK_TIMESTEP

    /*
      if(stardata->model.time > 0.0 &&
      stardata->star[0].stellar_type<3)
      {
      printf("KARSTEN %g %g %g %g %g\n",
      stardata->model.time,
      stardata->star[0].mass,
      stardata->star[1].mass,
      YEAR_LENGTH_IN_DAYS * stardata->common.orbit.period,
      stardata->common.orbit.angular_momentum

      );
      }
    */


    if(0)
    {
        const double Jtot = stardata->star[0].angular_momentum +
                            stardata->star[1].angular_momentum +
                            stardata->common.orbit.angular_momentum;

#define Second_derivative(IN,N)                             \
        ((stardata->IN.derivative[(N)] -                    \
          stardata->previous_stardata->IN.derivative[(N)])/ \
         stardata->model.dt)

        Printf("JJJ %30.20e %d %g %g %g %g %g %g %d %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g\n",
               stardata->model.time,//1
               stardata->star[0].stellar_type,//2
               stardata->star[0].mass,//3
               stardata->star[0].radius,//4
               stardata->star[0].roche_radius,//5
               stardata->star[0].angular_momentum,//6
               stardata->star[0].omega,//7
               stardata->star[0].derivative[DERIVATIVE_STELLAR_ANGMOM],//8
               stardata->star[1].stellar_type,//9
               stardata->star[1].mass,//10
               stardata->star[1].radius,//11
               stardata->star[1].roche_radius,//12
               stardata->star[1].angular_momentum,//13
               stardata->star[1].omega,//14
               stardata->star[1].derivative[DERIVATIVE_STELLAR_ANGMOM],//15
               stardata->common.orbit.angular_frequency,//16
               stardata->common.orbit.separation, // 17
               Jtot, //18
               stardata->model.derivative[DERIVATIVE_ORBIT_ANGMOM], //19
               stardata->model.derivative[DERIVATIVE_ORBIT_ANGMOM_GRAVITATIONAL_RADIATION], //20
               stardata->common.test, //21
               Second_derivative(model, DERIVATIVE_ORBIT_ANGMOM), //22
               Second_derivative(star[0], DERIVATIVE_STELLAR_ANGMOM), //23
               Second_derivative(star[1], DERIVATIVE_STELLAR_ANGMOM), //24
               moment_of_inertia(stardata, &stardata->star[0], stardata->star[0].radius), //25
               moment_of_inertia(stardata, &stardata->star[1], stardata->star[1].radius), //26
               stardata->model.dtm //27
              );
    }

    if(0 &&
            stardata->star[1].stellar_type == 7 &&
            stardata->previous_stardata->star[1].stellar_type == 9)
    {
        /*
         * While this shouldn't happen, the BSE algorithm
         * *can* convert stars of stellar type HeHG and HeGB
         * to CHeB, which means they go "backwards".
         */
        Exit_binary_c(2,
                      "oops stellar type backwards\n");
    }

    /*
      printf("OMEGA ω=%g J=%g I=%g : ω=%g J=%g I=%g\n",
      stardata->star[0].omega,
      stardata->star[0].angular_momentum,
      moment_of_inertia(stardata,&stardata->star[0],stardata->star[0].radius),
      stardata->star[1].omega,
      stardata->star[1].angular_momentum,
      moment_of_inertia(stardata,&stardata->star[0],stardata->star[1].radius));
      show_derivatives(stardata);
    */
    if(0 &&
            Is_zero(stardata->common.orbit.separation) &&
            stardata->star[0].stellar_type == TPAGB &&
            stardata->star[1].stellar_type == MASSLESS_REMNANT)
    {
        printf("MMM %g %g %g %g %g %g %g %g %g\n",
               stardata->model.time,
               stardata->star[0].mass,
               Outermost_core_mass(&stardata->star[0]),
               stardata->star[0].core_mass_no_3dup,
               stardata->star[0].num_thermal_pulses,
               stardata->star[0].luminosity,
               stardata->star[0].derivative[DERIVATIVE_STELLAR_MASS],
               stardata->star[0].dntp,
               stardata->star[0].time_next_pulse
              );
    }

    if(0)
    {
        printf("At %g sep = %g\n",
               stardata->model.time,
               stardata->common.orbit.separation);
    }

#ifdef SYSTEM_LOGGING

    /*
     * System logging
     */
#define _header "SYSLOG"

    /* system */
#define _sysheader                              \
    "N t dt a Jorb e omega tcirc "
#define _sysformat                              \
    "%d %30.20e %g %g %g %g %g %g "
#define _sys_data_items                             \
    stardata->model.model_number,                   \
        stardata->model.time,                       \
        stardata->model.dtm,                        \
        stardata->common.orbit.separation,          \
        stardata->common.orbit.angular_momentum,    \
        stardata->common.orbit.eccentricity,        \
        stardata->common.orbit.angular_frequency,   \
        stardata->common.orbit.tcirc

    /* stars */
#define _starformat "%d %g %g %g %g %g %g %g %g %g %d %s %g %g %g "
#ifdef MINT
#define _XHC(N) stardata->star[N].bint.XHc
#else
#define _XHC(N) 0.0
#endif

#define _star_data_items(N)                                             \
    stardata->star[N].stellar_type,                                     \
        stardata->star[N].mass,                                         \
        Outermost_core_mass(&stardata->star[N]),                        \
        stardata->star[N].radius,                                       \
        stardata->star[N].radius / stardata->star[N].roche_radius,      \
        stardata->star[N].angular_momentum,                             \
        stardata->star[N].omega,                                        \
        stardata->star[N].derivative[DERIVATIVE_STELLAR_MASS],          \
        stardata->star[N].derivative[DERIVATIVE_STELLAR_ANGMOM],        \
        stardata->star[N].stellar_timestep,                             \
        stardata->star[N].dtlimiter,                                    \
        string[abs(stardata->star[N].dtlimiter)],                       \
        Sync_timescale(N),                                              \
        _XHC(N),                                                        \
        moment_of_inertia(stardata,&stardata->star[N],stardata->star[N].radius)

#define _starheader(N)                                                  \
    "st%d M%d Mc%d R%d R/RL%d J%d omega%d dMdt%d dJdt%d dt%d dtlimiter%d dtlimstring%d tsync%d XHC%d I%d ", N,N,N,N,N,N,N,N,N,N,N,N,N,N,N \

    /*
     * This is NOT thread safe
     */
    const char * string[DT_LIMIT_NUMBER] = { DT_LIMIT_STRINGS };

    if(stardata->model.syslog_header_printed == FALSE)
    {
        stardata->model.syslog_header_printed = TRUE;
        Printf("%s ", _header);
        Printf(_sysheader);
        Printf(_starheader(0));
        Printf(_starheader(1));
        Printf("\n");
    }

    Printf("%s ", _header);
    Printf(_sysformat, _sys_data_items);
    Printf(_starformat, _star_data_items(0));
    Printf(_starformat, _star_data_items(1));
    Printf("\n");
#endif // SYSTEM_LOGGING


#ifdef ZW2019_SNIA_LOG
    {
        /*
         * SNIa progenitor logging for Zhengwei Liu
         * 19/09/2019
         */
        Boolean ZWlog =  FALSE;
        Star_number k = -1;

        if(stardata->model.ZW2019_Hestar == -1 &&
                (NAKED_HELIUM_STAR(stardata->star[0].stellar_type) ||
                 NAKED_HELIUM_STAR(stardata->star[1].stellar_type)) &&
                System_is_binary
           )
        {
            k = NAKED_HELIUM_STAR(stardata->star[0].stellar_type) ? 0 :
                NAKED_HELIUM_STAR(stardata->star[1].stellar_type) ? 1 :
                -1;
            stardata->model.ZW2019_Hestar = k;
            ZWlog = TRUE;
        }

        if(stardata->model.ZW2019_Hestar > 0 &&
                stardata->model.ZW2019_COWD == -1 &&
                (stardata->star[stardata->model.ZW2019_Hestar].stellar_type == COWD) &&
                System_is_binary)
        {
            k = stardata->model.ZW2019_Hestar;
            stardata->model.ZW2019_COWD = k;
            ZWlog = TRUE;
        }

        if(ZWlog == TRUE && k > 0)
        {
            Printf("ZW2019 %s %d t=%g prob=%g st1=%s st2=%s M1=%g M2=%g R1=%g R2=%g RL1=%g RL2=%g a=%g P=%g e=%g J=%g Z=%g\n",
                   stardata->model.ZW2019_COWD == TRUE ? "COWD" : "Hestar",
                   k,
                   stardata->model.time,
                   stardata->model.probability,
                   Short_stellar_type_string_from_star(&stardata->star[0]),
                   Short_stellar_type_string_from_star(&stardata->star[1]),
                   stardata->star[0].mass,
                   stardata->star[1].mass,
                   stardata->star[0].radius,
                   stardata->star[1].radius,
                   stardata->star[0].roche_radius,
                   stardata->star[1].roche_radius,
                   stardata->common.orbit.separation,
                   stardata->common.orbit.period,
                   stardata->common.orbit.eccentricity,
                   stardata->common.orbit.angular_momentum,
                   stardata->common.metallicity
                  );
        }
    }
#endif// ZW2019_SNIA_LOG

#ifdef GAIAHRD
    if(1)
    {
        /*
         * build stored gaia HRD chunk, if required
         */
        if(stardata->tmpstore->gaiaHRD == NULL)
        {
            stardata->tmpstore->gaiaHRD = Calloc(1,sizeof(struct gaiaHRD_chunk_t));
        }
        struct gaiaHRD_chunk_t * gaia = stardata->tmpstore->gaiaHRD;

        /*
         * Make space for gaia logging on this step, and perform
         * calculations (see gaia_log for details)
         */
        struct gaiaHRD_chunk_t * this = Malloc(sizeof(struct gaiaHRD_chunk_t));
        this->dtp = dtp;
        gaia_log(stardata, this);

        /*
         * If we should log, or this is the final timestep,
         * do more
         */
        if(this->do_log == TRUE || final == TRUE)
        {

            /*
             * If our location hasn't changed, just add dtp
             */
            if(final == FALSE &&
                    this->is_single == gaia->is_single &&
                    this->do_log == gaia->do_log &&
                    this->primary == gaia->primary &&
                    Fequal(this->Teff_binned, gaia->Teff_binned) &&
                    Fequal(this->L_binned, gaia->L_binned) &&
                    Fequal(this->gaia_magnitude_binned, gaia->gaia_magnitude_binned) &&
                    Fequal(this->gaia_colour_binned, gaia->gaia_colour_binned))
            {
                /*
                 * All same... just add dtp
                 */
                gaia->dtp += dtp;
            }
            else
            {
                /*
                 * Flush saved gaia HRD
                 */
                if(Is_not_zero(gaia->dtp) || final == TRUE)
                {
                    Printf("GAIAHRD %g %g %g %g %d %g %g\n",
                           stardata->model.time,
                           gaia->Teff_binned,
                           gaia->L_binned,
                           gaia->dtp,
                           gaia->is_single == TRUE ? 0 : 1,
                           gaia->gaia_colour_binned,
                           gaia->gaia_magnitude_binned
                          );
                }

                if(final == FALSE)
                {
                    /*
                     * Overwrite stored gaia data with new data
                     */
                    memcpy(gaia, this, sizeof(struct gaiaHRD_chunk_t));

                    /*
                     * Update previously logged Teff and L
                     * for adaptive timestepping
                     */
                    stardata->common.gaia_Teff = this->Teff_binned;
                    stardata->common.gaia_L = this->L_binned;
                }
            }
        }
        else if(gaia->do_log == TRUE)
        {
            /*
             * We were in a state of logging, now we're not,
             * so flush and clear the gaia struct
             */
            Printf("GAIAHRD %g %g %g %g %d %g %g\n",
                   stardata->model.time,
                   gaia->Teff_binned,
                   gaia->L_binned,
                   gaia->dtp,
                   gaia->is_single == TRUE ? 0 : 1,
                   gaia->gaia_magnitude_binned,
                   gaia->gaia_colour_binned);
            memset(gaia, 0, sizeof(struct gaiaHRD_chunk_t));

            /*
             * And make sure the stored do_log is FALSE
             */
            gaia->do_log = FALSE;
        }
        Safe_free(this);
    }
#endif//GAIAHRD

    Call_function_hook(custom_output);

    if(timestep_fixed_trigger(stardata, FIXED_TIMESTEP_TEST))
    {
        if(NUCLEAR_BURNING_BINARY)
        {
            Printf("test %g %d %d\n",
                   stardata->model.time,
                   stardata->star[0].stellar_type,
                   stardata->star[1].stellar_type
                  );
        }
    }


    /*
     * Clear the binary_c printf buffer here, if required
     */
    if(stardata->preferences->internal_buffering ==
            INTERNAL_BUFFERING_PRINT)
    {
        Clear_printf_buffer;
    }

    if(0 && System_is_binary)
        printf("RRR %15.12g %15.12g %15.12g %g %g %g %d %g %d\n",
               stardata->model.time,//1
               stardata->star[0].radius,//2
               stardata->star[0].roche_radius,//3
               stardata->star[0].mass,//4
               stardata->star[1].mass,//5
               stardata->star[0].q,//6
               stardata->star[0].stellar_type,//7
               stardata->star[0].radius / stardata->star[0].roche_radius,//8
               stardata->model.in_RLOF
              );



#ifdef DISCS
#ifdef STELLAR_POPULATIONS_ENSEMBLE
    if(1 &&
            Use_ensemble(STELLAR_POPULATIONS_ENSEMBLE_FILTER_CBDISCS) &&
            stardata->common.ndiscs > 0)
    {
        struct disc_t * const disc = & stardata->common.discs[0];
        struct libbinary_c_disc_info_t * info = binary_c_disc_info(stardata);

        if(info != NULL && Disc_is_disc(disc))
        {
            make_ensemble_cdict(stardata);

            if(Is_zero(info->lifetime))
            {
                Set_ensemble_value("IRAS init",
                                   "M_disc", (double)info->M
                                  );
                Set_ensemble_value("IRAS init",
                                   "J_disc", (double)info->J
                                  );
                Set_ensemble_value("IRAS init",
                                   "L_disc", (double)info->L
                                  );
                Set_ensemble_value("IRAS init",
                                   "R_in", (double)info->Rin
                                  );
                Set_ensemble_value("IRAS init",
                                   "R_out", (double)info->Rout
                                  );
                Set_ensemble_value("IRAS init",
                                   "T_in", (double)info->Tin
                                  );
                Set_ensemble_value("IRAS init",
                                   "T_out", (double)info->Tout
                                  );
                Set_ensemble_value("IRAS init",
                                   "Hin_Rin", (double)info->Hin_Rin
                                  );
                Set_ensemble_value("IRAS init",
                                   "Hout_Rout", (double)info->Hout_Rout
                                  );
            }
            Set_ensemble_value("IRAS final",
                               "lifetime", (double)disc->lifetime / YEAR_LENGTH_IN_SECONDS
                              );
            struct cdict_entry_t * const denominator =
                Set_ensemble_denominator("IRAS",
                                         "denominator");
            Set_ensemble_mean(denominator,
                              "IRAS",
                              "M_disc", (double)info->M
                             );
            Set_ensemble_mean(denominator,
                              "IRAS",
                              "J_disc", (double)info->J
                             );
            Set_ensemble_mean(denominator,
                              "IRAS",
                              "L_disc", (double)info->L
                             );
            Set_ensemble_mean(denominator,
                              "IRAS",
                              "R_in", (double)info->Rin
                             );
            Set_ensemble_mean(denominator,
                              "IRAS",
                              "R_out", (double)info->Rout
                             );
            Set_ensemble_mean(denominator,
                              "IRAS",
                              "T_in", (double)info->Tin
                             );
            Set_ensemble_mean(denominator,
                              "IRAS",
                              "T_out", (double)info->Tout
                             );
            Set_ensemble_mean(denominator,
                              "IRAS",
                              "Hin_Rin", (double)info->Hin_Rin
                             );
            Set_ensemble_mean(denominator,
                              "IRAS",
                              "Hout_Rout", (double)info->Hout_Rout
                             );
            Set_ensemble_mean(denominator,
                              "IRAS",
                              "Mdot_visc", (double)info->Mdot_visc
                             );
            Set_ensemble_mean(denominator,
                              "IRAS",
                              "Rin_a", (double)info->Rin_a
                             );
            Set_ensemble_mean(denominator,
                              "IRAS",
                              "Rout_a", (double)info->Rout_a
                             );
            Safe_free(info);
        }
    }
#endif // STELLAR_POPULATIONS_ENSEMBLE
#endif // DISCS

//#define LEWIS_TIDES
#ifdef LEWIS_TIDES
    if(stardata->model.comenv_count > 0)
    {
        // experiments for Lewis
        static double tcomenv = 0.0;
        if(Is_zero(tcomenv)) tcomenv = stardata->model.time;

        Star_number k;
        Starloop(k)
        {
            const double q = stardata->star[k].q;
            const double P = stardata->common.orbit.period * YEAR_LENGTH_IN_DAYS;
            const double tZ =
                1e4 * Pow2((1.0 + q) * (2.0 * q)) * Pow4(P);
            const double tZ2 =
                stardata->star[k].omega * 3.0 / 5.0 /
                fabs(
                    stardata->star[k].derivative[DERIVATIVE_STELLAR_ANGULAR_VELOCITY_TIDES] *
                    pow(fabs(stardata->common.orbit.angular_frequency / stardata->star[k].omega - 1.0), 7.0 / 3.0)
                );
            Printf("TSYNC%d t=%g Myr st=%d q=%g P=%g d, omega_spin=%g omega_orb=%g domega/dt=%g tsync=%g Zahn=%g %g\n",
                   k,
                   stardata->model.time - tcomenv,
                   stardata->star[k].stellar_type,
                   q,
                   stardata->common.orbit.period * YEAR_LENGTH_IN_DAYS,
                   stardata->star[k].omega, //y^-1
                   stardata->common.orbit.angular_frequency, // y^-1
                   stardata->star[k].derivative[DERIVATIVE_STELLAR_ANGULAR_VELOCITY_TIDES], //y^-2
                   Sync_timescale(k) * 1e-6,//My
                   tZ * 1e-6,//Myr
                   tZ2 * 1e-6 //Myr
                  );
        }
        Printf("LEWIS %g %g %g %g\n",
               stardata->star[1].q,
               stardata->common.orbit.period * YEAR_LENGTH_IN_DAYS,
               Sync_timescale(0) * 1e-6,
               Sync_timescale(1) * 1e-6);
    }
#endif // LEWIS_TIDES

#ifdef STELLAR_PATHWAYS
    printf("PATHWAY %g %g %g %d\n",
           stardata->model.time,
           stardata->common.zero_age.mass[0],
           stardata->star[0].mass,
           stardata->star[0].stellar_type);
#endif // STELLAR_PATHWAYS

//#define __ROTATION_TESTS
#ifdef __ROTATION_TESTS
    if(stardata->star[0].stellar_type <= MAIN_SEQUENCE)
    {
        const double I Maybe_unused = moment_of_inertia(stardata,
                                      &stardata->star[0],
                                      stardata->star[0].radius);
        const double P = YEAR_LENGTH_IN_DAYS * 2.0 * PI / stardata->star[0].omega;
        printf("ROT %g %g %g %g %g %g\n",
               stardata->model.time,
               stardata->star[0].angular_momentum,
               stardata->star[0].k2,//I,
               stardata->star[0].omega,
               stardata->star[0].radius,
               P);

        /*
          printf("HRD %g %g\n",
          log10(Teff_from_star_struct(&stardata->star[0])),
          log10(stardata->star[0].luminosity));
        */

        if(stardata->star[0].stellar_type <= 1)
            printf("GIO %g %g %g %g %g\n",
                   stardata->model.time,
                   0.7,
                   stardata->star[0].radius,
                   stardata->star[0].luminosity,
                   stardata->star[0].k2);
    }
#endif//__ROTATION_TESTS

#ifdef ORBITING_OBJECTS
    log_orbiting_objects(stardata);
#endif// ORBITING_OBJECTS

#ifdef NOVA_DEBUG
    printf("H-nova t=%g H-layer mass %g \nH-\nH-\n",
           stardata->model.time,
           stardata->star[0].dm_novaH);
#endif//NOVA_DEBUG

    if(stardata->preferences->progress_bar)
    {
        /*
         * Progress "bar"
         */
        const double runtime = (double)Timer_seconds(getticks() - stardata->common.start_tick) - stardata->warmup_timer_offset;
        const double runtime_system = (double)Timer_seconds(getticks() - stardata->common.system_start_tick) - stardata->warmup_timer_offset;
        printf("%7d %7d %7d : seed %10ld : runtime %7.2f %7.2f : checksum %lld \x0d",
               stardata->model.model_number,
               stardata->model.n_rejected_models,
               stardata->model.n_rejected_models_without_shorten,
               //stardata->preferences->random_systems == TRUE ? (int)stardata->common.random_systems_seed : (int)stardata->common.init_random_seed,
               (long int)stardata->common.random_seed,
               runtime_system,
               runtime,
               checksum_stardata(stardata)
              );
        if(final == TRUE ||
                Fequal(stardata->preferences->max_evolution_time,
                       stardata->model.time))
        {
            printf("\n\n");
        }
        fflush(stdout);
    }

    /*
     * Be stars
     */
    if(0 && System_is_binary)
    {
        Foreach_evolving_star(donor, accretor)
        {
            if(cs_Be_criterion(stardata,
                                donor,
                                accretor) == TRUE)
            {
                printf("%d : Be star t = %g\n",
                       donor->starnum,
                       stardata->model.time);
            }
        }
    }

//#define WRLOF_DUST_CHECK
#ifdef WRLOF_DUST_CHECK
    if(1)
    {

        Foreach_evolving_star(donor, accretor)
        {
            if(donor->stellar_type < 10 &&
                    WHITE_DWARF(accretor->stellar_type))
            {
                const double T_cond_Na = nucsyn_elemental_condensation_temperature(stardata, "Na");
                const double T_cond_Ca = nucsyn_elemental_condensation_temperature(stardata, "Ca");
                const double R_Na = R_of_T(donor, T_cond_Na);
                const double R_Ca = R_of_T(donor, T_cond_Ca);
                printf("%s->%s : donor L %g accretor %g : Teff %g T_Na %g T_Ca %g : R %g a %g : Rdust_Na %g Rdust_Ca %g : Rdust_Na/RL %g Rdust_Ca/RL %g : Rdust_Na/R %g Rdust_Ca/R %g \n",
                       Stellar_type_string_from_star(donor),
                       Stellar_type_string_from_star(accretor),
                       donor->luminosity,
                       accretor->luminosity,
                       Teff_from_star_struct(donor),
                       T_cond_Na,
                       T_cond_Ca,
                       donor->radius,
                       stardata->common.orbit.separation,
                       R_Na,
                       R_Ca,
                       R_Na / donor->roche_radius,
                       R_Ca / donor->roche_radius,
                       R_Na / donor->radius,
                       R_Ca / donor->radius
                      );
            }
        }
    }
#endif // WRLOF_DUST_CHECK

#ifdef PPISN
    /* PPISN test*/
    if(0 && stardata->preferences->save_pre_events_stardata == 1)
    {
        if(stardata->star[0].SN_type == SN_PPI || stardata->star[0].SN_type == SN_PI)
        {
            struct star_t pre_sd = stardata->pre_events_stardata->star[0];
            double helium_mass = 0;
            helium_mass = pre_sd.core_mass[CORE_He];
            Printf("helium_mass: %g\n", helium_mass);
            if(NAKED_HELIUM_STAR(pre_sd.stellar_type))
            {
                helium_mass = pre_sd.mass;
            }

            const double hydrogen_layer_mass = pre_sd.mass - helium_mass;
            Printf("preSN mass: %g preSN CO core mass: %g preSN he core mass: %g preSN h-layer mass: %g preSN st %d\n",
                   pre_sd.mass,
                   pre_sd.core_mass[CORE_CO],
                   helium_mass,
                   hydrogen_layer_mass,
                   pre_sd.stellar_type
            );
        }
    }
#endif // PPISN

    /* Handle the printing of the events */
#ifdef EVENT_BASED_LOGGING
    if(final==TRUE)
    {
        event_based_logging_print_event_logstrings(stardata);
    }
#endif // EVENT_BASED_LOGGING

    if(0 &&
       timestep_fixed_trigger(stardata,
                              FIXED_TIMESTEP_ARG))
    {
        /*
         * Output at command-line specified times
         */
        Printf("CAUGHT at T %20.12e\n",stardata->model.time);
    }


    if(0)
    {
        /*
         * JWST colour-magnitudes
         */
        double * magnitudes = Calloc(STELLAR_MAGNITUDE_NUMBER,
                                     sizeof(double));
        stellar_magnitudes(stardata,
                           &stardata->star[0],
                           &magnitudes);

#define JWST_FILTERS \
    X( F070W)        \
    X( F090W)        \
    X( F150W)        \
    X( F250M)        \
    X( F277W)        \
    X( F360M)        \
    X( F430M)        \
        X( F444W)    \
        X( F480M)


        /* header */
        if(stardata->model.model_number==0)
        {
#undef X
#define X(FILTER) "%s "
            static char * jwst_format = "JWST "JWST_FILTERS "\n";
#undef X
#define X(FILTER)                               \
            ,#FILTER

            Printf(jwst_format
                   JWST_FILTERS);
        }

        /* data */
        if(stardata->star[0].stellar_type <= 6)
        {
#undef X
#define X(FILTER) "%g "
            static char * jwst_format = "JWST "JWST_FILTERS "\n";

#undef X
#define X(FILTER)                                               \
            ,magnitudes[STELLAR_MAGNITUDE_YBC_JWST_##FILTER]

            Printf(jwst_format
                   JWST_FILTERS);
        }

        Printf("CMD %g %g\n",
               magnitudes[STELLAR_MAGNITUDE_YBC_JWST_F090W] - magnitudes[STELLAR_MAGNITUDE_YBC_JWST_F150W],
               magnitudes[STELLAR_MAGNITUDE_YBC_JWST_F090W]);

        Safe_free(magnitudes);
    }

    if(0 && stardata->star[1].TZO == TRUE)
    {
        char * corestring =
            core_string(&stardata->star[1],FALSE);
        Printf("t=%g TZO M=%g cores %s st=%d age=%g L=%g R=%g\n",
               stardata->model.time,
               stardata->star[1].mass,
               corestring,
               stardata->star[1].stellar_type,
               stardata->star[1].age,
               stardata->star[1].luminosity,
               stardata->star[1].radius
            );
        Safe_free(corestring);
    }

#ifdef MEMORY_USE
    if(final)
    {
        log_memory_use(stardata);
    }
#endif//MEMORY_USE

#ifdef __TESTING
    if(0)
    {
        if(stardata->star[0].mass > stardata->star[0].phase_start[MAIN_SEQUENCE].mass + 15.0)
        {
            Flexit;
        }
        else
        {
            struct star_t * const star = &stardata->star[0];
            const double mdot = Mdot_gain(star); /* Msun / year */
            const double mdot_th =
                star->radius * R_SUN * star->luminosity * L_SUN /
                (GRAVITATIONAL_CONSTANT * star->mass * M_SUN)
                / (M_SUN / YEAR_LENGTH_IN_SECONDS);
            if(mdot > 0.0)
            {
                printf("RRR %g %g %g %g\n",
                       stardata->star[0].mass - stardata->star[0].phase_start[MAIN_SEQUENCE].mass,
                       log10(stardata->star[0].radius),
                       stardata->star[0].Ith,
                       mdot / mdot_th /* >1 means thermally "unstable" */
                    );
            }
        }
    }
#endif // __TESTING__

    if(stardata->preferences->log_transients_2024 == TRUE)
    {
        Boolean have_logged_transient = FALSE;
        struct stardata_t * const previous_stardata = stardata->previous_stardata;

        Foreach_star(star,other_star)
        {
            struct star_t * const previous_other_star =
                &previous_stardata->star[other_star->starnum];

            /*
             * Detect if there was a comenv
             */
            const Boolean was_comenv =
                stardata->previous_stardata->model.comenv_count !=
                stardata->model.comenv_count;

            const Boolean newly_merged =
                /* one of the flags should be set */
                (
                    stardata->model.merged == TRUE ||
                    stardata->model.coalesce == TRUE ||
                    was_comenv == TRUE ||
                    stardata->model.comenv_type !=0
                    ) &&

                /* must change from binary to single */
                stardata->model.sgl==TRUE &&
                previous_stardata->model.sgl==FALSE &&

                /* companion must be a massless remnant and wasn't last time */
                other_star->stellar_type==MASSLESS_REMNANT &&
                previous_other_star->stellar_type!=MASSLESS_REMNANT &&
                Is_zero(other_star->mass) &&
                Is_not_zero(previous_other_star->mass);

            const Boolean new_transient =
                /* haven't already logged */
                have_logged_transient == FALSE &&
                (
                    /* comenv and this is the donor */
                    (stardata->model.comenv_type != 0 &&
                     stardata->model.comenv_overflower == star->starnum)
                    ||
                    was_comenv
                    ||
                    /* or a straightforward merger */
                    newly_merged
                    );

            if(new_transient)
            {
                Printf("TRANSIENT t=%g prob=%g ",
                       stardata->model.time,
                       stardata->model.probability);

                struct stardata_t * s = stardata->previous_stardata;
#define pair(s,_X) s->star[0]._X, s->star[1]._X
                Printf("%s st1=%s st2=%s M1=%g M2=%g Menv1=%g Menv2=%g R1=%g R2=%g L1=%g L2=%g RL1=%g RL2=%g RZAMS1=%g RZAMS2=%g a=%g P=%g e=%g ",
                       "pre",
                       Short_stellar_type_string_from_star(&s->star[0]),
                       Short_stellar_type_string_from_star(&s->star[1]),
                       pair(s,mass),
                       s->star[0].mass - Outermost_core_mass(&s->star[0]),
                       s->star[1].mass - Outermost_core_mass(&s->star[1]),
                       pair(s,radius),
                       pair(s,luminosity),
                       pair(s,roche_radius),
                       pair(s,rzams),
                       s->common.orbit.separation,
                       s->common.orbit.period,
                       s->common.orbit.eccentricity);

                s = stardata;
                Printf("%s st1=%s st2=%s M1=%g M2=%g Menv1=%g Menv2=%g R1=%g R2=%g L1=%g L2=%g a=%g P=%g e=%g\n",
                       "post",
                       Short_stellar_type_string_from_star(&s->star[0]),
                       Short_stellar_type_string_from_star(&s->star[1]),
                       pair(s,mass),
                       s->star[0].mass - Outermost_core_mass(&s->star[0]),
                       s->star[1].mass - Outermost_core_mass(&s->star[1]),
                       pair(s,radius),
                       pair(s,luminosity),
                       s->common.orbit.separation,
                       !newly_merged ? s->common.orbit.period : 0.0,
                       !newly_merged ? s->common.orbit.eccentricity : 0.0);

                have_logged_transient = TRUE;
            }
        }
    }

    if(0)
    {
        //if(stardata->star[1].stellar_type == GIANT_BRANCH)
        {
            Isotope_loop(i)
            {
                char * p = string_tolower(nucsyn_isotope_strings[i]);
                printf("%s %g\n",
                       p,
                       stardata->star[1].Xenv[i]);
                Safe_free(p);
            }
            Exit_binary_c(BINARY_C_NORMAL_EXIT,"Exit after isotopes\n");
        }
    }
}
