#include "../binary_c.h"
No_empty_translation_unit_warning;


void logwrap(struct stardata_t * Restrict const stardata,
             const char * Restrict const label,
             const char * const leadchar)
{
#ifdef FILE_LOG
    /*
     * Wrapper for the most common logging cases
     */
    output_to_logfile(stardata->model.log_fp,
                      stardata->model.time,
                      stardata->star[0].mass,
                      stardata->star[1].mass,
                      stardata->star[0].stellar_type,
                      stardata->star[0].stellar_type==COWD ? stardata->star[0].hybrid_HeCOWD : FALSE,
                      stardata->star[1].stellar_type,
                      stardata->star[1].stellar_type==COWD ? stardata->star[1].hybrid_HeCOWD : FALSE,
                      stardata->common.orbit.separation,
                      stardata->common.orbit.period,
                      stardata->common.orbit.eccentricity,
                      (Is_zero(stardata->star[0].roche_radius) ? 0.0 :
                       Max(0.0,stardata->star[0].radius/stardata->star[0].roche_radius)),
                      (Is_zero(stardata->star[1].roche_radius) ? 0.0 :
                       Max(0.0,stardata->star[1].radius/stardata->star[1].roche_radius)),
                      label,
                      stardata,
                      leadchar);
#endif // FILE_LOG
}
