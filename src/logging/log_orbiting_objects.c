#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef ORBITING_OBJECTS

void log_orbiting_objects(struct stardata_t * Restrict const stardata)
{
    char * header = "Object";
    if(stardata->preferences->orbiting_objects_log == TRUE)
    {
        Printf("%s Stars %30.20e %g %g\n",
               header,
               stardata->model.time,
               stardata->star[0].radius,
               stardata->star[1].radius);
        for(Orbiting_object_type j=0; j<ORBITING_OBJECT_TYPE_NUMBER; j++)
        {
            Foreach_orbiting_object(o)
            {
                const Boolean bound = o->type == ORBITING_OBJECT_TYPE_UNBOUND ? FALSE : TRUE;
                if(o->type == j)
                {
                    const double temperature =
                        Orbiting_object_is_bound(o) ?
                        orbiting_object_temperature(stardata, o) : -1.0;

                    const double omega_c =
                        o->central_object == NULL
                        ? -1.0 :
                        o->type == ORBITING_OBJECT_TYPE_CIRCUMSTELLAR
                        ? ((struct star_t const *)o->central_object)->omega :
                        o->type == ORBITING_OBJECT_TYPE_CIRCUMBINARY
                        ? ((struct stardata_t const *)o->central_object)->common.orbit.angular_frequency :
                        -1.0;

                    Printf("%s %zu %s%s%30.20e %s %g ",
                           header,
                           o->index, // 2
                           o->name != NULL ? o->name : "", // 3
                           o->name != NULL ? " " : "",
                           stardata->model.time, // 4
                           orbiting_object_central_object_short_string(stardata,o),//5
                           o->mass//6
                        );
                    if(bound == TRUE)
                    {
                        Printf("%g %g %g %g %g %g %g ",
                               o->orbit.angular_momentum, //7
                               o->orbit.separation,//8
                               o->orbit.period,//9
                               o->orbit.eccentricity,//10
                               temperature, // 11
                               o->orbit.angular_frequency, //12
                               omega_c//13
                            );
                    }
                    else
                    {
                        /* 7-13 unbound -> no data */
                        Printf("* * * * * * * ");
                    }
                    Printf("%d\n",
                           o->in_disc == TRUE ? 1 : 0 // 14
                        );
                }
            }
        }
    }
}

#endif // ORBITING_OBJECTS
