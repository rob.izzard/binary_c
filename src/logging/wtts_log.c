#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef WTTS_LOG

/*
 * Logging for Window To The Stars 2
 *
 * Warning : this code is subject to change!
 */

// TODO only copy the required data, not the whole star
// or, indeed, use the RADIUS derivative rather than copying at all

void wtts_log(struct stardata_t * Restrict const stardata,
              const double dt)
{
// data column type
    struct datacolumn_t
    {
        const char *name;
        double value;
    };

    RLOF_stars;

#define fp_sys (stardata->tmpstore->fp_sys)
#define fp_star (stardata->tmpstore->fp_star)

    struct datacolumn_t sys_cols[]={
        {"Model_number",(double)stardata->model.model_number},
        {"Time",stardata->model.time*1e6},
        {"Orbital_Separation_Rsun",stardata->common.orbit.separation},
        {"Orbital_Period_days",stardata->common.orbit.period*YEAR_LENGTH_IN_DAYS},
        {"Orbital_Angular_Frequency",stardata->common.orbit.angular_frequency},
        {"Orbital_Eccentricity",stardata->common.orbit.eccentricity},
        {"Orbital_Angular_Momentum",stardata->common.orbit.angular_momentum},
        {"da/dt_Rsun_per_year",stardata->model.derivative[DERIVATIVE_ORBIT_SEMI_MAJOR_AXIS]},
        {"de/dt",stardata->model.derivative[DERIVATIVE_ORBIT_ECCENTRICITY]},
        {"d(Jorb)/dt",stardata->model.derivative[DERIVATIVE_ORBIT_ANGMOM]},
        {"Stellar_Angular_Momentum",stardata->star[0].angular_momentum+stardata->star[1].angular_momentum},
        {"Total_Angular_Momentum",stardata->common.orbit.angular_momentum + stardata->star[0].angular_momentum + stardata->star[1].angular_momentum},
        {"Total_Mass",(stardata->star[0].mass+stardata->star[0].mass)/M_SUN},
        {"Relative_orbital_velocity_km_s",0.0},
        {"Orbital_Energy",0.0},
        {"Orbital_Kinetic_Energy",0.0},
        {"Orbital_Potential_Energy",0.0},
        {"System_Mass_Loss_rate_Msun_yr",0.0},
        {"RLOF_Mass_transfer_rate",
         donor->derivative[DERIVATIVE_STELLAR_MASS_RLOF_TRANSFER]},
        {"RLOF_Mass_accretion_rate",accretor->derivative[DERIVATIVE_STELLAR_MASS_RLOF_GAIN]},
        {"RLOF_Mass_transfer_efficiency_(beta)",
         donor->derivative[DERIVATIVE_STELLAR_MASS_RLOF_TRANSFER]>TINY ? accretor->derivative[DERIVATIVE_STELLAR_MASS_RLOF_GAIN]/donor->derivative[DERIVATIVE_STELLAR_MASS_RLOF_TRANSFER] : 0.0},


    };

    // calculate number of columns
    const size_t nsys_cols = (sizeof(sys_cols)/sizeof(struct datacolumn_t));

    if((Is_zero(stardata->model.time) && fp_sys==NULL)
       || fp_sys==NULL)
    {
        fp_sys = fopen("./system","w");
        if(fp_sys!=NULL)
        {
            int err=fprintf(fp_sys,"# log output for binary system (solar units where appropriate)\n#");
            for(size_t k=0;k<nsys_cols;k++)
            {
                err += fprintf(fp_sys," %zu=%s",k+1,sys_cols[k].name);
            }
            err += fprintf(fp_sys,"\n");
            if(err<0)
            {
                Exit_binary_c(BINARY_C_WRITE_FAILED,
                              "Failed to write stellar log header\n");
            }
        }
    }

    if(Is_zero(dt)) return;

    /*
     * Output to the system log file
     */

    if(fp_sys!=NULL)
    {
        int err = 0;
        for(size_t j=0;j<nsys_cols;j++)
        {
            err += fprintf(fp_sys,"%g",sys_cols[j].value) + fprintf(fp_sys," ");
        }
        err += fprintf(fp_sys,"\n");
        if(err<0)
        {
            Exit_binary_c(BINARY_C_WRITE_FAILED,"Failed to write system log\n");
        }
    }
#undef NUCSYN
    Foreach_evolving_star(star,companion)
    {
        if(star->stellar_type < stardata->preferences->max_stellar_type[star->starnum])
        {
#ifdef NUCSYN
            Abundance * const Xobs = nucsyn_observed_surface_abundances(star);
#else
#define XDUMMY (-1.0)
#endif//NUCSYN
            struct datacolumn_t star_cols[]={
                {"Model_number",(double)stardata->model.model_number},
                {"Age",stardata->model.time*1e6},
                {"Effective_Age",star->age*1e6},
                {"Timestep",dt*1e6},
                {"Stellar_type",star->stellar_type},
                {"Mass",star->mass},
                {"Mass_Ratio",star->q},
                {"logg",logg(star)},
                {"Separation",stardata->common.orbit.separation},
                {"da/dt",stardata->model.derivative[DERIVATIVE_ORBIT_SEMI_MAJOR_AXIS]},
                {"Radius",star->radius},
                {"Roche_Radius",star->roche_radius},
                {"dRoche_Radius/dt",star->derivative[DERIVATIVE_STELLAR_ROCHE_RADIUS]},
                {"Radius/Roche_Radius",star->radius/Max(1e-30,star->roche_radius)},
                {"RLOF_Mass_transfer_rate",stardata->star[ndonor].derivative[DERIVATIVE_STELLAR_MASS_RLOF_TRANSFER]},
                {"RLOF_Mass_accretion_rate",stardata->star[Other_star(ndonor)].derivative[DERIVATIVE_STELLAR_MASS_RLOF_GAIN]},
                {"Wind_mdot_loss",0.0},
                {"Wind_mdot_gain",0.0},
                {"Nuclear_mdot",0.0},
                {"Thermal_mdot",0.0},
                {"Dynamical_mdot",0.0},
                {"Luminosity",star->luminosity},
                {"Teff",Teff_from_star_struct(star)},
                {"Moment_of_Inertia",0.0},
                {"Angular_Momentum",star->angular_momentum},
                {"Angular_Velocity",star->omega},
                {"Equatorial_Velocity",star->v_eq},
#ifdef NUCSYN
                {"Xsurf_H1",Xobs[XH1]},
                {"Xsurf_He4",Xobs[XHe4]},
                {"Xsurf_C12",Xobs[XC12]},
                {"Xsurf_N14",Xobs[XN14]},
                {"Xsurf_O16",Xobs[XO16]}
#else
                {"Xsurf_H1",XDUMMY},
                {"Xsurf_He4",XDUMMY},
                {"Xsurf_C12",XDUMMY},
                {"Xsurf_N14",XDUMMY},
                {"Xsurf_O16",XDUMMY}
#endif//NUCSYN
            };

            FILE * fp = fp_star[companion->starnum];
            const size_t ncols = (size_t)(sizeof(star_cols)/sizeof(struct datacolumn_t));

            if(fp==NULL)
            {
                char *f;
                const int ret = asprintf(&f,"./star%d",star->starnum);
                if(ret > 0)
                {
                    fp_star[companion->starnum]=fopen(f,"w");
                    Safe_free(f);

                    if(fp_star[companion->starnum]==NULL)
                    {
                        Exit_binary_c(BINARY_C_FILE_OPEN_ERROR,"Failed to open file '%s' for output (wtts_log)\n",f);

                    }

                    fp = fp_star[companion->starnum];

                    int err=fprintf(fp,"# log output for binary system (solar units where appropriate)\n#");
                    for(size_t k=0;k<ncols;k++)
                    {
                        err += fprintf(fp," %zu=%s",k+1,star_cols[k].name);
                    }
                    err += fprintf(fp,"\n");
                    if(err<0)
                    {
                        Exit_binary_c(BINARY_C_WRITE_FAILED,"Failed to write stellar log header\n");
                    }
                }
                else
                {
                    Exit_binary_c(BINARY_C_FILE_OPEN_ERROR,
                                  "Failed to asprintf the filename string");

                }
            }

            if(fp != NULL)
            {
                int err Maybe_unused = 0;
                for(size_t j=0;j<ncols;j++)
                {
                    err += fprintf(fp,"%g",star_cols[j].value) + fprintf(fp," ");
                }
                err += fprintf(fp,"\n");
            }
        }
    }
}
#endif // WTTS_LOG
