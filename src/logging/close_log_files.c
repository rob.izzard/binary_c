/*
 * Function to close binary_c's log file streams
 */
#include "../binary_c.h"
No_empty_translation_unit_warning;


void close_log_files(FILE ** log_fp,
                     struct stardata_t * Restrict const stardata)
{
    if(stardata != NULL)
    {
#ifdef DISCS
#ifdef DISC_LOG_2D
        if(stardata->tmpstore != NULL &&
           stardata->tmpstore->disc_logfile2d != NULL)
        {
            binary_c_fclose(&stardata->tmpstore->disc_logfile2d);
            stardata->tmpstore->disc_logfile2d=NULL;
        }
#endif //DISC_LOG_2D
#ifdef DISC_LOG
        if(stardata->tmpstore != NULL &&
           stardata->tmpstore->disc_logfile != NULL)
        {
            binary_c_fclose(&stardata->tmpstore->disc_logfile);
            stardata->tmpstore->disc_logfile=NULL;
        }
#endif //DISC_LOG
#endif//DISCS


        /* if log_fp is stdout then just leave it! */
        if(!(*log_fp == stdout ||
             *log_fp==stderr))
        {
#ifdef FILE_LOG
            Dprint("FP trying to close files : log_fp = %p, *log_fp = %p\n",
                   (void*)log_fp,
                   (void*)(log_fp ? *log_fp : NULL));
            if(*log_fp != NULL)
            {
                const int ret = fclose(*log_fp);
                if(ret != 0)
                {
                    perror("open_log_files error on close log_fp");
                    char errorstring[1024];
                    if(strerror_r(errno,errorstring,1024)!=0)
                    {
                        fprintf(stderr,"%s\n",errorstring);
                    }
                    fflush(NULL);
                    Exit_binary_c(BINARY_C_FILE_CLOSE_ERROR,
                                  "Warning : Can't close log file, error number %d\n",
                                  errno);
                }
            }
            *log_fp=NULL;
            Dprint("FP closed, FP=%p=%p\n",(void*)log_fp,(void*)*log_fp);
#endif/* FILE_LOG */
        }
    }
}
