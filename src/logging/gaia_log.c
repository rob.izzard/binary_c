#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef GAIAHRD


/*
 * This function is deprecated. Use the ensemble log instead.
 */


/*
 * Calculate the data for the Gaia-like HRD
 *
 * if do_log is TRUE, Teff, L, primary and is_single are set.
 *
 * if do_log is FALSE, they are not set
 */

void gaia_log(struct stardata_t * const stardata,
              struct gaiaHRD_chunk_t * this)
{
    const double min_Teff = 1500.0; // K
    const double min_L = 0.5e-10; // minimum luminosity to be considered (Lsun)
    const double gaia_binwidth = 0.025; // match Vasily's Gaia bin width
    const double min_M2 = 0.01;  // min M2 mass to be considered a star Msun
    /* everything with P < 2 years could be binary */
    const double min_orbital_period_years = 2.0;

    /*
     * Is_luminous(n) tests star n to see if it
     * is NOT a massless remnant or black hole
     */
    if(Is_luminous(0) || Is_luminous(1))
    {
        /*
         * One of the stars is luminous
         */

        /*
         * M2min = 0.01 Msun
         *
         * Below this, only the primary is used
         */
        const Boolean M2_is_star = stardata->star[1].mass > min_M2;

        /*
         * Total luminosity and minimum observable limit
         */
        const double Lgaia =
            (Is_luminous(0) ? (stardata->star[0].luminosity) : 0.0) +
            ((M2_is_star && Is_luminous(1)) ? (stardata->star[1].luminosity) : 0.0);

        /*
         * Only log if L > min_L : otherwise nothing is seen
         */
        if(Lgaia > min_L)
        {
            /*
             * Mean effective temperature
             */
            const double weighted_Teff =
                (
                    0.0 +
                    /* contribution from star 0 */
                    (Is_luminous(0) ? (stardata->star[0].luminosity * Teff(0)) : 0.0) +
                    /* contribution from star 1 */
                    ((M2_is_star && Is_luminous(1)) ? (stardata->star[1].luminosity * Teff(1)) : 0.0)
                )
                /Lgaia;

            if(weighted_Teff > min_Teff)
            {
                /*
                 * Identify the primary star as the brightest
                 */
                const Star_number k =
                    M2_is_star == TRUE
                    ? ((Is_luminous(0) && stardata->star[0].luminosity > stardata->star[1].luminosity) ? 0 : 1)
                    : 0;

                /*
                 * Skip black holes, neutron stars, massless remnants
                 */
                if(Is_luminous(k))
                {
                    /*
                     * Radial velocity K1 of the primary, assuming sideways on
                     */
                    //const double K1 = 1e-5 * radial_velocity_K(stardata,PI/2.0,k+1); // km/s
                    //const double min_K1 = 5.0; // km/s

                    /*
                     * Hence is this a detected single or binary?
                     */
                    this->is_single =
                        Boolean_(
                            /* is M2 is not a star, this cannot be a binary */
                            M2_is_star == FALSE ||

                            /* the standard binary_c check for duplicity */
                            System_is_single ||

                            /* apply radial velocity check */
                            //K1 < min_K1 ||

                            /* Vasily's Gaia data selection chooses P<2 years */
                            stardata->common.orbit.period > min_orbital_period_years
                            );
                    this->Teff_binned = bin_data(log10(weighted_Teff),
                                                 stardata->preferences->gaia_Teff_binwidth);
                    this->L_binned = bin_data(log10(Lgaia),
                                              stardata->preferences->gaia_L_binwidth);
                    this->primary = k;
                    this->do_log = TRUE;

                    /*
                     * use the unresolved stellar colours
                     */
                    unresolved_stellar_magnitudes(stardata,
                                                  &stardata->tmpstore->unresolved_magnitudes);
                    double * const unresolved_magnitudes =
                        stardata->tmpstore->unresolved_magnitudes;

                    /*
                     * Raw Gaia data (not binned)
                     */
                    const double gaia_magnitude =
                        unresolved_magnitudes[STELLAR_MAGNITUDE_GAIA_G];

                    const double gaia_colour =
                        unresolved_magnitudes[STELLAR_MAGNITUDE_GAIA_GBP] -
                        unresolved_magnitudes[STELLAR_MAGNITUDE_GAIA_GRP];

                    /*
                     * Check the magnitude is physical
                     */
                    if(Magnitude_is_physical(gaia_magnitude))
                    {
                        /*
                         * Binned Gaia data: a bin width of 0.025
                         * matches Vasily's plots
                         */
                        this->gaia_magnitude_binned = bin_data(gaia_magnitude,
                                                               gaia_binwidth);
                        this->gaia_colour_binned = bin_data(gaia_colour,
                                                            gaia_binwidth);
                    }
                    else
                    {
                        this->gaia_magnitude_binned = STELLAR_MAGNITUDE_UNPHYSICAL;
                        this->gaia_colour_binned = STELLAR_MAGNITUDE_UNPHYSICAL;
                    }
                }
                else
                {
                    this->do_log = FALSE;
                    this->gaia_magnitude_binned = STELLAR_MAGNITUDE_UNPHYSICAL;
                    this->gaia_colour_binned = STELLAR_MAGNITUDE_UNPHYSICAL;
                }
            }
            else
            {
                this->do_log = FALSE;
                this->gaia_magnitude_binned = STELLAR_MAGNITUDE_UNPHYSICAL;
                this->gaia_colour_binned = STELLAR_MAGNITUDE_UNPHYSICAL;
            }
        }
        else
        {
            this->do_log = FALSE;
            this->gaia_magnitude_binned = STELLAR_MAGNITUDE_UNPHYSICAL;
            this->gaia_colour_binned = STELLAR_MAGNITUDE_UNPHYSICAL;
        }
    }
    else
    {
        /*
         * Both stars are massless or black holes :
         * set do_log false and make physical variables meaningless
         */
        this->do_log = FALSE;
        this->Teff_binned = -1.0;
        this->L_binned = -1.0;
        this->gaia_magnitude_binned = STELLAR_MAGNITUDE_UNPHYSICAL;
        this->gaia_colour_binned = STELLAR_MAGNITUDE_UNPHYSICAL;
        this->is_single = FALSE;
        this->primary = 0;
    }
}

#endif // GAIAHRD
