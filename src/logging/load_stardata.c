#include "../binary_c.h"
No_empty_translation_unit_warning;

#include <setjmp.h>
#include "../binary_c_code_options.h"

/*
 * Clean up SVN_REVISION or GIT_REVISION
 */
#define __DO_EXPAND(M) M ## 1
#define __EXPAND(M) __DO_EXPAND(M)
#if (!defined(SVN_REVISION)||(__EXPAND(SVN_REVISION)==1))
#undef SVN_REVISION
#undef SVN_URL
#endif

#define __DO_EXPAND(M) M ## 1
#define __EXPAND(M) __DO_EXPAND(M)
#if (!defined(SVN_REVISION)||(__EXPAND(SVN_REVISION)==1))
#undef GIT_REVISION
#undef GIT_URL
#endif

#define Replace_pointers(S)                                     \
    {                                                           \
        (S)->preferences = preferences;                         \
        (S)->store = store;                                     \
        (S)->tmpstore = tmpstore;                               \
        (S)->warmup_timer_offset = warmup_timer_offset;         \
        (S)->cpu_is_warm = cpu_is_warm;                         \
        (S)->previous_stardatas = previous_stardatas;           \
    }


void load_stardata(struct stardata_t * const stardata,
                   const char * const filename,
                   const Boolean overwrite_preferences)
{
    /*
     * Load a stardata struct from a file into the pointer at stardata.
     *
     * If overwrite_preferences is TRUE, then use the preferences
     * given in the file. Otherwise, the preferences in stardata are left
     * unchanged.
     */
    FILE * dumpfile = fopen(filename,"r");
    if(dumpfile==NULL)
    {
        Exit_binary_c(BINARY_C_FILE_OPEN_ERROR,
                      "Error when opening stardata dump file at %s\n",
                      filename);
    }

    const double maxt = stardata->model.max_evolution_time;

    /*
     * Allocate space and read in the data
     */
    struct stardata_dump_t * stardata_dump =
        Calloc(1,sizeof(struct stardata_dump_t));

    size_t have_read_bytes =
        fread(stardata_dump,
              1,
              sizeof(struct stardata_dump_t),
              dumpfile);

    /*
     * Check we've read the correct amount of data
     */
    if(have_read_bytes != sizeof(struct stardata_dump_t))
    {
        Exit_binary_c(BINARY_C_FILE_READ_ERROR,
                      "Error when reading from stardata dump file at %s: expected %zu bytes of data, but only read %zu.\n",
                      filename,
                      sizeof(struct stardata_dump_t),
                      have_read_bytes);
    }

    printf("n_previous = %u %u\n",
           stardata_dump->stardata.n_previous_stardatas,
           stardata->n_previous_stardatas);

    printf("Z previous %g\n",stardata_dump->stardata.common.metallicity);

    /*
     * Check SVN revision (if available) and internal format vs
     * current values. In theory these should match.
     */

#ifdef SVN_REVISION
    if(stardata_dump->svn_revision != SVN_REVISION)
    {
        fprintf(stderr,
                "Warning: loaded stardata dump with SVN revision %d, but current revision is %d. Problems may occur.\n",
                stardata_dump->svn_revision,
                SVN_REVISION);
    }
#endif
#ifdef GIT_REVISION
    if(strncmp(stardata_dump->git_revision,
               Stringof(GIT_REVISION),
               STRING_LENGTH-1) != 0)
    {
        fprintf(stderr,
                "Warning: loaded stardata dump with GIT revision %s, but current revision is %s. Problems may occur.\n",
                stardata_dump->git_revision,
                Stringof(GIT_REVISION));
    }
#endif
    if(stardata_dump->dump_format != STARDATA_DUMP_FORMAT)
    {
        fprintf(stderr,
                "Warning: loaded stardata dump with stardata_dump_format %d, but current STARDATA_DUMP_FORMAT is %d. Problems are likely to occur.\n",
                stardata_dump->dump_format,
                STARDATA_DUMP_FORMAT);
    }

    /*
     * Always copy stardata, but replace the pointers
     * and buffers
     */
    struct preferences_t * preferences = stardata->preferences;
    printf("PRELOAD prefs %p\n",(void*)stardata->preferences);
    struct store_t * store = stardata->store;
    struct tmpstore_t * tmpstore = stardata->tmpstore;
    double warmup_timer_offset = stardata->warmup_timer_offset;
    Boolean cpu_is_warm = stardata->cpu_is_warm;
    struct stardata_t ** previous_stardatas = stardata->previous_stardatas;

    /*
     * Free existing previous_stardatas, we don't want to overwrite
     * the pointers without freeing them!
     */
    free_stardata_stack(stardata);
    free_previous_stardatas(stardata);

    /*
     * Overwrite the stardata
     */
    deep_copy_stardata(&stardata_dump->stardata,
                       stardata);

    /* replace pointers */
    Replace_pointers(stardata);

    /*
     * Reset max evolution time
     */
    stardata->model.max_evolution_time = maxt;

    /*
     * Perhaps copy preferences
     */
    if(overwrite_preferences==TRUE)
    {
        memcpy(stardata->preferences,
               &stardata_dump->preferences,
               sizeof(struct preferences_t));
    }
    /*
     * require initial abundances
     */
#ifdef NUCSYN
    memcpy(stardata->preferences->the_initial_abundances,
           stardata_dump->preferences.the_initial_abundances,
           ISOTOPE_MEMSIZE);
#endif
    /*
     * Read in previous_stardatas stack
     */
    unsigned int i;
    stardata->previous_stardatas =
        Malloc(stardata->n_previous_stardatas*sizeof(struct stardata_t *));
    printf("PREVIOUS STARDATAS EXPECT %u\n",stardata->n_previous_stardatas);

    if(stardata->n_previous_stardatas)
    {
        for(i=0;i<stardata->n_previous_stardatas;i++)
        {
            stardata->previous_stardatas[i] = new_stardata(stardata->preferences);

            printf("Read into %p\n",(void*)stardata->previous_stardatas[i]);

            size_t have_read_bytes2 = fread(stardata->previous_stardatas[i],
                                           1,
                                           sizeof(struct stardata_t),
                                           dumpfile);
            printf("read %zu bytes into %p\n",
                   have_read_bytes2,
                   (void*)stardata->previous_stardatas[i]);

            if(have_read_bytes2 != sizeof(struct stardata_t))
            {
                Exit_binary_c(BINARY_C_FILE_READ_ERROR,
                              "Error when reading from stardata dump file (previous stardata %u/%u) at %s: expected %zu bytes of data, but only read %zu.\n",
                              i,
                              stardata->n_previous_stardatas,
                              filename,
                              sizeof(struct stardata_dump_t),
                              have_read_bytes2);
            }

            Replace_pointers(stardata->previous_stardatas[i]);
        }

        stardata->previous_stardata = stardata->previous_stardatas[0];
        printf("PREVIOUS STARDATA %p\n",(void*)stardata->previous_stardata);
    }
    /*
     * Close the file
     */
    if(fclose(dumpfile))
    {
        Exit_binary_c(BINARY_C_FILE_CLOSE_ERROR,
                      "Error when closing stardata dumpfile %s\n",
                      filename);
    }


    /*
     * Free memory and exit
     */
    Safe_free(stardata_dump);

}
