#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "logfile_macros.h"
/*
 * binary_c output to standard (BSE-format) logfile
 *
 * If this file is called with fp==NULL, reset the log_count
 */

void output_to_logfile(FILE * const fp,
                       const double time,
                       const double m1,
                       const double m2,
                       const Stellar_type stellar_type0,
                       const Boolean hybrid1,
                       const Stellar_type stellar_type1,
                       const Boolean hybrid2,
                       const double sep,
                       const double per,
                       const double ecc,
                       const double r1_rol, /* r1/roche_radius */
                       const double r2_rol, /* r2/roche_radius */
                       const char * const label,
                       struct stardata_t * Restrict const stardata,
                       const char * const leadchar
    )
{
    Dprint("Output to logfile\n");
    if(fp==NULL) return; // do nothing if no log file is open

#ifdef FILE_LOG
    Boolean skip = FALSE;
    char * logstring = logfile_string(time,
                                      m1,
                                      m2,
                                      stellar_type0,
                                      hybrid1,
                                      stellar_type1,
                                      hybrid2,
                                      sep,
                                      per,
                                      ecc,
                                      r1_rol,
                                      r2_rol,
                                      label,
                                      stardata,
                                      &skip,
                                      leadchar);

#ifdef STELLAR_TYPE_LOG
    _printf("LOG %11.4lf%9.3lf%9.3lf%3i%3i%13.3lf%6.2lf%8.3lf%8.3lf  %s\n",
            time,m1,m2,s1,s2,sep,ecc,Min(999.0,r1_rol),Min(999.0,r2_rol),label);
#endif

    if(cached == TRUE)
    {
        if(strncmp(prevlabel,"BEG_RCHE",8)==0 &&
           n_rlof > 0)
        {
            fprintf(fp,"%s %d\n",prevlogstring,n_rlof);
        }
        else
        {
            fprintf(fp,"%s\n",prevlogstring);
        }
    }

    /* output */
    if(fp != NULL && skip == FALSE)
    {
        if(stardata->tmpstore != NULL &&
           stardata->tmpstore->c_log_prefix != NULL)
        {
            fprintf(fp,
                    "%s%s",
                    stardata->tmpstore->c_log_prefix,
                    logstring);
        }
        else
        {
            fprintf(fp,"%s\n",logstring);
        }
    }

#ifdef FILE_LOG_ECHO
    /* echo to stdout */
    if(stardata->tmpstore != NULL &&
       stardata->tmpstore->c_log_prefix != NULL)
    {
        printf("%s",stardata->tmpstore->c_log_prefix);
    }
#ifdef FILE_LOG_COLOURS
    printf("%s%s%s\n",stardata->store->colours[YELLOW],logstring,stardata->store->colours[COLOUR_RESET]);
#else
    printf("%s\n",logstring);
#endif // FILE_LOG_COLOURS
#endif // FILE_LOG_ECHO

    if(fp != NULL &&
       strncmp(label,"MAX_TIME",8)==0)
    {
        fprintf(fp,"Probability : %.10g\n",
                stardata->model.probability);
    }

#ifdef FLUSH_LOG
    fflush(fp); // slow : but useful!
#ifdef FILE_LOG_ECHO
    fflush(stdout);
#endif //FILE_LOG_ECHO

#else
    /*
     * flush log if told to on the cmd line
     */
    if(stardata->preferences->flush_log == TRUE)
    {
        fflush(fp);
    }
#endif //FLUSH_LOG

    Safe_free(logstring);
#endif/*FILE_LOG*/
}
