#include "../binary_c.h"
No_empty_translation_unit_warning;



/*
 * Function to set a uuid in stardata
 *
 * Inspired by
 * https://stackoverflow.com/questions/51053568/generating-a-random-uuid-in-c
 */
void set_uuid(struct stardata_t * const stardata)
{
    /* Create uuid object*/
    uuid_t binuuid;
    uuid_generate_random(binuuid);

    /* Parse to string */
    char * uuid_string = malloc(UUID_STR_LEN * sizeof(char));
    uuid_unparse_upper(binuuid, uuid_string);

    /*
     * Set uuid in stardata.
     */
    memcpy(stardata->model.uuid,
           uuid_string,
           UUID_STR_LEN * sizeof(char));

    /*
     * And free.
     */
    Safe_free(uuid_string);
}
