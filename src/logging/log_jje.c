#include "../binary_c.h"
No_empty_translation_unit_warning;

#define WCO_TEST ((stardata->star[k].Xenv[XC12]/12.0+stardata->star[k].Xenv[XC13]/13.0+stardata->star[k].Xenv[XO16]/16.0)/(1e-14+stardata->star[k].Xenv[XHe4]/4.0))
#define XNMIN 0.01

#define JJNULL 0
#define JJRSG 1
#define JJISG 2
#define JJBSG 3
#define JJO 4
#define JJWNL 5
#define JJWNE 6
#define JJWC 7
#define JJWO 8
#define JJWR1 9
#define JJWR2 10
// make sure JJFINAL is always last
#define JJFINAL 11
#ifdef LOG_JJE
void log_jje(struct stardata_t * Restrict stardata)
{
    Star_number k; // star counter
    double teff; // effective temperature
    double dt; // timestep
    //Spectral_Type spectral_type; // spectral type

    double l; // log10(luminosity)
    Boolean flags[20]; // set to on or off depending on the type
    char* jjstrings[20]={"JJNULL","JJRSG","JJISG","JJBSG",
                         "JJO","JJWNL","JJWNE","JJWC","JJWO",
                         "JJWR1","JJWR2"};



    if(stardata->model.time<1e-10)
    {
        //first timestep: reset timer
        stardata->model.log_jje_tprev=0.0;
    }

    // reset all flags
    for(k=0;k<JJFINAL;k++)
    {
        flags[k]=0;
    }
    set_kelvin_helmholtz_times(stardata);
    // set timestep
    dt=1e6*(stardata->model.time-stardata->model.log_jje_tprev);
    stardata->model.log_jje_tprev=stardata->model.time;

    Starloop(k)
    {
        // for pre-remnant stars log stuff
        if((stardata->star[k].stellar_type<10)&&
           (stardata->star[k].stellar_type>0))
        {

            // log luminosity
            l=log10(stardata->star[k].luminosity);

            // calculate effective temperature
            teff = 1000.0*pow((1130.0*stardata->star[k].luminosity/
                               Pow2(stardata->star[k].radius)),
                              0.25);


            // star is a supergiant
            teff=log10(teff);

            // New way to define jje's stars
            if(stardata->star[k].Xenv[XH1]>0.4)
            {
                if(l>4.9)
                {
                    if(teff<3.66)
                    {
                        // red supergiant
                        //printf("JJRSG %g\n",dt);
                        flags[JJRSG]=1;
                    }
                    else if(teff>3.9)
                    {
                        // blue supergiant
                        //printf("JJBSG %g\n",dt);
                        flags[JJBSG]=1;
                    }
                    else
                    {
                        // intermediate supergiant
                        //printf("JJISG %g\n",dt);
                        flags[JJISG]=1;
                    }
                }

                if(teff>4.48)
                {
                    // O-star
                    //printf("JJO %g\n",dt);
                    flags[JJO]=1;
                }
            }

            /*printf("Check WR type %d teff %g X %g\n",
              stardata->star[k].stellar_type,
              teff,
              stardata->star[k].Xenv[XH1]);
            */
            if(//(teff>4.0)&& // dubious temperature check
                (stardata->star[k].Xenv[XH1]<0.4))
            {

                if(// always true here! (stardata->star[k].Xenv[XH1]<0.4)&&
                    (stardata->star[k].Xenv[XH1]>XNMIN))
                {
                    flags[JJWNL]=1;
                }
                else if(stardata->star[k].Xenv[XH1]<XNMIN)
                {
                    if(WCO_TEST > 1.0)
                    {
                        flags[JJWO]=1;
                    }
                    else if(WCO_TEST > 3e-2)
                    {
                        /*printf("WC type %d X=%g Y=%g C=%g N=%g O=%g\n",stardata->star[k].stellar_type,
                          stardata->star[k].Xenv[XH1],
                          stardata->star[k].Xenv[XHe4],
                          stardata->star[k].Xenv[XC12],
                          stardata->star[k].Xenv[XN14],
                          stardata->star[k].Xenv[XO16]

                          );
                        */
                        flags[JJWC]=1;
                    }
                    else
                    {
                        flags[JJWNE]=1;
                    }
                }

                // WR star (definition 1, perhaps 2)
                flags[JJWR1]=1;

                if(l>4.9)
                {
                    // WR definition 2
                    /*printf("JJWR2 %d %g\n",
                      wr_type,
                      dt);
                    */
                    flags[JJWR2]=1;
                }
            }

        }
    }

    for(k=1;k<JJFINAL;k++)
    {
        if(flags[k]==1)
        {
            printf("%s %g\n",jjstrings[k],dt);
        }
    }



}
#endif // LOG_JJE
