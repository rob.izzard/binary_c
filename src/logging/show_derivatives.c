#include "../binary_c.h"
No_empty_translation_unit_warning;


void show_derivatives(struct stardata_t * Restrict const stardata)
{
    /*
     * Show all (non-zero) derivatives
     */
    struct stardata_t * prev = stardata->previous_stardata;
    int d;
    Star_number n;
    Boolean shown_timestep = FALSE;

    Printf("************************************************************\n");
    Printf("Stellar Derivatives at model=%d t=%30.20e (dtm=%g) system a=%g e=%g\n",
           stardata->model.model_number,
           stardata->model.time,
           stardata->model.dtm,
           stardata->common.orbit.separation,
           stardata->common.orbit.eccentricity
        );

#define _show_star(STAR,COLOUR,HEADER)                                  \
    Printf("%s%s %d type %2d M=%10g Mc=%10g Menv=%10g L=%10g R=%10g RL=%10g J=%10g omega=%10g I=%10g %s\n", \
           (COLOUR),                                                    \
           (HEADER),                                                    \
           (STAR)->starnum,                                             \
           (STAR)->stellar_type,                                        \
           (STAR)->mass,                                                \
           Outermost_core_mass(STAR),                                   \
           envelope_mass(STAR),                                         \
           (STAR)->luminosity,                                          \
           (STAR)->radius,                                              \
           (STAR)->roche_radius,                                        \
           (STAR)->angular_momentum,                                    \
           (STAR)->omega,                                               \
           moment_of_inertia(stardata,(STAR),((STAR)->radius)),         \
           stardata->store->colours[COLOUR_RESET]                       \
        )

    Foreach_star(star)
    {
        _show_star(star,"","Star");
        if(prev != NULL)
        {
            _show_star(&prev->star[star->starnum],
                       stardata->store->colours[YELLOW],
                       "Prev");
        }
    }

    Boolean dolog = FALSE;
    double ** stellar_derivatives = make_stellar_derivatives_array(stardata);
    for(d=0; d<DERIVATIVE_STELLAR_NUMBER; d++)
    {
        Boolean dostar = FALSE;
        Starloop(n)
        {
            if(Is_really_not_zero(stellar_derivatives[n][d]))
            {
                dostar = TRUE;
            }
        }
        if(dostar == TRUE)
        {
            dolog = TRUE;
            if(shown_timestep == FALSE)
            {
                Printf("Timesteps dt = %10g, dtm = %10g\n",
                       stardata->model.dt,
                       stardata->model.dtm);
                Starloop(n)
                {
                    struct star_t * star = & stardata->star[n];
                    Printf("Star %d : Net dM/dt = %10g, dJ/dt = %10g\n",
                           n,
                           Mdot_net(star),
                           Jdot_net(star)
                        );
                }
                shown_timestep = TRUE;
            }
            Printf("d% 3d/dt %30s : ",d,Stellar_derivative_string(d));
            Starloop(n)
            {
                struct star_t * star = & stardata->star[n];
                Printf("% 20g : ",
                       star->derivative[d]);
            }
            if(prev != NULL)
            {
                Printf("%s",stardata->store->colours[YELLOW]);
                Starloop(n)
                {
                    Printf("% 20g %% : ",
                           Percent_diff(stellar_derivatives[n][d],
                                        prev->star[n].derivative[d]));
                }
            }

            Starloop(n)
            {
                struct star_t * star = & stardata->star[n];
#undef X
#define X(CODE,STRING,VAR,FUNC,TYPE,MINTCLEAR,ADDITION_METHOD) VAR ,
                double * stellar_derivative_variable_pointers[] = { STELLAR_DERIVATIVES_LIST };
#undef X
                Printf("%s ",
                       stardata->store->colours[CYAN]);
                if(stellar_derivative_variable_pointers[d] != NULL)
                {
                    if(Is_really_zero(stellar_derivatives[n][d]))
                    {
                        Printf("%20s %3s ","infty","");
                    }
                    else
                    {
                        const double t = (*stellar_derivative_variable_pointers[d]) / stellar_derivatives[n][d];
                        const double ft = fabs(t);
                        Printf("%20g %3s ",
                               ft > 1e9 ? (1e-9*t) :
                               ft > 1e6 ? (1e-6*t) :
                               ft,
                               ft > 1e9 ? "Gyr" :
                               ft > 1e6 ? "Myr" :
                               "yr");
                    }
                }
                else
                {
                    Printf("%20s   ","");
                }
            }
            Printf("%s\n",stardata->store->colours[COLOUR_RESET]);
        }
    }
    Starloop(n)
    {
        struct star_t * star = & stardata->star[n];
        Printf("JDOT gain %10g loss %10g net %10g\n",Jdot_gain(star),Jdot_loss(star),Jdot_net(star));
    }
    free_stellar_derivatives_array(stellar_derivatives);
#define _show_system(STARDATA,COLOUR,HEADER)                            \
    Printf("%s%s at t=%10g dt=%10g dtm=%10g a=%10g e=%10g J=%10g omega=%10g%s\n", \
           (COLOUR),                                                    \
           (HEADER),                                                    \
           (STARDATA)->model.time,                                      \
           (STARDATA)->model.dt,                                        \
           (STARDATA)->model.dtm,                                       \
           (STARDATA)->common.orbit.separation,                         \
           (STARDATA)->common.orbit.eccentricity,                       \
           (STARDATA)->common.orbit.angular_momentum,                   \
           (STARDATA)->common.orbit.angular_frequency,                  \
           stardata->store->colours[COLOUR_RESET]                       \
        )

    Printf("************************************************************\n");
    _show_system(stardata,"","Orbit and System Derivatives");
    if(prev != NULL)
    {
        _show_system(prev,stardata->store->colours[YELLOW],"Previous ...                ");
    }

#undef X
#define X(CODE,STRING,VAR,FUNC,TYPE,MINTCLEAR) VAR ,
    double * system_derivative_variable_pointers[] = { SYSTEM_DERIVATIVES_LIST };
#undef X
    for(d=0;d<DERIVATIVE_SYSTEM_NUMBER;d++)
    {
        if(Is_really_not_zero(stardata->model.derivative[d]))
        {
            dolog = TRUE;
            Printf("DERIV % 3d %30s : % 20g",
                   d,
                   System_derivative_string(d),
                   stardata->model.derivative[d]);
            if(prev != NULL)
            {
                Printf(" : % 20g %%",
                       Percent_diff(stardata->model.derivative[d],
                                    prev->model.derivative[d]));
            }

            Printf("%s ",
                   stardata->store->colours[CYAN]);
            if(system_derivative_variable_pointers[d] != NULL)
            {
                Printf("%20g y ",
                       (*system_derivative_variable_pointers[d]) / stardata->model.derivative[d]
                    );
            }
            else
            {
                Printf("%20s   ","");
            }


            Printf("\n");
        }
    }
    if(dolog==FALSE)
    {
        Printf("All derivatives (stellar and system) are zero\n");
    }
    Printf("************************************************************\n");

}
