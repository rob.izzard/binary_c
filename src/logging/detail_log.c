#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef DETAILED_LOG
void detail_log(FILE *fp,
                double a1,
                double a2,
                double a3,
                double a4,
                double a5,
                double a6,
                double a7)
{

#ifdef FILE_LOG
    if(fp!=NULL)
    {
        if(fprintf(fp,"%lf %lf %lf %lf %lf %lf %lf\n",
                   a1,a2,a3,a4,a5,a6,a7)<1)
        {
            fprintf(stderr,"Warning: output to detailed logfile truncated\n");
        }
        fflush(fp);
    }
#endif//FILE_LOG

}
#endif // DETAILED_LOG
