#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef HRDIAG
/*
 * Function to output hr diagram to a file HRDIAG_FILENAME
 * Uses Teff_from_star_struct macro from binary_c_macros.h
 */

/* internal functions */
//static double rebin(double x,double dx);
static double rebin_start(double x,double dx);
static void out_hr_log(double t,
                       struct star_t * star1,
                       struct star_t * star2,
                       double logt,
                       double dtp,
                       double p,
                       double p_burning,
                       double m,
                       double m_burning,
                       double L,
                       double L_burning,
                       double fill,
                       Boolean use2,
               struct stardata_t * stardata);

void log_hr(struct stardata_t * Restrict const stardata)
{
    if(!stardata->preferences->hrdiag_output) return;


    double logt=log10(stardata->model.time+TINY);
    double p = stardata->model.probability,p_burning;
    double m,m_burning,L,L_burning;

    Boolean zams=FALSE;

#define prevlog (stardata->common.hrdiag_prevlog)
#define done_zams (stardata->common.hrdiag_done_zams)
#define use2 (stardata->common.hrdiag_use2)
#define filltot (stardata->common.hrdiag_filltot)
#define prevbintb (stardata->common.hrdiag_prevbintb)

    /* first timestep: output ZAMS information */
    if(Is_zero(stardata->model.time)&&(done_zams==FALSE))
    {
        stardata->model.next_hrdiag_log_time=HRDIAG_START_LOG-6.0
            -0.5*HRDIAG_BIN_SIZE_LOGTIME+TINY;
        zams=TRUE;
        prevlog=0.0;
        /* choose whether to include the secondary star in stats */
        use2 = stardata->common.zero_age.mass[1] > 0.09;
        /* debug initial distributions */
        printf("HRDINIT %g %g %g %g %g\n",
               stardata->star[0].mass,
               stardata->star[1].mass,
               stardata->common.orbit.period*YEAR_LENGTH_IN_DAYS,
               stardata->common.orbit.separation,
               stardata->model.probability);
    }

    /* arbitrary time (or ZAMS) output */
    if(More_or_equal(logt,stardata->model.next_hrdiag_log_time) ||
       zams==TRUE)
    {
        /* time since the previous log */
        double dt=stardata->model.time - prevlog;

        /* time bin at the current time */
        double logtb=rebin_start(logt,HRDIAG_BIN_SIZE_LOGTIME)+6.0;
        // TODO : start or mid bin?

        /* time bin for the previously logged time */
        double logtb0;
        if(prevlog<TINY)
        {
            /*
             * first log :
             * log from the previous time bin's upper edge
             */
            logtb0=logtb-HRDIAG_BIN_SIZE_LOGTIME;
            prevlog=exp10(logtb0-6.0-0.5*HRDIAG_BIN_SIZE_LOGTIME+TINY);
            dt=stardata->model.time - prevlog;
        }
        else
        {
            logtb0=rebin_start(log10(prevlog),HRDIAG_BIN_SIZE_LOGTIME)+6.0;
        }

        {
            /* convenience structure pointers */
            struct star_t * star1 = &(stardata->star[0]);
            struct star_t * star2 = &(stardata->star[1]);

            /* must set luminosity to zero for massless remnants */
            if(star1->stellar_type==MASSLESS_REMNANT) star1->luminosity=0.0;
            if(star2->stellar_type==MASSLESS_REMNANT) star2->luminosity=0.0;

//#define SAMPLE_DEBUG
#ifdef SAMPLE_DEBUG
            printf("Log at logt=%g t=%g (binned=%g) prevlog=%g : dt=%g y\n",
                   logt+6.0,
                   stardata->model.time,
                   logtb,
                   log10(prevlog)+6.0,
                   dt*1e6);
#endif

            /*
             * dt may span bins : we want the fraction in each bin
             * The fractions are f0 and f1, we must determine what
             * these should be. (If all in one bin, then f0=1, f1=0)
             *
             * NB these are the FRACTION of DT spent IN EACH BIN,
             * not the fraction of the bin to which dt contributes
             * (this is different!) : these are
             *  dt0 / dt
             * and
             *  dt1 / dt
             */
            double f0,f1;
            double frac_bin0;
            double frac_bin1;

            /* find the bin boundary */
            double bin_boundary = exp10(log10(exp10(logtb0-6.0))+
                                      0.5*HRDIAG_BIN_SIZE_LOGTIME);

            /* find the location of the next and previous bin boundaries */
            double prev_bin_boundary = exp10(log10(bin_boundary)-HRDIAG_BIN_SIZE_LOGTIME);
            double next_bin_boundary = exp10(log10(bin_boundary)+HRDIAG_BIN_SIZE_LOGTIME);

#ifdef SAMPLE_DEBUG
            printf("Bins boundaries at %g (centre=%g) <%g> (centre=%g) <%g>\n",
                   log10(prev_bin_boundary)+6.0,
                   logtb0,
                   log10(bin_boundary)+6.0,
                   logtb,
                   log10(next_bin_boundary)+6.0);
#endif

            if((!Fequal(logtb,logtb0))&&(logtb0>0.0))
            {
                /* find the time spent in the previous bin */
                double dt0=bin_boundary - prevlog;

                /* and the time spent in the current bin */
                double dt1=stardata->model.time - bin_boundary;

#ifdef SAMPLE_DEBUG
                printf("Log from %g to %g (time in prev bin %g, time in current bin %g)\n",
                       log10(exp10(logt)-dt)+6.0,logt+6.0,dt0,dt1);
#endif

                /* calculate the fraction of each bin which these constitute */
                frac_bin0 = dt0/(bin_boundary - prev_bin_boundary);
                frac_bin1 = dt1/(next_bin_boundary - bin_boundary);

#ifdef SAMPLE_DEBUG
                printf("Bin boundary at %g (%g)\n",bin_boundary,
                       log10(bin_boundary)+6.0);
#endif

                /* calculate the fractional weightings */
                f0=dt0/dt;
                f1=1.0-f0;

#ifdef SAMPLE_DEBUG
                printf("Sampling error : %g vs %g : dt0=%g dt1=%g : f0=%g f1=%g\n",
                       logtb,logtb0,
                       dt0,dt1,
                       f0,f1);
#endif
            }
            else
            {
                /* all in one bin f0=1, f1=1 */
                f0=1.0;
                f1=0.0;
                logtb0=logtb;
                frac_bin0=dt/(bin_boundary-prev_bin_boundary);
                frac_bin1=0.0;
            }

            filltot+=frac_bin0;

            /* add up the mass and luminosity in stars and nuclear-burning stars */
            m=star1->mass;

            L = star1->stellar_type<NEUTRON_STAR ? star1->luminosity : 0.0;

            if(star1->stellar_type < HeWD)
            {
                m_burning = m;
                L_burning = L;
            }
            else
            {
                m_burning=0.0;
                L_burning=0.0;
            }

            if(use2)
            {
                m += star2->mass;
                double L2 = star2->stellar_type<NEUTRON_STAR ? star2->luminosity : 0.0;
                L += L2;
                if(star2->stellar_type < HeWD)
                {
                    m_burning += star2->mass;
                    L_burning += L2;
                }
            }

            /* add up the probability in nuclear-burning systems */
            p_burning = ((star1->stellar_type < HeWD)||((use2)&&(star2->stellar_type < HeWD))) ? 1.0 : 0.0;

        Boolean output = TRUE;

#ifdef  HRDIAG_THICK_DISK_GIANTS_ONLY
        /* select only thick disk giants */
        if(! (stardata->star[0].stellar_type>=HERTZSPRUNG_GAP &&
          stardata->star[0].stellar_type<HeWD &&
          stardata->model.time>1000))
        {
        output=FALSE;
        }
#endif

            if(zams)
            {
                logt=0.0;
                done_zams=TRUE; // don't do ZAMS twice
        if(output)
                out_hr_log(stardata->model.time,
                           star1,star2,
                           logt,
                           p/dt, // this becomes dtp=p*dt/dt=p
                           p,p_burning,m,m_burning,
                           L,L_burning,1.0,use2,
               stardata
                    );
            }
            else
            {
                logt=logtb;
                done_zams=FALSE;

#ifdef SAMPLE_DEBUG
                printf("(f0=%g f1=%g)\n",f0,f1);
#endif
                if(f0>TINY)
                {
            if(output)
                    out_hr_log(stardata->model.time,
                               star1,star2,
                               logtb0,f0*dt,
                               p,p_burning,
                               m,m_burning,
                               L,L_burning,
                               frac_bin0,
                               use2,
                   stardata
                        );
                    if(!Fequal(logtb0,prevbintb))
                    {
#ifdef SAMPLE_DEBUG
                        printf("Fill %g = %g\n",prevbintb,filltot);
#endif
                        prevbintb=logtb0;
                        filltot=0.0;
                    }
                }

                if(f1>TINY)
                {

#ifdef SAMPLE_DEBUG
                    double dtp = f1 * dt * stardata->model.probability;
                    printf("dtp1=%g, f1=%g\n",dtp,f1);
#endif

            if(output)
                    out_hr_log(stardata->model.time,
                               star1,star2,
                               logtb,f1*dt,
                               p,p_burning,
                               m,m_burning,
                               L,L_burning,
                               frac_bin1,
                               use2,
                   stardata
                        );

#ifdef SAMPLE_DEBUG
                    printf("Fill %g = %g\n",prevbintb,filltot);
#endif
                    prevbintb=logtb;
                    filltot=frac_bin1;
#ifdef SAMPLE_DEBUG
                    printf("Add %g to filltot -> %g\n",filltot,filltot);
#endif
                }
            }
        }

        /* update next log time */
        stardata->model.next_hrdiag_log_time =
            Max(exp10(stardata->model.next_hrdiag_log_time),
                stardata->model.time);

        /* set the next log time from the guessed timestep */
        stardata->model.next_hrdiag_log_time += stardata->model.hrdiag_log_dt;

#ifdef HRDIAG_DTLOG
        printf("HRDT (log_hr) now logt=%12.12e prev at (log) %12.12e next at (log) %12.12e (hrdiag_log_dt=%g)\n",
               log10(stardata->model.time),
               log10(stardata->model.prev_hrdiag_log_time),
               log10(stardata->model.next_hrdiag_log_time),
               stardata->model.hrdiag_log_dt
            );
#endif

#ifdef SAMPLE_DEBUG
        printf("\n");
#endif

        if(stardata->model.next_hrdiag_log_time <
           stardata->model.prev_hrdiag_log_time)
        {
            Exit_binary_c(BINARY_C_OUT_OF_RANGE,
                          "HRDT (log_hr) error : want to go backwards! (1)\n");

        }

        if(stardata->model.next_hrdiag_log_time <
           stardata->model.time)
        {
            Exit_binary_c(BINARY_C_OUT_OF_RANGE,
                          "HRDT (log_hr) error : want to go backwards! (2)\n");
        }

        stardata->model.next_hrdiag_log_time =
            log10(stardata->model.next_hrdiag_log_time);

        stardata->model.prev_hrdiag_log_time = stardata->model.time;

        if(stardata->model.time>TINY)
        {
            /* set dt to zero so we are forced to guess it in deltat */
            stardata->model.hrdiag_log_dt=0.0;
        }

        prevlog = zams ? 0.0 : stardata->model.time;
    }
    else if(stardata->model.hrdiag_log_dt > TINY)
    {
        /* don't log but timestep may have changed */
        stardata->model.next_hrdiag_log_time =
            (log10(stardata->model.prev_hrdiag_log_time+
                   stardata->model.hrdiag_log_dt));
    }

#ifdef HRDIAG_DTLOG
    printf("HRDT (log_hr) next @ t=%12.12e (now %12.12e)\n",
           exp10(stardata->model.next_hrdiag_log_time),
           stardata->model.time);
#endif

}
#undef use2
static void out_hr_log(double t,
                       struct star_t * star1,
                       struct star_t * star2,
                       double logt,
                       double dt,
                       double p,
                       double p_burning,
                       double m,
                       double m_burning,
                       double L,
                       double L_burning,
                       double fill,
                       Boolean use2,
               struct stardata_t * stardata)
{
    double dtp=dt*p;
    if((dtp<TINY)||((logt>0)&&(logt<HRDIAG_START_LOG))) return;
    char * logstring = Malloc(10000 * sizeof(char));

    if((star1->stellar_type < NEUTRON_STAR)||
       ((use2)&&(star2->stellar_type < NEUTRON_STAR)))

    {
        /* modulate with the probability */
        double pf=p*fill;

        m *= pf;
        m_burning *= pf;
        L *= pf;

        L_burning *= pf;
        p_burning *= pf;

        double L1 = star1->stellar_type<NEUTRON_STAR ? star1->luminosity : 0.0;
        double L2 = star2->stellar_type<NEUTRON_STAR ? star2->luminosity : 0.0;

        /* output HR diagram for each star  */

        if(use2)
        {
            snprintf(logstring,
                     10000-1,
                    "HRD2 %2.2f %g %g %g %g %g %g %g %g %d %g %g %g %g %g %d %g %g %g %g %g",
                    logt,
                    dtp,
                    fill,
                    pf,p_burning,
                    m,m_burning,
                    L,L_burning,

                    /* star 1 */
                    star1->stellar_type,
                    Teff_from_star_struct(star1),
                    log10(star1->luminosity),
                    logg(star1),
                    logLeff(star1),
                    L1*pf,

                    /* star 2 */
                    star2->stellar_type,
                    Teff_from_star_struct(star2),
                    log10(star2->luminosity),
                    logg(star2),
                    logLeff(star2),
                    L2*pf
                );
        }
        else
        {
            snprintf(logstring,
                    10000-1,
                    "HRD2 %2.2f %g %g %g %g %g %g %g %g %d %g %g %g %g %g",
                    logt,
                    dtp,
                    fill,
                    pf,p_burning,
                    m,m_burning,
                    L,L_burning,

                    /* star 1 */
                    star1->stellar_type,
                    Teff_from_star_struct(star1),
                    log10(star1->luminosity),
                    logg(star1),
                    logLeff(star1),
                    L1*pf
                );
        }

        Printf("%s",logstring);

#if defined NUCSYN && defined HRDIAG_EXTRA_RESOLUTION_AT_END_AGB
    snprintf(logstring,
                10000-1,
                " %g %g %g %g %g %g",
        star1->Xenv[XC12],
        star1->Xenv[XC13],
        star1->Xenv[XN14],
        nucsyn_square_multibracket(star1->Xenv,
                       stardata->common.Xsolar,
                       (Isotope[]){XC12,XC13},2,
                       (Isotope[]){XN14,XN15},2),
        stardata->model.time,
        star1->mass
        );
        Printf(" %s",logstring);
#endif
    Printf("\n");

    }

    Safe_free(logstring);
}

static double rebin_start(double x,double dx)
{
    /* bin x to nearest dx */
    if(x>0.0)
    {
        x = (((int)(x/dx+0.5))*dx);
    }
    else if(x<0.0)
    {
        x = (((int)(x/dx-0.5))*dx);
    }
    else
    {
        x=0.0;
    }

    return x;
}

/* static double rebin(double x,double dx) */
/* { */
/*     /\* bin x to nearest dx, return the middle of the bin *\/ */
/*     return ((x>0.0?1.0:-1.0)*0.5+(int)(x/dx))*dx; */
/* } */

#endif
