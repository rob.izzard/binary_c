#include "../binary_c.h"
No_empty_translation_unit_warning;

#if defined NUCSYN && defined SDB_CHECKS

/*
 * Handy macro:
 * use as MATCHOBS(X,A,B)
 * then returns TRUE if X is in A+/-B
 * FALSE otherwise
 */
#define MATCHOBS(X,A,B) (fabs((X)-(A))<(B))

void sdb_check(struct stardata_t * Restrict const stardata)
{

    Star_number k;
    double t1,t2;
    Boolean BI_Lyn;
    /* Function to check for sdB stars */

#ifdef BI_LYN
    /* Code to check for BI Lyn type systems */
    Starloop(k)
    {
        /* check star k as the "primary" (i.e. the luminous star) */
        /* What do we know about BI Lyn? */

        BI_Lyn=FALSE; /* first, we're not a BI Lyn star */

        /*
         * temperature of star k is 28600+/-1000
         * temperature of star Other_star(k) is 5840+/-940
         */
        t1=Teff(k);
        t2=Teff(Other_star(k));

        if(MATCHOBS(t1,28600,1000)&&
           MATCHOBS(t2,5840,960))
        {

            /*
             * Now the helium abundance... should be
             * 5% H1 and 95% He4 for star k
             * 90% H1 and 10% He4 for star Other_star(k)
             * What tolerances? 10%... ?
             * These are BY NUMBER!
             * i.e.
             * X=0.013, Y=0.987
             * X=0.69, Y=0.31
             * allow X<0.03 and Y>0.95
             * and 0.6<X<0.8 (0.7+/-0.1) and 0.2<Y<0.4 (0.3+/-0.1)
             */
            if(
                /* Strict abundance checking */
                //(stardata->star[k].Xenv[XH1]<0.03)&&
                //(stardata->star[k].Xenv[XHe4]>0.95)&&

                /* Less strict - just specify that it's a helium star */
                NAKED_HELIUM_STAR(k)&&

                MATCHOBS(stardata->star[Other_star(k)].Xenv[XH1],0.7,0.1)&&
                MATCHOBS(stardata->star[Other_star(k)].Xenv[XHe4],0.3,0.1))
            {

                /* Now the mass, M1=0.5  (+/-? say 0.2)
                 * M2 is not well constrained so forget it
                 */

                if(MATCHOBS(stardata->star[k].mass,0.5,0.2))
                {
                    /*
                     * Now luminosity log l1=3.32+/-0.1 log l2 is not
                     * well constrained but is probably between 1.5
                     * and 2.5
                     */
                    if(MATCHOBS(log10(stardata->star[k].luminosity),3.32,0.1)&&
                       MATCHOBS(log10(stardata->star[Other_star(k)].luminosity),2.0,0.5))
                    {
                        /*
                         * We might want to match the rotation
                         * here, but that's VERY dependent on the
                         * common envelope evolution - so forget it
                         * (for now!)
                         */
                        BI_Lyn=TRUE;
                    }
                }
            }
        }
        if(BI_Lyn==TRUE)
        {
            printf("BILYN %d %d %d M=%3.3e %3.3e L=%3.3e %3.3e\n",BI_Lyn,
                   stardata->star[k].stellar_type,
                   stardata->star[Other_star(k)].stellar_type,
                   stardata->star[k].mass,
                   stardata->star[Other_star(k)].mass,
                   stardata->star[k].luminosity,
                   stardata->star[Other_star(k)].luminosity

                );
        }
    }


#endif

}

#endif
