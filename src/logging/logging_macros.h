#pragma once
#ifndef LOGGING_MACROS_H
#define LOGGING_MACROS_H

/*
 * Macros used in binary_c's logging functions
 */


#include "ansi_colours.h"

/*
 * Set, unset and append log strings
 * N = log number (>=0)
 */
#define Set_logstring(N,FORMAT,...)                 \
    {                                               \
        set_logstring((Log_index)(N),               \
                      stardata,                     \
                      FORMAT                        \
                      Comma_arglist(__VA_ARGS__));  \
    }
#define Append_logstring(N,FORMAT,...)                  \
    {                                                   \
        append_logstring((Log_index)(N),                \
                         stardata,                      \
                         FORMAT                         \
                         Comma_arglist(__VA_ARGS__));   \
    }

#define Unset_logstring(N)                      \
    {                                           \
        set_logstring((Log_index)(N),           \
                      stardata,                 \
                      NULL);                    \
    }

#define Append_logstring_debug1                                         \
    if(0)printf("Append logstring: len of %p (%s) is %d, add Max %d chars of \"%s\" to %p which is \"%s\"\n", \
                (void*)stardata->model.logflagstrings[(N)],             \
                stardata->model.logflagstrings[(N)],                    \
                istring,                                                \
                (STRING_LENGTH-2-istring),                              \
                cstring,                                                \
                (void*)stardata->model.logflagstrings[(N)],             \
                stardata->model.logflagstrings[(N)]);

#define Append_logstring_debug2                                 \
    if(0)printf("post-Append strlen %d\n",                      \
                strlen(stardata->model.logflagstrings[(N)]));

/*
 * Colours in file logs
 */

#if defined FILE_LOG && \
    defined FILE_LOG_COLOUR
#define File_log_colour(COLOUR)                             \
    (                                                       \
        (                                                   \
            stardata->preferences->colour_log == TRUE &&    \
            stardata->preferences->clean_log == FALSE       \
            ) ? (COLOUR) : ""                               \
        )

#else
#define File_log_colour(COLOUR) /* do nothing */
#endif

#endif // LOGGING_MACROS_H
