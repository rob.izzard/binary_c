#include "../binary_c.h"
No_empty_translation_unit_warning;


/*
 * Report a SN event in the log file: should not depend on NUCSYN
 * being defined.
 */
#if defined LOG_SUPERNOVAE && defined FILE_LOG
static const char snstrings[NUM_SN_STRINGS][SN_STRING_LENGTH]=SN_STRINGS;
#endif
#ifdef SUPERNOVA_COMPANION_LOG
//#define BIN_SUPERNOVA_DATA

static void sncomp_log(const struct star_t * star);
#endif//SUPERNOVA_COMPANION_LOG

void log_supernova(const Supernova sntype Maybe_unused,
                   const FILE * Restrict const fp Maybe_unused,
                   const Star_number k Maybe_unused,
                   const double dm Maybe_unused,
                   const double dm_co Maybe_unused,
                   struct stardata_t * Restrict const stardata Maybe_unused
    )
{
#ifdef LOG_SUPERNOVAE

#ifdef FILE_LOG
    if(fp!=NULL)
    {
        fprintf(fp,"SUPERNOVA star %d mass lost to ISM %g (%g from CO core) at time %g in a type %d (%s) progenitor mass %g\n",
                k,
                dm,
                dm_co,
                stardata->model.time,
                (int)sntype,
                snstrings[sntype],stardata->star[k].mass);
    }
#endif//FILE_LOG

#ifdef SUPERNOVA_COMPANION_LOG2
    stardata->star[0].went_sn_last_time=k;
    stardata->star[1].went_sn_last_time=sntype;
    return;
#endif//SUPERNOVA_COMPANION_LOG2

#ifdef SUPERNOVA_COMPANION_LOG

#ifdef SNCOMP_BINARIES_ONLY
    if(!(stardata->model.sgl))
#endif//SNCOMP_BINARIES_ONLY

    {

        if(stardata->tmpstore->sn_header_printed==FALSE)
        {
            // first time: header (prefix l=log10)
            printf("#SNCOMP prob sntype t {pre: lP la e} {post: lP la e} {ZAMS: la lP} {exploder: st m Mc MZAMS lL lR lg lTeff} {companion: st m Mc MZAMS lL lR lg lTeff}\n");
            stardata->tmpstore->sn_header_printed=TRUE;
        }
        /* log previous timestep (just before explosion) */
        struct stardata_t *prev = stardata->previous_stardata;

        printf("SNCOMP %g %d %g ",
               stardata->model.probability,
               sntype,
#ifdef BIN_SUPERNOVA_DATA
               bin_data(log10(prev->model.time),
                        SNCOMP_LOGTIME_RESOLUTION
                   )
#else
               log10(prev->model.time)
#endif // BIN_SUPERNOVA_DATA
            );

        if((prev->common.orbit.separation<=TINY)||
           (prev->common.orbit.eccentricity<-TINY))
        {
            /* disrupted binary or single star */
            printf("* * * ");
        }
        else
        {
            /* pre-SN period, separation, eccentricity */
            printf("{%g %g %g} ",
#ifdef BIN_SUPERNOVA_DATA
                   bin_data(log10(prev->common.orbit.period),SNCOMP_LOGPER_RESOLUTION),
                   bin_data(log10(prev->common.orbit.separation),SNCOMP_LOGSEP_RESOLUTION),
                   // force ecc > 0 to avoid binning (-0) (!)
                   bin_data(Max(TINY,prev->common.orbit.eccentricity),
                            SNCOMP_ECC_RESOLUTION)
#else
                   log10(prev->common.orbit.period),
                   log10(prev->common.orbit.separation),
                   prev->common.orbit.eccentricity
#endif // BIN_SUPERNOVA_DATA
                );

            /* post-SN period, separation, eccentricity */
            printf("{%g %g %g} ",
#ifdef BIN_SUPERNOVA_DATA
                   bin_data(log10(stardata->common.orbit.period),SNCOMP_LOGPER_RESOLUTION),
                   bin_data(log10(stardata->common.orbit.separation),SNCOMP_LOGSEP_RESOLUTION),
                   // force ecc > 0 to avoid binning (-0) (!)
                   bin_data(Max(TINY,stardata->common.orbit.eccentricity),
                            SNCOMP_ECC_RESOLUTION)
#else
                   log10(stardata->common.orbit.period),
                   log10(stardata->common.orbit.separation),
                   stardata->common.orbit.eccentricity
#endif // BIN_SUPERNOVA_DATA
                );
        }

        printf("%g %g ",
#ifdef BIN_SUPERNOVA_DATA
               bin_data(log10(stardata->common.zero_age.separation[0]),
                        SNCOMP_LOGSEP_RESOLUTION),
               bin_data(log10(stardata->common.zero_age.orbital_period[0]),
                        SNCOMP_LOGPER_RESOLUTION)
#else
               log10(stardata->common.zero_age.separation[0]),
               log10(stardata->common.zero_age.orbital_period[0])
#endif // BIN_SUPERNOVA_DATA
            );

        /* log exploding star */
        sncomp_log(&(prev->star[k]));
        /* log companion */
        sncomp_log(&(prev->star[Other_star(k)]));

        /* log companion */


        printf("\n");
    }


#endif // SUPERNOVA_COMPANION_LOG
#endif // LOG_SUPERNOVAE
}

#if defined(SUPERNOVA_COMPANION_LOG)&&defined(LOG_SUPERNOVAE)
static void sncomp_log(const struct star_t * star)
{
    double g=GRAVITATIONAL_CONSTANT * star->mass*M_SUN/Pow2(R_SUN*star->radius);
    printf("%d %g %g %g %g %g %g %g ",
           star->stellar_type,
#ifdef BIN_SUPERNOVA_DATA
           bin_data(star->mass,SNCOMP_MASS_RESOLUTION),
           bin_data(stardata->common.zero_age.mass[star->starnum],SNCOMP_MASS_RESOLUTION),
           bin_data(Outermost_core_mass(star),SNCOMP_MASS_RESOLUTION),
           bin_data(log10(star->luminosity),SNCOMP_LOGL_RESOLUTION),
           bin_data(log10(star->radius),SNCOMP_LOGR_RESOLUTION),
           bin_data(log10(g),SNCOMP_LOGG_RESOLUTION),
           bin_data(log10(Teff_from_star_struct(star)),SNCOMP_LOGTEFF_RESOLUTION)
#else
           star->mass,
           stardata->common.zero_age.mass[star->starnum],
           Outermost_core_mass(star),
           log10(star->luminosity),
           log10(star->radius),
           log10(g),
           log10(Teff_from_star_struct(star))
#endif
        );
}

#endif// SUPERNOVA_COMPANION_LOG
