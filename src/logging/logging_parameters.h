#pragma once
#ifndef LOG_PARAMETERS_H
#define LOG_PARAMETERS_H

#include "logging_parameters.def"

#undef X
#define X(CODE) INTERNAL_LOG_PARAMETERS_##CODE,
enum { INTERNAL_LOG_PARAMETERS_STATES_LIST };
#undef X

#define X(CODE) DIFFSTATS_##CODE,
enum { DIFFSTATS_INDEX_LIST };
#undef X

#include "log_labels.def"

#undef X
#define X(CODE, STRING) CODE##_LABEL ,
enum { LOG_LABELS_LIST };
#undef X

/* logging flags */
#include "log_flags.def"
#undef X
#define X(CODE) LOG_##CODE,
enum { LOG_FLAGS_LIST };

/* logging locks */
#include "log_locks.def"
#undef X
#define X(CODE) LOCK_##CODE,
enum { LOG_LOCKS_LIST };
#undef X


#endif // LOG_PARAMETERS_H
