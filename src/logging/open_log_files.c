#include "../binary_c.h"
No_empty_translation_unit_warning;
#include "logfile_macros.h"

/*
 * Function to open binary_c's log files
 */
void open_log_files(FILE *(*log_fp),
                    struct stardata_t * Restrict const stardata)
{
#ifdef FILE_LOG
#ifdef FILE_LOG_REWIND
    if(likely(*log_fp!=NULL &&
              *log_fp!=stdout &&
              *log_fp!=stderr))
    {
        /* log is already open, just rewind and truncate it */
        Dprint("rewind log_fp\n");
        rewind(*log_fp);
        const int x = ftruncate(fileno(*log_fp), 0);
        if(x!=0)
        {
            fprintf(stderr,"Warning: ftructate failed in open_log_files\n");
        }
    }
#else
    /* check for null log */
    if(*log_fp!=NULL)
    {
        fclose(*log_fp);
        fprintf(stderr,"CLOSE before OPEN\n");
        *log_fp=NULL;
    }
#endif
    char * header;
    const char * const logspace = stardata->preferences->log_arrows == TRUE ? "  " : " ";
    char * const logsep = stardata->preferences->log_separator;

    if(!asprintf(&header,
                 "%s        "
                 "TIME%s%s   "
                 "M1/%s%s%s  "
                 "M2/%s%s%s%*s%s%s%*s%s%s       "
                 "%*sSEP/%s%s       "
                 "PER%s%s   "
                 "ECC%s%s  "
                 "R1/ROL1%s%s "
                 "R2/ROL2%s%s  "
                 "TYPE%s%s%s",
                 File_log_colour(stardata->store->colours[WHITE]),

                 logspace, // time 4
                 logsep,

                 M_SOLAR, // M1 6
                 logspace,
                 logsep,

                 M_SOLAR, // M2 9
                 logspace,
                 logsep,

                 0 + (stardata->preferences->log_legacy_stellar_types == TRUE ? 2 : 5), // stellar type 1 (12)
                 stardata->preferences->log_legacy_stellar_types == TRUE ? "K1" : "st1",
                 logspace,
                 logsep,

                 1 + (stardata->preferences->log_legacy_stellar_types == TRUE ? 3 : 5), // stellar type 2 (16)
                 stardata->preferences->log_legacy_stellar_types == TRUE ? "K2" : "st2",
                 logspace,
                 logsep,

                 stardata->preferences->log_legacy_stellar_types == TRUE ? 1 : 0, // separation (20)
                 "",
                 R_SOLAR,
                 logsep,

                 logspace, // period (24)
                 logsep,

                 logspace, // eccentricity (26)
                 logsep,

                 logspace, // R1/RL1 (28)
                 logsep,

                 logspace, // R2/RL2 (30)
                 logsep,

                 logspace, // type (32)
                 logsep,

                 File_log_colour(stardata->store->colours[COLOUR_RESET])
           ))
    {
        Exit_binary_c(BINARY_C_FILE_OPEN_ERROR,
                      "could not construct log file header string\n");
    }

    if(unlikely(Strings_equal(stardata->preferences->log_filename,
                              "/dev/null")))
    {
        /* force a NULL pointer so we do nothing */
        *log_fp = NULL;
    }
    else if(unlikely(Strings_equal(stardata->preferences->log_filename,
                                   "/dev/stdout")))
    {
        /* redirect output to stdout */
        *log_fp = stdout;
        if(stardata->tmpstore &&
           stardata->tmpstore->c_log_prefix)
        {
            fprintf(*log_fp,"%s",stardata->tmpstore->c_log_prefix);
        }
        fprintf(*log_fp,"%s",header);
#ifdef CIRCUMBINARY_DISK_DERMINE
        fprintf(*log_fp,"     MCBDISK");
#endif
        fprintf(*log_fp,"%s\n",File_log_colour(stardata->store->colours[COLOUR_RESET]));
#ifdef FILE_LOG_FLUSH
        fflush(*log_fp);
#endif
    }
    else if(unlikely(Strings_equal(stardata->preferences->log_filename,
                                   "/dev/stderr")))
    {
        /* redirect output to stderr */
        *log_fp = stderr;
        if(stardata->tmpstore &&
           stardata->tmpstore->c_log_prefix)
        {
            fprintf(*log_fp,"%s",stardata->tmpstore->c_log_prefix);
        }
        fprintf(*log_fp,"%s",header);
#ifdef CIRCUMBINARY_DISK_DERMINE
        fprintf(*log_fp,"     MCBDISK");
#endif // CIRCUMBINARY_DISK_DERMINE
        fprintf(*log_fp,"%s\n",
                File_log_colour(stardata->store->colours[COLOUR_RESET])
            );
#ifdef FILE_LOG_FLUSH
        fflush(*log_fp);
#endif // FILE_LOG_FLUSH
        printf("done head\n");fflush(stdout);
    }
    else
    {
        /* open log and check for error */
        if(*log_fp==NULL &&
           unlikely((*log_fp=fopen(stardata->preferences->log_filename,"w"))==NULL) )
        {
            perror("open_log_files error on open log_fp");
            char errorstring[1024];
            if(strerror_r(errno,errorstring,1024)!=0)
            {
                fprintf(stderr,"%s\n",errorstring);
            }
            fflush(NULL);
            Exit_binary_c(BINARY_C_FILE_OPEN_ERROR,
                          "Warning: Can't open %s for FILE_LOG output, error number %d\n",
                          stardata->preferences->log_filename,
                          errno);
        }
        else
        {
            if(stardata->tmpstore &&
               stardata->tmpstore->c_log_prefix)
            {
                fprintf(*log_fp,"%s",stardata->tmpstore->c_log_prefix);
            }
            fprintf(*log_fp,"%s",header);
#ifdef CIRCUMBINARY_DISK_DERMINE
            fprintf(*log_fp,"     MCBDISK");
#endif // CIRCUMBINARY_DISK_DERMINE
#ifdef LOG_REPEAT_NUMBER
            if(stardata->preferences->repeat>1)
            {
                fprintf(*log_fp," %d/%d",
                        1+stardata->model.repeat_number,
                        stardata->preferences->repeat);
            }
#endif//LOG_REPEAT_NUMBER
            fprintf(*log_fp,"%s\n",File_log_colour(stardata->store->colours[COLOUR_RESET]));
#ifdef FILE_LOG_FLUSH
            fflush(*log_fp);
#endif // FILE_LOG_FLUSH
        }
    }

#if defined RANDOM_SYSTEMS &&                   \
    defined RANDOM_SYSTEMS_SHOW_ARGS
    if(stardata->preferences->random_systems &&
       *log_fp != NULL &&
       stardata->tmpstore != NULL&&
       stardata->tmpstore->random_system_argstring != NULL)
    {
        fprintf(*log_fp,
                "%s\n",
                stardata->tmpstore->random_system_argstring);
    }
#endif // RANDOM_SYSTEMS && RANDOM_SYSTEMS_SHOW_ARGS

    /*
     * Flush log_fp, or everything in debug versions
     */
#if DEBUG==1
    fflush(NULL);
#endif
    /*
     * flush log if told to on the cmd line
     */
    if(stardata->preferences->flush_log == TRUE)
    {
        fflush(*log_fp);
    }

    Safe_free(header);
#endif/*FILE_LOG*/
}
