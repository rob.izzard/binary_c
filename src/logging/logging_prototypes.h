#ifndef LOGGING_PROTOTYPES_H
#define LOGGING_PROTOTYPES_H
#include "../binary_c_macros.h"
#include <stdio.h>
#include "../binary_c_structures.h"
#include "../binary_c_parameters.h"
/** These are the output functions **/
void output_to_logfile(FILE * const fp,
                       const double time,
                       const double m1,
                       const double m2,
                       const Stellar_type stellaxr_type0,
                       const Boolean hybrid1,
                       const Stellar_type stellar_type1,
                       const Boolean hybrid2,
                       const double sep,
                       const double per,
                       const double ecc,
                       const double r1_rol, /* r1/roche_radius */
                       const double r2_rol, /* r2/roche_radius */
                       const char * const type,
                       struct stardata_t * Restrict const stardata,
                       const char * const leadchar
                      );


char * logfile_string(const double time,
                      const double m1,
                      const double m2,
                      const Stellar_type stellar_type0,
                      const Boolean hybrid1,
                      const Stellar_type stellar_type1,
                      const Boolean hybrid2,
                      const double sep,
                      const double per,
                      const double ecc,
                      const double r1_rol, /* r1/roche_radius */
                      const double r2_rol, /* r2/roche_radius */
                      const char * const type,
                      struct stardata_t * Restrict const stardata,
                      Boolean * const skip,
                      const char * const leadchar
                     );
void log_every_timestep(struct stardata_t * Restrict const stardata);
void open_log_files(FILE * (*log_fp),
                    struct stardata_t * Restrict const stardata);
void close_log_files(FILE * (*log_fp),
                     struct stardata_t * Restrict const stardata);


#ifdef LOG_SUPERNOVAE
void log_sn(struct stardata_t * Restrict const stardata,
            const Star_number star_number,
            Supernova sn_type,
            const int temporal_position);

#endif /* LOG_SUPERNOVAE */

#ifdef DETAILED_LOG
void detailed_log(struct stardata_t * Restrict const stardata);
#endif /* DETAILED_LOG */


void output_string_to_log(struct stardata_t * Restrict const stardata,
                          const char * Restrict const c);

#ifdef HRDIAG
void log_hr(struct stardata_t * Restrict const stardata);
#endif


#ifdef SDB_CHECKS
void sdb_check(struct stardata_t * Restrict const stardata);
#endif

void log_xray_binary(struct stardata_t * Restrict const stardata,
                     const Star_number ndonor,
                     const Star_number naccretor,
                     const double accretion_rate);


#ifdef LOG_COMENV_RSTARS
void log_r_star(struct star_t *star, struct stardata_t *stardata);
#endif

void stellar_type_log(struct stardata_t * Restrict const stardata);
void log_jje(struct stardata_t * Restrict const stardata);
void log_jl(struct stardata_t * Restrict const stardata);
void giant_abundance_log(struct stardata_t * Restrict stardata);
void log_supernova(const Supernova sntype,
                   const FILE * Restrict const fp,
                   const Star_number k,
                   const double dm,
                   const double dm_co,
                   struct stardata_t * Restrict const stardata);

void log_barium_stars(struct stardata_t * Restrict stardata);

#ifdef SUPERNOVA_COMPANION_LOG
void log_supernova_sc(struct stardata_t * Restrict stardata);
#endif
#ifdef DETAILED_COMPACT_LOG
void output_to_detailed_compact_logfile(struct stardata_t * stardata);
#endif

void logwrap(struct stardata_t * Restrict const  stardata,
             const char * Restrict const label,
             const char * const leadchar);

#ifdef WTTS_LOG
void wtts_log(struct stardata_t * Restrict const stardata,
              const double dt);
#endif

void append_logstring(const Log_index N,
                      struct stardata_t * const stardata,
                      char * const format,
                      ...) Gnu_format_args(3, 4);

void set_logstring(const Log_index N,
                   struct stardata_t * const stardata,
                   char * const format,
                   ...) Gnu_format_args(3, 4);

void show_derivatives(struct stardata_t * Restrict const stardata);
void unit_tests(struct stardata_t * const stardata);


void dump_stardata(struct stardata_t * const stardata,
                   const char * const filename) No_return;

void load_stardata(struct stardata_t * const stardata,
                   const char * const filename,
                   const Boolean overwrite_preferences);

#ifdef GAIAHRD
void gaia_log(struct stardata_t * const stardata,
              struct gaiaHRD_chunk_t * this);
#endif//GAIAHRD
void log_rotation_checks(struct star_t *star Maybe_unused);

#ifdef ORBITING_OBJECTS
void log_orbiting_objects(struct stardata_t * Restrict const stardata);
#endif // ORBITING_OBJECTS

double ** make_stellar_derivatives_array(struct stardata_t * Restrict const stardata);
void free_stellar_derivatives_array(double ** array);

void set_uuid(struct stardata_t * const stardata);

#endif/*LOGGING_PROTOTYPES_H*/
