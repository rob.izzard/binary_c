#pragma once
#ifndef LOGFILE_MACROS_H
#define LOGFILE_MACROS_H

#define NANCHECKL(A)                                                    \
    if(isnan(A)!=0)                                                     \
    {                                                                   \
        Backtrace;                                                      \
        Exit_binary_c(BINARY_C_EXIT_NAN,"NaN in logfile at %s\n",Stringof(A)); \
    }

#define LOG_APPEND(S)                               \
    {                                               \
        char * tmp;                                 \
        if(asprintf(&tmp,"%s %s",logstring,(S))>0)  \
        {                                           \
            Safe_free(logstring);                   \
            logstring = tmp;                        \
        }                                           \
    }

/* save previous logging type */
#define prevlabel (stardata->common.file_log_prevlabel)
#define prev_rlof_exit (stardata->common.file_log_prev_rlof_exit)
#define cached (stardata->common.file_log_cached)
#define n_rlof (stardata->common.file_log_n_rlof)
#define prevlogstring (stardata->tmpstore->file_log_prevstring)

#define Log_RLOF_colour(X)                          \
    File_log_colour((X) < 0.8 ? stardata->store->colours[GREEN] :             \
                    (X) < 0.9 ? stardata->store->colours[YELLOW] :            \
                    (X) < 0.999 ? stardata->store->colours[BRIGHT_YELLOW] :   \
                    stardata->store->colours[BRIGHT_RED])

#endif // LOGFILE_MACROS_H
