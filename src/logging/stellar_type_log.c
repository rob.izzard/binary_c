#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef STELLAR_TYPE_LOG
void stellar_type_log(struct stardata_t * Restrict const stardata)
{

    printf("STELLARTYPES %d %d %g %g\n",
           stardata->star[0].stellar_type,
           stardata->star[1].stellar_type,
           stardata->model.dtm
           *stardata->model.probability,
           stardata->model.time);


}
#endif
