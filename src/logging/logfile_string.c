#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "logfile_macros.h"

/*
 * Make the string used in (BSE style) log file output,
 * return the string.
 */

char * logfile_string(const double time,
                      const double m1,
                      const double m2,
                      const Stellar_type stellar_type0,
                      const Boolean hybrid1,
                      const Stellar_type stellar_type1,
                      const Boolean hybrid2,
                      const double sep,
                      const double per,
                      const double ecc,
                      const double r1_rol, /* r1/roche_radius */
                      const double r2_rol, /* r2/roche_radius */
                      const char * const label,
                      struct stardata_t * Restrict const stardata,
                      Boolean * const skip,
                      const char * const leadchar
    )
{
    char * logstring = NULL;
    char *c1 = NULL, *c2 = NULL;
    char *up = NULL, *down = NULL;
#ifdef CGI_EXTENSIONS
    char * cgistring;
    char * cgistring_star[NUMBER_OF_STARS];
    unsigned int j;
#endif /* CGI_EXTENSIONS */

#define _Free_tmps                              \
    {                                           \
        Safe_free(c1);                          \
        Safe_free(c2);                          \
        Safe_free(up);                          \
        Safe_free(down);                        \
    }

#define _Free_and_return                        \
    {                                           \
        _Free_tmps;                             \
        Safe_free(logstring);                   \
        return NULL;                            \
    }

    /*
     * stellar type strings
     */
#ifdef NANCHECKS
    NANCHECKL(time);
    NANCHECKL(m1);
    NANCHECKL(m2);
    NANCHECKL(ecc);
#endif // NANCHECKS
    if(stardata->preferences->log_legacy_stellar_types == TRUE)
    {
        if(asprintf(&c1," %2d%s",stellar_type0,
                    stardata->star[0].TZO == TRUE ? "†" :
                    hybrid1 ? "*" : " ")==0 ||
           asprintf(&c2," %2d%s",stellar_type1,
                    stardata->star[1].TZO == TRUE ? "†" :
                    hybrid2 ? "*" : " ")==0)
        {
            _Free_and_return;
        }
    }
    else
    {
        if(asprintf(&c1," %5s%s",
                    Short_stellar_type_string_from_star(&stardata->star[0]),
                    hybrid1 ? "*" : " ")==0 ||
           asprintf(&c2," %5s%s",
                    Short_stellar_type_string_from_star(&stardata->star[1]),
                    hybrid2 ? "*" : " ")==0)
        {
            _Free_and_return;
        }
    }

    *skip = FALSE;

    if(strncmp(label,"INITIAL",7)==0)
    {
        prev_rlof_exit = 0;
        n_rlof = 0;
        cached = FALSE;
    }

    /*
     * Orbital period with, perhaps, units
     * (in the code, the unit is years).
     *
     * First convert to seconds.
     */
    double output_period;
    char output_period_unit[3];
#undef X
#define X(LONG_STRING,SHORT_STRING,MULTIPLIER,...) MULTIPLIER,
    static const double period_unit_multipliers[] = { 1.0,TIME_UNIT_LIST };
#undef X
#define X(LONG_STRING,SHORT_STRING,MULTIPLIER,...) SHORT_STRING,
    static const char * period_unit_strings_short[] = { "auto",TIME_UNIT_LIST };
#undef X
#define X(LONG_STRING,SHORT_STRING,MULTIPLIER,...) LONG_STRING,
    static const char * period_unit_strings_long[] = { "auto",TIME_UNIT_LIST };
    static const int n_period_options = sizeof(period_unit_strings_short)/sizeof(period_unit_strings_short[0]);
#undef X

    if(stardata->preferences->log_period_unit == 0)
    {
        /*
         * auto unit choice
         */
        output_period =
            /* otherwise, choose */
            per < DAY_LENGTH_IN_YEARS ? (per*YEAR_LENGTH_IN_HOURS) : // hours
            per < 1.0 ? (per*YEAR_LENGTH_IN_DAYS) : // days
            per < 1e3 ? per : // years
            per < 1e6 ? (per*1e-3) : // kiloyears
            per < 1e9 ? (per*1e-6) : // Megayears
            (per * 1e-9); // Gigayears
        char * _unit =  per < DAY_LENGTH_IN_YEARS ? "_h" :
            per < 1.0 ? "_d" :
            per < 1e3 ? "_y" :
            per < 1e6 ? "ky" :
            per < 1e9 ? "My" :
            "Gy";
        output_period_unit[0] = _unit[0];
        output_period_unit[1] = _unit[1];
    }
    else
    {
        /* chosen unit explicitly */
        const int n =
            Limit_range(stardata->preferences->log_period_unit,
                        0,
                        n_period_options);
        output_period = per * YEAR_LENGTH_IN_SECONDS /
            period_unit_multipliers[n];
        if(period_unit_strings_long[n][0] == 'd')
        {
            output_period_unit[0] = period_unit_strings_short[n][0];
            output_period_unit[1] = ' ';
        }
        else
        {
            output_period_unit[0] = period_unit_strings_short[n][0];
            output_period_unit[1] = period_unit_strings_short[n][1];
        }
    }

    if(stardata->preferences->clean_log == TRUE)
    {
        output_period_unit[0] = ' ';
        output_period_unit[1] = ' ';
    }
    output_period_unit[2] = '\0';

    const char * const nochange = stardata->store->nochangechar;
    if(asprintf(&up,"%s%s",stardata->store->colours[GREEN],stardata->store->upchar)==0 ||
       asprintf(&down,"%s%s",stardata->store->colours[RED],stardata->store->downchar)==0)
    {
        _Free_and_return;
    }


#define _Rising_or_falling(NOW,PREV) (                                  \
        (stardata->preferences->clean_log == FALSE && \
         stardata->preferences->log_arrows == TRUE) ?                   \
        (                                                               \
            (stardata->previous_stardata==NULL||stardata->model.model_number==0) ? " " : \
            Fequal((NOW),(PREV)) ? nochange                             \
            : (NOW) > (PREV) ? up : down                                \
            ) : ""                                                      \
        )

#define Rising_or_falling(VAR)                              \
    _Rising_or_falling(stardata->VAR,                       \
                       stardata->previous_stardata->VAR)
#define Rising_or_falling_ratio(VAR1,VAR2)                  \
    _Rising_or_falling(stardata->VAR1/stardata->VAR2,       \
                       stardata->previous_stardata->VAR1/   \
                       stardata->previous_stardata->VAR2)
    char * const separator = stardata->preferences->log_separator;

    if(asprintf(&logstring,
                "%s"
                "%s %11.4lf%s" // time
                "%s%9.3lf%s%s" // M1
                "%s%9.3lf%s%s" // M2
                "%s%3s%s%s" // stellar type 1
                "%s%3s%s%s" // stellar type 2
                "%s%13.5g%s%s" // separation
                "%s%9.3g%s%s%s%s" // orbital period
                "%6.2lf%s%s%s" // eccentricity
                "%s%s%8.3lf%s%s" // R1/RL1
                "%s%s%8.3lf%s%s" // R2/RL2
#ifdef TRIPLE
                "%12.7f " // inclination
                "%12.7f " // longitude of ascending node
                "%12.7f " // argument of periapsis
#endif // TRIPLE
                "  %s%s%s", // label

                (stardata->preferences->clean_log == FALSE && leadchar) ? leadchar : " ",

                File_log_colour(stardata->store->colours[ORANGE]),
                time,
                separator,

                File_log_colour(stardata->store->colours[RED]),
                m1,
                Rising_or_falling(star[0].mass),
                separator,

                File_log_colour(stardata->store->colours[RED]),
                m2,
                Rising_or_falling(star[1].mass),
                separator,

                File_log_colour(stardata->store->colours[WHITE]),
                c1,
                Rising_or_falling(star[0].stellar_type),
                separator,

                File_log_colour(stardata->store->colours[WHITE]),
                c2,
                Rising_or_falling(star[1].stellar_type),
                separator,

                File_log_colour(stardata->store->colours[YELLOW]),
                (NEITHER_STAR_MASSLESS ? sep : -1.0),
                Rising_or_falling(common.orbit.separation),
                separator,

                File_log_colour(stardata->store->colours[YELLOW]),

                /* period */
                (NEITHER_STAR_MASSLESS && sep > 0.0) ? output_period      : -1.0,
                (NEITHER_STAR_MASSLESS && sep > 0.0) ? output_period_unit : "  ",

                Rising_or_falling(common.orbit.period),
                File_log_colour(stardata->store->colours[YELLOW]),
                separator,

                (NEITHER_STAR_MASSLESS ? ecc : -1.0),
                Rising_or_falling(common.orbit.eccentricity),
                File_log_colour(stardata->store->colours[YELLOW]),
                separator,

                File_log_colour(r1_rol < 1.0 ? stardata->store->colours[COLOUR_RESET] : stardata->store->colours[UNDERLINE]),
                Log_RLOF_colour(r1_rol),
                (NEITHER_STAR_MASSLESS ? Min(999.0,r1_rol) : 0.0),
                Rising_or_falling_ratio(star[0].radius,star[0].roche_radius),
                separator,

                File_log_colour(r2_rol < 1.0 ? stardata->store->colours[COLOUR_RESET] : stardata->store->colours[UNDERLINE]),
                Log_RLOF_colour(r2_rol),
                (NEITHER_STAR_MASSLESS ? Min(999.0,r2_rol) : 0.0),
                Rising_or_falling_ratio(star[1].radius,star[1].roche_radius),
                separator,

#ifdef TRIPLE
                stardata->common.orbit.inclination,
                separator,

                stardata->common.orbit.longitude_of_ascending_node,
                separator,

                stardata->common.orbit.argument_of_periapsis,
                separator,
#endif // TRIPLE

                File_log_colour(stardata->store->colours[CYAN]),
                label,
                File_log_colour(stardata->store->colours[COLOUR_RESET])

           )>0){}

#ifdef CIRCUMBINARY_DISK_DERMINE
    {
        char * cb_log ;
        if(asprintf(cb_log,
                    "%s%3.3e ",
                    separator,
                    stardata->common.m_circumbinary_disk) >=0)
        {
            LOG_APPEND(cb_log);
        }
    }
#endif

#ifdef CGI_EXTENSIONS
    /*
     * extra output for CGI script:
     */

    /* effective temperatures */


    /* chemistry for each star */
    /* X,Y,C,N,O,Fe,Ba as mass fractions for each star */
    Number_of_stars_Starloop(j)
    {
#ifdef NUCSYN
        Abundance * const X = nucsyn_observed_surface_abundances(&stardata->star[j]);
        if(asprintf(&cgistring_star[j],
                    "%3.3e %3.3e %3.3e %3.3e %3.3e %3.3e %3.3e",
                    X[XH1],X[XHe4],X[XC12],X[XN14],X[XO16],X[XFe56],
                    nucsyn_elemental_abundance("Ba",X,stardata,stardata->store))==0)
        {
        }
#else
        cgistring_star[j] = Malloc(sizeof(char));
        cgistring_star[j][0] = '\0';
#endif //NUCSYN
    }
    if(asprintf(&cgistring,
                "%3.3e %3.3e %s %s",
                Teff(0),
                Teff(1),
                cgistring_star[0],
                cgistring_star[1]
           )>0)
    {
        LOG_APPEND(cgistring);
    }
    Number_of_stars_Starloop(j)
    {
        Safe_free(cgistring_star[j]);
    }
    Safe_free(cgistring);
#endif /* CGI_EXTENSIONS */

    /* look for rapid END_RCHE,BEG_RCHE : pretend it didn't happen */
    if(stardata->common.file_log_prevlabel &&
       (strncmp(label,"BEG_RCHE",8)==0)&&
       (strncmp(stardata->common.file_log_prevlabel,"END_RCHE",8)==0)&&
       (stardata->model.time - prev_rlof_exit < 0.01))
    {
        *skip = TRUE;
        n_rlof++;
    }
    /* for END_RCHE, always skip (is saved in prevlogstring) */
    else if(strncmp(label,"END_RCHE",8)==0)
    {
        cached = TRUE;
        *skip = TRUE;
    }
    /* anything but rapid BEG_RCHE, END_RCHE pair, or END_RCHE,
     * flush any cache */
    else if(cached==TRUE)
    {
        if(prevlogstring != NULL &&
           (strncmp(stardata->common.file_log_prevlabel,"BEG_RCHE",8)==0)
           && (n_rlof>0))
        {
            prevlogstring[72]='O';
            prevlogstring[73]='s';
            prevlogstring[74]='c';
        }
        cached = FALSE;
        *skip = FALSE;
        n_rlof = 0;
        prev_rlof_exit = 0.0;
    }

    /* save previous exit time for END_RCHE */
    if(strncmp(label,"END_RCHE",8)==0)
    {
        prev_rlof_exit = stardata->model.time;
    }

#ifndef CGI_EXTENSIONS
#undef strcpy
    stardata->common.file_log_prevlabel = (char*)label;
    Safe_free(prevlogstring);
    prevlogstring = Malloc(sizeof(char)*(1+strlen(logstring)));
    strcpy(prevlogstring,
           logstring);
#endif //CGI_EXTENSIONS

    Safe_free(up);
    Safe_free(down);
    Safe_free(c1);
    Safe_free(c2);

    return logstring;
}
