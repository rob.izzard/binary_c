#include "../binary_c.h"
No_empty_translation_unit_warning;

#if defined NUCSYN &&                           \
    defined LOG_BARIUM_STARS

void log_barium_stars(struct stardata_t * Restrict stardata)
{
    struct star_t * star;
    Star_number k;
    double dt;

    if(Fequal(0.0,stardata->model.time))
    {
        stardata->common.barium_prevt=0.0;
        dt=0.0;
    }
    else
    {
        dt=stardata->model.time - stardata->common.barium_prevt;
        stardata->common.barium_prevt= stardata->model.time;
    }
    double * XZAMS = stardata->preferences->zero_age.XZAMS;

    if(dt>TINY)
    {
        Foreach_star(star)
        {
            /* first select giants */
            if((star->stellar_type>MAIN_SEQUENCE)&&
               (star->stellar_type<=TPAGB)&&
               // avoid postAGB stars which pass through the
               // appropriate spectral type
               ((star->core_mass[CORE_He]>0)&&(star->mass - star->core_mass[CORE_He] > 0.05))
                )
            {
                /* then select either barium stars ... */
                int stype=spectral_type(stardata,star);

                if(stype==SPECTRAL_TYPE_G || stype==SPECTRAL_TYPE_K)
                {
                    /* get surface abundances */
                    double * const X = nucsyn_observed_surface_abundances(star);

                    /* calculate [Ba/Fe] */
                    const bafe = nucsyn_elemental_square_bracket("Ba","Fe",X,XZAMS,stardata);

                    if(bafe>0.5)
                    {
                        Printf("BARIUMstrong");
                    }
                    else if(bafe>0.2)
                    {
                        Printf("BARIUMmild");
                    }
                    else
                    {
                        Printf("GK");
                    }
                    Printf("_GIANT %g %g %g %g %g %g %d %g %g\n",
                           stardata->model.time,//1
                           dt,//2
                           stardata->model.probability,//3
                           stardata->common.orbit.period*YEAR_LENGTH_IN_DAYS,//4
                           stardata->common.orbit.eccentricity,//5
                           bafe,//6
                           star->stellar_type,//7
                           star->mass, //8
                           Teff(k) // 9
                        );

                }
            }
        }
    }
}
#endif // LOG_BARIUM_STARS && NUCSYN
