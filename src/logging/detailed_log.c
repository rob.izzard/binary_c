#include "../binary_c.h"

No_empty_translation_unit_warning;


#ifdef DETAILED_LOG

/*
 * Function to output a detailed log in a form that GNUPLOT (or another similar
 * plotting program) can understand
 */

/*
 * Some useful macros
 */
/* OMEGA - something to define the spin with sensible units */
/*
 * omega is in units of rad yr^-1, so use PI and YEAR_LENGTH_IN_SECONDS to
 * convert to rad s-1
 */
#define OMEGA(A) (stardata->star[A].omega/(YEAR_LENGTH_IN_SECONDS))
#define OMEGA1(A) (stardata->star[A].omega1/(YEAR_LENGTH_IN_SECONDS))
#define OMEGA2(A) (stardata->star[A].omega2/(YEAR_LENGTH_IN_SECONDS))
#define JSPIN(A) (stardata->star[A].jspin)
#define JSPIN1(A) (stardata->star[A].jspin1)
#define JSPIN2(A) (stardata->star[A].jspin2)

/* Define the strings here */

/* The header is something to guide the eye */
#define LOG_HEADER_STRING "!%9s %5s %5s %10s %10s %10s %10s %10s %10s %10s %10s %10s %10s %10s %10s %10s %10s\n"


#define LOG_HEADER "t",                         \
        "K1",                                   \
        "K2",                                   \
        "M1",                                   \
        "M2",                                   \
        "CoreMass1",                            \
        "CoreMass2",                            \
        "logRadius1",                           \
        "logRadius2",                           \
        "r1/rol1",                              \
        "r2/rol2",                              \
        "logLum1",                              \
        "logLum2",                              \
        "omega1",                               \
        "omega2",                               \
        "sep",                                  \
        "ecc"

/* The string contains the formatting */
#define LOG_STRING "% 6.3e %5d %5d % 6.3e % 6.3e % 6.3e % 6.3e % 6.3e % 6.3e % 6.3e % 6.3e % 6.3e % 6.3e % 6.3e % 6.3e % 6.3e % 6.3e\n"


/* These are the things that go in the string */
#define LOG_ARGS stardata->model.time,                              \
        stardata->star[0].stellar_type,                             \
        stardata->star[1].stellar_type,                             \
        stardata->star[0].mass,                                     \
        stardata->star[1].mass,                                     \
        Outermost_core_mass(stardata->star[0]),                     \
        Outermost_core_mass(stardata->star[1]),                     \
        log10(stardata->star[0].radius),                            \
        log10(stardata->star[1].radius),                            \
        stardata->star[0].radius/stardata->star[0].roche_radius,    \
        stardata->star[1].radius/stardata->star[1].roche_radius,    \
        log10(stardata->star[0].luminosity),                        \
        log10(stardata->star[1].luminosity),                        \
        stardata->star[0].omega,                                    \
        stardata->star[1].omega,                                    \
        stardata->common.orbit.separation,                          \
        stardata->common.orbit.eccentricity

void detailed_log(struct stardata_t * Restrict const stardata)
{
#define fp (stardata->common.detailed_log_fp)
#define count (stardata->common.detailed_log_count)

    if(count==0)
    {
        /* First time, open the log */
        if((fp=fopen(DETAILED_OUTPUT_FILENAME,"w"))==NULL)
        {
            /* Failed to open the log  */
            Exit_binary_c(BINARY_C_DETAILED_LOG_OPEN_FAILED,
                          "Could not open detailed log in detailed_log.c");
        }
        else
        {
            printf("detailed open ok\n");
        }
        fprintf(fp,LOG_HEADER_STRING,LOG_HEADER);
        count++;
    }
    else
    {
        /* If stardata is NULL then plot a blank line for gnuplot */
        if(stardata!=NULL)
        {
            fprintf(fp,LOG_STRING,LOG_ARGS);
        }
        else
        {
            fprintf(fp,"\n");
        }
    }
    fflush(fp);
}
#endif /* DETAILED_LOG */
