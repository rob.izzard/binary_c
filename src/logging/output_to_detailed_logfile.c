#include "../binary_c.h"
No_empty_translation_unit_warning;

/*
 * An attempt to make a detailed, yet compact, log output format
 * for debugging of individual systems.
 *
 * Enabled if DETAILED_COMPACT_LOG is defined
 */

#ifdef DETAILED_COMPACT_LOG

#define LOGSTRINGLENGTH 2048
#define LOGSTAR(A)                                                      \
    if(stardata->star[(A)].stellar_type<15)                             \
    {                                                                   \
    printf("%s% 3d% 6.2f% 6.2f % 4.1f % 4.1f %s% 5.2f",                 \
           (A)==0 ? stardata->store->colours[RED] : stardata->store->colours[YELLOW] ,                                      \
           stardata->star[(A)].stellar_type,                            \
           Min(99.99,stardata->star[(A)].mass),                         \
           Min(99.99,Outermost_core_mass(stardata->star[(A)]),          \
               log10(stardata->star[(A)].radius),                       \
               log10(stardata->star[(A)].luminosity),                   \
               (char*)(stardata->star[(A)].radius/stardata->star[(A)].roche_radius>0.99 ? ((A)==0 ? stardata->store->colours[BRIGHT_RED] : stardata->store->colours[BRIGHT_YELLOW]) : stardata->store->colours[COLOUR_RESET]) , \
               stardata->star[(A)].radius/stardata->star[(A)].roche_radius); \
           }                                                            \
    else                                                                \
    {                                                                   \
        printf("%s  *     *     *    *    *     * ",(A)==0 ? stardata->store->colours[RED] : stardata->store->colours[YELLOW]); \
    }

#define LOGSPEC(A,B) if(stardata->model.(A)) printf("%s ",(B));

void output_to_detailed_compact_logfile(struct stardata_t * stardata)
{
#define count (stardata->common.detailed_compact_log_count)
    int k;
    if(Is_zero(stardata->model.time)) count=0;
    if((count++)%10==0)
    {
        printf("DETLOG____Time ___dt logP loga _ecc st _Mass ___Mc logR logL _R/RL st _Mass ___Mc logR logL _R/RL");
        printf(" ___veq/vk ");
        printf(" _Jorb __Iorb ____J1 ____I1 ____J2 ____I2\n");
    }

    printf("DETLOG % 8.1f % 4.1f ",stardata->model.time,log10(stardata->model.dt));

    /* binary information */
    if(stardata->model.sgl==FALSE)
    {
        printf("% 4.1f % 4.1f % 4.1f",
               log10(stardata->common.orbit.period),
               Max(0.0,log10(stardata->common.orbit.separation)),
               stardata->common.orbit.eccentricity
            );
    }
    else
    {
        printf("   *    *    *");
    }

    /* stellar details */
    LOGSTAR(1);
    LOGSTAR(2);
    printf(" ");

    /* rotational velocity as a fraction of the critical velocity */
    {
        double v;
        Starloop(k)
        {
            if(stardata->star[k].stellar_type<15)
            {
                v=Min(9.99,stardata->star[k].v_eq/stardata->star[k].v_crit_eq);
                printf("%s%.2f ", v>0.99 ? stardata->store->colours[BRIGHT_GREEN] : stardata->store->colours[GREEN],v);
            }
            else
            {
                printf("%s  * ",stardata->store->colours[COLOUR_RESET]);
            }
        }
    }


    /* angular momenta, moments of inertia, Darwin instability */

    double I[NUMBER_OF_STARS];
    I[1]=(M_SUN*R_SUN*R_SUN)*moment_of_inertia(&(stardata->star[0]),stardata->star[0].radius);
    I[2]=(M_SUN*R_SUN*R_SUN)*moment_of_inertia(&(stardata->star[1]),stardata->star[1].radius);

    if(stardata->model.sgl==FALSE)
    {
        // binaries only
        double Iorb = (1.0/3.0) * M_SUN*stardata->star[0].mass*stardata->star[1].mass/(stardata->star[0].mass + stardata->star[1].mass) * Pow2(stardata->common.orbit.separation*R_SUN);
        printf("%s% 6.2f %6.2f ", Iorb>3.0*(I[1]+I[2]) || stardata->model.sgl ? stardata->store->colours[MAGENTA] : stardata->store->colours[BRIGHT_MAGENTA],
               log10(stardata->common.orbit.angular_momentum*ANGULAR_MOMENTUM_CGS),
               log10(Iorb));
    }
    else
    {
        printf("%s     *      * ",stardata->store->colours[MAGENTA]);
    }

    Starloop(k)
    {
        if(stardata->star[k].stellar_type<15)
        {
            printf("% 6.2f % 6.2f ",log10(stardata->star[0].jspin*ANGULAR_MOMENTUM_CGS),log10(I[k]));
        }
        else
        {
            printf("     *      * ");
        }
    }

    /* messages */
    printf("%s",stardata->store->colours[BRIGHT_CYAN]);
    if(stardata->model.in_RLOF)
    {
#define ALMOST1 (1.0-1e-10)
        if(stardata->star[0].radius >  ALMOST1 * stardata->star[0].roche_radius)
        {
            if(stardata->star[1].radius >  ALMOST1 * stardata->star[1].roche_radius)
            {
                printf("Contact ");
            }
            else
            {
                printf("RLOF1>2(%s) ", stardata->model.disk==TRUE ? "AccDisk" : "Stream");
            }
        }
        else
        {
            printf("RLOF2>1(%s) ", stardata->model.disk==TRUE ? "AccDisk" : "Stream");
        }
    }

    LOGSPEC(supernova,"SN");
    LOGSPEC(novae,"Novae");
    LOGSPEC(coalesce,"Merger");

    printf("%s\n",stardata->store->colours[COLOUR_RESET]);
}

#endif// DETAILED_COMPACT_LOG
