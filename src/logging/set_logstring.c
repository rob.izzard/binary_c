#include "../binary_c.h"
No_empty_translation_unit_warning;

#include <stdarg.h>

Gnu_format_args(3,4)
void set_logstring(const Log_index N,
                   struct stardata_t * const stardata,
                   char * const format,
                   ...)
{
    va_list args;
    va_start(args, format);
    const Boolean wasset = stardata->model.logflags[N];
    stardata->model.logflags[N]=TRUE;

    if(format == NULL)
    {
        /*
         * unset logstring
         */
        stardata->model.logflagstrings[N][0] = '\0';
        stardata->model.logflagstrings[N][1] = '\0';
        stardata->model.logflags[N] = FALSE;
        Log_index ilog = 0;
        while(ilog<LOG_NFLAGS)
        {
            if(stardata->model.logstack[ilog] == N)
            {
                stardata->model.logstack[ilog] = LOG_NONE;
            }
            ilog++;
        }
    }
    else
    {
        /*
         * Set a string in the log
         */
        vsnprintf(stardata->model.logflagstrings[N],
                  (STRING_LENGTH-1),
                  format,
                  args);

        /* set next free stack item to N if necessary */
        if(wasset == FALSE)
        {
            unsigned int ilog = 0;
            while(ilog<LOG_NFLAGS)
            {
                if(stardata->model.logstack[ilog]==LOG_NONE)
                {
                    if((ilog>0 &&
                        stardata->model.logstack[ilog-1]!=N) ||
                       ilog==0)
                    {
                        stardata->model.logstack[ilog] = N;
                        break;
                    }
                }
                ilog++;
            }
        }
    }

    va_end(args);
}
