#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef PADOVA_MC1TP
    /* Fits to the Padova group mc1tp below M=3 */
    double mbreak;
    if(m<3.0)
    {
        mbreak=(1.67600e+00)+(3.53260e+01)*z+(-9.55460e+02)*z*z;
        if(m<mbreak)
        {
            y=((4.28890e-01)+(1.91850e-01)*m+(-7.47420e-02)*m*m)*(1+(-5.01430e+00)*z+(1.70800e+02)*z*z);
        }
        else
        {
            y=((1.93420e-01)+(1.39170e-02)*m+(1.81220e-02)*m*m)*(1+(-1.63000e-01)*log10(z)+(-1.55370e-01)*log10(z));
        }

        return(y);
    }
#endif

