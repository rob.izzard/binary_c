#include "../binary_c.h"
No_empty_translation_unit_warning;


double Pure_function lmcgbf(const double mc,
                            const double * Restrict GB)
{
    double result;

    /*
     * A function to evaluate L given Mc for GB, AGB and NHe stars
     */
    if(mc < GB[GB_Mx])
    {
        result = GB[GB_D]*pow(mc,GB[GB_p]);
    }
    else
    {
        result = GB[GB_B]*pow(mc,GB[GB_q]);
    }
    /*printf("LMCGBF(mc=%g) : Min(Bq:%g, Dp:%g) = %g (Mx = %g)\n",
           mc,
           GB[GB_B]*pow(mc,GB[GB_q]),
           GB[GB_D]*pow(mc,GB[GB_p]),
           result,
           GB[GB_Mx]
        );
    */
    return result;
}
/***********************************************************/
