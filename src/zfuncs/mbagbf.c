#include "../binary_c.h"
No_empty_translation_unit_warning;


double Pure_function mbagbf(const double mc, /** mass of core **/
                            const double * Restrict const giant_branch_parameters)
{
    /*
     * A function to evaluate mass at the BAGB by inverting mcagbf.
     */
    const double mc4 = Pow4(mc);
    double result;
    if(mc4 > giant_branch_parameters[37])
    {
        result = pow(((mc4 - giant_branch_parameters[37])/
                      giant_branch_parameters[35]),
                     (1.0/giant_branch_parameters[36]));
    }
    else
    {
        result = 0.0;
    }

    return result;
}

/*****************************************************************/
