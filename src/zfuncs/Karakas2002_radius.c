#include "../binary_c.h"
No_empty_translation_unit_warning;


Constant_function double Karakas2002_radius_2param_m_part(struct stardata_t * Restrict const stardata,
                                                   const double m0,
                                                   double m,
                                                   double metallicity,
                                                   double l);

Constant_function double Karakas2002_radius_2param_m(struct stardata_t * Restrict const stardata,
                                              const double m0,
                                              const double m,
                                              const double metallicity,
                                              const double l)
{
    /*
     * We should interpolate the radius, not the coefficients!
     * So call the radius function for each fitted metallicity
     * and interpolate the result in metallicity (linearly, presumably)
     */
    Dprint("Karakas2002_radius from m0=%g m=%g metallicity=%g l=%g ... ",
           m,m,metallicity,l);
    double lowz,highz;

    if(More_or_equal(metallicity,0.008))
    {
        /* interpolate between Z=0.008 and Z=0.02 tables */
        lowz=0.008;
        highz=0.02;
    }
    else if(More_or_equal(metallicity,0.004))
    {
        /* interpolate between Z=0.004 and Z=0.008 tables */
        lowz=0.004;
        highz=0.008;
    }
    else
    {
        lowz=0.0001;
        highz=0.004;
    }

    /*
     * Fudge: make m0=M0MIN or larger: we have no fits below M~1.0 so it is
     * difficult to know what to do with these stars!
     */
#define M0MIN 0.5
    const double f=(metallicity-lowz)/(highz-lowz); // if z=lowz, f=0
    double radius;

    if(Fequal(f,0.0))
    {
        radius= (Karakas2002_radius_2param_m_part(stardata,Max(M0MIN,m0),m,lowz,l));
    }
    else if(Fequal(f,1.0))
    {
        radius= (Karakas2002_radius_2param_m_part(stardata,Max(M0MIN,m0),m,highz,l));
    }
    else
    {
        const double rlow=Karakas2002_radius_2param_m_part(stardata,Max(M0MIN,m0),m,lowz,l);
        const double rhigh=Karakas2002_radius_2param_m_part(stardata,Max(M0MIN,m0),m,highz,l);
        radius = (rlow*(1.0-f)+rhigh*f);
    }

    return radius;
}

double Karakas2002_radius_2param_m_part(struct stardata_t * Restrict stardata,
                                        const double m0,
                                        double m,
                                        double metallicity,
                                        double l)
{
    // 2-parameter table-based fit: envelope drop-off
    // modelled to total mass (relative to initial total mass)
    double r;
    /* Table-based fit */
    /* Fitting coefficients */
    /* coeffs array for each Z: M,a,b */


    /* interpolation coeffs */
    const double x[2]={metallicity,m0};
    double res[2];
    Interpolate(stardata->store->Karakas2002_radius,
                x,
                res,
                1);
    const double a = res[0], b = res[1];

    /* Convert menv/menv_1tp to log10 (menv/menv_1tp) and L to log10 L */
    l=log10(l);
    m=log10(m/m0);

    /* The fit is a 2-parameter fit */
    /* log10 R = r = a + b * log10 L -0.33 * log10 (M/M0) */
    r = a+b*l-0.33*m;
    /* cap to prevent divergence */
    r = Min(3.0,r);
    r = exp10(r);

#ifdef NANCHECKS
    if(isnan(r)!=0)
    {
        Exit_binary_c(BINARY_C_EXIT_NAN,
                      "Nan in TPAGB radius!\na=%g b=%g log10(l)=%g l=%g m=%g hence log10 r=%g\n",
                      a,b,l,exp10(l),m,a+b*l-0.33*m);
    }
#endif
    Dprint("R2PARAM %g\n",r);

    return(r);
}


double Karakas2002_ragb_teff(const double m,
                             const Abundance metallicity,
                             double l,
                             const double m0,
                             const double mc_1tp Maybe_unused,
                             const double menv Maybe_unused,
                             const double mcnodup Maybe_unused,
                             struct star_t * const star Maybe_unused,
                             struct stardata_t * const stardata)
{
    // 2-parameter fit
    double r = Karakas2002_radius_2param_m(stardata,m0,m,metallicity,l);
    Dprint("Karakas r(m0=%g,m=%g,Z=%g,L=%g) = %g\n",m0,m,metallicity,l,r);

#ifdef KARAKAS2002_SMOOTH_AGB_RADIUS_TRANSITION

    /* special phased transition from the old (jarrod) EAGB radius to the
     * amanda fit: should help with numerical stability (I hope)
     *
     * This should avoid sudden jumps...
     */
    {

        /* save terminal EAGB radius */
        if(star->stellar_type==EAGB) star->rTEAGB=star->radius;


        /* phase in over the first 10,000 years of the TPAGB from
         * the old radius to the new one
         */

        const double f=1.0e6*(star->age-TPAGB_start_time(star))/
            KARAKAS2002_SMOOTH_AGB_RADIUS_TRANSITION_SMOOTHING_TIME;

        f=Max(0.0,f);
        f=Min(1.0,f);

        const double r_old = star->rTEAGB;
        const double r_new = r;
        /* hence interpolate new radius */
        r= (1.0-f)*r_old  + f*r_new;

        /* Choose the max of the old and new values, do not want to shrink! */
        r=Max(r_old,r);

        Dprint("Phase in R (old=%g new=%g f=%g)\n",
               r_old,r_new,f);
    }
    Dprint("%g\n",r);


#endif //  KARAKAS2002_SMOOTH_AGB_RADIUS_TRANSITION

    return r;
}
