#include "../binary_c.h"
No_empty_translation_unit_warning;


/* internal functions */
static double Pure_function rgbf_0(const double m,const double lum,
                                   const double giant_branch_parameters[]);
static double Pure_function rgbf_1(const double m,const double lum,
                                   const double giant_branch_parameters[]);

#ifdef AXEL_RGBF_FIX
double Pure_function rgbf(const double m,
                          const double lum,
                          const double *Restrict const giant_branch_parameters)
{
#if (DEBUG==1)
    double rgbf_orig(const double m,const double lum,const double giant_branch_parameters[]);
#endif

    double alpha=log10(giant_branch_parameters[GBP_AXEL0]);
    alpha = -(log10(giant_branch_parameters[GBP_AXEL2])-alpha)/(alpha-log10(giant_branch_parameters[GBP_AXEL1]));

    double result= (exp10((alpha*log10(rgbf_1(m,lum,giant_branch_parameters))
                              + (1.0-alpha)*log10(rgbf_0(m,lum,giant_branch_parameters))
                            )));

    //printf("rgbf(m=%g, lum=%g) = %g\n",m,lum,result);

    return (result);
}

#if (DEBUG==1)
double Pure_function rgbf_orig(const double m,
                               const double lum,
                               const double giant_branch_parameters[])
{
    /*
     * A function to evaluate radius on the GB.
     * (JH 24/11/97)
     */

    double result=NO_RESULT_YET_FLOAT;
    double a1;
    a1 = Min(giant_branch_parameters[20]*pow(m,-giant_branch_parameters[21]),
             giant_branch_parameters[22]*pow(m,-giant_branch_parameters[23]));
    result = a1*(pow(lum,giant_branch_parameters[18]) + giant_branch_parameters[17]*pow(lum,giant_branch_parameters[19]));

    return(result);
}
#endif // DEBUG==1

static double Pure_function rgbf_0(const double m,const double lum,
                                   const double giant_branch_parameters[])
{
    return ((giant_branch_parameters[GBP_AXEL7]*pow(lum,giant_branch_parameters[GBP_AXEL9]) +
             pow(lum,giant_branch_parameters[GBP_AXEL8])
             *
             Min(giant_branch_parameters[GBP2_AXEL0]*pow(m,-giant_branch_parameters[GBP2_AXEL1]),
                 giant_branch_parameters[GBP2_AXEL2]*pow(m,-giant_branch_parameters[GBP2_AXEL3]))));
}

static double Pure_function rgbf_1(const double m,const double lum,
                                   const double giant_branch_parameters[])
{
    return((pow(lum,giant_branch_parameters[GBP_AXEL18]) +
            giant_branch_parameters[GBP_AXEL17]*pow(lum,giant_branch_parameters[GBP_AXEL19]))
           *
           (Min(giant_branch_parameters[GBP_AXEL20]*pow(m,-giant_branch_parameters[GBP_AXEL21]),
                giant_branch_parameters[GBP_AXEL22]*pow(m,-giant_branch_parameters[GBP_AXEL23])))
        );
}

#else // AXEL_RGBF_FIX

double Pure_function rgbf(const double m, /** mass of star? **/
                          const double lum,/** luminosity of star?**/
                          const double giant_branch_parameters[]/** this replaces common block GBCFF **/)
{
/*
 * A function to evaluate radius on the GB.
 * (JH 24/11/97)
 */
    const double a=giant_branch_parameters[20]*pow(m,-giant_branch_parameters[21]);
    const double result=giant_branch_parameters[22]*pow(m,-giant_branch_parameters[23]);

    result =
        Min(a,result)*(pow(lum,giant_branch_parameters[18]) + giant_branch_parameters[17]*pow(lum,giant_branch_parameters[19]));

    return result;
}

#endif //AXEL_RGBF_FIX

/***********************************************************/
