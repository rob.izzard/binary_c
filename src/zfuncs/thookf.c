#include "../binary_c.h"
No_empty_translation_unit_warning;


double Pure_function thookf(const double m /** star mass? **/,
                            const double * Restrict const main_sequence_parameters)
{

    /*
     * A function to evaluate the lifetime to the end of the MS hook ( for those 
     * models that have one ) as a fraction of the lifetime to the BGB Note that 
     * this function is only valid for M > Mhook. (JH 24/11/97) used a lot
     *
     * See Hurley et al. 2000 Eq. 7
     */
    double result;
    double r1=main_sequence_parameters[22]*pow(m,-main_sequence_parameters[23]);
    double r2=main_sequence_parameters[24]+main_sequence_parameters[25]*pow(m,-main_sequence_parameters[26]);
    result = 1.0 - 0.01*Max(r1,r2);  
    result = Max(result,0.5);
    
    return (result);
 
  
}

/****************************************************************/


