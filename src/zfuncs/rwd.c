#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef BSE
double Pure_function rwd(struct stardata_t * const stardata,
                         struct star_t * const star,
                         const double m,
                         const Stellar_type stellar_type,
                         const Boolean thermal_transition Maybe_unused)
{
    /*
     * White dwarf radius.
     *
     * Only valid for stars with M<MCh, so for stars with
     * mass exceeding this, return the neutron-star radius,
     * rns(m)
     */
    double rwd;
    const double MCh = chandrasekhar_mass_wrapper(stardata,m,m,stellar_type);
    if(More_or_equal(m,MCh))
    {
        rwd = rns(m);
    }
    else
    {
        if(stardata->preferences->white_dwarf_radius_model == WHITE_DWARF_RADIUS_NAUENBERG1972)
        {
            /*
             * Formula from Nauenberg (1972), adapted to prevent
             * the star from having zero radius as M->MCh.
             *
             * The stellar type should be the core's stellar type
             * if this function is being called to estimate the core
             * radius inside a nuclear-burning star.
             */
            const double x = Pow2(cbrt(MCh/m));
            rwd = 0.01150*sqrt(Max(1.48204e-06,x - 1.0/x));
        }
        else if(stardata->preferences->white_dwarf_radius_model == WHITE_DWARF_RADIUS_MU)
        {
            /*
             * You might want to consider the alternative expression:
             * R = 1e9 * (m/0.7)**(1/3) * (1-m/MCh)**0.5 * (mu_e/2)**(-5.0/3.0)
             *
             * Which gives, for he WDs, ~ twice the radius
             *
             * For COWDs the agreement is good within ~10-20% over most of the mass range
             *
             * (For helium mu_e ~ 4/3, For COWDs mu_e ~ 1.75)
             */
            const double mu = stellar_type == HeWD ? (5.0/3.0) : (7.0/4.0);
            rwd = 1e9 / R_SUN * cbrt(m/0.7) * sqrt(1.0-Min(MCh-1e-6,m)/MCh) * pow(mu*0.5,-5.0/3.0);
        }
        else if(stardata->preferences->white_dwarf_radius_model == WHITE_DWARF_RADIUS_CARRASCO2014)
        {
            /*
             * Table of R(mass,age) from Carrasco et al. (2014)
             */
            const double params[2] = {m,Max(4.0,log10(star->age)+6.0)};
            double results[2];
            struct data_table_t * const t =
                star->white_dwarf_atmosphere_type == WHITE_DWARF_ATMOSPHERE_HYDROGEN ?
                stardata->store->carrasco2014_cooling_table3 :
                star->white_dwarf_atmosphere_type == WHITE_DWARF_ATMOSPHERE_HELIUM ?
                stardata->store->carrasco2014_cooling_table4 :
                star->white_dwarf_atmosphere_type == WHITE_DWARF_ATMOSPHERE_HYDROGEN_HELIUM ?
                stardata->store->carrasco2014_cooling_table5 :
                NULL;

            if(t==NULL)
            {
                Exit_binary_c(BINARY_C_WRONG_ARGUMENT,
                              "Unknown WD atmosphere type when calculating R(mass,age) in lwd().");
            }

            Interpolate(stardata->store->carrasco2014_cooling_table3,
                        params,
                        results,
                        FALSE);
            rwd = exp10(results[1]);
        }
        else
        {
            Exit_binary_c(BINARY_C_WRONG_ARGUMENT,
                          "Unknown white dwarf radius model %d.\n",
                          stardata->preferences->white_dwarf_radius_model);

        }

        if(isnan(rwd))
        {
            Exit_binary_c(BINARY_C_EXIT_NAN,
                          "RWD is NAN : model %d mass %g Mch %g \n",
                          stardata->preferences->white_dwarf_radius_model,
                          star->mass,
                          MCh
                );
        }

        /*
         * white dwarf radius must exceed a neutron star
         */
        rwd = Max(rns(m), rwd);

#ifdef HeMS_HeWD_THERMAL_TRANSITION
        /*
         * If the star is a HeWD with a "carbon core", it *was*
         * a helium star or a star that has been stripped to its
         * helium core: in this case, shrink on a thermal timescale.
         */
        if(thermal_transition == TRUE &&
           star->stellar_type == HeWD &&
           star->core_stellar_type == COWD)
        {
            /*
             * We want to transition from HeMS, or stripped
             * HG or RG, to HeWD
             */
            const Stellar_type prev_stellar_type =
                stardata->previous_stardata->star[star->starnum].stellar_type;
            const double t =
                Min(0.0,
                    star->phase_end[prev_stellar_type].time - stardata->model.time);
            const double M = Is_not_zero(m) ? m : star->phase_end[prev_stellar_type].mass;
            const double tkh = 1e-6 * kelvin_helmholtz_time_from_LMR(
                star->phase_end[prev_stellar_type].luminosity,
                M,
                M,
                star->phase_end[prev_stellar_type].radius
                );
            const double x = exp(t/tkh);
            const double R = x * star->phase_end[prev_stellar_type].radius;
            rwd = Max(R, rwd);
            if(0)
            {
                fprintf(stdout,
                        "HeWD(HeMS)R star %d, type %d, core type %d, M=%g, mass=%g, t = %g, age = %30.20e, tkh = %30.20e, x = %30.20e : radii prev %30.20e HeWD %30.20e : interpolate -> %30.20e\n",
                        star->starnum,
                        star->stellar_type,
                        star->core_stellar_type,
                        M,
                        star->mass,
                        stardata->model.time,
                        -t,
                        tkh,
                        x,
                        star->phase_end[prev_stellar_type].radius,
                        rwd,
                        R
                    );

            }
            if(isinf(rwd))
            {
                Exit_binary_c(BINARY_C_UNDER_OR_OVERFLOW,
                              "rwd is inf, oops!\n");
            }
            else if(isnan(rwd))
            {
                Exit_binary_c(BINARY_C_EXIT_NAN,
                              "rwd is nan, oops!\n");
            }
            else if(Is_zero(star->mass))
            {
                Exit_binary_c(BINARY_C_EXIT_NAN,
                              "in rwd, stellar mass is zero\n");
            }
            else if(isinf(tkh))
            {
                Exit_binary_c(BINARY_C_UNDER_OR_OVERFLOW,
                              "in rwd tkh is inf, oops!\n");
            }
            else if(isnan(tkh))
            {
                Exit_binary_c(BINARY_C_EXIT_NAN,
                              "in rwd tkh is nan, oops!\n");
            }

        }
#endif // HeMS_HeWD_THERMAL_TRANSITION
    }
    Dprint("rwd (m=%g) = %g\n",m,rwd);
    return rwd;
}

#endif//BSE
