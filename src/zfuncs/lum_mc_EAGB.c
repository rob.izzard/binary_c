#include "../binary_c.h"
No_empty_translation_unit_warning;


double Pure_function lum_mc_EAGB(const double mc,
                                 const double * Restrict const GB)
{
    double result;

    /*
     * A function to evaluate Luminosity given CO-core mass for EAGB stars
     */
    result= GB[GB_AXEL_1]*pow(mc,GB[GB_AXEL_2]);


    return(result);

}
/***********************************************************/


