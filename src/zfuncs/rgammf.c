#include "../binary_c.h"
No_empty_translation_unit_warning;


double Pure_function rgammf(const double m,
                            const double * Restrict const main_sequence_parameters)
{
    double result;
    /*
     * A function to evaluate the radius gamma coefficent.
     * (JH 24/11/97)
     */
    if(m > main_sequence_parameters[140])
    {
        result = 0.0;
    }
    else
    {

        /*
         * The BSE algorithm is buggy:
         *
         * when m = RGAMM_M1 = 1.0 the algorithm
         * is unstable, because main_sequence_parameters[142] = 0
         * and we end up with a 1/0.
         *
         * Hence, instead, we extend the Less_or_equal to m <= 1+1e-8
         * and slightly extrapolate the function. This way 1/0 never happens.
         *
         * The problem may be that the Less_or_equal check (which is
         * equivalent to the original Fortran's <=) can compare m=1
         * to RGAMM_M1... they may both be 1.0 to within numerical
         * noise, but then the Less_or_equal check may or may not give
         * what you expect.
         *
         * Really, we should refit to make the algorithm not rely
         * on numerical noise...
         */

        if(Less_or_equal(m,RGAMM_M1*(1.0+1e-8)))
        {
            result = main_sequence_parameters[83] +
                main_sequence_parameters[84]*
                pow(fabs((m-main_sequence_parameters[85])),
                    main_sequence_parameters[86]);
        }
        else
        {
            if(Less_or_equal(m,main_sequence_parameters[88]))
            {
                result = main_sequence_parameters[138] +
                    main_sequence_parameters[139]*
                    pow(((m - RGAMM_M1)/main_sequence_parameters[142]),
                        main_sequence_parameters[87]);
            }
            else
            {
                result = main_sequence_parameters[137] -
                    main_sequence_parameters[141]*(m - main_sequence_parameters[88]);
            }
        }

        result = Max(result,0.0);
    }

    return result;
}
/***********************************************************/
