#include "../binary_c.h"
No_empty_translation_unit_warning;


Constant_function double rns(const double Maybe_unused m)
{
    /*
     * Neutron star radius in Rsun from Hurley et al. 2000/2002
     * ~9.75km
     */
    //return 1.4e-5;


    /*
     * 12km is an "average" value based on many equations
     * of state, e.g. Oezel and Freire (2016)
     * https://www.annualreviews.org/doi/pdf/10.1146/annurev-astro-081915-023322
     * See Fig. 7
     */
    return 12.0 * 1e5 / R_SUN;
}
