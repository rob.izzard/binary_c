#include "../binary_c.h"
No_empty_translation_unit_warning;


#define PERTA 0.1

Constant_function double rpertf(const double m,
                                const double mu,
                                const double r,
                                const double rc)
{
    /*
     * A function to obtain the exponent that perturbs radius.
     */
    double result;
    if(Less_or_equal(mu,0.0))
    {
        result = 0.0;
    }
    else
    {
        const double b = 0.006*Max(1.0,2.5/m);
        const double fac = Limit_range(PERTA/log(r/rc),
                                       -50.0,
                                       -14.0/log10(mu));
        result = Pow3(mu/b);
        result = (1.0 + Pow3(b)) * result * pow(mu,fac) / (1.0 + result);
        Clamp(result,0.0,1.0);
    }
    return result;
}
