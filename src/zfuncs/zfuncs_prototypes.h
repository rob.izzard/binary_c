/*
 * Header file for the interpolation functions used by BSE
 */
/*******************************************/

#ifndef ZFUNCS_PROTOTYPES_H
#define ZFUNCS_PROTOTYPES_H
#include "../binary_c_parameters.h"
#ifdef BSE
#include "../binary_c_structures.h"
#include "../binary_c_code_options.h"
#include "../binary_c_prototypes.h"

double Pure_function mbagbf(const double mc,
                            const double * Restrict const giant_branch_parameters);
double Pure_function ltmsf(const double m,
                           const double * Restrict const main_sequence_parameters) ;
double Pure_function rtmsf(const double m,
                           const double * Restrict const main_sequence_parameters);
double Pure_function lalphf(const double m,
                            const double * Restrict const main_sequence_parameters) ;
double Pure_function ralphf(const double m,
                            const double * Restrict const main_sequence_parameters) ;
double Pure_function lbetaf(const double m,
                            const double * Restrict const main_sequence_parameters) ;
double Pure_function lnetaf(const double m,
                            const double * Restrict const main_sequence_parameters) ;
double Pure_function rgammf(const double m,
                            const double * Restrict const main_sequence_parameters) ;
double Pure_function lzamsf(const double m,
                            const double * Restrict const main_sequence_parameters) ;
double Pure_function rzamsf(const double m,
                            const double * Restrict const main_sequence_parameters) ;
double Pure_function mheif(const double m,
                           const double mhefl,
                           const double mchefl,
                           const double * Restrict const giant_branch_parameters);
double Pure_function lbagbf(const double m,
                            const double mhefl,
                            const double * Restrict const giant_branch_parameters);
double Pure_function ragbf(const double m,
                           const double lum,
                           const double mhelf Maybe_unused,
                           const double * Restrict const giant_branch_parameters
#ifdef AXEL_RGBF_FIX
                           ,const double z
#endif
    );

double Pure_function thef(const double m,
                          const double mc,
                          const double mhefl,
                          const double * Restrict const giant_branch_parameters);

Constant_function double themsf(const double m );
double tbgbf(struct stardata_t * stardata,
             const double m,
         const double * Restrict const main_sequence_parameters,
         const double metallicity);
double Pure_function lheif( double m,
                            const double mhefl,
                            const double * Restrict const giant_branch_parameters);
double Pure_function rminf(const double m,
                           const double * Restrict const giant_branch_parameters);
Constant_function double rzhef(const double m);
double Pure_function tblf(const double m,
                          const double mhefl,
                          const double mfgb,
                          const double * Restrict const giant_branch_parameters,
                          const Abundance z) ;
double Pure_function lhef(const double m,
                          const double * Restrict const giant_branch_parameters/**this replaces common block GBCFF*/
    );

Constant_function double lzhef(const double m);

double Pure_function thookf(const double m,
                            const double * Restrict const main_sequence_parameters);
double Pure_function lzahbf(const double m,
                            const double mc,
                            const double mhefl,
                            const double * Restrict const giant_branch_parameters) ;
double Pure_function rzahbf(const double m,
                            const double mc,
                            const double mhefl,
                            const double * Restrict const giant_branch_parameters) ;
double Pure_function mcagbf(const double m,
                            const double * Restrict const giant_branch_parameters);
double Pure_function lgbtf(const double t,
                           const double A,
                           const double * Restrict GB,
                           const double tinf1,
                           const double tinf2,
                           const double tx);

double Pure_function lbgbf(const double m,
                           const double * Restrict const giant_branch_parameters);

double Pure_function lbgbdf(const double m,
                            const double * Restrict const giant_branch_parameters);
double Pure_function lmcgbf(const double m,
                            const double * Restrict GB);
double Pure_function mcgbf(const double lum,
                           const double * Restrict const GB,
                           const double lx);


double Pure_function mcheif(const double m,
                            const double mhefl,
                            const double mchefl,
                            const double * Restrict const giant_branch_parameters);
double Pure_function rgbf(const double m,
                          const double lum,
                          const double * Restrict const giant_branch_parameters);
double Pure_function rhookf(const double m ,
                            const double mhook,
                            const double * Restrict const main_sequence_parameters);
double Pure_function lhookf(const double m ,
                            const double mhook,
                            const double * Restrict const main_sequence_parameters);
Constant_function double mctmsf(const double m);

double rhelmf(Stellar_type * const stellar_type,
              const double m,
          const double lum,
          const double rx,
          const double lx);
Constant_function double lpertf(const double m,
                                const double mew);
Constant_function double rpertf(const double m,
                                const double mew,
                                const double r,
                                const double rc);
double Pure_function mcgbtf(const double t,
                            const double A,
                            const double * Restrict GB,
                            const double tinf1,
                            const double tinf2,
                            const double tx);
double Pure_function dmcgbtf_dt(const double t,
                                const double A,
                                const double * Restrict GB,
                                const double tinf1,
                                const double tinf2,
                                const double tx);

double Pure_function rbetaf(const double m,
                            const double * Restrict const main_sequence_parameters);

Constant_function double menvf(const Stellar_type stellar_type,
                               const double m,
                               const double mc,
                               const double age,
                               const double tm,
                               const double tbgb);
double Pure_function renvf(const Stellar_type stellar_type,
                           const double m,
                           const double r,
                           const double rc,
                           const double age,
                           const double tm,
                           const double tbgb,
                           const double * Restrict const main_sequence_parameters );

Constant_function double rhehgf(const double m,
                                const double lum,
                                const double rx,
                                const double lx);
Constant_function double rhegbf(const double lum);

double Pure_function lum_mc_EAGB(const double mc,
                                 const double * Restrict const GB);

double Pure_function mcx_eagbf(const double t,
                               const double A,
                               const double * Restrict const GB,
                               const double tinf1,
                               const double tinf2,
                               const double tx
    );

Constant_function double mass_CO_core_at_BAGB(const double m,
                                              const Abundance z);


double Pure_function rwd(struct stardata_t * const stardata,
                         struct star_t * const star,
                         const double m,
                         const Stellar_type stellar_type,
                         const Boolean thermal_transition Maybe_unused);
double Pure_function lwd(struct stardata_t * const stardata,
                         struct star_t * const star,
                         const double m,
                         const double age,
                         const Stellar_type st,
                         const Boolean thermal_transition Maybe_unused);
double fphase(struct stardata_t * const stardata,
              const double t,
              const struct star_t * const star);



void main_sequence_strip(const double effective_zams_mass, // ZAMS mass
             const double fractional_age, // age/t_MS
             const double mass, // M(t)
             double * const fluminosity,
             double * const fradius,
             double * const drdm);


#ifdef HALL_TOUT_2014_RADII

double Pure_function Hall_Tout_2014_low_mass_HG_RGB_radius(struct stardata_t * const stardata,
                                                           struct star_t * const star,
                                                           const double m);

double Pure_function Hall_Tout_2014_AGB_radius(struct stardata_t * const stardata,
                                               struct star_t * const star,
                                               const double m,
                                               const Stellar_type core_stellar_type);
double Constant_function Hall_Tout_2014_HeHG_radius(const double mc);
double Pure_function Hall_Tout_2014_HeGB_radius(struct stardata_t * const stardata,
                                                struct star_t * const star,
                                                const double mc);


#endif//HALL_TOUT_2014_RADII

Constant_function double mc_heburn(double m);
double Constant_function dm_intershell(const double mc);
double Pure_function mcbgb(struct star_t * Restrict const s,
                           struct stardata_t * Restrict const stardata);

double Pure_function min_He_ignition_core_mass(struct stardata_t * Restrict const stardata,
                                               struct star_t * Restrict const star Maybe_unused);
double Pure_function Hurley2002_mc1tp(const double phase_start_mass,
                                      struct stardata_t * Restrict const stardata);

#endif // BSE



double Karakas2002_lumfunc(Stellar_type stellar_type,
                           const double m,
                           const double mc_1tp,
                           const double menv,
                           const double mcno3dup,
                           const double phase_start_mass,
                           const double num_thermal_pulses,
                           const double interpulse_period,
                           const double time_first_pulse,
                           const double time_prev_pulse,
                           const double time_next_pulse,
                           const double age,
                           const Abundance metallicity,
                           struct stardata_t * Restrict const stardata,
                           double * const GB);



double Karakas2002_ragb_teff(const double m,
                             const Abundance metallicity,
                             double l,
                             const double m0,
                             const double mc_1tp Maybe_unused,
                             const double menv Maybe_unused,
                             const double mcnodup Maybe_unused,
                             struct star_t * const star Maybe_unused,
                             struct stardata_t * const stardata);

double Karakas2002_radius_2param_m(struct stardata_t * const stardata,
                                   const double m0,
                                   const double m,
                                   const double z,
                                   const double l);
double Karakas2002_interpulse_period(struct stardata_t * Restrict const stardata,
                                     const double mc,
                                     const double m,
                                     const double mc_1tp,
                                     Abundance Z,
                                     const double lambda,
                                     const struct star_t * const star);
double Pure_function Karakas2002_mc1tp(double m,
                                       const double phase_start_mass,
                                       struct stardata_t * stardata);


/*
 * Other zfuncs which are not part of BSE's library
 * or that are used in other stellar evolution libraries.
 */
Constant_function double rbh(const double m);
Constant_function double rns(const double Maybe_unused m);
Constant_function double lns(const double m,const double age);


Constant_function double gamma_rot(const double mass, const double luminosity);


double guess_mc1tp(const double mass,
                   const double phase_start_mass,
                   const struct star_t * Restrict const star,
                   struct stardata_t * Restrict const stardata);

Constant_function double effective_core_mass_fraction_of_MS_stars(const double m,
                                                                  const double z);

double Pure_function molecular_weight(const struct star_t * Restrict const star,
                                      const struct stardata_t * Restrict const stardata);
Constant_function double thick_disk_age(const double z);

Constant_function double m_conv_core(const double m,
                                     const double age,
                                 const double tms);
Constant_function double max_m_conv_core(const double m);

Constant_function double L_1DUP(const double m);
double mc_1DUP(struct stardata_t * Restrict const stardata,
               const double m,
               const double z);
double menv_1DUP(struct stardata_t * Restrict const stardata,
                 const double m,
                 const double z);
double Constant_function m_1dup(const double m);


#ifdef PRE_MAIN_SEQUENCE
Constant_function double preMS_lifetime(const double mass);
Constant_function double preMS_radius_factor(double mass,
                                             const double time);
double time_from_preMS_radius_factor(struct stardata_t * Restrict const stardata,
                                     const double mass,
                     const double radius_factor);
#endif // PRE_MAIN_SEQUENCE

double Pure_function R_of_T(const struct star_t * Restrict const star,
                            const double T);

double Pure_function escape_velocity(const struct star_t * Restrict const star);

#endif /* ZFUNCS_PROTOTYPES_H */
