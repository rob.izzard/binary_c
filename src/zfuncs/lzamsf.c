#include "../binary_c.h"
No_empty_translation_unit_warning;


double Pure_function lzamsf(const double m,
                            const double * Restrict const main_sequence_parameters)
{
    /*
     * A function to evaluate Lzams
     * (from Tout et al., 1996, MNRAS, 281, 257).
     */
    const double sm = sqrt(m);
    const double m2 = Pow2(m);
    const double m3 = m2*m;
    const double m5 = m3*m2;
    const double m8 = m5*m3;

    return (main_sequence_parameters[1]*m5*sm +
            main_sequence_parameters[2]*m8*m3)/
        (main_sequence_parameters[3] +
         m3 +
         main_sequence_parameters[4]*m5 +
         main_sequence_parameters[5]*m5*m2 +
         main_sequence_parameters[6]*m8+
         main_sequence_parameters[7]*m8*m*sm);
}
/***********************************************************/
