#include "../binary_c.h"
No_empty_translation_unit_warning;


double Pure_function lbetaf(const double m, 
              const double * Restrict const main_sequence_parameters)
{

    double result;
    /*
     * A function to evaluate the Luminosity beta coefficent.
     * (JH 24/11/97)
     */

    result=main_sequence_parameters[43]-main_sequence_parameters[44]*
        pow(m,main_sequence_parameters[45]);

    if(result>0.0)
    {
        double dm=m-main_sequence_parameters[46];
        if(dm>0.0)
        {
            result=Max(main_sequence_parameters[143]*(1.0-10.0*dm),0.0);
        }
    }
    else
    {
        result = 0.0;
    }

    return(result);
}
/***********************************************************/

