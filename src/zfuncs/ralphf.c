#include "../binary_c.h"
No_empty_translation_unit_warning;


double Pure_function ralphf(const double m,
                            const double * Restrict const main_sequence_parameters)
{
    double result;

    /*
     * A function to evaluate the radius alpha coefficent.
     * (JH 24/11/97)
     */

    if(Less_or_equal(m,0.65))
    {
        if(Less_or_equal(m,0.5))
        {
            result = main_sequence_parameters[73];
        }
        else
        {
            result = main_sequence_parameters[73] +
                main_sequence_parameters[128]*(m - 0.5);
        }

    }
    else if(Less_or_equal(m,main_sequence_parameters[71]))
    {
        if(Less_or_equal(m,main_sequence_parameters[70]))
        {
            result = main_sequence_parameters[74] +
                main_sequence_parameters[129]*(m - 0.65);
        }
        else
        {
            result = main_sequence_parameters[75] +
                ((main_sequence_parameters[76] - main_sequence_parameters[75])/
                 (main_sequence_parameters[71] - main_sequence_parameters[70]))*
                (m - main_sequence_parameters[70]);
        }
    }
    else
    {
        if(Less_or_equal(m,main_sequence_parameters[72]))
        {
            result = (main_sequence_parameters[65]*pow(m,main_sequence_parameters[67]))/
                (main_sequence_parameters[66] +pow(m,main_sequence_parameters[68]));
        }
        else
        {
            result = main_sequence_parameters[131]+ main_sequence_parameters[69]*
                (m - main_sequence_parameters[72]);
        }
    }


    return result;
}
/***********************************************************/
