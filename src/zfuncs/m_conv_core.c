#include "../binary_c.h"
No_empty_translation_unit_warning;


Constant_function double m_conv_core(const double m,
                                     const double age,
                                     const double tms
    )
{
    /*
     * Mass of the convective core as a function of time:
     * strictly only valid for z=z_solar
     */

    /* relative age on the MS */
    double fms=Max(0.0,Min(1.0,age/tms));

    /* maximum core mass (at t=0) */
    double max_mc_conv=max_m_conv_core(m);

    /* some kind of shrinkage function */
    return(pow(1.0-fms,0.2) * max_mc_conv);
}
