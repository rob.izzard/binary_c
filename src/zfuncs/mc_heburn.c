#include "../binary_c.h"
No_empty_translation_unit_warning;


Constant_function double mc_heburn(const double m)
{
    /*
     * Core mass above which helium is allowed to burn,
     * below which helium is extinguished and the star
     * becomes a white dwarf
     *
     * This is Hurley et al. 2002 Eq. 89
     */
    return 1.45 * m - 0.31;
}
