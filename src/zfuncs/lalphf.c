
#include "../binary_c.h"
No_empty_translation_unit_warning;


double Pure_function lalphf(const double m, /** mass of star? **/
              const double * Restrict const main_sequence_parameters/** this replaces common block MSCFF **/) 
{
    double result;
    /*
     * A function to evaluate the Luminosity alpha coefficent.
     * (JH 24/11/97)
     *
     * Hurley et al. 2000 Eq. 19
     */
    if(More_or_equal(m,LALPH_MCUT))
    {        
	result = (main_sequence_parameters[33] + 
		  main_sequence_parameters[34]*
		  pow(m,main_sequence_parameters[36]))/
	    (pow(m,0.4) + main_sequence_parameters[35]*pow(m,1.9));
    }
    else
    {

        // msp38 >1.0, msp37 >0.9 so both are always >0.7
        if(Less_or_equal(m,0.7))
        {
            if(Less_or_equal(m,0.5))
            {
                result = main_sequence_parameters[39];
            }
            else
            {  
                result = main_sequence_parameters[39] + main_sequence_parameters[136]*(m - 0.5);
            }
        }
        else if(Less_or_equal(m,main_sequence_parameters[37]))
	{
	    result = 0.3 + main_sequence_parameters[135]*(m - 0.7);
	}
        else if(Less_or_equal(m,main_sequence_parameters[38]))
	{
	    result = main_sequence_parameters[40] + 
		(main_sequence_parameters[133])*(m - main_sequence_parameters[37]);
	}
        else
        {
            result = main_sequence_parameters[41] + 
                (main_sequence_parameters[134])*(m - main_sequence_parameters[38]);
        }
    }

    return(result);
}
/***********************************************************/

