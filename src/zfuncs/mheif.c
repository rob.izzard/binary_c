#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef BSE
double Pure_function mheif(const double mc,
             const double mhefl, 
             const double mchefl,
             const double * Restrict const giant_branch_parameters)
{
    double result;
    /*
     * A function to evaluate mass at BGB or He ignition
     * (depending on mchefl) for IM & HM stars by inverting
     * mcheif
     */
    const double a3 = Pow4(mchefl)-giant_branch_parameters[33]*pow(mhefl,giant_branch_parameters[34]);
    
    result=Max(
        mbagbf(mc/0.95,giant_branch_parameters),
        pow((Pow4(mc)-a3)/giant_branch_parameters[33],1.0/giant_branch_parameters[34]) 
        );

    return result;  

}

/*****************************************************************/

#endif//BSE
