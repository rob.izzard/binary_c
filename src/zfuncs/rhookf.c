#include "../binary_c.h"
No_empty_translation_unit_warning;


double Pure_function rhookf(const double m ,
                            const double mhook, 
                            const double * Restrict const main_sequence_parameters)
{
    double result;
   
    if(Less_or_equal(m,mhook))
    {
        result = 0.0;
    }
    else if(Less_or_equal(m,main_sequence_parameters[94]))
    {
        result = sqrt((m-mhook)/(main_sequence_parameters[94]-mhook))*main_sequence_parameters[95];
    }
    else if(Less_or_equal(m,2.0))
    {   
        result = main_sequence_parameters[95] + main_sequence_parameters[122]*
            pow((m-main_sequence_parameters[94])*main_sequence_parameters[123],main_sequence_parameters[96]);
    }
    else
    {
        double m3=Pow3(m);
        result = (main_sequence_parameters[90] +
                  main_sequence_parameters[91]*m3*sqrt(m))/
            (main_sequence_parameters[92]*m3 + 
             pow(m,main_sequence_parameters[93])) - 1.0;
    }

    return result;
}


