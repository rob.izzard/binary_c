#include "../binary_c.h"
No_empty_translation_unit_warning;


double tbgbf(struct stardata_t * stardata,
             const double m,
             const double * Restrict const main_sequence_parameters,
             const double Z)
{
    /*
     * A function to evaluate the lifetime to the BGB or to
     * Helium ignition if no FGB exists.
     * (JH 24/11/97)
     */
    const double p7 = Pow7(m);
    const double oldage = ((
                         (main_sequence_parameters[17] + main_sequence_parameters[18]*Pow4(m)) + (main_sequence_parameters[19]*Pow5p5(m) + p7))/(main_sequence_parameters[20]*Pow2(m) + main_sequence_parameters[21]*p7));

#ifndef USE_2012_MAIN_SEQUENCE_LIFETIMES_TABLE
    return oldage;
#else
    /*
     * Use new tabular interpolation (RGI 05/06/2012): works better for high
     * mass stars (the previous interpolation is wrong by 7%).
     * Note that the tMS ~ tBGB is perfectly fine for massive stars,
     * the HG transit time may be significant at low mass.
     */
    if(m>18.0)
    {
        double parameters[2]={ log10(Z/0.02), log10(m) };
        double results[1];
        Interpolate(stardata->store->massive_MS_lifetimes,
                    parameters,
                    results,
                    FALSE /* TRUE=cache, FALSE=no cache */ );

        results[0]=exp10(results[0]-6.0);

        /*
         * Smoothly interpolate
         */
        double f = 1.0/(1.0 + pow(1e-3,m-20.0));
        return oldage * (1.0-f) + results[0] * f;
    }
    else
    {
        return oldage;
    }
#endif // USE_2012_MAIN_SEQUENCE_LIFETIMES_TABLE


}
/*****************************************************************/
