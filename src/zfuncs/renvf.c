#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef BSE
#define TAUMIN 5.0E-8
double Pure_function renvf(const Stellar_type stellar_type,
                           const double m,
                           const double r,
                           const double rc,
                           const double age,
                           const double tm,
                           const double tbgb,
                           const double * Restrict const main_sequence_parameters)
{
    double mx,rx,rzams,TAMS_radius,gamma,tau,xx;
    double result;

    /*
     * A function to estimate the depth of the convective envelope.
     */
    mx = 0.35E0;

    if(ON_MAIN_SEQUENCE(stellar_type))
    {
        tau = age/tm;
        if(Less_or_equal(m,mx))
    {
        result = r;
    }
        else if(More_or_equal(m,1.25))
    {
        result = 0.0;
    }
        else
        {
            rzams = rzamsf(mx,
                           main_sequence_parameters);
            TAMS_radius = (main_sequence_parameters[52] +
                    main_sequence_parameters[53] *
                    pow(mx,main_sequence_parameters[55]))/
                (main_sequence_parameters[54] +
                 pow(mx,main_sequence_parameters[56]));
            gamma = rgammf(mx,
                           main_sequence_parameters);
            rx = log10(TAMS_radius/rzams);
            if(tau>TAUMIN)
            {

                xx = main_sequence_parameters[73]*tau + 1.060*pow(tau,10) + gamma*pow(tau,40) +
                    (rx - main_sequence_parameters[73] - 1.060 - gamma)*Pow3(tau);
            }
            else
            {
                xx = main_sequence_parameters[73]*tau + (rx - main_sequence_parameters[73])*Pow3(tau);
            }
            rx = rzams*exp10(xx);
            result = rx*sqrt((1.250 - m)/0.90);
            result = Min(result,r);
        }

        result *= pow((1.0-tau),0.25);
    }
    else if(stellar_type==HERTZSPRUNG_GAP)
    {
        tau = (age-tm)/(tbgb-tm);
        result = sqrt(tau)*(r-rc);
    }
    else
    {
        result = r - rc;
    }

    result = Max(result,1.0E-10);

    return result;
}

/*********************************/

#endif//BSE
