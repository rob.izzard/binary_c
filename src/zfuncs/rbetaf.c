#include "../binary_c.h"
No_empty_translation_unit_warning;


double Pure_function rbetaf(const double m,
                            const double * Restrict const main_sequence_parameters)
{
    double result;

    /*
     * A function to evaluate the radius beta coefficent.
     * (JH 24/11/97)
     */

    if(Less_or_equal(m,1.0))
    {
	result=0.06;
    }
    else if(Less_or_equal(m,main_sequence_parameters[82]))
    {
	result = 0.06 + main_sequence_parameters[124]*(m-1.0);
    }
    else if(Less_or_equal(m,ZPAR_M2))
    {
	result = main_sequence_parameters[81] +main_sequence_parameters[126]*(m-main_sequence_parameters[82])-1.0;
	
    }
    else if(Less_or_equal(m,ZPAR_M3))
    {
	result = (main_sequence_parameters[77]*Pow3p5(m))/
	    (main_sequence_parameters[78] +pow(m,main_sequence_parameters[79]))-1.0;
    }
    else
    {
        result = main_sequence_parameters[127] + main_sequence_parameters[80]*(m - ZPAR_M3)-1.0;
    }
    
    return(result); 
}
/***********************************************************/

