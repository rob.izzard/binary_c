#include "../binary_c.h"
No_empty_translation_unit_warning;

Constant_function double effective_core_mass_fraction_of_MS_stars(const double m,
                                                                  const double z)
/*
 *  Glebbeek's Expression for the "effective core mass fraction" for
 *  main sequence stars, using Glebbeek et al equation 3.8 (in thesis)
 *
 * Requires as input paramerers:
 * m : mass in solar masses
 * z : metallicity, mass fraction of elements heavier than He at zero age
 *
 * This is basically a measure of the farction of hydrogen that is
 * burned during the main sequence, without specifying the precise core mass,
 * Glebbeek et al. ().
 *
 * This is meant to be used for mergers of main sequence stars to
 * better approximate the remaining life time
 *
 * Note, this function is only computed once at the moment of merger
 * of two main seuqence stars, so speed is not the main concern.
 *
 * Implementation: SdM May 2011
 * Todo: add low metallicity and move the constants to header file
 */
{
    // metallicities at which the fitting formulae was derived
    static const double z_computed[2] = {0.001, 0.02};

    // Compute powers of M to be used in the analytic fit, i.e. mpower(1) = m, mpower(x) = m^x
    unsigned int i;
    double mpower[8];
    mpower[0] = 1.0;
    mpower[1] = m;
    for (i=2; i<8; i++)
    {
        mpower[i] = m * mpower[i-1];
    }

    // Co-efficients for a metallicity Z=0.02
    static double const c_Z02[9] =
        {
            1.0,//0
            -0.685213, //1
            0.289269, //2
            0.0123223, //3
            3.3357E-6, //4
            2.70964, //5
            1.44963, //6
            0.629121,//7
            0.000493117//8
        };

    // Coeefficients for a metallicity Z=0.001
    static double const c_Z001 [9] =
        {
            1.0,//0
            -0.682671, //1
            0.185036, //2
            0.00658021,//3
            7.5450E-7, //4
            2.71437, //5
            0.334367, //6
            0.343889,//7
            0.000185160//8
        };

    double dividend, divisor, x;
    double effective_core_mass_fraction;
    double effective_core_mass_fraction_Z[2];

    /*
     * compute the value for Z = 0.02 and Z= 0.001 and interpolate afterwards.
     *   (Seemed better/faster than interpolating the coefficients.)
     */
    for(i=0; i<2; i++)
    {
        /*
         * store the appropriate array in c (please tell me there is an
         * easier more readible andcompact way to copy one array into another
         */
        const double *c = i==0 ? c_Z001 : c_Z02;

        /* Compute first part of analytic fit */
        dividend =
            c[0] +
            c[1] * mpower[1] +
            c[2] * mpower[3] +
            c[3] * mpower[5] +
            c[4] * mpower[7];

        /* Compute second part of analytic fit */
        divisor =
            c[5] +
            c[6] * mpower[2] +
            c[7] * mpower[4] +
            c[8] * mpower[6];

        effective_core_mass_fraction_Z[i] = dividend / Max( divisor, 1e-10);
    }

    /*
     * Interpolate linearly in metallicty (Is that the best thing to do,
     * or should I interpolate in the log of the metallicity?)
     */
    x = (z - z_computed[0]) / (z_computed[1]- z_computed[0]);

    /*
     * Allow extrapolation beyond boundaries (Is that wise to do? It does
     * not seem to be of much harm towards very low metallicity (as long
     * as we ineterpolate linearly in metallicity).  Towards higher
     * metallicity it may get inaccurate faster.)
     */
    effective_core_mass_fraction =
        effective_core_mass_fraction_Z[0] * (1.0 - x) +
        effective_core_mass_fraction_Z[1] *        x;

    return effective_core_mass_fraction;
}
