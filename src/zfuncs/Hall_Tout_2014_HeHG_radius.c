#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef HALL_TOUT_2014_RADII

double Constant_function Hall_Tout_2014_HeHG_radius(const double mc)
{

    /*
     * Radius of helium Herztsprung gap stars
     * Hall and Tout (2014)  Eq. 4 with mc =
     * core mass at the start of the Helium
     * Hertzsprung gap (i.e. constant through the gap)
     */
    const double rc =
        Quadratic(mc,0.0123,0.806,-0.00331)/
        Quadratic(mc,1.00,0.467,-0.0303);

    return rc;
}
#endif // HALL_TOUT_2014_RADII
