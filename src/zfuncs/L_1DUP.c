#include "../binary_c.h"
No_empty_translation_unit_warning;


Constant_function double L_1DUP(const double m)
{

    /*
     * Luminosity at 1st dredge up as a function of
     * (initial?) mass
     */

    /* fit for solar metallicity only */
    return(2.85420e+00+7.77930e-01*exp(-pow(m+3.71360e-01,2.0)/2.89200e+01)+8.95600e-02*m );
}
