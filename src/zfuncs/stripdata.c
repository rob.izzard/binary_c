
#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef MAIN_SEQUENCE_STRIP
#include "stripdata.h"

void main_sequence_strip(const double effective_zams_mass, // ZAMS mass
                         const double fractional_age, // age/t_MS
                         const double mass, // M(t)
                         double * const fluminosity,
                         double * const fradius,
                         double * const drdm)
{
    Const_data_table table[]={MAIN_SEQUENCE_STRIPDATA};
    double x[3],r[3]; 

    /*
     * Given the ZAMS mass, fractional main sequence age and 
     * fractional mass (relative to the ZAMS mass) calculate multipliers
     * for calculating radius and mass, as well as dR/dM
     */

    x[0]=effective_zams_mass;
    x[1]=fractional_age;
    x[2]=1.0-mass/effective_zams_mass;

    interpolate(table,3,3,
                MAIN_SEQUENCE_STRIPDATA_NUM_LINES,x,r,
                TABLE_MAIN_SEQUENCE_STRIPDATA,
                FALSE);

    *fluminosity=r[0];
    *fradius=r[1];
    *drdm=r[2];
}
#endif //MAIN_SEQUENCE_STRIP
