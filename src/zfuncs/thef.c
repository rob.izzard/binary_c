#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef BSE
double Pure_function thef(const double m,
                          const double mc,
                          const double mhefl,
                          const double * Restrict const giant_branch_parameters)
{
    /*
     * A function to evaluate the He-burning lifetime.  (OP 26/11/97)
     * For IM & HM stars, tHef is relative to tBGB.
     * Continuity between LM and IM stars is ensured by setting
     * thefl = tHef(mhefl,0.0,,0.0), and the call to themsf ensures
     * continuity between HB and NHe stars as Menv -> 0.
     */

    double result;
    if(Less_or_equal(m,mhefl))
    {
        result = (giant_branch_parameters[54] +
                  (themsf(mc) - giant_branch_parameters[54])*
                  pow((Max((mhefl - m)/(mhefl - mc),1.0E-12)),
                      giant_branch_parameters[55]));
#ifndef DISCONTINUOUS_HELIUM_IGNITION
        result *=
            (1.0 + giant_branch_parameters[57]*
             exp(m*giant_branch_parameters[56]));
#endif
    }
    else
    {
        const double mm=Pow5(m);
        result = (giant_branch_parameters[58]*
                  pow(m,giant_branch_parameters[61]) +
                  giant_branch_parameters[59]*mm)/
            (giant_branch_parameters[60] + mm);
    }

    return (result);
}

/****************************************************************/


#endif//BSE
