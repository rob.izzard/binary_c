#include "../binary_c.h"
No_empty_translation_unit_warning;


double Pure_function rminf(const double m, 
                           const double * Restrict const giant_branch_parameters)
{
    double mx;
    double result;

    /*
     * A function to evaluate the minimum radius during He-burning
     * for IM & HM stars  (OP 20/11/97)
     */
    mx = pow(m,giant_branch_parameters[53]);
    result = (giant_branch_parameters[49]*m + 
              pow((giant_branch_parameters[50]*m),
                  giant_branch_parameters[52])*mx)/
        (giant_branch_parameters[51] + mx);
    return(result);
} 
/***********************************************************/

