#include "../binary_c.h"
No_empty_translation_unit_warning;


/* helium main sequence lifetime */
Constant_function double themsf(const double m)
{
    const double p6=Pow6(m);
    return((0.4129 + 18.81*Pow4(m) + 1.853*p6)/(p6*sqrt(m)));
}

/****************************************************************/


