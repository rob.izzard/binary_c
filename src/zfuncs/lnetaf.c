#include "../binary_c.h"
No_empty_translation_unit_warning;


double Pure_function lnetaf(const double m, 
                            const double * Restrict const main_sequence_parameters) 
{
    double result;
    /*
     * A function to evaluate the Luminosity neta exponent.
     * (JH 24/11/97)
     */
    if(Less_or_equal(m,1.0))
    {
	result=10.0;
    }
    else if(More_or_equal(m,1.1))
    {
	result=20.0;
    }
    else
    {
        result=10.0+100.0*(m-1.0);
    }
    result=Min(result,main_sequence_parameters[97]);
    
    return(result); 
}
/***********************************************************/

