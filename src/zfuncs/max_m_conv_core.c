#include "../binary_c.h"
No_empty_translation_unit_warning;


Constant_function double max_m_conv_core(const double m)
{
    /*
     * Maximum size of the convective core as a function of (initial) mass
     */

    /* fit for solar metallicity */
    return(Max(0.0,-2.76500e-01+1.73750e-02*pow(m,1.8778)+3.08840e-01*m));
}
