#include "../binary_c.h"
No_empty_translation_unit_warning;


Constant_function double lns(const double m,
                             const double age)
{
    /*
     * luminosity of neutron star in Lsun from Hurley et al. 2000/2002 
     */
    const double a = Max(age, 0.10);
    return 0.02 * pow(m,0.67) / Pow2(a);
}
