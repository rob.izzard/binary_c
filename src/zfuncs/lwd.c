#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef BSE

double Pure_function lwd(struct stardata_t * const stardata,
                         struct star_t * const star,
                         const double m,
                         const double age,
                         const Stellar_type st,
                         const Boolean thermal_transition Maybe_unused)
{
    /*
     * calculate the luminosity of a WD
     */
    const double xx = (st==HeWD) ? AHE : ACO;
    const double q = xx*(age+0.1);
    double lwd;

    if(stardata->preferences->white_dwarf_cooling_model == WHITE_DWARF_COOLING_MESTEL)
    {
        /*
         * Mestel cooling
         */
        lwd = 635.0*m*stardata->common.metallicity_parameters[14]*pow(q,-1.4);
    }
    else if(stardata->preferences->white_dwarf_cooling_model == WHITE_DWARF_COOLING_MESTEL_MODIFIED)
    {
        /*
         * Mestel cooling (modified)
         */
        lwd = 300.0*m*stardata->common.metallicity_parameters[14] *
            (age<9e3 ? pow(q,-1.180) : (pow(9000.1*xx,5.3)*pow(q,-6.48)));
    }
    else if(stardata->preferences->white_dwarf_cooling_model == WHITE_DWARF_COOLING_CARRASCO2014)
    {
        /*
         * Table of L(mass,log10(age(yr))) from Carrasco et al. (2014)
         */
        const double params[2] = {m,Max(4.0,log10(age)+6.0)};
        double results[2];
        struct data_table_t *  t =
            star->white_dwarf_atmosphere_type == WHITE_DWARF_ATMOSPHERE_HYDROGEN ?
            stardata->store->carrasco2014_cooling_table3 :
            star->white_dwarf_atmosphere_type == WHITE_DWARF_ATMOSPHERE_HELIUM ?
            stardata->store->carrasco2014_cooling_table4 :
            star->white_dwarf_atmosphere_type == WHITE_DWARF_ATMOSPHERE_HYDROGEN_HELIUM ?
            stardata->store->carrasco2014_cooling_table5 :
            NULL;


        if(t==NULL)
        {
            Exit_binary_c(BINARY_C_WRONG_ARGUMENT,
                          "Unknown WD atmosphere type when calculating L(mass,age) in lwd().");
        }

        Interpolate(stardata->store->carrasco2014_cooling_table3,
                    params,
                    results,
                    FALSE);
        lwd = exp10(results[0]);
    }
    else
    {
        Exit_binary_c(BINARY_C_WRONG_ARGUMENT,
                      "Unknown white dwarf cooling model %d.\n",
                      stardata->preferences->white_dwarf_cooling_model);

    }

#ifdef NANCHECKS
    if(isnan(lwd)!=0)
    {
        Backtrace;
        Exit_binary_c(BINARY_C_EXIT_NAN,
                      "WD nan L breakage : M=%g age=%g st=%d xx=%g q=%g l=%g\n",
                      m,age,st,xx,q,lwd);
    }
#endif

#ifdef HeMS_HeWD_THERMAL_TRANSITION
    /*
     * If the star is a HeWD with a carbon core, it *was*
     * a HeMS star: in this case, shrink on a thermal
     * timescale.
     */
    if(thermal_transition == TRUE &&
       star->stellar_type == HeWD &&
       star->core_stellar_type == COWD)
    {
        /*
         * We want to transition from HeMS, or stripped
         * HG or RG, to HeWD
         */
        const Stellar_type prev_stellar_type =
            stardata->previous_stardata->star[star->starnum].stellar_type;
        const double t = Min(0.0,
                             star->phase_end[prev_stellar_type].time - stardata->model.time);
        const double M = Is_not_zero(m) ? m : star->phase_end[prev_stellar_type].mass;
        const double tkh = 1e-6 * kelvin_helmholtz_time_from_LMR(
            star->phase_end[prev_stellar_type].luminosity,
            M,
            M,
            star->phase_end[prev_stellar_type].radius);
        const double x = exp(t/tkh);
        const double L = x * star->phase_end[prev_stellar_type].luminosity;
        lwd = Max(L, lwd);
        if(0)
        {
            fprintf(stderr,"HeWD(HeMS)L star %d type=%d prev_type=%d M=%g  : t = %g, tkh = %g, x = %g : L prev %g HeWD %g -> %g\n",
                    star->starnum,
                    star->stellar_type,
                    prev_stellar_type,
                    M,
                    -t,
                    tkh,
                    x,
                    star->phase_end[prev_stellar_type].luminosity,
                    lwd,
                    L
                );
        }
        if(isinf(lwd))
        {
            Exit_binary_c(BINARY_C_UNDER_OR_OVERFLOW,
                          "lwd is inf, oops!\n");
        }
        else if(isnan(lwd))
        {
            Exit_binary_c(BINARY_C_EXIT_NAN,
                          "lwd is nan, oops!\n");
        }
        else if(Is_zero(star->mass))
        {
            Exit_binary_c(BINARY_C_EXIT_NAN,
                          "in lwd, stellar mass is zero\n");
        }
        else if(isinf(tkh))
        {
            Exit_binary_c(BINARY_C_UNDER_OR_OVERFLOW,
                          "in lwd tkh is inf, oops!\n");
        }
        else if(isnan(tkh))
        {
            Exit_binary_c(BINARY_C_EXIT_NAN,
                          "in lwd tkh is nan, oops!\n");
        }
    }
#endif // HeMS_HeWD_THERMAL_TRANSITION

    return lwd;
}
#endif
