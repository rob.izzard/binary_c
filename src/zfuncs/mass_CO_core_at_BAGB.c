#include "../binary_c.h"
No_empty_translation_unit_warning;


Constant_function double mass_CO_core_at_BAGB(const double m,
                                              const Abundance z)
{
    const double a[3][4]={{1.2867e-5,-3.7089e-5,6.5354e-5,1.6070e-5},
                          {5.8807,8.6173e-1,3.4101e-1,6.0385e-2},
                          {4.8777e-3,-1.7643e-4,1.9855e-3,6.6354e-4}}; 
    const double lzs=log10(z/0.02);
    double b[3];
    double result;
    unsigned int i;
    /*
     * A function to evaluate CO coremass at the base of AGB,
     * fitted to Onno's detailed model with core overshooting.
     */
    for (i=0;i<3;i++)
    {
        b[i]=a[i][0]+lzs*(a[i][1]+lzs*(a[i][2]+lzs*a[i][3]));
    }
  
    double mm=Min(6,m); /* no fits above m=6! so limit the function */
    result=pow(b[0]*pow(mm,b[1])+b[2],0.25);

    return(result);

}
/***********************************************************/


