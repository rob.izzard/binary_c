#include "../binary_c.h"
No_empty_translation_unit_warning;



/*
 * Mass of the helium (hydrogen exhausted) core at the end of the MS
 *
 * SdM implemented April 20th 2011
 *
 * This function may be useful if at some point during the main
 * sequence you want to know the core size at the end Combining this
 * function with max_m_conv_core you can estimate the amount of
 * helium present in the core at any time during the main sequence
 * evolution
 *
 */
/*
 * NB1: Only accurate for intermediate mass and high mass stars only
 * for low mass stars there is not a real end of the main sequence
 *
 * NB 2 This funtion is intended to be to determine the outcome when
 * two main seuqence stars merge!.  Please test carefully if you use it for
 * any other apllications.
 *
 *
 * Update: SdM May 2011 I switched to the routine
 * effective_core_mass_fraction based on Glebbeeks fits.  They are
 * more accurate, and metality dependend and valid over a wider
 * range of masses without fudging it
 *
 */

#ifdef __DEPRECATED__
double mass_He_core_at_endofMS(const double mass,
                               struct stardata_t * const Restrict stardata)
{
    const double * metallicity_parameters=stardata->common.metallicity_parameters;
    double core_mass_at_end_MS_as_a_fraction_of_the_BGB_value;
    double core_mass_at_start_BGB;
    double core_mass_at_end_MS;

    core_mass_at_end_MS_as_a_fraction_of_the_BGB_value =  mctmsf(mass);


    // For low mass stars (that probably have no well defined end of the MS)
    if Less_or_equal(mass,metallicity_parameters[ZPAR_MASS_HE_FLASH]) //low mass
                    {
                        core_mass_at_start_BGB= 0.1*mass;
                        // very simplified, but this is what was used for mergers before!
                    }
    // For intermediate mass stars
    else if Less_or_equal(mass,metallicity_parameters[ZPAR_MASS_FGB])
                         {
                             core_mass_at_start_BGB=mcheif(mass,metallicity_parameters[ZPAR_MASS_HE_FLASH],
                                                           metallicity_parameters[9], stardata);
                         }
    // For high mass  mass stars
    else
    {
        core_mass_at_start_BGB=mcheif(mass, metallicity_parameters[ZPAR_MASS_HE_FLASH],
                                      metallicity_parameters[10], stardata);
    }


    core_mass_at_end_MS =    core_mass_at_start_BGB * core_mass_at_end_MS_as_a_fraction_of_the_BGB_value ;


    printf ("MIX ZFUNCS : %g %g %g %g\n", mass,
            core_mass_at_end_MS_as_a_fraction_of_the_BGB_value,
            core_mass_at_start_BGB,
            core_mass_at_end_MS);

    return core_mass_at_end_MS;
}
#endif // __DEPRECATED__
