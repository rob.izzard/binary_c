#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef PRE_MAIN_SEQUENCE
/*
 * Pre-main sequence radii from Railton, Tout and Aarseth 2014
 * PASA 31,17
 */
static Constant_function double alphaPMS(const double mass, int j);
static Constant_function double betaPMS(const double mass, int j);
static Constant_function double gammaPMS(const double mass, int j);
static Constant_function double scaled_time(const double time,
                                            const double preMS_lifetime);


Constant_function double preMS_lifetime(const double mass)
{
    /*
     * Pre-main sequence lifetime (years)
     * Eq.1
     */
    return exp10(43.6 - 35.8 * pow(mass, 0.015) * exp(3.96e-3 * mass));
}

Constant_function static double scaled_time(const double time,
                                            const double preMS_lifetime)
{
    /* scaled time : Eq 2 */
    return 1.0 - time/preMS_lifetime;
}

/*
 * given a mass and radius, calculate the time
 */
double time_from_preMS_radius_factor(struct stardata_t * Restrict const stardata,
                                     const double mass,
                                     const double radius_factor)
{
    /*
     * Given a target radius_factor, find corresponding time
     */
    double pms_lifetime = preMS_lifetime(mass);
    double time;

    /* if radius factor is<=1, just return on the ZAMS */
    if(Less_or_equal(radius_factor,1.0))
    {
        time=pms_lifetime;
    }
    else
    {
        /*
         * if radius factor > at time zero,
         * then just return zero
         */
        double f = preMS_radius_factor(mass,0.0);
        time = 0;//pms_lifetime;

        /* otherwise zoom in on required factor */
        if(radius_factor<preMS_radius_factor(mass,0.0))
        {
            double err = 1e100;
            const double thresh = 1e-3;
            double dtime = 0.5*pms_lifetime;
            err = fabs((f-radius_factor)/f);
            int count = 0;
            const int maxcount=200;
            double alpha=0.1;
            double shorttime = pms_lifetime*1e-7;

            while(err>thresh && count<maxcount)
            {
                /* calc new time, keep in range */
                time = Max(0.0,Min(pms_lifetime,time+dtime));

                /* hence new radius factor */
                f = preMS_radius_factor(mass,time);

                /* derivative of the radius factor with time */
                double dfdt;
                if(time<TINY)
                {
                    dfdt = (preMS_radius_factor(mass,time+shorttime)-f)/shorttime;
                }
                else
                {
                    dfdt = (f-preMS_radius_factor(mass,time-shorttime))/shorttime;
                }

                /* and error relative to target */
                err = fabs(1.0-radius_factor/f);

                /*
                 * new 'timestep'
                 */
                dtime = - alpha *(f-radius_factor) / dfdt;

                /* do not allow dtime to be too large */
                if(fabs(dtime) > 0.1*pms_lifetime)
                {
                    dtime = Sign(dtime)*0.1*random_number(stardata,&stardata->common.random_seed)*pms_lifetime;
                }

                Dprint("(%d) at time = %g (/%g) : dfdt=%g : dtime = (%g-%g) / %g = %g\n",
                       count,time,pms_lifetime,dfdt,
                       f,radius_factor,dfdt,dtime);

                count++;

                /* accelerate convergence when doing well */
                if(err<0.1) alpha = Min(alpha*1.5,0.5);
            }

            /* if there is no convergence, return on the ZAMS (as small as possible) */
            if(err > thresh || count>=maxcount)
            {
                time = pms_lifetime;
                printf("no PMS=ROL convergence after %d iterations : return pms_lifetime %g\n",
                       count,
                       pms_lifetime);
            }
        }
    }
    Dprint("return time=%g\n",time);
    return time;
}

Constant_function double preMS_radius_factor(double mass,
                                             const double time)
{
    /* radius factor : R/RZAMS from Eq 3 */
    double factor;

    /* limit to fitted mass range */
    Clamp(mass,1.0,8.0);

    double tau = scaled_time(time,
                             preMS_lifetime(mass));

    if(In_range(tau,0.0,1.0))
    {
        /* j from Eq 6 */
        const int j = mass<1.0 ? 1 : mass < 2.0 ? 2 : 3;

        const double alpha = alphaPMS(mass,j),
            beta=betaPMS(mass,j),
            gamma=gammaPMS(mass,j);

        /* factor from Eq 4 */
        factor = (alpha*Pow3(tau)+
                  beta*Pow4(tau)+
                  gamma*Pow5(tau))/
            (1.05-tau);

        factor = exp10(factor);
    }
    else
    {
        /* not in preMS : factor must be 1.0 */
        factor = 1.0;
    }

    return factor;
}

Constant_function static double alphaPMS(const double mass, int j)
{
    /* Eq 5 */
    static const double alpha_table[3][4]=
        {{0.0,0.0,0.0,0.0},
         {-4.00772,4.00772,0.0,0.0},
         {1.6032,2.20401,-0.60433,0.05172}};
    j--;
    return
        alpha_table[j][0] +
        alpha_table[j][1] * mass +
        alpha_table[j][2] * Pow2(mass) +
        alpha_table[j][3] * Pow3(mass);
}

Constant_function static double betaPMS(const double mass, int j)
{
    /* Eq 5 */
    static const double beta_table[3][4]=
        {{0.0,0.0,0.0,0.0},
         {8.5656,-8.5656,0.0,0.0},
         {-4.56878,-4.05305,1.24575,-0.10922}};
    j--;
    return
        beta_table[j][0] +
        beta_table[j][1] * mass +
        beta_table[j][2] * Pow2(mass) +
        beta_table[j][3] * Pow3(mass);
}

Constant_function static double gammaPMS(const double mass, int j)
{
    /* Eq 5 */
    j--;
    static const double gamma_table[3][4]=
        {{0.07432,-0.09430,0.07439,0.0},
         {-4.50678,4.56118,0.0,0.0},
         {3.01153,1.85745,-0.64290,0.05759}};
    return
        gamma_table[j][0] +
        gamma_table[j][1] * mass +
        gamma_table[j][2] * Pow2(mass) +
        gamma_table[j][3] * Pow3(mass);
}



#endif // PRE_MAIN_SEQUENCE
