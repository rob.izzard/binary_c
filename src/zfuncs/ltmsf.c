#include "../binary_c.h"
No_empty_translation_unit_warning;


double Pure_function ltmsf(const double m,
                           const double * Restrict const main_sequence_parameters)
{
    /*
     * A function to evaluate the luminosity at the end of the MS
     * (JH 24/11/97)
     */
    const double m3 = Pow3(m);
    return (main_sequence_parameters[27]*m3+
            main_sequence_parameters[28]*m3*m+
            main_sequence_parameters[29]*
            pow(m,(main_sequence_parameters[32]+1.8)))
        /(main_sequence_parameters[30] +
          main_sequence_parameters[31]*m3*m*m+
          pow(m,main_sequence_parameters[32]));
}
/***********************************************************/
