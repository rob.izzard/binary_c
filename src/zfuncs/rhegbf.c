#include "../binary_c.h"
No_empty_translation_unit_warning;


Constant_function double rhegbf(const double lum)
{    
    /*
     * A function to evaluate Helium star radius on the giant branch. 
     */
    return 0.08*pow(lum,0.75);
}
