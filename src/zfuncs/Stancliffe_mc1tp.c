#include "../binary_c.h"
No_empty_translation_unit_warning;

/* 
 * Richard Stancliffe's prescription for the LMC and SMC (Stancliffe, Izzard
 * and Tout 2005?)
 *
 * This function is kept here as references. 
 * You probably shouldn't ever need to use it.
 */
//#define THIRD_DREDGE_UP_STANCLIFFE


#ifdef THIRD_DREDGE_UP_STANCLIFFE

    // Use Richard Stancliffe's fits for LMC and SMC metallicity
    if(z>0.006)
    {
        // LMC
        //y=0.56+(8.70850e-02*m-0.6/m)/(1+pow(9.51050e-03,m-2.44950e+00));
        // fit with new points at m=1.25,1.75,2.25
        // RMS error 0.0138
        y=9.55700e-01+3.17410e-01*exp(-Pow2(m-1.16400e+00)/1.11920e+00)+-8.23520e-01/(1+pow(3.18310e-01,2.75460e+00-m));
    }
    else
    {
        // SMC
        y=5.66330e-01+6.76030e-02*m/(1+pow(9.67880e-02,m-2.63540e+00));
    }
    printf("RS MC0 %g (from m=%g z=%g)\n",y,m,z);
    return(y);

#endif
