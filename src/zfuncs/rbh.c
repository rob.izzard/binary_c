#include "../binary_c.h"
No_empty_translation_unit_warning;



Constant_function double rbh(const double m)
{
    /*
     * radius of a black hole in Rsun from Hurley et al. 2000/2002
     *
     * m is the gravitational mass of the black hole
     */
    return 4.24e-6 * m;
}
