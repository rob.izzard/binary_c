#include "../binary_c.h"
No_empty_translation_unit_warning;


double Pure_function lbagbf(const double m,
              const double mhefl, 
              const double * Restrict const giant_branch_parameters)
{
    double result;

/*
 * A function to evaluate the BAGB luminosity. (OP 21/04/98)
 * Continuity between LM and IM functions is ensured by setting
 * giant_branch_parameters[16] = lbagbf(mhefl,0.0) with gbp(16) = 1.0.
 */

    
    
    if(m < mhefl)
    {

        double a4 = (giant_branch_parameters[9]*
                     pow(mhefl,giant_branch_parameters[10]) - 
                     giant_branch_parameters[16])/
            (exp(mhefl*giant_branch_parameters[11])*
             giant_branch_parameters[16]);
    

        result = giant_branch_parameters[9]*
            pow(m,
                giant_branch_parameters[10])
            /(1.0 + a4*exp(m*giant_branch_parameters[11]));
    }
    else
    {
        result = (giant_branch_parameters[12] +
                  giant_branch_parameters[13]*
                  pow(m,(giant_branch_parameters[15]+1.8))
            )/
            (giant_branch_parameters[14] + pow(m,giant_branch_parameters[15]));

    }

    return (result);
}
/*****************************************************************/

