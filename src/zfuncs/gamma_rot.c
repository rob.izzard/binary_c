#include "../binary_c.h"
No_empty_translation_unit_warning;



/*
 * Function to calculate the ratio of the luminosity to the
 * Eddington luminosity
 */
Constant_function double gamma_rot(const double mass,
                                   const double luminosity)
{
    const double ikappa = 1.0/0.3; // 0.2=e-scattering, 0.3="in practice"
    const double Ledd=4.0*PI*SPEED_OF_LIGHT*GRAVITATIONAL_CONSTANT*mass*M_SUN*ikappa;
    // in L_sun(cgs)

    return (luminosity * L_SUN/ Ledd);

}
