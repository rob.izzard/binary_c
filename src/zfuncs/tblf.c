#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef BSE
double Pure_function tblf(const double m, /** mass of stellar core? **/
                          const double mhefl,
                          const double mfgb,
                          const double * Restrict const giant_branch_parameters,
                          const Abundance z)
{

    double mr=mhefl/mfgb,m1,m2,r1,result;
    /*
     * A function to evaluate the blue-loop fraction of the He-burning
     * lifetime for IM & HM stars  (OP 28/01/98)
     */
    if(Less_or_equal(m,mfgb))
    {
        m1 = m/mfgb;
        m2 = log10(m1)/log10(mr);
        m2 = Max(m2,1.0e-12);
        result = giant_branch_parameters[64]*pow(m1,giant_branch_parameters[63]) +
            giant_branch_parameters[65]*pow(m2,giant_branch_parameters[62]);
    }
    else
    {
        r1 = 1.0 - rminf(m,giant_branch_parameters)/
            ragbf(m,
                  lheif(m,mhefl,giant_branch_parameters),
                  mhefl,
                  giant_branch_parameters
#ifdef AXEL_RGBF_FIX
                  ,z
#endif
                );
        r1 = Max(r1,1.0e-12);
        result = giant_branch_parameters[66]*pow(m,giant_branch_parameters[67])*
            pow(r1,giant_branch_parameters[68]);
    }
    result = Min(1.0,Max(0.0,result));

    if(result<1.0e-10)
    {
        result=0.0;
    }

    return result;
}

/***********************************************************/


#endif//BSE
