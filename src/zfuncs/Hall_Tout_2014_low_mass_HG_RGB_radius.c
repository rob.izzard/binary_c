#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef HALL_TOUT_2014_RADII

double Pure_function Hall_Tout_2014_low_mass_HG_RGB_radius(struct stardata_t * const stardata,
                                                           struct star_t * const star,
                                                           const double mc)
{

    /*
     * Radius for low-mass HG or RGB stars from
     * Hall and Tout (2014) Appendix A.
     */

    double xi = log10(stardata->common.metallicity / 0.02);

    double c0,c1,c2,c3;
    c0 = Quintic(xi,
                 2.817859,
                 4.331671e-1,
                 -8.152041e-1,
                 -1.329429,
                 -3.502317e-1,
                 0);

    c1 = Quintic(xi,
                 -1.454750e+1,
                 -3.844703e+0,
                 8.279414e+0,
                 1.252224e+1,
                 3.256815e+0,
                 0);

    c2 = Quintic(xi,
                 4.947425e+1,
                 1.369688e+1,
                 -2.513380e+1,
                 -3.773333e+1,
                 -9.791961e+0,
                 0
        );

    c3 = Quintic(xi,
                 -4.914713e+1,
                 -1.326969e+1,
                 4.137217e+1,
                 6.525871e+1,
                 2.617208e+1,
                 3.184125e+0
        );

    double fr = Cubic(mc,c0,c1,c2,c3);

    return fr * rwd(stardata,star,mc,HeWD,FALSE);
}
#endif // HALL_TOUT_2014_RADII
