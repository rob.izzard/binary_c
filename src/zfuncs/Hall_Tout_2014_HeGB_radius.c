#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef HALL_TOUT_2014_RADII

double Pure_function Hall_Tout_2014_HeGB_radius(struct stardata_t * const stardata,
                                                struct star_t * const star,
                                                const double mc)
{

    /*
     * Radius of helium Herztsprung giant stars
     * Hall and Tout (2014)  Eq. 5
     */
    double rc = (2.7-1.129*mc)*rwd(stardata,star,mc,HeHG,FALSE);
    return rc;
}
#endif // HALL_TOUT_2014_RADII
