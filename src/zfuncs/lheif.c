#include "../binary_c.h"
No_empty_translation_unit_warning;



double Pure_function lheif(double m,
                           const double mhefl,
                           const double * Restrict const giant_branch_parameters)
{
    double result;

    /*
     * A function to evaluate He-ignition luminosity  (OP 24/11/97)
     * Continuity between the LM and IM functions is ensured with a first
     * call setting lhefl = lHeIf(mhefl,0.0)
     */
    if(m < mhefl)
    {
        result = (giant_branch_parameters[38]*
                  pow(m,giant_branch_parameters[39]));
#ifndef DISCONTINUOUS_HELIUM_IGNITION
        result /= (1.0 +
                   giant_branch_parameters[41]*
                   exp(m*giant_branch_parameters[40]));
#endif
    }
    else
    {
        result = (giant_branch_parameters[42] +
                  giant_branch_parameters[43]*pow(m,3.8))/
            (giant_branch_parameters[44] + Pow2(m));
    }

    return(result);
}

/*****************************************************************/
