#include "../binary_c.h"
No_empty_translation_unit_warning;

#define p (GB[GB_p])
#define q (GB[GB_q])
#define B (GB[GB_B])
#define D (GB[GB_D])

double Pure_function lgbtf(const double t,
                           const double A,
                           const double * Restrict GB,
                           const double tinf1,
                           const double tinf2,
                           const double tx)
{
    /*
     * A function to evaluate L given t for GB, AGB and NHe stars
     */
    double result;

    if(Less_or_equal(t,tx))
    {
        result = D * pow((p-1) * A * D * (tinf1 - t),
                         -p / (p-1));
    }
    else
    {
        double x = q - 1.0;
        double y = tinf2 - t;

        /*
         * Limit y to prevent inf or nan.
         * This is generally not required, but
         * can be for helium giants, esp. when converging
         * for adpative RLOF.
         */
        y = Max(1e-2,y);

        result = B * pow(x * A * B * y,
                         -q / x);
    }

    return result;

}
/****************************************************/
