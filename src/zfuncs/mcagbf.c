#include "../binary_c.h"
No_empty_translation_unit_warning;


double Pure_function mcagbf(const double m, /** mass **/
                            const double * Restrict const giant_branch_parameters)
{
    double result;

    /*
     * A function to evaluate core mass at the BAGB (OP 25/11/97)
     */
    result = Pow1d4(
        giant_branch_parameters[37] + 
        giant_branch_parameters[35] *
        pow(m,giant_branch_parameters[36])
        );
    return(result);
}

/***********************************************************/



