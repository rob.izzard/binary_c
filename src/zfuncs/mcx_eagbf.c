#include "../binary_c.h"
No_empty_translation_unit_warning;


double Pure_function mcx_eagbf(const double t,
                               const double A,
                               const double * Restrict const GB,
                               const double tinf1,
                               const double tinf2,
                               const double tx)
{
    /*
     * A function to evaluate coremass on the EAGB
     */
    double t1 = Less_or_equal(t,tx) ? tinf1 : tinf2;
    double result = pow(((GB[GB_AXEL_2]-1.0)*A*GB[GB_AXEL_1]*(t1 - t)),
                 (1.0/(1.0-GB[GB_AXEL_2])));
    return(result);
}
/***********************************************************/


