#include "../binary_c.h"
No_empty_translation_unit_warning;


double Constant_function dm_intershell(const double mc)
{
    /*
     * Thickness of the intershell region in an AGB star in Msun
     * according to Iben.
     * (mc is the helium core mass in Msun)
     */

    return(exp10(-1.835+(1.73-2.67*mc)*mc));

}
