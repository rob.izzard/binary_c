#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef BSE
double Pure_function mcheif(const double m,
                            const double mhefl,
                            const double mchefl,
                            const double * Restrict const giant_branch_parameters)
{
    /*
     * A function to evaluate core mass at BGB or He ignition
     * (depending on mchefl) for IM & HM stars  (OP 25/11/97)
     *
     * This is Hurley 2000 Eq. 44 and associated text.
     */
    const double a3 = Pow4(mchefl)-
        giant_branch_parameters[33]*
        pow(mhefl,giant_branch_parameters[34]);
    const double a4 = giant_branch_parameters[33]*
        pow(m,giant_branch_parameters[34]);
    const double mcbagb = mcagbf(m,giant_branch_parameters);
    return Min(0.95*mcbagb,
               sqrt(sqrt(a3+a4)));
}

/***********************************************************/


#endif//BSE
