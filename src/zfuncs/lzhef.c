#include "../binary_c.h"
No_empty_translation_unit_warning;


Constant_function double lzhef(const double m)
{
    /*
     * A function to evaluate Naked Helium star 'ZAMS' luminosity
     */
    const double m15 = Pow1p5(m);
    const double l = 1.5262e4 * Pow2(Pow5(m)) * Pow1d4(m)/
        ( 0.0469 + Pow6(m) * ( 31.18 + m15*( 29.54 + m15 )));
    return(l);
}
/*****************************************************************/
