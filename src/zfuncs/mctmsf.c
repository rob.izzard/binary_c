#include "../binary_c.h"
No_empty_translation_unit_warning;


Constant_function double mctmsf(const double m)
{
    /*
     * A function to evaluate core mass at the end of the MS as a 
     * fraction of the BGB value, i.e. this must be multiplied by 
     * the BGB value (see below) to give the actual core mass (JH 5/9/99)
     *
     * This is Eq.30 in Hurley+ 2000
     */
    const double mm=pow(m,5.25);
    return ((1.586 + mm)/(2.434 + 1.02*mm));
}


