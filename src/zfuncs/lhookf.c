#include "../binary_c.h"
No_empty_translation_unit_warning;


double Pure_function lhookf(const double m,
                            const double mhook, 
                            const double * Restrict const main_sequence_parameters)
{
    double result;

    if(Less_or_equal(m,mhook))
    {
	result = 0.0;
    }
    else if(More_or_equal(m,main_sequence_parameters[51]))
    {
	result=Min(
	    (main_sequence_parameters[47]*pow(m,-main_sequence_parameters[48])),
	    (main_sequence_parameters[49]*pow(m,-main_sequence_parameters[50]))
	    );
    }
    else
    {
        result=main_sequence_parameters[121]*pow(((m-mhook)/(main_sequence_parameters[51]-mhook)),0.4);
    }

    return (result);
}
/****************************************************************/


