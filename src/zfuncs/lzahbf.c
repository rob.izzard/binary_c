#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef BSE
double Pure_function lzahbf(const double m, 
                            const double mc, 
                            const double mhefl, 
                            const double * Restrict const giant_branch_parameters)
{
    double a4,a5,mm,result;

    /*
     * A function to evaluate the ZAHB luminosity for LM stars. (OP 28/01/98)
     * Continuity with LHe,min for IM stars is ensured by setting
     * lx = lHeif(mhefl,z,0.0,1.0)*lHef(mhefl,z,mfgb), and the call to lzhef
     * ensures continuity between the ZAHB and the NHe-ZAMS as Menv -> 0.
     */

    a5 = lzhef(mc);
    a4 = (giant_branch_parameters[GBP_LZAHBF1] + a5)*exp((m-mhefl)*giant_branch_parameters[71])/
        (giant_branch_parameters[74] - a5);

    mm = Max((m-mc)/(mhefl - mc),1.0e-12);

    result = a5 + giant_branch_parameters[GBP_LZAHBF2]*pow(mm,giant_branch_parameters[70])/
        ((1.0 + giant_branch_parameters[72]*pow(mm,giant_branch_parameters[73]))*(1.0 + a4));

    return(result);
}
/***********************************************************/


#endif//BSE
