#include "../binary_c.h"
No_empty_translation_unit_warning;


Constant_function double lpertf(const double m,
                                const double mu)
{
    /*
     * A function to obtain the exponent that perturbs luminosity.
     */
    const double C = 3.0;
    const double b = 0.002*Max(1.0,(2.5/m));
    const double f = mu / b;
    const double fC = pow(f,C);
    return fC*(1.0 + pow(b,C)) / (1.0 + fC);
}
