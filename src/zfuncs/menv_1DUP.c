#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef FIRST_DREDGE_UP_HOLLY
#include "../tables/table_Holly_1DUP.h"
#endif

double menv_1DUP(struct stardata_t * Restrict const stardata Maybe_unused,
                 const double m Maybe_unused,
                 const double z Maybe_unused)
{
    /*
     * Mass co-ordinate at the base of the convective envelope
     * at the maximum extent of the convective envelope
     * during 1st dredge up as a function of initial mass
     * and metallicity.
     */
#ifdef FIRST_DREDGE_UP_EVERT
    if(m>15.0)
    {
        /* no 1st DUP in massive stars */
        return (0.0);
    }
    else
    {
        /* Evert's data */
        double x[2]={log10(z/0.02),m}; // parameters
        double r[1]; // results
        Interpolate(stardata->store->Evert_1DUP_table,
                    x,
                    r,
                    TRUE);
        /*
         * Table is limited to 0.5 Msun, so below this scale the result
         * with the total mass.
         */
        if(m<0.5) r[0] *= m/0.5;
    }

    return(r[0]);

#endif // FIRST_DREDGE_UP_EVERT

#ifdef FIRST_DREDGE_UP_HOLLY

    /*
     * Data table columns:
     * Metallicity,
     * Total Mass,
     * Initial Carbon,
     * Final Carbon,
     * Initial Nitrogen,
     * Final Nitrogen,
     * Mass co-ordinate of first dredge up
     * logg
     */

    // parameters
    double x[NPARAM];
    x[PARAM_METALLICITY] = log10(z);
    x[PARAM_MASS] = m;

    // results
    double r[NDATA];

    // do interpolation on Holly's data table
    Interpolate(stardata->store->Holly_1DUP_table,
                x,
                r,
                FALSE);

    /*
     * No data below 0.8Msun or above 20Msun, so scale results
     */
    if(m<0.8)
    {
        r[DATA_MCONV] *= m/0.8;
    }
    else if(m>20.0)
    {
        r[DATA_MCONV] *= m/20.0;
    }

#ifdef FIRST_DREDGE_UP_HOLLY_TESTING
    if(m>0.2 && 1)
        printf("HOLLY dredge up depth: M=%g Z=%g Mcoord=%g : Pre C=%g N=%g : Post C=%g N=%g [C/N]=%g\n",
               m,
               z,
               // max mass coord of 1DUP
               r[DATA_MCONV],
               // pre/post C,N
               r[DATA_INITIAL_CARBON],
               r[DATA_INITIAL_NITROGEN],
               r[DATA_FINAL_CARBON],
               r[DATA_FINAL_NITROGEN],
               log10(r[DATA_FINAL_CARBON]/r[DATA_FINAL_NITROGEN]) - log10(r[DATA_INITIAL_CARBON]/r[DATA_INITIAL_NITROGEN]) // [C/N]
            );
#endif

    return r[DATA_MCONV];
#endif // FIRST_DREDGE_UP_HOLLY


    /* no idea... return something silly */
    return -666.0;
}
