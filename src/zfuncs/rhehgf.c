#include "../binary_c.h"
No_empty_translation_unit_warning;


Constant_function double rhehgf(const double m,
                                const double lum,
                                const double rx,
                                const double lx)
{
    /*
     * A function to evaluate Helium star radius on the Hertzsprung gap
     * from its mass and luminosity.
     */
    const double m25 = Pow2p5(m);
    double result = 2e-3 * m25 / (2.0 + Pow2(m25)); // lambda in Eq.87
    result = rx*pow(lum/lx, 0.2) +
        0.020*(exp(result*lum) - exp(result*lx));
    result = Min(1e7,result);
    return result;
}
