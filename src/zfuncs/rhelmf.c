#include "../binary_c.h"
No_empty_translation_unit_warning;


double rhelmf(Stellar_type * const stellar_type,
              const double m, 
              const double lum, 
              const double rx, 
              const double lx)
{

    double ragb,rhe,cm;
    double result;
    /*
     * A function to evaluate Helium star radius from its mass and luminosity
     */
    ragb = 0.080 * Pow3d4(lum);
    cm = 2.0E-03*Pow2p5(m)/(2.0 + Pow5(m));
    rhe = rx*pow((lum/lx),0.2) + 0.02*(exp(cm*lum) - exp(cm*lx));
    if(rhe<ragb)
    { 
        *stellar_type = HeHG;
        result = rhe;
    }
    else
    {
        result = ragb;
    }

    return (result);

}

/*****************************************/



