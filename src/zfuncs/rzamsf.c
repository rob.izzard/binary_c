#include "../binary_c.h"
No_empty_translation_unit_warning;


double Pure_function rzamsf(const double m,
                            const double * Restrict const main_sequence_parameters)
{

    /*
     * A function to evaluate Rzams
     * ( from Tout et al., 1996, MNRAS, 281, 257 ).
     */

    /* Rob 03/04/2007 : rewritten to reduce pow(x,y) calls */

    const double m2 = m*m; // m^2
    const double m05 = sqrt(m); // m^0.5
    const double m25 = m05*m2; // m^2.5
    const double m65 = m25*m2*m2; // m ^ 6.5
    const double m90 = m25*m65; // m ^ 9
    const double m190 = m90*m90*m; // m ^ 19
    const double m195 = m190*m05; // m ^ 19.5


    // and pow(m,18.5) = m195/m=m195*im
    // and pow(m,8.5) = m90/m05 = m90*im05
    // and pow(m,11.0) = m25*m25*m65/m05
    //                 = m25*m25*m65*im05=m25*m90*im05
    const double rzams =
        (main_sequence_parameters[8]*m25 +
         main_sequence_parameters[9]*m65 +
         main_sequence_parameters[10]*m2*m90 + // m ^ 11
         main_sequence_parameters[11]*m190 +
         main_sequence_parameters[12]*m195)
        /
        (main_sequence_parameters[13] +
         m2*(main_sequence_parameters[14] +
             m65*(main_sequence_parameters[15] + // m^8.5
                  m90*m))+ // m^18.5
         main_sequence_parameters[16]*m195);


    /*
    printf("rzams(m=%g) m2=%g m05=%g m25=%g m65=%g m90=%g m190=%g m195=%g [MSP=%p 8=%g 9=%g 10=%g 11=%g 12=%g 13=%g 14=%g 15=%g 16=%g] -> %g\n",
           m,m2,m05,m25,m65,m90,m190,m195,
           (void*)main_sequence_parameters,
           main_sequence_parameters[8],
           main_sequence_parameters[9],
           main_sequence_parameters[10],
           main_sequence_parameters[11],
           main_sequence_parameters[12],
           main_sequence_parameters[13],
           main_sequence_parameters[14],
           main_sequence_parameters[15],
           main_sequence_parameters[16],
           rzams);
    */

    return rzams;
}
/***********************************************************/
