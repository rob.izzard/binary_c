

#include "../binary_c.h"
No_empty_translation_unit_warning;
#undef is_integer
#ifndef __HAVE_LIBCDICT__
#include "cdict.h"
#undef exit

#include <string.h>
#undef exit
#undef nestprint

/*
 * Nest data in a cdict structure.
 *
 * Returns the entry that is set, or NULL on error.
 */

#define nestprint(...)                          \
    if((cdict &&                                \
        (cdict->ancestor->vb==TRUE ||           \
         cdict->vb==TRUE)                       \
           ) )                                  \
    {                                           \
        printf(__VA_ARGS__);                    \
        fflush(stdout);                         \
    }
#undef nestprint
#define nestprint(...) /* do nothing */
//#define nestprint(...) {printf(__VA_ARGS__);fflush(stdout);}

static char * _key_paths_string(struct cdict_t * const cdict __CDict_maybe_unused,
                                union cdict_key_union * paths __CDict_maybe_unused,
                                const size_t npath __CDict_maybe_unused,
                                CDict_key_type * pathtypenums __CDict_maybe_unused);

CDict_API_function
struct cdict_entry_t *
cdict_nest(struct cdict_t * const cdict __CDict_maybe_unused,
           union cdict_key_union * paths __CDict_maybe_unused,
           const size_t npath __CDict_maybe_unused,
           CDict_key_type * pathtypenums __CDict_maybe_unused,
           union cdict_key_union key __CDict_maybe_unused,
           CDict_key_type keytype __CDict_maybe_unused,
           union cdict_value_union value __CDict_maybe_unused,
           const cdict_size_t nvalue,
           CDict_value_type valuetype __CDict_maybe_unused,
           void * metadata __CDict_maybe_unused,
           cdict_metadata_free_f metadata_free_function __CDict_maybe_unused,
           const CDict_action_type action
    )
{
    /*
     * Given a path in a cdict, append data.
     *
     * The path is a list of strings, e.g.
     *
     * "x", "y", "z"
     *
     * and each of those strings should point to a cdict.
     */
    nestprint("\n\n\ncdict_nest in %p, npath %zu, action %d = %s, paths = %p, pathtypenums = %p\n",
              (void*)cdict,
              npath,
              action,
              __CDict_action_string(action),
              (void*)paths,
              (void*)pathtypenums);

    struct cdict_t * h = NULL;
    const Boolean vb = cdict && cdict->vb;

    if(vb)
    {
        nestprint("Start input list\n");
        for(size_t i=0;i<npath;i++)
        {
            char * s = NULL;
            cdict_union_to_string(cdict,
                                  &paths[i],
                                  pathtypenums[i],
                                  NULL,
                                  &s);
            nestprint("%zu = %s \"%s\"\n",
                      i,
                      __CDict_key_descriptor(pathtypenums[i]),
                      s);
            CDict_Safe_free(cdict,s);
        }
        //nestprint("<data> ");
        {
            char * s = NULL;
            cdict_union_to_string(cdict,
                                  &key,
                                  keytype,
                                  NULL,
                                  &s);
            nestprint("key = \"%s\" : ",s);
            CDict_Safe_free(cdict,s);
        }
        {
            char * s = NULL;
            cdict_union_to_string(cdict,
                                  (union cdict_key_union*)&value,
                                  valuetype,
                                  NULL,
                                  &s);
            nestprint("value = \"%s\" \n",s);
            CDict_Safe_free(cdict,s);
        }
        nestprint("End input list\n");
    }


    /*
     * If not found in the cache, enter nested location
     * into the cdict
     */
    struct cdict_entry_t * entry = NULL;
    int isnew = -1;
    if(h == NULL)
    {
        /*
         * Check that the nest path exists
         */
        h = cdict;
        for(size_t i=0;i<npath;i++)
        {
            entry = cdict_contains(h,
                                   paths[i],
                                   pathtypenums[i]);

            nestprint("Nest level %zu : matching entry ? %s (at %p)\n",
                      i,
                      entry?"Yes":"No",
                      (void*)entry);
            if(entry != NULL)
            {
                /*
                 * Found an entry, but is it a cdict?
                 */
                if(entry->value.type == CDICT_DATA_TYPE_CDICT)
                {
                    /*
                     * we found a cdict
                     */
                    h = entry->value.value.cdict_pointer_data;
                    nestprint("Nest: At %zu : found cdict %p \n",
                              i,
                              (void*)h);
                }
                else
                {
                    char * svalue = NULL;
                    cdict_key_and_value_strings(h,
                                                &entry->key,
                                                &entry->value,
                                                &svalue);
                    char * skey = entry->key.string;
                    cdict_entry_to_value_string(h,
                                                entry,
                                                &svalue,
                                                NULL);
                    char * error_string = NULL;
                    if(cdict_asprintf(cdict,
                                      &error_string,
                                      "cdict_nest %d: When searching for a nested location in cdict at %p, I failed to find another cdict, instead I found an entry (at %p) with key = \"%s\" (type %d), value = \"%s\" (type %d, a cdict would be %d). This is a fatal error, as I cannot overwrite this (non-cdict) entry with a cdict.",
                                      __LINE__,
                                      (void*)cdict,
                                      (void*)entry,
                                      skey,
                                      entry->key.type,
                                      svalue,
                                      entry->value.type,
                                      CDICT_DATA_TYPE_CDICT)>0)
                    {
                        printf("%s : key paths %s\n",
                               error_string,
                               _key_paths_string(cdict,paths,npath,pathtypenums));

                        exit(0);
                        *(int*)0=0;

                        cdict_error(cdict,
                                    CDICT_ERROR_NEST_FOUND_NO_CDICT,
                                    error_string);
                    }
                    else
                    {
                        fprintf(stderr,
                                "cdict_nest %d: When searching for a nested location in cdict at %p, I failed to find another cdict, instead I found an entry (at %p) with key = \"%s\" (type %d), value = \"%s\" (type %d, a cdict would be %d). This is a fatal error, as I cannot overwrite this (non-cdict) entry with a cdict.",
                                __LINE__,
                                (void*)cdict,
                                (void*)entry,
                                skey,
                                entry->key.type,
                                svalue,
                                entry->value.type,
                                CDICT_DATA_TYPE_CDICT);
                        cdict_error(cdict,
                                    CDICT_ERROR_NEST_FOUND_NO_CDICT,
                                    "Please see output to stderr");
                    }
                    CDict_Safe_free(cdict,svalue);
                }
            }
            else
            {
                /*
                 * No cdict found: make a new cdict (new_cdict)
                 * with h as the parent (this automatically
                 * sets its ancestor).
                 */
                if(vb && isnew == -1)
                {
                    isnew = i;
                }
                CDict_new(new_cdict,h);
                CDict_set_with_types(h,
                                     paths[i],
                                     pathtypenums[i],
                                     new_cdict,
                                     CDICT_DATA_TYPE_CDICT);
                h = new_cdict;
                nestprint("Nest: At %zu : no cdict found, made cdict %p\n",i,(void*)h);
            }
        }
    }

    /*
     * Now, we set the data in h
     */
    struct cdict_entry_t * changed_entry;
    if(action == CDICT_NEST_ACTION_APPEND)
    {
        nestprint("Nest: append to nested cdict %p\n",(void*)h);
        changed_entry = cdict_append(h,
                                     key,
                                     keytype,
                                     value,
                                     nvalue,
                                     valuetype,
                                     metadata,
                                     metadata_free_function);
    }
    else
    {
        nestprint("Nest: set nested cdict %p\n",(void*)h);
        changed_entry = cdict_set(h,
                                  key,
                                  keytype,
                                  value,
                                  nvalue,
                                  valuetype,
                                  metadata,
                                  metadata_free_function);
    }



    if(vb &&
       isnew != -1)
    {
        for(size_t i=0;i<npath;i++)
        {
            char * s = NULL;
            cdict_union_to_string(cdict,
                                  &paths[i],
                                  pathtypenums[i],
                                  NULL,
                                  &s);
            if((int)i >= isnew)
            {
                nestprint("%sNEW: %zu = %s \"%s\"%s -> ",
                       CDICT_YELLOW,
                       i,
                       __CDict_key_descriptor(pathtypenums[i]),
                       s,
                       CDICT_COLOUR_RESET);
            }
            else
            {
                nestprint("OLD: %zu = %s \"%s\" -> ",
                       i,
                       __CDict_key_descriptor(pathtypenums[i]),
                       s);
            }
            CDict_Safe_free(cdict,s);
        }
        nestprint("<data>\n");
    }
    nestprint("Nest: done cdict_nest\n\n\n");

    return changed_entry;
}

static char * _key_paths_string(struct cdict_t * const cdict __CDict_maybe_unused,
                                union cdict_key_union * paths __CDict_maybe_unused,
                                const size_t npath __CDict_maybe_unused,
                                CDict_key_type * pathtypenums __CDict_maybe_unused)
{
    char * string = NULL;
    int string_len = 0;
    for(size_t i=0; i<npath;i++)
    {
        char * s = NULL;
        cdict_union_to_string(cdict,
                              &paths[i],
                              pathtypenums[i],
                              NULL,
                              &s);
        nestprint("key = \"%s\" : ",s);
        char * chunk;
        const int chunk_len = cdict_asprintf(cdict,
                                             &chunk,
                                             "[%zu {%s}]=%s%s",
                                             i,
                                             __CDict_key_descriptor(pathtypenums[i]),
                                             s,
                                             i!=npath-1 ? "->" : "");
        if(i==0)
        {
            string = chunk;
            string_len = chunk_len;
        }
        else if(chunk_len>0)
        {
            const size_t newsize =
                (string_len + chunk_len + 1) * sizeof(char);
            char * new_string = realloc(string, newsize);
            if(new_string)
            {
                string = new_string;
                memcpy(string + string_len, // ends just before original \0 in chunk
                       chunk,
                       chunk_len + 1); // +1 to include \0
                string_len += chunk_len;
            }
            else
            {
                cdict_error(cdict,
                            CDICT_ERROR_ASPRINTF_FAILED,
                            "asprintf or realloc failed when trying to realloc string.");
            }
        }
        else
        {
            free(string);
            return NULL;
        }
    }
    return string;
}

#endif // __HAVE_LIBCDICT__

typedef int prevent_ISO_C_warning;
        