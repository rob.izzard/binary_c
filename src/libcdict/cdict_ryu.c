

#include "../binary_c.h"
No_empty_translation_unit_warning;
#undef is_integer
#ifndef __HAVE_LIBCDICT__
#include "cdict.h"
#undef exit


#ifdef CDICT_USE_RYU
#include "ryu/ryu.h"
#include <math.h>
/*
 * Functions to combine cdict's memory allocation
 * with ryu
 */

char * cdict_ryu_d2s(struct cdict_t * const cdict,
                      double f)
{
    char * const result = (char *) __CDict_malloc(cdict,26);
    d2s_buffered(f, result);
    return result;
}

char * cdict_ryu_f2s(struct cdict_t * const cdict,
                      float f)
{
    char * const result = (char *) __CDict_malloc(cdict,16);
    f2s_buffered(f, result);
    return result;
}

CDict_API_function
int cdict_ryu_d2s_asprintf(struct cdict_t * const cdict,
                           char ** const s,
                           const double f)
{
    *s = __CDict_malloc(cdict,
                        CDICT_RYU_STRING_LENGTH_DOUBLE);
    int n;
    if(fabs(f)<10.0 && f==(int)f)
    {
        if(f>=0.0)
        {
            (*s)[0] = (int)f+'0';
            (*s)[1] = '.';
            (*s)[2] = '0';
            n=3;
        }
        else
        {
            (*s)[0] = '-';
            (*s)[1] = -(int)f+'0';
            (*s)[2] = '.';
            (*s)[3] = '0';
            n=4;
        }
    }
    else
    {
        n = d2s_buffered_n(f,*s);
    }
    *(*s+n) = '\0';
    return n;
}

CDict_API_function
int cdict_ryu_f2s_asprintf(struct cdict_t * const cdict,
                            char ** const s,
                            float f)
{
    *s = __CDict_malloc(cdict,
                       CDICT_RYU_STRING_LENGTH_FLOAT);
    int n;
    if(f==0.0)
    {
        (*s)[0] = '0';
        n=1;
    }
    else if(f == 1.0)
    {
        (*s)[0] = '1';
        n=1;
    }
    else
    {
        n = f2s_buffered_n(f,*s);
    }
    *(*s+n) = '\0';
    return n;
}

char * cdict_ryu_d2fixed(struct cdict_t * const cdict,
                          double d,
                          uint32_t precision)
{
    char * const buffer = (char *)__CDict_malloc(cdict,2000);
    const int index = d2fixed_buffered_n(d, precision, buffer);
    buffer[index] = '\0';
    return buffer;
}

char * cdict_ryu_d2exp(struct cdict_t * const cdict,
                        double d,
                        uint32_t precision)
{
    char * const buffer = (char *)__CDict_malloc(cdict,2000);
    const int index = d2exp_buffered_n(d, precision, buffer);
    buffer[index] = '\0';
    return buffer;
}

#endif // CDICT_USE_RYU

#endif // __HAVE_LIBCDICT__

typedef int prevent_ISO_C_warning;
        