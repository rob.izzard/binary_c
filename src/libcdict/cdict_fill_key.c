

#include "../binary_c.h"
No_empty_translation_unit_warning;
#undef is_integer
#ifndef __HAVE_LIBCDICT__
#include "cdict.h"
#undef exit


/*
 * Fill key data in a given key
 *
 * If do_map is True, we also set the map and
 * its format string, but we
 * give this as an option to speed up execution in
 * cases where you don't care about the map (e.g. when
 * simply looking for a key rather than sorting keys)
 */
void cdict_fill_key(struct cdict_key_t * const key,
                    struct cdict_t * const cdict,
                    const union cdict_key_union keydata,
                    const CDict_key_type keytype,
                    const Boolean do_map)
{
    key->key = keydata;
    key->type = keytype;
    key->string = NULL;
    key->format = __CDict_generic_format_string(key->type);
    if(do_map || cdict->force_maps)
    {
        cdict_set_map(cdict,
                      key,
                      keytype,
                      NULL);
    }
    if(cdict->vb)
    {
        printf("cdict_fill_key: Set key string \"%s\" len %zu, key->key = %s\n",
               key->string,
               strlen(key->string),
               keytype == CDICT_DATA_TYPE_STRING ? key->key.string_data : NULL);
    }
}

#endif // __HAVE_LIBCDICT__

typedef int prevent_ISO_C_warning;
        