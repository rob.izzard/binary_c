

#include "../binary_c.h"
No_empty_translation_unit_warning;
#undef is_integer
#ifndef __HAVE_LIBCDICT__
#include "cdict.h"
#undef exit


/*
 * As cdict_set_with_formats but for an array with n elements.
 */

__CDict_Nonnull_some_arguments(1)
CDict_API_function
struct cdict_entry_t * cdict_set_array_with_formats(
    struct cdict_t * const cdict,
    const union cdict_key_union key,
    const CDict_key_type keytype,
    const char * const keyformat,
    const union cdict_value_union value,
    const cdict_size_t nvalue,
    const CDict_value_type valuetype,
    const char * const valueformat,
    const void * const metadata,
    cdict_metadata_free_f metadata_free_function,
    const size_t n)
{
    struct cdict_entry_t * e =
        cdict_set_with_formats(cdict,
                               key,
                               keytype,
                               keyformat,
                               value,
                               nvalue,
                               valuetype,
                               valueformat,
                               metadata,
                               metadata_free_function);
    e->value.count = n;
    return e;
}

#endif // __HAVE_LIBCDICT__

typedef int prevent_ISO_C_warning;
        