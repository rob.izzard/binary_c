#include "../binary_c.h"
        #ifndef __HAVE_LIBCDICT__
        
#pragma once
#ifndef CDICT_H
#define CDICT_H
/*
 * Main header file of the cdict library.
 *
 * Please see the README file for documentation or
 *
 * https://gitlab.com/rob.izzard/libcdict
 */

#include <float.h>
#include "cdict_types.h"
#include "cdict_macros.h"
#include "uthash_libcdict.h"

#include "cdict_prototypes.h"
#include "cdict_API.h"

#define CDICT_VERSION "1.52"

/*
 * Global variables - very bad, I know, but these
 * names are very, very likely to be unique, and
 * thread safety is very likely not required.
 */
extern long double cdict_absolute_tolerance;
extern long double cdict_relative_tolerance;
extern size_t cdict_hashv_float_nbytes;
extern size_t cdict_hashv_double_nbytes;
extern size_t cdict_hashv_long_double_nbytes;
extern int fill_key_do_map;

#endif // CDICT_H

#endif // __HAVE_LIBCDICT__

typedef int prevent_ISO_C_warning;
       