

#include "../binary_c.h"
No_empty_translation_unit_warning;
#undef is_integer
#ifndef __HAVE_LIBCDICT__
#include "cdict.h"
#undef exit


#include <stdio.h>


struct cdict_keystats_t {
    size_t nkeys;
    size_t nvalues;
    size_t max_depth;
};

/*
 * cdict_stats outputs information about the cdict,
 * and (if enabled) logs memory allocations and frees.
 *
 * You can call it with a char * prepend_string which is prepended
 * to the output, allowing just one (thread-safe) printf
 * call. If this is NULL it is ignored.
 *
 * You can send the output to a (char**) outstring instead of
 * printing it to stdout which is the default if outstring is NULL.
 */

static void cdict_stats_recurse(struct cdict_t * const cdict,
                                 struct cdict_keystats_t * const stats,
                                 const unsigned int nesting_level);

CDict_API_function
int cdict_stats(struct cdict_t * const cdict,
                const char * const prepend_string,
                char ** outstring)
{
    struct cdict_keystats_t keystats;
    size_t nesting_level = 0;
    keystats.nkeys = 0;
    keystats.nvalues = 0;
    keystats.max_depth = 0;

#define _output(...)                                            \
    {                                                           \
        char * output;                                          \
        if(asprintf(&output,                                    \
                    __VA_ARGS__)==0)                            \
        {                                                       \
            cdict_error(cdict,                                  \
                        CDICT_ERROR_ASPRINTF_FAILED,            \
                        "asprintf failed in cdict_stats");      \
        }                                                       \
        if(output)                                              \
        {                                                       \
            if(outstring != NULL)                               \
            {                                                   \
                *outstring = output;                            \
            }                                                   \
            else                                                \
            {                                                   \
                if(fputs(output,stdout)<=0)                     \
                {                                               \
                    cdict_error(cdict,                          \
                                CDICT_ERROR_FPUTS_FAILED,       \
                                "fputs failed in cdict_stats"); \
                }                                               \
                free(output);                                   \
            }                                                   \
        }                                                       \
    }


    if(cdict != NULL)
    {
        if(cdict != cdict->ancestor)
        {
            _output("%sCdict %p is not its own ancestor (which is %p): find the ancestor and call cdict_stats() on it.\n",
                   prepend_string != NULL ? prepend_string : "",
                   (void*)cdict,
                   (void*)cdict->ancestor
                );
        }
        else
        {
            cdict_stats_recurse(cdict,
                                 &keystats,
                                 nesting_level);
            char * alloc_string = NULL;
            if(cdict->stats != NULL)
            {
                if(asprintf(&alloc_string,
                            ": (count size) malloc %zu %zu calloc %zu %zu realloc %zu %zu asprintf %zu %zu free %zu ",
                            cdict->stats->malloc_count,
                            cdict->stats->malloc_size,
                            cdict->stats->calloc_count,
                            cdict->stats->calloc_size,
                            cdict->stats->realloc_count,
                            cdict->stats->realloc_size,
                            cdict->stats->asprintf_count,
                            cdict->stats->asprintf_size,
                            cdict->stats->free_count
                       )<1)
                {
                    alloc_string = NULL;
                }
            }

            _output("%sCdict %p (ancestor %p, stats %p) : nkeys %zu, nvalues %zu, max_depth %zu %s\n",
                    prepend_string != NULL ? prepend_string : "",
                    (void*)cdict,
                    (void*)cdict->ancestor,
                    (void*)cdict->stats,
                    keystats.nkeys,
                    keystats.nvalues,
                    keystats.max_depth,
                    alloc_string != NULL ? alloc_string : " "
                );

            if(cdict->stats != NULL)
            {
                cdict_alloc_stats(cdict);
                if(alloc_string != NULL)
                {
                    free(alloc_string);
                }
            }
        }
    }
    else
    {
        _output("Cdict NULL : stats impossible\n");
    }
    return 0;
}

static void cdict_stats_recurse(struct cdict_t * const cdict,
                                 struct cdict_keystats_t * const keystats,
                                 const unsigned int nesting_level)
{
    keystats->max_depth = __CDict_Max(keystats->max_depth,
                                     nesting_level);
    CDict_loop(cdict,entry)
    {
        keystats->nkeys++;

        if(entry->value.type == CDICT_DATA_TYPE_CDICT &&
           entry->value.value.cdict_pointer_data != NULL)
        {
            keystats->nvalues++;
            /*
             * Require a recursive call
             */
            cdict_stats_recurse(entry->value.value.cdict_pointer_data,
                                 keystats,
                                 nesting_level+1);
        }
        else if(__CDict_datatype_is_array(entry->value.type))
        {
            if(entry->value.count > 0)
            {
                for(cdict_size_t i=0;i<entry->value.count;i++)
                {
                    keystats->nvalues++;
                }
            }
        }
        else
        {
            /*
             * Scalar type
             */
            keystats->nvalues++;
        }
    }
}

#endif // __HAVE_LIBCDICT__

typedef int prevent_ISO_C_warning;
        