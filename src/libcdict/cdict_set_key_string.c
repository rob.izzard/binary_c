

#include "../binary_c.h"
No_empty_translation_unit_warning;
#undef is_integer
#ifndef __HAVE_LIBCDICT__
#include "cdict.h"
#undef exit


#include <stdio.h>

__CDict_Nonnull_some_arguments(1,2)
CDict_API_function
int cdict_set_key_string(struct cdict_t * const cdict,
                         struct cdict_key_t * const key,
                         const char * const format)
{
    /*
     * Set key->string based on the data in the key.
     *
     * If format (passed in) is set, use it.
     * If key->format is non-NULL, use it.
     * Otherwise use the default format string.
     *
     * On return:
     *
     * If key->string is non-NULL:
     * We use asprintf to make the string if required,
     * and return the number of characters (not including
     * the trailing null character) in the string.
     *
     * If key->string is NULL:
     * We return CDICT_ERROR_TO_STRING_TYPE_FAILED.
     *
     * The key->string will be freed
     * automatically when the cdict is freed.
     */
    int ret = 0;
    const Boolean vb = FALSE;

    if(vb)printf("key_to_key_string : key=%p (type %d) string=%p: format \"%s\" vs key->format \"%s\" ",
                 (void*)key,
                 key->type,
                 (void*)key->string,format,key->format);

    if(key->string == NULL ||
       format==NULL ||
       (format!=NULL &&
        key->format!=NULL &&
        strcmp(format,key->format)!=0 ))
    {
        /*
         * Choose the format we'd like to use
         */
        const char * const use_format =
            format != NULL ? format :
            key->format != NULL ? key->format :
            __CDict_generic_format_string(key->type);

        if(vb)printf("key_to_key_string : USE FORMAT \"%s\"\n",use_format);

        if(key->string != NULL)
        {
            /*
             * Remove the previous key string
             */
            CDict_Safe_free(cdict,key->string);
        }

        /*
         * Make the string
         */
        if(use_format != NULL)
        {
            if(vb)printf("key_to_key_string : Key type %d = %s\n",key->type,__CDict_key_descriptor(key->type));
            ret = cdict_union_to_string(cdict,
                                        &key->key,
                                        key->type,
                                        use_format,
                                        &key->string);
        }
        else
        {
            key->string = NULL;
            ret = CDICT_ERROR_TO_STRING_TYPE_FAILED;
            if(vb)printf("key_to_key_string : string type failed\n");
        }
    }

    return ret;
}

#endif // __HAVE_LIBCDICT__

typedef int prevent_ISO_C_warning;
        