

#include "../binary_c.h"
No_empty_translation_unit_warning;
#undef is_integer
#ifndef __HAVE_LIBCDICT__
#include "cdict.h"
#undef exit


CDict_API_function
void cdict_clean_cdict(struct cdict_t * c)
{
    /*
     * Clean the contents of cdict c
     */
    if(c != NULL)
    {
        c->tofree = NULL;
        c->cdict_entry_list = NULL;
        c->parent = NULL;
        c->ancestor = NULL;
        c->stats = NULL;
        c->error_handler = NULL;
        c->alloced = 0;
        c->max_size = 0;
        c->tofree_n = 0;
        c->vb = FALSE;
        c->use_cache = TRUE;
        c->force_maps = TRUE;
    }
}

#endif // __HAVE_LIBCDICT__

typedef int prevent_ISO_C_warning;
        