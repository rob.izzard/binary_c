

#include "../binary_c.h"
No_empty_translation_unit_warning;
#undef is_integer
#ifndef __HAVE_LIBCDICT__
#include "cdict.h"
#undef exit



void cdict_free_stats(struct cdict_t * const cdict)
{
    /*
     * Free stats memory : this can only be done
     * if the cdict is its own ancestor, and if cdict->stats
     * is allocated.
     */
    if(cdict != NULL &&
       cdict == cdict->ancestor &&
       cdict->stats != NULL)
    {
        if(cdict->ancestor->stats->pointer_list != NULL)
        {
            for(size_t i=0; i<cdict->ancestor->stats->pointer_list_n; i++)
            {
                if(cdict->stats->pointer_list[i] != NULL)
                {
                    free(cdict->stats->pointer_list[i]);
                }
            }
            cdict->stats->pointer_list_n = 0;
            __CDict_Safe_free(cdict,
                              cdict->stats->pointer_list);
        }
        __CDict_Safe_free(cdict,
                          cdict->stats);
    }
}

#endif // __HAVE_LIBCDICT__

typedef int prevent_ISO_C_warning;
        