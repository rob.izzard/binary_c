

#include "../binary_c.h"
No_empty_translation_unit_warning;
#undef is_integer
#ifndef __HAVE_LIBCDICT__
#include "cdict.h"
#undef exit


/*
 * Return the size of libcdict type n (in bytes, i.e.
 * a size_t).
 */

#undef X
#define X(                                              \
    TYPE,                                               \
    MEMBER,						\
    CTYPE,						\
    DEFCTYPE,                                           \
    CARRAY,                                             \
    DESCRIPTOR,                                         \
    FORMAT,                                             \
    DEREF_FORMAT,                                       \
    DEREF_CTYPE,                                        \
    GROUP,                                              \
    ARRAY_TO_SCALAR_MAPPER,                             \
    SCALAR_TO_ARRAY_MAPPER,                             \
    NUMERIC,                                            \
    DEMO                                                \
    )                                                   \
    CDICT_DATA_TYPE_##SCALAR_TO_ARRAY_MAPPER##_ARRAY,

CDict_data_type __CDict_map_scalar_type_to_array_type(const CDict_data_type n)
{
    static const CDict_data_type __types[] = {
        __CDICT_DATA_TYPES__
    };

    return
        __CDict_group(n) == CDICT_GROUP_SCALAR ?
        __types[n] :
        CDICT_DATA_TYPE_UNKNOWN;

}
#undef X

#endif // __HAVE_LIBCDICT__

typedef int prevent_ISO_C_warning;
        