

#include "../binary_c.h"
No_empty_translation_unit_warning;
#undef is_integer
#ifndef __HAVE_LIBCDICT__
#include "cdict.h"
#undef exit


/*
 * Return the size of libcdict type n (in bytes, i.e.
 * a size_t).
 */

#undef X
#define X(                                              \
    TYPE,                                               \
    MEMBER,						\
    CTYPE,						\
    DEFCTYPE,                                           \
    CARRAY,                                             \
    DESCRIPTOR,                                         \
    FORMAT,                                             \
    DEREF_FORMAT,                                       \
    DEREF_CTYPE,                                        \
    GROUP,                                              \
    ARRAY_TO_SCALAR_MAPPER,                             \
    SCALAR_TO_ARRAY_MAPPER,                             \
    NUMERIC,                                            \
    DEMO                                                \
    )                                                   \
    CDICT_GROUP_##GROUP,

size_t __CDict_group(const CDict_data_type n)
{
    static const size_t __groups[] = {
        __CDICT_DATA_TYPES__
    };
    return __groups[n];
}
#undef X

#endif // __HAVE_LIBCDICT__

typedef int prevent_ISO_C_warning;
        