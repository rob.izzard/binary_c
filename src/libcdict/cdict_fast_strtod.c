

#include "../binary_c.h"
No_empty_translation_unit_warning;
#undef is_integer
#ifndef __HAVE_LIBCDICT__
#include "cdict.h"
#undef exit


#include <stdio.h>


/*
 * Wrapper for cdict_fast_double_parser to mimic the strtod
 * function of the standard C library.
 */
double cdict_fast_strtod(const char * nptr,
                          char ** const endptr)
{
    double x;
    char * const y = cdict_fast_double_parser(nptr,&x);
    if(endptr) *endptr = y;
    return x;
}

#endif // __HAVE_LIBCDICT__

typedef int prevent_ISO_C_warning;
        