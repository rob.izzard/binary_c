

#include "../binary_c.h"
No_empty_translation_unit_warning;
#undef is_integer
#ifndef __HAVE_LIBCDICT__
#include "cdict.h"
#undef exit

#include "cdict_sort_auto.h"

#define vprint(...)                             \
    if(vb)                                      \
    {                                           \
        printf(__VA_ARGS__);                    \
        fflush(stdout);                         \
    }
#undef vprint
#define vprint(...) /* do nothing */

__CDict_Nonnull_some_arguments(1)
void cdict_set_map(struct cdict_t * const cdict,
                   struct cdict_key_t * const key,
                   const CDict_key_type keytype,
                   const char * const keystring)
{
    const Boolean vb CDict_maybe_unused = cdict->vb;

    /*
     * Map between the value being set and a value that
     * can be sorted
     */
    vprint("cdict_set_map ( in) : key %p keystring \"%s\", key->string \"%s\", keytype %d == %s\n",
           (void*)key,
           keystring,
           key->string,
           keytype,
           __CDict_typename(keytype));

    if(keytype == CDICT_DATA_TYPE_UNKNOWN)
    {
        cdict_error(cdict,
                    CDICT_ERROR_DETECTED_UNKNOWN_TYPE,
                    "cdict_set_map: key type is CDICT_DATA_TYPE_UNKNOWN based on keytype = %d\n",
                    keytype);
    }

    if(keystring == NULL)
    {
        /*
         * We don't have a key string, construct one
         */
        cdict_set_key_string(cdict,
                             key,
                             NULL);
        vprint("cdict_set_map : from set_key_string \"%s\" at %p\n",
               key->string,
               (void*)key->string);

    }

    if(key->string == NULL)
    {
        cdict_error(cdict,
                    CDICT_ERROR_DETECTED_UNKNOWN_TYPE,
                    "cdict_set_map: key string is NULL, has the function to covert key strings for type %d = \"%s\" been written (in cdict_union_to_string) ?\n",
                    keytype,
                    __CDict_key_descriptor(keytype)
            );
    }
    else if(keytype == CDICT_DATA_TYPE_STRING)
    {
        /*
         * copy string data: you can't just set the string
         * to point to the same place, because the key string
         * may change hence sorting will fail
         */
        //key->map_data.string_data = key->string; // FAILS
        CDict_Safe_free(cdict,key->map_data.string_data);
        key->map_data.string_data = strdup(key->string);
        key->map_type = CDICT_DATA_TYPE_STRING;
        vprint("cdict_set_map : from strdup %s\n",
               key->map_data.string_data);

    }
    else if(__CDict_datatype_is_numeric_or_char_or_boolean(keytype))
    {
        if(keytype == CDICT_DATA_TYPE_BOOLEAN)
        {
            /* booleans -> (long long) ints */
            key->map_data.long_long_int_data = (long long int) CDict_key_scalar(key);
            key->map_type = CDICT_DATA_TYPE_LONG_LONG_INT;
            vprint("cdict_set_map : boolean -> long long int\n");
        }
        else if(__CDict_datatype_is_floating_point(keytype))
        {
            /* floats -> long double */
            key->map_data.long_double_data = (long double) CDict_key_scalar(key);
            key->map_type = CDICT_DATA_TYPE_LONG_DOUBLE;
            vprint("cdict_set_map : float -> long double %Lg\n",key->map_data.long_double_data);
        }
        else if(__CDict_datatype_is_int(keytype))
        {
            /* ints -> long long int */
            key->map_data.long_long_int_data = (long long int) CDict_key_scalar(key);
            key->map_type = CDICT_DATA_TYPE_LONG_LONG_INT;
            vprint("cdict_set_map : int -> long long int\n");
        }
        else
        {
            /* every other number -> long double */
            key->map_data.long_double_data = (long double) CDict_key_scalar(key);
            key->map_type = CDICT_DATA_TYPE_LONG_DOUBLE;
            vprint("cdict_set_map : all other numbers -> double type\n");
        }

        /*
         * Numeric types should be stored as a long double
         */
        key->map_as_long_double = (long double) CDict_key_scalar(key);

        vprint("cdict_set_map : from CDict_key_scalar (int or double or boolean), long double is %Lf\n",
               (long double)key->map_as_long_double);
    }
    else if(__CDict_datatype_is_pointer(keytype) &&
            keytype != CDICT_DATA_TYPE_STRING)
    {
        /* pointer type : set to char* type (actually a void*) */
        key->map_data.pointer_data =(void*) CDict_union_pointer(key);
        key->map_type = CDICT_DATA_TYPE_VOID_POINTER;

        vprint("cdict_set_map : from CDict_key_scalar, void pointer %p\n",
               (void*)key->map_data.pointer_data);
    }
    else
    {
        char * end;
        const long int test_long_int = strtol(keystring,&end,10);
        if(keystring != end)
        {
            /*
             * We can use the long long int type
             */
            key->map_data.long_long_int_data = (long long int) test_long_int;
            key->map_type = CDICT_DATA_TYPE_LONG_LONG_INT;
            vprint("cdict_set_map : from long long int %lld\n",
                   (long long int)key->map_data.long_long_int_data);
        }
        else
        {
            /*
             * Use doubles?
             */
            const double test_double = CDict_strtod(keystring,&end);
            if(keystring != end)
            {
                /*
                 * Can use doubles
                 */
                key->map_data.long_double_data = (long double) test_double;
                key->map_type = CDICT_DATA_TYPE_LONG_DOUBLE;
                vprint("cdict_set_map : from long double %Lf\n",
                       (long double)key->map_data.long_double_data);
            }
            else
            {
                /*
                 * Fall back to the string: this is just a
                 * char * pointer that can take anything.
                 */
                key->map_data.string_data = (char*) keystring;
                key->map_type = CDICT_DATA_TYPE_STRING;
                vprint("cdict_set_map : string fallback %s\n",
                       (char*)key->map_data.string_data);
            }
        }
    }

    vprint("cdict_set_map (out) : key %p keystring \"%s\", key->string \"%s\", keytype %d\n",
           (void*)key,
           keystring,
           key->string,
           keytype);

    key->mapped = TRUE;
}

#endif // __HAVE_LIBCDICT__

typedef int prevent_ISO_C_warning;
        