

#include "../binary_c.h"
No_empty_translation_unit_warning;
#undef is_integer
#ifndef __HAVE_LIBCDICT__
#include "cdict.h"
#undef exit

#include "cdict_sort_auto.h"

/*
 * Set up an entry in the cdict. If it already exists,
 * replace it with the new value.
 *
 * Returns the entry set in the cdict, or NULL on error.
 */

__CDict_Nonnull_some_arguments(1)
CDict_API_function
struct cdict_entry_t * cdict_set(struct cdict_t * const cdict,
                                 const union cdict_key_union key,
                                 const CDict_key_type keytype,
                                 const union cdict_value_union value,
                                 const cdict_size_t nvalue,
                                 const CDict_value_type valuetype,
                                 const void * const metadata,
                                 cdict_metadata_free_f metadata_free_function)
{
    if(cdict->vb)
    {
        printf("cdict_set: cdict=%p keytype=%d valuetype=%d (%s) metadata=%p\n",
               (void*)cdict,
               keytype,
               valuetype,
               __CDict_value_descriptor(valuetype),
               (void*)metadata);
    }

    /*
     * We want the size of the key
     */
    const size_t size = __CDict_sizeof(keytype);

    /*
     * either get the existing key, or make a new one
     */
    Boolean newkey;
    if(cdict->vb)
    {
        printf("cdict_set: call cdict_contains (pre) keytype=%d\n",
               keytype);
    }
    struct cdict_entry_t * entry =
        cdict_contains(cdict,
                       key,
                       keytype);

    if(cdict->vb)
    {
        printf("cdict_set: call cdict_contains (post) keytype=%d -> %p\n",
               keytype,
               (void*)entry);
    }

    if(entry == NULL)
    {
        /*
         * Make a new key: allocate and clear with Calloc
         */
        newkey = TRUE;
        entry = __CDict_calloc(cdict,sizeof(struct cdict_entry_t),1);
        if(cdict->vb) printf("cdict_set: require new key\n");

        /*
         * Set the new key
         */
        entry->key.type = keytype;
        entry->key.key = key;
        entry->key.format = __CDict_generic_format_string(entry->key.type);
        if(cdict->use_cache)
        {
            if(cdict->vb) printf("cdict_set: call cdict_set_map\n");

            cdict_set_map(cdict,
                          &entry->key,
                          keytype,
                          NULL);

            if(cdict->vb)
            {
                printf("cdict_set: Have set key data : mapped to string \"%s\"\n",
                       entry->key.string);
            }
        }
    }
    else
    {
        /*
         * Use an existing key, but we must clear it with memset
         */
        newkey = FALSE;
        memset(&entry->value.value,
               0,
               sizeof(union cdict_value_union));
        if(cdict->vb)
        {
            printf("cdict_set: use existing key at %p\n",
                   (void*)entry);
        }
    }

    /*
     * Set the value
     */
    entry->value.type = valuetype;
    if(__CDict_datatype_is_array(valuetype) &&
       nvalue > 0)
    {
        /*
         * Array types
         */
        entry->value.count = nvalue;

        if(valuetype == CDICT_DATA_TYPE_STRING_ARRAY)
        {
            /*
             * Array of strings, requires us to be a
             * bit more clever.
             */
            entry->value.value.string_array_data =
                __CDict_malloc(cdict,nvalue * sizeof(char*));
            cdict_push_tofree(cdict,
                              entry->value.value.string_array_data);
            //entry->value_array_len = nvalue;
            for(cdict_size_t i=0; i<nvalue; i++)
            {
                entry->value.value.string_array_data[i] =
                    strdup(value.string_array_data[i]);
                cdict_push_tofree(cdict,
                                  entry->value.value.string_array_data[i]);
                if(entry->value.value.string_array_data[i] == NULL)
                {
                    cdict_error(cdict,
                                CDICT_ERROR_STRDUP_FAILED,
                                "strdup failed in cdict_set(): out of memory or string has no terminating NULL?");
                }
            }
        }
        else if(valuetype == CDICT_DATA_TYPE_CDICT_ARRAY)
        {
            /*
             * Array of cdicts, just copy them using CDict_copy
             */
            entry->value.value.cdict_array_data =
                __CDict_malloc(cdict,nvalue * sizeof(struct cdict*));
            cdict_push_tofree(cdict,
                              entry->value.value.cdict_array_data);
            //entry->value_array_len = nvalue;
            for(cdict_size_t i=0; i<nvalue; i++)
            {
                CDict_new(c);
                CDict_copy(value.cdict_array_data[i],c);
                /* TODO : need to free c ? */
                if(c == NULL)
                {
                    cdict_error(cdict,
                                CDICT_ERROR_STRDUP_FAILED,
                                "cdict_copy failed in cdict_set(): out of memory ?");
                }
                else
                {
                    entry->value.value.cdict_array_data[i] = c;
                }
            }
        }
        else
        {
            /*
             * Allocate memory and copy data for non-string arrays.
             *
             * We do this to char_array_data but this is, naturally,
             * the same pointer we'd use for any type.
             */
            const size_t size2 = __CDict_sizeof_array_entry(valuetype) * nvalue;
            entry->value.value.char_array_data =
                __CDict_malloc(cdict,size2);
            cdict_push_tofree(cdict,
                              entry->value.value.char_array_data);
            memcpy(entry->value.value.char_array_data,
                   value.char_array_data,
                   size2);
        }
    }
    else
    {
        entry->value.value = value;
    }

    /*
     * You can also do it with the explicit
     * functions... but this is slower.
     */
    //Cdict_set_key(entry,key);
    //Cdict_set_value(entry,value);

    /*
     * Set the format strings to the defaults
     */
    entry->value.format = __CDict_generic_format_string(entry->value.type);
    entry->value.format_deref = __CDict_generic_deref_format_string(entry->value.type);

    /*
     * If we're given metadata, set them
     */
    if(metadata != NULL)
    {
        cdict_set_metadata(cdict,
                           entry,
                           metadata,
                           metadata_free_function);
    }

    /*
     * Add the key to the cdict if necessary
     */
    if(newkey)
    {
        if(cdict->vb) printf("cdict_set: call CDICT_UTHASH_ADD\n");

        /*
         * Note: if you get errors here about unallocated
         * memory or memory that has not been set, try
         * memset(&key,0,sizeof(key)) to make sure it is
         * completely cleared.
         */
        CDICT_UTHASH_ADD(cdict,
                         CDICT_HANDLE,
                         cdict->cdict_entry_list,
                         key,
                         size,
                         entry);

        if((cdict->max_size &&
            cdict->alloced > cdict->max_size) ||
           (cdict->ancestor &&
            cdict->ancestor->max_size &&
            cdict->ancestor->alloced > cdict->ancestor->max_size))
        {
            /*
             * Memory exceeded : this is a fatal error.
             * Return a NULL entry.
             */
            if(cdict->vb)
            {
                printf("cdict memory exceeded\n");
            }
            CDict_del(cdict,entry);
            entry = NULL;
        }
        if(cdict->vb)
        {
            printf("cdict_set: done CDICT_UTHASH_ADD entry = %p%s\n",
                   (void*)entry,
                   entry == NULL ? " >> This means there was an error <<" : "");
        }
    }
    return entry;
}

#endif // __HAVE_LIBCDICT__

typedef int prevent_ISO_C_warning;
        