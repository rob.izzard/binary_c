

#include "../binary_c.h"
No_empty_translation_unit_warning;
#undef is_integer
#ifndef __HAVE_LIBCDICT__
#include "cdict.h"
#undef exit


__CDict_Pure_function
double cdict_truncate_double(const double d,
                              const size_t nbytes)
{
    /*
     * Truncate double d to nbytes
     *
     * The union shares the double data, d, with
     * an 8-bit (one byte) integer, i. We assume
     * that sizeof(double) > sizeof(unit8_t)
     * which is true on any platform I know of.
     */
    union {
        double d; // full double number
        uint8_t i; // byte representation
    } u;

    /* set the double in the union */
    memcpy(&u.d, &d, sizeof(double));
    /* and clear the rest of the union, just in case */
    memset(&u.d+sizeof(u),0,sizeof(u)-sizeof(double));

    /*
     * This works on intel, probably depends on big-vs-little endian:
     * you have been warned!
     */
    for(size_t i=0; i<sizeof(u); i++)
    {
        const size_t offset = sizeof(u) - i - 1;
        *((uint8_t*)(&u.i + offset)) &= i<nbytes ? 0xff : 0x00;
    }
    return u.d;
}

#endif // __HAVE_LIBCDICT__

typedef int prevent_ISO_C_warning;
        