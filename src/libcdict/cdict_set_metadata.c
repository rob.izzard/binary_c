

#include "../binary_c.h"
No_empty_translation_unit_warning;
#undef is_integer
#ifndef __HAVE_LIBCDICT__
#include "cdict.h"
#undef exit

 
/*
 * Set metadata in an entry: to do this we
 * only need to copy the pointer.
 *
 * Note: we only set the label if it is non-NULL.
 */

__CDict_Nonnull_some_arguments(1)
CDict_API_function
void cdict_set_metadata(struct cdict_t * const cdict,
                        struct cdict_entry_t * const entry,
                        const void * const metadata,
                        cdict_metadata_free_f metadata_free_function)
{
    if(entry->metadata == NULL)
    {
        entry->metadata = __CDict_malloc(cdict,sizeof(struct cdict_metadata_t));
    }
    entry->metadata->data = (void *) metadata;
    entry->metadata->free_function = (cdict_metadata_free_f) metadata_free_function;
}

#endif // __HAVE_LIBCDICT__

typedef int prevent_ISO_C_warning;
        