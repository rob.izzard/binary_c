

#include "../binary_c.h"
No_empty_translation_unit_warning;
#undef is_integer
#ifndef __HAVE_LIBCDICT__
#include "cdict.h"
#undef exit


/*
 * Convenience wrapper around cdict_(value|key)_to_string.
 *
 * NB this does not take a format string: for that you
 * must call cdict_value_to_string and/or cdict_key_to_string
 * yourself.
 *
 * Returns the total number of characters in both strings.
 */

__CDict_Nonnull_some_arguments(1,2)
CDict_API_function
int cdict_entry_to_key_and_value_strings(struct cdict_t * const cdict,
                                         struct cdict_entry_t * const entry,
                                         char ** const value_string)
{
    return
        cdict_value_to_value_string(cdict,
                                    &entry->value,
                                    value_string,
                                    NULL,
                                    -1)
        +
        cdict_set_key_string(cdict,
                             &entry->key,
                             NULL);

}

#endif // __HAVE_LIBCDICT__

typedef int prevent_ISO_C_warning;
        