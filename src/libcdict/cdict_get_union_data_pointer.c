

#include "../binary_c.h"
No_empty_translation_unit_warning;
#undef is_integer
#ifndef __HAVE_LIBCDICT__
#include "cdict.h"
#undef exit



/*
 * Get union member, based on its type as stored in entry, and return a
 * void* pointer to it.
 */

CDict_API_function
void * cdict_get_union_data_pointer(struct cdict_entry_t * const entry)
{
    void * x; /* returned */
#undef X
#define X(TYPE,                                             \
          MEMBER,                                           \
          CTYPE,                                            \
          DEFCTYPE,                                         \
          CARRAY,                                           \
          DESCRIPTOR,                                       \
          FORMAT,                                           \
          DEREF_FORMAT,                                     \
          DEREF_CTYPE,                                      \
          GROUP,                                            \
          ARRAY_TO_SCALAR_MAPPER,                           \
          SCALAR_TO_ARRAY_MAPPER,                           \
          NUMERIC,                                          \
          DEMO)                                             \
    else if(entry->value.type == CDICT_DATA_TYPE_##TYPE)	\
    {                                                       \
        x = & entry->value.value.MEMBER##_data;             \
        break;                                              \
    }

    do {
        if(0){}
        /* loop over all types */
        __CDICT_DATA_TYPES__
            /* formally an error */
        else
        {
            x = NULL;
        }
    }while(0);
    return x;
}

#endif // __HAVE_LIBCDICT__

typedef int prevent_ISO_C_warning;
        