

#include "../binary_c.h"
No_empty_translation_unit_warning;
#undef is_integer
#ifndef __HAVE_LIBCDICT__
#include "cdict.h"
#undef exit


#include <stdio.h>
#ifdef CDICT_USE_RYU
#include "ryu/ryu.h"
#endif// CDICT_USE_RYU


/*
 * Convert data, which points to either a key or a value,
 * to a string. This is a function that does ONLY this.
 *
 * On success, *string is not NULL
 * and returns the number of characters in the string.
 *
 * On failure, *string is NULL and returns
 * CDICT_ERROR_TO_STRING_TYPE_FAILED
 */

__CDict_Nonnull_some_arguments(1,2,4)
int cdict_make_string_from_var(struct cdict_t * const cdict,
                               char ** const string,
                               const char * const format,
                               const void * const data,
                               const CDict_data_type type,
                               const cdict_size_t index
    )
{

    const Boolean vb = FALSE || cdict->vb;
    int ret;
    if(vb)
    {
        printf("cdict_make_string_from_var in cdict %p, type %d, string %p %p, data %p, format \"%s\"\n",
               (void*)cdict,
               type,
               (void*)string,
               (void*)*string,
               data,
               format);
        fflush(NULL);
    }

    union cdict_value_union * value = (union cdict_value_union*)data;

    if(type == CDICT_DATA_TYPE_STRING)
    {
        ret = cdict_asprintf(cdict,
                             string,
                             format,
                             value->string_data);
    }
    else if(type == CDICT_DATA_TYPE_CHAR_POINTER)
    {
        ret = cdict_asprintf(cdict,
                             string,
                             format,
                             (void*)value->char_pointer_data);
    }
    else if(type == CDICT_DATA_TYPE_DOUBLE)
    {
        if(strcmp(format,"%g")==0)
        {
#ifdef CDICT_USE_RYU
            /*
             * use ryu's double to string conversion
             * N.B. format is ignored
             */
            ret = cdict_ryu_d2s_asprintf(cdict,
                                         string,
                                         value->double_data);
#else
            /* use BSD's asprintf */
            ret = cdict_asprintf(cdict,string,
                                 format,
                                 value->double_data);
#endif // CDICT_USE_RYU
        }
        else
        {
            ret = cdict_asprintf(cdict,string,
                                 format,
                                 value->double_data);
        }
    }
    else if(type == CDICT_DATA_TYPE_LONG_DOUBLE)
    {
#ifdef CDICT_USE_RYU____NOT_UNTIL_WE_USE_RYU
        /*
         * use ryu's double to string conversion
         * N.B. format is ignored, and long double is
         * cast to double (so we lose precision)
         * this should be fixed!
         */
        ret = cdict_ryu_d2s_asprintf(cdict,string,
                                     (double)value->long_double_data);
#else
        /* use BSD's asprintf */
        ret = cdict_asprintf(cdict,string,
                             format,
                             value->long_double_data);
#endif // CDICT_USE_RYU
    }
    else if(type == CDICT_DATA_TYPE_INT)
    {
        ret = cdict_asprintf(cdict,string,
                             format,
                             value->int_data);
    }
    else if(type == CDICT_DATA_TYPE_UNSIGNED_INT)
    {
        ret = cdict_asprintf(cdict,string,
                             format,
                             value->unsigned_int_data);
    }
    else if(type == CDICT_DATA_TYPE_SHORT_INT)
    {
        ret = cdict_asprintf(cdict,string,
                             format,
                             value->short_int_data);
    }
    else if(type == CDICT_DATA_TYPE_UNSIGNED_SHORT_INT)
    {
        ret = cdict_asprintf(cdict,string,
                             format,
                             value->unsigned_short_int_data);
    }
    else if(type == CDICT_DATA_TYPE_LONG_INT)
    {
        ret = cdict_asprintf(cdict,string,
                             format,
                             value->long_int_data);
    }
    else if(type == CDICT_DATA_TYPE_UNSIGNED_LONG_INT)
    {
        ret = cdict_asprintf(cdict,string,
                             format,
                             value->unsigned_long_int_data);
    }
    else if(type == CDICT_DATA_TYPE_LONG_LONG_INT)
    {
        ret = cdict_asprintf(cdict,string,
                             format,
                             value->long_long_int_data);
    }
    else if(type == CDICT_DATA_TYPE_UNSIGNED_LONG_LONG_INT)
    {
        ret = cdict_asprintf(cdict,string,
                             format,
                             value->unsigned_long_long_int_data);
    }
    else if(type == CDICT_DATA_TYPE_CHAR)
    {
        ret = cdict_asprintf(cdict,string,
                             format,
                             value->char_data);
    }
    else if(type == CDICT_DATA_TYPE_FLOAT)
    {
#ifdef CDICT_USE_RYU
        /*
         * use ryu's float to string conversion
         * N.B. format is ignored
         */
        ret = cdict_ryu_f2s_asprintf(cdict,
                                     string,
                                     value->float_data);
#else
        /* use BSD's asprintf */
        ret = cdict_asprintf(cdict,
                             string,
                             format,
                             value->float_data);
#endif // CDICT_USE_RYU
    }
    else if(type == CDICT_DATA_TYPE_INT_POINTER)
    {
        ret = cdict_asprintf(cdict,string,
                             format,
                             value->int_pointer_data);
    }
    else if(type == CDICT_DATA_TYPE_DOUBLE_POINTER)
    {
        ret = cdict_asprintf(cdict,string,
                             format,
                             value->double_pointer_data);
    }
    else if(type == CDICT_DATA_TYPE_LONG_DOUBLE_POINTER)
    {
        ret = cdict_asprintf(cdict,string,
                             format,
                             value->long_double_pointer_data);
    }
    else if(type == CDICT_DATA_TYPE_UNSIGNED_INT_POINTER)
    {
        ret = cdict_asprintf(cdict,string,
                             format,
                             value->int_pointer_data);
    }
    else if(type == CDICT_DATA_TYPE_SHORT_INT_POINTER)
    {
        ret = cdict_asprintf(cdict,string,
                             format,
                             value->short_int_pointer_data);
    }
    else if(type == CDICT_DATA_TYPE_UNSIGNED_SHORT_INT_POINTER)
    {
        ret = cdict_asprintf(cdict,string,
                             format,
                             value->unsigned_short_int_pointer_data);
    }
    else if(type == CDICT_DATA_TYPE_LONG_INT_POINTER)
    {
        ret = cdict_asprintf(cdict,string,
                             format,
                             value->long_int_pointer_data);
    }
    else if(type == CDICT_DATA_TYPE_UNSIGNED_LONG_INT_POINTER)
    {
        ret = cdict_asprintf(cdict,string,
                             format,
                             value->unsigned_long_int_pointer_data);
    }
    else if(type == CDICT_DATA_TYPE_LONG_LONG_INT_POINTER)
    {
        ret = cdict_asprintf(cdict,string,
                             format,
                             value->long_long_int_pointer_data);
    }
    else if(type == CDICT_DATA_TYPE_UNSIGNED_LONG_LONG_INT_POINTER)
    {
        ret = cdict_asprintf(cdict,string,
                             format,
                             value->unsigned_long_long_int_pointer_data);
    }
    else if(type == CDICT_DATA_TYPE_VOID_POINTER)
    {
        ret = cdict_asprintf(cdict,string,
                             format,
                             value->void_pointer_data);
    }
    else if(type == CDICT_DATA_TYPE_BOOLEAN_POINTER)
    {
        ret = cdict_asprintf(cdict,string,
                             format,
                             value->Boolean_pointer_data);
    }
    else if(type == CDICT_DATA_TYPE_FLOAT_POINTER)
    {
        ret = cdict_asprintf(cdict,string,
                             format,
                             value->float_pointer_data);
    }
    else if(type == CDICT_DATA_TYPE_CDICT)
    {
        ret = cdict_asprintf(cdict,string,
                             format,
                             value->cdict_pointer_data);
    }
    else if(type == CDICT_DATA_TYPE_BOOLEAN)
    {
        ret = cdict_asprintf(cdict,string,
                             format,
                             value->Boolean_data == TRUE ? "true" : "false");
    }
    /*
     * array types : show only one value
     */
    else if(type == CDICT_DATA_TYPE_STRING_ARRAY)
    {
        ret = cdict_asprintf(cdict,string,
                             format,
                             *(value->string_array_data+index));
    }
    else if(type == CDICT_DATA_TYPE_INT_ARRAY)
    {
        ret = cdict_asprintf(cdict,string,
                             format,
                             *(value->int_array_data+index));
    }
    else if(type == CDICT_DATA_TYPE_DOUBLE_ARRAY)
    {
        if(strcmp(format,"%g")==0)
        {
#ifdef CDICT_USE_RYU
        /*
         * use ryu's double to string conversion
         * N.B. format is ignored
         */
        ret = cdict_ryu_d2s_asprintf(cdict,
                                     string,
                                     *(value->double_array_data+index));
#else
        /* use BSD's asprintf */
        ret = cdict_asprintf(cdict,string,
                             format,
                             *(value->double_array_data+index));
#endif // CDICT_USE_RYU
        }
        else
        {
            ret = cdict_asprintf(cdict,string,
                                 format,
                                 *(value->double_array_data+index));
        }
    }
    else if(type == CDICT_DATA_TYPE_LONG_DOUBLE_ARRAY)
    {
        ret = cdict_asprintf(cdict,string,
                             format,
                             *(value->long_double_array_data+index));
    }
    else if(type == CDICT_DATA_TYPE_UNSIGNED_INT_ARRAY)
    {
        ret = cdict_asprintf(cdict,string,
                             format,
                             *(value->int_array_data+index));
    }
    else if(type == CDICT_DATA_TYPE_SHORT_INT_ARRAY)
    {
        ret = cdict_asprintf(cdict,string,
                             format,
                             *(value->short_int_array_data+index));
    }
    else if(type == CDICT_DATA_TYPE_UNSIGNED_SHORT_INT_ARRAY)
    {
        ret = cdict_asprintf(cdict,string,
                             format,
                             *(value->unsigned_short_int_array_data+index));
    }
    else if(type == CDICT_DATA_TYPE_LONG_INT_ARRAY)
    {
        ret = cdict_asprintf(cdict,string,
                             format,
                             *(value->long_int_array_data+index));
    }
    else if(type == CDICT_DATA_TYPE_UNSIGNED_LONG_INT_ARRAY)
    {
        ret = cdict_asprintf(cdict,string,
                             format,
                             *(value->unsigned_long_int_array_data+index));
    }
    else if(type == CDICT_DATA_TYPE_LONG_LONG_INT_ARRAY)
    {
        ret = cdict_asprintf(cdict,string,
                             format,
                             *(value->long_long_int_array_data+index));
    }
    else if(type == CDICT_DATA_TYPE_UNSIGNED_LONG_LONG_INT_ARRAY)
    {
        ret = cdict_asprintf(cdict,string,
                             format,
                             *(value->unsigned_long_long_int_array_data+index));
    }
    else if(type == CDICT_DATA_TYPE_FLOAT_ARRAY)
    {
        ret = cdict_asprintf(cdict,string,
                             format,
                             *(value->float_array_data+index));
    }
    else if(type == CDICT_DATA_TYPE_CHAR_ARRAY)
    {
        ret = cdict_asprintf(cdict,string,
                             format,
                             *(value->char_array_data+index));
    }
    else if(type == CDICT_DATA_TYPE_VOID_POINTER_ARRAY)
    {
        ret = cdict_asprintf(cdict,string,
                             format,
                             *(value->void_pointer_array_data+index));
    }
    else if(type == CDICT_DATA_TYPE_BOOLEAN_ARRAY)
    {
        ret = cdict_asprintf(cdict,string,
                             format,
                             *(value->Boolean_array_data+index)==TRUE ? "true" : "false");
    }
    else if(type == CDICT_DATA_TYPE_CDICT_ARRAY)
    {
        ret = cdict_asprintf(cdict,string,
                             format,
                             value->cdict_array_data+index);
    }
    else
    {
        *string = NULL;
        ret = CDICT_ERROR_TO_STRING_TYPE_FAILED;
    }

    if(vb)
    {
        printf("cdict_make_string_from_var out \"%s\"\n",
               (char*)*string);
        fflush(NULL);
    }

    return ret;
}

#endif // __HAVE_LIBCDICT__

typedef int prevent_ISO_C_warning;
        