#include "../binary_c.h"
        #ifndef __HAVE_LIBCDICT__
        
#define _GNU_SOURCE
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <ctype.h>
#ifdef __CDICT_VARIABLES_TESTS__
#include "cdict_test_results.h"
#endif
#include <unistd.h>

/*
 * Function prototypes
 */
static Boolean test_for_valgrind(void);
#ifdef __CDICT_VARIABLES_TESTS__
static int _cdict_test_variables(void) __CDict_maybe_unused;
#endif //__CDICT_VARIABLES_TESTS__
static int _cdict_test_numerics(void) __CDict_maybe_unused;
static int _cdict_test_sorting(void) __CDict_maybe_unused;
static int _cdict_test_data_saving_and_loading(void) __CDict_maybe_unused;
static int _cdict_test_copying(void) __CDict_maybe_unused;
static int _cdict_test_grep(void) __CDict_maybe_unused;
static int _cdict_test_memory(const Boolean valgrind) __CDict_maybe_unused;
static int _cdict_test_API_functions(void) __CDict_maybe_unused;
static int _cdict_test_API_macros(void) __CDict_maybe_unused;
static int _cdict_test_CDICT_NARGS_macros(void);
static int _cdict_test_JSON_load(void);
static int _cdict_test_iter(void) __CDict_maybe_unused;
static int _cdict_test_nest(void) __CDict_maybe_unused;
static int _cdict_test_cdict_arrays(void) __CDict_maybe_unused;
static void _metadata_freer(struct cdict_t * const cdict,
                            struct cdict_entry_t * const entry);
static void getMemory(int * const currRealMem,
                      int * const peakRealMem,
                      int * const currVirtMem,
                      int * const peakVirtMem) __CDict_maybe_unused;
static int _error_handler(void * p,
                          const int error_number,
                          const char * const format,
                          va_list args);
static int _dummy_error_handler(void * p,
                                const int error_number,
                                const char * const format,
                                va_list args);
static int _test_assert_error_handler(void * p,
                                      const int error_number,
                                      const char * const format,
                                      va_list args);
static char * _slurp(char * const file);
static int cdict_sort_by_entry_test(struct cdict_t * const cdict,
                                    struct cdict_entry_t * const a,
                                    struct cdict_entry_t * const b);
static void _remove_pointers(char * const c);
static void _trim_trailing_whitespace(char * const c);
/*
 * Set generate_results to TRUE to make results
 * data in a format ready to put into cdict_test_results.h
 */
const Boolean generate_results = FALSE;

/*
 * Test verbosity: usually we just want to
 * know if tests pass or fail, set FALSE in that
 * case. If you want lots of output, set TRUE.
 */
const Boolean vb = FALSE;

/*
 * tests can either pass or fail
 */
#define CDICT_TEST_PASSED 0
#define CDICT_TEST_FAILED 1

/*
 * Wrapper for tests
 */
#define _test(FUNC,STRING,...)                                  \
    {                                                           \
        printf("CDict test : \"%s\" running ... \n",STRING);    \
        const int result = _cdict_test_##FUNC(__VA_ARGS__);     \
        printf("CDict test : \"%s\" result : %s\n",             \
               STRING,                                          \
               result == CDICT_TEST_PASSED                      \
               ? "passed" : "failed");                          \
        if(result == CDICT_TEST_FAILED) exit(1);                \
    }

/*
 * Wrapper for test failures
 */
#define _test_failed(...)                               \
    printf("libcdict test in %s at line %d failed : ",  \
           __FILE__,                                    \
           __LINE__);                                   \
    printf(__VA_ARGS__);                                \
    printf("\n");                                       \
    return CDICT_TEST_FAILED;


/*
 * M_PIl is not available on some platforms,
 * so invent it
 */
#ifndef M_PIl
#define M_PIl       0xc.90fdaa22168c235p-2L
#endif

#endif // __HAVE_LIBCDICT__

typedef int prevent_ISO_C_warning;
       