

#include "../binary_c.h"
No_empty_translation_unit_warning;
#undef is_integer
#ifndef __HAVE_LIBCDICT__
#include "cdict.h"
#undef exit

#include <math.h>
#include <stdio.h>
#include "cdict_sort_auto.h"

/*
 * Like cmp functions, but for two double precision numbers.
 *
 * Tolerances are set in cdict_sort_auto.
 */
extern long double cdict_absolute_tolerance;
extern long double cdict_relative_tolerance;

__CDict_Pure_function
int cdict_cf_doubles(const double x,
                      const double y)
{
    int ret;
    const double diff = fabs(x - y);
    if(diff <= (double)cdict_absolute_tolerance)
    {
        ret = 0;
    }
    else
    {
        const double largest = fabs(x) > fabs(y) ? x : y;
        if(diff <= largest * (double)cdict_relative_tolerance)
        {
            ret = 0;
        }
        else
        {
            ret = __cdict_cmp(x, y);
        }
    }
    return ret;
}

#endif // __HAVE_LIBCDICT__

typedef int prevent_ISO_C_warning;
        