

#include "../binary_c.h"
No_empty_translation_unit_warning;
#undef is_integer
#ifndef __HAVE_LIBCDICT__
#include "cdict.h"
#undef exit


/*
 * Functions to set key union elements
 */

/* for scalars */
CDict_API_function
union cdict_key_union cdict_union_from_double(double d)
{
    return (union cdict_key_union){
        .double_data = d
            };
}

CDict_API_function
union cdict_key_union cdict_union_from_long_double(long double d)
{
    return (union cdict_key_union){
        .long_double_data = d
            };
}

CDict_API_function
union cdict_key_union cdict_union_from_int(int i)
{
    return (union cdict_key_union){
        .int_data = i
            };
}

CDict_API_function
union cdict_key_union cdict_union_from_unsigned_int(unsigned int i)
{
    return (union cdict_key_union){
        .unsigned_int_data = i
            };
}


CDict_API_function
union cdict_key_union cdict_union_from_unsigned_short_int(unsigned short int i)
{
    return (union cdict_key_union){
        .unsigned_short_int_data = i
            };
}

CDict_API_function
union cdict_key_union cdict_union_from_unsigned_long_int(unsigned long int i)
{
    return (union cdict_key_union){
        .unsigned_long_int_data = i
            };
}

CDict_API_function
union cdict_key_union cdict_union_from_short_int(short int i)
{
    return (union cdict_key_union){
        .short_int_data = i
            };
}

CDict_API_function
union cdict_key_union cdict_union_from_long_int(long int i)
{
    return (union cdict_key_union){
        .long_int_data = i
            };
}

CDict_API_function
union cdict_key_union cdict_union_from_long_long_int(long long int i)
{
    return (union cdict_key_union){
        .long_long_int_data = i
            };
}

CDict_API_function
union cdict_key_union cdict_union_from_unsigned_long_long_int(unsigned long long int i)
{
    return (union cdict_key_union){
        .unsigned_long_long_int_data = i
            };
}

CDict_API_function
union cdict_key_union cdict_union_from_char(char s)
{
    return (union cdict_key_union){
        .char_data = s
            };
}

CDict_API_function
union cdict_key_union cdict_union_from_float(float f)
{
    return (union cdict_key_union){
        .float_data = f
            };
}

/* for pointers */
CDict_API_function
union cdict_key_union cdict_union_from_double_pointer(double * d)
{
    return (union cdict_key_union){
        .double_pointer_data = d
            };
}

CDict_API_function
union cdict_key_union cdict_union_from_long_double_pointer(long double * d)
{
    return (union cdict_key_union){
        .long_double_pointer_data = d
            };
}

CDict_API_function
union cdict_key_union cdict_union_from_int_pointer(int * i)
{
    return (union cdict_key_union){
        .int_pointer_data = i
            };
}

CDict_API_function
union cdict_key_union cdict_union_from_unsigned_int_pointer(unsigned int * i)
{
    return (union cdict_key_union){
        .unsigned_int_pointer_data = i
            };
}

CDict_API_function
union cdict_key_union cdict_union_from_short_int_pointer(short int * i)
{
    return (union cdict_key_union){
        .short_int_pointer_data = i
            };
}


CDict_API_function
union cdict_key_union cdict_union_from_unsigned_short_int_pointer(unsigned short int * i)
{
    return (union cdict_key_union){
        .unsigned_short_int_pointer_data = i
            };
}

CDict_API_function
union cdict_key_union cdict_union_from_long_int_pointer(long int * i)
{
    return (union cdict_key_union){
        .long_int_pointer_data = i
            };
}

CDict_API_function
union cdict_key_union cdict_union_from_long_long_int_pointer(long long int * i)
{
    return (union cdict_key_union){
        .long_long_int_pointer_data = i
            };
}


CDict_API_function
union cdict_key_union cdict_union_from_unsigned_long_int_pointer(unsigned long int * i)
{
    return (union cdict_key_union){
        .unsigned_long_int_pointer_data = i
            };
}

CDict_API_function
union cdict_key_union cdict_union_from_unsigned_long_long_int_pointer(unsigned long long int * i)
{
    return (union cdict_key_union){
        .unsigned_long_long_int_pointer_data = i
            };
}

CDict_API_function
union cdict_key_union cdict_union_from_string(char * s)
{
    return (union cdict_key_union){
        .string_data = s
            };
}

CDict_API_function
union cdict_key_union cdict_union_from_void_pointer(void * p)
{
    return (union cdict_key_union){
        .void_pointer_data = p
            };
}

CDict_API_function
union cdict_key_union cdict_union_from_float_pointer(float * f)
{
    return (union cdict_key_union){
        .float_pointer_data = f
            };
}

/* others */
CDict_API_function
union cdict_key_union cdict_union_from_Boolean(Boolean b)
{
    return (union cdict_key_union){
        .Boolean_data = b
            };
}

CDict_API_function
union cdict_key_union cdict_union_from_Boolean_pointer(Boolean * b)
{
    return (union cdict_key_union){
        .Boolean_pointer_data = b
            };
}


/* for arrays */
CDict_API_function
union cdict_key_union cdict_union_from_double_array(double * d)
{
    return (union cdict_key_union){
        .double_array_data = d
            };
}

CDict_API_function
union cdict_key_union cdict_union_from_long_double_array(long double * d)
{
    return (union cdict_key_union){
        .long_double_array_data = d
            };
}

CDict_API_function
union cdict_key_union cdict_union_from_int_array(int * i)
{
    return (union cdict_key_union){
        .int_array_data = i
            };
}

CDict_API_function
union cdict_key_union cdict_union_from_unsigned_int_array(unsigned int * i)
{
    return (union cdict_key_union){
        .unsigned_int_array_data = i
            };
}


CDict_API_function
union cdict_key_union cdict_union_from_char_array(char * s)
{
    return (union cdict_key_union){
        .char_array_data = s
            };
}

CDict_API_function
union cdict_key_union cdict_union_from_float_array(float * f)
{
    return (union cdict_key_union){
        .float_array_data = f
            };
}

CDict_API_function
union cdict_key_union cdict_union_from_Boolean_array(Boolean * b)
{
    return (union cdict_key_union){
        .Boolean_array_data = b
            };
}

CDict_API_function
union cdict_key_union cdict_union_from_string_array(char ** c)
{
    return (union cdict_key_union){
        .string_array_data = c
            };
}

CDict_API_function
union cdict_key_union cdict_union_from_void_pointer_array(void * v)
{
    return (union cdict_key_union){
        .void_pointer_array_data = v
            };
}


CDict_API_function
union cdict_key_union cdict_union_from_unknown(float f)
{
    return (union cdict_key_union){
        .float_data = f
            };
}

#endif // __HAVE_LIBCDICT__

typedef int prevent_ISO_C_warning;
        