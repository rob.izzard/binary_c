

#include "../binary_c.h"
No_empty_translation_unit_warning;
#undef is_integer
#ifndef __HAVE_LIBCDICT__
#include "cdict.h"
#undef exit


/*
 * As cdict_sort_auto, but in reverse
 */
__CDict_Pure_function
__CDict_Nonnull_all_arguments
CDict_API_function
int cdict_sort_auto_reverse(struct cdict_t * const cdict,
                            struct cdict_key_t * const a,
                            struct cdict_key_t * const b)
{
    return cdict_sort_auto(cdict,b,a);
}

#endif // __HAVE_LIBCDICT__

typedef int prevent_ISO_C_warning;
        