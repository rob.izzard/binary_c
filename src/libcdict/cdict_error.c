

#include "../binary_c.h"
No_empty_translation_unit_warning;
#undef is_integer
#ifndef __HAVE_LIBCDICT__
#include "cdict.h"
#undef exit

#include <stdarg.h>

CDict_API_function
int cdict_error(const struct cdict_t * const cdict,
                const int error_number,
                const char * const format,
                ...)
{
    /*
     * cdict's error catcher:
     *
     * if cdict->error_handler function is set, use it,
     * and send the error number (errno), format string and
     * va_args (for vsprintf) should the error handler
     * wish to use them.
     *
     * Our subsequent action depends on the error handler's
     * return value.
     *
     * Do we show the error string?
     * >=0 or no error handler set:
     *    output the error string to stderr.
     * <0  : do not show the error string.
     *
     * Do we exit?
     * >0  : exit
     * <=0 : do not exit
     *
     * If cdict is NULL, show the string and exit.
     */
    va_list args;
    va_start(args,format);
    int doexit = 1; /* default to exit after error */

    if(cdict != NULL &&
       cdict->error_handler != NULL)
    {
        va_list args_copy;
        va_copy(args_copy,args);
        doexit = (*cdict->error_handler)(cdict->error_data,
                                         error_number,
                                         format,
                                         args_copy);
        va_end(args_copy);
    }

    if(doexit>=0)
    {
        vfprintf(stderr,format,args);
    }
    va_end(args);
    if(doexit>0) exit((int)error_number);
    return doexit;
}

#endif // __HAVE_LIBCDICT__

typedef int prevent_ISO_C_warning;
        