#include "../binary_c.h"
        #ifndef __HAVE_LIBCDICT__
        
#pragma once
#ifndef CDICT_UTHASH_H
#define CDICT_UTHASH_H

/*

 * The original Uthash code in this file is distributed according to the
 * BSD licence which is compatible with libcdict's GPL.

 Copyright (c) 2003-2018, Troy D. Hanson     http://troydhanson.github.io/uthash/
 All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:

 * Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
 OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/


/*
 * Note: this version has been modified for libcdict and
 * requires C99. Thanks to Troy Hanson for the original, it's great!
 *
 * The UTHASH_VERSION has been modified to CDICT_UTHASH_VERSION,
 * and left at 2.1.0, but there are minor additions from RGI.
 *
 * Macros have had their names changed to prefix them with cdict or
 * CDICT in the hope of avoiding namespace clashes, and so you
 * can use the original UTHASH in your code without such clashes.
 *
 * Also note: RGI updated the URL in the copyright notice because
 *            the original is broken (.com should be .io)
 */


#define CDICT_UTHASH_VERSION 2.1.0

#include <string.h>   /* memcmp, memset, strlen */
#include <stddef.h>   /* ptrdiff_t */
#include <stdlib.h>   /* exit */

#define cdict_function_prefix cdict
#undef Concat
#define Concat(A,B) A##B

/* These macros use decltype or the earlier __typeof GNU extension.
   As decltype is only available in newer compilers (VS2010 or gcc 4.3+
   when compiling c++ source) this code uses whatever method is needed
   or, for VS2008 where neither is available, uses casting workarounds. */
#if !defined(CDICT_UTHASH_DECLTYPE) && !defined(NO_CDICT_UTHASH_DECLTYPE)
#if defined(_MSC_VER)   /* MS compiler */
#if _MSC_VER >= 1600 && defined(__cplusplus)  /* VS2010 or newer in C++ mode */
#define CDICT_UTHASH_DECLTYPE(x) (decltype(x))
#else                   /* VS2008 or older (or VS2010 in C mode) */
#define NO_CDICT_UTHASH_DECLTYPE
#endif
#elif                                           \
    defined(__BORLANDC__) ||                    \
    defined(__ICCARM__) ||                      \
    defined(__LCC__) ||                         \
    defined(__WATCOMC__)
#define NO_CDICT_UTHASH_DECLTYPE
#else                   /* GNU, Sun and other compilers */
#define CDICT_UTHASH_DECLTYPE(x) (__typeof(x))
#endif
#endif

#ifdef NO_CDICT_UTHASH_DECLTYPE
#define CDICT_UTHASH_DECLTYPE(x)
#define CDICT_UTHASH_DECLTYPE_ASSIGN(cdict_function_prefix, \
                                     dst,                   \
                                     src)                   \
    do {                                                    \
        char **_da_dst = (char**)(&(dst));                  \
        *_da_dst = (char*)(src);                            \
    } while (0)
#else
#define CDICT_UTHASH_DECLTYPE_ASSIGN(cdict_function_prefix, \
                                     dst,                   \
                                     src)                   \
    do {                                                    \
        (dst) = CDICT_UTHASH_DECLTYPE(dst)(src);            \
    } while (0)
#endif

/* a number of the hash function use uint32_t which isn't defined on Pre VS2010 */
#if defined(_WIN32)
#if defined(_MSC_VER) && _MSC_VER >= 1600
#include <stdint.h>
#elif defined(__WATCOMC__) || defined(__MINGW32__) || defined(__CYGWIN__)
#include <stdint.h>
#else
typedef unsigned int uint32_t;
typedef unsigned char uint8_t;
#endif
#elif defined(__GNUC__) && !defined(__VXWORKS__)
#include <stdint.h>
#else
typedef unsigned int uint32_t;
typedef unsigned char uint8_t;
#endif

#ifndef cdict_uthash_malloc
#define cdict_uthash_malloc(sz) __extension__                   \
    ({                                                          \
        malloc(sz);      /* malloc fcn                      */  \
    })
#endif
#ifndef cdict_uthash_free
#define cdict_uthash_free(ptr,sz) free(ptr)     /* free fcn                        */
#endif
#ifndef cdict_uthash_bzero
#define cdict_uthash_bzero(a,n) memset(a,'\0',n)
#endif
#ifndef cdict_uthash_strlen
#define cdict_uthash_strlen(s) strlen(s)
#endif

#ifdef cdict_uthash_memcmp
/* This warning will not catch programs that define cdict_uthash_memcmp AFTER including uthash.h. */
#warning "cdict_uthash_memcmp is deprecated; please use CDICT_UTHASH_KEYCMP instead"
#else
#define cdict_uthash_memcmp(a,b,n) memcmp(a,b,n)
#endif

#ifndef CDICT_UTHASH_KEYCMP
#define CDICT_UTHASH_KEYCMP(a,b,n) cdict_uthash_memcmp(a,b,n)
This should not happen
#endif

#ifndef cdict_uthash_noexpand_fyi
#define cdict_uthash_noexpand_fyi(tbl)          /* can be defined to log noexpand  */
#endif
#ifndef cdict_uthash_expand_fyi
#define cdict_uthash_expand_fyi(tbl)            /* can be defined to log expands   */
#endif

#ifndef CDICT_UTHASH_NONFATAL_OOM
#define CDICT_UTHASH_NONFATAL_OOM 0
#endif

#if CDICT_UTHASH_NONFATAL_OOM
/* malloc failures can be recovered from */

#ifndef cdict_uthash_nonfatal_oom
#define cdict_uthash_nonfatal_oom(cdict_function_prefix,    \
                                  obj)                      \
    do {} while (0)    /* non-fatal OOM error */

#endif

#define CDICT_UTHASH_RECORD_OOM(cdict_function_prefix,  \
                                oomed)                  \
    do { (oomed) = 1; } while (0)

#define CDICT_UTHASH_IF_CDICT_UTHASH_NONFATAL_OOM(x) x

#else
/*
 * malloc failures result in lost memory, hash tables are unusable
 *
 * Call the error handler if this happens, and if the
 * error_handler is set
 */
#ifndef cdict_uthash_fatal
#define cdict_uthash_fatal(cdict_function_prefix,               \
                           msg)                                 \
    cdict_error(cdict_function_prefix,                          \
                CDICT_ERROR_OUT_OF_MEMORY,                      \
                "cdict_uthash fatal error (out of memory?)\n");
#endif

#define CDICT_UTHASH_RECORD_OOM(cdict_function_prefix,  \
                                oomed)                  \
    cdict_uthash_fatal(cdict_function_prefix,           \
                       "out of memory")
#define CDICT_UTHASH_IF_CDICT_UTHASH_NONFATAL_OOM(x)

#endif

/* initial number of buckets */
#define CDICT_UTHASH_HASH_INITIAL_NUM_BUCKETS 32U     /* initial number of buckets        */
#define CDICT_UTHASH_HASH_INITIAL_NUM_BUCKETS_LOG2 5U /* lg2 of initial number of buckets */
#define CDICT_UTHASH_BKT_CAPACITY_THRESH 10U     /* expand when bucket count reaches */

/* calculate the element whose hash handle address is hhp */
#define CDICT_UTHASH_ELMT_FROM_HH(cdict_function_prefix,    \
                                  tbl,                      \
                                  hhp)                      \
    ((void*)(((char*)(hhp)) - ((tbl)->hho)))

/* calculate the hash handle from element address elp */
#define CDICT_UTHASH_HH_FROM_ELMT(cdict_function_prefix,            \
                                  tbl,                              \
                                  elp)                              \
    ((cdict_UT_hash_handle*)(void*)(((char*)(elp)) + ((tbl)->hho)))

#define CDICT_UTHASH_ROLLBACK_BKT(cdict_function_prefix,        \
                                  hh,                           \
                                  head,                         \
                                  itemptrhh)                    \
    do {                                                        \
        struct cdict_UT_hash_handle *_hd_hh_item = (itemptrhh); \
        unsigned _hd_bkt;                                       \
        CDICT_UTHASH_TO_BKT(cdict_function_prefix,              \
                            _hd_hh_item->hashv,                 \
                            (head)->hh.tbl->num_buckets,        \
                            _hd_bkt);                           \
        (head)->hh.tbl->buckets[_hd_bkt].count++;               \
        _hd_hh_item->hh_next = NULL;                            \
        _hd_hh_item->hh_prev = NULL;                            \
    } while (0)

#define CDICT_VALUE(cdict_function_prefix,      \
                    keyunionptr,                \
                    keylen,                     \
                    hashv)                      \
    do {                                        \
        HASH_FCN(cdict_function_prefix,         \
                 keyunionptr,                   \
                 keylen,                        \
                 hashv,                         \
                 __COUNTER__);                  \
    } while (0)


#define CDICT_UTHASH_FIND_BYHASHVALUE(cdict_function_prefix,    \
                                      hh,                       \
                                      head,                     \
                                      keyptr,                   \
                                      keylen,                   \
                                      hashval,                  \
                                      out)                      \
    do {                                                        \
        (out) = NULL;                                           \
        if(head)                                                \
        {                                                       \
            unsigned _hf_bkt;                                   \
            CDICT_UTHASH_TO_BKT(cdict_function_prefix,          \
                                hashval,                        \
                                (head)->hh.tbl->num_buckets,    \
                                _hf_bkt);                       \
            if(CDICT_UTHASH_BLOOM_TEST(cdict_function_prefix,   \
                                       (head)->hh.tbl,          \
                                       hashval) != 0)           \
            {                                                   \
                CDICT_UTHASH_FIND_IN_BKT(                       \
                    cdict_function_prefix,                      \
                    (head)->hh.tbl,                             \
                    hh,                                         \
                    (head)->hh.tbl->buckets[ _hf_bkt ],         \
                    keyptr,                                     \
                    keylen,                                     \
                    hashval,                                    \
                    out);                                       \
            }                                                   \
        }                                                       \
    } while (0)

#define CDICT_UTHASH_FIND(cdict_function_prefix,                    \
                          hh,                                       \
                          head,                                     \
                          keyptr,                                   \
                          keylen,                                   \
                          out)                                      \
    do {                                                            \
        (out) = NULL;                                               \
        if(head)                                                    \
        {                                                           \
            unsigned _hf_hashv;                                     \
            CDICT_VALUE(cdict_function_prefix,                      \
                        keyptr,                                     \
                        keylen,                                     \
                        _hf_hashv);                                 \
            CDICT_UTHASH_FIND_BYHASHVALUE(cdict_function_prefix,    \
                                          hh,                       \
                                          head,                     \
                                          keyptr,                   \
                                          keylen,                   \
                                          _hf_hashv,                \
                                          out);                     \
        }                                                           \
    } while (0)

#ifdef CDICT_UTHASH_BLOOM

#define CDICT_UTHASH_BLOOM_BITLEN               \
    (1UL << CDICT_UTHASH_BLOOM)

#define CDICT_UTHASH_BLOOM_BYTELEN                          \
    (CDICT_UTHASH_BLOOM_BITLEN/8UL) +                       \
    (((CDICT_UTHASH_BLOOM_BITLEN%8UL)!=0UL) ? 1UL : 0UL)

#define CDICT_UTHASH_BLOOM_MAKE(cdict_function_prefix,                  \
                                tbl,                                    \
                                oomed)                                  \
    do {                                                                \
        (tbl)->bloom_nbits = CDICT_UTHASH_BLOOM;                        \
        (tbl)->bloom_bv =                                               \
            (uint8_t*)cdict_uthash_malloc(CDICT_UTHASH_BLOOM_BYTELEN);  \
        if (!(tbl)->bloom_bv)                                           \
        {                                                               \
            CDICT_UTHASH_RECORD_OOM(cdict_function_prefix,oomed);       \
        }                                                               \
        else                                                            \
        {                                                               \
            cdict_uthash_bzero((tbl)->bloom_bv,                         \
                               CDICT_UTHASH_BLOOM_BYTELEN);             \
            (tbl)->bloom_sig = CDICT_UTHASH_BLOOM_SIGNATURE;            \
        }                                                               \
    } while (0)

#define CDICT_UTHASH_BLOOM_FREE(cdict_function_prefix,tbl)  \
    do {                                                    \
        cdict_uthash_free((tbl)->bloom_bv,                  \
                          CDICT_UTHASH_BLOOM_BYTELEN);      \
    } while (0)

#define CDICT_UTHASH_BLOOM_BITSET(cdict_function_prefix,bv,idx) (bv[(idx)/8U] |= (1U << ((idx)%8U)))
#define CDICT_UTHASH_BLOOM_BITTEST(cdict_function_prefix,bv,idx) (bv[(idx)/8U] & (1U << ((idx)%8U)))

#define CDICT_UTHASH_BLOOM_ADD(cdict_function_prefix,tbl,hashv)         \
    CDICT_UTHASH_BLOOM_BITSET(cdict_function_prefix,                    \
                              (tbl)->bloom_bv,                          \
                              ((hashv) &                                \
                               (uint32_t)((1UL << (tbl)->bloom_nbits) - 1U)))

#define CDICT_UTHASH_BLOOM_TEST(cdict_function_prefix,tbl,hashv)    \
    CDICT_UTHASH_BLOOM_BITTEST(cdict_function_prefix,               \
                               (tbl)->bloom_bv,                     \
                               ((hashv) & (uint32_t)(               \
                                   (1UL << (tbl)->bloom_nbits) - 1U \
                                   )))

#else
#define CDICT_UTHASH_BLOOM_MAKE(cdict_function_prefix,tbl,oomed)
#define CDICT_UTHASH_BLOOM_FREE(cdict_function_prefix,tbl)
#define CDICT_UTHASH_BLOOM_ADD(cdict_function_prefix,tbl,hashv)
#define CDICT_UTHASH_BLOOM_TEST(cdict_function_prefix,tbl,hashv) (1)
#define CDICT_UTHASH_BLOOM_BYTELEN 0U
#endif

#define CDICT_UTHASH_MAKE_TABLE(cdict_function_prefix,hh,head,oomed)    \
    do {                                                                \
        (head)->hh.tbl = (cdict_UT_hash_table*)                         \
            cdict_uthash_malloc(sizeof(cdict_UT_hash_table));           \
        if (!(head)->hh.tbl)                                            \
        {                                                               \
            CDICT_UTHASH_RECORD_OOM(cdict_function_prefix,oomed);       \
        }                                                               \
        else                                                            \
        {                                                               \
            cdict_uthash_bzero((head)->hh.tbl,                          \
                               sizeof(cdict_UT_hash_table));            \
            (head)->hh.tbl->tail = &((head)->hh);                       \
            (head)->hh.tbl->num_buckets =                               \
                CDICT_UTHASH_HASH_INITIAL_NUM_BUCKETS;                  \
            (head)->hh.tbl->log2_num_buckets =                          \
                CDICT_UTHASH_HASH_INITIAL_NUM_BUCKETS_LOG2;             \
            (head)->hh.tbl->hho =                                       \
                (char*)(&(head)->hh) - (char*)(head);                   \
            (head)->hh.tbl->buckets =                                   \
                (UT_hash_bucket*)cdict_uthash_malloc(                   \
                    CDICT_UTHASH_HASH_INITIAL_NUM_BUCKETS *             \
                    sizeof(struct UT_hash_bucket));                     \
            (head)->hh.tbl->signature = CDICT_UTHASH_SIGNATURE;         \
            if (!(head)->hh.tbl->buckets)                               \
            {                                                           \
                CDICT_UTHASH_RECORD_OOM(cdict_function_prefix,oomed);   \
                cdict_uthash_free((head)->hh.tbl,                       \
                                  sizeof(cdict_UT_hash_table));         \
            }                                                           \
            else                                                        \
            {                                                           \
                cdict_uthash_bzero(                                     \
                    (head)->hh.tbl->buckets,                            \
                    CDICT_UTHASH_HASH_INITIAL_NUM_BUCKETS *             \
                    sizeof(struct UT_hash_bucket));                     \
                CDICT_UTHASH_BLOOM_MAKE(cdict_function_prefix,(head)->hh.tbl, oomed); \
                CDICT_UTHASH_IF_CDICT_UTHASH_NONFATAL_OOM(              \
                    if (oomed) {                                        \
                        cdict_uthash_free((head)->hh.tbl->buckets,      \
                                          CDICT_UTHASH_HASH_INITIAL_NUM_BUCKETS*sizeof(struct UT_hash_bucket)); \
                        cdict_uthash_free((head)->hh.tbl, sizeof(cdict_UT_hash_table)); \
                    }                                                   \
                    )                                                   \
                    }                                                   \
        }                                                               \
    } while (0)

#define CDICT_UTHASH_REPLACE_BYHASHVALUE_INORDER(cdict_function_prefix, \
                                                 hh,                    \
                                                 head,                  \
                                                 fieldname,             \
                                                 keylen_in,             \
                                                 hashval,               \
                                                 add,                   \
                                                 replaced,              \
                                                 cmpfcn)                \
    do {                                                                \
        (replaced) = NULL;                                              \
        CDICT_UTHASH_FIND_BYHASHVALUE(cdict_function_prefix,            \
                                      hh,                               \
                                      head,                             \
                                      &((add)->fieldname),              \
                                      keylen_in,                        \
                                      hashval,                          \
                                      replaced);                        \
        if (replaced)                                                   \
        {                                                               \
            CDICT_UTHASH_DELETE(cdict_function_prefix,                  \
                                hh,                                     \
                                head,                                   \
                                replaced);                              \
        }                                                               \
        CDICT_UTHASH_ADD_KEYPTR_BYHASHVALUE_INORDER(cdict_function_prefix, \
                                                    hh,                 \
                                                    head,               \
                                                    &((add)->fieldname), \
                                                    keylen_in,          \
                                                    hashval,            \
                                                    add,                \
                                                    cmpfcn);            \
    } while (0)

#define CDICT_UTHASH_REPLACE_BYHASHVALUE(cdict_function_prefix,     \
                                         hh,                        \
                                         head,                      \
                                         fieldname,                 \
                                         keylen_in,                 \
                                         hashval,                   \
                                         add,                       \
                                         replaced)                  \
    do {                                                            \
        (replaced) = NULL;                                          \
        CDICT_UTHASH_FIND_BYHASHVALUE(cdict_function_prefix,        \
                                      hh,                           \
                                      head,                         \
                                      &((add)->fieldname),          \
                                      keylen_in,                    \
                                      hashval,                      \
                                      replaced);                    \
        if (replaced)                                               \
        {                                                           \
            CDICT_UTHASH_DELETE(cdict_function_prefix,              \
                                hh,                                 \
                                head,                               \
                                replaced);                          \
        }                                                           \
        CDICT_UTHASH_ADD_KEYPTR_BYHASHVALUE(cdict_function_prefix,  \
                                            hh,                     \
                                            head,                   \
                                            &((add)->fieldname),    \
                                            keylen_in,              \
                                            hashval,                \
                                            add);                   \
    } while (0)

#define CDICT_UTHASH_REPLACE(cdict_function_prefix,             \
                             hh,                                \
                             head,                              \
                             fieldname,                         \
                             keylen_in,                         \
                             add,                               \
                             replaced)                          \
    do {                                                        \
        unsigned _hr_hashv;                                     \
        CDICT_VALUE(cdict_function_prefix,                      \
                    &((add)->fieldname),                        \
                    keylen_in,                                  \
                    _hr_hashv);                                 \
        CDICT_UTHASH_REPLACE_BYHASHVALUE(cdict_function_prefix, \
                                         hh,                    \
                                         head,                  \
                                         fieldname,             \
                                         keylen_in,             \
                                         _hr_hashv,             \
                                         add,                   \
                                         replaced);             \
    } while (0)

#define CDICT_UTHASH_REPLACE_INORDER(cdict_function_prefix,             \
                                     hh,                                \
                                     head,                              \
                                     fieldname,                         \
                                     keylen_in,                         \
                                     add,                               \
                                     replaced,                          \
                                     cmpfcn)                            \
    do {                                                                \
        unsigned _hr_hashv;                                             \
        CDICT_VALUE(cdict_function_prefix,                              \
                    &((add)->fieldname),                                \
                    keylen_in,                                          \
                    _hr_hashv);                                         \
        CDICT_UTHASH_REPLACE_BYHASHVALUE_INORDER(cdict_function_prefix, \
                                                 hh,                    \
                                                 head,                  \
                                                 fieldname,             \
                                                 keylen_in,             \
                                                 _hr_hashv,             \
                                                 add,                   \
                                                 replaced,              \
                                                 cmpfcn);               \
    } while (0)

#define CDICT_UTHASH_APPEND_LIST(cdict_function_prefix,                 \
                                 hh,                                    \
                                 head,                                  \
                                 add)                                   \
    do {                                                                \
        (add)->hh.next = NULL;                                          \
        (add)->hh.prev = CDICT_UTHASH_ELMT_FROM_HH(cdict_function_prefix, \
                                                   (head)->hh.tbl,      \
                                                   (head)->hh.tbl->tail); \
        (head)->hh.tbl->tail->next = (add);                             \
        (head)->hh.tbl->tail = &((add)->hh);                            \
    } while (0)

#define CDICT_UTHASH_HASH_AKBI_INNER_LOOP(cdict_function_prefix,        \
                                          hh,                           \
                                          head,                         \
                                          add,                          \
                                          cmpfcn)                       \
    do {                                                                \
        do {                                                            \
            if (cmpfcn(CDICT_UTHASH_DECLTYPE(head)(_hs_iter),           \
                       add) > 0) {                                      \
                break;                                                  \
            }                                                           \
        } while (                                                       \
            (_hs_iter = CDICT_UTHASH_HH_FROM_ELMT(cdict_function_prefix, \
                                                  (head)->hh.tbl,       \
                                                  _hs_iter)->next));    \
    } while (0)

#ifdef NO_CDICT_UTHASH_DECLTYPE
#undef CDICT_UTHASH_HASH_AKBI_INNER_LOOP
#define CDICT_UTHASH_HASH_AKBI_INNER_LOOP(cdict_function_prefix,        \
                                          hh,                           \
                                          head,                         \
                                          add,                          \
                                          cmpfcn)                       \
    do {                                                                \
        char *_hs_saved_head = (char*)(head);                           \
        do {                                                            \
            CDICT_UTHASH_DECLTYPE_ASSIGN(cdict_function_prefix,         \
                                         head,                          \
                                         _hs_iter);                     \
            if (cmpfcn(head, add) > 0) {                                \
                CDICT_UTHASH_DECLTYPE_ASSIGN(cdict_function_prefix,     \
                                             head,                      \
                                             _hs_saved_head);           \
                break;                                                  \
            }                                                           \
            CDICT_UTHASH_DECLTYPE_ASSIGN(cdict_function_prefix,         \
                                         head,                          \
                                         _hs_saved_head);               \
        } while ((_hs_iter = CDICT_UTHASH_HH_FROM_ELMT(cdict_function_prefix, \
                                                       (head)->hh.tbl,  \
                                                       _hs_iter)->next)); \
    } while (0)
#endif

#if CDICT_UTHASH_NONFATAL_OOM

#define CDICT_UTHASH_ADD_TO_TABLE(cdict_function_prefix,                \
                                  hh,                                   \
                                  head,                                 \
                                  keyptr,                               \
                                  keylen_in,                            \
                                  hashval,                              \
                                  add,                                  \
                                  oomed)                                \
    do {                                                                \
        if (!(oomed)) {                                                 \
            unsigned _ha_bkt;                                           \
            (head)->hh.tbl->num_items++;                                \
            CDICT_UTHASH_TO_BKT(cdict_function_prefix,                  \
                                hashval,                                \
                                (head)->hh.tbl->num_buckets,            \
                                _ha_bkt);                               \
            CDICT_UTHASH_ADD_TO_BKT(cdict_function_prefix,              \
                                    (head)->hh.tbl->buckets[_ha_bkt],   \
                                    hh,                                 \
                                    &(add)->hh,                         \
                                    oomed);                             \
            if (oomed)                                                  \
            {                                                           \
                CDICT_UTHASH_ROLLBACK_BKT(cdict_function_prefix,        \
                                          hh,                           \
                                          head,                         \
                                          &(add)->hh);                  \
                CDICT_UTHASH_DELETE_HH(cdict_function_prefix,           \
                                       hh,                              \
                                       head,                            \
                                       &(add)->hh);                     \
                (add)->hh.tbl = NULL;                                   \
                cdict_uthash_nonfatal_oom(cdict_function_prefix,        \
                                          add);                         \
            }                                                           \
            else                                                        \
            {                                                           \
                CDICT_UTHASH_BLOOM_ADD(cdict_function_prefix,           \
                                       (head)->hh.tbl,                  \
                                       hashval);                        \
                CDICT_UTHASH_EMIT_KEY(cdict_function_prefix,            \
                                      hh,                               \
                                      head,                             \
                                      keyptr,                           \
                                      keylen_in);                       \
            }                                                           \
        } else {                                                        \
            (add)->hh.tbl = NULL;                                       \
            cdict_uthash_nonfatal_oom(cdict_function_prefix,            \
                                      add);                             \
        }                                                               \
    } while (0)

#else

#define CDICT_UTHASH_ADD_TO_TABLE(cdict_function_prefix,            \
                                  hh,                               \
                                  head,                             \
                                  keyptr,                           \
                                  keylen_in,                        \
                                  hashval,                          \
                                  add,                              \
                                  oomed)                            \
    do {                                                            \
        unsigned _ha_bkt;                                           \
        (head)->hh.tbl->num_items++;                                \
        CDICT_UTHASH_TO_BKT(cdict_function_prefix,                  \
                            hashval,                                \
                            (head)->hh.tbl->num_buckets,            \
                            _ha_bkt);                               \
        CDICT_UTHASH_ADD_TO_BKT(cdict_function_prefix,              \
                                (head)->hh.tbl->buckets[_ha_bkt],   \
                                hh,                                 \
                                &(add)->hh,                         \
                                oomed);                             \
        CDICT_UTHASH_BLOOM_ADD(cdict_function_prefix,               \
                               (head)->hh.tbl,                      \
                               hashval);                            \
        CDICT_UTHASH_EMIT_KEY(cdict_function_prefix,                \
                              hh,                                   \
                              head,                                 \
                              keyptr,                               \
                              keylen_in);                           \
    } while (0)

#endif


#define CDICT_UTHASH_ADD_KEYPTR_BYHASHVALUE_INORDER(cdict_function_prefix, \
                                                    hh,                 \
                                                    head,               \
                                                    keyptr,             \
                                                    keylen_in,          \
                                                    hashval,            \
                                                    add,                \
                                                    cmpfcn)             \
    do {                                                                \
        CDICT_UTHASH_IF_CDICT_UTHASH_NONFATAL_OOM( int _ha_oomed = 0; ) \
            (add)->hh.hashv = (hashval);                                \
        (add)->hh.key = (char*) (keyptr);                               \
        (add)->hh.keylen = (unsigned) (keylen_in);                      \
        if (!(head)) {                                                  \
            (add)->hh.next = NULL;                                      \
            (add)->hh.prev = NULL;                                      \
            CDICT_UTHASH_MAKE_TABLE(cdict_function_prefix,              \
                                    hh,                                 \
                                    add,                                \
                                    _ha_oomed);                         \
            CDICT_UTHASH_IF_CDICT_UTHASH_NONFATAL_OOM(                  \
                if (!_ha_oomed) { )                                     \
                (head) = (add);                                         \
                CDICT_UTHASH_IF_CDICT_UTHASH_NONFATAL_OOM( } )          \
                }                                                       \
        else                                                            \
        {                                                               \
            void *_hs_iter = (head);                                    \
            (add)->hh.tbl = (head)->hh.tbl;                             \
            CDICT_UTHASH_HASH_AKBI_INNER_LOOP(cdict_function_prefix,    \
                                              hh,                       \
                                              head,                     \
                                              add,                      \
                                              cmpfcn);                  \
            if (_hs_iter) {                                             \
                (add)->hh.next = _hs_iter;                              \
                if (((add)->hh.prev =                                   \
                     CDICT_UTHASH_HH_FROM_ELMT(cdict_function_prefix,   \
                                               (head)->hh.tbl,          \
                                               _hs_iter)->prev))        \
                {                                                       \
                    CDICT_UTHASH_HH_FROM_ELMT(cdict_function_prefix,    \
                                              (head)->hh.tbl,           \
                                              (add)->hh.prev)->next = (add); \
                }                                                       \
                else                                                    \
                {                                                       \
                    (head) = (add);                                     \
                }                                                       \
                CDICT_UTHASH_HH_FROM_ELMT(cdict_function_prefix,        \
                                          (head)->hh.tbl,               \
                                          _hs_iter)->prev = (add);      \
            }                                                           \
            else                                                        \
            {                                                           \
                CDICT_UTHASH_APPEND_LIST(cdict_function_prefix,         \
                                         hh,                            \
                                         head,                          \
                                         add);                          \
            }                                                           \
        }                                                               \
        CDICT_UTHASH_ADD_TO_TABLE(cdict_function_prefix,                \
                                  hh,                                   \
                                  head,                                 \
                                  keyptr,                               \
                                  keylen_in,                            \
                                  hashval,                              \
                                  add,                                  \
                                  _ha_oomed);                           \
        CDICT_UTHASH_FSCK(cdict_function_prefix,                        \
                          hh,                                           \
                          head,                                         \
                          "CDICT_UTHASH_ADD_KEYPTR_BYHASHVALUE_INORDER"); \
    } while (0)

#define CDICT_UTHASH_ADD_KEYPTR_INORDER(cdict_function_prefix,          \
                                        hh,                             \
                                        head,                           \
                                        keyptr,                         \
                                        keylen_in,                      \
                                        add,                            \
                                        cmpfcn)                         \
    do {                                                                \
        unsigned _hs_hashv;                                             \
        CDICT_VALUE(cdict_function_prefix,                              \
                    keyptr,                                             \
                    keylen_in,                                          \
                    _hs_hashv);                                         \
        CDICT_UTHASH_ADD_KEYPTR_BYHASHVALUE_INORDER(cdict_function_prefix, \
                                                    hh,                 \
                                                    head,               \
                                                    keyptr,             \
                                                    keylen_in,          \
                                                    _hs_hashv,          \
                                                    add,                \
                                                    cmpfcn);            \
    } while (0)

#define CDICT_UTHASH_ADD_BYHASHVALUE_INORDER(cdict_function_prefix,     \
                                             hh,                        \
                                             head,                      \
                                             fieldname,                 \
                                             keylen_in,                 \
                                             hashval,                   \
                                             add,                       \
                                             cmpfcn)                    \
    CDICT_UTHASH_ADD_KEYPTR_BYHASHVALUE_INORDER(cdict_function_prefix,  \
                                                hh,                     \
                                                head,                   \
                                                &((add)->fieldname),    \
                                                keylen_in,              \
                                                hashval,                \
                                                add,                    \
                                                cmpfcn)

#define CDICT_UTHASH_ADD_INORDER(cdict_function_prefix,     \
                                 hh,                        \
                                 head,                      \
                                 fieldname,                 \
                                 keylen_in,                 \
                                 add,                       \
                                 cmpfcn)                    \
    CDICT_UTHASH_ADD_KEYPTR_INORDER(cdict_function_prefix,  \
                                    hh,                     \
                                    head,                   \
                                    &((add)->fieldname),    \
                                    keylen_in,              \
                                    add,                    \
                                    cmpfcn)

#define CDICT_UTHASH_ADD_KEYPTR_BYHASHVALUE(cdict_function_prefix,      \
                                            hh,                         \
                                            head,                       \
                                            keyptr,                     \
                                            keylen_in,                  \
                                            hashval,                    \
                                            add)                        \
    do {                                                                \
        CDICT_UTHASH_IF_CDICT_UTHASH_NONFATAL_OOM( int _ha_oomed = 0; ) \
            (add)->hh.hashv = (hashval);                                \
        (add)->hh.key = (char*) (keyptr);                               \
        (add)->hh.keylen = (unsigned) (keylen_in);                      \
        if (!(head)) {                                                  \
            (add)->hh.next = NULL;                                      \
            (add)->hh.prev = NULL;                                      \
            CDICT_UTHASH_MAKE_TABLE(cdict_function_prefix,              \
                                    hh,                                 \
                                    add,                                \
                                    _ha_oomed);                         \
            CDICT_UTHASH_IF_CDICT_UTHASH_NONFATAL_OOM(                  \
                if (!_ha_oomed)                                         \
                { )                                                     \
                (head) = (add);                                         \
                CDICT_UTHASH_IF_CDICT_UTHASH_NONFATAL_OOM( } )          \
                }                                                       \
        else                                                            \
        {                                                               \
            (add)->hh.tbl = (head)->hh.tbl;                             \
            CDICT_UTHASH_APPEND_LIST(cdict_function_prefix,             \
                                     hh,                                \
                                     head,                              \
                                     add);                              \
        }                                                               \
        CDICT_UTHASH_ADD_TO_TABLE(cdict_function_prefix,                \
                                  hh,                                   \
                                  head,                                 \
                                  keyptr,                               \
                                  keylen_in,                            \
                                  hashval,                              \
                                  add,                                  \
                                  _ha_oomed);                           \
        CDICT_UTHASH_FSCK(cdict_function_prefix,                        \
                          hh,                                           \
                          head,                                         \
                          "CDICT_UTHASH_ADD_KEYPTR_BYHASHVALUE");       \
    } while (0)

#define CDICT_UTHASH_ADD_KEYPTR(cdict_function_prefix,  \
                                hh,                     \
                                head,                   \
                                keyptr,                 \
                                keylen_in,              \
                                add)                    \
    do {                                                \
        unsigned _ha_hashv;                             \
        CDICT_VALUE(                                    \
            cdict_function_prefix,                      \
            keyptr,                                     \
            keylen_in,                                  \
            _ha_hashv                                   \
            );                                          \
        CDICT_UTHASH_ADD_KEYPTR_BYHASHVALUE(            \
            cdict_function_prefix,                      \
            hh,                                         \
            head,                                       \
            keyptr,                                     \
            keylen_in,                                  \
            _ha_hashv,                                  \
            add                                         \
            );                                          \
    } while (0)

#define CDICT_UTHASH_ADD_BYHASHVALUE(cdict_function_prefix,     \
                                     hh,                        \
                                     head,                      \
                                     fieldname,                 \
                                     keylen_in,                 \
                                     hashval,                   \
                                     add)                       \
    CDICT_UTHASH_ADD_KEYPTR_BYHASHVALUE(cdict_function_prefix,  \
                                        hh,                     \
                                        head,                   \
                                        &((add)->fieldname),    \
                                        keylen_in,              \
                                        hashval,                \
                                        add)

#define CDICT_UTHASH_ADD(cdict_function_prefix,     \
                         hh,                        \
                         head,                      \
                         fieldname,                 \
                         keylen_in,                 \
                         add)                       \
    CDICT_UTHASH_ADD_KEYPTR(cdict_function_prefix,  \
                            hh,                     \
                            head,                   \
                            &((add)->fieldname),    \
                            keylen_in,              \
                            add)

#define CDICT_UTHASH_TO_BKT(cdict_function_prefix,  \
                            hashv,                  \
                            num_bkts,               \
                            bkt)                    \
    do {                                            \
        bkt = ((hashv) & ((num_bkts) - 1U));        \
    } while (0)

/* delete "delptr" from the hash table.
 * "the usual" patch-up process for the app-order doubly-linked-list.
 * The use of _hd_hh_del below deserves special explanation.
 * These used to be expressed using (delptr) but that led to a bug
 * if someone used the same symbol for the head and deletee, like
 *  CDICT_UTHASH_DELETE(hh,users,users);
 * We want that to work, but by changing the head (users) below
 * we were forfeiting our ability to further refer to the deletee (users)
 * in the patch-up process. Solution: use scratch space to
 * copy the deletee pointer, then the latter references are via that
 * scratch pointer rather than through the repointed (users) symbol.
 */
#define CDICT_UTHASH_DELETE(cdict_function_prefix,  \
                            hh,                     \
                            head,                   \
                            delptr)                 \
    CDICT_UTHASH_DELETE_HH(cdict_function_prefix,   \
                           hh,                      \
                           head,                    \
                           &(delptr)->hh)

#define CDICT_UTHASH_DELETE_HH(cdict_function_prefix,                   \
                               hh,                                      \
                               head,                                    \
                               delptrhh)                                \
    do {                                                                \
        struct cdict_UT_hash_handle *_hd_hh_del = (delptrhh);           \
        if ((_hd_hh_del->prev == NULL) && (_hd_hh_del->next == NULL))   \
        {                                                               \
            CDICT_UTHASH_BLOOM_FREE(cdict_function_prefix,              \
                                    (head)->hh.tbl);                    \
            cdict_uthash_free((head)->hh.tbl->buckets,                  \
                              (head)->hh.tbl->num_buckets *             \
                              sizeof(struct UT_hash_bucket));           \
            cdict_uthash_free((head)->hh.tbl,                           \
                              sizeof(cdict_UT_hash_table));             \
            (head) = NULL;                                              \
        }                                                               \
        else                                                            \
        {                                                               \
            unsigned _hd_bkt;                                           \
            if (_hd_hh_del == (head)->hh.tbl->tail)                     \
            {                                                           \
                (head)->hh.tbl->tail =                                  \
                    CDICT_UTHASH_HH_FROM_ELMT(cdict_function_prefix,    \
                                              (head)->hh.tbl,           \
                                              _hd_hh_del->prev);        \
            }                                                           \
            if (_hd_hh_del->prev != NULL)                               \
            {                                                           \
                CDICT_UTHASH_HH_FROM_ELMT(cdict_function_prefix,        \
                                          (head)->hh.tbl,               \
                                          _hd_hh_del->prev)->next =     \
                    _hd_hh_del->next;                                   \
            }                                                           \
            else                                                        \
            {                                                           \
                CDICT_UTHASH_DECLTYPE_ASSIGN(cdict_function_prefix,     \
                                             head,                      \
                                             _hd_hh_del->next);         \
            }                                                           \
            if (_hd_hh_del->next != NULL)                               \
            {                                                           \
                CDICT_UTHASH_HH_FROM_ELMT(cdict_function_prefix,        \
                                          (head)->hh.tbl,               \
                                          _hd_hh_del->next)->prev       \
                    = _hd_hh_del->prev;                                 \
            }                                                           \
            CDICT_UTHASH_TO_BKT(cdict_function_prefix,                  \
                                _hd_hh_del->hashv,                      \
                                (head)->hh.tbl->num_buckets,            \
                                _hd_bkt);                               \
            CDICT_UTHASH_DEL_IN_BKT(cdict_function_prefix,              \
                                    (head)->hh.tbl->buckets[_hd_bkt],   \
                                    _hd_hh_del);                        \
            (head)->hh.tbl->num_items--;                                \
        }                                                               \
        CDICT_UTHASH_FSCK(cdict_function_prefix,                        \
                          hh,                                           \
                          head,                                         \
                          "CDICT_UTHASH_DELETE_HH");                    \
    } while (0)

/* convenience forms of CDICT_UTHASH_FIND/CDICT_UTHASH_ADD/HASH_DEL */
#define CDICT_UTHASH_FIND_STR(cdict_function_prefix,    \
                              head,                     \
                              findstr,                  \
                              out)                      \
    do {                                                \
        unsigned _uthash_hfstr_keylen =                 \
            (unsigned)cdict_uthash_strlen(findstr);     \
        CDICT_UTHASH_FIND(cdict_function_prefix,        \
                          hh,                           \
                          head,                         \
                          findstr,                      \
                          _uthash_hfstr_keylen,         \
                          out);                         \
    } while (0)

#define CDICT_UTHASH_ADD_STR(cdict_function_prefix,                     \
                             head,                                      \
                             strfield,                                  \
                             add)                                       \
    do {                                                                \
        unsigned _uthash_hastr_keylen = (unsigned)cdict_uthash_strlen((add)->strfield); \
        CDICT_UTHASH_ADD(cdict_function_prefix,                         \
                         hh,                                            \
                         head,                                          \
                         strfield[0],                                   \
                         _uthash_hastr_keylen,                          \
                         add);                                          \
    } while (0)

#define CDICT_UTHASH_REPLACE_STR(cdict_function_prefix,                 \
                                 head,                                  \
                                 strfield,                              \
                                 add,                                   \
                                 replaced)                              \
    do {                                                                \
        unsigned _uthash_hrstr_keylen = (unsigned)cdict_uthash_strlen((add)->strfield); \
        CDICT_UTHASH_REPLACE(cdict_function_prefix,                     \
                             hh,                                        \
                             head,                                      \
                             strfield[0],                               \
                             _uthash_hrstr_keylen,                      \
                             add,                                       \
                             replaced);                                 \
    } while (0)

#define CDICT_UTHASH_FIND_INT(cdict_function_prefix,    \
                              head,                     \
                              findint,                  \
                              out)                      \
    CDICT_UTHASH_FIND(cdict_function_prefix,            \
                      hh,                               \
                      head,                             \
                      findint,                          \
                      sizeof(int),                      \
                      out)

#define CDICT_UTHASH_ADD_INT(cdict_function_prefix, \
                             head,                  \
                             intfield,              \
                             add)                   \
    CDICT_UTHASH_ADD(cdict_function_prefix,         \
                     hh,                            \
                     head,                          \
                     intfield,                      \
                     sizeof(int),                   \
                     add)

#define CDICT_UTHASH_REPLACE_INT(cdict_function_prefix,head,intfield,add,replaced) \
    CDICT_UTHASH_REPLACE(cdict_function_prefix,                         \
                         hh,                                            \
                         head,                                          \
                         intfield,                                      \
                         sizeof(int),                                   \
                         add,                                           \
                         replaced)

#define CDICT_UTHASH_FIND_PTR(cdict_function_prefix,    \
                              head,                     \
                              findptr,                  \
                              out)                      \
    CDICT_UTHASH_FIND(cdict_function_prefix,            \
                      hh,                               \
                      head,                             \
                      findptr,                          \
                      sizeof(void *),                   \
                      out)

#define CDICT_UTHASH_ADD_PTR(cdict_function_prefix, \
                             head,                  \
                             ptrfield,              \
                             add)                   \
    CDICT_UTHASH_ADD(cdict_function_prefix,         \
                     hh,                            \
                     head,                          \
                     ptrfield,                      \
                     sizeof(void *),                \
                     add)

#define CDICT_UTHASH_REPLACE_PTR(cdict_function_prefix, \
                                 head,                  \
                                 ptrfield,              \
                                 add,                   \
                                 replaced)              \
    CDICT_UTHASH_REPLACE(cdict_function_prefix,         \
                         hh,                            \
                         head,                          \
                         ptrfield,                      \
                         sizeof(void *),                \
                         add,                           \
                         replaced)

#define CDICT_UTHASH_DEL(cdict_function_prefix, \
                         head,                  \
                         delptr)                \
    CDICT_UTHASH_DELETE(cdict_function_prefix,  \
                        hh,                     \
                        head,                   \
                        delptr)

/* CDICT_UTHASH_FSCK checks hash integrity on every add/delete when CDICT_UTHASH_DEBUG is defined.
 * This is for uthash developer only; it compiles away if CDICT_UTHASH_DEBUG isn't defined.
 */
#ifdef CDICT_UTHASH_DEBUG
#include <stdio.h>   /* fprintf, stderr */
#define CDICT_UTHASH_OOPS(...) do { fprintf(stderr, __VA_ARGS__); exit(-1); } while (0)
#define CDICT_UTHASH_FSCK(hh,head,where)                                \
    do {                                                                \
        struct cdict_UT_hash_handle *_thh;                              \
        if (head) {                                                     \
            unsigned _bkt_i;                                            \
            unsigned _count = 0;                                        \
            char *_prev;                                                \
            for (_bkt_i = 0; _bkt_i < (head)->hh.tbl->num_buckets; ++_bkt_i) { \
                unsigned _bkt_count = 0;                                \
                _thh = (head)->hh.tbl->buckets[_bkt_i].hh_head;         \
                _prev = NULL;                                           \
                while (_thh)                                            \
                {                                                       \
                    if (_prev != (char*)(_thh->hh_prev)) {              \
                        CDICT_UTHASH_OOPS(cdict_function_prefix,        \
                                          "%s: invalid hh_prev %p, actual %p\n", \
                                          (where),                      \
                                          (void*)_thh->hh_prev,         \
                                          (void*)_prev);                \
                    }                                                   \
                    _bkt_count++;                                       \
                    _prev = (char*)(_thh);                              \
                    _thh = _thh->hh_next;                               \
                }                                                       \
                _count += _bkt_count;                                   \
                if ((head)->hh.tbl->buckets[_bkt_i].count !=  _bkt_count) \
                {                                                       \
                    CDICT_UTHASH_OOPS(cdict_function_prefix,            \
                                      "%s: invalid bucket count %u, actual %u\n", \
                                      (where),                          \
                                      (head)->hh.tbl->buckets[_bkt_i].count, \
                                      _bkt_count);                      \
                }                                                       \
            }                                                           \
            if (_count != (head)->hh.tbl->num_items)                    \
            {                                                           \
                CDICT_UTHASH_OOPS(cdict_function_prefix,                \
                                  "%s: invalid hh item count %u, actual %u\n", \
                                  (where),                              \
                                  (head)->hh.tbl->num_items,            \
                                  _count);                              \
            }                                                           \
            _count = 0;                                                 \
            _prev = NULL;                                               \
            _thh =  &(head)->hh;                                        \
            while (_thh)                                                \
            {                                                           \
                _count++;                                               \
                if (_prev != (char*)_thh->prev)                         \
                {                                                       \
                    CDICT_UTHASH_OOPS(cdict_function_prefix,            \
                                      "%s: invalid prev %p, actual %p\n", \
                                      (where),                          \
                                      (void*)_thh->prev,                \
                                      (void*)_prev);                    \
                }                                                       \
                _prev = (char*)CDICT_UTHASH_ELMT_FROM_HH(cdict_function_prefix, \
                                                         (head)->hh.tbl, \
                                                         _thh);         \
                _thh = (_thh->next ?                                    \
                        CDICT_UTHASH_HH_FROM_ELMT(cdict_function_prefix, \
                                                  (head)->hh.tbl,       \
                                                  _thh->next) :         \
                        NULL);                                          \
            }                                                           \
            if (_count != (head)->hh.tbl->num_items)                    \
            {                                                           \
                CDICT_UTHASH_OOPS(cdict_function_prefix,                \
                                  "%s: invalid app item count %u, actual %u\n", \
                                  (where),                              \
                                  (head)->hh.tbl->num_items,            \
                                  _count);                              \
            }                                                           \
        }                                                               \
    } while (0)
#else
#define CDICT_UTHASH_FSCK(cdict_function_prefix,    \
                          hh,                       \
                          head,                     \
                          where)
#endif

/* When compiled with -DCDICT_UTCDICT_UTHASH_EMIT_KEYS, length-prefixed keys are emitted to
 * the descriptor to which this macro is defined for tuning the hash function.
 * The app can #include <unistd.h> to get the prototype for write(2). */
#ifdef CDICT_UTCDICT_UTHASH_EMIT_KEYS
#define CDICT_UTHASH_EMIT_KEY(cdict_function_prefix,                    \
                              hh,                                       \
                              head,                                     \
                              keyptr,                                   \
                              fieldlen)                                 \
    do {                                                                \
        unsigned _klen = fieldlen;                                      \
        write(CDICT_UTCDICT_UTHASH_EMIT_KEYS, &_klen, sizeof(_klen));   \
        write(CDICT_UTCDICT_UTHASH_EMIT_KEYS, keyptr, (unsigned long)fieldlen); \
    } while (0)
#else
#define CDICT_UTHASH_EMIT_KEY(cdict_function_prefix,hh,head,keyptr,fieldlen)
#endif

/* default to Jenkin's hash unless overridden e.g. DHASH_FUNCTION=CDICT_UTHASH_SAX */
#ifdef CDICT_UTHASH_FUNCTION
#define HASH_FCN CDICT_UTHASH_FUNCTION
#else
#define HASH_FCN CDICT_UTHASH_JEN
#endif


/* The Bernstein hash function, used in Perl prior to v5.6. Note (x<<5+x)=x*33. */
#define CDICT_UTHASH_BER(cdict_function_prefix,key,keylen,hashv)    \
    do {                                                            \
        unsigned _hb_keylen = (unsigned)keylen;                     \
        const unsigned char *_hb_key = (const unsigned char*)(key); \
        (hashv) = 0;                                                \
        while (_hb_keylen-- != 0U) {                                \
            (hashv) = (((hashv) << 5) + (hashv)) + *_hb_key++;      \
        }                                                           \
    } while (0)


/* SAX/FNV/OAT/JEN hash functions are macro variants of those listed at
 * http://eternallyconfuzzled.com/tuts/algorithms/jsw_tut_hashing.aspx */
#define CDICT_UTHASH_SAX(cdict_function_prefix,key,keylen,hashv)    \
    do {                                                            \
        unsigned _sx_i;                                             \
        const unsigned char *_hs_key = (const unsigned char*)(key); \
        hashv = 0;                                                  \
        for (_sx_i=0; _sx_i < keylen; _sx_i++) {                    \
            hashv ^= (hashv << 5) + (hashv >> 2) + _hs_key[_sx_i];  \
        }                                                           \
    } while (0)
/* FNV-1a variation */
#define CDICT_UTHASH_FNV(cdict_function_prefix,key,keylen,hashv)    \
    do {                                                            \
        unsigned _fn_i;                                             \
        const unsigned char *_hf_key = (const unsigned char*)(key); \
        (hashv) = 2166136261U;                                      \
        for (_fn_i=0; _fn_i < keylen; _fn_i++) {                    \
            hashv = hashv ^ _hf_key[_fn_i];                         \
            hashv = hashv * 16777619U;                              \
        }                                                           \
    } while (0)

#define CDICT_UTHASH_OAT(cdict_function_prefix,key,keylen,hashv)    \
    do {                                                            \
        unsigned _ho_i;                                             \
        const unsigned char *_ho_key=(const unsigned char*)(key);   \
        hashv = 0;                                                  \
        for(_ho_i=0; _ho_i < keylen; _ho_i++) {                     \
            hashv += _ho_key[_ho_i];                                \
            hashv += (hashv << 10);                                 \
            hashv ^= (hashv >> 6);                                  \
        }                                                           \
        hashv += (hashv << 3);                                      \
        hashv ^= (hashv >> 11);                                     \
        hashv += (hashv << 15);                                     \
    } while (0)

#define CDICT_UTHASH_JEN_MIX(a,b,c)             \
    do {                                        \
        a -= b; a -= c; a ^= ( c >> 13 );       \
        b -= c; b -= a; b ^= ( a << 8 );        \
        c -= a; c -= b; c ^= ( b >> 13 );       \
        a -= b; a -= c; a ^= ( c >> 12 );       \
        b -= c; b -= a; b ^= ( a << 16 );       \
        c -= a; c -= b; c ^= ( b >> 5 );        \
        a -= b; a -= c; a ^= ( c >> 3 );        \
        b -= c; b -= a; b ^= ( a << 10 );       \
        c -= a; c -= b; c ^= ( b >> 15 );       \
    } while (0)

#define __CDICT_UTHASH_JEN_DEBUG1(cdict_function_prefix,            \
                                  key,                              \
                                  keylen,                           \
                                  hashv)                            \
    int __i;                                                        \
    for(__i=0;__i<(int)keylen;__i++)                                \
    {                                                               \
        printf("%d = %02x : ",__i,(unsigned int)*(_hj_key + __i));	\
    }                                                               \
    printf("jen\n");                                                \
    printf("hashv %u\n",hashv);                                     \
    printf("jen keylen %zu\n",keylen);                              \
    printf("jen key %p\n",(void*)key);                              \
    printf("hj_i %u\n",_hj_i);                                      \
    printf("hj_k %u\n",_hj_k);                                      \
    printf("hashv 0 += %u\n",_hj_key[0]);                           \
    printf("hashv 1 += %u\n",_hj_key[1]);                           \
    printf("hashv 2 += %u\n",_hj_key[2]);                           \
    printf("hashv 3 += %u\n",_hj_key[3]);                           \
    printf("hashv 4 += %u\n",_hj_key[4]);                           \
    printf("hashv 5 += %u\n",_hj_key[5]);                           \
    printf("hashv 6 += %u\n",_hj_key[6]);                           \
    printf("hashv 7 += %u\n",_hj_key[7]);                           \
    if(keylen>8)                                                    \
    {                                                               \
        printf("hashv 8 += %u\n",_hj_key[8]);                       \
        printf("hashv 9 += %u\n",_hj_key[9]);                       \
        printf("hashv 10 += %u\n",_hj_key[10]);                     \
        printf("hashv 11 += %u\n",_hj_key[11]);                     \
    }

#undef __CDICT_UTHASH_JEN_DEBUG1
#define __CDICT_UTHASH_JEN_DEBUG1(...)

#define CDICT_UTHASH_JEN(cdict_function_prefix,                         \
                         keyptr,                                        \
                         keylen_in,                                     \
                         hashv,                                         \
                         UNIQ)                                          \
    do {                                                                \
        unsigned _hj_i,_hj_j,_hj_k;                                     \
        /* make pointer to key's actual data (not the key itself) */	\
        unsigned const char * Concat(keydata,UNIQ) =                    \
            (keyptr)->type == CDICT_DATA_TYPE_STRING ?                  \
	  (unsigned const char *)(keyptr)->key.string_data :		\
            (unsigned const char *)&((keyptr)->key);                    \
        /* get length of the key's data */                              \
        const size_t Concat(keylen,UNIQ) =                              \
            (keyptr)->type == CDICT_DATA_TYPE_STRING ?                  \
	  (strlen((keyptr)->key.string_data)*sizeof(char)) :		\
            keylen_in;                                                  \
        /* make hashv */                                                \
        unsigned const char *_hj_key =                                  \
            (unsigned const char*)(Concat(keydata,UNIQ));               \
        hashv = 0xfeedbeefu;                                            \
        _hj_i = _hj_j = 0x9e3779b9u;                                    \
        _hj_k = (unsigned)(Concat(keylen,UNIQ));                        \
        __CDICT_UTHASH_JEN_DEBUG1(cdict_function_prefix,                \
                                  Concat(keydata,UNIQ),                 \
                                  keylen,                               \
                                  hashv) ;                              \
        while (_hj_k >= 12U)                                            \
        {                                                               \
            _hj_i +=    (_hj_key[0] + ( (unsigned)_hj_key[1] << 8 )     \
                         + ( (unsigned)_hj_key[2] << 16 )               \
                         + ( (unsigned)_hj_key[3] << 24 ) );            \
            _hj_j +=    (_hj_key[4] + ( (unsigned)_hj_key[5] << 8 )     \
                         + ( (unsigned)_hj_key[6] << 16 )               \
                         + ( (unsigned)_hj_key[7] << 24 ) );            \
            hashv += (_hj_key[8] + ( (unsigned)_hj_key[9] << 8 )        \
                      + ( (unsigned)_hj_key[10] << 16 )                 \
                      + ( (unsigned)_hj_key[11] << 24 ) );              \
                                                                        \
            CDICT_UTHASH_JEN_MIX(_hj_i, _hj_j, hashv);                  \
                                                                        \
            _hj_key += 12;                                              \
            _hj_k -= 12U;                                               \
        }                                                               \
        hashv += (unsigned)(Concat(keylen,UNIQ));                       \
        switch ( _hj_k ) {                                              \
        case 11: hashv += ( (unsigned)_hj_key[10] << 24 );              \
            __attribute__ ((fallthrough));/* FALLTHROUGH */             \
        case 10: hashv += ( (unsigned)_hj_key[9] << 16 );               \
            __attribute__ ((fallthrough));/* FALLTHROUGH */             \
        case 9:  hashv += ( (unsigned)_hj_key[8] << 8 );                \
            __attribute__ ((fallthrough)); /* FALLTHROUGH */            \
        case 8:  _hj_j += ( (unsigned)_hj_key[7] << 24 );               \
            __attribute__ ((fallthrough));/* FALLTHROUGH */             \
        case 7:  _hj_j += ( (unsigned)_hj_key[6] << 16 );               \
            __attribute__ ((fallthrough));/* FALLTHROUGH */             \
        case 6:  _hj_j += ( (unsigned)_hj_key[5] << 8 );                \
            __attribute__ ((fallthrough));/* FALLTHROUGH */             \
        case 5:  _hj_j += _hj_key[4];                                   \
            __attribute__ ((fallthrough));/* FALLTHROUGH */             \
        case 4:  _hj_i += ( (unsigned)_hj_key[3] << 24 );               \
            __attribute__ ((fallthrough));/* FALLTHROUGH */             \
        case 3:  _hj_i += ( (unsigned)_hj_key[2] << 16 );               \
            __attribute__ ((fallthrough));/* FALLTHROUGH */             \
        case 2:  _hj_i += ( (unsigned)_hj_key[1] << 8 );                \
            __attribute__ ((fallthrough));/* FALLTHROUGH */             \
        case 1:  _hj_i += _hj_key[0];                                   \
        }                                                               \
        CDICT_UTHASH_JEN_MIX(_hj_i, _hj_j, hashv);                      \
    } while (0)

/* The Paul Hsieh hash function */
#undef cdict_get16bits
#if (defined(__GNUC__) && defined(__i386__)) || defined(__WATCOMC__)    \
    || defined(_MSC_VER) || defined (__BORLANDC__) || defined (__TURBOC__)
#define cdict_get16bits(d) (*((const uint16_t *) (d)))
#endif

#if !defined (cdict_get16bits)
#define cdict_get16bits(d) ((((uint32_t)(((const uint8_t *)(d))[1])) << 8) \
                            +(uint32_t)(((const uint8_t *)(d))[0]) )
#endif
#define CDICT_UTHASH_SFH(cdict_function_prefix,                         \
                         key,                                           \
                         keylen,                                        \
                         hashv)                                         \
    do {                                                                \
        unsigned const char *_sfh_key=(unsigned const char*)(key);      \
        uint32_t _sfh_tmp, _sfh_len = (uint32_t)keylen;                 \
                                                                        \
        unsigned _sfh_rem = _sfh_len & 3U;                              \
        _sfh_len >>= 2;                                                 \
        hashv = 0xcafebabeu;                                            \
                                                                        \
        /* Main loop */                                                 \
        for (;_sfh_len > 0U; _sfh_len--)                                \
        {                                                               \
            hashv    += cdict_get16bits (_sfh_key);                     \
            _sfh_tmp  = ((uint32_t)(cdict_get16bits (_sfh_key+2)) << 11) ^ hashv; \
            hashv     = (hashv << 16) ^ _sfh_tmp;                       \
            _sfh_key += 2U*sizeof (uint16_t);                           \
            hashv    += hashv >> 11;                                    \
        }                                                               \
                                                                        \
        /* Handle end cases */                                          \
        switch (_sfh_rem) {                                             \
        case 3: hashv += cdict_get16bits (_sfh_key);                    \
            hashv ^= hashv << 16;                                       \
            hashv ^= (uint32_t)(_sfh_key[sizeof (uint16_t)]) << 18;     \
            hashv += hashv >> 11;                                       \
            break;                                                      \
        case 2: hashv += cdict_get16bits (_sfh_key);                    \
            hashv ^= hashv << 11;                                       \
            hashv += hashv >> 17;                                       \
            break;                                                      \
        case 1: hashv += *_sfh_key;                                     \
            hashv ^= hashv << 10;                                       \
            hashv += hashv >> 1;                                        \
        }                                                               \
                                                                        \
        /* Force "avalanching" of final 127 bits */                     \
        hashv ^= hashv << 3;                                            \
        hashv += hashv >> 5;                                            \
        hashv ^= hashv << 4;                                            \
        hashv += hashv >> 17;                                           \
        hashv ^= hashv << 25;                                           \
        hashv += hashv >> 6;                                            \
    } while (0)

/* iterate over items in a known bucket to find desired item */
#define CDICT_UTHASH_FIND_IN_BKT(cdict_function_prefix,                 \
                                 tbl,                                   \
                                 hh,                                    \
                                 head,                                  \
                                 keyptr,                                \
                                 keylen_in,                             \
                                 hashval,                               \
                                 out)                                   \
    do {                                                                \
        if ((head).hh_head != NULL)                                     \
        {                                                               \
            CDICT_UTHASH_DECLTYPE_ASSIGN(                               \
                cdict_function_prefix,                                  \
                out,                                                    \
                CDICT_UTHASH_ELMT_FROM_HH(cdict_function_prefix,        \
                                          tbl,                          \
                                          (head).hh_head));             \
        }                                                               \
        else                                                            \
        {                                                               \
            (out) = NULL;                                               \
        }                                                               \
        while ((out) != NULL) {                                         \
            if ((out)->hh.hashv == (hashval) &&                         \
                (out)->hh.keylen == (keylen_in))                        \
            {                                                           \
                if(CDICT_UTHASH_KEYCMP(cdict_function_prefix,           \
                                       (out)->hh.key,                   \
                                       keyptr,                          \
                                       keylen_in) == 0)                 \
                {                                                       \
                    break;                                              \
                }                                                       \
            }                                                           \
            if ((out)->hh.hh_next != NULL) {                            \
                CDICT_UTHASH_DECLTYPE_ASSIGN(                           \
                    cdict_function_prefix,                              \
                    out,                                                \
                    CDICT_UTHASH_ELMT_FROM_HH(cdict_function_prefix,    \
                                              tbl,                      \
                                              (out)->hh.hh_next));      \
            }                                                           \
            else                                                        \
            {                                                           \
                (out) = NULL;                                           \
            }                                                           \
        }                                                               \
    } while (0)



/* add an item to a bucket  */
#define CDICT_UTHASH_ADD_TO_BKT(cdict_function_prefix,                  \
                                head,                                   \
                                hh,                                     \
                                addhh,                                  \
                                oomed)                                  \
    do {                                                                \
        UT_hash_bucket *_ha_head = &(head);                             \
        _ha_head->count++;                                              \
        (addhh)->hh_next = _ha_head->hh_head;                           \
        (addhh)->hh_prev = NULL;                                        \
        if (_ha_head->hh_head != NULL) {                                \
            _ha_head->hh_head->hh_prev = (addhh);                       \
        }                                                               \
        _ha_head->hh_head = (addhh);                                    \
        if ((_ha_head->count >= ((_ha_head->expand_mult + 1U) * CDICT_UTHASH_BKT_CAPACITY_THRESH)) \
            && !(addhh)->tbl->noexpand) {                               \
            CDICT_UTHASH_EXPAND_BUCKETS(cdict_function_prefix,          \
                                        addhh,                          \
                                        (addhh)->tbl,                   \
                                        oomed);                         \
            CDICT_UTHASH_IF_CDICT_UTHASH_NONFATAL_OOM(                  \
                if (oomed) {                                            \
                    CDICT_UTHASH_DEL_IN_BKT(head,addhh);                \
                }                                                       \
                )                                                       \
                }                                                       \
    } while (0)

/* remove an item from a given bucket */
#define CDICT_UTHASH_DEL_IN_BKT(cdict_function_prefix,      \
                                head,                       \
                                delhh)                      \
    do {                                                    \
        UT_hash_bucket *_hd_head = &(head);                 \
        _hd_head->count--;                                  \
        if (_hd_head->hh_head == (delhh)) {                 \
            _hd_head->hh_head = (delhh)->hh_next;           \
        }                                                   \
        if ((delhh)->hh_prev) {                             \
            (delhh)->hh_prev->hh_next = (delhh)->hh_next;   \
        }                                                   \
        if ((delhh)->hh_next) {                             \
            (delhh)->hh_next->hh_prev = (delhh)->hh_prev;   \
        }                                                   \
    } while (0)

/* Bucket expansion has the effect of doubling the number of buckets
 * and redistributing the items into the new buckets. Ideally the
 * items will distribute more or less evenly into the new buckets
 * (the extent to which this is true is a measure of the quality of
 * the hash function as it applies to the key domain).
 *
 * With the items distributed into more buckets, the chain length
 * (item count) in each bucket is reduced. Thus by expanding buckets
 * the hash keeps a bound on the chain length. This bounded chain
 * length is the essence of how a hash provides constant time lookup.
 *
 * The calculation of tbl->ideal_chain_maxlen below deserves some
 * explanation. First, keep in mind that we're calculating the ideal
 * maximum chain length based on the *new* (doubled) bucket count.
 * In fractions this is just n/b (n=number of items,b=new num buckets).
 * Since the ideal chain length is an integer, we want to calculate
 * ceil(n/b). We don't depend on floating point arithmetic in this
 * hash, so to calculate ceil(n/b) with integers we could write
 *
 *      ceil(n/b) = (n/b) + ((n%b)?1:0)
 *
 * and in fact a previous version of this hash did just that.
 * But now we have improved things a bit by recognizing that b is
 * always a power of two. We keep its base 2 log handy (call it lb),
 * so now we can write this with a bit shift and logical AND:
 *
 *      ceil(n/b) = (n>>lb) + ( (n & (b-1)) ? 1:0)
 *
 */
#define CDICT_UTHASH_EXPAND_BUCKETS(cdict_function_prefix,              \
                                    hh,                                 \
                                    tbl,                                \
                                    oomed)                              \
    do {                                                                \
        unsigned _he_bkt;                                               \
        unsigned _he_bkt_i;                                             \
        struct cdict_UT_hash_handle *_he_thh, *_he_hh_nxt;              \
        UT_hash_bucket *_he_new_buckets, *_he_newbkt;                   \
        _he_new_buckets = (UT_hash_bucket*)cdict_uthash_malloc(         \
            2UL * (tbl)->num_buckets * sizeof(struct UT_hash_bucket));  \
        if (!_he_new_buckets) {                                         \
            CDICT_UTHASH_RECORD_OOM(cdict_function_prefix,              \
                                    oomed);                             \
        } else {                                                        \
            cdict_uthash_bzero(_he_new_buckets,                         \
                               2UL * (tbl)->num_buckets * sizeof(struct UT_hash_bucket)); \
            (tbl)->ideal_chain_maxlen =                                 \
                ((tbl)->num_items >> ((tbl)->log2_num_buckets+1U)) +    \
                ((((tbl)->num_items & (((tbl)->num_buckets*2U)-1U)) != 0U) ? 1U : 0U); \
            (tbl)->nonideal_items = 0;                                  \
            for (_he_bkt_i = 0; _he_bkt_i < (tbl)->num_buckets; _he_bkt_i++) { \
                _he_thh = (tbl)->buckets[ _he_bkt_i ].hh_head;          \
                while (_he_thh != NULL) {                               \
                    _he_hh_nxt = _he_thh->hh_next;                      \
                    CDICT_UTHASH_TO_BKT(cdict_function_prefix,          \
                                        _he_thh->hashv,                 \
                                        (tbl)->num_buckets * 2U,        \
                                        _he_bkt);                       \
                    _he_newbkt = &(_he_new_buckets[_he_bkt]);           \
                    if (++(_he_newbkt->count) > (tbl)->ideal_chain_maxlen) \
                    {                                                   \
                        (tbl)->nonideal_items++;                        \
                        if (_he_newbkt->count >                         \
                            _he_newbkt->expand_mult *                   \
                            (tbl)->ideal_chain_maxlen)                  \
                        {                                               \
                            _he_newbkt->expand_mult++;                  \
                        }                                               \
                    }                                                   \
                    _he_thh->hh_prev = NULL;                            \
                    _he_thh->hh_next = _he_newbkt->hh_head;             \
                    if (_he_newbkt->hh_head != NULL) {                  \
                        _he_newbkt->hh_head->hh_prev = _he_thh;         \
                    }                                                   \
                    _he_newbkt->hh_head = _he_thh;                      \
                    _he_thh = _he_hh_nxt;                               \
                }                                                       \
            }                                                           \
            cdict_uthash_free((tbl)->buckets,                           \
                              (tbl)->num_buckets *                      \
                              sizeof(struct UT_hash_bucket));           \
            (tbl)->num_buckets *= 2U;                                   \
            (tbl)->log2_num_buckets++;                                  \
            (tbl)->buckets = _he_new_buckets;                           \
            (tbl)->ineff_expands = ((tbl)->nonideal_items >             \
                                    ((tbl)->num_items >> 1)) ?          \
                ((tbl)->ineff_expands+1U) :                             \
                0U;                                                     \
            if ((tbl)->ineff_expands > 1U) {                            \
                (tbl)->noexpand = 1;                                    \
                cdict_uthash_noexpand_fyi(tbl);                         \
            }                                                           \
            cdict_uthash_expand_fyi(tbl);                               \
        }                                                               \
    } while (0)


/* This is an adaptation of Simon Tatham's O(n log(n)) mergesort */
/* Note that CDICT_UTHASH_SORT assumes the hash handle name to be hh.
 * CDICT_UTHASH_SRT was added to allow the hash handle name to be passed in. */
#define CDICT_UTHASH_SORT(cdicthead,cmpfcn)                 \
    CDICT_UTHASH_SRT(cdict_function_prefix,hh,head,cmpfcn)

#define CDICT_UTHASH_SRT(cdict_function_prefix,                         \
                         hh,                                            \
                         head,                                          \
                         cmpfcn)                                        \
    do {                                                                \
        unsigned _hs_i;                                                 \
        unsigned _hs_looping,_hs_nmerges,_hs_insize,                    \
            _hs_psize,_hs_qsize;                                        \
        struct cdict_UT_hash_handle *_hs_p, *_hs_q, *_hs_e,             \
            *_hs_list, *_hs_tail;                                       \
        if (head != NULL)                                               \
        {                                                               \
            _hs_insize = 1;                                             \
            _hs_looping = 1;                                            \
            _hs_list = &((head)->hh);                                   \
            while (_hs_looping != 0U) {                                 \
                _hs_p = _hs_list;                                       \
                _hs_list = NULL;                                        \
                _hs_tail = NULL;                                        \
                _hs_nmerges = 0;                                        \
                while (_hs_p != NULL) {                                 \
                    _hs_nmerges++;                                      \
                    _hs_q = _hs_p;                                      \
                    _hs_psize = 0;                                      \
                    for (_hs_i = 0; _hs_i < _hs_insize; ++_hs_i)        \
                    {                                                   \
                        _hs_psize++;                                    \
                        _hs_q = (                                       \
                            (_hs_q->next != NULL) ?                     \
                            CDICT_UTHASH_HH_FROM_ELMT(                  \
                                cdict_function_prefix,                  \
                                (head)->hh.tbl,                         \
                                _hs_q->next) :                          \
                            NULL);                                      \
                        if (_hs_q == NULL) {                            \
                            break;                                      \
                        }                                               \
                    }                                                   \
                    _hs_qsize = _hs_insize;                             \
                    while ((_hs_psize != 0U) ||                         \
                           ((_hs_qsize != 0U) && (_hs_q != NULL)))      \
                    {                                                   \
                        if (_hs_psize == 0U)                            \
                        {                                               \
                            _hs_e = _hs_q;                              \
                            _hs_q = (                                   \
                                (_hs_q->next != NULL) ?                 \
                                CDICT_UTHASH_HH_FROM_ELMT(              \
                                    cdict_function_prefix,              \
                                    (head)->hh.tbl,                     \
                                    _hs_q->next) : NULL);               \
                            _hs_qsize--;                                \
                        }                                               \
                        else if ((_hs_qsize == 0U) || (_hs_q == NULL))  \
                        {                                               \
                            _hs_e = _hs_p;                              \
                            if (_hs_p != NULL)                          \
                            {                                           \
                                _hs_p = (                               \
                                    (_hs_p->next != NULL) ?             \
                                    CDICT_UTHASH_HH_FROM_ELMT(          \
                                        cdict_function_prefix,          \
                                        (head)->hh.tbl,                 \
                                        _hs_p->next) : NULL);           \
                            }                                           \
                            _hs_psize--;                                \
                        }                                               \
                        else if (                                       \
                            (cmpfcn(                                    \
                                (cdict_function_prefix),                \
                                CDICT_UTHASH_DECLTYPE(head)(            \
                                    CDICT_UTHASH_ELMT_FROM_HH(          \
                                        cdict_function_prefix,          \
                                        (head)->hh.tbl,                 \
                                        _hs_p)),                        \
                                CDICT_UTHASH_DECLTYPE(head)(            \
                                    CDICT_UTHASH_ELMT_FROM_HH(          \
                                        cdict_function_prefix,          \
                                        (head)->hh.tbl,                 \
                                        _hs_q))                         \
                                )) <= 0) {                              \
                            _hs_e = _hs_p;                              \
                            if (_hs_p != NULL) {                        \
                                _hs_p =                                 \
                                    ((_hs_p->next != NULL)              \
                                     ?                                  \
                                     CDICT_UTHASH_HH_FROM_ELMT(         \
                                         cdict_function_prefix,         \
                                         (head)->hh.tbl, _hs_p->next)   \
                                     :                                  \
                                     NULL);                             \
                            }                                           \
                            _hs_psize--;                                \
                        }                                               \
                        else                                            \
                        {                                               \
                            _hs_e = _hs_q;                              \
                            _hs_q =                                     \
                                ((_hs_q->next != NULL) ?                \
                                 CDICT_UTHASH_HH_FROM_ELMT(             \
                                     cdict_function_prefix,             \
                                     (head)->hh.tbl, _hs_q->next) :     \
                                 NULL);                                 \
                            _hs_qsize--;                                \
                        }                                               \
                        if ( _hs_tail != NULL )                         \
                        {                                               \
                            _hs_tail->next =                            \
                                ((_hs_e != NULL) ?                      \
                                 CDICT_UTHASH_ELMT_FROM_HH(             \
                                     cdict_function_prefix,             \
                                     (head)->hh.tbl, _hs_e) : NULL);    \
                        } else {                                        \
                            _hs_list = _hs_e;                           \
                        }                                               \
                        if (_hs_e != NULL) {                            \
                            _hs_e->prev =                               \
                                ((_hs_tail != NULL) ?                   \
                                 CDICT_UTHASH_ELMT_FROM_HH(             \
                                     cdict_function_prefix,             \
                                     (head)->hh.tbl, _hs_tail) : NULL); \
                        }                                               \
                        _hs_tail = _hs_e;                               \
                    }                                                   \
                    _hs_p = _hs_q;                                      \
                }                                                       \
                if (_hs_tail != NULL) {                                 \
                    _hs_tail->next = NULL;                              \
                }                                                       \
                if (_hs_nmerges <= 1U) {                                \
                    _hs_looping = 0;                                    \
                    (head)->hh.tbl->tail = _hs_tail;                    \
                    CDICT_UTHASH_DECLTYPE_ASSIGN(                       \
                        cdict_function_prefix,                          \
                        head,                                           \
                        CDICT_UTHASH_ELMT_FROM_HH(                      \
                            cdict_function_prefix,                      \
                            (head)->hh.tbl,                             \
                            _hs_list));                                 \
                }                                                       \
                _hs_insize *= 2U;                                       \
            }                                                           \
            CDICT_UTHASH_FSCK(cdict_function_prefix,                    \
                              hh,                                       \
                              head,                                     \
                              "CDICT_UTHASH_SRT");                      \
        }                                                               \
    } while (0)

/* This function selects items from one hash into another hash.
 * The end result is that the selected items have dual presence
 * in both hashes. There is no copy of the items made; rather
 * they are added into the new hash through a secondary hash
 * hash handle that must be present in the structure. */
#define CDICT_UTHASH_SELECT(cdict_function_prefix,                      \
                            hh_dst,                                     \
                            dst,                                        \
                            hh_src,                                     \
                            src,                                        \
                            cond)                                       \
    do {                                                                \
        unsigned _src_bkt, _dst_bkt;                                    \
        void *_last_elt = NULL, *_elt;                                  \
        cdict_UT_hash_handle *_src_hh, *_dst_hh, *_last_elt_hh=NULL;    \
        ptrdiff_t _dst_hho = ((char*)(&(dst)->hh_dst) - (char*)(dst));  \
        if ((src) != NULL) {                                            \
            for (_src_bkt=0; _src_bkt < (src)->hh_src.tbl->num_buckets; _src_bkt++) { \
                for (_src_hh = (src)->hh_src.tbl->buckets[_src_bkt].hh_head; \
                     _src_hh != NULL;                                   \
                     _src_hh = _src_hh->hh_next) {                      \
                    _elt = CDICT_UTHASH_ELMT_FROM_HH(cdict_function_prefix, \
                                                     (src)->hh_src.tbl, \
                                                     _src_hh);          \
                    if (cond(_elt)) {                                   \
                        CDICT_UTHASH_IF_CDICT_UTHASH_NONFATAL_OOM( int _hs_oomed = 0; ) \
                            _dst_hh = (cdict_UT_hash_handle*)(void*)(((char*)_elt) + _dst_hho); \
                        _dst_hh->key = _src_hh->key;                    \
                        _dst_hh->keylen = _src_hh->keylen;              \
                        _dst_hh->hashv = _src_hh->hashv;                \
                        _dst_hh->prev = _last_elt;                      \
                        _dst_hh->next = NULL;                           \
                        if (_last_elt_hh != NULL) {                     \
                            _last_elt_hh->next = _elt;                  \
                        }                                               \
                        if ((dst) == NULL) {                            \
                            CDICT_UTHASH_DECLTYPE_ASSIGN(cdict_function_prefix,dst, _elt); \
                            CDICT_UTHASH_MAKE_TABLE(cdict_function_prefix,hh_dst, dst, _hs_oomed); \
                            CDICT_UTHASH_IF_CDICT_UTHASH_NONFATAL_OOM(  \
                                if (_hs_oomed) {                        \
                                    cdict_uthash_nonfatal_oom(cdict_function_prefix, \
                                                              _elt);    \
                                    (dst) = NULL;                       \
                                    continue;                           \
                                }                                       \
                                )                                       \
                                } else {                                \
                            _dst_hh->tbl = (dst)->hh_dst.tbl;           \
                        }                                               \
                        CDICT_UTHASH_TO_BKT(cdict_function_prefix,      \
                                            _dst_hh->hashv,             \
                                            _dst_hh->tbl->num_buckets,  \
                                            _dst_bkt);                  \
                        CDICT_UTHASH_ADD_TO_BKT(cdict_function_prefix,  \
                                                _dst_hh->tbl->buckets[_dst_bkt], \
                                                hh_dst,                 \
                                                _dst_hh,                \
                                                _hs_oomed);             \
                        (dst)->hh_dst.tbl->num_items++;                 \
                        CDICT_UTHASH_IF_CDICT_UTHASH_NONFATAL_OOM(      \
                            if (_hs_oomed) {                            \
                                CDICT_UTHASH_ROLLBACK_BKT(cdict_function_prefix, \
                                                          hh_dst,       \
                                                          dst,          \
                                                          _dst_hh);     \
                                CDICT_UTHASH_DELETE_HH(cdict_function_prefix, \
                                                       hh_dst,          \
                                                       dst,             \
                                                       _dst_hh);        \
                                _dst_hh->tbl = NULL;                    \
                                cdict_uthash_nonfatal_oom(cdict_function_prefix, \
                                                          _elt);        \
                                continue;                               \
                            }                                           \
                            )                                           \
                            CDICT_UTHASH_BLOOM_ADD(cdict_function_prefix, \
                                                   _dst_hh->tbl,        \
                                                   _dst_hh->hashv);     \
                        _last_elt = _elt;                               \
                        _last_elt_hh = _dst_hh;                         \
                    }                                                   \
                }                                                       \
            }                                                           \
        }                                                               \
        CDICT_UTHASH_FSCK(cdict_function_prefix,                        \
                          hh_dst,                                       \
                          dst,                                          \
                          "CDICT_UTHASH_SELECT");                       \
    } while (0)

#define CDICT_UTHASH_CLEAR(cdict_function_prefix,               \
                           hh,                                  \
                           head)                                \
    do {                                                        \
        if ((head) != NULL)                                     \
        {                                                       \
            CDICT_UTHASH_BLOOM_FREE(cdict_function_prefix,      \
                                    (head)->hh.tbl);            \
            cdict_uthash_free((head)->hh.tbl->buckets,          \
                              (head)->hh.tbl->num_buckets *     \
                              sizeof(struct UT_hash_bucket));   \
            cdict_uthash_free((head)->hh.tbl,                   \
                              sizeof(cdict_UT_hash_table));     \
            (head) = NULL;                                      \
        }                                                       \
    } while (0)

#define CDICT_UTHASH_OVERHEAD(cdict_function_prefix,                    \
                              hh,                                       \
                              head)                                     \
    (((head) != NULL) ? (                                               \
        (size_t)(((head)->hh.tbl->num_items   * sizeof(cdict_UT_hash_handle))   + \
                 ((head)->hh.tbl->num_buckets * sizeof(UT_hash_bucket))   + \
                 sizeof(cdict_UT_hash_table)                                   + \
                 (CDICT_UTHASH_BLOOM_BYTELEN))) : 0U)

#ifdef NO_CDICT_UTHASH_DECLTYPE
#define CDICT_UTHASH_ITER(cdict_function_prefix,                        \
                          hh,                                           \
                          head,                                         \
                          el,                                           \
                          tmp)                                          \
    for(((el)=(head)),                                                  \
            ((*(char**)(&(tmp)))=(char*)((head!=NULL)?(head)->hh.next:NULL)); \
        (el) != NULL;                                                   \
        ((el)=(tmp)), ((*(char**)(&(tmp)))=(char*)((tmp!=NULL)?(tmp)->hh.next:NULL)))
#else
#define CDICT_UTHASH_ITER(cdict_function_prefix,                        \
                          hh,                                           \
                          head,                                         \
                          el,                                           \
                          tmp)                                          \
    for(((el)=(head)), ((tmp)=CDICT_UTHASH_DECLTYPE(el)((head!=NULL)?(head)->hh.next:NULL)); \
        (el) != NULL; ((el)=(tmp)), ((tmp)=CDICT_UTHASH_DECLTYPE(el)((tmp!=NULL)?(tmp)->hh.next:NULL)))
#endif


/*
 * As CDICT_UTHASH_ITER but with the entry local to the loop
 *
 * NB changed for cdict
 */

#ifdef NO_CDICT_UTHASH_DECLTYPE
#define CDICT_UTHASH_ITER_C99(cdict_function_prefix,                    \
                              hh,                                       \
                              head,                                     \
                              el)                                       \
    for(struct cdict_entry_t * (el)=(head);(el);)                       \
        for(struct cdict_entry_t * __CDict_uniquename__;(el);)          \
            for(((*(char**)(&(__CDict_uniquename__)))=(char*)((head!=NULL)?(head)->hh.next:NULL)); \
                (el) != NULL; ((el)=(__CDict_uniquename__)), ((*(char**)(&(__CDict_uniquename__)))=(char*)((__CDict_uniquename__!=NULL)?(__CDict_uniquename__)->hh.next:NULL)))
#else
#define CDICT_UTHASH_ITER_C99(cdict_function_prefix,                    \
                              hh,                                       \
                              head,                                     \
                              el)                                       \
    for(struct cdict_entry_t * (el)=(head);(el);)                       \
        for(struct cdict_entry_t * __CDict_uniquename__;(el);)          \
            for((__CDict_uniquename__=CDICT_UTHASH_DECLTYPE(el)((head!=NULL)?(head)->hh.next:NULL)); \
                (el) != NULL; ((el)=(__CDict_uniquename__)), ((__CDict_uniquename__)=CDICT_UTHASH_DECLTYPE(el)((__CDict_uniquename__!=NULL)?(__CDict_uniquename__)->hh.next:NULL)))
#endif


/* obtain a count of items in the hash */
#define CDICT_UTHASH_COUNT(cdict_function_prefix,head) CDICT_UTHASH_CNT(cdict_function_prefix,hh,head)
#define CDICT_UTHASH_CNT(cdict_function_prefix,hh,head) ((head != NULL)?((head)->hh.tbl->num_items):0U)

typedef struct UT_hash_bucket {
    struct cdict_UT_hash_handle *hh_head;
    unsigned count;

    /* expand_mult is normally set to 0. In this situation, the max chain length
     * threshold is enforced at its default value, CDICT_UTHASH_BKT_CAPACITY_THRESH. (If
     * the bucket's chain exceeds this length, bucket expansion is triggered).
     * However, setting expand_mult to a non-zero value delays bucket expansion
     * (that would be triggered by additions to this particular bucket)
     * until its chain length reaches a *multiple* of CDICT_UTHASH_BKT_CAPACITY_THRESH.
     * (The multiplier is simply expand_mult+1). The whole idea of this
     * multiplier is to reduce bucket expansions, since they are expensive, in
     * situations where we know that a particular bucket tends to be overused.
     * It is better to let its chain length grow to a longer yet-still-bounded
     * value, than to do an O(n) bucket expansion too often.
     */
    unsigned expand_mult;

} UT_hash_bucket;

/* random signature used only to find hash tables in external analysis */
#define CDICT_UTHASH_SIGNATURE 0xa0111fe1u
#define CDICT_UTHASH_BLOOM_SIGNATURE 0xb12220f2u

typedef struct cdict_UT_hash_table {
    UT_hash_bucket *buckets;
    unsigned num_buckets, log2_num_buckets;
    unsigned num_items;
    struct cdict_UT_hash_handle *tail; /* tail hh in app order, for fast append    */
    ptrdiff_t hho; /* hash handle offset (byte pos of hash handle in element */

    /* in an ideal situation (all buckets used equally), no bucket would have
     * more than ceil(#items/#buckets) items. that's the ideal chain length. */
    unsigned ideal_chain_maxlen;

    /* nonideal_items is the number of items in the hash whose chain position
     * exceeds the ideal chain maxlen. these items pay the penalty for an uneven
     * hash distribution; reaching them in a chain traversal takes >ideal steps */
    unsigned nonideal_items;

    /* ineffective expands occur when a bucket doubling was performed, but
     * afterward, more than half the items in the hash had nonideal chain
     * positions. If this happens on two consecutive expansions we inhibit any
     * further expansion, as it's not helping; this happens when the hash
     * function isn't a good fit for the key domain. When expansion is inhibited
     * the hash will still work, albeit no longer in constant time. */
    unsigned ineff_expands, noexpand;

    uint32_t signature; /* used only to find hash tables in external analysis */
#ifdef CDICT_UTHASH_BLOOM
    uint32_t bloom_sig; /* used only to test bloom exists in external analysis */
    uint8_t *bloom_bv;
    uint8_t bloom_nbits;
#endif

} cdict_UT_hash_table;

typedef struct cdict_UT_hash_handle {
    struct cdict_UT_hash_table *tbl;
    void *prev;                       /* prev element in app order      */
    void *next;                       /* next element in app order      */
    struct cdict_UT_hash_handle *hh_prev;   /* previous hh in bucket order    */
    struct cdict_UT_hash_handle *hh_next;   /* next hh in bucket order        */
    void *key;                        /* ptr to enclosing struct's key  */
    unsigned keylen;                  /* enclosing struct's key len     */
    unsigned hashv;                   /* result of hash-fcn(key)        */
} cdict_UT_hash_handle;

#endif /* CDICT_UTHASH_H */

#endif // __HAVE_LIBCDICT__

typedef int prevent_ISO_C_warning;
       