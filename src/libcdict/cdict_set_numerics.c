

#include "../binary_c.h"
No_empty_translation_unit_warning;
#undef is_integer
#ifndef __HAVE_LIBCDICT__
#include "cdict.h"
#undef exit


void cdict_set_numerics(const double abs,
                        const double rel,
                        const size_t nbytes_double,
                        const size_t nbytes_float)
{
    /*
     * Function to set cdict's tolerances for floating-point
     * comparisons, and the number of bytes used in the cdict key
     * calculation
     */
    if(abs>0.0) cdict_absolute_tolerance = abs;
    if(rel>0.0) cdict_relative_tolerance = rel;

    if(nbytes_float>0) cdict_hashv_float_nbytes = nbytes_float;
    if(nbytes_double>0) cdict_hashv_double_nbytes = nbytes_double;
    if(nbytes_double>0) cdict_hashv_long_double_nbytes = nbytes_double;
}

#endif // __HAVE_LIBCDICT__

typedef int prevent_ISO_C_warning;
        