

#include "../binary_c.h"
No_empty_translation_unit_warning;
#undef is_integer
#ifndef __HAVE_LIBCDICT__
#include "cdict.h"
#undef exit


/*
 * Like cdict_set but also sets the entry's format strings
 */

__CDict_Nonnull_some_arguments(1)
CDict_API_function
struct cdict_entry_t * cdict_set_with_formats(struct cdict_t * const cdict,
                                              const union cdict_key_union key,
                                              const CDict_key_type keytype,
                                              const char * const keyformat,
                                              const union cdict_value_union value,
                                              const cdict_size_t nvalue,
                                              const CDict_value_type valuetype,
                                              const char * const valueformat,
                                              const void * const metadata,
                                              cdict_metadata_free_f metadata_free_function)
{
    struct cdict_entry_t * entry =
        cdict_set(cdict,
                  key,
                  keytype,
                  value,
                  nvalue,
                  valuetype,
                  metadata,
                  metadata_free_function);
    entry->key.format = (char*)keyformat;
    entry->value.format = (char*)valueformat;
    return entry;
}

#endif // __HAVE_LIBCDICT__

typedef int prevent_ISO_C_warning;
        