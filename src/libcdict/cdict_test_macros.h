#include "../binary_c.h"
        #ifndef __HAVE_LIBCDICT__
        
#pragma once
#ifndef CDICT_TEST_MACROS
#define CDICT_TEST_MACROS

/*
 * A collection of variable test macros used in libcdict.
 */

/*
 * Macro to concatenate three arguments
 */
#undef Concat3
#define Concat3(A,B,C) A##B##C

/*
 * Prevent -Wsizeof-pointer-div  warning in gcc >= 10 or
 * clang >= 9
 */
#if __GNUC__ >= 10
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wsizeof-pointer-div"
#endif

/*
 * Macro to return 1 if x is a scalar type,
 * otherwise 0.
 */
#define CDict_is_scalar(x)                      \
    _Generic((x),                               \
             int: 1,                            \
             unsigned int: 1,                   \
             long int: 1,                       \
             unsigned long int: 1,              \
             long long int: 1,                  \
             unsigned long long int: 1,         \
             float: 1,                          \
             double: 1,                         \
             long double: 1,                    \
             Boolean: 1,                        \
             char: 1,                           \
             default: 0)

/*
 * return 1 if x is a pointer or array type, 0 otherwise
 */
#define CDict_is_pointer_or_array(x)            \
    (CDict_is_scalar(x)==1 ?  0 : 1)

/*
 * Return 1 if x is possibly array of strings
 * (i.e. is char** type for _Generic), you will
 * need to use other tests to distinguish if
 * x is an array or pointer.
 */
#define CDict_is_array_of_strings(x)            \
    (                                           \
        _Generic(x),                            \
        char**:1,                               \
        default:0                               \
        )

/*
 * If two variables x and y have the same type,
 * return 1, otherwise 0.
 */
#define CDict_vars_same_type(x, y)              \
    (                                           \
        __builtin_types_compatible_p(           \
            typeof(x),                          \
            typeof(y)                           \
            )                                   \
        )

/*
 * If two variables x and y have different types,
 * return 0, otherwise 1.
 */
#define CDict_vars_different_type(x, y)             \
    (                                               \
        CDict_vars_same_type((x),(y))==0 ? 1 : 0    \
        )

/*
 * Cast the argument to a type that does not give errors
 * in the CDict_is_array macro if neither an array nor pointer,
 * or use the argument directly if possible.
 *
 * Inspired by FORCETYPE() from
 * https://www.mikeash.com/pyblog/friday-qa-2010-12-31-c-macro-tips-and-tricks.html
 */
#define __CDict_Cast(a)                         \
    __CDict_Cast_implementation(                \
        CDict_cast_implementation,              \
        __COUNTER__,                            \
        (a))
#define __CDict_Cast_implementation(LABEL,                              \
                                    LINE,                               \
                                    a)                                  \
    _Generic(                                                           \
        (a),                                                            \
        int: __extension__({int *Concat3(__x,LABEL,LINE);Concat3(__x,LABEL,LINE);}), \
        short int: __extension__({short int *Concat3(__x,LABEL,LINE);Concat3(__x,LABEL,LINE);}), \
        unsigned int: __extension__({unsigned int *Concat3(__x,LABEL,LINE);Concat3(__x,LABEL,LINE);}), \
        unsigned short int: __extension__({unsigned int *Concat3(__x,LABEL,LINE);Concat3(__x,LABEL,LINE);}), \
        long int: __extension__({long int *Concat3(__x,LABEL,LINE);Concat3(__x,LABEL,LINE);}), \
        unsigned long int: __extension__({unsigned long int *Concat3(__x,LABEL,LINE);Concat3(__x,LABEL,LINE);}), \
        long long int: __extension__({long long int *Concat3(__x,LABEL,LINE);Concat3(__x,LABEL,LINE);}), \
        unsigned long long int: __extension__({unsigned long long int *Concat3(__x,LABEL,LINE);Concat3(__x,LABEL,LINE);}), \
        float: __extension__({float *Concat3(__x,LABEL,LINE);Concat3(__x,LABEL,LINE);}), \
        double: __extension__({double *Concat3(__x,LABEL,LINE);Concat3(__x,LABEL,LINE);}), \
        long double: __extension__({long double *Concat3(__x,LABEL,LINE);Concat3(__x,LABEL,LINE);}), \
        Boolean: __extension__({Boolean *Concat3(__x,LABEL,LINE);Concat3(__x,LABEL,LINE);}), \
        char: __extension__({char *Concat3(__x,LABEL,LINE);Concat3(__x,LABEL,LINE);}), \
        union cdict_key_union: __extension__({union cdict_key_union *Concat3(__x,LABEL,LINE);Concat3(__x,LABEL,LINE);}), \
        union cdict_value_union: __extension__({union cdict_value_union *Concat3(__x,LABEL,LINE);Concat3(__x,LABEL,LINE);}), \
        default: (a)                                                    \
        )

/*
 * Test if x is an array, return 1 if it is, 0 otherwise.
 */
#define CDict_is_array(x)                       \
    (                                           \
        CDict_vars_different_type(              \
            __CDict_Cast(x),                    \
            (&(*__CDict_Cast(x)))               \
            )                                   \
        )

/*
 * Test whether x is uniquely a pointer, return
 * 1 if it is, 0 otherwise.
 */
#define CDict_is_pointer(x)                     \
    (                                           \
        (                                       \
            CDict_is_pointer_or_array(x) &&     \
            (sizeof(x) == sizeof(void*)) &&     \
            (!CDict_is_array(x))                \
            )                                   \
        ? 1 : 0                                 \
        )

/*
 * Return array size of x, or -1 if x is not an
 * array.
 *
 * Note: this returns an cdict_size_t which is a
 *       long long int.
 */
#define CDict_array_size(x)                             \
    (                                                   \
        (cdict_size_t)                                  \
        __builtin_choose_expr(                          \
            CDict_is_array(x),                          \
            (sizeof(__CDict_Cast(x))/                   \
             __extension__ sizeof(*(__CDict_Cast(x)))), \
            -1                                          \
            )                                           \
        )

/*
 * Return 1 if x is a compile-time constant.
 */
#define CDict_is_compile_time_constant(x)       \
    __builtin_constant_p(x)

/*
 * Return true if entry is a hash
 */
#define CDict_entry_is_hash(entry)                      \
    ((entry)!=NULL &&                                   \
     (entry)->value.type == CDICT_DATA_TYPE_CDICT &&    \
     (entry)->value.value.cdict_pointer_data != NULL)

#endif // CDICT_TEST_MACROS

#endif // __HAVE_LIBCDICT__

typedef int prevent_ISO_C_warning;
       