

#include "../binary_c.h"
No_empty_translation_unit_warning;
#undef is_integer
#ifndef __HAVE_LIBCDICT__
#include "cdict.h"
#undef exit


/*
 * Return a descriptor string describing
 * libcdict data type n
 */
#undef X
#define X(TYPE,                                 \
          MEMBER,                               \
          CTYPE,                                \
          DEFCTYPE,                             \
          CARRAY,                               \
          DESCRIPTOR,                           \
          FORMAT,                               \
          DEREF_FORMAT,                         \
          DEREF_CTYPE,                          \
          GROUP,                                \
          ARRAY_TO_SCALAR_MAPPER,               \
          SCALAR_TO_ARRAY_MAPPER,               \
          NUMERIC,                              \
          DEMO)                                 \
    DESCRIPTOR,

char * __CDict_descriptor(const CDict_data_type n)
{
    static const char * const __descriptors[] = {
        __CDICT_DATA_TYPES__
    };
    //printf("(datatype %d)\n",(int)n);
    return n < CDICT_DATA_NUMBER_OF_TYPES ? (char *) __descriptors[n] :
        "data type not recognised";
}
#undef X

#endif // __HAVE_LIBCDICT__

typedef int prevent_ISO_C_warning;
        