

#include "../binary_c.h"
No_empty_translation_unit_warning;
#undef is_integer
#ifndef __HAVE_LIBCDICT__
#include "cdict.h"
#undef exit

/*
 * Wrapper to cdict_sort_auto so we can take entries rather
 * than keys as arguments
 */
__CDict_Pure_function
__CDict_Nonnull_all_arguments
CDict_API_function
int cdict_sort_by_entry_auto(struct cdict_t * const cdict,
                             struct cdict_entry_t * const a,
                             struct cdict_entry_t * const b)
{
    return cdict_sort_auto(cdict,
                           &a->key,
                           &b->key);
}

#endif // __HAVE_LIBCDICT__

typedef int prevent_ISO_C_warning;
        