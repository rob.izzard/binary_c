

#include "../binary_c.h"
No_empty_translation_unit_warning;
#undef is_integer
#ifndef __HAVE_LIBCDICT__
#include "cdict.h"
#undef exit


#include <stdio.h>


/*
 * Function to load the JSON in filename
 * to the given cdict. Returns a pointer
 * to the buffer, or calls the cdict's error
 * function on failure (then returns NULL).
 */
__CDict_Nonnull_some_arguments(1,2)
CDict_API_function
void cdict_load_json(struct cdict_t * const cdict,
                     const char * const filename)
{
    /* open an existing file for reading */
    FILE * fp = fopen(filename, "r");

    /* quit if the file does not exist */
    if(fp == NULL)
    {
        cdict_error(cdict,
                    CDICT_ERROR_CANNOT_OPEN_FILE,
                    "Cannot open file at \"%s\", please check it exists and is readable.",
                    filename);
    }

    /* get file length and reseek to the start */
    fseek(fp, 0L, SEEK_END);
    size_t buffer_len = (size_t)ftell(fp);
    fseek(fp, 0L, SEEK_SET);

    /*
     * Allocate buffer memory.
     */
    char * buffer = (char*)calloc(buffer_len+1, sizeof(char));

    if(buffer == NULL)
    {
        cdict_error(cdict,
                    CDICT_ERROR_OUT_OF_MEMORY,
                    "Cannot allocate memory to load the JSON file \"%s\"",
                    filename);
    }

    /*
     * read JSON into the buffer and close the file
     */
    if(fread(buffer,
             sizeof(char),
             buffer_len,
             fp) < buffer_len)
    {
        cdict_error(cdict,
                    CDICT_ERROR_FREAD_FAIL,
                    "Failed to read all of JSON file \"%s\" into the buffer.",
                    filename);
    }

    if(fclose(fp))
    {
        cdict_error(cdict,
                    CDICT_ERROR_FCLOSE_FAIL,
                    "Failed to close JSON file \"%s\".",
                    filename);
    }

    /*
     * Use buffer to load to cdict
     */
    cdict_json_to(buffer,
                  strlen(buffer),
                  cdict);

    free(buffer);
}

#endif // __HAVE_LIBCDICT__

typedef int prevent_ISO_C_warning;
        