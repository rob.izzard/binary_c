

#include "../binary_c.h"
No_empty_translation_unit_warning;
#undef is_integer
#ifndef __HAVE_LIBCDICT__
#include "cdict.h"
#undef exit


#include <stdio.h>

__CDict_Nonnull_some_arguments(1,2,3,4)
CDict_API_function
void cdict_to_json_file(struct cdict_t * cdict,
                        const char * const filename,
                        const char * const item_separator,
                        const char * const key_separator,
                        const Boolean indent,
                        const Boolean newlines,
                        const Boolean sort)
{
    char * json_buffer = NULL;
    size_t json_size = 0;
    CDict_to_JSON(cdict,
                  json_buffer,
                  json_size,
                  item_separator,
                  key_separator,
                  indent,
                  newlines,
                  sort);

    /* open output file */
    FILE * fp = fopen(filename, "w");

    /* detect error */
    if(fp == NULL)
    {
        cdict_error(cdict,
                    CDICT_ERROR_CANNOT_OPEN_FILE,
                    "Cannot open file at \"%s\", please check it exists and is readable.",
                    filename);
    }

    if(fwrite(json_buffer,1,json_size,fp) < json_size)
    {
        cdict_error(cdict,
                    CDICT_ERROR_FWRITE_FAIL,
                    "Failed to write all of JSON file \"%s\" into the buffer.",
                    filename);
    }

    if(fclose(fp))
    {
        cdict_error(cdict,
                    CDICT_ERROR_FCLOSE_FAIL,
                    "Failed to close JSON file \"%s\".",
                    filename);
    }

    free(json_buffer);
}

#endif // __HAVE_LIBCDICT__

typedef int prevent_ISO_C_warning;
        