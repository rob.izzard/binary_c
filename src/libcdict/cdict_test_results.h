#include "../binary_c.h"
        #ifndef __HAVE_LIBCDICT__
        
char * __cdict_test_variables_expected[] = {

//Test variable string (CTYPE char *)
R""""({
    "test data" : {
        "test of type string" : "test string"
    }
}
)"""",
//Test variable double (CTYPE double)
R""""({
    "test data" : {
        "test of type double" : -1e10
    }
}
)"""",
//Test variable int (CTYPE int)
R""""({
    "test data" : {
        "test of type int" : -1
    }
}
)"""",
//Test variable unsigned int (CTYPE unsigned int)
R""""({
    "test data" : {
        "test of type unsigned int" : 1
    }
}
)"""",
//Test variable short int (CTYPE short int)
R""""({
    "test data" : {
        "test of type short int" : -1
    }
}
)"""",
//Test variable unsigned short int (CTYPE unsigned short int)
R""""({
    "test data" : {
        "test of type unsigned short int" : 1
    }
}
)"""",
//Test variable long int (CTYPE long int)
R""""({
    "test data" : {
        "test of type long int" : -1
    }
}
)"""",
//Test variable unsigned long int (CTYPE unsigned long int)
R""""({
    "test data" : {
        "test of type unsigned long int" : 1
    }
}
)"""",
//Test variable long long int (CTYPE long long int)
R""""({
    "test data" : {
        "test of type long long int" : -1
    }
}
)"""",
//Test variable unsigned long long int (CTYPE unsigned long long int)
R""""({
    "test data" : {
        "test of type unsigned long long int" : 1
    }
}
)"""",
//Test variable char (CTYPE char)
R""""({
    "test data" : {
        "test of type char" : "c"
    }
}
)"""",
//Test variable float (CTYPE float)
R""""({
    "test data" : {
        "test of type float" : -1e10
    }
}
)"""",
//Test variable Boolean (CTYPE Boolean)
R""""({
    "test data" : {
        "test of type Boolean" : true
    }
}
)"""",
//Test variable long double (CTYPE long double)
R""""({
    "test data" : {
        "test of type long double" : -1e+10
    }
}
)"""",
//Test variable double pointer (CTYPE double *)
R""""({
    "test data" : {
        "test of type double pointer" : "0x1"
    }
}
)"""",
//Test variable int pointer (CTYPE int *)
R""""({
    "test data" : {
        "test of type int pointer" : "0x1"
    }
}
)"""",
//Test variable unsigned int pointer (CTYPE unsigned int *)
R""""({
    "test data" : {
        "test of type unsigned int pointer" : "0x1"
    }
}
)"""",
//Test variable short int pointer (CTYPE short int *)
R""""({
    "test data" : {
        "test of type short int pointer" : "0x1"
    }
}
)"""",
//Test variable unsigned short int pointer (CTYPE unsigned short int *)
R""""({
    "test data" : {
        "test of type unsigned short int pointer" : "0x1"
    }
}
)"""",
//Test variable long int pointer (CTYPE long int *)
R""""({
    "test data" : {
        "test of type long int pointer" : "0x1"
    }
}
)"""",
//Test variable unsigned long int pointer (CTYPE unsigned long int *)
R""""({
    "test data" : {
        "test of type unsigned long int pointer" : "0x1"
    }
}
)"""",
//Test variable long long int pointer (CTYPE long long int *)
R""""({
    "test data" : {
        "test of type long long int pointer" : "0x1"
    }
}
)"""",
//Test variable unsigned long long int pointer (CTYPE unsigned long long int *)
R""""({
    "test data" : {
        "test of type unsigned long long int pointer" : "0x1"
    }
}
)"""",
//Test variable void pointer (CTYPE void *)
R""""({
    "test data" : {
        "test of type void pointer" : "0x1"
    }
}
)"""",
//Test variable float pointer (CTYPE float *)
R""""({
    "test data" : {
        "test of type float pointer" : "0x1"
    }
}
)"""",
//Test variable long double pointer (CTYPE long double *)
R""""({
    "test data" : {
        "test of type long double pointer" : "0x1"
    }
}
)"""",
//Test variable Boolean pointer (CTYPE Boolean *)
R""""({
    "test data" : {
        "test of type Boolean pointer" : "0x1"
    }
}
)"""",
//Test variable char pointer (CTYPE char*)
R""""({
    "test data" : {
        "test of type char pointer" : "0x1"
    }
}
)"""",
//Test variable double array (CTYPE double[])
R""""({
    "test data" : {
        "test of type double array" : [
            -1e10,
            2e-10
        ]
    }
}
)"""",
//Test variable long double array (CTYPE long double[])
R""""({
    "test data" : {
        "test of type long double array" : [
            -1e+10,
            2e-10
        ]
    }
}
)"""",
//Test variable int array (CTYPE int[])
R""""({
    "test data" : {
        "test of type int array" : [
            -1,
            2
        ]
    }
}
)"""",
//Test variable unsigned int array (CTYPE unsigned int[])
R""""({
    "test data" : {
        "test of type unsigned int array" : [
            1,
            2
        ]
    }
}
)"""",
//Test variable short int array (CTYPE short int[])
R""""({
    "test data" : {
        "test of type short int array" : [
            -1,
            2
        ]
    }
}
)"""",
//Test variable unsigned short int array (CTYPE unsigned short int[])
R""""({
    "test data" : {
        "test of type unsigned short int array" : [
            1,
            2
        ]
    }
}
)"""",
//Test variable long int array (CTYPE long int[])
R""""({
    "test data" : {
        "test of type long int array" : [
            -1,
            2
        ]
    }
}
)"""",
//Test variable unsigned long int array (CTYPE unsigned long int[])
R""""({
    "test data" : {
        "test of type unsigned long int array" : [
            1,
            2
        ]
    }
}
)"""",
//Test variable char array (CTYPE char[])
R""""({
    "test data" : {
        "test of type char array" : "abc"
    }
}
)"""",
//Test variable float array (CTYPE float[])
R""""({
    "test data" : {
        "test of type float array" : [
            -1e+10,
            2e-10
        ]
    }
}
)"""",
//Test variable Boolean array (CTYPE Boolean[])
R""""({
    "test data" : {
        "test of type Boolean array" : [
            true,
            false
        ]
    }
}
)"""",
//Test variable long long int array (CTYPE long long int[])
R""""({
    "test data" : {
        "test of type long long int array" : [
            -1,
            2
        ]
    }
}
)"""",
//Test variable unsigned long long int array (CTYPE unsigned long long int[])
R""""({
    "test data" : {
        "test of type unsigned long long int array" : [
            1,
            2
        ]
    }
}
)"""",
//Test variable string array (CTYPE char*[])
R""""({
    "test data" : {
        "test of type string array" : [
            "abc",
            "def"
        ]
    }
}
)"""",
//Test variable void pointer array (CTYPE void*[])
R""""({
    "test data" : {
        "test of type void pointer array" : [
            "0x1",
            "0x2"
        ]
    }
}
)"""",
//Test variable pointer to cdict (CTYPE struct cdict_t*)
R""""({
    "test data" : {
        "test of type pointer to cdict" : "(nil)"
    }
}
)""""
};

#endif // __HAVE_LIBCDICT__

typedef int prevent_ISO_C_warning;
       