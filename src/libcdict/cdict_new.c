

#include "../binary_c.h"
No_empty_translation_unit_warning;
#undef is_integer
#ifndef __HAVE_LIBCDICT__
#include "cdict.h"
#undef exit


__CDict_malloc_like
CDict_API_function
struct cdict_t * cdict_new(void)
{
    /*
     * Return a pointer to a new cdict, or NULL on
     * Malloc failure.
     */
    struct cdict_t * c = __CDict_malloc(NULL,sizeof(struct cdict_t));
    cdict_clean_cdict(c);
    return c;
}

#endif // __HAVE_LIBCDICT__

typedef int prevent_ISO_C_warning;
        