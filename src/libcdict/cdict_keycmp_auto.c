

#include "../binary_c.h"
No_empty_translation_unit_warning;
#undef is_integer
#ifndef __HAVE_LIBCDICT__
#include "cdict.h"
#undef exit


__CDict_Nonnull_all_arguments
CDict_API_function
int cdict_keycmp_auto(struct cdict_t * const cdict,
                      struct cdict_key_t * const a,
                      struct cdict_key_t * const b,
                      const size_t len __CDict_maybe_unused)
{
    /*
     * Compare two cdict keys for equality.
     * Returns zero on equality, any other value when not equal.
     */
    return cdict_sort_auto(cdict,
                           a,
                           b);
}

#endif // __HAVE_LIBCDICT__

typedef int prevent_ISO_C_warning;
        