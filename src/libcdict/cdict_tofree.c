

#include "../binary_c.h"
No_empty_translation_unit_warning;
#undef is_integer
#ifndef __HAVE_LIBCDICT__
#include "cdict.h"
#undef exit



/*
 * cdict_push_tofree
 *    Given a pointer p, push this onto the list of pointers
 *    stored in cdict. When cdict is freed, so are these pointers.
 *
 * cdict_free_tofree
 *    Function called when a cdict is freed to do the freeing.
 */

CDict_API_function
void cdict_push_tofree(struct cdict_t * const cdict,
                       void * p)
{
    if(p != NULL)
    {
        cdict->tofree = realloc(cdict->tofree,
                                (cdict->tofree_n+1) * sizeof(void *));
        cdict->tofree[cdict->tofree_n] = p;
        cdict->tofree_n++;
    }
}

CDict_API_function
void cdict_free_from_tofree(struct cdict_t * const cdict,
                            void * p)
{
    /*
     * Look for pointer p in the tofree list,
     * free it if it's there.
     */
    if(p != NULL &&
       cdict->tofree != NULL)
    {
        for(size_t i=0;i<cdict->tofree_n;i++)
        {
            if(p == cdict->tofree[i])
            {
                free(cdict->tofree[i]);
                cdict->tofree[i] = NULL;
                break;
            }
        }
    }
}


CDict_API_function
void cdict_replace_tofree(struct cdict_t * const cdict,
                          void * was,
                          void * is)
{
    /*
     * Look for pointer *was in the tofree list,
     * replace it with pointer *is if it is there.
     */
    if(was != NULL &&
       cdict->tofree != NULL)
    {
        for(size_t i=0;i<cdict->tofree_n;i++)
        {
            if(cdict->tofree[i] == was)
            {
                cdict->tofree[i] = is;
                break;
            }
        }
    }
}


CDict_API_function
void cdict_free_tofree(struct cdict_t * const cdict)
{
    if(cdict->tofree != NULL)
    {
        for(size_t i=0;i<cdict->tofree_n;i++)
        {
            if(cdict->tofree[i] != NULL)
            {
                free(cdict->tofree[i]);
                cdict->tofree[i] = NULL;
            }
        }
        free(cdict->tofree);
        cdict->tofree = NULL;
    }
}

#endif // __HAVE_LIBCDICT__

typedef int prevent_ISO_C_warning;
        