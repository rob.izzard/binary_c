

#include "../binary_c.h"
No_empty_translation_unit_warning;
#undef is_integer
#ifndef __HAVE_LIBCDICT__
#include "cdict.h"
#undef exit


__CDict_Pure_function
__CDict_Nonnull_all_arguments
Boolean cdict_contains_cdicts(const struct cdict_t * const cdict)
{
    /*
     * Return TRUE if this cdict contains cdicts
     * in its values (does not test the children).
     */
    CDict_loop(cdict,entry)
    {
        if(entry->value.type == CDICT_DATA_TYPE_CDICT)
        {
            return TRUE;
        }
    }
    return FALSE;
}

#endif // __HAVE_LIBCDICT__

typedef int prevent_ISO_C_warning;
        