#include "../binary_c.h"
        #ifndef __HAVE_LIBCDICT__
        
#pragma once
#ifndef UTHASH_BINARY_C_H
#define UTHASH_BINARY_C_H
#include <stdio.h>
/*
 * Wrapper to load uthash using libcdict
 */
#define __BINARY_C_UTHASH

#ifndef CDICT_UTHASH_KEYCMP
#define CDICT_UTHASH_KEYCMP(hash,a,b,len)       \
    (int)cdict_keycmp_auto(                     \
        (hash),                                 \
        (struct cdict_key_t *)(a),              \
        (struct cdict_key_t *)(b),              \
        (len))
#endif

/*
  #ifndef CDICT_UTHASH_FUNCTION
  #define CDICT_UTHASH_FUNCTION(cdict,s,len,hashv)            \
  (hashv) = cdict_key_hash((cdict),                       \
  (struct cdict_key_t *)(s),   \
  (len))
  #endif
*/
#include "cdict_uthash.h"

#endif // UTHASH_BINARY_C_H

#endif // __HAVE_LIBCDICT__

typedef int prevent_ISO_C_warning;
       