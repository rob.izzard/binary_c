#include "../binary_c.h"
        #ifndef __HAVE_LIBCDICT__
        
#pragma once
#ifndef CDICT_CODE_OPTIONS_H
#define CDICT_CODE_OPTIONS_H

#ifdef __GNUC__
// GNU C specific options
#if __GNUC__ >= 4
#define __CDict_hot_function __attribute__((hot))
#define __CDict_Nonnull_all_arguments __attribute__((nonnull))
#define __CDict_Nonnull_some_arguments(...) __attribute__((nonnull(__VA_ARGS__)))
#define __CDict_maybe_unused __attribute__ ((unused))
#if (__GNUC__ ==4 && __GNU_MINOR__>=7) || __GNUC__ > 4
#define __CDict_Gnu_format_args(...) __attribute__((format (gnu_printf,__VA_ARGS__)))
#define __CDict_malloc_like __attribute__((malloc))
#endif//gcc >=4.7
#if (__GNUC__ ==4 && __GNU_MINOR__>9) || __GNUC__ > 4
#define __CDict_autotype(X) __auto_type
#endif//gcc >=4.9
#elif __GNUC__ == 3
#define __CDict_maybe_unused /* nothing */
#else
#endif // GCC version check
#else // gcc check
#endif

#if defined __GNUC__ && __GNUC__ >=3
#define __CDict_Pure_function __attribute__((pure))
#if DEBUG==0
#define __CDict_Pure_function_if_no_debug __attribute__((pure))
#endif
#endif


/* defaults */
#ifndef __CDict_maybe_unused
#define __CDict_maybe_unused /* maybe unused : do nothing */
#endif
#ifndef __CDict_autotype
#define __CDict_autotype(X) typeof(X)
#endif
#ifndef __CDict_Gnu_format_args
#define __CDict_Gnu_format_args(...)
#endif
#ifndef __CDict_Nonnull_some_arguments
#define __CDict_Nonnull_some_arguments(...)
#endif
#ifndef __CDict_Nonnull_all_arguments
#define __CDict_Nonnull_all_arguments
#endif
#ifndef __CDict_malloc_like
# define __CDict_malloc_like
#endif
#ifndef __CDict_Pure_function
# define __CDict_Pure_function
#endif


#undef __CDict_Pure_function
#define __CDict_Pure_function

#undef __CDict_inline_function
#define __CDict_inline_function inline

#endif // CDICT_CODE_OPTIONS_H

#endif // __HAVE_LIBCDICT__

typedef int prevent_ISO_C_warning;
       