

#include "../binary_c.h"
No_empty_translation_unit_warning;
#undef is_integer
#ifndef __HAVE_LIBCDICT__
#include "cdict.h"
#undef exit


void cdict_free_entry_contents(struct cdict_t * const cdict,
                               struct cdict_t * const tofree_cdict,
                               struct cdict_entry_t ** const entryp,
                               cdict_metadata_free_f free_function,
                               const Boolean free_value_contents)
{
    /*
     * Free a cdict entry's contents.
     */
    const Boolean vb = cdict->vb;
    if(entryp == NULL) return;
    struct cdict_entry_t * const entry = *entryp;
    if(entry == NULL) return;

    if(vb)printf("Free entry %p -> value.type %d (metadata %p, tofree_cdict %p, free_function %p)\n",
                 (void*)entry,
                 entry->value.type,
                 (void*)entry->metadata,
                 (void*)tofree_cdict,
                 __CDict_cast_function_pointer(free_function));

    if(entry->value.type == CDICT_DATA_TYPE_CDICT)
    {
        /*
         * Nested cdict : recursively call cdict_free
         */
        cdict_free(&entry->value.value.cdict_pointer_data,
                   free_value_contents,
                   free_function);
    }
    else if(entry->value.type == CDICT_DATA_TYPE_CDICT_ARRAY)
    {
        for(cdict_size_t i=0; i<entry->value.count; i++)
        {
            cdict_free(&entry->value.value.cdict_array_data[i],
                       free_value_contents,
                       free_function);
        }
    }

    /*
     * Free cached strings
     */
    if(entry->key.string != NULL)
    {
        CDict_Safe_free(cdict,entry->key.string);
    }

    {
        struct cdict_key_t * pp = &entry->key;
        cdict_free_key(cdict,&pp,FALSE);
    }

    /*
     * Free value contents
     */
    if(free_value_contents == TRUE &&
       __CDict_datatype_is_pointer(entry->value.type))
    {
        void * p = cdict_union_to_pointer(&entry->value.value,
                                          entry->value.type);
        CDict_Safe_free(cdict,p);
    }

    if(vb)
    {
        printf("tofree_cdict %p metadata %p\n",
               (void*)tofree_cdict,
               (void*)entry->metadata
            );
    }

    if(tofree_cdict != NULL)
    {
        /*
         * Use provided function, if given
         */
        if(free_function != NULL)
        {
            void * const data_pointer = entry->metadata != NULL ? (void*)entry->metadata->data : NULL;
            if(vb)
            {
                printf("cdict_free : call cdict_contains (1) on type %d with data %p\n",
                       CDICT_DATA_TYPE_VOID_POINTER,
                       (void*)data_pointer
                    );
            }
            if(data_pointer != NULL &&
               cdict_contains(tofree_cdict,
                              __extension__(union cdict_key_union)data_pointer,
                              CDICT_DATA_TYPE_VOID_POINTER)==NULL)
            {
                struct cdict_tofree_t * tofree = malloc(sizeof(struct cdict_tofree_t));
                tofree->metadata = entry->metadata->data;
                entry->metadata->data = NULL;
                tofree->metadata_free_function = free_function;
                entry->metadata->free_function = NULL;
                if(vb)
                {
                    printf("Map entry %p (with metadata = %p, metadata->data = %p) to tofree %p : Set up tofree %p %p\n",
                           (void*)entry,
                           (void*)entry->metadata,
                           (void*)entry->metadata->data,
                           (void*)tofree,
                           (void*)tofree->metadata,
                           __CDict_cast_function_pointer(tofree->metadata_free_function));
                }
                CDict_set_with_types(tofree_cdict,
                                     /* key = metadata->data pointer */
                                     (void*)entry->metadata->data,
                                     CDICT_DATA_TYPE_VOID_POINTER,
                                     /* value = tofree_t struct */
                                     (void*)tofree,
                                     CDICT_DATA_TYPE_VOID_POINTER);
            }
        }
        /*
         * Use inbuilt free function, if it exists
         */
        else if(entry->metadata != NULL &&
                entry->metadata->free_function != NULL)
        {
            char * const c = (char*) entry->metadata;
            if(vb)
            {
                printf("cdict_free : call cdict_contains (2) on type %d\n",
                       CDICT_DATA_TYPE_VOID_POINTER
                    );
            }
            if(cdict_contains(tofree_cdict,
                              __extension__(union cdict_key_union)c,
                              CDICT_DATA_TYPE_VOID_POINTER) == NULL)
            {
                struct cdict_tofree_t * const tofree =
                    malloc(sizeof(struct cdict_tofree_t));
                tofree->metadata = entry->metadata->data;
                tofree->metadata_free_function = entry->metadata->free_function;
                CDict_set_with_types(tofree_cdict,
                                     (void*)entry->metadata->data,
                                     CDICT_DATA_TYPE_VOID_POINTER,
                                     (void*)tofree,
                                     CDICT_DATA_TYPE_VOID_POINTER);
            }
        }
    }

    /*
     * Free metadata struct
     */
    __CDict_Safe_free(cdict,entry->metadata);

    /*
     * Free pre-output struct
     */
    __CDict_Safe_free(cdict,entry->pre_output);
}

#endif // __HAVE_LIBCDICT__

typedef int prevent_ISO_C_warning;
        