#include "../binary_c.h"
        #ifndef __HAVE_LIBCDICT__
        
#pragma once
#ifndef CDICT_TYPES_T
#define CDICT_TYPES_T

/*
 * Variable types, e.g. structs and unions, and some
 * C types such as Boolean, as used in libcdict.
 */

/*
 * Hash key and value types are (unsigned, short) integers, listed
 * below, after the variable type definitions.
 */
#define CDict_data_type unsigned short int
#define CDict_group_type unsigned short int
#define CDict_numeric_type unsigned short int
#define CDict_key_type CDict_data_type
#define CDict_value_type CDict_data_type


/*
 * Signed size, useful because -1 is a size
 * that we use to represent an error.
 */
typedef long long int cdict_size_t;


/*
 * Action values
 */
#define CDict_action_type unsigned short int

/*
 * We require a Boolean type, also
 * define our own TRUE and FALSE
 */
#ifndef Boolean
#undef FALSE
#undef TRUE


#if __STDC_VERSION__ >= 199901L
#undef _C99
#define _C99
#endif

#ifdef _C99
#include <stdbool.h>
#ifndef __USE_C99_BOOLEAN__
#define __USE_C99_BOOLEAN__
#endif // __USE_C99_BOOLEAN__
#endif

#ifdef __USE_C99_BOOLEAN__
/*
 * Use C99's Boolean type
 */
#define Boolean bool
#define FALSE ((bool)false)
#define TRUE ((bool)true)
#else
/*
 * Use unsigned integer 0 and 1
 * as Boolean type
 */
typedef unsigned int Boolean;
#define FALSE (0)
#define TRUE (1)
#endif //_C99
#endif // __USE_C99_BOOLEAN__

#include "cdict_macros.h"
#include "uthash_libcdict.h"

/*
 * Data-type unions: these are either for keys or values.
 * Keys only differ in that they cannot be some types,
 * e.g. cdicts.
 */
#define __cdict_union_members                                       \
    float float_data;                                               \
    double double_data;                                             \
    long double long_double_data;                                   \
    int int_data;                                                   \
    short int short_int_data;                                       \
    long int long_int_data;                                         \
    long long int long_long_int_data;                               \
    char char_data;                                                 \
    Boolean Boolean_data;                                           \
    unsigned int unsigned_int_data;                                 \
    unsigned short int unsigned_short_int_data;                     \
    unsigned long int unsigned_long_int_data;                       \
    unsigned long long int unsigned_long_long_int_data;             \
    float * float_pointer_data;                                     \
    double * double_pointer_data;                                   \
    long double * long_double_pointer_data;                         \
    int * int_pointer_data;                                         \
    short int * short_int_pointer_data;                             \
    long int * long_int_pointer_data;                               \
    long long int * long_long_int_pointer_data;                     \
    void * void_pointer_data;                                       \
    char * string_data;                                             \
    char * char_pointer_data;                                       \
    unsigned int * unsigned_int_pointer_data;                       \
    unsigned short int * unsigned_short_int_pointer_data;           \
    unsigned long int * unsigned_long_int_pointer_data;             \
    unsigned long long int * unsigned_long_long_int_pointer_data;   \
    Boolean * Boolean_pointer_data;                                 \
    float * float_array_data;                                       \
    double * double_array_data;                                     \
    long double * long_double_array_data;                           \
    int * int_array_data;                                           \
    short int * short_int_array_data;                               \
    long int * long_int_array_data;                                 \
    long long int * long_long_int_array_data;                       \
    char * char_array_data;                                         \
    char ** string_array_data;                                      \
    void ** void_pointer_array_data;                                \
    unsigned int * unsigned_int_array_data;                         \
    unsigned short int * unsigned_short_int_array_data;             \
    unsigned long int * unsigned_long_int_array_data;               \
    unsigned long long int * unsigned_long_long_int_array_data;     \
    Boolean * Boolean_array_data

union cdict_key_union{
    __cdict_union_members;
};

union cdict_value_union{
    __cdict_union_members;
    struct cdict_t * cdict_pointer_data;
    struct cdict_t ** cdict_array_data;
};

/*
 * Union to store a key mapped to a sortable
 * type
 */
union cdict_mapped_key_union {
    long double long_double_data;
    char * string_data;
    void * pointer_data;
    long long int long_long_int_data;
};

/*
 * A cdict key
 */
struct cdict_key_t {
    union cdict_mapped_key_union map_data;
    union cdict_key_union key;
    char * format;
    char * string;
    long double map_as_long_double;
    CDict_key_type map_type;
    CDict_key_type type;
    Boolean mapped;
};


/*
 * A cdict value
 */
struct cdict_value_t {
    union cdict_value_union value;
    char * format;
    char * format_deref;
    cdict_size_t count;
    CDict_value_type type;
    Boolean dynamically_allocated;
};

/*
 * Struct to hold the cdict entry's key, value
 * and metadata.
 *
 * The key can be any normal variable type, but
 * not a nested cdict.
 *
 * The data can hold any normal variable type or
 * a nested cdict.
 *
 * The metadata is a void* that can be used to store
 * anything you might use as a "label". This is
 * never output: it is for internal accounting
 * only. It is set up as a void* so can, naturally,
 * point to anything with a cast. You are responsible
 * for freeing any memory to which it points, unless
 * CDict_free is called with a TRUE in its free_contents
 * parameter, in which case the metadata is freed with
 * CDict_free. However, this will not free a complicated
 * nested structure: such tasks are yours.
 *
 * Note that to avoid issues with circular compilation,
 * the cdict_metadata_free_f takes a cdict and a  void*, i.e. any pointer.
 * It will be fed a struct cdict_entry_t *.
 */

struct cdict_entry_t {
    cdict_UT_hash_handle CDICT_HANDLE;
    struct cdict_pre_output_t * pre_output;
    struct cdict_metadata_t * metadata;
    struct cdict_value_t value;
    struct cdict_key_t key;
//    size_t key_array_len;
//    size_t value_array_len;
};

struct cdict_metadata_t {
    void (*free_function)(struct cdict_t * cdict,
                          struct cdict_entry_t *);
    void * data;
};

struct cdict_pre_output_t {
    void (*function)(struct cdict_t * cdict,
                     struct cdict_entry_t * entry,
                     void * pre_output_function_data);
    void * data;
};
/*
 * Function pointers
 */
typedef void (*cdict_metadata_free_f)(struct cdict_t * cdict,
                                      struct cdict_entry_t *);

typedef void (*cdict_pre_output_f)(struct cdict_t * cdict,
                                   struct cdict_entry_t * entry,
                                   void * pre_output_function_data);

/*
 * Pointer-allocation list item
 */
struct cdict_pointer_list_item_t {
    void * pointer;
    size_t size;
    unsigned int mode;
};

/*
 * Statistics output: used for debugging
 */
struct cdict_stats_t {
    struct cdict_pointer_list_item_t ** pointer_list;
    size_t malloc_count;
    size_t calloc_count;
    size_t realloc_count;
    size_t asprintf_count;
    size_t free_count;
    size_t free_null_count;
    size_t malloc_size;
    size_t calloc_size;
    size_t realloc_size;
    size_t asprintf_size;
    size_t pointer_list_n;
    Boolean pointer_list_full;
};

/*
 * The main struct to hold a cdict.
 *
 * Once allocated, the pointer to this should not change (except
 * when the cdict is freed) however the cdict_entry_list can and
 * probably will (this is used by uthash).
 *
 * You should always use the cdict_t in your code.
 */
struct cdict_t {
    struct cdict_entry_t * cdict_entry_list;
    struct cdict_t * parent;
    struct cdict_t * ancestor;
    struct cdict_stats_t * stats;
    void ** tofree;
    void * error_data;
    int (*error_handler)(void *,
                         const int ,
                         const char * const ,
                         va_list );
    size_t alloced;
    size_t max_size;
    size_t tofree_n;
    Boolean vb;
    Boolean use_cache;
    Boolean force_maps;
};


struct cdict_tofree_t {
    cdict_metadata_free_f metadata_free_function;
    void * metadata;
};

struct cdict_iterator_t {
    struct cdict_entry_stack_t * stack;
};

struct cdict_entry_stack_t {
    struct cdict_entry_stack_item_t ** items;
    size_t nstack;
};

struct cdict_entry_stack_item_t {
    struct cdict_entry_t * entry;
    Boolean entered;
};

#endif // CDICT_TYPES_T

#endif // __HAVE_LIBCDICT__

typedef int prevent_ISO_C_warning;
       