

#include "../binary_c.h"
No_empty_translation_unit_warning;
#undef is_integer
#ifndef __HAVE_LIBCDICT__
#include "cdict.h"
#undef exit


#include <stdio.h>
#include "cdict_fast_double_parser.h"

/*
 * Fast double-precision number parser, based on code
 *
 * at https://github.com/lemire/fast_double_parser
 *
 * which is distributed under the apache licence,
 * in this directory at LICENSE_fast_double_parser.
 *
 * This C version is taken from binary_c
 * https://gitlab.com/binary_c/
 */

/*
 * fast_double_parser
 *
 * Parse the string contained in *p and
 * put the resulting number in outDouble.
 *
 * Return the final location of successful parsing,
 * similar to the endptr in strtod.
 *
 * Return a NULL pointer on error.
 */
char * cdict_fast_double_parser(const char * p,
                                double * const outDouble)
{
    /*
     * Detect empty input string: this is an error
     */
    if(strlen(p)<=0) return NULL;

    /*
     * Save the pointer in case of failure and requirement to
     * call strtod as a backup
     */
    const char * const pinit = p;

    /*
     * Skip leading plus signs
     */
    while(*p == '+')
    {
        ++p;
    }

    /*
     * Detect leading minus sign(s)
     * and hence the sign of the number
     */
    Boolean found_minus = FALSE;
    Boolean negative = FALSE;
    while(*p == '-')
    {
        found_minus = TRUE;
        ++p;
        negative = !negative;
    }

    /*
     * Detect leading '.'
     */
    const Boolean leading_decimal_point = *p == '.';

    /*
     * Detect trailing '.' and hence pmax, the maximum
     * value that p can take when we're looping
     */
    const Boolean trailing_decimal_point = *(p+strlen(p)-1) == '.';
    const char * const pmax = p + strlen(p) -
        (size_t)(trailing_decimal_point ? 1 : 0);

    /*
     * Allow e.g. 0000123 by skipping the zeroes
     */
    while(*p == '0' && is_integer(*(p+1)))
    {
        p++;
    }

    if(found_minus && !(is_integer(*p) || leading_decimal_point))
    {
        /*
         * a negative sign must be followed by an integer
         */
        return NULL;
    }

    /*
     * Save the location of the first digit
     * (except leading 0s)
     */
    const char *const first_digit = p - (leading_decimal_point ? 1 : 0);

    /*
     *  an unsigned int avoids signed overflows (which are bad)
     */
    uint64_t i;
    if(*p == '0')
    {
        /*
         *  0 cannot be followed by an integer
         */
        ++p;
        if(is_integer(*p))
        {
            return NULL;
        }
        i = 0;
    }
    else
    {
        if(!leading_decimal_point && !(is_integer(*p)))
        {
            /*
             *  must start with an integer
             */
            return NULL;
        }

        unsigned char digit;
        if(leading_decimal_point)
        {
            /*
             * Pretend we start with '0'
             */
            digit = 0;
        }
        else
        {
            digit = *p - '0';
            p++;
        }
        i = digit;

        /*
         *  the is_made_of_eight_digits_fast routine is unlikely to help here because
         *  we rarely see large integer parts like 123456789
         */
        while(p<=pmax && is_integer(*p))
        {
            digit = *p - '0';
            /*
             *  a multiplication by 10 is cheaper than an arbitrary integer
             *  multiplication
             */
            i = 10 * i + digit;

            /*
             * might overflow, we will handle the overflow later
             */
            ++p;
        }
    }

    int64_t exponent = 0;
    if(*p == '.' && p<pmax)
    {
        ++p;
        const char * first_after_decimal_point = p;
        if(is_exponent_char(*first_after_decimal_point))
        {
            /*
             * 1.e2 type, pretend this is 1.0e2
             */
        }
        else if(is_integer(*first_after_decimal_point))
        {
            const unsigned char digit = *p - '0';
            ++p;
            i = i * 10 + digit;
            /*
             *  might overflow + multiplication by 10 is likely
             *  cheaper than arbitrary mult.
             *  we will handle the overflow later
             */
        }
        else
        {
            return NULL;
        }

        while(is_integer(*p))
        {
            const unsigned char digit = *p - '0';
            ++p;
            i = i * 10 + digit;
            /*
             *  in rare cases, this will overflow, but that's ok
             *  because we have parse_highprecision_float later.
             */
        }
        exponent = first_after_decimal_point - p;
    }

    int digit_count =
        (int)(p - first_digit - 1); /* used later to guard against overflows */

    if(is_exponent_char(*p))
    {
        ++p;
        Boolean neg_exp = FALSE;
        if('-' == *p)
        {
            neg_exp = TRUE;
            ++p;
        }
        else if('+' == *p)
        {
            ++p;
        }

        if(!is_integer(*p))
        {
            return NULL;
        }

        unsigned char digit = *p - '0';
        int64_t exp_number = digit;
        p++;
        if(is_integer(*p))
        {
            digit = *p - '0';
            exp_number = 10 * exp_number + digit;
            ++p;
        }
        if(is_integer(*p))
        {
            digit = *p - '0';
            exp_number = 10 * exp_number + digit;
            ++p;
        }
        while(is_integer(*p))
        {
            digit = *p - '0';
            if(exp_number < 0x100000000)
            {
                /*
                 * we need to check for overflows
                 */
                exp_number = 10 * exp_number + digit;
            }
            ++p;
        }
        exponent += (neg_exp ? -exp_number : exp_number);
    }
    /*
     * If we frequently had to deal with long strings of digits,
     * we could extend our code by using a 128-bit integer instead
     * of a 64-bit integer. However, this is uncommon.
     */
    if(unlikely(digit_count >= 19))
    {
        /*
         * this is uncommon
         * It is possible that the integer had an overflow.
         * We have to handle the case where we have 0.0000somenumber.
         */
        const char *start = first_digit;
        while(*start == '0' || *start == '.')
        {
            start++;
        }

        /*
         * we over-decrement by one when there is a decimal separator
         */
        digit_count -= (int)(start - first_digit);
        if(digit_count >= 19)
        {
            /*
             *  Chances are good that we had an overflow!
             *  We start anew (with strtod)
             *  This will happen in the following examples:
             *  10000000000000000000000000000000000000000000e+308
             *  3.1415926535897932384626433832795028841971693993751
             */
            return parse_float_strtod(first_digit, negative, outDouble);
        }
    }

    if(unlikely(exponent < FASTFLOAT_SMALLEST_POWER) ||
       (exponent > FASTFLOAT_LARGEST_POWER))
    {
        /*
         *  this is almost never going to get called!!!
         *  exponent could be as low as 325
         */
        return parse_float_strtod(first_digit, negative, outDouble);
    }

    /*
     *  from this point forward, exponent >= FASTFLOAT_SMALLEST_POWER and
     *  exponent <= FASTFLOAT_LARGEST_POWER
     */
    Boolean success = TRUE;
    *outDouble = compute_float_64(exponent, i, negative, &success);
    if(!success)
    {
        /*
         * We are almost never going to get here.
         */
        return parse_float_strtod(first_digit, pinit, outDouble);
    }
    return (char*) p;
}


/*
 * Backup call to strtod when given a number that is hard to
 * parse
 */
static char * parse_float_strtod(const char * const ptr,
                                 const Boolean negative,
                                 double *const outDouble)
{
    char * endptr;
    *outDouble = strtod(ptr,&endptr) * (negative ? -1.0 : 1.0);
    return endptr;
}

/*
 * Attempts to compute i * 10^(power) exactly; and if "negative" is
 * true, negate the result.
 * This function will only work in some cases, when it does not work, success is
 * set to FALSE. This should work *most of the time* (like 99% of the time).
 * We assume that power is in the [FASTFLOAT_SMALLEST_POWER,
 * FASTFLOAT_LARGEST_POWER] interval: the caller is responsible for this check.
 */
static double compute_float_64(const int64_t power,
                               uint64_t i,
                               const Boolean negative,
                               Boolean * const success)
{
    /*
     * Precomputed powers of ten from 10^0 to 10^22. These
     * can be represented exactly using the double type.
     */
    static const double power_of_ten[] = {
        1e0,  1e1,  1e2,  1e3,  1e4,  1e5,  1e6,  1e7,  1e8,  1e9,  1e10, 1e11,
        1e12, 1e13, 1e14, 1e15, 1e16, 1e17, 1e18, 1e19, 1e20, 1e21, 1e22};

    /*
     * The mantissas of powers of ten from -308 to 308, extended out to sixty four
     * bits. The array contains the powers of ten approximated
     * as a 64-bit mantissa. It goes from 10^FASTFLOAT_SMALLEST_POWER to
     * 10^FASTFLOAT_LARGEST_POWER (inclusively).
     * The mantissa is truncated, and
     * never rounded up. Uses about 5KB.
     */
    static const uint64_t mantissa_64[] = { MANTISSA_64_DATA };
    /*
     * A complement to mantissa_64
     * complete to a 128-bit mantissa.
     * Uses about 5KB but is rarely accessed.
     */
    static const uint64_t mantissa_128[] = { MANTISSA_128_DATA};

    /*
     * we start with a fast path
     * It was described in
     * Clinger WD. How to read floating point numbers accurately.
     * ACM SIGPLAN Notices. 1990
     */
#if(FLT_EVAL_METHOD != 1) && (FLT_EVAL_METHOD != 0)
    /*
     * we do not trust the divisor
     */
    if(0 <= power && power <= 22 && i <= 9007199254740991)

#else
    if(-22 <= power && power <= 22 && i <= 9007199254740991)

#endif
    {
        /*
         * convert the integer into a double. This is lossless since
         * 0 <= i <= 2^53 - 1.
         * RGI : moved negative here.
         */
        double d = (negative ? -1.0 : 1.0) * (double)(i);

        /*
         * The general idea is as follows.
         * If 0 <= s < 2^53 and if 10^0 <= p <= 10^22 then
         * 1) Both s and p can be represented exactly as 64-bit floating-point
         * values
         * (binary64).
         * 2) Because s and p can be represented exactly as floating-point values,
         * then s * p
         * and s / p will produce correctly rounded values.
         */
        if(power < 0)
        {
            d /= power_of_ten[-power];
        }
        else
        {
            d *= power_of_ten[power];
        }

        *success = TRUE;
        return d;
    }

    /*
     * When 22 < power && power <  22 + 16, we could
     * hope for another, secondary fast path.  It wa
     * described by David M. Gay in  "Correctly rounded
     * binary-decimal and decimal-binary conversions." (1990)
     * If you need to compute i * 10^(22 + x) for x < 16,
     * first compute i * 10^x, if you know that result is exact
     * (e.g., when i * 10^x < 2^53),
     * then you can still proceed and do (i * 10^x) * 10^22.
     * Is this worth your time?
     * You need  22 < power *and* power <  22 + 16 *and* (i * 10^(x-22) < 2^53)
     * for this second fast path to work.
     * If you you have 22 < power *and* power <  22 + 16, and then you
     * optimistically compute "i * 10^(x-22)", there is still a chance that you
     * have wasted your time if i * 10^(x-22) >= 2^53. It makes the use cases of
     * this optimization maybe less common than we would like. Source:
     * http://www.exploringbinary.com/fast-path-decimal-to-floating-point-conversion/
     * also used in RapidJSON: https://rapidjson.org/strtod_8h_source.html
     */

    /*
     * The fast path has now failed, so we are failing back on the slower path.
     *
     * In the slow path, we need to adjust i so that it is > 1<<63 which is always
     * possible, except if i == 0, so we handle i == 0 separately.
     */
    if(i == 0)
    {
        return negative ? -0.0 : 0.0;
    }
    else
    {
        /*
         * We are going to need to do some 64-bit arithmetic to get a more precise product.
         * We use a table lookup approach.
         * It is safe because
         * power >= FASTFLOAT_SMALLEST_POWER
         * and power <= FASTFLOAT_LARGEST_POWER
         * We recover the mantissa of the power, it has a leading 1. It is always
         * rounded down.
         */
        const uint64_t factor_mantissa = mantissa_64[power - FASTFLOAT_SMALLEST_POWER];

        /*
         * The exponent is 1024 + 63 + power
         *     + floor(log(5**power)/log(2)).
         * The 1024 comes from the ieee64 standard.
         * The 63 comes from the fact that we use a 64-bit word.
         *
         * Computing floor(log(5**power)/log(2)) could be
         * slow. Instead we use a fast function.
         *
         * For power in (-400,350), we have that
         * (((152170 + 65536) * power ) >> 16);
         * is equal to
         *  floor(log(5**power)/log(2)) + power when power >= 0
         * and it is equal to
         *  ceil(log(5**-power)/log(2)) + power when power < 0
         *
         * The 65536 is (1<<16) and corresponds to
         * (65536 * power) >> 16 ---> power
         *
         * ((152170 * power ) >> 16) is equal to
         * floor(log(5**power)/log(2))
         *
         * Note that this is not magic: 152170/(1<<16) is
         * approximatively equal to log(5)/log(2).
         * The 1<<16 value is a power of two; we could use a
         * larger power of 2 if we wanted to.
         */
        const int64_t exponent = (((152170 + 65536) * power) >> 16) + 1024 + 63;

        /*
         * We want the most significant bit of i to be 1. Shift if needed.
         */
        int lz = leading_zeroes(i);
        i <<= lz;

        /*
         * We want the most significant 64 bits of the product. We know
         * this will be non-zero because the most significant bit of i is
         * 1.
         */
        struct value128_t product = full_multiplication(i, factor_mantissa);
        uint64_t lower = product.low;
        uint64_t upper = product.high;

        /*
         * We know that upper has at most one leading zero because
         * both i and  factor_mantissa have a leading one. This means
         * that the result is at least as large as ((1<<63)*(1<<63))/(1<<64).
         *
         * As long as the first 9 bits of "upper" are not "1", then we
         * know that we have an exact computed value for the leading
         * 55 bits because any imprecision would play out as a +1, in
         * the worst case.
         * Having 55 bits is necessary because
         * we need 53 bits for the mantissa but we have to have one rounding bit and
         * we can waste a bit if the most significant bit of the product is zero.
         * We expect this next branch to be rarely taken (say 1% of the time).
         * When (upper & 0x1FF) == 0x1FF, it can be common for
         * lower + i < lower to be TRUE (proba. much higher than 1%).
         */
        if(unlikely((upper & 0x1FF) == 0x1FF) && (lower + i < lower))
        {
            const uint64_t factor_mantissa_low =
                mantissa_128[power - FASTFLOAT_SMALLEST_POWER];
            /*
             * next, we compute the 64-bit x 128-bit multiplication, getting a 192-bit
             * result (three 64-bit values)
             */
            product = full_multiplication(i, factor_mantissa_low);

            const uint64_t product_low = product.low;
            const uint64_t product_middle2 = product.high;
            const uint64_t product_middle1 = lower;
            const uint64_t product_middle = product_middle1 + product_middle2;
            const uint64_t product_high = upper +
                (product_middle < product_middle1 ? 1 : 0);

            /*
             * we want to check whether mantissa *i + i would affect our result
             * This does happen, e.g. with 7.3177701707893310e+15
             */
            if(((product_middle + 1 == 0) &&
                ((product_high & 0x1FF) == 0x1FF) &&
                (product_low + i < product_low)))
            {
                /*
                 * let us be prudent and bail out.
                 * success = FALSE;
                 */
                return 0;
            }
            else
            {
                upper = product_high;
                lower = product_middle;
            }
        }

        /*
         * The final mantissa should be 53 bits with a leading 1.
         * We shift it so that it occupies 54 bits with a leading 1.
         */
        const uint64_t upperbit = upper >> 63;
        uint64_t mantissa = upper >> (upperbit + 9);
        lz += (int)(1 ^ upperbit);

        /*
         * Here we have mantissa < (1<<54).
         *
         * We have to round to even. The "to even" part
         * is only a problem when we are right in between two floats
         * which we guard against.
         * If we have lots of trailing zeros, we may fall right between two
         * floating-point values.
         */
        if(unlikely((lower == 0) && ((upper & 0x1FF) == 0) &&
                    ((mantissa & 3) == 1)))
        {
            /*
             * if mantissa & 1 == 1 we might need to round up.
             *
             * Scenarios:
             * 1. We are not in the middle. Then we should round up.
             *
             * 2. We are right in the middle. Whether we round up depends
             * on the last significant bit: if it is "one" then we round
             * up (round to even) otherwise, we do not.
             *
             * So if the last significant bit is 1, we can safely round up.
             * Hence we only need to bail out if(mantissa & 3) == 1.
             * Otherwise we may need more accuracy or analysis to determine whether
             * we are exactly between two floating-point numbers.
             * It can be triggered with 1e23.
             * Note: because the factor_mantissa and factor_mantissa_low are
             * almost always rounded down (except for small positive powers),
             * almost always should round up.
             */
            *success = FALSE;
            return 0;
        }
        else
        {
            mantissa += mantissa & 1;
            mantissa >>= 1;

            /*
             * Here we have mantissa < (1<<53), unless there was an overflow
             */
            if(mantissa >= (1ULL << 53))
            {
                /*
                 * This will happen when parsing values such as 7.2057594037927933e+16
                 */
                mantissa = (1ULL << 52);
                lz--; /* undo previous addition */
            }
            mantissa &= ~(1ULL << 52);
            const uint64_t real_exponent = exponent - lz;

            /*
             * we have to check that real_exponent is in range, otherwise we bail out
             */
            if(unlikely((real_exponent < 1) ||
                        (real_exponent > 2046)))
            {
                *success = FALSE;
                return 0;
            }
            else
            {
                mantissa |= real_exponent << 52;
                mantissa |= (((uint64_t)negative) << 63);
                double d;
                memcpy(&d, &mantissa, sizeof(d));
                *success = TRUE;
                return d;
            }
        }
    }
}

static __CDict_Pure_function inline Boolean is_integer(const char c)
{
    // this gets compiled to (uint8_t)(c - '0') <= 9 on all decent compilers
    return (c >= '0' && c <= '9');
}

static __CDict_Pure_function inline Boolean is_exponent_char(const char c)
{
    /* detect exponent char, e or E in C */
    return (('e' == c) ||
            ('E' == c) ||
            /* allow FORTRAN as well */
            ('d' == c) ||
            ('D' == c));

}

/* result might be undefined when input_num is zero */
static inline int leading_zeroes(uint64_t input_num)
{
#ifdef _MSC_VER
#if defined(_M_X64) || defined(_M_ARM64) || defined (_M_IA64)
    unsigned long leading_zero = 0;
    // Search the mask data from most significant bit (MSB)
    // to least significant bit (LSB) for a set bit (1).
    if (_BitScanReverse64(&leading_zero, input_num))
        return (int)(63 - leading_zero);
    else
        return 64;
#else
    const unsigned long x1 = (unsigned long)(x >> 32), top, bottom;
    _BitScanReverse(&top, x1);
    _BitScanReverse(&bottom, (unsigned long)x);
    return x1 ? top + 32 : bottom;
#endif // defined(_M_X64) || defined(_M_ARM64) || defined (_M_IA64)
#else
    return __builtin_clzll(input_num);
#endif // _MSC_VER
}


static struct value128_t full_multiplication(const uint64_t value1,
                                             const uint64_t value2)
{
    struct value128_t answer;
#ifdef FAST_DOUBLE_PARSER_REGULAR_VISUAL_STUDIO
#ifdef _M_ARM64
    // ARM64 has native support for 64-bit multiplications, no need to emultate
    answer.high = __umulh(value1, value2);
    answer.low = value1 * value2;
#else
    answer.low = _umul128(value1, value2, &answer.high); // _umul128 not available on ARM64
#endif // _M_ARM64
#else // SIMDJSON_REGULAR_VISUAL_STUDIO
#ifdef __SIZEOF_INT128__ // this is what we have on most 32-bit systems
    const __uint128_t r = ((__uint128_t)value1) * value2;
    answer.low = (uint64_t)(r);
    answer.high = (uint64_t)(r >> 64);
#else
    // fallback
    answer.low = Emulate64x64to128(&answer.high,
                                   value1,
                                   value2);
#endif
#endif
    return answer;
}

/*
 * We need a backup on old systems.
 * credit:
 * https://stackoverflow.com/questions/28868367/getting-the-high-part-of-64-bit-integer-multiplication
 */
static uint64_t __CDict_maybe_unused Emulate64x64to128(uint64_t * const r_hi,
                                                        const uint64_t x,
                                                        const uint64_t y)
{
    const uint64_t x0 = (uint32_t)x, x1 = x >> 32;
    const uint64_t y0 = (uint32_t)y, y1 = y >> 32;
    const uint64_t p11 = x1 * y1, p01 = x0 * y1;
    const uint64_t p10 = x1 * y0, p00 = x0 * y0;

    /*
     * 64-bit product + two 32-bit values
     */
    const uint64_t middle = p10 + (p00 >> 32) + (uint32_t)p01;

    /*
     * 64-bit product + two 32-bit values
     */
    *r_hi = p11 + (middle >> 32) + (p01 >> 32);

    /*
     * Add LOW PART and lower half of MIDDLE PART
     */
    return (middle << 32) | (uint32_t)p00;
}

#endif // __HAVE_LIBCDICT__

typedef int prevent_ISO_C_warning;
        