

#include "../binary_c.h"
No_empty_translation_unit_warning;
#undef is_integer
#ifndef __HAVE_LIBCDICT__
#include "cdict.h"
#undef exit

/*
 * Copy one cdict in src to another in dest.
 *
 * dest should be a new, empty cdict
 *
 * Note that metadata is not copied.
 *
 * Returns the number of bytes copied, which is
 * zero if either src or dest is NULL.
 */
static cdict_size_t copy_key_union_string_data(struct cdict_t * cdict,
                                               struct cdict_entry_t * const src,
                                               struct cdict_entry_t * const dest);
static cdict_size_t copy_value_string_and_array_data(struct cdict_t * cdict,
                                                     struct cdict_entry_t * const src,
                                                     struct cdict_entry_t * const dest);

#define cprint(...) printf(__VA_ARGS__);
//#define cprint(...) /* do nothing */

cdict_size_t cdict_copy(struct cdict_t * const src,
                        struct cdict_t * const dest)
{
    cdict_size_t ret = 0;
    cprint("cdict copy %p to %p\n",
           (void*)src,
           (void*)dest);
    if(src == NULL ||
       dest == NULL)
    {
        /* nothing to do */
    }
    else
    {
        CDict_loop(src,entry)
        {
            /*
             * Copy the entry
             */
            struct cdict_entry_t * entry_copy =
                cdict_malloc_function(dest,
                                      sizeof(struct cdict_entry_t));
            memcpy(entry_copy,
                   entry,
                   sizeof(struct cdict_entry_t));
            ret += sizeof(struct cdict_entry_t);
            entry_copy->metadata = NULL;
            entry_copy->pre_output = NULL;
            cdict_push_tofree(dest,
                              entry_copy);

            /*
             * Copy the entry's format string and
             * cache string.
             */
            if(entry->key.format!=NULL)
            {
                entry_copy->key.format = strdup(entry->key.format);
                ret += strlen(entry_copy->key.format);
                cdict_push_tofree(dest,
                                  entry_copy->key.format);
            }

            if(entry->key.string!=NULL)
            {
                entry_copy->key.string = strdup(entry->key.string);
                ret += strlen(entry_copy->key.string);
                cdict_push_tofree(dest,
                                  entry_copy->key.string);
            }

            cprint("cdict_copy key string \"%s\"\n",entry_copy->key.string);

            /*
             * Copy the key's union contents if a string
             */
            ret += copy_key_union_string_data(dest,
                                              entry,
                                              entry_copy);

            /*
             * Copy the value
             */
            memcpy(&entry_copy->value,
                   &entry->value,
                   sizeof(struct cdict_value_t));
            ret += sizeof(struct cdict_value_t);

            if(entry->value.format != NULL)
            {
                entry_copy->value.format =
                    strdup(entry_copy->value.format);
                ret += strlen(entry_copy->value.format);
                cdict_push_tofree(dest,
                                  entry_copy->value.format);
            }

            if(entry->value.type == CDICT_DATA_TYPE_CDICT_ARRAY)
            {
                /*
                 * Copy array of cdicts
                 */
                printf("copy array of cdicts\n");
                cdict_set(dest,
                          entry_copy->key.key,
                          entry_copy->key.type,
                          entry_copy->value.value,
                          entry_copy->value.count,
                          CDICT_DATA_TYPE_CDICT_ARRAY,
                          NULL,
                          NULL);
                for(cdict_size_t i=0; i<entry_copy->value.count; i++)
                {
                    ret += cdict_copy(entry->value.value.cdict_array_data[i],
                                      entry_copy->value.value.cdict_array_data[i]);
                }
            }
            else if(entry->value.type == CDICT_DATA_TYPE_CDICT)
            {
                /*
                 * Nested cdict struct : make it in the dest
                 *                       and call this function recursively
                 */
                CDict_renew(entry_copy->value.value.cdict_pointer_data,
                            dest);

                cprint("new nested copy %p in %p\n",
                       (void*)entry_copy->value.value.cdict_pointer_data,
                       (void*)dest);

                /*
                 * Set entry in cdict
                 */
                cdict_set(dest,
                          entry_copy->key.key,
                          entry_copy->key.type,
                          entry_copy->value.value,
                          entry_copy->value.count,
                          CDICT_DATA_TYPE_CDICT,
                          NULL,
                          NULL);
                ret += cdict_copy(entry->value.value.cdict_pointer_data,
                                  entry_copy->value.value.cdict_pointer_data);
            }
            else
            {
                cprint("copy value union\n");
                /*
                 * Set entry in cdict
                 */
                cdict_set(dest,
                          entry_copy->key.key,
                          entry_copy->key.type,
                          entry_copy->value.value,
                          entry_copy->value.count,
                          entry_copy->value.type,
                          NULL,
                          NULL);

                /*
                 * Copy array/string data
                 */
                ret += copy_value_string_and_array_data(dest,
                                                        entry,
                                                        entry_copy);
            }
        }
    }
    return ret;
}



static cdict_size_t copy_key_union_string_data(struct cdict_t * cdict,
                                               struct cdict_entry_t * const src,
                                               struct cdict_entry_t * const dest)
{
    /*
     * Copy a key union from src to dest.
     *
     * Scalar types were already copied.
     */

    /*
     * If string type, we should duplicate
     * the string.
     */
    cdict_size_t ret = 0;
    if(src->key.type == CDICT_DATA_TYPE_STRING)
    {
        /* copy string */
        dest->key.key.string_data = strdup(src->key.key.string_data);
        ret += strlen(dest->key.key.string_data);
        cdict_push_tofree(cdict,
                          dest->key.key.string_data);
    }
    return ret;
}

static cdict_size_t copy_value_string_and_array_data(struct cdict_t * cdict,
                                                     struct cdict_entry_t * const src,
                                                     struct cdict_entry_t * const dest)
{
    /*
     * Copy a value union from src to dest.
     *
     * Scalar types are already copied by a
     * previous memcpy.
     */
    cdict_size_t ret = 0;

    /* now make copies of the data when required */
    if(src->value.type == CDICT_DATA_TYPE_STRING)
    {
        /* copy string */
        dest->value.value.string_data = strdup(src->value.value.string_data);
        ret += strlen(dest->value.value.string_data);
        cdict_push_tofree(cdict,
                          dest->value.value.string_data);
    }
    else if(__CDict_datatype_is_array(src->value.type))
    {
        /* copy array */
        if(src->value.count > 0)
        {
            cprint("copy type %d sizeof %zu (int %zu) count %zu\n",
                   src->value.type,
                   __CDict_sizeof_array_entry(src->value.type),
                   sizeof(int),
                   (size_t)src->value.count);
            const size_t nbytes =
                (size_t)__CDict_sizeof_array_entry(src->value.type) *
                src->value.count;

            /*
             * Allocate space for the new array
             */
            dest->value.value.void_pointer_data =
                cdict_malloc_function(cdict,
                                      nbytes);
            dest->value.count = src->value.count;

            /*
             * We should copy the values byte by byte
             */
            cprint("copy %zu bytes from %p to %p\n",
                   nbytes,
                   src->value.value.void_pointer_data,
                   dest->value.value.void_pointer_data);
            memcpy(dest->value.value.void_pointer_data,
                   src->value.value.void_pointer_data,
                   nbytes);
            ret += nbytes;
            cprint("done copy\n");
            cdict_push_tofree(cdict,
                              dest->value.value.void_pointer_data);
        }
        else
        {
            dest->value.value.void_pointer_data = NULL;
        }
    }
    return ret;
}

#endif // __HAVE_LIBCDICT__

typedef int prevent_ISO_C_warning;
        