#include "../binary_c.h"
        #ifndef __HAVE_LIBCDICT__
        
#pragma once
#ifndef CDICT_MACROS_H
#define CDICT_MACROS_H
#include <float.h>
#include "cdict_code_options.h"
#include "cdict_test_macros.h"
#include "cdict_types.h"
#include "cdict_array_setting.h"

/*
 * Macros used in cdict library
 *
 * Function macros which are to be accessible to
 * the outside world are of the form:
 *     CDict_...(args)
 * and should be defined in CDict_API.h, not here!
 *
 * Function macros which are internal are of the form
 *     __CDict_...(args)
 *
 * Constants are upper case macros of the form
 *     CDICT_...
 */

/*
 * Hidden functions
 */
#define CDict_hidden_function __attribute__ ((visibility("hidden")))

/*
 * API function visibility
 */
#define CDict_API_function __attribute__ ((visibility("hidden")))

/*
 * Functions or variables that may be unused (hence
 * don't let the compiler warn about them).
 */
#define CDict_maybe_unused __attribute__ ((unused))


/*
 * Cast a function pointer to void to avoid
 * errors, e.g. with printf("%p",...)
 */
#define __CDict_cast_function_pointer(F) (((union {int (*from)(void); void *to;} *)&(F))->to)

/*
 * Make unique variable names in external .h files
 */
#define __CDict_concatenate___(x,y) x##y
#define __CDict_concatenate__(x,y) __CDict_concatenate___(x,y)
#define __CDict_uniquename__ __CDict_concatenate__(uniquevar_, __LINE__)

#define __CDict_concatenate_a___(x,y,z) x##y##z
#define __CDict_concatenate_a__(x,y,z) __CDict_concatenate_a___(x,y,z)
#define __CDict_uniquename_a__(A) __CDict_concatenate_a__(uniquevar_, __LINE__,A)

/*
 * Macro to concatenate three arguments
 */
#undef Concat3
#define Concat3(A,B,C) A##B##C

/*
 * Protect macro
 */
#define __CDict_protect(...) __VA_ARGS__

/*
 * Default floating-point comparison tolerances
 */
#define CDICT_ABSOLUTE_TOLERANCE_DEFAULT (2.0*DBL_EPSILON)
#define CDICT_RELATIVE_TOLERANCE_DEFAULT (2.0*DBL_EPSILON)

/*
 * Default number of bytes to which floats and
 * doubles are truncated.
 */
#define CDICT_HASHV_LONG_DOUBLE_NBYTES_DEFAULT 4
#define CDICT_HASHV_DOUBLE_NBYTES_DEFAULT 4
#define CDICT_HASHV_FLOAT_NBYTES_DEFAULT 3

/*
 * A hash handle for uthash to use
 */
#define CDICT_HANDLE rc_hh

/*
 * Choose default hash function
 */
#define __CDict_hash_function(hash,x,y,z)       \
    CDICT_UTHASH_JEN((hash),                    \
                     (x),                       \
                     (y),                       \
                     (z))

/*
 * Variable type groups
 */
enum {
    CDICT_GROUP_SCALAR,
    CDICT_GROUP_POINTER,
    CDICT_GROUP_ARRAY,
    CDICT_GROUP_STRING,
    CDICT_GROUP_CDICT,
    CDICT_GROUP_OTHER
};

/*
 * Numeric type groups
 */
enum {
    CDICT_NUMERIC_NOT,
    CDICT_NUMERIC_INT,
    CDICT_NUMERIC_FLOAT,
};

/*
 * Default type when we "dereference" scalars
 */
#define __NO_DEREF char

/*
 * Test pointers
 */
#define __TEST_POINTER ((void*)0x1)
#define __TEST_POINTER1 ((void*)0x1)
#define __TEST_POINTER2 ((void*)0x2)
#define __IGNORE_POINTER ((void*)0x3)

/*
 * Data type X macros
 *
 * The terms in each X macro are:
 *
 * 1) TYPE: libcdict type identifier : e.g. CHAR, FLOAT
 * 2) MEMBER: the cdict_entry -> value/key -> union member (not including trailing _data)
 * 2) CTYPE: corresponding C type, e.g. double, void *, that can be used by sizeof
 * 3) DEFCTYPE: corresponding C type used to declare a variable of this type
 * 4) CARRAY: statements that should come after declaring a variable of this type, e.g. [] for arrays
 * 5) DESCRIPTOR: a string to describe this type to humans
 * 6) FORMAT: a format specifier for the type or, for arrays, the type of one element
 * 7) DEREF_FORMAT: a format specifier for the dereferenced type pointer, or NULL if not applicable or an array
 * 8) DEREF_CTYPE: C type of the dereferenced pointer or single array element, or NO_DEREF
 * 9) GROUP: group specifier: SCALAR, POINTER, ARRAY, CDICT, OTHER
 * 10) ARRAY_TO_SCALAR_MAPPER: array  -> scalar cdict type map, or NONE
 * 11) SCALAR_TO_ARRAY_MAPPER: scalar -> array  cdict type map, or NONE
 * 12) NUMERIC: numeric type: NOT (not numeric), INT (integer of some sort) or FLOAT (float of some sort)
 * 13) DEMO: demonstration data: this is data used to test the type (see cdict_tests.c)
 */
#include "cdict_data_types.h"

/*
 * Enumerate list of key and value data types
 */
#undef X
#define X(TYPE,                                 \
          MEMBER,                               \
          CTYPE,                                \
          DEFCTYPE,                             \
          CARRAY,                               \
          DESCRIPTOR,                           \
          FORMAT,                               \
          DEREF_FORMAT,                         \
          DEREF_CTYPE,                          \
          GROUP,                                \
          ARRAY_TO_SCALAR_MAP,                  \
          SCALAR_TO_ARRAY_MAP,                  \
          NUMERIC,                              \
          DEMO                                  \
    ) CDICT_DATA_TYPE_##TYPE,
enum {
    __CDICT_DATA_TYPES__
    CDICT_DATA_NUMBER_OF_TYPES
};
#undef X

/*
 * special definitions
 */
#define CDICT_DATA_LAST_KEY_TYPE (CDICT_DATA_TYPE_CHAR_POINTER)
#define CDICT_DATA_TYPE_NONE (CDICT_DATA_TYPE_UNKNOWN)
#define CDICT_DATA_TYPE_NONE_ARRAY (CDICT_DATA_TYPE_UNKNOWN)

/*
 * Check if type is key type (TRUE) or either
 * key or value (FALSE)
 */
#define __CDict_type_is_key_type(N)             \
    ((N) <= CDICT_DATA_LAST_KEY_TYPE)

#define __CDict_key_descriptor(N)                       \
    (__CDict_descriptor((const CDict_data_type)(N)))

#define __CDict_value_descriptor(N)                     \
    (__CDict_descriptor((const CDict_data_type)(N)))



/*
 * Data type tests.
 *
 * These take a cdict variable type, N, and test
 * it against various groups of types.
 */

/* float or int */
#define __CDict_datatype_is_float_or_int(N)             \
    (                                                   \
        __CDict_numeric(N) == CDICT_NUMERIC_FLOAT ||    \
        __CDict_numeric(N) == CDICT_NUMERIC_INT         \
        )

/* scalars */
#define __CDict_datatype_is_scalar(N)           \
    (__CDict_group(N) == CDICT_GROUP_SCALAR)


#define __CDict_datatype_is_numeric(N)          \
    (                                           \
        __CDict_datatype_is_scalar(N) &&        \
        __CDict_datatype_is_float_or_int(N)     \
        )

#define __CDict_datatype_is_int(N)              \
    (                                           \
        __CDict_datatype_is_scalar(N) &&        \
        __CDict_numeric(N) == CDICT_NUMERIC_INT \
        )

#define __CDict_datatype_is_floating_point(N)       \
    (                                               \
        __CDict_datatype_is_scalar(N) &&            \
        __CDict_numeric(N) == CDICT_NUMERIC_FLOAT   \
        )

#define __CDict_datatype_is_numeric_or_char(N)  \
    (                                           \
        __CDict_datatype_is_numeric(N) ||       \
        (N) == CDICT_DATA_TYPE_CHAR             \
        )

#define __CDict_datatype_is_numeric_or_char_or_boolean(N)   \
    (                                                       \
        __CDict_datatype_is_scalar(N) &&                    \
        (                                                   \
            __CDict_datatype_is_float_or_int(N) ||          \
            (N) == CDICT_DATA_TYPE_BOOLEAN ||               \
            (N) == CDICT_DATA_TYPE_CHAR                     \
            )                                               \
        )

/* arrays */
#define __CDict_datatype_is_array(N)            \
    (__CDict_group(N) == CDICT_GROUP_ARRAY)

/* const macro to determine array type */
#define __CDict_consttest_datatype_is_array(N)  \
    ((N)>=CDICT_DATA_TYPE_DOUBLE_ARRAY &&       \
     (N)<=CDICT_DATA_TYPE_LONG_DOUBLE_ARRAY)

#define __CDict_datatype_is_numeric_array(N)    \
    (                                           \
        __CDict_datatype_is_array(N) &&         \
        __CDict_datatype_is_float_or_int(N)     \
        )

/* pointers */
#define __CDict_datatype_is_pointer(N)          \
    (__CDict_group(N) == CDICT_GROUP_POINTER)

#define __CDict_datatype_is_numerical_pointer(N)    \
    (                                               \
        __CDict_datatype_is_pointer(N) &&           \
        __CDict_datatype_is_float_or_int(N)         \
        )

/*
 * JSON output: keys must be strings
 * so are always quoted with this macro.
 */
#define __CDict_key_quotestring(N) "\""

/*
 * JSON values may be quoted, or may not,
 * choose with this macro.
 */
#define __CDict_value_quotestring(N)            \
    ((                                          \
        (N) == CDICT_DATA_TYPE_BOOLEAN ||       \
        (N) == CDICT_DATA_TYPE_BOOLEAN_ARRAY || \
        __CDict_datatype_is_numeric(N) ||       \
        __CDict_datatype_is_numeric_array(N)    \
        ) ? "" : "\""                           \
        )

/*
 * Create a unique name using the line number, useful
 * for making temporary variables in loops in C99+
 */
#define __CDict_line_name( prefix )             \
    __CDict_join( prefix,                       \
                  __LINE__ )
#define __CDict_join( symbol1,                  \
                      symbol2 )                 \
    __CDict_do_join( symbol1,                   \
                     symbol2 )
#define __CDict_do_join( symbol1,               \
                         symbol2 )              \
    symbol1##symbol2

/*
 * Options for JSON output
 */
enum {
    CDICT_JSON_INDENT = TRUE,
    CDICT_JSON_NO_INDENT = FALSE,
    CDICT_JSON_NEWLINES = TRUE,
    CDICT_JSON_NO_NEWLINES = FALSE,
    CDICT_JSON_SORT = TRUE,
    CDICT_JSON_NO_SORT = FALSE,
};

/*
 * JSON output return values
 */
enum {
    CDICT_TO_JSON_SUCCESS = -1,
    CDICT_TO_JSON_ALLOC_ERROR = -2,
    CDICT_TO_JSON_ITER_NULL = -3,
    CDICT_TO_JSON_SKIP = -4,
};

/*
 * Macros to convert Booleans to strings
 */
#define __CDict_Yesno(B) ((B)==TRUE ? "Yes" : "No")
#define __CDict_YESno(B) ((B)==TRUE ? "YES" : "NO")
#define __CDict_TrueFalse(B) ((B)==TRUE ? "True" : "False")
#define __CDict_truefalse(B) ((B)==TRUE ? "true" : "false")
#define __CDict_TRUEFALSE(B) ((B)==TRUE ? "TRUE" : "FALSE")

#define __CDict_Max(...)                        \
    __CDict_Max_implementation(                 \
        CDict_max,                              \
        __COUNTER__,                            \
        __VA_ARGS__)
#define __CDict_Max_implementation(LABEL,                       \
                                   LINE,                        \
                                   A,                           \
                                   B)                           \
    __extension__                                               \
    ({                                                          \
        const __CDict_autotype(A) Concat3(LABEL,LINE,_a) = (A); \
        const __CDict_autotype(B) Concat3(LABEL,LINE,_b) = (B); \
        __CDict_More_than(Concat3(LABEL,LINE,_a),               \
                          Concat3(LABEL,LINE,_b))               \
            ?                                                   \
            Concat3(LABEL,LINE,_a)                              \
            :                                                   \
            Concat3(LABEL,LINE,_b);                             \
    })

#define __CDict_Min(...)                        \
    __CDict_Min_implementation(                 \
        CDict_min,                              \
        __COUNTER__,                            \
        __VA_ARGS__)
#define __CDict_Min_implementation(LABEL,                       \
                                   LINE,                        \
                                   A,                           \
                                   B)                           \
    __extension__                                               \
    ({                                                          \
        const __CDict_autotype(A) Concat3(LABEL,LINE,_a) = (A); \
        const __CDict_autotype(B) Concat3(LABEL,LINE,_b) = (B); \
        __CDict_Less_than(Concat3(LABEL,LINE,_a),               \
                          Concat3(LABEL,LINE,_b))               \
            ?                                                   \
            Concat3(LABEL,LINE,_a)                              \
            :                                                   \
            Concat3(LABEL,LINE,_b);                             \
    })

#define __CDict_Less_than(A,B) ((A)<(B))
#define __CDict_More_than(A,B) ((A)>(B))

/*
 * How to (re)allocate and free memory
 */
#define __CDict_malloc(CDICT,A) cdict_malloc_function((CDICT),(A))
#define __CDict_calloc(CDICT,A,B) cdict_calloc_function((CDICT),(A),(B))
#define __CDict_realloc(CDICT,A,B) cdict_realloc_function((CDICT),(A),(B))
#define __CDict_Safe_free(CDICT,PTR)                            \
    __extension__                                               \
    ({cdict_free_alloc_function((CDICT),(PTR));(PTR)=NULL;})

/*
 * Make unions : note we explicitly memset them,
 * while setting them to {0} probably also zeros them
 *
 * cf. https://en.cppreference.com/w/cpp/language/zero_initialization
 */
#define __CDict_new_key_union(NAME)                     \
    union cdict_key_union NAME = {0};                   \
    memset(&(NAME),0,sizeof(union cdict_key_union));
#define __CDict_new_value_union(NAME)                   \
    union cdict_value_union NAME = {0};                 \
    memset(&(NAME),0,sizeof(union cdict_value_union));


/*
 * Explicitly state no metadata
 */
#define CDICT_NO_METADATA NULL,NULL

/*
 * Elvis operator
 */
#ifdef __GNUC__
#define __CDict_Elvis(A,B) ((A) ?: (B))
#else
#define __CDict_Elvis(...)                      \
    __CDict_Elvis_implementation(               \
        CDict_Elvis,                            \
        __COUNTER__,                            \
        __VA_ARGS__)
#define __CDict_Elvis_implementation(LABEL,             \
                                     LINE,              \
                                     A,                 \
                                     B)                 \
    __extension__                                       \
    ({                                                  \
        __typeof__(A) * Concat3(__x,LABEL,LINE) = (A);  \
        Concat3(__x,LABEL,LINE)                         \
            ?                                           \
            Concat3(__x,LABEL,LINE)                     \
            :                                           \
            (B);                                        \
    })
#endif//__GNUC__

#define __CDICT_CALLERS__ \
    X(       NONE)        \
    X(      FREE1)        \
    X(      FREE2)        \
    X(    ANYTYPE)        \
    X(     APPEND)        \
    X(        SET)        \
    X(       NEST)        \
    X( UNIT_TESTS)
#undef X
#define X(CALLER) CDICT_CALLER_##CALLER,
enum {
    __CDICT_CALLERS__
};
#undef X

/*
 * Nest actions
 */
#define __CDICT_NEST_ACTIONS__ \
    X( APPEND, "append")       \
    X(    SET,    "set")
#undef X
#define X(ACTION,STRING) CDICT_NEST_ACTION_##ACTION,
enum { __CDICT_NEST_ACTIONS__ };
#undef X
#define X(ACTION,STRING) STRING,
static CDict_maybe_unused char * __CDict_action_strings[] =
{
    __CDICT_NEST_ACTIONS__ "unknown"
};
#define __CDict_action_string(N) __CDict_action_strings[N]
#undef X

/*
 * Errors
 */
#define __CDICT_ERRORS__                 \
    X(                         NO_ERROR) \
    X(            TO_STRING_TYPE_FAILED) \
    X(            TO_DOUBLE_TYPE_FAILED) \
    X(                  KEY_STRING_NULL) \
    X(            DETECTED_UNKNOWN_TYPE) \
    X(                    OUT_OF_MEMORY) \
    X(                 CANNOT_OPEN_FILE) \
    X(                       FREAD_FAIL) \
    X(                      FCLOSE_FAIL) \
    X(                       JSMN_ERROR) \
    X(                      FWRITE_FAIL) \
    X(                    STRDUP_FAILED) \
    X( JSMN_CHILD_TYPE_DETECTION_FAILED) \
    X(      JSMN_CHILD_TYPE_UNSUPPORTED) \
    X(                         BAD_CHAR) \
    X(                    ASSERT_FAILED) \
    X(                  ASPRINTF_FAILED) \
    X(                     FPUTS_FAILED) \
    X(            NESTED_PATH_NOT_FOUND) \
    X(                       CDICT_NULL) \
    X(              NEST_FOUND_NO_CDICT)
#undef X
#define X(ERR) CDICT_ERROR_##ERR,
enum {  __CDICT_ERRORS__ };
#undef X

/*
 * Macro string formation
 */
#define __CDict_Stringify(item) "" #item
#define __CDict_Stringify_macro(item) __CDict_Stringify(item)

#ifdef __CDICT_HAVE_UINT128
/*
 * Use the ryu functions for converting floats and
 * doubles to strings. Note: no long doubles, assume
 * those are like doubles.
 *
 * Note: ryu requires uint128_t
 */
#define CDICT_USE_RYU
#define CDICT_RYU_STRING_LENGTH_FLOAT 16
#define CDICT_RYU_STRING_LENGTH_DOUBLE 25
#endif

/*
 * memory allocation modes
 */
#define __CDICT_ALLOC_MODES__ \
    X(   MALLOC)              \
    X(   CALLOC)              \
    X(  REALLOC)              \
    X( ASPRINTF)              \
    X(      ALL)
#undef X
#define X(MODE) CDICT_MODE_##MODE,
enum {
    __CDICT_ALLOC_MODES__
};
#undef X

/*
 * ANSI colours used by libcdict
 */
#define CDICT_GREEN "\x1B[0;32m"
#define CDICT_YELLOW "\x1B[0;33m"
#define CDICT_BLUE "\x1B[0;34m"
#define CDICT_MAGENTA "\x1B[0;35m"
#define CDICT_CYAN "\x1B[0;36m"
#define CDICT_COLOUR_RESET "\x1B[0;0m"

/*
 * Shortcuts to outputting with whitespace or not
 */
#define CDICT_JSON_NO_WHITESPACE                \
    ",",                                        \
        ":",                                    \
        CDICT_JSON_NO_INDENT,                   \
        CDICT_JSON_NO_NEWLINES,                 \
        CDICT_JSON_NO_SORT

#define CDICT_JSON_WHITESPACE                   \
    ",",                                        \
        " : ",                                  \
        CDICT_JSON_INDENT,                      \
        CDICT_JSON_NEWLINES,                    \
        CDICT_JSON_SORT

#define CDICT_JSON_WHITESPACE_NO_SORT           \
    ",",                                        \
        " : ",                                  \
        CDICT_JSON_INDENT,                      \
        CDICT_JSON_NEWLINES,                    \
        CDICT_JSON_NO_SORT

/*
 * Either use the C library's strtod or
 * use our fast_strtod
 */
//#define CDict_strtod(STRING,ENDPTR) strtod((STRING),(ENDPTR))
#define CDict_strtod(STRING,ENDPTR) cdict_fast_strtod((STRING),(ENDPTR))

/*
 * Likely/Unlikely wrappers
 */
#ifndef likely
#define likely(x)      __builtin_expect(!!(x), 1)
#endif
#ifndef unlikely
#define unlikely(x)    __builtin_expect(!!(x), 0)
#endif

/*
 * Tests on a C variable, x.
 *
 * For similar examples, see:
 * https://stackoverflow.com/questions/6280055/how-do-i-check-if-a-variable-is-of-a-certain-type-compare-two-types-in-c
 *
 * Note that our Boolean type is the same as unsigned int, so will
 * give that as a first response. We include a nested _Generic
 * to handle the case where Boolean isn't the same as one of the
 * other types (you never know...).
 *
 * In the following macros we return a string (__CDict_typename),
 * union member (__CDict_unionname), union member setting function
 * (__CDict_key_union_setter and __CDict_value_union_setter) or
 * libcdict data type macro (__CDict_keytype_num and
 * __CDict_valuetype_num).
 */
#define __CDict_typename(x)                                     \
    _Generic(                                                   \
        (x),                                                    \
        float: "float",                                         \
        double: "double",                                       \
        long double: "long double",                             \
        int: "int",                                             \
        short int: "short int",                                 \
        long int: "long int",                                   \
        long long int: "long long int",                         \
        char: "char",                                           \
        unsigned int: "unsigned int",                           \
        unsigned short int: "unsigned short int",               \
        unsigned long int: "unsigned long int",                 \
        unsigned long long int: "unsigned long long int",       \
        float * : "float*",                                     \
        double * :  "double*",                                  \
        long double * :  "long double*",                        \
        double const * : "double array",                        \
        int * : "int*",                                         \
        short int * : "short int*",                             \
        long int * : "long int*",                               \
        long long int * : "long long int*",                     \
        char * : "char*",                                       \
        unsigned int * : "unsigned int*",                       \
        unsigned short int * : "unsigned short int*",           \
        unsigned long int * : "unsigned long int*",             \
        unsigned long long int * : "unsigned long long int*",   \
        struct cdict_t * : "cdict_t*",                          \
        struct cdict_t ** : "cdict_t**",                        \
        default: _Generic((x),                                  \
                          Boolean: "Boolean",                   \
                          Boolean* : "Boolean *",               \
                          default: "unknown"                    \
            )                                                   \
        )

#define __CDict_unionname(x)                                        \
    _Generic(                                                       \
        (x),                                                        \
        float: float_data,                                          \
        double: double_data,                                        \
        long double: long_double_data,                              \
        int: int_data,                                              \
        short int: short_int_data,                                  \
        long int: long_int_data,                                    \
        long long int: long_long_int_data,                          \
        char: char_data,                                            \
        unsigned int: unsigned_int_data,                            \
        unsigned short int: unsigned_short_int,                     \
        unsigned long int: unsigned_long_int,                       \
        unsigned long long int: unsigned_long_long_int,             \
        float * : float_pointer_data,                               \
        double * : double_pointer_data,                             \
        long double * : long_double_pointer_data,                   \
        int * : int_pointer_data,                                   \
        short int * : short_int_pointer_data,                       \
        long int * : long_int_pointer_data,                         \
        long int * : long_long_int_pointer_data,                    \
        char * : string_data,                                       \
        unsigned int * : unsigned_int_pointer_data,                 \
        unsigned short int * : unsigned_short_int_pointer_data,     \
        unsigned long int * : unsigned_long_int_pointer_data,       \
        unsigned long long int * : unsigned_long_int_pointer_data,  \
        struct cdict_t  * : cdict_pointer_data,                     \
        struct cdict_t  ** : cdict_array_data,                      \
        default: _Generic((x),                                      \
                          Boolean: Boolean_data,                    \
                          Boolean* : Boolean_pointer_data,          \
                          default: unknown                          \
            )                                                       \
        )

#define __CDict_key_union_setter(x)                                     \
    _Generic(                                                           \
        (x),                                                            \
        float: cdict_union_from_float,                                  \
        double: cdict_union_from_double,                                \
        long double: cdict_union_from_long_double,                      \
        int: cdict_union_from_int,                                      \
        short int: cdict_union_from_short_int,                          \
        long int: cdict_union_from_long_int,                            \
        long long int: cdict_union_from_long_long_int,                  \
        char: cdict_union_from_char,                                    \
        unsigned int: cdict_union_from_unsigned_int,                    \
        unsigned short int: cdict_union_from_unsigned_short_int,        \
        unsigned long int: cdict_union_from_unsigned_long_int,          \
        unsigned long long int: cdict_union_from_unsigned_long_long_int, \
        float * : cdict_union_from_float,                               \
        double * : cdict_union_from_double_pointer,                     \
        long double * : cdict_union_from_long_double_pointer,           \
        int * : cdict_union_from_int_pointer,                           \
        short int * : cdict_union_from_short_int_pointer,               \
        long int * : cdict_union_from_long_int_pointer,                 \
        long long int * : cdict_union_from_long_long_int_pointer,       \
        char * : cdict_union_from_string,                               \
        char* * : cdict_union_from_string_array,                        \
        unsigned int * : cdict_union_from_unsigned_int_pointer,         \
        unsigned short int * : cdict_union_from_unsigned_short_int_pointer, \
        unsigned long int * : cdict_union_from_unsigned_long_int_pointer, \
        unsigned long long int * : cdict_union_from_unsigned_long_long_int_pointer, \
        void* : cdict_union_from_void_pointer,                          \
        default: _Generic((x),                                          \
                          Boolean: cdict_union_from_Boolean,            \
                          Boolean* : cdict_union_from_Boolean_pointer,  \
                          default: cdict_union_from_unknown             \
            )                                                           \
        )

#define __CDict_keytype_num(x)                                          \
    __builtin_choose_expr(                                              \
        CDict_is_array(x),                                              \
        _Generic(                                                       \
            (x),                                                        \
            char ** : CDICT_DATA_TYPE_STRING_ARRAY,                     \
            char * : CDICT_DATA_TYPE_STRING,                            \
            void ** : CDICT_DATA_TYPE_VOID_POINTER_ARRAY,               \
            double * : CDICT_DATA_TYPE_DOUBLE_ARRAY,                    \
            long double * : CDICT_DATA_TYPE_LONG_DOUBLE_ARRAY,          \
            float * : CDICT_DATA_TYPE_FLOAT_ARRAY,                      \
            int * : CDICT_DATA_TYPE_INT_ARRAY,                          \
            short int * : CDICT_DATA_TYPE_SHORT_INT_ARRAY,              \
            long int * : CDICT_DATA_TYPE_LONG_INT_ARRAY,                \
            long long int * : CDICT_DATA_TYPE_LONG_LONG_INT_ARRAY,      \
            unsigned int * : CDICT_DATA_TYPE_UNSIGNED_INT_ARRAY,        \
            unsigned short int * : CDICT_DATA_TYPE_UNSIGNED_SHORT_INT_ARRAY, \
            unsigned long int * : CDICT_DATA_TYPE_UNSIGNED_LONG_INT_ARRAY, \
            unsigned long long int * : CDICT_DATA_TYPE_UNSIGNED_LONG_LONG_INT_ARRAY, \
            Boolean * : CDICT_DATA_TYPE_BOOLEAN_ARRAY,                  \
            default: CDICT_DATA_TYPE_UNKNOWN)                           \
        ,                                                               \
        _Generic(                                                       \
            (x),                                                        \
            char * : CDICT_DATA_TYPE_STRING,                            \
            float: CDICT_DATA_TYPE_FLOAT,                               \
            double: CDICT_DATA_TYPE_DOUBLE,                             \
            long double: CDICT_DATA_TYPE_LONG_DOUBLE,                   \
            int: CDICT_DATA_TYPE_INT,                                   \
            Boolean: CDICT_DATA_TYPE_BOOLEAN,                           \
            short int: CDICT_DATA_TYPE_SHORT_INT,                       \
            long int: CDICT_DATA_TYPE_LONG_INT,                         \
            long long int: CDICT_DATA_TYPE_LONG_LONG_INT,               \
            char: CDICT_DATA_TYPE_CHAR,                                 \
            unsigned int: CDICT_DATA_TYPE_UNSIGNED_INT,                 \
            unsigned short int: CDICT_DATA_TYPE_UNSIGNED_SHORT_INT,     \
            unsigned long int: CDICT_DATA_TYPE_UNSIGNED_LONG_INT,       \
            unsigned long long int: CDICT_DATA_TYPE_UNSIGNED_LONG_LONG_INT, \
            double * : CDICT_DATA_TYPE_DOUBLE_POINTER,                  \
            long double * : CDICT_DATA_TYPE_LONG_DOUBLE_POINTER,        \
            float * : CDICT_DATA_TYPE_FLOAT_POINTER,                    \
            void * : CDICT_DATA_TYPE_VOID_POINTER,                      \
            int * : CDICT_DATA_TYPE_INT_POINTER,                        \
            short int * : CDICT_DATA_TYPE_SHORT_INT_POINTER,            \
            long int * : CDICT_DATA_TYPE_LONG_INT_POINTER,              \
            long long int * : CDICT_DATA_TYPE_LONG_LONG_INT_POINTER,    \
            unsigned int * : CDICT_DATA_TYPE_UNSIGNED_INT_POINTER,      \
            unsigned short int * : CDICT_DATA_TYPE_UNSIGNED_SHORT_INT_POINTER, \
            unsigned long int * : CDICT_DATA_TYPE_UNSIGNED_LONG_INT_POINTER, \
            unsigned long long int * : CDICT_DATA_TYPE_UNSIGNED_LONG_LONG_INT_POINTER, \
            Boolean * : CDICT_DATA_TYPE_BOOLEAN_POINTER,                \
            struct cdict_t  * : CDICT_DATA_TYPE_CDICT,                  \
            default: CDICT_DATA_TYPE_UNKNOWN)                           \
        )

#define __CDict_valuetype_num(x)                                        \
    __builtin_choose_expr(                                              \
        CDict_is_array(x),                                              \
        _Generic(                                                       \
            (x),                                                        \
            char ** : CDICT_DATA_TYPE_STRING_ARRAY,                     \
            char * : CDICT_DATA_TYPE_STRING,                            \
            void ** : CDICT_DATA_TYPE_VOID_POINTER_ARRAY,               \
            double * : CDICT_DATA_TYPE_DOUBLE_ARRAY,                    \
            long double * : CDICT_DATA_TYPE_LONG_DOUBLE_ARRAY,          \
            float * : CDICT_DATA_TYPE_FLOAT_ARRAY,                      \
            int * : CDICT_DATA_TYPE_INT_ARRAY,                          \
            short int * : CDICT_DATA_TYPE_SHORT_INT_ARRAY,              \
            long int * : CDICT_DATA_TYPE_LONG_INT_ARRAY,                \
            long long int * : CDICT_DATA_TYPE_LONG_LONG_INT_ARRAY,      \
            unsigned int * : CDICT_DATA_TYPE_UNSIGNED_INT_ARRAY,        \
            unsigned short int * : CDICT_DATA_TYPE_UNSIGNED_SHORT_INT_ARRAY, \
            unsigned long int * : CDICT_DATA_TYPE_UNSIGNED_LONG_INT_ARRAY, \
            unsigned long long int * : CDICT_DATA_TYPE_UNSIGNED_LONG_LONG_INT_ARRAY, \
            Boolean * : CDICT_DATA_TYPE_BOOLEAN_ARRAY,                  \
            struct cdict_t ** : CDICT_DATA_TYPE_CDICT_ARRAY,            \
            default: CDICT_DATA_TYPE_UNKNOWN),                          \
        _Generic(                                                       \
            (x),                                                        \
            char * : CDICT_DATA_TYPE_STRING,                            \
            char ** : CDICT_DATA_TYPE_STRING_ARRAY,                     \
            float: CDICT_DATA_TYPE_FLOAT,                               \
            double: CDICT_DATA_TYPE_DOUBLE,                             \
            long double: CDICT_DATA_TYPE_LONG_DOUBLE,                   \
            int: CDICT_DATA_TYPE_INT,                                   \
            Boolean: CDICT_DATA_TYPE_BOOLEAN,                           \
            short int: CDICT_DATA_TYPE_SHORT_INT,                       \
            long int: CDICT_DATA_TYPE_LONG_INT,                         \
            long long int: CDICT_DATA_TYPE_LONG_LONG_INT,               \
            char: CDICT_DATA_TYPE_CHAR,                                 \
            unsigned int: CDICT_DATA_TYPE_UNSIGNED_INT,                 \
            unsigned short int: CDICT_DATA_TYPE_UNSIGNED_SHORT_INT,     \
            unsigned long int: CDICT_DATA_TYPE_UNSIGNED_LONG_INT,       \
            unsigned long long int: CDICT_DATA_TYPE_UNSIGNED_LONG_LONG_INT, \
            double * : CDICT_DATA_TYPE_DOUBLE_POINTER,                  \
            long double * : CDICT_DATA_TYPE_LONG_DOUBLE_POINTER,        \
            float * : CDICT_DATA_TYPE_FLOAT_POINTER,                    \
            void * : CDICT_DATA_TYPE_VOID_POINTER,                      \
            void ** : CDICT_DATA_TYPE_VOID_POINTER_ARRAY,               \
            int * : CDICT_DATA_TYPE_INT_POINTER,                        \
            short int * : CDICT_DATA_TYPE_SHORT_INT_POINTER,            \
            long int * : CDICT_DATA_TYPE_LONG_INT_POINTER,              \
            long long int * : CDICT_DATA_TYPE_LONG_LONG_INT_POINTER,    \
            unsigned int * : CDICT_DATA_TYPE_UNSIGNED_INT_POINTER,      \
            unsigned short int * : CDICT_DATA_TYPE_UNSIGNED_SHORT_INT_POINTER, \
            unsigned long int * : CDICT_DATA_TYPE_UNSIGNED_LONG_INT_POINTER, \
            unsigned long long int * : CDICT_DATA_TYPE_UNSIGNED_LONG_LONG_INT_POINTER, \
            Boolean * : CDICT_DATA_TYPE_BOOLEAN_POINTER,                \
            struct cdict_t  * : CDICT_DATA_TYPE_CDICT,                  \
             struct cdict_t ** : CDICT_DATA_TYPE_CDICT_ARRAY,            \
            default: CDICT_DATA_TYPE_UNKNOWN)                           \
        )


#undef X
#endif // CDICT_MACROS_H

#endif // __HAVE_LIBCDICT__

typedef int prevent_ISO_C_warning;
       