

#include "../binary_c.h"
No_empty_translation_unit_warning;
#undef is_integer
#ifndef __HAVE_LIBCDICT__
#include "cdict.h"
#undef exit


#include <stdio.h>
#ifdef CDICT_USE_RYU
#include "ryu/ryu.h"
#endif// CDICT_USE_RYU


/*
 * Function to convert cdict key contents to a string
 *
 * Returns the number of characters in the new string,
 * as returned from asprintf, not including the trailing
 * null character.
 */

int cdict_union_to_string(struct cdict_t * cdict,
                          const union cdict_key_union * const key_union,
                          const CDict_key_type key_type,
                          const char * const format,
                          char ** target_p)
{
    int ret = 0;

    /*
     * Use default format if not given
     */
    const char * const use_format =
        format == NULL ? __CDict_generic_format_string(key_type) : format;

    ret = cdict_make_string_from_var(cdict,
                                     target_p,
                                     use_format,
                                     key_union,
                                     key_type,
                                     0);

    return ret;

#ifdef DEPRECATED

    if(key_type == CDICT_DATA_TYPE_STRING ||
       key_type == CDICT_DATA_TYPE_CHAR_POINTER)
    {
        ret = cdict_asprintf(cdict,
                             target_p,
                             use_format,
                             key_union->string_data);
    }
    else if(key_type == CDICT_DATA_TYPE_DOUBLE)
    {
#ifdef CDICT_USE_RYU
        /*
         * use ryu's double to string conversion
         * N.B. format is ignored
         */
        ret = cdict_ryu_d2s_asprintf(cdict,
                                     target_p,
                                     key_union->double_data);
#else
        /* use BSD's asprintf */
        ret = cdict_asprintf(cdict,
                             target_p,
                             use_format,
                             key_union->double_data);
#endif // CDICT_USE_RYU
    }
    else if(key_type == CDICT_DATA_TYPE_INT)
    {
        ret = cdict_asprintf(cdict,target_p,
                             use_format,
                             key_union->int_data);
    }
    else if(key_type == CDICT_DATA_TYPE_UNSIGNED_INT)
    {
        ret = cdict_asprintf(cdict,target_p,
                             use_format,
                             key_union->unsigned_int_data);
    }
    else if(key_type == CDICT_DATA_TYPE_SHORT_INT)
    {
        ret = cdict_asprintf(cdict,target_p,
                             use_format,
                             key_union->short_int_data);
    }
    else if(key_type == CDICT_DATA_TYPE_LONG_INT)
    {
        ret = cdict_asprintf(cdict,target_p,
                             use_format,
                             key_union->long_int_data);
    }
    else if(key_type == CDICT_DATA_TYPE_UNSIGNED_SHORT_INT)
    {
        ret = cdict_asprintf(cdict,target_p,
                             use_format,
                             key_union->unsigned_short_int_data);
    }
    else if(key_type == CDICT_DATA_TYPE_UNSIGNED_LONG_INT)
    {
        ret = cdict_asprintf(cdict,target_p,
                             use_format,
                             key_union->unsigned_long_int_data);
    }
    else if(key_type == CDICT_DATA_TYPE_BOOLEAN)
    {
        ret = cdict_asprintf(cdict,target_p,
                             use_format,
                             __CDict_truefalse(key_union->Boolean_data));
    }
    else if(key_type == CDICT_DATA_TYPE_CHAR)
    {
        ret = cdict_asprintf(cdict,target_p,
                             use_format,
                             key_union->char_data);
    }
    else if(key_type == CDICT_DATA_TYPE_FLOAT)
    {
#ifdef CDICT_USE_RYU
        /*
         * use ryu's float to string conversion
         * N.B. format is ignored
         */
        ret = cdict_ryu_f2s_asprintf(cdict,
                                     target_p,
                                     key_union->float_data);
#else
        /* use BSD's asprintf */
        ret = cdict_asprintf(cdict,target_p,
                             use_format,
                             key_union->float_data);
#endif // CDICT_USE_RYU
    }
    else if(key_type == CDICT_DATA_TYPE_INT_POINTER)
    {
        ret = cdict_asprintf(cdict,target_p,
                             use_format,
                             key_union->int_pointer_data);
    }
    else if(key_type == CDICT_DATA_TYPE_DOUBLE_POINTER)
    {
        ret = cdict_asprintf(cdict,target_p,
                             use_format,
                             key_union->double_pointer_data);
    }
    else if(key_type == CDICT_DATA_TYPE_UNSIGNED_INT_POINTER)
    {
        ret = cdict_asprintf(cdict,target_p,
                             use_format,
                             key_union->int_pointer_data);
    }
    else if(key_type == CDICT_DATA_TYPE_SHORT_INT_POINTER)
    {
        ret = cdict_asprintf(cdict,target_p,
                             use_format,
                             key_union->short_int_pointer_data);
    }
    else if(key_type == CDICT_DATA_TYPE_UNSIGNED_SHORT_INT_POINTER)
    {
        ret = cdict_asprintf(cdict,target_p,
                             use_format,
                             key_union->unsigned_short_int_pointer_data);
    }
    else if(key_type == CDICT_DATA_TYPE_LONG_INT_POINTER)
    {
        ret = cdict_asprintf(cdict,target_p,
                             use_format,
                             key_union->long_int_pointer_data);
    }
    else if(key_type == CDICT_DATA_TYPE_UNSIGNED_LONG_INT_POINTER)
    {
        ret = cdict_asprintf(cdict,target_p,
                             use_format,
                             key_union->unsigned_long_int_pointer_data);
    }
    else if(key_type == CDICT_DATA_TYPE_BOOLEAN_POINTER)
    {
        ret = cdict_asprintf(cdict,target_p,
                             use_format,
                             key_union->Boolean_pointer_data);
    }
    else if(key_type == CDICT_DATA_TYPE_VOID_POINTER)
    {
        ret = cdict_asprintf(cdict,target_p,
                             use_format,
                             key_union->void_pointer_data);
    }
    else if(key_type == CDICT_DATA_TYPE_FLOAT_POINTER)
    {
        ret = cdict_asprintf(cdict,target_p,
                             use_format,
                             key_union->float_pointer_data);
    }
    else
    {
        *target_p = NULL;
        ret = CDICT_ERROR_TO_STRING_TYPE_FAILED;
    }

    return ret;
#endif
}

#endif // __HAVE_LIBCDICT__

typedef int prevent_ISO_C_warning;
        