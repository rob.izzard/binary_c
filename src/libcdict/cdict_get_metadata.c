

#include "../binary_c.h"
No_empty_translation_unit_warning;
#undef is_integer
#ifndef __HAVE_LIBCDICT__
#include "cdict.h"
#undef exit


/*
 * Get metadata from an entry
 */

__CDict_Nonnull_some_arguments(1)
CDict_API_function
void * cdict_get_metadata(const struct cdict_entry_t * const entry)
{
    return entry->metadata->data;
}

#endif // __HAVE_LIBCDICT__

typedef int prevent_ISO_C_warning;
        