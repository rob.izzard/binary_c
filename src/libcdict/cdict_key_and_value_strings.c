

#include "../binary_c.h"
No_empty_translation_unit_warning;
#undef is_integer
#ifndef __HAVE_LIBCDICT__
#include "cdict.h"
#undef exit


/*
 * Convenience wrapper around cdict_(value|key)_to_string.
 *
 * NB this does not take a format string: for that you
 * must call cdict_value_to_string and/or cdict_key_to_string
 * yourself.
 */
int cdict_key_and_value_strings(struct cdict_t * const cdict,
                                struct cdict_key_t * const key,
                                struct cdict_value_t * const value,
                                char ** const value_string)
{
    int ret = cdict_set_key_string(cdict,
                                   key,
                                   NULL);
    if(ret >= 0)
    {
        ret = cdict_value_to_value_string(cdict,
                                          value,
                                          value_string,
                                          NULL,
                                          -1);

    }
    return ret;
}

#endif // __HAVE_LIBCDICT__

typedef int prevent_ISO_C_warning;
        