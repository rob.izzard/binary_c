

#include "../binary_c.h"
No_empty_translation_unit_warning;
#undef is_integer
#ifndef __HAVE_LIBCDICT__
#include "cdict.h"
#undef exit

#include <math.h>
#include <stdio.h>
#include "cdict_sort_auto.h"

/*
 * Like cmp functions, but for two double precision numbers.
 *
 * Tolerances are set in cdict_sort_auto.
 */
extern long double cdict_absolute_tolerance;
extern long double cdict_relative_tolerance;

__CDict_Pure_function
int cdict_cf_long_doubles(const long double x,
                           const long double y)
{
    int ret;
    const long double diff = fabsl(x - y);
    if(diff <= cdict_absolute_tolerance)
    {
        ret = 0;
    }
    else
    {
        const long double largest = fabsl(x) > fabsl(y) ? x : y;
        if(diff <= largest * cdict_relative_tolerance)
        {
            ret = 0;
        }
        else
        {
            ret = __cdict_cmp(x, y);
        }
    }
    return ret;
}

#endif // __HAVE_LIBCDICT__

typedef int prevent_ISO_C_warning;
        