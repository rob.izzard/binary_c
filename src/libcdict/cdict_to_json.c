

#include "../binary_c.h"
No_empty_translation_unit_warning;
#undef is_integer
#ifndef __HAVE_LIBCDICT__
#include "cdict.h"
#undef exit


#include <stdio.h>

/*
 * Function to set a buffer with JSON-style contents
 * of a cdict.
 */

#undef exit
__CDict_Nonnull_all_arguments
static void new_buffer(struct cdict_t * const cdict,
                       char ** const bufferp,
                       size_t * const buffer_sizep,
                       const Boolean newlines);
__CDict_Nonnull_all_arguments
static void show_end_of_buffer(struct cdict_t * const cdict,
                               const char * const colour,
                               const char * const buf,
                               const size_t buf_size);
__CDict_Nonnull_all_arguments
static void strip_trailing_comma(struct cdict_t * const cdict,
                                 char ** const bufferp,
                                 size_t * const buffer_sizep,
                                 const Boolean newlines);
__CDict_Nonnull_all_arguments
static void close_buffer(struct cdict_t * const cdict,
                         char ** const bufferp,
                         size_t * const buffer_sizep,
                         const size_t indent_chars,
                         const Boolean add_trailing_null,
                         const Boolean newlines);
__CDict_Nonnull_all_arguments
static void add_trailing_comma(struct cdict_t * const cdict,
                               char ** const bufferp,
                               size_t * const buffer_sizep,
                               const Boolean newlines);

/*
 * Verbose logging?
 */
#undef jprint
#define jprint(...)                             \
    if(cdict->vb)                               \
    {                                           \
        fprintf(stdout,"CDICT->JSON:");         \
        fprintf(stdout,__VA_ARGS__);            \
        fflush(stdout);                         \
    }
#undef jprint
#define jprint(...) /* do nothing */

/*
 * Standard indent string, 4 spaces
 */
static const char indent_char = ' ';
static const size_t indent_string_len = 4;
#define Indent_chars(N) (indent==TRUE ? ((size_t)(1 + (int)__CDict_Max((int)(N),(int)0))*indent_string_len) : 0)

/*
 * Macros to access the buffer and buffer_size quickly
 */
#define buffer (*bufferp)
#define buffer_size (*buffer_sizep)
#define buffer_end (buffer + buffer_size)

/************************************************************/

__CDict_Nonnull_some_arguments(1,2,3)
CDict_API_function
int cdict_to_json(struct cdict_t * const cdict,
                  char ** const bufferp,
                  size_t * const buffer_sizep,
                  const unsigned int nesting_level,
                  const char * const item_separator,
                  const char * const key_separator,
                  const Boolean indent,
                  const Boolean newlines,
                  const Boolean sort)
{
    /*
     * Set human-readable, JSON format version of "cdict" in buffer "*bufferp"
     * setting the buffer_sizep appropriately.
     *
     * Input:
     * cdict : the cdict whose contents will be put in the buffer.
     * **bufferp: pointer to the buffer to be set.
     * *buffer_sizep: size of the buffer.
     * nesting_level: starting indentation
     * item_separator: string that separates items, usually ",".
     * key_separator: string that separates keys from values, usually ":".
     * indent: if TRUE, indents output.
     * newlines: if TRUE, separates output with newlines where appropriate.
     * sort: if TRUE, sorts the output.
     */
    jprint("cdict_to_json : cdict = %p, ",(void*)cdict);
    jprint("cdict_entry_list %p, ",(void*)cdict->cdict_entry_list);
    jprint("json buffer = %p, ",(void*)bufferp);
    jprint("json *bufferp = buffer = %p, ",bufferp==NULL?NULL:(void*)*bufferp);
    jprint("buffer_sizep = %p (=%zu), ",(void*)buffer_sizep,*buffer_sizep);
    jprint("nesting_level %u, indent? %s\n",nesting_level,__CDict_Yesno(indent));

#define comma (item_separator == NULL ? "," : item_separator)
#define colon (key_separator == NULL ? " : " : key_separator)

    /*
     * Allocate space for buffer if required
     */
    if(buffer == NULL)
    {
        new_buffer(cdict,
                   bufferp,
                   buffer_sizep,
                   newlines);
        jprint("made new json buffer at %p (size %zu)\n",
               (void*)buffer,
               buffer_size);
    }
    else
    {
        jprint("Using existing bufferp = %p , buffer = %p, buffer_size = %zu\n",
               (void*)bufferp,
               (void*)buffer,
               buffer_size);
    }
    const size_t indent_chars = Indent_chars(nesting_level);

    if(sort == TRUE)
    {
        CDict_sort(cdict);
    }

    CDict_loop(cdict,entry)
    {

        /*
         * Make the JSON chunk : note that we strip off the trailing
         * NULL byte by reducing the chunk_size by 1. The trailing
         * byte is handled by us explicitly.
         */
        char * chunk = NULL;
        size_t chunk_size = 0;
        const char * const key_quotes = __CDict_key_quotestring(entry->key.type);

        jprint("ENTRY type %s (type %d, group %zu) value pointer %p\n",
               __CDict_value_descriptor(entry->value.type),
               entry->value.type,
               __CDict_group(entry->value.type),
               (void*)cdict_get_union_data_pointer(entry)
            );

        if(entry->value.type == CDICT_DATA_TYPE_CDICT &&
           entry->value.value.cdict_pointer_data != NULL)
        {
            /*
             * The cdict's key
             */
            chunk_size = asprintf(&chunk,
                                  "%s%s%s%s{%s",
                                  key_quotes,
                                  entry->key.string,
                                  key_quotes,
                                  colon,
                                  newlines == TRUE ? "\n" : ""
                );

            /*
             * Require a recursive call
             */
            jprint("call for nested cdict at %p buffer_size %zu\n",
                   (void*)entry->value.value.cdict_pointer_data,
                   buffer_size);
            if(cdict_to_json(entry->value.value.cdict_pointer_data,
                             &chunk,
                             &chunk_size,
                             nesting_level+1,
                             item_separator,
                             key_separator,
                             indent,
                             newlines,
                             sort) == CDICT_TO_JSON_SUCCESS)
            {
                jprint("add trailing comma to nested chunk, buffer_size %zu\n",buffer_size);
                add_trailing_comma(cdict,&chunk,&chunk_size,newlines);
                jprint("show end of buffer chunk_size %zu, buffer_size %zu\n",chunk_size,buffer_size);
                show_end_of_buffer(cdict,
                                   CDICT_YELLOW,
                                   chunk,
                                   chunk_size);
                jprint("show finished, buffer size %zu\n",buffer_size);
            }
        }
        else if(__CDict_datatype_is_array(entry->value.type))
        {
            /*
             * We have a pointer to an array. We treat this as a
             * list of items, i.e. JSON's [...].
             *
             * We need to know how many are stored (i.e. the size of the
             * array) and assume you have set this in entry->count
             */

            /*
             * Get the key string
             */

            /*
             * Open the list with keystring:
             */
            {
                chunk_size = asprintf(&chunk,
                                      "%s%s%s%s[%s",
                                      key_quotes,
                                      entry->key.string,
                                      key_quotes,
                                      colon,
                                      newlines == TRUE ? "\n" : ""
                    );
            }
            jprint("TO JSON ARRAY count %lld\n",entry->value.count);

            size_t sub_chunk_size;
            char * sub_chunk;
            if(entry->value.type == CDICT_DATA_TYPE_CDICT_ARRAY)
            {
                /*
                 * Special case : array of cdicts
                 */
                const cdict_size_t last = entry->value.count-1;
                for(cdict_size_t i=0;i<entry->value.count;i++)
                {
                    jprint("Call cdict_to_json at %p (index %lld)\n",
                           (void*)entry->value.value.cdict_array_data[i],
                           i);

                    sub_chunk_size = 0;
                    sub_chunk = NULL;
                    if(cdict_to_json(entry->value.value.cdict_array_data[i],
                                     &sub_chunk,
                                     &sub_chunk_size,
                                     nesting_level+2,
                                     item_separator,
                                     key_separator,
                                     indent,
                                     newlines,
                                     sort) == CDICT_TO_JSON_SUCCESS)
                    {
                        char * new_sub_chunk;
                        sub_chunk_size = (size_t) asprintf(&new_sub_chunk,
                                                           "%*s%.*s%s%s",
                                                           indent == TRUE ? ((int)(indent_chars+indent_string_len)) : 0,
                                                           "",
                                                           (int)sub_chunk_size,
                                                           sub_chunk,
                                                           i != last ? "," : "",
                                                           newlines == TRUE ? "\n" : "");
                        if(sub_chunk && new_sub_chunk)
                        {
                            free(sub_chunk);
                            sub_chunk = new_sub_chunk;
                        }

                        /* spacing fudges */
                        if(newlines == TRUE)
                        {
                            if(sub_chunk[sub_chunk_size-2] == ',' &&
                               sub_chunk[sub_chunk_size-3] == '\n')
                            {
                                sub_chunk[sub_chunk_size-2] = ' ';
                                sub_chunk[sub_chunk_size-3] = ',';
                            }
                            else if(sub_chunk[sub_chunk_size-2] == '\n' &&
                                    last == TRUE)
                            {
                                sub_chunk[sub_chunk_size-2] = ' ';
                            }
                        }

                        if(sub_chunk_size > 0)
                        {
                            /*
                             * Append sub_chunk onto chunk
                             */
                            jprint("realloc to %zu + %zu = %zu\n",
                                   chunk_size,
                                   sub_chunk_size,
                                   chunk_size + sub_chunk_size);

                            chunk = __CDict_realloc(cdict,
                                                    chunk,
                                                    chunk_size + sub_chunk_size);
                            if(chunk != NULL)
                            {
                                memcpy(chunk + chunk_size,
                                       sub_chunk,
                                       sub_chunk_size*sizeof(char));
                                chunk_size += sub_chunk_size;
                            }
                            else
                            {
                                buffer_size = 0;
                                return CDICT_TO_JSON_ALLOC_ERROR;
                            }
                            if(sub_chunk)
                            {
                                free(sub_chunk);
                            }
                        }
                    }
                }
            }
            else
            {
                if(entry->value.count > 0)
                {
                    /*
                     * Loop over the array
                     */
                    const cdict_size_t last = entry->value.count-1;
                    for(cdict_size_t i=0;i<entry->value.count;i++)
                    {
                        char * value_string = NULL;
                        CDict_entry_index_to_value_string(cdict,
                                                          entry,
                                                          value_string,
                                                          NULL,
                                                          i);
                        const char * const value_quotes = __CDict_value_quotestring(entry->value.type);
                        sub_chunk_size = (size_t) asprintf(&sub_chunk,
                                                           "%*s%s%s%s%s%s",
                                                           indent == TRUE ? ((int)(indent_chars+indent_string_len)) : 0,
                                                           "",
                                                           value_quotes,
                                                           value_string,
                                                           value_quotes,
                                                           i != last ? "," : "",
                                                           newlines == TRUE ? "\n" : "");
                        CDict_Safe_free(cdict,value_string);

                        if(sub_chunk_size > 0)
                        {
                            /*
                             * Append sub_chunk onto chunk
                             */
                            jprint("realloc to %zu + %zu = %zu\n",
                                   chunk_size,
                                   sub_chunk_size,
                                   chunk_size + sub_chunk_size);

                            chunk = __CDict_realloc(cdict,
                                                    chunk,
                                                    chunk_size + sub_chunk_size);
                            if(chunk != NULL)
                            {

                                memcpy(chunk + chunk_size,
                                       sub_chunk,
                                       sub_chunk_size*sizeof(char));
                                chunk_size += sub_chunk_size;
                            }
                            else
                            {
                                buffer_size = 0;
                                return CDICT_TO_JSON_ALLOC_ERROR;
                            }
                        }
                        CDict_Safe_free(cdict,sub_chunk);
                    }
                }
            }
            /*
             * Close the list
             */
            sub_chunk_size = asprintf(&sub_chunk,
                                      "%*s],%s",
                                      indent == TRUE ? ((int)(indent_chars)) : 0,
                                      "",
                                      newlines == TRUE ? "\n" : "");
            if(sub_chunk_size > 0)
            {
                jprint("realloc to %zu + %zu = %zu\n",
                       chunk_size,
                       sub_chunk_size,
                       chunk_size + sub_chunk_size);

                chunk = __CDict_realloc(cdict,
                                        chunk,
                                        chunk_size + sub_chunk_size);
                if(chunk != NULL)
                {
                    memcpy(chunk + chunk_size,
                           sub_chunk,
                           sub_chunk_size*sizeof(char));
                    chunk_size += sub_chunk_size;
                }
                else
                {
                    buffer_size = 0;
                    return CDICT_TO_JSON_ALLOC_ERROR;
                }
            }
            CDict_Safe_free(cdict,sub_chunk);
        }
        else
        {
            /*
             * Scalar type
             */

            /*
             * If the entry has a pre-output function,
             * call it
             */
            if(entry->pre_output != NULL &&
               entry->pre_output->function != NULL)
            {
                cdict_pre_output_f func =
                    (cdict_pre_output_f)(entry->pre_output->function);
                func(cdict,
                     entry,
                     entry->pre_output->data);
            }

            /*
             * use the key and value strings
             * to make the chunk
             */
            char * value_string = NULL;
            CDict_entry_to_key_and_value_strings(cdict,
                                                 entry,
                                                 value_string);

            /*
             * Make the chunk with asprintf : remember asprintf returns
             * the number of characters *excluding* the trailing NULL
             * byte, but this is what we want for the later memcpy.
             */
            const char * const key_quotes2 = __CDict_key_quotestring(entry->key.type);
            const char * const value_quotes2 = __CDict_value_quotestring(entry->value.type);
            if(newlines == TRUE)
            {
                jprint("key quotes %s\n",key_quotes2);
                jprint("key string %s\n",entry->key.string);
                jprint("colon %s\n",colon);
                jprint("value_quotes %s\n",value_quotes2);
                jprint("value string %s\n",value_string);
                jprint("comma %s\n",comma);
                chunk_size = (size_t) asprintf(&chunk,
                                               "%s%s%s%s%s%s%s%s\n",
                                               key_quotes2,
                                               entry->key.string,
                                               key_quotes2,
                                               colon,
                                               value_quotes2,
                                               value_string,
                                               value_quotes2,
                                               comma);
            }
            else
            {
                chunk_size = (size_t) asprintf(&chunk,
                                               "%s%s%s%s%s%s%s%s",
                                               key_quotes2,
                                               entry->key.string,
                                               key_quotes2,
                                               colon,
                                               value_quotes2,
                                               value_string,
                                               value_quotes2,
                                               comma);
            }
            jprint("chunk, size %zu, made \"%s\" \n",
                   (size_t)chunk_size,
                   chunk);
            /*
             * Free value_string as it is
             * are no longer required
             */
            CDict_Safe_free(cdict,value_string);
        }

        /*
         * Check if we have anything to insert, if so, do it
         */
        jprint("chunk size %zu\n",chunk_size);
        jprint("current buffer size %zu\n",buffer_size);
        if(chunk_size > 0)
        {
            /*
             * Save insert location offset : remember there is no NULL byte
             * so we just require the existing buffer_size
             */

            /*
             * Insert the chunk into resized buffer
             */
            jprint("attempt realloc of buffer = %p, current size %zu, to size %zu\n",
                   buffer,
                   buffer_size,
                   buffer_size + chunk_size);
            buffer = __CDict_realloc(cdict,
                                     buffer,
                                     buffer_size +
                                     chunk_size +
                                     indent_chars);

            jprint("realloced to %p\n",buffer);

            if(buffer != NULL)
            {
                /*
                 * Indent at the end of the buffer
                 * and move the end of the buffer
                 */
                if(indent==TRUE)
                {
                    memset(buffer_end,
                           indent_char,
                           indent_chars);
                    buffer_size += indent_chars;
                }

                /*
                 * Insert chunk at the end of the buffer
                 * and move the end of the buffer
                 */
                memcpy(buffer_end,
                       chunk,
                       (size_t)chunk_size);
                buffer_size += chunk_size;

                jprint("increased buffer at %p to size buffer_sizep = %zu (pre (re)alloc)\n",
                       buffer,
                       buffer_size);
            }
            else
            {
                /*
                 * Realloc failed : this is a critical failure.
                 * Free the chunk, and the buffer, and return
                 * CDICT_TO_JSON_ALLOC_ERROR.
                 */
                jprint("realloc failed\n");
                CDict_Safe_free(cdict,
                                chunk);
                buffer_size = 0;
                return CDICT_TO_JSON_ALLOC_ERROR;
            }
        }

        /*
         * Free the chunk
         */
        CDict_Safe_free(cdict,chunk);
    }


    show_end_of_buffer(cdict,
                       CDICT_CYAN,
                       buffer,
                       buffer_size);

    /*
     * Close the buffer
     */
    strip_trailing_comma(cdict,
                         bufferp,
                         buffer_sizep,
                         newlines);
    show_end_of_buffer(cdict,
                       CDICT_BLUE,
                       buffer,
                       buffer_size);

    close_buffer(cdict,
                 bufferp,
                 buffer_sizep,
                 nesting_level==0 ? 0 : Indent_chars(nesting_level-1),
                 nesting_level==0 ? TRUE : FALSE,
                 newlines);

    show_end_of_buffer(cdict,CDICT_MAGENTA,buffer,buffer_size);

    jprint("Final:\n%s\n",buffer);
    show_end_of_buffer(cdict,CDICT_GREEN,buffer,buffer_size);

    jprint("return from cdict to json (buffer size %zu)\n",buffer_size);
    return CDICT_TO_JSON_SUCCESS;
}

__CDict_Nonnull_all_arguments
static void strip_trailing_comma(struct cdict_t * const cdict,
                                 char ** const bufferp,
                                 size_t * const buffer_sizep,
                                 const Boolean newlines)
{
    if(newlines == TRUE)
    {
        if(buffer_size >= 2)
        {
            if(*(buffer_end - 2) == ',' &&
               *(buffer_end - 1) == '\n')
            {
                *(buffer_end - 2) = '\n';
                buffer = __CDict_realloc(cdict,
                                         buffer,
                                         buffer_size - 1);
                if(buffer)
                {
                    buffer_size--;
                }
                else
                {
                    buffer_size = 0;
                }
            }
        }
    }
    else
    {
        if(buffer_size >= 1)
        {
            if(*(buffer_end - 1) == ',')
            {
                buffer = __CDict_realloc(cdict,buffer,
                                         buffer_size - 1);
                if(buffer)
                {
                    buffer_size--;
                }
                else
                {
                    buffer_size = 0;
                }
            }
        }
    }
}



__CDict_Nonnull_all_arguments
static void add_trailing_comma(struct cdict_t * const cdict,
                               char ** const bufferp,
                               size_t * const buffer_sizep,
                               const Boolean newlines)
{
    /*
     * Add a trailing ,\n
     *
     * Note that bufferp and buffer_sizep cannot be NULL.
     */
    jprint("add trailing comma buffer_size %zu\n",buffer_size);
    if(*(buffer+buffer_size-1) == '\n')
    {
        /* insert comma before \n */
        buffer = __CDict_realloc(cdict,buffer,
                                 buffer_size+1);
        jprint("realloced to insert \\n %zu\n",buffer_size);
        if(buffer)
        {
            *(buffer+buffer_size-1) = ',';
            *(buffer+buffer_size) = '\n';
            buffer_size++;
        }
        else
        {
            buffer_size = 0;
        }
        jprint("buffer size then %zu\n",buffer_size);
    }
    else
    {
        if(newlines == TRUE)
        {
            /* insert ,\n at the end */
            buffer = __CDict_realloc(cdict,buffer,
                                     buffer_size+2);

            jprint("realloced to insert ,\\n %zu\n",buffer_size);
            if(buffer)
            {
                *(buffer+buffer_size) = ',';
                *(buffer+buffer_size+1) = '\n';
                buffer_size+=2;
            }
            else
            {
                buffer_size = 0;
            }
        }
        else
        {
            /* insert , at the end */
            buffer = __CDict_realloc(cdict,buffer,
                                     buffer_size+1);
            if(buffer)
            {
                *(buffer+buffer_size) = ',';
                buffer_size++;
            }
            else
            {
                buffer_size = 0;
            }
        }
    }
    jprint("return buffer size %zu\n",buffer_size);
}

__CDict_Nonnull_all_arguments
static void close_buffer(struct cdict_t * const cdict,
                         char ** const bufferp,
                         size_t * const buffer_sizep,
                         const size_t indent_chars,
                         const Boolean add_trailing_null,
                         const Boolean newlines)
{
    /*
     * Close the buffer by adding the final }\n\0.
     *
     * Note that bufferp and buffer_sizep cannot be NULL.
     */

    /*
     * Increase the buffer size
     */
    size_t new_size = buffer_size +
        indent_chars +
        1 + // for }
        (newlines == TRUE ? 1 : 0) + // for \n
        (add_trailing_null == TRUE ? 1 : 0);  // for \0

    jprint("realloc to size %zu\n",new_size);
    buffer = __CDict_realloc(cdict,
                             buffer,
                             new_size);
    if(buffer != NULL)
    {
        /*
         * Set indent space
         */
        if(indent_chars>0)
        {
            memset(buffer_end,
                   indent_char,
                   indent_chars);
            buffer_size += indent_chars;
        }

        *(buffer_end) = '}';
        buffer_size++;

        if(newlines == TRUE)
        {
            *(buffer_end) = '\n';
            buffer_size++;
        }
        if(add_trailing_null == TRUE)
        {
            *(buffer_end) = '\0';
            buffer_size++;
        }
    }
    else
    {
        buffer_size = 0;
    }
}

__CDict_Nonnull_all_arguments
static void show_end_of_buffer(struct cdict_t * const cdict CDict_maybe_unused,
                               const char * const colour CDict_maybe_unused,
                               const char * const buf CDict_maybe_unused,
                               const size_t buf_size CDict_maybe_unused)
{
    int i = 0;
    jprint("%s",colour);
    while(--i>-10)
    {
        jprint("offset %d : char %c\n",i,*(buf + buf_size + i));
    }
    jprint("%s",CDICT_COLOUR_RESET);
}


__CDict_Nonnull_all_arguments
static void new_buffer(struct cdict_t * const cdict,
                       char ** const bufferp,
                       size_t * const buffer_sizep,
                       const Boolean newlines)
{
    buffer_size = 1 + (newlines == TRUE ? 1 : 0);
    buffer = __CDict_malloc(cdict,buffer_size);
    *(buffer + 0) = '{';
    if(newlines==TRUE)
    {
        *(buffer + 1) = '\n';
    }
}

#endif // __HAVE_LIBCDICT__

typedef int prevent_ISO_C_warning;
        