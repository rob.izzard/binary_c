/*
 * The binary_c stellar population nucleosynthesis framework.
 *
 * Copyright Robert Izzard 2000-2025
 *
 * Contact rob.izzard@gmail.com
 *
 * Code repository:
 * https://gitlab.com/binary_c
 *
 * Documentation is in doc/
 * Code is in src/
 *
 * To build, run
 *
 * ---
 *
 * meson setup builddir
 * cd builddir
 * ninja binary_c_install
 *
 * ---
 *
 * Please see the files README, LICENCE and CHANGES,
 * and the doc/ directory for documentation which is mirrored
 * at https://binary_c.gitlab.io
 *
 **********************
 *
 * Subroutine: hoover_and_sms
 *
 * Purpose: When binary_c fails and segfaults (SIGSEGV), print a
 *          message to stdout and then call Exit_binary_c() to exit.
 *
 * Arguments:
 *     stardata : binary_c's stardata struct
 *     s : error code to be printed to screen and sent
 *         to Exit_binary_c
 *
 * Returns: nothing
 *
 * Note: this function may well be removed or renamed in future.
 *       It is a remnant of the original binary_c from 20 years ago
 *       and not really used any more.
 *
 **********************
 */

#include "binary_c.h"

void No_return hoover_and_sms(struct stardata_t * const stardata,
                              const unsigned int s)
{
    if(s==BINARY_C_TIMED_OUT)
    {
        _printf("Hoover up: Well, it would seem we have timed out\n");
    }
    else if(s==BINARY_C_CAUGHT_SEGFAULT)
    {
        _printf("Hoover up : caught segfault\n");
    }
    Exit_binary_c(s,"Exiting with error code : %d\n",(int)s);
}
