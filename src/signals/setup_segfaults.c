#include "../binary_c.h"
No_empty_translation_unit_warning;


#include <signal.h>
#include <unistd.h>

void catch_float_signals(void);
void catch_float(int signo) No_return;
static void Maybe_unused catch_sigsegv(int signum, siginfo_t* info, void*ptr);

void stackhandler(int sig) No_return;
void No_return stackhandler(int sig)
{
    Exit_binary_c_no_stardata(
        BINARY_C_STACK_HANDLER,
        "Stack handler %d\n",
        sig);
}

char * Maybe_unused setup_segfaults(void)
{
#ifdef ALTSTACK
    /*
     * Set up an alternative stack on which we can
     * capture segmentation faults.
     *
     * This subroutine should be called only once,
     * otherwise it will leak memory.
     *
     * The memory in altstack, the pointer which is
     * returned, is not freed here. You should free it
     * just before exiting binary_c.
     *
     * Also, while this function *can* be called from
     * the shared library interface, it probably shouldn't be,
     * as the parent application should handle segmentation faults
     * itself, and if you run many stars (each of which calls this function)
     * you will suffer a huge memory leak.
     */
    char * altstack = Malloc(sizeof(char) * SIGSTKSZ);

    /* set up the segfault stack */
    stack_t ss;
    memset(&ss,0,sizeof(ss));
    ss.ss_size = SIGSTKSZ;
    ss.ss_sp = altstack;
    sigaltstack(&ss,0);

    /* set up the signal action */
    struct sigaction sa;
    memset(&sa,0,sizeof(sa));

    /*
     * NB using sa.handler fails on modern Linux
     *    with libbacktrace, causing a segfault
     *    in libunwind.
     */
    //sa.sa_handler = (__sighandler_t) tstp_handler;
    //sa.sa_flags = SA_ONSTACK;

    /*
     * But using sa_sigaction is fine. weird!
     */
    sa.sa_sigaction = catch_sigsegv;
    sa.sa_flags = SA_SIGINFO;
    sigfillset(&sa.sa_mask);

    /* capture the signals */
    sigaction(SIGSEGV,&sa,NULL);
    sigaction(SIGSTKFLT,&sa,0);

#endif // ALTSTACK

/*
 * Catch the unix signals SIGALARM and SIGVTALARM?
 * and set the timer
 * so we can catch runs which go on for too long (5 seconds)
 */
#ifdef LINUX
    reset_binary_c_timeout();
#endif
    catch_timeouts();
    catch_float_signals();
#ifdef ALTSTACK
    return altstack;
#else
    return NULL;
#endif // ALTSTACK

}

static void Maybe_unused catch_sigsegv(int signum,
                                       siginfo_t * info Maybe_unused,
                                       void * ptr Maybe_unused)
{
    /*
     * Some output information for segfaults.
     * Given a backtrace if possible.
     */
#define SEGFAULT_MESSAGE "binary_c: Caught segfault in tstp_handler(). Signal %d for pid %d.%s\n", \
        signum,                                                            \
        getpid(),                                                       \
        signum==SIGSEGV ? " Consider trying 'ulimit -s unlimited'. ": ""

    if(stdout!=NULL)
    {
        _printf(SEGFAULT_MESSAGE);
        fflush(stdout);
    }

#ifdef BACKTRACE
    Backtrace;

#endif // BACKTRACE

#ifdef BATCHMODE
    if(stdout)
    {
        _printf("fin crash\nfin\ncrash\n");
        fflush(stdout);
    }
#endif

    hoover_and_sms(NULL,BINARY_C_CAUGHT_SEGFAULT);
}

void catch_timeouts(void)
{
#if defined LINUX && defined CATCH_SIGVTALRM
    /* Doesn't work on solaris...! */
    signal(SIGVTALRM,catch_timeout);
#endif
}

void uncatch_timeouts(void)
{
#if defined LINUX && defined CATCH_SIGVTALRM
    signal(SIGVTALRM,SIG_IGN);
#endif
}

void catch_float_signals(void)
{
#if defined FPU_CAPTURE_INVALID
    signal(SIGFPE,catch_float);
#endif
}

void No_return catch_float(int signo)
{
    fprintf(stderr,"Caught float signal %d\n",signo);
    Exit_binary_c_no_stardata(BINARY_C_FLOATING_POINT_ERROR,
                              "Caught SIGFPE\n");
}
