#include "binary_c.h"
No_empty_translation_unit_warning;


#ifndef VALGRIND

#undef exit

#ifdef __HAVE_VALGRIND__
#include "valgrind/valgrind.h"
#endif//__HAVE_VALGRIND__

void test_for_valgrind(void)
{
    /*
     * Test for whether we are running undef Valgrind.
     *
     * If we are, and we haven't built binary_c using
     * "meson setup -Dvalgrind=true" (or similar) stop running
     * because we are likely to crash.
     */
    Boolean running_under_valgrind = FALSE;

#ifdef __HAVE_VALGRIND__
    /*
     * If we have valgrind development files installed,
     * use the RUNNING_ON_VALGRIND macro to do the detection.
     */
    if(RUNNING_ON_VALGRIND) running_under_valgrind = TRUE;
#endif//__HAVE_VALGRIND__

    /*
     * Fallback: test LD_PRELOAD
     */
    char * ld_preload = getenv("LD_PRELOAD");
    if(ld_preload &&
       (
           strstr(ld_preload, "/valgrind/") != NULL
           ||
           strstr(ld_preload, "/vgpreload") != NULL)
        )
    {
        running_under_valgrind = TRUE;
    }


    if(running_under_valgrind == TRUE)
    {
        printf("You are trying to run binary_c through valgrind without building with valgrind support. On some architectures this will fail, so please configure binary_c with something like\n\nmeson setup -Dvalgrind=true builddir\n\nand rebuild.\n\nPlease note: your support libraries (e.g. libcdict, librinterpolate, libmemoize) also should be built with generic and/or valgrind support (use \"-Dgeneric=true\" like with binary_c). If you see valgrind failing on one of their functions, it is likely because they are not built correctly.\n");
        exit(1);
    }
}
#endif // VALGRIND
