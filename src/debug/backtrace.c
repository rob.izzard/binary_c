#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif
#ifndef __USE_GNU
#define __USE_GNU
#endif

#ifdef DARWIN
#include <sys/ucontext.h>
#else
#include <ucontext.h>
#endif //DARWIN
#include <execinfo.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

/* Obtain a backtrace and print it to stdout. */

binary_c_API_function
void Print_trace(FILE * stream Maybe_unused)
{
    if(stream==NULL)
    {
        stream = stdout;
    }
#ifdef BACKTRACE
    print_trace(stream);
#endif
}

#ifdef BACKTRACE
#include <execinfo.h>

#if defined __HAVE_LIBBACKTRACE__ &&            \
    defined __HAVE_BACKTRACE_H__
#include "libbacktrace.h"
#endif // __HAVE_LIBBACKTRACE__ && __HAVE_BACKTRACE_H__

void print_trace(FILE * stream Maybe_unused)
{
#if BACKTRACE_METHOD == BACKTRACE_METHOD_LIBBACKTRACE
#if defined __HAVE_LIBBACKTRACE__ &&            \
    defined __HAVE_BACKTRACE_H__
    /*
     * libbacktrace to show the trace.
     * This is the preferred method.
     */
    fprintf(stream,"backtrace with libbacktrace\n");
    fflush(stream);
    struct backtrace_state *state = backtrace_create_state (
        "binary_c",
        BACKTRACE_SUPPORTS_THREADS,
        libbacktrace_error_callback,
        NULL
        );
    fprintf(stream,"call libbacktrace_bt\n");
    fflush(stream);
    libbacktrace_bt(state,stream);
#endif // __HAVE_LIBBACKTRACE__ && __HAVE_BACKTRACE_H__
#elif BACKTRACE_METHOD == BACKTRACE_METHOD_GNU
    /*
     * Otherwise use GNU's backtrace_symbols, sending
     * output to stderr in case of failure.
     */
    fprintf(stream,"backtrace with gnu\n");
    fflush(stream);
#define TRACELENGTH 20

    void *array[TRACELENGTH];
    size_t size = backtrace (array, TRACELENGTH);
    backtrace_symbols_fd(array,size,STDERR_FILENO);

#elif BACKTRACE_METHOD == BACKTRACE_METHOD_GNU_BUFFER

    printf(stream,"backtrace with gnu buffer\n");
    /*
     * This puts the strings in a buffer.
     * This is bad, because it requires malloc which
     * may be broken.
     */
#define TRACELENGTH 20
    char **strings;
    void *array[TRACELENGTH];
    size_t size = backtrace (array, TRACELENGTH);
    strings = backtrace_symbols (array, size);
    fprintf(BACKTRACE_GNU_BUFFER_STREAM,"Obtained %zu stack frames.\n", size);
    fflush(BACKTRACE_GNU_BUFFER_STREAM);
    for(size_t i = 0; i < size; i++)
    {
        fprintf(BACKTRACE_GNU_BUFFER_STREAM,"Frame %zu\n",i);
        fprintf(BACKTRACE_GNU_BUFFER_STREAM,"   >  %s \n", strings[i]);
        fflush(BACKTRACE_GNU_BUFFER_STREAM);
    }
    free(strings);
#elif defined BACKTRACE_SYMBOLS_LOCAL &&        \
    BACKTRACE_METHOD == BACKTRACE_METHOD_LOCAL
    /*
     * Use local function, may be buggy
     */
#define TRACELENGTH 20
    fprintf(stream,"backtrace with local\n");
    fflush(stream);
    char **strings;
    void *array[TRACELENGTH];
    size_t size = backtrace (array, TRACELENGTH);
    backtrace_symbols_local(array,size);


#else
    fprintf(stream,"backtrace_symbols unavailable on this platform\n");
    fflush(stream);
#endif // BACKTRACE_SYMBOLS_LOCAL && BACKTRACE_METHOD == BACKTRACE_METHOD_LOCAL

}


#endif // BACKTRACE
