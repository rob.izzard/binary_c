#define __BINARY_C_LINT_SKIP
#define __NATIVE_ALL__

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif // _GNU_SOURCE
#include "binary_c_code_options.h"
#include "binary_c_debug.h"
#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef BACKTRACE_SYMBOLS_LOCAL

#include <link.h>

/*
 * RGI: hack to prevent problems on old bfd.h which require
 * "config.h" to be defined. This is a workaround for that bug.
 */
#ifndef PACKAGE
#define PACKAGE 1
#endif // !PACKAGE
#ifndef PACKAGE_VERSION
#define PACKAGE_VERSION 1
#endif // PACKAGE_VERSION

int find_matching_file(struct dl_phdr_info * info,
               size_t size Maybe_unused,
               void * data) Maybe_unused;


/*
  A hacky replacement for backtrace_symbols in glibc

  backtrace_symbols in glibc looks up symbols using dladdr which is limited in
  the symbols that it sees. libbacktracesymbols opens the executable and shared
  libraries using libbfd and will look up backtrace information using the symbol
  table and the dwarf line information.

  It may make more sense for this program to use libelf instead of libbfd.
  However, I have not investigated that yet.

  Derived from addr2line.c from GNU Binutils by Jeff Muizelaar

  Copyright 2007 Jeff Muizelaar
*/

#define _GNU_SOURCE
/*
 * Test if we have libiberty, and find where it its
 * header file should be (meson checks). We need to know
 * because Debian and Fedora use different locations.
 */
#ifdef __HAVE_LIBIBERTY__
#ifdef __HAVE_LIBIBERTYH__
#include <libiberty.h>
#endif // __HAVE_LIBIBERTYH__
#ifdef __HAVE_LIBIBERTY_LIBIBERTYH__
#include <libiberty/libiberty.h>
#endif // __HAVE_LIBIBERTY_LIBIBERTYH__
#endif // __HAVE_LIBIBERTY__
#include <string.h>
#ifdef __HAVE_MALLOC_H__
#include <malloc.h>
#endif//__HAVE_MALLOC_H__
#include <stdio.h>
#include <stdlib.h>
#include <execinfo.h>
#ifdef __HAVE_LIBBFD__

/*
 * RGI: hack to prevent problems on old bfd.h which require
 * "config.h" to be defined. This is a workaround for that bug.
 */
#ifndef PACKAGE
#define PACKAGE 1
#endif // !PACKAGE
#ifndef PACKAGE_VERSION
#define PACKAGE_VERSION 1
#endif // PACKAGE_VERSION


#include <bfd.h>
#include <dlfcn.h>
#include <link.h>


/* addr2line.c -- convert addresses to line number and function name
   Copyright 1997, 1998, 1999, 2000, 2001, 2002 Free Software Foundation, Inc.
   Contributed by Ulrich Lauther <Ulrich.Lauther@mchp.siemens.de>

   This file was part of GNU Binutils.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  */


/*
 * Enable USE_BACKTRACE_CACHE to speed up lookup, but
 * at the expense of more RAM.
 * The cache contains a maximum of MAX_BACKTRACE_CACHE_SIZE elements,
 * and each time it exceeds this number BACKTRACE_N_PER_TRIM are
 * removed. Obviously, BACKTRACE_N_PER_TRIM must not be bigger than
 * MAX_BACKTRACE_CACHE_SIZE.
 */
#define USE_BACKTRACE_CACHE
#define MAX_BACKTRACE_CACHE_SIZE 600
#define BACKTRACE_N_PER_TRIM 10

/* safe freeing of memory via this macro */
#define _SAFE_FREE(A) if((A)!=NULL){free(A);}

/* use stdlib exit, not binary_c's */
#undef exit

#define fatal(a, b) exit(1)
#define bfd_fatal(a) exit(1)
#define bfd_nonfatal(a) exit(1)
#define list_matching_formats(a) exit(1)

/* 2 characters for each byte, plus 1 each for 0, x, and NULL */
#define PTRSTR_LEN (sizeof(void *) * 2 + 3)
#define true 1
#define false 0


#if 0

void (*dbfd_init)(void);
bfd_vma (*dbfd_scan_vma)(const char *string, const char **end, int base);
bfd* (*dbfd_openr)(const char *filename, const char *target);
bfd_boolean (*dbfd_check_format)(bfd *abfd, bfd_format format);
bfd_boolean (*dbfd_check_format_matches)(bfd *abfd, bfd_format format, char ***matching);
bfd_boolean (*dbfd_close)(bfd *abfd);
bfd_boolean (*dbfd_map_over_sections)(bfd *abfd, void (*func)(bfd *abfd, asection *sect, void *obj),
                                      void *obj);
#define bfd_init dbfd_init

static void load_funcs(void)
{
    void * handle = dlopen("libbfd.so", RTLD_NOW);
    dbfd_init = dlsym(handle, "bfd_init");
    dbfd_scan_vma = dlsym(handle, "bfd_scan_vma");
    dbfd_openr = dlsym(handle, "bfd_openr");
    dbfd_check_format = dlsym(handle, "bfd_check_format");
    dbfd_check_format_matches = dlsym(handle, "bfd_check_format_matches");
    dbfd_close = dlsym(handle, "bfd_close");
    dbfd_map_over_sections = dlsym(handle, "bfd_map_over_sections");
}

#endif // __HAVE_LIBBFD__

#ifdef USE_BACKTRACE_CACHE
// set up cache
size_t cachesize = 0;
struct cache_item {
    void * address;
    char buf[255];
};
struct cache_item * cache = NULL;
static char ** backtrace_cache_lookup(void * address);
static void backtrace_cache_store(void * address,
                                  char ** buf);
void backtrace_free_cache_memory(void);
#endif

static asymbol **syms;		/* Symbol table.  */

/* 150 isn't special; it's just an arbitrary non-ASCII char value.  */
#define OPTION_DEMANGLER	(150)

static void slurp_symtab(bfd * abfd);
static void find_address_in_section(bfd *abfd, asection *section, void *data);

/* Read in the symbol table.  */

static void slurp_symtab(bfd * abfd)
{
    long symcount;
    unsigned int size;

    if ((bfd_get_file_flags(abfd) & HAS_SYMS) == 0)
        return;

    symcount = bfd_read_minisymbols(abfd, false, (PTR) & syms, &size);
    if (symcount == 0)
        symcount = bfd_read_minisymbols(abfd, true /* dynamic */ ,
                                        (PTR) & syms, &size);

    if (symcount < 0)
        bfd_fatal(bfd_get_filename(abfd));
}

/* These global variables are used to pass information between
   translate_addresses and find_address_in_section.  */

static bfd_vma pc;
static const char *filename;
static const char *functionname;
static unsigned int line;
static int found;

/* Look for an address in a section.  This is called via
   bfd_map_over_sections.  */

static void find_address_in_section(bfd *abfd,
                                    asection *section,
                                    void *data __attribute__ ((__unused__)) )
{
    bfd_vma vma;
    bfd_size_type size;

    if (found)
        return;
#ifdef BFD_2_33
    if ((bfd_section_flags(section) & SEC_ALLOC) == 0)
#else
    if ((bfd_get_section_flags(abfd, section) & SEC_ALLOC) == 0)
#endif
      return;

#ifdef BFD_2_33
    vma = bfd_section_vma(section);
#else
    vma = bfd_get_section_vma(abfd, section);
#endif
    if (pc < vma)
        return;

#ifdef BFD_2_33
    size = bfd_section_size(section);
#else
    size = bfd_section_size(abfd, section);
#endif
    if (pc >= vma + size)
        return;

    if (!filename)
    {
        found = 0;
    }
    else
    {
        //printf("nearest : abfd = %p, section %p, syms %p, offset %ld, filename %s, functionname %s, line %ld\n",abfd,section,syms,pc-vma,filename,functionname,line);

        fflush(stdout);
        found = bfd_find_nearest_line(abfd,
                                      section,
                                      syms,
                                      pc - vma,
                                      &filename,
                                  &functionname,
                                      &line);
    }
    //printf("found? %d\n",found);
}

/* Read hexadecimal addresses from stdin, translate into
   file_name:line_number and optionally function name.  */
#if 0
static void translate_addresses(bfd * abfd,
                                char (*addr)[PTRSTR_LEN],
                                int naddr)
{
    while (naddr) {
        pc = bfd_scan_vma(addr[naddr-1], NULL, 16);

        found = false;
        bfd_map_over_sections(abfd,
                              find_address_in_section,
                              (PTR) NULL);

        if (!found) {
            printf("[%s] \?\?() \?\?:0\n",addr[naddr-1]);
        } else {
            const char *name;

            name = functionname;
            if (name == NULL || *name == '\0')
                name = "??";
            if (filename != NULL) {
                char *h;

                h = strrchr(filename, '/');
                if (h != NULL)
                    filename = h + 1;
            }

            printf("\t%s:%u\t", filename ? filename : "??",
                   line);

            printf("%s()\n", name);

        }

        /* fflush() is essential for using this command as a server
           child process that reads addresses from a pipe and responds
           with line number information, processing one address at a
           time.  */
        fflush(stdout);
        naddr--;
    }
}
#endif

static char** translate_addresses_buf(bfd * abfd,
                                      bfd_vma *addr,
                                      int naddr)
{
    int naddr_orig = naddr;
    char b;
    int total  = 0;
    enum { Count, Print } state;
    char *buf = &b;
    int len = 0;
    char **ret_buf = NULL;
    /* iterate over the formating twice.
     * the first time we count how much space we need
     * the second time we do the actual printing */
    for (state=Count; state<=Print; state++)
    {
        if (state == Print)
        {
            ret_buf = Malloc(total + sizeof(char*)*naddr);
            buf = (char*)(ret_buf + naddr);
            len = total;
    }
    while (naddr)
        {
            if (state == Print)
            {
                ret_buf[naddr-1] = buf;
            }
            pc = addr[naddr-1];

            found = false;


            bfd_map_over_sections(abfd,
                                  find_address_in_section,
                                  (PTR) NULL);


            if (!found)
            {
                total +=
                    snprintf(buf, len,
                             "[0x%llx] \?\?() \?\?:0",
                             (long long unsigned int) addr[naddr-1]) + 1;
            }
            else
            {
                const char *name;

                name = functionname;
                if (name == NULL || *name == '\0')
                    name = "??";
                if (filename != NULL) {
                    char *h;

                    h = strrchr(filename, '/');
                    if (h != NULL)
                        filename = h + 1;
                }
                total += snprintf(buf,
                                  len,
                                  "%s:%u\t%s()",
                                  filename ? filename : "??",
                                  line,
                                  name) + 1;

            }
            if (state == Print) {
                /* set buf just past the end of string */
                buf = buf + total + 1;
            }
            naddr--;
    }
    naddr = naddr_orig;
    }
    return ret_buf;
}
/* Process a file.  */

static char **process_file(const char *file_name, bfd_vma *addr, int naddr)
{
    bfd *abfd;
    char **matching;
    char **ret_buf;

    abfd = bfd_openr(file_name, NULL);

    if (abfd == NULL)
        bfd_fatal(file_name);

    if (bfd_check_format(abfd, bfd_archive))
        fatal("%s: can not get addresses from archive", file_name);

    if (!bfd_check_format_matches(abfd, bfd_object, &matching)) {
        bfd_nonfatal(bfd_get_filename(abfd));
        if (bfd_get_error() ==
            bfd_error_file_ambiguously_recognized) {
            list_matching_formats(matching);
            free(matching);
        }
#ifdef __HAVE_LIBIBERTY__
        xexit(1);
#else
        exit(1);
#endif
    }


    slurp_symtab(abfd);

    ret_buf = translate_addresses_buf(abfd, addr, naddr);


    if (syms != NULL) {
        free(syms);
        syms = NULL;
    }

    bfd_close(abfd);
    return ret_buf;
}

#define MAX_DEPTH 16

struct file_match {
    const char *file;
    void *address;
    void *base;
    void *hdr;
};

int find_matching_file(struct dl_phdr_info * info,
                       size_t size Maybe_unused,
                       void * data)
{
    struct file_match *match = data;
    /* This code is modeled from Gfind_proc_info-lsb.c:callback() from libunwind */
    long n;
    const ElfW(Phdr) *phdr;
    ElfW(Addr) load_base = info->dlpi_addr;
    phdr = info->dlpi_phdr;
    for (n = info->dlpi_phnum; --n >= 0; phdr++) {
        if (phdr->p_type == PT_LOAD) {
            ElfW(Addr) vaddr = phdr->p_vaddr + load_base;

            // RGI : Cast to long int to prevent compiler warning
            if ((long int)match->address >= (long int)vaddr &&
                (long int)match->address < (long int)vaddr + (long int)phdr->p_memsz) {
                /* we found a match */
                match->file = info->dlpi_name;
                // RGI : Cast to void* to prevent compiler warning
                match->base = (void *)info->dlpi_addr;
            }
        }
    }
    return 0;
}

char ** backtrace_symbols_local(void *const *buffer, int size)
{
    int stack_depth = size - 1;
#ifdef USE_BACKTRACE_CACHE
    int fromcache[size];
#endif
    int x,y;
    /* discard calling function */
    int total = 0;

    char ***locations;
    char **final;
    char *f_strings;

    locations = Malloc(sizeof(char**) * (stack_depth+1));

    bfd_init();
    for(x=stack_depth, y=0; x>=0; x--, y++){

        struct file_match match = { .address = buffer[x] };
        char **ret_buf;
        bfd_vma addr;

#ifdef USE_BACKTRACE_CACHE
        // check if ret_buf can be got from the cache
        locations[x] = backtrace_cache_lookup(match.address);

        // no, so find it the long way
        if(locations[x] == NULL)
        {
            fromcache[x]=0;
#endif

            dl_iterate_phdr(find_matching_file, &match);

            addr = (char*)buffer[x] - (char*)match.base;

            if (match.file && strlen(match.file))
                ret_buf = process_file(match.file, &addr, 1);
            else
                ret_buf = process_file("/proc/self/exe", &addr, 1);

            locations[x] = ret_buf;
            total += strlen(ret_buf[0]) + 1;
#ifdef USE_BACKTRACE_CACHE
            backtrace_cache_store(match.address,ret_buf);
        }
        else
        {
            fromcache[x] = 1;
            total += strlen(locations[x][0]) + 1;
        }
#endif
    }

    /* allocate the array of char* we are going to return and extra space for
     * all of the strings */
    const size_t sss = (total + (stack_depth + 1)) * sizeof(char*);
    final = Calloc(sss,1);    /* get a pointer to the extra space */
    f_strings = (char*)(final + stack_depth + 1);

    /* fill in all of strings and pointers */
    size_t left = sss;
    for(x=stack_depth; x>=0; x--)
    {
        if(left > 1)
        {
            printf("strlcpy : left = %zu, want %zu, copy %s to %p\n",
                   left,
                   strlen(locations[x][0]),
                   locations[x][0],
                   (void*)f_strings
                );
            fflush(stdout);
            strlcpy(f_strings,
                    locations[x][0],
                    left);
#ifdef USE_BACKTRACE_CACHE
            if(fromcache[x]) _SAFE_FREE(locations[x][0]);
#endif
            _SAFE_FREE(locations[x]);
            final[x] = f_strings;
            size_t ds =  strlen(f_strings) + 1;
            f_strings += ds;
            left -= ds;
        }
    }

    _SAFE_FREE(locations);
    return final;
}


void backtrace_symbols_fd_local(void *const *buffer, int size, int fd Maybe_unused)
{
    int j;
    char **strings;

    strings = backtrace_symbols_local(buffer, size);
    if (strings == NULL)
    {
        perror("backtrace_symbols");
        exit(EXIT_FAILURE);
    }
    else
    {
        for (j = 0; j < size; j++)
        {
            printf("%s\n", strings[j]);
        }
        free(strings);
    }
}



#ifdef USE_BACKTRACE_CACHE
static char ** backtrace_cache_lookup(void * address)
{
    // check for cache
    if(cachesize==0) return NULL;
    // look up address in the cache, most recent first
    int i = cachesize-1;
    while(i>=0)
    {
        if(address == cache[i].address)
        {
            // cache match
            // copy to free-able content string
            char ** retbuf = Malloc(sizeof(char*));
            retbuf[0] =  Malloc(sizeof(char)*256);
            strlcpy(retbuf[0],cache[i].buf,255);
            return retbuf;
        }
        i--;
    }
    return NULL;
}

static void backtrace_cache_store(void * address,
                                  char ** buf)
{
    // store address in cache

    // check cachesize does not exceed limit
    if(cachesize < MAX_BACKTRACE_CACHE_SIZE)
    {
        // make cache bigger
        cache = realloc(cache, (cachesize+1) * sizeof(struct cache_item));
    }
    else
    {
        // cache is too big : trim BACKTRACE_N_PER_TRIM items off it
        cachesize -= BACKTRACE_N_PER_TRIM;
        memmove(cache,cache + BACKTRACE_N_PER_TRIM,
                cachesize*sizeof(struct cache_item));
    }

    // store information
    cache[cachesize].address = address;
    strlcpy(cache[cachesize].buf,buf[0],254);

    // increase cachesize for next time
    cachesize++;
}


#endif//USE_BACKTRACE_CACHE

#endif  //__HAVE_LIBBFD__


/*
 * Always provide the free_cache_memory function, even if it
 * doesn't actually do anything.
 */
void backtrace_free_cache_memory(void)
{
#if defined BACKTRACE && defined USE_BACKTRACE_CACHE
    _SAFE_FREE(cache);
#endif
}

#endif // BACKTRACE_SYMBOLS_LOCAL
