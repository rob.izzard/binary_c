#include "../binary_c.h"
No_empty_translation_unit_warning;



void show_stardata(struct stardata_t * const stardata Maybe_unused)
{
    /*
     * Show the contents of stardata using automated
     * output from make_showstruct.pl
     */
#if defined __SHOW_STARDATA__ && defined SHOW_STARDATA
    Printf("SHOW_STARDATA_START\n");
#include "stardata.h"
    Printf("SHOW_STARDATA_END\n");
#endif
}
