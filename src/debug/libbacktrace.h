#ifndef BINARY_C_LIBBACKTRACE_H
#define BINARY_C_LIBBACKTRACE_H

/*
 * Import headers required for libbacktrace support
 */

#include <backtrace.h>
#include <backtrace-supported.h>

/* This structure mirrors the one found in /usr/include/asm/ucontext.h */
typedef struct _sig_ucontext {
    unsigned long     uc_flags;
    struct ucontext   *uc_link;
    stack_t           uc_stack;
    struct sigcontext uc_mcontext;
    sigset_t          uc_sigmask;
} sig_ucontext_t;
struct bt_ctx {
    struct backtrace_state *state;
    int error;
};

/*
 * Number of function calls to skip:
 * the first is always the reporting function in
 * libbacktrace, so skip that.
 */
#define LIBBACKTRACE_SKIP 1

#include "libbacktrace_functions.h"

/************************************************************/

#endif // BINARY_C_LIBBACKTRACE_H
