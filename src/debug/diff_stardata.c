#include "../binary_c.h"
No_empty_translation_unit_warning;



void diff_stardata(struct stardata_t * const stardata Maybe_unused)
{
    /*
     * Diff the contents of stardata using automated
     * output from make_diffstruct.pl
     */
#if defined __DIFF_STARDATA__ && defined DIFF_STARDATA
    if(stardata &&
       stardata->previous_stardata)
    {
        Printf("DIFF_STARDATA_START\n");
#include "stardata_diff.h"
        Printf("DIFF_STARDATA_END\n");
    }
#endif
}
