#pragma once
#ifndef LIBBACKTRACE_PROTOTYPES_H
#define LIBBACKTRACE_PROTOTYPES_H

void libbacktrace_error_callback(void *data, const char *msg, int errnum);

void libbacktrace_syminfo_callback(void *data Maybe_unused,
                                   uintptr_t pc,
                                   const char *symname,
                                   uintptr_t symval Maybe_unused,
                                   uintptr_t symsize Maybe_unused);

int libbacktrace_full_callback(void *data,
                               uintptr_t pc,
                               const char *filename,
                               int lineno,
                               const char *function);

int libbacktrace_simple_callback(void *data,
                                 uintptr_t pc);

void libbacktrace_bt(struct backtrace_state *state,
                     FILE * stream);


#endif // LIBBACKTRACE_PROTOTYPES_H
