#pragma once
#ifndef DEBUG_COLOURS_H
#define DEBUG_COLOURS_H

#ifdef DEBUG_COLOURS
#define COLOUR_TIME ((stardata && stardata->store) ? (stardata->store->colours[CYAN]) : "")
#define COLOUR_LINENUMBERS ((stardata && stardata->store) ? (stardata->store->colours[GREEN]) : "")
#define COLOUR_ENDLINE ((stardata && stardata->store) ? (stardata->store->colours[COLOUR_RESET]) : "")
#define COLOUR_FILENAME ((stardata && stardata->store) ? (stardata->store->colours[RED]) : "")
#define COLOUR_CALLER ((stardata && stardata->store) ? (stardata->store->colours[YELLOW]) : "")
#define COLOUR_FLOAT_BITSTRING ((stardata && stardata->store) ? (stardata->store->colours[MAGENTA]) : "")
#define COLOUR_FUNCTIONS ((stardata && stardata->store) ? (stardata->store->colours[YELLOW]) : "")
#else
#define COLOUR_TIME ""
#define COLOUR_LINENUMBERS ""
#define COLOUR_ENDLINE ""
#define COLOUR_FILENAME ""
#define COLOUR_CALLER ""
#define COLOUR_FLOAT_BITSTRING ""
#define COLOUR_FUNCTIONS ""
#endif

#endif // DEBUG_COLOURS_H
