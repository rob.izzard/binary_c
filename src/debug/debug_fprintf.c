#include "../binary_c.h"
No_empty_translation_unit_warning;

#include <execinfo.h>
#include <errno.h>
#include "../debug/debug_colours.h"

/*
 * function called by Dprint
 * i.e. debugging print statements
 */

#if defined DEBUG_FAIL_ON_NAN || defined DEBUG_FAIL_ON_INF
#define DEBUG_FAIL_ON
static void fail_on(struct stardata_t * const Restrict stardata,
                    const Boolean check,
                    const char * const Restrict s,
                    const char * const Restrict string);
#endif

#if defined DEBUG_CALLER && defined BACKTRACE
static void caller_function (char * Restrict string);
#endif

static void filter_pointers(char * s);

void Gnu_format_args(6,7) debug_fprintf(struct stardata_t * const stardata,
                                        const char * Restrict const filename,
                                        const char * Restrict const function,
                                        const int fileline,
                                        const Boolean newline Maybe_unused,
                                        const char * Restrict const format,
                                        ...)
{
    /*
     * This function should not be called directly!
     * Use the Dprint macro instead.
     *
     * filename is the source code file with the Dprint statement
     *
     * fileline is the line at which the Dprint statement occurs
     *
     * format is the (printf) format string
     *
     * ... is the variable argument list
     *
     * Output format:
     *
     * Model_Time Filename : Line : message
     *
     */
    const double t = stardata != NULL ?
        stardata->model.time : -1.0;
    va_list args;
    va_start(args,format);

    /* s contains the message */
    char s[MAX_DEBUG_PRINT_SIZE],sid[STRING_LENGTH],show[STRING_LENGTH];
    vsnprintf(s,MAX_DEBUG_PRINT_SIZE,format,args);
    chomp(s);

    show[0] = '\0';

    int snprintf_returned;
    if(function != NULL)
    {
#ifdef BINARY_C_API
        if(stardata != NULL)
        {
            snprintf_returned = snprintf(sid,
                                         STRING_LENGTH-2,
                                         "(ID %d %s)",
                                         stardata->model.id_number,
                                         show
                );
        }
        else
#endif // BINARY_C_API
        {
            snprintf_returned = snprintf(sid,
                                         STRING_LENGTH-2,
                                         "(%s)",
                                         show
                );
        }

        if(snprintf_returned <= 0)
        {
            /*
             * On error, force truncation with NULL byte
             */
            sid[STRING_LENGTH-2] = '\0';
        }
    }

    if(function == NULL)
    {
        /* show only the expression */
        fprintf(DEBUG_STREAM,"%s ",sid);
        return;
    }


    /* make the filename, remove nan e.g. in remnant (replace with n_n) */
    char f[MAX_DEBUG_PRINT_SIZE],caller[MAX_DEBUG_PRINT_SIZE];

#ifdef DEBUG_SHOW_FILENAMES
    snprintf(f,
             MAX_DEBUG_PRINT_SIZE,
             "%s%-50.50s",
             COLOUR_FILENAME,
             filename);

#ifdef DEBUG_REMOVE_NAN_FROM_FILENAMES
    {
        char *x;
        x = strstr(f,"nan");
        while(x!=NULL)
        {
            *(x)='_';
            x = strstr(f,"nan");
        }
    }
#endif // DEBUG_REMOVE_NAN_FROM_FILENAMES
#else
    // no filenames
    f[0] = '\0';
#endif

#if defined DEBUG_CALLER && defined BACKTRACE
    {
        char cc[MAX_DEBUG_PRINT_SIZE];
        caller_function(cc);
        snprintf(caller,MAX_DEBUG_PRINT_SIZE,"%s%s",COLOUR_CALLER,cc);
    }

#ifdef DEBUG_REMOVE_NAN_FROM_CALLER
    {
        char * Restrict x = strstr(caller,"nan");
        while(x!=NULL)
        {
            *(x)='_';
            x = strstr(f,"nan");
        }
    }
#endif // DEBUG_REMOVE_NAN_FROM_CALLER

#else
    // no filenames
    caller[0] = '\0';
#endif // DEBUG_CALLER && BACKTRACE

    char * float_test_bitstring =
        floating_point_exception_string();

    /*
     * Make output string
     */
    char out[MAX_DEBUG_PRINT_SIZE];
#ifdef DEBUG_LINENUMBERS
    snprintf_returned = snprintf(out,
                                 MAX_DEBUG_PRINT_SIZE,
                                 "%s%s%s%s%20.12g %d %s%s% 6d %s :%s%s%s : %s\n",
                                 sid,
                                 COLOUR_FLOAT_BITSTRING,
                                 float_test_bitstring,
                                 COLOUR_TIME,
                                 t,
                                 stardata!=NULL ? stardata->model.intpol : 0,
                                 f,
                                 COLOUR_LINENUMBERS,
                                 fileline,
                                 COLOUR_FUNCTIONS,
                                 function,
                                 caller,
                                 COLOUR_ENDLINE,
                                 s);
#else
    snprintf_returned = snprintf(out,
                                 MAX_DEBUG_PRINT_SIZE,
                                 "%s%s%s%s%20.12g %d %s%s%s%s%s : %s\n",
                                 sid,
                                 COLOUR_FLOAT_BITSTRING,
                                 float_test_bitstring,
                                 COLOUR_TIME,
                                 t,
                                 stardata!=NULL ? stardata->model.intpol : 0,
                                 f,
                                 COLOUR_FUNCTIONS,
                                 function,
                                 caller,
                                 COLOUR_ENDLINE,
                                 s);
#endif //DEBUG_LINENUMBERS

    if(snprintf_returned < 0)
    {
        /*
         * Force truncation
         */
        out[MAX_DEBUG_PRINT_SIZE - 1] = '\0';
    }

    if(stardata != NULL &&
       stardata->preferences != NULL &&
       stardata->preferences->debug_filter_pointers == TRUE)
    {
        filter_pointers(out);
    }

    /*
     * Output to the appropriate stream or buffer.
     *
     * Note that the newline directive is ignored
     * if output is sent to the Printf (the buffer)
     * because such data must end in a newline.
     */
#ifdef DEBUG_STREAM_BUFFER
    Printf("%s",out);
#else
    fprintf(DEBUG_STREAM,"%s",out);
#endif

    fflush(NULL);

#ifdef FILE_LOG
    // flush log file if it exists
    if(stardata != NULL && stardata->model.log_fp!=NULL)
        fflush(stardata->model.log_fp);
#endif //FILE_LOG

    va_end(args);

#ifdef DEBUG_FAIL_ON
    if(stardata && stardata->preferences)
    {
#ifdef DEBUG_FAIL_ON_NAN
        fail_on(stardata,stardata->preferences->allow_debug_nan,sid,"nan");
        fail_on(stardata,stardata->preferences->allow_debug_nan,s,"nan");
#endif // DEBUG_FAIL_ON_NAN
#ifdef DEBUG_FAIL_ON_INF
        fail_on(stardata,stardata->preferences->allow_debug_inf,sid,"inf");
        fail_on(stardata,stardata->preferences->allow_debug_inf,s,"inf");
#endif // DEBUG_FAIL_ON_INF
    }
#endif//DEBUG_FAIL_ON

    /* free floating point test string */
    Safe_free(float_test_bitstring);
}

#ifdef DEBUG_FAIL_ON
static void fail_on(struct stardata_t * const Restrict stardata,
                    const Boolean check,
                    const char * const Restrict s,
                    const char * const Restrict string)
{
    /*
     * Check for a NaN in the output, stop if
     * one is found.
     */
    if(stardata==NULL ||
       stardata->preferences==NULL ||
       check == FALSE)
    {
        char *nans = strstr(s,string);
        if(
            nans != NULL &&
            nans != s &&
            /* avoid words, e.g. remnant */
            !(*(nans-1) >= 'a' && *(nans-1) <= 'z') &&
            !(*(nans-1) >= 'A' && *(nans-1) <= 'Z')
            )
        {
            char nanstring[MAX_DEBUG_PRINT_SIZE];
            strlcpy(nanstring,
                    s,
                    MAX_DEBUG_PRINT_SIZE-1);
            Backtrace;
            Exit_binary_c(
                BINARY_C_EXIT_NAN,
                "%s detected in output log (allow_debug_nan = %d): DEBUG_FAIL_ON_NAN/INF is set, exiting binary_c. log string is \"%s\"",
                string,
                ((stardata != NULL && stardata->preferences != NULL) ? check : -1),
                nanstring
                );
        }
    }
}
#endif // DEBUG_FAIL_ON


#if defined DEBUG_CALLER && defined BACKTRACE

static void caller_function(char * Restrict string)
{
    /*
     * Like print trace, but return only the first function
     * that is not debug_fprintf
     */

    void *array[DEBUG_CALLER_DEPTH+2];
    size_t size;
    char **strings;
    size = backtrace (array,DEBUG_CALLER_DEPTH+1);

#if BACKTRACE_METHOD == BACKTRACE_METHOD_GNU ||                 \
    BACKTRACE_METHOD == BACKTRACE_METHOD_LIBBACKTRACE ||        \
    BACKTRACE_METHOD == BACKTRACE_METHOD_GNU_BUFFER
    /*
     * Get strings using GNU's backtrace_symbols
     *
     * NB libbacktrace points here too until I understand
     * better how it works
     */
    strings = backtrace_symbols (array, size);
    strlcpy(string,
            strings[DEBUG_CALLER_DEPTH],
            MAX_DEBUG_PRINT_SIZE);
    char * p = strstr(string,"\t");
    if(p!=NULL) *p= '\0';

#elif BACKTRACE_METHOD == BACKTRACE_METHOD_LOCAL
    /*
     * Use the local backtrace function.
     * NB this is a hack!
     */
    strings = backtrace_symbols_local (array, size);
    char * p = strstr(strings[DEBUG_CALLER_DEPTH],"(");
    if(p!=NULL)
    {
        strlcpy(string,
                1+p,
                MAX_DEBUG_PRINT_SIZE);

        // detect whether we have a function name
        if(string[0] == ')')
        {
            // no function name given
            string[0] = '\0';
        }
        else
        {
            // remove pointer string after the +
            p = strstr(string,"+");
            if(p!=NULL) *p = '\0';
        }
    }
#endif

    Safe_free(strings);


}


#endif // DEBUG_CALLER && BACKTRACE


static void filter_pointers(char * s)
{
    /*
     * Replace 0x... in string s with "pointer"
     */
    static const char p[] = "pointer";
    static const size_t l = Array_size(p) - 1;
    while(TRUE)
    {
        s = strstr(s,"0x");
        if(s == NULL)
        {
            break;
        }
        s += 2;
        size_t i = 0;
        while(isxdigit(*s))
        {
            *(s++) = p[i++%l];
        }
    }
}
