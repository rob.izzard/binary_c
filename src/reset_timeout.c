#include "binary_c.h"

#ifdef __HAVE_VALGRIND__
#include "valgrind/valgrind.h"
#endif

/*
 * The binary_c stellar population nucleosynthesis framework.
 *
 * Copyright Robert Izzard 2000-2025
 *
 * Contact rob.izzard@gmail.com
 *
 * Code repository:
 * https://gitlab.com/binary_c
 *
 * Documentation is in doc/
 * Code is in src/
 *
 * To build, run
 *
 * ---
 *
 * meson setup builddir
 * cd builddir
 * ninja binary_c_install
 *
 * ---
 *
 * Please see the files README, LICENCE and CHANGES,
 * and the doc/ directory for documentation which is mirrored
 * at https://binary_c.gitlab.io
 *
 **********************
 *
 * Subroutine: reset_binary_c_timeout
 *
 * Purpose:
 *
 * Function to reset binary_c's timeout feature.
 *
 * TIMEOUT_SECONDS is the timeout duration, ignored if zero.
 *
 * TIMEOUT_SECONDS_WITH_VALGRIND is used if we are running under
 * valgrind.
 *
 * We require the setitimer command to be available if timeouts
 * are going to work. This is detected by Meson which sets
 * __HAVE_SETITIMER__ if we have setitimer.
 *
 * Arguments: none
 *
 * Returns: nothing
 *

 **********************
 *
 * Subroutine: disable_binary_c_timeout
 *
 * Purpose: Disables binary_c's timeout functionality.
 *
 * Arguments: none
 *
 * Returns: nothing
 *
 **********************
 */

void reset_binary_c_timeout(void)
{
#ifndef __HAVE_SETITIMER__
    if(TIMEOUT_SECONDS != 0)
    {
        /*
         * We don't have setitimer but we do want a timeout:
         * exit with a warning.
         */
        Exit_binary_c_no_stardata(BINARY_C_TIMED_OUT,
                                  "TIMEOUT_SECONDS is set to %ld but we have no access to setitimer to actually set the timeout. This will cause binary_c to not have a timeout, even though you have asked for it. Please set TIMEOUT_SECONDS to 0 in binary_c_code_options.h to fix this problem, or build on an operating system that supports setitimer.",
                                  (long int)TIMEOUT_SECONDS);
    }
#else
    /*
     * We have setitimer, so if TIMEOUT_SECONDS is non-zero,
     * set the timer.
     */
    if(TIMEOUT_SECONDS != 0)
    {
        /* Timing */
        struct itimerval value;
        struct timeval tv;
        /* set timeout limit in seconds */
#ifdef __HAVE_VALGRIND__
        if(RUNNING_ON_VALGRIND)
        {
            tv.tv_sec = TIMEOUT_SECONDS_WITH_VALGRIND;
        }
        else
        {
            tv.tv_sec = TIMEOUT_SECONDS;
        }
#else
        tv.tv_sec = TIMEOUT_SECONDS;
#endif  // __HAVE_VALGRIND__
        tv.tv_usec=0;

        value.it_value = tv;
        value.it_interval = tv;
        if(unlikely(setitimer(ITIMER_VIRTUAL,&value,0) == -1))
        {
            Exit_binary_c_no_stardata(BINARY_C_TIMED_OUT,
                                      "Failed to allocate virtual timer");
        }
    }
#endif //__HAVE_SETITIMER__

}

void disable_binary_c_timeout(void)
{
#ifdef __HAVE_SETITIMER__
    /*
     * disable the timer if it could have been set (i.e. if
     * TIMEOUT_SECONDS is non-zero)
     */
    if(TIMEOUT_SECONDS != 0)
    {
        struct itimerval value;
        struct timeval tv;
        tv.tv_sec=0; /* 0 disables the timer */
        tv.tv_usec=0;
        value.it_value=tv;
        value.it_interval=tv;

        if(setitimer(ITIMER_VIRTUAL,&value,0) == -1)
        {
            Exit_binary_c_no_stardata(BINARY_C_TIMED_OUT,
                                      "Failed to allocate virtual timer");
        }
    }
#endif // __HAVE_SETITIMER__
}
