#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef BINARY_C_API

#include "../setup/cmd_line_args.h"
#include "../setup/cmd_line_function_macros.h"

/*
 * Wrapper for list_available_args which
 * allows it to be called with only stardata
 */
void binary_c_API_function binary_c_list_args(
    struct libbinary_c_stardata_t * Restrict const stardata)
{
    struct cmd_line_arg_t cmd_line_args[] =
        {
#include "setup/cmd_line_args_list.h"
        };

    Save_buffering_state;
    set_default_preferences(stardata);
    Restore_buffering_state;

    /* dummy variables */
    int c=0,i=0,argc=0;
    char ** argv = NULL;

    /* call arg listing function */
    list_available_args(ARG_SUBROUTINE_ARGS);

    char * buffer = NULL;
    size_t nbytes = 0;
    binary_c_buffer_info(stardata,&buffer,&nbytes);
}

#endif // BINARY_C_API
