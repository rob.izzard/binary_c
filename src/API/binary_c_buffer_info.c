#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef BINARY_C_API

void binary_c_API_function binary_c_buffer_info(struct stardata_t * Restrict const stardata,
                                                char ** const buffer,
                                                size_t * const size)
{
    /*
     * API function to access the binary_c raw_buffer
     */
    buffer_info(stardata,buffer,size,NULL);
}
#endif
