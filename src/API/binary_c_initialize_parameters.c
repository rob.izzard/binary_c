#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef BINARY_C_API

void binary_c_API_function binary_c_initialize_parameters(
    struct stardata_t * Restrict const stardata)
{
    initialize_parameters(stardata);
}

#endif
