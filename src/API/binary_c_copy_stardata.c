#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef BINARY_C_API

/*
 * Wrapper for copy_stardata: copies from a stardata
 * struct (from) to a stardata struct (to),
 * with an option to copy the previous_stardata array
 * (copy_previous)
 */

binary_c_API_function
struct stardata_t *
Returns_nonnull Nonnull_all_arguments
binary_c_copy_stardata(
    struct stardata_t * Restrict const from,
    struct stardata_t * Restrict const to,
    const unsigned int copy_previous,
    const unsigned int copy_persistent
    )
{
    return copy_stardata(from,
                         to,
                         copy_previous,
                         copy_persistent);
}

#endif
