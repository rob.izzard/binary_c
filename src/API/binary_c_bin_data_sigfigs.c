#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef BINARY_C_API

binary_c_API_function
Pure_function double binary_c_bin_data_sigfigs(const double x,
                                               const double bin_width,
                                               const int sigfigs)
{
    /*
     * Wrapper for bin_data_sigfigs
     */
    return bin_data_sigfigs(x,bin_width,sigfigs);
}

#endif // BINARY_C_API
