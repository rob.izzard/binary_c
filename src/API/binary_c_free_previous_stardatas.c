#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef BINARY_C_API

void binary_c_API_function
binary_c_free_previous_stardatas(struct stardata_t * Restrict const stardata)
{
    free_previous_stardatas(stardata);
}
#endif//BINARY_C_API
