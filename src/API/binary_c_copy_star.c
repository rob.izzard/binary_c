#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef BINARY_C_API

/*
 * Wrapper for copy_star: copies from a star
 * struct (from) to a star struct (to).
 */

binary_c_API_function
struct stardata_t * Returns_nonnull Nonnull_all_arguments
binary_c_copy_star(
    struct star_t  * Restrict const from,
    struct star_t  * Restrict const to
    )
{
    return copy_star(NULL,
                     from,
                     to);
}

#endif // BINARY_C_API
