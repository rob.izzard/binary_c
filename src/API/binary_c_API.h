#pragma once

#ifndef BINARY_C_API_H
#define BINARY_C_API_H

#define __BINARY_C_API

#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef BINARY_C_API

#define binary_c_Event_handler_function Event_handler_function

#include "binary_c_API_prototypes.h"

#define API_LOG_PRE 0
#define API_LOG_POST 1

/* Set APIDEBUG to 1 for debugging output */
#define APIDEBUG 0
#define APIDebug(...)                                    \
    if(APIDEBUG||DEBUG)                                  \
    {                                                    \
        binary_c_API_debug_fprintf(stardata,             \
                                   __FILE__,             \
                                   __LINE__,             \
                                   __VA_ARGS__);         \
    }
#endif // BINARY_C_API



#endif// BINARY_C_API_H
