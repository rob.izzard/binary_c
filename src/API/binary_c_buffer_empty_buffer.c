#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef BINARY_C_API

void binary_c_API_function binary_c_buffer_empty_buffer(struct stardata_t * Restrict const stardata)
{
    /*
     * API wrapper for the buffer_empty_buffer
     */
    buffer_empty_buffer(stardata);
}

#endif
