#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef BINARY_C_API

 /*
  * binary_c API logging functions
  */

void binary_c_API_function binary_c_API_log(struct stardata_t * const stardata,
                                            const unsigned int loc)
{

    /* do nothing if the prefix is set to /dev/null */
    if(Strings_equal(stardata->preferences->api_log_filename_prefix,
                     "/dev/null")) return;

    /*
     * Open log file if it isn't already
     */
    if(stardata->model.api_log_fp == NULL)
    {
        binary_c_API_open_log(stardata);
    }

    static const char locations[2][10]={"API-pre","API-post"};

    APIDebug("API::binary_c_api_log API LOG at t=%g\n",stardata->model.time);
    output_to_logfile(stardata->model.api_log_fp,
                      stardata->model.time,
                      stardata->star[0].mass,
                      stardata->star[1].mass,
                      stardata->star[0].stellar_type,
                      stardata->star[0].hybrid_HeCOWD,
                      stardata->star[1].stellar_type,
                      stardata->star[1].hybrid_HeCOWD,
                      stardata->common.orbit.separation,
                      stardata->common.orbit.period,
                      stardata->common.orbit.eccentricity,
                      Is_not_zero(stardata->star[0].roche_radius) ? (stardata->star[0].radius/stardata->star[0].roche_radius) : 0.0,
                      Is_not_zero(stardata->star[1].roche_radius) ? (stardata->star[1].radius/stardata->star[1].roche_radius) : 0.0,
                      locations[loc],
                      stardata,
                      "");
    fflush(stardata->model.api_log_fp);
}

binary_c_API_function
char * binary_c_API_logstring(struct stardata_t * const stardata,
                              const unsigned int loc)
{
    /* do nothing if the prefix is set to /dev/null */
    if(Strings_equal(stardata->preferences->api_log_filename_prefix,
                     "/dev/null")) return NULL;

    static const char locations[2][10]={"API-pre","API-post"};
    Boolean skip Maybe_unused;
    return logfile_string(stardata->model.time,
                          stardata->star[0].mass,
                          stardata->star[1].mass,
                          stardata->star[0].stellar_type,
                          stardata->star[0].hybrid_HeCOWD,
                          stardata->star[1].stellar_type,
                          stardata->star[1].hybrid_HeCOWD,
                          stardata->common.orbit.separation,
                          stardata->common.orbit.period,
                          stardata->common.orbit.eccentricity,
                          Is_not_zero(stardata->star[0].roche_radius) ? (stardata->star[0].radius/stardata->star[0].roche_radius) : 0.0,
                          Is_not_zero(stardata->star[1].roche_radius) ? (stardata->star[1].radius/stardata->star[1].roche_radius) : 0.0,
                          locations[loc],
                          stardata,
                          &skip,
                          "");
}

void binary_c_API_function binary_c_API_open_log(struct stardata_t * const stardata)
{
    /* do nothing if the prefix is set to /dev/null */

    APIDebug("check prefix '%s'\n",
             stardata->preferences->api_log_filename_prefix);

    if(Strings_equal(stardata->preferences->api_log_filename_prefix,
                     "/dev/null"))
        return;

    /*
     * Filename is
     * <prefix><n>.log
     *
     * where <n> is the system ID number, and <prefix>
     * is set in stardata->preferences->api_log_filename_prefix
     */
    char filename[STRING_LENGTH*2];
    snprintf(filename,STRING_LENGTH*2-1,"%s%d.log",
             stardata->preferences->api_log_filename_prefix,
             stardata->model.id_number);

    APIDebug("API::binary_c_api_open_log Open API log at %s from id=%d\n",
             filename,stardata->model.id_number);

    binary_c_API_open_logfile(stardata,filename);

    fprintf(stardata->model.api_log_fp,
            "      TIME      M1       M2   K1 K2            SEP    PER    ECC  R1/ROL1 R2/ROL2  TYPE\n");
}


void binary_c_API_function binary_c_API_open_logfile(struct stardata_t * const stardata,
                                                     const char * const filename)
{

    if((stardata->model.api_log_fp = fopen(filename,"w"))==NULL)
    {
        Exit_binary_c(BINARY_C_FILE_OPEN_ERROR,
                      "Could not open binary_c API log file '%s' for writing in binary_c_api_log() (api_log.c)\n",
                      filename);
    }
}


void binary_c_API_function binary_c_API_close_logfile(struct stardata_t * const stardata)
{
    if(stardata != NULL &&
       stardata->model.api_log_fp != NULL &&
       fclose(stardata->model.api_log_fp) != 0)
    {
           Exit_binary_c(BINARY_C_FILE_CLOSE_ERROR,
                         "Could not close binary_c API log file for writing in binary_c_api_log() (api_log.c)\n");

    }
}


#endif// BINARY_C_API
