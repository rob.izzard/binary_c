#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef BINARY_C_API

binary_c_API_function
double Pure_function binary_c_bin_data(const double x,
                                       const double bin_width)
{
    /*
     * Wrapper for bin_data
     */
    return bin_data(x,bin_width);
}

#endif // BINARY_C_API
