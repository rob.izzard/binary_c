#include "../binary_c.h"
No_empty_translation_unit_warning;

#if defined BINARY_C_API &&                     \
    defined STELLAR_POPULATIONS_ENSEMBLE

void binary_c_API_function
binary_c_ensemble_apply_denominator(
    struct cdict_t * hash CDict_maybe_unused,
    struct cdict_entry_t * entry CDict_maybe_unused,
    void * data)
{
    /*
     * wrapper for ensemble_apply_denominator
     */
    ensemble_apply_denominator(hash,entry,data);
}

#endif // STELLAR_POPULATIONS_ENSEMBLE && BINARY_C_API
