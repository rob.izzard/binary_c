#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef BINARY_C_API

 /*
  * binary_c API logging function: log to a string
  */
binary_c_API_function
char * binary_c_API_make_logstring(struct stardata_t * const stardata,
                                   const unsigned int loc)
{

    /* do nothing if the prefix is set to /dev/null */
    if(Strings_equal(stardata->preferences->api_log_filename_prefix,
                     "/dev/null")) return NULL;
    static const char locations[2][10]={"API-pre","API-post"};
    APIDebug("API::binary_c_api_log API LOG to string at t=%g\n",stardata->model.time);
    Boolean skip Maybe_unused;
    return logfile_string(stardata->model.time,
                          stardata->star[0].mass,
                          stardata->star[1].mass,
                          stardata->star[0].stellar_type,
                          stardata->star[0].hybrid_HeCOWD,
                          stardata->star[1].stellar_type,
                          stardata->star[1].hybrid_HeCOWD,
                          stardata->common.orbit.separation,
                          stardata->common.orbit.period,
                          stardata->common.orbit.eccentricity,
                          Is_not_zero(stardata->star[0].roche_radius) ? (stardata->star[0].radius/stardata->star[0].roche_radius) : 0.0,
                          Is_not_zero(stardata->star[1].roche_radius) ? (stardata->star[1].radius/stardata->star[1].roche_radius) : 0.0,
                          locations[loc],
                          stardata,
                          &skip,
                          "");
}
#endif // BINARY_C_API
