#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef BINARY_C_API

int binary_c_API_function binary_c_evolve_for_dt(struct stardata_t * const stardata,
                                                 const double dt)
{
    /*
     * Evolve the stardata struct for time dt (Myr)
     */
    int ret;
    APIDebug("API :: Evolve the stardata at %p for time dt=%g (i.e. from %g to %g) max_evolution_time=%g\n",
             (void*)stardata,
             dt,
             stardata->model.time,
             stardata->model.time+dt,
             stardata->model.max_evolution_time);

    APIDebug("API call log at time %g\n",
           stardata->model.time);

    binary_c_API_log(stardata,API_LOG_PRE);

    check_nans_are_signalled();

    /*
     * Save max_evolution_time so it can be restored later
     */
    const Time maxtimewas = stardata->model.max_evolution_time;

    /*
     * Set it to evolve only for dt Myr
     */
    stardata->model.max_evolution_time = Min(stardata->model.max_evolution_time,
                                             stardata->model.time + dt);

    APIDebug("API :: Set max evolution time %g\n",
           stardata->model.max_evolution_time);

    /*
     * Check that the time is < max_evolution_time, otherwise
     * it makes no sense to evolve. Note, this is not a fatal
     * error by default, you have to enable this by changing 0
     * to 1.
     */
    if(0 && More_or_equal(stardata->model.time,
                          stardata->model.max_evolution_time))
    {
        Exit_binary_c(BINARY_C_ALGORITHM_BRANCH_FAILURE,
                      "API :: incoming stardata has time = %g which is >= the max_evolution_time = %g with dt = %g : this makes no sense.\n",
                      stardata->model.time,
                      stardata->model.max_evolution_time,
                      dt);
    }


    if(stardata->star[0].stellar_type==MASSLESS_REMNANT &&
       stardata->star[1].stellar_type==MASSLESS_REMNANT)
    {
        /*
         * Special case:
         * do nothing if both stars no longer exist
         */
        stardata->model.time += dt;
        ret = 0;
    }
    else
    {
        /*
         * 1) set the longjump to come back here on failure
         *   e.g. an Exit_binary_c call
         * 2) evolve the system for the given time
         */

        if(!setjmp(stardata->batchmode_setjmp_buf))
        {
            APIDebug("API :: call evolve_system (t=%g maxt=%g)\n",
                     stardata->model.time,
                     stardata->model.max_evolution_time);
            ret = evolve_system(stardata);
        }
        else
        {
            /*
             * Error detected
             */
            ret = -1;
        }
    }

    /*
     * Restore max_evolution_time
     */
    stardata->model.max_evolution_time = maxtimewas;

    /* API logging */
    if(!setjmp(stardata->batchmode_setjmp_buf))
    {
        char * buffer, * error_buffer;
        size_t size;
        buffer_info(stardata,&buffer,&size,&error_buffer);

        APIDebug("API :: post iterate t=%g, buffer = %p , buffer_size = %zu\n",
                 stardata->model.time,
                 (void*)buffer,
                 size
            );

        binary_c_API_log(stardata,API_LOG_POST);
    }
    else
    {
        ret = -1; /* error in log calls */
    }

    return ret;
}
#endif // BINARY_C_API
