#ifndef BINARY_C_API_PROTOTYPES_H
#define BINARY_C_API_PROTOTYPES_H

#include "../binary_c_structures.h"

/*
 * Note:
 * binary_c API functions should be declared with
 * the binary_c_API_function attribute so that they
 * are visible to the outside world through the
 * libbinary_c.so shared library.
 */


/*
 * Standard API functions.
 */
int binary_c_API_function binary_c_evolve_for_dt(
    struct libbinary_c_stardata_t * const stardata,
    const double dt);

void binary_c_API_function binary_c_new_system(
    struct libbinary_c_stardata_t ** stardata,
    struct libbinary_c_stardata_t *** previous_stardatas,
    struct libbinary_c_preferences_t ** preferences,
    struct libbinary_c_store_t ** store,
    struct libbinary_c_persistent_data_t ** persistent_data,
    char ** argv,
    int argc);

void binary_c_API_function binary_c_free_persistent_data(
    struct stardata_t * Restrict const stardata
    );

void binary_c_API_function binary_c_free_memory(
    struct stardata_t ** Restrict const stardata,
    const Boolean free_preferences,
    const Boolean free_stardata,
    const Boolean free_store,
    const Boolean free_raw_buffer,
    const Boolean free_persist);

void binary_c_API_function
binary_c_free_previous_stardatas(struct stardata_t * Restrict const stardata);

void binary_c_API_function binary_c_free_store_contents(
    struct libbinary_c_store_t * Restrict const store);
void binary_c_API_function binary_c_free_store(
    struct libbinary_c_store_t ** store_p);


void binary_c_API_function binary_c_buffer_info(
    struct libbinary_c_stardata_t * Restrict const stardata,
    char ** const buffer,
    size_t * size);

void binary_c_API_function binary_c_error_buffer(
    struct stardata_t * Restrict const stardata,
    char ** const error_buffer);

void binary_c_API_function binary_c_buffer_empty_buffer(
    struct stardata_t * Restrict const stardata);

void binary_c_API_function binary_c_help(
    struct stardata_t * Restrict const stardata,
    char * argstring);

void binary_c_API_function binary_c_help_all(
    struct stardata_t * Restrict const stardata);

void binary_c_API_function binary_c_argopts(
    struct stardata_t * Restrict const stardata,
    char * argstring);

binary_c_Event_handler_function
binary_c_API_function
binary_c_generic_event_handler(
    void * const eventp Maybe_unused,
    struct stardata_t * const stardata,
    void * data Maybe_unused);
int binary_c_API_function binary_c_check_reject_flags(
    struct stardata_t * Restrict const stardata
    );

/*
 * API debugging functions
 */

void binary_c_API_function binary_c_API_open_log(
    struct libbinary_c_stardata_t * const stardata);
void binary_c_API_function binary_c_API_log(
    struct libbinary_c_stardata_t * const stardata,
    const unsigned int loc);
void binary_c_API_function binary_c_API_debug_fprintf(
    struct libbinary_c_stardata_t * const stardata,
    const char * const filename ,
    const int fileline,
    const char * const format,
    ...) Gnu_format_args(4,5);
void binary_c_API_function binary_c_API_close_logfile(
    struct stardata_t * stardata);
void binary_c_API_function binary_c_API_open_logfile(
    struct stardata_t * const stardata,
    const char * const filename);

binary_c_API_function
char * binary_c_API_logstring(struct stardata_t * const stardata,
                              const unsigned int loc);

/*
 * API extension functions
 */

void binary_c_API_function binary_c_version(
    struct libbinary_c_stardata_t * Restrict const stardata);


Deprecated_function
void binary_c_API_function binary_c_show_instant_RLOF_period_or_separation(
    struct libbinary_c_stardata_t * const stardata); /* deprecated! */

void binary_c_API_function binary_c_show_instant_RLOF(
    struct libbinary_c_stardata_t * const stardata);

void binary_c_API_function binary_c_initialize_parameters(
    struct libbinary_c_stardata_t * Restrict const stardata);

void binary_c_API_function binary_c_list_args(
    struct libbinary_c_stardata_t * Restrict const stardata);



binary_c_API_function
struct stardata_t * Returns_nonnull Nonnull_all_arguments
binary_c_copy_stardata(
    struct stardata_t  * Restrict const from,
    struct stardata_t  * Restrict const to,
    const unsigned int copy_previous,
    const unsigned int copy_persistent
    );

binary_c_API_function
struct stardata_t * Returns_nonnull Nonnull_all_arguments
binary_c_copy_star(
    struct star_t  * Restrict const from,
    struct star_t  * Restrict const to
    );

binary_c_API_function
void binary_c_catch_events(struct stardata_t * stardata);

binary_c_API_function
void binary_c_events_replace_handler_functions(
    struct stardata_t * const stardata,
    const Event_type type,
    Event_handler_function func(EVENT_HANDLER_ARGS));

void binary_c_API_function
binary_c_erase_events(struct stardata_t * const stardata);

void binary_c_API_function
binary_c_output_to_json(struct stardata_t * const stardata);

void binary_c_API_function
binary_c_ensemble_apply_denominator(struct cdict_t * hash CDict_maybe_unused,
                                    struct cdict_entry_t * entry CDict_maybe_unused,
                                    void * data);

binary_c_API_function
Pure_function double binary_c_bin_data_sigfigs(const double x,
                                               const double bin_width,
                                               const int sigfigs);
binary_c_API_function
double Pure_function binary_c_bin_data(const double x,
                                       const double bin_width);
/*
 * FORTRAN interface
 */
#ifdef BINARY_C_API_FORTRAN


/* input */
#define FORTRAN_IN_VARLIST__                    \
    double *m1,                                 \
        double *mc1,                            \
        double *vrot1,                          \
        double *m2,                             \
        double *mc2,                            \
        double *vrot2,                          \
        double *per,                            \
        double *ecc,                            \
        double *metallicity,                    \
        double *time,                           \
        double *maxtime,                        \
        int *stellar_type1,                     \
        int *stellar_type2                      \

/* output */
#define FORTRAN_OUT_VARLIST__                   \
    double *m1,                                 \
        double *mc1,                            \
        double *r1,                             \
        double *omega1,                         \
        double *logg1,                          \
        double *lum1,                           \
        double *teff1,                          \
        double *xh_1,                           \
        double *xhe_1,                          \
        double *xc_1,                           \
        double *xn_1,                           \
        double *xo_1,                           \
        double *xfe_1,                          \
        double *m2,                             \
        double *mc2,                            \
        int *stellar_type2,                     \
        double *r2,                             \
        double *omega2,                         \
        double *logg2,                          \
        double *lum2,                           \
        double *teff2,                          \
        double *xh_2,                           \
        double *xhe_2,                          \
        double *xc_2,                           \
        double *xn_2,                           \
        double *xo_2,                           \
        double *xfe_2,                          \
        double *per,                            \
        double *ecc,                            \
        double * metallicity Maybe_unused,      \
        double *time,                           \
        double *maxtime,                        \
        int *stellar_type1

void binary_c_API_function binary_c_fortran_api_evolve_for_dt_(
    double * dt,
    struct stardata_t ** s);
void binary_c_API_function binary_c_fortran_api_new_system_(
    struct stardata_t ** s,
    struct stardata_t **** ps,
    struct preferences_t *** p,
    struct store_t ** store,
    struct persistent_data_t ** persistent,
    char * argv);

void binary_c_API_function binary_c_fortran_api_free_memory_(
    struct stardata_t ** s,
    Boolean free_preferences,
    Boolean free_stardata,
    Boolean free_store,
    Boolean free_raw_buffer,
    Boolean free_persist);
void binary_c_API_function binary_c_fortran_api_free_store_(
    struct store_t ** store);

void binary_c_API_function binary_c_fortran_api_stardata_info_(
    FORTRAN_OUT_VARLIST__,                                              \
    struct stardata_t ** s);

void binary_c_API_function binary_c_fortran_api_buffer_info_(
    struct stardata_t ** s,
    char ** const buffer,
    size_t * const size
    );

binary_c_API_function void binary_c_fortran_api_buffer_size_(
    struct stardata_t ** s,
    int * size
    );
binary_c_API_function char *  binary_c_fortran_api_buffer_buffer_(
    struct stardata_t ** s
    );


#endif // BINARY_C_API_FORTRAN

#endif //BINARY_C_API_PROTOTYPES_H
