#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef BINARY_C_API

/*
 * wrapper for show_instant_RLOF
 */

Deprecated_function void binary_c_API_function binary_c_show_instant_RLOF(struct stardata_t * const stardata)
{
    show_instant_RLOF(stardata);
}

#endif
