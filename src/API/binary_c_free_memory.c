#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef BINARY_C_API
void binary_c_API_function binary_c_free_memory(
    struct stardata_t ** Restrict const stardata,
    const Boolean free_preferences,
    const Boolean free_stardata_struct,
    const Boolean free_store,
    const Boolean free_raw_buffer,
    const Boolean free_persistent)
{
    /*
     * API wrapper for the free_memory function
     */
#ifdef ______TESTING
    fprintf(stderr,
            "API Freeing memory, sp = %p, *sp = %p, stardata = %p, free_preferences=%s, free_stardata_struct=%s, free_store=%s, free_raw_buffer=%s, free_persistent=%s\n",
            stardata,
            (stardata!=NULL ? *stardata : NULL),
            stardata,
            Yesno(free_preferences),
            Yesno(free_stardata_struct),
            Yesno(free_store),
            Yesno(free_raw_buffer),
            Yesno(free_persistent));
    fflush(NULL);
#endif
    free_memory(stardata,
                free_preferences,
                free_stardata_struct,
                free_store,
                free_raw_buffer,
                free_persistent);
}
#endif
