#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef BINARY_C_API

void binary_c_API_function binary_c_free_persistent_data(
    struct stardata_t * Restrict const stardata
    )
{
    /*
     * API wrapper for the free_persistent_data function
     */
    free_persistent_data(stardata);
}
#endif
