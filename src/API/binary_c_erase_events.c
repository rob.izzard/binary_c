#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef BINARY_C_API
/*
 * Wrapper for erase_events
 */
void binary_c_API_function
binary_c_erase_events(struct stardata_t * const stardata)
{
    erase_events(stardata);
    return;
}
#endif // BINARY_C_API
