#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef BINARY_C_API

void binary_c_API_function binary_c_version(struct stardata_t * Restrict const stardata)
{
    version(stardata);
}

#endif // BINARY_C_API
