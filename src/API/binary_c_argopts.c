#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef BINARY_C_API

void binary_c_API_function binary_c_argopts(struct stardata_t * Restrict const stardata,
                                            char * argstring)
{
    /*
     * API function to put options associated with the argument in argstring
     * into a buffer.
     */
    if(stardata != NULL)
    {
        int c = 0;
        const int argc = 2;
        char ** argv = Malloc(argc * sizeof(char *));

        if(argv != NULL)
        {
            if(asprintf(&argv[0],"binary_c") >= 0)
            {
                argv[1] = argstring;

                struct tmpstore_t * tmpstore;
                if(stardata->tmpstore == NULL)
                {
                    /* make new tmpstore */
                    tmpstore = New_tmpstore;
                    build_tmpstore_contents(tmpstore);
                    stardata->tmpstore = tmpstore;
                }
                else
                {
                    /* use existing tmpstore */
                    tmpstore = stardata->tmpstore;
                }
                binary_c_argopts_from_arg(
                    stardata->store->argstore->cmd_line_args,
                    stardata,
                    &c,
                    0, // dummy var
                    stardata->store->argstore->arg_count, // arg_count
                    argv,
                    -argc // argc
                    );
                Safe_free(argv[0]);
            }
            Safe_free(argv);
        }
    }
}
#endif//BINARY_C_API
