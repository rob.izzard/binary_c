#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef BINARY_C_API
/*
 * Wrapper for check_reject_flags
 */

int binary_c_API_function binary_c_check_reject_flags(
    struct stardata_t * Restrict const stardata
    )
{
    return check_reject_flags(stardata);
}
#endif // BINARY_C_API
