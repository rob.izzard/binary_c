#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef BINARY_C_API

void binary_c_API_function
binary_c_new_system(struct stardata_t ** stardata,
                    struct stardata_t *** previous_stardatas,
                    struct preferences_t ** preferences,
                    struct store_t ** store,
                    struct persistent_data_t ** persistent_data,
                    char ** argv,
                    int argc)
{
    /*
     * API function to make a new system
     */
    new_system(stardata,
               previous_stardatas,
               preferences,
               store,
               persistent_data,
               argv,
               argc);
}

#endif // BINARY_C_API
