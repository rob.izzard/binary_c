#pragma once
#ifndef UNITS_SI_H
#define UNITS_SI_H

/*
 * SI versions of some constants
 */
#define SI_length(X) ((X)*1e-2) /* cm -> m */
#define SI_speed(X) ((X)*1e-2)
#define SI_acceleration(X) ((X)*1e-2)
#define SI_area(X) (SI_length(SI_length(X)))
#define SI_mass(X) ((X)*1e-3) /* g -> kg */
#define SI_energy(X) ((X)*1e-7) /* erg -> J */
#define SI_power(X) ((X)*1e-7)
#define SI_luminosity(X) SI_power(X)
#define SI_pressure(X) ((X)*0.1) /* bareye (ba) -> Pa */
#define SI_density(X) ((X)*1e3)
#define SI_force(X) ((X)*1e-5) /* dyne -> Newton */
#define SI_electric_charge(X) ((X)/(SI_speed(SPEED_OF_LIGHT)*10.0)) /* franklin/statcoulomb -> Coulomb */
#define SI_magnetic_flux_density(X) ((X)*1e-4) /* Gauss -> Tesla */
#define cgs_energy(X) (1e7*(X))

#define SPEED_OF_LIGHT_SI SI_speed(SPEED_OF_LIGHT)
#define EPSILON0_SI (8.854187817e-12)
#define MU0_SI (1.0/(Pow2(SPEED_OF_LIGHT_SI)*EPSILON0_SI))
#define GRAVITATIONAL_CONSTANT_SI (GRAVITATIONAL_CONSTANT * 1e-3)
#define STEFAN_BOLTZMANN_CONSTANT_SI (STEFAN_BOLTZMANN_CONSTANT * 1e-3)
#define PLANCK_CONSTANT_SI (PLANCK_CONSTANT * 1e-7)
#define PLANCK_CONSTANT_BAR_SI (PLANCK_CONSTANT_BAR * 1e-7)
#define BOLTZMANN_CONSTANT_SI (BOLTZMANN_CONSTANT * 1e-7)
#define M_SUN_SI SI_mass(M_SUN)
#define R_SUN_SI SI_length(R_SUN)
#define L_SUN_SI SI_luminosity(L_SUN)
#define M_MERCURY_SI SI_mass(M_MERCURY)
#define M_VENUS_SI SI_mass(M_VENUS)
#define M_EARTH_SI SI_mass(M_EARTH)
#define M_MARS_SI SI_mass(M_MARS)
#define M_JUPITER_SI SI_mass(M_JUPITER)
#define M_SATURN_SI SI_mass(M_SATURN)
#define M_URANUS_SI SI_mass(M_URANUS)
#define M_NEPTUNE_SI SI_mass(M_NEPTUNE)
#define M_PLUTO_SI SI_mass(M_PLUTO)
#define A_MERCURY_SI SI_mass(A_MERCURY)
#define A_VENUS_SI SI_mass(A_VENUS)
#define A_EARTH_SI SI_mass(A_EARTH)
#define A_MARS_SI SI_mass(A_MARS)
#define A_JUPITER_SI SI_mass(A_JUPITER)
#define A_SATURN_SI SI_mass(A_SATURN)
#define A_URANUS_SI SI_mass(A_URANUS)
#define A_NEPTUNE_SI SI_mass(A_NEPTUNE)
#define A_PLUTO_SI SI_mass(A_PLUTO)
#define R_EARTH_SI SI_length(R_EARTH)
#define PARSEC_SI SI_length(PARSEC)
#define KILOPARSEC_SI SI_length(PARSEC)
#define MEGAPARSEC_SI SI_length(MEGAPARSEC)
#define ASTRONOMICAL_UNIT_SI SI_length(ASTRONOMICAL_UNIT)
#define M_PROTON_SI SI_mass(M_PROTON)
#define M_ELECTRON_SI SI_mass(M_ELECTRON)
#define M_NEUTRON_SI SI_mass(M_NEUTRON)
#define BOLOMETRIC_L0_SI SI_power(BOLOMETRIC_L0)
#define SIGMA_THOMPSON_SI SI_area(SIGMA_THOMPSON)
#define AMU_KG SI_mass(AMU_GRAMS)
#define RYDBERG_ENERGY_SI (M_ELECTRON_SI * Pow4(ELECTRON_CHARGE_SI) / (8.0 * Pow2(EPSILON0_SI * PLANCK_CONSTANT_SI)))
#define ELECTRON_CHARGE_SI SI_electric_charge(ELECTRON_CHARGE)
#define ELECTRON_VOLT_SI SI_energy(ELECTRON_VOLT)
#define MEGA_ELECTRON_VOLT_SI SI_energy(MEGA_ELECTRON_VOLT)
#define FINE_STRUCTURE_CONSTANT_SI (Pow2(ELECTRON_CHARGE_SI)/(2.0 * EPSILON0_SI * SPEED_OF_LIGHT_SI * PLANCK_CONSTANT_SI))
#define GAS_CONSTANT_SI (BOLTZMANN_CONSTANT_SI*AVOGADRO_CONSTANT)

#endif // UNITS_SI_H
