#pragma once
#ifndef UNITS_PROTOTYPES_H
#define UNITS_PROTOTYPES_H

void string_to_code_units(struct stardata_t * const Restrict stardata,
                          char * const Restrict s,
                          double * const Restrict value,
                          struct unit_t * const Restrict u,
                          int * const Restrict errnum);

void string_to_cgs_units(struct stardata_t * stardata Maybe_unused,
                         char * const Restrict s,
                         double * const Restrict value,
                         struct unit_t * const Restrict u,
                         int * const Restrict errnum);

#endif // UNITS_PROTOTYPES_H
