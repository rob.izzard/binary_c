/* Header file for the Binary Star Evolution Model */
#pragma once
#ifndef STELLAR_PARAMETERS_H
#define STELLAR_PARAMETERS_H
/*
 * The binary_c stellar population nucleosynthesis framework.
 *
 * Copyright Robert Izzard 2000-2025
 *
 * Contact rob.izzard@gmail.com
 *
 * Code repository:
 * https://gitlab.com/binary_c
 *
 * Documentation is in doc/
 * Code is in src/
 *
 * To build, run
 *
 * ---
 *
 * meson setup builddir
 * cd builddir
 * ninja binary_c_install
 *
 * ---
 *
 * Please see the files README, LICENCE and CHANGES,
 * and the doc/ directory for documentation which is mirrored
 * at https://binary_c.gitlab.io
 *
 **********************
 *
 * Purpose:
 *
 * This file contains various parameters for identifying types of stars in the
 * log of the binary star evolution model.
 */

/******************************************************************************/




/* Wolf-Rayet stars */
/*
 * There are three subclasses of WR stars: WN, WC and WO (in order of
 * increasing temperature)
 */
/*
 * These data are based on figures from Lynnette Dray which she seems to have
 * been compiled from Hamann and Koesterke 1995 and Hamann and Koesterke 1998
 */

/** Observational data **/

/* Progenitor Masses (in solar mass units) - Van der Hucht 2001 */
#define WR_OBS_WN_MIN 2.3
#define WR_OBS_WN_MAX 55.0
#define WR_OBS_WC_MIN 9.0
#define WR_OBS_WC_MAX 16.0
/* There are no estimates for WO stars since only 4 have been observed */

/* Log T_eff (Kelvin, presumably) */
#define WR_OBS_LOG_TEFF_WN_MIN 4.5
#define WR_OBS_LOG_TEFF_WN_MAX 5.15
#define WR_OBS_LOG_TEFF_WC_MIN 4.7
#define WR_OBS_LOG_TEFF_WC_MAX 5.0

/* Mass loss rates (Log Mdot in solar masses per year) */
#define WR_OBS_LOG_MDOT_WN_MIN -5.2
#define WR_OBS_LOG_MDOT_WN_MAX -3.7
#define WR_OBS_LOG_MDOT_WC_MIN -4.8
#define WR_OBS_LOG_MDOT_WC_MAX -3.9

/*
 * Here is how to differentiate between WN and WC Wolf-Rayet stars : log
 * (L/L_sun) (i.e. solar units)
 */
#define WR_OBS_LOG_L_WN_MIN 5.4
#define WR_OBS_LOG_L_WN_MAX 6.1
#define WR_OBS_LOG_L_WC_MIN 4.7
#define WR_OBS_LOG_L_WC_MAX 5.5

/** Theoretical Data **/
/*
 * These are the same mass-loss rates as used in astro-ph/0012396 "Winds from
 * massive stars: implications for the afterglows of gamma ray bursts)"
 * Ramirez-Ruiz, Dray, Madau and Tout (currently unpublished). Lynnette
 * describes them as for non-rotating single stars at solar metallicity. Values
 * are for the sequence O star -> WN -> WC -> WO with some WR stars missing out
 * WC,WO or both.
 */

/* Log Effective temperature (K) */
#define WR_LOG_TEFF_WN_MIN 4.0
#define WR_LOG_TEFF_WN_MAX 5.1
#define WR_LOG_TEFF_WC_MIN 4.65
#define WR_LOG_TEFF_WC_MAX 5.2
#define WR_LOG_TEFF_WO_MIN 5.0
#define WR_LOG_TEFF_WO_MAX 5.3

/* Log (L/L_sun) */
#define WR_LOG_L_WN_MIN 4.6
#define WR_LOG_L_WN_MAX 6.25
#define WR_LOG_L_WC_MIN 4.3
#define WR_LOG_L_WC_MAX 5.8
#define WR_LOG_L_WO_MIN 5.45
#define WR_LOG_L_WO_MAX 5.7

/* MS masses of stars which go on to become WR stars (solar units) */
#define WR_MIN_MS_MASS 26.0
/*
 * The maximum is whatever the maximum of the IMF is (80.0 for IMF of
 * Kroupa/Tout/Gilmore 1993, but take your pick). 1000.0 should cover the high
 * end :)
 */
#define WR_MAX_MS_MASS 1000.0

/* Masses at the start of WR PHASE */
#define WR_MASS_WN_START_MIN 5.0
#define WR_MASS_WN_START_MAX 89.0
#define WR_MASS_WC_START_MIN 4.0
#define WR_MASS_WC_START_MAX 25.0
#define WR_MASS_WO_START_MIN 6.0
#define WR_MASS_WO_START_MAX 13.0

/* Mass loss rates (Log Mdot, solar masses per year) */
#define WR_LOG_MDOT_WN_MIN -5.5
#define WR_LOG_MDOT_WN_MAX -4.1
#define WR_LOG_MDOT_WC_MIN -5.9
#define WR_LOG_MDOT_WC_MAX -3.7
#define WR_LOG_MDOT_WO_MIN -5.4
#define WR_LOG_MDOT_WO_MAX -4.4



/******************************************************************************
 * Some broad outlines for _any_ WR star based on the above theoretical data
 */

#define WR_LOG_MASS_LOSS_MIN -5.9
#define WR_LOG_MASS_LOSS_MAX -3.7
#define WR_LOG_LUM_MIN 4.3
#define WR_LOG_LUM_MAX 6.25
#define WR_LOG_TE_MIN 4
#define WR_LOG_TE_MAX 5.3

/*
 * See also the MAEDER_MEYNET_2000_EXTRA_ROTATIONAL_MASS_LOSS macro in
 * binary_c_parameters.h - if this is defined then there is extra mass loss due
 * to rotational effects.
 */



#endif /* STELLAR_PARAMETERS_H */
