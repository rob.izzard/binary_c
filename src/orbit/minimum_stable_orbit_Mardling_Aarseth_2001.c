#include "../binary_c.h"

double minimum_stable_orbit_Mardling_Aarseth_2001(const double eccentricity,
                                                 const double inclination,
                                                 const double q)
{
    /*
     * Mardling and Aarseth (2001)
     * MNRAS 321, 398
     *
     * Minimum stable semi-major axis
     * Their Eq. 90
     */
    return
        2.8/(1.0 - eccentricity)
        *
        (1.0 - 0.3 * inclination / PI)
        *
        pow( (1.0 + q) *
             (1.0 + eccentricity) /
             sqrt(1.0 - eccentricity),
             0.4)
        ;
}
