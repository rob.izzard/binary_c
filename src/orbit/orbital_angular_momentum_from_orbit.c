#include "../binary_c.h"
No_empty_translation_unit_warning;


/*
 * Given M1, M2, the orbital period, separation and eccentricity,
 * calculate the orbital angular momentum.
 */

double orbital_angular_momentum_from_orbit(const struct orbit_t * const orbit,
                                           const double m1,
                                           const double m2)
{
    return
        m1 * m2 / (m1+m2) *
        sqrt(1.0 - Pow2(orbit->eccentricity)) *
        Pow2(orbit->separation) *
        TWOPI/orbit->period;
}
