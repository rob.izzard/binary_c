#include "../binary_c.h"
No_empty_translation_unit_warning;


double Pure_function orbital_velocity_squared(struct stardata_t * Restrict const stardata)
{
    /* return the square of the orbital velocity (km^2/s^2) */
    double v2;
    if(Is_not_zero(stardata->common.orbit.separation))
    {
        v2 = fabs
            (1e-10 * GRAVITATIONAL_CONSTANT *
             system_gravitational_mass(stardata)*M_SUN/
             (stardata->common.orbit.separation*R_SUN));
    }
    else
    {
        v2 = 0.0;
    }
    return v2;
}

double Pure_function orbital_velocity_squared_cgs(struct stardata_t * Restrict const stardata)
{
    return
        orbital_velocity_squared(stardata) * 1e10;
}
