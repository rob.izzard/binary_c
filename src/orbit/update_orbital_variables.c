#include "../binary_c.h"
No_empty_translation_unit_warning;


void update_orbital_variables(struct stardata_t * Restrict const stardata,
                              struct orbit_t * Restrict const orbit,
                              struct star_t * Restrict const star1,
                              struct star_t * Restrict const star2)
{
    /*
     * Given an orbital angular momentum, calculate
     * the orbital separation, period and angular frequency,
     * and update Roche radii
     */
    const double total_mass = star1->mass + star2->mass;
    Dprint("Update orbit J = %g, e = %g\n",
           orbit->angular_momentum,
           orbit->eccentricity);

    if(unlikely(More_or_equal(orbit->eccentricity,1.0)||
                orbit->eccentricity<-TINY))
    {
        orbit->separation = 0.0;
    }
    else
    {
        const double denom = Pow2(stardata->star[0].mass*stardata->star[1].mass*TWOPI)*
            (Pow3(AU_IN_SOLAR_RADII)*(1.0 - Pow2(stardata->common.orbit.eccentricity)));
        orbit->separation = denom > TINY ? (total_mass*Pow2(stardata->common.orbit.angular_momentum)/denom) : 0.0;
    }


    Dprint("hence sep = %30.22e from Mtot=%30.22e * J^2=%30.22e / (M1*M2=%30.22e * (1-e^2)=%30.22e): da/dt = %g da/dt/a=%g Binary ? %d\n",
           orbit->separation,
           total_mass,
           Pow2(stardata->common.orbit.eccentricity),
           Pow2(stardata->star[0].mass*stardata->star[1].mass*TWOPI),
           (Pow3(AU_IN_SOLAR_RADII)*(1.0-Pow2(stardata->common.orbit.eccentricity))),
           stardata->model.derivative[DERIVATIVE_ORBIT_SEMI_MAJOR_AXIS],
           Is_not_zero(stardata->common.orbit.separation) ? stardata->model.derivative[DERIVATIVE_ORBIT_SEMI_MAJOR_AXIS]/stardata->common.orbit.separation : 0.0,
           System_is_binary
        );

    /*
     * Kepler's law converts the orbital separation to orbital period
     */
    update_orbital_period_and_angular_frequency(orbit,star1,star2);

    determine_roche_lobe_radii(stardata,orbit);
    Dprint("post-roche\n");
}
