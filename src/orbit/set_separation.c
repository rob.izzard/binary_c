#include "../binary_c.h"
No_empty_translation_unit_warning;


/*
 * Set the orbital separation, keeping masses
 * constant, and update the orbital period and
 * angular momentum.
 */
void set_separation(struct stardata_t * const stardata,
                    const double separation)
{
    stardata->common.orbit.separation = separation;
    update_orbital_period_and_angular_frequency(
        &stardata->common.orbit,
        &stardata->star[0],
        &stardata->star[1]);
    stardata->common.orbit.angular_momentum = orbital_angular_momentum(stardata);
}
