#include "../binary_c.h"
No_empty_translation_unit_warning;


/*
 * Given M1, M2, the orbital period, separation and eccentricity,
 * calculate the orbital angular momentum.
 */

double orbital_angular_momentum(struct stardata_t * const stardata)
{
    return
        stardata->star[0].mass*stardata->star[1].mass / Total_mass *
        sqrt(1.0 - Pow2(stardata->common.orbit.eccentricity)) *
        Pow2(stardata->common.orbit.separation) *
        TWOPI/stardata->common.orbit.period;
}
