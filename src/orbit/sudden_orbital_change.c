#include "../binary_c.h"
No_empty_translation_unit_warning;

/*
 * The binary_c stellar population nucleosynthesis framework.
 *
 * Copyright Robert Izzard 2000-2025
 *
 * Contact rob.izzard@gmail.com
 *
 * Code repository:
 * https://gitlab.com/binary_c
 *
 * Documentation is in doc/
 * Code is in src/
 *
 * To build, run
 *
 * ---
 *
 * meson setup builddir
 * cd builddir
 * ninja binary_c_install
 *
 * ---
 *
 * Please see the files README, LICENCE and CHANGES,
 * and the doc/ directory for documentation which is mirrored
 * at https://binary_c.gitlab.io
 *
 **********************
 *
 * Subroutine:
 *     sudden_orbital_change
 *
 * Purpose:
 *     Applies a sudden orbital change caused by a mass change
 *     to the system.
 *     Note: this function does not change the masses of the stars
 *           in the system, just sets the appropriate orbital parameters
 *           in the orbit_out struct.
 *
 * Arguments:
 *     struct stardata_t stardata - the binary system
 *     struct orbit_t orbit_in - the initial orbit. If NULL, use stardata's orbit.
 *     struct orbit_t orbit_out - the changed orbit. Cannot be NULL.
 *     double dm1, dm2 - the mass change of stars 0 and 1 (Msun). Note these take the
 *                true sign, so should be negative for mass loss.
 *     double phase_angle, mean_anomaly, eccentric_anomaly  -
 *              Variables to define orbital location at which the sudden mass loss
 *              occurs.
 *              These are ignored if -1.0, so you should either choose one to be >=0
 *              or set them all to -1.0 in which case a random location in the orbit
 *              is chosen.
 *
 *
 * Returns:
 *     nothing.
 *
 **********************
 */


void sudden_orbital_change(struct stardata_t * const stardata,
                           struct orbit_t * orbit_in,
                           struct orbit_t * const orbit_out,
                           const double dm1,
                           const double dm2,
                           const double phase_angle,
                           const double mean_anomaly,
                           const double eccentric_anomaly)
{

    /*
     * If orbit_in is NULL, use stardata's orbit
     */
    if(orbit_in==NULL)
    {
        orbit_in = &stardata->common.orbit;
    }

    /*
     * orbit_out should start as orbit_in
     */
    memcpy(orbit_out,orbit_in,sizeof(struct orbit_t));

    /*
     * Handy constants
     */
    const double e2 = 1.0 - Pow2(orbit_in->eccentricity);
    const double M = Sum_of_stars(mass);
    const double dm = -dm1 - dm2;
    const double f = dm / M;
    const double f1 = 1.0 - f;

    /*
     * Choose how to define the moment at which mass changes
     * hence r at that moment.
     */
    double r;
    if(phase_angle >= 0.0)
    {
        /*
         * Use the phase angle
         */
        r = orbit_in->separation * e2 /
            (1.0 + orbit_in->eccentricity * cos(phase_angle));
    }
    else if(mean_anomaly >= 0.0)
    {
        /*
         * We are given a mean anomaly: convert to eccentric anomaly
         * and use that.
         */
        const double E =
            eccentric_anomaly_from_mean_anomaly(stardata,
                                                orbit_in->eccentricity,
                                                mean_anomaly,
                                                0.1,
                                                MEAN_ANOMALY_TOLERANCE,
                                                MEAN_ANOMALY_MAX_ITERATIONS);
        r = orbit_in->separation * (1.0 - orbit_in->eccentricity * cos(E));
    }
    else if(eccentric_anomaly >= 0.0)
    {
        /*
         * Use the eccentric anomaly.
         */
        r = orbit_in->separation * (1.0 - orbit_in->eccentricity * cos(eccentric_anomaly));
    }
    else
    {
        /*
         * No location is given, choose randomly
         */
        const double Mam = Max(TINY, (double) random_number(stardata,NULL) * TWOPI);
        const double E =
            eccentric_anomaly_from_mean_anomaly(stardata,
                                                orbit_in->eccentricity,
                                                Mam,
                                                0.1,
                                                MEAN_ANOMALY_TOLERANCE,
                                                MEAN_ANOMALY_MAX_ITERATIONS);
        r = orbit_in->separation * (1.0 - orbit_in->eccentricity * cos(E));
    }

    const double f2 = (0.5 - orbit_in->separation/r * f);

    /*
     * Equations from Hills 1983 based on
     * See https://arxiv.org/pdf/1111.4100.pdf Eqs. 12 and 13
     */
    orbit_out->separation =
        orbit_in->separation * 0.5 *
        f1 / f2;
    orbit_out->eccentricity =
        sqrt(1.0 - e2 * 2.0 * f2 / Pow2(f1) );

    /*
     * Update the orbital period, angular frequency and angular momentum
     *
     * Note: the easiest way to do this is to update the masses in stardata
     *       then set them back at the end.
     */
    update_mass(stardata,&stardata->star[0],dm1);
    update_mass(stardata,&stardata->star[1],dm2);
    update_orbital_period_and_angular_frequency(orbit_out,
                                                &stardata->star[0],
                                                &stardata->star[1]);
    orbit_out->angular_momentum = orbital_angular_momentum_from_orbit(orbit_out,
                                                                      stardata->star[0].mass,
                                                                      stardata->star[1].mass);
    update_mass(stardata,&stardata->star[0],-dm1);
    update_mass(stardata,&stardata->star[1],-dm2);

}
