#include "../binary_c.h"
No_empty_translation_unit_warning;
/*
 * The binary_c stellar population nucleosynthesis framework.
 *
 * Copyright Robert Izzard 2000-2025
 *
 * Contact rob.izzard@gmail.com
 *
 * Code repository:
 * https://gitlab.com/binary_c
 *
 * Documentation is in doc/
 * Code is in src/
 *
 * To build, run
 *
 * ---
 *
 * meson setup builddir
 * cd builddir
 * ninja binary_c_install
 *
 * ---
 *
 * Please see the files README, LICENCE and CHANGES,
 * and the doc/ directory for documentation which is mirrored
 * at https://binary_c.gitlab.io
 *
 **********************
 *
 * Subroutine:
 *   eccentric_anomaly_from_mean_anomaly
 *
 * Purpose:
 *   calculates the eccentric anomaly given the mean anomaly and eccentricity
 *
 * Arguments:
 *   stardata : binary system stardata struct, used for logging only
 *   eccentricity : orbital eccentricity
 *   mean_anomaly : mean anomaly (can be randomly chosen)
 *   f : initial solution factor, usually we use f=0.1
 *
 * Returns:
 *   the eccentric anomaly
 *
 **********************
 */
double eccentric_anomaly_from_mean_anomaly(struct stardata_t * const stardata,
                                           const double eccentricity,
                                           const double mean_anomaly,
                                           const double f,
                                           const double tol,
                                           const int max_iterations)
{
    double eccentric_anomaly = mean_anomaly;

    /*
     * Newton-Raphson search for eccentric anomaly,
     * solves Eq. A4 in Hurley's PhD thesis
     *
     * f is usually 0.1
     */
    double dif = f * (eccentric_anomaly - eccentricity * sin(eccentric_anomaly) - mean_anomaly);
    int newton_count = 0;

    while(Abs_more_than(dif/mean_anomaly,tol))
    {
        if(newton_count++ > max_iterations)
        {
            Exit_binary_c(
                BINARY_C_NR_FAILED_IN_KICK,
                "Newton-Raphson in eccentric_anomaly_from_mean_anomaly() failed to converge after %d iterations: consider setting the initial guess to be smaller?\n",
                newton_count-1);
        }
        eccentric_anomaly -= dif / (1.0 - eccentricity * cos(eccentric_anomaly));
        dif = eccentric_anomaly - eccentricity * sin(eccentric_anomaly) - mean_anomaly;
    }

    return eccentric_anomaly;
}
