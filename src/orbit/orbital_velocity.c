#include "../binary_c.h"
No_empty_translation_unit_warning;


double Pure_function orbital_velocity(struct stardata_t * Restrict const stardata)
{
    /* return the orbital velocity (km/s) */
    return sqrt(orbital_velocity_squared(stardata));
}

double Pure_function orbital_velocity_cgs(struct stardata_t * Restrict const stardata)
{
    /* return the orbital velocity (cm/s) */
    return sqrt(orbital_velocity_squared_cgs(stardata));
}
