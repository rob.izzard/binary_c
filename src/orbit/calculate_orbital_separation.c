/* calculate_orbital_separation.c */

#include "../binary_c.h"
No_empty_translation_unit_warning;


double calculate_orbital_separation (struct stardata_t * Restrict const stardata)
{
    /*
     * Given the orbital period (years), M1 and M2 (Msun),
     * calculate the orbital separation using Kepler's law.
     */

    const double sep = (double)AU_IN_SOLAR_RADII *
        cbrt(Pow2(stardata->common.orbit.period)*Total_mass);

    Dprint("Calculating stellar separation: %g (M1+M2=%g Msun P=%g years using AU/RSUN=%g)\n",
           sep,
           Total_mass,
           stardata->common.orbit.period,
           AU_IN_SOLAR_RADII
        );

    return sep;
}
