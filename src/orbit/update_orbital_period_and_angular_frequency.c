#include "../binary_c.h"
No_empty_translation_unit_warning;


/*
 * update the orbital period and angular frequency
 */

void update_orbital_period_and_angular_frequency(struct orbit_t * Restrict const orbit,
                                                 struct star_t * Restrict const star1,
                                                 struct star_t * Restrict const star2)
{
    /*
     * Calculate the orbital period, in years, give the
     * orbit and stars structs
     */
    const double aa = orbit->separation/AU_IN_SOLAR_RADII;
    orbit->period = aa * sqrt(aa/(star1->mass + star2->mass));

    /*
     * Hence the orbital angular frequency
     */
    if(Is_zero(orbit->period))
    {
        orbit->angular_frequency = 
            UNDEFINED_ORBITAL_ANGULAR_FREQUENCY;
    }
    else
    {
        orbit->angular_frequency =
            TWOPI/orbit->period;
    }

}
