#pragma once
#ifndef BINARY_C_PHYSICAL_CONSTANTS
#define BINARY_C_PHYSICAL_CONSTANTS

/*
 * The binary_c stellar population nucleosynthesis framework.
 *
 * Copyright Robert Izzard 2000-2025
 *
 * Contact rob.izzard@gmail.com
 *
 * Code repository:
 * https://gitlab.com/binary_c
 *
 * Documentation is in doc/
 * Code is in src/
 *
 * To build, run
 *
 * ---
 *
 * meson setup builddir
 * cd builddir
 * ninja binary_c_install
 *
 * ---
 *
 * Please see the files README, LICENCE and CHANGES,
 * and the doc/ directory for documentation which is mirrored
 * at https://binary_c.gitlab.io
 *
 **********************
 *
 * Purpose:
 *
 * This file contains physical constants.  Note that
 * constants should be derived whenever possible: for this reason
 * only fundamental constants and a few measured constants are
 * given as numbers, the rest are combinations of the previously
 * defined constants.
 *
 * See also units/units.h
 */
#include "binary_c_maths.h"

/*
 ************************************************************
 * Fundamental constants
 ************************************************************
 */

/* Speed of Light cm s-1 */
#define SPEED_OF_LIGHT (2.99792458E10)

/* Gravitational constant in CGS units  */
//#define GRAVITATIONAL_CONSTANT (6.67259E-8)
// updated from http://physics.nist.gov/cgi-bin/cuu/Value?bg
// using CODATA 2014 recommended values
#define GRAVITATIONAL_CONSTANT (6.67408E-8)

/* Planck's constant : h */
#define PLANCK_CONSTANT (6.6260755e-27)

/* Boltzmann constant in erg/K */
#define BOLTZMANN_CONSTANT (1.38064852e-16)

/* electron charge (cgs, i.e. statcoulombs or franklin) */
#define ELECTRON_CHARGE (4.80320425e-10)

/* Avogadro's Constant */
#define AVOGADRO_CONSTANT (6.0221367e23)

/*
 ************************************************************
 * Other constants
 * see e.g.
 * http://asa.usno.navy.mil/static/files/2016/Astronomical_Constants_2016.pdf
 ************************************************************
 */

/*
 * Solar radius in CGS units (cm)
 * from Haberreiter, M.; Schmutz, W.; Kosovichev, A. G. (ApJ 2008 675,53)
 */
#define R_SUN (6.9566E10)

/*
 * astronomical unit in cm (IAU definition) :
 * NB this also defines the length of a year
 */
#define ASTRONOMICAL_UNIT (14959787070000.0)

/* one day in seconds */
#define DAY_LENGTH_IN_SECONDS (86400.0)

/* one hour in seconds */
#define HOUR_LENGTH_IN_SECONDS (3600.0)

/* Solar luminosity in CGS units (erg s^-1) */
#define L_SUN (3.8515E33)

/* Solar luminosity in neutrinos (Farag et al. ApJ 893, 133) */
#define L_NU_SUN (9.1795e31)

/* Solar mass in CGS units (g) */
#define M_SUN (1.9891E33)

/* Solar angular momentum (cgs) https://arxiv.org/abs/1112.4168 */
#define J_SPIN_SUN (1.92e48)

/* Planet masses (cgs) https://nssdc.gsfc.nasa.gov/planetary/factsheet/ */
#define M_MERCURY (3.30e26)
#define M_VENUS (4.87e27)
#define M_EARTH  (5.9722e27)
#define M_MARS (6.42e26)
#define M_JUPITER (1.898e30)
#define M_SATURN (5.68e29)
#define M_URANUS (8.68e28)
#define M_NEPTUNE (1.02e29)
#define M_PLUTO (1.27e25)
#define M_MOON (7.35e25)
#define M_IO (8.93e25)
#define M_EUROPA (4.80e25)
#define M_GANYMEDE (1.48e26)
#define M_CALLISTO (1.08e26)

/* Planet radii (cm) https://nssdc.gsfc.nasa.gov/planetary/factsheet/ */
#define R_MERCURY (0.5*4879.0*1e5)
#define R_VENUS (0.5*12104.0*1e5)
#define R_EARTH (6.378e8)
#define R_MARS (0.5*6792.0*1e5)
#define R_JUPITER (7.1492e9)
#define R_SATURN (0.5*120536.0*1e5)
#define R_URANUS (0.5*51118.0*1e5)
#define R_NEPTUNE (0.5*49528.0*1e5)
#define R_PLUTO (0.5*2370*1e5)

/* Distances to the Sun */
#define A_MERCURY (57.9*1e11)
#define A_VENUS (108.2*1e11)
#define A_EARTH (ASTRONOMICAL_UNIT)
#define A_MARS (227.9*1e11)
#define A_JUPITER (778.6*1e11)
#define A_SATURN (1433.5*1e11)
#define A_URANUS (2872.5*1e11)
#define A_NEPTUNE (4495.1*1e11)
#define A_PLUTO (5906.4*1e11)

/* Average distance of Moon orbit around the Earth */
#define A_MOON (0.384*1e11)

/* Jupiter orbital angular momentum (cgs) */
#define J_ORBITAL_JUPITER (1.9e50)

/* Jupiter spin angular momomentum (cgs) */
#define J_SPIN_JUPITER (6.9e39)

/* Earth orbital angular momentum (around the Sun, cgs) (~2.7e47) */
#define J_ORBITAL_EARTH (M_EARTH*sqrt(A_EARTH*GRAVITATIONAL_CONSTANT*M_SUN))
//#define J_ORBITAL_EARTH (2.661466854660284433043434882887e+47)

/* Earth spin angular momentum, cgs */
#define J_SPIN_EARTH (7.1e34)

/* Proton mass (g) */
#define M_PROTON (1.672621e-24)

/* Electron mass (g) */
#define M_ELECTRON (9.109383e-28)

/* Neutron mass (g) */
#define M_NEUTRON (1.674929e-24)

/* apparent magnitude of the Sun */
#define SOLAR_APPARENT_MAGNITUDE (-26.73)

/* Cosmic microwave background temperature */
#define CMB_TEMPERATURE (2.72548)

/*
 * Zero point of the bolometric magnitude scale,
 * in L_SUN, as defined in resolution B2 of the IAU
 */
#define BOLOMETRIC_L0 (3.0128e35)

/*
 ************************************************************
 * Derived constants
 ************************************************************
 */

/* electron volt / erg ~ 1.602176565e-12 */
#define ELECTRON_VOLT (ELECTRON_CHARGE * 1e8 / SPEED_OF_LIGHT)

#define MEGA_ELECTRON_VOLT (1e6 * ELECTRON_VOLT)

/* atomic mass units in g (1.660538921e-24) */
#define AMU_GRAMS (1.0/AVOGADRO_CONSTANT)

/* and in MeV */
#define AMU_MEV (AMU_GRAMS*SPEED_OF_LIGHT*SPEED_OF_LIGHT/MEGA_ELECTRON_VOLT)

/* Rydberg energy constant (erg) */
#define RYDBERG_ENERGY (M_ELECTRON * Pow4(ELECTRON_CHARGE) / (2.0 * Pow2(PLANCK_CONSTANT_BAR)))

/* one year in seconds */
//#define YEAR_LENGTH_IN_SECONDS (sqrt( Pow2(TWOPI) * Pow3(ASTRONOMICAL_UNIT) /(GRAVITATIONAL_CONSTANT * M_SUN)))

/* This number is the above pre-computed */
#define YEAR_LENGTH_IN_SECONDS (3.155324093699018657207489013672e+07)

/* year in days */
#define YEAR_LENGTH_IN_DAYS ((YEAR_LENGTH_IN_SECONDS)/(DAY_LENGTH_IN_SECONDS))

/* year in hours */
#define YEAR_LENGTH_IN_HOURS ((YEAR_LENGTH_IN_SECONDS)/(HOUR_LENGTH_IN_SECONDS))

/* day in hours */
#define DAY_LENGTH_IN_HOURS (DAY_LENGTH_IN_SECONDS/HOUR_LENGTH_IN_SECONDS)

/* day in years */
#define DAY_LENGTH_IN_YEARS (1.0/YEAR_LENGTH_IN_DAYS)

/* Rsun in km */
#define R_SUN_KM (R_SUN*1e-5)

/* solar radii in one AU = 1AU / Rsun */
//#define AU_IN_SOLAR_RADII (214.95)
#define AU_IN_SOLAR_RADII (ASTRONOMICAL_UNIT/R_SUN)

/* Planck's constant : hbar */
#define PLANCK_CONSTANT_BAR (PLANCK_CONSTANT/TWOPI)

/*
 * Conversion factor to ensure velocities are in km/s using mass and
 * radius in solar units.
 */
//#define GMRKM 1.906125E5
#define GMRKM (1e-10*GRAVITATIONAL_CONSTANT*M_SUN/R_SUN)

/*
 * Conversion factor to ensure angular velocity in year^-1
 * when velocities are in km/s and radii in solar units.
 * is ~45.5
 */
#define OMEGA_FROM_VKM (YEAR_LENGTH_IN_SECONDS*1e5/R_SUN)

/* Ideal gas constant */
#define GAS_CONSTANT (BOLTZMANN_CONSTANT*AVOGADRO_CONSTANT)

/* fine structure constant ~ 1/137 */
#define FINE_STRUCTURE_CONSTANT (Pow2(ELECTRON_CHARGE) / (PLANCK_CONSTANT_BAR * SPEED_OF_LIGHT) )

/* Thompson cross section (cm^2) ~ 0.665e-24 */
#define SIGMA_THOMPSON (8.0*PI/3.0 * Pow2(FINE_STRUCTURE_CONSTANT * PLANCK_CONSTANT_BAR / (M_ELECTRON * SPEED_OF_LIGHT)) )

/* Stefan-Boltzmann constant in erg cm-2 K-4 s-1 (5.67035E-5) */
#define STEFAN_BOLTZMANN_CONSTANT (Pow2(PI) * Pow4(BOLTZMANN_CONSTANT) / (60.0 * Pow3(PLANCK_CONSTANT_BAR) * Pow2(SPEED_OF_LIGHT)))

/* one parsec */
#define PARSEC (1296000.0 / TWOPI * ASTRONOMICAL_UNIT)

/* one kiloparsec */
#define KILOPARSEC (1e3 * PARSEC)

/* one megaparsec */
#define MEGAPARSEC (1e6 * PARSEC)

/* one gigaparsec */
#define GIGAPARSEC (1e9 * PARSEC)

/*
 * Define LIKE_ASTROPY to switch constants to
 * those used in astropy
 */
//#define LIKE_ASTROPY
#ifdef LIKE_ASTROPY
#undef GRAVITATIONAL_CONSTANT
#define GRAVITATIONAL_CONSTANT 6.67384e-08
#undef PI
#define PI 3.141592653589793
#undef BOLTZMANN_CONSTANT
#define BOLTZMANN_CONSTANT 1.3806488e-16
#undef M_PROTON
#define M_PROTON 1.672621777e-24
#undef STEFAN_BOLTZMANN_CONSTANT
#define STEFAN_BOLTZMANN_CONSTANT 5.670373e-05
#undef CMB_TEMPERATURE
#define CMB_TEMPERATURE 2.7
#endif//LIKE_ASTROPY

/* Planck units */
/* base units */
//#define PLANCK_MASS (sqrt(PLANCK_CONSTANT_BAR * SPEED_OF_LIGHT/GRAVITATIONAL_CONSTANT))
#define PLANCK_MASS (2.17647109235915924659e-05)
//#define PLANCK_TIME (sqrt(PLANCK_CONSTANT_BAR * GRAVITATIONAL_CONSTANT / Pow5(SPEED_OF_LIGHT)))
#define PLANCK_TIME (5.39115977020764519632e-44)
//#define PLANCK_LENGTH (sqrt(PLANCK_CONSTANT_BAR * GRAVITATIONAL_CONSTANT / Pow3(SPEED_OF_LIGHT)))
#define PLANCK_LENGTH (1.61622903898126498952e-33)
#define PLANCK_CHARGE (ELECTRON_CHARGE / sqrt(FINE_STRUCTURE_CONSTANT))
#define PLANCK_TEMPERATURE (PROTON_MASS * Pow2(SPEED_OF_LIGHT) / BOLTZMANN_CONSTANT)

/* derived units */
#define PLANCK_MOMENTUM (PLANCK_CONSTANT_BAR / PLANCK_LENGTH)
#define PLANCK_ENERGY (PLANCK_CONSTANT_BAR/PLANCK_TIME)
#define PLANCK_FORCE (PLANCK_ENERGY/PLANCK_LENGTH)
#define PLANCK_POWER (PLANCK_ENERGY/PLANCK_TIME)
#define PLANCK_DENSITY (PLANCK_MASS/Pow3(PLANCK_LENGTH))
#define PLANCK_ENERGY_DENSITY (PLANCK_ENERGY/Pow3(PLANCK_LENGTH))
#define PLANCK_INTENSITY (PLANCK_POWER/Pow2(PLANCK_LENGTH))
#define PLANCK_ANGULAR_FREQUENCY (1.0/PLANCK_TIME)
#define PLANCK_PRESSURE (PLANCK_FORCE / Pow2(PLANCK_LENGTH))
#define PLANCK_CURRENT (PLANCK_CHARGE/PLANCK_TIME)
#define PLANCK_VOLTAGE (PLANCK_ENERGY/PLANCK_CHARGE)
#define PLANCK_IMPEDANCE (PLANCK_VOLTAGE/PLANCK_CURRENT)
#define PLANCK_MAGNETIC_INDUCTANCE (PLANCK_FORCE/(PLANCK_LENGTH*PLANCK_CURRENT))
#define PLANCK_ELECTRICAL_INDUCTANCE (PLANCK_ENERGY/PLANCK_CURRENT)
#define PLANCK_VISCOSITY (PLANCK_PRESSURE*PLANCK_TIME)
#define PLANCK_ACCELERATION (SPEED_OF_LIGHT/PLANCK_TIME)
#define PLANCK_SPEED (SPEED_OF_LIGHT)
#define PLANCK_ANGULAR_MOMENTUM (PLANCK_CONSTANT_BAR)

/* one Foe (also known as a Bethe) is 10^51 erg == 10^44 Joule */
#define FOE (1e51)
#define BETHE (FOE)



#endif //BINARY_C_PHYSICAL_CONSTANTS
