#include "../binary_c.h"
No_empty_translation_unit_warning;


/*
 * Given stardata and a choice of stellar structure
 * algorithm, call the algorithm's appropriate shutdown
 * routines.
 */

int interface_shutdown(struct stardata_t * const stardata Maybe_unused)
{
    return 0;
}
