#include "../binary_c.h"
No_empty_translation_unit_warning;


/*
 * Given stardata and a choice of stellar structure
 * algorithm, call the algorithm's appropriate memory freeing
 * routines.
 */

int interface_free_memory(struct stardata_t * const stardata Maybe_unused)
{
    return 0;
}
