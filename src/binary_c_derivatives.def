#pragma once
#ifndef BINARY_C_DERIVATIVES_DEF
#define BINARY_C_DERIVATIVES_DEF

/*
 * The binary_c stellar population nucleosynthesis framework.
 *
 * Copyright Robert Izzard 2000-2025
 *
 * Contact rob.izzard@gmail.com
 *
 * Code repository:
 * https://gitlab.com/binary_c
 *
 * Documentation is in doc/
 * Code is in src/
 *
 * To build, run
 *
 * ---
 *
 * meson setup builddir
 * cd builddir
 * ninja binary_c_install
 *
 * ---
 *
 * Please see the files README, LICENCE and CHANGES,
 * and the doc/ directory for documentation which is mirrored
 * at https://binary_c.gitlab.io
 *
 **********************
 *
 * Purpose:
 *
 * Sets up the X macros defining the binary_c derivatives.
 * Please see binary_c_derivatives.h
 *
 **********************
 */

/*
 * Define the groups of derivatives in
 *
 * DERIVATIVES_GROUP_LIST
 *
 * these are prefixed with "DERIVATIVES_GROUP_"
 * when defined and should be declared here
 * in the desired solution order.
 */
#define DERIVATIVES_GROUP_LIST             \
    X(          SYSTEM,          "system") \
    X(         STELLAR,         "stellar") \
    X(           OTHER,           "other") \
    X( ORBITING_OBJECT, "orbiting object") \
    X(           EXTRA,           "extra")


/*
 * Columns:
 * 1 Label
 * 2 Descriptor string
 * 3 Varible pointer or NULL (star and stardata are available)
 * 4 Check function pointer or NULL
 * 5 Group: SYSTEM, STELLAR or OTHER (see DERIVATIVES_GROUP_LIST above)
 * 6 Boolean: whether MINT should clear this derivative before computing stellar structure (ignored by system and orbiting-object derivatives)
 * 7 Addition method. e.g. DERIVATIVE_ADD_SUM, DERIVATIVE_ADD_LUMINOSITY_WEIGHT
 */

#ifdef MINT
#define MINT_DERIVATIVES_LIST                                                                                                                                                    \
    X( CENTRAL_TEMPERATURE,     "Central temperature", &star->mint->central_temperature, &MINT_derivative_check_stellar_central_temperature, STELLAR, TRUE, DERIVATIVE_ADD_NONE) \
    X(    CENTRAL_HYDROGEN,        "Central hydrogen",                 &star->mint->XHc,    &MINT_derivative_check_stellar_central_hydrogen, STELLAR, TRUE, DERIVATIVE_ADD_NONE) \
    X(      CENTRAL_HELIUM,          "Central helium",                &star->mint->XHec,      &MINT_derivative_check_stellar_central_helium, STELLAR, TRUE, DERIVATIVE_ADD_NONE) \
    X(      CENTRAL_CARBON,          "Central carbon",                 &star->mint->XCc,                                               NULL, STELLAR, TRUE, DERIVATIVE_ADD_NONE) \
    X(      CENTRAL_OXYGEN,          "Central oxygen",                 &star->mint->XOc,                                               NULL, STELLAR, TRUE, DERIVATIVE_ADD_NONE) \
    X(  CENTRAL_DEGENERACY, "log(Central degeneracy)",  &star->mint->central_degeneracy,                                               NULL, STELLAR, TRUE, DERIVATIVE_ADD_NONE)
#else // MINT
#define MINT_DERIVATIVES_LIST /* do nothing */
#endif // MINT

#define STELLAR_DERIVATIVES_LIST                                                                                                                                                                   \
    X(                           MASS,                            "M (Total)",                           &star->mass,         &derivative_check_stellar_mass, STELLAR, FALSE, DERIVATIVE_ADD_NONE) \
    X(                 MASS_WIND_LOSS,                          "M Wind Loss",                                  NULL,                                   NULL, STELLAR, FALSE, DERIVATIVE_ADD_NONE) \
    X(                 MASS_WIND_GAIN,                          "M Wind Gain",                                  NULL,                                   NULL, STELLAR, FALSE, DERIVATIVE_ADD_NONE) \
    X(                         RADIUS,                               "Radius",                                  NULL,                                   NULL, STELLAR, FALSE, DERIVATIVE_ADD_NONE) \
    X(                     LUMINOSITY,                           "Luminosity",                                  NULL,                                   NULL, STELLAR, FALSE, DERIVATIVE_ADD_NONE) \
    X(                      CORE_MASS,                            "Core Mass",                                  NULL,                                   NULL, STELLAR, FALSE, DERIVATIVE_ADD_NONE) \
    X(                    TEMPERATURE,                  "Stellar temperature",                                  NULL,                                   NULL, STELLAR, FALSE, DERIVATIVE_ADD_NONE) \
    X(                 MASS_RLOF_LOSS,                          "M RLOF Loss",                                  NULL,                                   NULL, STELLAR, FALSE, DERIVATIVE_ADD_NONE) \
    X(                 MASS_RLOF_GAIN,                          "M RLOF Gain",                                  NULL,                                   NULL, STELLAR, FALSE, DERIVATIVE_ADD_NONE) \
    X(                      MASS_NOVA,                               "M Nova",                                  NULL,                                   NULL, STELLAR, FALSE, DERIVATIVE_ADD_NONE) \
    X(             MASS_RLOF_TRANSFER,                      "M RLOF Transfer",                                  NULL,                                   NULL, STELLAR, FALSE, DERIVATIVE_ADD_NONE) \
    X(                 MASS_DISC_GAIN,                          "M Disc Gain",                                  NULL,                                   NULL, STELLAR, FALSE, DERIVATIVE_ADD_NONE) \
    X(                 MASS_DISC_LOSS,                          "M Disc Loss",                                  NULL,                                   NULL, STELLAR, FALSE, DERIVATIVE_ADD_NONE) \
    X(            MASS_DECRETION_DISC,                "M Decretion Disc Loss",                                  NULL,                                   NULL, STELLAR, FALSE, DERIVATIVE_ADD_NONE) \
    X(               MASS_CBDISC_GAIN,                        "M CBdisc Gain",                                  NULL,                                   NULL, STELLAR, FALSE, DERIVATIVE_ADD_NONE) \
    X(      MASS_NONCONSERVATIVE_LOSS,              "M Non-conservative Loss",                                  NULL,                                   NULL, STELLAR, FALSE, DERIVATIVE_ADD_NONE) \
    X(          MASS_IRRADIATIVE_LOSS,                   "M Irradiative Loss",                                  NULL,                                   NULL, STELLAR, FALSE, DERIVATIVE_ADD_NONE) \
    X(                MASS_ARTIFICIAL,                         "M artificial",                                  NULL,                                   NULL, STELLAR, FALSE, DERIVATIVE_ADD_NONE) \
    X(                   MASS_COMMAND,                            "M command",                                  NULL,                                   NULL, STELLAR, FALSE, DERIVATIVE_ADD_NONE) \
    X(               ANGMOM_RLOF_LOSS,                          "J RLOF Loss",                                  NULL,                                   NULL, STELLAR, FALSE, DERIVATIVE_ADD_NONE) \
    X(               ANGMOM_RLOF_GAIN,                          "J RLOF Gain",                                  NULL,                                   NULL, STELLAR, FALSE, DERIVATIVE_ADD_NONE) \
    X(               ANGMOM_WIND_LOSS,                          "J Wind Loss",                                  NULL,                                   NULL, STELLAR, FALSE, DERIVATIVE_ADD_NONE) \
    X(               ANGMOM_WIND_GAIN,                          "J Wind Gain",                                  NULL,                                   NULL, STELLAR, FALSE, DERIVATIVE_ADD_NONE) \
    X(                   ANGMOM_TIDES,                              "J Tides",                                  NULL,                                   NULL, STELLAR, FALSE, DERIVATIVE_ADD_NONE) \
    X(        ANGMOM_MAGNETIC_BRAKING,                   "J Magnetic Braking",                                  NULL,                                   NULL, STELLAR, FALSE, DERIVATIVE_ADD_NONE) \
    X(          ANGMOM_DECRETION_DISC,                     "J Decretion Disc",                                  NULL,                                   NULL, STELLAR, FALSE, DERIVATIVE_ADD_NONE) \
    X(             ANGMOM_CBDISC_GAIN,                        "J CBdisc gain",                                  NULL,                                   NULL, STELLAR, FALSE, DERIVATIVE_ADD_NONE) \
    X(                    ANGMOM_NOVA,                               "J Nova",                                  NULL,                                   NULL, STELLAR, FALSE, DERIVATIVE_ADD_NONE) \
    X(    ANGMOM_NONCONSERVATIVE_LOSS,                   "J non-conservative",                                  NULL,                                   NULL, STELLAR, FALSE, DERIVATIVE_ADD_NONE) \
    X(              ANGMOM_ARTIFICIAL,                         "J artificial",                                  NULL,                                   NULL, STELLAR, FALSE, DERIVATIVE_ADD_NONE) \
    X(                 ANGMOM_COMMAND,                            "J command",                                  NULL,                                   NULL, STELLAR, FALSE, DERIVATIVE_ADD_NONE) \
    X(  ANGMOM_CORE_ENVELOPE_COUPLING,             "J core-envelope coupling",                                  NULL,                                   NULL, STELLAR, FALSE, DERIVATIVE_ADD_NONE) \
    X(                         ANGMOM,                            "J (Total)",               &star->angular_momentum,       &derivative_check_stellar_angmom, STELLAR, FALSE, DERIVATIVE_ADD_NONE) \
    X(                   ECCENTRICITY,                         "eccentricity",                                  NULL,                                   NULL, STELLAR, FALSE, DERIVATIVE_ADD_NONE) \
    X(         ANGULAR_VELOCITY_TIDES,             "angular velocity (tides)",                                  NULL,                                   NULL, STELLAR, FALSE, DERIVATIVE_ADD_NONE) \
    X(                   He_CORE_MASS,                         "He core mass",             &star->core_mass[CORE_He], &derivative_check_stellar_he_core_mass, STELLAR,  TRUE, DERIVATIVE_ADD_NONE) \
    X(                   GB_CORE_MASS,                         "GB core mass",                                  NULL,                                   NULL, STELLAR,  TRUE, DERIVATIVE_ADD_NONE) \
    X(                   CO_CORE_MASS,                         "CO core mass",             &star->core_mass[CORE_CO], &derivative_check_stellar_CO_core_mass, STELLAR,  TRUE, DERIVATIVE_ADD_NONE) \
    X(                  ONe_CORE_MASS,                        "ONe core mass",            &star->core_mass[CORE_ONe],                                   NULL, STELLAR,  TRUE, DERIVATIVE_ADD_NONE) \
    X(                   Si_CORE_MASS,                         "Si core mass",             &star->core_mass[CORE_Si],                                   NULL, STELLAR,  TRUE, DERIVATIVE_ADD_NONE) \
    X(                   Fe_CORE_MASS,                         "Fe core mass",             &star->core_mass[CORE_Fe],                                   NULL, STELLAR,  TRUE, DERIVATIVE_ADD_NONE) \
    X(              NEUTRON_CORE_MASS,                    "Neutron core mass",        &star->core_mass[CORE_NEUTRON],                                   NULL, STELLAR,  TRUE, DERIVATIVE_ADD_NONE) \
    X(                   BH_CORE_MASS,                 "Black-hole core mass",     &star->core_mass[CORE_BLACK_HOLE],                                   NULL, STELLAR,  TRUE, DERIVATIVE_ADD_NONE) \
    X(           He_CORE_MASS_NO_TDUP,               "He core mass (no 3DUP)",              &star->core_mass_no_3dup,                                   NULL, STELLAR,  TRUE, DERIVATIVE_ADD_NONE) \
    X(             NUM_THERMAL_PULSES,             "Number of thermal pulses",             &star->num_thermal_pulses,   &derivative_check_num_thermal_pulses, STELLAR,  TRUE, DERIVATIVE_ADD_NONE) \
    X( NUM_THERMAL_PULSES_SINCE_MCMIN, "Number of thermal pulses since mcmin", &star->num_thermal_pulses_since_mcmin,   &derivative_check_num_thermal_pulses, STELLAR,  TRUE, DERIVATIVE_ADD_NONE) \
    X(                   H_LAYER_MASS,                    "nova H layer mass",                       &star->dm_novaH,             &derivative_check_dm_novaH, STELLAR, FALSE, DERIVATIVE_ADD_NONE) \
    X(                  He_LAYER_MASS,                   "nova He layer mass",                      &star->dm_novaHe,            &derivative_check_dm_novaHe, STELLAR, FALSE, DERIVATIVE_ADD_NONE) \
    X(                   ROCHE_RADIUS,                         "Roche radius",                                  NULL,                                   NULL, STELLAR, FALSE, DERIVATIVE_ADD_NONE) \
    X(                      NUM_NOVAE,                      "Number of novae",                      &star->num_novae,            &derivative_check_num_novae, STELLAR, FALSE, DERIVATIVE_ADD_NONE) \
                                                                                                                               MINT_DERIVATIVES_LIST                                               \
    X (NUMBER, "Number", NULL, NULL,STELLAR, FALSE, DERIVATIVE_ADD_NONE )


#define SYSTEM_DERIVATIVES_LIST                                                                                                                \
    X(                               ORBIT_ANGMOM,                 "J (total)", &stardata->common.orbit.angular_momentum, NULL, SYSTEM, FALSE) \
    X(                      ORBIT_SEMI_MAJOR_AXIS,                         "a",                                     NULL, NULL, SYSTEM, FALSE) \
    X(                         ORBIT_ECCENTRICITY,                 "e (total)",     &stardata->common.orbit.eccentricity, NULL, SYSTEM, FALSE) \
    X(       ORBIT_ANGMOM_GRAVITATIONAL_RADIATION, "J Gravitational Radiation",                                     NULL, NULL, SYSTEM, FALSE) \
    X(                     ORBIT_ANGMOM_WIND_LOSS,               "J Wind Loss",                                     NULL, NULL, SYSTEM, FALSE) \
    X(                     ORBIT_ANGMOM_WIND_GAIN,               "J Wind Gain",                                     NULL, NULL, SYSTEM, FALSE) \
    X(                     ORBIT_ANGMOM_RLOF_LOSS,               "J RLOF Loss",                                     NULL, NULL, SYSTEM, FALSE) \
    X(                     ORBIT_ANGMOM_RLOF_GAIN,               "J RLOF Gain",                                     NULL, NULL, SYSTEM, FALSE) \
    X(                        ORBIT_ANGMOM_CBDISC,       "J Circumbinary Disc",                                     NULL, NULL, SYSTEM, FALSE) \
    X(                         ORBIT_ANGMOM_TIDES,                   "J tides",                                     NULL, NULL, SYSTEM, FALSE) \
    X(          ORBIT_ANGMOM_NONCONSERVATIVE_LOSS,         "J nonconservative",                                     NULL, NULL, SYSTEM, FALSE) \
    X(                          ORBIT_ANGMOM_NOVA,                    "J nova",                                     NULL, NULL, SYSTEM, FALSE) \
    X(              ORBIT_ANGMOM_ORBITING_OBJECTS,        "J orbiting objects",                                     NULL, NULL, SYSTEM, FALSE) \
    X(                    ORBIT_ANGMOM_ARTIFICIAL,              "J artificial",                                     NULL, NULL, SYSTEM, FALSE) \
    X(                       ORBIT_ANGMOM_COMMAND,                 "J command",                                     NULL, NULL, SYSTEM, FALSE) \
    X( ORBIT_ECCENTRICITY_GRAVITATIONAL_RADIATION, "e Gravitational Radiation",                                     NULL, NULL, SYSTEM, FALSE) \
    X(                   ORBIT_ECCENTRICITY_TIDES,                   "e Tides",                                     NULL, NULL, SYSTEM, FALSE) \
    X(                   ORBIT_ECCENTRICITY_WINDS,                   "e Winds",                                     NULL, NULL, SYSTEM, FALSE) \
    X(                  ORBIT_ECCENTRICITY_CBDISC,       "e Circumbinary Disc",                                     NULL, NULL, SYSTEM, FALSE) \
    X(                    ORBIT_ECCENTRICITY_RLOF,                    "e RLOF",                                     NULL, NULL, SYSTEM, FALSE) \
    X(                    SYSTEM_CBDISC_MASS_LOSS,    "Mdot Circumbinary Disc",                                     NULL, NULL, SYSTEM, FALSE) \
    X(                                SYSTEM_TEST,                      "Test",                   &stardata->common.test, NULL, SYSTEM, FALSE) \
    X(                              SYSTEM_NUMBER,                    "Number",                                     NULL, NULL, SYSTEM, FALSE)

#define ORBITING_OBJECT_DERIVATIVES_LIST                                                         \
    X(                     MASS,                     "Mass", NULL, NULL, ORBITING_OBJECT, FALSE) \
    X( ORBITAL_ANGULAR_MOMENTUM, "Orbital angular momentum", NULL, NULL, ORBITING_OBJECT, FALSE) \
    X(     ORBITAL_ECCENTRICITY,     "Orbital eccentricity", NULL, NULL, ORBITING_OBJECT, FALSE) \
    X(    SPIN_ANGULAR_MOMENTUM,    "Spin angular momentum", NULL, NULL, ORBITING_OBJECT, FALSE) \
    X(                   NUMBER,                   "Number", NULL, NULL, ORBITING_OBJECT, FALSE)


#define DERIVATIVE_ADDITIONS_LIST \
    X(                NONE)       \
    X(                 SUM)       \
    X( LUMINOSITY_WEIGHTED)       \
    X(       MASS_WEIGHTED)       \
    X(     RADIUS_WEIGHTED)       \
    X(       TEFF_WEIGHTED)

#endif // BINARY_C_DERIVATIVES_DEF
