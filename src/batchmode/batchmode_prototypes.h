#ifndef BATCHMODE_PROTOTYPES_H
#define BATCHMODE_PROTOTYPES_H

void batchmode_loop(struct preferences_t * preferences,
                    struct stardata_t * stardata,
                    ticks * start_tick);

#endif // BATCHMODE_PROTOTYPES_H
