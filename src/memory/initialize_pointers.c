#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "../binary_c_collision_matrix.h"

void initialize_pointers(struct stardata_t * const stardata,
                         struct preferences_t * const preferences,
                         struct store_t * const store)
{
    /*
     * Set up pointers to memory locations required
     * by binary_c structures, 
     * e.g. stardata, preferences
     *
     * Both stardata and preferences should be defined
     * elsewhere, and must not be NULL
     */

    /*
     * Point stardata to look at the preferences and store structs
     */
    stardata->preferences = preferences;
    stardata->store = store;
}
