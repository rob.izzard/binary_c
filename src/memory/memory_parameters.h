#pragma once
#ifndef MEMORY_PARAMETERS_H
#define MEMORY_PARAMETERS_H

#include "memory_parameters.def"

#undef X
#define X(CODE) COPY_STARDATA_##CODE,
enum { COPY_STARDATA_ACTIONS_LIST };
#undef X

#undef X
#define X(CODE) COPY_STARDATA_PERSISTENT_##CODE,
enum { COPY_STARDATA_PERSISTENT_LIST };
#undef X


#define X(CODE) CODESTAT_##CODE,
enum { CODESTATS_LIST };
#undef X



#endif // MEMORY_PARAMETERS_H
