/*
 * memory allocation functions that use aligned_alloc()
 */
#define MEMORY_ALLOCATION_MODEL_STRING "Aligned"

static inline void * Alloc_size_first Malloc_like aligned_malloc(size_t size);

static inline void * Alloc_size_product Malloc_like aligned_calloc(size_t nmemb,
                                                                   size_t size);
static inline void * Alloc_size_second aligned_realloc(void * ptr,
                                                       size_t size);

static inline void * Alloc_size_first Malloc_like aligned_malloc(size_t size)
{
    if(unlikely(size==0)) return NULL;
    alignas(ALIGNSIZE) void * const p = aligned_alloc(ALIGNSIZE,
                                                      Aligned_size(size));
    _Alloc_check(p);
    return p;
}

static inline void * Alloc_size_product Malloc_like aligned_calloc(size_t nmemb,size_t size)
{
    if(unlikely(size==0)) return NULL;
    const size_t n = Aligned_size(nmemb*size);
    alignas(ALIGNSIZE) void * const p = aligned_alloc(ALIGNSIZE,n);
    _Alloc_check(p);
    if(p)
    {
        memset(p,0,n);
    }
    return p;
}

static inline void * Alloc_size_second aligned_realloc(void * ptr,
                                                       size_t size)
{
    alignas(ALIGNSIZE) void * p;
    if(ptr == NULL)
    {
        p = aligned_malloc(size);
    }
    else
    {
        /*
         * try to use normal realloc, see if that is aligned
         */
        size = Aligned_size(size);
        p = realloc(ptr,size);
        _Alloc_check(p);
        if(p != NULL &&
           p != ptr &&
           alignof(p) != ALIGNSIZE)
        {
            /*
             * changed pointer, no longer aligned, damn:
             * reallocate and copy
             */
            alignas(ALIGNSIZE) void * pwas = p;
            p = aligned_malloc(size);
            _Alloc_check(p);
            if(p)
            {
                memcpy(p,pwas,size);
            }
            free(pwas);
        }
    }
    return p;
}

#define Assume_aligned(P) (P)

/*
 * Function macros for Malloc, Calloc, Realloc
 */
#define _Malloc(S)                              \
    __extension__                               \
    ({                                          \
        Increment_codestat(CODESTAT_MALLOC);    \
        (aligned_malloc(S));                    \
    })
#define _Calloc(N,S)                            \
    __extension__                               \
    ({                                          \
        Increment_codestat(CODESTAT_CALLOC);    \
        (aligned_calloc((N),(S)));              \
    })
#define _Realloc(P,S)                           \
    __extension__                               \
    ({                                          \
        Increment_codestat(CODESTAT_REALLOC);   \
        (aligned_realloc((P),(S)));             \
    })
