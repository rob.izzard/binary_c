#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "memory_alignment_checks.h"

/*
 * Zero the contents of a star struct, but preserve
 * its sub-struct pointers
 */
void zero_star(struct star_t * const Restrict star)
{
    /*
     * Save sub-struct pointers
     */
#ifdef MINT
    struct mint_t * mint = star->mint;
#endif//MINT
#ifdef BSE
    struct BSE_data_t * bse = star->bse;
#endif

    /*
     * Zero the contents
     */
    memset(star,0,sizeof(struct star_t));

    /*
     * Restore pointers
     */
#ifdef MINT
    star->mint = mint;
    if(star->mint != NULL)
    {
        memset(star->mint,0,sizeof(struct mint_t));
    }
#endif // MINT
#ifdef BSE
    star->bse = bse;
    if(star->bse != NULL)
    {
        Safe_free(star->bse->luminosities);
        Safe_free(star->bse->GB);
        Safe_free(star->bse->timescales);
    }
#endif//BSE
}
