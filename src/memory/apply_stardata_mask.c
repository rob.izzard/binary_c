#include "../binary_c.h"
No_empty_translation_unit_warning;

/*
 * Apply a mask to stardata.
 *
 * Note that we have to set up the unions so
 * the data in stardata, mask and mask_contents
 * can be accessed as char* types by the
 * apply_data_mask function.
 */

union Data {
    struct stardata_t * stardata_version;
    char * char_version;
};

void apply_stardata_mask(struct stardata_t * Restrict const stardata,
                         struct stardata_t * Restrict const mask,
                         struct stardata_t * Restrict const mask_contents)
{
    if(mask != NULL)
    {
        union Data stardata_union = { .stardata_version = stardata };
        const union Data mask_union = { .stardata_version = mask };
        const union Data mask_contents_union = { .stardata_version = mask_contents };
        apply_data_mask(stardata_union.char_version,
                        mask_union.char_version,
                        mask_contents_union.char_version,
                        sizeof(struct stardata_t));
    }
}
