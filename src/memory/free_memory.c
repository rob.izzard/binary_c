#include "binary_c.h"

/*
 * Function to free memory on exit from the binary_c program
 *
 * If free_preferences_bool is TRUE, the preferences struct is freed.
 * If free_stardata_bool is TRUE, the stardata struct is freed.
 * If free_store_bool is TRUE, the store struct is freed.
 * etc.
 *
 * Enable FMDebug to debug this function : Dprint might not
 * work because the structures it uses are successively freed
 * during this function.
 */

//#define FMDebug(...) fprintf(stderr,__VA_ARGS__);
#define FMDebug(...)

void free_memory(struct stardata_t ** Restrict const sp,
                 const Boolean free_preferences_bool,
                 const Boolean free_stardata_struct_bool,
                 const Boolean free_store_bool,
                 const Boolean free_raw_buffer_bool,
                 const Boolean free_persistent_bool)
{
    struct stardata_t * stardata = (sp==NULL) ? NULL : *sp;

    FMDebug(
        "binary_c free_memory: Freeing memory, sp = %p, *sp = %p, stardata = %p, free_preferences_bool=%s, free_stardata_struct_bool=%s, free_store_bool=%s, free_raw_buffer_bool=%s, free_persistent_bool=%s\n",
        (void*)sp,
        (sp!=NULL ? ((void*)*sp) : NULL),
        (void*)stardata,
        Yesno(free_preferences_bool),
        Yesno(free_stardata_struct_bool),
        Yesno(free_store_bool),
        Yesno(free_raw_buffer_bool),
        Yesno(free_persistent_bool));

    /*
     * What to do if stardata is NULL?
     */
    if(sp==NULL || stardata==NULL) return;

    /*
     * Free persistent data including calls to routines
     * to output the data if required.
     */
    if(stardata != NULL && free_persistent_bool == TRUE)
    {
        FMDebug("persistent_data %p\n",(void*)stardata->persistent_data);
        free_persistent_data(stardata);
    }

    FMDebug("done that\n");

#ifdef MEMMAP
    FMDebug("Call munmap\n");
    munmap(stardata->biglog,BIGLOG_SIZE);
#endif /* MEMMAP */
#ifdef DISCS
    FMDebug("Discs\n");
    disc_mem_cleanup(stardata);
#endif
    FMDebug("Diff stats\n");
#ifdef NUCSYN
    FMDebug("Nucsyn\n");
#endif // NUCSYN
#ifdef STELLAR_POPULATIONS_ENSEMBLE
    if(stardata->preferences->ensemble_defer == FALSE)
    {
        FMDebug("call free ensemble cdict\n");
        free_ensemble_cdict(stardata);
    }
#endif // STELLAR_POPULATIONS_ENSEMBLE
#ifdef BINARY_C_API
    binary_c_API_close_logfile(stardata);
#endif // BINARY_C_API
#ifdef FILE_LOG
    if(stardata != NULL &&
       stardata->model.log_fp != NULL)
    {
        FMDebug("log files\n");
        close_log_files(&(stardata->model.log_fp),
                        stardata);
        stardata->model.log_fp = NULL;
    }
#endif // FILE_LOG

#ifdef SAVE_MASS_HISTORY
    CDict_free_and_free_contents(stardata->model.mass_history);
#endif //SAVE_MASS_HISTORY

#ifdef MINT
    MINT_free(stardata);
#endif//MINT

    FMDebug("aux\n");
    free_aux_memory();

#if defined BACKTRACE && defined USE_BACKTRACE_CACHE
    backtrace_free_cache_memory();
#endif

    FMDebug("tmpstore %p\n",(void*)stardata->tmpstore);
    free_tmpstore(stardata->tmpstore,
                  free_raw_buffer_bool);

    if(free_store_bool == TRUE &&
       stardata->store != NULL)
    {
        FMDebug("Free store %p\n",(void*)stardata->store);
        free_store(&stardata->store);
    }

    struct preferences_t * prefs = stardata ? stardata->preferences : NULL;

    if(free_stardata_struct_bool == TRUE)
    {
        /*
         * Free stardatas but not preferences
         * (done later) because preferences are
         * required to tell free_stardata what is
         * allocated.
         */
        FMDebug("previous stardatas\n");
        if(stardata!=NULL)
        {
            free_previous_stardatas(stardata);
            free_stardata_stack(stardata);
        }
        FMDebug("Free stardata pointer (sp) stardata=%p sp=&stardata=%p\n",
                (void*)stardata,
                (void*)sp);
        free_stardata(sp);
    }

    if(free_preferences_bool==TRUE)
    {
        /*
         * Free preferences
         */
        FMDebug("preferences %p\n",(void*)prefs);
#ifdef ORBITING_OBJECTS
        Safe_free(prefs->zero_age.orbiting_object);
#endif // ORBITING_OBJECTS

        /*
         * Free unbounded string buffers
         */
        Safe_free(prefs->JSON_commands_string);

        /*
         * Free commands cdict
         */
        if(prefs->commands != NULL)
        {
            CDict_free(prefs->commands);
        }
        Safe_free(prefs);
    }

    FMDebug("free_memory returning, stardara=%p, sp=%p ",
            (void*)stardata,
            (void*)sp);

#ifdef COUNT_MEMORY
    FMDebug("memuse=%p",(void*)memuse);
#endif
    FMDebug("\n");
}
