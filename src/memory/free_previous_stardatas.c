#include "binary_c.h"

void free_previous_stardatas(struct stardata_t * Restrict const stardata)
{
    /*
     * free previous stardatas
     */
    if(stardata!=NULL)
    {
        if(stardata->previous_stardatas != NULL)
        {
            unsigned int i;
            for(i=0;i<stardata->n_previous_stardatas;i++)
            {
                free_stardata(&stardata->previous_stardatas[i]);
            }
            Safe_free_nocheck(stardata->previous_stardatas);
        }
        stardata->previous_stardata = NULL;
    }
}
