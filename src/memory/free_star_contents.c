#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "memory_alignment_checks.h"

/*
 * Free a star struct's contents
 */
void free_star_contents(struct star_t * const Restrict star)
{
    if(star != NULL)
    {
#if defined NUCSYN && defined NUCSYN_ID_SOURCES
        Sourceloop(j)
        {
            Safe_free(star->Xsource[j]);
            Safe_free(star->oldsource[j]);
        }
#endif // NUCSYN && NUCSYN_ID_SOURCES
#ifdef MINT
        MINT_free_star(star);
#endif//MINT
#ifdef BSE
        free_BSE_data(&star->bse);
#endif//BSE
    }
}
