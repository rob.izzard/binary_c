#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef BSE

void free_BSE_data(struct BSE_data_t ** const Restrict bse)
{
    /*
     * Free a BSE struct and its data
     */
    if(bse != NULL &&
       *bse != NULL)
    {
        Safe_free((*bse)->luminosities);
        Safe_free((*bse)->timescales);
        Safe_free((*bse)->GB);
        Safe_free(*bse);
    }
}

#endif//BSE
