#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "../binary_c_collision_matrix.h"
#include "../setup/defaults_sets.h"

void build_store_contents(struct stardata_t * const stardata,
                          struct store_t * const store)
{
    /*
     * Build data content in the store.
     * See also free_store_contents.c
     *
     * Note that data in the core is true *constant* data:
     * it must not change as a function of any parameters
     * (e.g. metallicity).
     *
     * Data that depends on parameters of the system
     * should be stored in the stardata->common struct,
     * is initialized at the start of evolution (see init_common).
     *
     * Note:
     * NOTHING in here can depend on the current stardata.
     * You will NOT have access to stardata here.
     */

#ifdef __HAVE_LIBPTHREAD__
    /*
     * Make mutexes for later locking
     */
    for(size_t i=0; i<BINARY_C_MUTEX_NUMBER; i++)
    {
        store->pthread_mutexes[i].locked = FALSE;
        const int init_status =
            pthread_mutex_init(&store->pthread_mutexes[i].mutex,
                               NULL);
        if(init_status != 0)
        {
            fprintf(stderr,
                    "\n failed to initialize pthread mutex %zu, error %d %s : this will cause problems!\n",
                    i,
                    init_status,
                    Mutex_error_string(init_status)
                );
        }
    }


    /*
     * Lock the store creation mutex
     */
    Pthread_lock_primitive(store,BINARY_C_MUTEX_BUILD_STORE_LOCK);

#endif//__HAVE_LIBPTHREAD__

    if(store->built==FALSE)
    {
        store->built = TRUE;

#ifdef NUCSYN
        nucsyn_build_store_contents(store);
#endif // NUCSYN
#ifdef MINT
        MINT_init_store(store);
#endif//MINT

#undef X
#define X(COLOUR,STRING) STRING,
        static const char * const ansi_colour_strings[] = { ANSI_COLOURS_LIST };
#undef X
        store->ANSI_colours_table = Malloc(sizeof(char*)*NUM_ANSI_COLOURS);
        store->no_colours_table = Malloc(sizeof(char*)*NUM_ANSI_COLOURS);
        for(int i=0;i<NUM_ANSI_COLOURS;i++)
        {
            store->no_colours_table[i] = Calloc(1,sizeof(char));
            if(asprintf(&store->ANSI_colours_table[i],
                        "%s",
                        ansi_colour_strings[i])==0){}
        }
        store->colours = store->ANSI_colours_table;

        /*
         * Set up collision stellar type matrix
         */
        static Stellar_type const_collision_matrix[Aligned_size(COLLISION_MATRIX_SIZE)][Aligned_size(COLLISION_MATRIX_SIZE)] Aligned  = COLLISION_MATRIX;

#ifndef ALLOW_HeWD_SUPERNOVAE
        /*
         * Force HeWD-HeWD mergers to become HeMS stars
         * (i.e. reignite but do not disrupt)
         */
        const_collision_matrix[HeWD][HeWD]=HeMS;
#endif // ALLOW_HeWD_SUPERNOVAE
        const size_t x = COLLISION_MATRIX_SIZE*sizeof(Stellar_type);
        for(int i=0;i<COLLISION_MATRIX_SIZE;i++)
        {
            store->collision_matrix[i] = Malloc(x);
            memcpy(store->collision_matrix[i],
                   const_collision_matrix[i],
                   x);
        }

#ifdef FILE_LOG
        if(asprintf(&store->upchar,
                    "%s",
                    Elvis(getenv("UPARROW"),"⇑"))==0){}
        if(asprintf(&store->downchar,
                    "%s",
                    Elvis(getenv("DOWNARROW"),"⇓"))==0){}
        if(asprintf(&store->nochangechar," ")==0){}
#undef X
#define X(CODE,STRING) #STRING,
        static const char * const const_label[STRING_LENGTH] = { LOG_LABELS_LIST };
        for(int i=0;i<NLOG_LABEL;i++)
        {
            store->label[i] = const_label[i];
        }
#endif // FILE_LOG
#ifdef STELLAR_COLOURS
        binary_magnitudes_allocate_memory(store);
        table_Eldridge2012_magnitudes(store);
        table_Carrasco2014(store);
#endif // STELLAR_COLOURS
        table_spectral_types(store);
        build_spectral_type_static_strings(store);

#ifdef USE_2012_MAIN_SEQUENCE_LIFETIMES_TABLE
        table_massive_MS_lifetimes(store);
#endif // USE_2012_MAIN_SEQUENCE_LIFETIMES_TABLE
        table_comenv(store);
#ifdef USE_BSE_TABLES
        table_BSE(store);
#endif // USE_BSE_TABLES
        table_Karakas_ncal(store);

#ifdef COMENV_POLYTROPES
        table_comenv_polytropes(store);
#endif // COMENV_POLYTROPES
#ifdef OPACITY_ALGORITHMS
#ifdef OPACITY_ENABLE_ALGORITHM_PACZYNSKI
        table_opacity_paczynski(store);
#endif // OPACITY_ENABLE_ALGORITHM_PACZYNSKI
#ifdef OPACITY_ENABLE_ALGORITHM_STARS
        table_opacity_STARS(store);
#endif // OPACITY_ENABLE_ALGORITHM_STARS
#ifdef OPACITY_ENABLE_ALGORITHM_FERGUSON_OPAL
        table_opacity_ferguson_opal(store);
#endif // OPACITY_ENABLE_ALGORITHM_FERGUSON_OPAL
#endif // OPACITY_ALGORITHMS
        table_miller_bertolami(store);
        table_Karakas2002_lumfunc(store);
        table_Karakas2002_radius(store);
        table_Temmink2022(stardata,store);
        make_pulsator_store(store);

        store->fallback_preferences = Calloc(1,sizeof(struct preferences_t));
        store->fallback_preferences->defaults_set = DEFAULTS_SET_FALLBACK;
        set_preferences(store->fallback_preferences);

        store->built = TRUE;
        store->debug_stopping = 0;
    }

    /*
     * Lock the store creation mutex
     */
    Pthread_unlock_primitive(store,BINARY_C_MUTEX_BUILD_STORE_LOCK);
}
