#include "../binary_c.h"
No_empty_translation_unit_warning;

/*
 * Erase a star's contents
 */

void clean_star(struct star_t * Restrict const star)
{
    free_star_contents(star);
    memset(star,0,sizeof(struct star_t));
}
