#include "../binary_c.h"
No_empty_translation_unit_warning;

/*
 * Wrapper function to free the store contents, at *store_p,
 * and then free the store itself.
 */
void free_store(struct store_t ** store_p)
{
    free_store_contents(*store_p);
    Safe_free_nocheck(*store_p);
}
