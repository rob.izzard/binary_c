#include "../binary_c.h"
No_empty_translation_unit_warning;


#include "../binary_c_collision_matrix.h"
#include "../logging/logfile_macros.h"


void build_tmpstore_contents(struct tmpstore_t * Restrict const tmpstore)
{
    if(tmpstore->error_buffer == NULL)
    {
        tmpstore->error_buffer = Malloc(sizeof(char)*BUFFERED_PRINTF_ERROR_BUFFER_SIZE);
    }

#ifdef GAIAHRD
    if(tmpstore->gaiaHRD==NULL)
    {
        tmpstore->gaiaHRD = Calloc(1,sizeof(struct gaiaHRD_chunk_t));
    }
#endif//GAIAHRD
    tmpstore->kaps_rentrop_GSL_m = NULL;
    tmpstore->kaps_rentrop_GSL_p = NULL;
    tmpstore->kaps_rentrop_GSL_n = 0;
    tmpstore->c_log_prefix = getenv("BINARY_C_LOG_PREFIX");
    tmpstore->file_log_prevstring = NULL;
    tmpstore->tidal_multipliers = NULL;
}
