/*
 * define macros for malloc, calloc, realloc
 *
 * Here we simply use the native system library
 * versions, which are NOT guaranteed to be
 * aligned to anything longer than 8 bytes.
 */
#define MEMORY_ALLOCATION_MODEL_STRING "Native"

#define _Malloc(S)                              \
    __extension__                               \
    ({                                          \
        Increment_codestat(CODESTAT_MALLOC);    \
        (malloc((S)));                          \
    })

#define _Calloc(N,S)                            \
    __extension__                               \
    ({                                          \
        Increment_codestat(CODESTAT_CALLOC);    \
        (calloc((N),(S)));                      \
    })

#define _Realloc(P,S)                           \
    __extension__                               \
    ({                                          \
        Increment_codestat(CODESTAT_REALLOC);   \
        (realloc((P),(S)));                     \
    })


#define Assume_aligned(P) (P)
