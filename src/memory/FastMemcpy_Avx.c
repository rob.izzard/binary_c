#include "../binary_c.h"
No_empty_translation_unit_warning;
#ifdef MEMCPY_USE_SKYWIND_FAST
#ifdef __HAVE_AVX__
/*
The MIT License (MIT)

Copyright (c) 2015 Linwei

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <assert.h>

#if (defined(_WIN32) || defined(WIN32))
#include <windows.h>
#include <mmsystem.h>
#ifdef _MSC_VER
#pragma comment(lib, "winmm.lib")
#endif
#elif defined(__unix)
#include <sys/time.h>
#include <unistd.h>
#else
#error it can only be compiled under windows or unix
#endif
#include "FastMemcpy_Avx.h"


//---------------------------------------------------------------------
// main routine
//---------------------------------------------------------------------

void* memcpy_fast(void *destination, const void *source, size_t size)
{
    unsigned char *dst = (unsigned char*)destination;
    const unsigned char *src = (const unsigned char*)source;
    static size_t cachesize = 0x200000; // L3-cache size
    size_t padding;

    // small memory copy
    if (size <= 256) {
        memcpy_tiny(dst, src, size);
        _mm256_zeroupper();
        return destination;
    }

    // align destination to 16 bytes boundary
    padding = (32 - (((size_t)dst) & 31)) & 31;

#if 0
    if (padding > 0) {
        __m256i head = _mm256_loadu_si256((const __m256i*)src);
        _mm256_storeu_si256((__m256i*)dst, head);
        dst += padding;
        src += padding;
        size -= padding;
    }
#else
    __m256i head = _mm256_loadu_si256((const __m256i*)src);
    _mm256_storeu_si256((__m256i*)dst, head);
    dst += padding;
    src += padding;
    size -= padding;
#endif

    // medium size copy
    if (size <= cachesize) {
        __m256i c0, c1, c2, c3, c4, c5, c6, c7;

        for (; size >= 256; size -= 256) {
            c0 = _mm256_loadu_si256(((const __m256i*)src) + 0);
            c1 = _mm256_loadu_si256(((const __m256i*)src) + 1);
            c2 = _mm256_loadu_si256(((const __m256i*)src) + 2);
            c3 = _mm256_loadu_si256(((const __m256i*)src) + 3);
            c4 = _mm256_loadu_si256(((const __m256i*)src) + 4);
            c5 = _mm256_loadu_si256(((const __m256i*)src) + 5);
            c6 = _mm256_loadu_si256(((const __m256i*)src) + 6);
            c7 = _mm256_loadu_si256(((const __m256i*)src) + 7);
            _mm_prefetch((const char*)(src + 512), _MM_HINT_NTA);
            src += 256;
            _mm256_storeu_si256((((__m256i*)dst) + 0), c0);
            _mm256_storeu_si256((((__m256i*)dst) + 1), c1);
            _mm256_storeu_si256((((__m256i*)dst) + 2), c2);
            _mm256_storeu_si256((((__m256i*)dst) + 3), c3);
            _mm256_storeu_si256((((__m256i*)dst) + 4), c4);
            _mm256_storeu_si256((((__m256i*)dst) + 5), c5);
            _mm256_storeu_si256((((__m256i*)dst) + 6), c6);
            _mm256_storeu_si256((((__m256i*)dst) + 7), c7);
            dst += 256;
        }
    }
    else {		// big memory copy
        __m256i c0, c1, c2, c3, c4, c5, c6, c7;
        /* __m256i c0, c1, c2, c3, c4, c5, c6, c7; */

        _mm_prefetch((const char*)(src), _MM_HINT_NTA);

        if ((((size_t)src) & 31) == 0) {	// source aligned
            for (; size >= 256; size -= 256) {
                c0 = _mm256_load_si256(((const __m256i*)src) + 0);
                c1 = _mm256_load_si256(((const __m256i*)src) + 1);
                c2 = _mm256_load_si256(((const __m256i*)src) + 2);
                c3 = _mm256_load_si256(((const __m256i*)src) + 3);
                c4 = _mm256_load_si256(((const __m256i*)src) + 4);
                c5 = _mm256_load_si256(((const __m256i*)src) + 5);
                c6 = _mm256_load_si256(((const __m256i*)src) + 6);
                c7 = _mm256_load_si256(((const __m256i*)src) + 7);
                _mm_prefetch((const char*)(src + 512), _MM_HINT_NTA);
                src += 256;
                _mm256_stream_si256((((__m256i*)dst) + 0), c0);
                _mm256_stream_si256((((__m256i*)dst) + 1), c1);
                _mm256_stream_si256((((__m256i*)dst) + 2), c2);
                _mm256_stream_si256((((__m256i*)dst) + 3), c3);
                _mm256_stream_si256((((__m256i*)dst) + 4), c4);
                _mm256_stream_si256((((__m256i*)dst) + 5), c5);
                _mm256_stream_si256((((__m256i*)dst) + 6), c6);
                _mm256_stream_si256((((__m256i*)dst) + 7), c7);
                dst += 256;
            }
        }
        else {							// source unaligned
            for (; size >= 256; size -= 256) {
                c0 = _mm256_loadu_si256(((const __m256i*)src) + 0);
                c1 = _mm256_loadu_si256(((const __m256i*)src) + 1);
                c2 = _mm256_loadu_si256(((const __m256i*)src) + 2);
                c3 = _mm256_loadu_si256(((const __m256i*)src) + 3);
                c4 = _mm256_loadu_si256(((const __m256i*)src) + 4);
                c5 = _mm256_loadu_si256(((const __m256i*)src) + 5);
                c6 = _mm256_loadu_si256(((const __m256i*)src) + 6);
                c7 = _mm256_loadu_si256(((const __m256i*)src) + 7);
                _mm_prefetch((const char*)(src + 512), _MM_HINT_NTA);
                src += 256;
                _mm256_stream_si256((((__m256i*)dst) + 0), c0);
                _mm256_stream_si256((((__m256i*)dst) + 1), c1);
                _mm256_stream_si256((((__m256i*)dst) + 2), c2);
                _mm256_stream_si256((((__m256i*)dst) + 3), c3);
                _mm256_stream_si256((((__m256i*)dst) + 4), c4);
                _mm256_stream_si256((((__m256i*)dst) + 5), c5);
                _mm256_stream_si256((((__m256i*)dst) + 6), c6);
                _mm256_stream_si256((((__m256i*)dst) + 7), c7);
                dst += 256;
            }
        }
        _mm_sfence();
    }

    memcpy_tiny(dst, src, size);
    _mm256_zeroupper();

    return destination;
}





#ifdef __MEMCPY_TESTING_CODE

//=====================================================================
//
// FastMemcpy.c - skywind3000@163.com, 2015
//
// feature:
// 50% speed up in avg. vs standard memcpy (tested in vc2012/gcc4.9)
//
//=====================================================================




unsigned int gettime(void)
{
    #if (defined(_WIN32) || defined(WIN32))
    return timeGetTime();
    #else
    static struct timezone tz={ 0,0 };
    struct timeval time;
    gettimeofday(&time,&tz);
    return (time.tv_sec * 1000 + time.tv_usec / 1000);
    #endif
}

void sleepms(unsigned int millisec)
{
#if defined(_WIN32) || defined(WIN32)
    Sleep(millisec);
#else
    usleep(millisec * 1000);
#endif
}



void benchmark(int dstalign, int srcalign, size_t size, int times)
{
    char *DATA1 = (char*)malloc(size + 64);
    char *DATA2 = (char*)malloc(size + 64);
    size_t LINEAR1 = ((size_t)DATA1);
    size_t LINEAR2 = ((size_t)DATA2);
    char *ALIGN1 = (char*)(((64 - (LINEAR1 & 63)) & 63) + LINEAR1);
    char *ALIGN2 = (char*)(((64 - (LINEAR2 & 63)) & 63) + LINEAR2);
    char *dst = (dstalign)? ALIGN1 : (ALIGN1 + 1);
    char *src = (srcalign)? ALIGN2 : (ALIGN2 + 3);
    unsigned int t1, t2;
    int k;

    sleepms(100);
    t1 = gettime();
    for (k = times; k > 0; k--) {
        memcpy(dst, src, size);
    }
    t1 = gettime() - t1;
    sleepms(100);
    t2 = gettime();
    for (k = times; k > 0; k--) {
        memcpy_fast(dst, src, size);
    }
    t2 = gettime() - t2;

    free(DATA1);
    free(DATA2);

    printf("result(dst %s, src %s): memcpy_fast=%dms memcpy=%d ms\n",
        dstalign? "aligned" : "unalign",
        srcalign? "aligned" : "unalign", (int)t2, (int)t1);
}


void bench(int copysize, int times)
{
    printf("benchmark(size=%d bytes, times=%d):\n", copysize, times);
    benchmark(1, 1, copysize, times);
    benchmark(1, 0, copysize, times);
    benchmark(0, 1, copysize, times);
    benchmark(0, 0, copysize, times);
    printf("\n");
}


void random_bench(int maxsize, int times)
{
    static char A[11 * 1024 * 1024 + 2];
    static char B[11 * 1024 * 1024 + 2];
    static int random_offsets[0x10000];
    static int random_sizes[0x8000];
    unsigned int i, p1, p2;
    unsigned int t1, t2;
    for (i = 0; i < 0x10000; i++) {	// generate random offsets
        random_offsets[i] = rand() % (10 * 1024 * 1024 + 1);
    }
    for (i = 0; i < 0x8000; i++) {	// generate random sizes
        random_sizes[i] = 1 + rand() % maxsize;
    }
    sleepms(100);
    t1 = gettime();
    for (p1 = 0, p2 = 0, i = 0; i < times; i++) {
        int offset1 = random_offsets[(p1++) & 0xffff];
        int offset2 = random_offsets[(p1++) & 0xffff];
        int size = random_sizes[(p2++) & 0x7fff];
        memcpy(A + offset1, B + offset2, size);
    }
    t1 = gettime() - t1;
    sleepms(100);
    t2 = gettime();
    for (p1 = 0, p2 = 0, i = 0; i < times; i++) {
        int offset1 = random_offsets[(p1++) & 0xffff];
        int offset2 = random_offsets[(p1++) & 0xffff];
        int size = random_sizes[(p2++) & 0x7fff];
        memcpy_fast(A + offset1, B + offset2, size);
    }
    t2 = gettime() - t2;
    printf("benchmark random access:\n");
    printf("memcpy_fast=%dms memcpy=%dms\n\n", (int)t2, (int)t1);
}


#ifdef _MSC_VER
#pragma comment(lib, "winmm.lib")
#endif

int main(void)
{
#if 1
    bench(32, 0x1000000);
    bench(64, 0x1000000);
    bench(512, 0x800000);
    bench(1024, 0x400000);
#endif
    bench(4096, 0x80000);
    bench(8192, 0x40000);
#if 1
    bench(1024 * 1024 * 1, 0x800);
    bench(1024 * 1024 * 4, 0x200);
#endif
    bench(1024 * 1024 * 8, 0x100);

    random_bench(2048, 8000000);

    return 0;
}




#endif// __MEMCPY_TESTING_CODE
#endif // __HAVE_AVX__
#endif // MEMCPY_USE_SKYWIND_FAST
