#pragma once
#ifndef MEMORY_FUNCTION_MACROS_H
#define MEMORY_FUNCTION_MACROS_H

#ifdef __HAVE_MALLOC_H__
#include <malloc.h>
#endif // __HAVE_MALLOC_H__
#include <stdlib.h>
#include <string.h>

#include "../binary_c_code_options.h"
#include "../binary_c_structures.h"
#include "../breakpoints/breakpoints_prototypes.h"
#include "../binary_c_exit_prototypes.h"
#include "memory_alignment_checks.h"

#ifdef CODESTATS
static void increment_codestat(const unsigned int n,
                               const char *file,
                               const int line);

static void Maybe_unused increment_codestat(const unsigned int n,
                                            const char *file,
                                            const int line)
{
    codestats.counters[n]++;
    struct codestats_entry_t *array = codestats.entries[n];
    unsigned int i;
    const int vb = 0;
    if(vb)printf("CODESTAT search n=%u file=%s line=%d\n",n,file,line);

    if(array!=NULL)
    {
        /* search for hash entry */
        for(i=0;i<codestats.nentries[n];i++)
        {
            if(vb)printf("CODESTAT CF args %d %s to entry %u %s\n",
                         line,
                         file,
                         codestats.entries[n][i].line,
                         codestats.entries[n][i].file);
            if(line == codestats.entries[n][i].line &&
               Strings_equal(codestats.entries[n][i].file,file))
            {
                /*
                 * match
                 */
                codestats.entries[n][i].count++;
                if(vb)printf("CODESTAT FOUND at %u,%u -> new count %lu\n",
                             n,
                             i,
                             codestats.entries[n][i].count);
                return;
            }
        }
    }
    /* not found */

    if(vb)printf("CODESTAT : NOT FOUND\n");
    /* allocate new entry */
    i = codestats.nentries[n];
    codestats.entries[n] = realloc(codestats.entries[n],
                                   (i+1)*sizeof(struct codestats_entry_t));

    codestats.entries[n][i].file = malloc(sizeof(char)*(strlen(file)+2));
    strlcpy(codestats.entries[n][i].file,
            file,
            strlen(file)+1);
    codestats.entries[n][i].line = line;
    codestats.entries[n][i].count = 1;

    /* up counter */
    codestats.nentries[n]++;
}
#endif


/*
 * Memory function macros for binary_c
 *
 * We have custom Malloc, Calloc and Realloc to replace
 * the system calls (or use them, whatever you like).
 *
 * There are also macros for allocating and clearing pieces
 * of memory, such as GSL vectors and functions.
 */

/*
 * "safe" freeing of memory via the Safe_free macro,
 * which enforces a NULL in the pointer after a call
 * to free.
 * Note tha the PTR must actually be a pointer (i.e. an lvalue),
 * not an expression.
 *
 * The _Generic() checks for star or stardata struct pointers
 * being passed in. These have their own functions (free_star and
 * free_stardata) which you should use.
 */
#define Safe_free(PTR)                                                  \
    if(_Generic((PTR),                                                  \
                struct star_t *: 1,                                     \
                struct stardata_t * : 2,                                \
                struct tmpstore_t * : 3,                                \
                struct persistent_data_t * : 4,                         \
                struct store_t * : 5,                                   \
                struct string_array_t * : 6,                            \
                struct int_array_t * : 7,                               \
                struct unsigned_int_array_t * : 8,                      \
                struct Boolean_array_t * : 9,                           \
                struct double_array_t * : 10,                           \
                struct cdict_t * : 11,                                  \
                struct data_table_t * : 12,                             \
                default: 0) != 0)                                       \
    {                                                                   \
        printf("Safe_free of pointer of type %s which should use its own free_* function. Oops! I will give you a backtrace: you should fix this.\n", \
               _Generic((PTR),                                          \
                        struct star_t *: "star_t",                      \
                        struct stardata_t * : "stardata_t",             \
                        struct tmpstore_t * : "tmpstore_t",             \
                        struct persistent_data_t * : "persistent_data_t", \
                        struct store_t * : "store_t",                   \
                        struct string_array_t * : "string_array_t",     \
                        struct int_array_t * : "int_array_t",           \
                        struct unsigned_int_array_t * : "unsigned_int_array_t", \
                        struct Boolean_array_t * : "Boolean_array_t",   \
                        struct double_array_t * : "double_array_t",     \
                        struct cdict_t * : "cdict_t",                   \
                        struct data_table_t * : "data_table_t",         \
                        default: "unknown type"));                      \
        fprintf(stderr,                                                 \
                "Safe_free of pointer of type %s which should use its own free_* function. Oops! I will give you a backtrace: you should fix this.\n", \
                _Generic((PTR),                                         \
                         struct star_t *: "star_t",                     \
                         struct stardata_t * :"stardata_t",             \
                         default: "unknown type"));                     \
        Backtrace;                                                      \
        Exit_binary_c_no_stardata(2,"Safe_free type failure");          \
    }                                                                   \
    Safe_free_nocheck(PTR);

/*
 * Safe free with no check of the pointer type
 */
#define Safe_free_nocheck(PTR)                  \
    if(likely((PTR) != NULL))                   \
    {                                           \
        free(PTR);                              \
        (PTR) = NULL;                           \
    };

/*
 * As Safe_free but without the NULL setting: this should be considered
 * unsafe, but is still better than just freeing the pointer without
 * a NULL check.
 */
#define Unsafe_free(PTR)                        \
    if(likely((PTR)!=NULL))                     \
    {                                           \
        free(PTR);                              \
    }

/*
 * Check if a pointer has been allocated correctly.
 */
#ifdef ALLOC_CHECKS
#define _Alloc_check(P)                                  \
    if(unlikely((P)==NULL))                              \
    {                                                    \
        fprintf(stderr,"Memory allocation failed.\n");   \
        Backtrace;                                       \
        Exit_binary_c_no_stardata(                       \
            BINARY_C_ALLOC_FAILED,                                \
            "Memory allocation failed.\n"                \
            );                                           \
    }
#else
#define _Alloc_check(P) /* _Alloc_check(P) do nothing */
#endif

/*
 * Macro to choose the size of an array so it is forced
 * to be memory-aligned. Note: this uses ints, you might
 * want size_t instead, but so far this has caused no problems.
 */
#ifdef ALIGNSIZE
#define Aligned_size(N) ( ALIGNSIZE * ( (int) (((int)(N))/((int)ALIGNSIZE)) + 1  ))
#else
#define Aligned_size(N) (N)
#endif

/*
 * Wrappers for attribute and alignas() which allow
 * for ALIGNSIZE to be undef
 */
#ifdef ALIGNSIZE
#define Aligned __attribute__ ((aligned (ALIGNSIZE)))
#define Alignas alignas(ALIGNSIZE)
#else

#define Aligned
#define Alignas
#endif // ALIGNSIZE

/*
 * Choose memory allocation model.
 *
 * The .h file included defines the
 * macros Malloc, Calloc and Realloc
 * for the user.
 */
#if MEMORY_ALLOCATION_MODEL == MEMORY_ALLOCATION_NATIVE
#include "memory_alloc_native.h"
#elif MEMORY_ALLOCATION_MODEL == MEMORY_ALLOCATION_ALIGNED
#include "memory_alloc_aligned.h"
#elif MEMORY_ALLOCATION_MODEL == MEMORY_ALLOCATION_POSIX_ALIGNED
#include "memory_alloc_posix_aligned.h"
#else
#warning No memory allocation model is defined! This is an error.
#endif


/*
 * Alloc_or_clear makes sure pointer P points
 * to a zeroed piece of memory of size S.
 * If P is NULL, this is done with Calloc.
 * If P is non-NULL, the memory at P is assumed to
 * be of the correct size, and is then memset to zero.

 * There are three variants:
 *
 * Clear_stack_location is for an array on the stack,
 * so simply clears it.
 *
 * Alloc_or_clear_pointer is for an array ALLOC'd on the heap
 *
 */

#define Clear_stack_location(P,S)               \
    memset((P),0,(S)/sizeof(*(P)));

#define Clear_heap_location(P,S)                \
    memset((P),0,(S));

#define Alloc_or_clear_pointer(P,S)                     \
    if((P)!=NULL)                                       \
    {                                                   \
        Clear_heap_location((P),(S));                   \
    }                                                   \
    else                                                \
    {                                                   \
        (P)=Calloc(1,(S));                              \
        _Alloc_check(P);                                \
    }



#define Alloc_or_clear(P,S) Clear_stack_location((P),(S))


#include <gsl/gsl_vector.h>
#include <gsl/gsl_multiroots.h>
/*
 * Wrappers for GSL allocation functions
 * which also perform binary_c's standard alloc checks
 */

#define New_GSL_vector(N) _New_GSL_vector((N))

static inline void *_New_GSL_vector(const int n);
static inline void *_New_GSL_vector(const int n)
{
    gsl_vector * v = gsl_vector_alloc(n);
    _Alloc_check(v);
    return v;
}
#define Safe_free_GSL_vector(PTR)                                       \
    if(likely((PTR)!=NULL))                                             \
    {                                                                   \
        gsl_vector_free(PTR);                                           \
        (PTR)=NULL;                                                     \
    };

#define GSL_multiroot_fsolver_ALLOC(T,N) _GSL_multiroot_fsolver_alloc((T),(N))

static inline void *_GSL_multiroot_fsolver_alloc(const gsl_multiroot_fsolver_type * T, size_t n);
static inline void *_GSL_multiroot_fsolver_alloc(const gsl_multiroot_fsolver_type * T, size_t n)
{
    gsl_multiroot_fsolver * s = gsl_multiroot_fsolver_alloc (T, n);
    _Alloc_check(s);
    return s;
}

#define Safe_free_GSL_multiroot_fsolver(PTR)    \
    if(likely((PTR)!=NULL))                     \
    {                                           \
        gsl_multiroot_fsolver_free(PTR);        \
        (PTR)=NULL;                             \
    };

/*
 * Override standard memcpy with Skywind's fast_memcpy?
 */
#ifdef MEMCPY_USE_SKYWIND_FAST
#undef memcpy
#define memcpy memcpy_fast
#ifdef HAVE_AVX
#include "FastMemcpy_Avx.h"
#else
#include "FastMemcpy.h"
#endif
#endif

/*
 * Override standard memcpy/memmove with Apex' functions?
 */
#ifdef MEMCPY_USE_APEX
#undef memmove
#undef memcpy
#define memmove apex_memmove
#define memcpy apex_memcpy
#endif


/*
 * Other memcpys
 */
#ifdef USE_MEMCPY_DDR4
#undef memcpy
#define memcpy fastMemcpy_ddr4
#endif

#ifdef MEMCPY_USE_ASMLIB
#undef memcpy
#define memcpy A_memcpy
#endif


#ifdef MEMCPY_USE_STRIDED
/*
 * Override standard memcpy with strided_memcpy.
 *
 * stride/chars   runtime/s
 * 1              1.63
 * 2              0.96
 * 4              0.67
 * 8              0.47
 * 16             0.33
 * 32             0.26
 * 64             0.24
 * 128            0.23
 * 256            0.23
 * 512            0.22
 * 1024           0.21
 * 2048           0.22
 */

#undef memcpy
#define STRIDED_MEMCPY_CHUNK (2048*sizeof(char))
#define memcpy(DST,SRC,SIZE)                    \
    strided_memcpy((void*)(DST),                        \
                   (void*)(SRC),                        \
                   (SIZE),                              \
                   (STRIDED_MEMCPY_CHUNK))


#endif // MEMCPY_USE_STRIDED


/*
 * Override memcpy with CODESTATS version
 */
#if defined CODESTATS && !defined memcpy
#define memcpy(D,S,N) codestats_memcpy((D),(S),(N),__FILE__,__LINE__)
#endif // CODESTATS && !memcpy

/*
 * Malloc with memcpy, where TYPE is
 * a variable or struct type, as would be
 * passed to sizeof(), and FROM is its
 * memory location.
 */
#define Malloc_from(FROM,TYPE)                          \
    memcpy(Malloc(sizeof(TYPE)),(FROM),sizeof(TYPE))

/*
 * stardata masking function:
a *
 * S is the stardata, V is the variable
 *
 * Here we set
 * 1) the mask contents to equal the value in stardata (at the
 *    appropriate offset because they're structures of identical
 *    definition)
 * 2) Set the bytes correspdoning to this value in the mask to 1
 *
 * Note you should set up the mask with New_stardata_mask to
 * ensure it is all zero prior to using Stardata_mask()
 *
 */
#define Stardata_mask(S,V)                                              \
    {                                                                   \
        /* printf("Apply mask to %s\n",Stringify(V)); */                \
        struct stardata_t * const _mask = S->model.restore_mask;        \
        struct stardata_t * const _mask_contents =                      \
            S->model.restore_mask_contents;                             \
        /* set the mask content */                                      \
        _mask_contents->V = S->V;                                       \
        /* set the masking bytes */                                     \
        char * _p = (char *) &_mask->V;                                 \
        /*char * _start = S;*/                                          \
        /* const char * _start = (char *) _mask;  */                    \
        char * const _end = (char *) _p + sizeof(_mask->V);             \
        while(_p<_end)                                                  \
        {                                                               \
            /* printf("Mask offset %zu\n",(size_t)(_p - _start)); */    \
            *_p = 1;                                                    \
            _p++;                                                       \
        }                                                               \
    }


/*
 * Macro to prevent us using memcmp and associatedly forgetting
 * the ==0
 */
#define Memory_equal(A,B,C) (memcmp((A),(B),(C))==0)


/*
 * Given a number of bytes, N, return a float in bytes, Kbytes,
 * Mbytes or Gbytes, along with a unit string, suitable for
 * a format string like "%6.2f %s".
 *
 * Two versions are provided: one for decimal (power ten, kilobytes)
 * counting and one for binary (power two) counting (e.g. kibibytes)
 */
#define Mem_size_string_decimal(N)                              \
    (                                                           \
        (float)                                                 \
        (                                                       \
            (N) < KILOBYTE ? (N) :                              \
            (N) < MEGABYTE ? ((float)(N)/(float)KILOBYTE) :     \
            (N) < GIGABYTE ? ((float)(N)/(float)MEGABYTE) :     \
            (N) < TERABYTE ? ((float)(N)/(float)GIGABYTE) :     \
            (N) < PETABYTE ? ((float)(N)/(float)TERABYTE) :     \
            ((float)(N) / (float)PETABYTE)                      \
            )                                                   \
        )                                                       \
    ,                                                           \
        (                                                       \
            (N) < KILOBYTE ? ((N) == 1 ? "Byte" : "Bytes") :    \
            (N) < MEGABYTE ? "KBytes" :                         \
            (N) < GIGABYTE ? "MBytes" :                         \
            (N) < TERABYTE ? "GBytes" :                         \
            (N) < PETABYTE ? "TBytes" :                         \
            "PBytes"                                            \
            )

#define Mem_size_string_binary(N)                               \
    (                                                           \
        (float)                                                 \
        (                                                       \
            (N) < KIBIBYTE ? (N) :                              \
            (N) < MEBIBYTE ? ((float)(N)/(float)KIBIBYTE) :     \
            (N) < GIBIBYTE ? ((float)(N)/(float)MEBIBYTE) :     \
            (N) < TEBIBYTE ? ((float)(N)/(float)GIBIBYTE) :     \
            (N) < PEBIBYTE ? ((float)(N)/(float)TEBIBYTE) :     \
            ((float)(N) / (float)PEBIBYTE)                      \
            )                                                   \
        )                                                       \
    ,                                                           \
        (                                                       \
            (N) < KIBIBYTE ? ((N) == 1 ? "Byte" : "Bytes") :    \
            (N) < MEBIBYTE ? "KiBytes" :                        \
            (N) < GIBIBYTE ? "MiBytes" :                        \
            (N) < TEBIBYTE ? "GiBytes" :                        \
            (N) < PEBIBYTE ? "TiBytes" :                        \
            "PiBytes"                                           \
            )

/*
 * Allocate memory for a new preferences struct
 */
#define New_preferences (Calloc(1,sizeof(struct preferences_t)))

/*
 * Allocate memory for a new tmpstore struct
 */
#define New_tmpstore (Calloc(1,sizeof(struct tmpstore_t)))

/*
 * Allocate memory for a new store struct
 */
#define New_store (Calloc(1,sizeof(struct store_t)))

/*
 * Wrappers for _Malloc and _Calloc. These are here
 * so you can wrap memory-allocation calls with your
 * own code should you wish to.
 */
#define Malloc(P) _Malloc(P)
#define Calloc(N,P) _Calloc((N),(P))

/*
 * Realloc() is a convenience macro that
 * calls _Realloc(), checks the result, then returns.
 *
 * This is very handy when you want to do something like
 *
 * p = Realloc(p,...)
 *
 * which usually would lose the contents of p when realloc()
 * fails and returns NULL which is set in p.
 * Instead, on failure, we free (P) and call
 * Binary_c_exit() for a safe shutdown.
 *
 * Note that this might not be what you want to do,
 * in which case use
 *
 * q = _Realloc(p,...);
 * if(q == NULL)
 * {
 *    ... act appropriately here ...
 * }
 *
 * and capture the error yourself.
 *
 * Whatever you do, don't do this:
 *
 * p = _Realloc(p,...);
 */
#undef Concat3
#define Concat3(A,B,C) A##B##C
#define Realloc(P,S) __Realloc_implementation(Realloc,__COUNTER,(P),(S))
#define __Realloc_implementation(LABEL,LINE,P,S)                        \
    __extension__                                                       \
    ({                                                                  \
        void * const Concat3(LABEL,LINE,p) = _Realloc((P),(S));         \
        if(Concat3(LABEL,LINE,p) == NULL)                               \
        {                                                               \
            /* Error */                                                 \
            Safe_free(P);                                               \
            if(stardata == NULL)                                        \
            {                                                           \
                Exit_binary_c_no_stardata(BINARY_C_ALLOC_FAILED,        \
                                          "Realloc failed to allocate %zu bytes to replace pointer %p.", \
                                          (size_t)(S),                  \
                                          (void*)(P));                  \
            }                                                           \
            else                                                        \
            {                                                           \
                Exit_binary_c(BINARY_C_ALLOC_FAILED,                    \
                              "Realloc failed to allocate %zu bytes to replace pointer %p.", \
                              (size_t)(S),                              \
                              (void*)(P));                              \
            }                                                           \
        }                                                               \
        (Concat3(LABEL,LINE,p));                                        \
    })

/*
 * Recalloc() is a convenience macro that reallocs
 * then clears the memory.
 */
#define Recalloc(P,S)                           \
    recalloc(stardata,(P),(S))


/*
 * Recalloc_new() is a convenience macro that reallocs
 * then clears the memory that is newly allocated.
 *
 * We need to know
 * P : the old pointer
 * SNEW : the new size
 * NOLD : the old number of items of size sizeof(P)
 */
#define Recalloc_new(P,SNEW,NOLD)               \
    realloc_new(stardata,                       \
                (S),                            \
                (SNEW),                         \
                (SOLD))

#endif // MEMORY_FUNCTION_MACROS_H
