#include "binary_c.h"
No_empty_translation_unit_warning;

#ifdef ALIGNSIZE

void *aligned_memcpy(void *dest, void *src, size_t size)
{
#ifdef CODESTATS
    extern struct codestats_t codestats;
    codestats.counters[CODESTAT_MEMCPY]++;
#endif
    void * d = Assume_aligned(dest);
    void * s = Assume_aligned(src);
    return memcpy(d,s,size);
}

#endif // ALIGNSIZE
