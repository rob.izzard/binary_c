#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "memory_alignment_checks.h"

/*
 * Make a new star struct and return a pointer to it.
 *
 * If src is non-NULL, copy its contents into dest.
 *
 * If src is NULL, the contents of the new struct are zeroed.
 */

#undef Cprint
#define Cprint(...)

Malloc_like
struct star_t * new_star(struct stardata_t * const Restrict stardata,
                         const struct star_t * const src)
{
    /*
     * Allocate destination memory
     */
    struct star_t * const dest = alloc_star(stardata);
    Cprint("alloc new star %p %p\n",
           (void*)dest,
           (void*)dest->mint);
    if(dest == NULL)
    {
        Exit_binary_c(BINARY_C_ALLOC_FAILED,
                      "Could not Malloc space for a new star_t struct.");
    }
    else
    {
        /*
         * If src is NULL, everything should be zero in the dest.
         *
         * If src is non-NULL, copy the contents.
         */
        if(src == NULL)
        {
            Cprint("zero star\n");
            zero_star(dest);
        }
        else
        {
            Cprint("copy star\n");
            copy_star(stardata,src,dest);
        }
    }

    /*
     * Assume the star sits in stardata and is
     * a normal star (rather than an embedded binary
     * proxy). If your star is a proxy then you have
     * to set this manually.
     */
    dest->type = STAR_NORMAL;
    dest->parent = stardata;
    dest->ancestor = stardata->ancestor;
    dest->stardata = NULL;

    return dest;
}
