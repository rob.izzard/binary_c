#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "memory_alignment_checks.h"

#undef Cprint
#define Cprint(...)
//#define Cprint(...) Dprint("COPY (%u) src 1-X=%g : ",i++,1.0-nucsyn_totalX(src->Xenv));Dprint(__VA_ARGS__);fflush(NULL);

/*
 * Copy the star in src to dest, which
 * should already be allocated memory.
 */
void * copy_star(struct stardata_t * const stardata Maybe_unused,
                 const struct star_t * const Restrict src,
                 struct star_t * const Restrict dest)
{
    unsigned int i Maybe_unused = 0; /* logging counter */

    /*
     * Save pointers in the dest that should be
     * restored after the memcpy
     */
    Cprint("copy_star %d: src %p (bse=%p bse->timescales=%p), dest %p (bse=%p bse->timescales=%p)\n",
           src ? src->starnum : -1,
           (void*)src,
           (void*)(src ? src->bse : NULL),
           (void*)((src &&src->bse) ? src->bse->timescales : NULL),
           (void*)dest,
           (void*)(dest ? dest->bse : NULL),
           (void*)((dest &&dest->bse) ? dest->bse->timescales : NULL)
        );

#ifdef MINT
    Cprint("MINT src: mint: %p, shells %p; dest: %p, shells %p\n",
           (void*)src->mint,
           (void*)(src->mint ? src->mint->shells : NULL),
           (void*)dest->mint,
           (void*)(dest->mint ? dest->mint->shells : NULL));

    struct mint_t * const mint_p = dest->mint;
    struct mint_shell_t * const mint_shells_p = dest->mint != NULL ? dest->mint->shells : NULL;
    const Shell_index mint_nshells = dest->mint != NULL ? dest->mint->nshells : 0;
#endif//MINT
#ifdef BSE
    struct BSE_data_t * const bse_p = dest->bse;
#endif//BSE
#if defined NUCSYN && \
    defined NUCSYN_ID_SOURCES
    Abundance * oldsource[SOURCE_NUMBER];
    Abundance * Xsource[SOURCE_NUMBER];

    memcpy(oldsource, dest->oldsource, sizeof(Abundance*)*SOURCE_NUMBER);
    memcpy(Xsource, dest->Xsource, sizeof(Abundance*)*SOURCE_NUMBER);
#endif

    /*
     * Copy most of the contents of the src star struct
     * to the dest star struct, note that this overwrites
     * the pointers in dest (which is why we saved them
     * above).
     */
    void * const ret =
        memcpy(dest, src, sizeof(struct star_t));
    Cprint("copied most of the struct\n");

    /*
     * Restore pointers
     */
#ifdef BSE
    dest->bse = bse_p;
    Cprint("restored bse pointers\n");
#endif//BSE

#ifdef MINT
    Cprint("restore mint from %p to %p\n",
           (void*)dest->mint,
           (void*)mint_p);
    dest->mint = mint_p;
    Cprint("restored MINT\n");
#endif//MINT

    /*
     * deep copy the contents of the pointers
     */
    if(src->stardata != NULL)
    {
        copy_stardata(src->stardata,
                      dest->stardata,
                      COPY_STARDATA_PREVIOUS_COPY,
                      COPY_STARDATA_PERSISTENT_FROM_POINTER);
        Cprint("Deep copied stardata\n");
    }

#if defined NUCSYN &&                           \
    defined NUCSYN_ID_SOURCES

    /* restore the dest->oldsource and dest->Xsource memory */
    memcpy(dest->oldsource, oldsource, sizeof(Abundance*)*SOURCE_NUMBER);
    memcpy(dest->Xsource, Xsource, sizeof(Abundance*)*SOURCE_NUMBER);

    /* set src's values in dest->oldsource and dest->Xsource */
    Sourceloop(j)
    {
        if(src->oldsource[j] != NULL)
        {
            if(dest->oldsource[j] == NULL)
            {
                dest->oldsource[j] = New_isotope_array;
            }
            Copy_abundances(src->oldsource[j],
                            dest->oldsource[j]);
        }

        if(src->Xsource[j] != NULL)
        {
            if(dest->Xsource[j] == NULL)
            {
                dest->Xsource[j] = New_isotope_array;
            }
            Copy_abundances(src->Xsource[j],
                            dest->Xsource[j]);
        }
    }
#endif


#ifdef BSE
    if(src->bse != dest->bse &&
       src->bse != NULL)
    {
        if(dest->bse == NULL)
        {
            dest->bse = Calloc(1,sizeof(struct BSE_data_t));
        }

#undef _copy
#define _copy(WHAT,N)                                           \
        if(src->bse->WHAT == NULL)                              \
        {                                                       \
            Cprint("src->bse->%s is NULL\n",#WHAT);             \
            Safe_free(dest->bse->WHAT);                         \
        }                                                       \
        else                                                    \
        {                                                       \
            Cprint("dest->bse->%s = %p (src->bse->%s = %p)\n",  \
                   #WHAT,                                       \
                   (void*)dest->bse->WHAT,                      \
                   #WHAT,                                       \
                   (void*)src->bse->WHAT);                      \
                                                                \
            if(dest->bse->WHAT == NULL)                         \
            {                                                   \
                /* need new alloc */                            \
                Cprint("malloc1\n");                            \
                dest->bse->WHAT =                               \
                    Malloc(sizeof(double)*N);                   \
                Cprint("now %p\n",(void*)dest->bse->WHAT);      \
            }                                                   \
            else if(dest->bse->WHAT == src->bse->WHAT)          \
            {                                                   \
                /* same space: need new alloc */                \
                Cprint("malloc2\n");                            \
                dest->bse->WHAT =                               \
                    Malloc(sizeof(double)*N);                   \
                Cprint("now %p\n",(void*)dest->bse->WHAT);      \
            }                                                   \
            if(dest->bse->WHAT != src->bse->WHAT)               \
            {                                                   \
                /* use existing */                              \
                Cprint("memmove(%p, %p)\n",                     \
                       (void*)dest->bse->WHAT,                  \
                       (void*)src->bse->WHAT);                  \
                memmove(dest->bse->WHAT,                        \
                        src->bse->WHAT,                         \
                        sizeof(double)*N);                      \
            }                                                   \
        }

        Cprint("pre-copy timescales\n");
        _copy(timescales,TSCLS_ARRAY_SIZE);
        Cprint("pre-copy GB\n");
        _copy(GB,GB_ARRAY_SIZE);
        Cprint("pre-copy luminosities\n");
        _copy(luminosities,LUMS_ARRAY_SIZE);
        Cprint("post-copy luminosities\n");
    }
#endif //BSE

#ifdef MINT
    if(src->mint != dest->mint &&
       src->mint != NULL)
    {
        if(dest->mint == NULL)
        {
            dest->mint = Malloc(sizeof(struct mint_t));
            Cprint("dest->mint is NULL, alloced to %p\n",
                   (void*)dest->mint);
        }

        Cprint("memcpy mint %p %p\n",
               (void*)dest->mint,
               (void*)src->mint);

        /*
         * Copy main MINT pointer
         */
        memcpy(dest->mint,
               src->mint,
               sizeof(struct mint_t));

        /*
         * Restore shells
         */
        Cprint("restore mint->shells from %p to %p\n",
               (void*)dest->mint->shells,
               (void*)mint_shells_p);
        dest->mint->shells = mint_shells_p;

        if(src->mint->shells == NULL)
        {
            /*
             * No shells to copy
             */
            Safe_free(dest->mint->shells);
        }
        else
        {
            /*
             * Copy shells
             */
            const size_t shellssize = sizeof(struct mint_shell_t)*
                src->mint->nshells;

            if(dest->mint->shells == NULL &&
               dest->mint->nshells > 0)
            {
                /*
                 * Require new memory for shells
                 */
                dest->mint->shells = Malloc(shellssize);
                Cprint("MINT shells Malloc at %p\n",
                       (void*)dest->mint->shells);
            }
            else if(dest->mint->shells != NULL &&
                    mint_nshells != dest->mint->nshells)
            {
                /*
                 * Shell number has changed, realloc
                 * or free (if new shell number is zero).
                 */
                if(shellssize == 0)
                {
                    Safe_free(dest->mint->shells);
                    Cprint("MINT shells set to %p\n",
                           (void*)dest->mint->shells);
                }
                else
                {
                    Cprint("realloc MINT shells to size %zu, was %p\n",
                           shellssize,
                           (void*)dest->mint->shells);
                    dest->mint->shells = Realloc(dest->mint->shells,
                                                 shellssize);
                }
            }

            Cprint("copy MINT shells %p to %p size %zu\n",
                   (void*)src->mint->shells,
                   (void*)dest->mint->shells,
                   shellssize);

            /*
             * Copy shells' contents if required
             */
            if(shellssize > 0 &&
               src->mint->shells != NULL &&
               dest->mint->shells != NULL)
            {
                memcpy(dest->mint->shells,
                       src->mint->shells,
                       shellssize);
            }
        }
    }
#endif//MINT

    Cprint("copy_star:out %p %p\n",
           (void*)src,
           (void*)dest);
#ifdef MINT
    Cprint("copy_star:out mint %p %p -> %p %p\n",
           (void*)src->mint,
           (void*)(src->mint ? src->mint->shells : NULL),
           (void*)dest->mint,
           (void*)(dest->mint ? dest->mint->shells : NULL));
#endif//MINT


    /*
     * If pointers are identical, this is an error so return NULL
     * (unless, in some cases, they are both NULL)
     */
    if(src == dest
#ifdef MINT
       ||
       (src->mint == dest->mint &&
        src->mint != NULL) ||
       (src->mint != NULL &&
        dest->mint != NULL &&
        src->mint->shells == dest->mint->shells &&
        src->mint->shells != NULL)
#endif//MINT
        )
    {
        return NULL;
    }

    return ret;
}
