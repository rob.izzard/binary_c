#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "memory_alignment_checks.h"

/*
 * Make a new stardata struct, with substructs
 * allocated as required by preferences.
 */

Malloc_like
struct stardata_t * new_stardata(struct preferences_t * const Restrict preferences)
{
    struct stardata_t * const stardata =
        Calloc(1,sizeof(struct stardata_t));

    for(int i=0;i<NUMBER_OF_STARS;i++)
    {
        alloc_star_contents(preferences,
                            &stardata->star[i]);
        stardata->star[i].starnum = i;
    }

    /*
     * Assume the stardata is an outer binary,
     * not something embedded. If you want to change
     * this, you have to do it yourself after this function
     * returns.
     */
    stardata->type = STARDATA_OUTER;
    stardata->preferences = preferences;
    stardata->ancestor = NULL;

    return stardata;
}
