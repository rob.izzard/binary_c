#include "../binary_c.h"
No_empty_translation_unit_warning;


void Nonnull_some_arguments(1)
    main_allocations(struct stardata_t ** const new_stardata_p,
                     struct stardata_t *** const previous_stardatas,
                     struct preferences_t ** const preferences,
                     struct store_t ** store,
                     struct persistent_data_t ** persistent_data)
{
    /*
     * Allocate memory for new_stardata_p, previous_stardata,
     * preferences and store.
     *
     * new_stardata_p should never be NULL.
     * If *new_stardata_p is NULL, it is assigned memory.
     * *new_stardata_p is then cleared (all set to zero).
     *
     * If preferences is NULL, (*new_stardata_p)->preferences is
     * allocated space for a preferences struct.
     * If preferences is non-NULL, and *preferences is NULL,
     * we allocate space for a preferences struct and put it in
     * (*new_stardata_p)->preferences = *preferences.
     * If preferences and *preferences are non-NULL, *preferences
     * is used in *new_stardata_p, but is cleared.
     *
     * If store is NULL, (*new_stardata_p)->store is allocated space
     * for a store struct, and the store is built.
     * If store is non-NULL, but *store is NULL,
     * (*new_stardata_p)->store = *store is allocated space for a store
     * and the store is built.
     * If store and *store are non-NULL, (*new_stardata_p)->store is
     * set to *store, and we assume the store is already built.
     *
     * If previous_stardatas is non-NULL, it is used to fill
     * stardata->previous_stardatas.
     *
     * A few preferences are set. Perhaps this part should
     * be moved elsewhere.
     */

    /*
    fprintf(stdout,
            "ALLOC %p (%p) %p (%p) %p (%p) %p (%p)\n",
            (void*)new_stardata_p,
            (void*)new_stardata_p ? (void*)*new_stardata_p : NULL,
            (void*)previous_stardatas,
            (void*)previous_stardatas ? (void*)*previous_stardatas : NULL,
            (void*)preferences,
            (void*)preferences ? (void*)*preferences : NULL,
            (void*)store,
            (void*)store ? (void*)*store : NULL);
    */

    /*
     * The stardata structure contains all of the stuff
     * to describe the physics of
     * the binary star system - see binary_c_structures.h for details.
     */
    if(*new_stardata_p == NULL)
    {
        /*
         * No stardata provided, make one.
         *
         * We send preferences if we have it.
         */
        *new_stardata_p = new_stardata(preferences == NULL ? NULL : *preferences);
    }
    else
    {
        /*
         * Stardata is provided: erase it and its
         * substructures (e.g. for MINT).
         */
        clear_stardata(*new_stardata_p);
    }
    struct stardata_t * stardata Maybe_unused = *new_stardata_p;

    /*
     * Set up the permanent store
     */
    Dprint("Make store\n");
    if(store==NULL)
    {
        /*
         * We're passed in nothing, so set up a store in
         * new_stardata_p.
         */
        (*new_stardata_p)->store = Calloc(1,sizeof(struct store_t));
        build_store_contents(*new_stardata_p,
                             (*new_stardata_p)->store);
    }
    else if(*store==NULL)
    {
        /*
         * We're passed in a store pointer, but it points to
         * nothing. We thus know where to put the store, but
         * must allocate its contents.
         */
        (*new_stardata_p)->store = Calloc(1,sizeof(struct store_t));
        *store = (*new_stardata_p)->store;
        build_store_contents(*new_stardata_p,
                             (*new_stardata_p)->store);
    }
    else
    {
        /*
         * We are given an existing store: set it in new_stardata_p
         */
        (*new_stardata_p)->store = *store;
    }

    /*
     * Set up the preferences
     */
    Dprint("Make preferences\n");
    if(preferences==NULL)
    {
        /*
         * We require a whole new preferences and must make
         * memory for it.
         */
        (*new_stardata_p)->preferences = New_preferences;
    }
    else if(*preferences==NULL)
    {
        /*
         * We are given a place to put the preferences,
         * but no content.
         */
        *preferences = New_preferences;
        (*new_stardata_p)->preferences = *preferences;
    }
    else
    {
        /*
         * We are given preferences to use
         */
        (*new_stardata_p)->preferences = *preferences;
    }

    /*
     * The tmpstore should always be reset
     */
    free_tmpstore((*new_stardata_p)->tmpstore,TRUE);
    (*new_stardata_p)->tmpstore = New_tmpstore;

    /*
     * Set up pointers to various sub-structures and join
     * new_stardata_p and preferencs
     */
    Dprint("Init pointers\n");
    initialize_pointers(*new_stardata_p,
                        (*new_stardata_p)->preferences,
                        (*new_stardata_p)->store);

    /*
     * Build the tmpstore
     */
    Dprint("Build tmpstore contents");
    build_tmpstore_contents((*new_stardata_p)->tmpstore);

    /*
     * If we are passed in a persistent_data, use it,
     * otherwise allocate space for it
     */
    Dprint("Alloc persistent data (passed in %p)\n",(void*)persistent_data);
    if(persistent_data != NULL &&
       *persistent_data != NULL)
    {
        (*new_stardata_p)->persistent_data =
            *persistent_data;
    }
    else
    {
        build_persistent_data(*new_stardata_p);
    }

    /*
     * Set up command line argument array
     */
    Dprint("Call parse_arguments to set up arrays\n");
    parse_arguments(0,0,NULL,*new_stardata_p);

    Dprint("Set previous_stardatas\n");
    if(previous_stardatas != NULL)
    {
        /*
         * If previous_stardatas is non-null and contains data, use it.
         * Assume number of stardatas requires is the same.
         */
        (*new_stardata_p)->previous_stardatas = *previous_stardatas;
        (*new_stardata_p)->n_previous_stardatas = (*previous_stardatas[0])->n_previous_stardatas;
    }
    else
    {
        /*
         * We must have at least one previous_stardata,
         * so allocate that, and allow the rest to be
         * allocated on the fly.
         */
        (*new_stardata_p)->previous_stardatas = Malloc(sizeof(struct stardata_t *));
        (*new_stardata_p)->previous_stardatas[0] = new_stardata((*new_stardata_p)->preferences);
        (*new_stardata_p)->n_previous_stardatas = 1;
    }
    (*new_stardata_p)->previous_stardata = (*new_stardata_p)->previous_stardatas[0];

    /*
     * Set preferences and store in previous_stardata :
     * this is for debugging (diff_stardata) output
     */
    (*new_stardata_p)->previous_stardata->preferences = (*new_stardata_p)->preferences;
    (*new_stardata_p)->previous_stardata->store = (*new_stardata_p)->store;
    (*new_stardata_p)->previous_stardata->tmpstore = (*new_stardata_p)->tmpstore;

    /*
     * The struct containing runtime preferences :
     * set some to defaults.
     */
    (*new_stardata_p)->preferences->repeat = 1;
#ifdef BATCHMODE
    (*new_stardata_p)->preferences->batchmode = BATCHMODE_OFF;
    (*new_stardata_p)->preferences->batch_submode = BATCH_SUBMODE_NORMAL;
#endif

#ifdef BUFFERED_STACK
    /* default output to stdout */
    (*new_stardata_p)->preferences->output_type = OUTPUT_TO_STREAM;
    (*new_stardata_p)->preferences->output_stream = stdout;
    (*new_stardata_p)->preferences->stream_buffer = NULL;
    (*new_stardata_p)->preferences->stream_buffer_stacksize = 0;
#endif
    set_default_preferences((*new_stardata_p));

    Dprint("pointers initialized : new_stardata_p=%p preferences=%p previous_stardatas=%p stardata->preferences=%p stardata=%p stardata->preferences=%p\n",
           (void*)stardata,
           (void*)stardata->preferences,
           (void*)stardata->previous_stardatas,
           (void*)stardata->preferences,
           (void*)stardata,
           (void*)stardata->preferences);

    fool_cpu();
}
