#include "../binary_c.h"
No_empty_translation_unit_warning;
#include "memory_alignment_checks.h"


struct stardata_t *
Returns_nonnull Nonnull_all_arguments
copy_stardata(
    struct stardata_t  * Restrict const from,
    struct stardata_t  * Restrict const to,
    const unsigned int copy_previous_option,
    const unsigned int copy_persistent_option
    )
{
    /*
     * Copy stardata in "from" to "to", where from != to.
     *
     * copy_previous_option:
     *   COPY_STARDATA_PREVIOUS_NONE  :  do nothing, the new previous_stardatas stack is left NULL
     *   COPY_STARDATA_PREVIOUS_COPY  :  make copy of history data into new previous_stardatas
     *   COPY_STARDATA_MAINTAIN_FROM_STACK_POINTERS : use from's previous_stardatas stack pointers
     *   COPY_STARDATA_MAINTAIN_TO_STACK_POINTERS : use to's previous_stardatas stack pointers
     *
     * copy_persistent_option:
     *   COPY_STARDATA_PERSISTENT_NONE : do nothing, leave persistent data NULL
     *   COPY_STARDATA_PERSISTENT_FROM_POINTER : copy from's pointer, i.e. use the existing persistent_data
     *   COPY_STARDATA_PERSISTENT_COPY_SHALLOW : make a shallow copy of the persistent_data struct
     *   COPY_STARDATA_PERSISTENT_COPY_DEEP : make a deep copy of the persistent_data struct (calls copy_persistent)
     *
     * If to == from, exit with an error (using "from" as the reference stardata).
     *
     * Returns a pointer to "to".
     *
     * Neither from nor to should ever be NULL.
     *
     * Note: we should use deep_copy_stardata NOT memcpy on stardata structs.
     */
    if(to == from)
    {
        struct stardata_t * const stardata = (struct stardata_t *) from;
        Exit_binary_c(BINARY_C_POINTER_FAILURE,
                      "to (%p) == from (%p) in copy_stardata : this should not happen.",
                      (void*)to,
                      (void*)from);
    }
    else
    {
        if(copy_previous_option == COPY_STARDATA_PREVIOUS_NONE)
        {
            deep_copy_stardata(from,to);

            /*
             * Don't copy history: force this to be NULL
             */
            to->previous_stardatas = NULL;
            to->previous_stardata = NULL;
            to->n_previous_stardatas = 0;
        }
        else if(copy_previous_option == COPY_STARDATA_MAINTAIN_FROM_STACK_POINTERS)
        {
            /*
             * Preserve existing previous_stardata and stardata_stack
             * from "from". This is just a straight memcpy.
             */
            deep_copy_stardata(from,to);
        }
        else if(copy_previous_option == COPY_STARDATA_MAINTAIN_TO_STACK_POINTERS)
        {
            /*
             * Preserve existing previous_stardata and stardata_stack
             * from "to"
             */
            const int np = to->n_previous_stardatas;
            const int ns = to->n_stardata_stack;
            struct stardata_t ** const pstack = to->previous_stardatas;
            struct stardata_t ** const sstack = to->stardata_stack;
            deep_copy_stardata(from,to);
            to->previous_stardatas = pstack;
            if(pstack != NULL) to->previous_stardata = pstack[0];
            to->stardata_stack = sstack;
            to->n_previous_stardatas = np;
            to->n_stardata_stack = ns;
        }
        else if(copy_previous_option == COPY_STARDATA_PREVIOUS_COPY)
        {
            /*
             * Free existing previous stardatas
             */
            free_previous_stardatas(to);

            /*
             * Copy the data onto a new stack
             */
            deep_copy_stardata(from,to);

            /*
             * Make the new stack
             */
            to->previous_stardatas =
                Malloc(sizeof(struct stardata_t*) *
                       Max((unsigned int)1,from->n_previous_stardatas));

            if(from->n_previous_stardatas == 0)
            {
                /*
                 * We require a previous stardata but don't have one,
                 * so just copy stardata
                 */
                to->n_previous_stardatas = 1;
                to->previous_stardatas[0] = New_stardata_from(to);
            }
            else
            {
                /*
                 * Copy the from stack's data to to the to stack
                 */
                for(unsigned int i=0;i<from->n_previous_stardatas;i++)
                {
                    to->previous_stardatas[i] = new_stardata(from->preferences);
                    deep_copy_stardata(from->previous_stardatas[i],
                                       to->previous_stardatas[i]);
                }
                to->n_previous_stardatas = from->n_previous_stardatas;
            }
            to->previous_stardata = to->previous_stardatas[0];
        }
        else
        {
            struct stardata_t * const stardata = (struct stardata_t *) from;
            Exit_binary_c(BINARY_C_ALGORITHM_OUT_OF_RANGE,
                          "Unknown copy_stardata algorithm");
        }

        /*
         * How should we treat persistent_data?
         */
        if(copy_persistent_option == COPY_STARDATA_PERSISTENT_NONE)
        {
            /*
             * do nothing: leave as a NULL pointer
             */
            to->persistent_data = NULL;
        }
        else if(copy_persistent_option == COPY_STARDATA_PERSISTENT_FROM_POINTER)
        {
            /*
             * Just copy the persistent_data pointer
             */
            to->persistent_data = from->persistent_data;
        }
        else if(copy_persistent_option == COPY_STARDATA_PERSISTENT_COPY_SHALLOW)
        {
            /*
             * Perform a shallow copy of the persistent data structure.
             * Note that this requires a malloc.
             */
            to->persistent_data = Malloc(sizeof(struct persistent_data_t));
            memcpy(to->persistent_data,
                   from->persistent_data,
                   sizeof(struct persistent_data_t));
        }
        else if(copy_persistent_option == COPY_STARDATA_PERSISTENT_COPY_DEEP)
        {
            /*
             * Perform a deep copy of the persistent data structure
             * Note that this requires a malloc.
             */
            to->persistent_data = Malloc(sizeof(struct persistent_data_t));
            copy_persistent(from->persistent_data,
                            to->persistent_data);
        }
    }
    return to;
}
