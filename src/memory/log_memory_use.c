#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef MEMORY_USE
void log_memory_use(struct stardata_t * Restrict const stardata)
{
    /*
     * Log memory use of binary_c
     */
    if(stardata->tmpstore->memuse == NULL)
    {
        stardata->tmpstore->memuse = Malloc(sizeof(struct binary_c_memuse_t));
    }

    get_memory_use(stardata,
                   stardata->tmpstore->memuse);
    printf("MEM model %d at t=%g : VmRSS = %lu, VmHWM = %lu, VmSize = %lu, VmPeak = %lu\n",
           stardata->model.model_number,
           stardata->model.time,
           stardata->tmpstore->memuse->VmRSS,
           stardata->tmpstore->memuse->VmHWM,
           stardata->tmpstore->memuse->VmSize,
           stardata->tmpstore->memuse->VmPeak);

    CDict_stats(stardata->model.ensemble_cdict);
    if(0)
    {
        const RAM upper_limit = 1500000;
        if(stardata->tmpstore->memuse->VmRSS > upper_limit)
        {
            Exit_binary_c(1, "vmRSS = "__RAM_FORMAT_SPECIFIER__" > upper limit = "__RAM_FORMAT_SPECIFIER__" \n",
                          stardata->tmpstore->memuse->VmRSS,
                          upper_limit
                );
        }
    }
}
#endif // MEMORY_USE
