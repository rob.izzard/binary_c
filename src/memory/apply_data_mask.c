#include "../binary_c.h"
No_empty_translation_unit_warning;

/*
 * Given a piece of data (data) of known size (size)
 * copy the mask_contents (mask_contents) over the top
 * of data when the mask is 1, otherwise do not copy.
 *
 * Note: data, mask and mask_contents should not be NULL.
 *
 * The sizes of data, mask and mask_contents should be
 * identical.
 *
 * The types of the data, mask and mask_contents are all
 * char* ... this means you have to make unions to
 * pass in the data.
 */

void apply_data_mask(char * const Restrict data,
                     const char * const Restrict mask,
                     const char * const Restrict mask_contents,
                     const size_t size)
{

    /*
     * We map the data, mask and mask_contents to char*
     * pointers: this means the mask acts at the resolution
     * of sizeof(char) (== one byte).
     */
    char * const s = (char*) data;
    const char * const m = (char*) mask;
    const char * const c = (char*) mask_contents;
    size_t offset = 0;
    while(offset < size)
    {
        if(*(m+offset) == 1)
        {
            *(s+offset) = *(c+offset);
        }
        offset++;
    }
}
