#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef BSE

Malloc_like
struct BSE_data_t * new_BSE_data(void)
{
    /*
     * Make a new, fresh BSE data struct
     */
    struct BSE_data_t * const bse = Calloc(1,sizeof(struct BSE_data_t));
    bse->timescales = Calloc(1,sizeof(double)*TSCLS_ARRAY_SIZE);
    bse->luminosities = Calloc(1,sizeof(double)*LUMS_ARRAY_SIZE);
    bse->GB = Calloc(1,sizeof(double)*GB_ARRAY_SIZE);
    return bse;
}

#endif//BSE
