#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "memory_alignment_checks.h"

/*
 * Free a star and all its contents
 */
void free_star(struct star_t ** Restrict star)
{
    if(star != NULL &&
       *star != NULL)
    {
        free_star_contents(*star);
        Safe_free_nocheck(*star);
    }
}
