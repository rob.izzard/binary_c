#include "../binary_c.h"
No_empty_translation_unit_warning;


#define Tmp_free(P)                             \
    fflush(NULL);                               \
    Safe_free(tmpstore->P)

void free_tmpstore(struct tmpstore_t * Restrict const tmpstore,
                   const Boolean free_raw_buffer)
{
    /*
     * Free the contents of the temporary data store (tmpstore)
     * if it's non-null, and free the tmpstore itself
     */
    if(tmpstore != NULL)
    {
        Safe_free(tmpstore->default_preferences);
        Delete_data_table(tmpstore->tidal_multipliers);
        free_difflogstack(tmpstore->logstack);
        free_double_array(&tmpstore->target_times);
#ifdef KICK_CDF_TABLE
        Delete_data_table(tmpstore->cdf_table);
#endif // KICK_CDF_TABLE
#ifdef MINT
        MINT_free_tmpstore(tmpstore);
#endif // MINT
#ifdef YBC
        YBC_free_instrument_list(tmpstore);
#endif // YBC
        Tmp_free(file_log_prevstring);
        Tmp_free(random_system_argstring);
        Tmp_free(unresolved_magnitudes);
        {
            Star_number k;
            Number_of_stars_Starloop(k)
            {
                Tmp_free(stellar_magnitudes[k]);
            }
        }

        if(free_raw_buffer == TRUE)
        {
            Tmp_free(error_buffer);
            Tmp_free(raw_buffer);
        }
        free_star(&tmpstore->stellar_structure_newstar);
        free_star(&tmpstore->stellar_evolution_newstar);
        Tmp_free(buffer_string);
        tmpstore->raw_buffer_size = 0;
        tmpstore->raw_buffer_alloced = 0;
#ifdef BATCHMODE
        Tmp_free(batchstring);
        Tmp_free(batchstring2);
        Tmp_free(batchargs);
#endif
        for(size_t i=0; i<tmpstore->n_unit_alphas; i++)
        {
            for(size_t j=0; j<tmpstore->unit_alphas[i].nunits; j++)
            {
                Tmp_free(unit_alphas[i].unit);
            }
        }
        Tmp_free(unit_alphas);
#ifdef STELLAR_POPULATIONS_ENSEMBLE
        Tmp_free(ensemble_type);
#endif // STELLAR_POPULATIONS_ENSEMBLE
#ifdef NUCSYN
        Tmp_free(Xwind_gain);
#ifdef NUCSYN_NOVAE
        Tmp_free(Xnovae);
#endif // NUCSYN_NOVAE
        {
            Star_number k;
            Number_of_stars_Starloop(k)
            {
                Tmp_free(nXacc[k]);
                Tmp_free(nXenv[k]);
            }
        }


#endif//NUCSYN

#ifdef WTTS_LOG
        {
            for(size_t i=0;i<2;i++)
            {
                Safe_fclose(tmpstore->fp_star[i]);
            }
            Safe_fclose(tmpstore->fp_sys);
        }
#endif // WTTS_LOG


#ifdef GAIAHRD
        Safe_free(tmpstore->gaiaHRD);
#endif//GAIAHRD

        /*
         * Free data allocated for GSL use
         */
        if(tmpstore->kaps_rentrop_GSL_n > 0)
        {
            tmpstore->kaps_rentrop_GSL_m->size1 = tmpstore->kaps_rentrop_GSL_n;
            tmpstore->kaps_rentrop_GSL_m->size2 = tmpstore->kaps_rentrop_GSL_n;
            tmpstore->kaps_rentrop_GSL_p->size = tmpstore->kaps_rentrop_GSL_n;
            gsl_matrix_free(tmpstore->kaps_rentrop_GSL_m);
            gsl_permutation_free(tmpstore->kaps_rentrop_GSL_p);
        }
        Safe_free_GSL_vector(tmpstore->kaps_rentrop_LU_backsub_b);
        Safe_free_GSL_vector(tmpstore->kaps_rentrop_LU_backsub_x);

#ifdef NUCSYN
        for(Reaction_network i=0; i<NUCSYN_NETWORK_NUMBER; i++)
        {
            if(tmpstore->network_jacobians[i] != NULL)
            {
                for(size_t j=1; j<tmpstore->network_jacobians_n[i]+1; j++)
                {
                    Safe_free(tmpstore->network_jacobians[i][j]);
                }
                Safe_free(tmpstore->network_jacobians[i]);
            }
#ifdef __HAVE_LIBSUNDIALS_CVODE__
            struct cvode_context_t * c = tmpstore->cvode[i];
            if(c != NULL)
            {
                if(c->abstol != NULL)
                {
                    N_VDestroy(c->abstol);
                }
                if(c->y != NULL)
                {
                    /* Free y and abstol vectors */
                    N_VDestroy(c->y);
                }
                if(c->cvode_mem != NULL)
                {
                    /* Free integrator memory */
                    CVodeFree(&c->cvode_mem);
                }
                if(c->LS != NULL)
                {
                    /* Free the linear solver memory */
                    SUNLinSolFree(c->LS);
                }
                if(c->A != NULL)
                {
                    /* Free the matrix memory */
                    SUNMatDestroy(c->A);
                }
                Safe_free(c);
            }
#endif // __HAVE_LIBSUNDIALS_CVODE__
        }
#endif // NUCSYN

#ifdef MEMORY_USE
        if(tmpstore->proc_self_status != NULL)
        {
            fclose(tmpstore->proc_self_status);
        }
        Safe_free(tmpstore->memuse);
#endif//MEMORY_USE

        /*
         * Free the memory in tmpstore, but don't set it
         * to NULL
         */
        Unsafe_free(tmpstore);
    }
}
