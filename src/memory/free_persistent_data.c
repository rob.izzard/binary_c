#include "../binary_c.h"
No_empty_translation_unit_warning;

void free_persistent_data(struct stardata_t * const Restrict stardata)
{
    /*
     * Free thread-persistent data, outputting its
     * contents if required.
     */
    Dprint("binary_c free_persistent_data (stardata=%p stardata->persistent_data=%p)\n",
           (void*)stardata,
           (void*)(stardata?stardata->persistent_data:NULL));

    if(stardata != NULL &&
       stardata->persistent_data != NULL)
    {
        /*
         * If there is a persistent ensemble, dump its
         * contents to the log. Note that ensemble_log
         * should free the ensemble cdict itself.
         */
#ifdef STELLAR_POPULATIONS_ENSEMBLE
        Dprint("free ensemble cdict at %p ?\n",
               (void*)stardata->persistent_data->ensemble_cdict);

        if(stardata->persistent_data->ensemble_cdict != NULL)
        {
            Dprint("call ensemble log\n");

            stardata->model.ensemble_cdict = stardata->persistent_data->ensemble_cdict;
            if(!Binary_c_error_code_is_bad(stardata->error_code))
            {
                ensemble_log(stardata,TRUE);
            }
            CDict_free(stardata->persistent_data->ensemble_cdict);
            Dprint("freed: cdict is now %p\n",
                   (void*)stardata->persistent_data->ensemble_cdict);

            stardata->persistent_data->ensemble_cdict = NULL;
            stardata->model.ensemble_cdict = NULL;
        }
#endif // STELLAR_POPULATIONS_ENSEMBLE


        Delete_data_table(stardata->persistent_data->Peters_grav_wave_merger_time_table);
        Safe_free_nocheck(stardata->persistent_data->Temmink2022_RLOF_mapped);


#ifdef COMENV_WANG2016
        //Tmp_free(comenv_lambda_data);
        Delete_data_table(stardata->persistent_data->comenv_lambda_table);
#endif

        Delete_data_table(stardata->persistent_data->comenv_Klencki_2020_Rmax);
        Delete_data_table(stardata->persistent_data->comenv_Klencki_2020_loglambda);

#ifdef NUCSYN
        if(stardata->persistent_data->cc_sn_tables != NULL)
        {
            for(size_t i=0;i<NUCSYN_CCSN_NUMBER;i++)
            {
                if(stardata->persistent_data->cc_sn_tables[i] != NULL)
                {
                    Delete_data_table(stardata->persistent_data->cc_sn_tables[i]);
                }
            }
            Safe_free(stardata->persistent_data->cc_sn_tables);
        }
        Safe_free_nocheck(stardata->persistent_data->Limongi_Chieffi_2018_mapped);
        Safe_free_nocheck(stardata->persistent_data->Gronow2021a_core_ejecta);
        Safe_free_nocheck(stardata->persistent_data->Gronow2021a_shell_ejecta);
        Safe_free_nocheck(stardata->persistent_data->Gronow2021a_isotope_list);
        Safe_free_nocheck(stardata->persistent_data->Gronow2021b_core_ejecta);
        Safe_free_nocheck(stardata->persistent_data->Gronow2021b_shell_ejecta);
        Safe_free_nocheck(stardata->persistent_data->Gronow2021b_isotope_list);
        Safe_free_nocheck(stardata->persistent_data->Radice2018_ejecta);
#endif // NUCSYN
        Safe_free_nocheck(stardata->persistent_data->Marassi_2019_mapped);

#ifdef YBC
        YBC_free_persistent_data_memory(stardata->persistent_data);
#endif//YBC

#ifdef MINT
        MINT_free_persistent_data(stardata);
#endif // MINT

        if(stardata->persistent_data->rinterpolate_data != NULL)
        {
#ifdef CHECK_RINTERPOLATE
            printf("FREE %p\n",stardata->persistent_data_t->rinterpolate_data);
#endif
            rinterpolate_free_data(stardata->persistent_data->rinterpolate_data);
            Safe_free(stardata->persistent_data->rinterpolate_data);
        }


        Dprint("freeing stardata=%p -> persistent_data = %p\n",
               (void*)stardata,
               stardata? (void*)stardata->persistent_data : NULL);

        Safe_free_nocheck(stardata->persistent_data);

    }
    else
    {
        Dprint("free persistent data called on stardata = %p stardata->persistent_data = %p\n",
               (void*)stardata,
               (void*)(stardata->persistent_data));

    }
}
