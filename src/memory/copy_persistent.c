#include "../binary_c.h"
No_empty_translation_unit_warning;


/*
 * Perform a deep copy of a persistent_data struct
 */
void Nonnull_all_arguments
copy_persistent(struct persistent_data_t * const Restrict from,
                struct persistent_data_t * const Restrict to)
{
    /*
     * First a shallow copy
     */
    memcpy(to,from,sizeof(struct persistent_data_t));

    /*
     * Now the more difficult copy of data inside
     */
    if(from->ensemble_cdict != NULL)
    {
        struct cdict_t * h = to->ensemble_cdict;
        if(h == NULL)
        {
            CDict_new(h2);
            to->ensemble_cdict = h2;
        }
        CDict_copy(from->ensemble_cdict,
                    to->ensemble_cdict);
    }
}
