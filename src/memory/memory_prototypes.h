#pragma once
#ifndef MEMORY_PROTOTYPES_H
#define MEMORY_PROTOTYPES_H
void free_all_memory(struct stardata_t ** Restrict const sp);
void free_memory(struct stardata_t ** Restrict const sp,
                 const Boolean free_preferences,
                 const Boolean free_stardata_struct,
                 const Boolean free_store,
                 const Boolean free_raw_buffer,
                 const Boolean free_persistent_data);
void free_aux_memory(void);
void Nonnull_some_arguments(1)
    main_allocations(struct stardata_t ** const stardata,
                     struct stardata_t *** const previous_stardatas,
                     struct preferences_t ** const preferences,
                     struct store_t ** const store,
                     struct persistent_data_t ** persistent_data);
void initialize_pointers(struct stardata_t * const stardata,
                         struct preferences_t * const preferences,
                         struct store_t * const store);

void build_store_contents(struct stardata_t * const stardata,
                          struct store_t * const store);
void build_tmpstore_contents(struct tmpstore_t * Restrict const tmpstore);
void free_store_contents(struct store_t * Restrict const store);
void free_store(struct store_t ** store_p);
void free_tmpstore(struct tmpstore_t * Restrict const tmpstore,
                   const Boolean free_raw_buffer);
void free_argstore(struct store_t * const store);
#ifdef ALIGNSIZE
void * aligned_memcpy(void * dest,
                      void * src,
                      size_t size);
void * check_aligned_memcpy_heap_source(void * dest,
                                        const void * src,
                                        size_t n);
void * check_aligned_memcpy_stack_source(void * dest,
                                         const void * src,
                                         size_t n);
No_return
void aligned_memcpy_fail(const void * dest,
                         const void * src,
                         const size_t n);

#endif

#ifdef MEMCPY_USE_SKYWIND_FAST
void* memcpy_fast(void *destination, const void *source, size_t size);
#endif

#ifdef MEMCPY_USE_APEX
#include "apex_memmove.h"
#endif

#ifdef USE_MEMCPY_DDR4
void *fastMemcpy_ddr4(void *pvDest, void *pvSrc, size_t nBytes);
#endif

#ifdef CODESTATS
void * Returns_nonnull Nonnull_all_arguments codestats_memcpy(void * dest,
                                                              const void * src,
                                                              size_t n,
                                                              char *const file,
                                                              const long int line);
#endif//CODESTATS

/*
 * stardata stack actions
 */
void free_previous_stardatas(struct stardata_t * Restrict const stardata);
void free_stardata_stack(struct stardata_t * Restrict const stardata);

/*
 * stardata struct actions
 */
void * deep_copy_stardata(struct stardata_t * const src,
                          struct stardata_t * const dest);
void free_stardata(struct stardata_t ** stardata);

struct stardata_t *
Returns_nonnull Nonnull_all_arguments
copy_stardata(
    struct stardata_t  * Restrict Aligned const from,
    struct stardata_t  * Restrict Aligned const to,
    const unsigned int copy_previous_option,
    const unsigned int copy_persistent_option
    );

/*
 * Allocation, copying and freeing of star structs
 */
void free_star_contents(struct star_t * const Restrict star);
void free_star(struct star_t ** Restrict star);

void * copy_stars(struct stardata_t * const src,
                  struct stardata_t * const dest);

void * copy_star(struct stardata_t * const stardata,
                 const struct star_t * const Restrict src,
                 struct star_t * const Restrict dest);

Malloc_like
struct star_t * alloc_star(struct stardata_t * const Restrict stardata);
Malloc_like
struct star_t * new_star(struct stardata_t * const Restrict stardata,
                         const struct star_t * const src);
Malloc_like
struct stardata_t * new_stardata(struct preferences_t * const Restrict preferences);
void clear_stardata(struct stardata_t * const Restrict stardata);

void zero_star(struct star_t * const Restrict star);

void Nonnull_all_arguments copy_persistent(
    struct persistent_data_t * const Restrict from,
    struct persistent_data_t * const Restrict to);

float Pure_function diff_struct_pc(void * const a,
                                   void * const b,
                                   const size_t len);
void * Returns_nonnull Nonnull_all_arguments strided_memcpy(void * dst,
                                                            void * src,
                                                            const size_t length,
                                                            const size_t blocksize);

#ifdef MEMORY_USE
void get_memory_use(struct stardata_t * const stardata,
                    struct binary_c_memuse_t * const memuse);
#endif // MEMORY_USE

void apply_stardata_mask(struct stardata_t * Restrict const stardata,
                         struct stardata_t * Restrict const mask,
                         struct stardata_t * Restrict const mask_contents);
void apply_data_mask(char * const Restrict data,
                     const char * const Restrict mask,
                     const char * const Restrict mask_contents,
                     const size_t size);

#ifdef STELLAR_POPULATIONS_ENSEMBLE
void make_ensemble_cdict(struct stardata_t * const Restrict stardata);
void free_ensemble_cdict(struct stardata_t * const Restrict stardata);
#endif // STELLAR_POPULATIONS_ENSEMBLE

void free_persistent_data(struct stardata_t * const Restrict stardata);

void clean_star(struct star_t * Restrict const star);

void free_BSE_data(struct BSE_data_t ** const Restrict bse);

Malloc_like
struct BSE_data_t * new_BSE_data(void);


void alloc_star_contents(struct preferences_t * const preferences,
                         struct star_t * const Restrict star);


struct star_pointers_t * save_star_pointers(struct star_t * const star);
void restore_star_pointers(struct star_t * const star,
                           struct star_pointers_t * const p);

void build_persistent_data(struct stardata_t * const stardata);
void build_persistent_data_contents(struct stardata_t * const stardata);

Alloc_size_third void * recalloc(struct stardata_t * const stardata,
                                 void * ptr,
                                 const size_t size);
#ifdef MEMORY_USE
void log_memory_use(struct stardata_t * Restrict const stardata);
#endif // MEMORY_USE

#endif // MEMORY_PROTOTYPES_H
