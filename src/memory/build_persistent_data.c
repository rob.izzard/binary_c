#include "../binary_c.h"
No_empty_translation_unit_warning;

void build_persistent_data(struct stardata_t * const stardata)
{
    if(stardata->persistent_data == NULL)
    {
        stardata->persistent_data =
            Calloc(1,sizeof(struct persistent_data_t));
        build_persistent_data_contents(stardata);
    }
}
