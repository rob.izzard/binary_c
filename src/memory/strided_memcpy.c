#include "../binary_c.h"
No_empty_translation_unit_warning;


void * Returns_nonnull Nonnull_all_arguments Hot_function strided_memcpy(void * dst_in,
                                                                         void * src_in,
                                                                         const size_t length,
                                                                         const size_t blocksize)
{
    /*
     * Copy memory from src to dst of size length in
     * blocksize strides.
     *
     * If the block hasn't changed, don't  copy it.
     *
     * The idea is that reading of RAM is faster than
     * writing of RAM, so if the number of changes is
     * small, it should be faster to not copy most
     * of the memory.
     *
     * Neither dst nor src should even be NULL.
     */
    char * dst = (char*)dst_in;
    char * src = (char*)src_in;
    const char * const start_dest = dst;
    const char * const end_dest = dst + length;

    while(dst < end_dest)
    {
        const size_t thisblocksize = Min((size_t)(end_dest - dst),
                                         blocksize);
        if(thisblocksize &&
           !Memory_equal(src,
                         dst,
                         thisblocksize))
        {
            /*
             * Differ : copy.
             * Use C-library memcpy for this: I tried a loop but it was slower.
             */
#undef memcpy
            memcpy(dst,src,thisblocksize);
        }
        src += thisblocksize;
        dst += thisblocksize;
    }
    return (void *)start_dest;
}
