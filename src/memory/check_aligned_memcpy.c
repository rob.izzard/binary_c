#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef ALIGNSIZE
#include "memory_alignment_checks.h"

void aligned_memcpy_fail(const void * dest,
                         const void * src,
                         const size_t n);


/*
 * Wrapper for memcpy to check that two
 * pieces of memory are aligned.
 *
 * NB this function must NOT include the
 * standard binary_c header files which
 * redefine memcpy and exit. We want to use
 * the standard C library calls.
 *
 * We assume that the target of the memcpy is always
 */

void *check_aligned_memcpy_heap_source(void * dest,
                                       const void * src,
                                       size_t n)
{
#ifdef CODESTATS
    codestats.counters[CODESTAT_MEMCPY]++;
#endif
    if((!is_aligned(dest)) || (!is_aligned(src)))
        aligned_memcpy_fail(dest,src,n);

    return memcpy(dest,src,n);
}

void *check_aligned_memcpy_stack_source(void * dest,
                                        const void * src,
                                        size_t n)
{
    /*
     * Stack source : heap dest
     */
#ifdef CODESTATS
    codestats.counters[CODESTAT_MEMCPY]++;
#endif
    if(!is_aligned(dest))
        aligned_memcpy_fail(dest,src,n);

    return memcpy(dest,src,n);
}
#endif //ALIGNSIZE
