#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef STELLAR_POPULATIONS_ENSEMBLE

void free_ensemble_cdict(struct stardata_t * const Restrict stardata)
{
    /*
     * Free the cdict and contents
     */
    Dprint("FREE CDICT model %p persist %p -> %p\n",
           (void*)stardata->model.ensemble_cdict,
           (void*)stardata->persistent_data,
           (void*)(stardata->persistent_data ? stardata->persistent_data->ensemble_cdict : NULL)
        );

    if(stardata->model.ensemble_cdict != NULL &&
       stardata->preferences->ensemble_defer == FALSE)
    {
        /*
         * Only free the memory if we are not deferring
         * output. If we *are* deferring output, the
         * call to free_persistent_data will free the cdict
         * at the end of the run.
         */
        CDict_free(stardata->model.ensemble_cdict);
        stardata->model.ensemble_cdict = NULL;
    }

}

#endif // STELLAR_POPULATIONS_ENSEMBLE
