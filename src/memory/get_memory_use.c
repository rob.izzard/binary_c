#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef MEMORY_USE

#include <stdio.h>
#include <stdlib.h>
#include <string.h>


/*
 * Measures the current (and peak) resident and virtual memories
 * usage of your linux C process, in kB
 *
 * based on
 * https://stackoverflow.com/questions/1558402/memory-usage-of-current-process-in-c
 *
 * On error, return values are zero.
 */
void get_memory_use(struct stardata_t * const stardata,
                    struct binary_c_memuse_t * const memuse)
{
    /*
     * Default results to zero
     */
    memset(memuse,0,sizeof(struct binary_c_memuse_t));

    /*
     * Linux has /proc/self/status, other operating
     * systems may also. If so, use it.
     */
    FILE * file;
    if(stardata->tmpstore->proc_self_status == NULL)
    {
        /*
         * Open the /proc/self/status file
         */
        file = stardata->tmpstore->proc_self_status =
            fopen("/proc/self/status", "r");
    }
    else
    {
        /*
         * Already open, seek to the start
         */
        file = stardata->tmpstore->proc_self_status;
        fseek(file,0,SEEK_SET);
    }

    if(file != NULL)
    {
        char * buffer = Malloc(1024*sizeof(char));
        if(buffer != NULL)
        {
            /*
             * read into the buffer as much as we can
             */
            while(fscanf(file, " %1023s", buffer) == 1)
            {
#undef X
#define X(TYPE)                                             \
                else if(strcmp(buffer, #TYPE":") == 0)      \
                {                                           \
                    if(fscanf(file,                         \
                              " "__RAM_FORMAT_SPECIFIER__,  \
                              &memuse->TYPE) != 1)          \
                    {                                       \
                        /* failed */                        \
                    }                                       \
                }

                if(0){} __RAM_TYPE_LIST__;

#undef X
            }
            Safe_free(buffer);
        }
    }
}
#endif//MEMORY_USE
