#include "../binary_c.h"
No_empty_translation_unit_warning;

/*
 * Like realloc but clears the memory that is set
 */
Alloc_size_third void * recalloc(struct stardata_t * const Restrict stardata,
                                 void * ptr,
                                 const size_t size)
{
    ptr = Realloc(ptr,size);
    if(ptr != NULL)
    {
        memset(ptr,0,size);
    }
    return ptr;
}
