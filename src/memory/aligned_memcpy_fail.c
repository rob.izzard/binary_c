#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "memory_alignment_checks.h"

#ifdef ALIGNSIZE
No_return
void  aligned_memcpy_fail(const void * dest,
                          const void * src,
                          const size_t n)
{

    Backtrace;
    fprintf(stderr,
            "memcpy dest=%p (%saligned) src=%p (%saligned) n=%ld : should both be aligned!n",
            dest,
            is_aligned(dest) ? "" : "not ",
            src,
            is_aligned(src) ? "" : "not ",
            (long int)n);
    fflush(stderr);
    Exit_binary_c_no_stardata(BINARY_C_ALLOC_FAILED,"aligned memcpy failed");
}
#endif //ALIGNSIZE
