#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "memory_alignment_checks.h"

/*
 * Allocate a star struct's contents
 */
void alloc_star_contents(struct preferences_t * const preferences Maybe_unused,
                         struct star_t * const Restrict star)
{
    if(star != NULL)
    {
#ifdef MINT
        if(preferences &&
           preferences->stellar_structure_algorithm ==
           STELLAR_STRUCTURE_ALGORITHM_MINT)
        {
            if(star->mint == NULL)
            {
                star->mint = Calloc(1,sizeof(struct mint_t));
            }
        }
#endif//MINT
    }
}
