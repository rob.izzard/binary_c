#include "../binary_c.h"
No_empty_translation_unit_warning;


#define Store_free(P) Safe_free(store->P);

void free_store_contents(struct store_t * Restrict const store)
{
    /*
     * Free the content in the store.
     * See also build_store_contents.c
     *
     * Note:
     * NOTHING in here can depend on the current stardata.
     * You will NOT have access to stardata here.
     */
    if(store)
    {
#ifdef NUCSYN
        nucsyn_free_store_contents(store);
#endif//NUCSYN
        for(int i=0;i<NUM_ANSI_COLOURS;i++)
        {
            Store_free(ANSI_colours_table[i]);
            Store_free(no_colours_table[i]);
        }
        Store_free(no_colours_table);
        Store_free(ANSI_colours_table);
        for(int kt=0;kt<COLLISION_MATRIX_SIZE;kt++)
        {
            Store_free(collision_matrix[kt]);
        }

        if(store->static_spectral_type_strings != NULL)
        {
            for(int i=0;i<NUM_SPECTRAL_TYPES;i++)
            {
                for(int j=0;j<NUM_LUMINOSITY_SUBCLASSES;j++)
                {
                    Store_free(static_spectral_type_strings[i][j]);
                }
                Store_free(static_spectral_type_strings[i]);
            }
            Store_free(static_spectral_type_strings);
        }

        Delete_data_table(store->jaschek_jaschek_dwarf);
        Delete_data_table(store->jaschek_jaschek_giant);
        Delete_data_table(store->jaschek_jaschek_supergiant);
#ifdef FILE_LOG
        Store_free(upchar);
        Store_free(downchar);
        Store_free(nochangechar);
#endif//FILE_LOG
#ifdef USE_2012_MAIN_SEQUENCE_LIFETIMES_TABLE
        Delete_data_table(store->massive_MS_lifetimes);
#endif
#ifdef MAIN_SEQUENCE_STRIP
        Delete_data_table(store->MS_strip);
#endif
#ifdef STELLAR_COLOURS
        Delete_data_table(store->Eldridge2012_colours);
        Delete_data_table(store->carrasco2014_table3);
        Delete_data_table(store->carrasco2014_table4);
        Delete_data_table(store->carrasco2014_table5);
        Delete_data_table(store->carrasco2014_cooling_table3);
        Delete_data_table(store->carrasco2014_cooling_table4);
        Delete_data_table(store->carrasco2014_cooling_table5);
        binary_magnitudes_free_memory(store);
#endif // STELLAR_COLOURS
#ifdef COMENV_WANG2016
        Store_free(tableh1);
        Store_free(tableh2);
        Store_free(tableh3);
        Store_free(tableb1);
        Store_free(tableb2);
        Store_free(tableb3);
        Store_free(tableg1);
        Store_free(tableg2);
        Store_free(tableg3);
        Store_free(comenv_maxR_table->data);
        Delete_data_table(store->comenv_maxR_table);
#endif
#ifdef COMENV_POLYTROPES
        Delete_data_table(store->comenv_polytrope_2d);
        Store_free(comenv_polytrope_3d);
        Delete_data_table(store->comenv_polytrope_rsms);
#endif
        Delete_data_table(store->Karakas_ncal);
#ifdef OPACITY_ALGORITHMS
#  ifdef OPACITY_ENABLE_ALGORITHM_FERGUSON_OPAL
        Store_free(opacity_ferguson_opal);
#  endif
#  ifdef OPACITY_ENABLE_ALGORITHM_STARS
        Store_free(opacity_STARS);
#  endif
#endif
        Safe_free(store->Karakas2002_lumfunc->label);
        Delete_data_table(store->Karakas2002_lumfunc);
        Delete_data_table(store->Karakas2002_radius);
        Delete_data_table_not_contents(store->miller_bertolami);
        Delete_data_table_not_contents(store->miller_bertolami_coeffs_L);
        Delete_data_table_not_contents(store->miller_bertolami_coeffs_R);
        Delete_data_table(store->Temmink2022_RLOF);
#ifdef MINT
        MINT_free_store(store);
#endif
        free_pulsator_store(store);
        Store_free(fallback_preferences);

#ifdef __HAVE_LIBPTHREAD__
        /*
         * Make mutexes for later locking
         */
        for(size_t i=0; i<BINARY_C_MUTEX_NUMBER; i++)
        {
            /*
             * First, we try to get the lock ourselves. We have
             * to do this otherwise the mutex is unlocked when
             * it is already unlocked. This causes errors.
             */
            Pthread_unlock_primitive(store,i);
            const int destroy_status =
                pthread_mutex_destroy(&store->pthread_mutexes[i].mutex);
            if(destroy_status != 0)
            {
                fprintf(stderr,
                        "\n failed to destroy pthread mutex %zu error %d %s: may cause problems\n",
                        i,
                        destroy_status,
                        Mutex_error_string(destroy_status));
            }
        }
#endif//__HAVE_LIBPTHREAD__
#ifdef HASH_ARGUMENTS
        CDict_free(store->argcdict);
#endif // HASH_ARGUMENTS
        free_argstore(store);

    }
}
