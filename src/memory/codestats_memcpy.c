#include "../binary_c.h"

No_empty_translation_unit_warning;

#ifdef CODESTATS
#undef memcpy
void * codestats_memcpy(void * dest,
                        const void * src,
                        size_t n,
                        char * const file,
                        const long int line)
{
    increment_codestat(CODESTAT_MEMCPY,file,line);
    return memcpy(dest,src,n);
}
#endif//CODESTATS
