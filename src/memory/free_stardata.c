#include "../binary_c.h"
No_empty_translation_unit_warning;
#include "memory_alignment_checks.h"

void free_stardata(struct stardata_t ** sp)
{
    /*
     * Deep free of a stardata struct
     */
    if(sp!=NULL && *sp != NULL)
    {
        struct stardata_t * const stardata = *sp;
        Star_number k;
        Number_of_stars_Starloop(k)
        {
            free_star_contents(&stardata->star[k]);
        }
        if(stardata->pre_events_stardata != NULL)
        {
            free_stardata(&stardata->pre_events_stardata);
        }
#ifdef ORBITING_OBJECTS
        if(stardata->common.orbiting_object != NULL)
        {
            for(size_t i=0; i<stardata->common.n_orbiting_objects; i++)
            {
                Safe_free(stardata->common.orbiting_object[i].name);
            }
        }
        Safe_free(stardata->common.orbiting_object);
#endif//ORBITING_OBJECTS
        Safe_free_nocheck(*sp);
    }
}
