#pragma once
#ifndef MEMORY_ALIGNMENT_CHECKS_H
#define MEMORY_ALIGNMENT_CHECKS_H

/* pointer alignment test macro */
#include <stdint.h>

#ifdef ALIGNSIZE
#define is_aligned(POINTER)                                     \
    (((uintptr_t)(const void *)(POINTER)) % (ALIGNSIZE) == 0)

#else

/* no ALIGNSIZE : cannot know if a pointer is aligned */
#define is_aligned(POINTER) 0

#endif

#endif // MEMORY_ALIGNMENT_CHECKS_H
