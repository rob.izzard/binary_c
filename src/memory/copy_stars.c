#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "memory_alignment_checks.h"

/*
 * Copy the star structs in stardata_t src to dest
 *
 * Returns NULL if either copy failed, otherwise
 * returns a pointer to the last star copied
 *
 * stardata is used for metadata, and should not
 * equal dest
 */
#undef Cprint
#define Cprint(...)

void * copy_stars(struct stardata_t * const src,
                  struct stardata_t * const dest)
{
    Star_number k;
    void * ret;
    Number_of_stars_Starloop(k)
    {
        Cprint("copy star %d : %p to %p\n",
               k,
               (void*)&src->star[k],
               (void*)&dest->star[k]);

        ret = copy_star(src,
                        &src->star[k],
                        &dest->star[k]);
        if(ret == NULL)
        {
            return NULL;
        }
    }

    return ret;
}
