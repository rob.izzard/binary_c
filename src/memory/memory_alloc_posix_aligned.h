/*
 * Memory allocation functions that use posix_memalign()
 */
#define MEMORY_ALLOCATION_MODEL_STRING "Posix aligned"

/* you could use the system realloc, but it's just as slow */
//#define Realloc(P,S) realloc((P),(S))

/* only in gcc > 4.7 or recent clang */
#if __GNUC_PREREQ(4,7) || defined __clang__
#define Builtin_aligned(P,A) __builtin_assume_aligned((P),(A))
#define Assume_aligned(P) __builtin_assume_aligned((P),(ALIGNSIZE))
#else
#define Builtin_aligned(P,A) (P)
#define Assume_aligned(P) (P)
#endif

static inline void * Alloc_size_first Malloc_like aligned_malloc(size_t size,
                                                                 size_t alignment);

static inline void * Alloc_size_first Malloc_like aligned_malloc(size_t size,
                                                                 size_t alignment)
{
    if(unlikely(size==0)) return NULL;
    void *p = NULL;
    if(0)fprintf(stderr, "size %ld at %p\n",
                 (long int)size,
                 (void*)p);
    int r = posix_memalign(&p,alignment,size);
    _Alloc_check(p);
    return r==0 ? (void *) Builtin_aligned(p,alignment) : NULL;
}

static inline void * Alloc_size_product Malloc_like aligned_calloc(size_t nelem,
                                                                   size_t elsize,
                                                                   size_t alignment);
static inline void * Alloc_size_product Malloc_like aligned_calloc(size_t nelem,
                                                                   size_t elsize,
                                                                   size_t alignment)
{
    if(unlikely(nelem==0)||unlikely(elsize==0)) return NULL;
    void *p = aligned_malloc(nelem*elsize,alignment);
    _Alloc_check(p);
    if(p) memset(p, 0, nelem*elsize);
    return p;
}

static inline void * Alloc_size_second aligned_realloc(void *ptr,
                                                       size_t size,
                                                       size_t alignment);
static inline void * Alloc_size_second aligned_realloc(void *ptr,
                                                       size_t size,
                                                       size_t alignment)
{
#ifdef HAVE_MALLOC_USABLE_SIZE
    if(unlikely(size==0)) return NULL;
    size_t usable = Malloc_usable_size(ptr);
    void *p = NULL;
    p = posix_memalign(&p,alignment,size)==0 ?
        (void *) Builtin_aligned(p,alignment) :
        NULL;
    _Alloc_check(p);

    if(p!=NULL && ptr!=NULL)
    {
        memcpy(p,ptr,usable<size?usable:size);
        free(ptr);
    }
    return p;
#else
    // no Malloc_usable_size : use system realloc
    void * p = realloc(ptr,size);
    _Alloc_check(p);
    return p;
#endif //HAVE_MALLOC_USABLE_SIZE
}


/*
 * Check that memcpy is aligned if CHECK_MEMCPY_ALIGNMENT
 * is defined.
 */
#ifdef CHECK_MEMCPY_ALIGNMENT
#define memcpy(dest,src,n)                              \
    check_aligned_memcpy_heap_source(dest,src,n)
#endif // CHECK_MEMCPY_ALIGNMENT

/*
 * define macros to replace malloc, calloc and realloc
 */
#define _Malloc(S)                                      \
    __extension__                                       \
    ({                                                  \
        Increment_codestat(CODESTAT_MALLOC);            \
        aligned_malloc((S),(ALIGNSIZE));                \
    })

#define _Calloc(N,S)                            \
    __extension__                               \
    ({                                          \
        Increment_codestat(CODESTAT_CALLOC);    \
        (aligned_calloc((N),(S),(ALIGNSIZE)));  \
    })

#define _Realloc(P,S)                           \
    __extension__                               \
    ({                                          \
        Increment_codestat(CODESTAT_REALLOC);   \
        (aligned_realloc((P),(S),(ALIGNSIZE))); \
    })
