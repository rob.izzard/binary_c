#include "../binary_c.h"
No_empty_translation_unit_warning;

struct star_pointers_t * save_star_pointers(struct star_t * const star)
{
    /*
     * Save star pointers and return the star_pointers struct
     */
    struct star_pointers_t * const p =
        Malloc(sizeof(struct star_pointers_t));

    if(p != NULL)
    {
#ifdef BSE
        p->bse = star->bse;
#endif // BSE

#ifdef MINT
        p->mint = star->mint;
        if(star->mint)
        {
            p->mint_shells = star->mint->shells;
            p->mint_nshells = star->mint->nshells;
        }
#endif // MINT

#if defined NUCSYN && \
    defined NUCSYN_ID_SOURCES
    memcpy(p->oldsource, star->oldsource, sizeof(Abundance*)*SOURCE_NUMBER);
    memcpy(p->Xsource, star->Xsource, sizeof(Abundance*)*SOURCE_NUMBER);
#endif // NUCSYN && NUCSYN_ID_SOURCES
    }
    return p;
}
