#include "../binary_c.h"
No_empty_translation_unit_warning;


int binary_c_flush(const struct stardata_t * Restrict const stardata,
                   FILE * Restrict const stream)
{
    /* wrapper around fflush to catch and log flushes */
#ifdef BACKTRACE
    fprintf(stderr,"fflush() at >>");
    Print_trace(stdout);
    fprintf(stderr,"<< to stream %p which is ",(void*)stream);
#endif // BACKTRACE

    if(stderr!=NULL)
    {
        if(stream==NULL)
        {
            fprintf(stderr,"NULL");
        }
        else if(stream==stdout)
        {
            fprintf(stderr,"stdout");
        }
        else if(stream==stderr)
        {
            fprintf(stderr,"stderr");
        }
#ifdef FILE_LOG
        else if(stream==stardata->model.log_fp)
        {
            fprintf(stderr,"file_log");
        }
#endif
        else
        {
            fprintf(stderr,"unknown/private\n");
        }
        fprintf(stderr,"\n");

        /* undef fflush to use library call */
#undef fflush
        fflush(stderr);
    }
    return fflush(stream);
}
