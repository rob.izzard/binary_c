#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "buffering_macros.h"

/*
 * printf into the buffer.
 *
 * This function is a wrapper for binary_c_buffered_vprintf,
 * please see that function for details.
 *
 * Note: if stardata is NULL, we just output to stdout.
 */

int binary_c_API_function
Gnu_format_args(3,4) binary_c_buffered_printf(
    struct stardata_t * Restrict const stardata,
    const Boolean do_deslash,
    const char * Restrict const format,
    ...)
{
    int retval; /* return value */
    va_list args;
    va_start(args,format);
    if(stardata)
    {
        retval = binary_c_buffered_vprintf(stardata,
                                           do_deslash,
                                           format,
                                           args);
    }
    else
    {
        printf(format,args);
        retval = 0;
    }
    va_end(args);
    return retval;
}
