#include "../binary_c.h"
No_empty_translation_unit_warning;


void buffer_empty_buffer(struct stardata_t * Restrict const stardata)
{
    if(stardata && stardata->tmpstore)
    {
        Safe_free(stardata->tmpstore->raw_buffer);
        Safe_free(stardata->tmpstore->error_buffer);
        stardata->tmpstore->raw_buffer_size = 0;
        stardata->tmpstore->raw_buffer_alloced = 0;
    }
}
