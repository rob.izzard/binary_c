#pragma once
#ifndef BUFFERING_PROTOTYPES_H
#define BUFFERING_PROTOTYPES_H

#include "../binary_c_structures.h"
#include <stdarg.h>



int binary_c_API_function
binary_c_buffered_printf(struct stardata_t * Restrict const stardata,
                         const Boolean deslash,
                         const char * Restrict const format,
                         ...) Gnu_format_args(3,4);

int binary_c_API_function
binary_c_buffered_vprintf(struct stardata_t * Restrict const stardata,
                          const Boolean deslash,
                          const char * Restrict const format,
                          va_list args) Gnu_format_args(3,0);

void buffer_info(const struct stardata_t * Restrict const stardata,
                 char ** const buffer,
                 size_t * const size,
                 char ** const error_buffer);
void buffer_empty_buffer(struct stardata_t * Restrict const stardata);
void buffer_dump_to_stderr(const struct stardata_t * Restrict const stardata);

int binary_c_flush(const struct stardata_t * Restrict const stardata,
                   FILE * Restrict const stream);

void binary_c_fail_fprintf(struct stardata_t * Restrict const stardata,
                           const char * Restrict const filename ,
                           const int fileline,
                           const char * Restrict const format,
                           ...) Gnu_format_args(4,5) No_return;

void clear_printf_buffer(struct stardata_t * Restrict const stardata);
void clear_error_buffer(struct stardata_t * Restrict const stardata);

size_t deslash(char * const string);

#endif // BUFFERING_PROTOTYPES_H
