#pragma once
#ifndef BUFFERING_FUNCTION_MACROS_H
#define BUFFERING_FUNCTION_MACROS_H

/*
 * Push Printf through the binary_c buffer.
 *
 * Works for up to 100 arguments.
 *
 * Does nothing (no-op) if no arguments are given.
 */
#define Get_printf_macro(                                           \
    _0_,                                                            \
    _1_,  _2_,  _3_,  _4_,  _5_,  _6_,  _7_,  _8_, _9_, _10_,       \
    _11_, _12_, _13_, _14_, _15_, _16_, _17_, _18_, _19_, _20_,     \
    _21_, _22_, _23_, _24_, _25_, _26_, _27_, _28_, _29_, _30_,     \
    _31_, _32_, _33_, _34_, _35_, _36_, _37_, _38_, _39_, _40_,     \
    _41_, _42_, _43_, _44_, _45_, _46_, _47_, _48_, _49_, _50_,     \
    _51_, _52_, _53_, _54_, _55_, _56_, _57_, _58_, _59_, _60_,     \
    _61_, _62_, _63_, _64_, _65_, _66_, _67_, _68_, _69_, _70_,     \
    _71_, _72_, _73_, _74_, _75_, _76_, _77_, _78_, _79_, _80_,     \
    _81_, _82_, _83_, _84_, _85_, _86_, _87_, _88_, _89_, _90_,     \
    _91_, _92_, _93_, _94_, _95_, _96_, _97_, _98_, _99_, _100_,    \
    NAME, ...) NAME

#define Printf_no_op(...) /* no-op */
#define Printf_n(...) binary_c_buffered_printf(stardata,FALSE,__VA_ARGS__);
#define Printf_n_deslash(...) binary_c_buffered_printf(stardata,TRUE,__VA_ARGS__);

#define vPrintf_no_op(...) /* no-op */
#define vPrintf_n(...) binary_c_buffered_printf(stardata,FALSE,__VA_ARGS__);
#define vPrintf_n_deslash(...) binary_c_buffered_printf(stardata,TRUE,__VA_ARGS__);

/* PRINTF = Printf for backwards compatibility */
#define PRINTF(...) Printf(__VA_ARGS__)

#define Printf(...)                                     \
    Get_printf_macro(                                   \
        _0,  __VA_ARGS__ __VA_OPT__(,)                  \
        Printf_n,Printf_n,Printf_n,Printf_n,Printf_n,   \
        Printf_n,Printf_n,Printf_n,Printf_n,Printf_n,   \
        Printf_n,Printf_n,Printf_n,Printf_n,Printf_n,   \
        Printf_n,Printf_n,Printf_n,Printf_n,Printf_n,   \
        Printf_n,Printf_n,Printf_n,Printf_n,Printf_n,   \
        Printf_n,Printf_n,Printf_n,Printf_n,Printf_n,   \
        Printf_n,Printf_n,Printf_n,Printf_n,Printf_n,   \
        Printf_n,Printf_n,Printf_n,Printf_n,Printf_n,   \
        Printf_n,Printf_n,Printf_n,Printf_n,Printf_n,   \
        Printf_n,Printf_n,Printf_n,Printf_n,Printf_n,   \
        Printf_n,Printf_n,Printf_n,Printf_n,Printf_n,   \
        Printf_n,Printf_n,Printf_n,Printf_n,Printf_n,   \
        Printf_n,Printf_n,Printf_n,Printf_n,Printf_n,   \
        Printf_n,Printf_n,Printf_n,Printf_n,Printf_n,   \
        Printf_n,Printf_n,Printf_n,Printf_n,Printf_n,   \
        Printf_n,Printf_n,Printf_n,Printf_n,Printf_n,   \
        Printf_n,Printf_n,Printf_n,Printf_n,Printf_n,   \
        Printf_n,Printf_n,Printf_n,Printf_n,Printf_n,   \
        Printf_n,Printf_n,Printf_n,Printf_n,Printf_n,   \
        Printf_n,Printf_n,Printf_n,Printf_n,Printf_n,   \
        Printf_no_op)(__VA_ARGS__)

#define vPrintf(...)                                        \
    Get_printf_macro(                                       \
        _0, __VA_ARGS__ __VA_OPT__(,)                       \
        vPrintf_n,vPrintf_n,vPrintf_n,vPrintf_n,vPrintf_n,  \
        vPrintf_n,vPrintf_n,vPrintf_n,vPrintf_n,vPrintf_n,  \
        vPrintf_n,vPrintf_n,vPrintf_n,vPrintf_n,vPrintf_n,  \
        vPrintf_n,vPrintf_n,vPrintf_n,vPrintf_n,vPrintf_n,  \
        vPrintf_n,vPrintf_n,vPrintf_n,vPrintf_n,vPrintf_n,  \
        vPrintf_n,vPrintf_n,vPrintf_n,vPrintf_n,vPrintf_n,  \
        vPrintf_n,vPrintf_n,vPrintf_n,vPrintf_n,vPrintf_n,  \
        vPrintf_n,vPrintf_n,vPrintf_n,vPrintf_n,vPrintf_n,  \
        vPrintf_n,vPrintf_n,vPrintf_n,vPrintf_n,vPrintf_n,  \
        vPrintf_n,vPrintf_n,vPrintf_n,vPrintf_n,vPrintf_n,  \
        vPrintf_n,vPrintf_n,vPrintf_n,vPrintf_n,vPrintf_n,  \
        vPrintf_n,vPrintf_n,vPrintf_n,vPrintf_n,vPrintf_n,  \
        vPrintf_n,vPrintf_n,vPrintf_n,vPrintf_n,vPrintf_n,  \
        vPrintf_n,vPrintf_n,vPrintf_n,vPrintf_n,vPrintf_n,  \
        vPrintf_n,vPrintf_n,vPrintf_n,vPrintf_n,vPrintf_n,  \
        vPrintf_n,vPrintf_n,vPrintf_n,vPrintf_n,vPrintf_n,  \
        vPrintf_n,vPrintf_n,vPrintf_n,vPrintf_n,vPrintf_n,  \
        vPrintf_n,vPrintf_n,vPrintf_n,vPrintf_n,vPrintf_n,  \
        vPrintf_n,vPrintf_n,vPrintf_n,vPrintf_n,vPrintf_n,  \
        vPrintf_n,vPrintf_n,vPrintf_n,vPrintf_n,vPrintf_n,  \
        vPrintf_no_op)(__VA_ARGS__)

#define Printf_deslash(...)                                             \
    Get_printf_macro(                                                   \
        _0, __VA_ARGS__ __VA_OPT__(,)                                   \
        Printf_n_deslash,Printf_n_deslash,Printf_n_deslash,Printf_n_deslash,Printf_n_deslash, \
        Printf_n_deslash,Printf_n_deslash,Printf_n_deslash,Printf_n_deslash,Printf_n_deslash, \
        Printf_n_deslash,Printf_n_deslash,Printf_n_deslash,Printf_n_deslash,Printf_n_deslash, \
        Printf_n_deslash,Printf_n_deslash,Printf_n_deslash,Printf_n_deslash,Printf_n_deslash, \
        Printf_n_deslash,Printf_n_deslash,Printf_n_deslash,Printf_n_deslash,Printf_n_deslash, \
        Printf_n_deslash,Printf_n_deslash,Printf_n_deslash,Printf_n_deslash,Printf_n_deslash, \
        Printf_n_deslash,Printf_n_deslash,Printf_n_deslash,Printf_n_deslash,Printf_n_deslash, \
        Printf_n_deslash,Printf_n_deslash,Printf_n_deslash,Printf_n_deslash,Printf_n_deslash, \
        Printf_n_deslash,Printf_n_deslash,Printf_n_deslash,Printf_n_deslash,Printf_n_deslash, \
        Printf_n_deslash,Printf_n_deslash,Printf_n_deslash,Printf_n_deslash,Printf_n_deslash, \
        Printf_n_deslash,Printf_n_deslash,Printf_n_deslash,Printf_n_deslash,Printf_n_deslash, \
        Printf_n_deslash,Printf_n_deslash,Printf_n_deslash,Printf_n_deslash,Printf_n_deslash, \
        Printf_n_deslash,Printf_n_deslash,Printf_n_deslash,Printf_n_deslash,Printf_n_deslash, \
        Printf_n_deslash,Printf_n_deslash,Printf_n_deslash,Printf_n_deslash,Printf_n_deslash, \
        Printf_n_deslash,Printf_n_deslash,Printf_n_deslash,Printf_n_deslash,Printf_n_deslash, \
        Printf_n_deslash,Printf_n_deslash,Printf_n_deslash,Printf_n_deslash,Printf_n_deslash, \
        Printf_n_deslash,Printf_n_deslash,Printf_n_deslash,Printf_n_deslash,Printf_n_deslash, \
        Printf_n_deslash,Printf_n_deslash,Printf_n_deslash,Printf_n_deslash,Printf_n_deslash, \
        Printf_n_deslash,Printf_n_deslash,Printf_n_deslash,Printf_n_deslash,Printf_n_deslash, \
        Printf_n_deslash,Printf_n_deslash,Printf_n_deslash,Printf_n_deslash,Printf_n_deslash, \
        PRINTFN_DESLASHOOP)(__VA_ARGS__)

#define vPrintf_deslash(...)                                            \
    Get_printf_macro(                                                   \
        _0, __VA_ARGS__ __VA_OPT__(,)                                   \
        vPrintf_n_deslash,vPrintf_n_deslash,vPrintf_n_deslash,vPrintf_n_deslash,vPrintf_n_deslash, \
        vPrintf_n_deslash,vPrintf_n_deslash,vPrintf_n_deslash,vPrintf_n_deslash,vPrintf_n_deslash, \
        vPrintf_n_deslash,vPrintf_n_deslash,vPrintf_n_deslash,vPrintf_n_deslash,vPrintf_n_deslash, \
        vPrintf_n_deslash,vPrintf_n_deslash,vPrintf_n_deslash,vPrintf_n_deslash,vPrintf_n_deslash, \
        vPrintf_n_deslash,vPrintf_n_deslash,vPrintf_n_deslash,vPrintf_n_deslash,vPrintf_n_deslash, \
        vPrintf_n_deslash,vPrintf_n_deslash,vPrintf_n_deslash,vPrintf_n_deslash,vPrintf_n_deslash, \
        vPrintf_n_deslash,vPrintf_n_deslash,vPrintf_n_deslash,vPrintf_n_deslash,vPrintf_n_deslash, \
        vPrintf_n_deslash,vPrintf_n_deslash,vPrintf_n_deslash,vPrintf_n_deslash,vPrintf_n_deslash, \
        vPrintf_n_deslash,vPrintf_n_deslash,vPrintf_n_deslash,vPrintf_n_deslash,vPrintf_n_deslash, \
        vPrintf_n_deslash,vPrintf_n_deslash,vPrintf_n_deslash,vPrintf_n_deslash,vPrintf_n_deslash, \
        vPrintf_n_deslash,vPrintf_n_deslash,vPrintf_n_deslash,vPrintf_n_deslash,vPrintf_n_deslash, \
        vPrintf_n_deslash,vPrintf_n_deslash,vPrintf_n_deslash,vPrintf_n_deslash,vPrintf_n_deslash, \
        vPrintf_n_deslash,vPrintf_n_deslash,vPrintf_n_deslash,vPrintf_n_deslash,vPrintf_n_deslash, \
        vPrintf_n_deslash,vPrintf_n_deslash,vPrintf_n_deslash,vPrintf_n_deslash,vPrintf_n_deslash, \
        vPrintf_n_deslash,vPrintf_n_deslash,vPrintf_n_deslash,vPrintf_n_deslash,vPrintf_n_deslash, \
        vPrintf_n_deslash,vPrintf_n_deslash,vPrintf_n_deslash,vPrintf_n_deslash,vPrintf_n_deslash, \
        vPrintf_n_deslash,vPrintf_n_deslash,vPrintf_n_deslash,vPrintf_n_deslash,vPrintf_n_deslash, \
        vPrintf_n_deslash,vPrintf_n_deslash,vPrintf_n_deslash,vPrintf_n_deslash,vPrintf_n_deslash, \
        vPrintf_n_deslash,vPrintf_n_deslash,vPrintf_n_deslash,vPrintf_n_deslash,vPrintf_n_deslash, \
        vPrintf_n_deslash,vPrintf_n_deslash,vPrintf_n_deslash,vPrintf_n_deslash,vPrintf_n_deslash, \
        PRINTFN_DESLASHOOP)(__VA_ARGS__)

#define Clear_error_buffer                          \
    if(stardata != NULL &&                          \
       stardata->tmpstore != NULL &&                \
       stardata->tmpstore->error_buffer != NULL)    \
    {                                               \
        clear_error_buffer(stardata);               \
    }


#define Clear_printf_buffer                                 \
    if(likely(stardata != NULL &&                           \
              stardata->preferences!=NULL &&                \
              stardata->preferences->internal_buffering))   \
    {                                                       \
        clear_printf_buffer(stardata);                      \
    }

#endif // BUFFERING_FUNCTION_MACROS_H
