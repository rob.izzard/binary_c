#include "../binary_c.h"
No_empty_translation_unit_warning;


#define STATE_MESSAGE "binary_c pid %d. stardata=%p (Z=%g Initial : M1=%g M2=%g sep=%g per=%g ecc=%g : Now : M1=%g M2=%g sep=%g per=%g ecc=%g) ", \
        getpid(),                                                       \
        (void*)stardata,                                                \
        stardata->common.metallicity,                                   \
        stardata->common.zero_age.mass[0],                              \
        stardata->common.zero_age.mass[1],                              \
        stardata->common.zero_age.separation[0],                        \
        stardata->common.zero_age.orbital_period[0],                    \
        stardata->common.zero_age.eccentricity[0],                      \
        stardata->star[0].mass,                                         \
        stardata->star[1].mass,                                         \
        stardata->common.orbit.separation,                              \
        stardata->common.orbit.period,                                  \
        stardata->common.orbit.eccentricity

// macro to print to stdout and stderr
#define PRINT2(...)                             \
    {                                           \
        if(stardata != NULL)                    \
        {                                       \
            if((stdout) != NULL)                \
            {                                   \
                _printf(STATE_MESSAGE);         \
                _printf(__VA_ARGS__);           \
            }                                   \
            if((stderr) != NULL)                \
            {                                   \
                fprintf(stderr,STATE_MESSAGE);  \
                fprintf(stderr,__VA_ARGS__);    \
            }                                   \
        }                                       \
    }

void Gnu_format_args(4,5) No_return
    binary_c_fail_fprintf(struct stardata_t * Restrict const stardata,
                          const char * Restrict const filename ,
                          const int fileline,
                          const char * Restrict const format,
                          ...)
{
    /*
     * General failure of some sort, a failstring
     * is output to stdout and stderr.
     */
    double t = stardata!=NULL ? stardata->model.time : -1.0;
    va_list args;
    va_start(args,format);

    /* s contains the message */
    char sid[14];
    char s[MAX_DEBUG_PRINT_SIZE];
    vsnprintf(s,MAX_DEBUG_PRINT_SIZE,format,args);
    chomp(s);

#ifdef BINARY_C_API
    if(stardata!=NULL &&
       stardata->model.id_number!=-1)
    {
        snprintf(sid,14,"(star %d) ",stardata->model.id_number);
    }
    else
#endif // BINARY_C_API
    {
        sid[0] = 0; // empty string
    }

    /* make the filename, remove nan e.g. in remnant (replace with n_n) */
    char f[MAX_DEBUG_PRINT_SIZE];
#ifdef DEBUG_COLOURS
    if(stardata && stardata->store)
    {
        snprintf(f,MAX_DEBUG_PRINT_SIZE,"%s%-40.40s",stardata->store->colours[RED],filename);
    }
    else
    {
        snprintf(f,MAX_DEBUG_PRINT_SIZE,"%-40.40s",filename);
    }
#else
    snprintf(f,MAX_DEBUG_PRINT_SIZE,"%-40.40s",filename);
#endif

#ifdef DEBUG_REMOVE_NAN_FROM_FILENAMES
    char *x=strstr(f,"nan");
    if(x!=NULL) *(x)='_';
#endif

    /* output to the appropriate stream and flush */
#ifdef DEBUG_COLOURS
#ifdef DEBUG_LINENUMBERS
    PRINT2("%sbinary_c_fail: %s%g %s% 6d%s : %s : %s%s\n",
           stardata->store->colours[CYAN],sid,t,
           stardata->store->colours[GREEN],fileline,
           stardata->store->colours[YELLOW],f,
           stardata->store->colours[COLOUR_RESET],s);
#else
    PRINT2("%sbinary_c_fail: %s%g %s%s%s : %s\n",
           stardata->store->colours[CYAN],sid,t,
           stardata->store->colours[YELLOW],f,
           stardata->store->colours[COLOUR_RESET],s);
#endif //DEBUG_LINENUMBERS
#else

#ifdef DEBUG_LINENUMBERS
    PRINT2("binary_c_fail: %g %s :% 6d : %s\n",t,f,fileline,s);
#else
    PRINT2("binary_c_fail: %g %s : %s\n",t,f,s);
#endif //DEBUG_LINENUMBERS
#endif //DEBUG_COLOURS

    fflush(DEBUG_STREAM);

#ifdef FILE_LOG
    // flush log if it exists
    if(stardata != NULL &&
       stardata->model.log_fp!=NULL)
    {
        fflush(stardata->model.log_fp);
    }
#endif // FILE_LOG

    va_end(args);

    Exit_binary_c(BINARY_C_DELIBERATE_FAIL,
                  "Deliberately failed for an unknown reason");
}
