#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "buffering_macros.h"

/*
 * testing settings
 */
/*
  #undef BUFFERED_STRING_OVERRUN_WARNINGS
  #define BUFFERED_STRING_OVERRUN_WARNINGS
  #undef BUFFERED_PRINTF_MAX_BUFFER_SIZE
  #define BUFFERED_PRINTF_MAX_BUFFER_SIZE ((size_t)(1024*2))
*/

static void __buffered_vprintf_error(struct tmpstore_t * t,
                                     const char * const format,
                                     ...);

/*
 * Send some output into the buffer.
 *
 * Returns the number of characters put into the buffer,
 * or an error code < 0 on error.
 *
 * Note: in case of error, do NOT call Exit_binary_c
 * because that will call this function which results
 * in a loop. Instead, use the error buffer and return
 * an error code (<=0, see buffered_printf_macros.h).
 *
 * There are several things we do:
 *
 * First, make the "buffer_string". This is the
 * string that is sent into this function. It has
 * a maximum length MAX_BUFFERED_STRING_LENGTH.
 *
 * Second, we allocate space to fill the raw_buffer.
 * This is the big buffer which is eventually output
 * e.g. through the API. This has maximum size
 * BUFFERED_PRINTF_MAX_BUFFER_SIZE if
 * BUFFERED_PRINTF_MAX_BUFFER_SIZE is non-zero.
 *
 * Third, we copy the buffer_string into the raw_buffer.
 *
 * If there is a failure, e.g. the raw_buffer is full,
 * return the appropriate error code and set the error_buffer.
 *
 * If we cannot set the error_buffer, output a message to stderr.
 *
 * If we cannot do that, we cannot do much and it's up to you
 * to fix things.
 *
 *
 * Note:
 *
 * binary_c_buffered_printf is also a binary_c_API_function because
 * it is exposed to the outside world through the API.
 *
 * It is *also* used internally, so is always built, whether
 * BINARY_C_API is defined or not.
 */

int binary_c_API_function
Gnu_format_args(3,0)
    binary_c_buffered_vprintf(
        struct stardata_t * Restrict const stardata,
        const Boolean do_deslash,
        const char * Restrict const format,
        va_list args)
{
    int retval = BUFFERED_PRINTF_NO_ERROR; /* return value */

    /* alias tmpstore often */
    struct tmpstore_t * Restrict const t =
        stardata != NULL ? stardata->tmpstore : NULL;

    if(t != NULL)
    {
        /*
         * Allocate memory for the error_buffer if it
         * is not already allocated
         */
        if(unlikely(stardata->tmpstore->error_buffer == NULL))
        {
            stardata->tmpstore->error_buffer =
                Malloc(sizeof(char)*BUFFERED_PRINTF_ERROR_BUFFER_SIZE);
        }

        /*
         * We cannot set the error buffer : output
         * to stderr and just return.
         */
        if(stardata->tmpstore->error_buffer == NULL)
        {
            fprintf(stderr,
                    "stardata->tmpstore->error_buffer could not be allocated in buffered_printf().\n");
            return BUFFERED_PRINTF_ERROR_BUFFER_ALLOC_ERROR;
        }
        else
        {

            /*
             * Make the string based on the format string
             * and args passed in.
             */
            char * string;
            const int vasprintf_ret = vasprintf(&string,
                                                format,
                                                args);
            size_t dn = (size_t) vasprintf_ret;
            size_t dnbytes = sizeof(char) * dn;

            if(vasprintf_ret < 0)
            {
                /*
                 * vasprintf error
                 */
                retval = BUFFERED_PRINTF_ASPRINTF_ERROR;

                __buffered_vprintf_error(t,
                                         "SYSTEM_ERROR failed to vasprintf string in buffered_printf()\n");
            }
            else
            {
                /*
                 * We made the output string
                 *
                 * Perhaps deslash it...
                 */
                if(unlikely(do_deslash == TRUE))
                {
                    /*
                     * convert _slash_ to /
                     */
                    dnbytes = deslash(string);
                    dn = dnbytes / sizeof(char);
                }

                if(stardata->preferences->internal_buffering == INTERNAL_BUFFERING_LINES)
                {
                    /*
                     * Call the external function which captures the line,
                     * this function should then set the line_buffer back to NULL
                     */
                    stardata->tmpstore->line_buffer = string;
                    if(stardata->preferences->function_hooks[BINARY_C_HOOK_flush_line_buffer] == NULL)
                    {
                        Exit_binary_c(BINARY_C_NO_FUNCTION_IN_FLUSH_LINE_BUFFER_HOOK,
                                      "Tried to output to lines but no line-buffer flushing function is set in the appropriate hook stardata->preferences->function_hooks[BINARY_C_HOOK_flush_line_buffer]. You need to set this if you want to use line buffering.");
                    }
                    else
                    {
                        stardata->preferences->function_hooks[BINARY_C_HOOK_flush_line_buffer](stardata);
                    }
                }
                else
                {
                    /*
                     * Fill the raw_buffer
                     *
                     * Now check the raw_buffer: do we need to
                     * make it larger?
                     */
                    if(dnbytes > 0)
                    {
                        if(stardata != NULL &&
                           stardata->preferences != NULL &&
                           stardata->preferences->internal_buffering > 0)
                        {
                            /*
                             * data should be buffered here, then dumped
                             * elsewhere (e.g. at the end of evolution)
                             */

                            /*
                             * If a buffer already exists, and it ends in \n\0,
                             * reduce the buffer size by 1 to overwrite the NULL
                             * char at the end with the string.
                             */
                            if(t->raw_buffer_size > 2 &&
                               t->raw_buffer[t->raw_buffer_size-1] == '\0'
                               && t->raw_buffer[t->raw_buffer_size-2] == '\n')
                            {
                                t->raw_buffer_size--;
                            }

                            /*
                             * If final char in the new string is a newline, require a NULL
                             * after it just in case this is the end
                             */
                            const size_t lastn = dnbytes == 0 ? 0 : (Max((size_t)0,(size_t)dnbytes-1));
                            const Boolean require_null = string[lastn] == '\n';

                            if(require_null==TRUE) dnbytes++;

                            /*
                             * Calculate the size of the new buffer
                             */
                            const size_t new_buffer_size =
                                t->raw_buffer_size + (size_t)(dnbytes + 1);

                            /*
                             * Calculate the size of what needs to be allocated
                             */
                            const size_t new_alloc_size =
                                new_buffer_size > t->raw_buffer_alloced ?

                                /* require more memory */
                                Max(new_buffer_size+1,
                                    Buffer_next) :

                                /* previous size was enough */
                                t->raw_buffer_alloced;

                            /*
                             * GCC gives errors when
                             * BUFFERED_PRINTF_MAX_BUFFER_SIZE == 0 because
                             * the inequality is always true. Hence we ignore
                             * type-limits warnings here.
                             */
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wtype-limits"
                            if(BUFFERED_PRINTF_MAX_BUFFER_SIZE == 0 ||
                               new_alloc_size < BUFFERED_PRINTF_MAX_BUFFER_SIZE)
                            {
#pragma GCC diagnostic pop
                                /*
                                 * Required size is less than the max buffer size,
                                 * or we have no limit.
                                 *
                                 * Allocate memory for the buffer if more
                                 * is required.
                                 *
                                 * Note: we assume a generic error unless
                                 *       we succeed.
                                 */
                                if(new_alloc_size > t->raw_buffer_alloced)
                                {
                                    t->raw_buffer =
                                        t->raw_buffer == NULL ?
                                        Malloc(new_alloc_size) :
                                        Realloc(t->raw_buffer,new_alloc_size);
                                }

                                /*
                                 * Check for allocation failure
                                 */
                                if(t == NULL || t->raw_buffer == NULL)
                                {
                                    retval = BUFFERED_PRINTF_ALLOC_ERROR;
                                    __buffered_vprintf_error(t,
                                                             "SYSTEM_ERROR Malloc/Realloc failed, out of memory? tmpstore = %p\n");
                                }

                                /*
                                 * Copy the string to the raw_buffer if we can
                                 */
                                else
                                {
                                    /*
                                     * Alloc success: update buffer size
                                     */
                                    t->raw_buffer_alloced = new_alloc_size;

                                    /* put string in the buffer */
                                    memcpy(t->raw_buffer + t->raw_buffer_size,
                                           string,
                                           dnbytes);

                                    t->raw_buffer_size += dnbytes;

                                    /* make trailing byte NULL if trailing byte is \n */
                                    if(require_null)
                                    {
                                        t->raw_buffer[t->raw_buffer_size-1]='\0';
                                    }

                                    /* return number of characters processed */
                                    retval = (int)dn;
                                }
                            }
                            else
                            {
                                /*
                                 * We cannot output :
                                 * this means we have filled the buffer.
                                 */
                                retval = BUFFERED_PRINTF_FULL_ERROR;
                                __buffered_vprintf_error(t,
                                                         "SYSTEM_ERROR Cannot Printf because buffer has been exceeded (want %zu bytes, max is %zu)\n",
                                                         new_alloc_size,
                                                         BUFFERED_PRINTF_MAX_BUFFER_SIZE);
                            }
                        }
                        else
                            /*
                             * no buffering, or stardata or stardata->preferences
                             * is NULL: write directly to stdout
                             */
                        {
                            fwrite((char *)string,dn,1,stdout);

                            /* return number of characters processed */
                            retval = dn;
                        }
                    }
                    else
                    {
                        /*
                         * dnbytes == 0, nothing to do, just
                         * return BUFFERED_PRINTF_NO_ERROR;
                         */
                        retval = BUFFERED_PRINTF_NO_ERROR;
                    }
                }
            }

            Safe_free(string);
        }
    }
    else
    {
        retval = BUFFERED_PRINTF_STARDATA_ERROR;
    }

    if(0 && Buffered_printf_error(retval))
    {
        __buffered_vprintf_error(
            t,
            "SYSTEM_ERROR buffered_printf error %d \"%s\" (stardata=%p stardata->preferences=%p stardata->store=%p stardata->tmpstore=%p t=%p raw_buffer=%p error_buffer=%p buffer_alloced=%zu next alloc=%zu max=%zu) : time = %g\n",
            retval,
            Buffered_printf_error_string(retval),
            stardata,
            stardata==NULL ? NULL : stardata->preferences,
            stardata==NULL ? NULL : stardata->tmpstore,
            stardata==NULL ? NULL : stardata->store,
            t,
            t==NULL ? 0 : t->raw_buffer,
            t==NULL ? 0 : t->error_buffer,
            t==NULL ? 0 : t->raw_buffer_alloced,
            t==NULL ? 0 : Buffer_next,
            (size_t)BUFFERED_PRINTF_MAX_BUFFER_SIZE,
            stardata!=NULL ? (stardata->model.time) : -1.0
            );

        /*
         * Uncomment the next line for backtraces:
         * this requires you to do a normal meson/ninja
         * build (NOT buildtype=release) AND have
         * libbacktrace installed, e.g. from
         * https://github.com/ianlancetaylor/libbacktrace
         */
        //Backtrace;
        terminate_evolution(stardata,
                            t==NULL ? "Unknown" : t->error_buffer);
    }

    return retval;
}


static void __buffered_vprintf_error(struct tmpstore_t * t,
                                     const char * const format,
                                     ...)
{
    /*
     * There was an error in buffered_printf...
     */
    va_list args;
    va_start(args,format);
    if(t != NULL &&
       t->error_buffer != NULL)
    {
        /*
         * Send error string to the error_buffer
         */
        if(vsnprintf(t->error_buffer,
                     BUFFERED_PRINTF_ERROR_BUFFER_SIZE,
                     format,
                     args) >= (int)BUFFERED_PRINTF_ERROR_BUFFER_SIZE)
        {
            /*
             * Truncated error string : warn through stderr
             */
            fprintf(stderr,
                    "Error was truncated : ");
            vfprintf(stderr,
                     format,
                     args);
        }
        t->error_buffer_set = TRUE;
    }
    else
    {
        /*
         * Everything failed : output to stderr
         * and hope someone is watching
         */
        vfprintf(stderr,
                 format,
                 args);
    }
    va_end(args);
}
