#include "../binary_c.h"
No_empty_translation_unit_warning;


void buffer_info(const struct stardata_t * Restrict const stardata,
                 char ** const buffer,
                 size_t * const size,
                 char ** const error_buffer
    )
{
    /*
     * Set the buffer location and size for external access
     */
    if(buffer != NULL)
    {
        *buffer = NULL;
    }
    if(size != NULL)
    {
        *size = 0;
    }
    if(error_buffer != NULL)
    {
        *error_buffer = NULL;
    }
    
    if(stardata != NULL &&
       stardata->tmpstore != NULL)
    {
        if(buffer != NULL)
        {
            *buffer = stardata->tmpstore->raw_buffer;
        }
        if(size != NULL)
        {
            *size = stardata->tmpstore->raw_buffer_size;
        }
    
        /*
         * Ditto for the error buffer
         */
        if(error_buffer != NULL)
        {
            *error_buffer = stardata->tmpstore->error_buffer;
        }
    }
}
