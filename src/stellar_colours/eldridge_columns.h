#ifndef ELDRIDGE_COLUMNS_H
#define ELDRIDGE_COLUMNS_H

#define ELDRIDGE_U 0
#define ELDRIDGE_B 1
#define ELDRIDGE_V 2
#define ELDRIDGE_R 3
#define ELDRIDGE_I 4
#define ELDRIDGE_J 5
#define ELDRIDGE_H 6
#define ELDRIDGE_K 7
#define ELDRIDGE_u 8
#define ELDRIDGE_g 9
#define ELDRIDGE_r 10
#define ELDRIDGE_i 11
#define ELDRIDGE_z 12
#define ELDRIDGE_f300w 13
#define ELDRIDGE_f336w 14
#define ELDRIDGE_f435w 15
#define ELDRIDGE_f450w 16
#define ELDRIDGE_f555w 17
#define ELDRIDGE_f606w 18
#define ELDRIDGE_f814w 19

#endif // ELDRIDGE_COLUMNS_H
