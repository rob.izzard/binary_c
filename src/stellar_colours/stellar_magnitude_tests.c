#include "../binary_c.h"
No_empty_translation_unit_warning;

#include <stdio.h>

#if (defined STELLAR_COLOURS) && (defined STELLAR_COLOUR_TESTS)

#define OUTPUT(FP,F,...) printf(F,__VA_ARGS__); fprintf(FP,F,__VA_ARGS__);

void stellar_magnitude_tests(struct stardata_t * const stardata,
                          struct star_t * const star)
{
    /*
     * Test magnitudes and colour conversions
     */
    double magnitudes[STELLAR_MAGNITUDE_NUMBER];
    double magnitudes2[STELLAR_MAGNITUDE_NUMBER];

    stardata->common.metallicity=0.02;
    star->luminosity=1.0;

    {
        FILE * const Fig11a = fopen("/tmp/Fig11a.dat","w");
        FILE * const Fig11b = fopen("/tmp/Fig11b.dat","w");
        FILE * const Fig13a = fopen("/tmp/Fig13a.dat","w");
        FILE * const Fig13b = fopen("/tmp/Fig13b.dat","w");

        double m;
        for(m=0.5; m<20; m+=1.0)
        {
            star->mass = m;

            double logteff;
            for(logteff=log10(1500.0); logteff<log10(4e5); logteff+=0.05)
            {
                double teff = exp10(logteff);

                // L = 4pi sigma R^2 Teff^4 hence
                // R = sqrt(L/(4pi sigma Teff^4))

                /* get colours */
                star->luminosity=1.0;
                star->radius = sqrt( L_SUN * star->luminosity / (4.0 * PI * STEFAN_BOLTZMANN_CONSTANT * Pow4(teff))) / R_SUN;
                stellar_magnitudes(stardata,star,&magnitudes);

                // A magnitude 1 star is exactly a hundred times brighter than a magnitude 6 star

                /* get colours for a star of twice the luminosity */
                star->luminosity=2.0;
                star->radius = sqrt( L_SUN * star->luminosity / (4.0 * PI * STEFAN_BOLTZMANN_CONSTANT * Pow4(teff))) / R_SUN;
                stellar_magnitudes(stardata,star,&magnitudes2);

                printf("MAGS ");
                Stellar_magntiude_index i;
                for(i=0;i<STELLAR_MAGNITUDE_NUMBER;i++)
                {
                    printf("%d = %s = %g; ",i,stellar_magnitude_strings[i],magnitudes[i]);
                }
                printf("\n");

                /*
                 * LA/LB = 2 = pow(100.0,(magA-magB)/5)
                 *
                 * hence 2.5 log10(LA/LB) = magA - magB = 2.5 * log10(2)
                 */
                const double testresult = 2.5 * log10(2.0);
                double err = 1e10;
                printf("TEST mag(L=1) vs mag(L=2) (should all be %g) ",testresult);
                for(i=0;i<STELLAR_MAGNITUDE_NUMBER;i++)
                {
                    printf("%d = %g; ",i,magnitudes[i] - magnitudes2[i]);
                    err = Min(fabs(magnitudes[i]-magnitudes2[i]-testresult),err);
                }
                printf(" : worst err = %5.2f %% \n",(err/testresult*100.0));

                // add magnitudes, see if we get what we expect
                printf("TEST ADD MAGS %g (should be ~%g)\n",add_magnitudes(5.1,4.6),4.07);

                /* magnitude difference test complete, go back to L=1 */
                star->luminosity=1.0;

                /*
                 * Data for Jordi et al 2010 Fig 11, using the cubic fit
                 */
                gaia_magnitudes(magnitudes,GAIA_CONVERSION_UBVRI_UNIVARIATE_JORDI2010);
                OUTPUT(Fig11a,
                       "%g %g %g %g %g %g %g %g %g\n",
                       teff,
                       star->radius,
                       magnitudes[STELLAR_MAGNITUDE_V] - magnitudes[STELLAR_MAGNITUDE_I],
                       magnitudes[STELLAR_MAGNITUDE_GAIA_G] - magnitudes[STELLAR_MAGNITUDE_V],
                       magnitudes[STELLAR_MAGNITUDE_V] - magnitudes[STELLAR_MAGNITUDE_GAIA_GBP],
                       magnitudes[STELLAR_MAGNITUDE_V] - magnitudes[STELLAR_MAGNITUDE_GAIA_GRP],
                       magnitudes[STELLAR_MAGNITUDE_GAIA_G] - magnitudes[STELLAR_MAGNITUDE_GAIA_GBP],
                       magnitudes[STELLAR_MAGNITUDE_GAIA_G] - magnitudes[STELLAR_MAGNITUDE_GAIA_GRP],
                       magnitudes[STELLAR_MAGNITUDE_GAIA_GBP] - magnitudes[STELLAR_MAGNITUDE_GAIA_GRP]
                    );

                /*
                 * Data for Jordi et al 2010 Fig 13,using the cubic fit
                 */
                gaia_magnitudes(magnitudes,GAIA_CONVERSION_ugriz_UNIVARIATE_JORDI2010);
                OUTPUT(Fig13a,
                       "%g %g %g %g %g %g %g\n",
                       teff,
                       star->radius,
                       magnitudes[STELLAR_MAGNITUDE_g] - magnitudes[STELLAR_MAGNITUDE_i],
                       magnitudes[STELLAR_MAGNITUDE_GAIA_G] - magnitudes[STELLAR_MAGNITUDE_g],
                       magnitudes[STELLAR_MAGNITUDE_g] - magnitudes[STELLAR_MAGNITUDE_GAIA_GBP],
                       magnitudes[STELLAR_MAGNITUDE_g] - magnitudes[STELLAR_MAGNITUDE_GAIA_GRP],
                       magnitudes[STELLAR_MAGNITUDE_GAIA_GBP] - magnitudes[STELLAR_MAGNITUDE_GAIA_GRP]);

                /*
                 * Data for Jordi et al 2010 Fig 11, using the cross-cubic fit
                 */
                gaia_magnitudes(magnitudes,GAIA_CONVERSION_UBVRI_BIVARIATE_JORDI2010);
                OUTPUT(Fig11b,
                       "%g %g %g %g %g %g %g %g %g\n",
                       teff,
                       star->radius,
                       magnitudes[STELLAR_MAGNITUDE_V] - magnitudes[STELLAR_MAGNITUDE_I],
                       magnitudes[STELLAR_MAGNITUDE_GAIA_G] - magnitudes[STELLAR_MAGNITUDE_V],
                       magnitudes[STELLAR_MAGNITUDE_V] - magnitudes[STELLAR_MAGNITUDE_GAIA_GBP],
                       magnitudes[STELLAR_MAGNITUDE_V] - magnitudes[STELLAR_MAGNITUDE_GAIA_GRP],
                       magnitudes[STELLAR_MAGNITUDE_GAIA_G] - magnitudes[STELLAR_MAGNITUDE_GAIA_GBP],
                       magnitudes[STELLAR_MAGNITUDE_GAIA_G] - magnitudes[STELLAR_MAGNITUDE_GAIA_GRP],
                       magnitudes[STELLAR_MAGNITUDE_GAIA_GBP] - magnitudes[STELLAR_MAGNITUDE_GAIA_GRP]
                    );

                /*
                 * Data for Jordi et al 2010 Fig 13,using the cross-cubic fit
                 */
                gaia_magnitudes(magnitudes,GAIA_CONVERSION_ugriz_BIVARIATE_JORDI2010);
                OUTPUT(Fig13b,
                       "%g %g %g %g %g %g %g %g %g %g\n",
                       teff,
                       star->radius,
                       magnitudes[STELLAR_MAGNITUDE_g] - magnitudes[STELLAR_MAGNITUDE_i],
                       magnitudes[STELLAR_MAGNITUDE_g] - magnitudes[STELLAR_MAGNITUDE_r],
                       magnitudes[STELLAR_MAGNITUDE_GAIA_G] - magnitudes[STELLAR_MAGNITUDE_g],
                       magnitudes[STELLAR_MAGNITUDE_g] - magnitudes[STELLAR_MAGNITUDE_GAIA_GBP],
                       magnitudes[STELLAR_MAGNITUDE_g] - magnitudes[STELLAR_MAGNITUDE_GAIA_GRP],
                       magnitudes[STELLAR_MAGNITUDE_GAIA_GBP] - magnitudes[STELLAR_MAGNITUDE_GAIA_GRP],
                       magnitudes[STELLAR_MAGNITUDE_GAIA_G] - magnitudes[STELLAR_MAGNITUDE_GAIA_GBP],
                       magnitudes[STELLAR_MAGNITUDE_GAIA_G] - magnitudes[STELLAR_MAGNITUDE_GAIA_GRP]);
                printf("\n");
            }
        }
        fclose(Fig11a);
        fclose(Fig11b);
        fclose(Fig13a);
        fclose(Fig13b);
    }
    Exit_binary_c(0,"stellar colour tests completed");
}

#endif // STELLAR_COLOURS && STELLAR_COLOUR_TESTS
