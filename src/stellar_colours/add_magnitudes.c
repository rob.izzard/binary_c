#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef STELLAR_COLOURS

/*
 * Function to add two magnitudes correctly
 *
 * See
 * https://www.cfa.harvard.edu/~dfabricant/huchra/ay145/magnitudes.pdf
 */



double Constant_function add_magnitudes(const double magA, const double magB)
{
    const double LA = luminosity_from_magnitude(magA);
    const double LB = luminosity_from_magnitude(magB);
    return magnitude_from_luminosity(LA+LB);
}

#endif
