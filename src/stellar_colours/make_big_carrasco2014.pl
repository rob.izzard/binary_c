#!/usr/bin/env perl
use strict;
use rob_misc;

foreach my $dat ('carrasco2014_table3.dat',
                 'carrasco2014_table4.dat',
                 'carrasco2014_table5.dat')
{
    my $h = $dat;
    my $name = ($h=~/(.*).dat/)[0];
    my $NAME = $name;
    $NAME =~s/(.*)/\U$1/gs;
    $h=~s/.dat$/.h/;
    my $data = slurp($dat);

    $data=~s/\s+$//;
    $data=~s/\n\s+/\n/g;
    $data=~s/^\s+//g;
    $data=~s/ +/,/g;
    $data=~s/\.,/.0,/g;
    $data=~s/$/,\\/gm;
    $data=~s/\s+$//;
    $data=~s/,\\$/\n/;

    # swap cols 1 and 2
    $data=~s/^([^,]+),([^,]+)/$2,$1/mg;

    # format for output
    $data = "#define $NAME \\
".$data;
    dumpfile($h,$data);
}
