#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef STELLAR_COLOURS

/*
 * Wrapper function to calculate all the stellar
 * colours that are available.
 *
 * magnitudes_p cannot be NULL.
 *
 * The *magnitudes_p is NULL the memory required to store
 * the magnitudes is allocated here.
 *
 * If both **magnitudes_p and *magnitudes_p are non-NULL,
 * we assume the space is already initialized.
 *
 * Please note that all magntiudes are Vega magnitudes.
 *
 * To understand the difference, please see this website:
 * https://www.astro.umd.edu/~ssm/ASTR620/mags.html
 */

void Nonnull_all_arguments stellar_magnitudes(struct stardata_t * const stardata,
                                              struct star_t * const star,
                                              double ** Restrict magnitudes_p)
{
    if(*magnitudes_p == NULL)
    {
        /*
         * Allocate space
         */
        *magnitudes_p = Calloc(STELLAR_MAGNITUDE_NUMBER,
                               sizeof(double));
    }
    double * const magnitudes = *magnitudes_p;

    /*
     * First, force magnitudes to be unphysical
     */
    int i;
    for(i=0;i<STELLAR_MAGNITUDE_NUMBER;i++)
    {
        magnitudes[i] = STELLAR_MAGNITUDE_UNPHYSICAL;
    }

    if(WHITE_DWARF(star->stellar_type))
    {
        /*
         * Get WD colours from Carrasco 2014.
         *
         * Note: these do not include Hubble filters
         */
        carrasco2014_wd_magnitudes(stardata,
                                   star,
                                   magnitudes);
    }
    else
    {
        /*
         * Other stars' colours from Eldridge's tables
         * which are based on the Basel library.
         */
        eldridge2015_magnitudes(stardata,
                                star,
                                magnitudes);


        /*
         * Make Gaia colours
         */
        gaia_magnitudes(stardata,
                        star,
                        magnitudes);
    }

#ifdef YBC
    if(stardata->preferences->YBC_path[0] != '\0' &&
       stardata->preferences->YBC_listfile[0] != '\0')
    {
        const double YBCf = YBC_magnitudes(stardata,
                                           star,
                                           magnitudes);

        /* YBC debugging */
        if(0 && star->stellar_type<=TPAGB)
        {
            printf("GAIAX %d %g %g %g %g %g %g %g %g\n",
                   star->stellar_type,
                   YBCf,
                   log10(Teff_from_star_struct(star)),
                   log10(star->luminosity),
                   logg(star),
                   magnitudes[STELLAR_MAGNITUDE_YBC_GAIA_GBP] - magnitudes[STELLAR_MAGNITUDE_YBC_GAIA_GRP],
                   magnitudes[STELLAR_MAGNITUDE_YBC_GAIA_G],
                   magnitudes[STELLAR_MAGNITUDE_GAIA_GBP] - magnitudes[STELLAR_MAGNITUDE_GAIA_GRP],
                   magnitudes[STELLAR_MAGNITUDE_GAIA_G]);
        }
    }
#endif // YBC

    Dprint("MAGS%d L = %g,  U = %g, B = %g, V = %g, R = %g, I = %g\n",
           star->starnum,
           star->luminosity,
           magnitudes[STELLAR_MAGNITUDE_U],
           magnitudes[STELLAR_MAGNITUDE_B],
           magnitudes[STELLAR_MAGNITUDE_V],
           magnitudes[STELLAR_MAGNITUDE_R],
           magnitudes[STELLAR_MAGNITUDE_I]);

}
#endif // STELLAR_COLOURS
