#include "../binary_c.h"
No_empty_translation_unit_warning;


/*
  PROGRAM mlrel
  implicit none
  integer i,j,k,n,nx,nxx
  integer nzgr,KURUCZ_NTGR,KURUCZ_NGGR
  parameter(nzgr=8,KURUCZ_NTGR=61,KURUCZ_NGGR= 11)
  real mass,z,lum,r,teff
  real logl,logte,logz,uminb,bminv,vmini
  real mv,mu,mb,mi,feh
  real dx,xmin,xmax,ymin,ymax,x(500),y(500),mvv(500),xx
  real y1(500),y2(500)
  real zgr(nzgr),tgr(KURUCZ_NTGR),ggr(KURUCZ_NGGR),ubv(nzgr,KURUCZ_NTGR,KURUCZ_NGGR,5)
  common /ubvdata/ zgr, tgr, ggr, ubv
  real zamsl,zamsr
  external zamsr,zamsl
*/

/*
 * Program to find the unresolved binary colours
 *
 * based on mlrel by Jarrod Hurley
 *
 * Takes in stardata and the number (0 or 1) of the star for which we are to
 * calculate magnitudes (for U,B,V,I) and colours. Then these magnitudes and
 * colours are placed in *M_v,  *U_minus_B, *B_minus_V, *V_minus_I, *M_b, *M_u
 * and *M_i
 */



#ifdef STELLAR_COLOURS

#define KURUCZ_NZGR 8
#define KURUCZ_NTGR 61
#define KURUCZ_NGGR 11

static void lt2ubv(struct store_t * store,
                   double logl, double logt, double logm, double logz,
                   double *mv, double *uminb, double *bminv, double *vmini,
                   double *mb, double *mu, double *mi, double *mbol_out);
#include "Kurucz.h"

void binary_magnitudes_free_memory(struct store_t * const store)
{
    /*
     * Call to free allocated memory
     */
    lt2ubv(store,
           0.0,0.0,0.0,0.0,
           NULL,NULL,NULL,NULL,
           NULL,NULL,NULL,NULL);
}

void binary_magnitudes_allocate_memory(struct store_t * const store)
{
    /*
     * Call to allocate memory
     */
    lt2ubv(store,
           0.0,0.0,0.0,0.0,
           NULL,NULL,NULL,NULL,
           NULL,NULL,NULL,NULL);
}

void Nonnull_some_arguments(1,2,4,5,6,7,8,9,10,11)
    binary_magnitudes(struct stardata_t * const stardata,
                      struct store_t * const store,
                      const Star_number starnum,
                      double * Restrict const M_v,
                      double * Restrict const U_minus_B,
                      double * Restrict const B_minus_V,
                      double * Restrict const V_minus_I,
                      double * Restrict const M_b,
                      double * Restrict const M_u,
                      double * Restrict const M_i,
                      double * Restrict const M_bol,
                      double * Restrict const L_u,
                      double * Restrict const L_b,
                      double * Restrict const L_v,
                      double * Restrict const L_i
        )
{
/************************************************************/
#ifdef CDEBUG
    printf("Call lt2ubv L=%g Teff=%g mass=%g Z=%g\n",
           stardata->star[starnum].luminosity,
           Teff(starnum),
           stardata->star[starnum].mass,
           stardata->common.metallicity);
#endif
    struct star_t * star = & stardata->star[starnum];
    const double logL = log10(star->luminosity);

    lt2ubv(store,
           logL,
           log10(Teff(starnum)),
           log10(star->mass),
           log10(stardata->common.metallicity/0.02),
           M_v, U_minus_B, B_minus_V, V_minus_I,
           M_b, M_u, M_i,M_bol);

    /* Use solar (M=1, Z=0.02) values to find beta factors */
    const double logL2 = 2.5 * logL;
    const double beta_u = *M_u - 6.34482 + logL2;
    const double beta_b = *M_b - 6.07549 + logL2;
    const double beta_v = *M_v - 5.35129 + logL2;
    const double beta_i = *M_i - 4.60777 + logL2;

    /* Hence calculate luminosities */
    if(L_u!=NULL)
    {
        *L_u = star->luminosity*exp10(-0.4*beta_u);
        *L_b = star->luminosity*exp10(-0.4*beta_b);
        *L_v = star->luminosity*exp10(-0.4*beta_v);
        *L_i = star->luminosity*exp10(-0.4*beta_i);
    }

#ifdef CDEBUG
/*
  printf("Beta factors and luminosities : \n");
  printf("U : beta = %g : L = %g\n",beta_u,L_u);
  printf("B : beta = %g : L = %g\n",beta_b,L_b);
  printf("V : beta = %g : L = %g\n",beta_v,L_v);
  printf("I : beta = %g : L = %g\n",beta_i,L_i);

  printf("Total L = %g c.f. sum %g\n",
  star->luminosity,
  star->luminosity*exp10(-0.4*beta_u)+
  star->luminosity*exp10(-0.4*beta_b)+
  star->luminosity*exp10(-0.4*beta_v)+
  star->luminosity*exp10(-0.4*beta_i));
*/
#endif
    return;
}


/************************************************************/
/************************************************************/
/************************************************************/
/************************************************************/
/************************************************************/

#define NEXTK karray[c++]

static void lt2ubv(struct store_t * store,
                   double logl, double logt, double logm, double logz,
                   double *mv, double *uminb, double *bminv, double *vmini,
                   double *mb, double *mu, double *mi, double *mbol_out)
{

    int ig,it,iz;
    int Pure_function indx(double ax, double *xx, int nx);
    double cm[6];
    const double gconst=-10.6071;
    double logg,dg1,dg2,dt1,dt2,dz1,dz2,mbol,bolc;

#define zgr (store->zgr)
#define tgr (store->tgr)
#define ggr (store->ggr)
#define ubv (store->ubv)

    double feh; /* Fe/H ? */
    int i,j,k;
    int c,d; /* counters */
/************************************************************/

    if(mv==NULL)
    {
        /*
         * If mv is NULL, then we either allocate
         * memory for the various arrays, or free it.
         */
        if(zgr!=NULL)
        {
            /*
             * Call to free memory
             */
            Safe_free(zgr);
            Safe_free(tgr);
            Safe_free(ggr);
            for(k=1; k<=KURUCZ_NZGR; k++)
            {
                for(i=1; i<=KURUCZ_NTGR; i++)
                {
                    for(j=1; j<=KURUCZ_NGGR; j++)
                    {
                        Safe_free(ubv[k][i][j]);
                    }
                    Safe_free(ubv[k][i]);
                }
                Safe_free(ubv[k]);
            }
            Safe_free(ubv);
            return;
        }
        else
        {
            /*
             * call to set up
             */
            extern double _binary_Kurucz_dat_start[];
            double * karray = _binary_Kurucz_dat_start;
                //{KURUCZ_DATA_ARRAY}; /* see Kurucz.h */
            zgr = Malloc(sizeof(double)*(KURUCZ_NZGR+1));
            tgr = Malloc(sizeof(double)*(KURUCZ_NTGR+1));
            ggr = Malloc(sizeof(double)*(KURUCZ_NGGR+1));

            /* get data from Kurucz.h  */
            c=0;
            ubv = Malloc(sizeof(double***)*(KURUCZ_NZGR+1));
            for(k=1; k<=KURUCZ_NZGR; k++)
            {
                ubv[k] = Malloc(sizeof(double**)*(KURUCZ_NTGR+1));
                for(i=1; i<=KURUCZ_NTGR; i++)
                {
                    ubv[k][i] = Malloc(sizeof(double*)*(KURUCZ_NGGR+1));
                    for(j=1; j<=KURUCZ_NGGR; j++)
                    {
                        ubv[k][i][j] = Malloc(sizeof(double)*6);
                        feh = NEXTK;
                        tgr[i] = NEXTK;
                        ggr[j] = NEXTK;
                        for(d=1;d<6;d++)
                        {
                            ubv[k][i][j][d] = NEXTK;
                        }
                    }
                    tgr[i]=log10(tgr[i]);
                }
//c....... zgr=log(Z/0.02), assuming X=0.76-3*Z and Z(sun)=0.02
                zgr[k] = -log10((3.0 + 37.425* exp10(-feh) )/38.0);
            }
        }
        return;
    }

/************************************************************/

    logg = logm + 4*logt - logl + gconst;
#ifdef CDEBUG
    printf("logm %g logt %g logl %g logg %g\n",logm,logt,logl,logg);
#endif

/*
 * find indices of log Z, log g and log T to interpolate between. don't allow
 * extrapolation outside log Z and log g grid.
 */

    ig = indx(logg,ggr,KURUCZ_NGGR);
    it = indx(logt,tgr,KURUCZ_NTGR);
    iz = indx(logz,zgr,KURUCZ_NZGR);

#ifdef CDEBUG
    printf("Indices : %d %d %d\n",ig,it,iz);
#endif

    dg1 = (logg - ggr[ig-1])/(ggr[ig] - ggr[ig-1]);
    dg1 = Max(0.0, Min(1.0, dg1));
    dg2 = 1.0 - dg1;
    dt1 = (logt - tgr[it-1])/(tgr[it] - tgr[it-1]);
    dt2 = 1.0 - dt1;
    dz1 = (logz - zgr[iz-1])/(zgr[iz] - zgr[iz-1]);
    dz1 = Min(0.0, Min(1.0, dz1));
    dz2 = 1.0 - dz1;
    for(k = 1; k<=5; k++)
    {
        cm[k] = ((ubv[iz][it][ig][k]*dg1 +
                  ubv[iz][it][ig-1][k]*dg2)*dt1
                 + (ubv[iz][it-1][ig][k]*dg1 +
                    ubv[iz][it-1][ig-1][k]*dg2)*dt2)*dz1 +
            ((ubv[iz-1][it][ig][k]*dg1 +
              ubv[iz-1][it][ig-1][k]*dg2)*dt1 +
             (ubv[iz-1][it-1][ig][k]*dg1 +
              ubv[iz-1][it-1][ig-1][k]*dg2)*dt2)*dz2;
#ifdef CDEBUG
        printf("cm[k=%d] = %g\n",k,cm[k]);
#endif
    }
    mbol = 4.75 - 2.5*logl;
    bolc = cm[1];

    /* Calculate M_v, U-B, B-V and V-I */
    *mv = mbol - bolc; /* bolc is bolc_V */
    *uminb = cm[2];
    *bminv = cm[3];
    *vmini = cm[4] + cm[5];

    /* Also M_b, M_u, M_i */
    *mb = *bminv + *mv;
    *mu = *uminb + *mb;
    *mi = *mv - *vmini;

    /* and mbol */
    *mbol_out = mbol;
}

int Pure_function indx(double ax, double *xx, int nx)
{
/*
 *  finds index of ax in monotonously increasing or decreasing array xx
 */
    int j,jl,jh,index;
    double sx;
//    printf("index : ax=%g nx=%d\n",ax,nx);
    sx = xx[nx] - xx[1];
    jl = 1;
    jh = nx;

    while (jh-jl > 1)
    {
        j = (jh + jl)/2;
        if((ax-xx[j])*sx > 0.0)
        {
            jl = j;
        }
        else
        {
            jh = j;
        }
    }
    index = jh;
    return index;
}


#endif /* STELLAR_COLOURS */
