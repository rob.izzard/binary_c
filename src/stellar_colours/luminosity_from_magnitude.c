#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef STELLAR_COLOURS

/*
 * convert absolute magnitude (in L_SUN) to luminosity
 *
 * Note: this is the definition of resolution B2 of the
 * International Astronomical Union.
 */

Constant_function double luminosity_from_magnitude(const double magnitude)
{
    return exp10(magnitude/-2.5) * BOLOMETRIC_L0 / L_SUN;
}

#endif // STELLAR_COLOURS
