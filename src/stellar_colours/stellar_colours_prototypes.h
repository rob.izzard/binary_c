#pragma once
#ifndef COLOURS_PROTOTYPES_H
#define COLOURS_PROTOTYPES_H
#ifdef STELLAR_COLOURS

void Nonnull_all_arguments eldridge2012_magnitudes(struct stardata_t * const stardata,
                                                   struct star_t * const star,
                                                   double * Restrict const magnitudes);
void Nonnull_all_arguments eldridge2015_magnitudes(struct stardata_t * const stardata,
                                                   struct star_t * const star,
                                                   double * Restrict const magnitudes);
void Nonnull_some_arguments(1,2,4,5,6,7,8,9,10,11) binary_magnitudes(struct stardata_t * const stardata,
                                                                     struct store_t * const store,
                                                                     const Star_number starnum,
                                                                     double * Restrict const M_v,
                                                                     double * Restrict const U_minus_B,
                                                                     double * Restrict const B_minus_V,
                                                                     double * Restrict const V_minus_I,
                                                                     double * Restrict const M_b,
                                                                     double * Restrict const M_u,
                                                                     double * Restrict const M_i,
                                                                     double * Restrict const M_bol,
                                                                     double * Restrict const L_u,
                                                                     double * Restrict const L_b,
                                                                     double * Restrict const L_v,
                                                                     double * Restrict const L_i);
void binary_magnitudes_free_memory(struct store_t * const store);
void binary_magnitudes_allocate_memory(struct store_t * const store);

void Nonnull_all_arguments gaia_magnitudes(struct stardata_t * const stardata,
                                           const struct star_t * const star,
                                           double * Restrict const magnitudes);

void Nonnull_all_arguments stellar_magnitudes(struct stardata_t * const stardata,
                                              struct star_t * const star,
                                              double ** Restrict magnitudes);
double Constant_function add_magnitudes(const double magA,
                                        const double magB);

#ifdef STELLAR_COLOUR_TESTS
void stellar_magnitude_tests(struct stardata_t * const stardata,
                             struct star_t * const star);
#endif
Constant_function double magnitude_from_luminosity(const double luminosity);
Constant_function double luminosity_from_magnitude(const double magnitude);


void Nonnull_all_arguments unresolved_stellar_magnitudes(struct stardata_t * const stardata,
                                                         double ** magnitudes_p);

void Nonnull_all_arguments carrasco2014_wd_magnitudes(
    struct stardata_t * const stardata,
    struct star_t * const star,
    double * const magnitudes);


#endif // STELLAR_COLOURS
#endif // COLOURS_PROTOTYPES_H
