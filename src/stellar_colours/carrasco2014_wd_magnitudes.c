#include "../binary_c.h"
No_empty_translation_unit_warning;

/*
 * Calculate stellar colours for white dwarfs according to
 * Carrasco et al. (2014, A&A 565, A11) based on their tables
 * 3-5.
 */

#ifdef STELLAR_COLOURS
#include "stellar_colour_macros.h"

void Nonnull_all_arguments carrasco2014_wd_magnitudes(
    struct stardata_t * const stardata,
    struct star_t * const star,
    double * const magnitudes)
{
    /*
     * Parameters are Teff and logg but are (note!)
     * inverted in order from Carrasco's tables
     * so we can interpolate them properly
     */
    const double params[2] = {
        logg(star),
        Teff_from_star_struct(star)
    };
    double mags[22];

    /*
     * Choose table based on WD atmosphere type
     */
    struct data_table_t * const t =
        star->white_dwarf_atmosphere_type == WHITE_DWARF_ATMOSPHERE_HYDROGEN ?
        stardata->store->carrasco2014_table3 :
        star->white_dwarf_atmosphere_type == WHITE_DWARF_ATMOSPHERE_HELIUM ?
        stardata->store->carrasco2014_table4 :
        star->white_dwarf_atmosphere_type == WHITE_DWARF_ATMOSPHERE_HYDROGEN_HELIUM ?
        stardata->store->carrasco2014_table5 :
        NULL;

    if(t==NULL)
    {
        Exit_binary_c(BINARY_C_WRONG_ARGUMENT,"Unknown WD atmosphere type when making stellar colours.");
    }

    Interpolate(t,
                params,
                mags,
                FALSE);

    /*
     * Get the WD luminosity assumed in the table
     */
    const double LWD = luminosity_from_magnitude(mags[CARRASCO2014_MBOL]);

    /*
     * Hence adjust magnitudes for non-Solar luminosity
     */
    const double ll =
        magnitude_from_luminosity(star->luminosity)
        - mags[CARRASCO2014_MBOL];


    if(0)printf("WDCHECK logg=%g Teff=%g L=%g Lsun (%g mag) vs table %g (from Mbol=%g, gives %g)\n",
                params[0],
                params[1],
                star->luminosity,
                magnitude_from_luminosity(star->luminosity),
                LWD,
                mags[CARRASCO2014_MBOL],
                luminosity_from_magnitude(mags[CARRASCO2014_MBOL])
        );

#define _Set_mag(FROM,TO)                       \
    magnitudes[TO] = mags[FROM] + ll;

    /* Johnson colours (Vega) */
    _Set_mag(CARRASCO2014_U,STELLAR_MAGNITUDE_U);
    _Set_mag(CARRASCO2014_B,STELLAR_MAGNITUDE_B);
    _Set_mag(CARRASCO2014_V,STELLAR_MAGNITUDE_V);
    _Set_mag(CARRASCO2014_R,STELLAR_MAGNITUDE_R);
    _Set_mag(CARRASCO2014_I,STELLAR_MAGNITUDE_I);
    _Set_mag(CARRASCO2014_J,STELLAR_MAGNITUDE_J);
    _Set_mag(CARRASCO2014_H,STELLAR_MAGNITUDE_H);
    _Set_mag(CARRASCO2014_KS,STELLAR_MAGNITUDE_K);

    /*
     * SDSS (AB)
     */
    _Set_mag(CARRASCO2014_u,STELLAR_MAGNITUDE_u);
    _Set_mag(CARRASCO2014_g,STELLAR_MAGNITUDE_g);
    _Set_mag(CARRASCO2014_r,STELLAR_MAGNITUDE_r);
    _Set_mag(CARRASCO2014_i,STELLAR_MAGNITUDE_i);
    _Set_mag(CARRASCO2014_z,STELLAR_MAGNITUDE_z);

    /*
     * Convert to Vega according to
     * http://www.astronomy.ohio-state.edu/~martini/usefuldata.html
     * (taken from Blanton et al., 2007)
     */
    magnitudes[STELLAR_MAGNITUDE_u] += + 0.91;
    magnitudes[STELLAR_MAGNITUDE_g] += - 0.08;
    magnitudes[STELLAR_MAGNITUDE_r] += + 0.16;
    magnitudes[STELLAR_MAGNITUDE_i] += + 0.37;
    magnitudes[STELLAR_MAGNITUDE_z] += + 0.54;

    /*
     * Gaia
     */
    _Set_mag(CARRASCO2014_GAIA_G,STELLAR_MAGNITUDE_GAIA_G);
    _Set_mag(CARRASCO2014_GAIA_GBP,STELLAR_MAGNITUDE_GAIA_GBP);
    _Set_mag(CARRASCO2014_GAIA_GRP,STELLAR_MAGNITUDE_GAIA_GRP);
    _Set_mag(CARRASCO2014_GAIA_GRVS,STELLAR_MAGNITUDE_GAIA_GRVS);
}
#endif // STELLAR_COLOURS
