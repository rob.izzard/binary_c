#!/usr/bin/env python3.8

import math
import numpy as np
from scipy.interpolate import griddata
import matplotlib.pyplot as plt

# read in the data
# https://stackoverflow.com/questions/43058462/reading-a-variable-white-space-delimited-table-in-python
data = np.genfromtxt('carrasco2014_table5.dat')

n = 50 # number of points on each side
minlogage = 4.0 # ignore log(age/y) less than this

# data columns (0 is first column)
col_mass = 2
col_age = 23
col_mag = 3
col_L = col_mag
col_Teff = 0
col_R = 0

Rsun = 6.956600000000000000000000000000e+10
Msun = 1.989100000000000004902937633489e+33
Lsun = 3.851500000000000274321803705319e+33
sigma = 5.670352798655924736991040813194e-05 # Stefan-Boltzmann

# convert mags to log10(luminosity)
data[:,col_L] = data[:,col_mag]/-2.5

# convert age to log10(age)
data[:,col_age] = np.log10(data[:,col_age])
# convert Teff to R
# L = 4pi sigma R^2 T^4
# R = (L/(4pi * sigma * T^4))**0.5
# and convert to Rsun
data[:,col_R] = np.log10((10**data[:,col_L]*Lsun/(4.0*math.pi*sigma*(data[:,col_Teff]**4)))**0.5/Rsun)

# get mins and maxes of the columnes
mins  = data.min(axis=0)
maxes = data.max(axis=0)

# interpolate
# https://docs.scipy.org/doc/scipy/reference/tutorial/interpolate.html#multivariate-data-interpolation-griddata

#print('logage from ',mins[col_age],maxes[col_age])
#print('mass from ', mins[col_mass], maxes[col_mass])
#print('logL from ', mins[col_L], maxes[col_L])
#exit()

grid_x, grid_y = np.mgrid[mins[col_mass]:maxes[col_mass]:(n*1j),
                          max(minlogage,mins[col_age]):maxes[col_age]:(n*1j)]

flatpoints = np.asarray(list(zip(grid_x.flatten(),grid_y.flatten())))

# make data for interpolation
points = []
valuesL = []
valuesR = []
for line in data:
    mass = line[col_mass]
    logage = line[col_age]
    logL = line[col_L]
    logR = line[col_R]
    if(logage > minlogage):
        points.append([mass,logage])
        valuesL.append(logL);
        valuesR.append(logR);

for point in flatpoints:
    # interpolate for L
    grid = griddata(points,valuesL,point,method='linear')
    if(np.isnan(grid[0])):
        grid = griddata(points,valuesL,point,method='nearest')
    L = grid[0]

    # interpolate for R
    grid = griddata(points,valuesR,point,method='linear')
    if(np.isnan(grid[0])):
        grid = griddata(points,valuesR,point,method='nearest')
    R = grid[0]

    # output
    print('{x} {y} {L} {R}'.format(x=point[0],y=point[1],L=L,R=R))
