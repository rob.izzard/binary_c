#pragma once
#ifndef YBC_PROTOTYPES_H
#define YBC_PROTOTYPES_H

/*
 * YBC function prototypes
 */
void YBC_load_fitsfile(struct stardata_t * const stardata,
                       struct YBC_FITSfile_info_t * const info,
                       char ** wanted_columns,
                       const int ncolumns,
                       const Boolean filter_null_data);
struct YBC_FITSfile_info_t ** YBC_load(struct stardata_t * const stardata Maybe_unused,
                                   const int index,
                                   char * const prefix,
                                   char * const file Maybe_unused,
                                   char ** wanted_columns,
                                   int wanted_ncolumns);

void load_YBC_tables_into_persistent_data(struct stardata_t * const stardata,
                                          char * const prefix);
double YBC_magnitudes(struct stardata_t * const stardata,
                      struct star_t * const star,
                      double * const magnitudes);
struct YBC_FITSfile_info_t ** YBC_analyse_files(struct stardata_t * const stardata Maybe_unused,
                                            char * const dirpath,
                                            char * const prefix,
                                            size_t * size);

int YBC_spectral_library_type(struct stardata_t * const stardata Maybe_unused,
                              char * const filepath);
struct YBC_FITSfile_info_t * YBC_analyse_file(struct stardata_t * const stardata Maybe_unused,
                                          char * const prefix,
                                          char * const dirpath,
                                          char * const filename);
void YBC_spectral_library_info(struct stardata_t * const stardata,
                               struct YBC_FITSfile_info_t * const info);

void YBC_spectral_library_info_ATLAS9(struct stardata_t * const stardata,
                                       struct YBC_FITSfile_info_t * const info);
void YBC_spectral_library_info_PHOENIX(struct stardata_t * const stardata,
                                       struct YBC_FITSfile_info_t * const info);
void YBC_spectral_library_info_COMARCS_MS(struct stardata_t * const stardata,
                                          struct YBC_FITSfile_info_t * const info);
void YBC_spectral_library_info_COMARCS_C(struct stardata_t * const stardata,
                                         struct YBC_FITSfile_info_t * const info);
void YBC_spectral_library_info_WM_BASIC(struct stardata_t * const stardata,
                                        struct YBC_FITSfile_info_t * const info);
void YBC_spectral_library_info_PoWR_WNL(struct stardata_t * const stardata,
                                       struct YBC_FITSfile_info_t * const info);
void YBC_spectral_library_info_PoWR_WNE(struct stardata_t * const stardata,
                                       struct YBC_FITSfile_info_t * const info);
void YBC_spectral_library_info_PoWR_WC(struct stardata_t * const stardata,
                                       struct YBC_FITSfile_info_t * const info);
void YBC_spectral_library_info_KOESTER(struct stardata_t * const stardata,
                                       struct YBC_FITSfile_info_t * const info);
void YBC_spectral_library_info_TUC47(struct stardata_t * const stardata,
                                     struct YBC_FITSfile_info_t * const info);
void YBC_spectral_library_info_TLUSTY(struct stardata_t * const stardata,
                                      struct YBC_FITSfile_info_t * const info);

void YBC_spectral_library_info_from_list_file(struct stardata_t * const stardata Maybe_unused,
                                              struct YBC_FITSfile_info_t * const info,
                                              size_t * const n_metadata);

void YBC_bin_column(struct data_table_t * const table,
                    const int ncol,
                    const double binwidth);
void YBC_filter_zero_column(struct data_table_t * const table,
                            const int ncol);
void YBC_clean_spectral_library_info(struct stardata_t * const stardata Maybe_unused,
                                     struct YBC_FITSfile_info_t * const info);


void YBC_combine_files(struct stardata_t * const stardata,
                       const int index,
                       struct YBC_FITSfile_info_t ** const infolist,
                       const size_t nfiles,
                       char ** wanted_columns,
                       const int wanted_ncolumns);


double YBC_library_type_float(const int table[2],
                              const double f,
                              const double log10mdot);

void YBC_free_persistent_data_memory(struct persistent_data_t * Restrict const persistent_data);
void YBC_free_info_struct(struct YBC_FITSfile_info_t ** info_p);
void YBC_free_info_structs(struct YBC_FITSfile_info_t *** infos_p,
                           const size_t nfiles);
void YBC_free_instrument_list(struct tmpstore_t * const tmpstore);
void YBC_set_instrument_list(struct stardata_t * const stardata);
void YBC_renew_instrument_list(struct stardata_t * const stardata);

#endif // YBC_PROTOTYPES_H
