/*
 * Calculate stellar colours according to Eldridge's 2012 table
 * for O stars
 */
#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef STELLAR_COLOURS

#include "stellar_colour_macros.h"


/* magnitudes should be an array : double magnitudes[8] */
void Nonnull_all_arguments eldridge2012_magnitudes(struct stardata_t * stardata,
                                                   struct star_t * star,
                                                   double * magnitudes)
{
    double param[2] = {
        Teff_from_star_struct(star),
        logg(star)
    };
    double colours[8];

    Interpolate(stardata->store->Eldridge2012_colours,
                param,
                colours,
                FALSE);

    /* set magnitudes */
    magnitudes[STELLAR_MAGNITUDE_U] = colours[ELDRIDGE_U];
    magnitudes[STELLAR_MAGNITUDE_B] = colours[ELDRIDGE_B];
    magnitudes[STELLAR_MAGNITUDE_V] = colours[ELDRIDGE_V];
    magnitudes[STELLAR_MAGNITUDE_R] = colours[ELDRIDGE_R];
    magnitudes[STELLAR_MAGNITUDE_I] = colours[ELDRIDGE_I];
    magnitudes[STELLAR_MAGNITUDE_J] = colours[ELDRIDGE_J];
    magnitudes[STELLAR_MAGNITUDE_H] = colours[ELDRIDGE_H];
    magnitudes[STELLAR_MAGNITUDE_K] = colours[ELDRIDGE_K];
        
    /* adjust for the star's luminosity (colours assume L=1) */
    {
        const double ll = magnitude_from_luminosity(star->luminosity);
        int i;
        for(i=0;i<=STELLAR_MAGNITUDE_K;i++)
        {
            magnitudes[i] += ll;
        }
    }
#ifdef CDEBUG
    printf("{%g %g %g %g %g %g %g %g}",
           magnitudes[0], magnitudes[1], magnitudes[2], magnitudes[3],
           magnitudes[4], magnitudes[5], magnitudes[6], magnitudes[7]);
#endif
}

#endif
