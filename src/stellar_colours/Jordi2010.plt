# 
# Gnuplot file to test stellar colour test data for
# comparison with Jordi et al 2010's Gaia colour conversions.
#
# Build binary_c with STELLAR_COLOURS and STELLAR_COLOURS_TESTS
# enabled.
# Run binary_c using tbse.
#
# This will output stuff to the screen, but also files in /tmp.
#
# Then, with this script you can plot figures to compare to Jordi et al.
#

set terminal pdfcairo enhanced
set output "/tmp/colour_tests.pdf"
print "Output file is /tmp/colour_tests.pdf"


# Fig 11 using the cubic fits vs V-I
set title "Compare to Fig. 11, top left panel"
set xlabel "V-I"
set ylabel "G-V"
plot "/tmp/Fig11a.dat" u 3:4 notitle

set title "Compare to Fig. 11, top right panel"
set ylabel "V-X"
plot "/tmp/Fig11a.dat" u 3:5 title "V-GBP",\
     "/tmp/Fig11a.dat" u 3:6 title "V-GRP"

set ylabel "G-X"
set title "Compare to Fig. 11, bottom left panel"
plot "/tmp/Fig11a.dat" u 3:7 title "G-GBP",\
     "/tmp/Fig11a.dat" u 3:8 title "G-GRP"

set ylabel "GBP-GRP"
set title "Compare to Fig. 11, bottom left panel"
plot "/tmp/Fig11a.dat" u 3:9 notitle



# Fig 11 using the cross-cubic fits vs V-I
set title "Compare to Fig. 11, top left panel"
set xlabel "V-I"
set ylabel "G-V"
plot "/tmp/Fig11b.dat" u 3:4 notitle

set title "Compare to Fig. 11, top right panel"
set ylabel "V-X"
plot "/tmp/Fig11b.dat" u 3:5 title "V-GBP",\
     "/tmp/Fig11b.dat" u 3:6 title "V-GRP"

set ylabel "G-X"
set title "Compare to Fig. 11, bottom left panel"
plot "/tmp/Fig11b.dat" u 3:7 title "G-GBP",\
     "/tmp/Fig11b.dat" u 3:8 title "G-GRP"

set ylabel "GBP-GRP"
set title "Compare to Fig. 11, bottom left panel"
plot "/tmp/Fig11b.dat" u 3:9 notitle

# Fig 13 using the cubic fits to g-i

set xlabel "g-i"

set ylabel "G-g"
set title "Compare to Fig 13 (top left panel)"
plot "/tmp/Fig13a.dat" u 3:4 notitle

set ylabel "g-X"
set title "Compare to Fig 13 (top right)"
plot "/tmp/Fig13a.dat" u 3:5 title "g-G_{BP}",\
      "/tmp/Fig13a.dat" u 3:6 title "g-G_{RP}"

set ylabel "G_{BP} - G_{RP}"
set title "Compare to Fig 13 (middle left panel)"
plot "/tmp/Fig13a.dat" u 3:7 notitle


# Fig 13 using the cross-cubic fits to g-i and g-r

set xlabel "g-i"

set ylabel "G-g"
set title "Compare to Fig 13 (top left panel)"
set xrange[-1:12]
set yrange[-12:1]
plot "/tmp/Fig13b.dat" u 3:5 notitle

set ylabel "g-X"
set yrange[-2:13]
set title "Compare to Fig 13 (top right)"
plot "/tmp/Fig13b.dat" u 3:6 title "g-G_{BP}",\
      "/tmp/Fig13b.dat" u 3:7 title "g-G_{RP}"

set ylabel "G_{BP} - G_{RP}"
set yrange[-2:10]
set title "Compare to Fig 13 (middle left panel)"
plot "/tmp/Fig13b.dat" u 3:8 notitle


set xlabel "g-r"
set ylabel "g-X"
set yrange[-8:2]
set xrange[-1.5:8]
set title "Compare to Fig 13 (bottom left)"
plot "/tmp/Fig13b.dat" u 4:9 title "g-G_{BP}",\
      "/tmp/Fig13b.dat" u 4:10 title "g-G_{RP}"
