#pragma once
#ifndef STELLAR_COLOUR_MACROS_H
#define STELLAR_COLOUR_MACROS_H

#include "eldridge_columns.h"
#include "carrasco2014_columns.h"
#include "stellar_colours.def"


#undef STELLAR_COLOUR_MACRO
#define STELLAR_COLOUR_MACRO(CODE,STRING) STELLAR_MAGNITUDE_##CODE,
enum { STELLAR_MAGNITUDES_LIST STELLAR_MAGNITUDE_NUMBER };
#undef STELLAR_COLOUR_MACRO
#define STELLAR_COLOUR_MACRO(CODE,STRING) STRING,
static const char *const stellar_magnitude_strings[] Maybe_unused = { STELLAR_MAGNITUDES_LIST "" };
#undef STELLAR_COLOUR_MACRO

/*
 * STELLAR_COLOUR_STRING_LENGTH is the maximum number
 * of characters in one of the above strings, +1
 */
#define STELLAR_COLOUR_STRING_LENGTH ((size_t)(1+5))

#define X(CODE) GAIA_CONVERSION_##CODE,
enum { GAIA_CONVERSION_METHODS_LIST };
#undef X
#define X(CODE) GAIA_CONVERSION_WD_##CODE,
enum { GAIA_CONVERSION_WD_METHODS_LIST };
#undef X


#define GAIA_CONVERSION_DEFAULT_METHOD (GAIA_CONVERSION_UBVRI_BIVARIATE_JORDI2010)


/*
 * Unphysical stellar magnitude
 */
#define STELLAR_MAGNITUDE_UNPHYSICAL (-1.0e10)

/*
 * And a macro to detect it
 */
#define Magnitude_is_unphysical(X) (Fequal(STELLAR_MAGNITUDE_UNPHYSICAL,(X)))
#define Magnitude_is_physical(X) (Magnitude_is_unphysical(X) ? FALSE : TRUE)

/*
 * Magnitude of a star with no luminosity (cannot be inf)
 */
#define DARK_STAR_MAGNITUDE (100.0)

/*
 * Macro to loop over stellar magnitudes
 */
#define Magnitude_loop(I) for((I)=0;(I)<STELLAR_MAGNITUDE_NUMBER;(I)++)


#endif// COLOUR_MACROS_H
