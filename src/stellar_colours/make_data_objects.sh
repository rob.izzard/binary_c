#!/bin/bash

# make data objects for stellar_colours with binary_c
echo
echo "make data objects for stellar_colours with binary_c"

source ../make_data_objects_setup.sh
$CC -fPIC -O0 ../../double2bin.c -o ./double2bin

HFILE=eldridge2015-basel.h
TMPFILE=eldridge2015-basel.dat
OBJFILE=eldridge2015-basel.o
echo "HFILE=$HFILE TMPFILE=$TMPFILE OBJFILE=$OBJFILE"

if [ $HFILE -nt $OBJFILE ]  || [ !-z $BINARY_C_REBUILD_DATA_OBJECTS] ; then

    cat $HFILE | tr , ' ' | sed s/\\\\// | grep -v define |grep -v \* |grep . | ./double2bin > $TMPFILE && \
    objcopy $OBJCOPY_OPTS $TMPFILE $OBJFILE && \
    ls -l $TMPFILE $OBJFILE double2bin && \
    rm $TMPFILE &
fi

echo "DATAOBJECT $OBJFILE"

HFILE=eldridge2015-ostar.h
TMPFILE=eldridge2015-ostar.dat
OBJFILE=eldridge2015-ostar.o
echo "HFILE=$HFILE TMPFILE=$TMPFILE OBJFILE=$OBJFILE"

if [ $HFILE -nt $OBJFILE ]  || [ !-z $BINARY_C_REBUILD_DATA_OBJECTS] ; then

    cat $HFILE | tr , ' ' | sed s/\\\\// | grep -v define |grep -v \* |grep . | ./double2bin > $TMPFILE && \
    objcopy $OBJCOPY_OPTS $TMPFILE $OBJFILE && \
    ls -l $TMPFILE $OBJFILE double2bin && \
    rm $TMPFILE &

fi

echo "DATAOBJECT $OBJFILE"

HFILE=Kurucz.h
TMPFILE=Kurucz.dat
OBJFILE=Kurucz.o
echo "HFILE=$HFILE TMPFILE=$TMPFILE OBJFILE=$OBJFILE"

if [ $HFILE -nt $OBJFILE ]  || [ !-z $BINARY_C_REBUILD_DATA_OBJECTS] ; then

    cat $HFILE | tr , ' ' | sed s/\\\\// | grep -v define |grep -v \* |grep . | ./double2bin > $TMPFILE && \
    objcopy $OBJCOPY_OPTS $TMPFILE $OBJFILE && \
    ls -l $TMPFILE $OBJFILE double2bin && \
    rm $TMPFILE &
fi

wait
rm ./double2bin
echo "DATAOBJECT $OBJFILE"
