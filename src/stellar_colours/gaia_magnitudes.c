#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef STELLAR_COLOURS

static double _cubic(const double x,
                     const double a,
                     const double b,
                     const double c,
                     const double d);

static double _quartic(const double x,
                       const double a,
                       const double b,
                       const double c,
                       const double d,
                       const double ee);

static double _crosscubic(const double x,
                          const double y,
                          const double a,
                          const double b,
                          const double c,
                          const double d,
                          const double e,
                          const double f,
                          const double g,
                          const double h);

void Nonnull_all_arguments gaia_magnitudes(struct stardata_t * const stardata,
                                           const struct star_t * const star,
                                           double * Restrict const magnitudes)
{
    /*
     * Given a populated magntiudes array,
     * convert to Gaia magntiudes based on
     * the conversion formulae of
     * Jordi et al. 2010 (DR1, A&A 523,A48)
     * Carrasco et al. (2014, A&A 565,A11)
     * Evans et al. 2018 (DR2)
     * Fusillo et al. 2019 (DR2, for WDs)
     * Riello et al. 2020 (DR3)
     *
     * We can do this from either UBVRI or ugriz,
     * but UBVRI fit better (Jordi et al 2010, sec.5.2
     * page 7).
     *
     * You can use single-magnitude fits (cubics), or from
     * two colours.
     *
     * If you want extinction, you should do it elsewhere.
     *
     * Note: this uses pre-computed colours. White dwarfs
     * should use the Carrasco et al. (2014) tables directly,
     * hence never call this function, although the fits
     * are given here for completeness.
     */

    if(WHITE_DWARF(star->stellar_type))
    {
        if(stardata->preferences->gaia_white_dwarf_colour_method == GAIA_CONVERSION_WD_CARRASCO2014_FITS)
        {

            /*
             * Carrasco et al. (2014) fits for white dwarfs
             * using V-Ic (UBVRI), Table 6.
             */
            const double vic = magnitudes[STELLAR_MAGNITUDE_V] - magnitudes[STELLAR_MAGNITUDE_I];
            if(star->white_dwarf_atmosphere_type == WHITE_DWARF_ATMOSPHERE_HYDROGEN)
            {
                /*
                 * Pure-H Teff>5000K: we use this for all
                 * hydrogen-rich white dwarfs
                 */
                magnitudes[STELLAR_MAGNITUDE_GAIA_G] =
                    magnitudes[STELLAR_MAGNITUDE_V] +
                    _cubic(vic, 0.0495, -0.0907, -0.6233, 0.4240);

                magnitudes[STELLAR_MAGNITUDE_GAIA_GBP] =
                    magnitudes[STELLAR_MAGNITUDE_V] -
                    _cubic(vic, -0.0601, -0.3186, 0.9421, -0.7939);

                magnitudes[STELLAR_MAGNITUDE_GAIA_GRP] =
                    magnitudes[STELLAR_MAGNITUDE_V] -
                    _cubic(vic, -0.0308, 0.8704, 0.1516, -0.1073);

                magnitudes[STELLAR_MAGNITUDE_GAIA_GRVS] =
                    magnitudes[STELLAR_MAGNITUDE_V] -
                    _cubic(vic, 0.0022, 1.1350, 0.0091, -0.0205);
            }
            else if(star->white_dwarf_atmosphere_type == WHITE_DWARF_ATMOSPHERE_HELIUM)
            {
                /*
                 * Pure-He for all Teff
                 */
                magnitudes[STELLAR_MAGNITUDE_GAIA_G] =
                    magnitudes[STELLAR_MAGNITUDE_V] +
                    _cubic(vic, -0.0085, -0.1051, -0.1541, 0.0046);

                magnitudes[STELLAR_MAGNITUDE_GAIA_GBP] =
                    magnitudes[STELLAR_MAGNITUDE_V] -
                    _cubic(vic, 0.0456, -0.3104, 0.0676, 0.0103);

                magnitudes[STELLAR_MAGNITUDE_GAIA_GRP] =
                    magnitudes[STELLAR_MAGNITUDE_V] -
                    _cubic(vic, -0.0082, 0.8854, 0.0089, 0.0021);

                magnitudes[STELLAR_MAGNITUDE_GAIA_GRVS] =
                    magnitudes[STELLAR_MAGNITUDE_V] -
                    _cubic(vic, 0.0009, 1.1255, -0.0043, -0.0011);
            }
            else
            {
                Exit_binary_c(BINARY_C_WRONG_ARGUMENT,
                              "Converting from magnitudes to Gaia colours failed because we do not know the white dwarf surface composition. Please see src/stellar_magnitudes/stellar_colour_macros.h and check your stellar evolution algorithm.\n");
            }
        }
        else if(stardata->preferences->gaia_white_dwarf_colour_method == GAIA_CONVERSION_WD_CARRASCO2014_TABLE)
        {
            /*
             * First two columns are Teff and logg
             */


        }
        else
        {
            Exit_binary_c(BINARY_C_WRONG_ARGUMENT,
                          "Converting from WD atmosphere to Gaia colours failed with unknown gaia_white_dwarf_colour_method %d. Please see src/stellar_magnitudes/stellar_colour_macros.h for available methods.\n",
                          stardata->preferences->gaia_white_dwarf_colour_method);

        }
    }
    else
    {

        if(stardata->preferences->gaia_colour_transform_method == GAIA_CONVERSION_UBVRI_UNIVARIATE_JORDI2010)
        {
            /*
             * Johnson-Cousins UBVRI conversion
             * assumes R=Rc and I=Ic.
             *
             * Fitting coefficients are from page 7
             * in Jordi et al 2010, Table 3, top panel
             */
            const double vic = magnitudes[STELLAR_MAGNITUDE_V] - magnitudes[STELLAR_MAGNITUDE_I];

            magnitudes[STELLAR_MAGNITUDE_GAIA_G] =
                magnitudes[STELLAR_MAGNITUDE_V] +
                _cubic(vic, -0.0257, -0.0924, -0.1623, 0.0090);

            magnitudes[STELLAR_MAGNITUDE_GAIA_GBP] =
                magnitudes[STELLAR_MAGNITUDE_V] -
                _cubic(vic, 0.0643, -0.3266, 0.0887, -0.0050);

            magnitudes[STELLAR_MAGNITUDE_GAIA_GRP] =
                magnitudes[STELLAR_MAGNITUDE_V] -
                _cubic(vic, -0.0017,0.8794,0.0273,-0.0008);

            magnitudes[STELLAR_MAGNITUDE_GAIA_GRVS] =
                magnitudes[STELLAR_MAGNITUDE_V] -
                _cubic(vic, 0.0119, 1.2092, -0.0188, -0.0005);
        }
        else if(stardata->preferences->gaia_colour_transform_method == GAIA_CONVERSION_UBVRI_UNIVARIATE_EVANS2018)
        {
            /*
             * Johnson-Cousins UBVRI conversion
             * assumes R=Rc and I=Ic.
             *
             * Fitting coefficients are from page 18
             * in Evans et al. 2018, table A.2, panel 4
             * EXCEPT FOR GVRS that is from Jordi et al 2010, Table 3, top panel as above
             */
            const double vic = magnitudes[STELLAR_MAGNITUDE_V] - magnitudes[STELLAR_MAGNITUDE_I];

            magnitudes[STELLAR_MAGNITUDE_GAIA_G] =
                magnitudes[STELLAR_MAGNITUDE_V] +
                _cubic(vic, -0.01746, 0.008092, -0.281, 0.03655);

            magnitudes[STELLAR_MAGNITUDE_GAIA_GBP] =
                magnitudes[STELLAR_MAGNITUDE_V] +
                _cubic(vic, -0.05204, 0.483, -0.2001, 0.02186);

            magnitudes[STELLAR_MAGNITUDE_GAIA_GRP] =
                magnitudes[STELLAR_MAGNITUDE_V] +
                _cubic(vic, 0.0002428,-0.8675,-0.02866,0.);

            /*
             * GRVS from Jordi 2010
             */
            magnitudes[STELLAR_MAGNITUDE_GAIA_GRVS] =
                magnitudes[STELLAR_MAGNITUDE_V] -
                _cubic(vic, 0.0119, 1.2092, -0.0188, -0.0005);
        }
        else if(stardata->preferences->gaia_colour_transform_method == GAIA_CONVERSION_UBVRI_RIELLO2020)
        {
            /*
             * Johnson-Cousins UBVRI conversion
             * assumes R=Rc and I=Ic.
             *
             * Fitting coefficients are from Table C.2
             * of Riello et al. (2020)
             * https://arxiv.org/pdf/2012.01916.pdf
             * EXCEPT FOR GVRS that is from Jordi et al 2010, Table 3, top panel as above
             *
             * These use the V-Ic fits which give G, GBP and GRP.
             */
            const double vic = magnitudes[STELLAR_MAGNITUDE_V] - magnitudes[STELLAR_MAGNITUDE_I];

            magnitudes[STELLAR_MAGNITUDE_GAIA_G] =
                magnitudes[STELLAR_MAGNITUDE_V] +
                _quartic(vic, -0.01597, -0.02809, -0.2483, 0.03656,  -0.002939);

            magnitudes[STELLAR_MAGNITUDE_GAIA_GBP] =
                magnitudes[STELLAR_MAGNITUDE_V] +
                _cubic(vic, -0.0143, 0.3564, -0.1332, 0.01212);

            magnitudes[STELLAR_MAGNITUDE_GAIA_GRP] =
                magnitudes[STELLAR_MAGNITUDE_V] +
                _cubic(vic, 0.01868, -0.9028, -0.005321, -0.004186);

            /*
             * GRVS from Jordi 2010
             */
            magnitudes[STELLAR_MAGNITUDE_GAIA_GRVS] =
                magnitudes[STELLAR_MAGNITUDE_V] -
                _cubic(vic, 0.0119, 1.2092, -0.0188, -0.0005);
        }
        else if(stardata->preferences->gaia_colour_transform_method == GAIA_CONVERSION_UBVRI_BIVARIATE_JORDI2010)
        {
            /*
             * Johnson-Cousins UBVRI conversion
             * assumes R=Rc and I=Ic.
             *
             * Fitting coefficients are from page 11
             * in Jordi et al 2010, Table 7, top panel
             *
             * These are the two-magnitude cross-cubic fits
             * which are supposedly the best.
             */
            const double vic = magnitudes[STELLAR_MAGNITUDE_V] - magnitudes[STELLAR_MAGNITUDE_I];
            const double bv =  magnitudes[STELLAR_MAGNITUDE_B] - magnitudes[STELLAR_MAGNITUDE_V];

            magnitudes[STELLAR_MAGNITUDE_GAIA_G] =
                magnitudes[STELLAR_MAGNITUDE_V] +
                _crosscubic(vic, bv, -0.0099,-0.2116,-0.1387,0.0060,0.1485,-0.0895,0.0094,0.0327);

            magnitudes[STELLAR_MAGNITUDE_GAIA_GBP] =
                magnitudes[STELLAR_MAGNITUDE_V] -
                _crosscubic(vic, bv, 0.0188,0.0504,-0.0181,0.0021,-0.4728,0.1586,-0.0176,0.0255);

            magnitudes[STELLAR_MAGNITUDE_GAIA_GRP] =
                magnitudes[STELLAR_MAGNITUDE_V] -
                _crosscubic(vic, bv, 0.0024,0.8328,0.0572,-0.0020,0.0722,-0.0126,0.0037,-0.0380);

            magnitudes[STELLAR_MAGNITUDE_GAIA_GRVS] =
                magnitudes[STELLAR_MAGNITUDE_V] -
                _crosscubic(vic, bv, -0.0188,1.4535,-0.0873,0.0038,-0.2977,0.0701,-0.0120,0.0383);
        }
        else if(stardata->preferences->gaia_colour_transform_method == GAIA_CONVERSION_ugriz_UNIVARIATE_JORDI2010)
        {
            /*
             * SDSS ugriz conversion
             *
             * Fitting coefficients are from Jordi et al 2010,
             * page 9, Table 5, using g-i
             */
            const double gi = magnitudes[STELLAR_MAGNITUDE_g] - magnitudes[STELLAR_MAGNITUDE_i];

            magnitudes[STELLAR_MAGNITUDE_GAIA_G] =
                magnitudes[STELLAR_MAGNITUDE_g] +          //this uses G and not g -> weird for consistency
                _cubic(gi,-0.0940, -0.5310, -0.0974, 0.0052);

            magnitudes[STELLAR_MAGNITUDE_GAIA_GBP] =
                magnitudes[STELLAR_MAGNITUDE_GAIA_G] -
                _cubic(gi,-0.1235, -0.3289, -0.0582, 0.0033);

            magnitudes[STELLAR_MAGNITUDE_GAIA_GRP] =
                magnitudes[STELLAR_MAGNITUDE_GAIA_G] -
                _cubic(gi,0.2566, 0.5086, -0.0678, 0.0032);

            magnitudes[STELLAR_MAGNITUDE_GAIA_GRVS] =
                magnitudes[STELLAR_MAGNITUDE_GAIA_G] -
                _cubic(gi,0.3931, 0.7250, -0.0927, 0.0032);
        }
        else if(stardata->preferences->gaia_colour_transform_method == GAIA_CONVERSION_ugriz_UNIVARIATE_EVANS2018)
        {
            /*
             * SDSS ugriz conversion
             *
             * Fitting coefficients are from Evans et al. 2018,
             * page 18, table A.2, third panel, using r-i
             * EXCEPT FOR GVRS that is from Jordi et al 2010, Table 5, top panel as above
             */
            const double ri = magnitudes[STELLAR_MAGNITUDE_r] - magnitudes[STELLAR_MAGNITUDE_i];

            magnitudes[STELLAR_MAGNITUDE_GAIA_G] =
                magnitudes[STELLAR_MAGNITUDE_r] +
                _cubic(ri,0.0014891, 0.36291, -0.81282, 0.14711);

            magnitudes[STELLAR_MAGNITUDE_GAIA_GBP] =
                magnitudes[STELLAR_MAGNITUDE_r] +
                _cubic(ri,0.1463, 1.7244, -1.1912, 0.22004);

            magnitudes[STELLAR_MAGNITUDE_GAIA_GRP] =
                magnitudes[STELLAR_MAGNITUDE_r] +
                _cubic(ri,-0.29869, -1.1303, 0., 0.);

            /*
             * GRVS from Jordi et al. 2010, using r-i
             */
            magnitudes[STELLAR_MAGNITUDE_GAIA_GRVS] =
                magnitudes[STELLAR_MAGNITUDE_GAIA_G] -
                _cubic(ri,0.4469, 1.9259, -0.6724, 0.0686);
        }
        else if(stardata->preferences->gaia_colour_transform_method == GAIA_CONVERSION_ugriz_BIVARIATE_JORDI2010)
        {
            /*
             * SDSS ugriz conversion
             *
             * Fitting coefficients are from page 11
             * in Jordi et al 2010, Table 7, top panel
             *
             * These are the two-magnitude cross-cubic fits
             * which are supposedly the best.
             */
            const double gi = magnitudes[STELLAR_MAGNITUDE_g] - magnitudes[STELLAR_MAGNITUDE_i];
            const double gr =  magnitudes[STELLAR_MAGNITUDE_g] - magnitudes[STELLAR_MAGNITUDE_r];

            magnitudes[STELLAR_MAGNITUDE_GAIA_G] =
                magnitudes[STELLAR_MAGNITUDE_g] +
                _crosscubic(gi, gr, -0.1005,-0.5358,-0.1207,0.0082,-0.0272,0.1270,-0.0205,-0.0176);

            magnitudes[STELLAR_MAGNITUDE_GAIA_GBP] =
                magnitudes[STELLAR_MAGNITUDE_g] -
                _crosscubic(gi, gr, -0.0428,0.0158,0.0122,0.0005,0.2382,0.1855,-0.0096,-0.0493);

            magnitudes[STELLAR_MAGNITUDE_GAIA_GRP] =
                magnitudes[STELLAR_MAGNITUDE_g] -
                _crosscubic(gi, gr, 0.3731,1.3335,-0.0242,-0.0047,-0.4086,-0.2729,0.0230,0.2166);

            magnitudes[STELLAR_MAGNITUDE_GAIA_GRVS] =
                magnitudes[STELLAR_MAGNITUDE_g] -
                _crosscubic(gi, gr, 0.5345,2.3063,-0.2919,0.0049,-1.6562,-0.1306,-0.0257,0.5419);
        }
        else if(stardata->preferences->gaia_colour_transform_method == GAIA_CONVERSION_ugriz_RIELLO2020)
        {
            /*
             * SDSS12 ugriz conversion
             *
             * Fitting coefficients are from Table C.2
             * of Riello et al. (2020)
             * https://arxiv.org/pdf/2012.01916.pdf
             * EXCEPT FOR GVRS that is from Jordi et al 2010, Table 3, top panel as above
             *
             * These use the g-i fits.
             */
            const double gi = magnitudes[STELLAR_MAGNITUDE_g] - magnitudes[STELLAR_MAGNITUDE_i];

            magnitudes[STELLAR_MAGNITUDE_GAIA_G] =
                magnitudes[STELLAR_MAGNITUDE_g] +
                _cubic(gi,-0.1064, -0.4964, -0.09339, 0.004444);

            magnitudes[STELLAR_MAGNITUDE_GAIA_GBP] =
                magnitudes[STELLAR_MAGNITUDE_g] +
                _cubic(gi, 0.06213, -0.2059, -0.06478, 0.007264);

            magnitudes[STELLAR_MAGNITUDE_GAIA_GRP] =
                magnitudes[STELLAR_MAGNITUDE_g] +
                _cubic(gi, -0.3306, -0.9847, -0.02874, 0.002112);

            /*
             * GVRS from Jordi et al. 2010, using g-i
             */
            magnitudes[STELLAR_MAGNITUDE_GAIA_GRVS] =
                magnitudes[STELLAR_MAGNITUDE_GAIA_G] -
                _cubic(gi,0.3931, 0.7250, -0.0927, 0.0032);
        }
        else
        {
            Exit_binary_c(BINARY_C_WRONG_ARGUMENT,"Converting from magnitudes to Gaia colours failed with unknown gaia_colour_transform_method %d. Please see src/stellar_magnitudes/stellar_colour_macros.h for available methods.\n",
                          stardata->preferences->gaia_colour_transform_method);
        }
    }
}

static double _cubic(const double x,
                     const double a,
                     const double b,
                     const double c,
                     const double d)
{
    return a+b*x+c*Pow2(x)+d*Pow3(x);
}

static double _quartic(const double x,
                       const double a,
                       const double b,
                       const double c,
                       const double d,
                       const double ee)
{
    return a+b*x+c*Pow2(x)+d*Pow3(x)+ee*Pow4(x);
}

static double _crosscubic(const double x,
                          const double y,
                          const double a,
                          const double b,
                          const double c,
                          const double d,
                          const double e,
                          const double f,
                          const double g,
                          const double h)
{
    return a+b*x+c*Pow2(x)+d*Pow3(x) +
        e*y+f*Pow2(y)+g*Pow3(y)+
        h*x*y;
}

#endif // STELLAR_COLOURS
