#pragma once
#ifndef YBC_H
#define YBC_H

#include "../binary_c.h"
No_empty_translation_unit_warning;

/*
 * Debugging is through the YBCdebug macro :
 * define it to do nothing if you don't want debugging statements
 */
#define YBCdebug(...)                           \
    if(YBC_debug == TRUE)                       \
    {                                           \
        printf("YBC %s %d: ",                   \
               __func__,                        \
               __LINE__);                       \
        printf(__VA_ARGS__);                    \
        fflush(stdout);                         \
    }



/*
 * Load tables with these X macros. Arguments are:
 * 1) binary_c identifier
 * 2) Directory path (will be searched in stardata->preferences->YBC_path)
 * 3) Array of strings which are the columns to be stored, surrounded by
 *    parentheses.
 * 4) Array of binary_c magnitude identifiers to be set corresponding to
 *    the above strings. These have STELLAR_MAGNITUDE_YBC_ prepended
 *    to them, and should match the definitions in stellar_colours.def.
 *
 * Note that NUMBER is there to count the number
 * of YBC_ types only, and the filename path and arrays are dummies.
 *
 * The arrays should be surrounded by _p(...) macros to protect them.
 */

#define _mag_strings(...) __VA_ARGS__

#define YBC_INSTRUMENTS_LIST                    \
    /* Gaia */                                  \
    X(  GAIA,                                   \
        "YBC/gaiaEDR3",                         \
        _mag_strings(                           \
            "G",                                \
            "G_BP",                             \
            "G_RP"                              \
            ),                                  \
        _mag_macros(                            \
            GAIA_G,                             \
            GAIA_GBP,                           \
            GAIA_GRP                            \
            )                                   \
        )                                       \
    /* Sloan digital sky survey (SDSS) */       \
    X( SLOAN,                                   \
       "YBC/sloan",                             \
       _mag_strings(                            \
           "u",                                 \
           "g",                                 \
           "r",                                 \
           "i",                                 \
           "z"                                  \
           ),                                   \
       _mag_macros(                             \
           SLOAN_u,                             \
           SLOAN_g,                             \
           SLOAN_r,                             \
           SLOAN_i,                             \
           SLOAN_z                              \
           )                                    \
        )                                       \
    /* JWST */                                  \
    X( JWST,                                    \
       "YBC/jwst_nircam_widemedium",            \
       _mag_strings(                            \
           "F070W",                             \
           "F090W",                             \
           "F115W",                             \
           "F150W",                             \
           "F200W",                             \
           "F277W",                             \
           "F356W",                             \
           "F444W",                             \
           "F150W2",                            \
           "F322W2",                            \
           "F140M",                             \
           "F162M",                             \
           "F182M",                             \
           "F210M",                             \
           "F250M",                             \
           "F300M",                             \
           "F335M",                             \
           "F360M",                             \
           "F410M",                             \
           "F430M",                             \
           "F460M",                             \
           "F480M",                             \
           "F150WS60"                           \
           ),                                   \
       _mag_macros(                             \
           JWST_F070W,                          \
           JWST_F090W,                          \
           JWST_F115W,                          \
           JWST_F150W,                          \
           JWST_F200W,                          \
           JWST_F277W,                          \
           JWST_F356W,                          \
           JWST_F444W,                          \
           JWST_F150W2,                         \
           JWST_F322W2,                         \
           JWST_F140M,                          \
           JWST_F162M,                          \
           JWST_F182M,                          \
           JWST_F210M,                          \
           JWST_F250M,                          \
           JWST_F300M,                          \
           JWST_F335M,                          \
           JWST_F360M,                          \
           JWST_F410M,                          \
           JWST_F430M,                          \
           JWST_F460M,                          \
           JWST_F480M,                          \
           JWST_F150WS60                        \
           )                                    \
        )


/*
 * YBC data directories are prefixed by "regrid"
 */
#define YBC_PREFIX "regrid"

/*
 * (Maximum) Length of column name strings (chars)
 */
#define COLUMN_NAME_STRING_LENGTH 100

/*
 * List file max line length (chars)
 */
#define YBC_LIST_FILE_MAX_LINE_LENGTH 256

/*
 * Table types : normal, rotating or limb darkening
 * (only NORMAL are currently supported)
 */
enum {
    YBC_NORMAL,
    YBC_ROTATING,
    YBC_LIMB_DARKENING,
};

/*
 * List of keys to be set in the info struct
 */
#define FITS_KEY_LIST                        \
    X( Mbol_sun, float,  TFLOAT, "MBOL_SUN") \
    X(     Lsun, float,  TFLOAT,  "LUM_SUN") \
    X( Nfilters,   int,    TINT, "NFILTERS") \
    X(     Ntot,   int,    TINT,  "TFIELDS") \
    X(     NAvs,   int,    TINT,     "NAvs") \
    X(  Avs_str, char*, TSTRING,      "Avs") \
    X(    naxis,   int,    TINT,    "NAXIS") \
    X(  version, char*, TSTRING,  "VERSION")

/*
 * List of strings to be set in the info struct
 */
#define FITS_STRING_LIST                        \
    X( FILTERlosNAMES, Nc+1,  Ntot-Nc, "TTYPE") \
    X(    FILTERNAMES, Nc+1, nfilters, "TTYPE")

/*
 * Make binary_c-YBC table identifiers
 */
#define X(ID, DIR, COLUMNS_ARRAY, MAGNTIUDE_ARRAY)  \
    YBC_INSTRUMENT_##ID ,
enum { YBC_INSTRUMENTS_LIST YBC_INSTRUMENT_NUMBER };
#undef X

/************************************************************/

/*
 * YBC's Spectral libraries
 *
 * args:
 * 1) ID for binary_c
 * 2) list of strings to match this ID
 * 3) metadata items
 */
#define YBC_LIBRARIES                                                              \
    X(     ATLAS9,           _mag_strings(  "odfnew"))                             \
    X(    PHOENIX,           _mag_strings(   "Settl"))                             \
    X( COMARCS_MS, _mag_strings( "COMARCS", "Mstars"))                             \
    X(  COMARCS_C, _mag_strings( "COMARCS", "Cstars"), metallicity, M_H, C_O, Cex) \
    X(   WM_BASIC,           _mag_strings(      "WM"))                             \
    X(   PoWR_WNL, _mag_strings(     "ori",    "wnl"))                             \
    X(   PoWR_WNE, _mag_strings(     "ori",    "wne"))                             \
    X(    PoWR_WC, _mag_strings(     "ori", "wcgrid"))                             \
    X(    KOESTER,           _mag_strings( "Koester"))                             \
    X(     TLUSTY,           _mag_strings(  "Tlusty"),          XH, XHe)           \
    X(      TUC47,           _mag_strings(   "47Tuc"))

#undef X
#define X(TYPE,STRINGS,...)                     \
    YBC_LIBRARY_##TYPE,
enum { YBC_LIBRARIES };

/*
 * Combined spectral libraries
 * 1) type
 * ...) interpolation variables
 */
#define YBC_COMBINED_LIBRARIES                                       \
    X(       ATLAS9,         M_H)                                    \
    X(      PHOENIX,         M_H)                                    \
    X(   COMARCS_MS,         M_H)                                    \
    X(    COMARCS_C, metallicity,       C_O)                         \
    X(     WM_BASIC, metallicity, log10mdot)                         \
    X( PoWR_WNL_WNE, metallicity,        XH)                         /* combined WNL + WNE */\
    X(  PoWR_WNE_WC, metallicity,       XHe)                         /* combined WNE + WC */\
    X(      KOESTER)                                                 \
    X(       TLUSTY,          XH)

#undef X
#define X(TYPE,...)                             \
    YBC_COMBINED_LIBRARY_##TYPE,
enum { YBC_COMBINED_LIBRARIES YBC_COMBINED_LIBRARY_NUMBER };

/*
 * Resolutions of the remapped tables
 * Where 0, these are not mapped, but each list
 * below must be of length YBC_COMBINED_MAXNPARAM
 * (which is 4 by default), plus one for the initial
 * label.
 *
 * You can increase _RES to globally increase the
 * resolution of the mapped tables, but remember
 * the time increase is cubed in some cases.
 *
 * The default _RES is 5 which is typical of the
 * number of data points in each original datasets
 * axis.
 */
#define _RES 10
#define YBC_COMBINED_LIBRARIES_MAPS             \
    X(       ATLAS9, 0,      0,      0,      0) \
    X(      PHOENIX, 0,      0, 2*_RES,      0) \
    X(   COMARCS_MS, 0,      0, 2*_RES,      0) \
    X(    COMARCS_C, 0, 2*_RES, 4*_RES, 4*_RES) \
    X(     WM_BASIC, 0,      0, 4*_RES, 2*_RES) \
    X( PoWR_WNL_WNE, 0,   _RES, 4*_RES, 2*_RES) \
    X(  PoWR_WNE_WC, 0,   _RES, 4*_RES, 2*_RES) \
    X(      KOESTER, 0,      0,      0,      0) \
    X(       TLUSTY, 0,      0,      0,      0)

/*
 * Max number of parameters in a combined table
 */
#define YBC_COMBINED_MAXNPARAM 4

/* function prototypes */
#undef X
#define X(TYPE,STRINGS,...)                         \
    void YBC_spectral_library_info_##TYPE(          \
        struct stardata_t * const stardata,         \
        struct YBC_FITSfile_info_t * const info);
#undef X

/*
 * Variables used to look up
 * the data in the YBC FITS files
 */
#define YBC_LOOKUP_VARS         \
    X(             metallicity) \
    X(                     M_H) \
    X(                alpha_Fe) \
    X(                     C_O) \
    X(                     Cex) \
    X(                    mdot) \
    X(               log10mdot) \
    X( microturbulent_velocity) \
    X(                      XH) \
    X(                     XHe) \
    X(                      XC) \
    X(                      XN) \
    X(                      XO) \
    X(                     XNe)

/*
 * Struct to hold information about a YBC FITS file
 */
struct YBC_FITSfile_info_t {
    /* automatically set variables in the info struct */
#define X(VAR, CTYPE, FITSTYPE, STRING)         \
    CTYPE VAR;
    FITS_KEY_LIST
#undef X
#define X(VAR, I, J, FITSTYPE)                  \
    char ** VAR;
    FITS_STRING_LIST

    /* filename and data set */
    char * path;
    char * dataset_extension;
    char * filepath;
    char * dataset;
    char * filename;
    char * dirpath;
    char * dirpath_with_prefix;
    char * prefix;

    /* data and number of axes */
    struct data_table_t * table;
    long int naxes[2];

#undef X
#define X(VAR) double VAR;
    YBC_LOOKUP_VARS
#undef X
    int type;
};

#include "ybc_prototypes.h"

/*
 * To reproduce Chen's 2019 plot (Fig. 1)
 * we need to convert libraries to a floating
 * point number. These are the basic numbers to
 * do this (but note that WM_BASIC also
 * depends on the mass-loss rate).
 */
#define YBC_LIBRARY_TYPE_FLOATS \
    X(    COMARCS_C, -2)        \
    X(   COMARCS_MS, -1)        \
    X(      PHOENIX,  0)        \
    X(       ATLAS9,  1)        \
    X(     WM_BASIC,  2)        \
    X(     WM_BASIC,  3)        \
    X(     WM_BASIC,  4)        \
    X( PoWR_WNL_WNE,  5)        \
    X(  PoWR_WNE_WC,  6)        \
    X(      KOESTER, 10)

/*
 * Null data filters on FITS file load
 */
#define YBC_FILTER_NULL_DATA TRUE
#define YBC_DO_NOT_FILTER_NULL_DATA FALSE

#endif // YBC_H
