#include "../binary_c.h"
No_empty_translation_unit_warning;


binary_c_API_function
void binary_c_breakpoint(const void * p Maybe_unused)
{
    /*
     * dummy function to act as a breakpoint for gdb
     *
     * For this to work, set this line:
     *
     * break src/debug/breakpoint.c:14
     *
     * in your .gdbinit file
     */
    const struct stardata_t * stardata Maybe_unused =
        (const struct stardata_t *) p;
    //printf("breakpoint\n");
}
