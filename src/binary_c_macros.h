#pragma once
#ifndef BINARY_MACROS_H
#define BINARY_MACROS_H

/*
 * The binary_c stellar population nucleosynthesis framework.
 *
 * Copyright Robert Izzard 2000-2025
 *
 * Contact rob.izzard@gmail.com
 *
 * Code repository:
 * https://gitlab.com/binary_c
 *
 * Documentation is in doc/
 * Code is in src/
 *
 * To build, run
 *
 * ---
 *
 * meson setup builddir
 * cd builddir
 * ninja binary_c_install
 *
 * ---
 *
 * Please see the files README, LICENCE and CHANGES,
 * and the doc/ directory for documentation which is mirrored
 * at https://binary_c.gitlab.io
 *
 **********************
 *
 * Purpose:
 *
 * This file loads most of the macros used in binary_c, and
 * sets some constant (non-function) macros that have no other
 * obvious home.
 *
 * You should not include this file directly, include binary_c.h
 * instead.
 */

#include "binary_c_code_options.h"
#include "binary_c_debug.h"
#include "binary_c_types.h"
#include "binary_c_maths.h"
#include "binary_c_string_macros.h"
#include "binary_c_parameters.h"
#include "binary_c_function_macros.h"
#include "binary_c_sources.h"
#include "binary_c_prescriptions.h"
#include "data/data_table_function_macros.h"
#include "timestep/timestep_macros.h"
#include "setup/metallicity_parameters.h"
#include "binary_c_error_codes.h"
#include "maths/maths_error_codes.h"
#include "binary_c_version.h"
#include "wind/wind_macros.h"
#include "massive/massive_macros.h"
#include "binary_c_derivatives.h"
#include "stellar_colours/stellar_colour_macros.h"
#include "events/events_macros.h"
#include "supernovae/sn.h"
#include "file/file_macros.h"
#ifdef DISCS
#include "disc/disc_macros.h"
#include "disc/disc_parameters.h"
#include "disc/disc_function_macros.h"
#endif
#ifdef NUCSYN
#include "nucsyn/nucsyn_macros.h"
#endif
#include "evolution/evolution_macros.h"
#include "evolution/rejection_macros.h"
#include "MINT/MINT_data_tables.h"
#ifdef MINT
#include "MINT/MINT_macros.h"
#include "MINT/MINT_function_macros.h"
#endif//MINT
#include "ensemble/ensemble_macros.h"
#include "logging/logging_macros.h"
#include "stellar_structure/stellar_structure_macros.h"
#include "supernovae/sn.h"
#include "binary_c_physical_constants.h"
#include "units/units.h"
#include "binary_c_stellar_types.h"
#ifdef ORBITING_OBJECTS
#include "orbiting_objects/orbiting_objects_macros.h"
#endif // ORBITING_OBJECTS
#include "transients/transients_macros.h"
#include "binary_star_functions/merger_algorithms.h"
#include "binary_star_functions/accretion_regimes.h"
#include "RLOF/RLOF_stability_macros.h"
#include "logging/logging_macros.h"
#include "circumstellar_disc/circumstellar_disc.h"
#ifdef EVENT_BASED_LOGGING
#include "event_based_logging/event_based_logging.h"
#endif // EVENT_BASED_LOGGING

/* detailed_logging : probably not used much (may be removed!) */
#define DETAILED_OUTPUT_FILENAME "/tmp/c_binary_details.dat"

/*
 * number of bytes in a MByte, GByte, etc.
 * note: cast to a size_t to prevent integer overruns
 */
#define __KILOBYTE_MULTIPLIER ((size_t)1000)
#define KILOBYTE (__KILOBYTE_MULTIPLIER)
#define MEGABYTE (KILOBYTE*__KILOBYTE_MULTIPLIER)
#define GIGABYTE (MEGABYTE*__KILOBYTE_MULTIPLIER)
#define TERABYTE (GIGABYTE*__KILOBYTE_MULTIPLIER)
#define PETABYTE (TERABYTE*__KILOBYTE_MULTIPLIER)
#define EXABYTE (PETABYTE*__KILOBYTE_MULTIPLIER)

/* for pedants */
#define __KIBIBYTE_MULTIPLIER ((size_t)1024)
#define KIBIBYTE (__KIBIBYTE_MULTIPLIER)
#define MEBIBYTE (KIBIBYTE*__KIBIBYTE_MULTIPLIER)
#define GIBIBYTE (MEBIBYTE*__KIBIBYTE_MULTIPLIER)
#define TEBIBYTE (GIBIBYTE*__KIBIBYTE_MULTIPLIER)
#define PEBIBYTE (TEBIBYTE*__KIBIBYTE_MULTIPLIER)
#define EXIBYTE (PEBIBYTE*__KIBIBYTE_MULTIPLIER)

/*
 * Default log filename : can be overridden with command
 * line option --log_filename.
 */
#ifdef CGI_EXTENSIONS
#define DEFAULT_LOG_OUTPUT_FILENAME "/tmp/c_log-www.dat"
#else//CGI_EXTENSIONS
#define DEFAULT_LOG_OUTPUT_FILENAME "/tmp/c_log.dat"
#endif //CGI_EXTENSIONS

#ifdef MEMMAP
/* Use mmap() for the log rather than a file */
#define BIGLOG_FILENAME "/tmp/c_biglog.dat"
/* Local biglog size is in local_binary_macros.h */
#define BIGLOG_SIZE ((size_t)(LOCAL_BIGLOG_SIZE*MEGABYTE))
#define BIGLOG_LINES ((size_t)(BIGLOG_SIZE/MEMMAP_STRING_LENGTH))

/* Note that MEMMAP_STRING_LENGTH must be <= STRING_LENGTH */
#define MEMMAP_STRING_LENGTH ((size_t)82)
#endif /* MEMMAP */

#define HRDIAG_FILENAME "/tmp/c_binary_hrdiag.dat"

/*
 * Min and max separations to be considered a binary star,
 * in R_sun
 */
#define MINIMUM_SEPARATION_TO_BE_CALLED_BINARY (1e-9)
#define MAXIMUM_SEPARATION_TO_BE_CALLED_BINARY (1e25)

/*
 * Observational limits for stellar masses
 */
#define MIN_OBSERVATIONAL_STELLAR_MASS (0.01)
#define MAX_OBSERVATIONAL_BINARY_ORBITAL_PERIOD (1e7*YEAR_LENGTH_IN_DAYS)

#define OBSERVABLE_RADIAL_VELOCITY_MIN_DEFAULT 1.0

#define TINY_ECCENTRICITY 1e-10

/*
 * Conversion factor for angular momentum : code units -> cgs
 * J (code) * ANGULAR_MOMENTUM_CGS = J (cgs)
 */
#define ANGULAR_MOMENTUM_CGS (M_SUN*R_SUN*R_SUN/YEAR_LENGTH_IN_SECONDS)
#define SPECIFIC_ANGULAR_MOMENTUM_CGS (R_SUN*R_SUN/YEAR_LENGTH_IN_SECONDS)

#define K2_GYRATION 0.1
#define CORE_MOMENT_OF_INERTIA_FACTOR 0.21
#define MR23YR 0.4311
#define MINIMUM_STELLAR_ANGMOM 1e-50
#define MAXIMUM_STELLAR_ANGMOM 1e100
#define MINIMUM_ORBITAL_ANGMOM 0.0
#define MAXIMUM_ORBITAL_ANGMOM 1e100
#define UNDEFINED_ORBITAL_ANGULAR_FREQUENCY 1e50

/* minimum metallicity used when log10(Z) is required */
#define MINIMUM_METALLICITY 1e-20


/*
 * Sizes of commonly-used arrays
 */
#define GIANT_BRANCH_PARAMETERS_SIZE 116
#define MAIN_SEQUENCE_PARAMETERS_SIZE 144
#define NUMBER_OF_METALLICITY_PARAMETERS 15
#define COLLISION_MATRIX_SIZE 15

/*
 * constants used in set_metallicity_parameters and/or zfuncs
 */
#define ZPAR_M2 (2.0)
#define ZPAR_M2_3 (8.0)
#define ZPAR_M2_3_5 (11.313708499)
#define ZPAR_M3 (16.0)
#define LALPH_MCUT (2.0)
#define RGAMM_M1 (1.0)


#ifdef DEBUG
/** indentation global variable, makes debugging information **/
/** easier to see **/
#define INDENTATION_CHARACTER "->"
#endif /* DEBUG */


/*
 * Max split depth is the maximum depth of splitting that can occur in
 * distributed kicking : see binary_star_functions/distributed_kick.c and
 * adaptive_grid/get_signal_from_filelog.c
 */
#define MAX_SPLIT_DEPTH 10

/*
 * TMP_LOCATION is the location of your /tmp direction - it gets prepended so
 * no trailing slash please - a value of "" leaves /tmp as your temporary
 * directory (the usual case)
 */
#define TMP_LOCATION ""

/* commonly used metallicity parameters */
enum {
    ZPAR_METALLICITY = 0,
    ZPAR_MASS_MS_HOOK = 1,
    ZPAR_MASS_HE_FLASH = 2,
    ZPAR_MASS_FGB = 3
};
#define Sqrt_eccfac sqrt(Max(1.0-stardata->common.orbit.eccentricity*stardata->common.orbit.eccentricity,0.0))

enum{INTERPOLATE_DO_NOT_USE_CACHE,
     INTERPOLATE_USE_CACHE};

/* Which function to check? */
#ifndef SYSCHECK
#define SYSCHECK /* */
#endif //!SYSCHECK

#define REMNANT_LUMINOSITY 1e-10
#define REMNANT_RADIUS 1e-10

/* a very large dM/dt (Msun/y) */
#define VERY_LARGE_MASS_TRANSFER_RATE (1e50)

/* a very small dM/dt (Msun/y) */
#define VERY_SMALL_MASS_TRANSFER_RATE (1e-50)

/*
 * minimum envelope mass for RLOF
 * (only when using BSE style RLOF interpolation : binary_c
 *  defaults to a more stable algorithm)
 */
#define MINIMUM_ENVELOPE_MASS_FOR_RLOF 0.05

/*
 * If the separation drops below this during BSE-style interpolation,
 * simply allow the system to merge.
 */
#define MINIMUM_RLOF_INTERPOLATION_SEPARATION 1e-6

#define GALACTIC_MODEL_NELEMANS2004 0



/*
 * Stack options
 */
enum { STACK_WARNING, STACK_SET, STACK_DO_NOTHING };

/*
 * Number of Lagrange points
 */
#define NUMBER_OF_LAGRANGE_POINTS 5

/*
 * Turn monochecks on or off.
 */

#define BISECT_RETURN_FAIL -1.0

/* muiltiplier to make the Roche radius very large */
#define HUGE_ROCHE_MULTIPLIER (1e10)

/* a large separation to mimic single stars */
#define VERY_LARGE_SEPARATION 1e50

/* an impossibly large stellar mass */
#define IMPOSSIBLY_LARGE_MASS 1e50

/* Equation of state algorithms */
#define EQUATION_OF_STATE_PACZYNSKI 0

/*
 * Integer to describe the stardata dump file format
 */
#define STARDATA_DUMP_FORMAT 1

/*
 * A real millisecond
 */
#ifdef CPUFREQ
#define millisecond (1e3*((float)CPUFREQ))
#endif

/*
 * An SVN version number to signal that SVN is undefined
 */
#define SVN_REVISION_UNDEFINED -1.0

/*
 * Solar symbol
 *
 * Use either:
 * U+2609 ☉
 * or
 * U+2299 ⊙
 */
#define SOLAR_SYMBOL "☉"

/*
 * Convenience macros
 */
#define L_SOLAR "L☉"
#define M_SOLAR "M☉"
#define R_SOLAR "R☉"
#define J_CGS "g cm^2 s^-1"

/*
 * Default gamma for Rappaport type systemsx
 */
#define MAGNETIC_BRAKING_GAMMA_DEFAULT 3.0

#define UPDATE_TIME TRUE
#define DO_NOT_UPDATE_TIME (!UPDATE_TIME)

/*
 * Nova settings
 */
#define BETA_REVERSE_NOVAE_GEOMETRY -1.0

/*
 * Macro to ignore artificial accretion
 */
#define ARTIFICIAL_ACCRETION_IGNORE -1.0

/*
 * Maximum stellar angular momentum change per timestep
 */
#define MAX_STELLAR_ANGMOM_CHANGE_DEFAULT 0.05

/*
 * Minimum angular momentum for timestep/rejection tests
 */
#define MIN_ANGMOM_FOR_TIMESTEP_AND_REJECTION_TESTS 1e-2

/*
 * Number of letters in the (English) alphabet
 */
enum { NUMBER_OF_LETTERS_IN_THE_ALPHABET = 26 };

/*
 * Number of alphabetical cases
 */
enum { NUMBER_OF_ALPHABETICAL_CASES = 2 };

/*
 * Default value for effective_metallicity and nucsyn_metallicity
 */
#define DEFAULT_TO_METALLICITY (-1.0)

/*
 * commands for calc_yields
 * NB YIELD_FINAL should never equal TRUE or FALSE
 */
enum {
    YIELD_NOT_FINAL,
    YIELD_FINAL
};

#ifdef __HAVE_LIBPTHREAD__
#include "binary_c_mutexes.def"
#undef X
#define X(MUTEX) BINARY_C_MUTEX_ ## MUTEX,
enum {
    BINARY_C_MUTEXES_LIST
    BINARY_C_MUTEX_NUMBER
};
#undef X
#endif// __HAVE_LIBPTHREAD__


/*
 * Timescale that forces re-calculation : this
 * cannot be physical
 */
#define FORCE_TIMESCALES_CALCULATION -666.67676767


#define TEST_RLOF_ONLY_THIS_TIMESTEP (FALSE)
#define TEST_RLOF_THIS_AND_PREVIOUS_TIMESTEP (TRUE)

/*
 * Function hooks
 */
#include "binary_c_function_hooks.def"
#undef X
#define X(F) BINARY_C_HOOK_##F,
enum { FUNCTION_HOOK_LIST NUMBER_OF_FUNCTION_HOOKS };
#undef X

/*
 * Arguments for skip_bad_args
 */
#define SKIP_BAD_ARGS_TYPE_LIST \
    X(         NONE)            \
    X( WITH_WARNING)            \
    X(   NO_WARNING)
#undef X
#define X(TYPE) SKIP_BAD_ARGS_##TYPE,
enum {
    SKIP_BAD_ARGS_TYPE_LIST
};
#undef X


/*
 * How to output random systems?
 */
enum {
    RANDOM_SYSTEM_OUTPUT_WITH_TIME,
    RANDOM_SYSTEM_OUTPUT_ARGS_ONLY
};

/*
 * Disable fixed-beta mass transfer efficiency
 * by default
 */
#define FIXED_BETA_MASS_TRANSFER_EFFICIENCY_DISABLED -1.0

/*
 * TZO states
 */
enum {
    TZO_NONE,
    TZO_MERGER,
    TZO_TRANSFER
};

/*
 * star->merge_state settings:
 * STAR_JUST_MERGED = merged this timestep
 * STAR_MERGED = merged previous timestep
 * STAR_NOT_MERGED = no merging
 */
enum {
    STAR_JUST_MERGED = 2,
    STAR_MERGED = 1,
    STAR_NOT_MERGED = 0
};

enum {
    STARDATA_OUTER,
    STARDATA_INNER
};

enum {
    STAR_NORMAL,
    STAR_PROXY
};


#endif /* BINARY_MACROS_H */
