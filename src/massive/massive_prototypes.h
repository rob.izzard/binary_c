#pragma once
#ifndef MASSIVE_PROTOTYPES_H
#define MASSIVE_PROTOTYPES_H


double Constant_function Fryer2012_Eq10(const double Mc_CO);
double Constant_function Fryer2012_Eq11(const double M,
                                        const double Mc_CO,
                                        const double M_proto);
double Constant_function Fryer2012_Eq12(const double M_proto,
                                        const double M_fb);
double Constant_function Fryer2012_Eq13(const double M_remnant_baryonic);
double Constant_function Fryer2012_Eq14(const double M_remnant);
double Constant_function Fryer2012_Eq15(const double Mc_CO Maybe_unused);
double Constant_function Fryer2012_Eq16(const double M,
                                        const double Mc_CO,
                                        const double M_proto);
double Constant_function Fryer2012_Eq17(const double M_proto,
                                        const double M_fb);
double Constant_function Fryer2012_Eq18(const double Mc_CO);
double Constant_function Fryer2012_Eq19(const double M,
                                        const double Mc_CO,
                                        const double M_proto);
double Constant_function Fryer2012_Eq20(const double M_proto,
                                        const double M_fb);
double Constant_function Fryer2012_remnant_gravitational_mass(const struct stardata_t * const stardata,
        const double M_baryon);

double Fryer2012_M_fb(struct stardata_t * const stardata,
                      const double M,
                      const double Mc_CO,
                      const double M_proto);
double Fryer2012_M_proto(struct stardata_t * const stardata,
                         const double Mc_CO);
double Fryer2012_f_fb(struct stardata_t * const stardata,
                      const double M,
                      const double Mc_CO);

double neutrino_emission_limited_baryonic_to_gravitational_remnant_mass(
    struct stardata_t * RESTRICT const stardata,
    const struct star_t * RESTRICT const star,
    const double Mrem_bary);
double baryonic_to_gravitational_remnant_mass(
    struct stardata_t * RESTRICT const stardata,
    const struct star_t * RESTRICT const star,
    const double Mrem_bary);

double Constant_function Gao2020_remnant_gravitational_mass(struct stardata_t * const stardata,
                                                            const double M_remnant_baryonic);


#endif // MASSIVE_PROTOTYPES_H
