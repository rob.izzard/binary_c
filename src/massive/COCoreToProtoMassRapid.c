#include "../binary_c.h"
No_empty_translation_unit_warning;


double Constant_function COCoreToProtoMassRapid(const double COCoreMass)
{
/*
      Equation 10 in Fryer et al 2012

      Parameters
      ------------
      COCoreMass : double
      Carbon Oxygen (CO) core mass in Msol

      Returns
      --------
      Mproto : double
      Mass of Fe/Ni proto core in Msol

      -- Simon Stevenson
    */
    if(COCoreMass < 4.82)
    {
        return 1.50;
    }
    else if(COCoreMass < 6.31)
    {
        return 2.11;
    }
    else if(COCoreMass < 6.75)
    {
        return 0.69 * COCoreMass - 2.26;
    }
    else
    {
        return 0.37 * COCoreMass - 0.07;
    }
}
