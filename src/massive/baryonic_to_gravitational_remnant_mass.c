#include "../binary_c.h"
No_empty_translation_unit_warning;

double baryonic_to_gravitational_remnant_mass(
    struct stardata_t * RESTRICT const stardata,
    const struct star_t * RESTRICT const star,
    const double M_remnant_baryonic)
{
    /*
     * Function to handle the choice of the prescription
     * to calculate the gravitational remnant mass.
     */
    double M_remnant_gravitational;

    if(stardata->preferences->baryonic_to_gravitational_remnant_mass_prescription == BARY_TO_GRAV_NEUTRINO_EMISSION_LIMITED)
    {
        M_remnant_gravitational = neutrino_emission_limited_baryonic_to_gravitational_remnant_mass(
            stardata,
            star,
            M_remnant_baryonic
            );
    }
    else if(stardata->preferences->baryonic_to_gravitational_remnant_mass_prescription == BARY_TO_GRAV_FRYER_2012)
    {
        M_remnant_gravitational = Fryer2012_remnant_gravitational_mass(
            stardata,
            M_remnant_baryonic
            );
    }
    else if(stardata->preferences->baryonic_to_gravitational_remnant_mass_prescription == BARY_TO_GRAV_GAO2020)
    {
        /*
         * Gao+ 2020
         * Frontiers of Physics, Volume 15, Issue 2, article id.24603
         */
        M_remnant_gravitational = Gao2020_remnant_gravitational_mass(
            stardata,
            M_remnant_baryonic
            );
    }
    else
    {
        Exit_binary_c(BINARY_C_WRONG_ARGUMENT, "the choice for baryonic_to_gravitational_remnant_mass_prescription (%d) is unknown.\n",
                      stardata->preferences->baryonic_to_gravitational_remnant_mass_prescription);
    }

    return M_remnant_gravitational;
}
