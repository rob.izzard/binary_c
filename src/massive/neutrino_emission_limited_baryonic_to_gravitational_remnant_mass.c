#include "../binary_c.h"
No_empty_translation_unit_warning;


double Pure_function neutrino_emission_limited_baryonic_to_gravitational_remnant_mass(struct stardata_t * RESTRICT const stardata,
        const struct star_t * RESTRICT const star,
        const double M_remnant_baryonic)
{
    /*
     * Function to take the baryonic mass and reduce it due to
     * neutrinos carrying away gravitational binding energy from the core during collapse
     *
     * In fryer 2012 (https://ui.adsabs.harvard.edu/abs/2012ApJ...749...91F) the give prescriptions
     * for the NS baryonic to gravitational mass, and for the BH
     *
     * We take the formula for NS from fryer, but for black holes we limit the total
     * mass carried away by neutrinos to at maximum 0.5 solar mass.
     *  This is motivated by the fact that the neutrinisation only occurs in the pre-collapse iron core
     *	which is at maximum 5 solar mass, and the finite time in which the neutrinos can be emitted.
     *  See e.g. https://doi.org/10.3847/2041-8213/aba74e
     */

    double M_remnant_gravitational;

    if(M_remnant_baryonic <= stardata->preferences->max_neutron_star_mass)
    {
        M_remnant_gravitational = Fryer2012_Eq13(M_remnant_baryonic);
    }
    else
    {
        // Based on the neutrino flux and the time it takes to form a black hole
        M_remnant_gravitational = M_remnant_baryonic - Min(0.5, 0.1 * M_remnant_baryonic);
    }

    Dprint("gravitationalRemnantMassGeneral: t=%30.12e modelno: %d star mass: %g stellar type: %d m_baryonic: %g m_grav: %g\n", stardata->model.time, stardata->model.model_number, star->mass, star->stellar_type, M_remnant_baryonic, M_remnant_gravitational);

    return M_remnant_gravitational;
}
