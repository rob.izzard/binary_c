#include "../binary_c.h"
No_empty_translation_unit_warning;

/*
 * Fryer et al. (ApJ 749,91 2012) functions
 *
 * https://iopscience.iop.org/article/10.1088/0004-637X/749/1/91/pdf
 *
 * The notation in here follows the paper, and I while
 * have written a few comments, you should check the paper
 * for details.
 *
 * Startrack algorithm: Eqs 10-14
 * Rapid algorithm: Eqs 15-17
 * Delayed algorithm: Eqs 18-20
 */

/*
 * Startrack algorithm
 */
double Constant_function Fryer2012_Eq10(const double Mc_CO)
{
    /*
     * Final Fe/Ni core mass, M_proto, based on
     * Hurley et al. (2000) and Timmes et al. (1996)
     */
    return
        Mc_CO < 4.82 ? 1.50 :
        Mc_CO < 6.31 ? 2.11 :
        Mc_CO < 6.75 ? (0.69 * Mc_CO - 2.26) :
        (0.37 * Mc_CO - 0.07);
}

double Constant_function Fryer2012_Eq11(const double M,
                                        const double Mc_CO,
                                        const double M_proto)
{
    /*
     * Fallback mass, M_fb, as a function of core mass.
     */
    const double M_fb =
        Mc_CO < 5.0 ? 0.0 :
        (
            (M - M_proto) *
            (
                Mc_CO < 7.6 ? (0.378 * Mc_CO - 1.889) :
                1.0
                )
            );
    return Limit_range(M_fb,0.0,M - M_proto);
}

double Constant_function Fryer2012_Eq12(const double M_proto,
                                        const double M_fb)
{
    /*
     * Remnant baryonic mass
     */
    return M_proto + M_fb;
}

double Constant_function Fryer2012_Eq13(const double M_remnant_baryonic)
{
    /*
     * Remnant gravitational mass of neutron stars
     */
    return M_remnant_baryonic - 0.075 * Pow2(M_remnant_baryonic);
}

double Constant_function Fryer2012_Eq14(const double M_remnant)
{
    /*
     * Remnant gravitational mass of black holes
     */
    return M_remnant * 0.9;
}

double Constant_function Fryer2012_remnant_gravitational_mass(const struct stardata_t * const stardata,
                                                              const double M_remnant_baryonic)
{
    /*
     * Wrapper for Eqs. 13,14 to act based on stellar type
     */
    return
        M_remnant_baryonic < stardata->preferences->max_neutron_star_mass ?
        Fryer2012_Eq13(M_remnant_baryonic): /* NS */
        Fryer2012_Eq14(M_remnant_baryonic); /* BH */
}

/*
 * Rapid algorithm
 */
double Constant_function Fryer2012_Eq15(const double Mc_CO Maybe_unused)
{
    /*
     * Proto-compact object mass
     */
    return 1.0;
}

double Constant_function Fryer2012_Eq16(const double M,
                                        const double Mc_CO,
                                        const double M_proto)
{
    /*
     * Fallback mass, M_fb
     */

    /* this is in the Fryer paper */
//#define Eq16_a1 (0.25 -  (1.275/M - M_proto))
    /* but it probably should be this, as in Spera+2015 */
#define Eq16_a1 (0.25 -  (1.275/(M - M_proto)))

#define Eq16_b1 (-11.0 * Eq16_a1 + 1.0)
    const double M_fb =
        Mc_CO < 2.5 ? 0.2 :
        Mc_CO < 6.0 ? (0.286 * Mc_CO - 0.514) :
        (
            (M - M_proto) * (
                Mc_CO < 7.0 ? 1.0 :
                Mc_CO < 11.0 ? (Eq16_a1 * Mc_CO + Eq16_b1) :
                1.0
                )
            );
    return Limit_range(M_fb,0.0,M - M_proto);
}

double Constant_function Fryer2012_Eq17(const double M_proto,
                                        const double M_fb)
{
    /*
     * Remnant baryonic mass
     */
    return M_proto + M_fb;
}


/*
 * Delayed algorithm
 */
double Constant_function Fryer2012_Eq18(const double Mc_CO)
{
    /*
     * M_proto
     */
    return
        Mc_CO < 3.5 ? 1.2 :
        Mc_CO < 6.0 ? 1.3 :
        Mc_CO < 11.0 ? 1.4 :
        1.6;
}

double Constant_function Fryer2012_Eq19(const double M,
                                        const double Mc_CO,
                                        const double M_proto)
{

/* this is in the paper */
//#define Eq19_a2 (0.133 - (0.093/M - M_proto))

/* but it probably should be this */
#define Eq19_a2 (0.133 - (0.093/(M - M_proto)))

#define Eq19_b2 (-11.0*Eq19_a2 + 1.0)
    const double M_fb =
        Mc_CO < 2.5 ? 0.2  :
        Mc_CO < 3.5 ? (0.5 * Mc_CO - 1.05) :
        (
            (M - M_proto) *
            (
                Mc_CO < 11.0 ? (Eq19_a2 * Mc_CO + Eq19_b2) :
                1.0
                )
            );
    return Limit_range(M_fb, 0.0, M - M_proto);
}

double Constant_function Fryer2012_Eq20(const double M_proto,
                                        const double M_fb)
{
    /*
     * Remnant baryonic mass
     */
    return M_proto + M_fb;
}

/*
 * Wrapper functions
 *
 * Use these to select based on which of the three prescriptions
 * you wish to use.
 */
double Fryer2012_M_fb(struct stardata_t * const stardata,
                      const double M,
                      const double Mc_CO,
                      const double M_proto)
{
    return
        stardata->preferences->BH_prescription==BH_FRYER12_DELAYED ?
        Fryer2012_Eq19(M,Mc_CO,M_proto) :
        stardata->preferences->BH_prescription==BH_FRYER12_RAPID ?
        Fryer2012_Eq16(M,Mc_CO,M_proto) :
        stardata->preferences->BH_prescription==BH_FRYER12_STARTRACK ?
        Fryer2012_Eq11(M,Mc_CO,M_proto) :
        0.0;
}

double Fryer2012_M_proto(struct stardata_t * const stardata,
                         const double Mc_CO)
{
    return
        stardata->preferences->BH_prescription==BH_FRYER12_DELAYED ?
        Fryer2012_Eq18(Mc_CO) :
        stardata->preferences->BH_prescription==BH_FRYER12_RAPID ?
        Fryer2012_Eq15(Mc_CO) :
        stardata->preferences->BH_prescription==BH_FRYER12_STARTRACK ?
        Fryer2012_Eq10(Mc_CO) :
        0.0;
}

double Fryer2012_f_fb(struct stardata_t * const stardata,
                      const double M,
                      const double Mc_CO)
{
    /*
     * Fallback fraction
     */
    const double M_proto =
                    Fryer2012_M_proto(stardata,Mc_CO);
    const double M_fb =
        Fryer2012_M_fb(stardata,M,Mc_CO,M_proto);

    return Limit_range(M_fb / (M - M_proto), 0.0, 1.0);
}
