#include "../binary_c.h"
No_empty_translation_unit_warning;

double Constant_function Gao2020_remnant_gravitational_mass(struct stardata_t * const stardata Maybe_unused,
                                                            const double M_remnant_baryonic)
{
    /*
     * Inversion of Gao et al. (2020)'s quadratic relation
     * between Mgrav and Mbary for non-spinning neutron stars.
     *
     * Frontiers of Physics, Volume 15, Issue 2, article id.24603
     * https://arxiv.org/pdf/1905.03784
     *
     * We solve the cubic in Equation 5
     *
     * Mb = Mg +A1 × Mg^2 + A2 × Mg^3
     *
     * hence
     *
     * 0 = A2 * Mgrav^3 + A1 * Mgrav^2 + 1.0 * Mgrav - Mbaryon
     */
    const double A1 = 0.0729;
    const double A2 = 0.0032;
    unsigned int nsolutions;
    double x[3],y[3];
    SolveCubic(A2,A1,1.0,-M_remnant_baryonic,&nsolutions,x,y);
    Dprint("baryonic core %g -> cubic solutions %g %g %g , %g %g %g\n",
           M_remnant_baryonic,
           x[0],x[1],x[2],
           y[0],y[1],y[2]);

    return
        Is_zero(y[0]) ? x[0] :
        (Is_zero(y[1]) && x[1]>TINY) ? x[1] :
        (Is_zero(y[2]) && x[2]>TINY) ? x[2] :
        x[0];
}
