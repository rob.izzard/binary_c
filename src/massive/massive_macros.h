#pragma once
#ifndef MASSIVE_MACROS_H
#define MASSIVE_MACROS_H

/*
 * List of prescriptions to handle the baryonic
 * to gravitational remnant mass calculations
 */
#define BARY_TO_GRAV_PRESCRIPTIONS_LIST \
    X(                FRYER_2012)       \
    X( NEUTRINO_EMISSION_LIMITED)       \
    X(                   GAO2020)

#define X(CODE) BARY_TO_GRAV_##CODE ,
enum { BARY_TO_GRAV_PRESCRIPTIONS_LIST };
#undef X


#endif // MASSIVE_MACROS_H
