#pragma once
#ifndef DISC_POWER_LAWS_H
#define DISC_POWER_LAWS_H

#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef DISCS

// NB power law for temperature is not defined here
#define POWER_LAW_TEMPERATURE -1
#define POWER_LAW_SIGMA 0
#define POWER_LAW_MASS_INTEGRAND 1
#define POWER_LAW_LUMINOSITY_INTEGRAND 2
#define POWER_LAW_ANGULAR_MOMENTUM_INTEGRAND 3
#define POWER_LAW_MOMENT_OF_INERTIA_INTEGRAND 4
#define POWER_LAW_GRAVITATIONAL_POTENTIAL_ENERGY_INTEGRAND 5
#define POWER_LAW_KINETIC_ENERGY_INTEGRAND 6
#define POWER_LAW_ANGULAR_MOMENTUM_FLUX_BINARY_INTEGRAND 7
#define POWER_LAW_MASS_WEIGHTED_VISCOUS_TIMESCALE 8
#define POWER_LAW_ANGULAR_MOMENTUM_CONSTRAINT 9
#define POWER_LAW_ANGULAR_MOMENTUM_FLUX_DISC_INTEGRAND 10

/*
 * Number of power laws to calculate in each disc
 */
#define DISCS_NUMBER_OF_POWER_LAWS 12

#define POWER_LAW_STRINGS {                     \
        "Sigma",                                \
        "Mass integrand",                       \
        "L integrand",                          \
        "J integrand",                          \
        "I integrand",                          \
        "grav PE integrand",                    \
        "KE integrand",                         \
        "F from binary->torqueF integrand",      \
        "Weighted tvisc",                       \
        "Ang mom constraint",                   \
        "F from disc->torqueF integrand",       \
    }

/*
 * Note that DISCS_NUMBER_OF_POWER_LAWS in disc_parameters.h
 * should be one greater than the largest power law defined
 * above
 */

#endif // DISCS
#endif // DISC_POWER_LAWS_H
