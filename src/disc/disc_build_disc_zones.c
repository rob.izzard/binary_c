#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef DISCS

int disc_build_disc_zones(struct stardata_t * const stardata,
                          struct disc_t * const disc,
                          const struct binary_system_t * const binary)
{
    /*
     * Make a new zone list in a disc and zone it.
     */
    const Boolean status = disc_new_zone_list(stardata,
                                              disc,
                                              binary,
                                              disc->thermal_zones);

    Discdebug(2,"new_zone_list Rin = %g, Rout = %g : ok ? %d\n",disc->Rin,disc->Rout,status);

    if(status == DISC_ZONES_OK)
    {
        disc_setup_zones(stardata,disc,binary);
    }
    return status;
}

#endif//DISCS
