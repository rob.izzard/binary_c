#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef DISCS

#ifdef POWER_LAW_SANITY_CHECKS
void disc_check_power_laws(const struct disc_t * const d,
                           const Boolean output)
{
    /*
     * Sanity checks for zone power laws
     */
    Disc_zone_counter i;
    Boolean canrepeat = Boolean_(output == FALSE);
    Boolean fail = Boolean_(output==TRUE);

    for(i=0;i<d->n_thermal_zones;i++)
    {
        const struct disc_thermal_zone_t * z = &d->thermal_zones[i], *prevz;
        struct power_law_t * PL, * prevPL=NULL;

        int j;
        prevz = i>0 ? &d->thermal_zones[i-1] : NULL;

        for(j=-1;j<DISCS_NUMBER_OF_POWER_LAWS;j++)
            //for(j=-1;j<2;j++) // TODO remove this line when other laws are repaired
        {
            if(j==-1)
            {
                PL = &z->Tlaw;
                if(i>0) prevPL = &prevz->Tlaw;
                if(output==TRUE) printf("ZONE %d : T(R)  , ",i);
            }
            else
            {
                PL = &z->power_laws[j];
                if(i>0) prevPL = &prevz->power_laws[j];
                if(output==TRUE) printf("ZONE %d : PL%d(R), ",i,j);
            }

            if(output==TRUE)
                printf("%g < R < %g, %g < A < %g, exponent %g\n",
                       PL->R0,
                       PL->R1,
                       PL->A0,
                       PL->A1,
                       PL->exponent
                    );

            /*
             * Tests:
             *
             * Check that radii are > 0
             */
            if(Less_or_equal(PL->R0,0.0) || Less_or_equal(PL->R1,0.0))
            {
                if(output==TRUE)
                    printf("%s >> PLAW ERROR R0 <= 0 or R1 <= 0 %s\n",stardata->store->colours[RED],stardata->store->colours[COLOUR_RESET]);
                fail=TRUE;
            }

            /*
             * Check that power laws evaluated at the beginning and
             * end of the zone match A0 and A1
             */
            if(!Fequal(power_law(PL,PL->R0)/PL->A0,1.0))
            {
                if(output==TRUE)
                    printf("%s >> power law evaluated at R0 (%g) does not equal A0 (%g) %s\n",
                           stardata->store->colours[RED],
                           power_law(PL,PL->R0),
                           PL->A0,
                           stardata->store->colours[COLOUR_RESET]);
                fail=TRUE;
            }
            if(!Fequal(power_law(PL,PL->R1)/PL->A1,1.0))
            {
                if(output==TRUE)
                    printf("%s >> power law evaluated at R1 (%g) does not equal A1 (%g) %% error = %g cf TINY=%g %g %s\n",
                           stardata->store->colours[RED],
                           power_law(PL,PL->R1),
                           PL->A1,
                           power_law(PL,PL->R1)/PL->A1-1.0,
                           TINY,
                           fabs(power_law(PL,PL->R1) - PL->A1),
                           stardata->store->colours[COLOUR_RESET]);
                fail=TRUE;
            }

            if(i!=0)
            {
                /*
                 * Multiple zones
                 */

                /*
                 * Check that power laws are continuous
                 */
                if(!Float_same_within_eps(PL->A0/prevPL->A1,
                                 1.0,
                                 1e-10))
                {
                    if(output==TRUE)
                        printf("%s >> PLAW ERROR : A0[%d]=%20.12e != A1[%d]=%20.12e (diff %g)%s\n",
                               stardata->store->colours[RED],
                               i,
                               PL->A0,
                               i-1,
                               prevPL->A1,
                               PL->A0/prevPL->A1 - 1.0,
                               stardata->store->colours[COLOUR_RESET]
                            );
                    fail=TRUE;
                }

                /*
                 * Check that temperature power law exponents drop outwards
                 */
                if(j==-1 && fabs(d->thermal_zones[i].Tlaw.exponent) > fabs(d->thermal_zones[i-1].Tlaw.exponent))
                {
                    if(output==TRUE)
                        printf("%s >> PLAW ERROR : exponent magntiude should descend outwards%s\n",
                               stardata->store->colours[RED],
                               stardata->store->colours[COLOUR_RESET]);
                    fail=TRUE;
                }
            }
        }
    }

    if(fail == TRUE)
    {
        printf("check %d %d\n",output,fail);
        /*
         * Failure : redo checks with output switched on, then exit.
         */
#ifdef POWER_LAW_SANITY_CHECKS
        if(canrepeat==TRUE) check_power_laws(d,TRUE);
#endif
        if(output==TRUE) Exit_binary_c_no_stardata(2,"Check power laws failed!\n");
    }


}
#endif // POWER_LAW_SANITY_CHECKS
#endif //DISCS
