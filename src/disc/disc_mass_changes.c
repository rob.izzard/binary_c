#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef DISCS
void disc_mass_changes(struct stardata_t * const stardata,
                       struct disc_t * const newdisc,
                       struct disc_t * const olddisc,
                       const struct binary_system_t * const binary,
                       const double multiplier,
                       const double ddt
    )
{
    /*
     * Calculate derivatives and apply disc mass and angular momentum
     * changes if the timestep passed in (ddt, seconds) is non-zero.
     *
     * Also set the changes the binary orbit (e.g. eccentricity
     * pumping) if ddt is non-zero.
     */
    Discdebug(1,"Post-structure: derivatives, edge strip\n");

    /*
     * Calculate derivatives based on the old disc structure,
     * also calculate evaporation radii.
     */
    disc_derivatives(newdisc,binary,stardata,multiplier,TRUE);

    Boolean success = FALSE;
    if(Is_not_zero(ddt))
    {
        /*
         * non-zero timestep : edge strip
         */
        disc_edge_stripping(stardata,
                            newdisc,
                            olddisc,
                            binary);

        /*
         * Recalculate slow derivatives based on the
         * new disc structure.
         * Do not recalculate evaporation radii.
         */
        disc_derivatives(newdisc,
                         binary,
                         stardata,
                         multiplier,
                         FALSE);

        /*
         * Apply slow derivatives, also updates the
         * orbital variables (e.g. eccentricity)
         */
        success = TRUE;
        disc_apply_derivatives(newdisc,
                               binary,
                               stardata,
                               ddt,
                               &success);
    }

    /*
     * If the disc has not been evaporated,
     * recalculate the angular momentum flux
     */
    if(success==TRUE && Disc_is_disc(newdisc))
    {
        newdisc->F = disc_angular_momentum_flux(newdisc->Rout,
                                                newdisc,
                                                binary);
    }
}

#endif // DISCS
