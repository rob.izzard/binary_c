#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef DISCS

void disc_show_disc(struct stardata_t * const stardata,
                    struct disc_t * const disc,
                    const struct binary_system_t * const binary)
{
    printf("===========================================================\n");
    printf("DISC %p at lifetime = %20.12g (%g 20.12y), M=%g (%g Msun), J=%g, F=%g, alpha=%g, gamma=%g, torqueF=%g, fuv=%g, kappa=%g, Rin=%g cm (%g Rsun %g AU), Rout=%g (%g Rsun %g AU), dt=%g, Tvisc0=%g, sigma0=%g, mu=%g, T(Rin)=%g; H(in)/R(in)=%g L=%g L_sun : solver %s guess %d\n",
           (void*)disc,
           disc->lifetime,
           disc->lifetime/YEAR_LENGTH_IN_SECONDS,
           disc->M,
           disc->M / M_SUN,
           disc->J,
           disc->F,
           disc->alpha,
           disc->gamma,
           disc->torqueF,
           disc->fuv,
           disc->kappa,
           disc->Rin,disc->Rin/R_SUN,disc->Rin/ASTRONOMICAL_UNIT,
           disc->Rout,disc->Rout/R_SUN,disc->Rout/ASTRONOMICAL_UNIT,
           disc->dt,
           disc->Tvisc0,
           disc->sigma0,
           disc->mu,
           disc->converged ? disc_inner_edge_temperature(disc) : -1,
           disc->converged ? disc_scale_height(disc->Rin,disc,binary)/disc->Rin : -1,
           disc->converged ? disc_total_luminosity(disc)/L_SUN : -1,
           Solver_string(disc->solver),
           disc->guess
        );
    disc_show_thermal_zones(stardata,
                            disc,
                            disc->thermal_zones,
                            disc->n_thermal_zones);
    double _M = disc_total_mass(disc);
    double _J = disc_total_angular_momentum(disc,binary);
    double _F = disc_total_angular_momentum_flux(disc,binary);
    double _epsM = fabs(disc->M/Max(_M,1e-100)-1.0);
    double _epsJ = fabs(disc->J/Max(_J,1e-100)-1.0);
    double _epsF = fabs(disc->F/Max(_F,1e-100)-1.0);
    double Fconstraint = disc_total_angular_momentum_constraint(disc,binary);
    printf("MJF disc %g %g %g integrals %g %g %g eps %g %g %g (sigma0 = %g Tvisc0 = %g) constraint %g\n",
           disc->M,disc->J,disc->F,
           _M,_J,_F,
           _epsM,_epsJ,_epsF,
           disc->sigma0,
           disc->Tvisc0,
           Fconstraint
        );

    printf("===========================================================\n");
}
#endif//DISCS
