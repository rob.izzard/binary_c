#pragma once
#ifndef DISC_POWER_LAW_PROTOTYPES_H
#define DISC_POWER_LAW_PROTOTYPES_H

void new_power_law(struct power_law_t * const p,
                   const double A0,
                   const double exponent,
                   const double R0,
                   const double R1);
void update_power_law(struct power_law_t * const p,
                      const double A,
                      const int n);
void new_related_power_law(const struct power_law_t * const old,
                           struct power_law_t * const new,
                           const double A0,
                           const double c,
                           const double m);

void match_power_laws(const struct power_law_t * const in,
                      struct power_law_t * const out);
void match_power_laws_inwards(const struct power_law_t * const in,
                              struct power_law_t * const out);
double Pure_function power_law(const struct power_law_t * const p,
                               const double r);
double Pure_function power_law_zone_integral(const struct power_law_t * const p);
double Pure_function power_law_zone_integral_region(const struct power_law_t * const p,
                                                    const double in,
                                                    const double out);

double power_law_crossing_radius(const struct power_law_t * const p0,
                                 const struct power_law_t * const p1);

#endif //DISC_POWER_LAW_PROTOTYPES_H
