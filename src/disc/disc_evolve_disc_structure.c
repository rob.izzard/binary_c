#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef DISCS

#include "disc_evolve_disc_structure.h"

/*
 * Evolve the disc for time dt (seconds).
 *
 * Returns the lifetime of the disc, or 0 if the
 * system is single (which means there can be no disc).
 */

double disc_evolve_disc_structure(struct stardata_t * const stardata,
                                  struct disc_t * const disc,
                                  const double dt)
{
#ifdef TEST_POWER_LAW_API
    /* or test the power law API */
    test_power_law_API();
#endif

    /*
     * Set the internal binary-star information structure
     */
    struct binary_system_t * binary = Calloc(1,sizeof(struct binary_system_t));
    disc_init_binary_structure(stardata,binary,disc);

    /*
     * Check the system is a binary star
     */
    Boolean system_is_binary = Boolean_(
        binary->separation > TINY &&
        Neither_is_zero(binary->m1,binary->m2)
        );

    /*
     * Time for which the disc is evolved in this subroutine
     */
    double t = 0.0;

    /*
     * Perhaps show the M,J parameter space?
     */
    const Boolean show_MJ_space = 0;

    if(system_is_binary==TRUE)
    {
        /*
         * Evolve for time dt.
         *
         * Note that we always calculate a timestep,
         * whether evolution is required or not.
         */

        struct disc_t * discwas = New_disc;

        /*
         * If we have a previous disc structure, put it in
         * discwas
         */
        if(disc->converged == TRUE)
        {
            Copy_disc(disc,discwas);
        }

        /*
         * Detect first timestep entry
         */
        Boolean first = Boolean_(Is_zero(disc->lifetime) || disc->first);

        /*
         * Assume we can evolve to start with
         */
        Boolean can_evolve = TRUE;

        /*
         * On the first timestep, or if something radical has changed,
         * the disc must be given an initial guess of a structure.
         */
        if(first)
        {
            Discdebug(1," %s \n",
                      first==TRUE ? "first timestep" :
                      disc->append==TRUE ? "append to previous disc" :
                      "!conv");

            /*
             * Use a small timestep the first time, but
             * not on append.
             */
            if((disc->append != TRUE && Is_not_zero(disc->lifetime))||
               Is_zero(disc->dt))
            {
                disc->dt = DISC_MIN_TIMESTEP;
            }

            /*
             * Perhaps explore the parameter space:
             * if so, we exit here
             */
            if(unlikely(show_MJ_space == TRUE))
            {
                disc_show_MJ_space(disc,binary,stardata);
            }

            memset(disc->loss,0,sizeof(struct disc_loss_t)*DISC_LOSS_N);

            disc_initial_structure(disc,
                                   binary,
                                   stardata,
                                   t,
                                   first,
                                   &can_evolve);

            if(!Disc_is_disc(disc))
            {
                can_evolve = FALSE;
            }

            if(can_evolve)
            {
                Discdebug(1,
                          "Have converged initial disc structure with M=%g J=%g\n",
                          disc->M/M_SUN,
                          disc->J);
                disc->converged = TRUE;
                Copy_disc(disc,discwas);
            }

            /* explore the parameter space */
            //disc_parameter_space(stardata,disc);
            //Exit_binary_c(2,"Found first structure? %s\n",Yesno(can_evolve));

        }

        /*
         * Time loop
         */
        while(can_evolve==TRUE &&
              dt - t > TINY)
        {
            Discdebug(1,
                      "can_evolve\n");

            if(!Disc_is_disc(disc))
            {
                can_evolve = FALSE;
            }

            /*
             * We have a previous (or initial) disc solution.
             */
            if(disc->converged == TRUE &&
               can_evolve == TRUE)
            {
                /*
                 * Set disc's timestep.
                 */
                disc_set_disc_timestep(stardata,disc,binary,dt);

                if(0)
                {
                    /* Sanity checks */

                    /*
                     * cf. Ragusa+ 2016 (MNRAS 1243,53)
                     */
                    double R=disc->Rout;
                    double H=disc_scale_height(R,disc,binary);
                    double tbin = 2.0*PI/binary->omega;

                    printf(
                              "SANITY 1 : R=%g H=%g : tvisc(Rout) = %g should be %g = %g\n",
                              R,H,
                              disc_viscous_timescale(R,disc,binary),
                              2.0/3.0 * Pow2(R)/disc_kinematic_viscosity(R,disc,binary),
                              5.0*sqrt(5.0)/(3.0*PI*disc->alpha) * Pow2(R/H) * tbin
                        );
                }

                /*
                 * Time for which we evolve the disc :
                 * either the disc's timestep or the
                 * remaining time, whichever is shorter.
                 */
                double ddt = Min(disc->dt,
                                 1.00000001*(dt - t));
                Discdebug(1,"[ddt %g disc->dt=%g dt=%g t=%g] y \n",
                          ddt/YEAR_LENGTH_IN_SECONDS,
                          disc->dt/YEAR_LENGTH_IN_SECONDS,
                          dt/YEAR_LENGTH_IN_SECONDS,
                          t/YEAR_LENGTH_IN_SECONDS);


                /*
                 * Calculate the structure at the next timestep
                 */
                int status;
                Discdebug(1,"check F %g\n",disc->F);
                if(disc->F < -TINY)
                {
                    /*
                     * If the angular momentum flux is < 0.0 we *cannot*
                     * converge and there is no possible stable structure.
                     *
                     * Set status to DISC_CONVERGENCE_FLUX_NEGATIVE and decide
                     * what to do below.
                     */
                    status = DISC_CONVERGENCE_FLUX_NEGATIVE;
                }
                else
                {
                    Discdebug(2,
                              "calculate new structure : Rin = %g Rout = %g Revap_in = %g Revap_out = %g\n",
                              disc->Rin / R_SUN,
                              disc->Rout / R_SUN,
                              disc->Revap_in / R_SUN,
                              disc->Revap_out / R_SUN
                        );
                    /*
                     * Calculate disc structure a time ddt later.
                     *
                     * at the inner and outer edge.
                     */
                    status = disc_calc_disc_structure(binary,
                                                      disc,
                                                      stardata,
                                                      t,
                                                      ddt);
                    Discdebug(1,"structure -> status = %d\n",status);
                }

                /*
                 * Check for convergence failure. If this happens,
                 * restore the disc.
                 */
                if(status == DISC_CONVERGENCE_FAILED ||
                   status == DISC_CONVERGENCE_FLUX_NEGATIVE)
                {
                    Discdebug(
                        1,
                        "struct->fail(%d,%s) restore and try with dt smaller\n",
                        status,
                        Convergence_status(status));

                    /*
                     * Try again with a smaller timestep if we are not shorter
                     * than the minimum timestep.
                     */
                    const double dtwas = disc->dt;
                    const double newdt = 0.1 * dtwas / DISC_TIMESTEP_HIGH_FACTOR;
                    const double mindt =
                        Min(DISC_MIN_TIMESTEP,
                            YEAR_LENGTH_IN_SECONDS *
                            stardata->preferences->minimum_timestep*1e6);

                    if(disc->lifetime > DISC_MAX_LIFETIME)
                    {
                        /*
                         * Disc exceeds maxmimum age
                         */
                        Discdebug(1,
                                  "Stop disc evolution : disc age %g exceeds DISC_MAX_LIFETIME = %g\n",
                                  disc->lifetime,
                                  DISC_MAX_LIFETIME);
                        Append_logstring(LOG_DISC,
                                         "Stop disc evolution : disc age %g exceeds DISC_MAX_LIFETIME = %g\n",
                                         disc->lifetime,
                                         DISC_MAX_LIFETIME);
                        Evaporate_disc(disc,
                                       "Disc age exceeds limit");
                    }
                    else if(disc->dt < mindt)
                    {
                        /*
                         * Timestep too small : stop evolution
                         */
                        can_evolve = FALSE;
                        if(disc->lifetime > TINY)
                        {
                            Append_logstring(LOG_DISC,
                                             "Stop disc evolution (dt = %g y too short, < mindt = %g) age %g y, M %g Msun, L = %g Lsun",
                                             disc->dt/YEAR_LENGTH_IN_SECONDS,
                                             mindt/YEAR_LENGTH_IN_SECONDS,
                                             disc->lifetime/YEAR_LENGTH_IN_SECONDS,
                                             disc->M/M_SUN,
                                             disc_total_luminosity(disc)/L_SUN);


                            Discdebug(1,"Disc failed to converge\n");
                            if(0)
                            {
                                Discdebug(1,"exploring the parameter space\n");
                                disc_parameter_space(stardata,disc);
                                Exit_binary_c(2,"Dt too small\n");
                            }
                        }
                        else
                        {
                            Unset_logstring(LOG_DISC);
                        }
                        Evaporate_disc(disc,
                                       "Disc timestep too small");
                    }
                    else
                    {
                        /*
                         * Timestep allowed : try to converge
                         */
                        can_evolve = TRUE;
                        Copy_disc(discwas,disc);
                        disc->F_stripping_correction = 0.0;
                        disc->dt = newdt;
                        Discdebug(1,
                                  "Try dt %g m = %g j = %g\n",
                                  disc->dt/YEAR_LENGTH_IN_SECONDS,
                                  disc->M/M_SUN,
                                  disc->J
                            );
                    }
                    Discdebug(1,
                              "trying again with disc->converged = %s, can_evolve = %d\n",
                              Truefalse(disc->converged),
                              can_evolve);
                    continue;
                }
                else
                {
                    /*
                     * Converged on new structure with correct
                     * derivatives.
                     */
                    Discdebug(1,"struct->ok\n");

                    /*
                     * Reset various flags that cannot apply twice
                     */
                    disc->first = FALSE;

                    /*
                     * Rin and Rout derivatives
                     */
                    disc->dRindt = (discwas->Rin - disc->Rin)/ddt;
                    disc->dRoutdt = (discwas->Rout - disc->Rout)/ddt;

                    /*
                     * Update ring detection parameter
                     */
                    disc->fRing = Is_zero(disc->Rin) ? 0.0 : fabs(disc->Rout/disc->Rin - 1.0);

                    /*
                     * Check for failure modes: failure_reason
                     *          is NULL if there is no failure.
                     */
                    char * failure_reason = disc_failure_mode(stardata,
                                                              disc,
                                                              binary,
                                                              status);
                    if(failure_reason && disc->append==FALSE)
                    {
#if defined DISC_LOG || defined DISC_LOG_2D
                        if(abs(stardata->preferences->disc_log) >= 2)
                        {
                            disc_logging(stardata,
                                         binary,
                                         DISC_LOG_EVOLVE_DISC_STRUCTURE2);
                        }
#endif // DISC_LOG or DISC_LOG_2D

                        Discdebug(1,
                                  "\nDisc failure : %s\n",
                                  failure_reason);
                        Append_logstring(LOG_DISC,
                                         "Evaporate disc (%s) age %g y, M %g %s, L = %g %s, Rin=%g %s, Rout=%g %s, Teff* %g %g ",
                                         failure_reason,
                                         disc->lifetime/YEAR_LENGTH_IN_SECONDS,
                                         Solar(disc->M,M),
                                         Solar(disc_total_luminosity(disc),L),
                                         Solar(disc->Rin,R),
                                         Solar(disc->Rout,R),
                                         Teff(0),
                                         Teff(1)
                            );
                        Evaporate_disc(disc,failure_reason);
                        Safe_free(failure_reason);

                        /*
                         * If the disc fails on this timestep,
                         * don't trust its feedback on the binary
                         */
                        Clear_disc_feedback(disc);

                        can_evolve = FALSE;
                        continue;
                    }
                    Safe_free(failure_reason);

                    /*
                     * Nothing is wrong, disc is converged with the correct
                     * derivatives : update the time
                     */
                    Discdebug(1,"disc good\n");

#if defined DISC_SHOW_TEST_EXPRESSIONS && DISC_SHOW_TEST_EXPRESSIONS==1
                    show_test_expressions(binary,disc);
#endif

                    /*
                     * Update the time
                     */
                    t += ddt;
                    if(disc->first==TRUE) disc->firstlog = TRUE;
                    disc->first = FALSE;
                    Discdebug(1,"t(+=%g)->%g\n",
                              ddt/YEAR_LENGTH_IN_SECONDS,
                              t/YEAR_LENGTH_IN_SECONDS);

                    /*
                     * save discwas for next time
                     */
                    Copy_disc(disc,discwas);

                    Discdebug(3,
                              "Conv ret M = %g J = %g F = %g sigma0 = %g Tvisc0 = %g\n",
                              disc->M,
                              disc->J,
                              disc->F,
                              disc->sigma0,
                              disc->Tvisc0);

                    /*
                     * Increase disc lifetime
                     */
                    disc->lifetime += ddt;

#ifdef DISC_LOG
                    /*
                     * Do logging
                     */
                    if(abs(stardata->preferences->disc_log)>=2)
                    {
                        disc_logging(stardata,
                                     binary,
                                     DISC_LOG_EVOLVE_DISC_STRUCTURE);
                    }
#endif//DISC_LOG
                }
            }
            else
            {
                disc->dM_binary = 0.0;
                disc->Mdot_binary = 0.0;
                disc->Mdot_ejected = 0.0;
                disc->Jdot_binary = 0.0;
                disc->edot_binary = 0.0;
            }

            if(can_evolve == FALSE)
            {
                /*
                 * Can no longer evolve : exit loop
                 */
                break;
            }
        }

        /* free memory */
        Safe_free(discwas);
    }
    else
    {
        /*
         * System is single
         */
        if(DISC_DEBUG)
            printf("Binary merged : cannot evolve circumbinary disc\n");
        Append_logstring(LOG_DISC,
                         "Evaporate disc : binary merged (age %g, M = %g Msun, L = %g Lsun)",
                         disc->lifetime/YEAR_LENGTH_IN_SECONDS,
                         disc->M/M_SUN,
                         disc_total_luminosity(disc)/L_SUN);
        Evaporate_disc(disc,
                       "Binary system is single");
        t = 0.0;
    }

    Safe_free(binary);
    Clear_disc_feedback(disc);
    return t;
}


#if defined DISC_SHOW_TEST_EXPRESSIONS && DISC_SHOW_TEST_EXPRESSIONS==1

static void show_test_expressions(struct binary_system_t * binary,
                                  struct disc_t * disc)
{
    double B =
        pow(Pow2(binary->Rstar) *
            (binary->flux*(1.0 - disc->albedo)
             /(4.0*STEFAN_BOLTZMANN_CONSTANT))
            *sqrt(disc->gamma*BOLTZMANN_CONSTANT/
                  (GRAVITATIONAL_CONSTANT*binary->mtot*M_PROTON)),
            2.0/7.0);
    double Tin = disc_temperature(disc->Rin,disc);
    Discdebug(2,
              "B from (Tin=%g) %g should be %g\n",
              Tin,
              Tin * pow(disc->Rin,-3.0/7.0),
              B
        );

    double sigmaRin = disc_column_density(disc->Rin,disc);

    Discdebug(2,"Sigma at Rin = %g\n",sigmaRin);
    Discdebug(2,"Sigma0 = %g expressions give %g %g\n",
              disc->sigma0,
              sigmaRin * Tin * Pow2(disc->Rin),
              disc->F * disc->mu / (3.0 * PI * disc->alpha *
                                    disc->gamma * BOLTZMANN_CONSTANT));


    double chi  = disc->mu /
        (3.0 *PI * disc->alpha * disc->gamma * BOLTZMANN_CONSTANT) /B;
    if(DISC_DEBUG)
    {
        printf("chi is %g\n",chi);
        printf("Mass integral gives %g\nExpression gives %g %g approx %g\n",
               disc_total_mass(disc),

               14.0/3.0 * PI * sigmaRin * Pow2(disc->Rin) *
               (pow(disc->Rout/disc->Rin, 3.0/7.0) - 1.0),

               14.0/3.0 * PI * disc->F * chi *
               pow(disc->Rin,3.0/7.0) *
               (pow(disc->Rout/disc->Rin,3.0/7.0)-1.0),

               14.0/3.0 * PI * disc->F * chi * pow(disc->Rout,3.0/7.0)
            );

        printf("Ang mom integral gives %g\nExpression gives %g %g approx %g\n",
               disc_total_angular_momentum(disc,binary),

               2.0*PI*sqrt(GRAVITATIONAL_CONSTANT*binary->mtot)*
               sigmaRin * pow(disc->Rin,5.0/2.0) *
               (pow(disc->Rout/disc->Rin,13.0/14.0)-1.0),

               28.0/13.0 * PI * sqrt(GRAVITATIONAL_CONSTANT * binary->mtot) *
               disc->F * chi * pow(disc->Rin,13.0/14.0) *
               (pow(disc->Rout/disc->Rin,13.0/14.0)-1.0),

               28.0/13.0 * PI * sqrt(GRAVITATIONAL_CONSTANT * binary->mtot) *
               disc->F * chi * pow(disc->Rout,13.0/14.0)

            );

        printf("Flux integral gives %g\nExpression gives %g approx should be 1 %g\n",
               disc_total_angular_momentum_flux(disc,binary),

               7.0/32.0 * PI * disc->torqueF * Pow4(binary->separation) * Pow2(binary->q) *
               GRAVITATIONAL_CONSTANT * binary->mtot * sigmaRin *
               Pow3(1.0/disc->Rin) *
               (1.0 - pow(disc->Rout/disc->Rin, -32.0/7.0)),

               7.0/32.0 * PI * disc->torqueF * Pow4(binary->separation) *
               Pow2(binary->q) * GRAVITATIONAL_CONSTANT * binary->mtot * chi *
               pow(disc->Rin,-32.0/7.0)
            );
    }
    Exit_binary_c_no_stardata(0,"Exit after testing disc expressions");
}
#endif // DISC_SHOW_TEST_EXPRESSIONS

void No_return disc_show_MJ_space(struct disc_t * const disc,
                                  struct binary_system_t * const binary,
                                  struct stardata_t * const stardata)
{
    /*
     * Using the given binary system, go through the
     * mass and angular momentum parameter space to
     * determine which structres are possible.
     */

    double logm,logj;
    const double dlogm = 0.1;
    const double dlogj = 0.1;

    struct disc_t * discwas = New_disc_from(disc);
    Copy_disc(disc,discwas);

    printf("MJSPACE# binary: a=%g aL2=%g M1=%g M2=%g Jorb=%g P=%g\n",
           binary->aL2/R_SUN,
           binary->separation/R_SUN,
           binary->m1/M_SUN,
           binary->m2/M_SUN,
           binary->Jorb,
           binary->orbital_period / YEAR_LENGTH_IN_SECONDS
        );

    for(logm=-10; logm<-1; logm += dlogm)
    {
        for(logj = 40; logj < 53; logj += dlogj)
        {
            /*
             * un-log and convert to cgs
             */
            double m = exp10(logm) * M_SUN;
            double j = exp10(logj);

            /*
             * Restore the disc
             */
            Copy_disc(discwas,disc);

            /*
             * Set the new mass and angular momentum
             */
            disc->M = m;
            disc->J = j;

            /*
             * Try to converge
             */
            //Boolean can_evolve = TRUE;
            disc->converged = FALSE;
            disc_calc_disc_structure(binary,disc,stardata,0.0,0.0);

            /*
             * Check for convergence
             */
            Boolean converged = disc->converged;

            /*
             * Check if we're inside L2
             */
            Boolean inside_L2 = (converged && disc->Rin < binary->aL2) ? TRUE : FALSE;

            /*
             * Log
             */
            printf("MJSPACE %20g %20g %20g %20g %d %d %20g %20g\n",
                   m/M_SUN,
                   j,
                   disc->M/M_SUN,
                   disc->J,
                   converged,
                   inside_L2,
                   converged ? (disc->Rin/R_SUN) : -1.0,
                   converged ? (disc->Rout/R_SUN) : -1.0
                );
            fflush(stdout);
        }
    }
    Exit_binary_c(BINARY_C_NORMAL_EXIT,
                  "Finished exploring M,J disc space\n");
}



#endif // DISCS
