#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef DISCS

/*
 * Given a converged disc, calculate some variables
 * that are required by other parts of the disc code.
 */

void disc_calculate_derived_variables(struct stardata_t * const stardata,
                                      struct disc_t * const disc,
                                      const struct binary_system_t * const binary)
{
    if(Disc_is_disc(disc))
    {
        disc->Hin = disc_scale_height(disc->Rin,disc,binary);
        Discdebug(1,"Hin = %g\n",disc->Hin);

        disc->RJhalf = disc_half_angular_momentum_radius(stardata,disc,binary);
        Discdebug(1,"RJhalf = %g cf Rin = %g\n",disc->RJhalf, disc->Rin);

        if(Is_not_zero(disc->RJhalf))
        {
            disc->HRJhalf = disc_scale_height(disc->RJhalf,disc,binary) /
                disc->RJhalf;
        }
        else
        {
            disc->HRJhalf = 0.0;
        }
        Discdebug(2,"HRJhalf = %g\n",disc->HRJhalf);
    }
}
#endif // DISCS
