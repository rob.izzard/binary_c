#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef DISCS

/*
 * Calculate the initial structure of the disc assuming
 * the derivatives are zero.
 *
 * Perhaps also reduce M and J to stabilise the disc.
 */
static void disc_reduced_initial_structure(struct disc_t * const disc,
                                           const struct binary_system_t * const binary,
                                           struct stardata_t * const stardata,
                                           const double t,
                                           const double Rring,
                                           Boolean * const can_evolve,
                                           int * const trycount,
                                           const Boolean vb);

void disc_initial_structure(struct disc_t * const disc,
                            const struct binary_system_t * const binary,
                            struct stardata_t * const stardata,
                            const double t,
                            const Boolean first,
                            Boolean * const can_evolve)
{
    const Boolean vb = FALSE;
    int kk = TRUE, trycount = 1;
    const double Rring = disc_ring_radius(disc,binary);

    while(trycount)
    {
        Discdebug(1,"ITRY%d ",trycount);
        double mwas = disc->M;
        double jwas = disc->J;
        Discdebug(2,
                  "try %d : Calc disc M = %g Msun, J = %g\n",
                  trycount,
                  mwas/M_SUN,
                  jwas);

        /*
         * First timestep disc structure
         */
        Discdebug(1,"init%d ",trycount);
        kk = disc_calc_disc_structure(binary,disc,stardata,t,0.0);
        Discdebug(1,"init->%d (%d) ",kk,DISC_CONVERGENCE_SUCCEEDED);

        if(kk == DISC_CONVERGENCE_SUCCEEDED &&
           Is_not_zero(disc->M))
        {
            if(vb)printf("\nINIT CONVERGED on try %d with M=%g J=%g\n",
                         trycount,
                         disc->M/M_SUN,
                         disc->J);
            trycount = 0;
            Discdebug(1,"OK CONVERGED INIT%d ",trycount);
        }
        else
        {
            /*
             * No stable structure found:
             * try reducing M and J to obtain one
             */
            if(first &&
               Is_not_zero(stardata->preferences->cbdisc_init_dM))
            {
                Discdebug(1,"\nReduceMJ ");
                disc_reduced_initial_structure(disc,
                                               binary,
                                               stardata,
                                               t,
                                               Rring,
                                               can_evolve,
                                               &trycount,
                                               vb);
            }
            else
            {
                /*
                 * We're not allowed to try lower mass solutions
                 * so just break the loop.
                 */
                trycount = 0;
                Evaporate_disc(disc,"Disc initial structure failed [no M reduction allowed]");
                Append_logstring(LOG_DISC,
                                 "Disc initial structure failed [no M reduction allowed]");
            }
            continue;
        }
    }

    /*
     * Check failure modes
     */
    char * s = disc_failure_mode(stardata,
                                 disc,
                                 binary,
                                 0);

    if(s != NULL)
    {
        //disc->converged = FALSE;
        //Evaporate_disc(disc,s);
        Discdebug(1,"Disc failure mode detected : %s\n",s);
        Append_logstring(LOG_DISC,
                         "failure mode detected : %s",s);
    }
    Safe_free(s);

    disc_convergence_status(stardata,"Initial",disc,binary);
    Discdebug(1,"initret ");
}

void disc_reduced_initial_structure(struct disc_t * const disc,
                                    const struct binary_system_t * const binary,
                                    struct stardata_t * const stardata,
                                    const double Rring,
                                    const double t,
                                    Boolean * const can_evolve,
                                    int * const trycount,
                                    const Boolean vb)

{
    /*
     * It may be that there is no stable disc structure when it is born.
     * In this case, reduce the mass more than the angular momentum.
     *
     * The initial Rin and Rout are important:
     *
     * Rin can be ~ twice the separation, while Rout
     * should be at least twice this, but try putting it
     * further out in order to converge.
     */
    disc->Rin = (1.0 + 0.1) * binary->separation;
    disc->Rout = DISC_LARGE_RADIUS;
    struct disc_t * disc_in = New_disc_from(disc);

    /*
     * Bisect in M,F with Rout = Rin
     */
#ifdef TWOSTEP
    const long int itmax = 10000;
    const long int itmaxM = itmax, itmaxF = itmax;
#endif // TWOSTEP
    Boolean converged = FALSE;

    if(Is_really_zero(disc->Tvisc0)) disc->Tvisc0 = 1e10 ;

    if(vb)printf("\nDISCINIT: INIT %g %g %g converged? %d\n",
                 disc->M/M_SUN,
                 disc->J,
                 disc->J/disc->M,
                 converged);
    /*
     * Mass is reduced by a factor multM < 1 each time,
     * where multM is stardata->preferences->cbdisc_init_dM
     *
     * Angular momentum is reduced by a factor multJ each time
     * where multJ = multM * dJdM
     * where dJdM = stardata->preferences->dJdM
     *
     * If xMJ < 1.0, this means that the disc increases its
     * specific angular momentum when mass is removed.
     *
     * Typically xMJ = 0.5.
     */
    const double multM = stardata->preferences->cbdisc_init_dM; // 0.9
    const double multJ  = multM * stardata->preferences->cbdisc_init_dJdM; // 0.5

    if(vb)printf("DISCINIT: INIT multM = %g, multJ = %g\n",multM, multJ);

    /*
     * Given
     * Rring  = J^2 / (G M^2 Mbin)
     *
     * estimate the mass required given Rring = 2a
     *
     * M = J / sqrt(2 G a Mbin)
     */
    {
        double mring = disc->J/sqrt(GRAVITATIONAL_CONSTANT * binary->separation * 2.0 * binary->mtot);
        Discdebug(2,"DISCINIT: A ring with J = %g at 2a = %g requires M = %g g = %g Msun\n",
                  disc->J,
                  2.0 * binary->separation,
                  mring,
                  mring/M_SUN
            );
    }

    while(converged == FALSE && *can_evolve == TRUE)
    {
        /*
         * Mass must be reduced
         */
        Discdebug(2,"DISCINIT: Mass must be reduced\n");

        double mwas = disc->M;
        double jwas = disc->J;
        double dM = - multM * mwas;
        double dJ = - multJ * jwas;

        if(vb)printf("DISCINIT: INIT dM = %g, dJ = %g\n",dM/M_SUN,dJ);

        disc->M = mwas + dM;
        disc->J = jwas + dJ;

        if(vb)printf("DISCINIT: INIT %g %g %g\n",
                     disc->M/M_SUN,
                     disc->J,
                     disc->J/disc->M);

        Discdebug(1,
                  "DISCINIT: reduce:M=%g(was %g),J=%g(was %g) ",
                  disc->M,mwas,
                  disc->J,jwas);
        Discdebug(2,
                  "DISCINIT:  >>> new M = %g, new J = %g\n",
                  disc->M,
                  disc->J);
        /*
         * Check that mass does not reduce below the
         * minimum possible.
         */
        if(disc->M / M_SUN < DISC_MINIMUM_DISC_MASS_MSUN)
        {
            Append_logstring(LOG_DISC,
                             "DISCINIT: No stable disc structure at age %g y, M = %g Msun, L = %g Lsun (Rring = %g, a = %g) ",
                             disc->lifetime/YEAR_LENGTH_IN_SECONDS,
                             disc->M/M_SUN,
                             disc_total_luminosity(disc)/L_SUN,
                             Rring / R_SUN,
                             stardata->common.orbit.separation
                );

            Discdebug(2,"DISCINIT: No stable (initial) disc structure found\n");
            Evaporate_disc(disc,"No stable initial disc structure found (M/M_SUN<DISC_MINIMUM_DISC_MASS_MSUN)");
            *can_evolve = FALSE;
            *trycount = 0;
            continue;
        }


        Boolean ok = FALSE;


        /*
         * Calculate new disc structure
         */
        int status = disc_calc_disc_structure(binary,disc,stardata,t,0.0);

        /*
         * The disc may have converged, in which case
         * check its structure against disc_failure_mode
         */
        if(disc->converged)
        {
            Discdebug(2,
                      "DISCINIT: Successfully Bisected for F = %g with Rin = %g cf a = %g\n",
                      disc->Rin,
                      disc->Rin,
                      binary->separation);

            /*
             * The solution is required to satisfy the disc_failure_mode
             */

            char * reason = disc_failure_mode(stardata,
                                              disc,
                                              binary,
                                              status);
            if(reason==NULL)
            {
                ok = TRUE;
            }
            else
            {
                Discdebug(1,"Disc failed because %s\n",reason);
                Safe_free(reason);
            }
        }


        if(ok == TRUE)
        {
            /*
             * A stable solution should follow
             */
            converged = TRUE;
            Discdebug(2,
                      "DISCINIT: Stable solution should follow >>>>>>> Rin = %g Rsun, Rout = %g Rsun, a = %g Rsun\n",
                      disc->Rin / R_SUN,
                      disc->Rout / R_SUN,
                      binary->separation / R_SUN);
            Discdebug(1,
                      "DISCINIT: ok:M=%g,J=%g ",
                      disc->M,
                      disc->J);
        }
        else
        {
            /*
             * Keep going unless the mass is so small we're under either
             * the hard limit or the limit set by the user
             */
            if(disc->M / M_SUN < DISC_MINIMUM_DISC_MASS_MSUN ||
               disc->M / M_SUN < stardata->preferences->cbdisc_minimum_mass)
            {
                Append_logstring(LOG_DISC,
                                 "DISCINIT: No stable disc structure at age %g y, M = %g Msun, L = %g Lsun (Rring = %g, a = %g) ",
                                 disc->lifetime/YEAR_LENGTH_IN_SECONDS,
                                 disc->M/M_SUN,
                                 disc_total_luminosity(disc)/L_SUN,
                                 Rring / R_SUN,
                                 stardata->common.orbit.separation);

                Discdebug(2,"DISCINIT: No stable (initial) disc structure found: M = %g < Min(hard = %g, user = %g)\n",
                          disc->M / M_SUN,
                          DISC_MINIMUM_DISC_MASS_MSUN,
                          stardata->preferences->cbdisc_minimum_mass);
                Evaporate_disc(disc,
                               "No stable initial disc structure found : M < minimum limit");
                *can_evolve = FALSE;
                *trycount = 0;
            }
        }
    }

    if(Disc_is_disc(disc))
    {
        Append_logstring(LOG_DISC,
                         "[ reduced M,J from %g,%g to %g,%g ] ",
                         disc_in->M/M_SUN,
                         disc_in->J,
                         disc->M/M_SUN,
                         disc->J);
    }
    else
    {
        Append_logstring(LOG_DISC,
                         "[ reduce M,J failed : no stable disc ]");
    }

    Safe_free(disc_in);
}


#endif // DISCS
