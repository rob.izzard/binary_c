#include "../binary_c.h"
No_empty_translation_unit_warning;

#if defined DISCS

//#undef DISC_DEBUG
//#define DISC_DEBUG 1

#define RIN_MIN_BISECT 1
#define RIN_MIN_QUADSECT 2

#include "disc_calc_disc_structure.h"
#include "disc_constraints.h"
#include "disc_boundary_conditions.h"

/************************************************************
 * disc_calc_disc_structure
 *
 * Calculate the structure of the disc given the constraints
 * mass M, angular momentum J and the flux at the inner edge.
 * The flux depends on Rin, J depends on Rout,
 * and the mass depends on both, so it's a complicated
 * minimization problem which we attempt to solve using GSL's
 * appropriate routines.
 *
 * The method is based on Lipunova's Greens Function solutions to
 * the viscous disc equations. See arXiv 1503.09093, also in ApJ.
 *
 *
 * The timestep, ddt (seconds), is passed in so that slow mass
 * and angular momentum changes can be performed. If ddt is zero,
 * the disc's derivatives

 * returns: DISC_CONVERGENCE_SUCCEEDED or DISC_CONVERGENCE_FAILED
 * depending on whether we can converge on a disc structure,
 * or not, respectively.
 *
 ************************************************************/

/*
 * TODO
 *
 * 1 Detect Rring solution : fix to that?
 *
 * 2 What is the effect on the eccentricity of mass accretion
 *   on the central binary?
 *   See e.g. Shi et al 2016 (Page 16, also Appendix A)
 *   also Lubow and Artymowicz 1993
 *
 * 3 I am still not convinced that the Lindblad resonance
 *   torque is different from the binary torque we take
 *   from Armitage. Are these really different things?
 *
 * 4 Forgan and Rice 2011 : disc fragmentation.
 */

int disc_calc_disc_structure(const struct binary_system_t * const binary,
                             struct disc_t * const disc,
                             struct stardata_t * const stardata,
                             const double t,
                             const double ddt)
{
    /* return value */
    int ret = DISC_CONVERGENCE_SUCCEEDED;

    /* Increase iteration counter */
    disc->iteration++;

    /* start with the prescribed binary torque */
    disc->torqueF = stardata->preferences->cbdisc_torquef;

    /* take a copy of the disc that was */
    struct disc_t * discwas = New_disc_from(disc);

    /*
     * Calculate derivatives and apply mass changes
     * only if the previous disc was converged,
     * otherwise its structure cannot be trusted.
     */
    if(Disc_is_disc(disc) && disc->converged == TRUE)
    {
        disc_mass_changes(stardata,disc,discwas,binary,1.0,ddt);

        /*
         * Overwrite discwas with the new fast-stripped disc,
         * with the latest derivatives
         */
        Copy_disc(disc,discwas);
    }

    /*
     * Testing: disc evaporation if wind evaporation lifetime < 1 year
     *
     * NB we allow edge stripping to be faster.
     */
    if(Disc_is_disc(disc) &&
       Is_not_zero(disc->t_m) &&
       Is_not_zero(stardata->preferences->cbdisc_minimum_evaporation_timescale) &&
       disc->t_m_slow < stardata->preferences->cbdisc_minimum_evaporation_timescale * YEAR_LENGTH_IN_SECONDS)
    {
        disc->dM_ejected += -disc->M;
        disc->dJ_ejected += -disc->J;
        Evaporate_disc(disc,
                       "Wind will evaporate the disc within 1 year");
        Append_logstring(LOG_DISC,
                         "Wind will evaporate disc in < %g y (tdisc=%g y, Mdisc=%g %s, Ldisc = %g %s)\n",
                         stardata->preferences->cbdisc_minimum_evaporation_timescale,
                         disc->lifetime/YEAR_LENGTH_IN_SECONDS,
                         Solar(disc->M,R),
                         Solar(disc_total_luminosity(disc),L)

            );
    }

    /*
     * if the disc has not evaporated, and enough time has passed,
     *  calculate its new structure
     */
    if(Disc_is_disc(disc))
    {
        /*
         * set up convergence parameter_types, constraints
         * and boundary conditions
         */
        disc_parameter * parameter_types;
        disc_constraint * constraints;
        const int n = disc_setup_convergence_parameter_types_and_constraints(stardata,
                                                                             disc,
                                                                             binary,
                                                                             &parameter_types,
                                                                             &constraints);


        /* start not converged */
        discwas->converged = FALSE;


#ifdef MEMOIZE
        memoize_clear_hash_contents(disc->memo);
#endif

        /*
         * Converge, if possible, with the default inner edge torque.
         */
        converge_with_torque(n,
                             parameter_types,
                             constraints,
                             binary,
                             disc,
                             discwas,
                             stardata,
                             t,
                             &ret);


#ifdef DISABLED____
        if(0){

            /*
             * If we found a disc, but the inner edge requires
             * shifting, bisect for the appropriate torque.
             */
            if(Rin_min_bisect != FALSE)
            {
                if(Rin_min_bisect == RIN_MIN_BISECT)
                {
                    /* use a bisector : very slow */
                    int error;
                    generic_bisect(&error,
                                   BISECT_USE_MONOCHECKS,
                                   BISECTOR_DISC_RIN_MIN,
                                   &converge_with_torque_wrapper,
                                   disc->torqueF,
                                   DISC_MINIMUM_TORQUE,
                                   DISC_MAXIMUM_TORQUE,
                                   DISC_TORQUEF_TOLERANCE,
                                   DISC_BISECTION_TORQUEF_MAX_ITERATIONS,
                                   DISC_BISECTION_TORQUEF_USELOG,
                                   1.0,
                                   n,
                                   parameter_types,
                                   constraints,
                                   binary,
                                   disc,
                                   discwas,
                                   stardata,
                                   t,
                                   &ret,
                                   &Rin_min_bisect);

                    if(error != BINARY_C_BISECT_ERROR_NONE)
                    {
                        ret = DISC_CONVERGENCE_FAILED;
                        disc->converged = FALSE;
                        Discdebug(1,
                                  "bisect on torquef failed: generic bisect error %d\n",
                                  error);
                    }
                    else
                    {
                        /*
                         * NB here you should NOT check that Rin >= Rin_min
                         * because it might not be. In fact, it should be Rin_min
                         * within a small tolerance which is NOT the same as the
                         * torquef tolerance (because the problem is non-linear).
                         */
                        if(disc->converged)
                        {
                            ret = DISC_CONVERGENCE_SUCCEEDED;
                            disc_calculate_derived_variables(disc,binary);
                            Discdebug(1,
                                      "bisect on torquef succeeded : Rin = %g, Rin_min = %g\n",
                                      disc->Rin/R_SUN,
                                      disc->Rin_min/R_SUN
                                );
                        }
                        else
                        {
                            ret = DISC_CONVERGENCE_FAILED;
                            disc->converged = FALSE;
                            Discdebug(1,
                                      "bisect on torquef failed: converged = %s, Rin>Rinmin = %s (%g,%g) %d ? Rout = %g\n",
                                      True_or_false(disc->converged),
                                      True_or_false(More_or_equal(disc->Rin,disc->Rin_min)),
                                      disc->Rin,
                                      disc->Rin_min,
                                      disc->Rin > disc->Rin_min,
                                      disc->Rout
                                );
                        }
                    }
                }
                else
                {
                    /*
                     * Use a quadsector with the GSL routines:
                     * this usually works and is very fast
                     */
                    converge_with_torque(n,
                                         parameter_types,
                                         constraints,
                                         binary,
                                         disc,
                                         discwas,
                                         stardata,
                                         t,
                                         &ret,
                                         &Rin_min_bisect);
                }
            }

        }
#endif // DISABLED

        /*
         * Calculate derived variables
         */
        if(disc->converged)
        {
            disc_calculate_derived_variables(stardata,disc,binary);
        }


        Discdebug(1,"disc_calc_disc_structure->%s ret %d (%s)\n",
                  True_or_false(disc->converged),
                  ret,
                  Convergence_status(disc->converged));

        /*
         * Check convergence status
         */
        disc_convergence_status(stardata,"calc_structure_return",disc,binary);

        /*
         * Check the solution is a local minimum
         */
        if(0 && disc->converged)
        {
            check_local_minimum(stardata,disc,binary);
        }

#ifdef DISC_SAVE_EPSILONS
        /*
         * Save epsilons for later logging
         */
        if(disc->converged)
        {
            for(int i=0;i<DISC_NUMBER_OF_CONSTRAINTS;i++)
            {
                disc->epsilon[i] = Disc_residual(disc,
                                                 binary,
                                                 i);

#ifdef DISC_RESIDUAL_WARNING
                if(i<4 && // what is wrong with residual 5?
                   disc->epsilon[i] > DISC_RESIDUAL_WARNING_THRESHOLD
                    )
                {
                    fprintf(stderr,
                            "Warning : disc residual (# %d) is %g which exceeds threshold for warning %g\n",
                            i,
                            disc->epsilon[i],
                            DISC_RESIDUAL_WARNING_THRESHOLD);
                }
#endif // DISC_EPSILON_WARNING

            }
        }
#endif//DISC_LOG

        /*
         * Free allocated memory
         */
        disc_free_convergence_parameter_types_and_constraints(&parameter_types,
                                                              &constraints);
    }


    if(disc->converged)
    {
        /*
         * Set M, J, F etc. to their converged values
         * as given by integrals over the disc structure.
         *
         * TOOD set Rin Rout when required
         */
        disc->M = Disc_constraint_value(disc,binary,DISC_CONSTRAINT_M_TO_DISC_M);
        disc->J = Disc_constraint_value(disc,binary,DISC_CONSTRAINT_J_TO_DISC_J);
        disc->F = Disc_constraint_value(disc,binary,DISC_CONSTRAINT_F_TO_DISC_F);
    }


    /*
     * free memory and return
     */
    Safe_free(discwas);

    return ret;
}
#ifdef DISABLED____

static double converge_with_torque_wrapper(const double torquef,
                                           void * p)
{
    /*
     * Map between va_args and the converge_with_torque function
     */
    Map_GSL_params(p,args);
    Map_varg(const int,n,args);
    Map_varg(const disc_parameter *,parameter_types,args);
    Map_varg(const disc_constraint *,constraints,args);
    Map_varg(const struct binary_system_t *,binary,args);
    Map_varg(struct disc_t *,disc,args);
    Map_varg(struct disc_t *,discwas,args);
    Map_varg(struct stardata_t *,stardata,args);
    Map_varg(const double, t,args);
    Map_varg(int *,ret,args);
    //Map_varg(Boolean *,Rin_min_bisect,args);
    va_end(args);

    disc->torqueF = torquef;
    discwas->torqueF = torquef;


    double x = converge_with_torque(n,
                                    parameter_types,
                                    constraints,
                                    binary,
                                    disc,
                                    discwas,
                                    stardata,
                                    t,
                                    ret
                                    //,Rin_min_bisect
        );

    return x;
}
#endif

static double converge_with_torque(const int n,
                                   const disc_parameter * parameter_types,
                                   const disc_constraint * constraints,
                                   const struct binary_system_t * binary,
                                   struct disc_t * disc,
                                   struct disc_t * discwas,
                                   struct stardata_t * stardata,
                                   const double t,
                                   int * ret
                                   //,Boolean * Rin_min_bisect,
    )
{
    Discdebug(2,
              "converge with torque n=%d disc=%p discwas=%p stardata=%p visc0=%g Rout=%g F=%g torqueF=%g Jdot(evap)=%g ret=%p \n",
              n,
              (void*)disc,
              (void*)discwas,
              (void*)stardata,
              disc->Tvisc0,
              disc->Rout,
              disc->F,
              disc->torqueF,
              disc->F_stripping_correction,
              (void*)ret
        );

    /* Ring radius : useful for a quick check on stability */
    const double Rring = disc_ring_radius(disc,binary);
    Discdebug(1,
              "Rring = %g Rsun cf a = %g Rsun\n",
              Rring/R_SUN,
              binary->separation / R_SUN);

    Discdebug(2,
              "RING (M=%g Msun, J=%g) RADIUS %g Rsun cf. separation %g\n",
              disc->M/M_SUN,
              disc->J,
              Rring/R_SUN,
              binary->separation/R_SUN
        );
    Discdebug(3,
              "Conv : initial guess Rin = %g Rout = %g\n",
              disc->Rin,
              disc->Rout);

    /*
     * Start unconverged
     */
    disc->converged = FALSE;

    /*
     * If the ring sits inside the binary
     * then the disc cannot be stable.
     *
     * NB this check should not be required if the
     * "too ringlike" check is also enabled.
     */
    if(stardata->preferences->cbdisc_fail_ring_inside_separation &&
       Rring < binary->separation)
    {
        Discdebug(1,"->Ring(%g)<a(%g)->fail\n",
                  Rring/R_SUN,
                  binary->separation/R_SUN
            );
        *ret = DISC_CONVERGENCE_FAILED;
    }
    else
    {
        /* debug */
        if(Disc_do_debug(3))
        {
            double _M = disc_total_mass(disc);
            double _J = disc_total_angular_momentum(disc,binary);
            double _F = disc_total_angular_momentum_flux(disc,binary);
            double _epsM = fabs(disc->M/Max(_M,1e-100)-1.0);
            double _epsJ = fabs(disc->J/Max(_J,1e-100)-1.0);
            double _epsF = fabs(disc->F/Max(_F,1e-100)-1.0);
            Discdebug(3,
                      "Conv init EPS %g %g %g : M = %g J = %g F = %g sigma0 = %g Tvisc0 = %g\n",
                      _epsM,_epsJ,_epsF,
                      disc->M,disc->J,disc->F,disc->sigma0,disc->Tvisc0);
        }


#ifdef CONVERGENCE_LOG
        /* start convergence log */
        FILE * convlog_fp = NULL;
        if(!convlog_fp)
        {
            convlog_fp = fopen("/tmp/convergence.log","w");
            fprintf(convlog_fp,
                    "# attempting to converge on M=%g J=%g 0\n#%11s%12s%12s%12s%12s%12s%12s%12s%12s%12s\n",
                    disc->M,
                    disc->J,
                    "M","J","F","Rin","Rout","epsM","epsJ","epsF","count","convd?");
        }
        /* output initial guess to convergence log */
        fprintf(convlog_fp,
                "% 12g% 12g% 12g% 12g% 12g%12s%12s%12s%12s%12s%12s\n\n",
                disc->M,
                disc->J,
                disc->F,
                disc->Rin,
                disc->Rout,
                "*","*","*","0","0","I"
            );
#endif // CONVERGENCE_LOG

        /*
         * At this point, disc->M, disc->J and disc->F
         * are the *desired* mass, angular momentum and
         * angular momentum flux.
         *
         * We require a Tvisc0, Rin and Rout that match.
         */

        Discdebug(2,"enter convergence loop\n");

        /*
         * Guesses for the disc structure.
         */
        double initial_guesses[DISC_SOLVER_NINITIAL_GUESSES+1][n];
        memset(initial_guesses,
               0,
               (DISC_SOLVER_NINITIAL_GUESSES+1)*n*sizeof(double));
        disc_set_disc_initial_guesses(n,
                                      parameter_types,
                                      initial_guesses,
                                      stardata,
                                      disc,
                                      discwas,
                                      binary,
                                      Rring);


        /*
         * Choose to use the previously succesful solver
         * first. On the first timestep,
         * disc->solver == DISC_SOLVER_NONE
         */
        int solvers[DISC_SOLVER_NSOLVERS+1] = {
            disc->solver,
            DISC_SOLVER_ORDERED_LIST
        };

        /*
         * Every DISC_SOLVER_RESET_MODULO iterations,
         * or if we have yet to complete DISC_SOLVER_RESET_MIN iterations,
         * or if DISC_SOLVER_RESET_MODULO is zero,
         * switch back to the default list which prioritises GSL solvers.
         * This means things should (on average) move
         * more quickly.
         */
        if(DISC_SOLVER_RESET_MODULO == 0 ||
           disc->iteration % DISC_SOLVER_RESET_MODULO == 0 ||
           disc->iteration < DISC_SOLVER_RESET_MIN)
        {
            /*
             * Reset the solver list.
             */
            solvers[0] = DISC_SOLVER_NONE;
        }
        else
        {

            /*
             * Do not allow a solver to be attempted twice.
             * DISC_SOLVER_NONE is simply ignored.
             */
            for(int i=1;i<DISC_SOLVER_NSOLVERS+1;i++)
            {
                if(solvers[i] == disc->solver)
                {
                    solvers[i] = DISC_SOLVER_NONE;
                    break;
                }
            }
        }

        Boolean failed = FALSE;

        struct solver_guess_pair_t {
            int isolver;
            int iguess;
            Boolean monte_carlo;
        };
        struct solver_guess_pair_t * solver_guess_pairs = NULL;
        int npairs = 0;

        /*
         * Make a list of {isolver, iguess} pairs
         *
         * First, we loop over the solvers going through the
         * guesses but not the monte carlo options.
         *
         * Then we add monte carlo options as a fallback,
         * because they are very slow.
         */

#define Add_pair(ISOLVER,IGUESS,MC)                                     \
        {                                                               \
            npairs ++;                                                  \
            solver_guess_pairs =                                        \
                Realloc(solver_guess_pairs,                             \
                        sizeof(struct solver_guess_pair_t) * npairs);   \
            struct solver_guess_pair_t * pair =                         \
                solver_guess_pairs + npairs - 1;                        \
            pair->isolver = (ISOLVER);                                  \
            pair->iguess = (IGUESS);                                    \
            pair->monte_carlo = (MC);                                   \
        }


        /*
         * Non-monte-carlo attempts
         */
        for(int isolver=0;
            (isolver < (DISC_SOLVER_NSOLVERS+1) && disc->converged==FALSE);
            isolver++)
        {
            if(isolver != DISC_SOLVER_NONE)
            {
                for(int iguess=0;
                    iguess < DISC_SOLVER_NINITIAL_GUESSES;
                    iguess++)
                {
                    if(Is_not_zero(initial_guesses[iguess][0]))
                    {
                        Add_pair(isolver,
                                 iguess,
                                 FALSE);
                    }
                }
            }
        }

        /*
         * Add monte-carlo fallback attempts
         */
        if(stardata->preferences->disc_n_monte_carlo_guesses > 0)
        {
            for(int isolver=0;
                (isolver < (DISC_SOLVER_NSOLVERS+1) && disc->converged==FALSE);
                isolver++)
            {
                if(isolver != DISC_SOLVER_NONE)
                {
                    Add_pair(isolver,
                             0, // guess is irrelevant
                             TRUE);
                }
            }
        }

        /*
         * Loop over {guess,solver} pairs
         */
        int npair = 0;
        int i_monte_carlo = 0;
        while(npair < npairs && disc->converged == FALSE)
        {
            struct solver_guess_pair_t * pair = solver_guess_pairs + npair;
            int isolver = pair->isolver;
            int iguess = pair->iguess;

            int solver = solvers[isolver];

            if(pair->monte_carlo)
            {
                /*
                 * Do disc_n_monte_carlo_guesses random guesses
                 */
                if(++i_monte_carlo < stardata->preferences->disc_n_monte_carlo_guesses)
                {
                    /*
                     * Keep looping
                     */
                    npair--;

                    /*
                     * Set monte carlo guess
                     */
                    disc_set_monte_carlo_guess(stardata,
                                               parameter_types,
                                               disc,
                                               n,
                                               &initial_guesses[iguess][0]);
                }
                else
                {
                    i_monte_carlo = 0;
                }
            }

            Discdebug(1,
                      "Solver %d (cf %d), test %g\n",
                      solver,
                      DISC_SOLVER_NONE,
                      initial_guesses[iguess][0]);
            /* debug */
            Discdebug(1,
                      "Try initial guess %d\n",
                      iguess);

            /* restore the old disc */
            Copy_disc(discwas,disc);

            /* save solver and guess number for later logging */
            disc->solver = solver;
            disc->guess = iguess;

            /* set the initial guess number iguess */
            disc_set_disc_initial_guess(stardata,
                                        disc,
                                        iguess,
                                        n,
                                        parameter_types,
                                        initial_guesses);

            Discdebug(1,
                      "Guess : Tvisc0 = %g, Rin = %g, Rout = %g\n",
                      disc->Tvisc0,
                      disc->Rin,
                      disc->Rout)

            if(solver != DISC_SOLVER_BISECTION)
            {
                /*
                 * Solve using GSL : this is
                 * the recommended option
                 */
                Discdebug(1,
                          "Call GSL multiroot solver %d = %s\n",
                          solver,
                          GSL_Multiroot_fsolver_string(solver));
                int fail = 0;
                disc->converged = disc_GSL_multiroot(solver,
                                                     n,
                                                     parameter_types,
                                                     constraints,
                                                     disc,
                                                     binary,
                                                     stardata,
                                                     &fail
                    );
            }
            else
            {
                /*
                 * Bisect : slow but steady
                 */
                disc->converged = FALSE;

                Discdebug(1,
                          "calc_disc_structure : need to bisect, this is bad (converged? %s)\n",
                          Yesno(disc->converged));

                disc->converged = disc_bisection_rooter(n,
                                                        parameter_types,
                                                        constraints,
                                                        stardata,
                                                        disc,
                                                        binary,
                                                        &failed);

                /*
                 * Debugging:
                 * now see if the GSL rooter works,
                 * then exit
                 */
                if(0)
                {
                    solver = GSL_MULTIROOT_FSOLVER_HYBRIDS;
                    Boolean fail = FALSE;
                    disc->converged = disc_GSL_multiroot(solver,
                                                         n,
                                                         parameter_types,
                                                         constraints,
                                                         disc,
                                                         binary,
                                                         stardata,
                                                         &fail
                        );
                    Exit_binary_c_no_stardata(0,
                                              "Exit after seeing if the GSL rooter works from a bisected solution");
                }
            }

            {
                /* debug */
                Discdebug_plain(1,
                                "%s[",
                                disc->converged==TRUE ? "converged" : "fail");
                double unknown Maybe_unused = UNUSED_FLOAT;
                for(int i=0;i<n;i++)
                {
                    Discdebug_plain(1,
                                    "%s=%g%s",
                                    Disc_parameter_string(parameter_types[i]),
                                    Disc_parameter(disc,parameter_types[i]),
                                    ((i==n-1) ? "," : "]"));
                }
            }

            Discdebug_plain(1,"\n");

            if(disc->converged == TRUE)
            {
                /*
                 * Ring detection parameter
                 */
                disc->fRing = fabs(disc->Rout/disc->Rin - 1.0);
                Discdebug(1,
                          "fRing = %g\n",
                          disc->fRing);

                if(Is_zero(disc->lifetime)&&Is_zero(t))
                    Append_logstring(LOG_DISC,
                                     "CONVERGED t=%g iguess=%d solver=%d MC=%d : ",
                                     disc->lifetime,
                                     iguess,
                                     solver,
                                     i_monte_carlo
                        );
            }

            /*
             * Try the next solution pair
             */
            npair++;
        } // loop over {guess,solver} pairs

        Safe_free(solver_guess_pairs);

        /*
         * We must have converged M, J and F, or failed
         */
        if(disc->converged == FALSE)
        {
            Discdebug(1,
                      "Fail %d : M=%g J=%g F=%g\n",
                      failed,
                      disc->M,
                      disc->J,
                      disc->F);
            *ret = DISC_CONVERGENCE_FAILED;
        }
        else
        {
            /*
             * Converged disc.
             */
            *ret = DISC_CONVERGENCE_SUCCEEDED;

            /*
             * Show disc if required
             */
            if(SHOW_CONVERGED_DISC_STRUCTURE &&
               Disc_is_disc(disc))
                disc_show_disc(stardata,disc,binary);

#ifdef CONVERGENCE_LOG
            if(convlog_fp)fclose(convlog_fp);
            printf("convergence log active : exiting here\n");
            Exit_binary_c_no_stardata(0,"Exit after convergence log\n");
#endif//CONVERGENCE_LOG
        }
    }
    return 0.0;

#ifdef OLDCODE
    /*
     * Perhaps adapt the torque to fix Rin >= Rin_min
     */
    Boolean adapt_torque =
        Disc_is_disc(disc)
        &&
        Is_not_zero(disc->Rin_min)
        &&
        disc->converged == TRUE
        &&
        Is_not_zero(disc->lifetime)
        &&
        (*Rin_min_bisect != FALSE ||
         disc->Rin < disc->Rin_min);

    Discdebug(2,
              "adapt torque? %d %d %d (%d || %d) -> %d (Rin = %g, Rin_min = %g)\n",
              Is_not_zero(disc->Rin_min),
              disc->converged == TRUE,
              Is_not_zero(disc->lifetime),
              *Rin_min_bisect != FALSE,
              disc->Rin < disc->Rin_min,
              adapt_torque,
              disc->Rin/R_SUN,
              disc->Rin_min/R_SUN
        );

    if(adapt_torque)
    {
        /*
         * Increase the torque on the disc
         * to prevent flow back into L2
         */
        //*Rin_min_bisect = RIN_MIN_BISECT;
        *Rin_min_bisect = RIN_MIN_QUADSECT;
        Discdebug(
            1,
            "Set Rin_min_bisect TRUE (Rin = %g < Rin_min = %g\n",
            disc->Rin,
            disc->Rin_min
            );

        /*
         * return convergence parameter
         * (when d=0, it is converged)
         */
        double d = disc->Rin/disc->Rin_min - 1.0;
#ifdef DISC_LOG
        disc->epstorquef = d;
#endif
        return d;
    }
    else
    {
#ifdef DISC_LOG
        disc->epstorquef = 0.0;
#endif
        return 0.0;
    }
#endif // OLDCODE
}


static int disc_GSL_multiroot(const int solver,
                              ...)
{
    /*
     * See if we can converge with the (superfast) GSL solver.
     *
     * The va_args contain pointers to the disc and binary structs
     *
     * solver is one of:
     *
     * GSL_MULTIROOT_FSOLVER_HYBRIDS
     * GSL_MULTIROOT_FSOLVER_HYBRID
     * GSL_MULTIROOT_FSOLVER_DNEWTON
     * GSL_MULTIROOT_FSOLVER_BROYDEN
     *
     * But, in general, HYBRID(S) are the most (only?) stable options.
     */
    int i;
    va_list args,args_main;
    va_start(args_main,solver);

    /* get parameter_types and required structs from args */
    va_copy(args,args_main);
    Map_varg(const int Maybe_unused, n,args);
    Map_varg(const disc_parameter * Maybe_unused,parameter_types,args);
    Map_varg(const disc_constraint * Maybe_unused, constraints,args);
    Map_varg(struct disc_t *,disc,args);
    Map_varg(const struct binary_system_t *,binary,args);
    Map_varg(struct stardata_t *,stardata,args);
    Map_varg(int * Maybe_unused,fail,args);
    va_end(args);

    /* reuse args */
    va_copy(args,args_main);

    Discdebug(2,
              "************************************************************\ntrying GSL multiroot solver %d (%s)\nDISC = %p, BINARY = %p\nTvisc0 = %g, Rin = %g, Rout = %g, torqueF = %g\n",
              solver,
              GSL_Multiroot_fsolver_string(solver),
              (void*)disc,
              (void*)binary,
              disc->Tvisc0,
              disc->Rin,
              disc->Rout,
              disc->torqueF);

    /*
     * Allocate and set up the solver routine(s)
     */
    const gsl_multiroot_fsolver_type * T =
        GSL_Multiroot_fsolver(solver);

    /*
     * Check the solver is valid
     */
#ifdef ALLOC_CHECKS
    if(T == NULL)
    {
        Exit_binary_c(BINARY_C_ALGORITHM_BRANCH_FAILURE,
                      "Unknown GSL multiroot solver %d (function pointer %p)",
                      solver,
                      Cast_function_pointer(T));
    }
#endif // ALLOC_CHECKS

    gsl_multiroot_fsolver * s
        = GSL_multiroot_fsolver_ALLOC (T, (size_t) n);

    Discdebug(2,"Allocated s = %p s->x = %p\n",
              (void*)s,
              (void*)s->x);

#ifdef ALLOC_CHECKS
    if(s == NULL)
    {
        Exit_binary_c(BINARY_C_ALLOC_FAILED,
                      "Failed to allocated memory for GSL multiroot fsolver\n");
    }
#endif//ALLOC_CHECKS

    /*
     * Set up the test function
     */
    gsl_multiroot_function * F = Malloc(sizeof(gsl_multiroot_function));
    F->f = &GSL_multiroot_testfunc;

    /*
     * Set the number of dimensions based on whether
     * we fix Rin or Rout or both, and whether Rin or Rout
     * are growing or shrinking.
     *
     * Or, perhaps the Jacobian is well behaved when it
     * is constant everywhere? hmm. maybe. maybe not.
     */

    F->n = (size_t) n;
    F->params = &args;

    /*
     * Set up the initial guess vector
     */
    gsl_vector * guess_vector = New_GSL_vector(n);
    double unknown = 0.0;
    for(i=0;i<n;i++)
    {
        const double x = (double)Disc_parameter(disc,parameter_types[i]);
        gsl_vector_set(
            guess_vector,
            (size_t)i,
#ifdef DISC_NSECTION_USELOG
            log(x)
#else
            x
#endif//DISC_NSECTION_USELOG
            );
    }

    /*
     * set up the solver function
     */
    int iter = 0;
    Discdebug(2,"\n\nSetting up multiroot fsolver\n");
    int status = gsl_multiroot_fsolver_set(s,
                                           F,
                                           guess_vector);
    Discdebug(2,"Solver is set up, status %d s=%p s->x=%p, F=%p\n\n\n",
              status,
              (void*)s,
              (void*)s->x,
              Cast_function_pointer(F));

    if(status != 0)
    {
        /*
         * Some kind of error (e.g. singularity).
         * Simply return a non-converged disc.
         */
        Discdebug(2,
                  "GSL error : status = %d = %s\n",
                  status,
                  gsl_strerror(status)
            );
    }
    else
    {
        /*
         * Solve to within DISC_TOLERANCE
         */
        double tol = DISC_TOLERANCE;

        status =
            gsl_multiroot_test_residual (s->x, tol);

        if(status != GSL_CONTINUE)
        {
            /*
             * Immediately converged
             */
            gsl_vector_memcpy(s->f,s->x);
        }
        else
        {
            /*
             * Further convergence required
             */
            status = GSL_CONTINUE;

            do
            {
                iter++;

                Discdebug(2,
                          "Call gsl_multiroot_fsolver_iterate n=%d s=%p s->x=%p s->f=%p : ",
                          n,
                          (void*)s,
                          (void*)s->x,
                          Cast_function_pointer(s->f));
                for(i=0;i<n;i++)
                {
                    Discdebug_plain(2,
                                    "%d = %u = %s = %g ",
                                    i,
                                    constraints[i],
                                    Disc_parameter_string(constraints[i]),
                                    gsl_vector_get(s->x,i));
                }

                Discdebug_plain(2,
                                ": disc->Rin = %g disc->Rout = %g\n",
                                disc->Rin,
                                disc->Rout
                    );

                status = gsl_multiroot_fsolver_iterate(s);

                /*
                disc_monotonic_check(stardata,
                                     disc,
                                     binary,
                                     n);
                */

                /*
                 * If the guess is NaN then something
                 * has gone very wrong and we should bail as
                 * soon as possible.
                 */
                for(i=0;i<n;i++)
                {
                    if(unlikely(isnan(gsl_vector_get(s->x,i))))
                    {
                        status = 1;
                        break;
                    }
                }

                /* check for problems */
                Discdebug_plain(2,
                                "gsl_multiroot_fsolver_iterate status %d\n",
                                status);

                if(status != 0)
                {
                    Discdebug(1,
                              "GSL error : status = %d = %s, iter = %d\n",
                              status,
                              gsl_strerror(status),
                              iter);
                    break;
                }

                Discdebug(2,
                          "iter = % 4d/% 4d, status %d : parameter_types ",
                          iter,
                          DISC_ROOT_FINDER_MAX_ATTEMPTS,
                          status);
                for(i=0;i<n;i++)
                {
                    Discdebug_plain(2,
                                    "%s = %g ",
                                    Disc_parameter_string(parameter_types[i]),
                                    gsl_vector_get(s->x,i));
                }
                Discdebug_plain(2,": residuals ");
                for(i=0;i<n;i++)
                {
                    Discdebug_plain(2,
                              "%s = %g ",
                              Disc_constraint_string(constraints[i]),
                              gsl_vector_get(s->f,i));
                }
                Discdebug(2,"\n");

                status =
                    gsl_multiroot_test_residual (s->f, tol);

            }while(status == GSL_CONTINUE &&
                   iter < DISC_ROOT_FINDER_MAX_ATTEMPTS);

            Discdebug(2,
                      "status = %d = %s\niter = %d\n",
                      status,
                      gsl_strerror(status),
                      iter);
        }
    }
    /*
     * Free memory, end va_args
     */
    Safe_free(F);
    Safe_free_GSL_multiroot_fsolver(s);
    Safe_free_GSL_vector(guess_vector);
    va_end(args);
    va_end(args_main);

    if(status == GSL_SUCCESS && iter < DISC_ROOT_FINDER_MAX_ATTEMPTS)
    {
        /* converged */
        Discdebug(2,"GSL return TRUE (converged)\n");
        return TRUE;
    }
    else
    {
        /* failed to converge */
        Discdebug(2,"GSL return FALSE (not converged)\n");
        return FALSE;
    }
}

static int GSL_multiroot_testfunc(const gsl_vector * x,
                                  void * p,
                                  gsl_vector * f)
{
    /*
     * the parameter values are in GSL vector x,
     * and can be accessed as doubles from x->data.
     *
     * The residuals are set in f->data.
     */
    int ret = GSL_SUCCESS;
    const int n = x->size;

    /*
     * Get the data from GSL into a C-array for disc_nsector
     */
    double * parameter_values = Calloc(1,sizeof(double)*n);

    for(int i=0;i<n;i++)
    {
        parameter_values[i] =
            gsl_vector_get(x,i);
#ifdef DISC_NSECTION_USELOG
        parameter_values[i] = exp(parameter_values[i]);
#endif
    }
    double * residuals = Calloc(n,sizeof(double));

    disc_nsector(n,
                 parameter_values,
                 residuals,
                 p);

    for(int i=0;i<n;i++)
    {
        gsl_vector_set(f,i,residuals[i]);
    }

    /*
     * NaNs cannot be right : return a deliberately failed solution
     */
#ifdef NANCHECKS
    Boolean nan = FALSE;
    for(int i=0;i<n;i++)
    {
        if(unlikely(isnan(residuals[i])))
        {
            nan=TRUE;
            break;
        }
    }
    if(unlikely(nan == TRUE))
    {
        for(int i=0;i<n;i++)
        {
            gsl_vector_set(f,i,-1.0);
        }
    }
#endif // NANCHECKS

    Safe_free(parameter_values);
    Safe_free(residuals);
    return ret;
}


#endif // DISCS





#ifdef OLDCODE
static int quadGSL_multiroot_testfunc(const gsl_vector * x,
                                      void * p,
                                      gsl_vector * f)
{
    /*
     * get the parameter values
     */
    double Tvisc0,Rin,Rout,torquef;
    Get_GSL_4vector(x,Tvisc0,Rin,Rout,torquef);

    /*
     * Half the parameter space should fail:
     * in this case, invert
     */
    if(Rin > Rout)
    {
        double temp = Rin;
        Rin = Rout;
        Rout = temp;
    }

    Discdebug(2,
              "quadGSL_multiroot_testfunc in p=%p f=%p x=%p x[]=Tvisc0=%30.20e Rin=%30.20e Rout=%30.20e torquef=%302.0e\n",
              (void*)p,
              Cast_function_pointer(f),
              x,
              Tvisc0,
              Rin,
              Rout,
              torquef
        );

    double xx[4];
    int ret;

/*
 * Exclude invalid paramter space and
 * perform NaN checks
 */
    if(Rin > Rout
       || Tvisc0 < 0.0
       || Rout < 0.0
       || Rin < 0.0
       || torquef < -TINY
#ifdef NANCHECKS
       || isnan(Tvisc0)
       || isnan(Rin)
       || isnan(Rout)
       || isnan(torquef)
#endif // NANCHECKS
        )
    {
        /*
         * Cannot be right : return a deliberately failed solution
         * that leads us back to the true parameter space
         */
        xx[0] = Tvisc0 > 0.0 ? -1.0 : Tvisc0;
        xx[1] = Rin > 0.0 ? -1.0 : Rin;
        xx[2] = Rout > 0.0 ? -1.0 : Rout;
        xx[3] = torquef > 0.0 ? -1.0 : torquef;

        Discdebug(2,"Trying RIN (%g from %g) > ROUT (%g from %g) : Tvisc0 = %g from %g : torquef = %g from %g : fail\n",
                  xx[1],Rin,
                  xx[2],Rout,
                  xx[0],Tvisc0,
                  xx[3],torquef
            );

        /*
         * Return success because we treat this as if it
         * worked with values that are clearly wrong.
         */
        ret = GSL_SUCCESS;
    }
    else
    {
        /*
         * Call the trisector function to find residuals
         */
        disc_quadsector(Tvisc0,
                        Rin,
                        Rout,
                        torquef,
                        xx,
                        p);

        ret = GSL_SUCCESS;
    }

/*
 * NaNs cannot be right : return a deliberately failed solution
 */
#ifdef NANCHECKS
    if(isnan(xx[0]) ||
       isnan(xx[1]) ||
       isnan(xx[2]) ||
       isnan(xx[3]))
    {
        xx[0] = xx[1] = xx[2] = xx[3] = -1.0;
    }
#endif // NANCHECKS

/* set the solution vector and return */
    Set_GSL_4vector_array(f,xx);

    return ret;
}
#endif //OLDCODE
