#pragma once
#ifndef DISC_THERMAL_ZONES_H
#define DISC_THERMAL_ZONES_H

/*
 * Minimum width of a thermal zone (cm)
 * Default 1cm.
 */
#define DISC_MINIMUM_THERMAL_ZONE_WIDTH 1.0

/************************************************************
 * Function macros
 ************************************************************/

/*
 * Given a radius, return the zone number
 */
//#define Disc_zone_n(R,D) ((D)->n_thermal_zones==1 ? 0 : (R) < (D)->thermal_zones[1].rstart ? 0 : 1)

#if defined MEMOIZE && defined DISCS_MEMOIZE
#define Disc_zone_n(R,D) (disc_zone_n_memoize((R),(D)))
#else//MEMOIZE
#define Disc_zone_n(R,D) (disc_zone_n((R),(D)))
#endif//MEMOIZE && DISCS_MEMOIZE

#define Disc_nearest_zone_n(R,D) (disc_nearest_zone_n((R),(D)))

/*
 * Disc's inner and outer zone numbers
 */
#define Disc_inner_zone_n(D) (0)
#define Disc_outer_zone_n(D) ((D)->n_thermal_zones>0 ? ((D)->n_thermal_zones-1) : 0)

/*
 * Given a radius, return the zone type
 */
#define Disc_zone_type(R,D) ((D)->thermal_zones[Disc_zone_n((R),(D))].type)

/*
 * Given a radius, return (a pointer to) the current zone structure
 */
#define Disc_zone(R,D) (&((D)->thermal_zones[Disc_zone_n((R),(D))]))
#define Disc_nearest_zone(R,D) (&((D)->thermal_zones[Disc_nearest_zone_n((R),(D))]))

/*
 * Pointers to the inner and outer zone, respectively
 */
#define Disc_inner_zone(D) (&((D)->thermal_zones[Disc_inner_zone_n(D)]))
#define Disc_outer_zone(D) (&((D)->thermal_zones[Disc_outer_zone_n(D)]))

/*
 * Pointer to the previous and next zone,
 * or NULL if these do not exist.
 */
#define Disc_previous_zone(D,Z) (               \
        (Z)->type==0 ? NULL                     \
        : &((D)->thermal_zones[(Z)->type-1]))
#define Disc_next_zone(D,Z) (                   \
        (Z)->type==DISCS_MAX_N_ZONES ? NULL     \
            : &((D)->thermal_zones[(Z)->type+1]))

#define Disczoneloop(D,I) for(i=0;i<(D)->n_thermal_zones;i++)

/*
 * Zone types, listed by innermost to outermost
 */
#define DISC_ZONE_TYPE_VISCOUS 0
#define DISC_ZONE_TYPE_RADIATIVE_INNER 1
#define DISC_ZONE_TYPE_RADIATIVE_OUTER 2
#define DISC_ZONE_TYPE_UNDEFINED -1

#define Disc_valid_zone_type_z(Z)               \
    (                                           \
        ((Z)->valid == TRUE) ?                  \
        ((Z)->type) :                           \
        DISC_ZONE_TYPE_UNDEFINED                \
        )

#define Disc_valid_zone_type(DISC,N)                            \
    (                                                           \
        Disc_valid_zone_type_z(&(DISC)->thermal_zones[(N)])     \
        )

#define Disc_zone_type_string(Z)                                \
    (                                                           \
        (Z->type)==DISC_ZONE_TYPE_VISCOUS ? "Viscous" :         \
        (Z->type)==DISC_ZONE_TYPE_RADIATIVE_INNER ? "Rad in " : \
        (Z->type)==DISC_ZONE_TYPE_RADIATIVE_OUTER ? "Rad out" : \
        "undefined"                                             \
        )

#define Disc_zone_valid_string(Z)               \
    ((Z)->valid ? "  valid" : "invalid")

/*
 * Temperature power law definitions
 * (prefactors and exponents)
 */

// a
#define Prefactor_viscous_fraction                                      \
    (                                                                   \
        27.0*disc->kappa*disc->alpha*disc->gamma*                       \
        BOLTZMANN_CONSTANT*sqrt(GRAVITATIONAL_CONSTANT*binary->mtot)/   \
        (64.0*STEFAN_BOLTZMANN_CONSTANT*disc->mu)                       \
        )
#define Prefactor_viscous                                               \
    (                                                                   \
        pow(Prefactor_viscous_fraction*Pow2(disc->sigma0),1.0/5.0)      \
        )

#define Exponent_viscous (-11.0/10.0)

// b
#define Prefactor_radiative_out                                         \
    (                                                                   \
        pow(                                                            \
            1.0/7.0 * Pow2(binary->Rstar) *                             \
            (binary->flux *                                             \
             (1.0 - disc->albedo)/                                      \
             STEFAN_BOLTZMANN_CONSTANT)                                 \
            *sqrt(disc->gamma*BOLTZMANN_CONSTANT/                       \
                  (GRAVITATIONAL_CONSTANT*binary->mtot*disc->mu)),      \
            2.0/7.0)                                                    \
        )
#define Exponent_radiative_out (-3.0/7.0)

// c
#define Prefactor_massloss                                      \
    (                                                           \
    Pow1d4(                                                     \
        GRAVITATIONAL_CONSTANT*mdot*binary->mtot/               \
        (2.0*PI*binary->separation*STEFAN_BOLTZMANN_CONSTANT)   \
        )

#define Exponent_massloss (-1.0/2.0)

// d
#define Prefactor_radiative_in                              \
    (                                                       \
        Pow1d4(                                             \
            2.0 * Pow3(binary->Rstar) * binary->flux *      \
            (1.0 - disc->albedo)/   \
            (3.0*PI*STEFAN_BOLTZMANN_CONSTANT)              \
            )                                               \
        )
#define Exponent_radiative_in (-3.0/4.0)




/*
 * Copy thermal zone from source Z1 to destination Z2
 */
#define Copy_zone(Z1,Z2) memcpy((Z2),(Z1),sizeof(struct disc_thermal_zone_t));

/*
 * Copy thermal zone N1 to N2 in disc D
 */

#define Copy_zone_n(D,N1,N2)                    \
    Copy_zone(&((D)->thermal_zones[(N1)]),      \
              &((D)->thermal_zones[(N2)]));


#define Disc_zone_status(A)   (                                         \
        (A)==DISC_ZONES_FAILED ? "Failed (generic)" :                   \
            (A)==DISC_ZONES_OK ? "OK" :                                 \
            (A)==DISC_ZONES_NEW_ZONE_LIST_RIN_EQUALS_ROUT ? "Rin==Rout" : \
            (A)==DISC_ZONES_NO_ZONES ? "No zones are present" :         \
            (A)==DISC_ZONES_RIN_EXCEEDS_ROUT ? "Rin > Rout" :           \
            "Unknown")




#endif // DISC_THERMAL_ZONES_H
