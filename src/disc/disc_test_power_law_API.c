#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef DISCS


#ifdef TEST_POWER_LAW_API
static void disc_test_power_law_API(void)
{

    struct power_law_t P,Q;

    /* constant power law between 0.0 and 1.0 */
    double val = 1.0;
    printf("Make constant power law, takes on value %g between 0 and 1\n",val);

    new_power_law(&P,
                  val,
                  0.0,
                  0.0,
                  1.0);
    printf("It has R0=%g R1=%g A0=%g A1=%g exponent=%g\n",P.R0,P.R1,P.A0,P.A1,P.exponent);

    /* test integration */
    double I = power_law_zone_integral(&P);
    printf("Its integral is %g\n",I);
    I = power_law_zone_integral_region(&P,0.0,0.5);
    printf("Its integral between 0 and 0.5 is %g\n",I);
    I = power_law_zone_integral_region(&P,0.5,1.0);
    printf("Its integral between 0.5 and 1.0 is %g\n",I);

    printf("************************************************************\n");

    printf("Make power law with exponent 1 between r=1 and r=2 with value 3 at r=1.0\n");
    new_power_law(&P,
                  3.0,
                  1.0,
                  1.0,
                  2.0);
    printf("It has R0=%g R1=%g A0=%g A1=%g exponent=%g\n",P.R0,P.R1,P.A0,P.A1,P.exponent);
    I = power_law_zone_integral(&P);
    printf("Its integral is %g (should be 4.5)\n",I);
    I = power_law_zone_integral_region(&P,1.0,1.5);
    printf("Its integral between 1.0 and 1.5 is %g\n",I);
    I = power_law_zone_integral_region(&P,1.5,2.0);
    printf("Its integral between 1.5 and 2.0 is %g\n",I);

    printf("************************************************************\n");

    printf("Make related power law with exponent (1*a-1) i.e. const which is 10 at r=1\n");
    new_related_power_law(&P, &Q,
                          10.0,
                          -1.0,
                          1.0);
    printf("It has R0=%g R1=%g A0=%g A1=%g exponent=%g\n",Q.R0,Q.R1,Q.A0,Q.A1,Q.exponent);

    printf("************************************************************\n");

    printf("Make related power law with exponent (-2*a-2) which is 10 at r=1\n");
    new_related_power_law(&P, &Q,
                          10.0,
                          -2.0,
                          -2.0);
    printf("It has R0=%g R1=%g A0=%g A1=%g exponent=%g (should be -2*2-2=-4)\n",Q.R0,Q.R1,Q.A0,Q.A1,Q.exponent);
    printf("At R=1 is %g\n",power_law(&Q,1));
    printf("At R=2 is %g\n",power_law(&Q,2));
    printf("************************************************************\n");

    printf("Make power law with exponent 2 between r=1e5 and r=1e10 with value 1e12 at r=1e5\n");
    new_power_law(&P,
                  1e12,
                  2.0,
                  1e5,
                  1e10);
    printf("It has R0=%g R1=%g A0=%g A1=%g exponent=%g\n",P.R0,P.R1,P.A0,P.A1,P.exponent);
    I = power_law_zone_integral(&P);
    printf("Its integral is %g\n",I);
    I = power_law_zone_integral_region(&P,1e5,1e7);
    printf("Its integral between 1e5 and 1e7 is %g\n",I);
    I = power_law_zone_integral_region(&P,1e7,1e10);
    printf("Its integral between 1e7 and 1e10 is %g\n",I);

    Exit_binary_c_no_stardata(2,"API TEST END");
}
#endif // TEST_POWER_LAW_API



#endif // DISCS
