#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef DISCS
void disc_setup_zones(struct stardata_t * const stardata,
                      struct disc_t * const disc,
                      const struct binary_system_t * const binary)
{
    /*
     * set the zone radii,
     * derive the various power laws in the disc,
     * perhaps check them for consistency.
     */
    if(disc_determine_zone_radii(stardata,disc) == TRUE)
    {
        disc_make_derived_powerlaws(disc,binary);

#ifdef POWER_LAW_SANITY_CHECKS
        check_power_laws(disc,FALSE);
#endif
    }
}
#endif // DISCS
