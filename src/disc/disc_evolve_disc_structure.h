#pragma once
#ifndef DISC_EVOLVE_DISC_STRUCTURE_H
#define DISC_EVOLVE_DISC_STRUCTURE_H

#if defined DISC_SHOW_TEST_EXPRESSIONS &&       \
    DISC_SHOW_TEST_EXPRESSIONS==1

static void show_test_expressions(struct binary_system_t * binary,
                                  struct disc_t * disc);

#endif // DISC_SHOW_TEST_EXPRESSIONS

#endif //DISC_EVOLVE_DISC_STRUCTURE_H
