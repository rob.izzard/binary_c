#include "../binary_c.h"

No_empty_translation_unit_warning;


#ifdef DISCS
char * disc_failure_mode(struct stardata_t * const stardata,
                         struct disc_t * const disc,
                         const struct binary_system_t * const binary,
                         const int status)
{
    /*
     * Check for disc failure mode.
     *
     * On failure, returns a string explaining why. This string
     * needs to be freed.
     *
     * On success, returns NULL.
     */


    /*
     * Check if the disc is a narrow ring.
     * In this case, stop.
     */
    Boolean disc_is_ring =
        disc->Rout - disc->Rin < DISC_MINIMUM_WIDTH ?
        TRUE : FALSE;

    /*
     * Special case : flux is negative,
     * disc should be evaporated
     */
    const Boolean flux_negative =
        status == DISC_CONVERGENCE_FLUX_NEGATIVE ?
        TRUE : FALSE;

    /*
     * Reasons for failure...
     */
    const double Ldisc = disc_total_luminosity(disc);
    const Boolean too_dim =
        (stardata->preferences->cbdisc_minimum_luminosity >-TINY ?
         (Ldisc < L_SUN * stardata->preferences->cbdisc_minimum_luminosity) :
         (Ldisc/binary->L < stardata->preferences->cbdisc_minimum_luminosity));

    const Boolean too_low_mass =
        disc->M < stardata->preferences->cbdisc_minimum_mass * M_SUN;

    const Boolean too_ringlike =
        Is_not_zero(disc->fRing) &&
        (disc->fRing < stardata->preferences->cbdisc_minimum_fRing);

    const Boolean too_old =
        disc->lifetime > DISC_MAX_LIFETIME ||
        (disc->type == DISC_CIRCUMBINARY &&
         Is_not_zero(stardata->preferences->cbdisc_max_lifetime) &&
         disc->lifetime > YEAR_LENGTH_IN_SECONDS * stardata->preferences->cbdisc_max_lifetime);

    /*
     * Hence failure string: can be multiple
     * reasons concurrently.
     */
    char * reason;
    if(too_dim ||
       too_low_mass ||
       flux_negative ||
       too_ringlike ||
       disc_is_ring ||
       too_old)
    {
        if(asprintf(&reason,
                    "%s%s%s%s%s%s : disc->M=%6.3e Msun (%6.3e), disc->J=%6.3e (%6.3e), disc->F=%6.3e",
                    (too_low_mass ?  "too low mass" : ""), // 12
                    (too_dim ?       " too dim" : ""), // + 8 = 20
                    (disc_is_ring ?  " is ring" : ""), // + 8 = 28
                    (flux_negative ? " flux negative" : ""), // + 14 = 42
                    (too_ringlike ?  " too ringlike" : ""), // + 13 = 55
                    (too_old ?       " too old " : ""), // + 8 = 63
                    disc->M/M_SUN, //
                    disc_total_mass(disc)/M_SUN,
                    disc->J,
                    disc_total_angular_momentum(disc,binary),
                    disc->F
               )<0)
        {
            reason = NULL;
        }
    }
    else
    {
        reason = NULL;
    }
    return reason;
}
#endif//DISCS
