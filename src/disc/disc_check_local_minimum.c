#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef DISCS


Boolean check_local_minimum(struct stardata_t * stardata,
                            struct disc_t * disc,
                            const struct binary_system_t * binary)
{
    /*
     * Check if the disc is at a local minimum in parameter space
     */

    /*
     * Step size: assume 10 * tolerance, although this
     * is a bit rough.
     */
    double d = DISC_TOLERANCE * 100.0;

    /*
     * Make a copy of the input disc
     */
    struct disc_t * testdisc = New_disc;


    /*
     * epsilons
     */

#define Perturb(V,N)                                                    \
    double d##V = d * disc->V;                                          \
    double xx##V##low[3],xx##V##mid[3],xx##V##hi[3];                    \
                                                                        \
    Copy_disc(disc,testdisc);                                           \
    testdisc->V = disc->V - d##V;                                       \
    disc_trisector2(stardata,testdisc,binary,xx##V##low);               \
    Printf("LOCAL %s LOW xx##V %g %g %g\n",                             \
           Stringify(V),                                                \
           xx##V##low[0],                                               \
           xx##V##low[1],                                               \
           xx##V##low[2]);                                              \
                                                                        \
    Copy_disc(disc,testdisc);                                           \
    testdisc->V = disc->V;                                              \
    disc_trisector2(stardata,testdisc,binary,xx##V##mid);               \
    Printf("LOCAL %s MID xx##V %g %g %g\n",                             \
           Stringify(V),                                                \
           xx##V##mid[0],                                               \
           xx##V##mid[1],                                               \
           xx##V##mid[2]);                                              \
                                                                        \
    Copy_disc(disc,testdisc);                                           \
    testdisc->V = disc->V + d##V;                                       \
    disc_trisector2(stardata,testdisc,binary,xx##V##hi);                \
    Printf("LOCAL %s HIGH xx##V %g %g %g\n",                            \
           Stringify(V),                                                \
           xx##V##hi[0],                                                \
           xx##V##hi[1],                                                \
           xx##V##hi[2]);                                               \
                                                                        \
    if(fabs(xx##V##low[N]) > fabs(xx##V##mid[N]) &&                     \
       fabs(xx##V##hi[N]) > fabs(xx##V##mid[N]))                        \
    {                                                                   \
        Printf("LOCALTEST OK\n");                                       \
    }                                                                   \
    else                                                                \
    {                                                                   \
        Printf("LOCALTEST FAIL\n");                                     \
        if(0)Exit_binary_c(2,"pants");                                  \
    }

    /*
     * Perturb variables
     */

    Perturb(Tvisc0,0);
    Perturb(Rin,1);
    Perturb(Rout,2);

    Safe_free(testdisc);

    return TRUE;
}
#endif // DISCS
