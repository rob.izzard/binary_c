#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef DISCS
void evolve_discs(struct stardata_t * const stardata)
{
    /*
     * Evolve discs around the stars and around the binary
     *
     * Also detect when the system becomes single, and hence circumbinary
     * discs are evaporated.
     */
    if(stardata->model.dtm > TINY)
    {
        int i;
        const double dtsecs = stardata->model.dtm * 1e6 * YEAR_LENGTH_IN_SECONDS;


        /*
         * Circumstellar discs
         */
        // TODO

        /*
         * Circumbinary discs
         */
        if(stardata->model.sgl == TRUE)
        {
            disc_evaporate_cbdiscs(stardata);
        }
        else
        {
            if(stardata->common.ndiscs > 0)
            {
                for(i=0; i<stardata->common.ndiscs; i++)
                {
                    struct disc_t * d = & stardata->common.discs[i];
                    Dprint("At t=%g DISC %d has delete count %d and mass %g\n",
                           stardata->model.time,
                           i,
                           d->delete_count,
                           d->M);
                    if(d->delete_count == 0)
                    {
                         evolve_disc(stardata,d,dtsecs);
                    }
                }
            }
        }

        /*
         * Remove evaporated circumbinary discs
         * or discs that are about to be evaporated.
         */
        if(stardata->common.ndiscs > 0)
        {
            for(i=0; i<stardata->common.ndiscs; i++)
            {
                struct disc_t * d = & stardata->common.discs[i];
                if(Is_really_zero(d->M))
                {
                    /*
                     * But only remove if the delete count is 2 :
                     * this allows a line of logging before the disc
                     * disappears.
                     */
                    if(++(d->delete_count) >= 2)
                    {
                        Dprint("Evaporated disc %d at t=%g, call remove_disc (ndiscs = %d, delete count %d)\n",
                               i,
                               stardata->model.time,
                               stardata->common.ndiscs,
                               d->delete_count
                            );
                        remove_disc(stardata,
                                    stardata->common.discs[i].type,
                                    stardata,
                                    i,
                                    DISC_REMOVE_EVAPORATE);
                    }
                }
            }
        }
    }
}
#endif //DISCS
