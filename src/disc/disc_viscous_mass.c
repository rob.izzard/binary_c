#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef DISCS

#define tvisc(R)                                \
    (disc_viscous_timescale((R),disc,binary))

static double bisector(const double r,
                       void * p);

double No_return disc_viscous_mass(struct disc_t * const disc,
                                   const struct binary_system_t * const binary,
                                   const double dt)
{
    /*
     * Return the mass of the disc which is viscously
     * coupled more quickly than the current timestep
     */
    int error;
    printf("\n");
    double r = generic_bisect(&error,
                              BISECT_USE_MONOCHECKS,
                              BISECTOR_DISC_VISCOUS,
                              &bisector,
                              0.5*(disc->Rout+disc->Rin),
                              disc->Rin,
                              disc->Rout,
                              DISC_TOLERANCE,
                              1000,
                              DISC_BISECT_VISCOUS_USELOG,
                              1.0,
                              disc,
                              binary,
                              dt);

    printf("\n\ndt = tvisc at r = %g (error %d %s)\n",
           r,
           error,
           gsl_strerror(error)

        );
    Exit_binary_c_no_stardata(0,"Exit after disc viscous mass");
    //return 0.0;
}


static double bisector(const double r,
                       void * p)
{
    /*
     * Bisect the half angular momentum radius
     */
    Map_GSL_params(p,args);
    Map_varg(struct disc_t *,disc,args);
    Map_varg(struct binary_system_t *,binary,args);
    Map_varg(const double,dt,args);
    va_end(args);

    double tv = tvisc(r);
    double d = tv/dt - 1.0;
    printf("\nat r = %g (%g << %g) : tv = %g cf dt = %g -> d = %g\n",
           r / R_SUN,
           disc->Rin / R_SUN,
           disc->Rout / R_SUN,
           tv / YEAR_LENGTH_IN_SECONDS,
           dt / YEAR_LENGTH_IN_SECONDS,
           d);

    return d;
}
#endif // DISCS
