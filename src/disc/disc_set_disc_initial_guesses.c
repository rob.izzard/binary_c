#include "../binary_c.h"

No_empty_translation_unit_warning;

#ifdef DISCS
#if __GNUC__ >= 11
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wvla-parameter"
#endif
#include "disc_constraints.h"

void disc_set_disc_initial_guesses(int n,/* VLA initial_guesses REQUIRES this to be type int, not const int */
                                   const disc_parameter * const parameter_types,
                                   double initial_guesses[DISC_SOLVER_NINITIAL_GUESSES+1][n],
                                   struct stardata_t * const stardata,
                                   struct disc_t * const disc,
                                   struct disc_t * const discwas,
                                   const struct binary_system_t * const binary,
                                   const double Rring
    )
{
    /*
     * Set the initial guesses for disc parameters prior to
     * attempts to solve for the disc structure.
     *
     * This is important on the first timestep, but subsequently
     * the convergence disc structure from the previous timestep
     * will be used.
     */
    Boolean have_previous_disc = Is_not_zero(discwas->Rin);
    memset(initial_guesses, 0,
           DISC_SOLVER_NINITIAL_GUESSES * n * sizeof(double));

#define Set_guess(N,TVISC,RIN,ROUT,TORQUEF)                             \
    if(0)printf("Set_guess : N=%d TVISC=%g RIN=%g ROUT=%g TORQUEF=%g\n", \
                N,TVISC,RIN,ROUT,TORQUEF);                              \
                                                                        \
    for(int i=0;i<n;i++)                                                \
    {                                                                   \
        initial_guesses[(N)][i] =                                       \
            parameter_types[i] == DISC_PARAMETER_TVISC0 ? (TVISC) :     \
            parameter_types[i] == DISC_PARAMETER_RIN ? (RIN) :          \
            parameter_types[i] == DISC_PARAMETER_ROUT ? (ROUT) :        \
            parameter_types[i] == DISC_PARAMETER_TORQUEF ? (TORQUEF) :  \
            -1.0;                                                       \
        if(0)printf("Set guess element %d : parameter type %u %s : %g\n", \
                    i,                                                  \
                    parameter_types[i],                                 \
                    Disc_parameter_string(parameter_types[i]),          \
                    initial_guesses[(N)][i]);                           \
    }

    /*
     * First guess is the previous disc, or all zero (hence ignored)
     */
    if(have_previous_disc)
    {
        Set_guess(0,
                  discwas->Tvisc0,
                  discwas->Rin,
                  discwas->Rout,
                  discwas->torqueF);
    }

    /*
     * Second guess is based on Rring and the separation
     */
    Set_guess(1,
              1e17,
              Rring * 2.0,
              Max(Rring*20.0,
                  Min(DBL_MAX*1e-30,binary->separation)*1e30),
              stardata->preferences->cbdisc_torquef
        );

    /*
     * Very large parameter space
     */
    Set_guess(2,
              1e-10,
              1.0,
              Max(Rring*10.0,
                  Min(DBL_MAX*1e-10,binary->separation)*1e10),
              stardata->preferences->cbdisc_torquef
        );

    /*
     * Third guess is a radiative disc
     */
    if(have_previous_disc == FALSE)
    {
        disc->Rin = 2.0 * binary->separation;
        disc->Rout = 1e3 * disc->Rin;
        disc->Tvisc0 = 1e15;
    }
    disc_initial_radiative_guesses(binary,
                                   disc);
    Set_guess(3,
              disc->Tvisc0,
              disc->Rin,
              disc->Rout,
              disc->torqueF);


    /*
     * Expanded ring
     */
     Set_guess(4,
               disc->Tvisc0,
               0.1 * Rring,
               100.0 * Rring,
               disc->torqueF);

}
#if __GNUC__ >= 10
#pragma GCC diagnostic pop
#endif
#endif // DISCS
