#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef DISCS

Disc_zone_counter disc_nearest_zone_n(const double radius,
                                      const struct disc_t * const disc)
{
    /*
     * Return the nearest zone to the given radius
     */
    if(radius < disc->Rin)
    {
        return 0;
    }
    else if(radius > disc->Rout)
    {
        return disc->n_thermal_zones-1;
    }
    else
    {
        return disc_zone_n(radius,disc);
    }
}

#endif
