#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef DISCS
#include "disc_constraints.h"

static double disc_nsector_wrapper(const double x,
                                   void * p);
static void disc_nsector_wrapper2(const int n,
                                  const double * parameter_value,
                                  double * residual,
                                  ...);


/*
 * Hard-coded bisection rooter
 */

int disc_bisection_rooter(const unsigned int n,
                          const disc_parameter * parameters,
                          const disc_constraint * constraints,
                          struct stardata_t * stardata,
                          struct disc_t * disc,
                          const struct binary_system_t * binary,
                          Boolean * return_failed)
{
    unsigned long int bisection_count = 0;
    unsigned long int maxcount = DISC_BISECTION_MAX_ATTEMPTS;
    Boolean converged = FALSE;
    Boolean failed = FALSE;
    int error;
    unsigned int i,j;

    //printf("BISECTOR at t = %g y\n",disc->lifetime / YEAR_LENGTH_IN_SECONDS);

    while(converged == FALSE && failed == FALSE)
    {
        /*
         * Loop over each parameter in turn
         */
        i = bisection_count % n;

        Discdebug(1,
                  "BISECT %lu param %u/%u %u %s : constraint %u %s = %g\n",
                  bisection_count,
                  i,
                  n,
                  parameters[i],
                  Disc_parameter_string(parameters[i]),
                  constraints[i],
                  Disc_constraint_string(constraints[i]),
                  Disc_constraint_value(disc,binary,constraints[i])/Disc_constraint_unit(constraints[i])
            );

        double unknown Maybe_unused = 0.0;
        /*
         * make local, 1D version of parameters
         */
        disc_constraint local_constraints[1] =
            {
                constraints[i]
            };
        disc_parameter local_parameters[1] =
            {
                parameters[i]
            };
        double local_parameter_value[1] =
            {
                Disc_parameter(disc,
                               local_parameters[0])
            };

        if(local_constraints[0] == DISC_CONSTRAINT_F_TO_DISC_F)
        {
            disc->F = disc_angular_momentum_flux(disc->Rout,disc,binary);
        }

        int bisect_failed = 0;
        Discdebug(1,"Call generic_bisect : %g < %g < %g\n",
                  Disc_parameter_min(disc,
                                     local_parameters[0]),
                  local_parameter_value[0],
                  Disc_parameter_max(disc,
                                     local_parameters[0])
            );
        double new_parameter_value =
            generic_bisect(&error,
                           BISECT_USE_MONOCHECKS,
                           BISECTOR_DISC_BISECTION_ROOTER,
                           &disc_nsector_wrapper,
                           local_parameter_value[0],
                           Disc_parameter_min(disc,
                                              local_parameters[0]),

                           Disc_parameter_max(disc,
                                              local_parameters[0]),
                           DISC_BISECTION_TOLERANCE,
                           DISC_BISECTION_MAX_ITERATIONS,
                           FALSE,//DISC_BISECT_BISECTION_ROOTER_USELOG,
                           1.0,

                           /* va_args */
                           1, // nn
                           local_parameters,
                           local_constraints,
                           disc,
                           binary,
                           stardata,
                           &bisect_failed
                );

        /*
         * Detect error
         */
        Discdebug(1,
                  "Bisection gave error %d \"%s\"\n",
                  error,
                  Bisect_error_string(error));

#ifdef ____DEBUG
        if(0 && disc->lifetime > 1000.0 * YEAR_LENGTH_IN_SECONDS)
        {
            /*
            printf("Want disc->M = %g have %g\n",
                   disc->M,
                   disc_total_mass(disc));
            */
            double Tvwas = disc->Tvisc0;
            double dTv = 1e-4 * disc->Tvisc0;
            struct disc_t * discwas = New_disc_from(disc);
            double Tv = 1.0819e17;
            double Tvmax = 1.09e17;
            double residual[n];
            int failure;

            while(Tv < Tvmax)
/*            for(Tv = 0.9 * Tvwas;
                Tv <= 1.1 * Tvwas;
                Tv += dTv)
*/          {
                Copy_disc(discwas,disc);
                disc->Tvisc0 = Tv;
                disc_nsector2(stardata,
                              disc,
                              binary,
                              n,
                              constraints,
                              residual,
                              &failure);
                const double eps =  (disc_total_mass(disc) - disc->M)/disc->M;
                printf("CHECKNEAR %30.15e %30.15g eps %g nzones %u zone 0 type %d Rin %g Rout %g\n",
                       disc->Tvisc0,
                       disc_total_mass(disc),
                       eps,
                       disc->n_thermal_zones,
                       disc->thermal_zones[0].type,
                       disc->Rin,
                       disc->Rout
                    );
                disc_show_thermal_zones(disc,NULL,-1);
                if(eps > 0.0)
                {
                    Flexit;
                }
                Tv += dTv;
            }

            //disc_monotonic_check(stardata,disc,binary,n);
            Flexit;
        }
#endif
        if(error != BINARY_C_BISECT_ERROR_NONE)
        {
            failed = i + 1;
            continue;
        }

        /*
         * Update the disc property
         */
        Disc_parameter(disc,local_parameters[0]) = new_parameter_value;

        /*
         * Update secondary properties
         */
        if(local_constraints[0] == DISC_CONSTRAINT_F_TO_DISC_F ||
           local_constraints[0] == DISC_CONSTRAINT_M_TO_DISC_M)
        {
            /*
             * When we update Tvisc0 we must also update disc->F
             */
            disc->F = disc_angular_momentum_flux(disc->Rout,disc,binary);
        }
        else if(local_constraints[0] == DISC_CONSTRAINT_J_TO_DISC_J)
        {
            Disc_outer_zone(disc)->rend = disc->Rout;
        }


        /*
         * Check vs maximum number of iterations
         */
        if(bisection_count > maxcount &&
           converged==FALSE)
        {
            Discdebug(1,
                      "   >>>>>>> Failed to converge in %lu attempts\n",
                      maxcount);
            failed = 7;
        }

        if(DEBUG_BISECTION_LOOP)
        {
            printf("Bisection loop %lu : converged? %d : failed %d\n",
                   bisection_count,
                   converged,
                   failed);
        }

        /*
         * Check for convergence
         */
        converged = TRUE;
        Boolean loop = TRUE;
        for(j=0;(j<n && loop==TRUE);j++)
        {
            if(DEBUG_BISECTION_LOOP)
            {
                printf("Check constraint %u : conv? %d : residual %g\n",
                       j,
                       Disc_constraint_converged(disc,binary,constraints[j]),
                       Disc_residual(disc,binary,constraints[j]));
            }
            else
            {
                Discdebug(1,
                          "Check constraint %u : %g vs %g : conv? %d : residual %g\n",
                          j,
                          Disc_constraint_constant(disc,binary,constraints[j]),
                          Disc_constraint_value(disc,binary,constraints[j]),
                          Disc_constraint_converged(disc,binary,constraints[j]),
                          Disc_residual(disc,binary,constraints[j]));
            }

            if(Disc_constraint_converged(disc,binary,constraints[j]) == FALSE)
            {
                Discdebug(1,
                          "Constraint failed : set converged = FALSE\n");
                converged = FALSE;
                loop = FALSE;
            }
        }

        Discdebug(1,
                  "Convd %lu : converged = %d, failed = %d, eps(MJF)=[%g,%g,%g] MJF=[%g,%g,%g] Tvisc0=%g Rin=%g (%g Rsun) Rout=%g (%g Rsun)\n",
                  bisection_count,
                  converged,
                  failed,
                  Disc_residual(disc,binary,DISC_CONSTRAINT_M_TO_DISC_M),
                  Disc_residual(disc,binary,DISC_CONSTRAINT_J_TO_DISC_J),
                  Disc_residual(disc,binary,DISC_CONSTRAINT_F_TO_DISC_F),
                  disc->M,
                  disc->J,
                  disc->F,
                  disc->Tvisc0,
                  disc->Rin,
                  disc->Rin/R_SUN,
                  disc->Rout,
                  disc->Rout/R_SUN
            );


        if(bisection_count > 1200)
        {
            /*
             * We're in trouble
             */
            //printf("Exit on long bisection\n");
            //disc_parameter_space(stardata,disc);
            //Flexit;
        }

        bisection_count++;
    }


    /*
     * Set return values
     */
    *return_failed = failed;

    /*
     * Set disc's convergence flag
     */
    if(converged == TRUE)
    {
        disc->converged = TRUE;

        /*
         * Check that the inner edge of the disc is not
         * inside the binary
         */
        if(disc->Rout < binary->separation)
        {
            Discdebug(1,"\n   >>> WARNING : converged disc is all inside the binary orbit (Rout = %g Rsun, a = %g Rsun)\n",
                      disc->Rout/R_SUN,
                      binary->separation/R_SUN
                );
        }
        else if(disc->Rin < binary->separation)
        {
            Discdebug(1,"\n   >>> WARNING : converged disc inner edge (at Rin=%g Rsun) is inside the binary orbit (a=%g Rsun)\n",
                      disc->Rin / R_SUN,
                      binary->separation / R_SUN
                );
        }

        /*
         * Perhaps check for a local minimum in the parameter
         * space?
         */
        if(0) check_local_minimum(stardata,disc,binary);
    }

    return converged;
}

static double disc_nsector_wrapper(const double x,
                                   void * p)
{
    /*
     * Function to be called by generic_bisect()
     */


    Map_GSL_params(p,args);
    Map_varg(const int,n,args);
    Map_varg(const disc_parameter *,local_parameters,args);
    Map_varg(const disc_constraint *,local_constraints,args);
    Map_varg(struct disc_t *,disc,args);
    Map_varg(const struct binary_system_t *,binary,args);
    Map_varg(struct stardata_t *,stardata,args);
    Map_varg(int *,failed,args);
    va_end(args);
    Discdebug(2,"in disc_nsector_wrapper\n");

    const double parameter_value[1] = { x };
    double residual[1];

    disc_nsector_wrapper2(n,
                          parameter_value,
                          residual,
                          n,
                          local_parameters,
                          local_constraints,
                          disc,
                          binary,
                          stardata,
                          failed
        );

    ((struct GSL_args_t *)p)->error = *failed;

    Discdebug(2,
              "return from disc_nsector_wrapper, failed = %d\n",
              *failed);

    return residual[0];
}

static void disc_nsector_wrapper2(const int n,
                                  const double * parameter_value,
                                  double * residual,
                                  ...)
{
    /*
     * Wrapper function
     */
    va_list args;
    va_start(args, residual);
    disc_nsector(n,
                 parameter_value,
                 residual,
                 &args);
    va_end(args);
}

#endif // DISCS
