#include "../binary_c.h"

No_empty_translation_unit_warning;

#ifdef DISCS

double disc_J_bisector(const double Rout,
                       void * p)
{
    /*
     * Bisect for J at fixed M (i.e. sigma0) and Rin (i.e. ang mom flux)
     */
    Map_GSL_params(p,args);
    Map_varg(struct stardata_t *,stardata,args);
    Map_varg(struct disc_t *,disc,args);
    Map_varg(struct binary_system_t *,binary,args);
    va_end(args);

    /*
     * We're given Rout, the outer radius of the outermost zone.
     * Tvisc0 and Rin are fixed.
     */
    disc->Rout = Rout;

    /*
     * update zone radii and power laws
     */
    int status = disc_build_disc_zones(stardata,disc,binary);
    double ret;
    if(status==DISC_ZONES_OK)
    {
        double J = disc_total_angular_momentum(disc,binary);
        if(DEBUG_ANGMOM_BISECTOR)
        {
            double M = disc_total_mass(disc);
            printf("J: Given Rin = %g, Rout = %g, Tvisc0 = %g : M=%g (want %g) J=%g (want %g)\n",
                   disc->Rin,
                   disc->Rout,
                   disc->Tvisc0,
                   M,disc->M,
                   J,disc->J
                );
        }
        ret = Bisection_result(disc->J,J);
    }
    else
    {
        ret = -1.0;
    }
    return ret;
}



#endif
