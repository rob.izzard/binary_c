#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef DISCS_CIRCUMBINARY_FROM_WIND
void disc_stellar_wind_to_cbdisc(struct stardata_t * const stardata)
{
    /*
     * Convert a fraction of the stellar wind lost from
     * the system into a circumbinary disc
     */
    if(Is_not_zero(stardata->preferences->wind_disc_mass_fraction))
    {
        double mdot_wind = 0.0;
        double jdot_wind Maybe_unused = 0.0;
        double net_mdot[NUMBER_OF_STARS] = {0};
        Foreach_star(star)
        {
            Star_number k = star->starnum;
            net_mdot[k] =
                star->derivative[DERIVATIVE_STELLAR_MASS_WIND_LOSS] +
                star->derivative[DERIVATIVE_STELLAR_MASS_WIND_GAIN];
            mdot_wind += net_mdot[k];
            jdot_wind +=
                star->derivative[DERIVATIVE_STELLAR_ANGMOM_WIND_LOSS] +
                star->derivative[DERIVATIVE_STELLAR_ANGMOM_WIND_GAIN];
        }


        if(mdot_wind < -TINY)
        {
            printf("At %20.12g mdot_wind = %g\n",
                   stardata->model.time,
                   mdot_wind);

            /*
             * Mass to be added (grams, positive number)
             */
            const double dM_wind = - mdot_wind * stardata->model.dtm * 1e6 * M_SUN;
            const double dM_disc = dM_wind * stardata->preferences->wind_disc_mass_fraction;

            /*
             * Orbital angular momentum in the wind (cgs, positive number)
             */
            const double dJ_wind = -stardata->model.derivative[DERIVATIVE_ORBIT_ANGMOM_WIND_LOSS] *
                stardata->model.dtm * 1e6 * ANGULAR_MOMENTUM_CGS;

            if(dJ_wind > TINY)
            {
                /*
                 * The ejected angular momentum is f_J * dJ_wind
                 */
                double f_J;
                printf("At t = %g add M = %g Msun to the cbdisc, dJ = %g\n",
                       stardata->model.time,
                       dM_wind / M_SUN,
                       dJ_wind);

                /* move this back into the if */
                struct coordinate_t L[NUMBER_OF_LAGRANGE_POINTS];
                double a1,a2;
                lagrange_points(stardata->star[0].mass,
                                stardata->star[1].mass,
                                stardata->common.orbit.separation*R_SUN,
                                L,
                                &a1,
                                &a2);
                const double RL1 = Pythag3_coord(L[0]);
                const double RL2 = Pythag3_coord(L[1]);
                const double RL3 = Pythag3_coord(L[2]);
                const double ROL1 = R_SUN * Roche_radius(stardata->star[0].q,
                                                   stardata->common.orbit.separation);
                const double ROL2 = R_SUN * Roche_radius(stardata->star[1].q,
                                                   stardata->common.orbit.separation);

                /* estimate rol1 from rl1 and a1 */
                const double rol_estimate1 = -a1 + RL1;
                const double rol_estimate2 = -a2 + RL2;

                printf("a=%g e=%g a1=%g a2=%g RL1=%g RL2=%g RL3=%g ROL1=%g (estimate %g, ratio %g) ROL2=%g (estimate %g, ratio %g)\n",
                       stardata->common.orbit.separation*R_SUN,
                       stardata->common.orbit.eccentricity,
                       a1,
                       a2,
                       RL1,
                       RL2,
                       RL3,
                       ROL1,
                       rol_estimate1,
                       ROL1/rol_estimate1,
                       ROL2,
                       rol_estimate2,
                       ROL2/rol_estimate2

                    );

                /*
                 * Orbital period in years
                 */
                const double orbital_period_years =
                    stardata->common.orbit.separation/AU_IN_SOLAR_RADII*
                    sqrt(stardata->common.orbit.separation/
                         (AU_IN_SOLAR_RADII*(stardata->star[0].mass + stardata->star[1].mass)));

                /*
                 * Angular frequency in radians per year
                 */
                const double orbital_angular_frequency = TWOPI /
                    orbital_period_years;

                /*
                 * And in radians per second
                 */
                const double omega_binary = orbital_angular_frequency /
                    YEAR_LENGTH_IN_SECONDS;

                if(Fequal(stardata->preferences->wind_disc_angmom_fraction,
                          (double)CBDISC_ANGMOM_FROM_WIND_L2))
                {
                    /*
                     * Take the angular momentum from L2
                     */

                    /*
                     * v = omega * R
                     *
                     * hence
                     *
                     * v = omega_orb * RL2
                     *
                     * assuming co-rotation (omega(R) = omega_orb) out to L2.
                     *
                     * Then j = m v r = dM_disc * omega_orb * RL2 * RL2.
                     *
                     * and
                     *
                     * h = v * r = omega_orb * RL2^2
                     */
                    f_J = dM_disc * omega_binary * Pow2(RL2);

                    f_J /= dJ_wind;
                }
                else if(More_or_equal(stardata->preferences->wind_disc_angmom_fraction,
                                      0.0))
                {
                    f_J = stardata->preferences->wind_disc_angmom_fraction;
                }
                else
                {
                    /*
                     * Unknown algorithm
                     */
                    f_J = 0.0;
                    Exit_binary_c(BINARY_C_ALGORITHM_BRANCH_FAILURE,
                                  "Unknown wind_disc_angmom_fraction algorithm (is %g). Can be >=0 or %g.\n",
                                  stardata->preferences->wind_disc_angmom_fraction,

                                  (double)CBDISC_ANGMOM_FROM_WIND_L2);
                }


                const double dJ_disc = dJ_wind * f_J;

                {
                    const double hcb = dJ_disc / dM_disc;
                    const double hcb2 = omega_binary * Pow2(RL2);
                    const double Rinj = Pow2(hcb) /
                        (GRAVITATIONAL_CONSTANT * M_SUN *
                         (stardata->star[0].mass + stardata->star[1].mass));
                    printf("Inject wind with ang mom = %g, h = %g (h_orb = %g), inj rad %g %s cf L2 %g %s ratio %g : sep = %g %s\n",
                           dJ_disc,
                           hcb,
                           hcb2,
                           Solar(Rinj,R),
                           Solar(RL2,R),
                           RL2/Rinj,
                           Solar(stardata->common.orbit.separation,R));
                }

                const Boolean put_in_disc =
                    Boolean_(Is_really_not_zero(dM_disc) &&
                             Is_really_not_zero(dJ_disc));

                if(put_in_disc == TRUE)
                {
                    struct disc_t * cbdisc = new_disc(stardata,
                                                      DISC_CIRCUMBINARY,
                                                      stardata,
                                                      DISC_OUTER_EDGE);
                    if(cbdisc == NULL)
                    {
                        /*
                         * Append to existing disc
                         */
                        cbdisc = &stardata->common.discs[stardata->common.ndiscs-1];
                        cbdisc->first = TRUE;
                        cbdisc->append = TRUE;
#ifdef NUCSYN
                        /*
                         * Mix new wind material with existing
                         */
                        Foreach_star(star)
                        {
                            nucsyn_dilute_shell(
                                cbdisc->M,
                                cbdisc->X,
                                Max(0.0,-net_mdot[star->starnum]),
                                star->Xenv);
                        }
#endif
                    }
                    else
                    {
                        /*
                         * New disc
                         */
                        Set_logstring(LOG_DISC,
                                      "0New CB disc: M=%g %s J=%g (%g and %g %% of wind) ",
                                      Solar(dM_disc,M),
                                      dJ_disc,
                                      stardata->preferences->wind_disc_mass_fraction,
                                      f_J);
                        cbdisc->first = TRUE;
                        cbdisc->append = FALSE;
#ifdef NUCSYN
                        /*
                         * New wind material all comes from this timestep's wind
                         */
                        nucsyn_dilute_shell_to(
                            Max(0.0,-net_mdot[0]),
                            stardata->star[0].Xenv,
                            Max(0.0,-net_mdot[1]),
                            stardata->star[1].Xenv,
                            cbdisc->X);
#endif
                        disc_initialize_disc(stardata,cbdisc,NULL);
                    }

                    /*
                     * For the donor type, choose the star with
                     * the strongest wind. Remember the net_mdot[...]
                     * are negative.
                     */
                    cbdisc->overflower_stellar_type =
                        net_mdot[0] < net_mdot[1] ?
                        stardata->star[0].stellar_type :
                        stardata->star[1].stellar_type;

                    printf("Add M=%g %s, J=%g to the cbdisc (which had M=%g %s J=%g), donor type %d\n",
                           Solar(dM_disc,M),
                           dJ_disc,
                           Solar(cbdisc->M,M),
                           cbdisc->J,
                           cbdisc->overflower_stellar_type);

                    cbdisc->M += dM_disc;
                    cbdisc->J += dJ_disc;

                    printf("DISC M = %g J = %g\n",
                           cbdisc->M,
                           cbdisc->J);
                }
            }
        }
    }
}
#endif // DISCS_CIRCUMBINARY_FROM_WIND
