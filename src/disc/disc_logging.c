#include "../binary_c.h"
No_empty_translation_unit_warning;


#if defined DISCS &&                            \
    (defined DISC_LOG_POPSYN ||                 \
     defined DISC_LOG ||                        \
     defined DISC_LOG_2D)

#ifdef __HAVE_VALGRIND__
#include "valgrind/valgrind.h"
#endif
#include "disc_constraints.h"
static void disc_log_end(struct stardata_t * const stardata,
                         struct disc_t * const disc);
static void close_disc_logs(struct stardata_t * const stardata);

/*
 * Perform logging for the disc.
 *
 * Note : the binary struct can be NULL. If this is the case,
 *        it is set up here. The alternative is to pass in a predefined
 *        binary struct, thus saving us the effort.
 */


void disc_logging(struct stardata_t * const stardata,
                  const struct binary_system_t * const binary,
                  const int logger)
{
    /*
     * Disc logging
     */
    int i;
    for(i=0; i<stardata->common.ndiscs; i++)
    {
        struct disc_t * const disc = & stardata->common.discs[i];

        if(Is_really_zero(disc->M) &&
           disc->lifetime > 0.0)
        {
            /*
             * Disc did exist, now doesn't. End its log.
             */
            if(stardata->preferences->disc_legacy_logging == TRUE)
            {
                disc_log_end(stardata,disc);
            }
        }
        else if(disc->converged == TRUE)
        {
            Boolean do_log_output = FALSE;

            /*
             * Calculate the time since the previous log
             * output for this disc, and update the last log time
             * to this time.
             */
            double tsince =
                Is_not_zero(disc->lastlogtime) ?
                (disc->lifetime - disc->lastlogtime) :
                0.0;

            double log_dt;

            if(stardata->preferences->disc_log_dt < 0.0)
            {
                /*
                 * Use an algorithm to decide how often to output
                 */
                if(ON_GIANT_BRANCH(stardata->star[0].stellar_type)||
                   ON_GIANT_BRANCH(stardata->star[1].stellar_type))
                {
                    /*
                     * When we have a giant, log everything
                     */
                    log_dt = 0.0;
                }
                else
                {
                    /*
                     * Stars are dwarfs, log every Myr
                     */
                    log_dt = 1.0 * 1e6 * YEAR_LENGTH_IN_SECONDS;
                }
            }
            else
            {
                log_dt = stardata->preferences->disc_log_dt;
            }

            /*
             * If disc_log_dt is zero, or tsince is zero, force output.
             */
            if(Is_zero(log_dt) || Is_zero(tsince))
            {
                do_log_output = TRUE;
            }
            /*
             * If disc_log_dt is nonzero, we only output every disc_log_dt.
             */
            else if(tsince > log_dt * YEAR_LENGTH_IN_SECONDS * 1e6)
            {
                do_log_output = TRUE;
            }

#if 0
            printf("at lifetime %g : do log ? %d : tsince = %g (cf log_dt %g)\n",
                   disc->lifetime/YEAR_LENGTH_IN_SECONDS,
                   do_log_output,
                   tsince/YEAR_LENGTH_IN_SECONDS,
                   log_dt * 1e6
                );
#endif
            /*
             * If we're called multiple times at the same time,
             * only output once. (you probably don't want this)
             */
            /*
              if(Is_not_zero(tsince))
              {
              do_log_output = FALSE;
              }
            */

            if(do_log_output == TRUE)
            {
                disc->lastlogtime = disc->lifetime;

                Discdebug(2,
                          "tsince = %g : called by %s, disc %d\n",
                          tsince/YEAR_LENGTH_IN_SECONDS,
                          Disc_logger(logger),
                          i);

                /*
                 * Allocate memory for the binary struct and
                 * initialize it if required
                 */
                struct binary_system_t * b;
                if(binary==NULL)
                {
                    b = Calloc(1,sizeof(struct binary_system_t));
                    disc_init_binary_structure(stardata,b,disc);
                }
                else
                {
                    b = (struct binary_system_t *) binary;
                }

#ifdef DISC_EQUATION_CHECKS
                disc_check_equations_everywhere(disc,b);
#endif // DISC_EQUATION_CHECKS

#ifdef DISC_LOG_POPSYN
                if(stardata->preferences->disc_legacy_logging == TRUE)
                {
                    if(disc->firstlog == TRUE &&
                       stardata->common.discs[i].delete_count == 0 &&
                       Disc_is_disc(disc))
                    {
                        disc->firstlog = FALSE;
                        Star_number overflower =
                            stardata->previous_stardata->star[0].radius > stardata->previous_stardata->star[0].roche_radius ?
                            (stardata->previous_stardata->star[1].radius > stardata->previous_stardata->star[1].roche_radius ? 2 : 0) : 1;
                        Printf("DISC_START %g %g %d %d %d %d %g %g %g %g %g %g %g\n",
                               stardata->model.time,
                               stardata->model.probability,
                               stardata->star[0].stellar_type,
                               stardata->star[1].stellar_type,
                               overflower,
                               disc->overflower_stellar_type,
                               disc->M / M_SUN,
                               disc->J,
                               stardata->common.zero_age.mass[0],
                               stardata->common.zero_age.mass[1],
                               stardata->common.zero_age.orbital_period[0],
                               stardata->common.zero_age.separation[0],
                               stardata->common.zero_age.eccentricity[0]
                            );
                    }

                    if(Is_not_zero(tsince) &&
                       disc->n_thermal_zones > 0 &&
                       Disc_is_disc(disc))
                    {
                        /* dtp = probabilty * time (years) */
                        const double dtp = stardata->model.probability *
                            tsince / YEAR_LENGTH_IN_SECONDS;

                        /* calculate integrated properties of the disc */
                        const double Tin = disc_inner_edge_temperature(disc);
                        const double Tout = disc_outer_edge_temperature(disc);
                        const double Ldisc = disc_total_luminosity(disc);
                        const double Mdisc = disc_total_mass(disc);
                        const double Jdisc = disc_total_angular_momentum(disc,b);
                        const double Idisc = disc_total_moment_of_inertia(disc);
                        const double Jbinary = ANGULAR_MOMENTUM_CGS * stardata->common.orbit.angular_momentum;
                        const double Mbinary = stardata->star[0].mass + stardata->star[1].mass;
                        const double P_ISM = disc->PISM;
                        const double R_ISM = disc->RISM;
                        const double Revap_in = disc->Revap_in;
                        const double min_Toomre_Q = disc_minimum_Toomre_Q(stardata,disc,b);

                        //Printf("DISC_ECC %g\n",
                        //stardata->common.orbit.eccentricity);

                        /* output */
                        Printf("DISC_EV %20.10e %20.10e %d %d %u %d %d %d %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g\n",
                               dtp, // 1
                               disc->lifetime/YEAR_LENGTH_IN_SECONDS,
                               stardata->star[0].stellar_type,
                               stardata->star[1].stellar_type,
                               disc->n_thermal_zones,
                               (disc->thermal_zones[0].valid==TRUE ? disc->thermal_zones[0].type : DISC_ZONE_TYPE_UNDEFINED),
                               (disc->thermal_zones[1].valid==TRUE ? disc->thermal_zones[1].type : DISC_ZONE_TYPE_UNDEFINED),
                               (disc->thermal_zones[2].valid==TRUE ? disc->thermal_zones[2].type : DISC_ZONE_TYPE_UNDEFINED),
                               disc->Rin/R_SUN,
                               disc->Rout/R_SUN,
                               b->separation/R_SUN, // 11
                               b->orbital_period/YEAR_LENGTH_IN_SECONDS,
                               More_or_equal(R_ISM,(1.0-1e-6)*disc->Rout) ? -1.0 : R_ISM/R_SUN,
                               Revap_in / R_SUN,
                               stardata->star[0].radius,
                               stardata->star[1].radius,
                               stardata->star[0].roche_radius,
                               stardata->star[1].roche_radius,
                               Ldisc/L_SUN,
                               Idisc,
                               Jdisc,// 21
                               Jbinary,
                               Mdisc/M_SUN, //);Printf("%g %g %g %g %g %g %g %g %g %g %g %g %g %g %g ",
                               Mbinary,
                               -disc->loss[DISC_LOSS_INNER_VISCOUS].mdot / M_SUN * YEAR_LENGTH_IN_SECONDS, // 25
                               -disc->loss[DISC_LOSS_GLOBAL].mdot / M_SUN * YEAR_LENGTH_IN_SECONDS,
                               -disc->loss[DISC_LOSS_INNER_L2_CROSSING].mdot / M_SUN * YEAR_LENGTH_IN_SECONDS,
                               -disc->loss[DISC_LOSS_ISM].mdot / M_SUN * YEAR_LENGTH_IN_SECONDS,
                               -disc->loss[DISC_LOSS_FUV].mdot / M_SUN * YEAR_LENGTH_IN_SECONDS,
                               -disc->loss[DISC_LOSS_XRAY].mdot / M_SUN * YEAR_LENGTH_IN_SECONDS,
                               stardata->common.orbit.eccentricity, // 31
                               disc->loss[DISC_LOSS_RESONANCES].edot,
                               Tin,
                               Tout,
                               disc_density(disc->Rin,disc,b),
                               disc_density(disc->Rout,disc,b),
                               disc_gravitational_pressure(disc->Rin,disc,b),
                               disc_gravitational_pressure(disc->Rout,disc,b),//);Printf("%g %g %g %g %g %g %g %g %g %g %g %g %g %g %g\n",
                               P_ISM,
                               disc_scale_height(disc->Rin,disc,b)/disc->Rin,
                               disc_scale_height(disc->Rout,disc,b)/disc->Rout, // 41
                               Max(b->Teff[0], b->Teff[1]),
                               Tin / Max(b->Teff[0],b->Teff[1]),
                               b->L / L_SUN,
                               b->LX / L_SUN, //45
                               disc_total_luminosity(disc) / b->L,
                               disc_viscous_timescale(disc->Rin, disc,b) / YEAR_LENGTH_IN_SECONDS,
                               disc_viscous_timescale(disc->Rout,disc,b) / YEAR_LENGTH_IN_SECONDS,
                               disc_total_gravitational_potential_energy(disc,b),
                               disc_total_kinetic_energy(disc,b),
                               disc->sigma0, // 51
                               disc->Tvisc0,
                               disc->Rout / disc->Rin,
                               disc->Rin / Max(1e-50,b->separation),
                               disc->Rout / Max(1e-50,b->separation),
                               min_Toomre_Q // 56
                            );
                    }

                    /*
                     * If the disc has evaporated, log its end.
                     */
                    if(Is_really_zero(disc->M) &&
                       disc->end_count==0)
                    {
                        disc_log_end(stardata,disc);
                    }
#ifdef __HAVE_VALGRIND__
                    /*
                     * Discs can greatly slow evolution, and when
                     * running with valgrind cause timeouts.
                     * Reset the timeout here.
                     */
                    if(RUNNING_ON_VALGRIND)
                    {
                        reset_binary_c_timeout();
                    }
#endif // __HAVE_VALGRIND__

                }
#endif // DISC_LOG_POPSYN


#ifdef DISC_LOG_2D
                if(stardata->preferences->disc_log2d != DISC_LOG_LEVEL_NONE &&
                   Disc_is_disc(disc))
                {
                    // tried at 1e-2, 1e-5 is more accurate but still there are issues
                    double logR,dlogR = (log(disc->Rout) - log(disc->Rin))/
                        (double)DISC_ADAM_RESOLUTION;

                    // numerically integrated mass, luminosity, angular momentum etc. in the disc
                    double Mint = 0.0;
                    double Lint = 0.0;
                    double Jint = 0.0;
                    double Iint = 0.0;
                    double KEint = 0.0;
                    double PEint = 0.0;

                    /*
                     * File log for splots
                     */
                    if(Disc_can_log2d == TRUE &&
                       Disc_is_disc(disc) &&
                       Disc_log_filepointer2d==NULL)
                    {
                        char * disc_log_filename;
                        int len = asprintf(&disc_log_filename,
                                           "%s/cbdisc.dat",
                                           stardata->preferences->disc_log_directory);

                        if(len<0)
                        {
                            Exit_binary_c(BINARY_C_XPRINTF_FAILED,
                                          "snprintf of stardata->preferences->disc_log_directory/cbdisc.dat failed (1 len=%d)\n",
                                          len);
                        }

                        Disc_log_filepointer2d =
                            binary_c_fopen(stardata,
                                           disc_log_filename,
                                           "w",
                                           DISC_LOG_2D_MAX_BYTES);

                        if(Disc_log_filepointer2d == NULL)
                        {
                            Exit_binary_c(BINARY_C_FILE_OPEN_ERROR,
                                          "disc_logging: failed to open %s/cbdisc.dat for writing\n",
                                          stardata->preferences->disc_log_directory);
                        }

                        stardata->tmpstore->disc_logfilecount2d++;
                        binary_c_fprintf(Disc_log_filepointer2d,
                                         "\"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%s\"\n",
                                         "t",
                                         "type",
                                         "R",
                                         "T",
                                         "Sigma",
                                         "M",
                                         "L",
                                         "J",
                                         "rho",
                                         "h",
                                         "H",
                                         "H/R",
                                         "cs",
                                         "P",
                                         "nu",
                                         "F",
                                         "tvisc",
                                         "Owensigmadot",
                                         "Owentimescale",
                                         "ToomreQ"
                            );
                        Safe_free(disc_log_filename);
                    }

                    for(logR = log(disc->Rin) + 0.5*dlogR; logR < log(disc->Rout); )
                    {
                        double R = exp(logR); // radius in cm

                        // dR = dlogR *R
                        double dR = dlogR * R;

                        // dA = 2 pi R dR
                        double dA = 2.0 * PI * R * dR;

                        /* temperature */
                        double T = disc_temperature(R,disc);

                        /* surface density */
                        double Sigma = disc_column_density(R,disc);

                        // Bolometric luminosity dL = 2 * sigma T^4 dA
                        Lint += 2.0 * dA * STEFAN_BOLTZMANN_CONSTANT * Pow4(T);

                        double dM = Sigma * dA;

                        // add up disc mass
                        Mint += dM;

                        // dJ = dM * h
                        Jint += dM * disc_specific_angular_momentum(R,b);

                        // dI = dM * R^2
                        double dI = dM * R * R;
                        Iint += dI;

                        // KE
                        double omega = disc_orbital_frequency(R,b);
                        KEint += 0.5 * Pow2(omega) * dI;

                        // PE
                        PEint += - GRAVITATIONAL_CONSTANT * b->mtot * dM / R;

                        binary_c_fprintf(
                            Disc_log_filepointer2d,
                            "%g %d %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g\n",
                            disc->lifetime/YEAR_LENGTH_IN_SECONDS,
                            Disc_zone_type(R,disc),
                            R/R_SUN,
                            T,
                            Sigma,
                            Mint/M_SUN,
                            Lint/L_SUN,
                            Jint,
                            disc_density(R,disc,b),
                            disc_specific_angular_momentum(R,b),
                            disc_scale_height(R,disc,b),
                            disc_scale_height(R,disc,b)/R,
                            disc_sound_speed(R,disc),
                            disc_pressure(R,disc,b),
                            disc_kinematic_viscosity(R,disc,b),
                            disc_angular_momentum_flux(R,disc,b),
                            disc_viscous_timescale(R,disc,b),
                            disc_Owen_2012_sigmadot(disc,b,R),
                            disc_Owen_2012_mass_loss_timescale(disc,b,R),
                            disc_Toomre_Q(R,disc,b)
                            );


                        logR += dlogR;
                    }

                    binary_c_fprintf(Disc_log_filepointer2d,"\n");
                    binary_c_fflush(Disc_log_filepointer2d);
                    if(0)
                    {
                        // numerical tests
                        printf("TEST M : disc->M = %g, disc_total_mass = %g, Mint = %g\nTEST L : disc_total_luminosity() = %g, Lint = %g Lsun\nTEST J :  disc->J = %g, disc_total_angular_momentum = %g, Jint = %g\nTEST I : disc_moment_of_inertia() = %g, Iint = %g\nTEST KE : disc_total_kinetic_energy = %g, KEint = %g\nTEST PE : disc_total_gravitational_potential_energy = %g, PEint = %g\n",

                               // M test
                               disc->M/M_SUN,
                               disc_total_mass(disc)/M_SUN,
                               Mint/M_SUN,
                               // L test
                               disc_total_luminosity(disc)/L_SUN,
                               Lint/L_SUN,
                               // J test
                               disc->J,
                               disc_total_angular_momentum(disc,b),
                               Jint,
                               // I test
                               disc_total_moment_of_inertia(disc),
                               Iint,
                               // disc kinetic energy
                               disc_total_kinetic_energy(disc,b),
                               KEint,
                               // disc potential energy
                               disc_total_gravitational_potential_energy(disc,b),
                               PEint
                            );
                    }
                }


#endif //DISC_LOG_2D



#ifdef DISC_LOG
                if(Disc_can_log &&
                   Disc_log_filepointer == NULL &&
                   Disc_is_disc(disc))
                {
                    if(DISC_DEBUG)
                        printf("evolve disc open logfile\n");

                    char * disc_log_filename;
                    int len = asprintf(&disc_log_filename,
                                       "%s/cbdisc.log",
                                       stardata->preferences->disc_log_directory);
                    if(len<0)
                    {
                        Safe_free(disc_log_filename);
                        Exit_binary_c(BINARY_C_XPRINTF_FAILED,
                                      "snprintf of stardata->preferences->disc_log_directory/cbdisc.log failed (2 len=%d)\n",
                                      len);
                    }

                    Disc_log_filepointer =
                        binary_c_fopen(
                            stardata,
                            disc_log_filename,
                            "w",
                            DISC_LOG_MAX_BYTES);

                    if(Disc_log_filepointer == NULL)
                    {
                        Exit_binary_c(BINARY_C_FILE_OPEN_ERROR,
                                      "disc_logging: failed to open %s/cbdisc.log for writing\n",
                                      stardata->preferences->disc_log_directory);
                    }

                    stardata->tmpstore->disc_logfilecount++;
                    binary_c_fprintf(
                        Disc_log_filepointer,
                        "\"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%s\"\n",
                        "t",
                        "st1",
                        "st2",
                        "nzones",
                        "zone0",
                        "zone1",
                        "zone2",
                        "Rin",
                        "Rout",
                        "RMhalf",
                        "RJhalf",
                        "separation",
                        "a1",
                        "a2",
                        "aL1",
                        "aL2",
                        "aL3",
                        "Revap_in",
                        "Revap_out",
                        "RISM",
                        "R0",
                        "R1",
                        "RL0",
                        "RL1",
                        "RZ0",
                        "RZ1",
                        "torquef",
                        "Ldisc",
                        "Idisc",
                        "Fdisc",
                        "Jdisc",
                        "Jbin",
                        "hdisc",
                        "Mdisc",
                        "Mbin",
                        "Mc1",
                        "Mc2",
                        "Menv1",
                        "Menv2",

                        "Mdotvisc",
                        "Mdotglobal",
                        "MdotL2",
                        "MdotISM",
                        "MdotFUV",
                        "MdotXray",
                        "Mdotinner",
                        "Mdotouter",

                        "Jdotvisc",
                        "Jdotglobal",
                        "JdotL2",
                        "JdotISM",
                        "JdotFUV",
                        "JdotXray",
                        "Jdotinner",
                        "Jdotouter",
                        "Jdottorque",
                        "hwind",

                        "ecc",
                        "eccdot",
                        "Tin",
                        "Tout",
                        "rhoin",
                        "rhoout",
                        "Sigmain",
                        "Sigmaout",
                        "Pin",
                        "Pout",
                        "PISM",
                        "H/Rin",
                        "H/Rout",
                        "T*",
                        "Tin/T*",
                        "L*",
                        "LX",
                        "Ldisc/L*",
                        "tviscin",
                        "tviscout",
                        "Epot",
                        "Ekin",
                        "sigma0",
                        "Tvisc0",
                        "epsM",
                        "epsJ",
                        "epsF",
                        "epstorquef",
                        "solver",
                        "guess",
                        "fRing",
                        "minToomreQ",
#ifdef DISC_EQUATION_CHECKS
                        "eq.dT/T%",
                        "eq.Mdot/others%",
#else
                        "","",
#endif
                        "Mc1dot",
                        "Mc2dot",
                        "M1dotwind",
                        "M2dotwind",
                        "M1dotdisc",
                        "M2dotdisc"

                        );
                    binary_c_fprintf(Disc_log_filepointer,
                                     "# alpha=%g gamma=%g kappa=%g f_{torque,init}=%g f_{c}=%g f_{visc}=%g f_{L2}=%g viscous_L2_coupling=%d : L_{min}=%g M_{min}=%g : Comenv f_{M}=%g f_{J}=%g : ",
                                     disc->alpha,
                                     disc->gamma,
                                     disc->kappa,
                                     disc->torqueF,
                                     stardata->preferences->cbdisc_mass_loss_constant_rate,
                                     stardata->preferences->cbdisc_mass_loss_inner_viscous_multiplier,
                                     stardata->preferences->cbdisc_mass_loss_inner_L2_cross_multiplier,
                                     stardata->preferences->cbdisc_viscous_L2_coupling,
                                     stardata->preferences->cbdisc_minimum_luminosity,
                                     stardata->preferences->cbdisc_minimum_mass,
                                     stardata->preferences->comenv_disc_mass_fraction,
                                     stardata->preferences->comenv_disc_angmom_fraction);
#ifdef SVN_REVISION
                    binary_c_fprintf(Disc_log_filepointer,
                                     "SVN %d",
                                     SVN_REVISION+0
                        );
#endif // SVN_REVISION
#ifdef GIT_REVISION
                    binary_c_fprintf(Disc_log_filepointer,
                                     "git %s",
                                     Stringof(GIT_REVISION)
                        );
#endif // GIT_REVISION
                    binary_c_fprintf(Disc_log_filepointer,"\n");
                    Safe_free(disc_log_filename);
                }

                if(stardata->preferences->disc_log != DISC_LOG_LEVEL_NONE &&
                   Disc_is_disc(disc))
                {
                    /* calculate integrated properties of the disc */
                    const double Tin = disc_inner_edge_temperature(disc);
                    const double Tout = disc_outer_edge_temperature(disc);
                    const double Ldisc = disc_total_luminosity(disc);
                    const double Mdisc = disc_total_mass(disc);
                    const double Jdisc = disc_total_angular_momentum(disc,b);
                    const double hdisc = Mdisc / Jdisc;
                    const double Fdisc = disc_total_angular_momentum_flux(disc,b);
                    const double Idisc = disc_total_moment_of_inertia(disc);
                    const double Jbinary = ANGULAR_MOMENTUM_CGS * stardata->common.orbit.angular_momentum;
                    const double Mbinary = stardata->star[0].mass + stardata->star[1].mass;//Msun
                    const double P_ISM = disc->PISM;
                    const double R_ISM = disc->RISM;
                    const double Revap_in = disc->Revap_in;
                    const double Revap_out = disc->Revap_out;
                    const double RMhalf = disc_half_mass_radius(stardata,disc,b);
                    const double RJhalf = disc_half_angular_momentum_radius(stardata,disc,b);
                    const double Mdotwind =
                        Disc_Mdot(DISC_LOSS_GLOBAL)+
                        Disc_Mdot(DISC_LOSS_ISM)+
                        Disc_Mdot(DISC_LOSS_FUV)+
                        Disc_Mdot(DISC_LOSS_XRAY)+
                        Disc_Mdot(DISC_LOSS_INNER_EDGE_STRIP)+
                        Disc_Mdot(DISC_LOSS_OUTER_EDGE_STRIP);

                    const double Jdotwind =
                        Disc_Jdot(DISC_LOSS_GLOBAL)+
                        Disc_Jdot(DISC_LOSS_ISM)+
                        Disc_Jdot(DISC_LOSS_FUV)+
                        Disc_Jdot(DISC_LOSS_XRAY)+
                        Disc_Jdot(DISC_LOSS_INNER_EDGE_STRIP)+
                        Disc_Jdot(DISC_LOSS_OUTER_EDGE_STRIP);

                    const double hwind = Is_really_not_zero(Mdotwind) ?
                        (Jdotwind / Mdotwind) : 0.0;

                    const double min_Toomre_Q = disc_minimum_Toomre_Q(stardata,disc,b);

                    /* zone crossing radii */
                    double * RZcross;
                    disc_zone_crossing_radii(stardata,
                                             disc,
                                             b,
                                             &RZcross);

                    /* output */
                    binary_c_filter_fprintf(
                        Disc_log_filepointer,
                        FILE_FILTER_STRIP_ARGUMENTS,
                        "t=%.20e st1=%d st2=%d nzones=%u zone0=%d zone1=%d zone2=%d Rin=%20.12g Rout=%20.12g RMhalf=%20.12g RJhalf=%20.12g a=%20.12g a1=%20.12g a2=%20.12g aL1=%20.12g aL2=%20.12g aL3=%20.12g Revapin=%20.12g Revapout=%20.12g RISM=%20.12g R0=%20.12g R1=%20.12g RL0=%20.12g RL1=%20.12g RZ0=%20.12g RZ1=%20.12g torqueF=%20.12g Ldisc=%20.12g Idisc=%20.12g Fdisc=%20.12g Jdisc=%20.12g Jbinary=%20.12g hdisc=%20.12g Mdisc=%20.12g Mbinary=%20.12g Mc0=%20.12g Mc1=%20.12g Menv1=%20.12g Menv2=%20.12g Mdotvisc=%20.12g Mdoscglobal=%20.12g MdotL2=%20.12g MdotISM=%20.12g MdotFUV=%20.12g MdotX=%20.12g Mdotin=%20.12g Mdotout=%20.12g Jdotvisc=%20.12g Jdotglobal=%20.12g JdotL2=%20.12g JdotISM=%20.12g JdotFUV=%20.12g JdotX=%20.12g Jdotin=%20.12g Jdotout=%20.12g Jdottorque=%20.12g hwind=%20.12g e=%20.12g edot=%20.12g Tin=%20.12g Tout=%20.12g densityin=%20.12g densityout=%20.12g sigmain=%20.12g sigmaout=%20.12g Pgravin=%20.12g Pgravout=%20.12g PISM=%20.12g HRin=%20.12g HRout=%20.12g Teffmaxstar=%20.12g Tratio=%20.12g Lstar=%20.12g LstarX=%20.12g Ldisc=%20.12g tviscin=%20.12g tviscout=%20.12g PEdisc=%20.12g KEdisc=%20.12g sigma0=%20.12g Tvisc0=%20.12g epsM=%20.12g epsJ=%20.12g epsF=%20.12g epstorqueF=%20.12g solver=%d guess=%d fRing=%20.12g ToomreQ=%20.12g Terrorpc=%20.12g Mdoterrorpc=%20.12g Mc0dot=%20.12g Mc1dot=%20.12g M1dotwind=%20.12g M2dotwind=%20.12g M1dotdisc=%20.12g M2dotdisc=%20.12g\n",
                        disc->lifetime/YEAR_LENGTH_IN_SECONDS,

                        // stellar types
                        stardata->star[0].stellar_type,
                        stardata->star[1].stellar_type,

                        // thermal zone information
                        disc->n_thermal_zones,
                        Disc_valid_zone_type(disc,0),
                        Disc_valid_zone_type(disc,1),
                        Disc_valid_zone_type(disc,2),

                        // radii
                        disc->Rin/R_SUN,
                        disc->Rout/R_SUN,
                        RMhalf/R_SUN,
                        RJhalf/R_SUN,
                        b->separation/R_SUN,
                        b->a1/R_SUN,
                        b->a2/R_SUN,
                        b->aL1/R_SUN,
                        b->aL2/R_SUN,
                        b->aL3/R_SUN,
                        Is_not_zero(Revap_in) ? Revap_in/R_SUN : -1.0,
                        Is_not_zero(Revap_out) ? Revap_out/R_SUN : -1.0,
                        More_or_equal(R_ISM,(1.0-1e-6)*disc->Rout) ? -1.0 : R_ISM/R_SUN,
                        stardata->star[0].radius,
                        stardata->star[1].radius,
                        stardata->star[0].roche_radius,
                        stardata->star[1].roche_radius,

                        // zone crossing radii, or negative
                        RZcross != NULL ? (RZcross[0]/R_SUN) : -1.0,
                        RZcross != NULL ? (RZcross[1]/R_SUN) : -1.0,

                        // misc.
                        disc->torqueF,
                        Ldisc/L_SUN,
                        Idisc,
                        Fdisc,
                        Jdisc,
                        Jbinary,
                        hdisc,
                        Mdisc/M_SUN,
                        Mbinary,

                        // stellar mass
                        Outermost_core_mass(&stardata->star[0]),
                        Outermost_core_mass(&stardata->star[1]),
                        envelope_mass(&stardata->star[0]),
                        envelope_mass(&stardata->star[1]),

                        // disc mass loss rates

                        Disc_Mdot(DISC_LOSS_INNER_VISCOUS),
                        Disc_Mdot(DISC_LOSS_GLOBAL),
                        Disc_Mdot(DISC_LOSS_INNER_L2_CROSSING),
                        Disc_Mdot(DISC_LOSS_ISM),
                        Disc_Mdot(DISC_LOSS_FUV),
                        Disc_Mdot(DISC_LOSS_XRAY),
                        Disc_Mdot(DISC_LOSS_INNER_EDGE_STRIP),
                        Disc_Mdot(DISC_LOSS_OUTER_EDGE_STRIP),

                        // angular momentum rates
                        Disc_Jdot(DISC_LOSS_INNER_VISCOUS),
                        Disc_Jdot(DISC_LOSS_GLOBAL),
                        Disc_Jdot(DISC_LOSS_INNER_L2_CROSSING),
                        Disc_Jdot(DISC_LOSS_ISM),
                        Disc_Jdot(DISC_LOSS_FUV),
                        Disc_Jdot(DISC_LOSS_XRAY),
                        Disc_Jdot(DISC_LOSS_INNER_EDGE_STRIP),
                        Disc_Jdot(DISC_LOSS_OUTER_EDGE_STRIP),
                        Disc_Jdot(DISC_LOSS_BINARY_TORQUE),

                        // specific angular momentum loss rate in winds
                        hwind,

                        // eccentricity
                        stardata->common.orbit.eccentricity,
                        disc->loss[DISC_LOSS_RESONANCES].edot,

                        // disc tempeatures
                        Tin,
                        Tout,

                        // densities
                        disc_density(disc->Rin,disc,b),
                        disc_density(disc->Rout,disc,b),

                        // column densities
                        disc_column_density(disc->Rin,disc),
                        disc_column_density(disc->Rout,disc),

                        // pressures
                        disc_gravitational_pressure(disc->Rin,disc,b),
                        disc_gravitational_pressure(disc->Rout,disc,b),
                        P_ISM,

                        // scale heights H/R
                        (Is_zero(disc->Rin)||Is_zero(disc->Rout)||Fequal(disc->Rin,disc->Rout)) ? 0.0 : (disc_scale_height(disc->Rin,disc,b)/disc->Rin),
                        (Is_zero(disc->Rin)||Is_zero(disc->Rout)||Fequal(disc->Rin,disc->Rout)) ? 0.0 : (disc_scale_height(disc->Rout,disc,b)/disc->Rout),

                        // effective tempeartures of the stars
                        Max(b->Teff[0],b->Teff[1]),
                        Tin / Max(b->Teff[0],b->Teff[1]),

                        // luminosities
                        b->L / L_SUN,
                        b->LX / L_SUN,
                        disc_total_luminosity(disc) / b->L,

                        // viscous timescales
                        disc_viscous_timescale(disc->Rin, disc,b) / YEAR_LENGTH_IN_SECONDS,
                        disc_viscous_timescale(disc->Rout,disc,b) / YEAR_LENGTH_IN_SECONDS,

                        // energies
                        disc_total_gravitational_potential_energy(disc,b),
                        disc_total_kinetic_energy(disc,b),

                        disc->sigma0,
                        disc->Tvisc0,

                        // solution epsilons
                        disc->epsilon[DISC_CONSTRAINT_M_TO_DISC_M],
                        disc->epsilon[DISC_CONSTRAINT_J_TO_DISC_J],
                        disc->epsilon[DISC_CONSTRAINT_F_TO_DISC_F],

                        disc->epstorquef,

                        disc->solver,
                        disc->guess,
                        disc->fRing,
                        min_Toomre_Q,
#ifdef DISC_EQUATION_CHECKS
                        disc->equations_T_error_pc,
                        disc->equations_mass_loss_term_pc,

#else
                        0.0,
                        0.0,
#endif // DISC_EQUATION_CHECKS

                        /* core growth rates */
                        stardata->star[0].derivative[DERIVATIVE_STELLAR_CO_CORE_MASS],
                        stardata->star[1].derivative[DERIVATIVE_STELLAR_CO_CORE_MASS],
                        /* stellar wind rates (fabs: >0) */
                        fabs(stardata->star[0].derivative[DERIVATIVE_STELLAR_MASS_WIND_LOSS]),
                        fabs(stardata->star[1].derivative[DERIVATIVE_STELLAR_MASS_WIND_LOSS]),
                        /* accretion rates */
                        disc->lifetime > 0.0 ? stardata->star[0].derivative[DERIVATIVE_STELLAR_MASS_CBDISC_GAIN] : 0.0,
                        stardata->star[1].derivative[DERIVATIVE_STELLAR_MASS_CBDISC_GAIN]

                        );
                    binary_c_fflush(Disc_log_filepointer);
                    Safe_free(RZcross);
                }

#endif // DISC_LOG

                if(disc->lifetime > 0.0 &&
                   Is_really_zero(disc->M) &&
                   disc->end_count==0)
                {
                    disc_log_end(stardata,disc);
                }

                if(!Disc_is_disc(disc))
                {
                    close_disc_logs(stardata);
                }

                /*
                 * Free memory if required
                 */
                if(binary==NULL)
                {
                    Safe_free(b);
                }
            }
        }

        /*
         * Force a close of the disc log
         */
        if(disc->converged == FALSE ||
           Is_really_zero(disc->M))
        {
            close_disc_logs(stardata);
        }
    }
}

static void close_disc_logs(struct stardata_t * const stardata)
{
#ifdef DISC_LOG_2D
    if(stardata->preferences->disc_log2d!= DISC_LOG_LEVEL_NONE &&
       Disc_log_filepointer2d != NULL)
    {
        binary_c_fclose(&Disc_log_filepointer2d);
        Disc_log_filepointer2d = NULL;
    }
#endif //DISC_LOG_2D
#ifdef DISC_LOG
    if(stardata->preferences->disc_log != DISC_LOG_LEVEL_NONE &&
       Disc_log_filepointer != NULL)
    {
        binary_c_fclose(&Disc_log_filepointer);
        Disc_log_filepointer = NULL;
    }
#endif //DISC_LOG
}

static void disc_log_end(struct stardata_t * const stardata,
                         struct disc_t * const disc)
{
    disc->end_count++;
    Printf("DISC_END %30.20g %g %g %g %d %d %g %g %g %g %g\n",
           stardata->model.time,
           stardata->model.probability,
           disc->lifetime/YEAR_LENGTH_IN_SECONDS,
           stardata->common.orbit.eccentricity,
           stardata->star[0].stellar_type,
           stardata->star[1].stellar_type,
           stardata->common.zero_age.mass[0],
           stardata->common.zero_age.mass[1],
           stardata->common.zero_age.orbital_period[0],
           stardata->common.zero_age.separation[0],
           stardata->common.zero_age.eccentricity[0]
        );
    //Backtrace;

}

#endif // DISCS and any DISC logging
