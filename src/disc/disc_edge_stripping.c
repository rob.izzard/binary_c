#include "../binary_c.h"

No_empty_translation_unit_warning;



#ifdef DISCS

void disc_edge_stripping(struct stardata_t * const stardata,
                         struct disc_t * const newdisc,
                         struct disc_t * const olddisc,
                         const struct binary_system_t * const binary)
{
    /*
     * Perform outer edge mass loss
     */
    if(stardata->preferences->cbdisc_outer_edge_stripping==TRUE)
    {
        disc_outer_edge_mass_loss(stardata,newdisc,olddisc,binary);

        if(Is_not_zero(olddisc->Revap_out) &&
           !Fequal(olddisc->Revap_out,newdisc->Revap_out))
        {
            if(newdisc->Revap_out < olddisc->Revap_out)
            {
                /*
                 * New material is caught up in the stripping which should
                 * be removed. This material is now gone, along with its
                 * angular momentum, and there is no adjustment to the
                 * angular momentum flux.
                 */
            }
            else
            {
                /*
                 * newdisc->Revap_out > olddisc->Revap_out
                 *
                 * The disc expands outwards to fill the gap.
                 *
                 * No mass is lost.
                 */
                newdisc->loss[DISC_LOSS_OUTER_EDGE_STRIP].mdot = 0.0;
                newdisc->loss[DISC_LOSS_OUTER_EDGE_STRIP].jdot = 0.0;
                newdisc->loss[DISC_LOSS_OUTER_EDGE_STRIP].edot = 0.0;
            }
        }
    }

    /*
     * Perform inner edge mass loss
     */
    if(stardata->preferences->cbdisc_inner_edge_stripping==TRUE)
    {
        disc_inner_edge_mass_loss(stardata,newdisc,olddisc,binary);

        if(Is_not_zero(olddisc->Revap_in)
           &&
           !Fequal(olddisc->Revap_in,newdisc->Revap_in))
        {
            if(newdisc->Revap_in > olddisc->Revap_in)
            {
                /*
                 * New material is caught up in the stripping which should
                 * be removed. This material is now gone, along with its
                 * angular momentum, and there is no adjustment to the
                 * angular momentum flux.
                 */
            }
            else
            {
                /*
                 * Revap_in moves inwards
                 *
                 * The disc can expand inwards to fill the gap,
                 * no extra mass is lost.
                 *
                 * Suppress viscous inflow which would have flowed
                 * through this inner edge, and adjust the angular momentum
                 * flux.
                 */
                newdisc->suppress_viscous_inflow = TRUE;
                newdisc->loss[DISC_LOSS_INNER_EDGE_STRIP].mdot = 0.0;
                newdisc->loss[DISC_LOSS_INNER_EDGE_STRIP].jdot = 0.0;
                newdisc->loss[DISC_LOSS_INNER_EDGE_STRIP].edot = 0.0;
            }
        }
    }
}


#endif
