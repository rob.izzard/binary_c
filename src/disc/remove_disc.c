#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef DISCS

void remove_disc(
    struct stardata_t * const stardata,
    const Disc_type type,
    void * const object, // star or stardata
    const int pos,
    const int reason
    )
{
    /*
     * Remove disc from given object (and shift all other discs
     * down one in the object's disc list).
     *
     * pos is the disc number in the list, or DISC_INNER_EDGE, which
     * sets it to zero, or DISC_OUTER_EDGE, which sets it to the final
     * disc in the list.
     *
     * Returns nothing (void).
     */
    if(type == DISC_CIRCUMSTELLAR)
    {
        struct star_t * star = object;
        if(star->ndiscs==0)
        {
            /* no disc to remove! */
            Exit_binary_c_no_stardata(
                BINARY_C_OUT_OF_RANGE,
                "No circumstellar disc to remove in remove_disc\n");
        }
        const int n = Disc_pos_to_n(star,pos);
#ifdef MEMOIZE
        memoize_free(&star->discs[n]->memo);
#endif // MEMOIZE
        Safe_free(star->discs[n]);
        int i;
        for(i=n; i<star->ndiscs;i++)
        {
            star->discs[i] = star->discs[i+1];
        }
        star->discs[star->ndiscs-1]=NULL;
        if(--star->ndiscs>0)
        {
            star->discs=Realloc(star->discs, (sizeof (struct disc_t)) * star->ndiscs);
            if(star->discs==NULL)
            {
                Exit_binary_c_no_stardata(
                    BINARY_C_MALLOC_FAILED,
                    "Memory de-allocation (Realloc) the circumstellar disc list failed");
            }
        }
        else
        {
            Safe_free(star->discs);
        }
    }
    else if(type == DISC_CIRCUMBINARY)
    {
        struct common_t * const common = &stardata->common;
        if(common->ndiscs==0)
        {
            /* no disc to remove! */
            Exit_binary_c(
                BINARY_C_OUT_OF_RANGE,
                "No circumbinary disc to remove in remove_disc at position %d\n",
                pos);
        }
        else
        {
            int n = Disc_pos_to_n(common,pos);

            if(DISC_DEBUG)
            {
                printf("Remove disc n=%d pos=%d at %p because %s (dM=%g dJ=%g M1=%g, dt_zoomfac=%g)\n",
                       n,
                       pos,
                       (void*)&common->discs[n],
                       Disc_removal_string(reason),
                       common->discs[n].dM_binary,
                       common->discs[n].dJ_binary,
                       stardata->star[0].mass,
                       stardata->model.dt_zoomfac
                    );
                fflush(stdout);
            }
            if(common->ndiscs>0)
            {
                Dprint("remove disc %d at time %g from stardata = %p (dt_zoomfac %g) because %s\n",
                       common->ndiscs,
                       stardata->model.time,
                       (void*)stardata,
                       stardata->model.dt_zoomfac,
                       Disc_removal_string(reason)
                    );
                struct disc_t * disc = &common->discs[n];
#ifdef MEMOIZE
                if(stardata != NULL &&
                   stardata->previous_stardata != NULL &&
                   stardata->common.discs[n].memo != NULL &&
                   stardata != stardata->previous_stardata)
                {
                    /* the memo will be freed, so remove the reference to it */
                    stardata->previous_stardata->common.discs[n].memo = NULL;
                }
                if(disc->memo != NULL)
                {
                    Dprint("call memoize_free\n");
                    memoize_free(&disc->memo);
                }
#endif//MEMOIZE
                memset(disc,0,sizeof(struct disc_t));
                common->ndiscs--;
            }
        }
    }
    else
    {
        /*
        Exit_binary_c_no_stardata(
            BINARY_C_UNKNOWN_DISC_TYPE,
            "Disc type %d unknown",
            type);
        */
    }
}

#endif // DISCS
