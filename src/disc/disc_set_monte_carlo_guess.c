#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef DISCS

#include "disc_constraints.h"

/*
 * Set an initial guess for the disc parameters using a Monte Carlo
 * procedure.
 */

void disc_set_monte_carlo_guess(struct stardata_t * const stardata,
                                const disc_parameter * const parameter_types,
                                struct disc_t * const disc,
                                const int n,
                                double * const guess)
{
    int i,iRin=-1,iRout=-1;
    for(i=0;i<n;i++)
    {
        const int param = parameter_types[i];

#ifdef DISC_MONTE_CARLO_GUESSES_USE_LOG
#define __Log(X) (log(X))
#define __Raise(X) (exp(X))
#else
#define __Log(X) (X)
#define __Raise(X) (X)
#endif //      DISC_MONTE_CARLO_GUESSES_USE_LOG

        const double min =
            __Log(Disc_parameter_min(disc,param));
        const double max =
            Max(min,__Log(Disc_parameter_max(disc,param)));
        //printf("MC%d : min = %g, max = %g\n",i,min,max);
        guess[i] =
            __Raise(min + (max - min) * random_number(stardata,NULL));

        if(param == DISC_PARAMETER_RIN)
        {
            iRin = i;
        }
        else if(param == DISC_PARAMETER_ROUT)
        {
            iRout = i;
         }
    }

    /*
     * If Rin > Rout, swap them
     */
    if(iRin>=0 &&
       iRout>=0 &&
       guess[iRin] > guess[iRout])
    {
        Swap(guess[iRin],guess[iRout]);
    }

    /*
    int ip;
    printf("INMC : ");
    for(ip=0;ip<n;ip++)
    {
        printf("%g ",guess[ip]);
    }
    printf("\n");
    */
}

#endif // DISCS
