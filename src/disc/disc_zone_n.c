#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef DISCS

#if defined MEMOIZE && defined DISCS_MEMOIZE

Disc_zone_counter disc_zone_n_memoize(const double radius,
                                      struct disc_t * Restrict const disc)
{
    return
    (Disc_zone_counter)
    Memoize(
        disc->memo,
        "disc_zone_n",
        1,
        scalar,
        Disc_zone_counter,
        1,
        radius,
        const double,
        1,
        disc_zone_n(radius,disc)
        );
}

#endif // MEMOIZE && DISCS_MEMOIZE

Disc_zone_counter disc_zone_n(const double radius,
                              const struct disc_t * Restrict const disc)
{
    /*
     * Given a disc structure and a radial coordinate (radius)
     * find the corresponding zone.
     *
     * Note: if the radius is within DISC_EDGE_EPS of the edge,
     *       and outside the disc, put it at the edge.
     *       This prevents floating point errors.
     */

#if DISC_DEBUG >= 3
    double d = radius-Disc_inner_zone(disc)->rstart;
    Discdebug(3,
              "R = %g, Disc Rin = %g, Rout = %g, Zone R inner edge = %g, R outer edge = %g, diff = %g, diff / R = %g, diff / INNER = %g, Abs_diff %g\n",
              radius,
              disc->Rin,
              disc->Rout,
              Disc_inner_zone(disc)->rstart,
              Disc_outer_zone(disc)->rend,
              d,
              d/radius,
              d/Disc_inner_zone(disc)->rstart,
              Abs_diff(radius,Disc_inner_zone(disc)->rstart)
        );
#endif //DISC_DEBUG>=3

    /*
     * Negative logic : use || (it's faster)
     *
     * Match to a zone if R is in the zone to within
     * a fraction 1.0 +/- ZONE_FLUFF
     */
#define ZONE_FLUFF (TINY)

#define Is_not_our_zone(RADIUS,ZONE) (                  \
        unlikely((ZONE)->valid == FALSE) ||             \
        (RADIUS) < (ZONE)->rstart*(1.0 - ZONE_FLUFF) || \
        (RADIUS) > (ZONE)->rend*(1.0 + ZONE_FLUFF)      \
        )

#define Is_our_zone(RADIUS,ZONE)                \
    (!(Is_not_our_zone((RADIUS),(ZONE))))

    /* one zone : must be in it! */
    if(disc->n_thermal_zones==1)
    {
        return 0;
    }

    /* compare to inner and outer edges */
    else if(unlikely(Float_same_within_eps(radius/Disc_outer_zone(disc)->rend,
                                           1.0,
                                           DISC_EDGE_EPS)))
    {
        return Disc_outer_zone_n(disc);
    }
    else if(unlikely(Float_same_within_eps(radius/Disc_inner_zone(disc)->rstart,
                                           1.0,
                                           DISC_EDGE_EPS)))
    {
        return Disc_inner_zone_n(disc);
    }
    else
    {
        /*
         * cycle estimation with callgrind (valgrind)
         *
         * orig code: self = 1.21
         *
         * with z pointer ++ = 0.84
         *
         * with z pointer + i = 0.98 : slower
         *
         * with "unsigned int i" = 1.21 : no change
         *
         * invert logic = 1.13
         *
         * invert logic + z pointer ++ = 0.76
         *
         * invert logic + zz pointer  = 0.90
         *
         * invert logic + z pointer ++ + while i loop = 0.76
         *
         * invert logic + z pointer ++ + while z loop (with zmax) = 0.77
         *
         * invert logic + z pointer ++ + while z loop (with zmax) and no i = 0.80
         *
         * invert logic + z pointer ++ + while i loop + unsigned (Disc_zone_counter) i = 0.70
         *
         * invert logic + z pointer ++ + while i loop + unsigned (Disc_zone_counter) i + multiply instead of divide = 0.68
         */




        /*
         * Search all zones
         */
        Disc_zone_counter i = 0;
        struct disc_thermal_zone_t * z =
            (struct disc_thermal_zone_t *) disc->thermal_zones + i;

        for(;i<DISCS_MAX_N_ZONES;i++,z++)
        {
            if(Is_our_zone(radius,z))
            {
                /* all conditions ok : this is our zone! */
                return i;
            }

            /*
             * positive logic: use &&, requires
             * all to be true, so is slower than with ||
             */
            /*
              if(z->valid==TRUE &&
              More_or_equal(radius/z->rstart,1.0) &&
              Less_or_equal(radius/z->rend,1.0))
              {
              return i;
              }
            */
        }
    }

    fprintf(stderr,"disc_zone_n failed\n");

    {
        int i;
        for(i=0;i<DISCS_MAX_N_ZONES;i++)
        {
            const struct disc_thermal_zone_t * z = &(disc->thermal_zones[i]);
            fprintf(stderr,
                    "ZONE %d : CF R = %g to Rstart = %g, Rend = %g : valid %d : %g %g\n",
                    i,
                    radius,
                    z->rstart,
                    z->rend,
                    z->valid,
                    radius/z->rstart,
                    radius/z->rend
                );
        }
    }

    /* radius is not in the disc! fail */
    fprintf(stderr,
           "eps in cf %g %g = %g\n",
           radius,
           Disc_inner_zone(disc)->rstart,
           Abs_diff(radius,Disc_inner_zone(disc)->rstart));
    fprintf(stderr,
           "eps out cf %g %g = %g\n",
           radius,
           Disc_outer_zone(disc)->rend,
           Abs_diff(radius,Disc_outer_zone(disc)->rend));

    // failure
    Backtrace;

#if DISC_DEBUG<3
    // not defined above...
    double d = radius-Disc_inner_zone(disc)->rstart;
#endif

    fprintf(stderr,
            "R = %g, Disc Rin = %g, Rout = %g, Zone R inner edge = %g, R outer edge = %g, diff = %g, diff / R = %g, diff / INNER = %g, Abs_diff %g\n",
            radius,
            disc->Rin,
            disc->Rout,
            Disc_inner_zone(disc)->rstart,
            Disc_outer_zone(disc)->rend,
            d,
            d/radius,
            d/Disc_inner_zone(disc)->rstart,
            Abs_diff(radius,Disc_inner_zone(disc)->rstart)
        );

    Exit_binary_c_no_stardata(
        BINARY_C_OUT_OF_RANGE,
        "Radius %22.10e is outside the disc (%22.10e < R / cm < %22.10e) so I cannot calculate a zone number (Abs_diff = Rin:%g Rout:%g FEQUALs %d %d, inner zone valid? %d outer zone valid? %d)",
        radius,
        disc->Rin,
        disc->Rout,
        Abs_diff(radius,Disc_inner_zone(disc)->rstart),
        Abs_diff(radius,Disc_outer_zone(disc)->rend),
        Fequal(radius,Disc_inner_zone(disc)->rstart),
        Fequal(radius,Disc_outer_zone(disc)->rend),
        Disc_inner_zone(disc)->valid,
        Disc_outer_zone(disc)->valid
        );


    return DISC_ZONE_TYPE_UNDEFINED;
}

#endif
