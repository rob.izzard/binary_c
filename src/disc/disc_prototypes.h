#pragma once
#ifndef DISC_PROTOTYPES_H
#define DISC_PROTOTYPES_H

#ifdef DISCS

#include "disc_power_law_prototypes.h"
#include "disc_data_types.h"

void evolve_discs(struct stardata_t * const stardata);

/************************************************************/
/* disc-related prototypes */
void disc_logging(struct stardata_t * const stardata,
                  const struct binary_system_t * const binary,
                  const int logger);
void No_return disc_testing(struct stardata_t * const stardata);
void disc_status(const struct stardata_t * const stardata);
struct disc_t * new_disc(struct stardata_t * const stardata,
                         const Disc_type type,
                         void * object,
                         const int pos);

void remove_disc(struct stardata_t * const stardata,
                 const Disc_type type,
                 void * object,
                 const int pos,
                 const int reason);

int ndiscs(const Disc_type type,
           const void * const object);

struct disc_t * inner_disc(const Disc_type type,
                           const void * object);
struct disc_t * outer_disc(const Disc_type type,
                           const void * object);

double mass_in_discs(const Disc_type type,
                     const void * const object);


void remove_discs(struct stardata_t * const stardata,
                  const Disc_type type,
                  void * const object);

void disc_mem_cleanup(struct stardata_t * const stardata);

double disc_evolve_disc_structure(struct stardata_t * const stardata,
                                  struct disc_t * const disc,
                                  const double dt);

void disc_initialize_disc(struct stardata_t * const stardata,
                          struct disc_t * const disc,
                          struct binary_system_t * const binary_in);


void disc_init_binary_structure(const struct stardata_t * const stardata,
                                struct binary_system_t * const binary,
                                struct disc_t * const cbdisc);

void evolve_disc(struct stardata_t * const stardata,
                 struct disc_t * const disc,
                 const double dt);

double mass_brent_wrapper(const double sig0,
                          va_list args);
double disc_temperature(const double radius,
                        struct disc_t * const disc);
double Pure_function disc_inner_edge_temperature(const struct disc_t * const disc);
double Pure_function disc_outer_edge_temperature(const struct disc_t * const disc);
double disc_column_density(const double radius,
                           DISC_CONST struct disc_t * const disc);
double zone_mass_numerical(struct disc_t * disc,
                           const struct disc_thermal_zone_t * zone);
double disc_specific_angular_momentum(const double radius,
                                      const struct binary_system_t * binary);
double disc_orbital_frequency(const double radius,
                              const struct binary_system_t * const binary);
double disc_injection_radius(const double h,
                             const struct binary_system_t * const binary);
double disc_velocity(const double radius,
                     const struct binary_system_t * const binary);
double disc_gravitational_pressure(const double radius,
                                   DISC_CONST struct disc_t * const disc,
                                   const struct binary_system_t * const binary);
double disc_pressure_radius(struct stardata_t * const stardata,
                            struct disc_t * const disc,
                            const struct binary_system_t * const binary,
                            const double P);
double disc_scale_height(const double radius,
                         struct disc_t * const disc,
                         const struct binary_system_t * const binary);
double disc_sound_speed(const double radius,
                        struct disc_t * const disc);
double disc_critical_surface_density_for_gravitational_collapse(const double radius,
                                                                struct disc_t * const disc);
double disc_Toomre_Q(const double radius,
                     struct disc_t * const disc,
                     const struct binary_system_t * const binary);
double disc_minimum_Toomre_Q(struct stardata_t * const stardata,
                             const struct disc_t * const disc,
                             const struct binary_system_t * const binary);

double disc_opacity(const double radius,
                    const struct disc_t * const disc);
double disc_density(const double radius,
                    struct disc_t * const disc,
                    const struct binary_system_t * const binary);
double disc_pressure(const double radius,
                     struct disc_t * const disc,
                     const struct binary_system_t * const binary);
double disc_mass_inflow_rate(const double radius,
                             struct disc_t * const disc,
                             const struct binary_system_t * const binary);
double disc_kinematic_viscosity(const double radius,
                                struct disc_t * const disc,
                                const struct binary_system_t * const binary);
double Pure_function specific_torque_Armitage(const double radius,
                                              const struct binary_system_t * const binary);
double Pure_function disc_binary_angular_momentum_flux(const struct disc_t * const disc,
                                                       const struct binary_system_t * const binary);
double Pure_function disc_total_angular_momentum(const struct disc_t * const disc,
                                                 const struct binary_system_t * const binary);
double Pure_function disc_total_angular_momentum_constraint(const struct disc_t * const disc,
                                                            const struct binary_system_t * const binary);
double disc_total_angular_momentum_numerical(const struct disc_t * const disc,
                                             const struct binary_system_t * const binary);
double Pure_function disc_mean_specific_angular_momentum(const struct disc_t * const disc,
                                                         const struct binary_system_t * const binary);
double Pure_function disc_partial_angular_momentum(const struct disc_t * const disc,
                                                   const struct binary_system_t * const binary,
                                                   const double r0,
                                                   const double r1);
double disc_total_mass_numerical(struct disc_t * const disc);
double Pure_function disc_total_mass(const struct disc_t * const disc);
double Pure_function disc_partial_mass(const struct disc_t * const disc,
                                       const double r0,
                                       const double r1);
double Pure_function disc_zone_mass(const struct disc_t * const disc,
                                    const int n);
double Pure_function disc_total_kinetic_energy(const struct disc_t * const disc,
                                               const struct binary_system_t * const binary);
double Pure_function disc_total_gravitational_potential_energy(const struct disc_t * const disc,
                                                               const struct binary_system_t * const binary);

double Pure_function disc_total_angular_momentum_flux(const struct disc_t * const disc,
                                                                  const struct binary_system_t * const binary);

double Pure_function disc_zone_angular_momentum(const struct disc_t * const disc,
                                                const struct binary_system_t * const binary,
                                                const int n);
double Pure_function disc_total_luminosity(const struct disc_t * const disc);
double Pure_function disc_total_luminosity_numerical(const struct disc_t * const disc);
double Pure_function disc_total_moment_of_inertia(const struct disc_t * const disc);
double disc_viscous_timescale(const double radius,
                              struct disc_t * const disc,
                              const struct binary_system_t * const binary);
double disc_mass_weighted_viscous_timescale(struct disc_t * const disc,
                                            const struct binary_system_t * const binary);
Disc_zone_counter disc_zone_n(const double radius,
                              const struct disc_t * Restrict const disc);
Disc_zone_counter disc_zone_n_memoize(const double radius,
                                      struct disc_t * Restrict const disc);

double generic_power_law(double radius,
                         const struct disc_t * Restrict const disc,
                         const int n);
Disc_zone_counter disc_nearest_zone_n(const double radius,
                                      const struct disc_t * const disc);

void cbdisc_eccentricity_pumping_rate(struct stardata_t * const stardata,
                                      struct disc_t * const disc,
                                      const struct binary_system_t * const binary,
                                      double * const edot,
                                      double * const jdot);
double disc_half_angular_momentum_radius(struct stardata_t * const stardata,
                                         const struct disc_t * const disc,
                                         const struct binary_system_t * const binary);
double disc_half_mass_radius(struct stardata_t * stardata,
                             const struct disc_t * const disc,
                             const struct binary_system_t * const binary);
double disc_mass_radius(struct stardata_t * stardata,
                        const struct disc_t * const disc,
                        const struct binary_system_t * const binary,
                        const double m);
double disc_angular_momentum_radius(struct stardata_t * const stardata,
                                    const struct disc_t * const disc,
                                    const struct binary_system_t * const binary,
                                    const double j);
double disc_inner_edge_accretion_f(struct stardata_t * stardata);

double Pure_function disc_angular_momentum_flux(const double radius,
                                                const struct disc_t * const disc,
                                                const struct binary_system_t * const binary);

double disc_Owen_2012_total_mdot(struct stardata_t * const stardata,
                                 const struct binary_system_t * const binary);

double disc_Owen_2012_sigmadot(struct disc_t * const disc,
                               const struct binary_system_t * const binary,
                               const double R);

double disc_Owen_2012_mass_loss_timescale(struct disc_t * disc,
                                          const struct binary_system_t * binary,
                                          const double R);
double disc_Owen_2012_sigmadot_integral(struct disc_t * disc,
                                        const struct binary_system_t * binary,
                                        const double Rin,
                                        const double Rout);
void disc_Owen_2012_normalize(struct stardata_t * const stardata,
                              struct disc_t * const disc,
                              const struct binary_system_t * const binary);

double disc_Owen_2012_mdot(struct disc_t * disc,
                           const struct binary_system_t * binary,
                           const double Rin,
                           const double Rout);

double disc_Owen_2012_angmom_integral(struct stardata_t * const stardata,
                                      struct disc_t * const disc,
                                      const struct binary_system_t * const binary,
                                      const double Rin,
                                      const double Rout);


void disc_show_thermal_zones(struct stardata_t * const stardata,
                             struct disc_t * const disc,
                             const struct disc_thermal_zone_t * zones,
                             const int n);
void disc_show_disc(struct stardata_t * const stardata,
                    struct disc_t * const disc,
                    const struct binary_system_t * const binary);

double disc_Rcross(const struct disc_thermal_zone_t * const z1,
                   const struct disc_thermal_zone_t * const z2);
Boolean disc_determine_zone_radii(struct stardata_t * const stardata,
                                  struct disc_t * const disc);


int disc_build_disc_zones(struct stardata_t * const stardata,
                          struct disc_t * const disc,
                          const struct binary_system_t * const binary);

void disc_initial_radiative_guesses(const struct binary_system_t * const binary,
                                    struct disc_t * const disc);

double disc_ring_radius(const struct disc_t * const disc,
                        const struct binary_system_t * const binary);

void disc_make_derived_powerlaws(struct disc_t * const disc,
                                 const struct binary_system_t * const binary);
Boolean disc_new_zone_list(struct stardata_t * const stardata,
                           struct disc_t * const disc,
                           const struct binary_system_t * const binary,
                           struct disc_thermal_zone_t * const thermal_zones);

void disc_check_power_laws(const struct disc_t * const d,
                           const Boolean output);

void disc_extend_powerlaw_from_reference_point(struct disc_t * const disc,
                                               const int ilaw,
                                               const double Rref,
                                               const double Aref,
                                               const double ref_exponent);

double disc_J_bisector(const double Rout,
                       void * p);
double disc_F_bisector(const double Rin,
                       void * p);
double disc_M_bisector(const double Tvisc0,
                       void * p);


int disc_calc_disc_structure(const struct binary_system_t * const binary,
                             struct disc_t * const disc,
                             struct stardata_t * const stardata,
                             const double t,
                             const double ddt);


void disc_initial_structure(struct disc_t * const disc,
                            const struct binary_system_t * const binary,
                            struct stardata_t * const stardata,
                            const double t,
                            const Boolean first,
                            Boolean * can_evolve);


void disc_apply_derivatives(struct disc_t * const disc,
                            const struct binary_system_t * const binary,
                            struct stardata_t * const stardata,
                            const double ddt,
                            Boolean * const can_evolve);
void disc_derivatives(struct disc_t * const disc,
                      const struct binary_system_t * const binary,
                      struct stardata_t * const stardata,
                      const double multiplier,
                      const Boolean edges);

void disc_trisector(double Tvisc0,
                    double Rin,
                    double Rout,
                    double *xx,
                    void * p);
void disc_trisector2(struct stardata_t * stardata,
                     struct disc_t * disc,
                     const struct binary_system_t * binary,
                     double *xx);

void disc_quadsector2(struct stardata_t * stardata,
                      struct disc_t * disc,
                      const struct binary_system_t * binary,
                      double *residual);

void disc_quadsector(double Tvisc0,
                     double Rin,
                     double Rout,
                     double torquef,
                     double *residual,
                     void * p);

void disc_nsector2(struct stardata_t * stardata,
                   struct disc_t * disc,
                   struct binary_system_t * binary,
                   const unsigned int n,
                   const disc_constraint * constraints,
                   double * residual,
                   int * failed);

void disc_nsector(unsigned int n,
                  const double parameter_value[n],
                  double * residual,
                  void * p);

void disc_convergence_status(struct stardata_t * const stardata,
                             const char * const s,
                             const struct disc_t * const disc,
                             const struct binary_system_t * const binary);

double disc_Owen_2012_clearing_radius(struct stardata_t * const stardata,
                                      struct disc_t * const disc,
                                      const struct binary_system_t *const binary);



void disc_Owen_2012_clearing_radii(struct stardata_t * stardata,
                                   struct disc_t * disc,
                                   const struct binary_system_t * binary,
                                   double * const R_clear_in,
                                   double * const R_clear_out);


void disc_initialize_every_timestep(struct stardata_t * const stardata);

void disc_evaporate_cbdiscs(struct stardata_t * const stardata);

#ifdef DISCS_CIRCUMBINARY_FROM_WIND
void disc_stellar_wind_to_cbdisc(struct stardata_t * const stardata);
#endif // DISCS_CIRCUMBINARY_FROM_WIND

void disc_parameter_space(struct stardata_t * const stardata,
                          struct disc_t * const disc);

double No_return disc_viscous_mass(struct disc_t * const disc,
                                   const struct binary_system_t * const binary,
                                   const double dt);

double disc_calc_natural_timescale(const struct stardata_t * const stardata,
                                   struct disc_t * const disc,
                                   const struct binary_system_t * const binary,
                                   const double dt,
                                   unsigned int * why);

void disc_natural_timescales(const struct stardata_t * const stardata,
                             struct disc_t * const disc,
                             const struct binary_system_t * const binary,
                             const double dt);

void disc_set_disc_timestep(struct stardata_t * const stardata,
                            struct disc_t * const disc,
                            const struct binary_system_t * const binary,
                            const double dt);
char * disc_failure_mode(struct stardata_t * const stardata,
                         struct disc_t * const disc,
                         const struct binary_system_t * const binary,
                         const int status);
double disc_outer_edge_mass_loss(struct stardata_t * const stardata,
                                 struct disc_t * const newdisc,
                                 struct disc_t * const olddisc,
                                 const struct binary_system_t * const binary);
double disc_inner_edge_mass_loss(struct stardata_t * const stardata,
                                 struct disc_t * const newdisc,
                                 struct disc_t * const olddisc,
                                 const struct binary_system_t * const binary);
void disc_edge_stripping(struct stardata_t * const stardata,
                         struct disc_t * const newdisc,
                         struct disc_t * const olddisc,
                         const struct binary_system_t * const binary);

void disc_calculate_derived_variables(struct stardata_t * const stardata,
                                      struct disc_t * const disc,
                                      const struct binary_system_t * const binary);

void disc_edge_loss_angular_momentum_flux(struct disc_t * const disc,
                                          const struct binary_system_t * const binary);
void disc_mass_changes(struct stardata_t * const stardata,
                       struct disc_t * const newdisc,
                       struct disc_t * const olddisc,
                       const struct binary_system_t * const binary,
                       const double multiplier,
                       const double ddt);

Boolean disc_rezone(struct disc_t * const disc,
                    const struct binary_system_t * const binary);

void disc_setup_zones(struct stardata_t * const stardata,
                      struct disc_t * const disc,
                      const struct binary_system_t * const binary);

void disc_set_temperature_power_laws(struct disc_t * const disc);

int disc_bisection_rooter(const unsigned int i,
                          const disc_parameter * parameters,
                          const disc_constraint * constraints,
                          struct stardata_t * stardata,
                          struct disc_t * disc,
                          const struct binary_system_t * binary,
                          Boolean * return_failed);

int old_disc_bisection_rooter(const int i,
                              const disc_parameter * parameters,
                              const disc_constraint * constraints,
                              struct stardata_t * stardata,
                              struct disc_t * disc,
                              const struct binary_system_t * binary,
                              Boolean * return_failed);

Boolean check_local_minimum(struct stardata_t * stardata,
                            struct disc_t * disc,
                            const struct binary_system_t * binary);


int disc_setup_convergence_parameter_types_and_constraints(struct stardata_t * const stardata,
                                                           struct disc_t * const disc,
                                                           const struct binary_system_t * const binary,
                                                           disc_parameter ** parameter_types_p,
                                                           disc_constraint ** constraints_p);
//#pragma GCC diagnostic push
//#pragma GCC diagnostic ignored "-Wpedantic"

void disc_set_disc_initial_guess(struct stardata_t * const stardata,
                                 struct disc_t * const disc,
                                 const int iguess,
                                 int n,/* VLA initial_guesses REQUIRES this to be type int, not const int */
                                 const disc_parameter * const parameter_types,
                                 double initial_guesses[DISC_SOLVER_NINITIAL_GUESSES+1][n]);

void disc_set_disc_initial_guesses(int n,/* VLA initial_guesses REQUIRES this to be type int, not const int */
                                   const disc_parameter * const parameter_types,
                                   double initial_guesses[DISC_SOLVER_NINITIAL_GUESSES+1][n],
                                   struct stardata_t * const stardata,
                                   struct disc_t * const disc,
                                   struct disc_t * const discwas,
                                   const struct binary_system_t * const binary,
                                   const double Rring
    );
//#pragma GCC diagnostic pop

void disc_set_monte_carlo_guess(struct stardata_t * const stardata,
                                const disc_parameter * const parameter_types,
                                struct disc_t * const disc,
                                const int n,
                                double * const guess);

void disc_free_convergence_parameter_types_and_constraints(disc_parameter ** parameter_types_p,
                                                           disc_constraint ** constraints_p);

double disc_inner_edge_loss_timescale(struct stardata_t * const stardata,
                                      struct disc_t * const disc,
                                      const struct binary_system_t * const binary);
double disc_outer_edge_loss_timescale(struct stardata_t * const stardata,
                                      struct disc_t * const disc,
                                      const struct binary_system_t * const binary);
double disc_generic_stripping_timescale(struct disc_t * const disc,
                                        const struct binary_system_t * const binary,
                                        const double radius,
                                        const int algorithm);

void No_return disc_show_MJ_space(struct disc_t * const disc,
                                  struct binary_system_t * const binary,
                                  struct stardata_t * const stardata);


#ifdef DISC_EQUATION_CHECKS
void disc_check_equations_everywhere(struct disc_t * const disc,
                                     const struct binary_system_t * const binary);
void disc_equation_checker(const double r,
                           struct disc_t * const disc,
                           const struct binary_system_t * const binary,
                           double * const model,
                           double * const star);
#endif // DISC_EQUATION_CHECKS

int disc_n_valid_zones(const struct disc_t * const disc);


void disc_zone_crossing_radii(struct stardata_t * const stardata,
                              struct disc_t  * const disc,
                              const struct binary_system_t * const binary,
                              double ** radii_p);

Boolean disc_new_zone_radii(struct disc_t * const disc);


Boolean disc_monotonic_check(struct stardata_t * const stardata,
                             struct disc_t * const disc,
                             struct binary_system_t * const binary,
                             const unsigned int n
    );


int disc_hottest_zone_at_radius(struct disc_thermal_zone_t * const zones,
                                const double radius,
                                const Boolean all,
                                double * Thottest_p);

#ifdef DISCS
binary_c_API_function
struct disc_info_t *
binary_c_disc_info(struct libbinary_c_stardata_t * Restrict const stardata);
#endif // DISCS

#endif // DISCS
#endif // DISC_PROTOTYPES_H
