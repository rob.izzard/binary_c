#pragma once
#ifndef DISC_PARAMETERS_H
#define DISC_PARAMETERS_H
#include <float.h>
#include "../binary_c_maths.h"
#include "disc_macros.h"
#include "disc_debug.h"

/*
 * Logging for individual systems with discs
 */
#define DISC_LOG
#define DISC_LOG_2D

/*
 * Disc log max sizes (0 == unlimited)
 */
#define DISC_LOG_MAX_BYTES ((size_t)(1000*MEGABYTE))
#define DISC_LOG_2D_MAX_BYTES ((size_t)(1000*MEGABYTE))

/*
 * if DISC_INTERMEDIATE_LOG is defined, then we log on
 * disc timesteps as well as binary_c timesteps
 */
#define DISC_INTERMEDIATE_LOG

/*
 * Disc debugging:
 * 0 = no debug, 1 = some, 2 = more, etc.
 */
#define DISC_DEBUG 0

/*
 * Disc equation checks
 */
#define DISC_EQUATION_CHECKS

/*
 * Log equation checks
 */
//#define DISC_EQUATION_CHECKS_LOG

/*
 * If fabs(1-Rin/Rout) < DISC_RADIUS_DIFFERENCE_THRESHOLD_FOR_FAILURE
 * consider the disc a failure
 */
#define DISC_RADIUS_DIFFERENCE_THRESHOLD_FOR_FAILURE 1e-8


/*
 * Warn if residual > threshold
 */
//#define DISC_RESIDUAL_WARNING
#define DISC_RESIDUAL_WARNING_THRESHOLD 1e-6

/*
 * Maximum number of disc zones
 */
#define DISCS_MAX_N_ZONES 3

/*
 * Maximum age of a disc
 */
#define DISC_MAX_LIFETIME (1e9*YEAR_LENGTH_IN_SECONDS)

/*
 * Max and min disc timestep
 */
#define DISC_MAX_TIMESTEP (1e+8 * YEAR_LENGTH_IN_SECONDS)
#define DISC_MIN_TIMESTEP (1e-1 * YEAR_LENGTH_IN_SECONDS)

/* debugging */
#define DEBUG_ALL 0

/* API testing */
//#define TEST_POWER_LAW_API

/*
 * debugging flags
 * 0 = no debugging output
 * 1 = some debugging output
 * 2 = full debugging output (less useful in most cases)
 */
#if defined DEBUG_ALL && DEBUG_ALL==1
/*
 * Force all debugging to be on
 */
#define DEBUG_FIRST_GUESSES 1
#define DEBUG_ZONE_RADII 1
#define DEBUG_BISECTION_LOOP 1
#define DEBUG_MASS_BISECTOR 1
#define DEBUG_ANGMOM_BISECTOR 1
#define DEBUG_ANGMOM_FLUX_BISECTOR 1
#define DEBUG_ZONE_LIST_CONSTRUCTION 1
#define DEBUG_DERIVED_POWER_LAWS 1
#define SHOW_VISCOUS_WARNING 1
#define SHOW_CONVERGED_DISC_STRUCTURE 1
#define TEST_ZONE_CONTINUITY 0
#define DISC_NANS
#define DISC_DEBUG_MJE_CHANGES 1
#else
/*
 * You choose...
 */
#define DEBUG_FIRST_GUESSES 0
#define DEBUG_ZONE_RADII 0
#define DEBUG_BISECTION_LOOP 0
#define DEBUG_MASS_BISECTOR 0
#define DEBUG_ANGMOM_BISECTOR 0
#define DEBUG_ANGMOM_FLUX_BISECTOR 0
#define DEBUG_ZONE_LIST_CONSTRUCTION 0
#define DEBUG_DERIVED_POWER_LAWS 0
#define TEST_ZONE_CONTINUITY 0
#define DISC_DEBUG_MJE_CHANGES 0
#define SHOW_CONVERGED_DISC_STRUCTURE 0
#undef DISC_NANS
#define SHOW_VISCOUS_WARNING 0
#endif

#ifndef SHOW_ZONE_LIST_POWER_LAWS
#define SHOW_ZONE_LIST_POWER_LAWS 0
#endif
#ifndef SHOW_CONVERGED_DISC_STRUCTURE
#define SHOW_CONVERGED_DISC_STRUCTURE 0
#endif
#ifndef DISC_SHOW_TEST_EXPRESSIONS
#define DISC_SHOW_TEST_EXPRESSIONS 0
#endif

//#undef DEBUG_ZONE_RADII
//#define DEBUG_ZONE_RADII 1

//#define POWER_LAW_SANITY_CHECKS

//#define Discprint(...)
#define Discprint(...) printf(__VA_ARGS__);

/*
 * Define this to build possibly buggy code
 */
//#define BUILD_DUBIOUS_DISC_CODE

//#define DISC_BUILD_DEPRECATED_CODE


/*
 * Timestep change limiters (defaults low 0.8, high 1.2)
 */
#define DISC_TIMESTEP_LOW_FACTOR 0.8
#define DISC_TIMESTEP_HIGH_FACTOR 1.2

/*
 * A long time (seconds, default 1e100)
 */
#define DISC_LONG_TIME 1e100


/*
 * Reset the disc solver list to the default
 * if
 * disc->iteration < DISC_SOLVER_RESET_MIN
 * or
 * disc->iteration % DISC_SOLVER_RESET_MODULO == 0
 *
 * If DISC_SOLVER_RESET_MODULO is 0 then the solver
 * list is reset every timestep.
 */
#define DISC_SOLVER_RESET_MIN 1
#define DISC_SOLVER_RESET_MODULO 0

/*
 * Order in which we should attempt
 * the solvers
 */
#define DISC_SOLVER_ORDERED_LIST                \
    GSL_MULTIROOT_FSOLVER_HYBRIDS,              \
        GSL_MULTIROOT_FSOLVER_HYBRID,           \
        DISC_SOLVER_BISECTION

/*
 * How many initial guesses?
 */
#define DISC_SOLVER_NINITIAL_GUESSES 5

/*
 * Numerical solver tolerance and max number iterations
 * of iterations of bisection rooters (NB: you might not
 * use the bisection method, e.g. if you are using GSL's method(s)
 * and these work fine). Defaults: 1e-6 and 1000.
 */

#define DISC_TOLERANCE 1e-6
#define DISC_BISECTION_MAX_ITERATIONS 1000

/*
 * Mass bisection tolerance (1e-6) and
 * maximum number of iterations
 */
#define DISC_MASS_TOLERANCE (DISC_TOLERANCE)
#define DISC_BISECTION_MASS_MAX_ITERATIONS DISC_BISECTION_MAX_ITERATIONS

/*
 * When Rout is not known, but the disc total angular momentum is,
 * Rout is set with this tolerance on J (1e-6)
 */
#define DISC_ANGMOM_TOLERANCE (DISC_TOLERANCE)
#define DISC_BISECTION_ANGMOM_MAX_ITERATIONS DISC_BISECTION_MAX_ITERATIONS

/*
 * Angular momentum flux tolerance (1e-6)
 */
#define DISC_ANGMOM_FLUX_TOLERANCE (DISC_TOLERANCE)
#define DISC_BISECTION_ANGMOM_FLUX_MAX_ITERATIONS DISC_BISECTION_MAX_ITERATIONS

/*
 * Torquef bisection tolerance (1e-6)
 * and max iterations (1000)
 */
#define DISC_TORQUEF_TOLERANCE (DISC_TOLERANCE)
#define DISC_BISECTION_TORQUEF_MAX_ITERATIONS 1000

/*
 * Hence the error at the disc edge:
 * this should be greater than the above tolerances
 */
#define DISC_EDGE_EPS (                         \
        2.0*Max4(                               \
            DISC_MASS_TOLERANCE,                \
            DISC_ANGMOM_TOLERANCE,              \
            DISC_ANGMOM_FLUX_TOLERANCE,         \
            DISC_TORQUEF_TOLERANCE)             \
        )


/*
 * General disc bisection tolerance.
 *
 * I usually set this to 1e-8 because it can quickly be
 * far more accurate than the disc convergence tolerance.
 */
#define DISC_BISECTION_TOLERANCE 1e-8
#define DISC_BISECTION_ATTEMPTS 1000

/*
 * Disc pressure calculation tolerance (default DISC_BISECTION_TOLERANCE)
 */
#define DISC_PRESSURE_RADIUS_TOLERANCE (DISC_BISECTION_TOLERANCE)
#define DISC_PRESSURE_RADIUS_ATTEMPTS (DISC_BISECTION_ATTEMPTS)

/*
 * Disc R from J calculation tolerance (default DISC_BISECTION_TOLERANCE)
 */
#define DISC_BISECT_RJ_TOLERANCE (DISC_BISECTION_TOLERANCE)
#define DISC_BISECT_RJ_ATTEMPTS (DISC_BISECTION_ATTEMPTS)

/*
 * Disc R from M calculation tolerance (default DISC_BISECTION_TOLERANCE)
 */
#define DISC_BISECT_RM_TOLERANCE (DISC_BISECTION_TOLERANCE)
#define DISC_BISECT_RM_ATTEMPTS (DISC_BISECTION_ATTEMPTS)

/*
 * If the disc < this width, it's a ring. Evaporate it.
 * Default 1e5 (cm).
 */
#define DISC_MINIMUM_WIDTH 1e5

/*
 * If the disc evaporation timescale is < this,
 * evaporate it instantly.
 */
#define DISC_MINIMUM_EVAPORATION_TIMESCALE (YEAR_LENGTH_IN_SECONDS)

/*
 * Maximum number of attempts at bisection
 */
#define _DISC_BISECTION_MAX_ATTEMPTS 10000

#define DISC_BISECTION_MAX_ATTEMPTS (Is_zero(disc->lifetime) ? \
                                     (10 * _DISC_BISECTION_MAX_ATTEMPTS) \
                                     : _DISC_BISECTION_MAX_ATTEMPTS)

/*
 * Maximum number of iterations through the GSL root finder
 */
#define DISC_ROOT_FINDER_MAX_ATTEMPTS 1000

#define DISC_SMALL_RADIUS (1e2)
#define DISC_LARGE_RADIUS (1e50)
#define DISC_LARGE_MASS (1e50)

/*
 * Circumbinary disc angular momentum selectors
 */
#define CBDISC_ANGMOM_FROM_WIND_L2 -1


/*
 * Minimum disc mass (in Msun)
 */
#define MINIMUM_DISC_MASS 1e-6

/*
 * Bisector limits
 */

#define DISC_TVISC0_MIN (1e-50)
#define DISC_TVISC0_MAX (1e50)

#define DISC_R_MIN (2.95e5)
#define DISC_R_MAX DISC_LARGE_RAIUS

#define DISC_ROUT_MIN (disc->converged == TRUE ?        \
                       Max(DISC_R_MIN,disc->Rin) :      \
                       2.95e5)
#define DISC_ROUT_MAX DISC_LARGE_RADIUS
/* Rin is in cm : cannot be < BH size */
#define DISC_RIN_MIN (2.95e5)
#define DISC_RIN_MAX (disc->converged == TRUE ?         \
                      Min(disc->Rout,DISC_R_MAX) :      \
                      DISC_R_MAX)

/* reference length at which power laws are started (cm) */
#define DISC_REFERENCE_LENGTH (1.0)

/*
#define DISC_RIN_MIN DISC_REFERENCE_LENGTH
#define DISC_RIN_MAX 1e100
#define DISC_ROUT_MIN DISC_REFERENCE_LENGTH
#define DISC_ROUT_MAX 1e100
*/
/*
 * Minimum disc mass, in Msun.
 * Default: 1e-20
 */
#define DISC_MINIMUM_DISC_MASS_MSUN 1e-20

/*
 * Minimum disc F. Default 1e-3.
 */
#define DISC_MINIMUM_DISC_F 1e-3

/*
 * Number of shells in numerical integrals.
 * Default: 100
 */
#define DISC_ADAM_RESOLUTION 100

/*
 * Minimum and maximum torque factor.
 * You may think that the preferences->cbdisc_torquef is a good minimum,
 * and often it is (because removing material from the inner
 * edge requires an increase in the torque factor) however, may
 * be binary orbit changes that require a smaller factor.
 */
#define DISC_MINIMUM_TORQUE 1e-20
#define DISC_MAXIMUM_TORQUE 1e20

/*
 * Fix the orbit?
 */
//#define FIX_BINARY


/*
 * How to lose mass to the ISM?
 * Either a global Mdot (DISC_LOSS_ISM_GLOBAL)
 * or outer edge stripping (DISC_LOSS_ISM_OUTER_EDGE)
 */
#define DISC_LOSS_ISM_ALGORITHM DISC_LOSS_ISM_OUTER_EDGE

/*
 * When stripping the inner or outer edge, allow the disc
 * to reduce its mass (M) or angular momentum (J) by at most
 * DISC_STRIP_FRAC_(M|J)_(INNER|OUTER) * (M|J)
 *
 * e.g. setting this to 0.1 will only allow 10% of the mass to be
 *      stripped in one timestep.
 *
 * To allow the whole disc to be stripped very quickly, set
 * all these to 1.0.
 *
 * Note that the OUTER values are currently 1.0. If you set these to,
 * say, 0.1, then the algorithm gets into a loop where not stripping
 * enough (the 90%) of the outer edge means the mass loss rate goes up faster
 * than stripping can happen. Really, we want a shorter timestep to
 * allow this to happen accurately, but instead we should just let the
 * outer edge of the disc adapt instantaneously.
 *
 * This also happens on the inner edge, but it's less bad.
 */
#define DISC_STRIP_FRAC_INNER 1.0
#define DISC_STRIP_FRAC_M_INNER DISC_STRIP_FRAC_INNER
#define DISC_STRIP_FRAC_J_INNER DISC_STRIP_FRAC_INNER
#define DISC_STRIP_FRAC_M_OUTER 1.0
#define DISC_STRIP_FRAC_J_OUTER 1.0

/*
 * Use log space? recommended.
 */
#define DISC_MONTE_CARLO_GUESSES_USE_LOG


/*
 * Use log(x) instead of (x) in nsection? This is
 * slightly quicker and prevents negative values
 * being used.
 */
#define DISC_NSECTION_USELOG

/*
 * Use logarithm(x) instead of (x) in bisection?
 *
 * Usually this is very slightly slower, but may help explore
 * larger parameter spaces faster.
 */
#define DISC_BISECT_USELOG TRUE

#define DISC_BISECT_OWEN_RADIUS_USELOG DISC_BISECT_USELOG
#define DISC_BISECT_RJ_USELOG DISC_BISECT_USELOG
#define DISC_BISECT_RM_USELOG DISC_BISECT_USELOG
#define DISC_BISECT_PRESSURE_RADIUS_USELOG DISC_BISECT_USELOG
#define DISC_BISECT_VISCOUS_USELOG DISC_BISECT_USELOG
#define DISC_BISECT_BISECTION_ROOTER_USELOG DISC_BISECT_USELOG
#define DISC_BISECT_TORQUEF_USELOG DISC_BISECT_USELOG


#if defined DISC_RESIDUAL_WARNING || defined DISC_LOG
#define DISC_SAVE_EPSILONS
#endif

#endif // DISC_PARAMETERS_H
