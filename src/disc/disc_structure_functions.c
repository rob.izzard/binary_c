#include "../binary_c.h"

No_empty_translation_unit_warning;

#ifdef DISCS
#include "disc_structure_functions.h"

/*
 * Disc structure functions, e.g. T(R), Sigma(R) etc.
 */



double disc_temperature(const double radius,
                        struct disc_t * const disc)
{
    /*
     * Temperature as a function of radius (K).
     */
#if defined MEMOIZE && defined DISCS_MEMOIZE
    /*
     * Memoize only 1 call back, because it's either
     * going to be a repeated call at a given radius, or
     * be different.
     */
    return Memoize(
        disc->memo,
        "discT",
        1,
        scalar,
        double,
        1,
        radius,
        double,
        1,
        Max(0.0,generic_power_law(radius,disc,POWER_LAW_TEMPERATURE))
        );
#else
    /*
     * Do not memoize, just a function call
     */
    return Max(0.0,generic_power_law(radius,disc,POWER_LAW_TEMPERATURE));
#endif
}

double Pure_function disc_inner_edge_temperature(const struct disc_t * const disc)
{
    /*
     * Temperature at the disc's inner edge (K)
     */
    const struct disc_thermal_zone_t * zone = Disc_inner_zone(disc);
    return Max(0.0,power_law(&(zone->Tlaw),zone->rstart));
}

double Pure_function disc_outer_edge_temperature(const struct disc_t * const disc)
{
    /*
     * Temperature at the disc's outer edge (K)
     */
    const struct disc_thermal_zone_t * zone = Disc_outer_zone(disc);
    return Max(0.0,power_law(&(zone->Tlaw),zone->rend));
}

double generic_power_law(double radius,
                         const struct disc_t * Restrict const disc,
                         const int n)
{
    /*
     * Wrapper to acquire the value of a power law
     * at some point in the disc.
     *
     * n is the power law number, or -1 for temperature
     */
    const struct disc_thermal_zone_t * z;

    if(unlikely(radius) < 0.0)
    {
        /*
         * if radius is negative, we seek only the nearest zone
         */
        radius = - radius;
        z = Disc_nearest_zone(radius,disc);
    }
    else
    {
        /*
         * But by default, we seek a zone INSIDE the disc,
         * i.e. there is some error checking.
         */
        z = Disc_zone(radius,disc);
    }

    return power_law(n==POWER_LAW_TEMPERATURE ?
                     (&z->Tlaw) : (z->power_laws+n),
                     radius);
}


double disc_column_density(const double radius,
                           DISC_CONST struct disc_t * const disc)
{
    /*
     * Column density as a function of radius
     * (g/cm^2)
     */
    return generic_power_law(radius,disc,POWER_LAW_SIGMA);
}

double Pure_function disc_specific_angular_momentum(const double radius,
                                                    const struct binary_system_t * binary)
{
    /*
     * Specific angular momentum as a function of radius (cm^2/s)
     */
    return sqrt(GRAVITATIONAL_CONSTANT * binary->mtot * radius);
}

double Pure_function disc_injection_radius(const double h,
                                           const struct binary_system_t * const binary)
{
    /*
     * Given specific angular momentum h, and a binary
     * system, determine the radius at which material will be
     * injected.
     */
    return Pow2(h)/(GRAVITATIONAL_CONSTANT * binary->mtot);
}

double Pure_function disc_orbital_frequency(const double radius,
                                            const struct binary_system_t * const binary)
{
    /*
     * Orbital frequency as a function of radius (rad/s)
     */
    return Is_zero(radius) ? 0.0 : (sqrt(GRAVITATIONAL_CONSTANT*binary->mtot/Pow3(radius)));
}

double Pure_function disc_velocity(const double radius,
                                   const struct binary_system_t * const binary)
{
    /*
     * Velocity as a function of radius
     */
    return radius * disc_orbital_frequency(radius,binary);
}



double disc_gravitational_pressure(const double radius,
                                   DISC_CONST struct disc_t * const disc,
                                   const struct binary_system_t * const binary)
{
    /*
     * Gravitational pressure as a function of radius,
     * neglecting the disc mass
     */
    return Is_zero(radius) ? 0.0 :
        (GRAVITATIONAL_CONSTANT * binary->mtot *
         disc_column_density(radius,disc) / Pow2(radius));
}

double disc_scale_height(const double radius,
                         struct disc_t * const disc,
                         const struct binary_system_t * const binary)
{
    /*
     * Disc scale height as a function of radius (cm)
     */
    return sqrt(BOLTZMANN_CONSTANT * disc_temperature(radius,disc) * disc->gamma * Pow3(radius)/
                (disc->mu * GRAVITATIONAL_CONSTANT * binary->mtot));
}

double disc_sound_speed(const double radius,
                        struct disc_t * const disc)
{
    /*
     * Sound speed as a function of radius (cm/s)
     */
    return sqrt(disc->gamma * BOLTZMANN_CONSTANT * disc_temperature(radius,disc) / disc->mu);
}

double disc_critical_surface_density_for_gravitational_collapse(const double radius,
                                                                struct disc_t * const disc)
{
    /*
     * Critical surface density for disc collapse
     * by gravitational instability
     */
    return disc_sound_speed(radius,disc) * disc_opacity(radius,disc)/
        (PI * GRAVITATIONAL_CONSTANT);
}

double disc_Toomre_Q(const double radius,
                     struct disc_t * const disc,
                     const struct binary_system_t * const binary)
{
    /*
     * Return the Toomre Q value:
     * Q>1 : stable
     * Q<1 : unstable
     *
     * See e.g.
     * Toomre, Astrophysical Journal, vol. 139, p. 1217-1238 (1964)
     */
    const double Q =
        disc_sound_speed(radius,disc) * disc_orbital_frequency(radius,binary) /
        (PI * GRAVITATIONAL_CONSTANT * disc_column_density(radius,disc));
    return Q;
}

static double Toomre_Q_minimizer(double x,
                                 void * params);

double disc_minimum_Toomre_Q(struct stardata_t * const stardata,
                             const struct disc_t * const disc,
                             const struct binary_system_t * const binary)
{
    /*
     * Return the minimum Q in the disc
     *
     * First we sample the disc at n points to determine whether there
     * is a bracketed minimum, then if there is we use the Brent
     * algorithm to find the minimum.
     */
    int status = 0;
    double minx;

    const double minQ  = generic_minimizer(&status,
                                           10,
                                           TRUE,
                                           0.01,
                                           &minx,
                                           &Toomre_Q_minimizer,
                                           disc->Rin,
                                           disc->Rout,
                                           disc,
                                           binary);

    if(status!=0)
    {
        Discdebug(1,"Warning: generic_minimizer returned an error code %d\n",status);
    }

    Discdebug(2,
              "Toomre disc minimum Q : minQ(R=%g cm) = %g\n",minx,minQ);

    return minQ;
}


static double Toomre_Q_minimizer(double x,
                                 void * p)
{
    Map_GSL_params(p,args);
    Map_varg(struct disc_t *,disc,args);
    Map_varg(const struct binary_system_t *,binary,args);
    va_end(args);
    const double Q = disc_Toomre_Q(x,disc,binary);
    return Q;
}

double Pure_function disc_opacity(const double radius Maybe_unused,
                                  const struct disc_t * const disc)
{
    /*
     * Opacity may vary with radius.
     */

    /* not yet! */
    return disc->kappa;
}


double disc_mass_inflow_rate(const double radius,
                             struct disc_t * const disc,
                             const struct binary_system_t * const binary)
{
    /*
     * Mass inflow rate Mdot = 3*pi*nu(R)*Sigma(R)
     */
    return 3.0 * PI *
        disc_kinematic_viscosity(radius,disc,binary) *
        disc_column_density(radius,disc);
}

double disc_density(const double radius,
                    struct disc_t * const disc,
                    const struct binary_system_t * const binary)
{
    /*
     * Density (g/cm^3) = Sigma / H
     */
    const double H = disc_scale_height(radius,disc,binary);
    return Is_zero(H) ? 0.0 : (disc_column_density(radius,disc) / H);
}

double disc_pressure(const double radius,
                     struct disc_t * disc,
                     const struct binary_system_t * const binary)
{
    /*
     * Pressure as a function of radius (erg/cm^3)
     */
    return BOLTZMANN_CONSTANT * disc_temperature(radius,disc) *
        disc_density(radius,disc,binary) / disc->mu;
}

double disc_kinematic_viscosity(const double radius,
                                struct disc_t * const disc,
                                const struct binary_system_t * const binary)
{
    /*
     * Kinematic viscosity (nu(R)) as a function of radius
     * (cm^2/s)
     */
    const double cs = disc_sound_speed(radius,disc);
    const double Omega = disc_orbital_frequency(radius,binary);
    return disc->alpha * cs * cs / Omega;
}

double Pure_function disc_ring_radius(const struct disc_t * const disc,
                                      const struct binary_system_t * const binary)
{
    /*
     * If we were to put the whole disc in a ring,
     * where would it be?
     */
    return Pow2(disc->J) /
        (Pow2(disc->M) * binary->mtot * GRAVITATIONAL_CONSTANT);
}

double Pure_function specific_torque_Armitage(const double radius,
                                              const struct binary_system_t * const binary)
{
    /*
     * Central binary specific torque, erg/g,
     * from Armitage and Natarajan (2002).
     *
     * radius is the distance from the centre of mass.
     */
    return
        Pow2(binary->m1/binary->m2)*
        GRAVITATIONAL_CONSTANT * binary->mtot *
        Pow4(binary->separation)/(2.0*Pow5(radius));
}

double Pure_function disc_binary_angular_momentum_flux(const struct disc_t * const disc,
                                                       const struct binary_system_t * const binary Maybe_unused)
{
    /*
     * Angular momentum flux through the disc
     * caused by the binary torque
     */
    return
        3.0 * PI * disc->alpha * disc->gamma *
        BOLTZMANN_CONSTANT * disc->sigma0 / disc->mu
        ;
}

double Pure_function disc_angular_momentum_flux(const double radius Maybe_unused,
                                                const struct disc_t * const disc,
                                                const struct binary_system_t * const binary)
{
    /*
     * Angular momentum flux, F(radius).
     *
     * Note that there is (in theory) no radius dependence
     * using our standard disc model, however it's useful
     * to have the radius passed in so we can test the calculation
     * by using an alternative procedure.
     */
    const double F1 = disc_binary_angular_momentum_flux(disc,binary);
    const double Fevap = disc->F_stripping_correction;

    /*
     * Note that if angular momentum is being lost from the
     * disc, we assume the disc recovers instantly such that
     * F(R) includes this evaporation.
     */
    const double F = F1 + Fevap;

    /*
    if(0)printf("FFF Target F = 3pi (alpha=%g) (gamma=%g) kB (sigma0=%g) / (mu=%g) from Tvisc0 = %g, Fevap = %g -> F = %g (was disc->F = %g)\n",

                disc->alpha,
                disc->gamma,
                disc->sigma0,
                disc->mu,
                disc->Tvisc0,
                Fevap,
                F,
                disc->F);
    */

    return F;

    /* alternative (but slower)
    //const double H = disc_scale_height(radius,disc,binary);
    const double Sigma = disc_column_density(radius,disc);
    const double h = disc_specific_angular_momentum(radius,binary);
    const double nu = disc_kinematic_viscosity(radius,disc,binary);
    return 3.0 * PI * h * Sigma * nu;
    */
}

double disc_viscous_timescale(const double radius,
                              struct disc_t * const disc,
                              const struct binary_system_t * const binary)
{
    /*
     * Viscous timescale at a given radius
     */
    return Is_zero(radius) ? 0.0 :
        (Pow2(radius)/(5.0 * disc_kinematic_viscosity(radius,disc,binary)));
}

double disc_half_angular_momentum_radius(struct stardata_t * const stardata,
                                         const struct disc_t * const disc,
                                         const struct binary_system_t * const binary)
{
    /*
     * Function to bisect on the half angular
     * momentum radius.
     */

    double rjhalf = disc_angular_momentum_radius(stardata,
                                                 disc,
                                                 binary,
                                                 disc->J*0.5);

    /*
     * On error, return the previous value, which may be zero.
     */
    if(Is_zero(rjhalf))
    {
        rjhalf = Max(disc->Rin,Min(disc->Rout,disc->RJhalf));
    }

    Discdebug(2,
              "RJHALF Bisect gave %g (was %g)\n",
              rjhalf,
              disc->RJhalf);

    return rjhalf;

}

double disc_angular_momentum_radius(struct stardata_t * const stardata,
                                    const struct disc_t * const disc,
                                    const struct binary_system_t * const binary,
                                    const double j)
{
    /*
     * Function to bisect on the half angular
     * momentum radius.
     *
     * Returns 0.0 on error.
     */
    double radius;

    if(Is_zero(j))
    {
        /*
         * No angular momentum inside r = 0 : should never happen
         */
        radius = 0.0;
    }
    else if(More_or_equal(j, disc_total_angular_momentum(disc,binary)))
    {
        /*
         * We want to be outside the disc : return Rout
         */
        radius = disc->Rout;
    }
    else
    {
        int error;
        const double r = generic_bisect(&error,
                                        BISECT_USE_MONOCHECKS,
                                        BISECTOR_DISC_RHALFJ,
                                        &disc_angular_momentum_radius_bisector,
                                        disc->Rout,
                                        disc->Rin,
                                        disc->Rout,
                                        DISC_BISECT_RJ_TOLERANCE,
                                        DISC_BISECT_RJ_ATTEMPTS,
                                        DISC_BISECT_RJ_USELOG,
                                        1.0,
                                        stardata,
                                        disc,
                                        binary,
                                  j);

        /*
         * On error, return zero.
         */
        if(unlikely(error))
        {
            Discdebug(2,
                      "angmom->radius: error = %d = %s\n",
                      error,
                      gsl_strerror(error));
        }
        radius = (error == BINARY_C_BISECT_ERROR_NONE ? r : 0.0);
    }

    return radius;
}

double disc_half_mass_radius(struct stardata_t * const stardata,
                             const struct disc_t * const disc,
                             const struct binary_system_t * const binary)
{

    /*
     * Function to bisect on the half mass
     * radius.
     *
     * Returns 0.0 on error.
     */
    return disc_mass_radius(stardata,
                            disc,
                            binary,
                            disc->M*0.5);
}

double disc_mass_radius(struct stardata_t * const stardata,
                        const struct disc_t * const disc,
                        const struct binary_system_t * const binary,
                        const double m)
{
    /*
     * Function to bisect on a particular mass co-ordinate
     * and return its radius.
     *
     * Returns 0.0 on error, and we assume m=0 is an error also.
     */
    double radius;
    if(Is_zero(m))
    {
        radius = 0.0;
    }
    else if(More_or_equal(m,disc_total_mass(disc)))
    {
        radius = disc->Rout;
    }
    else
    {
        int error;
        const double r = generic_bisect(
            &error,
            BISECT_USE_MONOCHECKS,
            BISECTOR_DISC_MASS_RADIUS,
            &disc_mass_radius_bisector,
            disc->Rout,
            disc->Rin,
            disc->Rout,
            DISC_BISECT_RM_TOLERANCE,
            DISC_BISECT_RM_ATTEMPTS,
            DISC_BISECT_RM_USELOG,
            1.0,
            stardata,
            disc,
            binary,
            m);

        /*
         * On error, return 0.0
         */
        if(error)
        {
            printf(
                  "mass->radius: error = %d = %s, r = %g\n",
                  error,
                  gsl_strerror(error),
                  r);
        }

        radius = (error == BINARY_C_BISECT_ERROR_NONE ? r : 0.0);
    }

    return radius;
}

static double disc_angular_momentum_radius_bisector(const double r,
                                                    void * p)
{
    /*
     * Bisect the half angular momentum radius
     */
    Map_GSL_params(p,args);
    Map_varg(struct stardata_t *,stardata,args);
    Map_varg(struct disc_t *,disc,args);
    Map_varg(struct binary_system_t *,binary,args);
    Map_varg(const double,j,args);
    va_end(args);

    const double low = disc_partial_angular_momentum(disc,
                                                     binary,
                                                     disc->Rin,
                                                     disc->Rin);
    const double high = disc_partial_angular_momentum(disc,
                                                     binary,
                                                     disc->Rin,
                                                     disc->Rout);

    const double Jpart = Max(0.0,
                             disc_partial_angular_momentum(disc,
                                                           binary,
                                                           disc->Rin,
                                                           r));
    const double f = Bisection_result(j,Jpart);
    Discdebug(2,
              "J->R bisector: At r = %g (Rin = %g : Rout = %g : in disc ? %d) : Jwant = %g, Jpart = %g, Jdisc=%g : return f = %g [bounds %g %g]\n",
              r,
              disc->Rin,
              disc->Rout,
              r>=disc->Rin && r<=disc->Rout,
              j,
              Jpart,
              disc->J,
              f,
              low,
              high

        );
    return f;
}

static double disc_mass_radius_bisector(const double r,
                                        void * p)
{
    /*
     * Bisect for a given mass
     */
    Map_GSL_params(p,args);
    Map_varg(struct stardata_t *,stardata,args);
    Map_varg(struct disc_t *,disc,args);
    Map_varg(struct binary_system_t *, binary,args);
    Map_varg(const double,m,args);
    va_end(args);

    const double Mpart = disc_partial_mass(disc,
                                           disc->Rin,
                                           r);
    const double f = Bisection_result(m,Mpart);

    Discdebug(1,
              "M->R bisector: At r = %g (%g << %g : in disc ? %d ) : Mwant = %g, Mpart = %g, Mdisc=%g Msun : return f = %g (binary = %p)\n",
              r,
              disc->Rin,
              disc->Rout,
              r>=disc->Rin && r<=disc->Rout,
              m/M_SUN,
              Mpart/M_SUN,
              disc->M/M_SUN,
              f,
              (void*)binary);
    return f;
}


double disc_pressure_radius(struct stardata_t * const stardata,
                            struct disc_t * const disc,
                            const struct binary_system_t * const binary,
                            const double P)
{
    /*
     * Function to find the radius at which the pressure
     * is P.
     *
     * Assumes pressure monotonically decreases outwards.
     *
     * Gives outer/inner edge radii if the P is
     * less/greater than that at the outer/inner edge
     * of the disc, respectively.
     *
     * Returns 0.0 on error.
     */
    double radius;
    const double P_at_Rin = disc_gravitational_pressure(disc->Rin,disc,binary);
    const double P_at_Rout = disc_gravitational_pressure(disc->Rout,disc,binary);
    Discdebug(2,
              "Find P = %g radius between Pin(Rin=%g) = %g, Pout(Rout=%g) = %g\n",
              P,
              disc->Rin,
              P_at_Rin,
              disc->Rout,
              disc_gravitational_pressure(disc->Rout,disc,binary));

    if(Less_or_equal(P,P_at_Rout))
    {
        /*
         * Wanted pressure is below that at the disc edge,
         * so return the outer edge.
         */
        radius = disc->Rout;
    }
    else if(More_or_equal(P,P_at_Rin))
    {
        /*
         * Wanted pressure exceeds that at the inner edge,
         * so return the inner edge.
         */
        radius = disc->Rin;
    }
    else
    {
        /*
         * Bisect for the required pressure.
         */
        int error;
        const double r = generic_bisect(&error,
                                        BISECT_USE_MONOCHECKS,
                                        BISECTOR_DISC_PRESSURE_RADIUS,
                                        &disc_pressure_radius_bisector,
                                        disc->Rin,
                                        disc->Rin,
                                        disc->Rout,
                                        DISC_PRESSURE_RADIUS_TOLERANCE,
                                        DISC_PRESSURE_RADIUS_ATTEMPTS,
                                        DISC_BISECT_PRESSURE_RADIUS_USELOG,
                                        1.0,
                                        P,
                                        disc,
                                        binary);
        radius = (error == BINARY_C_BISECT_ERROR_NONE ? r : 0.0);
    }

    return radius;
}

static double disc_pressure_radius_bisector(const double r,
                                            void * p)
{
    Map_GSL_params(p,args);
    /*
     * Bisect for the radius at which the pressure is Pwant
     */
    Map_varg(double,Pwant,args);
    Map_varg(struct disc_t *,disc,args);
    Map_varg(struct binary_system_t *,binary,args);
    va_end(args);
    const double P = disc_gravitational_pressure(r,disc,binary);
    /*
     * P drops outwards, so use Bisection_result_inverse()
     */
    return Bisection_result_inverse(Pwant,P);
}

#ifdef DISC_EQUATION_CHECKS

void disc_check_equations_everywhere(struct disc_t * const disc,
                                     const struct binary_system_t * const binary)
{
    /*
     * Call equation checkers throughout the disc
     */
    if(
        Disc_is_disc(disc) &&
        !Fequal(disc->Rin,disc->Rout) &&
        disc->Rin < disc->Rout &&
        disc_n_valid_zones(disc) > 0
        )
    {
        const int n = 100;
        disc->equations_T_error_pc = 0.0;
        disc->equations_mass_loss_term_pc = 0.0;
        double logr, dlogr = Max(1e-10,(log10(disc->Rout) - log10(disc->Rin))/(1.0*n));
        if(Is_not_zero(dlogr))
        {
            for(logr = log10(disc->Rin);
                logr < log10(disc->Rout);
                logr += dlogr)
            {
                const double r = exp10(logr);
                double modelT4,starT4;
                disc_equation_checker(r,disc,binary,&modelT4,&starT4);
            }
        }
    }
}

int disc_n_valid_zones(const struct disc_t * const disc)
{
    /*
     * Return the number of valid zones in the disc
     */
    int n=0;
    for(n=0;n<DISCS_MAX_N_ZONES;n++)
    {
        if(disc->thermal_zones[n].valid)
        {
            n++;
        }
    }
    return n;
}

void disc_equation_checker(const double r,
                           struct disc_t * const disc,
                           const struct binary_system_t * const binary,
                           double * const model Maybe_unused,
                           double * const star)
{
    /*
     * At radius r evaluate dL/dR from the star and from the disc model
     */

    /*
     * star : Lstar = 4PI sigma R^2 T^4
     *
     * hence
     *
     *  dL/dR = 4PI sigma 2R T^4
     *
     * because T is a constant.
     *
     * where sigma is the Stefan-Boltzmann constant
     */
    *star = binary->L / (4.0 * PI * Pow2(r) * STEFAN_BOLTZMANN_CONSTANT);

    /*
     * Disc temperature as a function of radius
     */
    const double T_LHS = disc_temperature(r,disc);

    /*
     * Evaluate T^4 RHS from disc model
     */
    const double nu = disc_kinematic_viscosity(r,disc,binary);
    const double Sigma =  disc_column_density(r,disc);
    const double omega = disc_orbital_frequency(r,binary);
    const double eps = 1e-7;
    const double rlow = r*(1.0-eps);
    const double H = disc_scale_height(r,disc,binary);
    const double Hlow = disc_scale_height(rlow,disc,binary);
    const double dHdR = (H-Hlow) / (r - rlow);
    const double dlnHdlnR = (r/H) * dHdR;

    double mdot = 0.0;
    /*
     * Use non-edge mass loss
     */
    for(int i=0;i<DISC_LOSS_N;i++)
    {
        if(!Disc_loss_strip_from_edge(i))
        {
            mdot += disc->loss[i].mdot;
        }
    }

    /*
     * A, B, C, D are Eqs. 4, 6, 9 and 10
     */
    const double A = 27.0 * disc->kappa * nu * Pow2(Sigma * omega) / (64.0 * STEFAN_BOLTZMANN_CONSTANT);
    const double B = 2.0/(3.0*PI) * binary->flux * (1.0 - disc->albedo) * Pow3(binary->Rstar / r) / STEFAN_BOLTZMANN_CONSTANT;
    const double C = 3.0 * PI * H / (4.0 * binary->Rstar) * (dlnHdlnR - 1.0);
    const double D = GRAVITATIONAL_CONSTANT * binary->mtot * mdot /
        (2.0 * PI * binary->separation * Pow2(r) * STEFAN_BOLTZMANN_CONSTANT);

    /*
     * The usual RHS : note that the D term does not contribute
     */
    const double T4_RHS = A + 0.0*D + B * (1.0 + C);
#ifdef DISC_EQUATION_CHECKS_LOG
    const double T4_LHS = Pow4(T_LHS);
#endif // DISC_EQUATION_CHECKS_LOG
    const double T_RHS = Pow1d4(T4_RHS);
    const double dT = T_LHS - T_RHS;

    /* save for logging */
    disc->equations_T_error_pc = Max(disc->equations_T_error_pc,
                                     fabs(dT/T_LHS)*100.0);
    disc->equations_mass_loss_term_pc = Max(disc->equations_mass_loss_term_pc,
                                            fabs(D/(A+B*(1+C)))*100.0);

#ifdef DISC_EQUATION_CHECKS_LOG
    printf("EQN2 r=%12g T_LHS=%12g T^4(LHS) = %12g == (A=%12g) + (B=%12g) * (1 + (C=%12g)) + (D=%12g) = %12g :: T4 ratio %12g : dT = %12g : dT/T = %5.2f %% : D/(A+B*(1+C)) = %5.2f %%\n",
           r,
           T_LHS,
           T4_LHS,
           A,
           B,
           C,
           D,
           T4_RHS,
           T4_LHS/T4_RHS,
           dT,
           disc->equations_T_error_pc,
           disc->equations_mass_loss_term_pc
        );
#endif// DISC_EQUATION_CHECKS_LOG
}

#endif // DISC_EQUATION_CHECKS

#endif // DISCS
