#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef DISCS


/*
 * Function to return the number of discs surrounding
 * an object
 */

int ndiscs(const Disc_type type,
           const void * const object)
{
    int ndiscs = -1;
    if(type == DISC_CIRCUMSTELLAR)
    {
        const struct star_t * const star = object;
        ndiscs = star->ndiscs;
    }
    else if(type == DISC_CIRCUMBINARY)
    {
        const struct stardata_t * const stardata = object;
        ndiscs = stardata->common.ndiscs;
    }
    else
    {
        Exit_binary_c_no_stardata(
            BINARY_C_UNKNOWN_DISC_TYPE,
            "Disc type %d unknown",
            type);

    }
    return ndiscs;
}

#endif//DISCS
