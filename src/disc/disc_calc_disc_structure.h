#pragma once
#ifndef DISC_CALC_DISC_STRUCTURE_H
#define DISC_CALC_DISC_STRUCTURE_H

#include <gsl/gsl_vector.h>
#include <gsl/gsl_multiroots.h>

static int disc_GSL_multiroot(const int solver,
                              ...);

/*
 * Define LOG_GSL_PARAMS to take logs before converging :
 * usually this is faster and definitely it's more stable
 * because it means there are never negative parameter_types
 */
//#define LOG_GSL_PARAMS

//#define CONVERGENCE_LOG

/*
static double converge_with_torque_wrapper(const double torquef,
                                           void * p);
*/
static double converge_with_torque(const int n,
                                   const disc_parameter * parameter_types,
                                   const disc_constraint * constraints,
                                   const struct binary_system_t * binary,
                                   struct disc_t * disc,
                                   struct disc_t * discwas,
                                   struct stardata_t * stardata,
                                   const double t,
                                   int * ret
                                   //,Boolean * Rin_min_bisect
    );

static int GSL_multiroot_testfunc(const gsl_vector * x,
                    void * p,
                    gsl_vector * f);

#ifdef OLDCODE
static int quadGSL_multiroot_testfunc(const gsl_vector * x,
                        void * p,
                        gsl_vector * f);
#endif//OLDCODE



#endif // DISC_CALC_DISC_STRUCTURE_H
