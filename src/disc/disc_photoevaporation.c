#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef DISCS
#include <gsl/gsl_integration.h>
#include "disc_photoevaporation.h"


/*
 * Functions to calculate photoevaporation rates, mostly
 * based on Owen, Clarke and Ercolano (2012, MNRAS 433, 1880).
 */

double disc_Owen_2012_total_mdot(struct stardata_t * const stardata,
                                 const struct binary_system_t * const binary)
{
    /*
     * Equation B4 of Owen et al. (2012) gives the total
     * mass loss rate of a disc with a hole.
     *
     * NB returns a negative rate in cgs, i.e. g s^-1
     */
    const double mdot =
        -4.8e-9 * pow(binary->mtot/M_SUN,-0.148) *
        pow(binary->LX * 1e-30, 1.14) *
        M_SUN / YEAR_LENGTH_IN_SECONDS;

    Discdebug(2,
              "OWEN total mdot = %g Msun/y (M* = %g Msun, LX = %g erg/s = %g Lsun)\n",
              mdot * YEAR_LENGTH_IN_SECONDS / M_SUN,
              binary->mtot/M_SUN,
              binary->LX,binary->LX/L_SUN);
    return mdot;
}

double disc_Owen_2012_sigmadot(struct disc_t * const disc,
                               const struct binary_system_t * const binary,
                               const double R)
{
    const Boolean vb = FALSE;
    double sigmadot;
    if(Is_really_zero(disc->Owen2012_norm))
    {
        sigmadot = 0.0;
    }
    else
    {
        /*
         * Eq. B5 of Owen et al. (2012) gives d(Sigma)/dt
         *
         * Input : R in cm
         *
         * Units are not given in the paper, but Owen
         * says (private communication) that y and R are in AU,
         * so we must divide R and disc->Rin by ASTRONOMICAL_UNIT.
         *
         * i.e.
         *
         * integral (sigmadot * 2pi R dR) = Mdot = Eq. B4
         *
         * which means that, locally, Eq. B4 * sigmadot
         * will be in Msun yr^-1 AU^-2
         */


        /*
         * all radii in Owen's fitting formulae are in AU
         */
        const double Rhole = disc->Rin / ASTRONOMICAL_UNIT;
        const double RAU = R / ASTRONOMICAL_UNIT;

        if(vb)printf("RAU %g : ",RAU);

#define B6(R) ( 0.95 *                          \
                Max(0.0,                        \
                    Min( ((R) - Rhole) , 60.0 ) \
                    ) *                         \
                ( M_SUN / binary->mtot ) )

        /*
         * NB Coefficients were sent to RGI by James Owen,
         * so they are slightly more accurate than in
         * his 2012 paper.
         */
#define a2 (-4.3822599312953403e-01)
#define b2 (-1.0658387115013401e-01)
#define c2 (5.6994636363474982e-01)
#define d2 (1.0732277075017336e-02)
#define e2 (-1.3180959703632333e-01)
#define f2 (-1.3228570869396541e+00)

#define expterm(A,B,Y,R) ((A)*(B)*exp((B)*(Y))/(R))

        /*
         * Eq. B6 gives y. If this is <0 there is no mass loss,
         * although by construction this is impossible with the
         * definition of B6 above (it *is* possible in Owen's
         * original paper).
         */
        const double y = B6(RAU);

        if(unlikely(y<0.0))
        {
            sigmadot = 0.0;
            if(vb)printf("y = 0 : ");
        }
        else
        {
            /*
             * Eq. B5 has units AU^-2, convert to cm^-2
             */
            const double B5 = (
                expterm(a2,b2,y,RAU) +
                expterm(c2,d2,y,RAU) +
                expterm(e2,f2,y,RAU)
                ) * exp(-pow(y/57.0, 10.0)) /
                Pow2(ASTRONOMICAL_UNIT);


            if(vb)printf("y = %g, B5 = %g : ",y,B5);
            /*
             * Normalization.
             *
             * The factor disc->Owen2012_norm is
             * set by a call to disc_Owen2012_normalize
             *
             * It includes a correction for the normalization function
             * not integrating to 1.0 (it has the correct shape),
             * and the factor for the total mass loss rate of the disc.
             */
            sigmadot = B5 * disc->Owen2012_norm;

#ifdef NANCHECKS
            if(isnan(B5) || isnan(y) || isnan(sigmadot))
            {
                if(vb)printf("NAN error : B5=%g y=%g sigmadot=%g Owen2012_norm=%g\n",
                             B5,y,sigmadot,disc->Owen2012_norm);
                Exit_binary_c_no_stardata(
                    BINARY_C_EXIT_NAN,
                    "NAN error : B5=%g y=%g sigmadot=%g Owen2012_norm=%g\n",
                    B5,y,sigmadot,disc->Owen2012_norm);
            }
#endif
        }
        if(vb)printf("sigmadot = %g\n",sigmadot);
    }

    if(vb)printf("sigmadot = %g (Owen2012_norm = %g)\n",
                 sigmadot,
                 disc->Owen2012_norm);

    return sigmadot;
}

void disc_Owen_2012_normalize(struct stardata_t * const stardata,
                              struct disc_t * const disc,
                              const struct binary_system_t * const binary)
{
    /*
     * Re-calculate the Owen 2012 normalization for the
     * current disc.
     * Do this from 0 to 100AU, this should be sufficient.
     */
    disc->Owen2012_norm = 1.0;
    const double total_mdot = disc_Owen_2012_total_mdot(stardata,binary);

    /*
     * Calculate disc mass loss timescale, if it's longer
     * than 1Gyr assume it is is zero.
     */
    const double tmdot = disc->M / -total_mdot;

    if(tmdot > YEAR_LENGTH_IN_SECONDS * 1.0 * 1e9)
    {
        disc->Owen2012_norm = 0.0;
    }
    else
    {
        /*
         * normalize out to OWEN2012_OUTER_RADIUS = 1.65*57 AU
         * with Rin = OWEN2012_INNER_RADIUS = 0
         *
         * This outer limit is as in Owen's paper. It
         * makes little difference if you choose a larger outer
         * limit: the function flattens off anyway.
         */
        const double Rinwas = disc->Rin;
        disc->Rin = 0.0;

        const double I =
            disc_Owen_2012_sigmadot_integral(disc,
                                             binary,
                                             OWEN2012_INNER_RADIUS,
                                             OWEN2012_OUTER_RADIUS);

        /* restore Rin */
        disc->Rin = Rinwas;

        /* hence normalization factor */
        disc->Owen2012_norm = total_mdot/I;

        Discdebug(2,
                  "OWEN NORM mdot=%g (= %g Msun/y) I = %g cm^-2 = %g AU^-2\n",
                  total_mdot,
                  total_mdot*YEAR_LENGTH_IN_SECONDS/M_SUN,
                  I,
                  I * Pow2(ASTRONOMICAL_UNIT));
    }
}

double disc_Owen_2012_mdot(struct disc_t * disc,
                           const struct binary_system_t * binary,
                           const double Rin,
                           const double Rout)
{
    /*
     * Mass loss rate from the disc by integrating the
     * Sigmadot function. NB This is NOT the same as
     * the "total" mass loss rate: that function assumes all the
     * disc is there, while some may be lost.
     */
    const double mdot =
        Is_really_zero(disc->Owen2012_norm) ? 0.0 :
        disc_Owen_2012_sigmadot_integral(disc,
                                         binary,
                                         Rin,
                                         Rout);
    return mdot;
}

static double Owen2012_sigmadot_f(double x,
                                  void *params)
{
    Map_GSL_params(params,args);
    Map_varg(struct disc_t *,disc,args);
    Map_varg(const struct binary_system_t *,binary,args);
    va_end(args);
    const double R = x;
    const double sigmadot = disc_Owen_2012_sigmadot(disc,binary,R);
    const double dI = sigmadot * 2.0 * PI * R;
    return dI;
}

double disc_Owen_2012_sigmadot_integral(struct disc_t * disc,
                                        const struct binary_system_t * binary,
                                        const double Rin,
                                        const double Rout)
{
    /*
     * Integrate to find the mass loss rate
     * because of X-ray photoevaporation
     * between radii Rin and Rout.
     */
    double error;
    const double sigmadot = GSL_integrator(Rin,
                                           Rout,
                                           PHOTOEVAPORATION_GSL_INTEGRAL_TOLERANCE,
                                           &error,
                                           GSL_INTEGRATOR_QAG,
                                           &Owen2012_sigmadot_f,
                                           disc,
                                           binary);

    return sigmadot;
}


double disc_Owen_2012_angmom_integral(struct stardata_t * const stardata,
                                      struct disc_t * const disc,
                                      const struct binary_system_t * const binary,
                                      const double Rin,
                                      const double Rout)
{
    /*
     * Use GSL to integrate the angular momentum
     * loss rate from photoevaporation.
     */
    double error;
    return
        Is_really_zero(disc->Owen2012_norm) ? 0.0 :
        GSL_integrator(Rin,
                       Rout,
                       PHOTOEVAPORATION_GSL_INTEGRAL_TOLERANCE,
                       &error,
                       GSL_INTEGRATOR_QAG,
                       &Owen2012_angmom_f,
                       stardata,
                       disc,
                       binary);
}


static double Owen2012_angmom_f(double x,
                                void *params)
{
    Map_GSL_params(params,args);
    Map_varg(struct stardata_t const *,stardata,args);
    Map_varg(struct disc_t DISC_CONST *,disc,args);
    Map_varg(struct binary_system_t const *,binary,args);
    va_end(args);
    const double R = x;
    const double h = disc_specific_angular_momentum(R,binary);

    const double sigma = disc_column_density(R,disc);
    const double sigmadot = disc_Owen_2012_sigmadot(disc,binary,R);
    const double dMdot_dR = 2.0 * PI * R * sigmadot;
    const double dJ_dR = 2.0 * PI * R * sigma * h;
    const double dJdot_dR = h * dMdot_dR;
    const double local_timescale = - dJ_dR / dJdot_dR;

    Discdebug(3,
              "At R = %g Rsun : sigmadot = %g, d(Mdot)/dR = %g, dJ/dr = %g, dJdot/dR = %g, local timescale %g years\n",
              R/R_SUN,
              sigmadot,
              dMdot_dR,
              dJ_dR,
              dJdot_dR,
              local_timescale / YEAR_LENGTH_IN_SECONDS);
    return h * dMdot_dR;
}

double disc_Owen_2012_mass_loss_timescale(struct disc_t * const disc,
                                          const struct binary_system_t * const binary,
                                          const double R)
{
    /*
     * Return the local mass-loss timescale at a location
     * in the disc given by radius R
     */
    const double sigma = disc_column_density(R,disc);
    const double sigmadot = disc_Owen_2012_sigmadot(disc,binary,R);
    return sigma/Max(fabs(sigmadot),1e-50);
}

double disc_Owen_2012_clearing_radius(struct stardata_t * const stardata,
                                      struct disc_t * const disc,
                                      const struct binary_system_t * const binary)
{
    /*
     * Radius at which the Owen 2012 mass loss timescale
     * equals the local viscous timescale.
     *
     *
     * If the local viscous time is shorter than the mass loss
     * time at the inner edge, then the radius is just the inner
     * edge radius (i.e. the inner edge remains unchanged).
     *
     * If the mass loss is faster at the outer edge, then the disc
     * should be evaporated until the outer edge.
     *
     * Otherwise, we return the radius at which the viscous timescale
     * and the mass loss timescale are equal.
     */
    Discdebug(2,
              "\nOWEN M=%g J=%g %g\nOWEN Inner edge Rin = %g Tin = %g, tvisc %g y tmdot %g y\n",
              disc->M/M_SUN,
              disc->J,
              disc_total_angular_momentum(disc,binary),
              disc->Rin/R_SUN,
              disc_temperature(disc->Rin,disc),
              tvisc(disc->Rin)/YEAR_LENGTH_IN_SECONDS,
              tmdot(disc->Rin)/YEAR_LENGTH_IN_SECONDS);
    Discdebug(2,
              "\nOWEN Outer edge Rout = %g Tout = %g tvisc %g y tmdot %g y\n",
              disc->Rout/R_SUN,
              disc_temperature(disc->Rout,disc),
              tvisc(disc->Rout)/YEAR_LENGTH_IN_SECONDS,
              tmdot(disc->Rout)/YEAR_LENGTH_IN_SECONDS);

    double r;
    if(tvisc(disc->Rin) < tmdot(disc->Rin))
    {
        /*
         * Viscous transport is faster than X-ray mass
         * loss at the inner edge, so the disc is
         * dominated by viscosity and can expand to take into
         * account mass loss.
         */
        r = disc->Rin;
        Discdebug(2,
                  "Whole disc is dominated by viscosity (timescales : mdot [inner edge = %g , outer edge %g], viscosity [inner edge %g, outer edge %g)\n",
                  tmdot(disc->Rin)/YEAR_LENGTH_IN_SECONDS,
                  tvisc(disc->Rin)/YEAR_LENGTH_IN_SECONDS,
                  tmdot(disc->Rout)/YEAR_LENGTH_IN_SECONDS,
                  tvisc(disc->Rout)/YEAR_LENGTH_IN_SECONDS
            );
    }
    else if(tvisc(disc->Rout) > tmdot(disc->Rout))
    {
        /*
         * X-ray mass loss dominates at the outer edge,
         * so the whole disc is dominated by X-ray mass loss.
         */
        r = disc->Rout;
        Discdebug(2,
                  "Whole disc is dominated by mass loss (timescales : mdot [inner edge = %g , outer edge %g], viscosity [inner edge %g, outer edge %g)\n",
                  tmdot(disc->Rin)/YEAR_LENGTH_IN_SECONDS,
                  tvisc(disc->Rin)/YEAR_LENGTH_IN_SECONDS,
                  tmdot(disc->Rout)/YEAR_LENGTH_IN_SECONDS,
                  tvisc(disc->Rout)/YEAR_LENGTH_IN_SECONDS
            );
    }
    else
    {
        /*
         * The radius is bracketed: find it.
         */
        Discdebug(2,
                  "call generic bisector for Owen radius\n");
        int error;
        r = generic_bisect(&error,
                           BISECT_USE_MONOCHECKS,
                           BISECTOR_DISC_OWEN_RADIUS,
                           &disc_Owen_2012_clearing_radius_bisector,
                           0.5*(disc->Rout+disc->Rin),
                           disc->Rin,
                           disc->Rout,
                           DISC_TOLERANCE,
                           1000,
                           DISC_BISECT_OWEN_RADIUS_USELOG,
                           1.0,
                           stardata,
                           disc,
                           binary);
        if(error != BINARY_C_BISECT_ERROR_NONE)
        {
            Discdebug(2,
                      "Warning : Owen radius bisect failed, returning r = Rout\n");
            r = disc->Rout;
        }
    }
    Discdebug(2,
              "OWEN clearing radius %g (Rin = %g, Rout = %g) (Rsun)\n",
              r / R_SUN,
              disc->Rin / R_SUN,
              disc->Rout / R_SUN);
    return r;
}


void disc_Owen_2012_clearing_radii(struct stardata_t * const stardata,
                                   struct disc_t * const disc,
                                   const struct binary_system_t * const binary,
                                   double * const R_clear_in,
                                   double * const R_clear_out)
{
    if(disc->Rin > 0.0 &&
       disc->Rout > 0.0)
    {
        /*
         * Find the radii outside of which mass loss
         * dominates viscous loss.
         */

        /*
         * Default radii
         */
        *R_clear_in = disc->Rin;
        *R_clear_out = disc->Rout;

#define Viscosity_dominated(R) (tvisc(R) < tmdot(R))
#define Wind_dominated(R) (!Viscosity_dominated(R))
#define Dominator_string(R) (Wind_dominated(R) ? "Wind" : "Viscosity")

        Discdebug(2,"clearing: inner edge %s dominated, outer edge %s dominated\n",
                  Dominator_string(disc->Rin),
                  Dominator_string(disc->Rout));

        const double dr_init = (disc->Rout - disc->Rin) / 1000.0;
        const double drmin = 0.001 * R_SUN; /* accuracy */
        const double fzoom = 0.5; /* factor for each zoom */
        const int nzoom_max = 100; /* max zoom level */
        int nzoom;
        const int nres = 1000;
        double r,dr = (disc->Rout - disc->Rin) / ((double)nres);

        /*
         * Check for anywhere being wind dominated
         */
        Boolean action_required = FALSE;
        Boolean somewhere_is_viscous Maybe_unused = FALSE;
        for(r=disc->Rin; r<disc->Rout; r+=dr)
        {
            if(Wind_dominated(r))
            {
                /* wind dominated */
                action_required = TRUE;
            }
            else
            {
                /* viscosity dominated */
                somewhere_is_viscous = TRUE;
            }
        }

        if(action_required)
        {
            if(Wind_dominated(disc->Rin))
            {
                /*
                 * Inner edge is wind dominated. Move out until
                 * we find where the disc is viscously dominated.
                 */

                Discdebug(2,"Find inner clearing radius\n");

                dr = dr_init;
                nzoom = 0;
                for(r=disc->Rin; r<disc->Rout+dr*0.1; r+=dr)
                {

                    Discdebug(2,
                              "clearing in dr = %g %s (nzoom = %d) : At r=Rin=%g %s tvisc = %g, twind = %g : %s dominated\n",
                              Solar(dr,R),
                              nzoom,
                              Solar(r,R),
                              tvisc(r)/YEAR_LENGTH_IN_SECONDS,
                              tmdot(r)/YEAR_LENGTH_IN_SECONDS,
                              Dominator_string(r));


                    if(Viscosity_dominated(r))
                    {
                        /* zoom! */
                        r -= dr;
                        dr *= fzoom;

                        if(dr < drmin || ++nzoom > nzoom_max)
                        {
                            break;
                        }
                    }

                    *R_clear_in = Min(disc->Rout,Max(disc->Rin,r));
                }
            }

            if(Wind_dominated(disc->Rout))
            {
                /*
                 * Outer edge is wind dominated. Move out until
                 * we find where the disc is viscously dominated.
                 */
                nzoom = 0;
                dr = dr_init;
                for(r=disc->Rout; r<*R_clear_in; r-=dr)
                {
                    Discdebug(2,"clearing out dr = %g %s (nzoom = %d) : At r=Rin=%g %s tvisc = %g, twind = %g : %s dominated\n",
                              Solar(dr,R),
                              nzoom,
                              Solar(r,R),
                              tvisc(r)/YEAR_LENGTH_IN_SECONDS,
                              tmdot(r)/YEAR_LENGTH_IN_SECONDS,
                              Dominator_string(r));

                    if(Viscosity_dominated(r))
                    {
                        r += dr;
                        dr *= fzoom;
                        if(dr < drmin || ++nzoom > nzoom_max)
                        {
                            break;
                        }
                    }
                    *R_clear_out = Max3(disc->Rin,
                                        *R_clear_in,
                                        Min(disc->Rout,r));
                }
            }
        }
    }

    Discdebug(2,
              "OWEN clearing radii in = %g %s, out = %g %s\n",
              Solar(*R_clear_in,R),
              Solar(*R_clear_out,R));
}






static double disc_Owen_2012_clearing_radius_bisector(const double r,
                                                      void * p)
{
    /*
     * Bisect the half angular momentum radius
     */
    Map_GSL_params(p,args);
    Map_varg(struct stardata_t *,stardata,args);
    Map_varg(struct disc_t *,disc,args);
    Map_varg(struct binary_system_t *,binary,args);
    va_end(args);

    const double tm = tmdot(r);
    const double tv = tvisc(r);
    const double sigma = disc_column_density(r,disc);
    const double sigmadot = disc_Owen_2012_sigmadot(disc,binary,r);

    Discdebug(2,
              "at r=%g Rsun (Rin = %g, Rout = %g Rsun): sigma=%g sigmadot=%g : tmdot = %g, tvisc = %g : ratio %g\n",
              r/R_SUN,
              disc->Rin/R_SUN,
              disc->Rout/R_SUN,
              sigma,
              sigmadot,
              tm/YEAR_LENGTH_IN_SECONDS,
              tv/YEAR_LENGTH_IN_SECONDS,
              tm/tv);

    return tm - tv;
}
#endif //DISCS
