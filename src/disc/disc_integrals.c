#include "../binary_c.h"

No_empty_translation_unit_warning;

#if defined DISCS

#include "disc_integrals.h"

/*
 * Disc integral functions.
 *
 * Local (static) functions:
 *
 * The general integral function over the whole disc is disc_integral.
 *
 * A (relatively untested) function disc_partial_integral can be
 * used to integrate over part of the disc.
 *
 * Public functions:2
 *
 * disc_total_mass, disc_total_angular_momentum, disc_total_luminosity,
 * disc_total_moment_of_inertia
 * etc.
 */

static double disc_integral(const struct disc_t * const disc,
                            const int nlaw)
{
    /*
     * General integrator over the disc's radius
     * for a function A(R) with power laws offsets
     * in the viscous and radiative zones, respectively.
     *
     * The exponent required is (c + m * zone->exponent)
     * where c and m are input parameters.
     */


    Disc_zone_counter i;
    double I=0.0;
    if(nlaw==POWER_LAW_TEMPERATURE)
    {
        /*
         * You should probably never integrate the temperature!
         */
        /*
          for(i=0; i<disc->n_thermal_zones; i++)
          {
          I += power_law_zone_integral(&disc->thermal_zones[i].Tlaw);
          }
        */
        Exit_binary_c_no_stardata(
            2,
            "You should not try to integrate the temperature power law directly: use one of the ..._INTEGRAND power laws\n");
    }
    else
    {
        for(i=0; i<disc->n_thermal_zones; i++)
        {
            const double dI =
                power_law_zone_integral(&(disc->thermal_zones[i].power_laws[nlaw]));
            I += dI;
        }
    }
    return I;
}

static double disc_partial_integral(const struct disc_t * const disc,
                                    const int nlaw,
                                    const double r0,
                                    const double r1)
{
    /*
     * As disc_integral between radii r0 and r1
     */
    Disc_zone_counter i;
    double I=0.0;
    int vb = 0;

    if(nlaw==POWER_LAW_TEMPERATURE)
    {
        /*
         * You should probably never integrate the temperature!
         */
        Exit_binary_c_no_stardata(
            2,
            "You should not try to integrate the temperature power law directly: use one of the ..._INTEGRAND power laws\n");
    }
    else if(Fequal(r1,r0))
    {
        /* radii equal */
    }
    else if(r1 > r0)
    {
        if(vb)printf("PARTIAL INTEGRAL from %20.10g to %20.10g\n",r0/R_SUN,r1/R_SUN);
        for(i=0; i<disc->n_thermal_zones; i++)
        {
            double dI;
            if(More_or_equal(disc->thermal_zones[i].rstart,r0) &&
               Less_or_equal(disc->thermal_zones[i].rend,r1))
            {
                /* zone is entirely inside the integral region */
                if(vb)printf("WHOLE ZONE %u from %g to %g\n",
                             i,
                             disc->thermal_zones[i].rstart/R_SUN,
                             disc->thermal_zones[i].rend/R_SUN
                    );
                dI = power_law_zone_integral(&(disc->thermal_zones[i].power_laws[nlaw]));
            }
            else if(Fequal(disc->thermal_zones[i].rstart/disc->thermal_zones[i].rend,1.0))
            {
                /* zero width */
                if(vb)printf("NO CONTRIBUTION from zone %u\n",i);
                dI = 0.0;
            }
            else if(r1 < disc->thermal_zones[i].rstart ||
                    r0 > disc->thermal_zones[i].rend)
            {
                /* integral region is outside the zone */
                if(vb)printf("NO CONTRIBUTION from zone %u\n",i);
                dI = 0.0;
            }
            else
            {
                const double r_in = Max(r0,disc->thermal_zones[i].rstart);
                const double r_out = Min(r1,disc->thermal_zones[i].rend);

                if(Fequal(r_in/r_out,1.0))
                {
                    if(vb)printf("NO CONTRIBUTION from zone %u\n",i);
                    dI = 0.0;
                }
                else
                {

                    if(vb)printf("PARTIAL ZONE %u from %20.10g to %20.10g == ? %d (limits = %20.10g,%20.10g, Rin=%20.10g, Rout=%20.10g, zone from %20.10g to %20.10g)\n",
                                 i,
                                 r_in/R_SUN,
                                 r_out/R_SUN,
                                 Fequal(r_in,r_out),
                                 r0/R_SUN,
                                 r1/R_SUN,
                                 disc->Rin/R_SUN,
                                 disc->Rout/R_SUN,
                                 disc->thermal_zones[i].rstart/R_SUN,
                                 disc->thermal_zones[i].rend/R_SUN);
                    dI = power_law_zone_integral_region(&(disc->thermal_zones[i].power_laws[nlaw]),
                                                        r_in,
                                                        r_out);
                }
            }
            if(vb)printf("dI = %20.10g\n",dI/M_SUN);
            I += dI;
            if(vb)printf("I = %20.10g\n",I/M_SUN);
        }
    }
    else
    {
        //printf("Partial integral : r0 = %30.20g > r1 = %30.20g error\n",r0,r1);
    }
    return I;
}


/************************************************************/

double Pure_function disc_total_mass(const struct disc_t * const disc)
{
    /*
     * Total mass in the disc (cgs).
     */
    return disc_integral(disc,POWER_LAW_MASS_INTEGRAND);
}

double Pure_function disc_partial_mass(const struct disc_t * const disc,
                                       const double r0,
                                       const double r1)
{
    /*
     * Partial mass integral (cgs).
     */
    return disc_partial_integral(disc,POWER_LAW_MASS_INTEGRAND,r0,r1);
}

double Pure_function disc_zone_mass(const struct disc_t * const disc,
                                    const int n)
{
    /*
     * Mass of zone n
     */
    const double r0 = disc->thermal_zones[n].Tlaw.R0;
    const double r1 = disc->thermal_zones[n].Tlaw.R1;
    return disc_partial_integral(disc,POWER_LAW_MASS_INTEGRAND,r0,r1);
}

double Pure_function disc_total_luminosity(const struct disc_t * const disc)
{
    /*
     * Bolometric disc luminosity (cgs), from both sides of
     * the disc.
     */
    return disc_integral(disc,POWER_LAW_LUMINOSITY_INTEGRAND);
}

double disc_partial_angular_momentum(const struct disc_t * const disc,
                                     const struct binary_system_t * const binary Maybe_unused,
                                     const double r0,
                                     const double r1)
{
    return disc_partial_integral(disc,POWER_LAW_ANGULAR_MOMENTUM_INTEGRAND,r0,r1);
}

double disc_zone_angular_momentum(const struct disc_t * const disc,
                                  const struct binary_system_t * const binary Maybe_unused,
                                  const int n)
{
    /*
     * Angular momentum in zone n
     */
    const double r0 = disc->thermal_zones[n].Tlaw.R0;
    const double r1 = disc->thermal_zones[n].Tlaw.R1;
    return disc_partial_integral(disc,POWER_LAW_ANGULAR_MOMENTUM_INTEGRAND,r0,r1);
}

double Pure_function disc_total_angular_momentum(const struct disc_t * const disc,
                                                 const struct binary_system_t * const binary Maybe_unused)
{
    /*
     * Total angular momentum in the disc (cgs)
     */
    return disc_integral(disc,POWER_LAW_ANGULAR_MOMENTUM_INTEGRAND);
}

double Pure_function disc_mean_specific_angular_momentum(const struct disc_t * const disc,
                                                         const struct binary_system_t * const binary Maybe_unused)
{
    /*
     * mean specific angular momentum in the disc (cgs)
     */
    return disc_total_angular_momentum(disc,binary) / disc_total_mass(disc);
}


double Pure_function disc_total_moment_of_inertia(const struct disc_t * const disc)
{
    /*
     * Total moment of inertial, assuming a thin disc (cgs)
     */
    return disc_integral(disc,POWER_LAW_MOMENT_OF_INERTIA_INTEGRAND);
}

double disc_total_gravitational_potential_energy(const struct disc_t * const disc,
                                                 const struct binary_system_t * const binary Maybe_unused)
{
    /*
     * Total gravitational potential energy in the disc
     */
    return disc_integral(disc,POWER_LAW_GRAVITATIONAL_POTENTIAL_ENERGY_INTEGRAND);
}

double Pure_function disc_total_kinetic_energy(const struct disc_t * const disc,
                                               const struct binary_system_t * const binary Maybe_unused)
{
    /*
     * Total kinetic energy in the disc
     */
    return disc_integral(disc,POWER_LAW_KINETIC_ENERGY_INTEGRAND);
}


double Pure_function disc_total_angular_momentum_flux(const struct disc_t * const disc,
                                                      const struct binary_system_t * const binary Maybe_unused)
{
    /*
     * Total angular momentum flux throughout the disc.
     *
     * In the absence of mass loss, this is given by the disc integral
     * over the angular momentum flux. The extra term, F_stripping_correction,
     * specifies the angular momentum required to remove material from the disc.
     */
    return
        disc_integral(disc,POWER_LAW_ANGULAR_MOMENTUM_FLUX_BINARY_INTEGRAND)
        +
        disc->F_stripping_correction;
}



double Pure_function disc_total_angular_momentum_constraint(const struct disc_t * const disc,
                                                            const struct binary_system_t * const binary Maybe_unused)
{
    /*
     * Angular momentum constraint: used only for debugging
     */
    return disc_integral(disc,POWER_LAW_ANGULAR_MOMENTUM_CONSTRAINT);
}

double Pure_function disc_mass_weighted_viscous_timescale(struct disc_t * const disc,
                                                          const struct binary_system_t * const binary Maybe_unused)
{
    return disc_integral(disc,POWER_LAW_MASS_WEIGHTED_VISCOUS_TIMESCALE);
}


#endif // DISCS
