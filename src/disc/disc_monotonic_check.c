#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef DISCS

Boolean disc_monotonic_check(struct stardata_t * const stardata,
                             struct disc_t * const disc,
                             struct binary_system_t * const binary,
                             const unsigned int n
    )
{
    /*
     * Check that the disc's M,J,F change monotonically in
     * the disc parameters.
     */
    double unknown Maybe_unused = 0.0;

    /*
     * Allocate memory for the checks
     */
    double *ref = Malloc(sizeof(double)*n);
    disc_parameter * parameters = Malloc(sizeof(disc_parameter)*n);
    disc_constraint * constraints = Malloc(sizeof(disc_constraint)*n);
#define Free_all                                \
    Safe_free(ref);                             \
    Safe_free(parameters);                      \
    Safe_free(constraints);                     \
    Safe_free(discwas);

    /*
     * Set the parameters that will be changed
     */
    parameters[0] = DISC_PARAMETER_TVISC0;
    parameters[1] = DISC_PARAMETER_RIN;
    parameters[2] = DISC_PARAMETER_ROUT;

    /*
     * Calculate pre-pertubation reference parameters
     */
    for(unsigned int i=0;i<n;i++)
    {
        ref[i] = Disc_parameter(disc,parameters[i]);
    }

    /*
     * Set up constraints;
     */
    constraints[0] = DISC_CONSTRAINT_M_TO_DISC_M;
    constraints[1] = DISC_CONSTRAINT_J_TO_DISC_J;
    constraints[2] = DISC_CONSTRAINT_F_TO_DISC_F;

    /*
     * Backup disc
     */
    struct disc_t * discwas = New_disc_from(disc);

    /*
     * Vary each in turn
     */
    const double eps = 1e-2;
    for(unsigned int i=0;i<n;i++)
    {
        /*
         * Perturb parameter i by -eps and +eps
         */
        const double d = eps * ref[i];
        double derivative = 0.0;
        for(int j=-1; j<=+1; j+=2)
        {
            /*
             * Restore disc
             */
            Copy_disc(discwas,disc);

            /*
             * Set parameters in the disc
             */
            Disc_parameter(disc,parameters[i]) = ref[i] + (double)j * d;

            /*
             * Calculate the new disc structure
             */
            double residual[n];
            int failure;
            disc_nsector2(stardata,
                          disc,
                          binary,
                          n,
                          constraints,
                          residual,
                          &failure);

            derivative =
                j == -1 ? Disc_constraint_value(disc,binary,constraints[i]) :
                j == +1 ? ((Disc_constraint_value(disc,binary,constraints[i]) - derivative)/(2.0*d)) :
                0.0;

            printf("PERTURB %u : %d : %30.15e : ",
                   i,j,Disc_parameter(disc,parameters[i]));
            printf("Residuals %g %g %g",
                   residual[0],
                   residual[1],
                   residual[2]
                );
            if(j==+1)
            {
                printf(", derivative %u = %g",i,derivative);
            }
            printf("\n");
        }
    }


#define __FULLRANGE
#define __LOG

    Boolean debug = FALSE;

#ifdef __FULLRANGE
    /*
     * check parameters over the full range
     */
    const int N = 100.0;
    double d[n],prev[n];
    for(unsigned int i=0;i<n;i++)
    {
        d[i] = 0.0;
    }

    for(unsigned int i=0;i<n;i++)
    {
        int sign[n];
        for(unsigned int k=0;k<n;k++)
        {
            sign[k] = 0;
        }

        const double logxmin = log10(Disc_parameter_min(disc,parameters[i]));
        const double logxmax = log10(Disc_parameter_max(disc,parameters[i]));
        const double dlogx = (logxmax - logxmin)/(1.0*(double)N);
        double logx = logxmin;
        int count = 0;
        while(logx < logxmax)
        {
            double x = exp10(logx);
            if(debug)printf("Set var %u = %g\n",i,x);
            /*
             * Restore disc
             */
            Copy_disc(discwas,disc);

            /*
             * Set parameter i in the disc
             */
            Disc_parameter(disc,parameters[i]) = x;

            /*
             * Calculate the new disc structure
             */
            double residual[n];
            int failure;
            disc_nsector2(stardata,
                          disc,
                          binary,
                          n,
                          constraints,
                          residual,
                          &failure);

            double c[n];
            for(unsigned int k=0;k<n;k++)
            {
                c[k] = Disc_constraint_value(disc,binary,constraints[k]);
                if(debug)printf("Set constraint %u = %30.15e\n",k,c[k]);
            }

            /*
             * If we have prev, use it
             */
            if(count>0)
            {
                for(unsigned int k=0;k<n;k++)
                {
                    /* derivative (to within a positive constant) */
                    d[k] = Abs_diff(c[k],prev[k]);

                    /* numerical error handling */
                    if(fabs(d[k]) < 1e-8)
                    {
                        d[k]=0.0;
                    }
                    if(debug)printf("Set derivative %u = %g (zero? %s)\n",
                                    k,
                                    d[k],
                                    Yesno(Is_really_zero(d[k])));
                }

                if(count == 1)
                {
                    /* first time : save the sign of the derivative */
                    for(unsigned int k=0;k<n;k++)
                    {
                        sign[k] = Sign_is_really_int(d[k]);
                        if(debug)printf("Set sign (first time) %u = %d\n",
                                        k,
                                        sign[k]);
                    }
                }
                else if(count>=2)
                {
                    /* subsequent times : check the sign is the same */
                    int newsign[n];
                    for(unsigned int k=0;k<n;k++)
                    {
                        newsign[k] = Sign_is_really_int(d[k]);
                        if(debug)printf("Set newsign %u = %d\n",k,newsign[k]);

                        if(sign[k] != 0 &&
                           newsign[k] != 0 &&
                           newsign[k] != sign[k])
                        {
                            Free_all;
                            Exit_binary_c(BINARY_C_BISECT_ERROR,
                                          "%u is Non-monotonic (sign now %d, was %d)\n",
                                          k,
                                          Sign_is_really_int(d[k]),
                                          sign[k]
                                );
                        }
                    }
                    if(debug)
                    {
                        printf("SIGNS ");
                        for(unsigned int k=0;k<n;k++)
                        {
                            printf("%d ",Sign_is_really_int(d[k]));
                        }
                        printf("\n");
                    }
                }
            }

#ifdef __LOG
            printf("FULLRANGE %u %30.15e %30.15e %30.15e %30.15e %g %g %g\n",
                   i,
                   x,
                   c[0],
                   c[1],
                   c[2],
                   fabs(residual[0]),
                   fabs(residual[1]),
                   fabs(residual[2])
                );
#endif
            logx += dlogx;

            count++;
            for(unsigned int k=0;k<n;k++)
            {
                prev[k] = c[k];
            }
        }
    }
#endif // __FULLRANGE

    /*
     * Restore the disc
     */
    Copy_disc(discwas,disc);

    Free_all;
    return TRUE;
}

#endif // DISCS
