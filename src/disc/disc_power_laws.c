/*
 * Power law API
 *
 * The power laws are defined inthe power_law_t structures,
 * which contain R0 and R1, the range of radii in which the power
 * law is valid, and A0 and A1, the power law values at R0 and R1,
 * respectively.
 * Also stored in the structure is the power law exponent.
 *
 * The subroutines here set up and modify these power_law_t
 * structures. Such modifications should NOT be made by
 * hand, please use the API functions which are well tested!
 *
 * Please see the comments in the subroutines below.
 */
#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef DISCS
void new_power_law(struct power_law_t * const p,
                   const double A0,
                   const double exponent,
                   const double R0,
                   const double R1)
{
    /*
     * Set a power law in *p from radii R0 to R1 with starting
     * value (at R0) of A0, with the given exponent.
     *
     * R1 may be unknown, in which case zero (0.0) should be passed in.
     * A1 is automatically calculated
     */
    p->A0 = A0;
    p->exponent = exponent;
    p->R0 = R0;
    p->R1 = R1;
    if(Is_really_not_zero(R1)) update_power_law(p,A0,0);
}

void update_power_law(struct power_law_t * const p,
                      const double A,
                      const int n)
{
    /*
     * Given a new An, set it and recalculate A(1-n),
     * where n=0 or 1
     */
    if(n==0)
    {
        p->A0 = A;
        p->A1 = power_law(p,p->R1);
    }
    else
    {
        p->A1 = A;
        p->A0 = A * pow(p->R0/p->R1,p->exponent);
    }
}

void new_related_power_law(const struct power_law_t * const old,
                           struct power_law_t * const new,
                           const double A0,
                           const double c,
                           const double m)
{
    /*
     * Make a new power law (*new) with its radii
     * taken from old power law (*old), but A0 set manually,
     * and new exponent (b) related to the old law (a)
     * by b = m*a + c
     */
    new->R0 = old->R0;
    new->R1 = old->R1;
    new->exponent = m * old->exponent + c;
    new->A0 = A0;
    update_power_law(new,A0,0);
}


void match_power_laws(const struct power_law_t * const in,
                      struct power_law_t * const out)
{
    /*
     * Match power law "out" to "in"
     */
    out->R0 = in->R1;
    out->A0 = in->A0 * pow(in->R1/in->R0,in->exponent);
    update_power_law(out,out->A0,0);
}

void match_power_laws_inwards(const struct power_law_t * const out,
                              struct power_law_t * const in)
{
    /*
     * Match power law "in" to "out"
     */
    in->R1 = out->R0;
    in->A1 = out->A0;
    update_power_law(in,in->A1,1);
}

double Pure_function power_law(const struct power_law_t * const p,
                               const double r)
{
    /*
     * Evaluate power_law p at r
     */
    return Is_really_zero(p->R0) ? 0.0 : (p->A0*pow(r/p->R0,p->exponent));
}

double Pure_function power_law_zone_integral(const struct power_law_t * const p)
{
    /*
     * Return the integral of a power law over its
     * region of validity
     */
    return power_law_zone_integral_region(p,p->R0,p->R1);
}

double Pure_function power_law_zone_integral_region(const struct power_law_t * const p,
                                                    const double in,
                                                    const double out)
{
    /*
     * Return the integral of a power law over
     * an arbitrary region
     */
    if(0)printf("Power law integral region %g < R < %g where A0 = %g : exponent=%g (1+exponent=%g)\n",
                in,out,p->A0,
                p->exponent,
                p->exponent+1.0);
    return p->A0 *
        pow(p->R0,-p->exponent)/(1.0 + p->exponent) *
        (pow(out,1.0+p->exponent) - pow(in,1.0+p->exponent));
}

double Pure_function power_law_crossing_radius(const struct power_law_t * const p0,
                                               const struct power_law_t * const p1)
{
    /*
     * Return the crossing radius of two power laws
     */
    return pow(p0->A0/p1->A0*
               pow(p0->R0,-p0->exponent)*
               pow(p1->R0, p1->exponent),
               1.0/(p1->exponent - p0->exponent));
}

#endif//DISCS
