#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef DISCS

void disc_mem_cleanup(struct stardata_t * const stardata)
{
    /* destroy all discs */
    remove_discs(stardata,
                 DISC_CIRCUMSTELLAR,
                 &stardata->star[0]);
    remove_discs(stardata,
                 DISC_CIRCUMSTELLAR,
                 &stardata->star[1]);
    remove_discs(stardata,
                 DISC_CIRCUMBINARY,
                 stardata);
}

#endif//DISCS
