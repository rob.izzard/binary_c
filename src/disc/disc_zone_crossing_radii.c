#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef DISCS

void disc_zone_crossing_radii(
    struct stardata_t * const stardata,
    struct disc_t  * const disc,
    const struct binary_system_t * const binary,
    double ** radii_p
    )
{
    /*
     * Set the crossing radii of disc in **radii.
     *
     * If the new zone list cannot be made, set *radii to NULL.
     */
    struct disc_thermal_zone_t * P =
        Malloc(sizeof(struct disc_thermal_zone_t)*
               (DISCS_MAX_N_ZONES));

    if(disc_new_zone_list(stardata,disc,binary,P) == DISC_ZONES_OK)
    {
        Disc_zone_counter i,j,current=0;

        /*
         * Allocate space for radii list: this should
         * be freed by the calling function
         */
        *radii_p = Malloc(sizeof(double)*DISCS_MAX_N_ZONES);
        double * const radii = *radii_p;

        /*
         * Default all radii to unphysical -1.0
         * and make all zones valid to start with
         */
        for(i=0;i<DISCS_MAX_N_ZONES;i++)
        {
            radii[i] = -1.0;
            P[i].valid = TRUE;
        }

        /*
         * The first radius should be the shortest
         */
        double r = DISC_LARGE_RADIUS;
        for(i=0;i<DISCS_MAX_N_ZONES;i++)
        {
            for(j=0;j<DISCS_MAX_N_ZONES;j++)
            {
                const double Rx = disc_Rcross(&P[i],&P[j]);
                if(Rx < r)
                {
                    r = Rx;
                    current = i;
                }
            }
        }
        P[current].valid = FALSE;
        //printf("Zone at 1cm %u\n",current);

        /*
         * Loop to find the next
         */
        Boolean valid = TRUE;
        Disc_zone_counter icount = 0;
        while(valid == TRUE)
        {
            double next_crossing_radius = DISC_LARGE_RADIUS;
            Disc_zone_counter next_crossing_index = current;
            Boolean found = FALSE;

            /*
             * Find the next crossing radius
             */
            for(i=0;i<DISCS_MAX_N_ZONES;i++)
            {
                /*
                printf("check %u : current? %s : valid? %s\n",
                       i,
                       Yesno(i==current),
                       Yesno(P[i].valid));
                */
                if(i != current &&
                   P[i].valid == TRUE)
                {
                    const double Rx = disc_Rcross(P + current,
                                                  P + i);
                    if(Rx > r &&
                       Rx < next_crossing_radius)
                    {
                        next_crossing_radius = Rx;
                        next_crossing_index = i;
                        found = TRUE;
                    }
                }
            }
            /*
            printf("found? %s : next X at Rx = %g with zone i = %u, icount = %u\n",
                   Yesno(found),
                   next_crossing_radius,
                   next_crossing_index,
                   icount);
            */
            if(found == TRUE)
            {
                /*
                 * Save to radii list
                 */
                radii[icount] = next_crossing_radius;
                //printf("set radius %u to %g\n",icount,radii[icount]);
                icount++;

                /*
                 * Make current zone invalid
                 */
                current = next_crossing_index;
                P[current].valid = FALSE;

                /*
                 * Stop looping if all zones are invalid
                 */
                valid = FALSE;
                for(i=0;i<DISCS_MAX_N_ZONES;i++)
                {
                    if(P[i].valid == TRUE)
                    {
                        valid = TRUE;
                    }
                }
            }
            else
            {
                valid = FALSE;
            }
        }
    }
    else
    {
        *radii_p = NULL;
    }

    Safe_free(P);
}

#endif
