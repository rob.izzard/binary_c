#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef DISCS

static void resonance_radii(const struct binary_system_t * binary,
                            const double l,
                            const double m,
                            double *r_corotation,
                            double *r_inner,
                            double *r_outer);

void cbdisc_eccentricity_pumping_rate(struct stardata_t * const stardata,
                                      struct disc_t * const disc,
                                      const struct binary_system_t * const binary,
                                      double * const dedt,
                                      double * const dJdt)
{
//#undef DISC_DEBUG
//#define DISC_DEBUG 2
    /*
     * Eccentricty pumping rate caused by
     * the presence of a circumbinary disc.
     *
     * Sets de/dt and dJorb/dt in CGS.
     */

    /*
     * Separation and eccentricity in CGS.
     */
#define a (binary->separation)
#define e (binary->eccentricity)
#define Jorb (binary->Jorb)

    Discdebug(2,
              "cbdisc eccentricity pumping method = %d : a = %g , e = %g, Jorb = %g\n",
              stardata->preferences->cbdisc_eccentricity_pumping_method,
              a,e,Jorb);

    if(Valid_eccentricity(e) == FALSE)
    {
        Discdebug(2,
                  "Cannot pump e = %g %g\n",
                  e,
                  stardata->common.orbit.eccentricity);
        *dedt = 0.0;
        *dJdt = 0.0;
        return;
    }

    if(stardata->preferences->cbdisc_eccentricity_pumping_method ==
       CBDISC_ECCENTRICITY_PUMPING_DERMINE)
    {
        /*
         * Lindblad-resonance pumping according to
         * Dermine et al. (2013)
         */
        double l = 1.0;
        double m = 2.0;
        double rc,Rres_in,Rres_out;
        resonance_radii(binary,l,m,&rc,&Rres_in,&Rres_out);

        double alpha = disc->alpha;
        double ethresh = sqrt(alpha) * 0.1;
        double deda = 0.0;

        /*
         * Require the half angular momentum radius
         */
        if(Is_zero(disc->RJhalf))
        {
            disc->RJhalf = disc_half_angular_momentum_radius(stardata,disc,binary);
        }

        double R = disc->RJhalf;

        /*
         * Binary system mass and reduced mass (cgs)
         */
        double Mb = binary->mtot;
        double Mmu = binary->reduced_mass;

        /*
         * Binary angular frequency (cgs)
         */
        double omega_binary = binary->omega;

        /* 1.0 - e * e : used often */
        double ee = 1.0 - e * e;

        /*
         * Rate of change of the separation because of interaction
         * with the circumbinary disc l=1, m=2 resonance.
         * Dermine eq. 7 (multiplied by a) in CGS
         */
        double dadt = -2.0 * (l/m) *
            (disc->M / Mmu) * alpha *
            Pow2(disc->HRJhalf) * (a/R) *
            omega_binary * a *
            stardata->preferences->cbdisc_resonance_multiplier;

        if(DISC_DEBUG)
        {
            Discdebug(1,"ECC PUMP radii Rc=%g Rres_in=%g Rres_out=%g\n",
                      rc,Rres_in,Rres_out);
            Discdebug(2,
                      "a=%g RJhalf=%g H/R(RJhalf)=%g\n",a,R,disc->HRJhalf);
            Discdebug(2,
                      "dadt = -2.0 * (l/m=)%g * (disc->M/Mmu=)%g * (alpha=)%g * (HRJhalf^2=)%g * (a/R)=%g * (omega_binary=)%g * (a=)%g = %g cm/s = %g Rsun/y\n",
                      l/m,
                      disc->M / Mmu,
                      alpha,
                      Pow2(disc->HRJhalf),
                      a/R,
                      omega_binary,
                      a,
                      dadt,
                      dadt / R_SUN * YEAR_LENGTH_IN_SECONDS
                );

            // Pattern speed (cgs)
            //double omega_pattern = l/m * omega_binary;
        }

        int using = -1;
        if(e > 0.7)
        {
            /* saturation : no e-pumping */
            deda = 0.0;
            using = 0;
            Discdebug(2,"e = %g > 0.7 : no pumping\n",e);
        }
        else if(e < ethresh)
        {
            /*
             * Dermine Eq 8
             * Weakly coupled l=m resonance.
             */
            deda = - 50.0 * e / (alpha * a);
            using = 8;
            Discdebug(2,"e = %g < ethresh = %g : weak coupling de/da = 50 * %g /(%g * %g) = %g\n",
                   e,
                   ethresh,
                   e,
                   alpha,
                   a,
                   deda);
        }
        else
        {
            /*
             * Strong disc-binary coupling.
             *
             * Dermine Eq 9 see also Fleming (2017, MNRAS 464, 3343)
             *
             * Note that the l=1, m=2 resonance which is the same
             * as the 1:3 EOLR (eccentric outer Lindblad resonance)
             */

            deda = 2.0 * ee / (e + alpha / (100.0 * e)) *
                (l/m - 1.0/sqrt(ee)) / a;
            using = 9;
            Discdebug(2,"Rin = %g < Rres_Out = %g : strong coupling e = %g -> de/da = %g\n",
                      disc->Rin,
                      Rres_out,
                      e,
                      deda);

        }

        if(stardata->preferences->cbdisc_resonance_damping == TRUE)
        {
            /*
             * Damp the resonance because the disc moves outside
             * the location of the resonance.
             */
            deda *= Pow3(Min(2.0*a/Max(TINY,disc->Rin),1.0));
            Discdebug(2,"Resonance damped : de/da = %g\n",deda);
        }

        /*
         * We have de/da : require de/dt so need da/dt = da/dJ * dJ/dt
         */
        *dedt = deda * dadt;
        Discdebug(2,"de/dt = (de/da = %g) * (da/dt = %g) = %g\n",
                  deda,
                  dadt,
                  *dedt);
        /*
         * Should be > 0
         */
        *dedt = Max(*dedt, 0.0);

        /*
         * Dermine Eq. 11 gives dJ/dt (CGS)
         * however this is probably just the same
         * as the binary torque which is already
         * in the model. Don't count it twice!
         */
        *dJdt = 0.0;// Jorb * (dadt/(2.0*a) - e * *dedt / ee);

        {
            Discdebug(2,
                      "e = %g : ethresh = %g : using %d :ee = %g : deda = %g : dadt = %g : dedt = %g : dJdt = %g\n",
                      e,ethresh,using,ee,deda,dadt,*dedt,*dJdt);
            Discdebug(2,"ART %g (4.3e-4), %g (1.9e-3)\n",
                      dadt/a/(omega_binary*disc->M/Mb),
                      *dedt*e/(omega_binary*disc->M/Mb)
                );
        }
    }
    else
    {
        *dedt = 0.0;
        *dJdt = 0.0;
    }


    Discdebug(2,"return DEDT %g DJDT %g\n",*dedt,*dJdt);


#ifdef NANCHECKS
    if(isnan(*dedt) || isnan(*dJdt))
    {
        Exit_binary_c(BINARY_C_EXIT_NAN,
                      "cbdisc ecc pumping: Either de/dt = %g or dJ/dt = %g is nan\n",
                      *dedt,
                      *dJdt);
    }
#endif //NANCHECKS

}

static void resonance_radii(const struct binary_system_t * binary,
                            const double l,
                            const double m,
                            double *r_corotation,
                            double *r_inner,
                            double *r_outer)
{
    /*
     * Calculate the radius at which a resonance occurs
     */
    *r_corotation = Pow2d3(  m       / l ) * binary->separation;
    *r_inner      = Pow2d3( (m - 1.0)/ l ) * binary->separation;
    *r_outer      = Pow2d3( (m + 1.0)/ l ) * binary->separation;
}



#endif // DISCS
