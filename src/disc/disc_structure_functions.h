#ifndef DISC_ADAM_STRUCTURE_H
#define DISC_ADAM_STRUCTURE_H


#include <gsl/gsl_errno.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_min.h>


static double disc_mass_radius_bisector(const double r,
                                        void * p);
static double disc_pressure_radius_bisector(const double r,
                                            void * p);
static double disc_angular_momentum_radius_bisector(const double r,
                                                    void *p);


#endif //  DISC_ADAM_STRUCTURE_H
