#include "../binary_c.h"

No_empty_translation_unit_warning;

#ifdef DISCS

void disc_make_derived_powerlaws(struct disc_t * const disc,
                                 const struct binary_system_t * const binary)
{
    /*
     * Calculate power law coefficients for power laws
     * other than temperature.
     *
     * e.g. Sigma (mass column density), and integrands
     * (functions which, when integrated, give something useful
     *  e.g. mass or angular momentum)
     */
    Disc_zone_counter i;
    static const char * pls[30] = POWER_LAW_STRINGS;

    /*
     * First loop sets up the inner A0
     * and exponents.
     */
    for(i=0;i<disc->n_thermal_zones;i++)
    {
        struct disc_thermal_zone_t * z = disc->thermal_zones+i;

        /*
         * T0, R0 and Sigma(R0) are used often, so calculate once
         */
        const double T0 = z->Tlaw.A0;
        const double R0 = z->Tlaw.R0;
        const double sigmaR0 =  disc->sigma0 / (T0 * Pow2(R0));

        /* Sigma(R) */
        new_related_power_law(&z->Tlaw,
                              &z->power_laws[POWER_LAW_SIGMA],
                              sigmaR0,
                              -2.0,
                              -1.0);

        /* mass integrand */
        new_related_power_law(&z->Tlaw,
                              &z->power_laws[POWER_LAW_MASS_INTEGRAND],
                              TWOPI * R0 * sigmaR0,
                              -1.0,
                              -1.0);


        /* angular momentum flux integrand, using the modified disc->torqueF */
        new_related_power_law(&z->Tlaw,
                              &z->power_laws[POWER_LAW_ANGULAR_MOMENTUM_FLUX_DISC_INTEGRAND],
                              PI * disc->torqueF * Pow4(binary->separation) * Pow2(binary->q) * GRAVITATIONAL_CONSTANT * binary->mtot  * (1.0/Pow4(R0)) * sigmaR0,
                              -6.0,
                              -1.0);

        /*
         * Ditto using the torqueF specified in the binary struct
         * which is a copy of the preferences value.
         */
        new_related_power_law(&z->Tlaw,
                              &z->power_laws[POWER_LAW_ANGULAR_MOMENTUM_FLUX_BINARY_INTEGRAND],
                              PI * binary->torqueF * Pow4(binary->separation) * Pow2(binary->q) * GRAVITATIONAL_CONSTANT * binary->mtot  * (1.0/Pow4(R0)) * sigmaR0,
                              -6.0,
                              -1.0);

        /* angular momentum flux constraint (used for debugging only) */
        new_related_power_law(&(z->Tlaw),
                              &(z->power_laws[POWER_LAW_ANGULAR_MOMENTUM_CONSTRAINT]),
                              disc->torqueF * Pow4(binary->separation) * Pow2(binary->q) * GRAVITATIONAL_CONSTANT * binary->mtot  * disc->mu * (1.0/Pow6(R0)) / (3.0 * disc->alpha * BOLTZMANN_CONSTANT * disc->gamma * T0),
                              -6.0,
                              -1.0);

        /* angular momentum integrand */
        new_related_power_law(&(z->Tlaw),
                              &(z->power_laws[POWER_LAW_ANGULAR_MOMENTUM_INTEGRAND]),
                              TWOPI * sqrt(GRAVITATIONAL_CONSTANT * binary->mtot * R0) * R0 * sigmaR0,
                              -0.5,
                              -1.0);

        /* luminosity integrand (for both sides) */
        new_related_power_law(&(z->Tlaw),
                              &(z->power_laws[POWER_LAW_LUMINOSITY_INTEGRAND]),
                              2.0 * TWOPI * STEFAN_BOLTZMANN_CONSTANT * Pow4(T0)  * R0,
                              1.0,
                              4.0);

        /* moment of inertia integrand */
        new_related_power_law(&(z->Tlaw),
                              &(z->power_laws[POWER_LAW_MOMENT_OF_INERTIA_INTEGRAND]),
                              TWOPI*Pow3(R0)*sigmaR0,
                              1.0,
                              -1.0);


        /* gravitational potential energy integrand */
        double energyC = PI * GRAVITATIONAL_CONSTANT * binary->mtot * sigmaR0;
        new_related_power_law(&(z->Tlaw),
                              &(z->power_laws[POWER_LAW_GRAVITATIONAL_POTENTIAL_ENERGY_INTEGRAND]),
                              -2.0 * energyC,
                              -2.0,
                              -1.0);

        /* kinetic energy integrand */
        new_related_power_law(&(z->Tlaw),
                              &(z->power_laws[POWER_LAW_KINETIC_ENERGY_INTEGRAND]),
                              energyC,
                              -2.0,
                              -1.0);

        /* mass-weighted viscous timescale */
        new_related_power_law(&(z->Tlaw),
                              &(z->power_laws[POWER_LAW_MASS_WEIGHTED_VISCOUS_TIMESCALE]),
                              2.0*PI*disc->mu*sqrt(GRAVITATIONAL_CONSTANT*binary->mtot)/
                              (5.0*disc->alpha*disc->gamma*BOLTZMANN_CONSTANT* disc->M)
                              *
                              sigmaR0 * pow(R0,1.5) / T0,
                              -1.5,
                              -1.0);

        if(DEBUG_DERIVED_POWER_LAWS)
        {
            if(i>0)
            {
                int j;
                for(j=0;j<6;j++)
                {
                    printf("PL%d (%20s) prevR1 = % 12g , thisR0 = % 12g ..... prevA0 = % 12g, prevA1 = % 12g : thisA0 = % 12g thisA1=% 12g\n",
                           j,
                           pls[j],
                           disc->thermal_zones[i-1].power_laws[j].R1,
                           disc->thermal_zones[i].power_laws[j].R0,
                           disc->thermal_zones[i-1].power_laws[j].A0,
                           disc->thermal_zones[i-1].power_laws[j].A1,
                           disc->thermal_zones[i].power_laws[j].A0,
                           disc->thermal_zones[i].power_laws[j].A1
                        );
                }
            }
        }
    }

    if(TEST_ZONE_CONTINUITY)
    {
        /*
         * Test for continuity in the integrands
         */
        for(i=0;i<disc->n_thermal_zones;i++)
        {
            struct disc_thermal_zone_t * z = disc->thermal_zones+i;
            struct disc_thermal_zone_t * prevz = i==0 ? NULL : disc->thermal_zones+i-1;
            const int vb=0;
            unsigned int j;
            for(j=0;j<DISCS_NUMBER_OF_POWER_LAWS;j++)
                //for(j=0;j<2;j++) // TODO check other power laws when they are repaired
            {
                if(vb)printf("test matching of power law %u (%s) in zone %u, R0=%g R1=%g\n",
                             j,
                             pls[j],
                             i,
                             z->power_laws[j].R0,
                             z->power_laws[j].R1
                    );
                if(vb)printf("A0=%g\n",z->power_laws[j].A0);
                if(i>0)
                {
                    if(vb)printf("prev A1=%g\n",prevz->power_laws[j].A1);
                    if(!Fequal(z->power_laws[j].A0/prevz->power_laws[j].A1,1.0))
                    {
                        Exit_binary_c_no_stardata(
                            2,
                            "Power law %u (%s) integrand boundary matching failed :(\n",
                            j,pls[j]);
                    }
                }
            }
        }
    }
}

#endif//DISCS
