#pragma once
#ifndef DISC_FUNCTION_MACROS_H
#define DISC_FUNCTION_MACROS_H


/* convert position to disc index for an existing disc */
#define Disc_pos_to_n(OBJ,POS)                          \
    ((POS) == DISC_INNER_EDGE ? 0 :                     \
     (POS) == DISC_OUTER_EDGE ? (OBJ)->ndiscs-1 :       \
     (POS))

/* convert position to disc index for a new disc */
#define Disc_newpos_to_n(OBJ,POS)               \
    ((POS) == DISC_INNER_EDGE ? 0 :             \
     (POS) == DISC_OUTER_EDGE ? (OBJ)->ndiscs : \
     (POS))

#define Discloop(OBJ,A)                         \
    for((A)=0;(A)<(OBJ)->ndiscs;(A)++)



#define Convergence_status(S)                                   \
    (                                                           \
        (S) == DISC_CONVERGENCE_FAILED ? "Failed" :                  \
        (S) == DISC_CONVERGENCE_SUCCEEDED ? "Success" :              \
        (S) == DISC_CONVERGENCE_FLUX_NEGATIVE ? "Flux negative" :    \
        "Unknown"                                               \
        )



/*
 * Macro to determine whether a disc "really exists".
 *
 * This can often be used to determine whether other
 * functions, like integrals over the disc, will work.
 */
#define Disc_is_disc(D)                         \
    (                                           \
        Is_nonzero_number((D)->M)               \
        &&                                      \
        Is_nonzero_number((D)->J)               \
        &&                                      \
        (D)->lifetime < DISC_MAX_LIFETIME       \
        )



#define Disc_removal_string(S)                                          \
    (                                                                   \
        (S)==DISC_REMOVE_FORMED_RING ? "Formed ring" :                  \
        (S)==DISC_REMOVE_REPLACE ? "Replaced" :                         \
        (S)==DISC_REMOVE_EVAPORATE ? "Evaporated" :                     \
        (S)==DISC_REMOVE_ALL ? "Remove all" :                           \
        (S)==DISC_REMOVE_FIRST_TIMESTEP_FAILED ? "Fire timestep failed" : \
        "Unknown"                                                       \
        )


/* solvers */
#define Solver_string(N) (                              \
        (N)==(DISC_SOLVER_BISECTION) ? "Bi" :           \
        (N)==(GSL_MULTIROOT_FSOLVER_HYBRIDS) ? "Hs" :   \
        (N)==(GSL_MULTIROOT_FSOLVER_HYBRID)  ? "H"  :   \
        "?"                                             \
        )


#define Show_disc_derivative(N)                                         \
        Discdebug(1,                                                    \
                  "d(%20s)/dt = M % 20g (Msun/y), J % 20g (cgs), e % 20g (s^-1)\n", \
                  Disc_derivative_string(N),                            \
                  disc->loss[(N)].mdot / M_SUN * YEAR_LENGTH_IN_SECONDS, \
                  disc->loss[(N)].jdot,                                 \
                  disc->loss[(N)].edot);


#define Clear_disc_feedback(D)                  \
    {                                           \
        (D)->dM_ejected =                       \
            (D)->dM_binary =                    \
            (D)->dJ_binary =                    \
            (D)->de_binary = 0.0;               \
    }

#define Disc_derivative_string(A) (                                 \
        (A) == DISC_LOSS_GLOBAL ? "Global" :                        \
        (A) == DISC_LOSS_INNER_VISCOUS ? "Viscous" :                \
        (A) == DISC_LOSS_INNER_L2_CROSSING ? "L2 crossing":         \
        (A) == DISC_LOSS_FUV ? "FUV" :                              \
        (A) == DISC_LOSS_XRAY ? "X-ray" :                           \
        (A) == DISC_LOSS_BINARY_TORQUE ? "Binary torque" :          \
        (A) == DISC_LOSS_ISM ? "ISM ram stripping" :                \
        (A) == DISC_LOSS_RESONANCES ? "Resonances" :                \
        (A) == DISC_LOSS_NEGATIVE_FLUX ? "Negative flux" :          \
        (A) == DISC_LOSS_INNER_EDGE_STRIP ? "Inner edge strip" :    \
        (A) == DISC_LOSS_OUTER_EDGE_STRIP ? "Outer edge strip" :    \
        (A) == DISC_LOSS_INNER_EDGE_SLOW ? "Inner edge slow" :      \
        (A) == DISC_LOSS_OUTER_EDGE_SLOW ? "Outer edge slow" :      \
        "Unknown" )


/* macro to define mass loss from the inner or outer edge */
#define Disc_loss_strip_from_edge(I)                  \
    ((I)==DISC_LOSS_INNER_EDGE_STRIP ||               \
     (I)==DISC_LOSS_OUTER_EDGE_STRIP)


/*
 * ... and a string to log them.
 */
#define Disc_logger(A) (                                                \
        (A) == DISC_LOG_EVOLVE_DISC_STRUCTURE ? "disc_evolve_disc_structure" : \
        (A) == DISC_LOG_EVOLVE_DISC_STRUCTURE2 ? "disc_evolve_disc_structure (2)" : \
        (A) == DISC_LOG_EVERY_TIMESTEP ? "log_every_timestep" :         \
        "Unknown")



/*
 * Copy disc from source S to destination D
 */
#define Copy_disc(S,D) memcpy((D),(S),sizeof(struct disc_t));

/*
 * New disc copied from another
 */
#define New_disc_from(D) Copy_disc((D),New_disc)

#define Evaporate_disc(D,REASON)                                        \
    (D)->M = 0.0;                                                       \
    (D)->J = 0.0;                                                       \
    (D)->dt = LONG_TIMESTEP;                                            \
    (D)->converged = FALSE;                                             \
    if(stardata->preferences->cbdisc_end_evolution_after_disc == TRUE)  \
    {                                                                   \
        stardata->model.evolution_stop = TRUE;                          \
    }                                                                   \
    if(DISC_DEBUG)                                                      \
        printf("Disc evaporated at line %d in file %s because %s\n",    \
               __LINE__,                                                \
               __FILE__,                                                \
               REASON);                                                 \
/* disc evaporated */

#define Disc_timestep_limit_string(X)                           \
    (                                                           \
        (X) == DISC_TIMESTEP_LIMIT_M ? "M" :                    \
        (X) == DISC_TIMESTEP_LIMIT_J ? "J" :                    \
        (X) == DISC_TIMESTEP_LIMIT_F ? "F" :                    \
        (X) == DISC_TIMESTEP_LIMIT_MIN ? "Min" :                \
        (X) == DISC_TIMESTEP_LIMIT_MAX ? "Max" :                \
        (X) == DISC_TIMESTEP_LIMIT_TOO_FAST ? "Too fast" :      \
        "Unknown"                                               \
        )


#define Disc_Mdot(X) (Max(0.0,-disc->loss[(X)].mdot / M_SUN * YEAR_LENGTH_IN_SECONDS))
#define Disc_Jdot(X) (Max(0.0,-disc->loss[(X)].jdot))


/*
 * scale height ratio macro : takes in or out as an argument
 */
#define Disc_scale_height_ratio(X)              \
    (                                           \
        Is_zero(disc->R##X ) ||                 \
        Is_zero(disc->R##X) ||                  \
        Fequal(disc->Rin,disc->Rout))           \
    ? 0.0 :                                     \
    (disc_scale_height(disc->R##X,              \
                       disc,                    \
                       b)/                      \
     disc->R##X                                 \
        )


#endif // DISC_FUNCTION_MACROS_H
