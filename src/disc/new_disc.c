#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef DISCS
#include "disc_constraints.h"

struct disc_t * new_disc(
    struct stardata_t * const stardata,
    const Disc_type type,
    void * object,
    const int pos
    )
{
    /*
     * Make a new disc either around the "object", a star (star_t)
     * or the binary (stardata_t).
     *
     * pos is the disc number in the list, or DISC_INNER_EDGE, which
     * sets it to zero, or DISC_OUTER_EDGE, which sets it to the final
     * disc in the list.
     *
     * This routine allocates memory for the disc, it does not
     * set any properties of the disc.
     *
     * Returns
     * 1) a pointer to the new disc structure, if a new disc can be made
     * 2) NULL if a disc is already in place.
     *
     * Exits on malloc failure.
     *
     * Beware that if the disc is removed, the pointer returned will not
     * automatically be set to NULL. If you rely on such pointers being
     * persistent, you'll be in dire trouble. Instead, just get the disc
     * from the object->discs list.
     */
    void * ret = NULL;
    if(type == DISC_CIRCUMSTELLAR)
    {
        struct star_t * const star = object;
        star->discs = Realloc(star->discs, (sizeof (struct disc_t)) * (star->ndiscs+1));
        if(star->discs==NULL)
        {
            Exit_binary_c_no_stardata(
                BINARY_C_ALLOC_FAILED,
                "Memory allocation (Realloc) of a new circumstellar disc pointer failed");
        }
        const int n = Disc_newpos_to_n(star,pos);
        int i;
        for(i=star->ndiscs-1; i>=n; i--)
        {
            star->discs[i+1] = star->discs[i];
        }
        star->discs[n] = Calloc(1,sizeof(struct disc_t));
        if(star->discs[n]==NULL)
        {
            Exit_binary_c_no_stardata(
                BINARY_C_ALLOC_FAILED,
                "Memory allocation (calloc) of a new circumstellar disc structure failed");

        }
        star->discs[n]->type = type;
        ret = star->discs[n];
        star->ndiscs++;
    }
    else if(type == DISC_CIRCUMBINARY)
    {
        struct stardata_t * const _stardata = object;
        struct common_t * const common = &(_stardata->common);
        int n = Disc_newpos_to_n(common,pos);
        if(n < NDISCS)
        {
            struct disc_t * disc = &common->discs[n];
            disc->type = type;
            disc->delete_count = 0;
            disc->firstlog = TRUE;
            disc->first = TRUE;
            disc->lifetime = 0.0;
            ret = &(common->discs[common->ndiscs]);
            common->ndiscs++;
        }
        else
        {
            /*
             * There is no space for a new disc, which means
             * the disc already exists. Usually we return
             * a NULL pointer so the function calling
             * knows to append to the disc (or otherwise).
             * However, if the disc has a non-zero delete_count
             * then it is actually waiting to be deleted.
             * In that case, delete it, then return a new disc.
             *
             * While it is very unlikely that the above will
             * happen (it requires a common envelope ejection
             * on the same timestep as a previously set up disc
             * is evaporated) it has been known!
             */
            n--;
            struct disc_t * disc = &common->discs[n];
            if(disc->delete_count != 0)
            {
                /*
                 * Remove and replace old disc with a new disc
                 */
                remove_disc(stardata,
                            disc->type,
                            _stardata,
                            n,
                            DISC_REMOVE_REPLACE);
                common->ndiscs++;
                disc->type = type;
                disc->delete_count = 0;
                ret = disc;
            }
            else
            {
                ret = NULL;
            }
        }
        if(ret)
        {
            /*
             * Set the structural parameters of the disc
             * to the defaults from stardata->preferences
             */
            struct disc_t * cbdisc;
            cbdisc = (struct disc_t *) ret;
            cbdisc->alpha = stardata->preferences->cbdisc_alpha;
            cbdisc->gamma = stardata->preferences->cbdisc_gamma;
            cbdisc->torqueF = stardata->preferences->cbdisc_torquef;
            cbdisc->kappa = stardata->preferences->cbdisc_kappa;
            cbdisc->albedo = stardata->preferences->cbdisc_albedo;
        }
    }
    else
    {
        Exit_binary_c_no_stardata(
            BINARY_C_UNKNOWN_DISC_TYPE,
            "Disc type %d unknown : cannot allocate memory for it",
            type);
    }

    return ret;
}

#endif // DISCS
