#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef DISCS

double disc_calc_natural_timescale(const struct stardata_t * const stardata,
                                   struct disc_t * const disc,
                                   const struct binary_system_t * const binary,
                                   const double dt,
                                   unsigned int * const why)
{
    /*
     * Calculate natural timescales for M, J and e changes.
     */
    disc_natural_timescales(stardata,
                            disc,
                            binary,
                            dt);

    /*
     * Hence the shortest timescale is the disc's
     * natural timescale
     */
    double dt_natural = Which_Min3(*why,
                                   disc->t_m,
                                   disc->t_j,
                                   disc->t_e);

    Discdebug(1,
              "disc natural timescale = %g y (from %s)\n",
              dt_natural/YEAR_LENGTH_IN_SECONDS,
              (*why==0 ? "dM/dt" :
               *why==1 ? "dJ/dt" :
               *why==2 ? "de/dt" :
               "?")
        );

    return dt_natural;
}

#endif // DISCS
