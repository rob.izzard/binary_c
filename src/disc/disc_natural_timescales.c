#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef DISCS

#define Timescale(VAR,RATE)                     \
    (                                           \
        Is_really_zero(RATE) ?                  \
        DISC_LONG_TIME :                        \
        1.0/Max(1e-20,fabs((RATE) / (VAR)))     \
        )
#define Absolute(X) (fabs(X))

void disc_natural_timescales(const struct stardata_t * const stardata,
                             struct disc_t * const disc,
                             const struct binary_system_t * const binary,
                             const double dt)
{
    /*
     * Timescales associated with
     * mass and angular momentum changes
     */
    int i;
    double mdot=0.0,jdot=0.0,edot=0.0,mdot_slow=0.0;

    /*
     * The question here is whether to resolve
     * the absolute timescales, or the net?
     *
     * Net (no fabs) is faster ... but absolute (with fabs)
     * guarantees resolution of any changes.
     *
     * Generally, mass loss dominates so it doesn't matter,
     * so choose the absolute (fabs) option.
     */

    for(i=0;i<DISC_LOSS_N;i++)
    {
        mdot += Absolute(disc->loss[i].mdot);
        jdot += Absolute(disc->loss[i].jdot);
        edot += Absolute(disc->loss[i].edot);
        Show_disc_derivative(i);
    }

    /*
     * Slow (wind-like) mass loss
     */
    mdot_slow =
        Absolute(disc->loss[DISC_LOSS_GLOBAL].mdot)+
        Absolute(disc->loss[DISC_LOSS_INNER_VISCOUS].mdot)+
        Absolute(disc->loss[DISC_LOSS_XRAY].mdot)+
        ((stardata->preferences->cbdisc_outer_edge_stripping == FALSE) ? Absolute(disc->loss[DISC_LOSS_ISM].mdot) : 0.0);


    Discdebug(1,
              "MDOT timestep disc %g g/s = %g Msun/y -> timescale = %g (Msun) / %g (Msun/y) = %g y\n",
              mdot,
              mdot * YEAR_LENGTH_IN_SECONDS / M_SUN,
              disc->M / M_SUN,
              mdot * YEAR_LENGTH_IN_SECONDS / M_SUN,
              disc->M / (mdot * YEAR_LENGTH_IN_SECONDS)
        );
    Discdebug(1,
              "JDOT disc dJ/dt = %g with -> timescale = %g (cgs) / %g (cgs/y) = %g y\n",
              jdot,
              disc->J,
              (jdot * YEAR_LENGTH_IN_SECONDS),
              disc->J / (jdot * YEAR_LENGTH_IN_SECONDS)
        );

    /*
     * eccentricity must be > 0, so
     * if edot * dt reduces the eccentricity
     * below zero, don't do anything
     */
    if(binary->eccentricity + edot * dt < 0.0)
    {
        edot = 1e-50;
    }
    else
    {
        Discdebug(1,
                  "EDOT disc %g cf e = %g, de/dt [sum = %g : GR = %g, tid = %g, winds = %g, cbdisc = %g]\n",
                  edot,
                  binary->eccentricity,
                  stardata->model.derivative[DERIVATIVE_ORBIT_ECCENTRICITY],
                  stardata->model.derivative[DERIVATIVE_ORBIT_ECCENTRICITY_GRAVITATIONAL_RADIATION],
                  stardata->model.derivative[DERIVATIVE_ORBIT_ECCENTRICITY_TIDES],
                  stardata->model.derivative[DERIVATIVE_ORBIT_ECCENTRICITY_WINDS],
                  stardata->model.derivative[DERIVATIVE_ORBIT_ECCENTRICITY_CBDISC]
            );

        if(stardata->model.derivative[DERIVATIVE_ORBIT_ECCENTRICITY] < 0.0)
        {
            /*
             * If tidal circularization dominates,
             * ignore cbdisc eccentricity pumping here.
             */
            edot = 0.0;
        }
        else
        {
            /*
             * Resolve eccentricity pumping, remember to
             * convert de/dt to s^-1
             */
            edot = stardata->model.derivative[DERIVATIVE_ORBIT_ECCENTRICITY_CBDISC] /
                YEAR_LENGTH_IN_SECONDS;
        }
    }

    /*
     * Hence the timescales.
     */
    disc->t_m = Timescale(disc->M,mdot);
    disc->t_j = Timescale(disc->J,jdot);
    disc->t_e = Timescale(binary->eccentricity,edot);
    disc->t_m_slow = Timescale(disc->M,mdot_slow);

    Discdebug(1,
              "natural timescales: t=%30.12e y tM=%g tJ=%g te=%g years (M = %g Msun, mdot = %g Msun/y, jdot = %g; edot = %g; stardata = %g; e = %g)\n",
                      disc->lifetime/YEAR_LENGTH_IN_SECONDS,
                      disc->t_m/YEAR_LENGTH_IN_SECONDS,
                      disc->t_j/YEAR_LENGTH_IN_SECONDS,
                      disc->t_e/YEAR_LENGTH_IN_SECONDS,

              disc->M/M_SUN,
              mdot * YEAR_LENGTH_IN_SECONDS / M_SUN,
              jdot,
              edot,
              stardata->model.derivative[DERIVATIVE_ORBIT_ECCENTRICITY],
              stardata->common.orbit.eccentricity
        );

}

#endif // DISCS
