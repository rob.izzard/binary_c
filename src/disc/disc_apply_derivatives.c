#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef DISCS

/*
 * Apply various derivatives to change
 * the disc's mass, angular momentum, etc.
 *
 * Note that changes to the binary are summed
 * over the timesteps ddt and converted to derivatives
 * elsewhere prior to handing back to the main binary_c
 * time-evolution code (this is done because the disc code
 * uses its own timesteps, here ddt, rather than the
 * binary_c timestep).
 *
 * The sums are stored in cgs.
 *
 * The edge-stripping algorithm is NOT applied here, this is
 * done immediately after a converged structure is made in
 * disc_calc_disc_structure().
 */

void disc_apply_derivatives(struct disc_t * const disc,
                            const struct binary_system_t * const binary,
                            struct stardata_t * const stardata,
                            const double ddt,
                            Boolean * const success)
{
    static const char ** ds = disc_loss_strings;
    *success = TRUE;

    if(Disc_is_disc(disc))
    {
        /*
         * disc->dT is the time over which the disc
         * evolves during which the quantities stored here
         * should act. NB this may well be (much) shorter
         * than a binary_c timestep. If this is not taken
         * into account, your stars will accrete far more
         * than they should...
         */
        disc->dT += ddt;
    }
    else
    {
        disc->M = 0.0;
        disc->J = 0.0;
    }

    if(Disc_is_disc(disc))
    {
        int i;
        double mdot = 0.0;
        for(i=0;i<DISC_LOSS_N;i++)
        {
            if(!Disc_loss_strip_from_edge(i))
            {
                mdot += disc->loss[i].mdot;
            }
        }

        /*
         * Limit changes to at most 10% of the disc mass
         */
        const double dM_max = disc->M * 0.1;

        /*
         * Update the mass of the disc
         */
        const double dM = Min(dM_max,Max(-dM_max,mdot * ddt));
        disc->M = Max(0.0, disc->M + dM);

        /*
         * Mass that falls onto the binary
         */

        disc->dM_binary +=
            Min(dM_max,
                -ddt * (
                    disc->loss[DISC_LOSS_INNER_VISCOUS].mdot +
                    disc->loss[DISC_LOSS_INNER_L2_CROSSING].mdot
                    ));

#ifdef ___UNLIKELY
        if(0)
            printf("DMBIN substep ddt=%g : integrated dM=%g Msun in dT=%g y, rate = %g Msun/y (ddt = %g y, disc->dT = %g y, stardata dt = %g y) disc->M = %g Msun\n",
                   ddt/YEAR_LENGTH_IN_SECONDS,
                   disc->dM_binary/M_SUN,
                   disc->dT/YEAR_LENGTH_IN_SECONDS,
                   (disc->dM_binary * YEAR_LENGTH_IN_SECONDS) / (disc->dT * M_SUN),
                   ddt/YEAR_LENGTH_IN_SECONDS,
                   disc->dT/YEAR_LENGTH_IN_SECONDS,
                   stardata->model.dtm * 1e6,
                   disc->M/M_SUN
                );
#endif
        /*
         * Mass that is ejected from the system
         */
        disc->dM_ejected +=
            Max(-dM_max,
                ddt * (
                    disc->loss[DISC_LOSS_GLOBAL].mdot +
                    disc->loss[DISC_LOSS_ISM].mdot +
                    disc->loss[DISC_LOSS_FUV].mdot +
                    disc->loss[DISC_LOSS_XRAY].mdot +
                    disc->loss[DISC_LOSS_INNER_EDGE_STRIP].mdot +
                    disc->loss[DISC_LOSS_OUTER_EDGE_STRIP].mdot
                    ));
    }

    /*
     * check for evaporation
     */
    Discdebug(2,"check for evaporation : M = %g\n",disc->M);

    if(!Disc_is_disc(disc))
    {
        Append_logstring(LOG_DISC,
                         "Evaporate disc (M==0) age %g y",
                         disc->lifetime/YEAR_LENGTH_IN_SECONDS);
        Discdebug(1,
                  "Disc has evaporated because its mass is %g (= %g Msun)!\n",disc->M,disc->M/M_SUN);
        *success = FALSE;
        disc->J = 0.0;
    }
    else
    {
        /*
         * Apply angular momentum changes
         */
        {
            Discdebug(2,"apply angmom changes\n");
            int i;
            double jdot = 0.0;
            for(i=0;i<DISC_LOSS_N;i++)
            {
                /*
                 * Do not apply for inner and outer edge
                 * stripping : these are dealt with by applying
                 * new disc bounaries (at the top of this subroutine).
                 */
                if(!Disc_loss_strip_from_edge(i))
                {
                    jdot += disc->loss[i].jdot;

                    Discdebug(2,"DD %3d %30s = Mdot=% 15g (g/s) Jdot=% 15g edot=% 15g : tM=% 15g tJ=% 15g te=% 15g y\n",
                              i,
                              ds[i],
                              disc->loss[i].mdot,
                              disc->loss[i].jdot,
                              disc->loss[i].edot,
                              fabs(disc->M/disc->loss[i].mdot),
                              fabs(disc->J/disc->loss[i].jdot),
                              fabs(binary->eccentricity/disc->loss[i].edot)
                        );
                }
            }
            double dj = jdot * ddt;
            const double newJ = disc->J + dj;
            Discdebug(2,"NEWJ %g\n",newJ);

            if(newJ < 0.0)
            {
                /*
                 * Panic: J<0. instead, reduce J by 10%
                 */
                dj = - 0.1 * disc->J;
            }

            disc->J = Max(0.0, disc->J + dj);
            disc->dJ_ejected += dj;

            /*
             * Net binary orbit angular momentum derivative:
             * this is:
             *
             * 1) the torque applied to the disc (negative for the binary)
             * 2) gain from inflow (positive for the binary)
             * 3) loss from resonances
             * 4) gain from L2 crossing material
             *
             * The sign should be the negative of the sign for the disc.
             */
            disc->dJ_binary += - ddt *
                (
                    disc->loss[DISC_LOSS_INNER_VISCOUS].jdot +
                    disc->loss[DISC_LOSS_INNER_L2_CROSSING].jdot +
                    disc->loss[DISC_LOSS_BINARY_TORQUE].jdot +
                    disc->loss[DISC_LOSS_RESONANCES].jdot +
                    0.0
                    );
        }


        {
            /*
             * Central binary eccentricity pumping
             * (units s^-1)
             */
            disc->de_binary += ddt *
                disc->loss[DISC_LOSS_RESONANCES].edot;
            Discdebug(2,
                      "EDOTBIN %g = %g * %g\n",
                      disc->de_binary,
                      ddt,
                      disc->loss[DISC_LOSS_RESONANCES].edot);
        }


        /*
         * Convert to real derivatives
         */
        if(disc->dT > TINY)
        {

            /*
             * The derivatives are only valid while the disc is alive.
             * This is taken into account during the derivative
             * application process (with its factor ftdisc).
             */
            disc->Mdot_ejected = disc->dM_ejected / disc->dT;
            disc->Jdot_ejected = disc->dJ_ejected / disc->dT;
            disc->Mdot_binary = disc->dM_binary / disc->dT;
            disc->Jdot_binary = disc->dJ_binary / disc->dT;
            disc->edot_binary = disc->de_binary / disc->dT;
#ifdef ___DISABLED
            if(0)
                printf("disc dMej/dt=%g dMbin/dt=%g Msun/y for dT=%g y, edot_bin=%g\n",
                       disc->Mdot_ejected * YEAR_LENGTH_IN_SECONDS / M_SUN,
                       disc->Mdot_binary * YEAR_LENGTH_IN_SECONDS / M_SUN,
                       disc->dT / YEAR_LENGTH_IN_SECONDS,
                       disc->edot_binary
                    );
            printf("disc Mdot binary (timestep) %g\n",disc->Mdot_binary);
#endif
        }
    }
}

#endif //DISCS
