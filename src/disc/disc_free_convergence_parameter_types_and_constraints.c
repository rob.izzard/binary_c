#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef DISCS

void disc_free_convergence_parameter_types_and_constraints(disc_parameter ** parameter_types_p,
                                                                  disc_constraint ** constraints_p)
{
    /*
     * Free the memory allocated in
     * disc_setup_convergence_parameter_types_and_constraints
     */
    Safe_free(*parameter_types_p);
    Safe_free(*constraints_p);
}

#endif // DISCS
