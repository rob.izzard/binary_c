#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef DISCS

#include "disc_constraints.h"
#include "disc_boundary_conditions.h"

/*
 * Macro to set up of parameter_types, constraints and boundary
 * conditions, and update the _p pointers
 */
#define Add_constraint(N,PARAMETER,CONSTRAINT)                          \
    {                                                                   \
        Discdebug(2,                                                    \
                  "Add constraint %d : %d = %s, parameter %d = %s : constraints = %p, parameter_types = %p\n", \
                  (N),                                                  \
                  (CONSTRAINT),                                         \
                  Disc_constraint_string(CONSTRAINT),                   \
                  (PARAMETER),                                          \
                  Disc_parameter_string(PARAMETER),                     \
                  (void*)constraints,                                   \
                  (void*)parameter_types);                              \
        parameter_types = Realloc(parameter_types,                      \
                                  sizeof(disc_parameter) * ((N)+1));    \
        constraints = Realloc(constraints,                              \
                              sizeof(disc_constraint) * ((N)+1));       \
        parameter_types[(N)] = (PARAMETER);                             \
        constraints[(N)] = (CONSTRAINT);                                \
        *parameter_types_p = parameter_types;                           \
        *constraints_p = constraints;                                   \
        (N)++;                                                          \
    }



int disc_setup_convergence_parameter_types_and_constraints(
    struct stardata_t * const stardata Maybe_unused,
    struct disc_t * const disc,
    const struct binary_system_t * const binary Maybe_unused,
    disc_parameter ** parameter_types_p,
    disc_constraint ** constraints_p)
{
    int n = 0;

    /*
     * Given the nature of the disc set boundary conditions
     * and constraints.
     *
     * Returns the number of constraints, n, which is the same
     * as the number of parameter_types.
     */

    /*
     * Pointers to the constraints and parameter_types.
     * These start NULL and calls to Realloc populate them.
     */
    disc_constraint * constraints = NULL;
    disc_parameter * parameter_types = NULL;

    /*
     * Mass must always be conserved
     */
    Add_constraint(n,
                   DISC_PARAMETER_TVISC0,
                   DISC_CONSTRAINT_M_TO_DISC_M);
    Discdebug(2,"CONSTRAINT: M always constrained by disc->M\n");

    /*
     * Choose inner boundary condition
     */
    disc_boundary_condition inner_bc Maybe_unused;
    if(Is_not_zero(disc->Revap_in))
    {
        inner_bc = DISC_BOUNDARY_CONDITION_INNER_REVAP_IN;
        /*
         * Rin is just specified : we have no parameter
         */
        Discdebug(2,"CONSTRAINT: Rin specified\n");
    }
    else
    {
        Discdebug(2,"CONSTRAINT: Rin constrained, add F boundary condition\n");
        inner_bc = DISC_BOUNDARY_CONDITION_INNER_BINARY_TORQUE;
        Add_constraint(n,
                       DISC_PARAMETER_RIN,
                       DISC_CONSTRAINT_F_TO_DISC_F);
    }

    /*
     * Choose outer boundary condition
     */
    disc_boundary_condition outer_bc Maybe_unused;
    if(Is_not_zero(disc->Revap_out))
    {
        /*
         * Rout is just specified: we have no parameter
         */
        outer_bc = DISC_BOUNDARY_CONDITION_OUTER_REVAP_OUTER;
        Discdebug(2,
                  "CONSTRAINT: Rout specified by Revap_out (=%g Rsun)\n",
                  disc->Revap_out/R_SUN);
    }
    else
    {
        Discdebug(2,"CONSTRAINT: Rout constrained, add J boundary condition\n");
        Add_constraint(n,
                       DISC_PARAMETER_ROUT,
                       DISC_CONSTRAINT_J_TO_DISC_J);
    }


    /* todo : implement disc torque if we need to adapt this */


    Discdebug(1,"Made n = %d constraints/parmeters\n",n);

    /* return number of constraints/parameter_types */
    return n;
}
#endif //DISCS
