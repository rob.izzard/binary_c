#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef DISCS
#include "disc_constraints.h"

#ifdef __HAVE_VALGRIND__
#include <valgrind/valgrind.h>
#endif //__HAVE_VALGRIND__
/*
 * Function to be minimized by GSL routines
 * such that we form a disc.
 *
 * In the function's normal use as a trisector,
 * the three constraints are M, J and F,
 * the mass, angular momentum and angular momentum flux,
 * respectively, where disc->M, disc->J and disc->F are
 * set elsewhere.
 *
 * n is the number of parameter_types passed in. These
 * are in params[].
 *
 * n is also the number of contraints, and the array
 * constraints[], of size n, tells us which constaints
 * they are. This function then implements the constraints.
 *
 * parameter_value contains the values of each parameter,
 * but the index is not a disc_parameter, rather it just
 * matches.
 *
 * The constraints and parameter_types are defined in disc_constraints.h
 */
//#undef DISC_DEBUG
//#define DISC_DEBUG 2
void disc_nsector(unsigned int n,
                  const double parameter_value[n],
                  double * residual,
                  void * p)
{
    /*
     * Map va_args in *p to variables
     */
    Map_GSL_params(p,args);
    Map_varg(const int Maybe_unused,nn,args);
    Map_varg(const disc_parameter *,parameter_types,args);
    Map_varg(const disc_constraint *,constraints,args);
    Map_varg(struct disc_t *,disc,args);
    Map_varg(struct binary_system_t *,binary,args);
    Map_varg(struct stardata_t *,stardata,args);
    Map_varg(int *,failure,args);
    va_end(args);
    double unknown Maybe_unused;
    struct disc_t * discwas;
    unsigned int i,j;

    /*
     * Back up disc
     */
    discwas = New_disc_from(disc);

    /*
     * Check for nans
     */
    for(i=0;i<n;i++)
    {
        Discdebug(2,
                  "Nsector parameter %3u/%3u type %3u %40s value = %30.15e\n",
                  i,
                  n,
                  parameter_types[i],
                  Disc_parameter_string(parameter_types[i]),
                  parameter_value[i]);

        if(isnan(parameter_value[i]))
        {
            Discdebug(2,
                      "Found nan parameter : restoring disc and returning DISC_CONSTRAINT_FAILED == %g\n",
                      DISC_CONSTRAINT_FAILED);
            /*
             * Found nan : return all DISC_CONSTRAINT_FAILED
             * (which is usually -1.0)
             */
            for(j=0;j<n;j++)
            {
                residual[j] = DISC_CONSTRAINT_FAILED;
            }
            Copy_disc(discwas,disc);
            Safe_free(discwas);
            return;
        }
    }

    /*
     * No nans : run parameter checks. If one fails, return.
     */
    for(i=0;i<n;i++)
    {
        if(Disc_parameter_check(disc,
                                parameter_types[i],
                                parameter_value[i])
           == FALSE)
        {
            for(j=0;j<n;j++)
            {
                residual[j] = DISC_CONSTRAINT_FAILED;
            }
            Discdebug(2,"nsector : parameter %u type %u %s failed validity check, tried to set value = %g (Rin %g Rout %g)\n",
                      i,
                      parameter_types[i],
                      Disc_parameter_string(parameter_types[i]),
                      parameter_value[i],
                      disc->Rin,
                      disc->Rout
                );
            Copy_disc(discwas,disc);
            Safe_free(discwas);
            return;
        }
    }

    /*
     * All checks passed: set parameters
     */
    for(i=0;i<n;i++)
    {
        if(parameter_types[i] != DISC_PARAMETER_NONE)
        {
            Disc_parameter(disc,parameter_types[i]) =
                parameter_value[i];

            Discdebug(2,"Nsector: set parameter %u (type %u %s) to %g (now disc : Tvisc0 = %g, Rin = %g, Rout = %g) \n",
                      i,
                      parameter_types[i],
                      Disc_parameter_string(parameter_types[i]),
                      parameter_value[i],
                      disc->Tvisc0,
                      disc->Rin,
                      disc->Rout
                );
        }
    }

    Discdebug(2,
              "Nsector: call nsector2 with disc-> : Rin=%g Rsun Rout=%g Rsun : M=%g Msun, J=%g, F=%g\n",
              disc->Rin,
              disc->Rout,
              disc->M/M_SUN,
              disc->J,
              disc->F);

    /*
     * Call nsector2 to set up the disc zones and
     * calculate the residuals
     */
    disc_nsector2(stardata,
                  disc,
                  binary,
                  n,
                  constraints,
                  residual,
                  failure);

    Safe_free(discwas);

    Discdebug(2,"Return from nsector\n");
}



void disc_nsector2(struct stardata_t * stardata Maybe_unused,
                   struct disc_t * disc,
                   struct binary_system_t * binary,
                   const unsigned int n,
                   const disc_constraint * constraints,
                   double * residual,
                   int * failed)
{
    /*
     * Function that does the work. This may be
     * called from elsewhere for debugging purposes,
     * so is not a static function.
     */
    Discdebug(2,
              "Nsector: disc Tvisc0 = %g Rin=%g Rout=%g\n",
              disc->Tvisc0,
              disc->Rin,
              disc->Rout
        );

    int status = disc_build_disc_zones(stardata,disc,binary);
    unsigned int i;

    if(DISC_DEBUG>=2)
    {
        Discdebug(2,
                  "Nsector: Built disc zones: %s : Rin=%g Rout=%g Rsun (equal? %d, 1 - ratio = %g)\n",
                  status==DISC_ZONES_OK ? "ok" : "failed",
                  disc->Rin/R_SUN,
                  disc->Rout/R_SUN,
                  Fequal(disc->Rin,disc->Rout),
                  1.0 - disc->Rin/disc->Rout
            );
        Discdebug(2,
                  "Nsector: disc->M = %g Msun cf M = %g Msun, disc->J = %g cf J = %g, disc->F = %g cf F = %g, residual = %30.15e\n",
                  disc->M/M_SUN,
                  disc_total_mass(disc)/M_SUN,
                  disc->J,
                  disc_total_angular_momentum(disc,binary),
                  disc->F,
                  disc_total_angular_momentum_flux(disc,binary),
                  Disc_residual(disc,binary,DISC_CONSTRAINT_M_TO_DISC_M)
            );
        Discdebug(2,
                  "Nsector: (disc->J = %g) / (J = %g) - 1.0 = %g, residual = %30.15e\n",
                  disc->J,
                  disc_total_angular_momentum(disc,binary),
                  disc->J/disc_total_angular_momentum(disc,binary)-1.0,
                  Disc_residual(disc,binary,DISC_CONSTRAINT_J_TO_DISC_J)
            );
        double F = disc_total_angular_momentum_flux(disc,
                                                                binary);
        Discdebug(2,
                  "Nsector: (disc->F = %g) / (F = %g) - 1.0 = %30.15e\n",
                  disc->F,
                  F,
                  disc->F / F - 1.0
            );
    }

    if(status==DISC_ZONES_OK)
    {
        /*
         * Set residuals
         */
        for(i=0;i<n;i++)
        {
            const unsigned int constraint = constraints[i];
            residual[i] = Disc_residual(disc,binary,constraint);
            Discdebug(2,
                      "Nsector: constraint %3u type %3u %40s residual = %30.15g\n",
                      i,
                      constraint,
                      Disc_constraint_string(constraint),
                      residual[i]);
        }
        *failed = 0;
    }
    else
    {
        /*
         * Something went wrong:
         * set all residuals to DISC_CONSTRAINT_FAILED
         */
        for(i=0;i<n;i++)
        {
            residual[i] = DISC_CONSTRAINT_FAILED;
        }
        *failed = 1;
    }


    Discdebug(2,
              "return from nsector2 : failed = %d\n",
              *failed);
}

#endif // DISCS
