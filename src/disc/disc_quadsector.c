#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef DISCS
/*
 * Function to be minimized by GSL routines
 * such that we form a disc.
 *
 * The four constraints are M, J, F and torquef,
 * the mass, angular momentum, angular momentum flux,
 * and multiplier on the binary torque respectively.
 */

void disc_quadsector(double Tvisc0,
                     double Rin,
                     double Rout,
                     double torquef,
                     double *xx,
                     void * p)
{
    Map_GSL_params(p,args);
    Map_varg(struct disc_t *,disc,args);
    Map_varg(const struct binary_system_t *,binary,args);
    Map_varg(struct stardata_t *,stardata,args);
    va_end(args);

    /* check for nans */
    if(isnan(Tvisc0)!=0 ||
       isnan(Rin)!=0 ||
       isnan(Rout)!=0 ||
       isnan(torquef)!=0 ||
       /* and torquef must be positive */
       torquef < 0.0)
    {
        printf("\nQuadsector >>> NAN <<< Rin = %g (%d), Rout = %g (%d), Tvisc0 = %g (%d), torquef = %g -> xx = -1 (fail)\n",
               disc->Rin,isnan(disc->Rin),
               disc->Rout,isnan(disc->Rout),
               disc->Tvisc0,isnan(disc->Tvisc0),
               torquef
            );
        Discdebug(2,
                  "Quadsector: Found NAN : Rin=%g Rout=%g Tvisc0=%g torquef=%g\n",
                  Rin,
                  Rout,
                  Tvisc0,
                  torquef
            );
        xx[0] = xx[1] = xx[2] = xx[3] = -1.0;
    }
    else if(Rin > Rout)
    {
        Discdebug(2,
                  "Quadsector: Rin > Rout fail : Rin = %g Rout = %g Tvisc0 = %g\n",
                  Rin,
                  Rout,
                  Tvisc0);
        xx[0] = xx[1] = xx[2] = xx[3] = -1.0;
    }
    else
    {
        /*
         * No nans : all (probably) ok
         */
        disc->Tvisc0 = Tvisc0;
        disc->Rin = Max(1e-10,Rin);
        disc->Rout = Max(Rin,Rout);
        disc->torqueF = torquef;

        Discdebug(2,
                  "Quadsect Rin=%g Rout=%g Tvisc0=%g Torquef=%g (disc=%p binary=%p)\n",
                  Rin,
                  Rout,
                  Tvisc0,
                  torquef,
                  (void*)disc,
                  (void*)binary);

        disc_quadsector2(stardata,
                         disc,
                         binary,
                         xx);
    }
}



void disc_quadsector2(struct stardata_t * stardata Maybe_unused,
                      struct disc_t * disc,
                      const struct binary_system_t * binary,
                      double * xx)
{
    /*
     * Function that does the work. This may be
     * called from elsewhere for debugging purposes,
     * so is not a static function.
     */

    int status = disc_build_disc_zones(stardata,disc,binary);

    Discdebug(2,
              "Quadsector: Built disc zones: %s : Rin=%g Rout=%g\n",
              status==DISC_ZONES_OK ? "ok" : "failed",
              disc->Rin,
              disc->Rout);

    if(status==DISC_ZONES_OK)
    {
        double M = disc_total_mass(disc);
        double J = disc_total_angular_momentum(disc,binary);
        double F = disc_total_angular_momentum_flux(disc,binary);

        Discdebug(2,
                  "Quadsector : Rin = %g, Rout = %g Rsun, Tvisc0 = %g, torquef = %g : M=%30.20e (want %30.20g) J=%30.20e (want %30.20g) F = %30.20e (want %30.20g) Rin = %g (want Rin_min = %g)\n",
                  disc->Rin/R_SUN,
                  disc->Rout/R_SUN,
                  disc->Tvisc0,
                  disc->torqueF,
                  M/M_SUN,disc->M/M_SUN,
                  J,disc->J,
                  F,disc->F,
                  disc->Rin, disc->Rin_min
            );

        xx[0] = M/disc->M - 1.0;
        xx[1] = J/disc->J - 1.0;
        xx[2] = fabs(F/disc->F) - 1.0;
        xx[3] = disc->Rin / disc->Rin_min - 1.0;

        Discdebug(2,
                  "Quadsector : M %g/%g-1 = %g : J %g/%g - 1 = %g : F %g/%g -1 = %g : Rin %g/%g - 1 = %g\n",
                  M,
                  disc->M,
                  xx[0],
                  J,
                  disc->J,
                  xx[1],
                  F,
                  disc->F,
                  xx[2],
                  disc->Rin,
                  disc->Rin_min,
                  xx[3]);
    }
    else
    {
        xx[0] = xx[1] = xx[2] = xx[3] = -1.0;
    }
}

#endif // DISCS
