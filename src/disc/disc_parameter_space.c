#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef DISCS

/*
 * Function to loop through the 3D disc parameter
 * space, finding the residual of the various disc equations,
 * and output them.
 */

void disc_parameter_space(struct stardata_t * const stardata,
                          struct disc_t * const disc)
{
    struct binary_system_t * binary = Calloc(1,sizeof(struct binary_system_t));
    disc_init_binary_structure(stardata,binary,disc);

    /*
     *
     * Best solution is
     * 1e+17, 9.76328e+14, 2.51189e+15
     * with residual 9.78066e-06
     *
     * But the solver finds
     * 4.0705e+19, Rin = 4.61463e+12, Rout = 4.68749e+12
     * with residual 0.000269746
     *
     * Why?
     *
     */
#define STREAM stdout
#define DD 1.0
#define VAR0 logTvisc0
#define VAR0L 0.0
#define VAR0H 50.0
#define VAR0D DD
#define VAR1 logRin
#define VAR1L Max(8,log10(binary->separation)-3)
#define VAR1H 30.0
#define VAR1D DD
#define VAR2 logRout
#define VAR2L 8.0
#define VAR2H 30.0
#define VAR2D DD
#define NO_MINIMUM 1e100

#define Logstream stdout
#define Varloop(N)                              \
    for((VAR ## N) = (VAR ## N ## L);           \
        (VAR ## N) <= (VAR ## N ## H);          \
        (VAR ## N) += (VAR ## N ## D))
#define Residual(N) ( ( residuals[ (N) ] ) )
#define Set_min(N) minima[(N)] = VAR ## N
#define Set_minima                              \
    Set_min(0);                                 \
    Set_min(1);                                 \
    Set_min(2);
#define Tolerance DISC_TOLERANCE
#define Fail(N) (                                       \
        Fequal(Residual(N), -1.0) ||                    \
        (fabs(Residual(N)) > Min(10.0, DISC_TOLERANCE)) \
        )
#define Failure (Fail(0) || Fail(1) || Fail(2))


#define OK(N) (                                 \
        fabs(Residual(N))<0.9                   \
        )
#define Terrible (!OK(0) || !OK(1) || OK(2))
#define Not_terrible (!Terrible)

#define Product (fabs(Residual(0) * Residual(1) * Residual(2)))
#define Sum (fabs(Residual(0)) + fabs(Residual(1)) + fabs(Residual(2)))
#define Residual_list Residual(0) , Residual(1) , Residual(2)
#define Var_list Tvisc0, Rin, Rout
#define Disc_var_list disc->Tvisc0, disc->Rin, disc->Rout

#define Show(F,N)                               \
    fprintf(                                    \
        (F),                                    \
        "# %s from %g to %g step %g\n",         \
        Stringify_macro(VAR ## N),              \
        (VAR ## N ## L),                        \
        (VAR ## N ## H),                        \
        (VAR ## N ## D)                         \
        );
#define Show_pc(F,N)                            \
    fprintf(                                    \
        (F),                                    \
        "# %5.2f %%     \x0d",                  \
        (VAR ## N - VAR ## N ## L) /            \
        (VAR ## N ## H - VAR ## N ## L) *       \
        100.0                                   \
        );

/* wrapper for prints */
#define print(...) fprintf(Logstream,__VA_ARGS__)

/*
 * Call Trisect to find the residuals
 */
#define Trisect disc_trisector2(stardata,disc,binary,residuals)

/*
 * Call Calc_structure to converge on a solution
 * using GSL routines (or otherwise)
 */
#define Calc_structure                          \
    disc_calc_disc_structure(binary,            \
                             disc,              \
                             stardata,          \
                             0.0,\
                             0.0)

    /* NB all are log10 */
    double logTvisc0,logRin,logRout;
    double Var_list;
    double residuals[3];
    double minima[3] = {NO_MINIMUM,NO_MINIMUM,NO_MINIMUM};
    double minimum = NO_MINIMUM;

    disc->Tvisc0 = 1e+17;
    disc->Rin = 9.76328e+14;
    disc->Rout =2.51189e+15;

    print("\nFirst trisect\n");

    Trisect;

    print(
        "Start at 'best' Tvisc0 = %g, Rin = %g, Rout = %g\n",
        Disc_var_list);
    print("Residuals %g %g %g : %g\n",
          Residual_list,
          Product);
    print("Failure? %s\n",Yes_or_no(Failure));

    Calc_structure;
    Trisect;

    print("Converging from there gives Tvisc0 = %g, Rin = %g, Rout = %g\n",
          Disc_var_list);
    print("Residuals %g %g %g : %g\n",
          Residual_list,
          Product);
    print("Failure? %s\n",Yes_or_no(Failure));


    print("************************************************************\n");

    if(disc->converged)
    {
        disc_trisector2(stardata,disc,binary,residuals);
        print("Disc in was converged with solution : Tvisc0 = %g, Rin = %g, Rout = %g : M=%g J=%g which has residual %g\n",
              Disc_var_list,
              disc->M/M_SUN,
              disc->J,
              Product);
    }
    else
    {
        print("Input disc is not converged : want M=%g J=%g\n",
              disc->M/M_SUN,
              disc->J
            );
    }
    print("Searching parameter space:\n");
    Show(Logstream,0);
    Show(Logstream,1);
    Show(Logstream,2);
#ifdef STREAM
    Show(STREAM,0);
    Show(STREAM,1);
    Show(STREAM,2);
#endif // STREAM


    fprintf(STREAM,
            "#PSPACE %19s %20s %20s %20s %20s %20s %s\n",
            "Var0",
            "Var1",
            "Var2",
            "Res0",
            "Res1",
            "Res2",
            "newmin");

    Varloop(0)
    {
        Show_pc(Logstream,0);
        Varloop(1)
        {
            Varloop(2)
            {
                Tvisc0 = exp10(VAR0);
                Rin = exp10(VAR1);
                Rout = exp10(VAR2);

                //printf("VARLOOP %g %g %g\n",VAR0,VAR1,VAR2);

                if(Rin < Rout)
                {
                    disc->Tvisc0 = Tvisc0;
                    disc->Rin = Rin;
                    disc->Rout = Rout;

                    Trisect;

                    if(!Fequal(disc->Tvisc0,Tvisc0) ||
                       !Fequal(disc->Rin,Rin) ||
                       !Fequal(disc->Rout,Rout))
                    {
                        fprintf(stderr,"MISMATCH\n");
                    }


                    //double score = Product;
                    double score = Sum;
                    Boolean new_minimum;

                    if(unlikely(score < minimum))
                    {
                        Set_minima;
                        minimum = score;
                        new_minimum = TRUE;
                    }
                    else
                    {
                        new_minimum = FALSE;
                    }

                    //if(new_minimum == TRUE)
                    {
                        fprintf(STREAM,
                                "PSPACE %20g %20g %20g %20g %20g %20g %20g %s\n",
                                Var_list,
                                Residual_list,
                                score,
                                (new_minimum == TRUE ? "*" : ""));
                        fflush(NULL);
                    }
                }
            }
#ifdef STREAM
            //fprintf(STREAM,"\n");
#endif
        }
    }

    print ("\nnear end\n");fflush(NULL);
    /*
    if(0 && minimum / NO_MINIMUM > 0.99)
    {
        print("Parameter space search failed: no solution found\n");
    }
    else
        */
    {
        print("Minimum residual %g at Tvisc0 = %g, Rin = %g, Rout = %g\n",
              minimum,
              exp10(minima[0]),
              exp10(minima[1]),
              exp10(minima[2]));
        fflush(NULL);

        /* try calculating a structure from there */
        disc->Tvisc0 = exp10(minima[0]);
        disc->Rin = exp10(minima[1]);
        disc->Rout = exp10(minima[2]);
        disc->vb = 0;
        disc->converged = FALSE;
        disc->lifetime = 1.0;

        print("call calc_structure\n");
        Calc_structure;


        print("call trisect\n");
        Trisect;

        print("Converging from there gives Tvisc0 = %g, Rin = %g, Rout = %g\n",
              Disc_var_list);
        print("With residuals %g, %g, %g : product %g (Tvisc0 = %g, Rin = %g, Rout = %g)\n",
              Residual(0),
              Residual(1),
              Residual(2),
              Product,
              Disc_var_list
            );
        if(Failure)
        {
            print("*** warning : this is a failed solution! ***\n");
        }
        fflush(NULL);
    }
    Safe_free(binary);
    fflush(NULL);

    printf("disc_parameter_space done\n");
    return;
}


#endif // DISCS
