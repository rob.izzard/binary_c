#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef DISCS

/*
 * Remove all discs of a given type from an object
 */

void remove_discs(struct stardata_t * const stardata,
                  const Disc_type type,
                  void * const object)
{
    if(type == DISC_CIRCUMSTELLAR || type == DISC_ANY)
    {
        struct star_t * const star = object;
        while(star->ndiscs>0)
        {
            remove_disc(stardata,
                        type,
                        object,
                        DISC_OUTER_EDGE,
                        DISC_REMOVE_ALL);
        }
    }
    else if(type == DISC_CIRCUMBINARY || type == DISC_ANY)
    {
        while(stardata->common.ndiscs>0)
        {
            remove_disc(stardata,
                        type,
                        object,
                        DISC_OUTER_EDGE,
                        DISC_REMOVE_ALL);
        }
    }
    else
    {
        Exit_binary_c(
            BINARY_C_UNKNOWN_DISC_TYPE,
            "Disc type %d unknown",
            type);
    }
}



#endif//DISCS
