#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef DISCS

void disc_extend_powerlaw_from_reference_point(struct disc_t * const disc,
                                               const int ilaw,
                                               const double Rref,
                                               const double Aref,
                                               const double ref_exponent)
{
    /*
     * Given a disc and a derived power law, index ilaw,
     * set up A0 and A1 in each zone given Aref at Rref.
     *
     * Note that Rref may be outside the current zone structure,
     * in which case you should give both Aref at Rref, and
     * the exponent which should be used to extrapolate back
     * into the zone structure.
     *
     * A0 is then calculated using
     */

    /*
     * Find the zone nearest to Rref.
     *
     * Note that this zone IS a valid zone and should be the outermost
     * zone.
     */
    int iref = Disc_nearest_zone_n(Rref,disc);
    struct disc_thermal_zone_t * zref = Disc_nearest_zone(Rref,disc);
    int n_nearest = Disc_nearest_zone_n(Rref,disc);

    /*
     * Hence A0 and A1 in this zone
     */
    {
        struct power_law_t * P = &zref->power_laws[ilaw];
        double exponent = Is_really_not_zero(ref_exponent) ? ref_exponent : P->exponent;
        double A0 = Aref * pow(Rref/P->R0, -exponent);

        if(DISC_DEBUG)
        {
            printf("CF Rref =%g, P->R1 = %g\n",Rref,P->R1);

            if(Rref > P->R1)
            {
                printf("WARNING : Rref is outside the last zone (nearest zone is %d), use exponent %g (ref_exponent = %g) to extrapolate inwards\n",iref,exponent,ref_exponent);
            }

            printf("A0(zone=%d, plaw=%d) = %g from Aref = %g, Rref = %g, R0 = %g, exponent = %g\n",
                   n_nearest,
                   ilaw,
                   A0,
                   Aref,
                   Rref,
                   P->R0,
                   exponent);
        }
        update_power_law(P,A0,0);
    }

    /*
     * update outwards
     */
    Disc_zone_counter i;
    for(i=iref+1;i<disc->n_thermal_zones;i++)
    {
        if(DISC_DEBUG) printf("Match %u to %u (outwards)\n",i-1,i);
        match_power_laws(&disc->thermal_zones[i-1].power_laws[ilaw],
                         &disc->thermal_zones[i].power_laws[ilaw]);
    }

    /*
     * update inwards
     */
    for(i=iref;i>=1;i--)
    {
        if(DISC_DEBUG) printf("Match %u to %u (inwards)\n",i,i-1);
        match_power_laws_inwards(&disc->thermal_zones[i].power_laws[ilaw],
                                 &disc->thermal_zones[i-1].power_laws[ilaw]);
    }

    for(i=0;i<disc->n_thermal_zones;i++)
    {
        struct disc_thermal_zone_t * z = &disc->thermal_zones[i];
        struct power_law_t * P = &z->power_laws[ilaw];

        if(DISC_DEBUG)
            printf("ZONE %u, power law %d (exponent %g), from R0=%g to R1=%g, A0=%g, A1=%g\n",
                   i,
                   ilaw,
                   P->exponent,
                   P->R0,
                   P->R1,
                   P->A0,
                   P->A1);
    }


}





#endif//DISCS
