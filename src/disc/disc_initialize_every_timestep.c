#include "../binary_c.h"
No_empty_translation_unit_warning;

/*
 * Initialize all discs every timestep
 */
#ifdef DISCS

static void initialize_discs_every_timestep(struct star_t * const star);
void disc_initialize_every_timestep(struct stardata_t * const stardata)
{

    int i;

    /*
     * Circumbinary discs
     */

    for(i=0; i<stardata->common.ndiscs; i++)
    {
        struct disc_t * disc = & stardata->common.discs[i];
        disc->first = FALSE;
        disc->append = FALSE;
    }

    /*
     * Circumstellar discs
     */
    Star_number k;
    Starloop(k)
    {
        initialize_discs_every_timestep(&stardata->star[k]);
    }
}

static void initialize_discs_every_timestep(struct star_t * const star Maybe_unused)
{
    /* todo */
}

#endif // DISCS
