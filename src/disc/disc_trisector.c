#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef DISCS


/*
 * Function to be minimized by GSL routines
 * such that we form a disc.
 *
 * The three constraints are M, J and F,
 * the mass, angular momentum and angular momentum flux,
 * respectively.
 *
 * disc->M and disc->J are set elsewhere.
 *
 * disc->F is set after the disc is built,
 * and then compared to an integral form. When
 * these agree, and M, J also converge, the disc
 * is converged.
 */

#define DD(...) printf(__VA_ARGS__)
//#define DD(...) Discdebug(2,__VA_ARGS__)

void disc_trisector(double Tvisc0,
                    double Rin,
                    double Rout,
                    double *residual,
                    void * p)
{
    Map_GSL_params(p,args);
    Map_varg(struct disc_t *,disc,args);
    Map_varg(const struct binary_system_t *,binary,args);
    Map_varg(struct stardata_t *,stardata,args);
    va_end(args);

    if(isnan(Tvisc0)!=0 ||
       isnan(Rin)!=0 ||
       isnan(Rout)!=0)
    {
        printf("Trisector: \n >>> NAN <<< Rin = %g (%d), Rout = %g (%d), Tvisc0 = %g (%d) -> residual = -1 (fail)\n",
               disc->Rin,isnan(disc->Rin),
               disc->Rout,isnan(disc->Rout),
               disc->Tvisc0,isnan(disc->Tvisc0));
        DD("Trisector: Found NAN : Rin=%g Rout=%g Tvisc0=%g\n",
           Rin,
           Rout,
           Tvisc0);
        residual[0] = -1.0;
        residual[1] = -1.0;
        residual[2] = -1.0;
    }
    else if(Rin > Rout)
    {
        DD("Trisector: Rin > Rout fail : Rin = %g Rout = %g Tvisc0 = %g\n",
           Rin,
           Rout,
           Tvisc0);
        residual[0] = -1.0;
        residual[1] = -1.0;
        residual[2] = -1.0;
    }
    else
    {
        /*
         * No nans : all ok
         */
        disc->Tvisc0 = Tvisc0;
        disc->Rin = Max(1e-10,Rin);
        disc->Rout = Max(Rin,Rout);

        DD(
            "Trisect Rin=%g Rout=%g Tvisc0=%g (disc=%p binary=%p)\n",
            Rin,
            Rout,
            Tvisc0,
            (void*)disc,
            (void*)binary);

        disc_trisector2(stardata,
                        disc,
                        binary,
                        residual);
    }
}



void disc_trisector2(struct stardata_t * stardata Maybe_unused,
                     struct disc_t * disc,
                     const struct binary_system_t * binary,
                     double *residual)
{
    /*
     * Function that does the work. This may be
     * called from elsewhere for debugging purposes,
     * so is not a static function.
     */
    int status = disc_build_disc_zones(stardata,disc,binary);

    DD("Trisector: Built disc zones: %s : Rin=%g Rout=%g Rsun\n",
       status==DISC_ZONES_OK ? "ok" : "failed",
       disc->Rin/R_SUN,
       disc->Rout/R_SUN);

    if(status==DISC_ZONES_OK)
    {
        double M = disc_total_mass(disc);
        double J = disc_total_angular_momentum(disc,binary);
        double F = disc_total_angular_momentum_flux(disc,binary);
        DD("Trisector : Rin = %g, Rout = %g Rsun, Tvisc0 = %g : M=%30.20e (want %30.20g) J=%30.20e (want %30.20g) F = %30.20e (want %30.20g) zone 0 : R = %g - %g cm\n",
           disc->Rin/R_SUN,
           disc->Rout/R_SUN,
           disc->Tvisc0,
           M/M_SUN,disc->M/M_SUN,
           J,disc->J,
           F,disc->F,
           Disc_inner_zone(disc)->rstart,
           Disc_inner_zone(disc)->rend
            );
        residual[0] = M/disc->M - 1.0;
        residual[1] = J/disc->J - 1.0;
        residual[2] = F/disc->F - 1.0;

        DD("Trisector residual = %g, %g %g\n",
           residual[0],
           residual[1],
           residual[2]);
    }
    else
    {
        residual[0] = -1.0;
        residual[1] = -1.0;
        residual[2] = -1.0;
    }
}

#endif // DISCS
