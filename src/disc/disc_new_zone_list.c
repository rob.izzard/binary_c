#include "../binary_c.h"

No_empty_translation_unit_warning;

#ifdef DISCS
Boolean disc_new_zone_list(struct stardata_t * const stardata,
                           struct disc_t * const disc,
                           const struct binary_system_t * const binary,
                           struct disc_thermal_zone_t * const thermal_zones)
{
    /*
     * Make a new set of thermal zones in thermal_zones[]
     *
     * The prefactors and exponents are defined in disc_thermal_zones.h
     *
     * Returns FALSE on failure, TRUE on success.
     *
     * The parameter Tvisc0, which determines sigma0, hence the
     * mass scaling, determines the profile of temperature in the disc.
     * This function also sets sigma0.
     *
     * Note that the zones may later be determined to be invalid,
     * e.g. inside or outside the disc.
     * This function does not make such determinations.
     */


    /*
     * Fail if input parameters are unphysical, e.g. Tvisc0 < 0
     */
    if(unlikely(disc->Tvisc0 < REALLY_TINY))
    {
        return FALSE;
    }
    /*
     * Fail if Rin and Rout are sufficiently close
     */
    else if(unlikely(fabs(1.0 - disc->Rin / disc->Rout) <
                DISC_RADIUS_DIFFERENCE_THRESHOLD_FOR_FAILURE))
    {
        Discdebug(1,
                  "Disc inner (%g) and outer (%g) radius are equal (to within %g) : returning %s = %d\n",
                  disc->Rin,
                  disc->Rout,
                  fabs(1.0-disc->Rin/disc->Rout),
                  Stringify(DISC_ZONES_NEW_ZONE_LIST_RIN_EQUALS_ROUT),
                  DISC_ZONES_NEW_ZONE_LIST_RIN_EQUALS_ROUT);
        return DISC_ZONES_NEW_ZONE_LIST_RIN_EQUALS_ROUT;
    }
    else if(unlikely(disc->Rin > disc->Rout))
    {
        Discdebug(1,
                  "Disc inner radius (%g) exceeds outer radius (%g) : returning %s = %d\n",
                  disc->Rin,
                  disc->Rout,
                  Stringify(DISC_ZONES_RIN_EXCEEDS_ROUT),
                  DISC_ZONES_RIN_EXCEEDS_ROUT);
        return DISC_ZONES_RIN_EXCEEDS_ROUT;
    }
    else
    {
        /*
         * Create pointers to the zones
         * (for convenience and ease of reading in English)
         */
        struct disc_thermal_zone_t * const radiative_in =
            &(thermal_zones[DISC_ZONE_TYPE_RADIATIVE_INNER]);
        struct disc_thermal_zone_t * const radiative_out =
            &(thermal_zones[DISC_ZONE_TYPE_RADIATIVE_OUTER]);
        struct disc_thermal_zone_t * const viscous =
            &(thermal_zones[DISC_ZONE_TYPE_VISCOUS]);

        /*
         * Set up the outer radiative zone, in which Rref should lie
         */
        new_power_law(&(radiative_out->Tlaw),
                      Prefactor_radiative_out,
                      Exponent_radiative_out,
                      DISC_REFERENCE_LENGTH, /* R0=1 */
                      0.0 /* R1=0 is ignored */);
        radiative_out->type = DISC_ZONE_TYPE_RADIATIVE_OUTER;

        /*
         * Set up the inner radiative zone
         */
        new_power_law(&(radiative_in->Tlaw),
                      Prefactor_radiative_in,
                      Exponent_radiative_in,
                      DISC_REFERENCE_LENGTH, /* R0=1 */
                      0.0 /* R1=0 is ignored */);
        radiative_in->type = DISC_ZONE_TYPE_RADIATIVE_INNER;

        /*
         * Set sigma0 from Tvisc0 (which is a free parameter)
         */
        disc->sigma0 = pow(disc->Tvisc0,5.0/2.0)
            / sqrt(Prefactor_viscous_fraction);

        /*
         * And hence update the disc angular momentum flux, which
         * depends only on sigma0 and some constants of the disc (alpha, mu,
         * gamma)
         */
        disc->F = disc_angular_momentum_flux(disc->Rout,disc,binary);

        /*
         * Set up the viscous zone (requires disc->sigma0 to be set)
         */
        new_power_law(&(viscous->Tlaw),
                      Prefactor_viscous,
                      Exponent_viscous,
                      DISC_REFERENCE_LENGTH, /* R0 = 1 */
                      0.0 /* R1 = 0 is ignored */);
        viscous->type = DISC_ZONE_TYPE_VISCOUS;

        /*
         * Save temperatures for later sanity checks (if we converge)
         */
        disc->Tvisc = power_law(&viscous->Tlaw,DISC_REFERENCE_LENGTH);
        disc->Tradin = power_law(&radiative_in->Tlaw,DISC_REFERENCE_LENGTH);
        disc->Tradout = power_law(&radiative_out->Tlaw,DISC_REFERENCE_LENGTH);

        if(DEBUG_ZONE_LIST_CONSTRUCTION)
        {
            /*
             * Debugging:
             * Check that Rref = 1cm is inside the viscous zone
             */
            double RxVRin = disc_Rcross(viscous,radiative_in);
            double RxVRout = disc_Rcross(viscous,radiative_out);

            Discdebug(1,
                      "Crossing radii visc/radin=%g visc/radout=%g\n",
                      RxVRin,
                      RxVRout);

            if(DEBUG_ZONE_LIST_CONSTRUCTION>1)
            {
                /*
                 * Check that T = Tvisc0 at R=1cm
                 */
                printf("T(visc) at 1cm: visc=%g (should be %g, at disc->Rin T=%g), Tradin(1cm)=%g Tradout(1cm)=%g\n",
                       disc->Tvisc,
                       disc->Tvisc0,
                       power_law(&viscous->Tlaw,disc->Rin),
                       disc->Tradin,
                       disc->Tradout);

                /*
                 * Check that sigma*T^4 is the same either side of the visc/radin transition
                 */
                double TviscX = viscous->Tlaw.A0 * pow(RxVRin,viscous->Tlaw.exponent);
                double TradinX = radiative_in->Tlaw.A0 * pow(RxVRin,radiative_in->Tlaw.exponent);
                printf("sigma * T4 = %g and %g either side of the visc/Radin transition\n",
                       disc->sigma0 / (TviscX * Pow2(RxVRin)) * Pow4(TviscX),
                       disc->sigma0 / (TradinX * Pow2(RxVRin)) * Pow4(TradinX));

            }
        }

        if(SHOW_ZONE_LIST_POWER_LAWS)
        {
            /*
             * Output power laws at various R
             */
            double logR,R;
            for(logR=0.0; logR<20.0; logR+=0.1)
            {
                R = exp10(logR);
                printf("TESTR %g %g %g %g %g\n",
                       disc->Tvisc0,
                       R/R_SUN,
                       power_law(&viscous->Tlaw,R),
                       power_law(&radiative_in->Tlaw,R),
                       power_law(&radiative_out->Tlaw,R));
            }
            printf("TESTR\n");
            Exit_binary_c_no_stardata(0,"exit after outputting power laws");
        }

        if(DEBUG_ZONE_LIST_CONSTRUCTION)
        {
            printf("----------------------\npowerlaw: exponent, multiplier, magnitude estimate (with masses=Msun, L=1e4Lsun, R=Rsun)\n");
            printf("viscous:       %g %g %g\n",
                   viscous->Tlaw.exponent,
                   viscous->Tlaw.A0,
                   pow((27.0*1.0*1.0*1.0*BOLTZMANN_CONSTANT*sqrt(GRAVITATIONAL_CONSTANT*M_SUN)/(64.0*M_PROTON*STEFAN_BOLTZMANN_CONSTANT)),1.0/5.0) * pow(disc->sigma0,2.0/3.0)

                );
            printf("radiative_in:  %g %g %g\n",
                   radiative_in->Tlaw.exponent,
                   radiative_in->Tlaw.A0,
                   pow(R_SUN * 1e4 * L_SUN / (6.0*PI*PI*STEFAN_BOLTZMANN_CONSTANT),1.0/4.0));

            printf("radiative_out: %g %g %g\n",
                   radiative_out->Tlaw.exponent,
                   radiative_out->Tlaw.A0,
                   pow((1.0/7.0)*(1e4*L_SUN/(4.0*PI))*sqrt(1.0*BOLTZMANN_CONSTANT/(GRAVITATIONAL_CONSTANT*M_SUN*M_PROTON)),2.0/7.0)
                );
            printf("----------------------\n");
        }

        /*
         * Default all thermal zones to be invalid
         */
        {
            Disc_zone_counter i;
            for(i=0;i<DISCS_MAX_N_ZONES;i++)
            {
                thermal_zones[i].valid = FALSE;
            }
        }

        return DISC_ZONES_OK;

    }
}


#endif
