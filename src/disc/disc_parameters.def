#pragma once
#ifndef DISC_PARAMETERS_DEF
#define DISC_PARAMETERS_DEF



/* disc types (unsigned int) */
#define DISC_TYPES_LIST   \
    X(           ANY, -1) \
    X(          NONE,  0) \
    X( CIRCUMSTELLAR,  1) \
    X(  CIRCUMBINARY,  2)


/* disc positions */
#define DISC_POSITIONS_LIST \
    X( INNER, -1)           \
    X( OUTER, -2)

/* constraints to be bisected */
#define DISC_BISECTION_CONSTRAINTS_LIST \
    X( M, 1)                            \
    X( J, 2)                            \
    X( F, 3)

/* convergence results */
#define DISC_CONVERGENCE_RESULTS_LIST \
    X(            FAILED)             \
    X(         SUCCEEDED)             \
    X(     FLUX_NEGATIVE)             \
    X( IMMEDIATE_FAILURE)


/*
 * Reasons for disc removal
 */
#define DISC_REMOVAL_REASONS_LIST \
    X(           FORMED_RING)     \
    X(               REPLACE)     \
    X(             EVAPORATE)     \
    X(                   ALL)     \
    X( FIRST_TIMESTEP_FAILED)

/*
 * List of disc solvers
 */
#define DISC_SOLVERS_LIST \
    X(      NONE,  0)     \
    X( BISECTION, -1)     \
    X(  NSOLVERS,  3)

/*
 * Circumbinary disc eccentricity pumping algorithms
 */
#define CBDISC_ECCENTRICITY_PUMPING_LIST \
    X(    NONE)                          \
    X( DERMINE)


/*
 * Circumbinary disc from commom envelope
 * angular momentum selectors
 */
#define CBDISC_ANGMOM_FROM_LIST    \
    X(     MOMENTS_OF_INERTIA, -1) \
    X( COMENV_SPECIFIC_ANGMOM, -2) \
    X(              POSTCE_L2, -3) \
    X(           PRECE_RADIUS, -4)


/*
 * Circumbinary viscous accretion which star gains mass method
 */
#define CBDISC_MASS_LOSS_INNER_VISCOUS_ACCRETION_METHOD_LIST \
    X( YOUNG_CLARKE_2015)                                    \
    X(       GEROSA_2015)                                    \
    X(             EQUAL)                                    \
    X(              NONE)

/* disc mass/angmom/ecc loss/gain mechanisms */
#define DISC_LOSS_LIST                                  \
    X(            GLOBAL,                     "Global") \
    X(     INNER_VISCOUS,              "Inner Viscous") \
    X( INNER_L2_CROSSING,          "Inner L2 Crossing") \
    X(               FUV,                        "FUV") \
    X(              XRAY,                      "X-ray") \
    X(     BINARY_TORQUE,              "Binary torque") \
    X(               ISM, "ISM ram stripping (global)") \
    X(        RESONANCES,                 "Resonances") \
    X(     NEGATIVE_FLUX,              "Negative flux") \
    X(  INNER_EDGE_STRIP,           "Inner edge strip") \
    X(  OUTER_EDGE_STRIP,           "Outer edge strip") \
    X(   INNER_EDGE_SLOW,            "Inner edge slow") \
    X(   OUTER_EDGE_SLOW,            "Outer edge slow") \
    X(                 N,                           "")

#define DISC_ZONES_STATUS_LIST        \
    X(                        FAILED) \
    X(                            OK) \
    X( NEW_ZONE_LIST_RIN_EQUALS_ROUT) \
    X(                      NO_ZONES) \
    X(              RIN_EXCEEDS_ROUT)

#define DISC_LOSS_ISM_LIST \
    X(     GLOBAL)         \
    X( OUTER_EDGE)

/*
 * Disc logging caller functions
 */
#define DISC_LOG_LIST          \
    X(  EVOLVE_DISC_STRUCTURE) \
    X( EVOLVE_DISC_STRUCTURE2) \
    X(         EVERY_TIMESTEP)

#define DISC_EDGE_STRIPPING_LIST \
    X(     IN_CONVERGENCE_STEP)  \
    X(  AFTER_CONVERGENCE_STEP)  \
    X( BEFORE_CONVERGENCE_STEP)

/*
 * Disc timestep limiters
 */
#define DISC_TIMESTEP_LIMIT_LIST \
    X(        M)                 \
    X(        J)                 \
    X(        F)                 \
    X(      MIN)                 \
    X(      MAX)                 \
    X( TOO_FAST)

/*
 * disc log levels
 */
#define DISC_LOG_LEVEL_LIST             \
    X(                        NONE,  0) \
    X(                      NORMAL,  1) \
    X(                 SUBTIMESTEP,  2) \
    X(      NORMAL_FIRST_DISC_ONLY, -1) \
    X( SUBTIMESTEP_FIRST_DISC_ONLY, -2)

/*
 * Macros to define the stripping timescale options
 */
#define DISC_STRIPPING_TIMESCALE_LIST \
    X(  INSTANT, 1)                   \
    X( INFINITE, 2)                   \
    X(  VISCOUS, 3)                   \
    X(    ORBIT, 4)

/*
 * Viscous coupling timescales
 */
#define CBDISC_VISCOUS_PHOTOEVAPORATIVE_COUPLING_LIST \
    X(    NONE)                                       \
    X( INSTANT)                                       \
    X( VISCOUS)

#endif // DISC_PARAMETERS_DEF
