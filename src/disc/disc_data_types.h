#pragma once
#ifndef DISC_DATA_TYPES_H
#define DISC_DATA_TYPES_H

/*
 * Disc data types
 */
#define disc_parameter unsigned int
#define disc_constraint unsigned int
#define disc_boundary_condition unsigned int
 

#endif // DISC_DATA_TYPES_H
