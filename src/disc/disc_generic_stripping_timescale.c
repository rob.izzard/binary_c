#include "binary_c.h"
No_empty_translation_unit_warning;

#ifdef DISCS

/*
 * Given an algorithm to define the mass loss timescale
 * at the inner or outer edge, calculate the timescale.
 */

double disc_generic_stripping_timescale(struct disc_t * const disc,
                                        const struct binary_system_t * const binary,
                                        const double radius Maybe_unused,
                                        const int algorithm)
{
    double timescale;

    switch(algorithm)
    {
    case DISC_STRIPPING_TIMESCALE_INSTANT:

        /*
         * Instant
         */
        timescale = 0.0;
        break;

    case DISC_STRIPPING_TIMESCALE_INFINITE:

        /*
         * Very long
         */
        timescale = DISC_LONG_TIME;
        break;


    case DISC_STRIPPING_TIMESCALE_ORBIT:
        /*
         * limit to orbital timescale
         */

        timescale = 2.0 * PI /
            disc_orbital_frequency(disc->Rout,binary);
        break;

    case DISC_STRIPPING_TIMESCALE_VISCOUS:
        /*
         * limit to viscous timescale
         */
        timescale = disc_viscous_timescale(disc->Rout,
                                           disc,
                                           binary);
        break;

    default:
        /*
         * What to do?
         * Return a negative timescale to indicate an error.
         */
        timescale = -1.0;
    }
    return timescale;

}

#endif // DISCS
