#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef DISCS

//#define REZONE_DEBUG

#ifdef REZONE_DEBUG
#define Rezone_debug(...)                       \
    printf("REZONE : ");                        \
    printf(__VA_ARGS__);                        \
    fflush(stdout);
#else
#define Rezone_debug(...) /**/
#endif // REZONE_DEBUG

Boolean disc_rezone(struct disc_t * const disc,
                    const struct binary_system_t * const binary)
{
    Boolean ret;
    /*
     * If Rin and/or Rout have changed, update
     * the thermal zones to match
     */
    if(Disc_is_disc(disc))
    {

        Rezone_debug("start with %d zones\n",
                     disc->n_thermal_zones);

        /*
         * Trim zones
         */
        Disc_zone_counter i;
        Boolean change = FALSE;
        struct disc_thermal_zone_t * zone;
        Disczoneloop(disc,i)
        {
            zone = &disc->thermal_zones[i];

            if(disc->Rin > zone->rstart)
            {
#ifdef REZONE_DEBUG
                double dmzone = disc_partial_mass(disc,zone->rstart,disc->Rin);
#endif
                Rezone_debug("zone %d starts inside the disc : material between old rstart = %g Rsun and Rin = %g Rsun : dMzone = %g Msun\n",
                             i,
                             zone->rstart/R_SUN,
                             disc->Rin/R_SUN,
                             dmzone/M_SUN
                    );
                zone->rstart = disc->Rin;
                change = TRUE;
            }

            if(disc->Rout < zone->rend)
            {
#ifdef REZONE_DEBUG
                double dmzone = disc_partial_mass(disc,disc->Rout,zone->rend);
#endif
                Rezone_debug("zone %d ends outside the disc : material between old rend = %g Rsun and Rout = %g Rsun : dMzone = %g Msun\n",
                             i,
                             disc->Rout/R_SUN,
                             zone->rend/R_SUN,
                             dmzone/M_SUN
                    );
                zone->rend = disc->Rout;
                change = TRUE;
            }

            Rezone_debug("zone %d/%d is between %g and %g Rsun and starts with A0 = %g, R0 = %g, exponent = %g\n",
                         i,
                         disc->n_thermal_zones,
                         zone->rstart/R_SUN,
                         zone->rend/R_SUN,
                         zone->Tlaw.A0,
                         zone->Tlaw.R0,
                         zone->Tlaw.exponent
                );
        }

        if(change == TRUE)
        {
            /*
             * Detect bad zones
             */
            Disczoneloop(disc,i)
            {
                zone = &disc->thermal_zones[i];
                if(Fequal(zone->rstart/zone->rend, 1.0) ||
                   zone->rend/disc->Rin < 1.0-TINY ||
                   zone->rstart/disc->Rout > 1.0+TINY)
                {
                    Rezone_debug("zone %d is invalid\n",i);
                    zone->valid = FALSE;
                }
            }

            /*
             * Delete bad thermal zones
             */
            Disczoneloop(disc,i)
            {
                if(disc->thermal_zones[i].valid == FALSE)
                {
                    /*
                     * Zone is invalid, move all zones outward of this one inwards
                     */
                    Rezone_debug("move zone %d to %d\n",i+1,i);
                    Copy_zone_n(disc,i+1,i);
                    disc->n_thermal_zones--;
                }
            }

            /*
             * Update the innermost temperature power law
             * to be correct at the new rstart
             */
            Rezone_debug("set T power laws\n");
            zone = Disc_inner_zone(disc);
            zone->Tlaw.A0 = power_law(&zone->Tlaw,
                                      zone->rstart);
            zone->Tlaw.R0 = zone->rstart;
            zone->Tlaw.R1 = zone->rend;
            update_power_law(&zone->Tlaw,zone->Tlaw.A0,0);
            Rezone_debug("T at Rin = %g = %g K, R0 = %g, R1 = %g Rsun, A0 = %g\n",
                         power_law(&zone->Tlaw,
                                   zone->rstart),
                         disc_temperature(disc->Rin,disc),
                         zone->Tlaw.R0/R_SUN,
                         zone->Tlaw.R1/R_SUN,
                         zone->Tlaw.A0

                );

            /*
             * Match temperature power laws in any
             * further zones
             */
            if(disc->n_thermal_zones>1)
            {
                for(i=1;i<disc->n_thermal_zones;i++)
                {
                    zone = disc->thermal_zones + i;
                    match_power_laws(&(zone-1)->Tlaw,
                                     &zone->Tlaw);
                }
            }

            if(disc->n_thermal_zones > 0)
            {
                Rezone_debug("make derived powerlaws for %d zones\n",
                             disc->n_thermal_zones+1);
                disc_make_derived_powerlaws(disc,binary);
#ifdef POWER_LAW_SANITY_CHECKS
                disc_check_power_laws(disc,FALSE);
#endif
            }

#ifdef REZONE_DEBUG
            Disczoneloop(disc,i)
            {
                zone = &(disc->thermal_zones[i]);
                Rezone_debug("zone %d is between %g and %g Rsun has A0 = %g, R0 = %g, exponent = %g, mass %g Msun\n",
                             i,
                             zone->rstart/R_SUN,
                             zone->rend/R_SUN,
                             zone->Tlaw.A0,
                             zone->Tlaw.R0,
                             zone->Tlaw.exponent,
                             disc_partial_mass(disc,zone->rstart,zone->rend)/M_SUN
                    );
            }
#endif
        }

        Rezone_debug("have %d zones : return %d\n",
                     disc->n_thermal_zones,
                     disc->n_thermal_zones==0 ? FALSE : TRUE);

        ret = disc->n_thermal_zones==0 ? FALSE : TRUE;
    }
    else
    {
        Rezone_debug("disc != disc : return FALSE\n");

        ret = FALSE;
    }
    return ret;
}

#endif // DISCS
