#include "../binary_c.h"
No_empty_translation_unit_warning;

#if defined DISCS

/*
 * Supplementary routines to numerically
 * calculate various integrals over the disc.
 *
 * Should be used to compare to the analytic routines.
 */

#ifdef TOPORT

double disc_total_luminosity_numerical(struct disc_t * disc)
{
    /*
     * Total luminosity in (one side of the) disc
     */
    double logR,dlogR = log10(disc->Rout - disc->Rin)/(double)DISC_ADAM_RESOLUTION;
    double Lint = 0.0;
    for(logR = log(disc->Rin); logR < log(disc->Rout); )
    {
        logR += 0.5 * dlogR;
        double R = exp(logR); // radius in cm
        double dR = dlogR * R;
        double T = disc_temperature(R,disc);
        double dA = 2.0 * PI * R * dR;
        Lint += dA * Pow4(T) * STEFAN_BOLTZMANN_CONSTANT;
        logR += 0.5 * dlogR;
    }
    return Lint;
}

double disc_total_mass_numerical(struct disc_t * disc)
{
    /*
     * Total mass in the disc
     */
    double logR,dlogR = log10(disc->Rout - disc->Rin)/
        (double)(DISC_ADAM_RESOLUTION);
    double Mint = 0.0;
    for(logR = log(disc->Rin); logR < log(disc->Rout); )
    {
        logR += 0.5 * dlogR;
        double R = exp(logR); // radius in cm
        double dR = dlogR * R;
        double sigma = disc_column_density(R,disc);
        double dM = sigma * 2.0 * PI * R * dR;
        Mint += dM;
        logR += 0.5 * dlogR;
    }

    return Mint;
}
double disc_total_angular_momentum_numerical(struct disc_t * disc,
                                             struct binary_system_t * binary)
{
    /*
     * Total angular momentum in the disc
     */
    double logR,dlogR = log10(disc->Rout - disc->Rin)/
        (double)DISC_ADAM_RESOLUTION;
    double Jint = 0.0;
    for(logR = log(disc->Rin); logR < log(disc->Rout); )
    {
        logR += 0.5 * dlogR;
        double R = exp(logR); // radius in cm
        double dR = dlogR * R;
        double dM = disc_column_density(R,disc) * 2.0 * PI * R * dR;
        double dJ = dM * disc_specific_angular_momentum(R,binary);
        Jint += dJ;
        logR += 0.5 * dlogR;
    }


    return Jint;
}


double zone_mass_numerical(struct disc_t * disc,
                               struct disc_thermal_zone_t * zone)
{
    double logR,dlogR = log10(zone->rstart - zone->rend)/
        (double)DISC_ADAM_RESOLUTION;
    double Mint = 0.0;

    for(logR = log(zone->rstart); logR < log(zone->rend); )
    {
        logR += 0.5 * dlogR;
        double R = exp(logR); // radius in cm
        double dR = dlogR * R;
        Mint += disc_column_density(R,disc) * 2.0 * PI * R * dR;
        logR += 0.5 * dlogR;
    }
    return Mint;
}


#endif //TOPORT
#endif // DISCS
