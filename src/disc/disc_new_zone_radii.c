#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef DISCS

/*
 * Zprintf is the local debugging function
 */
#undef Zprintf
#define Zprintf(...) /* */
//#define Zprintf(...) printf(__VA_ARGS__);fflush(NULL);

Boolean disc_new_zone_radii(struct disc_t * const disc)
{
    /*
     * Determine disc zone locations and their radii.
     *
     * Note that this function assumes that the temperature
     * functions are of the form,
     *
     *      T(R) = T_0 R^k,
     *
     * where k<0.
     *
     * You will need a better algorithm if there is a
     * temperature inversion (i.e. k>0 somewhere in the disc).
     *
     *
     * On input to this function, disc->thermal_zones contains
     * all possible zones.
     *
     * We use two arrays of thermal zones, P and Q.
     *
     * First we copy the disc's zones to P, set all their inner
     * radii to the disc reference length (1cm), and make them
     * all "valid".
     *
     * We then set the first zone in Q[0] : this is the hottest
     * at the reference length. Then we find the next crossing
     * point which defines zone Q[1], then continue to Q[2].
     *
     * Next the zones are trimmed according to the disc size,
     * and finally set into the disc.
     */
    Zprintf("\n\n\nNew zones %g %s %g %s; %g %s %g %s\n\n\n",
            disc->Rin/R_SUN,
            R_SOLAR,
            disc->Rout/R_SUN,
            R_SOLAR,
            disc->M/M_SUN,
            M_SOLAR,
            disc->J,
            J_CGS);

    /* allocate memory for P and Q */
    struct disc_thermal_zone_t * Restrict P =
        Malloc(sizeof(struct disc_thermal_zone_t)*
               (DISCS_MAX_N_ZONES));
    struct disc_thermal_zone_t * Restrict Q =
        Malloc(sizeof(struct disc_thermal_zone_t)*
               (DISCS_MAX_N_ZONES));

    /*
     * Copy the disc's zones to P and set all P's
     * zones to be valid.
     */
    Disc_zone_counter i;
    for(i=0;i<DISCS_MAX_N_ZONES;i++)
    {
        Copy_zone(disc->thermal_zones+i,P+i);
        P[i].valid = TRUE;
        P[i].Tlaw.R0 = DISC_REFERENCE_LENGTH;
        Zprintf("ZONE %u : A0 = %g, exponent = %g, R0 = %g\n",
               i,P[i].Tlaw.A0,P[i].Tlaw.exponent,P[i].Tlaw.R0);
    }
    Zprintf("\n\n\n");


    /*
     * Now we use the variable r as the inner edge of the "next"
     * zone. At the beginning, we set r at the disc reference length,
     * which is usually 1cm (i.e. unfeasibly close to the centre
     * of mass of the binary system).
     */
    double r = DISC_REFERENCE_LENGTH;

    /*
     * Counters: the number of remaining and allocated zones
     */
    Disc_zone_counter zones_remaining = DISCS_MAX_N_ZONES;
    Disc_zone_counter zones_allocated = 0;

//#define ___DEBUGGING
#ifdef ___DEBUGGING
    if(1)
    {
        /*
         * Debugging:
         * Show the crossing radius matrix
         */
        Disc_zone_counter i,j;
        for(i=0;i<DISCS_MAX_N_ZONES;i++)
        {
            for(j=0;j<=i;j++)
            {
                if(i!=j)
                {
                    const double Rx = disc_Rcross(&P[i],
                                                  &P[j]);
                    printf("%u x %u : %g %s\n",
                           i,
                           j,
                           Rx/R_SUN,
                           R_SOLAR);
                }
            }
        }
    }
#endif// ___DEBUGGING

    /*
     * Loop until all zones have been allocated
     */
    while(zones_remaining > 0)
    {
        /*
         * Find the hottest valid zone at this radius.
         *
         * Zones that have already been used are ignored.
         */
        int ihottest =
            disc_hottest_zone_at_radius(P,r,FALSE,NULL);

        Zprintf("Hottest at r = %30.15e %s is zone %d (disc Rin %g, Rout %g)\n",
                r/R_SUN,
                R_SOLAR,
                ihottest,
                disc->Rin,
                disc->Rout);

        /*
         * If ihottest is negative we're in trouble:
         * this should not happen, but it can when
         * one of the power laws has A=0 or something
         * like that. Such solutions should be rejected,
         * so return FALSE.
         */
        if(ihottest < 0)
        {
            Safe_free(P);
            Safe_free(Q);
            return FALSE;
        }

        /*
         * Zone P[ihottest] is the hottest zone at radius r
         *
         * So copy this onto the new zone list, Q, at
         * its next position, and set P[ihottest] to be invalid
         * so it cannot be set into Q twice.
         */
        Zprintf("Set zone P[%d] in Q[%u]\n",ihottest,zones_allocated);
        Copy_zone(&P[ihottest],
                  &Q[zones_allocated]);
        P[ihottest].valid = FALSE;
        zones_remaining--;

        /*
         * Set the end of this new zone, and r,
         * to the next zone boundary. This is where
         * we cross into the next zone.
         *
         * If there are no zones remaining, just set
         * next_crossing_radius to be DISC_LARGE_RADIUS,
         * and leave found_boundary as FALSE.
         */
        Boolean found_boundary = FALSE;
        double next_crossing_radius = DISC_LARGE_RADIUS;
        Disc_zone_counter Maybe_unused next_crossing_radius_index;

        if(zones_remaining > 0)
        {
            for(i=0;i<DISCS_MAX_N_ZONES;i++)
            {
                /* note: ihottest>=0 after previous check */
                if(i != (unsigned int)ihottest &&
                   P[i].valid == TRUE)
                {
                    Zprintf("Call RCross %d (%g %g %g %s) %u (%g %g %g %s)\n",
                            ihottest,
                            P[ihottest].Tlaw.A0,
                            P[ihottest].Tlaw.exponent,
                            P[ihottest].Tlaw.R0/R_SUN,
                            R_SOLAR,
                            i,
                            P[i].Tlaw.A0,
                            P[i].Tlaw.exponent,
                            P[i].Tlaw.R0/R_SUN,
                            R_SOLAR
                      );

                    /*
                     * Find the crossing point of the hottest
                     * zone at r with the other zones
                     */
                    const double Rx = disc_Rcross(&P[ihottest],
                                                  &P[i]);

                    /*
                     * If this crossing radius, Rx, is outside the
                     * current radius but smaller than next_crossing_radius,
                     * replace next_crossing_radius with Rx.
                     */
                    if(Rx > r &&
                       Rx < next_crossing_radius)
                    {
                        next_crossing_radius = Rx;
                        next_crossing_radius_index = i;
                        found_boundary = TRUE;
                    }
                    Zprintf("Zone %u : Rx at %30.15e %s\n",
                            i,
                            Rx/R_SUN,
                            R_SOLAR);
                }
            }
        }

        /*
         * The end of the zone is at the crossing point.
         *
         * We should also set r here because this is the new test point
         * on the next iteration of the main loop.
         */
        r = Q[zones_allocated].rend = next_crossing_radius;
        Zprintf("This zone (%u) ends at next crossing radius = %30.15e %s\n",
                zones_allocated,
                Q[zones_allocated].rend/R_SUN,
                R_SOLAR);

        /*
         * Make new zone in Q valid
         */
        Q[zones_allocated].valid = TRUE;

        if(found_boundary == TRUE)
        {
            Zprintf("Next crossing with zone %u, R = %30.15e %s\n",
                    next_crossing_radius_index,
                    next_crossing_radius/R_SUN,
                    R_SOLAR);
        }
        else
        {
            Zprintf("No more zones found\n");
        }

        /*
         * Move to next zone
         */
        zones_allocated ++;
    }

    /*
     * Set starting radii
     */
    Q[0].rstart = DISC_REFERENCE_LENGTH;
    for(i=1;i<DISCS_MAX_N_ZONES;i++)
    {
        Q[i].rstart = Q[i-1].rend;
        Zprintf("Set zone %u rstart = %30.15e %s\n",
                i,
                Q[i].rstart/R_SUN,
                R_SOLAR);
    }

    for(i=0;i<DISCS_MAX_N_ZONES;i++)
    {
        const struct disc_thermal_zone_t * Maybe_unused z = Q + i;
        Zprintf("ZONE %u %s %s : rstart = %30.15e %s to rend = %30.15e %s\n",
                i,
                Disc_zone_type_string(z),
                Disc_zone_valid_string(z),
                z->rstart/R_SUN,
                R_SOLAR,
                z->rend/R_SUN,
                R_SOLAR);
    }

    /*
     * Trim the zones in Q to make sure they are inside the disc
     */
    for(i=0;i<DISCS_MAX_N_ZONES;i++)
    {
        Clamp(Q[i].rstart, disc->Rin, disc->Rout);
        Clamp(Q[i].rend,   disc->Rin, disc->Rout);
    }

    /* eliminate zones with zero or negative width, or that are invalid */
    Disc_zone_counter nzones = DISCS_MAX_N_ZONES;
    for(i=0;i<nzones;i++)
    {
        const double width = Q[i].valid == FALSE ? 0.0 : (Q[i].rend - Q[i].rstart);
        if(width < DISC_MINIMUM_THERMAL_ZONE_WIDTH)
        {
            Disc_zone_counter j;
            for(j=i;j<nzones-1;j++)
            {
                Copy_zone(Q+j+1,Q+j);
            }
            i--;
            nzones--;
        }
    }

    /*
     * Copy valid thermal zones from Q
     * back into the disc
     */
    for(i=0;i<nzones;i++)
    {
        Copy_zone(Q+i,&disc->thermal_zones[i]);
    }
    disc->n_thermal_zones = nzones;

    /*
     * Check zone radii
     */
    for(i=0;i<disc->n_thermal_zones;i++)
    {
        const struct disc_thermal_zone_t * const z = disc->thermal_zones + i;
        if(z->rstart > z->rend)
        {
            fprintf(stderr,
                    "Disc zone %u has rstart = %g %s > rend = %g %s\n",
                    i,
                    z->rstart/R_SUN,
                    R_SOLAR,
                    z->rend/R_SUN,
                    R_SOLAR
                );
            Safe_free(P);
            Safe_free(Q);
            return FALSE;
        }
    }


    /*
     * Set up the temperature power laws
     */
    disc_set_temperature_power_laws(disc);

    Zprintf("\n\n\n");
    //disc_show_thermal_zones(disc,disc->thermal_zones,DISCS_MAX_N_ZONES);

    Safe_free(P);
    Safe_free(Q);

    return disc->n_thermal_zones==0 ? FALSE : TRUE;
}

#endif//DISCS
