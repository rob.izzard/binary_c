#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef DISCS

/*
 * Timescale on which material is stripped from the outer
 * edge of a disc.
 *
 * You might want this to be the orbital timescale,
 * or perhaps you think it should be zero (i.e. the
 * stripping is instantaneous) ?
 */

double disc_outer_edge_loss_timescale(struct stardata_t * const stardata,
                                      struct disc_t * const disc,
                                      const struct binary_system_t * const binary)
{
    const double t =
        disc_generic_stripping_timescale(disc,
                                         binary,
                                         disc->Revap_out,
                                         stardata->preferences->cbdisc_outer_edge_stripping_timescale);

    if(t < 0.0)
    {
        Exit_binary_c(BINARY_C_ALGORITHM_OUT_OF_RANGE,
                      "Stripping timescale at the outer edge is < 0 : this indicates that the algorithm has gone wrong. Check cbdisc_outer_edge_stripping_timescale is valid.\n");
    }

    return t;
}

#endif //DISCS
