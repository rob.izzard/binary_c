#pragma once
#ifndef DISC_DEBUG_H
#define DISC_DEBUG_H
/*
 * Macros for disc debugging statements 
 */
#include "../debug/debug_colours.h"

#define Disc_do_debug(N)                        \
    ((DISC_DEBUG) >= (N))

#define DISC_DEBUG_STREAM stdout

#define Discdebug(N,...)                        \
    if(Disc_do_debug(N))                        \
    {                                           \
        fprintf(DISC_DEBUG_STREAM,              \
                "%s%s%s %d%s : ",               \
                COLOUR_FILENAME,                \
                __FILE__,                       \
                COLOUR_LINENUMBERS,             \
                __LINE__,                       \
                stardata->store->colours[COLOUR_RESET]);                  \
        fprintf(DISC_DEBUG_STREAM,__VA_ARGS__); \
        fflush(DISC_DEBUG_STREAM);              \
    }

#define Discdebug_plain(N,...)                  \
    if(Disc_do_debug(N))                        \
    {                                           \
        fprintf(DISC_DEBUG_STREAM,__VA_ARGS__); \
        fflush(DISC_DEBUG_STREAM);              \
    }


#endif // DISC_DEBUG_H
