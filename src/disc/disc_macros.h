#pragma once
#ifndef DISC_MACROS_H
#define DISC_MACROS_H

/*
 * Macros related to the binary_c treatment of discs
 */

#include "disc_parameters.def"

#undef X
#define X(CODE,NUM) DISC_##CODE = (NUM),
enum { DISC_TYPES_LIST };
#undef X

#define X(CODE,NUM) DISC_##CODE##_EDGE = (NUM),
enum { DISC_POSITIONS_LIST };
#undef X

#define X(CODE,NUM) BISECT_##CODE = (NUM),
enum { DISC_BISECTION_CONSTRAINTS_LIST };
#undef X

#define X(CODE) DISC_CONVERGENCE_##CODE,
enum { DISC_CONVERGENCE_RESULTS_LIST };
#undef X

#define X(CODE) DISC_REMOVE_##CODE,
enum { DISC_REMOVAL_REASONS_LIST };
#undef X

#define X(CODE,NUM) DISC_SOLVER_##CODE = (NUM),
enum { DISC_SOLVERS_LIST };
#undef X

#define X(CODE) CBDISC_ECCENTRICITY_PUMPING_##CODE,
enum { CBDISC_ECCENTRICITY_PUMPING_LIST };
#undef X

#define X(CODE,NUM) CBDISC_ANGMOM_FROM_##CODE = (NUM),
enum { CBDISC_ANGMOM_FROM_LIST };
#undef X

#define X(CODE) CBDISC_MASS_LOSS_INNER_VISCOUS_ACCRETION_METHOD_##CODE,
enum { CBDISC_MASS_LOSS_INNER_VISCOUS_ACCRETION_METHOD_LIST };
#undef X

#define X(CODE,STRING) DISC_LOSS_##CODE,
enum { DISC_LOSS_LIST };
#undef X
#define X(CODE,STRING) STRING,
static const char * disc_loss_strings[] Maybe_unused = { DISC_LOSS_LIST };
#undef X

#define X(CODE) DISC_ZONES_##CODE,
enum { DISC_ZONES_STATUS_LIST };
#undef X

#define X(CODE) DISC_LOSS_ISM_##CODE,
enum { DISC_LOSS_ISM_LIST };
#undef X

#define X(CODE) DISC_LOG_##CODE,
enum { DISC_LOG_LIST };
#undef X

#define X(CODE) DISC_EDGE_STRIPPING_##CODE,
enum { DISC_EDGE_STRIPPING_LIST };
#undef X

#define X(CODE) DISC_TIMESTEP_LIMIT_##CODE,
enum { DISC_TIMESTEP_LIMIT_LIST };
#undef X

#define X(CODE,NUM) DISC_LOG_LEVEL_##CODE = (NUM),
enum { DISC_LOG_LEVEL_LIST };
#undef X

#define X(CODE,NUM) DISC_STRIPPING_TIMESCALE_##CODE = (NUM),
enum { DISC_STRIPPING_TIMESCALE_LIST };
#undef X

#define X(CODE) CBDISC_VISCOUS_PHOTOEVAPORATIVE_COUPLING_##CODE,
enum { CBDISC_VISCOUS_PHOTOEVAPORATIVE_COUPLING_LIST };
#undef X

/************************************************************/

/*
 * File pointers used to do logging
 */
#define Disc_log_filepointer (stardata->tmpstore->disc_logfile)
#define Disc_log_filepointer2d (stardata->tmpstore->disc_logfile2d)

/*
 * Macro that returns true if we want
 * to reopen log files
 */
#define Disc_can_reopen_log (                   \
        ((stardata->preferences->disc_log) > 0) \
        ? TRUE : FALSE                          \
        )
#define Disc_can_reopen_log2d (                         \
        ((stardata->preferences->disc_log2d) > 0)       \
        ? TRUE : FALSE                                  \
        )
/*
 * Macros that returns TRUE if we want to log
 */
#define Disc_can_log (                                          \
        (stardata->preferences->disc_log != DISC_LOG_LEVEL_NONE \
         &&                                                     \
         (stardata->tmpstore->disc_logfilecount==0 ||           \
          Disc_can_reopen_log))                                 \
        ? TRUE : FALSE                                          \
        )
#define Disc_can_log2d (                                                \
        (stardata->preferences->disc_log2d != DISC_LOG_LEVEL_NONE       \
         &&                                                             \
         (stardata->tmpstore->disc_logfilecount2d==0 ||                 \
          Disc_can_reopen_log2d))                                       \
        ? TRUE : FALSE                                                  \
        )

/************************************************************/

/*
 * Space for a new disc
 */
#define New_disc Malloc(sizeof(struct disc_t))

/*
 * Macro to determine whether "const disc" should be used
 * in function calls, or "disc". If we're using MEMOIZE,
 * disc can be changed (because it contains disc->memo which
 * is writable) so we don't want the const.
 */
#ifdef MEMOIZE
#define DISC_CONST
#else
#define DISC_CONST const
#endif

#endif // DISC_MACROS_H
