#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef DISCS

/*
 * Return a pointer to the innermost disc of object
 */

struct disc_t * outer_disc(const Disc_type type,
                           const void * object)
{
    struct disc_t * disc = NULL;
    if(type == DISC_CIRCUMSTELLAR)
    {
        struct star_t * star = (struct star_t *) object;
        disc = star->discs[Disc_pos_to_n(star,DISC_OUTER_EDGE)];
    }
    else if(type == DISC_CIRCUMBINARY)
    {
        struct stardata_t * stardata = (struct stardata_t *) object;
        disc = &stardata->common.discs[Disc_pos_to_n(&(stardata->common),DISC_OUTER_EDGE)];
    }
    else
    {
        Exit_binary_c_no_stardata(
            BINARY_C_UNKNOWN_DISC_TYPE,
            "Disc type %d unknown",
            type);

    }
    return disc;
}

#endif//DISCS
