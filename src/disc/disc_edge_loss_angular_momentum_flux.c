#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef DISCS

void disc_edge_loss_angular_momentum_flux(struct disc_t * const disc,
                                          const struct binary_system_t * const binary)
{

    /*
     * Calculate correction to the angular momentum flux
     * caused because of mass loss on, or slower than, the
     * disc's viscous timescale.
     */
    const double h_out = disc_specific_angular_momentum(disc->Rout,
                                                        binary);
    const double h_in = disc_specific_angular_momentum(disc->Rin,
                                                       binary);
    disc->F_stripping_correction =
        - 0.5 * (h_out - h_in) * (disc->loss[DISC_LOSS_XRAY].mdot +
                                  disc->loss[DISC_LOSS_FUV].mdot +
                                  disc->loss[DISC_LOSS_INNER_VISCOUS].mdot);
}


#endif //DISCS
