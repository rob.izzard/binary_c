#include "../binary_c.h"
No_empty_translation_unit_warning;
#include "common_envelope_prescriptions.h"

/*
 * Wrapper for common envelope evolution:
 *
 * here we have to choose which method, and hence subroutine,
 * to use, then call it.
 *
 * Usually we use the comenv_prescription set in preferences,
 * but if this has been called too often (which can happen)
 * we default to the BSE method.
 */
int common_envelope_evolution (
    struct star_t * const donor,
    struct star_t * const accretor,
    struct stardata_t *const stardata
)
{
    /*
     * Choose common envelope prescription
     *
     * If we reach MAX_NUMBER_OF_COMENVS - 3, switch
     * to Hurley et al. (2002) which avoid repeated comenvs.
     */
    const int comenv_prescription =
        (COMENV_BSE_AFTER >= 0 &&
         stardata->model.comenv_count >= COMENV_BSE_AFTER) ?
        COMENV_BSE :
        stardata->preferences->comenv_prescription[stardata->model.comenv_count];

    /*
     * Check we haven't exceeded MAX_NUMBER_OF_COMENVS
     */
    if(stardata->model.comenv_count >= MAX_NUMBER_OF_COMENVS)
    {
        Exit_binary_c(BINARY_C_ALGORITHM_OUT_OF_RANGE,
                      "Common envelope evolution called %d times, max. allowed is %d\n",
                      (int)stardata->model.comenv_count,
                      MAX_NUMBER_OF_COMENVS);
    }

    /*
     * Save stats for logging and choice of prescription
     */
    stardata->model.comenv_times[stardata->model.comenv_count] =
        stardata->model.time;
    stardata->model.comenv_overflower = donor->starnum;

    /*
     * Call chosen function
     */
    Dprint("call comenv function");
    int ret = 0;
    if(stardata->preferences->comenv_algorithm == COMENV_ALGORITHM_BINARY_C)
    {
        /*
         * New generic prescription
         */
        Dprint("Call binary_c's comenv\n");
        ret = common_envelope_evolution_binary_c(donor,
                                                accretor,
                                                stardata,
                                                comenv_prescription);
    }
    else
    {
        /*
         * Legacy codes
         */
#ifdef MINT
        if(stardata->preferences->stellar_structure_algorithm == STELLAR_STRUCTURE_ALGORITHM_MINT)
        {
            ret = common_envelope_evolution_MINT(donor,
                                                 accretor,
                                                 stardata,
                                                 comenv_prescription);
        }
#endif//MINT
#ifdef BSE
        if(stardata->preferences->stellar_structure_algorithm == STELLAR_STRUCTURE_ALGORITHM_MODIFIED_BSE)
        {
            ret = common_envelope_evolution_BSE(donor,
                                                accretor,
                                                stardata,
                                                comenv_prescription);
        }
#endif//BSE
    }

    /*
     * Update count and return
     */
    stardata->model.comenv_count++;
    return ret;
}
