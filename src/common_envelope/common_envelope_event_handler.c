#include "../binary_c.h"
No_empty_translation_unit_warning;


/*
 * Event handling function for common-envelope evolution
 *
 * Replaces calls in
 * 1) RLOF_dynamical_comenv
 * 2) contact_system
 */

Event_handler_function common_envelope_event_handler(void * eventp,
                                                     struct stardata_t * stardata,
                                                     void * data)
{
    struct binary_c_event_t * const event = eventp;

    struct binary_c_new_common_envelope_event_t * const event_data =
        (struct binary_c_new_common_envelope_event_t*) data;

    struct star_t * const donor = event_data->donor;
    struct star_t * const accretor = event_data->accretor;

    stardata->common.RLOF_do_overshoot = FALSE;
    if(Strings_equal(event->caller,"RLOF_dynamical_comenv"))
    {
        /*
         * Common-envelope evolution (q>qc) during RLOF.
         */
        Dprint("call comenv sep=%12.12e\n",stardata->common.orbit.separation);

        common_envelope_evolution(
            donor,
            accretor,
            stardata
            );

        donor->epoch = stardata->model.time - donor->age;

        /* check for merger */
        if(stardata->model.coalesce==TRUE)
        {
            stardata->model.com = TRUE;
        }
        else
        {
            /* reset epoch */
            accretor->epoch = stardata->model.time - accretor->age;
            Dprint("Set epoch %g\n",accretor->epoch);

            /* check for break up */
            if(stardata->common.orbit.eccentricity>1.0)
            {
#ifdef BSE
                Foreach_star(star)
                {
                    if(star->stellar_type>=NEUTRON_STAR)
                    {
                        double rc = core_radius(stardata,
                                                star,
                                                star->stellar_type,
                                                star->core_stellar_type,
                                                star->mass,
                                                star->mass,
                                                star->phase_start_core_mass,
                                                stardata->common.metallicity_parameters[ZPAR_MASS_HE_FLASH]);

                        if(Is_zero(rc)) rc = star->radius;

                        star->omega = star->angular_momentum/(CORE_MOMENT_OF_INERTIA_FACTOR*Pow2(rc)*star->mass);
                    }
                }
#endif//BSE
            }
            else
            {
                /*
                 * Reset orbital parameters as separation may have changed.
                 */
                stardata->common.orbit.period = (stardata->common.orbit.separation/AU_IN_SOLAR_RADII)*sqrt(stardata->common.orbit.separation/(AU_IN_SOLAR_RADII*Total_mass));
                stardata->common.orbit.angular_frequency = TWOPI/stardata->common.orbit.period;
            }
        }
    }
    else if(Strings_equal(event->caller,
                          "contact_system_call_comenv"))
    {


        /*
         * A system has gone into contact hence common envelope
         * evolution.
         */
        common_envelope_evolution(
            donor,
            accretor,
            stardata
            );
#ifdef BSE
        update_MS_lifetimes(stardata);
#endif//BSE

        const Star_number ndonor Maybe_unused = donor->starnum;
        Dprint("post comenv ndonor=%d m1=%g m01=%g\n",
               ndonor,
               stardata->star[ndonor].mass,
               stardata->star[ndonor].phase_start_mass);

        stardata->model.com = TRUE;
    }
    else
    {
        Exit_binary_c(BINARY_C_ALGORITHM_OUT_OF_RANGE,
                      "Unknown function called common_envelope_event_handler");
    }


    return NULL;
}
