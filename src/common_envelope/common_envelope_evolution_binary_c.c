#include "../binary_c.h"
No_empty_translation_unit_warning;

#define _COMMON_ENVELOPE_BINARY_C_STATIC_FUNCTIONS
#include "common_envelope_evolution.h"
#include "common_envelope_evolution_binary_c.h"
#include "common_envelope_evolution_binary_c_prototypes.h"

int common_envelope_evolution_binary_c (
    struct star_t * const donor,
    struct star_t * const accretor,
    struct stardata_t * const stardata,
    const int comenv_prescription
    )
{
    /*
     * Common-envelope evolution
     *
     * Binary_c version, suitable for any
     * stellar-evolution algorithms
     * (e.g. MINT, binary_c's BSE).
     *
     * Robert Izzard 2024
     *
     * todo:
     * per comenv number ejecta
     * various nelemans options
     * disc physics?
     */

    /*
     * Check the system is not single
     */
    if(System_is_single)
    {
        CEprint("Cannot do comenv on a single system\n");
        return FALSE;
    }

    /*
     * Entry set up: make an input_stardata for late
     * comparison (e.g. to compute yields), and deny supernovae
     * during the common-envelope phase
     */
    CEprint("**** Common envelope evolution (binary_c) at time %g model %d ****\n",
            stardata->model.time,
            stardata->model.model_number);
    CEprint("orbit in: separation %g, eccentricity %g, period %g, angular momentum %g\n",
            stardata->common.orbit.separation,
            stardata->common.orbit.eccentricity,
            stardata->common.orbit.period,
            stardata->common.orbit.angular_momentum);
    struct stardata_t * input_stardata = New_stardata_from(stardata);

    /*
     * Some functions require previous_stardata(s) to be set,
     * so just set the pointers (these should not be changed)
     */
    input_stardata->previous_stardata = stardata->previous_stardata;
    input_stardata->previous_stardatas = stardata->previous_stardatas;

    struct common_envelope_state_t * CE_state[CE_STATE_NUMBER] = { NULL };
    Foreach_star(star)
    {
        CEprint("star %d in: type %d%s, mass %g Msun, core mass %g Msun, radius %g Rsun, luminosity %g Lsun, age %g, omega %g, angular momentum %g\n",
                star->starnum,
                star->stellar_type,
                star->TZO == TRUE ? " TZO" : "",
                star->mass,
                Outermost_core_mass(star),
                star->radius,
                star->luminosity,
                star->age,
                star->omega,
                star->angular_momentum);
        star->deny_SN++;
    }

    /*
     * Setup: we need to erase pending events that would
     * interfere with common-envelope evolution
     * TODO: do we still need to do this? probably not!
     */
    //CEprint("Erase events");
    //_erase_events(_CE_args);

    /*
     * Update stellar structures
     */
    CEprint("Update stellar structures donor=%p accretor=%p stardata %p %p",
            (void*)donor,
            (void*)accretor,
            (void*)&stardata->star[0],
            (void*)&stardata->star[1]);
    _Update_stellar_structures(&donor,
                               &accretor);

#ifdef NUCSYN
    CEprint("Nucsyn");
    _init_comenv_nucsyn(_CE_args);
#endif // NUCSYN

    /*
     * Set up the input state struct
     */
    CEprint("Set up input state");
    CE_state[CE_PRE] = _CE_set_input_state_from_stars(_CE_args);
    CE_state[CE_PRE]->prescription = comenv_prescription;

    /*
     * Opeing log
     */
    _opening_log(_CE_args,
                 CE_state[CE_PRE]);

    /*
     * Update binding energies
     */
    CEprint("Update energies");
    _update_binding_energy(stardata,
                           CE_state[CE_PRE]);
    _update_orbital_energy(_CE_args,
                           CE_state[CE_PRE]);
    CEprint("system energy %g\n",
            CE_state[CE_PRE]->orbital_energy + CE_state[CE_PRE]->binding_energy);

    /*
     * common envelope phase
     */
    CEprint("Do common-envelope phase");
    CE_state[CE_DURING] = _common_envelope_phase(_CE_args,
                                                 CE_state[CE_PRE]);
    CEprint("Done common-envelope phase");

    /*
     * After common envelope
     */
    CE_state[CE_POST] = _CE_copy_state(stardata,
                                       CE_state[CE_DURING],
                                       CE_POST);

#ifdef NUCSYN
    /*
     * compute nucleosynthetic yields from ejection of the common envelope
     * (but not any subsequent supernova)
     */
    Abundance * Xej = NULL;
#endif // NUCSYN

    CEprint("Set ejecta and nucsyn\n");
    const double mass_lost Maybe_unused = _ejecta_and_nucsyn(_CE_args,
#ifdef NUCSYN
                                                             input_stardata,
#endif // NUCSYN
                                                             CE_state[CE_PRE],
                                                             CE_state[CE_POST]
#ifdef NUCSYN
                                                             ,&Xej
#endif // NUCSYN
        );



    /*
     * Check for stripped cores: they might need
     * their core masses updating
     */
    _check_stripped_cores(_CE_args,
                          CE_state[CE_POST]);

    /*
     * Set binary_c variables from post-CE state
     * and output log, and update stellar structures
     */
    CEprint("Set binary_c stars from state\n");
    _CE_set_binary_c_from_state(_CE_args,
                                CE_state[CE_POST]);
    CEprint("Stellar types %d %d\n",
            donor->stellar_type,
            accretor->stellar_type);
    CEprint("Donor Mgrav %g Mbary %g Mc He %g CO %g ONe %g n %g BH %g\n",
            donor->mass,
            donor->baryonic_mass,
            donor->core_mass[CORE_He],
            donor->core_mass[CORE_CO],
            donor->core_mass[CORE_ONe],
            donor->core_mass[CORE_NEUTRON],
            donor->core_mass[CORE_BLACK_HOLE]);
    CEprint("Radii %g %g\n",
            donor->radius,
            accretor->radius);
    CEprint("compute stellar age of donor\n");

    /*
     * Compute the age for non-dwarfs
     */
    if(!ON_EITHER_MAIN_SEQUENCE(donor->stellar_type))
    {
        _compute_stellar_age(stardata,
                             donor);
    }

    CEprint("Update stellar structures\n");
    _Update_stellar_structures(&donor,
                               &accretor);
    CEprint("Stellar types %d %d\n",
            donor->stellar_type,
            accretor->stellar_type);
    CEprint("Donor Mgrav %g Mbary %g Mc He %g CO %g\n",
            donor->mass,
            donor->baryonic_mass,
            donor->core_mass[CORE_He],
            donor->core_mass[CORE_CO]);
    CEprint("Radii %g %g\n",
            donor->radius,
            accretor->radius);

    if(CE_state[CE_POST]->coalesce == FALSE)
    {
        /*
         * update stellar spins such that they are in
         * tidal equilibrium
         */
        _update_detached_stellar_spins(_CE_args,
                                       CE_state[CE_POST]);

        /*
         * Use binary_c's variables to form a CB disc
         */
        _disc_formation(_CE_args,
                        CE_state[CE_POST]);
    }
    else
    {
        _update_merged_stellar_spin(_CE_args,
                                    CE_state[CE_POST]);
    }
    update_orbital_variables(stardata,
                             &stardata->common.orbit,
                             &stardata->star[0],
                             &stardata->star[1]);
    update_binary_star_variables(stardata,
                                 NULL);
    /*
     * Final log output
     */
    _closing_log(_CE_args,CE_state[CE_POST]);

    /*
     * clean up
     */
    _cleanup(_CE_args,CE_state);
    zero_derivatives(stardata);
#ifdef NUCSYN
    Safe_free(Xej);
#endif // NUCSYN

    CEprint("Comenv returns BINARY_C_NORMAL_EXIT sep=%g",
            stardata->common.orbit.separation);

    CEprint("orbit out: separation %g, eccentricity %g, period %g, angular momentum %g, angular frequency %g\n",
            stardata->common.orbit.separation,
            stardata->common.orbit.eccentricity,
            stardata->common.orbit.period,
            stardata->common.orbit.angular_momentum,
            stardata->common.orbit.angular_frequency
        );
    Foreach_star(star)
    {
        CEprint("star %d out: type %d%s, Mgrav %g Msun, Mbary %g Msun, Mc %g Msun, radius %g Rsun, Roche radius %g Rsun, luminosity %g Lsun, age %g, omega %g, angular momentum %g\n",
                star->starnum,
                star->stellar_type,
                star->TZO == TRUE ? " TZO" : "",
                star->mass,
                star->baryonic_mass,
                Outermost_core_mass(star),
                star->radius,
                star->roche_radius,
                star->luminosity,
                star->age,
                star->omega,
                star->angular_momentum);
        star->deny_SN--;
        if(star->TZO)
        {
            Append_logstring(LOG_TZO,
                             "TZO (comenv) : cores He %g CO %g n %g\n",
                             star->core_mass[CORE_He],
                             star->core_mass[CORE_CO],
                             star->core_mass[CORE_NEUTRON]);
        }
    }
    free_stardata(&input_stardata);
    return BINARY_C_NORMAL_EXIT;
}

static void _cleanup(_CE_args_in,
                     struct common_envelope_state_t ** const CE_state)

{
    /*
     * Free memory
     */
    for(int i=0; i<CE_STATE_NUMBER; i++)
    {
        if(CE_state[i] != NULL)
        {
            _free_state(&CE_state[i]);
        }
    }
}

static void _opening_log(_CE_args_in,
                         struct common_envelope_state_t * const state)
{
    /*
     * Logging on entry to common-envelope evolution
     */
    char * event_string Maybe_unused = events_stack_string(stardata);
    char * corestring[NUMBER_OF_STARS] = {
        core_string(&stardata->star[0],FALSE),
        core_string(&stardata->star[1],FALSE)
    };
    Append_logstring(LOG_COMENV_PRE,
                     "0COMENV n=%d nmod=%d presc=%d %g(%s;%s)+%g(%s;%s) ",
                     stardata->model.comenv_count
                     ,
                     stardata->model.model_number,
                     state->prescription,
                     donor->mass,
                     Short_stellar_type_string(stardata->star[0].stellar_type),
                     corestring[0],
                     accretor->mass,
                     Short_stellar_type_string(stardata->star[1].stellar_type),
                     corestring[1]
        );
    Safe_free(corestring[0]);
    Safe_free(corestring[1]);
    Safe_free(event_string);
}

static char * _merger_info_string(_CE_args_in,
                                  Boolean * const merger_info_alloced)
{
    /*
     * Return a string describing the eventual merger
     * (or not) of a system
     */
    char * merger_info;

    /*
     * time to merge by gravitational wave radiation (Myr)
     */
    const double dtmerge = Peters_grav_wave_merger_time(
        stardata,
        stardata->star[0].mass,
        stardata->star[1].mass,
        stardata->common.orbit.separation,
        stardata->common.orbit.eccentricity
        )*1e-6;

    if(dtmerge < 1e40)
    {
        if(asprintf(&merger_info,
                    "merge in %g Gyr at t = %g Gyr",
                    dtmerge*1e-3,
                    (stardata->model.time + dtmerge) * 1e-3)==-1)
        {
            merger_info = "no merger info";
            *merger_info_alloced = FALSE;
        }
        else
        {
            *merger_info_alloced = TRUE;
        }
    }
    else
    {
        merger_info = "never merges";
        *merger_info_alloced = FALSE;
    }
    return merger_info;
}

static void _closing_log(_CE_args_in,
                         struct common_envelope_state_t * const state)
{
    /*
     * log at end of common-envelope evolution
     * (note: should not depend on CE_state)
     */
    Boolean merger_info_alloced;
    const Boolean single = Is_zero(stardata->common.orbit.separation);
    char * corestring[NUMBER_OF_STARS] = {
        core_string(donor,FALSE),
        core_string(accretor,FALSE)
    };
    char * merger_info = _merger_info_string(_CE_args,
                                             &merger_info_alloced);
    Append_logstring(LOG_COMENV_POST,
                     "post-COMENV M=%g(%s;%s;Mbaryon=%g,Menv=%g,Mstart=%g,Teff=%g,age=%g) + M=%g(%s;%s;Mbaryon=%g,Menv=%g,Mstart=%g,Teff=%g,age=%g) a_out=%g%s P_out=%g (%g d) Jorb_out=%g R1=%g RL1=%g (R1/RL1=%g) Ω1/Ω1crit=%g, R2=%g RL2=%g (R2/RL2=%g) Ω2/Ω2crit=%g : Ωorb=%g Ω1=%g Ω2=%g [SN? %d %d] Single? %s : %s, events pending? %d",
                     // donor
                     donor->mass,
                     Short_stellar_type_string(donor->stellar_type),
                     corestring[0],
                     donor->baryonic_mass,
                     envelope_mass(donor),
                     donor->phase_start_mass,
                     Teff_from_star_struct(donor),
                     donor->age,
                     // accretor
                     accretor->mass,
                     Short_stellar_type_string(accretor->stellar_type),
                     corestring[1],
                     accretor->baryonic_mass,
                     envelope_mass(accretor),
                     accretor->phase_start_mass,
                     Teff_from_star_struct(accretor),
                     accretor->age,
                     // orbit
                      stardata->common.orbit.separation,
                     state->Politano2021 == TRUE ? " [Politano2021]" : "",
                     stardata->common.orbit.period, //y
                     stardata->common.orbit.period*YEAR_LENGTH_IN_DAYS,
                     Angular_momentum_code_to_cgs(stardata->common.orbit.angular_momentum),
                     donor->radius,
                     single ? 0.0 : donor->roche_radius,
                     single ? 0.0 : (donor->radius/Max(1e-10,donor->roche_radius)),
                     donor->omega/donor->omega_crit,
                     donor->omega,
                     accretor->omega,
                     stardata->common.orbit.angular_frequency,
                     accretor->radius,
                     single ? 0.0 : accretor->roche_radius,
                     single ? 0.0 : (accretor->radius/Max(1e-10,accretor->roche_radius)),
                     accretor->omega/accretor->omega_crit,
                     donor->SN_type,
                     accretor->SN_type,
                     Yesno(System_is_single),
                     merger_info,
                     events_pending(stardata)
        );
    Safe_free(corestring[0]);
    Safe_free(corestring[1]);
    if(merger_info_alloced)
    {
        Safe_free(merger_info);
    }
}

static void _erase_events(_CE_args_in)
{
    /*
     * Previous contact systems and common envelopes
     * should be erased.
     */
    if(0&&stardata->preferences->disable_events == FALSE)
    {
        erase_events_of_type(stardata,
                             BINARY_C_EVENT_CONTACT_SYSTEM,
                             stardata->common.current_event);
        erase_events_of_type(stardata,
                             BINARY_C_EVENT_COMMON_ENVELOPE,
                             stardata->common.current_event);
    }
}


static struct common_envelope_state_t * _CE_copy_state(struct stardata_t * const stardata,
                                                       struct common_envelope_state_t * const in,
                                                       const int phase)
{
    /*
     * Return a copy of the passed in common-envelope
     * state struct, or NULL on failure.
     */
    const size_t s = sizeof(struct common_envelope_state_t);
    struct common_envelope_state_t * const out = Calloc(1,s);
    if(out)
    {
        memcpy(out,in,s);
        out->donor = New_star_from(in->donor);
        out->accretor = New_star_from(in->accretor);
        out->phase = phase;
    }
    return out;
}

static void _CE_set_binary_c_from_state(_CE_args_in,
                                        struct common_envelope_state_t * const state)
{
    if(state != NULL)
    {
        /*
         * Set binary_c's stardata from our internal variables
         * after the common-envelope phase
         */

        /*
         * Many variables are automatically mapped
         * through the CE_STELLAR_VARIABLE_MAP_LIST
         * and CE_SYTEM_VARIABLE_MAP_LIST
         *
         * Note: here we divide by the multiplier to
         * convert back to binary_c code units
         */
#undef X
#define X(CE_STUB,BINARY_C_VAR,MULTIPLIER)                              \
        donor->BINARY_C_VAR  = state->CE_STUB##_donor / (MULTIPLIER);   \
        accretor->BINARY_C_VAR = state->CE_STUB##_accretor / (MULTIPLIER);
        CE_STELLAR_VARIABLE_MAP_LIST;
#undef X
#define X(CE_STUB,BINARY_C_VAR,MULTIPLIER)                      \
        stardata->BINARY_C_VAR = state->CE_STUB / (MULTIPLIER);
        CE_ORBIT_VARIABLE_MAP_LIST;
#undef X

        CEprint("Donor has core mass %g\n",
                Outermost_core_mass(donor));

        /*
         * Other orbital variables
         */
        stardata->common.orbit.angular_momentum = Angular_momentum_cgs_to_code(state->orbital_angular_momentum);
        stardata->common.orbit.period =
            (stardata->common.orbit.separation/AU_IN_SOLAR_RADII)*
            sqrt(stardata->common.orbit.separation/(AU_IN_SOLAR_RADII*(donor->mass + accretor->mass)));
        stardata->common.orbit.angular_frequency =  TWOPI / stardata->common.orbit.period;

        /*
         * Roche radii
         */
        _calc_roche_lobe_radii(state);
        _calc_cores_roche_lobe_radii(state);
        state->common_envelope_radius = Max(state->radius_donor,
                                            state->radius_accretor);
    }
}

static void _CE_update_state_orbit(_CE_args_in,
                                   struct common_envelope_state_t * const state)
{
    /*
     * Update system variables in a state from
     * the current stardata
     */
#undef X
#define X(CE_STUB,BINARY_C_VAR,MULTIPLIER)                      \
    state->CE_STUB = (stardata->BINARY_C_VAR) * (MULTIPLIER);
    CE_ORBIT_VARIABLE_MAP_LIST;
#undef X


    /*
     * Angular momenta (cgs)
     */
    state->orbital_angular_momentum =
        state->mass_reduced *
        sqrt(GRAVITATIONAL_CONSTANT * state->mass_total * M_SUN *
             stardata->common.orbit.separation * R_SUN  *
             (1.0 - Pow2(stardata->common.orbit.eccentricity)));
    state->stellar_angular_momentum = Angular_momentum_code_to_cgs(donor->angular_momentum + accretor->angular_momentum);
    state->stellar_cores_orbital_angular_momentum =
        (Outermost_core_mass(donor)*M_SUN)*(Outermost_core_mass(accretor)*M_SUN)*sqrt(GRAVITATIONAL_CONSTANT*(stardata->common.orbit.separation*R_SUN)/(M_SUN*(Outermost_core_mass(donor)+Outermost_core_mass(accretor))));

    state->stellar_cores_spin_angular_momentum =
        Angular_momentum_code_to_cgs( (Outermost_core_mass(donor)/ donor->mass)* donor->angular_momentum + (Outermost_core_mass(accretor)/ accretor->mass)* accretor->angular_momentum);

    state->stellar_cores_total_angular_momentum =
        state->stellar_cores_orbital_angular_momentum +
        state->stellar_cores_spin_angular_momentum;

    state->total_angular_momentum =
        state->stellar_angular_momentum +
        state->orbital_angular_momentum;

    /*
     * Orbital period and angular frequency
     */
    state->orbital_period = YEAR_LENGTH_IN_SECONDS *
        (stardata->common.orbit.separation/AU_IN_SOLAR_RADII)*
        sqrt(stardata->common.orbit.separation/(AU_IN_SOLAR_RADII*(donor->mass + accretor->mass)));
    state->orbital_angular_frequency = TWOPI / state->orbital_period;

}

static void _CE_update_state_from_stars(_CE_args_in,
                                        struct common_envelope_state_t * const state,
                                        const int location)
{
    /*
     * Update stellar state variables from star_t structs,
     * from either donor, accretor (in _CE_args) if
     * location == 0 or state->donor, state->accretor if
     * location == 1.
     *
     * Do not change the orbit or other
     * system variables, in a state from stardata
     */

    /*
     * Many variables are automatically mapped
     * through the CE_STELLAR_VARIABLE_MAP_LIST
     * and CE_SYTEM_VARIABLE_MAP_LIST
     */
    struct star_t * const d = location == 0 ? donor : state->donor;
    struct star_t * const a = location == 0 ? accretor : state->accretor;


    CEprint("ZZZ 1 %g %g donor %p state->donor %p\n",state->mass_donor/M_SUN,donor->mass,(void*)donor,(void*)state->donor);
#undef X
#define X(CE_STUB,BINARY_C_VAR,MULTIPLIER)                          \
    state->CE_STUB##_donor = (d->BINARY_C_VAR) * (MULTIPLIER);      \
    state->CE_STUB##_accretor = (a->BINARY_C_VAR) * (MULTIPLIER);
    CE_STELLAR_VARIABLE_MAP_LIST;
#undef X

    /*
     * Total and reduced masses
     */
    state->mass_total = state->mass_donor + state->mass_accretor;
    state->mass_reduced = state->mass_donor * state->mass_accretor /
        state->mass_total;
    CEprint("Set mass total %g + %g = %g Msun, reduced %g Msun\n",
            state->mass_donor / M_SUN,
            state->mass_accretor / M_SUN,
            state->mass_total / M_SUN,
            state->mass_reduced / M_SUN);

    /*
     * Core masses: if a star is a TZO,
     * use its neutron-star/black-hole core.
     */
    state->core_mass_donor =
        (d->TZO ? d->core_mass[CORE_NEUTRON] : Outermost_core_mass(d)) * M_SUN;
    state->core_mass_accretor =
        (a->TZO ? a->core_mass[CORE_NEUTRON] : Outermost_core_mass(a)) * M_SUN;
    CEprint("Set gravitational core masses donor %g accretor %g Msun\n",
            state->core_mass_donor / M_SUN,
            state->core_mass_accretor / M_SUN);

    /*
     * Baryonic core masses
     */
    state->baryonic_core_mass_donor = state->core_mass_donor;
    state->baryonic_core_mass_accretor = state->core_mass_accretor;
    if(d->stellar_type == NEUTRON_STAR || d->stellar_type == BLACK_HOLE)
    {
        state->baryonic_core_mass_donor +=
            (d->baryonic_mass - d->mass) * M_SUN;
    }
    if(a->stellar_type == NEUTRON_STAR || a->stellar_type == BLACK_HOLE)
    {
        state->baryonic_core_mass_accretor +=
            (a->baryonic_mass - a->mass) * M_SUN;
    }
    CEprint("Set baryonic core masses donor %g accretor %g Msun\n",
            state->baryonic_core_mass_donor / M_SUN,
            state->baryonic_core_mass_accretor / M_SUN);

#ifdef NUCSYN
    /*
     * Nucleosynthesis (optional)
     */
    state->Xenv_donor = d->Xenv;
    state->Xenv_accretor = a->Xenv;
#endif // NUCSYN

    /*
     * Compute effective core masses and radii
     */
    _calc_effective_core_masses(stardata,
                                state);
    _calc_effective_core_radii(state);

    /*
     * Hence effective envelope masses
     */
    state->effective_envelope_mass_accretor =  state->mass_accretor - state->effective_core_mass_accretor;
    state->effective_envelope_mass_donor =  state->mass_donor - state->effective_core_mass_donor;

    /*
     * ZAMS radii
     */
#define _set_ZAMS_radius(STAR)                                  \
    if(Is_zero(state->zams_radius_##STAR))                      \
    {                                                           \
        state->zams_radius_##STAR = R_SUN *                     \
            rzamsf(state->phase_start_mass_##STAR/M_SUN,        \
                   stardata->common.main_sequence_parameters);  \
    }
    _set_ZAMS_radius(donor);
    _set_ZAMS_radius(accretor);

    /*
     * We need to save the state of the most evolved COWD core
     * in the case one or both stars is a TPAGB star. In the case
     * of a merger, the number of thermal pulses is set to the
     * maximum of the two stars.
     */
    set_ntp(&state->ntp,
            &state->ntp_since_mcmin,
            &d->prev_tagb,
            stardata);

}

static struct common_envelope_state_t * _CE_set_input_state_from_stars(_CE_args_in)
{
    /*
     * Set common-envelope state variables
     * from binary_c's star_t and stardata_t
     */
    struct common_envelope_state_t * const state =
        Calloc(1,sizeof(struct common_envelope_state_t));

    if(unlikely(state==NULL))
    {
        Exit_binary_c(BINARY_C_ALLOC_FAILED,
                      "Failed to allocated CE state (PRE).");
    }
    else
    {
        state->phase = CE_PRE;
        state->donor = New_star_from(donor);
        state->accretor = New_star_from(accretor);
        state->envelope_response = COMENV_ENVELOPE_RESPONSE_EQUILIBRIUM;

        /*
         * Set up the state of the system just before common envelope,
         * using cgs units throughout so here we convert from
         * binary_c/BSE units to cgs.
         */

        /*
         * Stellar parameters e.g. mass, radius
         */
        _CE_update_state_from_stars(_CE_args,
                                    state,
                                    0);

        /*
         * Orbit
         */
        _CE_update_state_orbit(_CE_args,
                               state);

        /*
         * Stellar angular frequencies
         */
#define _ANGFREQ(STAR) \

        state->angular_frequency_donor =
            donor->angular_momentum /
            (donor->moment_of_inertia_factor*Pow2(donor->radius)*envelope_mass(donor)
             +
             CORE_MOMENT_OF_INERTIA_FACTOR*Pow2(donor->core_radius)* Outermost_core_mass(donor))
            / YEAR_LENGTH_IN_SECONDS;
        state->angular_frequency_accretor =
            accretor->angular_momentum /
            (accretor->moment_of_inertia_factor*Pow2(accretor->radius)*envelope_mass(accretor)
             +
             CORE_MOMENT_OF_INERTIA_FACTOR*Pow2(accretor->core_radius)* Outermost_core_mass(accretor))
            / YEAR_LENGTH_IN_SECONDS;


        /*
         * Hence total mass that contributes to the
         * common envelope
         */
        state->common_envelope_mass =
            state->effective_envelope_mass_accretor +
            state->effective_envelope_mass_donor;
        CEprint("Common envelope mass %g Msun\n",
                state->common_envelope_mass/M_SUN);

        /*
         * Roche radii
         */
        CEprint("Compute Roche radii\n");
        _calc_roche_lobe_radii(state);
        _calc_cores_roche_lobe_radii(state);


        /*
         * Common-envelope radius
         */
        state->common_envelope_radius = Max(state->radius_donor,
                                            state->radius_accretor);

        /*
         * assume not merged
         */
        state->coalesce = FALSE;
    }

    return state;
}

static void _update_stellar_structures(_CE_args_in,
                                       ...)
{
    /*
     * Foreach star_t struct passed in, updates its star_t struct
     * with the timestep set to zero (so there is no actual
     * evolution).
     */
    va_list stars;
    va_start(stars,stardata);
    struct star_t ** starp;

    /* evolve at zero timestep */
    const double dtwas = stardata->model.dt;
    stardata->model.dt = 0.0;

    while((starp = va_arg(stars,struct star_t **)))
    {
        if((char*)starp == (char*)donor ||
           (char*)starp == (char*)accretor)
        {
            Exit_binary_c(BINARY_C_ALGORITHM_BRANCH_FAILURE,
                          "Cannot have starp==(donor|accretor) in common envelope binary_c -> update_stellar_structures : use pointers to pointers please.\n");
        }

        struct star_t * const star = *starp;
        CEprint("Call stellar structure %d\n",star->stellar_type);
        stellar_structure(stardata,
                          STELLAR_STRUCTURE_CALLER_common_envelope_evolution_binary_c,
                          star,
                          NULL);

         {
             /*
              * Mass lost to the envelope may have
              * stripped us into the cores, shrink them if
              * so.
              */
             const double mc_outer = Outermost_core_mass(star);
             for(Core_type i=0; i<NUMBER_OF_CORES; i++)
             {
                 star->core_mass[i] = Min3(star->core_mass[i],
                                           star->mass,
                                           mc_outer);
             }
        }
        CEprint("After stellar structure %d model %d\n",star->stellar_type,stardata->model.model_number);
    }

    va_end(stars);

    /* erase any subsequent events */
    _erase_events(_CE_args);
    zero_derivatives(stardata);

    /* reset timestep */
    stardata->model.dt = dtwas;
}

static void _update_binding_energy(struct stardata_t * const stardata,
                                   struct common_envelope_state_t * const state)
{
    /*
     * Compute envelope binding energy, updating
     * lambdas in the process.
     */
#define _lambda_Debug(STAR)                                             \
    CEprint("call lambda %s : M0 %g, Mc %g, L %g, R %g, RZAMS %g, Rc %g, st %d, convfrac %g\n", \
            #STAR,                                                      \
            state->phase_start_mass_##STAR/M_SUN,                       \
            state->core_mass_##STAR/M_SUN,                              \
            state->luminosity_##STAR/L_SUN,                             \
            state->radius_##STAR/R_SUN,                                 \
            state->zams_radius_##STAR/R_SUN,                            \
            state->core_radius_##STAR/R_SUN,                            \
            state->stellar_type_##STAR,                                 \
            convfrac);

#ifdef NUCSYN
#define _Xenv_arg(STAR) state->Xenv_##STAR,
#else
#define _Xenv_arg(STAR)
#endif

#define _compute_lambda(STAR)                                           \
                                                                        \
    if(GIANT_LIKE_STAR(state->stellar_type_##STAR))                     \
    {                                                                   \
        const double menv = state->mass_##STAR - state->effective_core_mass_##STAR; \
        const double convfrac = Is_zero(menv) ? 0.0 : (state->mass_convective_envelope_##STAR / menv); \
        if(!In_range(convfrac,0.0,1.0))                                 \
        {                                                               \
            Exit_binary_c(BINARY_C_ALGORITHM_OUT_OF_RANGE,              \
                          "convfrac error  mconv=%g / (m-mc)=%g = %g\n",  \
                          state->mass_convective_envelope_##STAR/M_SUN, \
                          menv/M_SUN,                                   \
                          convfrac);                                    \
        }                                                               \
        _lambda_Debug(STAR);                                            \
        state->lambda_##STAR =                                          \
            common_envelope_lambda(                                     \
                state->phase_start_mass_##STAR/M_SUN,                   \
                state->effective_core_mass_##STAR/M_SUN,                \
                state->luminosity_##STAR/L_SUN,                         \
                state->radius_##STAR/R_SUN,                             \
                state->zams_radius_##STAR/R_SUN,                        \
                state->core_radius_##STAR/R_SUN,                        \
                convfrac,                                               \
                state->stellar_type_##STAR,                             \
                _Xenv_arg(STAR)                                         \
                stardata                                                \
                );                                                      \
                                                                        \
        CEprint("lambda %s : mass %g Msun, phase start mass %g Msun, core mass %g Msun, L %g Lsun, R %g Rsun, RZAMS %g Rsun, Rc %g Rsun, convfrac %g, st %d (TZO? %s) -> %g\n", \
                #STAR,                                                  \
                state->mass_##STAR/M_SUN,                               \
                state->phase_start_mass_##STAR/M_SUN,                   \
                state->effective_core_mass_##STAR/M_SUN,                \
                state->luminosity_##STAR/L_SUN,                         \
                state->radius_##STAR/R_SUN,                             \
                state->zams_radius_##STAR/R_SUN,                        \
                state->core_radius_##STAR/R_SUN,                        \
                convfrac,                                               \
                state->stellar_type_##STAR,                             \
                Yesno(state->TZO_##STAR),                               \
                state->lambda_##STAR);                                  \
    }                                                                   \
    else                                                                \
    {                                                                   \
        state->lambda_##STAR = LARGE_LAMBDA;                            \
    }

    _compute_lambda(donor);
    _compute_lambda(accretor);

    if(isnan(state->lambda_donor) ||
       isnan(state->lambda_accretor) ||
       isinf(state->lambda_donor) ||
       isinf(state->lambda_accretor))
    {
        CEprint("lambda is inf or nan error\n");
        Exit_binary_c(BINARY_C_COMENV_FAILED,
                      "Lambda is inf of nan in common envelope evolution (binary_c).");
    }
    /*
     * Hence binding energies
     */
#define _BE(STAR)                                                       \
    __extension__                                                       \
        ({                                                              \
            const double BE =                                           \
                Is_zero(state->lambda_##STAR) ||                        \
                Is_zero(state->radius_##STAR) ||                        \
                Is_zero(state->mass_##STAR) ?                           \
                0.0 :                                                   \
                (- GRAVITATIONAL_CONSTANT *                             \
                 state->mass_##STAR *                                   \
                 state->effective_envelope_mass_##STAR /                \
                 (state->lambda_##STAR * state->radius_##STAR)          \
                    );                                                  \
            CEprint("BE env star %s : type %d, lambda %g, Mgrav %g Msun, Mbary %g Msun, Mcgrav %g Msun, Mcbary %g Msun, Menv %g Msun, radius %g Rsun : BE %g erg\n", \
                    #STAR,                                              \
                    state->stellar_type_##STAR,                         \
                    state->lambda_##STAR,                               \
                    state->mass_##STAR/M_SUN,                           \
                    state->baryonic_mass_##STAR/M_SUN,                           \
                    state->core_mass_##STAR/M_SUN,                      \
                    state->baryonic_core_mass_##STAR/M_SUN,                      \
                    state->effective_envelope_mass_##STAR/M_SUN,        \
                    state->radius_##STAR/R_SUN,                         \
                    BE                                                  \
                );                                                      \
            fabs(BE) < TINY ? 0.0 : BE;                                 \
        })

    state->binding_energy_donor = _BE(donor);
    if(state->stellar_type_accretor != MASSLESS_REMNANT)
    {
        state->binding_energy_accretor = _BE(accretor);
    }
    else
    {
        state->binding_energy_accretor = 0.0;
    }
    state->binding_energy = state->binding_energy_donor + state->binding_energy_accretor;

    CEprint("envelope binding energy %g + %g = %g\n",
            state->binding_energy_donor,
            state->binding_energy_accretor,
            state->binding_energy);

    CEprint("EbindI %g + %g = %g\n",
            state->binding_energy_donor,
            state->binding_energy_accretor,
            state->binding_energy);

    {
        /*
         * Only log first time?
         *
         * We do this because otherwise, in the case of merged stars, there is
         * a *lot* of output to the log while we converge on a mass.
         */
        state->lambda_logged++;
        if(state->lambda_logged == 1)
        {
#undef X
#define X(CODE,NUM) #CODE,
            static char * lambda_prescription_strings[] = {
                "fixed",
                COMENV_LAMBDA_LIST
            };
#undef X
            const double _lambda = stardata->preferences->lambda_ce[stardata->model.comenv_count];
            const double _lambda_ion = stardata->preferences->lambda_ionisation[stardata->model.comenv_count];
            Append_logstring(LOG_COMENV_PRE,
                             "LAMBDA(%s,st=%d,m01=%g,mc1=%g,l1=%g,r1=%g,rzams=%g,lambda_ion=%g)=%g ",
                             lambda_prescription_strings[
                                 More_or_equal(_lambda,0.0) ? 0 : (int)(-_lambda+0.5)
                                 ],
                             state->stellar_type_donor,
                             state->phase_start_mass_donor/M_SUN,
                             state->core_mass_donor/M_SUN,
                             state->luminosity_donor/L_SUN,
                             state->radius_donor/R_SUN,
                             state->zams_radius_donor/R_SUN,
                             _lambda_ion,
                             state->lambda_donor);
        }
    }
}

static double _alpha(_CE_args_in,
                     struct common_envelope_state_t * const state Maybe_unused)
{
    return Max(1e-10,
               stardata->preferences->alpha_ce[stardata->model.comenv_count]);
}

static void _update_orbital_energy(_CE_args_in,
                                   struct common_envelope_state_t * const state)
{
    /*
     * Orbital energy
     */
    CEprint("orbital energy : separation %g Rsun\n",
            state->orbital_separation/R_SUN);
    state->orbital_energy =
        Is_zero(state->orbital_separation) ?
        -LARGE_ENERGY :
        (
            - GRAVITATIONAL_CONSTANT *
            state->effective_core_mass_donor *
            state->effective_core_mass_accretor
            /(2.0*state->orbital_separation)
            );
    CEprint("OOORB %g\n",
            state->orbital_energy);

    /*
     * Orbital energy assuming a circular orbit
     * with the same semi-major axis
     */
    state->circular_orbital_energy =
        state->orbital_energy /
        (1.0-Pow2(state->orbital_eccentricity));
    CEprint("orbital energy %g (circularised %g)\n",
            state->orbital_energy,
            state->circular_orbital_energy);
}

static double _gamma(_CE_args_in,
                     struct common_envelope_state_t * const state Maybe_unused)
{
    return stardata->preferences->nelemans_gamma;
}

static Boolean _core_accretion(_CE_args_in,
                               struct common_envelope_state_t * const state Maybe_unused)
{
    /*
     * Accrete material onto cores
     *
     * Return TRUE if material accretes, FALSE if not.
     */
    CEprint("no core accretion\n");
    return FALSE;
}

static struct common_envelope_state_t *
_common_envelope_phase(_CE_args_in,
                       struct common_envelope_state_t * const pre_ejection_state)
{
    /*
     * Common envelope evolutionary phase
     *
     * Returns the post-CE state
     */
    struct common_envelope_state_t * const state = _CE_copy_state(stardata,
                                                                  pre_ejection_state,
                                                                  CE_DURING);

    if(state != NULL)
    {
        /*
         * Do any core accretion
         */
        if(_core_accretion(_CE_args,
                           state))
        {
            CEprint("update stellar structures because of core accretion");
            _Update_stellar_structures(&donor,
                                       &accretor);
        }

        /*
         * Try to eject the envelope
         */
        CEprint("try to eject common envelope");
        if(_try_to_eject_envelope(_CE_args,pre_ejection_state,state) == FALSE)
        {
            /*
             * Failed to solve the orbit
             */
            Exit_binary_c(BINARY_C_ALGORITHM_BRANCH_FAILURE,
                          "Failed to solve orbit in common envelope (binary_c).");
        }
        CEprint("Separation when envelope is ejected %g Rsun\n",
                state->orbital_separation/R_SUN);
        CEprint("Effective core masses %g %g Msun\n",
                pre_ejection_state->effective_core_mass_donor/M_SUN,
                pre_ejection_state->effective_core_mass_accretor/M_SUN);

        /*
         * Detect merging systems
         */
        CEprint("detect merging systems\n");
        _detect_merger(_CE_args,
                       state);

        CEprint("Merge? %s\n",Yesno(state->coalesce));

        if(state->coalesce == TRUE)
        {
            /*
             * merged stars: envelope is not (completely) ejected.
             * mix_stars() automatically updates stellar structures
             */
            CEprint("merged");

            /*
             * Merge stars assuming no mass loss: we determine
             * the remaining envelope mass later
             */
            const double mass_loss_fraction_was = stardata->preferences->merger_mass_loss_fraction;
            stardata->preferences->merger_mass_loss_fraction = 0.0;
            _mix_stars(_CE_args,
                       state);
            _CE_update_state_from_stars(_CE_args,
                                        state,
                                        0);
            CEprint("Merged star type %d, M %g Msun, Mc %g Msun, R %g Rsun\n",
                    state->stellar_type_donor,
                    state->mass_donor/M_SUN,
                    state->core_mass_donor/M_SUN,
                    state->radius_donor/R_SUN);

            /*
             * We now have a merged star, in the "donor",
             * so update its binding energy
             */
            stardata->preferences->merger_mass_loss_fraction = mass_loss_fraction_was;
            CEprint("update binding energies\n");
            _update_binding_energy(stardata,
                                   state);

            struct common_envelope_state_t * merged_state = _CE_copy_state(stardata,
                                                                           state,
                                                                           CE_MERGED);

            _update_orbital_energy(_CE_args,
                                   merged_state);

            CEprint("merged state M %g Mc %g %g Msun, Eorb %g Ebind %g\n",
                    merged_state->mass_donor/M_SUN,
                    merged_state->effective_core_mass_donor/M_SUN,
                    merged_state->core_mass_donor/M_SUN,
                    merged_state->orbital_energy,
                    merged_state->binding_energy
                );

            CEprint("EorbF (coalesce) = Max(%g * %g / (2 * %g), %g)\n",
                    state->core_mass_donor/M_SUN,
                    pre_ejection_state->mass_accretor/M_SUN,
                    state->orbital_separation/R_SUN,
                    pre_ejection_state->orbital_energy);

            if(state->binding_energy >= 0.0)
            {
                /*
                 * Totally unbound envelope
                 */
                donor->mass = Outermost_core_mass(donor);
                donor->baryonic_mass = donor->mass + baryonic_mass_offset(donor);
            }
            else
            {
                /*
                 * Need final binding energy in the envelope
                 * Hurley's PhD Eq. 3.144
                 *
                 * To know the binding energy target, we need to know
                 * alpha.
                 */
                const double dEorb =
                    pre_ejection_state->orbital_energy - state->orbital_energy;

                CEprint("dEorb = %g - %g\n",
                        pre_ejection_state->orbital_energy,
                        state->orbital_energy);
                CEprint("Pre-ejection binding energy %g\n",pre_ejection_state->binding_energy);
                CEprint("Change in orbital energy %g\n",dEorb);
                CEprint("Orbital energy used to unbind the envelope %g\n",state->alpha * dEorb);
                CEprint("Sep just before merge %g\n",merged_state->orbital_separation/R_SUN);
                /*
                 * We use what energy we can to unbind the merged
                 * star's envelope.
                 *
                 * Note:
                 * Other formalisms (e.g. BSE) might want to use
                 *   pre_ejection_state->binding_energy + state->alpha * dEorb;
                 * but does this make sense to you?
                 */
                const double binding_energy_target =
                    pre_ejection_state->binding_energy + state->alpha * dEorb;
                //merged_state->binding_energy + state->alpha * dEorb;

                CEprint("Hence new target orbital energy %g\n",
                        binding_energy_target);
                CEprint("Binding energy target %g + %g * %g = %g\n",
                        pre_ejection_state->binding_energy,
                        state->alpha,
                        dEorb,
                        binding_energy_target);

                double Ebind_min, Ebind_max;

                /* binding energy with the full envelope */
                CEprint("Find binding energy with full envelope\n");
                Ebind_max = _binding_energy_merged_energy(_CE_args,
                                                          merged_state,
                                                          state);

                /* binding energy without the envelope */
                const double mass_offset_baryonic =
                    merged_state->baryonic_mass_donor - merged_state->mass_donor;
                CEprint("donor struct %p\n",(void*)donor);
                CEprint("Find binding energy without any envelope\n");
                CEprint("Donor was Mgrav %g Mbary %g core mass %g baryonic mass offset %g\n",
                        state->mass_donor / M_SUN,
                        state->baryonic_mass_donor / M_SUN,
                        state->core_mass_donor / M_SUN,
                        mass_offset_baryonic/M_SUN);
                {
                    const double mass_was = state->mass_donor;
                    const double core_mass_was = state->core_mass_donor;
                    const double baryonic_mass_was = state->baryonic_mass_donor;
                    const double radius_was = state->radius_donor;
                    const double effective_envelope_mass_was = state->effective_envelope_mass_accretor;

                    /* remove the envelope */
                    state->mass_donor = state->effective_core_mass_donor;
                    const double dm = mass_was - state->mass_donor;
                    state->baryonic_mass_donor -= dm;
                    state->effective_envelope_mass_donor = 0.0;
                    state->radius_donor = state->core_radius_donor;

                    CEprint("New Mgrav %g Mbary %g core mass %g dm %g (should be positive)\n",
                            state->mass_donor/M_SUN,
                            state->baryonic_mass_donor/M_SUN,
                            state->effective_core_mass_donor/M_SUN,
                            dm/M_SUN);

                    /* compute envelope binding energy (should be zero!) */
                    Ebind_min = _binding_energy_merged_energy(_CE_args,
                                                              merged_state,
                                                              state);
                    /* restore */
                    state->mass_donor = mass_was;
                    state->baryonic_mass_donor = baryonic_mass_was;
                    state->core_mass_donor = core_mass_was;
                    state->effective_envelope_mass_donor = effective_envelope_mass_was;
                    state->radius_donor = radius_was;
                    CEprint("Restored state after testing envelope removal: Mgrav = %g, Mbary = %g, Mc = %g, baryonic offset %g\n",
                            state->mass_donor / M_SUN,
                            state->baryonic_mass_donor / M_SUN,
                            state->core_mass_donor / M_SUN,
                            mass_offset_baryonic/M_SUN);
                }

                CEprint("binding energy target %g : range %g %g\n",
                        binding_energy_target,
                        Ebind_max,
                        Ebind_min);

                if(Ebind_min < Ebind_max)
                {
                    CEprint("*** WARNING Ebind_min %g < Ebind_max %g\n",
                            Ebind_min,
                            Ebind_max);
                }


                if(In_range(binding_energy_target,Ebind_max,Ebind_min))
                {
                    /*
                     * Check mononticity of binding energy vs mass:
                     * binding energy should rise as mass is removed
                     */
                    double mass = merged_state->mass_donor;
                    double min_mass = 0.0;
                    double max_mass = 0.0;

                    {
                        double Ebindwas = -1e100;

                        while(mass > state->effective_core_mass_donor)
                        {
                            state->mass_donor = mass;
                            state->baryonic_mass_donor = mass + mass_offset_baryonic;
                            state->effective_envelope_mass_donor = state->mass_donor - state->effective_core_mass_donor;
                            const double Ebind = _binding_energy_merged_energy(_CE_args,
                                                                               merged_state,
                                                                               state);
                            if(0)printf("BEX2 %g %g %g rising inwards? %s\n",
                                        mass/M_SUN,
                                        Ebind,
                                        binding_energy_target,
                                        Yesno(Ebind > Ebindwas));

                            if(Ebind > binding_energy_target &&
                               Ebindwas < binding_energy_target)
                            {
                                /*
                                 * We have exceeded the binding energy target
                                 * as mass decreases.
                                 *
                                 * Use this and the previous as the new
                                 * [min_mass,max_mass] bracket for bisection
                                 */
                                min_mass = mass;
                                max_mass = mass + 1e-2 * M_SUN;
                                break;
                            }

                            Ebindwas = Ebind;
                            mass -= 1e-2 * M_SUN;
                        }
                    }

                    if(Is_zero(min_mass))
                    {
                        /*
                         * Failed to find a solution that straddles
                         * has the target binding energy. This should not really be
                         * possible, but you never know.
                         *
                         * Assume the envelope is lost.
                         */
                        state->mass_donor = state->effective_core_mass_donor;
                        state->baryonic_mass_donor = state->mass_donor + mass_offset_baryonic;
                        state->effective_envelope_mass_donor = 0.0;
                    }
                    else
                    {
                        /*
                         * We found bounds, converge on the solution
                         * assuming the binding energy is monotonically
                         * decreasing (with increasing mass) between them
                         */
                        Boolean converged = FALSE;
                        size_t imass = 0;
                        const size_t max_imass = 100;
                        double m;
                        while(converged == FALSE &&
                              imass < max_imass)
                        {

                            CEprint("Try bisect with max_mass = %g Msun\n",
                                    max_mass/M_SUN);

                            const double guess_mass = min_mass + 0.01 * (max_mass - min_mass);
                            CEprint("guess mass from %g Msun\n",
                                    merged_state->mass_donor/M_SUN
                                );
                            const double tolerance = 1e-8;
                            const int max_iter = 100;
                            const double alpha = 1.0;
                            CEprint("bisect for binding energy %g : mass range %g to %g, initial guess %g\n",
                                    binding_energy_target,
                                    min_mass/M_SUN,
                                    max_mass/M_SUN,
                                    guess_mass/M_SUN);

                            int error = 0;
                            m = generic_bisect(
                                &error,
                                BISECT_USE_MONOCHECKS,
                                BISECTOR_COMENV_BINDING_ENERGY,
                                &_binding_energy_merged_energy_check_wrapper,
                                guess_mass, // initial guess
                                min_mass, // min
                                max_mass, // max
                                tolerance, // tolerance
                                max_iter, // max number of iterations
                                FALSE, // log values?
                                alpha, // alpha convergence

                                // va_args passed to _binding_energy_merged_energy_check_wrapper()
                                _CE_args,
                                merged_state, // state of merger without mass loss
                                state, // fluctuating system
                                binding_energy_target,
                                mass_offset_baryonic
                                );

                            CEprint("Merged star mass %g Msun (error %d)\n",
                                    m/M_SUN,
                                    error);

                            if(error || m < 0.0)
                            {
                                CEprint("ERROR in iterations : imass = %zu\n",imass);
                                converged = FALSE;
                                imass++;
                            }
                            else
                            {
                                CEprint("converged : m = %g = %g = %g of orig (new menv %g) state m %g (imass %zu)\n",
                                        m,
                                        m/M_SUN,
                                        m/merged_state->mass_donor,
                                        (m - state->core_mass_donor)/M_SUN,
                                        state->mass_donor/M_SUN,
                                        imass
                                    );
                                converged = TRUE;
                            }
                        }

                        /*
                         * Exit on failure
                         */
                        if(converged == FALSE)
                        {
                            Exit_binary_c(BINARY_C_COMENV_FAILED_TO_CONVERGE,
                                          "Failed to converge on required binding energy. This should not happen: it is a bug.");
                        }

                        state->mass_donor = m;
                        state->baryonic_mass_donor = m + mass_offset_baryonic;
                        Dprint("Set baryonic mass donor %g\n",m);
                        state->effective_envelope_mass_donor = state->mass_donor - state->effective_core_mass_donor;
                    }


                    state->stellar_type_donor = merged_state->stellar_type_donor;
                    state->mass_accretor = 0.0;
                    state->baryonic_mass_accretor = 0.0;
                    state->core_mass_accretor = 0.0;
                    state->effective_core_mass_accretor = 0.0;
                    state->stellar_type_accretor = MASSLESS_REMNANT;

                    CEprint("Final state donor: M = %g, Menv = %g, Mc = %g Msun, stellar type %d\n",
                            state->mass_donor/M_SUN,
                            state->effective_envelope_mass_donor/M_SUN,
                            state->effective_core_mass_donor/M_SUN,
                            state->stellar_type_donor
                        );

                }
                else
                {
                    /*
                     * Sufficient binding energy to eject the envelope
                     */
                    state->mass_donor = state->core_mass_donor;
                    state->baryonic_mass_donor = state->baryonic_core_mass_donor;
                    CEprint("Set baryonic mass donor %g\n",state->baryonic_mass_donor);
                    state->effective_envelope_mass_donor = state->mass_donor - state->effective_core_mass_donor;
                    _CE_update_state_from_stars(_CE_args,
                                                state,
                                                0);
                    CEprint("Binding energy = %g outside available range : eject envelope\n",
                            binding_energy_target);
                }
            }
            _free_state(&merged_state);
        }
        else
        {
            /*
             * ejection is successful: we're left with a binary.
             *
             * First, strip all the material from the donor and accretor.
             */
            CEprint("common envelope ejected\n");
            state->mass_donor = state->effective_core_mass_donor;
            state->mass_accretor = state->effective_core_mass_accretor;
            state->baryonic_mass_donor = state->effective_baryonic_core_mass_donor;
            state->baryonic_mass_accretor = state->effective_baryonic_core_mass_accretor;
            CEprint("Set baryonic mass of accretor %g Msun\n",state->effective_core_mass_accretor/M_SUN);

            state->mass_total = state->mass_donor + state->mass_accretor;
            state->mass_reduced = state->mass_donor * state->mass_accretor / state->mass_total;
            _update_orbit(_CE_args,
                          state);

            /*
             * Perhaps leave a small mass inside the donor's
             * Roche lobe?
             */
            CEprint("add small envelope to donor? %s\n",
                    Yesno(stardata->preferences->post_ce_objects_have_envelopes));
            _add_envelope_to_remnants(_CE_args,
                                      pre_ejection_state,
                                      state);

            /*
             * Update other masses
             */
            state->mass_total = state->mass_donor + state->mass_accretor;
            state->mass_reduced = state->mass_donor * state->mass_accretor / state->mass_total;


            /*
             * Update orbit
             */
            CEprint("update orbit\n");
            _Reset_orbit(state,
                         &state->orbital_separation,
                         &state->orbital_eccentricity);
            _solve_orbit(_CE_args,
                         state);

            /*
             * Update binary_c variables
             */
            _CE_set_binary_c_from_state(_CE_args,
                                        state);
        }
    }
#if DEBUG==1
    fflush(NULL);
#endif

    return state;
}

static double _binding_energy_merged_energy(_CE_args_in,
                                            struct common_envelope_state_t * const merged_state,
                                            struct common_envelope_state_t * const state)
{
    /*
     * Compute the envelope binding energy
     *
     * How we do this depends on state->envelope_response
     *
     * If this is COMENV_ENVELOPE_RESPONSE_DYNAMIC we use Hurley et al.'s
     * dynamical apporach.
     *
     * Otherwise, assume an equilibrium structure.
     */
    const double x = stardata->common.metallicity_parameters[7];
    const double menv = (state->mass_donor - state->effective_core_mass_donor)/M_SUN;
    CEprint("BIND menv = %g - %g = %g Msun\n",
            state->mass_donor / M_SUN,
            state->effective_core_mass_donor / M_SUN,
            menv);

    if(menv < 0.0)
    {
        Exit_binary_c(BINARY_C_COMENV_FAILED,
                      "error: menv = %g < 0 in common-envelope binding energy computation.\n",
                      menv);
    }
    if(state->envelope_response == COMENV_ENVELOPE_RESPONSE_DYNAMIC)
    {
        /*
         * Update giant's radius assuming dynamical
         * changes to its original radius (Hurley PhD Eq. 3.143)
         */
        state->radius_donor = merged_state->radius_donor * pow(merged_state->mass_donor / state->mass_donor,x);
    }
    else if(state->envelope_response == COMENV_ENVELOPE_RESPONSE_EQUILIBRIUM)
    {
        /*
         * Update stellar radius assuming
         * equilibrium structure without
         * changing the stellar age or core mass.
         */
        copy_star(stardata,donor,state->donor); // restore star
        state->stellar_type_donor = merged_state->stellar_type_donor;
        state->donor->stellar_type = merged_state->stellar_type_donor;
        state->donor->mass = state->mass_donor / M_SUN;
        state->donor->baryonic_mass = state->baryonic_mass_donor / M_SUN;
        state->donor->phase_start_mass = state->phase_start_mass_donor / M_SUN;

        CEprint("YYY pre (state) Mgrav = %g Mbary = %g, Mceff %g, Menv = %g, Mstart = %g, R = %g, st = %d\n",
                state->mass_donor/M_SUN,
                state->baryonic_mass_donor/M_SUN,
                state->effective_core_mass_donor/M_SUN,
                (state->mass_donor - state->effective_core_mass_donor)/M_SUN,
                state->phase_start_mass_donor / M_SUN,
                state->radius_donor/R_SUN,
                state->stellar_type_donor);
        CEprint("YYY pre (donor) Mgrav = %g Mbary = %g, Mc = %g, Menv = %g, menv = %g, frac = %g, R = %g, st = %d\n",
                state->donor->mass,
                state->donor->baryonic_mass,
                Outermost_core_mass(state->donor),
                envelope_mass(state->donor),
                state->donor->menv,
                Is_zero(envelope_mass(state->donor)) ? 0.0 :
                state->donor->menv / envelope_mass(state->donor),
                state->donor->radius,
                state->donor->stellar_type);

        _Update_stellar_structures(&state->donor);

        CEprint("YYY state->donor stellar struct M %g Mc %g Menv(M-Mc) %g R %g st %d Menv(conv) %g Menv/M %g\n",
                state->donor->mass,
                Outermost_core_mass(state->donor),
                envelope_mass(state->donor),
                state->donor->radius,
                state->donor->stellar_type,
                state->donor->menv,
                state->donor->menv/state->donor->mass);

        /* update radius and convective envelope mass : masses should not change */
        state->radius_donor = state->donor->radius * R_SUN;

        /*
         * update convective envelope mass :
         * note that there are times when calling the stellar structure
         * algorithm changes the core mass, e.g. second dredge up,
         * so just use the fractional envelope mass
         */

        const double _donor_menv = envelope_mass(state->donor);
        state->mass_convective_envelope_donor =
            (Is_zero(_donor_menv)|| Less_or_equal(state->donor->menv,1e-11))
            ? 0.0  :
            (
                /* fraction */
                state->donor->menv / _donor_menv *
                /* x actual common envelope mass */
                (state->mass_donor - state->core_mass_donor)
                );




        CEprint("YYY post M = %g, Mc = %g, R = %g, st = %d, Menv = %g %g %g frac conv %g\n",
                state->mass_donor/M_SUN,
                state->core_mass_donor/M_SUN,
                state->radius_donor/R_SUN,
                state->stellar_type_donor,
                state->mass_convective_envelope_donor/M_SUN,
                (state->mass_donor - state->core_mass_donor)/M_SUN,
                (state->mass_donor - state->effective_core_mass_donor)/M_SUN,
                Is_zero(state->mass_donor - state->effective_core_mass_donor) ? 0.0 :
                (state->mass_convective_envelope_donor/(state->mass_donor - state->effective_core_mass_donor)));

        /* restore state->donor from donor */
        copy_star(stardata,donor,state->donor);
    }
    else
    {
        Exit_binary_c(BINARY_C_ALGORITHM_OUT_OF_RANGE,
                      "Unknown common-envelope response to merging %d.\n",
                      state->envelope_response);
    }
    CEprint("Call binding energy update: donor mass %g core mass %g eff core mass %g envelope mass %g conv env mass %g\n",
            state->mass_donor/M_SUN,
            state->core_mass_donor/M_SUN,
            state->effective_core_mass_donor/M_SUN,
            (state->mass_donor - state->core_mass_donor)/M_SUN,
            state->mass_convective_envelope_donor/M_SUN);
    _update_binding_energy(stardata,
                           state);

    return state->binding_energy_donor;
}

static double _binding_energy_merged_energy_check(_CE_args_in,
                                                  struct common_envelope_state_t * const merged_state,
                                                  struct common_envelope_state_t * const state,
                                                  const double mass,
                                                  const double binding_energy_target,
                                                  const double mass_offset_baryonic)
{
    /*
     * We're trying to converge the binding energy
     * on binding_energy_transport.
     *
     * Note: we use and modify "state".
     */

    /*
     * Update masses given the mass passed in. The effective core
     * mass is fixed, but we change the total and envelope
     * masses.
     */
    state->mass_donor = mass;
    state->baryonic_mass_donor = mass + mass_offset_baryonic;
    state->effective_envelope_mass_donor = state->mass_donor - state->effective_core_mass_donor;
    const double menv = mass - state->effective_core_mass_donor;
    _binding_energy_merged_energy(_CE_args,
                                  merged_state,
                                  state);

    const double err =
        state->binding_energy_donor/binding_energy_target - 1.0;

    CEprint("mass = %g : menv = %g : (%g/%g-1) = %g\n",
            mass/M_SUN,
            menv/M_SUN,
            state->binding_energy_donor,
            binding_energy_target,
            err);

    CEprint("BEcheck mass = %g : menv = %g : (%g/%g-1) = %g\n",
            mass/M_SUN,
            menv/M_SUN,
            state->binding_energy_donor,
            binding_energy_target,
            err);

    return err;
}

static double _binding_energy_merged_energy_check_wrapper(const double mass,
                                                          void * p)
{
    /*
     * Generic bisect wrapper for _binding_energy_merged_energy_check
     */
    Map_GSL_params(p,args);
    Map_varg(struct star_t *,donor,args);
    Map_varg(struct star_t *,accretor,args);
    Map_varg(struct stardata_t *,stardata,args);
    Map_varg(struct common_envelope_state_t *,merged_state,args);
    Map_varg(struct common_envelope_state_t *,state,args);
    Map_varg(double,binding_energy_target,args);
    Map_varg(double,mass_offset_baryonic,args);
    va_end(args);
    return _binding_energy_merged_energy_check(_CE_args,
                                               merged_state,
                                               state,
                                               mass,
                                               binding_energy_target,
                                               mass_offset_baryonic);
}

static void _mix_stars(_CE_args_in,
                       struct common_envelope_state_t * const state)
{
    /*
     * Mix stars in state
     */
    donor->mass = state->mass_donor/M_SUN;
    donor->baryonic_mass = state->baryonic_mass_donor/M_SUN;
    accretor->mass = state->mass_accretor/M_SUN;
    accretor->baryonic_mass = state->baryonic_mass_accretor/M_SUN;

    CEprint("Donor : stellar type %d (TZO? %s) core masses [He %g CO %g ONeWD %g Fe %g n %g BH %g] supernova? %s\n",
            donor->stellar_type,
            Yesno(donor->TZO),
            donor->core_mass[CORE_He],
            donor->core_mass[CORE_CO],
            donor->core_mass[CORE_ONe],
            donor->core_mass[CORE_Fe],
            donor->core_mass[CORE_NEUTRON],
            donor->core_mass[CORE_BLACK_HOLE],
            Yesno(donor->SN_type != SN_NONE)
        );
    CEprint("Accretor : stellar type %d (TZO? %s) core masses [He %g CO %g ONeWD %g Fe %g n %g BH %g] supernova? %s\n",
            accretor->stellar_type,
            Yesno(accretor->TZO),
            accretor->core_mass[CORE_He],
            accretor->core_mass[CORE_CO],
            accretor->core_mass[CORE_ONe],
            accretor->core_mass[CORE_Fe],
            accretor->core_mass[CORE_NEUTRON],
            accretor->core_mass[CORE_BLACK_HOLE],
            Yesno(accretor->SN_type != SN_NONE)
        );

    CEprint("pre-merge radii 0=%g 1=%g donor=%g accretor=%g\n",
            stardata->star[0].radius,
            stardata->star[1].radius,
            donor->radius,
            accretor->radius);

    CEprint("Call merge_stars without ejection\n");
    struct star_t * merged_star = merge_stars(stardata,
                                              donor->starnum,
                                              FALSE
#ifdef NUCSYN
                                              ,state->common_envelope_abundance
#endif // NUCSYN
        );

    CEprint("merged star M=%g Mc He=%g CO=%g\n",
            merged_star->mass,
            merged_star->core_mass[CORE_He],
            merged_star->core_mass[CORE_CO]);

    CEprint("merged star radius %g\n",
            merged_star->radius);

    copy_star(stardata,
              merged_star,
              donor);
    stellar_structure_make_massless_remnant(stardata,
                                            accretor);

#ifdef NUCSYN
    nucsyn_zero_yields(accretor);
#endif

    CEprint("Mixed stars star_t : new stellar types %d %d (TZO? %s) core masses [He %g CO %g ONeWD %g Fe %g n %g BH %g] supernova? %s\n",
            donor->stellar_type,
            accretor->stellar_type,
            Yesno(donor->TZO),
            donor->core_mass[CORE_He],
            donor->core_mass[CORE_CO],
            donor->core_mass[CORE_ONe],
            donor->core_mass[CORE_Fe],
            donor->core_mass[CORE_NEUTRON],
            donor->core_mass[CORE_BLACK_HOLE],
            Yesno(donor->SN_type != SN_NONE)
        );
    CEprint("Mixed stars : new stellar types %d %d, masses %g %g, radii %g %g\n",
            donor->stellar_type,
            accretor->stellar_type,
            donor->mass,
            accretor->mass,
            donor->radius,
            accretor->radius);
    free_star(&merged_star);
}




static void _detect_merger(_CE_args_in,
                           struct common_envelope_state_t * const state)
{
    if(Is_zero(state->orbital_separation) || state->orbital_separation < 0.0)
    {
        CEprint("Separation is zero : merge\n");
        state->coalesce = TRUE;
    }
    else
    {
        _calc_cores_roche_lobe_radii(state);

        /*
         * Check for RLOF from one core onto the other :
         * we assume such systems merge. Set the orbital_separation
         * to the separation (semi-major axis, since e=0) at which they merge.
         */
        const double f_donor = rL1(state->effective_core_mass_donor / state->effective_core_mass_accretor);
        const double f_accretor = rL1(state->effective_core_mass_accretor / state->effective_core_mass_donor);
        //const double RL_donor = f_donor * state->orbital_separation;
        //const double RL_accretor = f_accretor * state->orbital_separation;
        const double aL_donor = state->effective_core_radius_donor / f_donor;
        const double aL_accretor = state->effective_core_radius_accretor / f_accretor;

        CEprint("(effective) q %g %g \n",
                state->effective_core_mass_donor / state->effective_core_mass_accretor,
                state->effective_core_mass_accretor / state->effective_core_mass_donor);
        CEprint("Core Roche radii %g %g\n",
                state->roche_radius_donor_effective_core/R_SUN,
                state->roche_radius_accretor_effective_core/R_SUN);
        CEprint("Core radii %g %g\n",
                state->effective_core_radius_donor/R_SUN,
                state->effective_core_radius_accretor/R_SUN);

        if(aL_donor > state->orbital_separation ||
           aL_accretor > state->orbital_separation)
        {
            struct star_t * const overflower = aL_donor > aL_accretor ? donor : accretor;
            const int first_star = overflower->starnum;
            state->coalesce = TRUE;
            stardata->model.coalesce = first_star;

            /*
             * core Roche radius at moment of overflow
             *
             * Which core overflows first?
             *
             * RLOF happens when
             *    Rc1 > a * f1=rL1(Mc1/Mc2)
             *  or
             *    Rc2 > a * f2=rL1(Mc2/Mc1)
             *
             * Rc1/f1 = aL_donor > a or Rc2/f2 = aL_accretor > a
             *
             * Which happens first? Whichever Rc/RL is
             * largest. This gives aL, the separation
             * at the moment of overflow, i.e. the final
             * orbital separation of a merging system.
             */
            state->orbital_separation = overflower == donor ? aL_donor : aL_accretor;

            CEprint("%s core overflows : Rcore = %g, f_core = %g, RL_core = %g, aL = %g (Rsun) [ aL other star %g, sep %g ]\n",
                    overflower == donor ? "Donor" : "Accretor",
                    (overflower == donor ? state->effective_core_radius_donor : state->effective_core_radius_accretor) / R_SUN,
                    overflower == donor ? f_donor : f_accretor,
                    (overflower == donor ? state->roche_radius_donor_effective_core : state->roche_radius_accretor_effective_core) / R_SUN,
                    state->orbital_separation / R_SUN,
                    (overflower == donor ? aL_accretor : aL_donor ) / R_SUN,
                    state->orbital_separation / R_SUN
                );

            /*
             * And update final orbital energy
             */
            _update_orbital_energy(_CE_args,
                                   state);
            CEprint("cores merge: Rc0=%g > RcL0=%g ? %s : Rc1=%g > RcL1=%g %s : star %d overflows first : a_merge = %g\n",
                    state->effective_core_radius_donor/R_SUN,
                    state->roche_radius_donor_effective_core/R_SUN,
                    Yesno(state->effective_core_radius_donor > state->roche_radius_donor_effective_core),
                    state->effective_core_radius_accretor/R_SUN,
                    state->roche_radius_accretor_effective_core/R_SUN,
                    Yesno(state->effective_core_radius_accretor > state->roche_radius_accretor_effective_core),
                    overflower->starnum,
                    state->orbital_separation/R_SUN
                );
        }
        else
        {
            state->coalesce = FALSE;
            stardata->model.coalesce = -1;
            CEprint("cores do not merge\n");
            CEprint("cores do not merge : separation %g\n",
                    state->orbital_separation/R_SUN);
        }
    }
}

static void _calc_roche_lobe_radii(struct common_envelope_state_t * const state)
{
    /*
     * calculate Roche radii of the stars
     */
    const double e1 = 1.0 - state->orbital_eccentricity;
    state->roche_radius_donor = Roche_radius3(state->mass_donor,
                                              state->mass_accretor,
                                              state->orbital_separation) * e1;
    state->roche_radius_accretor = Roche_radius3(state->mass_accretor,
                                                 state->mass_donor,
                                                 state->orbital_separation) * e1;
}

static void _calc_cores_roche_lobe_radii(struct common_envelope_state_t * const state)
{
    /*
     * calculate Roche radii of the effective cores
     */
    const double e1 = 1.0 - state->orbital_eccentricity;
    state->roche_radius_donor_effective_core    = e1 * Roche_radius3(state->effective_core_mass_donor,
                                                                     state->effective_core_mass_accretor,
                                                                     state->orbital_separation);
    state->roche_radius_accretor_effective_core = e1 * Roche_radius3(state->effective_core_mass_accretor,
                                                                     state->effective_core_mass_donor,
                                                                     state->orbital_separation);
}


static Boolean _try_to_eject_envelope(_CE_args_in,
                                      struct common_envelope_state_t * const pre_CE_state,
                                      struct common_envelope_state_t * const state)
{
    /*
     * Attempt to eject the common envelope.
     */

    /*
     * Compute post-common-envelope separation assuming the envelope is removed
     */
    CEprint("try to eject : prescription %d\n",state->prescription);
    if(state->prescription == COMENV_NELEMANS_TOUT)
    {
        /*
         * Nelemans and Tout (2005) angular momentum prescription
         */
        state->gamma = _gamma(_CE_args,
                              state);
        state->orbital_angular_momentum -= state->gamma * state->orbital_angular_momentum;
        state->orbital_eccentricity = stardata->preferences->comenv_post_eccentricity;
        _Reset_orbit(state,
                     &state->orbital_angular_momentum,
                     &state->orbital_eccentricity);
    }
    else if(state->prescription == COMENV_SCATTER)
    {
        /*
         * Scatter algorithm
         *
         * Di Stefano et al. ApJ 944, 87
         *
         * Thanks to Matthias Kruckow for helping with this!
         */
        const double dim = stardata->preferences->comenv_Scatter_dim; // free parameter
        const double eta =
            /* Fit of Patrick */
            Fequal(stardata->preferences->comenv_Scatter_eta,COMENV_SCATTER_ETA_FIT_PATRICK) ?
            exp10(0.603 - 0.952 * log10(state->core_mass_donor/(state->core_mass_donor + state->mass_accretor))) :

            /* constant */
            stardata->preferences->comenv_Scatter_eta; // free parameter

        const double mc = state->core_mass_donor;
        const double me = state->effective_envelope_mass_donor;
        const double m2 = state->mass_accretor; //   companion[0].m[n];
        const double fstar = pow(rL1(mc/m2),dim); // pow(rocherad(1.0, mc/m2),dim);
        const double fcompanion = pow(rL1(m2/mc),dim); // pow(rocherad(1.0, m2/mc),dim);
        const double multiplier =
            (m2 + mc)*
            Pow2(1.0 + me/mc)/(m2 + mc+me)*
            exp(-2.0*eta*(me/(m2 + mc))*(fstar*m2/mc + fcompanion*mc/m2)/(fstar + fcompanion));
        state->orbital_separation *= multiplier;
        state->orbital_eccentricity = stardata->preferences->comenv_post_eccentricity;
        _Reset_orbit(state,
                     &state->orbital_separation,
                     &state->orbital_eccentricity);
        printf("SCATTER mc %g, me %g, m2 %g, fstar %g, fcompanion %g, af/ai = %g\n",
               mc/M_SUN,
               me/M_SUN,
               m2/M_SUN,
               fstar,
               fcompanion,
               multiplier);
    }
    else if(state->prescription == COMENV_CONVECTION)
    {
        /*
         * Use convective turnover time to set the orbital period
         */
        const double tconv_s =
            convective_turnover_time(state->radius_donor/R_SUN,
                                     state->donor,
                                     stardata) *
            YEAR_LENGTH_IN_SECONDS;

        state->orbital_period = tconv_s *
            stardata->preferences->comenv_convection_multiplier;
        state->orbital_eccentricity = 0.0;

        _Reset_orbit(state,
                     &state->orbital_period,
                     &state->orbital_eccentricity);

        _solve_orbit(_CE_args,
                     state);

        /* required? */
        _return_angmom(_CE_args,
                       pre_CE_state,
                       state);
    }
    else
    {
        /*
         * alpha-prescription based on energy
         * (Hurley PhD Eqs. 3.138 and 3.139)
         *
         * Note that alpha must be positive, hence
         * it is at least 1e-100
         */
        state->alpha = (_alpha(_CE_args,pre_CE_state));
        state->orbital_eccentricity = stardata->preferences->comenv_post_eccentricity;

        state->orbital_separation =
            - GRAVITATIONAL_CONSTANT *
            state->effective_core_mass_donor * state->effective_core_mass_accretor /
            (2.0 * (pre_CE_state->orbital_energy +
                    pre_CE_state->binding_energy / state->alpha));

        if(stardata->preferences->Politano2021_comenv_criterion == TRUE)
        {
            /*
             * Use Politano's criterion
             */
            state->Politano2021 = TRUE;
            if(TRUE == _Politano2010_merge_criterion(stardata,
                                                     state,
                                                     state->orbital_separation,
                                                     stardata->preferences->Politano2021_comenv_criterion_use_cores))
            {
                state->orbital_separation = 0.0;
            }
            else
            {
                CEprint("POLITANO : sep was %g ... \n",
                        state->orbital_separation / R_SUN);
                state->orbital_separation = _Rhalt(state->effective_core_mass_donor);
                CEprint("POLITANO : sep now %g\n",
                        state->orbital_separation / R_SUN);
            }
        }

        CEprint("Alpha prescription: alpha = %g, a = %g Rsun (core mass donor %g Msun, accretor mass %g Msun, EorbF = %g erg)\n",
                state->alpha,
                state->orbital_separation / R_SUN,
                state->effective_core_mass_donor / M_SUN,
                state->effective_core_mass_accretor / M_SUN,
                (pre_CE_state->orbital_energy + pre_CE_state->binding_energy / state->alpha));

        CEprint("alpha = %g prescription -> separation = %g cm = %g Rsun\n",
                state->alpha,
                state->orbital_separation,
                state->orbital_separation/R_SUN);

        /*
         * Update the orbit
         */
        _Reset_orbit(state,
                     &state->orbital_separation,
                     &state->orbital_eccentricity);
        _solve_orbit(_CE_args,
                     state);

        /*
         * Aritificially return angular momentum to the orbit?
         */
        _return_angmom(_CE_args,
                       pre_CE_state,
                       state);
    }

    /*
     * Solve orbit
     */
    return _solve_orbit(_CE_args,
                        state);
}

static Maybe_unused
double Pure_function _nelemans_separation(const double ai, // initial separation
                                          const double Mi, // initial giant mass (Mg)
                                          const double mi, // initial companion mass // m
                                          const double Mf, // final giant mass (Mc)
                                          const double mf, // final companion mass // m
                                          const double gamma, // parameter
                                          struct stardata_t * Restrict const stardata)
{
    const double mitot = Max(TINY,Mi+mi);
    const double x = Max(TINY,Mf*mf);
    const double af = (ai*((Mf+mf)/mitot)*Pow2((Mi*mi)/x*
                                               (1.0-gamma*(Mi-Mf)/mitot)));

    CEprint("nelemans separation: Mi=%g mi=%g Mf=%g mf=%g\n",Mi,mi,Mf,mf);
    CEprint("Separation was %g now %g (factors %g %g %g)\n",ai,af,
            ai,
            ((Mf+mf)/mitot),
            Pow2((Mi*mi)/x*
                 (1.0-gamma*(Mi-Mf)/mitot))
        );

    /*
     * Alternative formula from his later paper, which gives
     * the same result - phew!
     */
    /*
      double Mg=Mi, m=mi, Mc=Mf, Me=Mg-Mc;
      double q=Mg/m, mu=Mc/Mg, Delta=Me/Mg, Dq=Delta*q/(1.0+q);

      double bracket1=(1-gamma*Dq)/(1-Delta);
      double bracket2=(1.0-Dq);
      CEprint("Alternative formula: %g (brackets=%g,%g)\n",
      ai * Pow2(bracket1) * bracket2,bracket1,bracket2
      );
    */

    /*
     * check the j-factor, if it is >=1.0 then return zero
     * because the binary merges
     */
    if(nelemans_j_factor(Mi,mi,Mf,mf,gamma,stardata) >= 1.0)
    {
        return 0.0;
    }
    else
    {
        return af;
    }
}

/************************************************************/

static Maybe_unused
double Pure_function _nelemans_j_factor(const double Mi,
                                        const double mi,
                                        const double Mf,
                                        const double mf,
                                        const double gamma,
                                        struct stardata_t * Restrict const stardata)
{
    // gamma * Delta M / (M_binary)
    const double f = gamma *(Mi+mi-Mf-mf) /(Mi+mi);
    CEprint("Nelemans J factor (gamma=%g deltaM=%g Mtotinit=%g) = %g\n",
            gamma,Mi+mi-Mf-mf,Mi+mi,f);

    /*
     * f must be in the range 0 to nelemans_max_frac_j_change
     * (note: this is very artificial)
     */
    return Limit_range(f,
                       0.0,
                       stardata->preferences->nelemans_max_frac_j_change);
}


static void _calc_effective_core_masses(struct stardata_t * const stardata Maybe_unused,
                                        struct common_envelope_state_t * const state)
{
    /*
     * Calculate the effective core masses - MS stars behave as cores during
     * the spiral in - hence their effective core mass is their total mass.
     * Other stars actually have a well defined core.
     *
     * An exception is a MS star spiralling into a giant:
     * the MS star is absorbed into the envelope so should
     * be treated as coreless.
     *
     * TYPE in _Mceff is either nothing, or baryonic_ to choose
     * the baryonic masses.
     */
#define _Mceff(STAR,TYPE)                                       \
    (                                                           \
        ON_EITHER_MAIN_SEQUENCE(state->stellar_type_##STAR) ?   \
        (state->TYPE##mass_##STAR) :                            \
        LATER_THAN_WHITE_DWARF(state->stellar_type_##STAR) ?    \
        (state->TYPE##mass_##STAR) :                            \
        (state->TYPE##core_mass_##STAR)                         \
        );
    state->effective_core_mass_donor = _Mceff(donor,);
    state->effective_core_mass_accretor = _Mceff(accretor,);
    state->effective_baryonic_core_mass_donor = _Mceff(donor,baryonic_);
    state->effective_baryonic_core_mass_accretor = _Mceff(accretor,baryonic_);

    CEprint("Effective core masses: gravitational %g %g Msun; baryonic %g %g Msun\n",
            state->effective_core_mass_donor/M_SUN,
            state->effective_core_mass_accretor/M_SUN,
            state->effective_baryonic_core_mass_donor/M_SUN,
            state->effective_baryonic_core_mass_accretor/M_SUN);
}

static void _calc_effective_core_radii(struct common_envelope_state_t * const state)
{
    /*
     * Calculate the effective core radii - MS stars behave as cores during
     * the spiral in - hence their effective core mass is their total mass.
     * Other stars actually have a well defined core.
     *
     * An exception is a MS star spiralling into a giant:
     * the MS star is absorbed into the envelope so should
     * be treated as coreless.
     */
#define _Reff(STAR)                                             \
    (                                                           \
        ON_EITHER_MAIN_SEQUENCE(state->stellar_type_##STAR) ?   \
        (state->radius_##STAR) :                                \
        LATER_THAN_WHITE_DWARF(state->stellar_type_##STAR) ?    \
        (state->radius_##STAR) :                                \
        (state->core_radius_##STAR)                             \
        );
    state->effective_core_radius_donor = _Reff(donor);
    state->effective_core_radius_accretor = _Reff(accretor);
}



static void _disc_formation(_CE_args_in,
                            struct common_envelope_state_t * const state Maybe_unused)
{
    /* todo */
}


static Boolean _solve_orbit(_CE_args_in,
                            struct common_envelope_state_t * const state Maybe_unused)
{
    /*
     * Given a set of orbital variables, solve for unknown (NaN)
     * values
     */
    Boolean nan_found = TRUE;

    const Boolean allow_debug_nan = stardata->preferences->allow_debug_nan;
    stardata->preferences->allow_debug_nan = TRUE;
    int iter = 0;
    const int max_iter = 20;
    while(nan_found == TRUE &&
          iter < max_iter)
    {
        iter++;
#undef X
#define X(VAR,FAC,UNIT)                                                 \
        CEprint("Iter %d %s : %s\n",                                    \
                iter,                                                   \
                #VAR,                                                   \
                isnan(state->orbital_##VAR) ? "unsolved" : "",          \
                isnan(state->orbital_##VAR) ? 0.0 : state->orbital_##VAR); \
        _CE_ORBIT_VARIABLES_LIST;
#undef X

        /* update nan_found */
        nan_found = FALSE;
#undef X
        int i = 0;
#define X(VAR,FAC,UNIT)                         \
        if(isnan(state->orbital_##VAR))         \
        {                                       \
            i++;                                \
            nan_found = TRUE;                   \
        }
        _CE_ORBIT_VARIABLES_LIST;
#undef X

        /* update orbit */
        _update_orbit(_CE_args,
                      state);

        CEprint("iter %d : nan count %d\n",iter,i);

    }
    stardata->preferences->allow_debug_nan = allow_debug_nan;
    _calc_roche_lobe_radii(state);
    _calc_cores_roche_lobe_radii(state);
    return iter >= max_iter ? FALSE : TRUE;
}

static void _update_orbit(_CE_args_in,
                          struct common_envelope_state_t * const state Maybe_unused)
{
    /*
     * given a state, update the orbit
     */
    if(isnan(state->orbital_period))
    {
        const double Porb = TWOPI * state->orbital_separation * sqrt(state->orbital_separation/(GRAVITATIONAL_CONSTANT * state->mass_total));
        if(!isnan(Porb))
        {
            state->orbital_period = Porb;
            CEprint("set Porb = %g s = %g days = %g years\n",
                    Porb,
                    Porb/DAY_LENGTH_IN_SECONDS,
                    Porb/YEAR_LENGTH_IN_SECONDS);
        }
    }

    if(isnan(state->orbital_separation))
    {
        {
            const double aorb = cbrt(GRAVITATIONAL_CONSTANT * state->mass_total * Pow2(state->orbital_period) / (4.0 * PI * PI));
            if(!isnan(aorb))
            {
                state->orbital_separation = aorb;
                CEprint("Set orbital separation = %g cm = %g Rsun\n",
                        aorb,
                        aorb/R_SUN);
            }
        }
        {
            const double aorb =
                Pow2(state->orbital_angular_momentum / state->mass_reduced)
                /
                (GRAVITATIONAL_CONSTANT * state->mass_total * (1.0 - Pow2(state->orbital_eccentricity)));
            if(!isnan(aorb))
            {
                state->orbital_separation = aorb;
                CEprint("Set orbital separation = %g cm = %g Rsun\n",
                        aorb,
                        aorb/R_SUN);
            }
        }
    }

    if(isnan(state->orbital_angular_frequency))
    {
        const double omega_orb = TWOPI / state->orbital_period;
        if(!isnan(omega_orb))
        {
            state->orbital_angular_frequency = omega_orb;
            CEprint("Set orbital angular frequency %g s^-1 = %g d^-1 = %g yr^-1\n",
                    omega_orb,
                    omega_orb * DAY_LENGTH_IN_SECONDS,
                    omega_orb * YEAR_LENGTH_IN_SECONDS);
        }
    }

    if(isnan(state->orbital_energy))
    {
        const double Eorb_was = state->orbital_energy;
        const double Eorb_circ_was = state->circular_orbital_energy;
        _update_orbital_energy(_CE_args,
                               state);
        if(isnan(state->orbital_energy)||
           isnan(state->circular_orbital_energy))
        {
            state->orbital_energy = Eorb_was;
            state->circular_orbital_energy = Eorb_circ_was;
        }

        CEprint("set orbital energy %g erg\n",
                state->orbital_energy);
    }

    if(isnan(state->orbital_eccentricity))
    {
        const double e2 =
            1.0 - Pow2(state->orbital_angular_momentum  / (state->mass_reduced * Pow2(state->orbital_separation) * state->orbital_angular_frequency));
        if(!isnan(e2))
        {
            state->orbital_eccentricity = e2 > 0.0 ? sqrt(e2) : 0.0;
            CEprint("set orbital eccentricity %g\n",
                    state->orbital_eccentricity);
        }
    }

    if(isnan(state->orbital_angular_momentum))
    {
        const double Jorb =
            state->mass_reduced *
            sqrt(GRAVITATIONAL_CONSTANT *
                 state->mass_total *
                 state->orbital_separation *
                 (1.0 - Pow2(state->orbital_eccentricity)));
        if(!isnan(Jorb))
        {
            state->orbital_angular_momentum = Jorb;
            CEprint("set orbital angular momentum %g cgs = %g code units\n",
                    Jorb,
                    Angular_momentum_cgs_to_code(Jorb));
        }
    }
}

static void _reset_orbit(_CE_args_in,
                         struct common_envelope_state_t * const state Maybe_unused,
                         ...)
{
    const size_t s = sizeof(struct common_envelope_state_t);
    struct common_envelope_state_t * out = Calloc(1,s);
    memcpy(out,state,s);

    /*
     * Given a list of pointers to orbital variables in
     * state struct, state, set these in a new state struct, out,
     * while setting all other orbital variables to NAN
     */

    /* set all orbital vars to NaN */
#undef X
#define X(VAR,FAC,UNIT) out->orbital_##VAR = NAN;
    _CE_ORBIT_VARIABLES_LIST;
#undef X

    /* set those passed in */
    va_list items;
    va_start(items,state);
    double * item;
    while((item = va_arg(items,double*)))
    {
        const ptrdiff_t offset =
            (ptrdiff_t)((char*)item - (char*)state);

        *(double*) ((char*)out + offset) = * item;

#undef X
#define X(VAR,FAC,UNIT)                                                 \
        if(                                                             \
            (ptrdiff_t)(                                                \
                (char*)&state->orbital_##VAR                            \
                -                                                       \
                (char*)state                                            \
                )                                                       \
            ==                                                          \
            (ptrdiff_t) offset                                          \
            )                                                           \
        {                                                               \
            CEprint("Set var %s at offset "                             \
                    __PTRDIFFT_FORMAT_SPECIFIER__                       \
                    " to %g %s\n",                                      \
                    #VAR,                                               \
                    (__PTRDIFFT_FORMAT_CAST__)offset,                   \
                    *item/(FAC),                                        \
                    (UNIT));                                            \
        }
        _CE_ORBIT_VARIABLES_LIST;
#undef X
    }

    /* copy into state */
    memcpy(state,out,s);

#undef X
#define X(VAR,FAC,UNIT)                             \
    if(isnan(state->orbital_##VAR))                 \
    {                                               \
        CEprint("reset state->orbit_%s\n",#VAR);    \
    }                                               \
    else                                            \
    {                                               \
        CEprint("keep state->orbit_%s = %g\n",      \
                #VAR,                               \
                state->orbital_##VAR);              \
    }
    _CE_ORBIT_VARIABLES_LIST;
#undef X

    /* free memory */
    Safe_free(out);
}


#ifdef NUCSYN
static void _init_comenv_nucsyn(_CE_args_in)
{
    CEprint("In comenv (nucsyn/MIXDEBUG)\n");
    Foreach_star(star)
    {
        CEprint("Star %d : surface H1=%g He4=%g C12=%g\n",
                star->starnum,
                star->Xenv[XH1],
                star->Xenv[XHe4],
                star->Xenv[XC12]);

        /* Mix any existing accretion layers into the envelopes */
        nucsyn_mix_accretion_layer_and_envelope(stardata,star,-1.0);

#ifdef COMENV_NORMALIZE_ABUNDS
        nucsyn_renormalize_abundance(star->Xenv);
#endif // COMENV_NORMALIZE_ABUNDS
    }

#ifdef NUCSYN_FORCE_DUP_IN_MERGERS
    Foreach_star(star)
    {
        /*
         * MS stars should perhaps have a first dredge up like event
         * when they are mixed into the envelope... ?
         *
         * HG and GB stars have a helium-rich region which should be mixed
         * into the envelope - this is the first dredge up
         *
         * CHeB (and later) stars have a He-rich core which is
         * (according to Jarrod's thesis, p102, section 3.9.1)
         * dense enough that it cannot be mixed into the envelope
         */
#ifdef NUCSYN_FIRST_DREDGE_UP
        if(((star->stellar_type==HERTZSPRUNG_GAP)||
            (star->stellar_type==GIANT_BRANCH))&&
           (star->first_dredge_up==FALSE)
#ifdef NUCSYN_WR
           &&(star->effective_zams_mass<NUCSYN_WR_MASS_BREAK)
#endif
            )
        {
            nucsyn_set_1st_dup_abunds(star->Xenv,
                                      star->mass,
                                      star->stellar_type,
                                      star,
                                      stardata,
                                      TRUE
                );
            star->first_dredge_up=TRUE;
        }
#endif // NUCSYN_FIRST_DREDGE_UP
#ifdef NUCSYN_STRIP_AND_MIX
        /*
         * Mix down to the core in this star
         */
        mix_down_to_core(star);

        /*
         * And disable strip and mix: we can now
         * just use traditional NUCSYN.
         */
        if(can_use_strip_and_mix(stardata,star)==FALSE)
            star->strip_and_mix_disabled=TRUE;
#endif//NUCSYN_STRIP_AND_MIX
    }
#endif // NUCSYN_FORCE_DUP_IN_MERGERS

#ifdef COMENV_NORMALIZE_ABUNDS
    Foreach_star(star)
    {
        nucsyn_renormalize_abundance(star->Xenv);
    }
#endif // COMENV_NORMALIZE_ABUNDS

}
#endif // NUCSYN

static void _update_detached_stellar_spins(_CE_args_in,
                                           struct common_envelope_state_t * const state Maybe_unused)
{
    /*
     * Update detached stellar spins so they match the
     * orbital angular frequency
     */

    /*
     * First, update rotation variables, e.g. v_crit and omega_crit
     */
    Foreach_star(star)
    {
        calculate_rotation_variables(star,star->radius);
    }

    /*
     * Now choose omega (angular frequency) of each star
     * or perhaps leave them as they were
     */
    if(stardata->preferences->comenv_ejection_spin_method == COMENV_EJECTION_SPIN_METHOD_SYNCHRONIZE)
    {
        donor->omega = state->orbital_angular_frequency * YEAR_LENGTH_IN_SECONDS;
        accretor->omega = state->orbital_angular_frequency * YEAR_LENGTH_IN_SECONDS;

    }
    else if(stardata->preferences->comenv_ejection_spin_method == COMENV_EJECTION_SPIN_METHOD_SYNCHRONIZE_IF_POSSIBLE)
    {
        /*
         * As the above synchronize, but limit to the star's omega_crit
         */
        donor->omega = Min(state->orbital_angular_frequency * YEAR_LENGTH_IN_SECONDS,
                           donor->omega_crit);
        accretor->omega = Min(state->orbital_angular_frequency * YEAR_LENGTH_IN_SECONDS,
                              accretor->omega_crit);
    }
    else if(stardata->preferences->comenv_ejection_spin_method == COMENV_EJECTION_SPIN_METHOD_BREAKUP_DONOR)
    {
        donor->omega = donor->omega_crit;
    }
    else if(stardata->preferences->comenv_ejection_spin_method == COMENV_EJECTION_SPIN_METHOD_BREAKUP_ACCRETOR)
    {
        accretor->omega = accretor->omega_crit;
    }
    else if(stardata->preferences->comenv_ejection_spin_method == COMENV_EJECTION_SPIN_METHOD_BREAKUP_BOTH)
    {
        donor->omega = donor->omega_crit;
        accretor->omega = accretor->omega_crit;
    }

    /*
     * Update angular momenta
     */
    donor->angular_momentum = stellar_angular_momentum(donor,
                                                       ANGULAR_MOMENTUM_STAR,
                                                       donor->radius,
                                                       donor->core_radius);
    accretor->angular_momentum = stellar_angular_momentum(accretor,
                                                          ANGULAR_MOMENTUM_STAR,
                                                          accretor->radius,
                                                          accretor->core_radius);

    /*
     * Recompute rotation variables, e.g. v_crit_ratio
     */
    Foreach_star(star)
    {
        calculate_rotation_variables(star,star->radius);
    }
}

static void _update_merged_stellar_spin(_CE_args_in,
                                        struct common_envelope_state_t * const pre_CE_state)
{
    /*
     * What spin should we assign a merged star?
     *
     * Note: we expect the donor's binary_c star struct to
     *       contain the merged star.
     */
    if(stardata->preferences->comenv_merger_spin_method ==
       COMENV_MERGER_SPIN_METHOD_CONSERVE_ANGMOM)
    {
        /*
         * Assume all angular momentum which went into the common
         * envelope merger system is kept in the systemx
         */
        donor->angular_momentum = pre_CE_state->total_angular_momentum;
    }
    else if(stardata->preferences->comenv_merger_spin_method ==
            COMENV_MERGER_SPIN_METHOD_CONSERVE_OMEGA)
    {
        /*
         * BSE assumes that the final spin rate of the envelope equals
         * that of the system just before CEE.
         * This seems unlikely, as the resulting equatorial
         * velocity is often thousands of km/s.
         */
        donor->angular_momentum =
            pre_CE_state->orbital_angular_frequency * YEAR_LENGTH_IN_SECONDS *
            moment_of_inertia(stardata, donor, donor->radius);
    }
    else if(stardata->preferences->comenv_merger_spin_method ==
            COMENV_MERGER_SPIN_METHOD_SPECIFIC)
    {

        /*
         * Because I don't believe the, limit the angular momentum to
         * (M1 / Min) * jtot, i.e. angular momentum is conserved during the
         * common envelope evolution, and ejected material takes the
         * envelope's specific angular momentum with it.
         */
        const double l = pre_CE_state->total_angular_momentum / pre_CE_state->mass_total;
        donor->angular_momentum = Min(donor->angular_momentum,
                                      donor->mass * l);
    }
    else if(stardata->preferences->comenv_merger_spin_method ==
            COMENV_MERGER_SPIN_METHOD_BREAKUP)
    {
        /*
         * Assume material falls back down onto the merged star
         * from the ejected envelope. The merged object that hasn't
         * been ejected will be rotating at its Keplerian rate,
         * and material that accretes from a surrounding "disc"
         * will accrete at the Keplerian rate, so leave the star
         * at breakup.
         *
         * This is wrong if no material is ejected!
         */
        donor->angular_momentum = breakup_angular_momentum(stardata,
                                                           donor);
    }
    else
    {
        Exit_binary_c(BINARY_C_ALGORITHM_BRANCH_FAILURE,
                      "Unknown comenv merger spin method %d",
                      stardata->preferences->comenv_merger_spin_method);
    }
    donor->omega = donor->angular_momentum /
        moment_of_inertia(stardata,
                          donor,
                          donor->radius);


}

static void _add_envelope_to_remnants(_CE_args_in,
                                      struct common_envelope_state_t * const pre_CE_state,
                                      struct common_envelope_state_t * const state)
{
    /*
     * Add envelope to remnants of a detached post-CEE systems
     * if they are giant-like stars.
     *
     * Non-giant like stars cannot have similar accretion
     * because the accreted material would be far too dense
     * to be part of the common envelope.
     *
     * When passed in, both stars have already been stripped.
     */

    /*
     * donor envelope mass
     */
    if(stardata->preferences->post_ce_objects_have_envelopes)
    {
        const double menv_donor =
            GIANT_LIKE_STAR(state->stellar_type_donor) ?
            _add_envelope_to_remnant(_CE_args,
                                     pre_CE_state,
                                     state,
                                     donor,
                                     state->roche_radius_donor_effective_core,
                                     pre_CE_state->core_mass_donor,
                                     pre_CE_state->stellar_type_donor) :
            0.0;
        const double menv_accretor =
            GIANT_LIKE_STAR(state->stellar_type_accretor) ?
            _add_envelope_to_remnant(_CE_args,
                                     pre_CE_state,
                                     state,
                                     accretor,
                                     state->roche_radius_accretor_effective_core,
                                     pre_CE_state->core_mass_accretor,
                                     pre_CE_state->stellar_type_accretor) :
            0.0;

        /*
         * Update donor and state mass variables
         */
        state->mass_donor += menv_donor;
        state->baryonic_mass_donor += menv_donor;
        state->mass_accretor += menv_accretor;
        state->baryonic_mass_accretor += menv_accretor;
        state->common_envelope_mass -= menv_donor + menv_accretor;
        state->mass_reduced = state->mass_donor * state->mass_accretor / state->mass_total;

        /*
         * Update the stellar parts of the state
         */
        _CE_update_state_from_stars(_CE_args,
                                    state,
                                    0);
    }
}

static double _add_envelope_to_remnant(_CE_args_in,
                                       struct common_envelope_state_t * const pre_CE_state,
                                       struct common_envelope_state_t * const state,
                                       struct star_t * const pre_star,
                                       const double target_radius_in,
                                       const double initial_core_mass,
                                       const Stellar_type initial_stellar_type)
{
    /*
     * Return envelope mass in grams
     */
    struct star_t * star = NULL;
    double menv;
    if(stardata->preferences->post_ce_adaptive_menv == TRUE)
    {
        const double target_radius = target_radius_in * 0.999 / R_SUN; // R_SUN
        const double tolerance = 1e-6; // fraction
        const double dm_min = 1e-9; // stop at this
        CEprint("Add adaptive envelope to star %d: target radius %g Rsun, tolerance %g Msun\n",
                pre_star->starnum,
                target_radius,
                tolerance);

        /*
         * Start with a very massive envelope, then
         * remove it until we're at the target radius
         */
        menv = envelope_mass(pre_star); // initially lots of mass
        const double mass_offset_baryonic = baryonic_mass_offset(pre_star);
        double prev_radius = pre_star->radius;
        double dm = 0.95 * menv; // abs(delta mass) : hence positive (M_SUN)
        int iter = 0;
        const int max_iter = 100;
        while(iter++ < max_iter)
        {
            if(star != NULL)
            {
                free_star(&star);
            }

            /*
             * Update structure of the star with the current menv
             */
            star = New_star_from(pre_star);
            star->stellar_type = initial_stellar_type;
            star->mass = initial_core_mass/M_SUN + menv;
            star->baryonic_mass = star->mass + mass_offset_baryonic;
            _Update_stellar_structures(&star);
            CEprint("%d: dm = %g, menv = %g Msun -> radius %g Rsun vs target %g Rsun\n",
                    iter,
                    dm,
                    menv,
                    star->radius,
                    target_radius);

            /*
             * if exceeded target, reset mass and zoom
             */
            if(star->radius < target_radius)
            {
                CEprint("Radius below target : go back and halve dm\n");
                menv += dm;
                dm *= 0.5;
            }
            else if(star->radius < prev_radius)
            {
                /* shrinking : accelerate */
                CEprint("Shrinking: this is good\n");
                dm *= 1.2;
                dm = Min(0.5*menv, dm);
            }

            /*
             * Otherwise add mass
             */
            else
            {
                prev_radius = star->radius;
            }

            /*
             * Stop if dm < 1e-8 Msun (resolution limit)
             * or we've exceeded the maximum mass
             */
            if(dm < dm_min)
            {
                CEprint("dm too small : stop\n");
                break;
            }
            else if(Abs_diff(target_radius,star->radius) < tolerance)
            {
                CEprint("Converged!\n");
                break;
            }

            /*
             * Update menv for next time
             */
            menv = Max(0.5*menv,
                       menv - dm);
        }
    }
    else
    {
        /*
         * Fixed menv
         */
        star = New_star_from(pre_CE_state->donor);
        menv =
            (state->stellar_type_donor==TPAGB ? POST_CE_ENVELOPE_DM_TPAGB :
             state->stellar_type_donor==EAGB ? POST_CE_ENVELOPE_DM_EAGB :
             POST_CE_ENVELOPE_DM_GB) * M_SUN;
        update_mass(stardata,star,menv / M_SUN);
        _Update_stellar_structures(&star);
    }

    /*
     * Copy to existing star, and free memory
     */
    copy_star(stardata,star,pre_star);

    CEprint("Star %d menv %g Msun, core mass %g Msun, radius %g Rsun, luminosity %g Lsun \n",
            star->starnum,
            menv/M_SUN,
            Outermost_core_mass(star),
            star->radius,
            star->luminosity);
    free_star(&star);

    return menv;
}

static void _free_state(struct common_envelope_state_t ** statep)
{
    struct common_envelope_state_t * state = *statep;
    free_star(&state->donor);
    free_star(&state->accretor);
    Safe_free(state);
}



static void _check_stripped_cores(_CE_args_in,
                                  struct common_envelope_state_t * const post_CE_state)
{
    /*
     * Check for stripped primary, might need to alter the
     * core mass(es) so that giant_age gets its age computation
     * right. This happens when we have a star with CO and He
     * cores, i.e. EAGB stars, that become He stars.
     */
    if(Less_or_equal(post_CE_state->mass_donor,
                     post_CE_state->effective_core_mass_donor))
    {
        if(post_CE_state->stellar_type_donor == EAGB)
        {
            post_CE_state->stellar_type_donor = HeHG;
            donor->stellar_type = HeHG;
            donor->core_mass[CORE_He] = 0.0;
        }
    }
    if(Less_or_equal(post_CE_state->mass_accretor,
                     post_CE_state->effective_core_mass_accretor))
    {
        if(post_CE_state->stellar_type_accretor == EAGB)
        {
            post_CE_state->stellar_type_accretor = HeHG;
            accretor->stellar_type = HeHG;
            accretor->core_mass[CORE_He] = 0.0;
        }
    }
}


static double _Rflat(const double core_mass)
{
    /*
     * Eq.2 of Politano (2021, A&A 648, L6)
     *
     * input: core_mass is in g
     * output: Rflat in cm
     */
    const double mc = core_mass / M_SUN;
    return
        6.0 * exp10(
            mc < 0.7
            ? (9.0*mc +5.4)
            : (0.7857*mc + 11.15)
            );

}

static double _Rhalt(const double core_mass)
{
    /*
     * Halting radius
     *
     * Politano (2021, A&A 648, L6, page 3 left side)
     */
    return _Rflat(core_mass)/6.0;
}

static Boolean _Politano2010_merge_criterion(struct stardata_t * const stardata Maybe_unused,
                                             struct common_envelope_state_t * const state,
                                             const double a_ej,
                                             const Boolean use_effective_cores)
{
    /*
     * Merging criterion of Politano (2021, A&A 648)
     * Eq. 3.
     *
     * Note: a_ej is in cm
     *
     * Return:
     * TRUE if the system merges,
     * FALSE if it remains detached.
     *
     * If use_effective_cores is TRUE, we use the effective
     * core radius and core Roche radius rather than the
     * total stellar equivalents. These may be more appropriate
     * in the case of two compact cores surrounded by a
     * common envelope.
     */
    const double Rhalt = _Rhalt(state->effective_core_mass_donor);

    /*
     * Roche radius of the secondary when the orbital
     * separation == Rhalt
     *
     * Note: we use the total mass of the donor here.
     *       Presumably we should really use the effective
     *       core masses of both?
     */
    const double R_LShalt =
        (
            use_effective_cores == FALSE ?
            Roche_radius3(state->mass_accretor,
                          state->mass_donor,
                          Rhalt) :
            Roche_radius3(state->effective_core_mass_accretor,
                          state->effective_core_mass_donor,
                          Rhalt)
            );

    const double Rs =
        use_effective_cores ? state->radius_accretor : state->effective_core_radius_accretor;

    const Boolean survive =
        Rs < R_LShalt &&
        R_LShalt < Rhalt &&
        Rhalt < a_ej;

    CEprint("POLITANO : Eq 3 : survive if Rs = %g < R_LShalt = %g < Rhalt = %g (log %g) < a_ej = %g : %s\n",
            Rs/R_SUN,
            R_LShalt/R_SUN,
            Rhalt/R_SUN,
            log10(Rhalt),
            a_ej/R_SUN,
            Yesno(survive));

    return !survive;
}



static double _ejecta_and_nucsyn(_CE_args_in,
#ifdef NUCSYN
                                 struct stardata_t * const input_stardata,
#endif
                                 struct common_envelope_state_t * const state_pre,
                                 struct common_envelope_state_t * const state_post
#ifdef NUCSYN
                                 ,Abundance ** Xej
#endif // NUCSYN
    )
{
    /*
     * compute nucleosynthetic yields from ejection of the common envelope
     * (but not any subsequent supernova)
     *
     * For merged systems:
     *
     * We do this by calling merge_stars with the (now known)
     * mass-loss fraction, which sets the abundance of
     * the ejected material.
     *
     * For detached systems:
     *
     * Just mix the envelopes.
     */
    const double mass_lost_donor = (state_pre->mass_donor - state_post->mass_donor)/M_SUN;
    const double mass_lost_accretor = (state_pre->mass_accretor - state_post->mass_accretor)/M_SUN;
    const double mass_lost = mass_lost_donor + mass_lost_accretor;

    const double mass_before = state_pre->mass_donor + state_pre->mass_accretor;
    const double mass_after = state_post->mass_donor + state_post->mass_accretor;
    const double mass_ejected = mass_before - mass_after;
#ifdef NUCSYN
    *Xej = New_isotope_array;
    if(state_post->coalesce == TRUE)
    {
        /*
         * Merged: compute Xej
         */
        const double mass_loss_fraction_was = stardata->preferences->merger_mass_loss_fraction;
        stardata->preferences->merger_mass_loss_fraction =
            1.0 - mass_after / mass_before;
        struct stardata_t * tmp_stardata = New_stardata_from(input_stardata);
        struct star_t * merged_star = merge_stars(tmp_stardata,
                                                  0,
                                                  FALSE,
                                                  *Xej);

        free_star(&merged_star);
        free_stardata(&tmp_stardata);
        stardata->preferences->merger_mass_loss_fraction = mass_loss_fraction_was;
    }
    else
    {
        /*
         * Detached
         */
        nucsyn_dilute_shell_to(mass_lost_donor,
                               donor->Xenv,
                               mass_lost_accretor,
                               accretor->Xenv,
                               *Xej);
    }
#endif // NUCSYN
    if(Is_not_zero(mass_ejected))
    {
        calc_yields(stardata,
                    donor,
                    mass_ejected/M_SUN,
#ifdef NUCSYN
                    *Xej,
#endif
                    0.0,
#ifdef NUCSYN
                    NULL,
#endif
                    donor->starnum,
                    FALSE,
                    SOURCE_COMENV);
    }
    return mass_lost;
}


static void _compute_stellar_age(struct stardata_t * const stardata,
                                 struct star_t * const star)
{
    /*
     * Given a star of particular stellar type and core mass,
     * compute its age.
     */

    /*
     * First, update existing timescales.
     * We may need these, e.g. if a CHeB.
     */
    Boolean alloc[3] = {FALSE};
    stellar_structure_BSE_alloc_arrays(star,
                                       NULL,
                                       alloc);
    stellar_timescales(stardata,
                       star,
                       star->bse,
                       star->phase_start_mass,
                       star->mass,
                       star->stellar_type);

    double mc = Outermost_core_mass(star);
    if(star->stellar_type == CHeB)
    {
        /*
         * giant_age requires the fractional CHeB age
         */
        star->age /= star->bse->tm;
        CEprint("set age of CHeB star %g with tm = %g\n",
                star->age,
                star->bse->tm);
    }

    CEprint("call giant_age with age=%g st=%d, mc=%g, TZO=%d\n",
            star->age,
            star->stellar_type,
            mc,
            star->TZO);
    giant_age(&mc,
              star->mass,
              &star->stellar_type,
              &star->phase_start_mass,
              &star->age,
              stardata,
              star);
    CEprint("after giant_age age=%g st=%d, mc=%g, TZO=%d\n",
            star->age,
            star->stellar_type,
            mc,
            star->TZO);
}

static void _return_angmom(_CE_args_in,
                           struct common_envelope_state_t * const state_pre,
                           struct common_envelope_state_t * const state_post)
{
    /*
     * Return angular momentum either from the input
     * state or the final state. This is completely artificial
     * and only for experimentation!
     */

    const double dJ =
        state_pre->orbital_angular_momentum *
        stardata->preferences->comenv_pre_angmom_return_fraction
        +
        state_post->orbital_angular_momentum *
        stardata->preferences->comenv_post_angmom_return_fraction;

    if(Is_not_zero(dJ))
    {
        double a_was = state_post->orbital_separation;


        state_post->orbital_angular_momentum =
            Max(0.0,
                dJ + state_post->orbital_angular_momentum);

        _Reset_orbit(state_post,
                     &state_post->orbital_angular_momentum,
                     &state_post->orbital_eccentricity);

        _solve_orbit(_CE_args,
                     state_post);

        CEprint("Artificial angular momentum, dJ = %g, changed the orbital separation from %g to %g Rsun\n",
               dJ,
               a_was/R_SUN,
               state_post->orbital_separation/R_SUN);
    }
}
