#!/bin/bash

# make data objects for common envelope evolution with binary_c

echo "make data objects for common envelope evolution with binary_c"

source ../make_data_objects_setup.sh

# large 3D polytrope table
HFILE=common_envelope_polytrope_3d.h
TMPFILE=common_envelope_polytrope_3d.dat
OBJFILE=common_envelope_polytrope_3d.o
echo "HFILE=$HFILE TMPFILE=$TMPFILE OBJFILE=$OBJFILE"
$CC ../../double2bin.c -o ./double2bin

if [ $HFILE -nt $OBJFILE ]  || [ !-z $BINARY_C_REBUILD_DATA_OBJECTS] ; then

    cat $HFILE | tr , ' ' | sed s/\\\\// | grep -v define |grep -v \* | ./double2bin > $TMPFILE && \
    OPTS="--rename-section .data=.rodata,alloc,load,readonly,data,contents " && \
    objcopy $OBJCOPY_OPTS $OPTS $TMPFILE $OBJFILE && \
    ls -l $TMPFILE $OBJFILE double2bin && \
    rm $TMPFILE &
fi

wait
rm ./double2bin
echo "DATAOBJECT $OBJFILE"
