#ifndef COMMON_ENVELOPE_PROTOTYPES_H
#define COMMON_ENVELOPE_PROTOTYPES_H
int common_envelope_evolution (
    struct star_t * const donor,
    struct star_t * const accretor,
    struct stardata_t * const stardata
    );
#ifdef BSE
int common_envelope_evolution_BSE (
    struct star_t * const donor,
    struct star_t * const accretor,
    struct stardata_t *const stardata,
    const int comenv_prescription
    );
#endif//BSE
#ifdef MINT
int common_envelope_evolution_MINT (
    struct star_t * const donor,
    struct star_t * const accretor,
    struct stardata_t *const stardata,
    const int comenv_prescription
    );
#endif//MINT

int common_envelope_evolution_binary_c (
    struct star_t * const donor,
    struct star_t * const accretor,
    struct stardata_t * const stardata,
    const int comenv_prescription
    );

double Pure_function common_envelope_dewi_tauris(const double convfrac,
                                                 const double lum,
                                                 const double m,
                                                 const double rzams,
                                                 const double rad,
                                                 const Stellar_type stellar_type,
                                                 struct stardata_t * Restrict const stardata);

#ifdef BSE
Constant_function double common_envelope_lambda(const double m,
                                                const double mc,
                                                const double lum,
                                                const double rad,
                                                const double rzams,
                                                const double rc,
                                                const double convfrac,
                                                const Stellar_type stellar_type,
#ifdef NUCSYN
                                                Abundance * const Xenv,
#endif
                                                struct stardata_t * Restrict const stardata);
#endif//BSE

double common_envelope_wang2016(
    const double m,
    const double rad,
    const Stellar_type stellar_type,
    struct stardata_t * Restrict const stardata
    );


double Pure_function common_envelope_Klencki2020(const double convfrac,
                                                 const double m,
                                                 const double lum,
                                                 const double rad,
                                                 const double rzams,
                                                 const Stellar_type stellar_type Maybe_unused,
                                                 struct stardata_t * Restrict const stardata);

double Constant_function calc_alpha_kin_Nandez2016(double m1,
                                                   double m2,
                                                   double mc1);

double Constant_function calc_alpha_unbound_Nandez2016(double m1,
                                                       double m2,
                                                       double mc1);

double Constant_function calc_alpha_lambda_Nandez2016(double m1,
                                                      double m2,
                                                      double mc1);



double common_envelope_polytrope(
    double m,
    double mc,
    double r,
    double rc,
#ifdef NUCSYN
    Abundance *X,
#endif
    struct stardata_t * stardata
    );
void cetest(void);


void add_envelope_to_remnant(struct stardata_t * const stardata,
                             struct star_t * const donor,
                             const double envelope_mass,
                             const double Rtarget,
                             const Stellar_type stellar_typein);

void mixdebug_fprintf(struct stardata_t * const stardata,
                      const char * const filename ,
                      const int fileline,
                      const char * const format,
                      ...) Gnu_format_args(4,5);

double Pure_function nelemans_separation(const double ai,
                                         const double Mi,
                                         const double mi,
                                         const double Mf,
                                         const double mf,
                                         const double gamma,
                                         struct stardata_t * Restrict const stardata);
double Pure_function nelemans_j_factor(const double Mi,
                         const double mi,
                         const double Mf,
                         const double mf,
                         const double gamma,
                         struct stardata_t * Restrict const stardata);
void set_ntp(double *ntp,
             double *ntp_since_mcmin,
             double *prev_tagb,
             struct stardata_t * stardata);

#ifdef NUCSYN
void init_comenv_nucsyn(struct stardata_t * stardata);
void AGB_merger_nucsyn(Stellar_type stellar_type,
                       double ntp,
                       double ntp_since_mcmin,
                       double prev_tagb,
                       struct star_t * star1,
                       struct stardata_t * stardata );

#endif//NUCSYN

void calc_effective_core_masses(double * const effective_mc1,
                                double * const effective_mc2,
                                const Stellar_type stellar_type1,
                                const Stellar_type stellar_type2,
                                const double m1,
                                const double mc1,
                                const double m2,
                                const double mc2);

#if defined COMENV_ACCRETION

void comenv_accretion(struct stardata_t * stardata,
                      const Star_number nstar1,
                      const Star_number nstar2,
                      double *GB,
                      double *tscls1,
                      double *tscls2,
                      double *lums,
                      double *m1,
                      double *m2,
                      double *m01,
                      double *m02,
                      double *age_donor,
                      double *age_accretor,
                      double *l1,
                      double *l2,
                      double *r1,
                      double *r2,
                      double *menv,
                      double *renv,
                      double *tm1,
                      double *tm2,
                      double *mc1,
                      double *mc2,
                      double *rc1,
                      double *rc2,
                      double *k21,
                      double *k22,
                      double *tn,
                      double common_envelope_mass,
                      Stellar_type *stellar_type1,
                      Stellar_type *stellar_type2
    );
#endif // COMENV_ACCRETION

Event_handler_function common_envelope_event_handler(void * eventp,
                                                     struct stardata_t * stardata,
                                                     void * data
    );

#endif// COMMON_ENVELOPE_PROTOTYPES_H
