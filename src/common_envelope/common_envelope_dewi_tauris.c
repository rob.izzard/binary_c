#include "../binary_c.h"
No_empty_translation_unit_warning;



double Pure_function common_envelope_dewi_tauris(const double convfrac,
                                                 const double lum,
                                                 const double m,
                                                 const double rzams,
                                                 const double rad,
                                                 const Stellar_type stellar_type,
                                                 struct stardata_t * Restrict const stardata)
{
    double result;

    /* lambda (structure) */
    double lambda_structure=0.0;
    /* lambda (ionisation energy contribution) */
    double lambda_ion=0.0;

    double fac_ion = stardata->preferences->lambda_ionisation[stardata->model.comenv_count];

    /*
     * Fits to Dewi & Tauris' lambda
     *
     * NB No fits yet for naked He stars...
     */
    if(More_or_equal(fac_ion,0.0))
    {
        if(stellar_type>TPAGB)
        {
            /* post AGB stars : assume lambda = 0.5 */
            result = 0.5;

            Dprint("Post AGB : use lambda=0.5\n");
        }
        else
        {
            Dprint("convfrac=%g\n",convfrac);

            if(convfrac>0.0)
            {
                /*
                 * Formulae for giant-like stars i.e. those
                 * with some convective envelope;
                 * also used for HG and CHeB stars close
                 * to the Hayashi track.
                 */
                double logl = log10(lum);
                double logm = log10(m);

                if(stellar_type<TPAGB)
                {
                    /* pre-TPAGB stars */
                    double m1 = m;
                    if(stellar_type>GIANT_BRANCH) m1 = 100.0;
                    lambda_structure = Min(3.0/(2.40 + 1.0*pow(m1,-1.5)) - 0.150*logl,0.8);
                }
                else
                {
                    /* TPAGB (and post TPAGB) */
                    lambda_structure = -3.50 - 0.750*logm + logl;
                }

                if(stellar_type>GIANT_BRANCH)
                {
                    double tmp;
                    tmp = Min(0.90,0.580 + 0.750*logm) - 0.080*logl;
                    if(stellar_type<TPAGB)
                    {
                        lambda_structure = Min(tmp,lambda_structure);
                    }
                    else
                    {
                        lambda_structure = Min(1.0,Max(tmp,lambda_structure));
                    }
                }
                lambda_structure *= 2.0;

                Dprint("Lambda (pre ion correction) = %g\n",lambda_structure);

#ifdef LOG_CE_PARAMETERS
                lambda_pre_ion=lambda_structure;
#endif

                if(fac_ion>0.0)
                {
                    /*
                     * Use a fraction FAC_ION of the ionization energy in the energy balance.
                     */
                    double aa,bb,cc;
                    if(stellar_type<=GIANT_BRANCH)
                    {
                        aa = Min((1.20*Pow2(logm - 0.250) - 0.70),-0.50);
                    }
                    else
                    {
                        aa = Max(-0.20 - logm,-0.50);
                    }
                    bb = Max(3.0 - 5.0*logm,1.50);
                    cc = Max(3.70 + 1.60*logm,3.30 + 2.10*logm);

                    /* 1/(lambda ion) fit */
                    lambda_ion = aa + atan(bb*(cc - logl));

                    if(stellar_type<=GIANT_BRANCH)
                    {
                        double dd = Max(0.0,Min(0.150,0.150 - 0.250*logm));
                        lambda_ion += dd*(logl - 2.0);
                    }

                    /* limit to sensible values and do the proper calculation */
                    lambda_ion = Max(lambda_ion,1E-2);
                    lambda_ion = Max(1.0/lambda_ion,lambda_structure);

                    if(More_or_equal(fac_ion,1.0))
                    {
                        /* why should fac_ion be > 1 ? */
                        lambda_structure = lambda_ion;
                    }
                    else
                    {
                        /* why lambda_ion - lambda_structure ? */
                        lambda_structure += fac_ion*(lambda_ion - lambda_structure);
                    }
                    Dprint("fac_ion ion > 0 : lambda_structure=%g lambda_ion=%g\n",lambda_structure,lambda_ion);
                }
            }

            /*
             * Stars in which the envelope is not entirely
             * convective e.g. HG stars
             */
            if(convfrac<1.0-TINY)
            {
                /* Formula for HG stars; also reasonable for CHeB stars in blue loop.
                 */
                lambda_ion = 0.420*pow((rzams/rad),0.40);
                /* Alternatively:
                 *           lambda_ion = 0.30*(TAMS_radius/rad)**0.40
                 */
                lambda_ion *= 2.0;
                Dprint("Use lambda_ion for small envelope = %g\n",lambda_ion);
            }

            if(Less_or_equal(convfrac,0.0))
            {
                /* zero, perhaps, but < 0 ???? */
                result = lambda_ion;
            }
            else if(More_or_equal(convfrac,1.0))
            {
                /* this is the case for most stars e.g. TPAGB */
                result = lambda_structure;
            }
            else
            {
                /*
                 * Interpolate between HG and GB values depending on conv. envelope mass.
                 */
                result = lambda_ion + sqrt(convfrac)*(lambda_structure - lambda_ion);
            }
            Dprint("Final lambda=%g\n",result);
        }
    }
    else
    {
        /* fixed value based on the parameter passed in */
        result = -1.0*fac_ion;
    }
    return result;
}
