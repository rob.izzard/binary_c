#include "../binary_c.h"
No_empty_translation_unit_warning;


double common_envelope_wang2016(
    const double m Maybe_unused,
    const double rad Maybe_unused,
    const Stellar_type stellar_type Maybe_unused,
    struct stardata_t * Restrict const stardata
    )
{
    double result;
#ifdef COMENV_WANG2016

    if(stellar_type==HeHG || stellar_type==HeGB)
    {
        /*
         * Assume lambda = 0.5 for helium giants
         */
        result = 0.5;
    }
    else
    {
        /*
         * lambda_CE calculated according to the table
         * of Wang, Jia and Li 2016 (RAA 2016 Vol 16, Number 8)
         *
         * We use only their "wind 1" case, as this has the
         * more complete coverage.
         *
         * We also have to guess the "stage" as it is not
         * quantiatively defined in their paper. They say:
         *
         * "Stage 1 begins at the exhaustion of central hydrogen and
         * ends when the star starts to shrink (i.e., near the ignition
         * of central He). Stage 2 is the following shrinking phase,
         * and in Stage 3 the star expands again, until the end of the
         * evolution."
         *
         * Thus, I approximate this as
         * Stage 1: MS, HG, GB, HeMS
         * Stage 2: CHeB
         * Stage 3: EAGB, TPAGB, HeHG, HeGB
         *
         * NB These tables must all be of identical size otherwise
         * they require separate table id numbers. In the latest binary_c,
         * this is irrelevant.
         *
         * The final lambda is constructed from Eq. 5 and a combination of
         *
         * lambdag : integral (GM(r)/r) dm (from Eq.2)
         * lambdab : as lambdag including internal energy U (cf. Dewi/Tauris, from Eq. 3)
         * lambdah : as lambdab including P/rho i.e. the enthalpy (Eq. 4)
         *
         * The combinations of internal energy and enthalpy are controlled by the
         * lambda_ionisation and lambda_enthalpy parameters, respectively.
         *
         */
        double *tableg, *tableb, *tableh;
        if(stellar_type<=GIANT_BRANCH || stellar_type==HeMS)
        {
            tableg = stardata->store->tableg1;
            tableb = stardata->store->tableb1;
            tableh = stardata->store->tableh1;
        }
        else if(stellar_type==CHeB)
        {
            tableg = stardata->store->tableg2;
            tableb = stardata->store->tableb2;
            tableh = stardata->store->tableh2;
        }
        else
        {
            tableg = stardata->store->tableg3;
            tableb = stardata->store->tableb3;
            tableh = stardata->store->tableh3;
        }

        double p[1]; /* parameter array for interpolation */
        double loglambda = 0.0;
        if(stardata->persistent_data->comenv_lambda_data == NULL)
        {
            stardata->persistent_data->comenv_lambda_data =
                Malloc(sizeof(double)*18*2);
            NewDataTable_from_Pointer(
                stardata->persistent_data->comenv_lambda_data,
                stardata->persistent_data->comenv_lambda_table,
                1,
                1,
                18
                );
        }
        Dprint("tableg: \n");
        {
            int j;
            for(j=0;j<18;j++)
            {
                Dprint("%d %g %g %g %g\n",
                       j,
                       *(tableg + 8*j + 0),
                       *(tableg + 8*j + 1),
                       *(tableg + 8*j + 2),
                       *(tableg + 8*j + 3));
            }
        }

        /*
         * Make a table of lambda given this radius,
         * then interpolate for the current mass, giving
         * the lambda h, b and g.
         */
#define WIDTH 8
#define LINES 18
#define Construct_lambda(LABEL,LAMBDA,TABLE)                            \
        /* construct interpolation table of loglambda(mass) */          \
        {                                                               \
            int j;                                                      \
            double *tt = stardata->persistent_data->comenv_lambda_table->data; \
            double a[1];                                                \
            for(j=0;j<LINES;j++)                                        \
            {                                                           \
                int i;                                                  \
                double *tj = (TABLE)+j*WIDTH;                           \
                p[0] = Limit_range(*tj,1.0,60.0);                       \
                                                                        \
                /* get max radius for this mass */                      \
                Interpolate(stardata->store->comenv_maxR_table,         \
                            p,a,FALSE);                                 \
                                                                        \
                /* hence radius for the formula */                      \
                double r = Max(0.5,Min(rad,a[0]));                      \
                double x = r;                                           \
                Dprint("%d r=%g (rmax=%g) loglambda%s= ",               \
                       j,r,a[0],(LABEL));                               \
                                                                        \
                /* get mass */                                          \
                *tt++ = *tj++;                                          \
                /* construct loglambda */                               \
                Dprint(" + %g ",*tj);                                   \
                loglambda = *tj++;                                      \
                for(i=2; i<WIDTH-1; i++)                                \
                {                                                       \
                    Dprint(" + %g * %g ",*tj,x);                        \
                    loglambda += *tj++ * x;                             \
                    x *= r;                                             \
                }                                                       \
                Dprint(" + %g * %g ",*tj,x);                            \
                loglambda += *tj * x;                                   \
                *tt++ = loglambda;                                      \
                Dprint("loglambda%s=%g\n",(LABEL),*(tt-1));             \
            }                                                           \
                                                                        \
                                                                        \
            /* interpolate in mass */                                   \
            p[0]=Limit_range(m,1.0,60.0);                               \
            Interpolate(stardata->persistent_data->comenv_lambda_table, \
                        p,a,FALSE);                                     \
                                                                        \
            loglambda = a[0];                                           \
        }                                                               \
        (LAMBDA) = Saferpow(10.0,loglambda);                            \
        Dprint("LAM %s for stellar_type=%d m=%g loglambda%s = %g lambda%s = %g\n", \
               (LABEL),stellar_type,p[0],                               \
               (LABEL),loglambda,                                       \
               (LABEL),(LAMBDA));

        double lambdag,lambdab,lambdah;
        Construct_lambda("h",lambdah, tableh);
        Construct_lambda("b",lambdab, tableb);
        Construct_lambda("g",lambdag, tableg);

        /*
         * Use the above to construct lambda
         */
        result =
            lambdag
            + stardata->preferences->lambda_ionisation[stardata->model.comenv_count] * (lambdab - lambdag)
            + stardata->preferences->lambda_enthalpy[stardata->model.comenv_count] * (lambdah - lambdab);
        Dprint("lambda = (g)=%g + %g * (b-g)=%g + %g * (h-b)=%g = %g\n",
               lambdag,
               stardata->preferences->lambda_ionisation[stardata->model.comenv_count],
               lambdab - lambdag,
               stardata->preferences->lambda_enthalpy[stardata->model.comenv_count],
               lambdah - lambdab,
               result);
    }

#else
    Exit_binary_c(BINARY_C_SETUP_UNKNOWN_ARGUMENT,
                  "Tried to use Wang 2016 lambda_ce when it is not compiled in. Enable LAMBDA_CE_WANG_2016 and rebuild.\n");
#endif //LAMBDA_CE_WANG_2016


    return result;
}
