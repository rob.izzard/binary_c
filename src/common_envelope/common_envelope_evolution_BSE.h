#pragma once
#ifndef COMMON_ENVELOPE_EVOLUTION_BSE_H
#define COMMON_ENVELOPE_EVOLUTION_BSE_H


#if (defined(MIXDEBUG) && (!defined(NUCSYN)))
#undef MIXDEBUG
#endif

#ifndef MIXDEBUG
#define MIXDEBUG 0
#else
#undef MIXDEBUG
#define MIXDEBUG 1
#endif


#define DENY_EVENTS TRUE
#define ALLOW_EVENTS FALSE

/*
 * Use logarithms when finding an envelope mass?
 */
#define BISECTOR_COMENV_DM_USELOG FALSE


/*
 * conditions for using Nelemans common envelope prescription:
 * 1) companion is unevolved (MS)
 * 2) q=M2/M1>0.2
 * 3) This is the nth common envelope phase where n < preferences->nelemans_n_comenvs
 */
/* relax the MS condition */
#define USE_NELEMANS                                                    \
    (                                                                   \
        stardata->model.comenv_count<=stardata->preferences->nelemans_n_comenvs \
        &&                                                              \
        m1init/m2init >stardata->preferences->nelemans_minq             \
        )

#define UNBOUND (stardata->common.orbit.eccentricity > 1.0)
#define RETURN_IF_UNBOUND if(UNBOUND)    goto end_section;

#define Mprint(...) if(MIXDEBUG)                                    \
        mixdebug_fprintf(stardata,__FILE__,__LINE__,__VA_ARGS__);

#if defined MIXDEBUG && defined NUCSYN

#define Total_carbon_abundance(A) (A[XC12]/12.0+A[XC13]/13.0)
#define Total_oxygen_abundance(A) (A[XO16]/16.0+A[XO17]/17.0+A[XO18]/18.0)
#define Carbon_oxygen_ratio(A) (Is_not_zero(Total_oxygen_abundance(A)) ? (Total_carbon_abundance(A)/Total_oxygen_abundance(A)) : 0.0)
#define _Abundance_list3(A) A[XH1],A[XHe4],A[XC12]
#define _Abundance_list5(A) A[XH1],A[XHe4],A[XC12],A[XN14],A[XO16]
#define _Abundance_list5Fe(A) A[XH1],A[XHe4],A[XC12],A[XO16],A[XFe56]
#define _Abundance_list6(A) A[XH1],A[XHe4],A[XC12],A[XC13],A[XN14],A[XO16]
#define _Abundance_list7(A) _Abundance_list6(A),Carbon_oxygen_ratio(A)
#define _Abundance_list8(A) _Abundance_list7(A),nucsyn_totalX(A)


#define Mix_debug_in_out(A) {                                           \
        Mprint("Comenv %s - total mass %g (+yields %g)\n",              \
               A,                                                       \
               donor->mass+accretor->mass,msum);                        \
        Mprint("Star 1 m=%g (m0=%g) mc=%g menv=%g type=%d R=%g Rc=%g\n", \
               donor->mass,                                             \
               donor->phase_start_mass,                                 \
               Outermost_core_mass(donor),                              \
               envelope_mass(donor),                                    \
               donor->stellar_type,                                     \
               r1,                                                      \
               rc1);                                                    \
        Mprint("XH1=%12.12e XHe4=%g XC12=%g XC13=%g XN14=%g XO16=%g (C/O=%g) Xtot=%g\n", \
               _Abundance_list8(stardata->star[donor->starnum].Xenv));  \
        Mprint("Star 2 m=%g (m0=%g) mc=%g menv=%g type=%d R=%g Rc=%g\n", \
               accretor->mass,                                          \
               accretor->phase_start_mass,                              \
               Outermost_core_mass(accretor),                           \
               envelope_mass(accretor),                                 \
               accretor->stellar_type,                                  \
               r2,                                                      \
               rc2);                                                    \
        Mprint("XH1=%g XHe4=%g XC12=%g XC13=%g XN14=%g XO16=%g (C/O=%g) Xtot=%g\n", \
               _Abundance_list8(stardata->star[donor->starnum].Xenv));  \
        fflush(stdout);                                                 \
    }


#else //MIXDEBUG

#define Mix_debug_in_out(A) /**/


#endif //MIXDEBUG




#define ACCRETE_ARGS                            \
    stardata,                                   \
        donor->starnum,                         \
        accretor->starnum,                      \
        bse1,                                   \
        bse2,                                   \
        &donor->mass,                           \
        &accretor->mass,                        \
        &donor->phase_start_mass,               \
        &accretor->phase_start_mass,            \
        &donor->age,                            \
        &accretor->age,                         \
        &l1,                                    \
        &l2,                                    \
        &r1,                                    \
        &r2,                                    \
        &menv,                                  \
        &renv,                                  \
        &tm1,                                   \
        &tm2,                                   \
        &Outermost_core_mass(donor),            \
        &Outermost_core_mass(accretor),         \
        &rc1,                                   \
        &rc2,                                   \
        &moment_of_inertia_factor1,             \
        &moment_of_inertia_factor2,             \
        &tn,                                    \
        common_envelope_mass,                   \
        &donor->stellar_type,                   \
        &accretor->stellar_type




#define MDEBUG_PRETTY_STARS_IN                                          \
    Mprint("Trying to dilute to comenv: H1_1=%g * %g H1_2=%g * %g -> H1_env=%g\n", \
           m1init-effective_mc1,stardata->star[donor->starnum].Xenv[XH1], \
           m2init-effective_mc2,stardata->star[accretor->starnum].Xenv[XH1], \
           common_envelope_abundance[XH1]);                             \
    Mprint("Effective cores:\n");                                       \
    Mprint("Star 1 m=%g (m0=%g) mc'=%g menv=%g type=%d\n",              \
           donor->mass,                                                 \
           donor->phase_start_mass,                                     \
           effective_mc1,                                               \
           donor->mass-effective_mc1,                                   \
           donor->stellar_type);                                        \
    Mprint("Star 2 m=%g (m0=%g) mc'=%g menv=%g type=%d\n",              \
           accretor->mass,                                              \
           accretor->phase_start_mass,                                  \
           effective_mc2,                                               \
           accretor->mass-effective_mc2,                                \
           accretor->stellar_type);                                     \
    Mprint("total mass at this stage %g\n",donor->mass+accretor->mass); \
    Mprint("/---------------------------\\\n");                         \
    Mprint("|         %3.3e         |\n",common_envelope_mass);         \
    Mprint("|                           |\n");                          \
    Mprint("|                    <-     |\n");                          \
    Mprint("|                     |     |\n");                          \
    Mprint("|  /-------\\     /-------\\  |\n");                        \
    Mprint("| /%3.3e\\   /%3.3e\\ |\n",effective_mc1,effective_mc2);    \
    Mprint("| \\  %4d   /   \\  %4d   / |\n",donor->stellar_type,accretor->stellar_type); \
    Mprint("|  \\-------/     \\-------/  |\n");                        \
    Mprint("|      |                    |\n");                          \
    Mprint("|      ->                   |\n");                          \
    Mprint("|                           |\n");                          \
    Mprint("\\---------------------------/\n");                         \
                                                                        \
    Mprint("Common envelope mass %g (mass in effective cores = %g, total = %g)\n",common_envelope_mass, \
           effective_mc1+effective_mc2,                                 \
           effective_mc1+effective_mc2+common_envelope_mass);           \
    Mprint("Common envelope abundances : XH1=%12.12e XHe4=%g XC12=%g XC13=%g XN14=%g XO16=%g C/O=%g Total=%g\n", \
           _Abundance_list8(common_envelope_abundance));

#define MDEBUG_PRETTY_MERGER                                            \
    Mprint("/ - - - - - - - - - - - - - - - \\\n");                     \
    Mprint("| To ISM : %3.3e            |\n",dm);                       \
    Mprint("  /---------------------------\\\n");                       \
    Mprint("| |Envelope %3.3e (C%2.2f) | |\n",envelope_mass(donor),dM2/(envelope_mass(donor)+TINY)); \
    Mprint("  |Type     %4d              |\n",merged_stellar_type);     \
    Mprint("| |       /---------\\         | |\n");                     \
    Mprint("  |       |Core     |         |\n");                        \
    Mprint("| |       |%3.3e|         | |\n",Outermost_core_mass(donor)); \
    Mprint("  |       \\---------/         |\n");                       \
    Mprint("| |                           | |\n");                      \
    Mprint("  \\---------------------------/\n");                       \
    Mprint("\\ - - - - - - - - - - - - - - - /\n");                     \
    Mprint("New envelope for merged cores: %12.12g from common envelope, %12.12g from stars\n", \
           dM1,dM2-dM1);

#define MDEBUG_PRETTY_NOT_MERGER                                        \
    Mprint(" - - - - - - - - - - - - - -  \n");                         \
    Mprint("| TO ISM: %3.3e         |\n",common_envelope_mass);         \
    Mprint("                             \n");                          \
    Mprint("|                    <-     |\n");                          \
    Mprint("                      |      \n");                          \
    Mprint("|  /-------\\     /-------\\  |\n");                        \
    Mprint("  /%3.3e\\   /%3.3e\\  \n",donor->mass,accretor->mass);     \
    Mprint("  |%3.3e|   |%3.3e|  \n",effective_mc1,effective_mc2);      \
    Mprint("| \\  %4d   /   \\  %4d   / |\n",donor->stellar_type,accretor->stellar_type); \
    Mprint("   \\-------/     \\-------/   \n");                        \
    Mprint("|      |                    |\n");                          \
    Mprint("       ->                    \n");                          \
    Mprint("|                           |\n");                          \
    Mprint(" - - - - - - - - - - - - - - \n");



#if defined MIXDEBUG && defined NUCSYN

#define TOTAL_YIELD (stardata->star[0].Xyield[i]+   \
                     stardata->star[1].Xyield[i])

#define MDEBUG_CALC_MSUM msum=0.0;                          \
    Isotope_loop(i)                                         \
    {                                                       \
        msum+=TOTAL_YIELD;                                  \
    }                                                       \
    msum+=stardata->star[0].mass+stardata->star[1].mass;
#else
#define MDEBUG_CALC_MSUM /**/
#endif

/*
 * Note that this is called twice but with different radii
 * for the secondary : in the case of a main sequence star, R2=r2
 * but for stars with cores R2=rc2
 */

#define CORE_RLOF(R2) (More_or_equal(rc1/rl1,(R2)/rl2))

#define CHECK_FOR_CORE_RLOF(R2)                                         \
    stardata->model.coalesce = FALSE;                                   \
    separation_at_core_RLOF = 0.0;                                      \
    if(CORE_RLOF(R2))                                                   \
    {                                                                   \
        if(rc1 > rl1 || Is_zero(final_separation))                      \
        {                                                               \
            stardata->model.coalesce = 1;                               \
            separation_at_core_RLOF = Is_zero(final_separation) ? rc1 : (final_separation * rc1/rl1); \
            Dprint("SETSEPL1 %g : sepF = %g rc1 = %g rl1 = %g\n",separation_at_core_RLOF,final_separation,rc1,rl1); \
            CEprint("Donor core overflows : R = RL = %g, aL = %g\n",    \
                    rl1,                                                \
                    separation_at_core_RLOF);                           \
        }                                                               \
    }                                                                   \
    else if( (R2) > rl2 || Is_zero(final_separation))                   \
    {                                                                   \
        stardata->model.coalesce = 2;                                   \
        separation_at_core_RLOF = Is_zero(final_separation) ? (R2) : (final_separation * (R2)/rl2); \
        Dprint("SETSEPL2 %g\n",separation_at_core_RLOF);                \
        CEprint("Accretor core overflows\n");                           \
    }                                                                   \
    Dprint("Merger? %u\n",stardata->model.coalesce);                    \
                                                                        \
    if(stardata->model.coalesce>0)                                      \
    {                                                                   \
        Append_logstring(LOG_COMENV_PRE,                                \
                         "Merge%u [rc1=%g%crl1=%g or R2=%g%crl2=%g] ",  \
                         stardata->model.coalesce,                      \
                         rc1,                                           \
                         (stardata->model.coalesce==1 ? '>' : '<'),     \
                         rl1,                                           \
                         R2,                                            \
                         (stardata->model.coalesce==2 ? '>' : '<'),     \
                         rl2                                            \
            );                                                          \
        stardata->model.coalesce = TRUE;                                \
        Dprint("cores have merged\n");                                  \
    }                                                                   \
    Append_logstring(LOG_COMENV_PRE,                                    \
                     "sepF=%g 1:Rc=%g~RL=%g 2:Rc=%g~RL=%g ",            \
                     final_separation,rc1,rl1,r2,rl2);


#define AGE_HELIUM_STAR_OR_CORE                                 \
    if(donor->stellar_type<=FIRST_GIANT_BRANCH)                 \
    {                                                           \
        fage1 = 0.0;                                            \
        if(accretor->stellar_type==COWD)                        \
        {                                                       \
            fage1 = accretor->mass/                             \
                (accretor->mass+Outermost_core_mass(donor));    \
        }                                                       \
    }                                                           \
    else if(donor->stellar_type>=TPAGB)                         \
    {                                                           \
        fage1 = 1.0;                                            \
    }                                                           \
    else                                                        \
    {                                                           \
        fage1 = (donor->age - bse1->timescales[2])/             \
            (bse1->timescales[13] - bse1->timescales[2]);       \
    }                                                           \
                                                                \
    if(accretor->stellar_type<=FIRST_GIANT_BRANCH ||            \
       accretor->stellar_type==HeWD)                            \
    {                                                           \
        fage2 = 0.0;                                            \
    }                                                           \
    else if(accretor->stellar_type==HeMS)                       \
    {                                                           \
        /* the TINY in the numerator and denominator */         \
        /* make sure that fage2 = 1 when both are zero */       \
        fage2 = (accretor->age+TINY)/(bse2->tm+TINY);           \
        mc22 = accretor->mass;                                  \
    }                                                           \
    else if(accretor->stellar_type>=TPAGB)                      \
    {                                                           \
        fage2 = 1.0;                                            \
    }                                                           \
    else                                                        \
    {                                                           \
        fage2 = (accretor->age - bse2->timescales[2])/          \
            (bse2->timescales[13] - bse2->timescales[2]);       \
    }



/*
 * Calc-lum-and-evol-time and stellar_structure calls in macro form
 */


#define TIMESCALESmerge                                     \
    stellar_timescales(stardata,                            \
                       &(stardata->star[donor->starnum]),   \
                       bse1,                                \
                       donor->phase_start_mass,             \
                       donor->mass,                         \
                       merged_stellar_type);

#define TIMESCALESmerge_t2                                  \
    stellar_timescales(stardata,                            \
                       &(stardata->star[donor->starnum]),   \
                       bse2,                                \
                       donor->mass,                         \
                       donor->mass,                         \
                       merged_stellar_type);

#define TIMESCALES1                                         \
    stellar_timescales(stardata,                            \
                       &(stardata->star[donor->starnum]),   \
                       bse1,                                \
                       donor->phase_start_mass,             \
                       donor->mass,                         \
                       donor->stellar_type);



#define STELLAR_STRUCTUREnewtype                                        \
    {                                                                   \
        Boolean alloc[3];                                               \
        stellar_structure_BSE_alloc_arrays(donor,                       \
                                           donor->bse,                  \
                                           alloc);                      \
        donor->bse->timescales[0] = FORCE_TIMESCALES_CALCULATION;       \
        stellar_structure(stardata,                                     \
                          STELLAR_STRUCTURE_CALLER_common_envelope_evolution, \
                          donor,                                        \
                          NULL);                                        \
        bse1->tm = donor->tm;                                           \
        bse1->tn = donor->tn;                                           \
        r1 = donor->radius;                                             \
        rc1 = donor->core_radius;                                       \
        menv = donor->menv;                                             \
        moment_of_inertia_factor1 = donor->moment_of_inertia_factor;    \
        memcpy(bse1->timescales,                                        \
               donor->bse->timescales,                                  \
               sizeof(double)*TSCLS_ARRAY_SIZE);                        \
        stellar_structure_BSE_free_arrays(donor,alloc);                 \
    }





/*
 * Debugging output for star 1
 */

#define PRECALL1 printf("Precall &donor->age=%g m01=%g m1=%15.10e mc1=%15.10e (m1-mc1=%g) ntp=%g :: moment_of_inertia_factor1=%g l1=%g menv=%g r1=%15.10e rc1=%g renv=%g tm1=%g tn=%g st=%d\n", \
                        donor->age,                                     \
                        donor->phase_start_mass,                        \
                        donor->mass,                                    \
                        Outermost_core_mass(donor),                     \
                        envelope_mass(donor),                           \
                        stardata->star[donor->starnum].num_thermal_pulses, \
                        moment_of_inertia_factor1,                      \
                        l1,                                             \
                        menv,                                           \
                        r1,                                             \
                        rc1,                                            \
                        renv,                                           \
                        tm1,                                            \
                        tn,                                             \
                        donor->stellar_type                             \
        );


#define POSTCALL1 printf("Postcall &donor->age=%g m01=%g m1=%15.10e mc1=%15.10e (m1-mc1=%g) ntp=%g :: moment_of_inertia_factor1=%g l1=%g menv=%g r1=%15.10e rc1=%g renv=%g tm1=%g tn=%g st=%d\n", \
                         donor->age,                                    \
                         donor->phase_start_mass,                       \
                         donor->mass,                                   \
                         Outermost_core_mass(donor),                    \
                         envelope_mass(donor),                          \
                         stardata->star[donor->starnum].num_thermal_pulses, \
                         moment_of_inertia_factor1,                     \
                         l1,                                            \
                         menv,                                          \
                         r1,                                            \
                         rc1,                                           \
                         renv,                                          \
                         tm1,                                           \
                         tn,                                            \
                         donor->stellar_type                            \
        );


#define STRUCTURE1(DENY)                                                \
    {                                                                   \
        Boolean deny_was = stardata->model.deny_new_events;             \
        Set_event_denial(DENY);                                         \
        Dprint("1pre = { stellar_type=%d, m01=%g m1=%g r1=%g J1=%g }\n", \
               donor->stellar_type,                                     \
               donor->phase_start_mass,                                 \
               donor->mass,                                             \
               r1,                                                      \
               donor->angular_momentum);                                \
        Boolean alloc[3];                                               \
        stellar_structure_BSE_alloc_arrays(donor,                       \
                                           donor->bse,                  \
                                           alloc);                      \
        donor->bse->timescales[0] = FORCE_TIMESCALES_CALCULATION;       \
        stellar_structure(stardata,                                     \
                          STELLAR_STRUCTURE_CALLER_common_envelope_evolution, \
                          donor,                                        \
                          NULL);                                        \
        Set_event_denial(deny_was);                                     \
        bse1->tm = donor->tm;                                           \
        bse1->tn = donor->tn;                                           \
        r1 = donor->radius;                                             \
        rc1 = donor->core_radius;                                       \
        menv = donor->menv;                                             \
        /*renv = donor->renv;*/                                         \
        moment_of_inertia_factor1 = donor->moment_of_inertia_factor;    \
        memcpy(bse1->timescales,                                        \
               donor->bse->timescales,                                  \
               sizeof(double)*TSCLS_ARRAY_SIZE);                        \
        stellar_structure_BSE_free_arrays(donor,alloc);                 \
    }


#define STRUCTURE2(DENY)                                                \
    {                                                                   \
        /*stellar_type = accretor->stellar_type;*/                      \
        Boolean deny_was = stardata->model.deny_new_events;             \
        Set_event_denial(DENY);                                         \
        Dprint("2pre = { stellar_type=%d, m02=%g m2=%g r2=%g J2=%g } deny %d\n", \
               accretor->stellar_type,                                  \
               accretor->phase_start_mass,                              \
               accretor->mass,                                          \
               r2,                                                      \
               accretor->angular_momentum,                              \
               DENY);                                                   \
        Boolean alloc[3];                                               \
        stellar_structure_BSE_alloc_arrays(accretor,                    \
                                           accretor->bse,               \
                                           alloc);                      \
        accretor->bse->timescales[0] = FORCE_TIMESCALES_CALCULATION;    \
        Dprint("call comenv BSE 2 structure\n");                        \
        stellar_structure(stardata,                                     \
                          STELLAR_STRUCTURE_CALLER_common_envelope_evolution, \
                          accretor,                                     \
                          NULL);                                        \
        Dprint("done comenv BSE 2 structure\n");                        \
        Set_event_denial(deny_was);                                     \
        bse2->tm = accretor->bse->tm;                                   \
        bse2->tn = accretor->bse->tn;                                   \
        r2 = accretor->radius;                                          \
        rc2 = accretor->core_radius;                                    \
        menv = accretor->menv;                                          \
        moment_of_inertia_factor2 = accretor->moment_of_inertia_factor; \
        memcpy(bse2->timescales,                                        \
               accretor->bse->timescales,                               \
               sizeof(double) * TSCLS_ARRAY_SIZE);                      \
        stellar_structure_BSE_free_arrays(accretor,                     \
                                          alloc);                       \
    }




double stellar_radius(const Stellar_type stellar_type,
                      const double mass,
                      const double mc,
                      const double lum,
                      const double age,
                      const double z,
                      struct star_t * star,
                      struct stardata_t * stardata
    );

double dmdr(struct star_t * star,
            struct stardata_t * stardata);


#endif // COMMON_ENVELOPE_EVOLUTION_BSE_H
