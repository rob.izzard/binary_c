#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "common_envelope_evolution.h"

double Constant_function calc_alpha_lambda_Nandez2016(double m1,
                                                      double m2,
                                                      double mc1)
{
    /*
     * alpha * lambda from Nandez and Ivanova (2016)
     * Eq. 15
     */
    Clamp(m1,1.177,1.799);
    Clamp(m2,0.32,0.40);
    Clamp(mc1,0.318,0.364);
    return 0.92 + 0.55 * m1 - 0.79 * m2 - 1.19 * mc1;
}

double Constant_function calc_alpha_kin_Nandez2016(double m1,
                                                   double m2,
                                                   double mc1)
{
    /*
     * alpha_kin = -Ekin(unbound) / Eorb
     * from Nandez and Ivanova (2016) Eq. 17
     */
    Clamp(m1,1.177,1.799);
    Clamp(m2,0.32,0.40);
    Clamp(mc1,0.318,0.364);
    return 0.20 - 0.26 * m1 + 0.44 * m2 + 0.92 * mc1; 
}

double Constant_function calc_alpha_unbound_Nandez2016(double m1,
                                                       double m2,
                                                       double mc1)
{
    /*
     * alpha_unbound = -Etot(unbound) / Eorb
     * from Nandez and Ivanova (2016) Eq. 16
     */
    Clamp(m1,1.177,1.799);
    Clamp(m2,0.32,0.40);
    Clamp(mc1,0.318,0.364);
    return -0.16 - 0.30 * m1 + 0.49 * m2 + 2.27 * mc1; 
}
