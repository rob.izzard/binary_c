/*
 * A modified form of BSE's Common Envelope Evolution.
 *
 *     Author : C. A. Tout
 *     Date :   18th September 1996
 *
 *     Redone : J. R. Hurley
 *     Date :   7th July 1998
 *
 *     C version by Rob Izzard, 24/01/2001 and onwards
 *     with many additions and updates.
 */

#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef BSE

/* enable (MIX)DEBUG for pretty debugging output that is easy to understand */
#if (DEBUG==1)
#define MIXDEBUG 1
#define ITER_DEBUG
#endif //DEBUG==1

#include "common_envelope_evolution.h"
#include "common_envelope_evolution_BSE.h"

#ifdef __HAVE_VALGRIND__
#include "valgrind/valgrind.h"
#endif //__HAVE_VALGRIND__

static double comenv_stripped_radius(struct star_t * star,
                                     struct stardata_t * stardata,
                                     const double Rtarget);

static void comenv_calc_roche_lobe_radii(const double sep,
                                         const double ecc,
                                         const double m1,
                                         const double m2,
                                         double * rl1,
                                         double * rl2);

int common_envelope_evolution_BSE (
    struct star_t * const donor,
    struct star_t * const accretor,
    struct stardata_t * const stardata,
    const int comenv_prescription
    )
{
    /*
     * Common envelope evolution.
     *
     * Entered only when by giant primary stars.
     *
     * For "simplicity" energies are divided by -G.
     */
    Dprint("pre-append ");

    CEprint("**** Common envelope evolution (BSE) at time %g model %d ****\n",
            stardata->model.time,
            stardata->model.model_number);

    CEprint("orbit in: separation %g, eccentricity %g, period %g, angular momentum %g\n",
            stardata->common.orbit.separation,
            stardata->common.orbit.eccentricity,
            stardata->common.orbit.period,
            stardata->common.orbit.angular_momentum);
    Foreach_star(star)
    {
        CEprint("star %d in: type %d, mass %g Msun, core mass %g Msun, radius %g Rsun, luminosity %g Lsun, age %g, omega %g, angular momentum %g\n",
                star->starnum,
                star->stellar_type,
                star->mass,
                Outermost_core_mass(star),
                star->radius,
                star->luminosity,
                star->age,
                star->omega,
                star->angular_momentum);
        star->deny_SN++;
    }

    char * corestring[NUMBER_OF_STARS];
    char * event_string = events_stack_string(stardata);
    corestring[0] = core_string(&stardata->star[0],FALSE);
    corestring[1] = core_string(&stardata->star[1],FALSE);

    Append_logstring(LOG_COMENV_PRE,
                     "0COMENV n=%d presc=%d %g(%s;%s)+%g(%s;%s) ",
                     stardata->model.comenv_count,
                     comenv_prescription,
                     stardata->star[0].mass,
                     corestring[0],
                     Short_stellar_type_string(stardata->star[0].stellar_type),
                     stardata->star[1].mass,
                     corestring[1],
                     Short_stellar_type_string(stardata->star[1].stellar_type)
        );
    Dprint(
        "PRE_COMENV prob=%20.12g time=%g M1=%20.12g %s st1=%d (prev %d) M2=%20.12g %s st2=%d (prev %d) PER=%20.12g alpha=%20.12g : events pending %d, stack \"%s\" \n",
        stardata->model.probability,
        stardata->model.time,
        stardata->star[0].mass,
        corestring[0],
        stardata->star[0].stellar_type,
        stardata->previous_stardata->star[0].stellar_type,
        stardata->star[1].mass,
        corestring[1],
        stardata->star[1].stellar_type,
        stardata->previous_stardata->star[1].stellar_type,
        stardata->common.orbit.period*YEAR_LENGTH_IN_DAYS,
        stardata->preferences->alpha_ce[stardata->model.comenv_count],
        events_pending(stardata),
        event_string
        );
    Safe_free(event_string);
    Safe_free(corestring[0]);
    Safe_free(corestring[1]);

    if(stardata->preferences->disable_events == FALSE)
    {
        /*
         * Previous contact systems and common envelopes
         * should be erased.
         */
        erase_events_of_type(stardata,
                             BINARY_C_EVENT_CONTACT_SYSTEM,
                             stardata->common.current_event);
        erase_events_of_type(stardata,
                             BINARY_C_EVENT_COMMON_ENVELOPE,
                             stardata->common.current_event);
    }

    const double * const metallicity_parameters = stardata->common.metallicity_parameters;
    const double * const main_sequence_parameters = stardata->common.main_sequence_parameters;
    struct star_t * star1 = &stardata->star[donor->starnum];
    struct star_t * star2 Maybe_unused = &stardata->star[accretor->starnum];

    struct BSE_data_t * bse1 = new_BSE_data();
    struct BSE_data_t * bse2 = new_BSE_data();

    double alpha_ce = stardata->preferences->alpha_ce[stardata->model.comenv_count];
    double EbindI,EbindF=0.0,EorbI=0.0,EorbF=0.0,Ecirc,lambda1,lambda2;
    double r1=0.0,r2=0.0,rc1=0.0,rc2=0.0,rl1=0.0,rl2=0.0,final_separation=0.0;
    double separation_at_core_RLOF=0.0;
    double fage1,fage2,menv,mc22=0.0,mc3,mf,dM1,dM2,dm,moment_of_inertia_factor1,moment_of_inertia_factor2;

    const double jorb_cgs = (donor->mass*M_SUN)*(accretor->mass*M_SUN)/
        (M_SUN*(donor->mass+accretor->mass))*
        sqrt(GRAVITATIONAL_CONSTANT *
             (donor->mass + accretor->mass)*M_SUN *
             stardata->common.orbit.separation * R_SUN  *
             (1.0 - Pow2(stardata->common.orbit.eccentricity))
            );
    const double jorb = Angular_momentum_cgs_to_code(jorb_cgs);
    const double jtot = jorb + donor->angular_momentum + accretor->angular_momentum;

    /* add the stellar spins to this */
    const double Maybe_unused jtot_cgs =
        jorb_cgs +
        Angular_momentum_code_to_cgs(donor->angular_momentum +
                                     accretor->angular_momentum);

#if defined DISCS && defined DISCS_CIRCUMBINARY_FROM_COMENV
    struct disc_t * cbdisc = NULL;
#endif

    double Maybe_unused initial_orbital_period_years = (stardata->common.orbit.separation/AU_IN_SOLAR_RADII)*
        sqrt(stardata->common.orbit.separation/(AU_IN_SOLAR_RADII*(donor->mass + accretor->mass)));

    /* angular frequency in radians per year */
    double Maybe_unused initial_orbital_angular_frequency = TWOPI / initial_orbital_period_years;

#ifdef CONSERVE_ANGMOM_IN_COMENV
/* save the contribution from the cores */
    double djcores=
        // orbital
        (Outermost_core_mass(donor)*M_SUN)*(Outermost_core_mass(accretor)*M_SUN)*sqrt(GRAVITATIONAL_CONSTANT*(stardata->common.orbit.separation*R_SUN)/(M_SUN*(Outermost_core_mass(donor)+Outermost_core_mass(accretor))))
        // spin
        + Angular_momentum_code_to_cgs( (Outermost_core_mass(donor)/ donor->mass)* donor->angular_momentum + (Outermost_core_mass(accretor)/ accretor->mass)* accretor->angular_momentum);
#endif // CONSERVE_ANGMOM_IN_COMENV

    double ospin[NUMBER_OF_STARS],
        orbital_period_years,orbital_angular_frequency,
        effective_mc1,effective_mc2;
    double common_envelope_mass;
    const double m1init = donor->mass, m2init = accretor->mass;
    double Maybe_unused comenv_radius;
#ifdef COMENV_CO_MERGER_GIVES_IIa
    double coco_mass = 0.0;
#endif
#ifdef LOG_COMENV_RSTARS
    const double l1init=star1->luminosity, l2init=star2->luminosity;
#endif // LOG_COMENV_RSTARS
#ifdef NUCSYN
    const double Maybe_unused mc1init = Outermost_core_mass(donor),
        mc2init = Outermost_core_mass(accretor);
    Abundance common_envelope_abundance[ISOTOPE_ARRAY_SIZE];
#ifdef MIXDEBUG
    double msum;
#endif /* MIXDEBUG */
#endif /* NUCSYN */
    Stellar_type merged_stellar_type;
    const Stellar_type stellar_type1in = donor->stellar_type;
    const Stellar_type stellar_type2in = accretor->stellar_type;
    Boolean tzobject = FALSE;
    Star_number kk = 0;
    Yield_source comenv_source = SOURCE_COMENV;

    /* remember the overflowing star that led to comenv */
    Dprint("In comenv # %d : pre sep=%g \n",
           stardata->model.comenv_count,
           stardata->common.orbit.separation);

#ifdef STELLAR_TYPE_LOG
    printf("STELLARTYPES COMENV\n");
#endif // STELLAR_TYPE_LOG

    Dprint("donor->starnum = %d, accretor->starnum = %d, Masses in %g %g; stellar types %d %d; Radii %g %g; J %g %g\n",
           donor->starnum,
           accretor->starnum,
           donor->mass,
           accretor->mass,
           donor->stellar_type,
           accretor->stellar_type,
           stardata->star[0].radius,
           stardata->star[1].radius,
           donor->angular_momentum,
           accretor->angular_momentum
        );
    Dprint("Core masses in donor=%g accretor=%g\n",
           Outermost_core_mass(donor),
           Outermost_core_mass(accretor));
    Dprint("comenv in m1=%g mc1=%g age_donor=%g donor->starnum->phase_start_mass=%g\n",
           donor->mass,
           Outermost_core_mass(donor),
           donor->age,
           star1->phase_start_mass);

#ifdef NUCSYN
    init_comenv_nucsyn(stardata);
#endif // NUCSYN
    /* calculate effective core masses */
    calc_effective_core_masses(&effective_mc1,
                               &effective_mc2,
                               donor->stellar_type,
                               accretor->stellar_type,
                               donor->mass,
                               Outermost_core_mass(donor),
                               accretor->mass,
                               Outermost_core_mass(accretor));
    Dprint("Effective core masses %g %g (m1=%g m2=%g)\n",
           effective_mc1,
           effective_mc2,
           donor->mass,
           accretor->mass);

    CEprint("Effective core masses %g %g Msun\n",
            effective_mc1,
            effective_mc2);

    /* and hence the mass of the common envelope */
    common_envelope_mass = (m1init - effective_mc1) + (m2init - effective_mc2);
    stardata->model.previous_common_envelope_mass = common_envelope_mass;

    CEprint("Common envelope mass %g Msun\n",
            common_envelope_mass);

    Dprint("Comenv mass = %g from m1init=%g effective_mc1=%g (type %d) m2init=%g effective_mc2=%g (type %d)\n",
           common_envelope_mass,
           m1init,
           effective_mc1,
           donor->stellar_type,
           m2init,
           effective_mc2,
           accretor->stellar_type);


    MDEBUG_CALC_MSUM;

    /* Firstly, assume the cores do not coalesce */
    stardata->model.coalesce = FALSE;

    /*
     * Calculate the binding energy of the envelope (multiplied by lambda).
     */
    {
        /*
         * Obtain the core masses and radii and make sure any nucleosynthesis up to
         * this point has been done. First, for star 1.
         */
        {
            Boolean deny_was = stardata->model.deny_new_events;
            Set_event_denial(DENY_EVENTS);
            Dprint("1pre = { stellar_type=%d, m01=%g m1=%g r1=%g J1=%g }n",
                   donor->stellar_type,
                   donor->phase_start_mass,
                   donor->mass,
                   r1,
                   donor->angular_momentum);
            Boolean alloc[3];
            stellar_structure_BSE_alloc_arrays(donor,
                                               donor->bse,
                                               alloc);
            donor->bse->timescales[0] = FORCE_TIMESCALES_CALCULATION;
            donor->deny_SN++;
            stellar_structure(stardata,
                              STELLAR_STRUCTURE_CALLER_common_envelope_evolution,
                              donor,
                              NULL);
            donor->deny_SN--;

            Set_event_denial(deny_was);
            r1 = donor->radius;
            rc1 = donor->core_radius;
            menv = donor->menv;
            moment_of_inertia_factor1 = donor->moment_of_inertia_factor;

            memcpy(bse1->timescales,
                   donor->bse->timescales,
                   sizeof(double)*TSCLS_ARRAY_SIZE);

            stellar_structure_BSE_free_arrays(donor,alloc);
        }

        TIMESCALES1;

        /*
         * menv is the convective envelope mass
         * m1-mc1 is the mass in the envelope outside the core
         * convfrac is the ratio.
         */
        double convfrac = donor->menv / envelope_mass(donor);
        Dprint("convfrac = %g from menv=%g m1=%g mc1=%g\n",
               convfrac,
               donor->menv,
               donor->mass,
               Outermost_core_mass(donor));

        lambda1 = common_envelope_lambda(donor->phase_start_mass,
                                         Outermost_core_mass(donor),
                                         donor->luminosity,
                                         donor->radius,
                                         rzamsf(donor->phase_start_mass,main_sequence_parameters),
                                         donor->core_radius,
                                         convfrac,
                                         donor->stellar_type,
#ifdef NUCSYN
                                         donor->Xenv,
#endif//NUCSYN
                                         stardata);
        CEprint("lambda donor : phase start mass %g Msun, core mass %g Msun, L %g Lsun, R %g Rsun, RZAMS %g Rsun, Rc %g Rsun, convfrac %g, st %d -> %g\n",
                donor->phase_start_mass,
                Outermost_core_mass(donor),
                donor->luminosity,
                donor->radius,
                rzamsf(donor->phase_start_mass,main_sequence_parameters),
                donor->core_radius,
                convfrac,
                donor->stellar_type,
                lambda1);
#if defined COMENV_STRUCTURE && defined ENVELOPE_INTEGRATIONS

        struct envelope_t * envelope = Calloc(1,sizeof(struct envelope_t));

        calculate_stellar_envelope_structure(
            stardata,
            star1,
            envelope,
            Outermost_core_mass(donor) * 1.01,
            1e-4 // mass resolution
            );

        envelope_properties(stardata,
                            star1,
                            envelope);

        printf("Envelope E1 = %g, E2 = %g, E3 = %g : Ebind = %g : lambda %g\n",
               envelope->E1,
               envelope->E2,
               envelope->E3,
               envelope->E_bind,
               envelope->lambda_bind
            );

        Safe_free(envelope->shells);
        Safe_free(envelope);

        Exit_binary_c_no_stardata(0,"Exit from comenv after envelope structure calc\n");
#endif // COMENV_STRUCTURE && ENVELOPE_INTEGRATIONS

#ifdef COMENV_POLYTROPES
        printf("LAMBDA from prescription %g\n",lambda1);
        common_envelope_polytrope(donor->phase_start_mass,
                                  Outermost_core_mass(donor),
                                  donor->radius,
                                  donor->core_radius,
#ifdef NUCSYN
                                  donor->Xenv,
#endif//NUCSYN
                                  stardata);

        Exit_binary_c_no_stardata(0,"Exit from comenv after polytrope calc\n");
#endif // COMENV_POLYTROPES

        {
#undef X
#define X(CODE,NUM) #CODE,
            static char * lambda_prescription_strings[] = {
                "fixed",
                COMENV_LAMBDA_LIST
            };
#undef X
            const double _lambda = stardata->preferences->lambda_ce[stardata->model.comenv_count];
            Append_logstring(LOG_COMENV_PRE,
                             "LAMBDA(%s,st=%d,m01=%g,mc1=%g,l1=%g,r1=%g,rzams=%g,convfrac=%g,lambda_ion=%g)=%g ",
                             lambda_prescription_strings[
                                 More_or_equal(_lambda,0.0) ? 0 : (int)(-_lambda+0.5)
                                 ],
                             donor->stellar_type,
                             donor->phase_start_mass,
                             Outermost_core_mass(donor),
                             donor->luminosity,
                             donor->radius,
                             donor->rzams,
                             convfrac,
                             stardata->preferences->lambda_ionisation[stardata->model.comenv_count],
                             lambda1);
        }

        /*
         * Add a possible contribution from star 2 if it's
         * a giant
         */
        STRUCTURE2(DENY_EVENTS);

        /*
         * Calculate lambda, the binding energy coefficients.
         */
        if(comenv_prescription == COMENV_NANDEZ2016)
        {
            /*
             * The Nandez and Ivanova formalism does not
             * include a contribution from star 2, and they give
             * a fitting formula only for alpha * lambda, so set
             * alpha=1 and then lambda1 is the a proxy for alpha*lambda.
             *
             * We cannot formally include the envelope energy of a giant secondary
             * with this formalism, but instead we calculate the energy contribution
             * from each star merging with the companion's core.
             *
             * NB Typically, there is common envelope between two giants
             * after fast accretion from a giant onto a white dwarf, which forms
             * a new envelope (often a TPAGB star). The mass of the envelope is
             * usually small, so mc2 ~ m2 in any case.
             *
             * Be warned: the Nandez and Ivanova prescription is only
             * valid over a very limited range of M1, Mc1, M2, and the
             * values are simply clamped to this range. This means they are
             * likely to be wrong.
             */
            alpha_ce = 1.0;
            if(GIANT_LIKE_STAR(accretor->stellar_type))
            {
                lambda1 = calc_alpha_lambda_Nandez2016(donor->mass,
                                                       Outermost_core_mass(accretor),
                                                       Outermost_core_mass(donor));
                lambda2 = calc_alpha_lambda_Nandez2016(accretor->mass,
                                                       Outermost_core_mass(donor),
                                                       Outermost_core_mass(accretor));
            }
            else
            {
                lambda1 = calc_alpha_lambda_Nandez2016(donor->mass,
                                                       accretor->mass,
                                                       Outermost_core_mass(donor));
                lambda2 = LARGE_LAMBDA;
            }
            Dprint("CE Nandez+Ivanova : alpha=%g lambda1=%g lambda2=%g (m2=%g mc2=%g)\n",
                   alpha_ce,
                   lambda1,
                   lambda2,
                   accretor->mass,
                   Outermost_core_mass(accretor));
        }
        else
        {
            if(GIANT_LIKE_STAR(accretor->stellar_type))
            {
                const double convfrac2 = accretor->menv/envelope_mass(accretor);
                Dprint("convfrac2 = %g/(%g-%g) = %g\n",
                       menv,
                       donor->mass,
                       Outermost_core_mass(donor),
                       convfrac2);

                lambda2 = common_envelope_lambda(accretor->phase_start_mass,
                                                 Outermost_core_mass(accretor),
                                                 accretor->luminosity,
                                                 accretor->radius,
                                                 rzamsf(accretor->phase_start_mass,main_sequence_parameters),
                                                 accretor->core_radius,
                                                 convfrac2,
                                                 accretor->stellar_type,
#ifdef NUCSYN
                                                 stardata->star[accretor->starnum].Xenv,
#endif//NUCSYN
                                                 stardata);

                CEprint("lambda accretor : phase start mass %g Msun, core mass %g Msun, L %g Lsun, R %g Rsun, RZAMS %g Rsun, Rc %g Rsun, convfrac %g, st %d -> %g\n",
                        accretor->phase_start_mass,
                        Outermost_core_mass(accretor),
                        accretor->luminosity,
                        accretor->radius,
                        rzamsf(accretor->phase_start_mass,main_sequence_parameters),
                        accretor->core_radius,
                        convfrac2,
                        accretor->stellar_type,
                        lambda2);
            }
            else
            {
                lambda2 = LARGE_LAMBDA;
            }
        }
#define LARGE_ENERGY (1e100)

        const double EbindI_donor =
            donor->mass * envelope_mass(donor)/(lambda1*r1);
        const double EbindI_accretor =
            accretor->mass*envelope_mass(accretor)/(lambda2*r2);
        EbindI =
            (Is_zero(lambda1) ||
             Is_zero(lambda2) ||
             Is_zero(r1) ||
             Is_zero(r2)) ?
            LARGE_ENERGY :
            (EbindI_donor + EbindI_accretor);

        CEprint("EbindI %g + %g = %g\n",
                fabs(EbindI_donor) < TINY ? 0.0 : (EbindI_donor * CGS_ENERGY),
                fabs(EbindI_accretor) < TINY ? 0.0 : (EbindI_accretor * CGS_ENERGY),
                EbindI * CGS_ENERGY);

        Dprint("EbindI %g\n",
               EbindI * CGS_ENERGY);
    }

    /*
     * Calculate the initial orbital energy
     */
    if(CEFLAG != 3)
    {
        EorbI =
            Is_zero(stardata->common.orbit.separation) ?
            LARGE_ENERGY :
            (Outermost_core_mass(donor) * (GIANT_LIKE_STAR(accretor->stellar_type) ? Outermost_core_mass(accretor) : accretor->mass)/(2.0*stardata->common.orbit.separation));

        Dprint("EorbI %g (donor->mc=%g accretor->core_mass=%g accretor->mass=%g accretor->stellar_type=%d sep=%g)\n",
               EorbI * CGS_ENERGY,
               Outermost_core_mass(donor),
               Outermost_core_mass(accretor),
               accretor->mass,
               accretor->stellar_type,
               stardata->common.orbit.separation);
    }

    // this should include star 2's contribution to the envelope radius
    double vescape = sqrt(2.0 * GRAVITATIONAL_CONSTANT *  M_SUN * common_envelope_mass / (R_SUN*r1));

    /*
     * Calculate alpha_kinetic
     */
    double Maybe_unused alpha_kin = calc_alpha_kin_Nandez2016(donor->mass,
                                                              accretor->mass,
                                                              Outermost_core_mass(donor));

    /*
     * Calculate alpha_unbound
     */
    double Maybe_unused alpha_unbound = calc_alpha_unbound_Nandez2016(donor->mass,
                                                                      accretor->mass,
                                                                      Outermost_core_mass(accretor));

    /*
     * calculate stellar spins
     */
    ospin[0] = donor->angular_momentum / (moment_of_inertia_factor1*Pow2(r1)*envelope_mass(donor)+CORE_MOMENT_OF_INERTIA_FACTOR*Pow2(rc1)* Outermost_core_mass(donor));
    ospin[1] = accretor->angular_momentum / (moment_of_inertia_factor2*Pow2(r2)*envelope_mass(accretor)+CORE_MOMENT_OF_INERTIA_FACTOR*Pow2(rc2)* Outermost_core_mass(accretor));

    double ntp,ntp_since_mcmin;

    /*
     * We need to save the state of the most evolved COWD core
     * in the case one or both stars is a TPAGB star. In the case
     * of a merger, the number of thermal pulses is set to the
     * maximum of the two stars.
     */
    set_ntp(&ntp,&ntp_since_mcmin,&donor->prev_tagb,stardata);

    Dprint("post-stellar_structure stellar types %d %d\n",
           donor->stellar_type,
           accretor->stellar_type);
    Mix_debug_in_out("Entry");
    Dprint("Comenv in: separation = %g\n",stardata->common.orbit.separation);

    /*
     * Handle accretion of material
     */
#ifdef COMENV_ACCRETION
    comenv_accretion(ACCRETE_ARGS);
#endif //COMENV_ACCRETION

#ifdef NUCSYN
    nucsyn_dilute_shell_to(m1init-effective_mc1,
                           star1->Xenv,
                           m2init-effective_mc2,
                           star2->Xenv,
                           common_envelope_abundance);
    MDEBUG_PRETTY_STARS_IN;
#endif /* NUCSYN */

    /*
     * Common envelope radius is approximately the
     * maximum of the radii of the two stars. Almost always
     * this is the radius of the giant component.
     */
    comenv_radius = Max(r1,r2);

    /*
     * Allow for an eccentric orbit.
     */
    Ecirc = EorbI / (1.0-Pow2(stardata->common.orbit.eccentricity));

    /*
     * Calculate the final orbital energy assuming the envelope
     * is driven off before the cores merge.
     *
     * Note: if alpha_ce == 0.0, we set alpha_ce = 1e-30 (i.e.
     * very small) and continue to prevent Inf.
     */
    EorbF = EorbI + EbindI / (Is_zero(alpha_ce) ? 1e-30 : alpha_ce);

    /* Logging */
    if(comenv_prescription==COMENV_NELEMANS_TOUT &&
       USE_NELEMANS)
    {
        Append_logstring(LOG_COMENV_PRE,"Nelemans gamma=%g ",
                         stardata->preferences->nelemans_gamma);
    }
    else
    {
        Append_logstring(LOG_COMENV_PRE,"alpha=%g lambda=%g ",alpha_ce,lambda1);
    }
    Append_logstring(LOG_COMENV_PRE,
                     "a_in=%g P_in=%g Jtot_in=%g Jorb_in=%g EorbI=%g EbindI=%g ",
                     stardata->common.orbit.separation,
                     stardata->common.orbit.period*365.25, // days
                     jtot_cgs,
                     Angular_momentum_code_to_cgs(stardata->common.orbit.angular_momentum),
                     -CGS_ENERGY*EorbI,
                     CGS_ENERGY*EbindI
        );

    Dprint("EorbF = (EorbI=%g) + (EbindI=%g)/(alpha=%g) ... = %g\n",
           EorbI*CGS_ENERGY,
           EbindI*CGS_ENERGY,
           alpha_ce,
           EorbF*CGS_ENERGY);
    Dprint("Hence critical separation = %g (r1=%g mc1=%g mc2=%g)\n",
           1.0/(2.0*(donor->mass)*envelope_mass(donor)/
                (alpha_ce*lambda1*r1*(Outermost_core_mass(donor))*(
                    GIANT_LIKE_STAR(accretor->stellar_type)?Outermost_core_mass(accretor):accretor->mass))
                +1.0/(stardata->common.orbit.separation)),
           r1,
           Outermost_core_mass(donor),
           GIANT_LIKE_STAR(accretor->stellar_type)?Outermost_core_mass(accretor):accretor->mass);
    Dprint("Radii of cores %g %g\n",
           rc1,
           rc2);

    /*
     * Now deal with the specific cases for each stellar type
     *
     * First, secondary/accretor is a main sequence star.
     */
    if(ON_EITHER_MAIN_SEQUENCE(accretor->stellar_type))
    {
        Dprint("Secondary/accretor is on either main sequence\n");

        /*
         * calculate final separation, note that this assume e = 0
         */
        if(comenv_prescription==COMENV_NELEMANS_TOUT &&
           USE_NELEMANS)
        {
            final_separation = nelemans_separation(stardata->common.orbit.separation,
                                                   m1init,
                                                   m2init,
                                                   effective_mc1,
                                                   effective_mc2,
                                                   stardata->preferences->nelemans_gamma,stardata);
        }
        /*
         * Assume the energy generated by forcing the
         * secondary to co-rotate goes into the envelope
         * (experimental - not tested by RGI!).
         */
        else if(CEFLAG>5)
        {
            orbital_period_years = (final_separation/AU_IN_SOLAR_RADII)*
                sqrt(final_separation/(AU_IN_SOLAR_RADII*(Outermost_core_mass(donor) + accretor->mass)));
            orbital_angular_frequency = TWOPI/orbital_period_years;
            double dely = 0.50* accretor->mass *r2*r2*(ospin[1]*ospin[1] - orbital_angular_frequency*orbital_angular_frequency)/3.91e+08;
            dely *= moment_of_inertia_factor2;
            EbindI = Max(0.0,EbindI - dely);
            EorbF = EorbI + EbindI/alpha_ce;
            final_separation = Outermost_core_mass(donor) * accretor->mass/(2.0*EorbF);
        }
        else
        {
            final_separation = Outermost_core_mass(donor) * accretor->mass/(2.0*EorbF);

            CEprint("Alpha prescription: alpha = %g, a = %g Rsun (core mass donor %g Msun, accretor mass %g Msun, EorbF = %g erg)\n",
                    alpha_ce,
                    final_separation,
                    Outermost_core_mass(donor),
                    accretor->mass,
                    EorbF * CGS_ENERGY)
        }


        Dprint("Final separation = %g (mc1=%g m2=%g EorbF=%g)\n",
               final_separation,
               Outermost_core_mass(donor),
               accretor->mass,
               EorbF*CGS_ENERGY);

        CEprint("Separation when envelope is ejected %g Rsun\n",
                final_separation);
        CEprint("Effective core masses %g %g Msun\n",
                effective_mc1,
                effective_mc2);
        if(Is_zero(final_separation))
        {
            stardata->model.coalesce = TRUE;
            separation_at_core_RLOF = TINY;
            rl1 = rl2 = TINY;
        }
        else
        {
            /* calculate Roche lobes of the cores */
            comenv_calc_roche_lobe_radii(final_separation,
                                         stardata->preferences->comenv_post_eccentricity,
                                         Outermost_core_mass(donor),
                                         effective_mc2,//accretor->mass,
                                         &rl1,
                                         &rl2);
            Dprint("rl1 = %g rl2 = %g from mc1 = %g m2 = %g sep = %g ecc = %g\n",
                   rl1,
                   rl2,
                   Outermost_core_mass(donor),
                   accretor->mass,
                   final_separation,
                   stardata->preferences->comenv_post_eccentricity);
            CEprint("(effective) q %g %g\n",
                    Outermost_core_mass(donor)/effective_mc2,
                    effective_mc2/Outermost_core_mass(donor));
            CEprint("Roche radii %g %g\n",
                    rl1,rl2);
            CEprint("Core radii %g %g\n",
                    rc1,rc2);
            separation_at_core_RLOF = rl1 + rl2;
            Dprint("RLOF (rc1/rl1=%g/%g=%g r2/rl2=%g/%g=%g) if rc1=%g > rl1=%g or r2=%g > rl2=%g\n",
                   rc1,rl1,rc1/rl1,
                   r2,rl2,r2/rl2,
                   rc1,rl1,
                   r2, rl2);

            /*
             * Check if the cores merge to form a single star
             */
            Dprint("Check for RLOF rc1 = %g, r2 = %g\n",rc1,r2);
            CHECK_FOR_CORE_RLOF(r2);
        }

        CEprint("Merge? %s\n",Yesno(stardata->model.coalesce));

        if(stardata->model.coalesce == TRUE)
        {
            /*
             * Cores have merged
             *
             * The collision_matrix array contains
             * the stellar type of the merged object.
             */
            merged_stellar_type =
                stardata->store->collision_matrix[donor->stellar_type][accretor->stellar_type] - 100;

            Dprint("set new stellar type from [%d,%d] -> %d\n",
                   donor->stellar_type,
                   accretor->stellar_type,
                   merged_stellar_type);

            /* core mass of new star is the core of star 1 */
            mc3 = Outermost_core_mass(donor);

            /*
             * which leaves a mixture of the original common envelope with the
             * envelope of star 2, though we don't know how much mass remains
             */
            if(accretor->stellar_type==HeMS &&
               merged_stellar_type==CHeB)
            {
                /*
                 * an exception : if the companion is a HeMS star and the
                 * star is a core helium burning star then the helium star
                 * merges with the core helium burning star's core
                 */
                mc3 += accretor->mass;
            }

            CEprint("Merged star type %d, M %g Msun, Mc %g Msun\n",
                    merged_stellar_type,
                    donor->mass,
                    mc3);

            /* Coalescence - calculate final orbital binding energy */
            EorbF = Max(Outermost_core_mass(donor) * //accretor->mass
                        effective_mc2
                        /(2.0*separation_at_core_RLOF), EorbI);
            CEprint("EorbF (coalesce) = Max(%g * %g / (2 * %g), %g)\n",
                    Outermost_core_mass(donor),
                    accretor->mass,
                    separation_at_core_RLOF,
                    EorbI * CGS_ENERGY);

            /* .. and the final binding energy */
            const double dEorb = EorbI - EorbF;
            CEprint("dEorb = %g - %g\n",
                    EorbI * CGS_ENERGY,
                    EorbF * CGS_ENERGY);

            EbindF = EbindI + alpha_ce*dEorb;

            CEprint("Binding energy target %g + %g * %g = %g\n",
                    EbindI * CGS_ENERGY,
                    alpha_ce,
                    dEorb * CGS_ENERGY,
                    EbindF * CGS_ENERGY);

            Dprint("EbindF from core coal = %g, EbindI=%g lambda1=%g alpha_ce=%g EorbF=%g EorbI=%g\n",
                   EbindF*CGS_ENERGY,
                   EbindI*CGS_ENERGY,
                   lambda1,
                   alpha_ce,
                   EorbF*CGS_ENERGY,
                   EorbI*CGS_ENERGY);
        }
        else
        {
            Dprint("cores do not collide : final separation %g : final Roche lobes 1=%g 2=%g, donor R=%g Rc=%g rc1=%g\n",
                   final_separation,
                   rl1,
                   rl2,
                   donor->radius,
                   donor->core_radius,
                   rc1
                );
            Dprint("stellar_type %d, m1=%g mc1=%g m1-mc1=%g (m01=%g)\n",
                   donor->stellar_type,
                   donor->mass,
                   Outermost_core_mass(donor),
                   envelope_mass(donor),
                   donor->phase_start_mass);

            /*
             * Primary is stripped and becomes a
             * black hole, neutron star, white dwarf or helium star.
             * Update the stellar_structure.
             */
            mf = donor->mass;

            if(stardata->preferences->post_ce_objects_have_envelopes == TRUE)
            {
                if(final_separation < stardata->common.orbit.separation &&
                   envelope_mass(donor) > MIN_ENV_MASS_TO_HAVE_ANY_LEFT_OVER)
                {
                    /*
                     * Rob's additon: add a thin envelope to the remnant
                     * to better simulate the crossing of the HRD.
                     *
                     * NB only for GB/AGB stars at the moment
                     */


                    /*
                     * But what is the new Roche radius?
                     * It's a function of the mass ratio (which
                     * we don't know) and the separation (which
                     * we do know).
                     *
                     * Now, the mass is *approximately* mc1 ...
                     * plus a little bit.
                     */
                    double Rtarget =  CE_ROCHE_FILL_FACTOR *
                        Roche_radius(Outermost_core_mass(donor) / accretor->mass,
                                     final_separation);

                    /*
                     * Correct for eccentricity
                     */
                    Rtarget *= (1.0 - stardata->preferences->comenv_post_eccentricity);

                    const double Rstripped = comenv_stripped_radius(donor,
                                                                    stardata,
                                                                    Rtarget);

                    /*
                     * The radius cannot exceed the radius
                     * of the stripped core
                     */
                    Rtarget = Max(Rtarget,Rstripped);

                    Dprint("BISECT R target %g, R1 = %g, Rstripped = %g (q=%g/%g=%g, a=%g, fill factor %g)\n",
                           Rtarget,
                           star1->radius,
                           Rstripped,
                           Outermost_core_mass(donor),
                           accretor->mass,
                           Outermost_core_mass(donor)/ accretor->mass,
                           final_separation,
                           CE_ROCHE_FILL_FACTOR);

                    if(Rtarget > star1->radius)
                    {
                        /*
                         * If the target radius exceeds the current
                         * radius, there is no solution.
                         *
                         * In this case proceed as with the old
                         * algorithm, although what's happening
                         * doesn't really make any sense.
                         */
                        donor->mass = Outermost_core_mass(donor);
                        printf("R target exceeds current radius : no solution\n");
                    }
                    else
                    {
                        Dprint("Call add env : star = %d, merged_stellar_type=%d\n",
                               donor->starnum,donor->stellar_type);

                        add_envelope_to_remnant(stardata,
                                                donor,
                                                common_envelope_mass,
                                                Rtarget,
                                                stellar_type1in);

                        Dprint("COMENV New envelope: M = %g, mc = %g -> menv = %g, R = %g vs sep = %g, Rtarget = %g\n",
                               mf,
                               Outermost_core_mass(donor),
                               mf-Outermost_core_mass(donor),
                               r1,
                               final_separation,
                               Rtarget);
                    }
                }
                else
                {
                    donor->mass = Outermost_core_mass(donor);
                }

            }
            else
            {
                /*
                 * no envelope for post-CE object (Hurley treatment):
                 * m1 is just the original core
                 */
                donor->mass = Outermost_core_mass(donor);
            }

            Dprint("comenv kw=%d R=%g age=%g\n",
                   star1->stellar_type,
                   star1->radius,
                   star1->age
                );

            STRUCTURE1(ALLOW_EVENTS);

            Dprint("comenv kw=%d R=%g age=%g\n",
                   donor->stellar_type,
                   r1,
                   donor->age
                );
        }
    }
    else
    {
        /*
         * Degenerate or giant secondary.
         *
         * Calculate the new separation of the cores, note that
         * this assumes e = 0
         */
        if(comenv_prescription==COMENV_NELEMANS_TOUT
           && USE_NELEMANS)
        {
            final_separation = nelemans_separation(stardata->common.orbit.separation,
                                                   m1init,m2init,
                                                   effective_mc1,effective_mc2,
                                                   stardata->preferences->nelemans_gamma,stardata);
        }
        else
        {
            final_separation = Outermost_core_mass(donor) * Outermost_core_mass(accretor)/(2.0*EorbF);
        }


        Dprint("sepF=%g\n",final_separation);

        /*
         * Assume the energy generated by forcing the secondary to co-rotate goes into
         * the envelope (experimental).
         */
        if(COMPACT_OBJECT(accretor->stellar_type) && CEFLAG>5)
        {
            orbital_period_years = final_separation/AU_IN_SOLAR_RADII*sqrt(final_separation/(AU_IN_SOLAR_RADII*(Outermost_core_mass(donor) + accretor->mass)));
            orbital_angular_frequency = TWOPI/orbital_period_years;
            const double dely = 0.50* accretor->mass * Pow2(r2) * (Pow2(ospin[1]) - Pow2(orbital_angular_frequency))/3.91e+08
                * CORE_MOMENT_OF_INERTIA_FACTOR;
            EbindI = Max(0.0,EbindI - dely);
            EorbF = EorbI + EbindI/alpha_ce;
            final_separation = Outermost_core_mass(donor) * accretor->mass/(2.0*EorbF);
        }

        /*
         * Check for core Roche lobe overflow of either
         * core onto the other.
         */
        comenv_calc_roche_lobe_radii(final_separation,
                                     stardata->preferences->comenv_post_eccentricity,
                                     Outermost_core_mass(donor),
                                     Outermost_core_mass(accretor),
                                     &rl1,
                                     &rl2);
        Dprint("rl1 = %g rl2 = %g from mc1 = %g mc2 = %g sep = %g\n",
               rl1,rl2,Outermost_core_mass(donor),Outermost_core_mass(accretor),final_separation);
        Dprint("Check for core RLOF rc1 = %g, rc2 = %g\n",rc1,rc2);
        CHECK_FOR_CORE_RLOF(rc2);

        if(stardata->model.coalesce == TRUE)
        {
            Dprint("Cores merge\n");

            /*
             * If the secondary was a neutron star or black hole the outcome
             * is an unstable Thorne-Zytkow object that leaves only the core.
             */
            final_separation = 0.0;
            if(POST_SN_OBJECT(accretor->stellar_type))
            {
                Dprint("T-Z object!\n");
                tzobject=TRUE;

#ifdef ANTI_TZ
                star1->TZ_object=TRUE;
                star1->TZ_mass = donor->mass + accretor->mass;
                star1->TZ_NS_mass = accretor->mass;
                star1->TZ_channel=TZ_CHANNEL_COMENV;
#endif //ANTI_TZ
                stellar_structure_make_massless_remnant(stardata,
                                                        donor);


#ifdef __DEPRECATED
                /*
                 * Set the SN type for ensemble logging, and add a supernova
                 * event for the log
                 */
                struct star_t * news Maybe_unused =
                  new_supernova(stardata,donor,accretor,NULL);
#endif//__DEPRECATED
                /*
                 * The envelope mass is not required in this case.
                 */
                goto end_section;
            }

            Dprint("merge from %d %d\n",donor->stellar_type,accretor->stellar_type);

            merged_stellar_type =
                stardata->store->collision_matrix[donor->stellar_type][accretor->stellar_type] - 100;

            //Stellar_type core_stellar_type = Core_stellar_type(stellar_type);

            mc3 = Outermost_core_mass(donor) + Outermost_core_mass(accretor);
            Dprint("new core mass %g + %g -> %g\n",
                   Outermost_core_mass(donor),
                   Outermost_core_mass(accretor),
                   mc3);

            /*
             * Calculate the final orbital and envelope binding energy.
             */
            EorbF = Max( (Outermost_core_mass(donor) * Outermost_core_mass(accretor)/(2.0*separation_at_core_RLOF)) , EorbI);
            EbindF = EbindI - alpha_ce*(EorbF - EorbI);
            CEprint("EbindF = (EbindI = %g) - (alpha = %g) * (EorbF = %g - EorbI = %g) = %g\n",
                    EbindF,
                    EbindI,
                    alpha_ce,
                    EorbF,
                    EorbI
                );
            Dprint("recalc ? EbindF from core coal = %g, EbindI=%g lambda1=%g alpha_ce=%g EorbF=%g EorbI=%g (mc1=%g)\n",
                   EbindF*CGS_ENERGY,
                   EbindI*CGS_ENERGY,
                   lambda1,
                   alpha_ce,
                   EorbF*CGS_ENERGY,
                   EorbI*CGS_ENERGY,
                   Outermost_core_mass(donor));

            /*
             * Check if we have the merging of two degenerate cores and if so
             * then see if the resulting core will survive or change form.
             */
            if(donor->stellar_type == TPAGB &&
               (accretor->stellar_type == TPAGB || accretor->stellar_type >= COWD))
            {
                Dprint("Call degenerate_core_merger 1\n");
                degenerate_core_merger(stardata,
                                       donor,
                                       accretor,
                                       donor->stellar_type,
                                       accretor->stellar_type,
                                       &merged_stellar_type,
                                       Outermost_core_mass(donor),
                                       Outermost_core_mass(accretor),
                                       &mc3,
                                       &EbindF);
                CEprint("Degenerate core merger: new mc %g Msun, EbindF %g erg\n",
                        mc3,
                        EbindF * CGS_ENERGY);
            }
            else if(donor->stellar_type <= FIRST_GIANT_BRANCH &&
                    Less_or_equal((donor->phase_start_mass),metallicity_parameters[ZPAR_MASS_HE_FLASH]))
            {
                if((accretor->stellar_type >= HERTZSPRUNG_GAP &&
                    accretor->stellar_type <= FIRST_GIANT_BRANCH &&
                    Less_or_equal(accretor->phase_start_mass,metallicity_parameters[ZPAR_MASS_HE_FLASH]))
                   || accretor->stellar_type ==HeWD )
                {
                    Dprint("Call degenerate_core_merger 2\n");
                    degenerate_core_merger(stardata,
                                           donor,
                                           accretor,
                                           donor->stellar_type,
                                           accretor->stellar_type,
                                           &merged_stellar_type,
                                           Outermost_core_mass(donor),
                                           Outermost_core_mass(accretor),
                                           &mc3,
                                           &EbindF);
                    Dprint("degenerate_core_merger gave stellar type %d\n",
                           merged_stellar_type);
                    CEprint("Degenerate core merger: new mc %g Msun, EbindF %g erg\n",
                            mc3,
                            EbindF * CGS_ENERGY);

                    if(merged_stellar_type>=HeWD)
                    {
                        donor->stellar_type = merged_stellar_type;
                        donor->core_stellar_type = Core_stellar_type(donor->stellar_type);
                        set_no_core(donor);
                        const Core_type core_id = ID_core(merged_stellar_type);
                        if(core_id != CORE_NONE)
                        {
                            donor->core_mass[core_id] = mc3;
                        }
                        donor->mass = mc3;
                        donor->age = 0.0;
                        Dprint(">=HeWD mc1=%g m1=%g\n",Outermost_core_mass(donor),donor->mass);

                        if(donor->stellar_type<MASSLESS_REMNANT)
                        {
                            donor->phase_start_mass = mc3;
                        }
                        stellar_structure_make_massless_remnant(stardata,
                                                                accretor);
                        goto end_section;
                    }
                }
            }

        }
        else
        {
            /*
             * The cores do not coalesce - assign the correct masses and ages.
             */
            Dprint("Cores do not merge\n");

            if(stardata->preferences->post_ce_objects_have_envelopes == TRUE)
            {
                if((final_separation < stardata->common.orbit.separation)&&
                   (envelope_mass(donor) > MIN_ENV_MASS_TO_HAVE_ANY_LEFT_OVER))
                {
                    add_envelope_to_remnant(stardata,
                                            donor,
                                            common_envelope_mass,
                                            rl1,
                                            stellar_type1in);
                }
                else
                {
                    donor->mass = Outermost_core_mass(donor);
                }
            }
            else
            {
                /* no envelope for post-CE object (Hurley treatment) */
                donor->mass = Outermost_core_mass(donor);
            }

            STRUCTURE1(ALLOW_EVENTS);

            Dprint("STELLAR_STRUCTURE leaves us with stellar types %d %d\n",
                   donor->stellar_type,
                   accretor->stellar_type);
            Dprint("Supernovae? %d %d\n",donor->SN_type,accretor->SN_type);

            mf = accretor->mass;
            accretor->mass = Outermost_core_mass(accretor);

            STRUCTURE2(ALLOW_EVENTS);
        }
    }


#ifdef NUCSYN
    if(stardata->model.coalesce==TRUE)
    {
        AGB_merger_nucsyn(merged_stellar_type,
                          ntp,
                          ntp_since_mcmin,
                          donor->prev_tagb,
                          star1,
                          stardata);
    }
#endif //NUCSYN

    if(stardata->model.coalesce == TRUE)
    {
        mc22 = Outermost_core_mass(accretor);
        Dprint("merge mc22=%g\n",mc22);
        if(merged_stellar_type==CHeB ||
           merged_stellar_type==HeMS )
        {
            /*
             * If making a helium burning star calculate the fractional age
             * depending on the amount of helium that has burnt.
             */
            AGE_HELIUM_STAR_OR_CORE;
        }
        else
        {
            fage1 = fage2 = 0.0;
        }
    }
    else
    {
        mc22 = 0.0;
        fage1 = fage2 = 0.0;
    }

#ifdef STELLAR_POPULATIONS_ENSEMBLE

    /*
     * If there is a merger, make the comenv type negative
     */
    if(stardata->model.coalesce == TRUE)
    {
        stardata->model.comenv_type =
            stellar_type1in == HG ? ENSEMBLE_COMENV_HG_MERGER :
            stellar_type1in == GIANT_BRANCH ? ENSEMBLE_COMENV_GB_MERGER :
            stellar_type1in == CHeB ? ENSEMBLE_COMENV_CHeB_MERGER :
            stellar_type1in == EAGB ? ENSEMBLE_COMENV_EAGB_MERGER :
            stellar_type1in == TPAGB ? ENSEMBLE_COMENV_TPAGB_MERGER :
            stellar_type1in == HeHG ? ENSEMBLE_COMENV_HeHG_MERGER :
            stellar_type1in == HeGB ? ENSEMBLE_COMENV_HeGB_MERGER :
            ENSEMBLE_COMENV_OTHER_MERGER;

        stardata->model.comenv_type *= -1;
    }
    else
    {
        stardata->model.comenv_type =
            stellar_type1in == HG ? ENSEMBLE_COMENV_HG_DETACHED :
            stellar_type1in == GIANT_BRANCH ? ENSEMBLE_COMENV_GB_DETACHED :
            stellar_type1in == CHeB ? ENSEMBLE_COMENV_CHeB_DETACHED :
            stellar_type1in == EAGB ? ENSEMBLE_COMENV_EAGB_DETACHED :
            stellar_type1in == TPAGB ? ENSEMBLE_COMENV_TPAGB_DETACHED :
            stellar_type1in == HeHG ? ENSEMBLE_COMENV_HeHG_DETACHED :
            stellar_type1in == HeGB ? ENSEMBLE_COMENV_HeGB_DETACHED :
            ENSEMBLE_COMENV_OTHER_DETACHED;
    }

    /* 0 is a failure : shouldn't ever happen */
    if(stardata->model.comenv_type==0)
    {
        Exit_binary_c(BINARY_C_WRONG_ARGUMENT,"comenv_type set to 0, i.e. unknown, from stellar_type1in=%d\n",
                      stellar_type1in);
    }

#endif // STELLAR_POPULATIONS_ENSEMBLE

    /*
     * Now calculate the final mass following coalescence.  This requires a
     * Newton-Raphson iteration.
     */
    if(stardata->model.coalesce == TRUE)
    {
        /*
         * The cores have coalesced but some envelope has been lost:
         * calculate how much mass is lost
         */
        Dprint("Newton-Raphson m1=%g mc1=%g\n",donor->mass,Outermost_core_mass(donor));

        /*
         * Calculate the orbital spin just before coalescence.
         * This is the orbital period in years.
         */
        orbital_period_years = (separation_at_core_RLOF/AU_IN_SOLAR_RADII)*
            sqrt(separation_at_core_RLOF/(AU_IN_SOLAR_RADII*(Outermost_core_mass(donor) +
                                                      Outermost_core_mass(accretor))));

        Dprint("orbital period just before merger : %g from sepL=%g mc1=%g mc2=%g\n",
               orbital_period_years,
               separation_at_core_RLOF,
               Outermost_core_mass(donor),
               Outermost_core_mass(accretor)
            );

        /*
         * ... and hence the orbital angular frequency (radians per year)
         */
        orbital_angular_frequency = TWOPI / orbital_period_years;

        Dprint("omega orb %g\n",orbital_angular_frequency);
        CEprint("Binding energy target %g\n",EbindF * CGS_ENERGY);

        if(Less_or_equal(EbindF,0.0))
        {
            /*
             * Binding energy is positive : all the envelope is lost
             */
            mf = mc3;
            CEprint("Binding energy = %g >= 0 : eject envelope\n",
                    EbindF * CGS_ENERGY);
        }
        else
        {
            /*
             * Envelope is bound : calculate how much mass
             * remains ...
             */
            const double Ebindratio = EbindF/EbindI;
            const double xx = 1.0 + metallicity_parameters[7];
            const double some_constant_thing = (pow((donor->mass + accretor->mass),xx))*
                (envelope_mass(donor) + accretor->mass - mc22)*Ebindratio;

            /* Initial Guess.*/
            mf = Max(Outermost_core_mass(donor) + mc22,
                     (donor->mass + accretor->mass)*pow(Ebindratio,1.0/xx));
            const double delm = Outermost_core_mass(donor) + mc22;
            double dely = pow(mf,xx)*(mf - delm) - some_constant_thing;

            Dprint("iterate for mf, m1=%g; Initial guess mf=%g : delm=%g (%g-%g) dely=%g\n",
                   donor->mass,mf,delm,Outermost_core_mass(donor),mc22,dely);

            while (Abs_more_than(dely/mf,ACCURACY))
            {
                const double deri = pow(mf,metallicity_parameters[7])*
                    ((1.0+xx)*mf - xx*(Outermost_core_mass(donor) + mc22));
                const double delmf = dely/deri;
                mf -= delmf;
                dely = pow(mf,xx)*(mf - delm) - some_constant_thing;
                CEprint("iteration %g\n",dely/mf);
            }
            Dprint("mf=%g\n",mf);
            CEprint("Merged star mass %g Msun\n",mf);
        }

        /*
         * Set the masses and separation.
         */
        if(Is_zero(mc22)) mf = Max(mf,Outermost_core_mass(donor)+accretor->mass);

#ifdef STRIP_SUPERMASSIVE_STARS
        mf = Min(MAXIMUM_STELLAR_MASS,mf);
        trim_cores(donor,MAXIMUM_STELLAR_MASS);
#endif // STRIP_SUPERMASSIVE_STARS

        /* donor now has all the mass */
        donor->mass = mf;


        Dprint("Final mass %g (%g), stellar types %d %d, ages %g %g\n",
               mf,
               MAXIMUM_STELLAR_MASS,
               donor->stellar_type,
               accretor->stellar_type,
               donor->age,
               accretor->age);

        /*
         * Combine the core masses.
         */
        Dprint("Combine the cores: new stellar type %d age %g\n",
               merged_stellar_type,
               donor->age);

        if(merged_stellar_type==HERTZSPRUNG_GAP)
        {

            TIMESCALESmerge_t2; // bse2 contains the merged timescales

            if(More_or_equal(bse2->GB[GB_MC_BGB],           \
                             Outermost_core_mass(donor)))
            {
                /*
                 * Rejuvenation
                 */
                donor->phase_start_mass = donor->mass;
                donor->age = bse2->tm + (bse2->timescales[T_BGB] - bse2->tm)*
                    (donor->age - bse1->tm)/(bse1->timescales[T_BGB] - bse1->tm);
            }
            else
            {
                Dprint("No rejuvenate HG donor age %g",donor->age);
            }
        }
        else if(merged_stellar_type==HeMS)
        {
            donor->phase_start_mass = donor->mass;
            TIMESCALESmerge;
            donor->age = bse1->tm *
                (fage1 * Outermost_core_mass(donor) + fage2 * mc22)/
                (Outermost_core_mass(donor) + mc22);
            Dprint("Rejuvenate HeMS donor age %g",donor->age);
        }
        else if(merged_stellar_type == CHeB ||
                Is_not_zero(mc22) ||
                merged_stellar_type != donor->stellar_type)
        {
            if(merged_stellar_type==CHeB)
            {
                const double mc1 = Outermost_core_mass(donor);
                donor->age = (fage1*mc1 + fage2*mc22)/(mc1 + mc22)
                    * bse1->tm;
                Dprint("merged CHeB new age fage1=%g fage2=%g Mc=%g,%g\n",
                       fage1,fage2,mc1,mc22);
            }
            else
            {
                Dprint("merged but not CHeB donor age %g",donor->age)
            }

#ifdef NUCSYN
            if(merged_stellar_type >= GIANT_BRANCH)
            {
                /*
                 * We have reached the giant branch by a circuitous route:
                 * disable 1st dredge up
                 */
                star1->first_dredge_up = TRUE;
                if(merged_stellar_type >= EAGB)
                {
                    /*
                     * We have reached the AGB (or later) by a circuitous route:
                     * disable 2nd dredge up
                     */
                    stardata->star[0].second_dredge_up = TRUE;
                }
            }
#endif // NUCSYN

            Dprint("Combine cores -> %s, non-zero mc2 or change of stellar type: new age %g\n",
                   Short_stellar_type_string(merged_stellar_type),
                   donor->age);

            /* merge the cores */
            corestring[0] = core_string(donor,FALSE);
            corestring[1] = core_string(accretor,FALSE);
            Dprint("Pre core merge %s + %s\n",
                   corestring[0],
                   corestring[1]);
            Safe_free(corestring[0]);
            Safe_free(corestring[1]);

            /*
             * Merge the various core types
             */
            merge_cores(stardata,
                        donor,
                        accretor,
                        donor);

            if(merged_stellar_type == CHeB)
            {
                /*
                 * BSE merges EAGB stars with CHeB
                 * stars into a new CHeB star assuming
                 * the CO core is disrupted
                 */
                donor->core_mass[CORE_CO] = 0.0;
            }
            else if((merged_stellar_type == EAGB ||
                     merged_stellar_type == TPAGB) &&
                    Is_zero(donor->core_mass[CORE_He]))
            {
                /*
                 * We have merged a helium giant with a CO core
                 * and no "helium core" with another star.
                 * Resurrect the helium core.
                 */
                donor->core_mass[CORE_He] =
                    Min(dm_intershell(donor->core_mass[CORE_CO])+
                        donor->core_mass[CORE_CO],
                        donor->mass);
            }


            corestring[0] = core_string(donor,FALSE);
            Dprint("Post core merge %s\n",
                   corestring[0]);
            Safe_free(corestring[0]);

            /*
             * Obtain a new age for the giant.
             */
            double _mc = Outermost_core_mass(donor);
            corestring[0] = core_string(donor,FALSE);
            Dprint("Pre-Giant age cores are %s mc1=%g m1=%g kw=%d &donor->phase_start_mass=%g age_donor=%g (merged stellar type %d)\n",
                   corestring[0],
                   _mc,
                   donor->mass,
                   merged_stellar_type,
                   donor->phase_start_mass,
                   donor->age,
                   merged_stellar_type);
            Safe_free(corestring[0]);

            giant_age(&_mc,
                      donor->mass,
                      &merged_stellar_type,
                      &donor->phase_start_mass,
                      &donor->age,
                      stardata,
                      star1);
            const Core_type core_id = ID_core(merged_stellar_type);
            if(core_id != CORE_NONE)
            {
                donor->core_mass[core_id] = _mc;
            }
            corestring[0] = core_string(donor,FALSE);
            Dprint("Post-Giant age star cores are %s kw=%d &donor->phase_start_mass=%g m1=%g age=%g, new stellar type %d\n",
                   corestring[0],
                   merged_stellar_type,
                   donor->phase_start_mass,
                   donor->mass,
                   donor->age,
                   merged_stellar_type);
            Safe_free(corestring[0]);
        }

        /*
         * Ensure the core type of the merged object is not reduced
         */
        stardata->star[donor->starnum].compact_core_type =
            Max(stardata->star[0].compact_core_type,
                stardata->star[1].compact_core_type);

        Dprint("Comenv call hrd t[1]=%g merged_stellar_type=%d\n",
               bse1->timescales[1],
               merged_stellar_type
            );

        /*
         * Update merged stellar structure, but
         * don't allow its stellar type to change.
         */
#ifdef NUCSYN
        donor->dont_change_stellar_type = TRUE;
#endif//NUCSYN
        donor->stellar_type = merged_stellar_type;
//        STELLAR_STRUCTUREnewtype;



        Boolean alloc[3];
        stellar_structure_BSE_alloc_arrays(donor,
                                           donor->bse,
                                           alloc);
        donor->bse->timescales[0] = FORCE_TIMESCALES_CALCULATION;
        donor->deny_SN++;
        stellar_structure(stardata,
                          STELLAR_STRUCTURE_CALLER_common_envelope_evolution,
                          donor,
                          NULL);
        donor->deny_SN--;
        bse1->tm = donor->tm;
        bse1->tn = donor->tn;
        r1 = donor->radius;
        rc1 = donor->core_radius;
        menv = donor->menv;
        moment_of_inertia_factor1 = donor->moment_of_inertia_factor;
        memcpy(bse1->timescales,
               donor->bse->timescales,
               sizeof(double)*TSCLS_ARRAY_SIZE);
        stellar_structure_BSE_free_arrays(donor,alloc);


        Dprint("post structure stellar type %d\n",
               donor->stellar_type);
#ifdef NUCSYN
        donor->dont_change_stellar_type = FALSE;
#endif
#ifdef COMENV_CO_MERGER_GIVES_IIa
        /*
         * CO - CO merger -> IIa
         *
         * In standard BSE it gives a NS, but stellar_structure is such that
         * if the star is massive enough it gives a massive ONeWD which
         * collapses to a NS. This would (and does) give you an electron
         * collapse SN. If the star is not massive enough, you get a
         * type IIa supernova (because carbon ignition is determined
         * by the total stellar mass).
         *
         * On the other hand, what if a CO+CO merger inside a common
         * envelope explodes like a Ia, but the envelope causes it
         * to look like a type II at early times: i.e. a type IIa.
         *
         * There will be no remnant in this case.
         */
        if(merged_stellar_type >= NEUTRON_STAR &&
           ((stellar_type1in == COWD || stellar_type1in == TPAGB)
            &&
            (stellar_type2in == COWD || stellar_type2in == TPAGB)))
        {
            Dprint("types in %d %d\n",
                   stellar_type1in,
                   stellar_type2in);

            merged_stellar_type = MASSLESS_REMNANT;

            /* coco_mass is the mass of the (merged) cores which explode */
            coco_mass = Outermost_core_mass(donor) + Outermost_core_mass(accretor);
            stellar_structure_make_massless_remnant(stardata,
                                                    donor);
            stellar_structure_make_massless_remnant(stardata,
                                                    accretor);
        }
#endif // COMENV_CO_MERGER_GIVES_IIa

        Dprint("third stellar_structure stellar types %d %d\n",donor->stellar_type,accretor->stellar_type);

        if(stardata->preferences->comenv_merger_spin_method ==
           COMENV_MERGER_SPIN_METHOD_CONSERVE_ANGMOM)
        {
            /*
             * Assume all angular momentum which went into the common
             * envelope merger system is kept in the systemx
             */
            donor->angular_momentum = jtot;
            Dprint("Set JSPIN1 (1) = jtot = %g\n",donor->angular_momentum);
        }
        else if(stardata->preferences->comenv_merger_spin_method ==
                COMENV_MERGER_SPIN_METHOD_CONSERVE_OMEGA)
        {
            /*
             * BSE assumes that the final spin of the envelope equals
             * that of the system just before CEE.
             * This seems unlikely, as the resulting equatorial
             * velocity is often thousands of km/s.
             */
            donor->angular_momentum =
                orbital_angular_frequency *
                (moment_of_inertia_factor1*Pow2(r1) * envelope_mass(donor)+
                 CORE_MOMENT_OF_INERTIA_FACTOR *Pow2(rc1)* Outermost_core_mass(donor));
            Dprint("Set JPSIN1 (2) = omega_orb = %g * (moment_of_inertia_factor1=%g * (r1=%g)^2 * (m1=%g - mc1=%g) + (rc1=%g)^2 * mc1=%g  =  %g\n",
                   orbital_angular_frequency,
                   moment_of_inertia_factor1,
                   r1,
                   donor->mass,
                   Outermost_core_mass(donor),
                   rc1,
                   Outermost_core_mass(donor),
                   donor->angular_momentum);
        }
        else if(stardata->preferences->comenv_merger_spin_method ==
                COMENV_MERGER_SPIN_METHOD_SPECIFIC)
        {

            /*
             * Because I don't believe it, limit the angular momentum to
             * (M1 / Min) * jtot, i.e. angular momentum is conserved during the
             * common envelope evolution, and ejected material takes this
             * specific angular momentum with it.
             */
            Dprint("Set JSPIN1 (3) = Min(%g, %g) (jtot = %g, m1=%g m1init=%g m2init=%g\n",
                   donor->angular_momentum,
                   donor->mass * jtot / (m1init + m2init),
                   donor->mass,
                   jtot,
                   m1init,
                   m2init
                );

            donor->angular_momentum = Min( donor->angular_momentum,
                                           donor->mass * jtot / (m1init + m2init));
        }
        else if(stardata->preferences->comenv_merger_spin_method ==
                COMENV_MERGER_SPIN_METHOD_BREAKUP)
        {
            /*
             * Assume material falls back down onto the merged star
             * from the ejected envelope. The merged object that hasn't
             * been ejected will be rotating at its Keplerian rate,
             * and material that accretes from a surrounding "disc"
             * will accrete at the Keplerian rate, so leave the star
             * at breakup.
             *
             * This is wrong if no material is ejected!
             */
            donor->angular_momentum = breakup_angular_momentum(stardata,
                                                               donor);
        }
        else
        {
            Exit_binary_c(BINARY_C_ALGORITHM_BRANCH_FAILURE,
                          "Unknown comenv merger spin method %d",
                          stardata->preferences->comenv_merger_spin_method);
        }
        donor->omega = donor->angular_momentum / moment_of_inertia(stardata,
                                                                   donor,
                                                                   donor->radius);
        accretor->angular_momentum = 0.0;
        /* accretor is now a remnant */
        stellar_structure_make_massless_remnant(stardata,
                                                accretor);

        if(merged_stellar_type == MASSLESS_REMNANT)
        {
            stellar_structure_make_massless_remnant(stardata,
                                                    donor);
        }
        else
        {
            donor->stellar_type = merged_stellar_type;
            donor->core_stellar_type = Core_stellar_type(merged_stellar_type);
        }

        stardata->common.orbit.eccentricity = 0.0;
    }
    else
    {

        /*
         * Set spins on comenv exit.
         *
         * First, calculate new orbital period and frequency.
         */
        orbital_period_years = final_separation/AU_IN_SOLAR_RADII*
            sqrt(final_separation/(AU_IN_SOLAR_RADII*(donor->mass + accretor->mass)));
        orbital_angular_frequency = TWOPI/orbital_period_years;

        Dprint("orbit.angular_frequency=%g from orbital_period_years=%g from final_separation=%g separation_at_core_RLOF=%g mc1=%g mc2=%g\n",
               orbital_angular_frequency,
               orbital_period_years,
               final_separation,
               separation_at_core_RLOF,
               donor->mass,
               accretor->mass);

        if(comenv_prescription == COMENV_NELEMANS_TOUT &&
           USE_NELEMANS)
        {
            /*
             * orbital angular momentum is given by Nelemans' prescription, so
             * given that, recalculate the eccentricity
             */
            Dprint("Final separation %g\n",final_separation);

            if(stardata->preferences->nelemans_recalc_eccentricity)
            {
                stardata->common.orbit.eccentricity =
                    sqrt(Max(0.0,Min(1.0,-(Pow2(stardata->common.orbit.angular_momentum / (donor->mass * accretor->mass/((donor->mass + accretor->mass)))
                                                /(Pow2(final_separation)*orbital_angular_frequency))-1.0))));
            }
            stardata->common.orbit.eccentricity = Max(stardata->preferences->comenv_post_eccentricity,
                                                      stardata->common.orbit.eccentricity);
            Dprint("Eccentricity %g\n",stardata->common.orbit.eccentricity);

            const double f = nelemans_j_factor(m1init,
                                               m2init,
                                               donor->mass,
                                               accretor->mass,
                                               stardata->preferences->nelemans_gamma,
                                               stardata);
            Dprint("Lose ang mom frac %g of %g ... ",f,stardata->common.orbit.angular_momentum);
            stardata->common.orbit.angular_momentum *= 1.0 - f;

            Dprint("now %g\n",stardata->common.orbit.angular_momentum);


        }
        else
        {
            /*
             * Check if any eccentricity remains in the orbit by first using
             * energy to circularise the orbit before removing angular momentum.
             * (note this should not be done in case of CE SN ... fix).
             */
            stardata->common.orbit.eccentricity = EorbF<Ecirc ? sqrt(1.0 - EorbF/Ecirc) : 0.0;
            stardata->common.orbit.eccentricity = Max(stardata->preferences->comenv_post_eccentricity,
                                                      stardata->common.orbit.eccentricity);

            Dprint("Jorb was %g now ",stardata->common.orbit.angular_momentum);
            stardata->common.orbit.angular_momentum =
                donor->mass * accretor->mass/(donor->mass + accretor->mass)*
                sqrt(1.0-Pow2(stardata->common.orbit.eccentricity))*Pow2(final_separation)*orbital_angular_frequency;
            Dprint("%g\n",stardata->common.orbit.angular_momentum);
        }

        /*
         * Set stellar spins
         */
        if(stardata->preferences->comenv_ejection_spin_method ==
           COMENV_EJECTION_SPIN_METHOD_DO_NOTHING)
        {
            /*
             * leave the original stellar spins and let tides do the rest later
             */
            donor->angular_momentum = ospin[0]*(moment_of_inertia_factor1*Pow2(r1)*envelope_mass(donor)+CORE_MOMENT_OF_INERTIA_FACTOR*Pow2(rc1)* Outermost_core_mass(donor));
            accretor->angular_momentum = ospin[1]*(moment_of_inertia_factor2*Pow2(r2)*envelope_mass(accretor)+CORE_MOMENT_OF_INERTIA_FACTOR*Pow2(rc2)* Outermost_core_mass(accretor));
        }
        else if(stardata->preferences->comenv_ejection_spin_method ==
                COMENV_EJECTION_SPIN_METHOD_SYNCHRONIZE)
        {
            /*
             * Tidally synchronize spins with the orbit, if there is
             * enough angular momentum to do this.
             */
            donor->omega = orbital_angular_frequency;
            donor->angular_momentum = orbital_angular_frequency*
                (moment_of_inertia_factor1*Pow2(r1)*envelope_mass(donor)+CORE_MOMENT_OF_INERTIA_FACTOR*Pow2(rc1)* Outermost_core_mass(donor));
            accretor->omega = orbital_angular_frequency;
            accretor->angular_momentum = orbital_angular_frequency*
                (moment_of_inertia_factor2*Pow2(r2)*envelope_mass(accretor)+CORE_MOMENT_OF_INERTIA_FACTOR*Pow2(rc2)* Outermost_core_mass(accretor));
        }
        else if(stardata->preferences->comenv_ejection_spin_method ==
                COMENV_EJECTION_SPIN_METHOD_SYNCHRONIZE_IF_POSSIBLE)
        {
            /*
             * as SYNCHRONIZE but only up to Keplerian rate
             */
            calculate_rotation_variables(donor,donor->radius);
            donor->omega = Min(orbital_angular_frequency,donor->omega_crit);
            donor->angular_momentum = donor->omega *
                (moment_of_inertia_factor1*Pow2(r1)*envelope_mass(donor)+CORE_MOMENT_OF_INERTIA_FACTOR*Pow2(rc1)* Outermost_core_mass(donor));
            accretor->omega = Min(orbital_angular_frequency,accretor->omega_crit);
            accretor->angular_momentum = accretor->omega *
                (moment_of_inertia_factor2*Pow2(r2)*envelope_mass(accretor)+CORE_MOMENT_OF_INERTIA_FACTOR*Pow2(rc2)* Outermost_core_mass(accretor));
        }
        else
        {
            Exit_binary_c(BINARY_C_ALGORITHM_BRANCH_FAILURE,
                          "Unknown comenv ejection spin method %d",
                          stardata->preferences->comenv_ejection_spin_method);
        }
    }


end_section:

    stardata->common.orbit.separation = final_separation;
    Dprint("Set orbital separation %g\n",stardata->common.orbit.separation);

#ifdef NUCSYN

    /*
     * So now we have the total envelope mass from m1 and m2 and the effective core
     * masses, assume this is totaly mixed to give the envelope abundances
     */

    Dprint("coalesce boolean %s ",Truefalse(stardata->model.coalesce));

    /* Consider sources for yields and possibly remaining envelope. */
    Dprint("pre-source stellar types %d %d\n",donor->stellar_type,accretor->stellar_type);

    if(stardata->model.coalesce==FALSE)
    {
        Dprint("No coalescence of the cores. All of envelope removed.\n");
        /* Easy. Remove the common envelope with the common envelope abundances. */
    }
    else
    {
        Dprint("The cores coalesce. Perhaps some envelope remains. Mcs %g %g\n",
               Outermost_core_mass(donor),
               mc22);

        /*
         * Difficult. The remaining star has an envelope which can be due
         * to: Material from the common envelope. Material from the
         * secondary star, which if it was a MS star will form the
         * envelope. So calculate the difference in mass between the final
         * system and the original system, this must be lost from the
         * common envelope. What remains, apart from the *real* cores, is
         * the new stellar envelope.
         */

        if(stellar_type2in <= MAIN_SEQUENCE)
        {
            /*
             * MS secondary, cores merge, put MS secondary in the
             * envelope
             */
            Dprint("Secondary is a MS star, but cores merge, assume mixed into common envelope\n");

            nucsyn_dilute_shell(common_envelope_mass,
                                common_envelope_abundance,
                                m2init,
                                star2->Xenv);

            common_envelope_mass+=m2init;
            Dprint("Common envelope mass now %g\n",common_envelope_mass);
            Mprint("Common envelope abunds now XH1=%g XHe4=%g XC12=%g XC13=%g XN14=%g XO16=%g\n",
                   _Abundance_list6(common_envelope_abundance));

        }
    }

    Mprint("Common envelope mass %g (mass in cores = %g, total = %g)\nCommon envelope abundance : \nXH1=%g XHe4=%g XC12=%g XC13=%g XN14=%g XO16=%g C/O=%g Total X : %g \n",
           common_envelope_mass,
           effective_mc1+effective_mc2,
           effective_mc1+effective_mc2+common_envelope_mass,
           _Abundance_list8(common_envelope_abundance)
        );



    if(Is_not_zero(stardata->preferences->degenerate_core_merger_dredgeup_fraction))
    {
        /*
         * If we have two compact cores merging, dredge-up some carbon into the envelope
         * e.g. to make R stars
         */
        Dprint("Check for dredge-up at common envelope merger: stellar types in %d %d, merger? %u\n",
               stellar_type1in,
               stellar_type2in,
               stardata->model.coalesce);

        /*
         * merge two compact cores
         */
        if(stardata->model.coalesce==TRUE &&
           stellar_type1in >= HG &&
           stellar_type2in >= HG)
        {
            /*
             * Fix dredge-up material here
             */
            Abundance merger_dredgeup_abundance[ISOTOPE_ARRAY_SIZE],xc,xo;

            // first set to the original abundance so the metals are not diluted
            Copy_abundances(common_envelope_abundance,merger_dredgeup_abundance);

            /*
             * assume that whatever happens, the merger event is hot enough to
             * burn both hydrogen and helium to carbon
             */

            // burn hydrogen (and all CNO->N14)
            nucsyn_burn_CNO_completely(merger_dredgeup_abundance);

            /*
             * burn helium to a fraction xc carbon and xo oxygen (mass fractions), N14->Ne22
             * numbers from Onno. Note if the cores are helium cores then we
             * expect them to ignite helium on collision, CO cores will just merge
             * and already have these sort of abundances.
             */
            //xc=0.53; xo=0.42; // from onno for in the core
            xc=0.7; xo=0.15; // from onno for at the surface of the core
            //xc=0.8; xo=0.2; // even more optimistic

            merger_dredgeup_abundance[XC12]+=xc*merger_dredgeup_abundance[XHe4];
            merger_dredgeup_abundance[XO16]+=xo*merger_dredgeup_abundance[XHe4];
            merger_dredgeup_abundance[XHe4]*=(1.0-xc-xo);
            merger_dredgeup_abundance[XNe22]+=merger_dredgeup_abundance[XN14];
            merger_dredgeup_abundance[XN14]=0.0;

            Dprint("Common envelope merger mixing:\n");
            Mprint("Comenv abundance: H1=%g He4=%g C12=%g N14=%g O16=%g\n",
                   _Abundance_list5(common_envelope_abundance));
            Mprint("Dredge-up abundance: H1=%g He4=%g C12=%g N14=%g O16=%g\n",
                   _Abundance_list5(merger_dredgeup_abundance));
            Mprint("Mix %g solar masses from the combined cores (%g) into the %g solar masses of comenv\n",
                   stardata->preferences->degenerate_core_merger_dredgeup_fraction *
                   Outermost_core_mass(donor),
                   Outermost_core_mass(donor),common_envelope_mass);


            /*
             * mix dm_dredge =
             * (degenerate_core_merger_dredgeup_fraction * mc1)
             * of material into the common envelope
             */
            const double dm_dredge =
                Min(common_envelope_mass,
                    stardata->preferences->degenerate_core_merger_dredgeup_fraction *
                    Outermost_core_mass(donor));

            nucsyn_dilute_shell(dm_dredge,
                                merger_dredgeup_abundance,
                                common_envelope_mass,
                                common_envelope_abundance);

            Mprint("Post merger abundance: H1=%g He4=%g C12=%g N14=%g O16=%g\n",
                   _Abundance_list5(common_envelope_abundance));

            Mprint("Post-DUP C/O (by number) = %g\n",
                   ((common_envelope_abundance[XC12]/12.0+
                     common_envelope_abundance[XC13]/13.0)
                    /
                    (common_envelope_abundance[XO16]/16.0+
                     common_envelope_abundance[XO17]/17.0+
                     common_envelope_abundance[XO18]/18.0)));

        }
    }
#endif // NUCSYN


    /* calculate yields */

    /*
     * First, calculate mass lost from the common envelope, which might be more
     * that the mass of your original guess, e.g. in the case of a TZ object.
     *
     * dm_RLOF_lose and dM2 are positive numbers.
     */
    dM1 = m1init - donor->mass;
    dM2 = m2init - accretor->mass;

    /* calculate total mass lost */
    dm = dM1 + dM2;

#ifdef NUCSYN
    Dprint("Calc dm from Min %g %g(=%g+%g)\n",common_envelope_mass,dM1+dM2,dM1,dM2);
    Mprint("CALC dms: dM1=%g-%g=%g dM2=%g-%g=%g\n",
           m1init,donor->mass,dM1,m2init,accretor->mass,dM2);
    Mprint("Yield mass %g from the common envelope (%2.2f %%)\n",
           dm,100.0*dm/common_envelope_mass);

#endif //NUCSYN
    double dm_yield=dm; // save the yield
    /*
     * check for supernovae
     *
     * Always check star 1 but in the case
     * of a merger ignore star 2 (which is a
     * MASSLESS_REMNANT)
     *
     * If check_for_supernova returns TRUE then
     * material yielded in the check_for_supernova
     * function is not yielded by a call to calc_yields.
     *
     */
    Dprint("Nucsyn check merger\n");
    if(stardata->model.coalesce==TRUE)
    {

        /* Merged star: might be NS,BH,MASSLESS? */
        if(tzobject==TRUE)
        {
#ifdef NUCSYN
            /*
             * donor->starnum is a Thorne-Zytkow object :
             * set the surface abundances appropriately
             * prior to (envelope ejection
             */
            nucsyn_TZ_surface_abundances(star1);
            Copy_abundances(star1->Xenv,
                            common_envelope_abundance);
            comenv_source = SOURCE_THORNE_ZYTKOW;
#endif//NUCSYN
        }
        else
        {
#ifdef COMENV_CO_MERGER_GIVES_IIa
            if(coco_mass>TINY)
            {
                /* temporarily set the core mass to the CO+CO core mass */
                set_no_core(star1);
                star1->core_mass[CORE_CO] = coco_mass;
            }
#endif//COMENV_CO_MERGER_GIVES_IIa
        }

#ifdef NUCSYN_STRIP_AND_MIX
        /*
         * make sure star1 contains the merged star,
         * star2 is a MASSLESS_REMNANT, prior to applying
         * nucsyn_mix_stars
         */
        Boolean swap = FALSE;
        if(stardata->star[0].stellar_type==MASSLESS_REMNANT)
        {
            stardata->star[0].mass = 0.0;
            set_no_core(stardata->star[0]);
            Swap_stars(&stardata->star[0],&stardata->star[1]);
            stardata->star[0].starnum=0;
            stardata->star[1].starnum=1;
            star1=&stardata->star[0];
            star2=&stardata->star[1];
            swap = TRUE;
        }

        nucsyn_mix_stars(stardata,dm,star1->mass);
        star2->strip_and_mix_disabled = TRUE;
        star1->strip_and_mix_disabled = !can_use_strip_and_mix(stardata,star1);

        /* and swap back */
        if(swap == TRUE)
        {
            Swap_stars(&stardata->star[0],&stardata->star[1]);
            stardata->star[0].starnum=0;
            stardata->star[1].starnum=1;
            star1=&stardata->star[0];
            star2=&stardata->star[1];
        }
#endif//NUCSYN_STRIP_AND_MIX
    }


#ifdef NUCSYN_STRIP_AND_MIX
    star1->strip_and_mix_disabled = !can_use_strip_and_mix(stardata,star1);
    star2->strip_and_mix_disabled = !can_use_strip_and_mix(stardata,star2);
#endif//NUCSYN_STRIP_AND_MIX

    dm = dm_yield;

#if defined DISCS && defined DISCS_CIRCUMBINARY_FROM_COMENV
    Dprint("Make new CBdisc\n");
    /*
     * Check that the stars do not merge and mass is ejected
     * which can form a disc.
     */
    if(stardata->model.coalesce==FALSE && dm>0.0)
    {
        /*
         * Leave a fraction of the mass and angular momentum
         * that is put into the common envelope in a circumbinary disc.
         *
         * f_M is the fraction of the common envelope ejected mass
         *     put into the disc
         */
        double mass_ejected = dm * M_SUN;
        double f_M = stardata->preferences->comenv_disc_mass_fraction;
        Clamp(f_M,0.0,1.0);

        /*
         * f_J is the fraction of the common envelope angular momentum
         *     put into the disc. This is set as an option, unless it is
         *     negative in which case an algorithm is used.
         *
         * f_J == -1 == CBDISC_ANGMOM_FROM_MOMENTS_OF_INERTIA
         *
         *     The mass fraction f_M is in a disc with moment of inertia
         *           (1/2) * f_M * r^2
         *
         *     The rest (1-f_M) is in a sphere with moment of inertia
         *           (2/5) * (1-f_M) * r^2
         *
         *     and the material is all co-rotating at every radius
         *     then the fraction of angular momentum in the disc is
         *
         *        5 f_M /(4+f_M) ~ f_M
         *
         *     Conversely, if the material from the common envelope is
         *     ejected as a disc, then the fraction of angular momentum
         *     in the remaining disc is just f_M.
         *
         *     The difference is small.
         *
         * f_J == -2 == CBDISC_ANGMOM_FROM_COMENV_SPECIFIC_ANGMOM
         *
         *     Conserve specific angular momentum, i.e. the disc has the
         *     same specific angular momentum as the ejected envelope.
         *     In this case, f_J = f_M.
         *
         * f_J == -3 == CBDISC_ANGMOM_FROM_POSTCE_L2
         *
         *     Use the specific orbital angular momentum at L2
         *     and give this to all the mass in the disc.
         *
         * f_J == -4 == CBDISC_ANGMOM_FROM_PRECE_RADIUS
         *
         *     Use the pre-CE radius and angular velocity to
         *     set the specific angular momentum of the disc.
         */

        /*
         * Calculate angular momentum ejected in the envelope
         * by conserving it then subtracting the final orbit and
         * stellar spins.
         *
         * NB units of Jorb_new, Jspins_new and J_ejected are cgs.
         */

        double Jspins_new = Angular_momentum_code_to_cgs( donor->angular_momentum + accretor->angular_momentum );

        double Jorb_new = donor->mass*M_SUN * accretor->mass*M_SUN *
            sqrt(GRAVITATIONAL_CONSTANT*
                 (1.0 - Pow2(stardata->common.orbit.eccentricity)) *
                 stardata->common.orbit.separation * R_SUN /(M_SUN * ( donor->mass + accretor->mass )));

        /*
         * Angular momentum that is ejected
         */
        double J_ejected = jtot_cgs - Jorb_new - Jspins_new;
        stardata->model.previous_common_envelope_angmom = J_ejected;

        /*
         * Of which we take a fraction f_J and put this into
         * a disc.
         */
        double f_J;

        if(Fequal(stardata->preferences->comenv_disc_angmom_fraction,
                  (double)CBDISC_ANGMOM_FROM_POSTCE_L2))
        {
            /*
             * Take the angular momentum from post-CE L2
             * Hence Jdisc = Mdisc * omega_binary * RL2^2
             * where omega_binary and RL2 are calculated after
             * common envelope ejection.
             */
            struct coordinate_t L[NUMBER_OF_LAGRANGE_POINTS];
            lagrange_points(donor->mass,
                            accretor->mass,
                            stardata->common.orbit.separation*R_SUN,
                            L,
                            NULL,
                            NULL);

            /* L2 radius */
            double RL2 = Pythag3_coord(L[1]);
            orbital_period_years = final_separation/AU_IN_SOLAR_RADII*
                sqrt(final_separation/(AU_IN_SOLAR_RADII*(donor->mass + accretor->mass)));
            orbital_angular_frequency = TWOPI/orbital_period_years; // rad/y
            double omega_binary = orbital_angular_frequency /
                YEAR_LENGTH_IN_SECONDS; // rad/s

            if(DISC_DEBUG)
                printf("Disc angmom from L2 : Porb = %g years : omega = %g s^-1 : omega_binary = %g s^-1\n",
                       orbital_period_years,
                       orbital_angular_frequency,
                       omega_binary);

            f_J = f_M * mass_ejected / J_ejected * omega_binary * Pow2(RL2) ;
        }
        else if(Fequal(stardata->preferences->comenv_disc_angmom_fraction,
                       (double)CBDISC_ANGMOM_FROM_PRECE_RADIUS))
        {
            /*
             * Use the pre-CE radius and pre-CE angular velocity to
             * set the specific angular momentum of the disc
             */
            double omega_binary = initial_orbital_angular_frequency /
                YEAR_LENGTH_IN_SECONDS;
            f_J = f_M * mass_ejected / J_ejected * omega_binary * Pow2(comenv_radius*R_SUN);
        }
        else if(Fequal(stardata->preferences->comenv_disc_angmom_fraction,
                       (double)CBDISC_ANGMOM_FROM_MOMENTS_OF_INERTIA))
        {
            f_J = 5.0 * f_M / ( 4.0 + f_M );
        }
        else if(Fequal(stardata->preferences->comenv_disc_angmom_fraction,
                       (double)CBDISC_ANGMOM_FROM_COMENV_SPECIFIC_ANGMOM))
        {
            f_J = f_M;
        }
        else if(More_or_equal(
                    stardata->preferences->comenv_disc_angmom_fraction,
                    0.0))
        {
            f_J = stardata->preferences->comenv_disc_angmom_fraction;
        }
        else
        {
            /*
             * Unknown algorithm
             */
            f_J = 0.0;
            Exit_binary_c(BINARY_C_ALGORITHM_BRANCH_FAILURE,
                          "Unknown comenv_disc_angmom_fraction algorithm (is %g). Can be >=0 or %d, %d, %d or %d.\n",
                          stardata->preferences->comenv_disc_angmom_fraction,
                          CBDISC_ANGMOM_FROM_MOMENTS_OF_INERTIA,
                          CBDISC_ANGMOM_FROM_COMENV_SPECIFIC_ANGMOM,
                          CBDISC_ANGMOM_FROM_POSTCE_L2,
                          CBDISC_ANGMOM_FROM_PRECE_RADIUS
                );
        }

        f_J = Max(0.0,f_J);

        if(0)
            printf("NEWDISC fm=%g fj=%g jej=%g mej=%g\n",f_M,f_J,J_ejected,mass_ejected);
        Boolean make_new_disc =
            Boolean_(Is_really_not_zero(f_M) &&
                     Is_really_not_zero(f_J) &&
                     J_ejected > 0.0 &&
                     mass_ejected > 0.0
                );

        if(DISC_DEBUG || DEBUG)
        {
            Dprint("NEW DISC? (f_M=%g, f_J=%g, mass ejected %g, J ejected %g) -> %s\n",
                   f_M,
                   f_J,
                   mass_ejected,
                   J_ejected,
                   Yes_or_no(make_new_disc)
                );
        }

        if(make_new_disc == TRUE)
        {

#ifdef __HAVE_VALGRIND__
            /*
             * Discs can greatly slow evolution, and when
             * running with valgrind cause timeouts.
             * Reset the timeout here.
             */
            if(RUNNING_ON_VALGRIND)
            {
                reset_binary_c_timeout();
            }
#endif // __HAVE_VALGRIND__

            cbdisc = new_disc(stardata,
                              DISC_CIRCUMBINARY,
                              stardata,
                              DISC_OUTER_EDGE);
            double discM = mass_ejected * f_M;
            double discJ = J_ejected    * f_J;

            Append_logstring(LOG_COMENV_PRE,
                             "Try disc with M=%g (f_M %g) J=%g (f_J %g) ",
                             discM,
                             f_M,
                             discJ,
                             f_J);
            /*
             * If cbdisc is NULL, we already have a
             * circumbinary disc.
             */
            if(cbdisc != NULL)
            {
                Dprint("New cbdisc at %p\n",(void*)cbdisc);
                cbdisc->M = discM;
                cbdisc->Rin = 0.0; // auto set by inner binary
                cbdisc->Rout = 0.0; // auto set from J
                cbdisc->first = TRUE;
                cbdisc->append = FALSE;

                /*
                 * The stellar type should be that of the
                 * brighter of the two stars. This is a bit ad-hoc!
                 */
                cbdisc->overflower_stellar_type = donor->stellar_type;
                cbdisc->overflower_starnum = donor->starnum;
                Dprint("DONOR TYPE %d (stellar type in %d %d)\n",
                       cbdisc->overflower_stellar_type,
                       stellar_type1in,
                       stellar_type2in);

#ifdef NUCSYN
                /*
                 * Set disc abundances
                 */
                Copy_abundances(common_envelope_abundance,
                                cbdisc->X);
#endif//NUCSYN

                /*
                 * Angular momentum in the disc is
                 * Initial Angmom - Angmom left in stars
                 */
                cbdisc->J = discJ;

                /*
                  printf("Disc start :\n  Disc mass M = %g Msun = %g g\n  binary separation a= %g Rsun, %g cm\n  Disc radii: Rin = %g Rsun = %g cm, Rout = %g Rsun = %g cm\n  Stars: M1=%g M2=%g Msun, P=%g days, A=%g Rsun, L1+L2=%g, R1=%g R2=%g Rsun\n  Disc angular momentum Jdisc=%g (%g %%)\n\n",
                  cbdisc->M/M_SUN,
                  cbdisc->M,
                  stardata->common.orbit.separation,
                  stardata->common.orbit.separation*R_SUN,
                  cbdisc->Rin,
                  cbdisc->Rin*R_SUN,
                  cbdisc->Rout,
                  cbdisc->Rout*R_SUN,
                  donor->mass,
                  accretor->mass,
                  365.65*(final_separation/AU_IN_SOLAR_RADII)*sqrt(final_separation/(AU_IN_SOLAR_RADII*(donor->mass + accretor->mass))),
                  stardata->common.orbit.separation,
                  l1+l2,
                  stardata->star[0].radius,
                  stardata->star[1].radius,
                  cbdisc->J,
                  cbdisc->J*100.0/jtot_cgs
                  );
                  printf("cbdisc alpha = %g\n",cbdisc->alpha);
                */

                Dprint("2a");
                Append_logstring(LOG_DISC,
                                 "0New CB disc: M=%g Msun J = %g = %g Msun km/s AU (%g and %g %% of CE, Jorb = %g; Lorb = %g, Ldisc = %g km/s AU) ",
                                 cbdisc->M/M_SUN,
                                 cbdisc->J,
                                 cbdisc->J / (M_SUN * 1e5 * ASTRONOMICAL_UNIT),
                                 f_M*100.0,
                                 f_J*100.0,
                                 jorb_cgs / (M_SUN * 1e5 * ASTRONOMICAL_UNIT),
                                 jorb_cgs / (M_SUN * 1e5 * ASTRONOMICAL_UNIT) / (M_SUN*(m1init + m2init)),
                                 cbdisc->J / (M_SUN * 1e5 * ASTRONOMICAL_UNIT) / cbdisc->M

                    );
                Dprint("2");
                if(stardata->preferences->disc_legacy_logging == TRUE)
                {
                    Printf("DISC_COMENV %d\n",
                           (stardata->star[0].luminosity > stardata->star[1].luminosity  ?
                            stardata->star[0].stellar_type :
                            stardata->star[1].stellar_type));
                }
            }
            else
            {
#ifdef DISCS_COMENV_APPEND
                /*
                 * We already have a disc, so feed it
                 * with more material
                 */

                Dprint("We already have a circumbinary disc\n");
                cbdisc = &stardata->common.discs[stardata->common.ndiscs-1];
                cbdisc->overflower_stellar_type = donor->stellar_type;

#ifdef NUCSYN
                nucsyn_dilute_shell(cbdisc->M,
                                    cbdisc->X,
                                    discM,
                                    common_envelope_abundance);
#endif//NUCSYN
                cbdisc->M += discM;
                cbdisc->J += discJ;
                /*
                 * Set disc->append to TRUE to allow the disc to
                 * restabilize itself after mass is appended
                 */
                cbdisc->first = FALSE;
                cbdisc->append = TRUE;
#if defined MEMOIZE && defined DISCS_MEMOIZE
                if(cbdisc->memo)
                {
                    Dprint("call memoize_free on appended cbdisc\n");
                    memoize_free(&cbdisc->memo);
                }
#endif // MEMOIZE && DISCS_MEMOIZE
                Set_logstring(LOG_DISC,
                              "0Append CB disc: age=%g y, M+=%g=%g, Msun, J+=%g=%g (+=%g and +=%g %% of CE) ",
                              cbdisc->lifetime/YEAR_LENGTH_IN_SECONDS,
                              discM/M_SUN,
                              cbdisc->M/M_SUN,
                              discJ,
                              cbdisc->J,
                              f_M,
                              f_J);
                Dprint("Append CB disc: age=%g y, M+=%g=%g Msun, J+=%g=%g (+=%g and +=%g %% of CE)\n",
                       cbdisc->lifetime/YEAR_LENGTH_IN_SECONDS,
                       discM/M_SUN,
                       cbdisc->M/M_SUN,
                       discJ,
                       cbdisc->J,
                       f_M,
                       f_J);
#endif//DISCS_COMENV_APPEND
            }
        }
    }
    Dprint("Done disc setup\n");

#endif //DISCS

    Dprint("Do Yields?");

    /* Put all the (remaining) yields in star 1 */
    if(dm>TINY)
    {
        calc_yields(stardata,
                    star1,
                    dm,
#ifdef NUCSYN
                    common_envelope_abundance,
#endif // NUCSYN
                    0.0,
#ifdef NUCSYN
                    NULL,
#endif // NUCSYN
                    donor->starnum,
                    YIELD_FINAL,
                    comenv_source);
    }

#ifdef NUCSYN

    Dprint("Total mass ejected during common envelope phase : %g (+stellar masses = %g)\n",
           dM1+dM2,+dM1+dM2+donor->mass+accretor->mass);

    /* Now we need to set the new envelope abundances if the cores have merged */
    if(stardata->model.coalesce==TRUE)
    {
        /*
         * calculate the mass of common envelope that remains, dM1 ... this
         * is the original mass of common envelope minus the mass lost to
         * space
         */
        dM1 = common_envelope_mass-dm;

        /* calculate the mass of the new stellar envelope, dM2 */
        dM2 = envelope_mass(donor);

        MDEBUG_PRETTY_MERGER;

        /*
         * If there is any envelope, it must be from the common envelope,
         * otherwise leave the stellar abundances appropriate for the
         * stellar type (set in stellar_structure)
         */
        if(Is_not_zero(dM2))
        {
            /* there is some common envelope remaining */
            Copy_abundances(common_envelope_abundance,
                            star1->Xenv);
        }

    }
#ifdef MIXDEBUG
    else
    {
        MDEBUG_PRETTY_NOT_MERGER;
    }
#endif//MIXDEBUG

    /*
     * if the new star is a MS star reset its Xinit array
     * Note this applies if it is a new HeMS star!
     */
    Foreach_star(star)
    {
        if(ON_EITHER_MAIN_SEQUENCE(star->stellar_type))
        {
            if(star->stellar_type < HERTZSPRUNG_GAP)
            {
                Copy_abundances(star->Xenv,
                                star->Xinit);
            }
            /* reset ZAMS mass as well */
            star->effective_zams_mass = star->mass;
        }
    }

#ifdef NUCSYN_YIELDS
    /*
     * Do yields for common envelope evolution - dm is the change in mass of
     * each star, but this can be +ve or -ve.
     * dM1 + dM2 is the total change in
     * mass of the stars, so assume half this is lost from each star
     */

#ifdef MIXDEBUG
    Dprint("COMENVYIELD m01=%g m1=%g dM1=%g m02=%g m2=%g dM2=%g\n",m1init,donor->mass,dM1,m2init,accretor->mass,dM2);

#endif /* MIXDEBUG */

#endif /* NUCSYN_YIELDS */

    /*
     * If either star is a remnant (WD,NS,BH,MASSLESS_REMNANT), make sure its
     * abundances are set (just in case stellar_structure hasn't been called for it)
     */
    Starloop(kk)
    {
        if(WHITE_DWARF(stardata->star[kk].stellar_type))
        {
            nucsyn_set_WD_abunds(stardata,
                                 stardata->star[kk].Xenv,
                                 stardata->star[kk].stellar_type);
        }
        else if(stardata->star[kk].stellar_type>=NS)
        {
            nucsyn_set_remnant_abunds(stardata->star[kk].Xenv);
        }
    }

    MDEBUG_CALC_MSUM;
    Mix_debug_in_out("Exit");

#endif /* NUCSYN */

    stardata->model.intpol=0;

#ifdef NUCSYN
#ifdef NUCSYN_PLANETARY_NEBULAE
    if(Is_not_zero(dM1+dM2))
    {
        nucsyn_planetary_nebulae(stardata,
                                 PNE_COMENV,
                                 1,
                                 -(donor->mass+accretor->mass)+(m1init+m2init),
                                 common_envelope_abundance,
                                 // exp. velocity
                                 // factor 1e-5 to convert to km/s

                                 // escape velocity - better but still high!
                                 1e-5*sqrt(GRAVITATIONAL_CONSTANT*M_SUN*(m1init+m2init)/(comenv_radius*R_SUN))
                                 // Vexp from EbindI
                                 //1e-5*sqrt(2.0*EbindI*(M_SUN*M_SUN*GRAVITATIONAL_CONSTANT/R_SUN)/(M_SUN*common_envelope_mass))

            );
    }
#endif // NUCSYN_PLANETARY_NEBULAE
#endif //NUCSYN

#ifdef MIXDEBUG
    Dprint("end stellar types %d %d\n",donor->stellar_type,accretor->stellar_type);
#endif // MIXDEBUG

#ifdef DETMERS_LOG
    /*
     * We want NS/BH - Hestar post-comenv binaries.
     * Note: period is in HOURS
     */
    double perhours=24.0*365.65*(final_separation/AU_IN_SOLAR_RADII)*sqrt(final_separation/(AU_IN_SOLAR_RADII*(donor->mass + accretor->mass)));

    if(stardata->model.coalesce == FALSE &&
       ((GRB_PROGENITOR(donor->stellar_type) && NAKED_HELIUM_STAR(accretor->stellar_type))||
        (GRB_PROGENITOR(accretor->stellar_type) && NAKED_HELIUM_STAR(donor->stellar_type)))
        )
    {
        Dprint("DETMERS %g %g %g %g\n",donor->mass,accretor->mass,perhours,stardata->model.probability);
    }
#endif //  DETMERS_LOG

#ifdef LOG_COMENV_RSTARS
    if(stardata->model.coalesce==TRUE)
    {
        /*
         * Here we set the R star flag to be logged elsewhere
         */
        struct star_t *star;

        /* choose the R star (or R star to be) */
        star = star1;

        /*
         * Three types of He star can merge with the GB star:
         * a normal helium star, a core-helium burning star, a helium WD.
         *
         * We assume these are progenitors for the R-stars
         */

        /* R star can be either GB or CHeB star */
        if(((stellar_type==CHeB)||(stellar_type==GIANT_BRANCH))
           /* secondary must have a helium (or later) core */
           &&(stellar_type1in>=HG)&&(stellar_type2in>=HG))
        {
            star->r_star_progenitor=TRUE;
        }

        /* save pre-comenv data */
        star->r_star_precomenv_stellar_type1=stellar_type1in;
        star->r_star_precomenv_stellar_type2=stellar_type2in;
        star->r_star_precomenv_m1=m1init;
        star->r_star_precomenv_m2=m2init;
        star->r_star_precomenv_mc1=mc1init;
        star->r_star_precomenv_mc2=mc2init;
        star->r_star_precomenv_l1=l1init;
        star->r_star_precomenv_l2=l2init;
        star->r_star_prejorb=jorb;
        star->r_star_prejtot=jtot;
        star->r_star_postcomenv_m=donor->mass;
        star->r_star_postcomenv_mc=Outermost_core_mass(donor);
        star->r_star_postcomenv_l=donor->luminosity;

        /* save some post-comenv stellar type data */
        star->r_star_postcomenv_stellar_type1 = donor->stellar_type;
        star->r_star_postcomenv_stellar_type2 = accretor->stellar_type;


        /* calulate post-merger critical velocity in km/s */
        star->r_star_postvcrit=1e-5*sqrt(GRAVITATIONAL_CONSTANT*(donor->mass)*M_SUN/(r1*R_SUN)); // km/s


        /* conserve angular momentum to estimate the spin of the
         * merged star */
        star->r_star_postvrot=jtot_cgs/((moment_of_inertia_factor1*envelope_mas(donor)*r1
                                         +CORE_MOMENT_OF_INERTIA_FACTOR*(Outermost_core_mass(donor)))
                                        *M_SUN*R_SUN) * 1e-5;


        /* Vrot2 */
        /* calculate with core angular momentum conserved in the cores */
        jtot_cgs=Max(0.0,jtot_cgs-djcores); // should never be <0!
        star->r_star_postvrot2=jtot_cgs/((moment_of_inertia_factor1*envelope_mass(donor)*r1
                                          +CORE_MOMENT_OF_INERTIA_FACTOR*(Outermost_core_mass(donor)))
                                         *M_SUN*R_SUN) * 1e-5;

        /* Vrot3 */
        /* and lose angular momentum in mass lost? */
        double dm=-(donor->mass-m1init-m2init);
        double dJ=dm*star->r_star_postvcrit*r1*(1e5*M_SUN*R_SUN);
        jtot_cgs=Max(0.0,jtot_cgs-dJ);//sometimes < 0!
        star->r_star_postvrot3=jtot_cgs/((moment_of_inertia_factor1*envelope_mass(donor)*r1
                                          +CORE_MOMENT_OF_INERTIA_FACTOR*(Outermost_core_mass(donor)))
                                         *M_SUN*R_SUN) * 1e-5;

        /* save the final spin rate (but is this used any more?) */
        star->r_star_postomega=star->r_star_postvrot/(r1*R_SUN);

        star->r_star_postjspin=donor->angular_momentum;

        /* store the time of the common envelope */
        star->r_star_comenv_time=stardata->model.time;
    }
#endif // LOG_COMENV_RSTARS

#ifdef EXTRA_IA_LOGGING
    /* calculate grav wave radiation timescale (Myr)
     * (see Greggio 2005 for details)
     */
    const double tgw = gravitational_radiation_timescale(stardata->common.orbit.separation,
                                                         donor->mass,
                                                         accretor->mass);

    Dprint("Post-CE %d sep=%g tgw=%g finalm1=%g finalm2=%g\n",
           stardata->model.comenv_count,
           stardata->common.orbit.separation,
           tgw,
           Max(donor->mass,accretor->mass),
           Min(donor->mass,accretor->mass));
#endif //EXTRA_IA_LOGGING


#ifdef COMENV_LOG
    /*
     * Log RGB stars which go on to evolve, find out their masses
     */
    if(stellar_type1in==GIANT_BRANCH && stardata->star[0].stellar_type<10)
    {
        printf("CEEGB %g %g %d %d\n",
               m1init,
               stardata->star[0].mass,
               stardata->star[0].stellar_type,
               stardata->model.coalesce);
    }
#endif // COMENV_LOG

    {
        Abundance xenv,xCE;
#ifdef NUCSYN
        xenv = donor->Xenv[XH1];
        xCE = common_envelope_abundance[XH1];
#else
        xenv = -1.0;
        xCE = -1.0;
#endif//NUCSYN

        char * cores1 = core_string(&stardata->star[0],FALSE);
        char * cores2 = core_string(&stardata->star[1],FALSE);
        Dprint("COMENV OUT kw=%d Xdonor=%g Xcomenv=%g  : %d, age=%g %g, jorb=%g, a=%g P=%g (M1=%g %s M2=%g %s m1=%g m01=%g jspin1=%g SN %d) R=%g %g\n",
               donor->stellar_type,
               xenv,
               xCE,
               accretor->stellar_type,
               donor->age,accretor->age,
               stardata->common.orbit.angular_momentum,
               stardata->common.orbit.separation,
               stardata->common.orbit.period,
               stardata->star[0].mass,
               cores1,
               stardata->star[1].mass,
               cores2,
               donor->mass,donor->phase_start_mass,
               donor->angular_momentum,
               donor->SN_type,
               r1,r2
            );
        Safe_free(cores1);
        Safe_free(cores2);
    }
/*
 * Check for CE occurence while primary evolves from AGB
 * to WD and secondary stays on MS
 */
#ifdef DENISE_LOG
    if((stardata->star[0].stellar_type>4) &&
       (stardata->star[0].stellar_type<13) &&
       (stardata->star[1].stellar_type<2))
    {
        //stardata->common.PN_CE = TRUE;
    }
    else
    {
        //stardata->common.PN_CE = FALSE;
    }
#endif // DENISE_LOG


    if(stardata->model.coalesce == TRUE)
    {
        double Eejecta = (EbindF - (EorbI + EbindI)) * CGS_ENERGY;
        double dE = EorbF - EorbI;
        Append_logstring(LOG_COMENV_PRE,
                         ": Mf=%g(%g,%d) EbindF=%g Eej=%g alpha_ej=%g",
                         donor->mass,
                         Outermost_core_mass(donor),
                         donor->stellar_type,
                         EbindF * CGS_ENERGY,
                         /*
                          * The energy in the ejected material is
                          * the difference in energy between the start
                          * and end of CEE. NB EorbF=0 because of the
                          * merger.
                          */
                         Eejecta,
                         Is_zero(dE) ? 0.0 : (-Eejecta/(CGS_ENERGY*dE))
            );


    }
    else
    {
        double Eejecta = common_envelope_mass * M_SUN * 0.5 * Pow2(vescape);
        double dE = EorbF - EorbI;
        Append_logstring(LOG_COMENV_PRE,
                         ": Mf1=%g(%g,%d) Mf2=%g(%g,%d) af=%g Jf=%g Jej=%g Eorbf=%g dE=%g Eej=%g alpha_ej=%g",
                         donor->mass,
                         Outermost_core_mass(donor),
                         donor->stellar_type,
                         accretor->mass,
                         Outermost_core_mass(accretor),
                         accretor->stellar_type,
                         stardata->common.orbit.separation,
                         Angular_momentum_code_to_cgs(stardata->common.orbit.angular_momentum),

                         jtot_cgs - Angular_momentum_code_to_cgs(stardata->common.orbit.angular_momentum-
                                                                 donor->angular_momentum + accretor->angular_momentum),

                         (donor->mass * accretor->mass)/(2.0* stardata->common.orbit.separation)*CGS_ENERGY,
                         /*
                          * The envelope is unbound,
                          * and has gained energy -EbindI.
                          */
                         -EbindI*CGS_ENERGY,
                         /*
                          * Hence calculate the ejecta KE from the
                          * escape velocity of the original envelope
                          */
                         Eejecta,
                         Is_zero(dE) ? 0.0 : (-Eejecta/(CGS_ENERGY*dE))
            );
    }


    /*
     * Systematic derivatives are likely totally wrong at this point
     */
    zero_derivatives(stardata);

    /*
     * Update rotation and orbital variables
     */
    Starloop(kk)
    {
        calculate_rotation_variables(&stardata->star[kk],
                                     stardata->star[kk].radius);
    }
    determine_mass_ratios(stardata);
    update_orbital_variables(stardata,
                             &stardata->common.orbit,
                             &stardata->star[0],
                             &stardata->star[1]);
    update_binary_star_variables(stardata,NULL);
    comenv_calc_roche_lobe_radii(final_separation,
                                 stardata->preferences->comenv_post_eccentricity,
                                 donor->mass,
                                 accretor->mass,
                                 &rl1,
                                 &rl2);

#if defined DISCS && defined DISCS_CIRCUMBINARY_FROM_COMENV
    /*
     * Start any disc evolution
     */
    if(cbdisc!=NULL)
    {
        /*
         * Converge on first disc structure if possible
         */
        struct stardata_t * tmp_stardata = New_stardata_from(stardata);
        tmp_stardata->star[0].radius = donor->radius;
        tmp_stardata->star[1].radius = accretor->radius;
        tmp_stardata->star[0].phase_start_mass = donor->phase_start_mass;
        tmp_stardata->star[1].phase_start_mass = accretor->phase_start_mass;
        tmp_stardata->star[0].mass = donor->mass;
        tmp_stardata->star[1].mass = accretor->mass;
        Copy_core_masses(donor,&tmp_stardata->star[0]);
        Copy_core_masses(accretor,&tmp_stardata->star[1]);
        tmp_stardata->star[0].stellar_type = donor->stellar_type;
        tmp_stardata->star[1].stellar_type = accretor->stellar_type;
        tmp_stardata->star[0].luminosity = donor->luminosity;
        tmp_stardata->star[1].luminosity = accretor->luminosity;

        disc_initialize_disc(tmp_stardata,cbdisc,NULL);

        /*
         * If the disc mass is zero, no stable disc is possible.
         * Log this and stop evolution if required.
         */
        if(Is_zero(cbdisc->M))
        {
            Append_logstring(LOG_DISC,
                             " initial stable disc not possible :(");

            if(stardata->preferences->cbdisc_end_evolution_after_disc == TRUE)
            {
                stardata->model.evolution_stop = TRUE;
            }
        }
        free_stardata(&tmp_stardata);
    }
#endif // DISCS && DISCS_CIRCUMBINARY_FROM_COMENV

    const Boolean m = Is_zero(stardata->common.orbit.separation);

    /* time to merge in Myr */
    const double dtmerge = Peters_grav_wave_merger_time(
        stardata,
        stardata->star[0].mass,
        stardata->star[1].mass,
        stardata->common.orbit.separation,
        stardata->common.orbit.eccentricity)*1e-6;

    corestring[0] = core_string(&stardata->star[0],FALSE);
    corestring[1] = core_string(&stardata->star[1],FALSE);
    char * merger_info;
    Boolean merger_info_alloced;
    if(dtmerge < 1e40)
    {
        if(asprintf(&merger_info,
                    "merge in %g Gyr at t = %g Gyr",
                    dtmerge*1e-3,
                    (stardata->model.time + dtmerge) * 1e-3)==-1)
        {
            merger_info = "no merger info";
            merger_info_alloced = FALSE;
        }
        else
        {
            merger_info_alloced = TRUE;
        }
    }
    else
    {
        merger_info = "never merges";
        merger_info_alloced = FALSE;
    }

    Append_logstring(LOG_COMENV_POST,
                     "post-COMENV M=%g(%s;%s;Menv=%g,Mstart=%g,Teff=%g,age=%g) + M=%g(%s;%s;Menv=%g,Mstart=%g,Teff=%g,age=%g) a_out=%g P_out=%g (%g d) Jorb_out=%g R1=%g RL1=%g (R1/RL1 = %g) R2=%g RL2=%g (R2/RL2 = %g) SN? %d %d Single? %s : %s, events pending? %d",
                     stardata->star[0].mass,
                     corestring[0],
                     Short_stellar_type_string(stardata->star[0].stellar_type),
                     envelope_mass(&stardata->star[0]),
                     stardata->star[0].phase_start_mass,
                     Teff(0),
                     stardata->star[0].age,
                     stardata->star[1].mass,
                     corestring[1],
                     Short_stellar_type_string(stardata->star[1].stellar_type),
                     envelope_mass(&stardata->star[1]),
                     stardata->star[1].phase_start_mass,
                     Teff(1),
                     stardata->star[1].age,
                     stardata->common.orbit.separation,
                     stardata->common.orbit.period, //y
                     stardata->common.orbit.period*YEAR_LENGTH_IN_DAYS,
                     Angular_momentum_code_to_cgs(stardata->common.orbit.angular_momentum),
                     r1,
                     m ? 0.0 : rl1,
                     m ? 0.0 : (r1/Max(1e-10,rl1)),
                     r2,
                     m ? 0.0 : rl2,
                     m ? 0.0 : (r2/Max(1e-10,rl2)),
                     donor->SN_type,
                     accretor->SN_type,
                     Yesno(System_is_single),
                     merger_info,
                     events_pending(stardata)
        );
    if(merger_info_alloced == TRUE)
    {
        Safe_free(merger_info);
    }
    Safe_free(corestring[0]);
    Safe_free(corestring[1]);

    if(0)Printf(
        "POST_COMENV prob=%20.12g M1=%20.12g Mc1=%20.12g (Menv1=%20.12g) st1=%d age=%20.12g; M2=%20.12g Mc2=%20.12g st2=%d age=%20.12g PER=%20.12g SEP=%20.12g\n",
        stardata->model.probability,
        stardata->star[0].mass,
        Outermost_core_mass(&stardata->star[0]),
        envelope_mass(&stardata->star[0]),
        stardata->star[0].stellar_type,
        stardata->star[0].age,
        stardata->star[1].mass,
        Outermost_core_mass(&stardata->star[1]),
        stardata->star[1].stellar_type,
        stardata->star[1].age,
        stardata->common.orbit.period*YEAR_LENGTH_IN_DAYS,
        stardata->common.orbit.separation
        );

    /* free memory */
    free_BSE_data(&bse1);
    free_BSE_data(&bse2);
    CEprint("orbit out: separation %g, eccentricity %g, period %g, angular momentum %g, angular frequency %g\n",
            stardata->common.orbit.separation,
            stardata->common.orbit.eccentricity,
            stardata->common.orbit.period,
            stardata->common.orbit.angular_momentum,
            stardata->common.orbit.angular_frequency
        );
    Foreach_star(star)
    {
        CEprint("star %d out: type %d, mass %g Msun, core mass %g Msun, radius %g Rsun, luminosity %g Lsun, age %g, omega %g, angular momentum %g\n",
                star->starnum,
                star->stellar_type,
                star->mass,
                Outermost_core_mass(star),
                star->radius,
                star->luminosity,
                star->age,
                star->omega,
                star->angular_momentum);
        star->deny_SN--;
    }

    Dprint("Comenv returns BINARY_C_NORMAL_EXIT");
    return BINARY_C_NORMAL_EXIT;
}
/************************************************************/


double Pure_function nelemans_separation(const double ai, // initial separation
                                         const double Mi, // initial giant mass (Mg)
                                         const double mi, // initial companion mass // m
                                         const double Mf, // final giant mass (Mc)
                                         const double mf, // final companion mass // m
                                         const double gamma,
                                         struct stardata_t * Restrict const stardata)
{
    const double mitot=Max(TINY,Mi+mi);
    const double x=Max(TINY,Mf*mf);
    double af=(ai*((Mf+mf)/mitot)*Pow2((Mi*mi)/x*
                                       (1.0-gamma*(Mi-Mf)/mitot)));

    Dprint("nelemans separation: Mi=%g mi=%g Mf=%g mf=%g\n",Mi,mi,Mf,mf);
    Dprint("Separation was %g now %g (factors %g %g %g)\n",ai,af,
           ai,
           ((Mf+mf)/mitot),
           Pow2((Mi*mi)/x*
                (1.0-gamma*(Mi-Mf)/mitot))
        );

    /*
     * Alternative formula from his later paper, which gives
     * the same result - phew!
     */
    /*
      double Mg=Mi, m=mi, Mc=Mf, Me=Mg-Mc;
      double q=Mg/m, mu=Mc/Mg, Delta=Me/Mg, Dq=Delta*q/(1.0+q);

      double bracket1=(1-gamma*Dq)/(1-Delta);
      double bracket2=(1.0-Dq);
      Dprint("Alternative formula: %g (brackets=%g,%g)\n",
      ai * Pow2(bracket1) * bracket2,bracket1,bracket2
      );
    */
    /*
     * check the j-factor, if it is >=1.0 then return zero
     * because the binary merges
     */
    double f=nelemans_j_factor(Mi,mi,Mf,mf,gamma,stardata);
    if(f>=1.0) af=0.0;

    Dprint("J factor=%g so final separation is %g\n",f,af);


    return (af) ;
}

/************************************************************/

double Pure_function nelemans_j_factor(const double Mi,
                                       const double mi,
                                       const double Mf,
                                       const double mf,
                                       const double gamma,
                                       struct stardata_t * Restrict const stardata)
{
    // gamma * Delta M / (M_binary)
    double f = gamma *(Mi+mi-Mf-mf) /(Mi+mi);
    Dprint("Nelemans J factor (gamma=%g deltaM=%g Mtotinit=%g) = %g\n",
           gamma,Mi+mi-Mf-mf,Mi+mi,f);

    /*
     * f must be in the range 0 to nelemans_max_frac_j_change
     * (note: this is very artificial)
     */
    Clamp(f,0.0,stardata->preferences->nelemans_max_frac_j_change);

    return f;
}

/************************************************************/



static double comenv_dm_bisector_wrapper(const double menv,
                                         void * p);

static double comenv_dm_bisector(const double menv,
                                 struct star_t * s,
                                 struct star_t * star,
                                 struct stardata_t * stardata,
                                 const double Rtarget);

static double comenv_dm_bisector_wrapper(const double menv,
                                         void * p)
{

    Map_GSL_params(p,args);
    struct star_t *s = va_arg(args,struct star_t *);
    struct star_t * star = va_arg(args,struct star_t *);
    struct stardata_t * stardata = va_arg(args,struct stardata_t *);
    double Rtarget = va_arg(args,double);
    va_end(args);
    return comenv_dm_bisector(menv,s,star,stardata,Rtarget);
}

static double comenv_stripped_radius(struct star_t * star,
                                     struct stardata_t * stardata,
                                     const double Rtarget)
{
    /*
     * Find the stripped (remnant) radius of the star.
     */
    struct star_t * s = New_star_from(star);
    s->rzams = star->rzams;
    s->core_radius = star->core_radius;
    s->TAMS_radius = star->TAMS_radius;

    const double f = comenv_dm_bisector(0.0,
                                        s,
                                        star,
                                        stardata,
                                        Rtarget);
    /*
     * f = R/Rtarget - 1
     *
     * Rtarget * (f+1) = R
     */
    const double R = Rtarget * ( f + 1.0 );
    /*
    printf("POST STRIP R=%g (s->radius=%g star->radius=%g)) KW=%d\n",
           R,
           s->radius,
           star->radius,
           s->stellar_type);
    */
    free_star(&s);
    return R;
}

static double comenv_dm_bisector(double menv,
                                 struct star_t * s,
                                 struct star_t * star,
                                 struct stardata_t * stardata,
                                 const double Rtarget)
{
    /*
     * Bisector function for the generic_bisect (Brent)
     * interpolator
     */
    stardata->common.logflag = TRUE;
    const int vb = 0;
    const double ntp = star->num_thermal_pulses;
    if(vb)
    {
        printf("comenv_dm_bisector: menv = %g, star in mass = %g, s mass = %g\n",
               menv,
               star->mass,
               s->mass);
    }
    struct star_t * news = New_star;
    copy_star(stardata,s,news);

    if(vb)
    {
        printf("comenv_dm_bisector: news mass = %g, \n",
               news->mass);
    }
    news->mass = Outermost_core_mass(news) + menv;
    news->stellar_type = s->stellar_type;
    news->core_stellar_type = Core_stellar_type(s->stellar_type);

    /*
     * require timescales, luminosities, etc. to be
     * recalculated
     */
    news->bse->timescales = NULL;
    news->bse->luminosities = NULL;
    news->bse->GB = NULL;

    if(vb)
        printf("comenv_dm_bisector: Call structure with st=%d mc=%g m=%g m0=%g age=%g McHe=%g McCO=%g rzams=%g TAMS_radius=%g Rc=%g j=%g NTP=%g\n",
               news->stellar_type,
               Outermost_core_mass(news),
               news->mass,
               news->phase_start_mass,
               news->age,
               news->core_mass[CORE_He],
               news->core_mass[CORE_CO],
               news->rzams,
               news->TAMS_radius,
               news->core_radius,
               news->angular_momentum,
               star->num_thermal_pulses
            );

    const Boolean deny_was = stardata->model.deny_new_events;
    Set_event_denial(DENY_EVENTS);
    news->deny_SN++;
    stellar_structure(stardata,
                      STELLAR_STRUCTURE_CALLER_common_envelope_evolution_dm_bisector,
                      news,
                      NULL);
    news->deny_SN--;

    /*
     * stellar type change and we still exceed the desired radius
     *
     * try stripping to the core: see if we can reach the radius
     */
    if(news->radius > Rtarget &&
       news->stellar_type != s->stellar_type)
    {
        if(vb)
            printf("comenv_dm_bisector: Remnant radius > target radius: cannot fulfil : stellar type changed(?) from %d to %d\n",
                   s->stellar_type,
                   news->stellar_type);

        news->mass = Outermost_core_mass(s);
        if(vb) printf("new mass = %g : call stellar structure again\n",news->mass);

        news->deny_SN++;
        stellar_structure(stardata,
                          STELLAR_STRUCTURE_CALLER_common_envelope_evolution_dm_bisector,
                          news,
                          NULL);
        news->deny_SN--;

        if(news->radius > Rtarget &&
           (news->stellar_type == HeHG ||
            news->stellar_type == HeGB))
        {
            /*
             * Still exceed required radius : strip again if
             * we can.
             *
             * This tends to happen when an EAGB star is stripped
             * to make a HeGB star which is then stripped down
             * to its carbon core.
             */
            news->mass = Outermost_core_mass(news);
            news->deny_SN++;
            stellar_structure(stardata,
                              STELLAR_STRUCTURE_CALLER_common_envelope_evolution_dm_bisector,
                              news,
                              NULL);
            news->deny_SN--;
        }

        Dprint("new radius %g\n",news->radius);
    }


    if(vb)
    {
        printf("comenv_dm_bisector : type %d (was %d) M = %g, Menv = %g, R = %g vs Rtarget = %g, SN? %d \n",
               news->stellar_type,
               star->stellar_type,
               news->mass,
               menv,
               news->radius,
               Rtarget,
               news->SN_type
            );
    }

    const double f = Bisection_result(Rtarget, news->radius);
    if(vb)
    {
        printf("f= %g\n",f);
    }
    free_star(&news);
    stardata->model.deny_new_events = deny_was;
    star->num_thermal_pulses = ntp;
    return f;
}

void add_envelope_to_remnant(struct stardata_t * const stardata,
                             struct star_t * const donor,
                             const double _envelope_mass,
                             const double Rtarget,
                             const Stellar_type stellar_typein)
{
    /*
     * Rob's additon: add a thin envelope to the remnant
     * of the donor star to better simulate the crossing of
     * the HRD, e.g. post-AGB stars.
     *
     * Rtarget is the target radius for the star, the final
     * mass is set in *mf.
     *
     * Note that it is not perfect:
     * In the case where we are called with a helium star,
     * which was the core of an EAGB star, the final solution
     * often lies outside (in mass coordinates) the original
     * EAGB core... this is not correct !
     *
     * On the other hand, the EAGB star should be stripped to
     * a helium star, according to detailed stellar models.
     * So this is better than leaving a 'compact' EAGB star
     * which is just wrong... perhaps...it's all approximate
     * anyway. :(
     */

#ifdef NANCHECKS
    if(isnan(Rtarget))
    {
        Exit_binary_c(BINARY_C_EXIT_NAN,"Rtarget = NAN\n");
    }
#endif

    if(stardata->preferences->post_ce_adaptive_menv)
    {
        /*
         * Check that the target radius is less than the current
         * radius: if it is *larger* then there is no solution
         */
        if(Rtarget > donor->radius)
        {
            Dprint("Warning: target radius exceeds stellar radius! What to do?\n");
            return;
        }

        /*
         * First, calculate the structure (i.e. L,R and stellar type)
         * of the *completely stripped star*
         */
        char * corestring = core_string(donor,FALSE);
        Dprint("First structure: stellar type was %d, m1=%g %s m1-mc1=%g\n",
               donor->stellar_type,
               donor->mass,
               corestring,
               envelope_mass(donor));
        Safe_free(corestring);

        /* store number of thermal pulses */
        const double ntp = donor->num_thermal_pulses;

        /* make a copy of the star : we work on s */
        struct star_t * s = New_star_from(donor);

        /*
         * Now decide on the stellar type of the final object:
         * If the stripped star is a white dwarf, just add the original envelope
         * back on. However, if it is not a white dwarf it must be some kind of stripped
         * helium core which means the remnant should be a helium star, which is
         * actually what the stripped star is.
         */
        if(s->stellar_type>=HeWD)
        {
            /* it's a WD : use the original giant stellar type */
            s->stellar_type = stellar_typein;
            s->core_stellar_type = MASSLESS_REMNANT;
        }

        const double first_guess = 1e-2;
        const double min = 0.0;
        const double max = _envelope_mass;
        const double tolerance = 1e-10;
        const int itmax = 300;
        int error;

        /*
         * Use Brent routine to find the required menv
         */
        const double alpha = 1.0; // convergence factor
        Dprint("Call bisector with stardata = %p, star = %p, star %d, mass = %g, core mass = %g, Rtarget = %g, min=%g max=%g first_guess=%g\n",
               (void*)stardata,
               (void*)s,
               s->starnum,
               s->mass,
               Outermost_core_mass(s),
               Rtarget,
               min,
               max,
               first_guess
            );

        s->menv = generic_bisect(&error,
                                 BISECT_NO_MONOCHECKS,
                                 BISECTOR_COMENV_DM,
                                 &comenv_dm_bisector_wrapper,
                                 first_guess,
                                 min,
                                 max,
                                 tolerance,
                                 itmax,
                                 BISECTOR_COMENV_DM_USELOG,
                                 alpha,
                                 s,
                                 donor,
                                 stardata,
                                 Rtarget);

        Dprint("bisector error %d, gave menv = %g on type %d\n",
               error,
               s->menv,
               s->stellar_type);

        if(s->stellar_type == EAGB && s->menv < 2.0 * tolerance)
        {
            /*
             * We failed to find a solution for an EAGB star
             * so try a helium star based on its core instead
             */
            Dprint("Failed for EAGB : trying its core alone\n");
            s->mass = s->core_mass[CORE_He];
            s->phase_start_mass = s->mass;
            s->stellar_type = HeGB;
            s->radius = donor->radius;
            char * corestring1 = core_string(s,FALSE);
            Dprint("pre giant age  st=%d m0=%g mt=%g %s age=%g\n",
                   s->stellar_type,
                   s->phase_start_mass,
                   s->mass,
                   corestring1,
                   s->age);
            Safe_free(corestring1);

            giant_age(&s->core_mass[CORE_He],
                      s->mass,
                      &s->stellar_type,
                      &s->phase_start_mass,
                      &s->age,
                      stardata,
                      s);

            corestring = core_string(s,FALSE);
            Dprint("giant age gave st=%d m0=%g mt=%g %s age=%g\n",
                   s->stellar_type,
                   s->phase_start_mass,
                   s->mass,
                   corestring,
                   s->age);
            Safe_free(corestring);
            s->menv = generic_bisect(&error,
                                     BISECT_NO_MONOCHECKS,
                                     BISECTOR_COMENV_DM,
                                     &comenv_dm_bisector_wrapper,
                                     first_guess,
                                     min,
                                     max,
                                     tolerance,
                                     itmax,
                                     BISECTOR_COMENV_DM_USELOG,
                                     alpha,
                                     s,
                                     donor,
                                     stardata,
                                     Rtarget);
        }

        Dprint("bisector error %d, gave menv = %g on type %d\n",
               error,
               s->menv,
               s->stellar_type);


        if(error != BINARY_C_BISECT_ERROR_NONE)
        {
            //Printf("Warning: common envelope bisect failed, setting the remnant with no envelope, which is not what you wanted!\n");
            s->menv = 0.0;
        }

        /* set final mass */
        s->mass = Outermost_core_mass(s) + s->menv;

        Dprint("FINAL kw=%d r=%g target=%g menv=%g\n",
               s->stellar_type,
               s->radius,
               Rtarget,
               s->menv);

        // reset NTP!
        s->num_thermal_pulses = ntp; // just in case!

        copy_star(stardata,s,donor);

        /* clean up memory */
        free_star(&s);
    }
    else
    {
        /*
         * use fixed amount of mass on top of the core
         */
        donor->phase_start_mass = Outermost_core_mass(donor);
        donor->mass = donor->phase_start_mass;

        Dprint("Add fixed envelope : was mass=%g phase_start_mass=%g\n",
               donor->mass,
               donor->phase_start_mass);

        const double dm =
            donor->stellar_type==TPAGB ? POST_CE_ENVELOPE_DM_TPAGB :
            donor->stellar_type==EAGB ? POST_CE_ENVELOPE_DM_EAGB :
            POST_CE_ENVELOPE_DM_GB;

        update_mass(stardata,donor,dm);
        donor->phase_start_mass += dm;

        /*
         * Age star
         */

        giant_age(Outermost_core_mass_pointer(donor),
                  donor->mass,
                  &donor->stellar_type,
                  &donor->phase_start_mass,
                  &donor->age,
                  stardata,
                  donor);
    }
}



#ifdef NUCSYN
void init_comenv_nucsyn(struct stardata_t * stardata)
{
    Mprint("In comenv (nucsyn/MIXDEBUG)\n");
    Foreach_star(star)
    {
        Dprint("Star %d : surface H1=%g He4=%g C12=%g\n",
               star->starnum,
               _Abundance_list3(star->Xenv));

        /* Mix any existing accretion layers into the envelopes */
        nucsyn_mix_accretion_layer_and_envelope(stardata,star,-1.0);


#ifdef COMENV_NORMALIZE_ABUNDS
        nucsyn_renormalize_abundance(star->Xenv);
#endif // COMENV_NORMALIZE_ABUNDS
    }

#ifdef NUCSYN_FORCE_DUP_IN_MERGERS

    Foreach_star(star)
    {
        /*
         * MS stars should perhaps have a first dredge up like event
         * when they are mixed into the envelope... ?
         *
         * HG and GB stars have a helium-rich region which should be mixed
         * into the envelope - this is the first dredge up
         *
         * CHeB (and later) stars have a He-rich core which is
         * (according to Jarrod's thesis, p102, section 3.9.1)
         * dense enough that it cannot be mixed into the envelope
         */
#ifdef NUCSYN_FIRST_DREDGE_UP
        if(((star->stellar_type==HERTZSPRUNG_GAP)||
            (star->stellar_type==GIANT_BRANCH))&&
           (star->first_dredge_up==FALSE)
#ifdef NUCSYN_WR
           &&(star->effective_zams_mass<NUCSYN_WR_MASS_BREAK)
#endif
            )
        {
            nucsyn_set_1st_dup_abunds(star->Xenv,
                                      star->mass,
                                      star->stellar_type,
                                      star,
                                      stardata,
                                      TRUE
                );
            star->first_dredge_up=TRUE;
        }
#endif // NUCSYN_FIRST_DREDGE_UP
#ifdef NUCSYN_STRIP_AND_MIX
        /*
         * Mix down to the core in this star
         */
        mix_down_to_core(star);

        /*
         * And disable strip and mix: we can now
         * just use traditional NUCSYN.
         */
        if(can_use_strip_and_mix(stardata,star)==FALSE)
            star->strip_and_mix_disabled=TRUE;
#endif
    }
#endif // NUCSYN_FORCE_DUP_IN_MERGERS

}

void AGB_merger_nucsyn(Stellar_type stellar_type,
                       double ntp,
                       double ntp_since_mcmin,
                       double prev_tagb,
                       struct star_t * star1,
                       struct stardata_t * stardata )
{
    /*
     * The CO core mass of the new star should be the sum
     * of the CO core masses of the parent stars
     */

    star1->mcx_EAGB=stardata->star[0].mcx_EAGB+stardata->star[1].mcx_EAGB;

    /*
     * Merger makes a TPAGB star : reset number of thermal pulses etc
     * to previously saved values
     */
    if(stellar_type == TPAGB)
    {
        star1->num_thermal_pulses = ntp;
        star1->num_thermal_pulses_since_mcmin=
            ntp_since_mcmin;
        star1->prev_tagb=prev_tagb;
        // don't bother setting star 2 (it no longer exists!)
    }
    else
    {
        star1->num_thermal_pulses=-2.0;
        star1->num_thermal_pulses_since_mcmin=0.0;
        star1->prev_tagb=0.0;
    }
}


#endif //NUCSYN


void set_ntp(double *ntp,
             double *ntp_since_mcmin,
             double *prev_tagb,
             struct stardata_t * stardata)
{
    /*
     * Set the number of thermal pulse of a merged star
     */
    *ntp = -1.0;
    *ntp_since_mcmin = *prev_tagb = 0.0;
    if(stardata->star[0].stellar_type==TPAGB)
    {
        *ntp = stardata->star[0].num_thermal_pulses;
        *ntp_since_mcmin = stardata->star[0].num_thermal_pulses_since_mcmin;
        *prev_tagb = stardata->star[0].prev_tagb;
    }
    if(stardata->star[1].stellar_type==TPAGB)
    {
        // NB if star 1 is not a TPAGB, ntp etc. are zero before this
        *ntp = Max(stardata->star[1].num_thermal_pulses,*ntp);
        *ntp_since_mcmin = Max(stardata->star[1].num_thermal_pulses_since_mcmin,
                               *ntp_since_mcmin);
        *prev_tagb = Max(stardata->star[1].prev_tagb,*prev_tagb);
    }
}




void calc_effective_core_masses(double * const effective_mc1,
                                double *const effective_mc2,
                                const Stellar_type stellar_type1,
                                const Stellar_type stellar_type2,
                                const double m1,
                                const double mc1,
                                const double m2,
                                const double mc2)
{
    /*
     * Calculate the effective core masses - MS stars behave as cores during
     * the spiral in - hence their effective core mass is their total mass.
     * Other stars actually have a well defined core.
     *
     * An exception is a MS star spiralling into a giant:
     * the MS star is absorbed into the envelope so should
     * be treated as coreless.
     */
#define _eff(STELLAR_TYPE,M,MC)                         \
    ON_EITHER_MAIN_SEQUENCE(STELLAR_TYPE) ? (M) :       \
        LATER_THAN_WHITE_DWARF(STELLAR_TYPE) ? (M) :    \
        (MC);

    *effective_mc1 = _eff(stellar_type1,m1,mc1);
    *effective_mc2 = _eff(stellar_type2,m2,mc2);
}


#ifdef MIXDEBUG
/* debug function */
#define DEBUG_LINENUMBERS
void Gnu_format_args(4,5) mixdebug_fprintf(struct stardata_t * const stardata,
                                           const char * const filename ,
                                           const int fileline,
                                           const char * const format,
                                           ...)
{
    /*
     * This function should not be called directly!
     * Use the Mprint macro instead.
     *
     * filename is the source code file with the Dprint statement
     *
     * fileline is the line at which the Dprint statement occurs
     *
     * format is the (printf) format string
     *
     * ... is the variable argument list
     *
     * Output format:
     *
     * Model_Time Filename : Line : message
     *
     */
    va_list args;
    va_start(args,format);

    /* s contains the message */
    char s[MAX_DEBUG_PRINT_SIZE];
    vsnprintf(s,MAX_DEBUG_PRINT_SIZE,format,args);
    chomp(s);

    //Dprint("MIXING ");

    /* make the filename, remove nan e.g. in remnant (replace with n_n) */
    char f[MAX_DEBUG_PRINT_SIZE];
#ifdef DEBUG_COLOURS
    snprintf(f,MAX_DEBUG_PRINT_SIZE,"%s%-40.40s",stardata->store->colours[RED],filename);
#else
    snprintf(f,MAX_DEBUG_PRINT_SIZE,"%-40.40s",filename);
#endif

#ifdef DEBUG_REMOVE_NAN_FROM_FILENAMES
    char *x=strstr(f,"nan");
    if(x!=NULL) *(x)='_';
#endif


    /* output to the appropriate stream and flush */
#ifdef DEBUG_COLOURS
#ifdef DEBUG_LINENUMBERS
    Dprint("%s%g %s%s :%s% 6d%s : %s\n",
           stardata->store->colours[CYAN],stardata->model.time,
           stardata->store->colours[YELLOW],f,
           stardata->store->colours[GREEN],fileline,
           stardata->store->colours[COLOUR_RESET],s);
#else
    Dprint("%s%g %s%s%s : %s\n",
           stardata->store->colours[CYAN],stardata->model.time,
           stardata->store->colours[YELLOW],f,
           stardata->store->colours[COLOUR_RESET],s);
#endif //DEBUG_LINENUMBERS
#else

#ifdef DEBUG_LINENUMBERS
    Dprint("%g %s :% 6d : %s\n",stardata->model.time,f,fileline,s);
#else
    Dprint("%g %s : %s\n",stardata->model.time,f,s);
#endif
#endif
    fflush(DEBUG_STREAM);
    va_end(args);
}

#endif //MIXDEBUG


#ifdef COMENV_ACCRETION

void comenv_accretion(struct stardata_t * stardata,
                      const Star_number nstar1,
                      const Star_number nstar2,
                      double *bse1,
                      double *bse2,
                      double *m1,
                      double *m2,
                      double *m01,
                      double *m02,
                      double *age_donor,
                      double *age_accretor,
                      double *l1,
                      double *l2,
                      double *r1,
                      double *r2,
                      double *menv,
                      double *renv,
                      double *tm1,
                      double *tm2,
                      double *mc1,
                      double *mc2,
                      double *rc1,
                      double *rc2,
                      double *moment_of_inertia_factor1,
                      double *moment_of_inertia_factor2,
                      double *tn,
                      double common_envelope_mass,
                      Stellar_type *stellar_type1,
                      Stellar_type *stellar_type2
    )


{
#ifdef COMENV_NS_ACCRETION
    if((stardata->preferences->comenv_ns_accretion_fraction >TINY)||
       (stardata->preferences->comenv_ns_accretion_mass >TINY))
    {
        double maccrete;
        if(stardata->preferences->comenv_ns_accretion_fraction>0.0)
        {
            /*
             * Accrete a fraction comenv_ns_accretion_factor of the
             * envelope onto the neutron star: logging of AIC NS->BH
             * is taken care of in stellar_structure, but you might want to output
             * extra things here
             */
            maccrete=stardata->preferences->comenv_ns_accretion_fraction*common_envelope_mass;

            Dprint("Maccrete = %g from fraction = %g * Mcomenv =%g\n",
                   maccrete,
                   stardata->preferences->comenv_ns_accretion_fraction,
                   common_envelope_mass);

        }
        else
        {
            /*
             * Accrete a fixed mass during common envelope,
             * but of course this cannot be > the mass in the
             * common envelope...
             */
            maccrete=Min(stardata->preferences->comenv_ns_accretion_mass,common_envelope_mass);

            Dprint("Maccrete = %g from fixed mass = Min(%g , Mcomenv =%g)\n",
                   maccrete,
                   stardata->preferences->comenv_ns_accretion_mass,
                   common_envelope_mass);

        }
        Dprint("Maccrete %g\n",maccrete);

        int dohrd=0;
        if(*stellar_type2==NEUTRON_STAR)
        {
            Dprint("Accrete %g onto NS (2)\n",maccrete);
            *m2+=maccrete;
            *m1-=maccrete;
            dohrd=1;
        }
        else if(*stellar_type1==NEUTRON_STAR)
        {
            // do we *EVER* get here?
            maccrete*=-1.0; // reverse accretion direction
            Dprint("Accrete %g onto NS (1)\n",maccrete);
            *m2+=maccrete;
            *m1-=maccrete;
            dohrd=1;
        }

        if(dohrd==1)
        {
            stellar_structure(m01,
                              age_donor,
                              m1,
                              tm1,
                              tn,
                              bse1->timescales,
                              bse1->luminosities,
                              bse1->GB,
                              r1,
                              l1,
                              stellar_type1,
                              mc1,
                              mcCO1,
                              mcGB1,
                              mcmaxMS1,
                              rc1,
                              menv,
                              renv,
                              moment_of_inertia_factor1,
                              &(stardata->star[donor->starnum]),
                              stardata,
                              STELLAR_STRUCTURE_CALLER_common_envelope_evolution);

            stellar_structure(m02,
                              age_accretor,
                              m2,
                              tm2,
                              tn,
                              bse2->timescales,
                              lums,
                              GB,
                              r2,
                              l2,
                              stellar_type2,
                              mc2,
                              &accretor->core_mass[CORE_CO],
                              &Outermost_core_mass(accretor),
                              &accretor->max_MS_core_mass,
                              rc2,
                              menv,
                              renv,
                              moment_of_inertia_factor2,
                              &(stardata->star[accretor->starnum]),
                              stardata,STELLAR_STRUCTURE_CALLER_common_envelope_evolution);

        }

    }
#endif // COMENV_NS_ACCRETION

#if defined NUCSYN && defined COMENV_MS_ACCRETION
    /*
     * Allow accretion onto any pre-WD star.
     */
    if((stardata->preferences->comenv_ms_accretion_mass >TINY)&&
       (*stellar_type2<HeWD))
    {
        double maccrete=maccrete=Min(stardata->preferences->comenv_ms_accretion_mass,common_envelope_mass);
        Mprint("Comenv accrete: mass=%g Pre-CE secondary [C/Fe]=%g\n",
               maccrete,
               nucsyn_elemental_square_bracket("C","Fe",stardata->star[1].Xenv,stardata->common.Xsolar,stardata)
            );
        *m2+=maccrete;
        *m1-=maccrete;
        *m02+=maccrete;

        /*
         * put the material from donor->starnum into accretor->starnum's accretion layer
         */
        star2->dmacc=maccrete;
        Copy_abundances(star1->Xenv,star2->Xacc);

        /* and perhaps mix it in */
        if(stardata->preferences->no_thermohaline_mixing==FALSE)
        {
            nucsyn_mix_accretion_layer_and_envelope(&(stardata->star[1]),-1.0);
        }


        /* rejuvenation :
         * Algorithm stolen from
         * rejuvenate_MS_secondary_and_age_primary
         * function (though here we do not age the primary,
         * it has enough trouble losing its envelope!)
         */
        calc_lum_and_evol_time(*stellar_type2,
                               *m02,
                               *m2,
                               tm2,
                               tn,
                               bse2->timescales,
                               lums,
                               GB,
                               stardata,
                               &(stardata->star[accretor->starnum]));

        Dprint("Rejuvenate MS star by a factor : %g\n",*tm2/star2->tms*
               (*m02 - maccrete)/(*m02));

        /*
         * perturb the age for MS stars
         * age of all other stars depends on core mass,
         * so a call to giant_age is required
         */
        if(*stellar_type2<=1)
        {
            /* MS stars */
            *age_accretor *= *tm2/star2->tms*
                (*m02 - maccrete)/(*m02);
        }

        /* should we set the epoch? seems to make little difference */
        star2->epoch = stardata->model.time - *age_accretor;

        /* call stellar_structure to reset stellar properties */
        Dprint("call stellar structure 2\n");
        stellar_structure(m02,
                          age_accretor,
                          m2,
                          tm2,
                          tn,
                          bse2->timescales,
                          bse2->luminosities,
                          bse2->GB,
                          r2,
                          l2,
                          stellar_type2,
                          mc2,
                          &accretor->core_mass[CORE_CO],
                          &Outermost_core_mass(accretor),
                          &accretor->max_MS_core_mass,
                          rc2,
                          menv,
                          renv,
                          moment_of_inertia_factor2,
                          &(stardata->star[accretor->starnum]),
                          stardata,
                          STELLAR_STRUCTURE_CALLER_common_envelope_evolution);


        Mprint("Post-CE secondary [C/Fe]=%g\n",
               nucsyn_elemental_square_bracket("C","Fe",stardata->star[1].Xenv,stardata->common.Xsolar,stardata)
            );
    }

#endif // NUCSYN && COMENV_MS_ACCRETION
}
#endif // COMENV_ACCRETION

double stellar_radius(const Stellar_type stellar_type,
                      const double mass,
                      const double mc,
                      const double lum,
                      const double age,
                      const double z,
                      struct star_t * star,
                      struct stardata_t * stardata
    )
{
    double r;
    /*
     * The stellar radius as a function of mass for
     * constant luminosity : extracted from stellar_structure,
     * only for non-compact stars (i.e. the star which
     * initiated the mass transfer/comenv)
     */

    double mflash = stardata->common.metallicity_parameters[ZPAR_MASS_HE_FLASH];

    Dprint("Radius from kw=%d m=%g lum=%g mflash=%g z=%g ... =",
           stellar_type,mass,lum,mflash,z);

    /* radius */
    r = stellar_type<CHeB || NAKED_HELIUM_STAR(stellar_type)
        ? rgbf(mass,lum,stardata->common.giant_branch_parameters)
        : ragbf(mass,lum,mflash,stardata->common.giant_branch_parameters
#ifdef AXEL_RGBF_FIX
                ,z
#endif
            );

    /* remnant radius */
    const double rr = remnant_radius(stardata,
                                     star,
                                     stardata->common.metallicity_parameters,
                                     stellar_type,
                                     mass,
                                     mc,
                                     r);

    /* core and luminosity (assumed constant) */

    const double mtc = NAKED_HELIUM_STAR(stellar_type) ? Min(mass,(1.450*(mass)-0.310)) : 0.0;

    /*
     * Rewrite to use stellar_structure structs
     */
    struct star_t * s = New_star_from(star);
    set_no_core(s);
    const Core_type core_id = ID_core(stellar_type);
    if(core_id != CORE_NONE)
    {
        s->core_mass[core_id] = mc;
    }
    s->mass = mass;
    s->phase_start_mass = mass;
    s->age = age;
    s->menv = mass - mc;
    s->luminosity = lum;
    s->radius = r;
    s->stellar_type = stellar_type;
    s->core_stellar_type = Core_stellar_type(stellar_type);

    /* apply small envelope perturbations *at constant L* */
    /*
      double l=lum,rc;
      stellar_structure_small_envelope_perturbations(stellar_type,
      mass,
      mc,
      &l,
      &r,
      &rc,
      mtc,
      lum,
      rr);
    */
    stellar_structure_small_envelope_perturbations(stardata,
                                                   s,
                                                   mtc,
                                                   lum,
                                                   rr);

    Dprint("%g (remn-ant rr=%g)\n",r,rr);

    r = s->radius;
    free_star(&s);
    return r;
}

Maybe_unused  double dmdr(struct star_t * star,
                          struct stardata_t * stardata)
{
    /* dM/dR using the stellar_radius function */
    double retval;
    double dm=1e-6;
    double rlow = stellar_radius(star->stellar_type,
                                 star->mass,
                                 Outermost_core_mass(star),
                                 star->luminosity,
                                 star->age,
                                 stardata->common.metallicity,
                                 star,
                                 stardata);
    double rhigh = stellar_radius(star->stellar_type,
                                  star->mass+dm,
                                  Outermost_core_mass(star),
                                  star->luminosity,
                                  star->age,
                                  stardata->common.metallicity,
                                  star,
                                  stardata);

    retval = (rhigh-rlow)/dm;

    Dprint("DMDR rhigh=%g rlow=%g dm/dr=%g\n",
           rhigh,rlow,retval);

    return retval;
}




static void comenv_calc_roche_lobe_radii(const double sep,
                                         const double ecc,
                                         const double m1,
                                         const double m2,
                                         double * rl1,
                                         double * rl2)
{
    /*
     * NB usually ecc=0, but if it's not
     * calculate at periastron (hence 1-ecc factor).
     */
    *rl1 = Roche_radius3(m1,m2,sep) * (1.0 - ecc);
    *rl2 = Roche_radius3(m2,m1,sep) * (1.0 - ecc);
}

#endif //BSE
