#pragma once

#ifndef COMMON_ENVELOPE_EVOLUTION_BINARY_C_H
#define COMMON_ENVELOPE_EVOLUTION_BINARY_C_H

#include "common_envelope_evolution_binary_c.def"

enum {
    CE_PRE,
    CE_DURING,
    CE_MERGED,
    CE_POST,
    CE_STATE_NUMBER
};

#undef X
#define X(TYPE, NUMBER) COMENV_ENVELOPE_RESPONSE_##TYPE = NUMBER,
enum {
    COMENV_ENVELOPE_RESPONSES_LIST
};
#undef X

#define _Update_stellar_structures(...)         \
    _update_stellar_structures(_CE_args,        \
                               __VA_ARGS__,     \
                               NULL);           \
    _erase_events(_CE_args);

#define _Reset_orbit(...)                       \
    _reset_orbit(_CE_args,__VA_ARGS__,NULL)

#define LARGE_ENERGY (1e100)
//#define LARGE_LAMBDA (1e100)

#define _CE_args donor,accretor,stardata
#define _CE_args_in struct star_t * const donor Maybe_unused,   \
        struct star_t * const accretor Maybe_unused,            \
        struct stardata_t * const stardata Maybe_unused

struct common_envelope_state_t {
    struct star_t * donor;
    struct star_t * accretor;

    /* stars */
    double phase_start_mass_donor;
    double phase_start_mass_accretor;
#ifdef NUCSYN
    Abundance * Xenv_donor;
    Abundance * Xenv_accretor;
#endif // NUCSYN
    double mass_total;
    double baryonic_mass_total;
    double mass_reduced;
    double mass_donor;
    double mass_accretor;
    double baryonic_mass_donor;
    double baryonic_mass_accretor;
    double mass_convective_envelope_donor;
    double mass_convective_envelope_accretor;
    double core_mass_donor;
    double core_mass_accretor;
    double baryonic_core_mass_donor;
    double baryonic_core_mass_accretor;
    double effective_core_mass_donor;
    double effective_core_mass_accretor;
    double effective_baryonic_core_mass_donor;
    double effective_baryonic_core_mass_accretor;
    double effective_envelope_mass_donor;
    double effective_envelope_mass_accretor;
    double luminosity_donor;
    double luminosity_accretor;
    double radius_donor;
    double radius_accretor;
    double effective_core_radius_donor;
    double effective_core_radius_accretor;
    double zams_radius_donor;
    double zams_radius_accretor;
    double roche_radius_donor;
    double roche_radius_accretor;
    double core_radius_donor;
    double core_radius_accretor;
    double roche_radius_donor_core;
    double roche_radius_accretor_core;
    double roche_radius_donor_effective_core;
    double roche_radius_accretor_effective_core;
    double lambda_donor;
    double lambda_accretor;
    double moment_of_inertia_factor_donor;
    double moment_of_inertia_factor_accretor;
    double binding_energy_donor;
    double binding_energy_accretor;

    /* summed stars */
    double stellar_angular_momentum;
    double stellar_cores_spin_angular_momentum;
    double stellar_cores_orbital_angular_momentum;
    double stellar_cores_total_angular_momentum;

    /* orbit */
#undef X
#define X(VAR,FAC,UNIT) double orbital_##VAR;
    _CE_ORBIT_VARIABLES_LIST
#undef X
    double angular_frequency_donor;
    double angular_frequency_accretor;
    double angular_frequency_orbit;

    /* common envelope */
    double total_angular_momentum;
    double common_envelope_angular_momentum;
    double common_envelope_mass;
    double common_envelope_radius;

    double lambda;

    /* changes to the common envelope */
    double delta_orbital_energy;
    double delta_orbital_angular_momentum;

    /* free parameters */
    double alpha;
    double gamma;

    /* AGB data */
    double ntp;
    double ntp_since_mcmin;

#ifdef NUCSYN
    Abundance common_envelope_abundance[ISOTOPE_ARRAY_SIZE];
#endif
    double binding_energy;
    double circular_orbital_energy;
    Stellar_type stellar_type_donor;
    Stellar_type stellar_type_accretor;
    int phase;
    int prescription;
    int envelope_response;
    int lambda_logged;
    Boolean coalesce;
    Boolean update_orbital_energy;
    Boolean update_orbital_angular_momentum;
    Boolean TZO_donor;
    Boolean TZO_accretor;
    Boolean Politano2021;
};
#include "common_envelope_evolution_binary_c_prototypes.h"

#endif // COMMON_ENVELOPE_EVOLUTION_BINARY_C_H
