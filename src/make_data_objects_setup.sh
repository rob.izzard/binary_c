#!/bin/bash
#
# setup for make_data_objects_scripts
#
# to be sourced by make_data_objects scripts
#
# see also
# http://gareus.org/wiki/embedding_resources_in_executables

: ${CC:="gcc"}
: ${OBJCOPY_ARCH:=$(objdump -f /bin/bash |grep architecture | gawk "{print \$2}" | sed s/,//)}
: ${OBJCOPY_TARGET:=$(objdump -f /bin/bash  | grep format | gawk "{print \$4}")}
: ${OBJCOPY_OPTS:="-I binary -B $OBJCOPY_ARCH -O $OBJCOPY_TARGET"}

echo "CC=$CC"
echo "OBJCOPY_OPTS=$OBJCOPY_OPTS"
echo "OBJCOPY_TARGET=$OBJCOPY_TARGET"
echo "OBJCOPY_ARCH=$OBJCOPY_ARCH"
