#pragma once
#ifndef STRING_MACROS_H
#define STRING_MACROS_H

/*
 * The binary_c stellar population nucleosynthesis framework.
 *
 * Copyright Robert Izzard 2000-2025
 *
 * Contact rob.izzard@gmail.com
 *
 * Code repository:
 * https://gitlab.com/binary_c
 *
 * Documentation is in doc/
 * Code is in src/
 *
 * To build, run
 *
 * ---
 *
 * meson setup builddir
 * cd builddir
 * ninja binary_c_install
 *
 * ---
 *
 * Please see the files README, LICENCE and CHANGES,
 * and the doc/ directory for documentation which is mirrored
 * at https://binary_c.gitlab.io
 *
 **********************
 *
 * Purpose:
 *
 * In this file are macros to deal with macros as strings.
 * These are used e.g. in the version() function.
 */


enum {
    STRING_COPY,
    STRING_POINTERS
};

#define Slash() /
#define Double_slash() Slash()Slash()

/* stringize macro to output macro names */
#define Stringize(x) #x
/* macro to output value of a macro as a string */
#define Stringof(x) Stringize(x)


/*
 * Macros to say yes/no, true/false, in captials and colour too
 */
#define _POSITIVE_COLOUR stardata->store->colours[BRIGHT_GREEN]
#define _NEGATIVE_COLOUR stardata->store->colours[BRIGHT_RED]

#define Yesno(B) ((B)==TRUE ? "Yes" : "No")
#define YESNO(B) ((B)==TRUE ? "YES" : "NO")
#define Colouryesno(X)                                  \
    ((X)==FALSE ? _NEGATIVE_COLOUR : _POSITIVE_COLOUR), \
        Yesno(X),                                       \
        stardata->store->colours[COLOUR_RESET]
#define ColourYESNO(X)                                  \
    ((X)==FALSE ? _NEGATIVE_COLOUR : _POSITIVE_COLOUR), \
        YESNO(X),                                       \
        stardata->store->colours[COLOUR_RESET]
#define Truefalse(B) ((B)==TRUE ? "True" : "False")
#define TRUEFALSE(B) ((B)==TRUE ? "TRUE" : "FALSE")
#define Colourtruefalse(X)                              \
    ((X)==FALSE ? _NEGATIVE_COLOUR : _POSITIVE_COLOUR), \
        Truefalse(X),                                   \
        stardata->store->colours[COLOUR_RESET]
#define ColourTRUEFALSE(X)                              \
    ((X)==FALSE ? _NEGATIVE_COLOUR : _POSITIVE_COLOUR), \
        TRUEFALSE(X),                                   \
        stardata->store->colours[COLOUR_RESET]


/*
 * Macros to prevent us using strcmp and strcasecmp while
 * associatedly forgetting the ==0
 */
#define Strings_equal(A,B)                      \
    (strcmp((A),(B))==0)
#define Strings_equal_case_insensitive(A,B)     \
    (strcasecmp((A),(B))==0)
#define String_startswith(A,B)                  \
    (strncmp((A),(B),strlen(B))==0)

#define Streqig(X,A) ((X)==(A) || (X)==(A)- 'a' + 'A')

/*
 * Macro to match a string S to true.
 *
 * An empty string is FALSE.
 */
#define String_is_true(S)                                               \
    (                                                                   \
        (                                                               \
            (S)[0] != '\0' &&                                           \
            (                                                           \
                ((S)[0]=='1' && (S)[1]=='\0')                           \
                ||                                                      \
                (                                                       \
                    ((S)[0] == 'T' ||                                   \
                     (S)[0] == 't') &&                                  \
                    (                                                   \
                        Strings_equal_case_insensitive((S),"T") ||      \
                        Strings_equal_case_insensitive((S),"TRUE")      \
                        )                                               \
                    )                                                   \
                ||                                                      \
                (                                                       \
                    ((S)[0] == 'Y' ||                                   \
                     (S)[0] == 'y') && (                                \
                         Strings_equal_case_insensitive((S),"Y") ||     \
                         Strings_equal_case_insensitive((S),"YES")      \
                         )                                              \
                    )                                                   \
                ||                                                      \
                (                                                       \
                    ((S)[0] == 'O' || (S)[0] == 'o') &&                 \
                    (                                                   \
                        Strings_equal_case_insensitive((S),"ON")        \
                        )                                               \
                    )                                                   \
                )                                                       \
            )                                                           \
        ? TRUE : FALSE)

/*
 * Macro to match a string S to FALSE.
 *
 * An empty string is FALSE.
 */
#define String_is_false(S)                                              \
    (                                                                   \
        (                                                               \
            (S)[0] == '\0'                                              \
            ||                                                          \
            (                                                           \
                ((S)[0] == '0' && (S)[1] == '\0')                       \
                ||                                                      \
                (                                                       \
                    ((S)[0] == 'F' ||                                   \
                     (S)[0] == 'f') &&                                  \
                    (                                                   \
                        Strings_equal_case_insensitive((S),"F") ||      \
                        Strings_equal_case_insensitive((S),"FALSE")     \
                        )                                               \
                    )                                                   \
                ||                                                      \
                (                                                       \
                    ((S)[0] == 'N' ||                                   \
                     (S)[0] == 'n') &&                                  \
                    (                                                   \
                        Strings_equal_case_insensitive((S),"N") ||      \
                        Strings_equal_case_insensitive((S),"NO")        \
                        )                                               \
                    )                                                   \
                ||                                                      \
                (                                                       \
                    ((S)[0] == 'O' ||                                   \
                     (S)[0] == 'o') &&                                  \
                    (                                                   \
                        Strings_equal_case_insensitive((S),"Off")       \
                        )                                               \
                    )                                                   \
                )                                                       \
            )                                                           \
        ? TRUE : FALSE)

#define String_is_boolean(S)                    \
    (                                           \
        (                                       \
            Arg_is_true(S) ||                   \
            Arg_is_false(S)                     \
            )                                   \
        ? TRUE: FALSE)



/*
 * Given a PATH string, construct the filename
 * without the leading path
 */
#define Filename(PATH)                          \
    __extension__                               \
    ({                                          \
        char * const c = strrchr(PATH,          \
                           PATH_SEPARATOR);     \
        c[0] == PATH_SEPARATOR ? (c+1) : c;     \
    })


#endif // STRING_MACROS_H
