#!/bin/bash

randf() {
    # random float between $1 and $2
    RAND=`bc -l <<< "scale=4 ; $((RANDOM % 10000 ))/10000 * ($2-$1)"`
}
randi() {
    # random integer between 0 and $1 (not inclusive)
    RAND=$(( RANDOM % $1 ))
}

randf 0.1 100
M1=$RAND # Msun
randf 0.1 $M1
M2=$RAND # Msun
randi 15
STELLAR_TYPE1=$RAND
randi 15
STELLAR_TYPE2=$RAND
randf 0.1 1000.0
PER=$RAND # days
randf 0.0 3.0
ALPHA=$RAND

COMENV_EJECTION_SPIN_METHOD=COMENV_EJECTION_SPIN_METHOD_SYNCHRONIZE_IF_POSSIBLE
DISABLE_DEBUG=1
POST_CE_ADAPTIVE_MENV=1
POST_CE_OBJECTS_HAVE_ENVELOPES=1

CMD="tbse valgrind M_1 $M1 M_2 $M2 stellar_type_1 $STELLAR_TYPE1 stellar_type_2 $STELLAR_TYPE2 orbital_period $PER alpha_ce $ALPHA allow_debug_nan true comenv_ejection_spin_method $COMENV_EJECTION_SPIN_METHOD debug_filter_pointers 1 derivative_logging 0 sn_kick_dispersion_IBC 0 sn_kick_dispersion_II 0 post_ce_objects_have_envelopes $POST_CE_OBJECTS_HAVE_ENVELOPES post_ce_adaptive_menv $POST_CE_ADAPTIVE_MENV disable_debug $DISABLE_DEBUG  "

LIST="old new"
i=0
for X in $LIST;
do
    if [[ $X == 'old' ]]; then
        CODE="LEGACY"
    else
        CODE="BINARY_C"
    fi
    COMMAND="$CMD comenv_code COMENV_CODE_$CODE  log_filename /tmp/c_log.$X"
    echo $i : $COMMAND
    $COMMAND | ANSIstrip.pl > /tmp/$X.debug.out &
    pid[$i]=$! # save PID for wait
    i=$(( $i+1 ))
done

i=0
for X in $LIST;
do
    wait ${pid[$i]}
    ret[$i]=$?
    echo "Return code $i : ${ret[$i]}"
    i=$(( $i+1 ))
done

for X in $LIST;
do
    cat /tmp/$X.debug.out | filter CEE > /tmp/$X.out
done


if [[ `diff /tmp/old.out /tmp/new.out` ]]; then
    echo "Diff detected in CEE logs : please check"
fi
