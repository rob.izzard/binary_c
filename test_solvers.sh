#!/bin/bash

############################################################

# early merger
#SYSTEM="M_1 1 M_2 1 separation 1 eccentricity 0.5"

# tidally interacting 
SYSTEM="M_1 1 M_2 1 separation 4 eccentricity 0.5"
#SYSTEM="M_1 10 M_2 10 separation 20000 eccentricity 0.5"
#SYSTEM="M_1 30 M_2 1 separation 2000 eccentricity 0.9"
#SYSTEM="M_1 0.5 M_2 0.2 separation 10 eccentricity 0.9"

############################################################

PRE="time tbse orbital_period 0 $SYSTEM max_evolution_time 10 repeat 1 $*"
OUT="/tmp"
OUT="$HOME/tex/notes/solvers"
OUT="$HOME/tex/notes/solvers/various"
OUT="$HOME/tex/notes/solvers/various0.1"
OUT="$HOME/tex/notes/solvers/various0.01"
OUT="$HOME/tex/notes/solvers/various0.001"
OUT="$HOME/tex/notes/solvers/2nd_deriv"
OUT="$HOME/tex/notes/solvers/anal"


# make outdir
mkdir -p "$OUT"

echo "Forward Euler"
CMD="$PRE --log_filename $OUT/c_log.euler --solver 0"
echo "$CMD"

#"$CMD" maximum_timestep 1000 | filter JJJ > "$OUT"/jjj.1000.euler
#"$CMD" maximum_timestep 1 | filter JJJ > "$OUT"/jjj.1.euler
#"$CMD" maximum_timestep 0.1 | filter JJJ > "$OUT"/jjj.0.1.euler
#"$CMD" maximum_timestep 0.01 | filter JJJ > "$OUT"/jjj.0.01.euler
"$CMD" | filter JJJ > "$OUT"/jjj.euler

echo "RK2"
CMD="$PRE --log_filename $OUT/c_log.RK2 --solver 1" 
echo "$CMD"
"$CMD" | filter JJJ > "$OUT"/jjj.RK2

echo "RK4"
CMD="$PRE --log_filename $OUT/c_log.RK4 --solver 2" 
echo "$CMD"
"$CMD" | filter JJJ > "$OUT"/jjj.RK4

echo "PEC"
CMD="$PRE --log_filename $OUT/c_log.PEC --solver 3" 
echo "$CMD"
"$CMD" | filter JJJ > "$OUT"/jjj.PEC

# at 1Gyr: Euler = 2.75, RK2 = 2.9 : RK2 is more correct
